<?php

namespace MommyCom;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\HttpKernel\KernelInterface;

class ConsoleApplication extends Application
{
    /**
     * ConsoleApplication constructor.
     *
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        parent::__construct($kernel);

        $inputDefinition = $this->getDefinition();
        $inputDefinition->addOption(new InputOption('--app', null, InputOption::VALUE_REQUIRED, 'The Application name.', 'console'));
    }
}
