<?php

namespace MommyCom\Model\BackendTablet;

use CFormModel;
use MommyCom\YiiComponent\BackendTablet\AdmUserIdentity;
use Yii;

class AuthForm extends CFormModel
{
    /**
     * @var string
     */
    public $login = '';

    /**
     * @var string
     */
    public $password = '';

    /**
     * @var bool
     */
    public $remember = false;

    /**
     * @var AdmUserIdentity|null
     */
    private $identity = null;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['login, password', 'required'],
            ['password', 'authenticate'],
        ];
    }

    /**
     * @return void
     */
    public function authenticate()
    {
        if (!$this->hasErrors()) {
            $this->identity = new AdmUserIdentity($this->login, $this->password);
            if (!$this->identity->authenticate()) {
                $this->addError('password', $this->identity->errorMessage);
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return bool
     * @throws \CException
     */
    public function login()
    {
        if (null === $this->identity) {
            $this->identity = new AdmUserIdentity($this->login, $this->password);
            $this->identity->authenticate();
        }

        if ($this->identity->errorCode === AdmUserIdentity::ERROR_NONE) {
            $duration = 12 * 60 * 60;
            Yii::app()->user->login($this->identity, $duration);

            return true;
        }

        return false;
    }
}
