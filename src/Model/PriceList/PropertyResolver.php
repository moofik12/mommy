<?php

namespace MommyCom\Model\PriceList;

use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Model\Product\SizeFormats;

final class PropertyResolver
{
    /**
     * @param GroupedProduct $product
     *
     * @return array
     */
    public static function paramTarget($product)
    {
        static $validTargets = [
            ProductTargets::TARGET_BOY => 'для мальчиков',
            ProductTargets::TARGET_GIRL => 'для девочек',
            ProductTargets::TARGET_MAN => 'мужской',
            ProductTargets::TARGET_WOMAN => 'женский',
        ];

        $values = [];
        foreach ($product->getTargets() as $target) {
            if (isset($validTargets[$target])) {
                $values[] = $validTargets[$target];
            }
        }

        if (!empty($values)) {
            return [
                'label' => 'Пол',
                'value' => implode(', ', $values),
            ];
        }

        return [];
    }

    /**
     * @param GroupedProduct $product
     *
     * @return string|null
     */
    public static function typePrefix($product)
    {
        static $types = [
            'блуза', 'блузка', 'боди', 'болеро', 'ботинки', 'брюки', 'бриджи', 'бюстгалтер',
            'вешалка',
            'гольф',
            'джемпер', 'джинсы',
            'жакет', 'жилет',
            'заварник', 'зонт',
            'кальсоны', 'капри', 'кардиган', 'кенгуру', 'комбинезон', 'колготки', 'колготы',
            'корсет', 'костюм', 'кофта', 'кроссовки', 'куртка',
            'лампа',
            'майка',
            'нож',
            'парка', 'парео', 'поднос', 'перчатки', 'песочник', 'пиджак', 'пижама', 'платок', 'платье', 'поло', 'полувер',
            'реглан', 'рубашка', 'ручка',
            'салфетки', 'сапоги', 'сарафан', 'свитер', 'сковорода',
            'толстовка', 'топик', 'туалетная вода', 'туника', 'тарелка', 'трусы', 'трусики',
            'худи',
            'шапка', 'шорты', 'штаны',
            'юбка',
            'чайник', 'часы', 'чашка',
            'утюг',
            'футболка',
            'щетка',
        ];

        $name = $product->product->name;
        $description = $product->product->description;
        foreach ($types as $type) {
            if (mb_stripos($name, $type) !== false) {
                return $type;
            }
        }

        foreach ($types as $type) {
            if (mb_stripos($description, $type) !== false) {
                return $type;
            }
        }

        return null;
    }

    /**
     * @param GroupedProduct $product
     *
     * @return array
     */
    public static function paramSize($product)
    {
        static $sizeFormatMappings = [
            SizeFormats::SIZEFORMAT_EU => 'EU',
            SizeFormats::SIZEFORMAT_US => 'US',
            SizeFormats::SIZEFORMAT_INT => 'INT',
            SizeFormats::SIZEFORMAT_RU => 'RU',
            //SizeFormats::SIZEFORMAT_CHN, // not supported
            SizeFormats::SIZEFORMAT_UK => 'UK',
            SizeFormats::SIZEFORMAT_FR => 'FR',
            SizeFormats::SIZEFORMAT_IT => 'IT',
            SizeFormats::SIZEFORMAT_JP => 'Japan',
            SizeFormats::SIZEFORMAT_HEIGHT => 'Height',
            //SizeFormats::SIZEFORMAT_HEADWEAR, // not supported
            //SizeFormats::SIZEFORMAT_GLOVES, // not supported
            //SizeFormats::SIZEFORMAT_AGE // not supported
        ];

        $sizes = $product->sizes;
        $sizeFormat = $product->sizeformat;

        if (!isset($sizeFormatMappings[$sizeFormat]) || empty($sizes)) {
            return [];
        }

        return [
            'label' => 'Размер',
            'value' => implode(', ', $sizes),
            'attributes' => [
                'unit' => $sizeFormatMappings[$sizeFormat],
            ],
        ];
    }
}
