<?php

namespace MommyCom\Model\Product;

use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class AgeGroups
{
    const AGE_BABY = 'baby'; // 0 - 24 месяца
    const AGE_BABY_FROM = 0;
    const AGE_BABY_TO = 23;

    const AGE_PRESCHOOL = 'preschool'; // 2-4 года
    const AGE_PRESCHOOL_FROM = 24;
    const AGE_PRESCHOOL_TO = 47;

    const AGE_JUNIORSCHOOL = 'junior'; // 5 - 9
    const AGE_JUNIORSCHOOL_FROM = 48;
    const AGE_JUNIORSCHOOL_TO = 119;

    const AGE_SCHOOL = 'school'; //
    const AGE_SCHOOL_FROM = 120;
    const AGE_SCHOOL_TO = 99999999; // 140, специально на случай ошибки поставщика

    protected static $_buildinGroups = [
        self::AGE_BABY => [self::AGE_BABY_FROM, self::AGE_BABY_TO],
        self::AGE_PRESCHOOL => [self::AGE_PRESCHOOL_FROM, self::AGE_PRESCHOOL_TO],
        self::AGE_JUNIORSCHOOL => [self::AGE_JUNIORSCHOOL_FROM, self::AGE_JUNIORSCHOOL_TO],
        self::AGE_SCHOOL => [self::AGE_SCHOOL_FROM, self::AGE_SCHOOL_TO],
    ];

    protected function __construct()
    {
    }

    /**
     * @return AgeGroups
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new self;
        }
        return $instance;
    }

    public function getList()
    {
        return self::$_buildinGroups;
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return [
            self::AGE_BABY => \Yii::t('common', '0 - 24 months'),
            self::AGE_PRESCHOOL => \Yii::t('common', '2 - 4 years'),
            self::AGE_JUNIORSCHOOL => \Yii::t('common', '5 - 9 years'),
            self::AGE_SCHOOL => \Yii::t('common', '10 - 16 years'),
        ];
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function getLabel($name)
    {
        $labels = $this->getLabels();
        return isset($labels[$name]) ? $labels[$name] : '';
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function isValid($name)
    {
        return in_array($name, static::$_buildinGroups);
    }

    /**
     * Возвращает диапазон возрастов для указанной группы
     *
     * @param string $name название группы
     *
     * @return array|false false при ошибке
     */
    public function getAgeRange($name)
    {
        return isset(static::$_buildinGroups[$name]) ? static::$_buildinGroups[$name] : false;
    }

    /**
     * парсит разные значения и возвращает массив '0 - 5 лет' вернет array(0, 60)
     *
     * @var string $string
     * @return array
     **/
    public function parseAgeStringRange($string)
    {
        $exploded = Utf8::words($string);

        $base = '';
        if (count($exploded) > 1) {
            $base = array_pop($exploded);
        }

        $string = Utf8::replace(',', '.', Utf8::implode(' ', $exploded));

        list($from, $to) = array_map([Cast::class, 'toUFloat'], Utf8::explode('-', $string)) + [0, 0];

        if ($from > 0 && $to == 0) {
            $to = $from;
        }

        switch ($base) {
            case 'months':
            case 'month':
                $from *= 1;
                $to *= 1;
                break;

            case '':
            case 'years':
            case 'year':
                $from *= 12;
                $to *= 12;
                break;
        }

        $from = intval($from);
        $to = intval($to);

        return [$from, $to];
    }

    /**
     * @param array $range
     *
     * @return string
     */
    public function convertAgeRangeToString(array $range)
    {
        if (count($range) !== 2 || ($range[0] == 0 && $range[1] == 0)) {
            return '';
        }

        if ($range[1] > self::AGE_BABY_TO) {
            $range[0] = round($range[0] / 12, 1);
            $range[1] = round($range[1] / 12, 1);

            if ($range[0] == $range[1]) {
                return $range[0] . ' ' . \Yii::t('common', 'year|years', $range[0]);
            } else {
                return $range[0] . ' - ' . $range[1] . ' ' . \Yii::t('common', 'year|years', $range[1]);
            }
        } else {
            if ($range[0] == $range[1]) {
                return $range[0] . ' ' . \Yii::t('common', 'month|months', $range[0]);
            } else {
                return $range[0] . ' - ' . $range[1] . ' ' . \Yii::t('common', 'month|months', $range[0]);
            }
        }
    }

    /**
     * @param $range
     *
     * @return array array(1, 60) вернет array('baby', 'preschool', 'junior')
     */
    public function rangeToGroups($range)
    {
        if ($range[0] == 0 && $range[1] == 0) {
            return [];
        }

        $groups = [];
        foreach (self::$_buildinGroups as $name => $value) {
            if ($value[0] >= $range[0] || $value[1] <= $range[1]) {
                $groups[] = $name;
            }
        }
        return $groups;
    }
}
