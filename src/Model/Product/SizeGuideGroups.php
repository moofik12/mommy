<?php

namespace MommyCom\Model\Product;

class SizeGuideGroups
{
    const TYPE_OTHER = '';
    const CATEGORY_OTHER = '';

    const TYPE_FOOTWEAR = 'footwear';
    const TYPE_CLOTH = 'cloth';
    const TYPE_HEADWEAR = 'headwear';

    const CATEGORY_FOOTWEAR_BOYS = 'footwearBoys';
    const CATEGORY_FOOTWEAR_GIRLS = 'footwearGirls';
    const CATEGORY_FOOTWEAR_MAN = 'footwearMan';
    const CATEGORY_FOOTWEAR_WOMAN = 'footwearWoman';

    const CATEGORY_CLOTH_BOYS = 'clothBoys';
    const CATEGORY_CLOTH_GIRLS = 'clothGirls';
    const CATEGORY_CLOTH_MAN = 'clothMan';
    const CATEGORY_CLOTH_WOMAN = 'clothWoman';

    const CATEGORY_HEADWEAR_BOYS = 'headwearBoys';
    const CATEGORY_HEADWEAR_GIRLS = 'headwearGirls';
    const CATEGORY_HEADWEAR_MAN = 'headwearMan';
    const CATEGORY_HEADWEAR_WOMAN = 'headwearWoman';

    const TARGET_GROUP_CLOTH_WOMAN = 'groupClothWoman';
    const TARGET_GROUP_CLOTH_MAN = 'groupClothMan';
    const TARGET_GROUP_CLOTH_CHILDREN = 'groupClothChildren';
    const TARGET_GROUP_FOOTWEAR = 'groupFootwear';

    protected static function getTargetGroups()
    {
        return [
            self::TARGET_GROUP_CLOTH_WOMAN => \Yii::t('common', 'Women\'s clothing'),
            self::TARGET_GROUP_CLOTH_MAN => \Yii::t('common', 'Men\'s Clothing'),
            self::TARGET_GROUP_CLOTH_CHILDREN => \Yii::t('common', 'baby clothes'),
            self::TARGET_GROUP_FOOTWEAR => \Yii::t('common', 'footwear'),
        ];
    }

    /**
     * недоступные для показа, см. self::getGroupTargetList
     *
     * @var array
     */
    protected static $_targetGroupsNotAvailable = [

    ];

    protected static $_buildinCategories = [
        self::CATEGORY_FOOTWEAR_BOYS,
        self::CATEGORY_FOOTWEAR_GIRLS,
        self::CATEGORY_FOOTWEAR_MAN,
        self::CATEGORY_FOOTWEAR_WOMAN,

        self::CATEGORY_CLOTH_BOYS,
        self::CATEGORY_CLOTH_GIRLS,
        self::CATEGORY_CLOTH_MAN,
        self::CATEGORY_CLOTH_WOMAN,

        self::CATEGORY_HEADWEAR_BOYS,
        self::CATEGORY_HEADWEAR_GIRLS,
        self::CATEGORY_HEADWEAR_MAN,
        self::CATEGORY_HEADWEAR_WOMAN,
    ];

    protected static $_associations = [
        self::CATEGORY_FOOTWEAR_BOYS => ['обувь для мальчиков'],
        self::CATEGORY_FOOTWEAR_GIRLS => ['обувь для девочек'],
        self::CATEGORY_FOOTWEAR_MAN => ['обувь для мужчин'],
        self::CATEGORY_FOOTWEAR_WOMAN => ['обувь для женщин'],

        self::CATEGORY_CLOTH_BOYS => ['одежда для мальчиков', 'штаны для мальчиков', 'белье для мальчиков'],
        self::CATEGORY_CLOTH_GIRLS => ['одежда для девочек', 'штаны для девочек', 'белье для девочек', 'платье для девочек'],
        self::CATEGORY_CLOTH_MAN => ['одежда для мужчин', 'штаны для мужчин', 'белье для мужчин'],
        self::CATEGORY_CLOTH_WOMAN => ['одежда для женщин', 'штаны для женщин', 'белье для женщин', 'платье для женщин'],

        self::CATEGORY_HEADWEAR_BOYS => ['головной убор для мальчиков', 'очки для мальчиков'],
        self::CATEGORY_HEADWEAR_GIRLS => ['головной убор для девочек', 'очки для девочек'],
        self::CATEGORY_HEADWEAR_MAN => ['головной убор для мужчин', 'очки для мужчин'],
        self::CATEGORY_HEADWEAR_WOMAN => ['головной убор для женщин', 'очки для женщин'],
    ];

    protected static $_typeAssociations = [
        self::TYPE_FOOTWEAR => [
            self::CATEGORY_FOOTWEAR_BOYS,
            self::CATEGORY_FOOTWEAR_GIRLS,
            self::CATEGORY_FOOTWEAR_MAN,
            self::CATEGORY_FOOTWEAR_WOMAN,
        ],
        self::TYPE_CLOTH => [
            self::CATEGORY_CLOTH_BOYS,
            self::CATEGORY_CLOTH_GIRLS,
            self::CATEGORY_CLOTH_MAN,
            self::CATEGORY_CLOTH_WOMAN,
        ],
        self::TYPE_HEADWEAR => [
            self::CATEGORY_HEADWEAR_BOYS,
            self::CATEGORY_HEADWEAR_GIRLS,
            self::CATEGORY_HEADWEAR_MAN,
            self::CATEGORY_HEADWEAR_WOMAN,
        ],
    ];

    protected static $_targetGroupAssociations = [
        self::TARGET_GROUP_CLOTH_WOMAN => [
            self::CATEGORY_CLOTH_WOMAN,
        ],
        self::TARGET_GROUP_CLOTH_MAN => [
            self::CATEGORY_CLOTH_MAN,
        ],
        self::TARGET_GROUP_CLOTH_CHILDREN => [
            self::CATEGORY_CLOTH_BOYS,
            self::CATEGORY_CLOTH_GIRLS,
        ],
        self::TARGET_GROUP_FOOTWEAR => [
            self::CATEGORY_FOOTWEAR_BOYS,
            self::CATEGORY_FOOTWEAR_GIRLS,
            self::CATEGORY_FOOTWEAR_MAN,
            self::CATEGORY_FOOTWEAR_WOMAN,
        ],
    ];

    protected static $_targetAssociations = [
        self::CATEGORY_FOOTWEAR_BOYS => ProductTargets::TARGET_BOY,
        self::CATEGORY_FOOTWEAR_GIRLS => ProductTargets::TARGET_GIRL,
        self::CATEGORY_FOOTWEAR_MAN => ProductTargets::TARGET_MAN,
        self::CATEGORY_FOOTWEAR_WOMAN => ProductTargets::TARGET_WOMAN,

        self::CATEGORY_CLOTH_BOYS => ProductTargets::TARGET_BOY,
        self::CATEGORY_CLOTH_GIRLS => ProductTargets::TARGET_GIRL,
        self::CATEGORY_CLOTH_MAN => ProductTargets::TARGET_MAN,
        self::CATEGORY_CLOTH_WOMAN => ProductTargets::TARGET_WOMAN,

        self::CATEGORY_HEADWEAR_BOYS => ProductTargets::TARGET_BOY,
        self::CATEGORY_HEADWEAR_GIRLS => ProductTargets::TARGET_GIRL,
        self::CATEGORY_HEADWEAR_MAN => ProductTargets::TARGET_MAN,
        self::CATEGORY_HEADWEAR_WOMAN => ProductTargets::TARGET_WOMAN,
    ];

    protected function __construct()
    {
    }

    /**
     * @return SizeGuideGroups
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    public function getList()
    {
        return self::$_buildinCategories;
    }

    public function isValid($name)
    {
        return is_string($name) && in_array($name, $this->getList());
    }

    /**
     * @param string $name
     *
     * @return boolean
     */
    public function isInternalValid($name)
    {
        if (!is_string($name)) {
            return false;
        }

        foreach (static::$_associations as $categories) {
            if (in_array($name, $categories)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $name название группы категорий
     *
     * @return array|false внутренние категории
     */
    public function getInternalCategories($name)
    {
        return isset(self::$_associations[$name]) ? self::$_associations[$name] : false;
    }

    /**
     * @param string $name название внутренних категорий товара
     *
     * @return string|false внутренние категории
     */
    public function getCategory($name)
    {
        foreach (self::$_associations as $key => $names) {
            if (in_array($name, $names)) {
                return $key;
            }
        }
        return false;
    }

    /**
     * Возвращает тип одежды по категории
     *
     * @param string $name
     *
     * @return int|false
     */
    public function getType($name)
    {
        foreach (self::$_typeAssociations as $type => $categories) {
            if (in_array($name, $categories)) {
                return $type;
            }
        }
        return false;
    }

    /**
     * Возвращает тип для кого по категории
     *
     * @param string $name
     *
     * @return int|false
     */
    public function getTarget($name)
    {
        return isset(self::$_targetAssociations[$name]) ? self::$_targetAssociations[$name] : false;
    }

    /**
     * @param bool $onlyAvailable
     *
     * @return array
     */
    public function getGroupTargetList($onlyAvailable = false)
    {
        $result = self::getTargetGroups();

        if ($onlyAvailable) {
            $result = array_flip(array_diff(array_flip($result), self::$_targetGroupsNotAvailable));
        }

        return $result;
    }

    /**
     * Возвращает группу к которой принадлежит категория
     *
     * @param string $name
     *
     * @return bool|string
     */
    public function getGroupTargetForCategory($name)
    {
        foreach (self::$_targetGroupAssociations as $type => $categories) {
            if (in_array($name, $categories)) {
                return $type;
            }
        }

        return false;
    }

    /**
     * @param $name
     *
     * @return bool|string
     */
    public function getGroupTargetForAssociation($name)
    {
        $category = $this->getCategory($name);

        if ($category === false) {
            return false;
        }

        return $this->getGroupTargetForCategory($category);
    }
}
