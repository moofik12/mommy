<?php

namespace MommyCom\Model\Product;

use CMap;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Service\CurrentTime;
use MommyCom\YiiComponent\ArrayUtils;

/**
 * Class GroupedProducts
 * Контейнер для сгрупированых товаров
 * @method GroupedProduct[] toArray()
 *
 * @property-read string[] $sizes
 * @property-read BrandRecord[] $brands
 * @property-read string[] $colors
 * @property-read string[] $ageGroups
 * @property-read string[] $targets
 * @property-read SimilarProducts $similarProducts
 */
class GroupedProducts extends CMap
{
    /**
     * Время последнего изменения товаров
     *
     * @var int
     */
    protected $_changedAt = 0;

    /**
     * @var SimilarProducts
     */
    protected $_similarProducts;

    /**
     * @var int
     */
    protected $_similarProductsGeneratedAt = 0;

    /**
     * @param EventProductRecord[] $products
     *
     * @return GroupedProducts
     */
    public static function fromProducts(array $products)
    {
        $self = new self();
        $self->addProducts($products);
        $self->setReadOnly(true);
        return $self;
    }

    /**
     * @return EventProductRecord[]
     */
    public function toProducts()
    {
        $products = [];
        foreach ($this->toArray() as $group) {
            $products = array_merge($products, $group->eventProducts);
        }
        return $products;
    }

    /**
     * @param string $order
     *
     * @return $this
     * @throws \CException
     */
    private function sortByIsSoldOut(string $order): self
    {
        $order = ('asc' === $order);
        $this->setReadOnly(false);

        foreach ($this->toArray() as $key => $groupedProduct) {
            if ((bool)$groupedProduct->isSoldOut === $order) {
                $this->remove($key);
                $this->add($key, $groupedProduct);
            }
        }

        $this->setReadOnly(true);

        return $this;
    }

    /**
     * @param string $attribute
     * @param string $order
     *
     * @return $this
     * @throws \CException
     */
    public function sortBy($attribute, $order = 'asc'): self
    {
        if (!$this->getCount()) {
            return $this;
        }

        if ('isSoldOut' === $attribute) {
            return $this->sortByIsSoldOut($order);
        }

        $items = $this->toArray();

        if ($order == 'asc') {
            usort($items, function ($item1, $item2) use ($attribute) {
                return strnatcmp($item1->$attribute, $item2->$attribute);
            });
        } else {
            usort($items, function ($item1, $item2) use ($attribute) {
                return strnatcmp($item2->$attribute, $item1->$attribute);
            });
        }

        $this->setReadOnly(false);
        $this->copyFrom($items);
        $this->setReadOnly(true);

        return $this;
    }

    /**
     * @param EventProductRecord[] $products
     *
     * @throws \CException
     */
    public function addProducts(array $products): void
    {
        $this->setReadOnly(false);

        $productGroups = [];
        foreach ($products as $product) {
            $productGroups[$product->product_id][] = $product;
        }

        $catalogProducts = [];
        foreach (ProductRecord::model()->idIn(array_keys($productGroups))->cache(30)->findAll() as $catalogProduct) {
            $catalogProducts[$catalogProduct->id] = $catalogProduct;
        }

        foreach ($productGroups as $productId => $productGroup) {
            /** @var EventProductRecord $product */
            $product = reset($productGroup);
            if (false === $product) {
                continue;
            }

            $eventId = $product->event_id;

            if ($product->event) {
                $event = $product->event;
            } else {
                // ничего плохого не случится если закешировать данную информацию на 20 сек
                $event = EventRecord::model()->cache(20)->findByPk($eventId);
            }

            /* @var $event EventRecord */

            $catalogProduct = isset($catalogProducts[$productId]) ? $catalogProducts[$productId] : ProductRecord::model()->cache(5)->findByPk($productId);
            /* @var $catalogProduct ProductRecord */

            $key = $eventId . '_' . $productId;
            if (!$this->contains($key)) {
                $this->add($key, new GroupedProduct($event, $catalogProduct, $productGroup));
            }
        }

        $this->_changedAt = CurrentTime::getUnixTimestamp();
        $this->setReadOnly(true);
    }

    /**
     * возвращает возможные бренды по текущим условиям
     *
     * @return BrandRecord[]
     */
    public function getBrands()
    {
        $result = [];
        foreach ($this->toArray() as $product) {
            $eventProduct = reset($product->eventProducts);
            if ($eventProduct !== null) {
                $result[$eventProduct->brand_id] = $eventProduct->brand;
            }
        }
        return $result;
    }

    /**
     * возвращает возможные размеры по текущим условиям
     *
     * @return string[]
     */
    public function getSizes()
    {
        $result = [];
        foreach ($this->toArray() as $product) {
            $result = array_merge($result, ArrayUtils::getColumn($product->eventProducts, 'size'));
        }
        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);
        return $result;
    }

    /**
     * цвета, массив значений
     *
     * @return string[]
     */
    public function getColors()
    {
        $result = [];
        foreach ($this->toArray() as $product) {
            $result[] = $product->product->color;
        }
        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);
        return $result;
    }

    /**
     * года, массив значений из class AgeGroups
     *
     * @return array
     */
    public function getAgeGroups()
    {
        $result = [];

        $buildin = AgeGroups::instance()->getList();
        $buildinCount = count($buildin);

        foreach ($this->toArray() as $product) {
            $manyGroups = ArrayUtils::getColumn($product->eventProducts, 'ageGroups');
            foreach ($manyGroups as $groups) {
                foreach ($groups as $group) {
                    if (ArrayUtils::indexOf($result, $group) == -1) {
                        $result[] = $group;
                    }
                }
            }

            if ($buildinCount == count($result)) {
                break;
            }
        }

        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);
        return $result;
    }

    /**
     * целевая аудитория, массив значений из class ProductTargets
     *
     * @return array @see ProductTargets
     */
    public function getTargets()
    {
        $buildin = ProductTargets::instance()->getList();
        $buildinCount = count($buildin);

        $result = [];
        foreach ($this->toArray() as $product) {
            foreach ($product->product->target as $target) {
                if (!in_array($target, $result) && in_array($target, $buildin)) {
                    $result[] = $target;
                }
            }

            if ($buildinCount === count($result)) {
                break;
            }
        }
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);

        return $result;
    }

    /**
     * @param bool $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return SimilarProducts
     */
    public function getSimilarProducts($onlyActiveEvents = true, $limit = 10, $cache = 0)
    {
        $recreate = $this->_similarProductsGeneratedAt < $this->_changedAt;
        $recreate = $recreate || $this->_similarProducts === null;
        $recreate = $recreate || $this->_similarProducts->isOnlyActiveEvents != $onlyActiveEvents;
        $recreate = $recreate || $this->_similarProducts->limit != $limit;

        if ($recreate) {
            $products = ArrayUtils::getColumn($this, 'product');
            $this->_similarProducts = SimilarProducts::fromProducts($products, $onlyActiveEvents, $limit, $cache);
        }

        return $this->_similarProducts;
    }
}
