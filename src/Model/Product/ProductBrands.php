<?php

namespace MommyCom\Model\Product;

use CException;
use CMap;
use LogicException;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\ArrayUtils;
use Yii;

/**
 * Class ProductBrands
 * @method BrandRecord[]|array toArray()
 */
final class ProductBrands extends CMap
{
    /**
     * @param EventProductRecord[] $products
     *
     * @return ProductBrands
     */
    public static function fromProducts(array $products)
    {
        $self = new self();
        $self->addFromProducts($products);
        $self->setReadOnly(true);
        return $self;
    }

    /**
     * @param EventRecord[] $events
     *
     * @return ProductBrands
     */
    public static function fromEvents(array $events)
    {
        $self = new self();
        $self->addFromEvents($events);
        $self->setReadOnly(true);
        return $self;
    }

    /**
     * @param array $list
     *
     * @return ProductBrands
     */
    public static function fromBrandsId(array $list)
    {
        $self = new self();
        $self->addFromBrandsId($list);
        $self->setReadOnly(true);
        return $self;
    }

    /**
     * @param BrandRecord[] $brands
     *
     * @return ProductBrands
     */
    public static function fromBrands(array $brands)
    {
        $self = new self();
        $self->addFromBrands($brands);
        $self->setReadOnly(true);
        return $self;
    }

    /**
     * @param EventProductRecord[] $products
     *
     * @return static
     */
    public function addFromProducts(array $products)
    {
        $this->setReadOnly(false);
        $productBrands = ArrayUtils::groupBy($products, 'brand_id'); // групируем по брендам
        /** @var BrandRecord[] $catalogBrands */
        $catalogBrands = BrandRecord::model()->idIn(array_keys($productBrands))->cache(10)->findAll();

        foreach ($catalogBrands as $brand) {
            if (!$this->contains($brand->id)) {
                $this->add($brand->id, $brand);
            }
        }

        $this->setReadOnly(true);

        return $this;
    }

    /**
     * @param array EventRecord[] $events
     *
     * @return static
     */
    public function addFromEvents(array $events)
    {
        $this->setReadOnly(false);
        $eventsUnique = ArrayUtils::groupBy($events, 'id'); //групируем
        $brandsIds = Yii::app()->db
            ->cache(60)
            ->createCommand()
            ->selectDistinct('brand_id')
            ->from(EventProductRecord::model()->tableName())
            ->where(['IN', 'event_id', array_keys($eventsUnique)])
            ->queryColumn();

        /** @var BrandRecord[] $catalogBrands */
        $catalogBrands = BrandRecord::model()->idIn($brandsIds)->cache(10)->findAll();

        foreach ($catalogBrands as $brand) {
            if (!$this->contains($brand->id)) {
                $this->add($brand->id, $brand);
            }
        }

        $this->setReadOnly(true);
        return $this;
    }

    /**
     * @param array $list
     *
     * @return static
     */
    public function addFromBrandsId(array $list)
    {
        $this->setReadOnly(false);
        /** @var BrandRecord[] $catalogBrands */
        $catalogBrands = BrandRecord::model()->idIn($list)->cache(10)->findAll();

        foreach ($catalogBrands as $brand) {
            if (!$this->contains($brand->id)) {
                $this->add($brand->id, $brand);
            }
        }

        $this->setReadOnly(true);

        return $this;
    }

    /**
     * @param BrandRecord[] $brands
     *
     * @return $this
     * @throws CException
     */
    public function addFromBrands(array $brands)
    {
        $firstBrand = reset($brands);
        if ($firstBrand !== false && !($firstBrand instanceof BrandRecord)) {
            throw new LogicException('Brand must be instance of BrandRecord');
        }

        $this->setReadOnly(false);
        foreach ($brands as $brand) {
            if (!$this->contains($brand->id)) {
                $this->add($brand->id, $brand);
            }
        }
        $this->setReadOnly(true);

        return $this;
    }

    /**
     * @param bool $random
     *
     * @return BrandRecord[]|array
     */
    public function getHasLogo($random = false)
    {
        $brands = array_filter($this->toArray(), function ($brand) {
            /** @var BrandRecord $brand */
            return !empty($brand->logo_fileid);
        });

        if ($random) {
            shuffle($brands);
        }

        return $brands;
    }

    /**
     * @param string $attribute see BrandRecord::model()->attributes
     *
     * @return static
     */
    public function sort($attribute = 'name')
    {
        $this->setReadOnly(false);

        $data = $this->toArray();
        $vales = [];
        foreach ($data as $key => $item) {
            $vales[$key] = $item->$attribute;
        }
        natsort($vales);
        $data = array_replace($vales, $data);
        $this->copyFrom($data);

        $this->setReadOnly(true);

        return $this;
    }

    /**
     * @return $this
     * @throws CException
     */
    public function random()
    {
        $this->setReadOnly(false);

        $data = $this->toArray();
        shuffle($data);
        $this->copyFrom($data);

        $this->setReadOnly(true);

        return $this;
    }
} 
