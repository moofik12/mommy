<?php

namespace MommyCom\Model\Product;

use CModel;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\ViewsTracking\ViewsTracking;
use MommyCom\YiiComponent\ArrayUtils;
use Yii;

/**
 * Class GroupedProduct
 * Класс для группировки товаров событий по базовым товарам
 *
 * @property-read integer $id
 * @property-read string $category
 * @property-read string $categoryId
 * @property-read integer $maxPerBuy
 * @property-read array $targets
 * @property-read float $priceOld
 * @property-read float $price
 * @property-read float $discount
 * @property-read integer $discountPercent
 * @property-read string[] $sizes
 * @property-read integer $sizeCount
 * @property-read array productBaseAttributes
 * @property-read string $sizeformat
 * @property-read string $sizeformatReplacement
 * @property-read string $ageRange
 * @property-read string $ageRangeReplacement
 * @property-read SimilarProducts $similarProducts
 * @property-read integer $totalAvailable
 * @property-read boolean $isSoldOut
 * @property-read boolean $isTopOfSelling
 * @property-read boolean $isLimited
 */
class GroupedProduct extends CModel
{
    /**
     * @var ProductRecord
     */
    public $product;

    /**
     * @var EventProductRecord[]
     */
    public $eventProducts;

    /**
     * @var EventRecord
     */
    public $event;

    protected $_cache = [];

    /**
     * @var SimilarProducts
     */
    protected $_similarProducts;

    /**
     * @param EventRecord $event
     * @param ProductRecord $product
     * @param EventProductRecord[] $eventProducts
     */
    public function __construct(EventRecord $event, ProductRecord $product, array $eventProducts)
    {
        $this->event = $event;
        $this->product = $product;
        $this->eventProducts = ArrayUtils::changeKeyColumn($eventProducts, 'size');
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'product' => \Yii::t($category, 'Product'),
            'eventProducts' => \Yii::t($category, 'Products'),
            'event' => \Yii::t($category, 'Flash-sale'),
        ];
    }

    /**
     * @return mixed
     */
    public function getMaxPerBuy()
    {
        return max(ArrayUtils::getColumn($this->eventProducts, 'max_per_buy'));
    }

    /**
     * Входит ли товар в Топ Продаж
     *
     * @return boolean
     */
    public function getIsTopOfSelling()
    {
        $cache = \Yii::app()->cache;
        $key = 'GroupedProduct_TopOfSelling_' . $this->event->id;
        $cacheTime = 120;

        $topProductIds = $cache->get($key);

        if ($topProductIds === false) {
            $topProductMinCount = 3; // минимальное количество
            $topProductPercent = 0.15; // оптимальный процент топа

            $topProductIds = [];
            $selector = EventProductRecord::model()
                ->eventId($this->event->id);
            $selector->groupBy('product_id');
            $selector->orderBy('SUM(number_sold_real)', 'desc');

            $productIds = $selector->findColumnDistinct('product_id');
            $productGroups = ArrayUtils::groupBy(
                EventProductRecord::model()->eventId($this->event->id)->hasNumberSoldReal()->with('event')->findAll(),
                'product_id'
            );

            $availableProducts = [];
            foreach ($productIds as $productId) {
                if (!isset($productGroups[$productId])) {
                    continue;
                }

                $products = $productGroups[$productId];
                /* @var $products EventProductRecord[] */

                foreach ($products as $product) {
                    if ($product->number_sold_real > 0 && $product->getNumberAvailable(false) == 0) {
                        continue;
                    }

                    $availableProducts[$productId] = $productId;

                    if ($product->number_sold_real > 0) {
                        $topProductIds[$productId] = $productId;
                        break;
                    }
                }
            }

            $availableProductsCount = count($availableProducts);
            $topProductCount = count($topProductIds);
            $downscaleTo = round(max($topProductMinCount, min($availableProductsCount * $topProductPercent, $topProductCount)));
            $topProductIds = array_slice($topProductIds, 0, min($downscaleTo, $topProductCount));

            $cache->set($key, $topProductIds, $cacheTime);
        }

        return in_array($this->product->id, $topProductIds);
    }

    /**
     * @return boolean
     */
    public function getIsLimited()
    {
        if ($this->event->is_stock) {
            return false; // стоковые акции не поддерживаются
        }

        $cache = \Yii::app()->cache;
        $key = 'GroupedProduct_IsLimited_' . $this->product->id;
        $cacheTime = 120;

        $availablePercent = $cache->get($key);
        if ($availablePercent === false) {
            $totalReserved = 0;
            $totalSold = 0;
            foreach ($this->eventProducts as $product) {
                $totalReserved += $product->number;
                $totalSold += ($product->getNumberAvailable(false) - $product->number) * -1;
            }

            $availablePercent = $totalReserved > 0 ? $totalSold / $totalReserved : 1.0;
            $cache->set($key, $availablePercent, $cacheTime);
        }

        return $availablePercent >= 0.25; // товар заканчивается если его в наличии меньше чем 25% от резерва
    }

    /**
     * @return bool
     */
    public function getIsSoldOut()
    {
        return $this->getTotalAvailable() == 0;
    }

    /**
     * @return array
     */
    public function getTargets()
    {
        if (isset($this->_cache['targets'])) {
            return $this->_cache['targets'];
        }

        $targets = [];
        foreach ($this->eventProducts as $product) {
            $targets = ProductTargets::instance()->mergeTargets($targets, $product->target);
        }

        return $this->_cache['targets'] = $targets;
    }

    /**
     * @param $target
     *
     * @return bool
     */
    public function isForTarget($target)
    {
        return in_array($target, $this->getTargets());
    }

    /**
     * @return float
     */
    public function getPriceOld()
    {
        if (isset($this->_cache['priceOld'])) {
            return $this->_cache['priceOld'];
        }

        return $this->_cache['priceOld'] = max(ArrayUtils::getColumn($this->eventProducts, 'price_market'));
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        if (isset($this->_cache['price'])) {
            return $this->_cache['price'];
        }

        return $this->_cache['price'] = min(ArrayUtils::getColumn($this->eventProducts, 'price'));
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return round(1 - $this->price / $this->priceOld, 2);
    }

    /**
     * @return integer
     */
    public function getDiscountPercent()
    {
        return (int)($this->getDiscount() * 100);
    }

    /**
     * @param EventProductRecord $product
     *
     * @return integer
     */
    protected function _getProductAvailable($product)
    {
        $cache = \Yii::app()->cache;
        $key = 'GroupedProduct_ProductAvailability_' . $product->id;

        $available = $cache->get($key);
        if ($available === false) {
            $available = $product->numberAvailable;
            $cache->set($key, $available, 30);
        }

        return $available;
    }

    /**
     * @return int
     */
    public function getTotalAvailable()
    {
        if (isset($this->_cache['totalAvailable'])) {
            return $this->_cache['totalAvailable'];
        }

        $totalAvailable = 0;
        foreach ($this->eventProducts as $product) {
            $totalAvailable += $this->_getProductAvailable($product);
        }

        return $this->_cache['totalAvailable'] = $totalAvailable;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->product->category;
    }

    /**
     * @return string
     */
    public function getCategoryId()
    {
        return sprintf('%u', crc32($this->product->category));
    }

    /**
     * @param bool $onlyAvailable
     *
     * @return string[]
     */
    public function getSizes($onlyAvailable = true)
    {
        if ($onlyAvailable) {
            $sizes = [];
            foreach ($this->eventProducts as $product) {
                if ($this->_getProductAvailable($product) > 0) {
                    $sizes[] = $product->size;
                }
            }
        } else {
            $sizes = ArrayUtils::getColumn($this->eventProducts, 'size');
        }
        $sizes = ArrayUtils::onlyNonEmpty(array_unique($sizes));
        sort($sizes, SORT_NATURAL | SORT_FLAG_CASE);
        return $sizes;
    }

    /**
     * @param bool $onlyAvailable
     *
     * @return int
     */
    public function getSizeCount($onlyAvailable = false)
    {
        return count($this->getSizes($onlyAvailable));
    }

    /**
     * @return string
     */
    public function getSizeformat()
    {
        $formats = ArrayUtils::getColumn($this->eventProducts, 'sizeformat');
        return reset($formats);
    }

    /**
     * @return string
     */
    public function getSizeformatReplacement()
    {
        return SizeFormats::instance()->getLabel($this->getSizeformat());
    }

    /**
     * @return array
     */
    public function getAgeRange()
    {
        $ranges = ArrayUtils::getColumn($this->eventProducts, 'ageRange', false);
        $from = [];
        $to = [];

        foreach ($ranges as $range) {
            $from[] = $range[0];
            $to[] = $range[1];
        }

        $range = [
            (int)min($from), (int)max($to),
        ];
        return $range;
    }

    /**
     * @return string
     */
    public function getAgeRangeReplacement()
    {
        return AgeGroups::instance()->convertAgeRangeToString($this->getAgeRange());
    }

    /**
     * @return int
     */
    public function getId()
    {
        if (count($this->eventProducts) == 0) {
            return 0;
        }

        $product = reset($this->eventProducts);
        /* @var EventProductRecord $product */

        return $product->id;
    }

    /**
     * @return string
     */
    public function getSizeGuide()
    {
        if (count($this->eventProducts) == 0) {
            return '';
        }

        $product = reset($this->eventProducts);
        /* @var EventProductRecord $product */

        return $product->size_guide;
    }

    /**
     * Увеличивает каунтер просмотров
     *
     * @return boolean
     */
    public function trackView()
    {
        $tracker = Yii::createComponent(ViewsTracking::class);
        /* @var $tracker ViewsTracking */

        if (\Yii::app()->user->isGuest) { // don't track guest views
            return false;
        }

        return $tracker->add($this->product, \Yii::app()->user->id);
    }

    /**
     * Возвращает количество просмотров данного товара
     *
     * @return int
     */
    public function getViewsNumber()
    {
        $tracker = Yii::createComponent(ViewsTracking::class);
        /* @var $tracker ViewsTracking */

        return $tracker->get($this->product);
    }

    /**
     * @param boolean $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return SimilarProducts
     */
    public function getSimilarProducts($onlyActiveEvents = true, $limit = 10, $cache = 0)
    {
        $recreate = $this->_similarProducts === null;
        $recreate = $recreate || $this->_similarProducts->isOnlyActiveEvents != $onlyActiveEvents;
        $recreate = $recreate || $this->_similarProducts->limit != $limit;

        if ($recreate) {
            $this->_similarProducts = SimilarProducts::fromProduct($this->product, $onlyActiveEvents, $limit, $cache);
        }

        return $this->_similarProducts;
    }

    public function getProductBaseAttributes()
    {
        if (count($this->eventProducts) == 0) {
            return [];
        }

        $product = reset($this->eventProducts);
        /* @var EventProductRecord $product */

        $names = [
            'composition',
            'color',
            'brand.name',
            'design_in',
            'made_in',
            'article' => 'id',
            'shelflife',
            'dimensions',
            'weightReplacement',
        ];

        $result = [];

        foreach ($names as $key => $value) {
            $name = is_numeric($key) ? $value : $key;
            $item = [
                'label' => $product->getAttributeLabel($name),
                'value' => ArrayUtils::getValueByPath($product, $value),
            ];
            if (!empty($item['value'])) {
                $result[$name] = $item;
            }
        }

        return $result;
    }

    /**
     * @return null|string
     */
    public function getCertificate()
    {
        if (isset($this->_cache['certificate'])) {
            return $this->_cache['certificate'];
        }

        foreach ($this->eventProducts as $eventProduct) {
            if (!empty($eventProduct->certificate)) {
                return $this->_cache['certificate'] = $eventProduct->certificate;
            }
        }

        return null;
    }
}
