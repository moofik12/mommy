<?php

namespace MommyCom\Model\Product;

use MommyCom\YiiComponent\ArrayUtils;

/**
 * Class ProductColorCodes
 * Класс соответствий между кодами цветов и их значениями
 */
final class ProductColorCodes
{
    const COLOR_CODE_BLACK = 1;
    const COLOR_CODE_RED = 2;
    const COLOR_CODE_YELLOW = 3;
    const COLOR_CODE_GREEN = 4;
    const COLOR_CODE_BLUE = 5;
    const COLOR_CODE_WHITE = 6;
    const COLOR_CODE_ORANGE = 7;
    const COLOR_CODE_GOLD = 8;
    const COLOR_CODE_SILVER = 9;
    const COLOR_CODE_PURPLE = 10;
    const COLOR_CODE_FUL = 11;
    const COLOR_CODE_BEIGE = 12;
    const COLOR_CODE_TURQUOISE = 13;
    const COLOR_CODE_BURGUNDY = 14;
    const COLOR_CODE_CORAL = 15;
    const COLOR_CODE_BRAWN = 16;
    const COLOR_CODE_ROSE = 17;
    const COLOR_CODE_LIME_GREEN = 18;
    const COLOR_CODE_GRAY = 19;
    const COLOR_CODE_DARK_BLUE = 20;
    const COLOR_CODE_LILAC = 21;
    const COLOR_CODE_KHAKI = 22;

    protected static $_buildingColorCodes = [
        self::COLOR_CODE_BEIGE,
        self::COLOR_CODE_WHITE,
        self::COLOR_CODE_TURQUOISE,
        self::COLOR_CODE_BURGUNDY,
        self::COLOR_CODE_BLUE,
        self::COLOR_CODE_YELLOW,
        self::COLOR_CODE_GREEN,
        self::COLOR_CODE_GOLD,
        self::COLOR_CODE_CORAL,
        self::COLOR_CODE_BRAWN,
        self::COLOR_CODE_RED,
        self::COLOR_CODE_ORANGE,
        self::COLOR_CODE_FUL,
        self::COLOR_CODE_ROSE,
        self::COLOR_CODE_LIME_GREEN,
        self::COLOR_CODE_SILVER,
        self::COLOR_CODE_GRAY,
        self::COLOR_CODE_DARK_BLUE,
        self::COLOR_CODE_LILAC,
        self::COLOR_CODE_PURPLE,
        self::COLOR_CODE_KHAKI,
        self::COLOR_CODE_BLACK,
    ];

    protected function __construct()
    {
    }

    /**
     * @return ProductColorCodes
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return self::$_buildingColorCodes;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function isValid($name)
    {
        return is_string($name) && in_array($name, $this->getList());
    }

    /**
     * @param null $names
     *
     * @return array
     */
    public function getUnifiedLabels($names = null) {
        $list = [
            self::COLOR_CODE_BEIGE => 'beige',
            self::COLOR_CODE_WHITE => 'white',
            self::COLOR_CODE_TURQUOISE => 'turquoise',
            self::COLOR_CODE_BURGUNDY => 'claret',
            self::COLOR_CODE_BLUE => 'baby blue',
            self::COLOR_CODE_YELLOW => 'yellow',
            self::COLOR_CODE_GREEN => 'green',
            self::COLOR_CODE_GOLD => 'gold',
            self::COLOR_CODE_CORAL => 'coral',
            self::COLOR_CODE_BRAWN => 'brown',
            self::COLOR_CODE_RED => 'red',
            self::COLOR_CODE_ORANGE => 'orange',
            self::COLOR_CODE_FUL => 'multicolored',
            self::COLOR_CODE_ROSE => 'pink',
            self::COLOR_CODE_LIME_GREEN => 'light-green',
            self::COLOR_CODE_SILVER => 'silver',
            self::COLOR_CODE_GRAY => 'gray',
            self::COLOR_CODE_DARK_BLUE => 'blue',
            self::COLOR_CODE_LILAC => 'lilac',
            self::COLOR_CODE_PURPLE => 'purple',
            self::COLOR_CODE_KHAKI => 'khaki',
            self::COLOR_CODE_BLACK => 'black',
        ];

        if ($names !== null) {
            $list = ArrayUtils::valuesByKeys($list, $names, true);
        }

        return $list;
    }

    /**
     * @param array|null $names
     *
     * @return array
     */
    public function getLabels($names = null)
    {
        $list = [
            self::COLOR_CODE_BEIGE => \Yii::t('common','beige'),
            self::COLOR_CODE_WHITE => \Yii::t('common','white'),
            self::COLOR_CODE_TURQUOISE => \Yii::t('common','turquoise'),
            self::COLOR_CODE_BURGUNDY => \Yii::t('common','claret'),
            self::COLOR_CODE_BLUE => \Yii::t('common','baby blue'),
            self::COLOR_CODE_YELLOW => \Yii::t('common','yellow'),
            self::COLOR_CODE_GREEN => \Yii::t('common','green'),
            self::COLOR_CODE_GOLD => \Yii::t('common','gold'),
            self::COLOR_CODE_CORAL => \Yii::t('common','coral'),
            self::COLOR_CODE_BRAWN => \Yii::t('common','brown'),
            self::COLOR_CODE_RED => \Yii::t('common','red'),
            self::COLOR_CODE_ORANGE => \Yii::t('common','orange'),
            self::COLOR_CODE_FUL => \Yii::t('common','multicolored'),
            self::COLOR_CODE_ROSE => \Yii::t('common','pink'),
            self::COLOR_CODE_LIME_GREEN => \Yii::t('common','light-green'),
            self::COLOR_CODE_SILVER => \Yii::t('common','silver'),
            self::COLOR_CODE_GRAY => \Yii::t('common','gray'),
            self::COLOR_CODE_DARK_BLUE => \Yii::t('common','blue'),
            self::COLOR_CODE_LILAC => \Yii::t('common','lilac'),
            self::COLOR_CODE_PURPLE => \Yii::t('common','purple'),
            self::COLOR_CODE_KHAKI => \Yii::t('common','khaki'),
            self::COLOR_CODE_BLACK => \Yii::t('common','black'),
        ];

        if ($names !== null) {
            $list = ArrayUtils::valuesByKeys($list, $names, true);
        }

        return $list;
    }

    /**
     * @param string $name
     *
     * @return string|false
     */
    public function getLabel($name)
    {
        $labels = $this->getLabels();
        return isset($labels[$name]) ? $labels[$name] : '';
    }

    /**
     * @param array|string $colorCodeOne
     * @param array|string $colorCodeTwo
     *
     * @return array
     */
    public function mergeColorCodes($colorCodeOne, $colorCodeTwo)
    {
        $result = [];
        foreach ([$colorCodeOne, $colorCodeTwo] as $colorCodes) {
            if (!is_array($colorCodes)) {
                $colorCodes = [$colorCodes];
            }
            foreach ($colorCodes as $colorCode) {
                if ($this->isValid($colorCode) && !in_array($colorCode, $result)) {
                    $result[] = $colorCode;
                }
            }
        }
        return $result;
    }

    /**
     * @param $colorCodes
     *
     * @return array
     */
    public function sanitizeTargets($colorCodes)
    {
        if (!is_array($colorCodes)) {
            $colorCodes = [$colorCodes];
        }

        foreach ($colorCodes as $index => $target) {
            if (!$this->isValid($target)) {
                unset($colorCodes[$index]);
            }
        }

        return $colorCodes;
    }

    /**
     * @param null $names
     *
     * @return array
     */
    public function listFrontClasses($names = null)
    {
        $list = [
            self::COLOR_CODE_BEIGE => 'color_beige',
            self::COLOR_CODE_WHITE => 'color_white',
            self::COLOR_CODE_TURQUOISE => 'color_turquoise',
            self::COLOR_CODE_BURGUNDY => 'color_burgundy',
            self::COLOR_CODE_BLUE => 'color_blue',
            self::COLOR_CODE_YELLOW => 'color_yellow',
            self::COLOR_CODE_GREEN => 'color_green',
            self::COLOR_CODE_GOLD => 'color_gold',
            self::COLOR_CODE_CORAL => 'color_coral',
            self::COLOR_CODE_BRAWN => 'color_brawn',
            self::COLOR_CODE_RED => 'color_red',
            self::COLOR_CODE_ORANGE => 'color_orange',
            self::COLOR_CODE_FUL => 'color_ful',
            self::COLOR_CODE_ROSE => 'color_rose',
            self::COLOR_CODE_LIME_GREEN => 'color_lime_green',
            self::COLOR_CODE_SILVER => 'color_silver',
            self::COLOR_CODE_GRAY => 'color_gray',
            self::COLOR_CODE_DARK_BLUE => 'color_dark_blue',
            self::COLOR_CODE_LILAC => 'color_lilac',
            self::COLOR_CODE_PURPLE => 'color_purple',
            self::COLOR_CODE_KHAKI => 'color_khaki',
            self::COLOR_CODE_BLACK => 'color_black',
        ];

        if ($names !== null) {
            $list = ArrayUtils::valuesByKeys($list, $names, true);
        }

        return $list;
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function getFrontClassName($name)
    {
        $classes = $this->listFrontClasses();
        return isset($classes[$name]) ? $classes[$name] : '';
    }
}
