<?php

namespace MommyCom\Model\Product;

use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

/**
 * Class GroupedProducts
 * Контейнер с фильтрами для сгрупированых товаров
 */
final class GroupedProductsFilter
{
    /** @var int */
    private $_countProducts = 0;

    /**
     * @var BrandRecord[]
     */
    private $_brands = [];

    /** @var string[] */
    private $_sizes = [];

    /** @var string[] */
    private $_colors = [];

    /** @var string[] */
    private $_ageGroups = [];

    /** @var string[] */
    private $_targets = [];

    /**
     * @param $record EventRecord
     * @param $productsId array Список продуктов которые входят в акцию
     * @param $cache integer time to cache query
     *
     * @return static
     */
    public static function fromEvent(EventRecord $record, $productsId = [], $cache = 60)
    {
        $self = new self();
        $command = Yii::app()->db->cache($cache)
            ->createCommand([
                'distinct' => true,
                'select' => ['id', 'product_id', 'brand_id', 'size', 'category', 'color', 'age_from', 'age_to', 'target'],
                'from' => EventProductRecord::model()->tableName(),
                'where' => 'event_id=:id',
                'params' => [':id' => $record->id],
            ]);

        if ($productsId) {
            $command->andWhere(['IN', 'product_id', Cast::toUIntArr($productsId)]);
        }

        $liteEventProductsData = $command->queryAll();
        $self->_initialize($liteEventProductsData);

        return $self;
    }

    /**
     * @param $ids array
     * @param $productsId array Список продуктов которые входят в акцию
     * @param $cache integer time to cache query
     *
     * @return static
     */
    public static function fromEventsId(array $ids, $productsId = [], $cache = 60)
    {
        $self = new self();
        $command = Yii::app()->db->cache($cache)
            ->createCommand([
                'distinct' => true,
                'select' => ['id', 'product_id', 'brand_id', 'size', 'category', 'color', 'age_from', 'age_to', 'target'],
                'from' => EventProductRecord::model()->tableName(),
                'where' => ['IN', 'event_id', Cast::toUIntArr($ids)],
            ]);

        if ($productsId) {
            $command->andWhere(['IN', 'product_id', Cast::toUIntArr($productsId)]);
        }

        $liteEventProductsData = $command->queryAll();

        $self->_initialize($liteEventProductsData);

        return $self;
    }

    /**
     * @param EventProductRecord[] $products
     *
     * @return GroupedProducts
     */
    public static function fromEventProducts(array $products)
    {
        $self = new self();
        $self->_initialize($products);
        return $self;
    }

    /**
     * @param $liteEventProductsData
     */
    private function _initialize($liteEventProductsData)
    {
        $liteProductsData = Yii::app()->db->cache(60)
            ->createCommand([
                'distinct' => true,
                'select' => ['id', 'target', 'color'],
                'from' => ProductRecord::model()->tableName(),
                'where' => ['IN', 'id', ArrayUtils::getColumn($liteEventProductsData, 'product_id')],
            ])
            ->queryAll();

        $this->_countProducts = count($liteEventProductsData);
        $this->_fetchBrands($liteEventProductsData);
        $this->_fetchSizes($liteEventProductsData);
        $this->_fetchAgeGroups($liteEventProductsData);

        $this->_fetchColors($liteProductsData);
        $this->_fetchTargets($liteProductsData);
    }

    /**
     * @return int
     */
    public function getCountEventProducts()
    {
        return $this->_countProducts;
    }

    /**
     * возвращает возможные бренды по текущим условиям
     *
     * @return BrandRecord[]
     */
    public function getBrands()
    {
        return $this->_brands;
    }

    /**
     * возвращает возможные размеры по текущим условиям
     *
     * @return string[]
     */
    public function getSizes()
    {
        return $this->_sizes;
    }

    /**
     * цвета, массив значений
     *
     * @return string[]
     */
    public function getColors()
    {
        return $this->_colors;
    }

    /**
     * года, массив значений из class AgeGroups
     *
     * @return string[]
     */
    public function getAgeGroups()
    {
        return $this->_ageGroups;
    }

    /**
     * целевая аудитория, массив значений из class ProductTargets
     *
     * @return string[] @see ProductTargets
     */
    public function getTargets()
    {
        return $this->_targets;
    }

    /**
     * @param array $liteEventProductsData
     */
    private function _fetchBrands(array $liteEventProductsData)
    {
        $brandsId = ArrayUtils::getColumn($liteEventProductsData, 'brand_id');

        $this->_brands = BrandRecord::model()->idIn($brandsId)->cache(60)->findAll();
    }

    /**
     * @param array $liteEventProductsData
     */
    private function _fetchSizes(array $liteEventProductsData)
    {
        $result = ArrayUtils::getColumn($liteEventProductsData, 'size');
        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);

        $this->_sizes = $result;
    }

    /**
     * @param array $liteProductsData
     */
    private function _fetchColors(array $liteProductsData)
    {
        $result = ArrayUtils::getColumn($liteProductsData, 'color');
        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);

        $this->_colors = $result;
    }

    /**
     * @param array $liteEventProductsData
     */
    private function _fetchAgeGroups(array $liteEventProductsData)
    {
        $result = ArrayUtils::getColumn($liteEventProductsData, 'color');
        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);
        $this->_colors = $result;

        $result = [];

        $buildin = AgeGroups::instance()->getList();
        $buildinCount = count($buildin);

        $recordAgeRanges = ArrayUtils::getColumns($liteEventProductsData, ['age_from', 'age_to']);
        foreach ($recordAgeRanges as $recordAgeRange) {
            $groups = AgeGroups::instance()->rangeToGroups([$recordAgeRange['age_from'], $recordAgeRange['age_to']]);
            foreach ($groups as $group) {
                if (ArrayUtils::indexOf($result, $group) == -1) {
                    $result[] = $group;
                }
            }

            if ($buildinCount == count($result)) {
                break;
            }
        }

        $result = ArrayUtils::onlyNonEmpty(array_unique($result));
        sort($result, SORT_NATURAL | SORT_FLAG_CASE);

        $this->_ageGroups = $result;
    }

    /**
     * @param array $liteProductsData
     */
    private function _fetchTargets(array $liteProductsData)
    {
        $buildin = ProductTargets::instance()->getList();
        $buildinCount = count($buildin);

        $result = [];
        foreach ($liteProductsData as $product) {
            $targets = !empty($product['target']) && is_string($product['target']) ? explode(',', $product['target']) : [];
            foreach ($targets as $target) {
                if (!in_array($target, $result) && in_array($target, $buildin)) {
                    $result[] = $target;
                }
            }

            if ($buildinCount === count($result)) {
                break;
            }
        }

        sort($result, SORT_NATURAL | SORT_FLAG_CASE);

        $this->_targets = $result;
    }
}
