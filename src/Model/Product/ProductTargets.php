<?php

namespace MommyCom\Model\Product;

use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Utf8;

/**
 * Class ProductTargets
 */
final class ProductTargets
{
    const TARGET_BOY = 'boy';
    const TARGET_GIRL = 'girl';
    const TARGET_WOMAN = 'woman';
    const TARGET_MAN = 'man';
    const TARGET_ACCESSORIES = 'accessory';
    const TARGET_HOME = 'home';

    protected static $_supportsAges = [self::TARGET_BOY, self::TARGET_GIRL];

    protected static $_buildinTargets = [
        self::TARGET_BOY,
        self::TARGET_GIRL,
        self::TARGET_WOMAN,
        self::TARGET_MAN,
        self::TARGET_ACCESSORIES,
        self::TARGET_HOME,
    ];

    protected function __construct()
    {
    }

    /**
     * @return ProductTargets
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return self::$_buildinTargets;
    }

    /**
     * @return array
     */
    public function getSupportsAges()
    {
        return self::$_supportsAges;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function getIsSupportsAge($name)
    {
        return is_string($name) && in_array($name, $this->getSupportsAges());
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function isValid($name)
    {
        return is_string($name) && in_array($name, $this->getList());
    }

    /**
     * @param array|null $names
     *
     * @return array
     */
    public function getLabels($names = null)
    {
        $list = [
            self::TARGET_BOY => \Yii::t('common', 'for boys'),
            self::TARGET_GIRL => \Yii::t('common', 'for girls'),
            self::TARGET_WOMAN => \Yii::t('common', 'for women'),
            self::TARGET_MAN => \Yii::t('common', 'for men'),
            self::TARGET_HOME => \Yii::t('common', 'for home'),
            self::TARGET_ACCESSORIES => \Yii::t('common', 'accessories'),
        ];

        if ($names !== null) {
            $list = ArrayUtils::valuesByKeys($list, $names, true);
        }

        return $list;
    }

    /**
     * @param string $name
     *
     * @return string|false
     */
    public function getLabel($name)
    {
        $labels = $this->getLabels();
        return isset($labels[$name]) ? $labels[$name] : '';
    }

    /**
     * Резолвит ЦА для товара
     *
     * @param $string
     *
     * @return array|false прим. array(self::TARGET_BOY, self::TARGET_GIRL)
     */
    public function parseTargetString($string)
    {
        $targetGroups = [
            'for boys' => [self::TARGET_BOY],
            'for girls' => [self::TARGET_GIRL],
            'for boys and girls' => [self::TARGET_BOY, self::TARGET_GIRL],
            'for women' => [self::TARGET_WOMAN],
            'for men' => [self::TARGET_MAN],
            'for men and women' => [self::TARGET_MAN, self::TARGET_WOMAN],
            'for home' => [self::TARGET_HOME],
            'accessories' => [self::TARGET_ACCESSORIES],
            'home accessories' => [self::TARGET_HOME, self::TARGET_ACCESSORIES],
        ];

        $result = [];
        $targets = array_map([Utf8::class, 'trim'], Utf8::explode(',', $string));

        foreach ($targets as $target) {
            if (isset($targetGroups[$target])) {
                $result = $this->mergeTargets($result, $targetGroups[$target]);
            }
        }
        return $result;
    }

    /**
     * @param array|string $targetOne
     * @param array|string $targetTwo
     *
     * @return array
     */
    public function mergeTargets($targetOne, $targetTwo)
    {
        $result = [];
        foreach ([$targetOne, $targetTwo] as $targets) {
            if (!is_array($targets)) {
                $targets = [$targets];
            }
            foreach ($targets as $target) {
                if ($this->isValid($target) && !in_array($target, $result)) {
                    $result[] = $target;
                }
            }
        }
        return $result;
    }

    /**
     * @param $targets
     *
     * @return array
     */
    public function sanitizeTargets($targets)
    {
        if (!is_array($targets)) {
            $targets = [$targets];
        }

        foreach ($targets as $index => $target) {
            if (!$this->isValid($target)) {
                unset($targets[$index]);
            }
        }

        return $targets;
    }
}
