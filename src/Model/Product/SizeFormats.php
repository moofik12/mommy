<?php

namespace MommyCom\Model\Product;

final class SizeFormats
{
    const SIZEFORMAT_EU = 'eu';
    const SIZEFORMAT_US = 'us';
    const SIZEFORMAT_INT = 'int';
    const SIZEFORMAT_RU = 'ru';
    const SIZEFORMAT_CHN = 'chn';
    const SIZEFORMAT_UK = 'uk';
    const SIZEFORMAT_FR = 'fr';
    const SIZEFORMAT_IT = 'it';
    const SIZEFORMAT_JP = 'jp';
    const SIZEFORMAT_HEIGHT = 'height';
    const SIZEFORMAT_HEADWEAR = 'headwear';
    const SIZEFORMAT_GLOVES = 'gloves';
    const SIZEFORMAT_AGE = 'age';

    protected static $_buildinSizes = [
        self::SIZEFORMAT_EU,
        self::SIZEFORMAT_US,
        self::SIZEFORMAT_INT,
        self::SIZEFORMAT_RU,
        self::SIZEFORMAT_CHN,
        self::SIZEFORMAT_UK,
        self::SIZEFORMAT_FR,
        self::SIZEFORMAT_IT,
        self::SIZEFORMAT_JP,
        self::SIZEFORMAT_HEIGHT,
        self::SIZEFORMAT_HEADWEAR,
        self::SIZEFORMAT_GLOVES,
        self::SIZEFORMAT_AGE,
    ];

    protected function __construct()
    {
    }

    /**
     * @return SizeFormats
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return self::$_buildinSizes;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function isValid($name)
    {
        return is_string($name) && in_array($name, $this->getList());
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return [
            self::SIZEFORMAT_EU => 'EU',
            self::SIZEFORMAT_US => 'US',
            self::SIZEFORMAT_INT => 'INT',
            self::SIZEFORMAT_RU => 'RU',
            self::SIZEFORMAT_CHN => 'CHN',
            self::SIZEFORMAT_UK => 'UK',
            self::SIZEFORMAT_FR => 'FR',
            self::SIZEFORMAT_IT => 'IT',
            self::SIZEFORMAT_JP => 'JP',
            self::SIZEFORMAT_HEIGHT => 'Рост',
            self::SIZEFORMAT_HEADWEAR => 'Головной убор',
            self::SIZEFORMAT_GLOVES => 'Перчатки',
            self::SIZEFORMAT_AGE => 'Возраст',
        ];
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function getLabel($name)
    {
        $labels = $this->getLabels();
        return isset($labels[$name]) ? $labels[$name] : '';
    }
}
