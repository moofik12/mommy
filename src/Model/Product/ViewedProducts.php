<?php

namespace MommyCom\Model\Product;

use CDbConnection;
use CException;
use CMap;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

/**
 * Class ViewedProducts
 *
 * @property-read boolean $isOnlyActiveEvents
 * @property-read integer $limit
 * @method toArray():ProductRecord
 */
class ViewedProducts extends CMap
{
    /**
     * @var boolean
     */
    protected $_onlyActiveEvents;

    /**
     * @var integer
     */
    protected $_limit;

    /**
     * @var EventProductRecord[]
     */
    protected $eventProducts;

    /**
     * @param UserRecord $user
     * @param bool $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return ViewedProducts
     * @throws CException
     */
    public static function fromUser(UserRecord $user, bool $onlyActiveEvents = true, int $limit = 10, int $cache = 0)
    {
        $onlyActiveEvents = Cast::toBool($onlyActiveEvents);
        $limit = Cast::toUInt($limit);

        $self = new self();
        $self->_onlyActiveEvents = $onlyActiveEvents;
        $self->_limit = $limit;

        $eventProductsSelector = EventProductRecord::model();

        if ($onlyActiveEvents) {
            $eventProductsSelector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'onlyTimeActive' => null // call without params
                    ],
                ],
            ]);
        }

        $eventProductsSelector->with([
            'viewsByUser' => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => [
                    'userId' => $user->id,
                    'orderBy' => [['viewsByUser.number', 'viewsByUser.updated_at'], 'desc'],
                ],
            ],
        ]);

        if ($cache > 0) {
            $eventProductsSelector->cache($cache);
        }

        $productIds = $eventProductsSelector->findColumnDistinct('product_id', $limit);

        $result = [];
        foreach ($productIds as $productId) {
            $productsSelector = ProductRecord::model();

            if ($cache > 0) {
                $productsSelector->cache($cache);
            }

            $product = $productsSelector->findByPk($productId);

            if ($product !== null) {
                $result[] = $product;
            }
        }

        $self->clear();
        $self->copyFrom($result);
        $self->setReadOnly(true);

        return $self;
    }

    /**
     * @return bool
     */
    public function getIsOnlyActiveEvents()
    {
        return $this->_onlyActiveEvents;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return EventProductRecord[]
     */
    public function getEventProducts()
    {
        if ($this->eventProducts !== null) {
            return $this->eventProducts;
        }

        $productsIds = ArrayUtils::getColumn($this->toArray(), 'id');

        $products = [];

        foreach ($productsIds as $productId) {
            $selector = EventProductRecord::model()->productId($productId);
            if ($this->_onlyActiveEvents) {
                $selector->with([
                    'event' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'onlyTimeActive' => null // call without params
                        ],
                    ],
                ]);
            }

            $products = array_merge($products, $selector->findAll());
        }

        return $products;
    }

    /**
     * alias of toArray
     *
     * @return ProductRecord
     */
    public function getProducts()
    {
        return $this->toArray();
    }

    /**
     * @param int $limit
     *
     * @return GroupedProducts
     * @throws CException
     */
    public function getMostViewedProducts(int $limit): GroupedProducts
    {
        /** @var CDbConnection $db */
        $db = Yii::app()->db;

        /** @var SplitTesting $splitTesting */
        $splitTesting = \Yii::app()->splitTesting;

        if (!$splitTesting->isSplitTestEnabled(SplitTesting::SPLITTEST_NAME_MADE_IN)) {
            $query = 'SELECT object_id FROM views_tracking ORDER BY number DESC LIMIT :limit';
        } else {
            $madeIn = $splitTesting->getMadeInCountries()[$splitTesting->getMadeInVariant()];
            $madeIn = implode("','", $madeIn);

            $query = <<<SQL
SELECT object_id
FROM views_tracking, events_products
WHERE
  views_tracking.object_id = events_products.id AND
  events_products.made_in IN ('{$madeIn}')
ORDER BY views_tracking.number DESC
LIMIT :limit
SQL;
        }

        $cmd = $db->createCommand($query);
        $cmd->bindValues(['limit' => $limit]);
        $productIds = $cmd->query();

        $eventProductRecords = [];
        foreach ($productIds as $tableKey => $row) {
            $eventProductRecords[] = EventProductRecord::model()
                ->find("product_id=:id", [':id' => $row['object_id']]);
        }

        return GroupedProducts::fromProducts($eventProductRecords);
    }
}
