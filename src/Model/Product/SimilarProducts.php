<?php

namespace MommyCom\Model\Product;

use CMap;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\ViewsTracking\ViewsTracking;
use MommyCom\Model\ViewsTracking\ViewTrackingSimilarRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class SimilarProducts
 *
 * @property-read boolean $isOnlyActiveEvents
 * @property-read integer $limit
 * @method toArray():ProductRecord
 */
class SimilarProducts extends CMap
{
    /**
     * @var boolean
     */
    protected $_onlyActiveEvents;

    /**
     * @var integer
     */
    protected $_limit;

    /**
     * @var EventProductRecord[]
     */
    protected $eventProducts;

    /**
     * @param EventProductRecord[] $products
     * @param bool $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return SimilarProducts
     */
    public static function fromEventProducts($products, $onlyActiveEvents = true, $limit = 10, $cache = 0)
    {
        return self::fromProducts(ArrayUtils::getColumn($products, 'product'), $onlyActiveEvents, $limit, $cache);
    }

    /**
     * @param EventProductRecord $product
     * @param bool $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return SimilarProducts
     */
    public static function fromEventProduct($product, $onlyActiveEvents = true, $limit = 10, $cache = 0)
    {
        return self::fromEventProducts([$product], $onlyActiveEvents, $limit, $cache);
    }

    /**
     * @param ProductRecord[] $products
     * @param bool $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return SimilarProducts
     */
    public static function fromProducts($products, $onlyActiveEvents = true, $limit = 10, $cache = 0)
    {
        $products = Cast::toObjArr($products, 'MommyCom\Model\Db\ProductRecord');
        $productIds = ArrayUtils::getColumn($products, 'id');

        $onlyActiveEvents = Cast::toBool($onlyActiveEvents);
        $limit = Cast::toUInt($limit);

        $self = new self();
        $self->_onlyActiveEvents = $onlyActiveEvents;
        $self->_limit = $limit;

        $similarProductIds = ViewTrackingSimilarRecord::model()
            ->objectType(ViewsTracking::getObjectType('ProductRecord'))
            ->objectIdIn($productIds)
            ->similarObjectIdNotIn($productIds)
            ->orderBy(['number', 'updated_at'], 'desc')
            ->limit(30)
            ->cache(600)
            ->findColumnDistinct('object_id');

        $eventProductsSelector = EventProductRecord::model();
        $eventProductsSelector->productIdIn($similarProductIds);

        if ($onlyActiveEvents) {
            $eventProductsSelector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'onlyTimeActive' => null // call without params
                    ],
                ],
            ]);
        }

        if ($cache > 0) {
            $eventProductsSelector->cache($cache);
        }

        $productIds = $eventProductsSelector->findColumnDistinct('product_id', $limit);

        $result = [];
        foreach ($productIds as $productId) {
            $productsSelector = ProductRecord::model();

            if ($cache > 0) {
                $productsSelector->cache($cache);
            }

            $product = $productsSelector->findByPk($productId);

            if ($product !== null) {
                $result[] = $product;
            }
        }

        $self->clear();
        $self->copyFrom($result);
        $self->setReadOnly(true);

        return $self;
    }

    /**
     * @param ProductRecord $product
     * @param bool $onlyActiveEvents
     * @param int $limit
     * @param int $cache
     *
     * @return SimilarProducts
     */
    public static function fromProduct($product, $onlyActiveEvents = true, $limit = 10, $cache = 0)
    {
        return self::fromProducts([$product], $onlyActiveEvents, $limit, $cache);
    }

    /**
     * @return bool
     */
    public function getIsOnlyActiveEvents()
    {
        return $this->_onlyActiveEvents;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return EventProductRecord[]
     */
    public function getEventProducts()
    {
        if ($this->eventProducts !== null) {
            return $this->eventProducts;
        }

        $productsIds = ArrayUtils::getColumn($this->toArray(), 'id');

        $products = [];

        foreach ($productsIds as $productId) {
            $selector = EventProductRecord::model()->productId($productId);
            if ($this->_onlyActiveEvents) {
                $selector->with([
                    'event' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'onlyTimeActive' => null // call without params
                        ],
                    ],
                ]);
            }

            $products = array_merge($products, $selector->findAll());
        }

        return $products;
    }

    /**
     * alias of toArray
     *
     * @return ProductRecord
     */
    public function getProducts()
    {
        return $this->toArray();
    }
} 
