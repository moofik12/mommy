<?php

namespace MommyCom\Model\Product;

use CMap;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\ViewsTracking\ViewsTracking;
use MommyCom\Model\ViewsTracking\ViewTrackingByUserRecord;
use MommyCom\Model\ViewsTracking\ViewTrackingIncomingRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class ViewedProducts
 *
 * @property-read boolean $isOnlyActiveEvents
 * @property-read integer $limit
 * @method toArray():ProductRecord
 */
class LastViewedProducts extends CMap
{
    /**
     * @var boolean
     */
    protected $_onlyActiveEvents;

    /**
     * @var boolean
     */
    protected $_onlyEventsVisible;

    /**
     * @var integer
     */
    protected $_limit;

    /**
     * @var EventProductRecord[]
     */
    protected $eventProducts;

    /**
     * @param UserRecord $user
     * @param bool $onlyActiveEvents
     * @param bool $onlyEventsVisible
     * @param int $limit
     *
     * @return LastViewedProducts
     */
    public static function fromUser(UserRecord $user, $onlyActiveEvents = true, $onlyEventsVisible = true, $limit = 10)
    {
        $limit = Cast::toUInt($limit);

        $self = new self();
        $self->_onlyActiveEvents = $onlyActiveEvents;
        $self->_onlyEventsVisible = $onlyEventsVisible;
        $self->_limit = $limit;

        $viewedProductIds = ViewTrackingIncomingRecord::model()
            ->userId($user->id)
            ->objectType(ViewsTracking::getObjectType('ProductRecord'))
            ->orderBy('time', 'desc')
            ->limit($limit)
            ->findColumnDistinct('object_id');

        if (count($viewedProductIds) < $limit) {
            $subLimit = $limit - count($viewedProductIds);
            $viewedProductIds = array_merge(
                $viewedProductIds,
                ViewTrackingByUserRecord::model()
                    ->userId($user->id)
                    ->objectType(ViewsTracking::getObjectType('ProductRecord'))
                    ->orderBy('updated_at', 'desc')
                    ->limit($subLimit)
                    ->findColumnDistinct('object_id')
            );
        }

        $eventProductsSelector = EventProductRecord::model();

        if ($onlyActiveEvents) {
            $eventProductsSelector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'onlyTimeActive' => null // call without params
                    ],
                ],
            ]);
        }

        if ($onlyEventsVisible) {
            $eventProductsSelector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'isVisible' => [$onlyEventsVisible],
                    ],
                ],
            ]);
        }

        $eventProductsSelector->productIdIn($viewedProductIds);
        if (count($viewedProductIds) > 0) {
            $eventProductsSelector->orderBy('FIELD(product_id, "' . implode('", "', $viewedProductIds) . '")');
        }

        $productIds = $eventProductsSelector->findColumnDistinct('product_id', $limit);
        /* @var $productIds integer[] */

        $result = [];
        foreach ($productIds as $productId) {
            $productsSelector = ProductRecord::model();

            $product = $productsSelector->findByPk($productId);

            if ($product !== null) {
                $result[] = $product;
            }
        }

        $self->clear();
        $self->copyFrom($result);
        $self->setReadOnly(true);

        return $self;
    }

    /**
     * @return bool
     */
    public function getIsOnlyActiveEvents()
    {
        return $this->_onlyActiveEvents;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return EventProductRecord[]
     */
    public function getEventProducts()
    {
        if ($this->eventProducts !== null) {
            return $this->eventProducts;
        }

        $productsIds = ArrayUtils::getColumn($this->toArray(), 'id');

        $products = [];

        foreach ($productsIds as $productId) {
            $selector = EventProductRecord::model()->productId($productId);
            if ($this->_onlyActiveEvents) {
                $selector->with([
                    'event' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'onlyTimeActive' => null // call without params
                        ],
                    ],
                ]);
            }

            $products = array_merge($products, $selector->findAll());
        }

        return $products;
    }

    /**
     * alias of toArray
     *
     * @return ProductRecord
     */
    public function getProducts()
    {
        return $this->toArray();
    }
} 
