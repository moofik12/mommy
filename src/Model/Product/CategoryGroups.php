<?php

namespace MommyCom\Model\Product;

final class CategoryGroups
{
    /*
     * rules
    мальчикам    'детская одежда', 'товары для детей' + берется пол из соответствующего столбца
    девочкам    'детская одежда', 'товары для детей' + берется пол из соответствующего столбца
    новорожденным    'одежда для новорожденных', 'товары для новорожденных'
    будущим мамам    'товары для беременных'
    мамам и папам    'одежда для взрослых', 'красота и здоровье'
    игрушки и книги    'игрушки', 'книги'
    аксессуары    'аксессуары'
    товары для дома    'товары для дома'
    */

    const CATEGORY_BOYS = 'boys'; // мальчикам
    const CATEGORY_GIRLS = 'girls'; // девочкам
    const CATEGORY_MOMS_AND_DADS = 'momsAndDads'; // мамам и папам
    const CATEGORY_TOYS = 'toys'; // игрушки и книги
    const CATEGORY_ACCESSORIES = 'accessories'; // аксессуары
    const CATEGORY_HOME = 'home'; // товары для дома
    const CATEGORY_BEAUTY = 'beauty'; // красота и здоровье
    const CATEGORY_MOMS = 'moms'; // мамам
    const CATEGORY_DADS = 'dads'; // папам

    protected static $_associations = [
        self::CATEGORY_BOYS => [
            'Baby Clothing',
            'Goods for babies',
            'baby clothes',
            'Products for children',
            'for boys',
            'for boys and girls',
            'toys',
            'toys for boys',
            'books',
            'books for boys',
        ],
        self::CATEGORY_GIRLS => [
            'Baby Clothing',
            'Goods for babies',
            'baby clothes',
            'Products for children',
            'for girls',
            'for boys and girls',
            'toys',
            'toys for girls',
            'books',
            'books for girls',
        ],
        self::CATEGORY_MOMS_AND_DADS => [
            'clothes for adults',
            'goods for pregnant women',
            'clothes for women',
            'clothes for men',
            'for men and women',
            'for men',
            'for women',
        ],
        self::CATEGORY_TOYS => ['toys', 'books'],
        self::CATEGORY_ACCESSORIES => ['accessories'],
        self::CATEGORY_HOME => ['household products', 'for home'],
        self::CATEGORY_BEAUTY => ['beauty and health'],
        self::CATEGORY_MOMS => [
            'goods for pregnant women',
            'clothes for adults',
            'clothes for women',
            'for men and women',
            'for women',

            'accessories',
            'accessories for women',
            'cosmetics',
            'cosmetics for women',
        ],
        self::CATEGORY_DADS => [
            'clothes for adults',
            'clothes for men',
            'for men and women',
            'for men',

            'accessories',
            'accessories for men',
            'cosmetics',
            'cosmetics for men',
        ],
    ];

    protected static $_targetAssociations = [
        self::CATEGORY_BOYS => [ProductTargets::TARGET_BOY],
        self::CATEGORY_GIRLS => [ProductTargets::TARGET_GIRL],
        self::CATEGORY_MOMS_AND_DADS => [ProductTargets::TARGET_WOMAN, ProductTargets::TARGET_MAN],
        self::CATEGORY_TOYS => [ProductTargets::TARGET_BOY, ProductTargets::TARGET_GIRL],
        self::CATEGORY_ACCESSORIES => [ProductTargets::TARGET_ACCESSORIES],
        self::CATEGORY_HOME => [ProductTargets::TARGET_HOME],
        self::CATEGORY_BEAUTY => [ProductTargets::TARGET_WOMAN, ProductTargets::TARGET_MAN],
        self::CATEGORY_DADS => [ProductTargets::TARGET_MAN],
        self::CATEGORY_MOMS => [ProductTargets::TARGET_WOMAN],
    ];

    protected static $_buildinCategories = [
        self::CATEGORY_BOYS,
        self::CATEGORY_GIRLS,
        self::CATEGORY_TOYS,
        self::CATEGORY_MOMS_AND_DADS,
        self::CATEGORY_BEAUTY,
        self::CATEGORY_ACCESSORIES,
        self::CATEGORY_HOME,
        self::CATEGORY_DADS,
        self::CATEGORY_MOMS,
    ];

    protected function __construct()
    {
    }

    /**
     * @return CategoryGroups
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    public function getList()
    {
        return self::$_buildinCategories;
    }

    public function isValid($name)
    {
        return is_string($name) && in_array($name, $this->getList());
    }

    /**
     * @param string $name
     *
     * @return boolean
     */
    public function isInternalValid($name)
    {
        if (!is_string($name)) {
            return false;
        }

        foreach (static::$_associations as $categories) {
            if (in_array($name, $categories)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return [
            self::CATEGORY_BOYS => \Yii::t('common', 'boys'),
            self::CATEGORY_GIRLS => \Yii::t('common', 'girls'),
            self::CATEGORY_MOMS_AND_DADS => \Yii::t('common', 'moms and dads'),
            self::CATEGORY_TOYS => \Yii::t('common', 'toys and books'),
            self::CATEGORY_BEAUTY => \Yii::t('common', 'beauty and health'),
            self::CATEGORY_ACCESSORIES => \Yii::t('common', 'accessories'),
            self::CATEGORY_HOME => \Yii::t('common', 'household products'),
            self::CATEGORY_MOMS => \Yii::t('common', 'mothers'),
            self::CATEGORY_DADS => \Yii::t('common', 'fathers'),
        ];
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function getLabel($name)
    {
        $labels = $this->getLabels();
        return isset($labels[$name]) ? $labels[$name] : '';
    }

    /**
     * @param string $name название группы категорий
     *
     * @return array|false внутренние категории
     */
    public function getCategories($name)
    {
        return isset(self::$_associations[$name]) ? self::$_associations[$name] : false;
    }

    /**
     * Возвращает внутренние категории по категории товара
     * Так же возможна дополнительная фильтрация по таргету
     *
     * @param string $name название категории товара
     * @param string|string[]|null $target
     *
     * @return string[] внутренние категории
     */
    public function getInternalCategories($name, $target = null)
    {
        $result = [];
        foreach (self::$_associations as $key => $names) {
            if (in_array($name, $names)) {
                $result[] = $key;
            }
        }

        // дополнительная фильтрация по таргету
        if ($target !== null) {
            $targets = is_array($target) ? $target : [$target];
            foreach ($result as $index => $category) {
                $categoryTarget = $this->getTarget($category);

                $allowed = $categoryTarget === false ||
                    array_intersect($categoryTarget, $targets) !== [];

                if (!$allowed) {
                    unset($result[$index]);
                }
            }
            $result = array_values($result);
        }

        return $result;
    }

    /**
     * Возвращает пол к которой привязана категорий, если false то привязки нет
     *
     * @param $name
     *
     * @return string[]|false
     */
    public function getTarget($name)
    {
        return isset(self::$_targetAssociations[$name]) ? self::$_targetAssociations[$name] : false;
    }

    /**
     * @param $productTarget
     *
     * @return array
     */
    public function getCategoriesByTarget($productTarget)
    {
        $targetAssociations = self::$_targetAssociations;
        $categoriesByTarget = [];
        foreach ($targetAssociations as $category => $productTargets) {
            if (in_array($productTarget, $productTargets)) {
                $categoriesByTarget[] = $category;
            }
        }
        return $categoriesByTarget;
    }
}
