<?php

namespace MommyCom\Model\Product;

use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class ProductDimensions
 */
final class ProductDimensions
{
    const LENGTH_MILLIMETER = 'mm';
    const LENGTH_CENTIMETER = 'cm';
    const LENGTH_DECIMETER = 'dm';
    const LENGTH_METER = 'm';

    protected static $_variants = [
        self::LENGTH_MILLIMETER => [
            'мм',
            'мм.',
            'миллиметров',
            self::LENGTH_MILLIMETER,
        ],
        self::LENGTH_CENTIMETER => [
            'см',
            'см.',
            'сантиментров',
            self::LENGTH_CENTIMETER,
            '' // it's default than empty
        ],
        self::LENGTH_DECIMETER => [
            'дм',
            'дм.',
            'дециметров',
            self::LENGTH_DECIMETER,
        ],
        self::LENGTH_METER => [
            'м',
            'м.',
            'метр',
            'метр.',
            'метров',
            self::LENGTH_METER,
        ],
    ];

    protected static $_multipliers = [ // сдвиги по основе 10
        self::LENGTH_MILLIMETER => 3,
        self::LENGTH_CENTIMETER => 2,
        self::LENGTH_DECIMETER => 1,
        self::LENGTH_METER => 0,
    ];

    protected function __construct()
    {
    }

    /**
     * @return ProductDimensions
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * @param string $value
     *
     * @return string|false
     */
    protected function _getLengthBase($value)
    {
        $value = Utf8::trim($value);
        foreach (self::$_variants as $base => $variants) {
            if (in_array($value, $variants)) {
                return $base;
            }
        }
        return false;
    }

    /**
     * @param string $inLengthFormat
     * @param string $outLengthFormat
     *
     * @return int
     */
    protected function _getMultiplier($inLengthFormat, $outLengthFormat)
    {
        return pow(10, self::$_multipliers[$outLengthFormat] - self::$_multipliers[$inLengthFormat]);
    }

    protected function _getIdealBase(array $dimensions)
    {
        /* @todo write smart code here */
        return self::LENGTH_CENTIMETER;
    }

    /**
     * @param string $value
     *
     * @return array array(width, height, z-width)|false all values is centimeters
     */
    public function parseDimensionString($value)
    {
        $value = Utf8::trim(Cast::toStr($value));

        if (mb_strlen($value) === 0) {
            return false;
        }

        $replacements = [
            // ideal result is 10x10x15
            ',' => '.',
            'х' => 'x', // cyrillic 'x' to english 'x'
            '*' => 'x', // format 10*10*15 to 10 10 15
        ];

        $value = str_replace(
            array_keys($replacements),
            array_values($replacements),
            mb_strtolower($value)
        );

        $words = Utf8::words($value);

        if (count($words) !== 2) {
            return false;
        }

        $items = Utf8::explode('x', $words[0]);
        $base = $words[1];
        $internalBase = $this->_getLengthBase($base);

        if (count($items) !== 3) {
            return false;
        }

        if ($internalBase === false) {
            return false;
        }

        list($width, $height, $widthZ) = $items;

        if (!is_numeric($width) || !is_numeric($height) || !is_numeric($widthZ)) {
            return false;
        }

        $multiplier = $this->_getMultiplier($internalBase, self::LENGTH_CENTIMETER);

        $outWidth = Cast::toUFloat($width) * $multiplier;
        $outHeight = Cast::toUFloat($height) * $multiplier;
        $outWidthZ = Cast::toUFloat($widthZ) * $multiplier;

        return [$outWidth, $outHeight, $outWidthZ];
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    public function isValid($value)
    {
        return $this->parseDimensionString($value) !== false;
    }

    /**
     * Возвращает обьем занимаемый товаром
     *
     * @param array|string $dimensions
     *
     * @return float|false cubic centimetres
     */
    public function getVolume($dimensions)
    {
        if (is_string($dimensions)) {
            $dimensions = $this->parseDimensionString($dimensions);
        }
        if (!is_array($dimensions) || count($dimensions) !== 3) {
            return false;
        }

        return $dimensions[0] * $dimensions[1] * $dimensions[2];
    }

    /**
     * @param array $dimensions
     *
     * @return string
     */
    public function convertToString(array $dimensions)
    {
        $formattedDimensions = Utf8::implode('x', Cast::toStrArr($dimensions));

        $idealBase = self::_getIdealBase($dimensions);
        $idealBaseVariant = self::$_variants[$idealBase];
        $formattedBase = reset($idealBaseVariant);

        return $formattedDimensions . ' ' . $formattedBase;
    }
}
