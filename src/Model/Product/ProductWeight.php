<?php

namespace MommyCom\Model\Product;

use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

final class ProductWeight
{
    const WEIGHT_KILOGRAM = 'kg';
    const WEIGHT_GRAM = 'gr';

    private $_variants = [
        self::WEIGHT_KILOGRAM => [
            'kg',
            'kilogram',
            'kg.',
            '', // by default abbr can be empty
        ],
        self::WEIGHT_GRAM => [
            'g',
            'g.',
        ],
    ];

    protected function __construct()
    {
        $this->_variants[self::WEIGHT_KILOGRAM][] = \Yii::t('common', 'kg');
        $this->_variants[self::WEIGHT_GRAM][] = \Yii::t('common', 'g');
    }

    /**
     * @return ProductWeight
     */
    public static function instance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * @param string $value
     *
     * @return string|false
     */
    protected function _getWeightBase($value)
    {
        $value = Utf8::trim($value);
        foreach ($this->_variants as $base => $variants) {
            if (in_array($value, $variants)) {
                return $base;
            }
        }
        return false;
    }

    protected function _sanitizeNumber($value)
    {
        return (float)str_replace(',', '.', trim($value));
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function isValid($value)
    {
        $exploded = Utf8::words($value);

        if (count($exploded) != 2) {
            return false;
        }

        list($value, $base) = $exploded;

        $value = Utf8::replace(',', '.', $value);

        if (!is_numeric($value)) {
            return false;
        }

        $base = $this->_getWeightBase($exploded[1]);
        if ($base === false) {
            Yii::trace('base not found');
            return false;
        }

        return true;
    }

    /**
     * @param string $value
     *
     * @return integer|false weight in grams
     */
    public function parseString($value)
    {
        $exploded = Utf8::words($value);

        if (!$this->isValid($value)) {
            return false;
        }

        $base = self::WEIGHT_KILOGRAM;
        if (count($exploded) > 1) {
            $base = $this->_getWeightBase($exploded[1]);
        }

        $number = $this->_sanitizeNumber($exploded[0]);

        switch ($base) {
            case self::WEIGHT_KILOGRAM:
                return (int)($number * 1000);
            case self::WEIGHT_GRAM:
                return (int)$number;
        }

        return false;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function convertToString($value)
    {
        $base = self::WEIGHT_GRAM;

        if ($value >= 1000) {
            $base = self::WEIGHT_KILOGRAM;
            $value = round($value / 1000, 2);
        }

        $baseVariant = $this->_variants[$base];
        return Cast::toStr($value) . ' ' . reset($baseVariant);
    }
}
