<?php

namespace MommyCom\Model\Misc;

use CList;
use CModel;
use Exception;
use IDataProvider;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class CsvReader
 */
class CsvFile extends CList
{
    /**
     * @param $filename
     * @param string $delimiter
     * @param string $enclosure
     *
     * @return static
     */
    public static function load($filename, $delimiter = ',', $enclosure = '"')
    {
        $instance = new static();

        $handle = fopen($filename, 'r');
        while (($lineData = fgetcsv($handle, 0, $delimiter, $enclosure)) !== false) {
            foreach ($lineData as &$item) {
                $item = Utf8::replace('\\\\', '\\', $item); // unescape slashes
            }
            unset($item);

            $instance[] = $lineData;
        }
        fclose($handle);

        return $instance;
    }

    /**
     * @param array $array
     *
     * @return static
     */
    public static function loadFromArray($array)
    {
        return new static($array);
    }

    protected static function _solveColumns(array $columns)
    {
        $result = [];
        foreach ($columns as $key => $value) {
            $newKey = is_callable($value) ? $key : $value;
            $result[$newKey] = $value;
        }
        return $result;
    }

    protected static function _resolveModelHeader(array $models, array $columns, array $header)
    {
        if ($header === [] && $models !== []) {
            $model = reset($models);
            /** @var $model CModel */
            $attributeNames = $columns === [] ? $model->attributeNames() : $columns;
            $header = array_map([$model, 'getAttributeLabel'], $attributeNames);
        }
        return Cast::toStrArr($header);
    }

    protected static function _modelsToArray(array $models, array $columns = [])
    {
        $result = [];

        if ($columns === []) {
            foreach ($models as $model) {
                $result[] = $model->getAttributes();
            }
        } else {
            foreach ($models as $model) {
                $attributes = [];
                foreach ($columns as $value) {
                    if (is_callable($value)) {
                        $value = call_user_func($value, $model);
                    } else {
                        $value = ArrayUtils::getValueByPath($model, $value);
                    }

                    $attributes[] = Cast::toStr($value);
                }
                $result[] = $attributes;
            }
        }

        return $result;
    }

    /**
     * @param CModel[] $models
     * @param array $columns пустой массив - автоопределение
     * @param array|false $header пустой массив - автоопределение, false - нет заголовков
     *
     * @return static
     */
    public static function loadFromModels(array $models, array $columns = [], $header = [])
    {
        $result = [];

        $columns = self::_solveColumns($columns);
        $columnNames = array_keys($columns);

        if ($header !== false) {
            $result[] = self::_resolveModelHeader($models, $columnNames, $header);
        }

        $result = array_merge($result, self::_modelsToArray($models, $columns));

        return new static($result);
    }

    /**
     * @param IDataProvider $provider
     * @param array $columns пустой массив - автоопределение
     * @param array|false $header пустой массив - автоопределение, false - нет заголовков
     * @param boolean $fetchAll
     *
     * @return static
     */
    public static function loadFromProvider(IDataProvider $provider, array $columns = [], $header = [], $fetchAll = true)
    {
        $result = [];

        $columns = self::_solveColumns($columns);
        $columnNames = array_keys($columns);

        $pagination = $provider->getPagination();

        if ($fetchAll) {
            $pagination->currentPage = 0;
        }

        $models = $provider->getData();
        $result = array_merge($result, self::_modelsToArray($models, $columns));

        if ($header !== false) {
            $headerData = self::_resolveModelHeader($models, $columnNames, $header);
        }

        if ($pagination !== false && $fetchAll) {
            $pageCount = min($pagination->pageCount, intval(100000 / $pagination->pageSize));
            //$pageCount = $pagination->pageCount;
            for ($page = $pagination->currentPage + 1; $page < $pageCount; $page++) {
                $pagination->currentPage = $page;
                $models = $provider->getData(true);
                $result = array_merge($result, self::_modelsToArray($models, $columns));

                if ($page % 5 == 0 && function_exists('gc_collect_cycles')) {
                    gc_collect_cycles();
                }
            }
        }

        if ($header !== false) {
            array_unshift($result, $headerData);
        }

        return new static($result);
    }

    /**
     * @return string
     */
    public function toString()
    {
        $filename = tempnam(sys_get_temp_dir(), rand());
        $this->save($filename);
        $text = file_get_contents($filename);
        try {
            unlink($filename); //лишний раз не мусорим
        } catch (Exception $e) {
        }
        return $text;
    }

    public function __toString()
    {
        return $this->toString();
    }

    public function save($filename)
    {
        $handle = fopen($filename, 'w+');

        foreach ($this as $items) {
            //fputcsv($handle, $items, ',', '"');

            foreach ($items as &$item) {
                $item = Utf8::replace('"', '""', $item); // escape double quotes
                $item = Utf8::replace('\\', '\\\\', $item); // escape slashes
            }
            unset($item);

            $csvRow = '"' . Utf8::implode('","', $items) . '"' . "\n";
            fwrite($handle, $csvRow); // стандартная fputcsv обрезает лидирующие '0' у строк
        }

        fclose($handle);
    }
}
