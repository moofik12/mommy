<?php

namespace MommyCom\Model\Misc;

use CList;
use InvalidArgumentException;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class StringList
 * Обертка
 */
class StringList extends CList
{
    /**
     * @param string|array|null $data
     * @param bool $readOnly
     *
     * @throws InvalidArgumentException
     */
    public function __construct($data = null, $readOnly = false)
    {
        if (is_array($data)) {
            $data = Cast::toStrArr($data);
        } else if (is_scalar($data)) {
            $data = Utf8::trimArr(Utf8::explode(',', Cast::toStr($data)));
        }
        if (!is_array($data)) {
            $data = [];
        }
        parent::__construct($data, $readOnly);
    }

    public function __toString()
    {
        return Utf8::implode(', ', $this->toArray());
    }
}
