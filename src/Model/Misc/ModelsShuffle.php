<?php

namespace MommyCom\Model\Misc;

use CException;
use CMap;
use CModel;
use LogicException;
use Yii;

class ModelsShuffle extends CMap
{
    /**
     * Результат перемешивания кэшируется
     *
     * @var bool
     */
    private $_cache;

    /**
     * UnitTime время на которое будет кэширован результат
     *
     * @var int
     */
    private $_cacheTime;

    /**
     * @var string
     */
    protected $_cacheKey = '';

    /**
     * @param CModel[] $objects
     * @param array $attributesDependency в зависимости от каких атрибутов кэш считается валидным
     * @param bool $cache
     * @param int $cacheTime
     *
     * @throws CException
     */
    public function __construct(array $objects, $attributesDependency = [], $cache = true, $cacheTime = 60)
    {
        $data = [];
        $this->_cache = $cache;
        $this->_cacheTime = $cacheTime;
        $attributesDependency = empty($attributesDependency) ? null : $attributesDependency;

        /** @var CModel $object */
        foreach ($objects as $object) {
            if (!is_object($object) || !$object instanceof CModel) {
                throw new LogicException('Array must have only objects and instance of CModel');
            }

            $crc = crc32(implode('_', $object->getAttributes($attributesDependency)));
            $key = sprintf("%u", $crc);

            $data[$key] = $object;
        }

        $this->_cacheKey = $this->_getKey($data);

        $this->copyFrom($data);

        $this->_shuffle();

        $this->setReadOnly(true);
    }

    public function shuffle()
    {
        $this->_shuffle(true);
    }

    public function getData()
    {
        return array_values($this->toArray());
    }

    private function _shuffle($force = false)
    {
        $this->setReadOnly(false);
        $cacheProvider = Yii::app()->cache;
        $dataShuffle = [];
        $data = $this->toArray();
        $keys = array_keys($data);
        shuffle($keys);

        if ($this->_cache && $cacheProvider) {
            $cacheSorted = $cacheProvider->get($this->_cacheKey);
            if ($cacheSorted && !$force) {
                $keys = $cacheSorted;
            } else {
                $cacheProvider->set($this->_cacheKey, $keys, $this->_cacheTime);
            }
        }

        foreach ($keys as $key) {
            $dataShuffle[$key] = $data[$key];
        }

        $this->copyFrom($dataShuffle);

        $this->setReadOnly(true);
    }

    private function _getKey(array $data)
    {
        $keys = array_keys($data);
        sort($keys);

        $key = __CLASS__ . md5(implode('_', $keys));
        return $key;
    }
}
