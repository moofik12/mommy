<?php

namespace MommyCom\Model\Assortment;

use CModel;
use CVarDumper;
use MommyCom\Model\Assortment\Exception\AssortmentException;
use MommyCom\Model\Assortment\Exception\BrandNotFoundException;
use MommyCom\Model\Assortment\Exception\InvalidColumnCountException;
use MommyCom\Model\Assortment\Exception\SupplierNotFoundException;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Model\Product\SizeFormats;
use MommyCom\Model\Product\SizeGuideGroups;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use ReflectionClass;

class AssortmentProduct extends CModel
{
    const ASSORTMENT_LINE_COLUMN_NUMBER = 39;

    const HEIGHT_DEFAULT_UNIT = ' см'; //при формировании из csv автоматическое добавление ед. измерения если её нет

    /*
    "Поставщик", "Артикул","Штрихкод","Код","Бренд","НомерГТД","Категория товара","Раздел на сайте","Подаздел на сайте",
    "Наименование","СтранаДизайн","СтранаПроизводство","Пол","РЕЗЕРВ",
    "EU","US","INT","UA/RU","CHN","UK","FR","IT","JP","РОСТ","ГоловнойУбор","Перчатки", "Размерная сетка","РазмерЭтикетка"
    ,"ВОЗРАСТ",
    "КодЦвета","Цвет","Состав","Срок годности","Описание","ВесБрутто","Габариты ","ЗакупочнаяЦена",
    "РыночнаяЦена","ЦенаПродажи","Фото"
     */

    public $lineNum;
    public $lineColumnCount;

    public $supplier;
    public $article;
    public $barcode;
    public $internalCode;
    public $brand;

    public $category;
    public $name;

    public $designIn;
    public $madeIn;

    public $target;

    public $number;

    public $sizeformat;
    public $size;
    public $sizeGuide;

    /**
     * @var array все доступные размерные сетки
     */
    public $sizes;

    /**
     * @var string
     */
    public $label;

    public $age;

    public $colorCode;
    public $color;

    /**
     * Состав
     *
     * @var string
     */
    public $composition;

    /**
     * Срок годности
     *
     * @var string
     */
    public $shelfLife;

    public $description;

    public $weight;
    public $dimensions;

    public $pricePurchase;
    public $priceMarket;
    public $price;

    public $photo;
    public $parentSection;
    public $section;
    protected static $_sections = [];
    /** @var  int */
    protected $_sectionId;
    /** @var  int */
    protected $_colorCode;

    protected $_sizesPriorities = [
        'int',
        'ru',
        'eu',
        'us',
        'chn',
        'uk',
        'fr',
        'it',
        'jp',
        'height',
        'age',
        'headwear',
        'gloves',
    ];

    protected $assortmentTable;

    public function attributeNames()
    {
        $class = new ReflectionClass(get_class($this));
        $names = [];
        foreach ($class->getProperties() as $property) {
            $name = $property->getName();
            if ($property->isPublic() && !$property->isStatic()) {
                $names[] = $name;
            }
        }
        return $names;
    }

    public function rules()
    {
        return [
            ['lineColumnCount', 'compare', 'compareValue' => static::ASSORTMENT_LINE_COLUMN_NUMBER],
            ['article, brand, name, madeIn, designIn, color, category, target', 'required'],
            ['supplier, article, brand, name, madeIn, designIn, color, colorCode, category, target, sizeGuide, parentSection, section', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['number', 'required'],
            ['number', 'numerical', 'min' => 1],

            ['pricePurchase, priceMarket, price', 'required'],
            ['pricePurchase, priceMarket, price', 'filter', 'filter' => [Cast::class, 'smartStringToUFloat']],
            ['pricePurchase, priceMarket, price', 'numerical', 'min' => 1, 'max' => 1000000000], // million is enough

            ['pricePurchase', 'compare', 'compareAttribute' => 'price', 'operator' => '<=', 'message' => \Yii::t('common', 'Стоимость закупки должна быть меньше чем стоимость продажи')],
            ['price', 'compare', 'compareAttribute' => 'priceMarket', 'operator' => '<=', 'message' => \Yii::t('common', 'Стоимость продажи быть меньше чем рыночная стоимость')],

            //array('sizeformat, size', 'required'),

            ['weight', 'extendedWeightValidation'],

            ['supplier', 'extendedSupplierValidation'],
            ['brand', 'extendedBrandValidation'],
            ['age', 'extendedAgeValidation'],
            ['target', 'extendedTargetValidation'],
            ['category', 'extendedCategoryValidation'],
            ['sizeGuide', 'extendedSizeGuideValidation', 'targetAttribute' => 'target'],
            ['section', 'validSection'],
            ['colorCode', 'validColorCode'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'lineNum' => \Yii::t('common', 'Номер строки'),
            'lineColumnCount' => \Yii::t('common', 'Количество колонок'),

            'id' => '#',
            'name' => \Yii::t('common', 'Название'),
            'brand' => \Yii::t('common', 'Бренд'),
            'supplier' => \Yii::t('common', 'Поставщик'),
            'article' => \Yii::t('common', 'Артикул'),
            'number' => \Yii::t('common', 'Количество'),

            'price' => \Yii::t('common', 'Стоимость'),
            'pricePurchase' => \Yii::t('common', 'Базовая стоимость'),
            'priceMarket' => \Yii::t('common', 'Рыночная стоимость'),
            'color' => \Yii::t('common', 'Цвет'),

            'age' => \Yii::t('common', 'Возраст'),

            'sizeformat' => \Yii::t('common', 'Тип размера'),
            'size' => \Yii::t('common', 'Размер'),
            'sizes' => \Yii::t('common', 'Размеры'),
            'sizeGuide' => \Yii::t('common', 'Размерная сетка'),

            'parentSection' => \Yii::t('common', 'Раздел на сайте'),
            'section' => \Yii::t('common', 'Подраздел на сайте'),
        ];
    }

    public function validColorCode($attribute)
    {
        $colorCode = Cast::toStr($this->$attribute);
        if ($colorCode != '') {
            $productColorCodes = ProductColorCodes::instance();
            $productColorCodeList = $productColorCodes->getUnifiedLabels();
            if (!in_array(mb_strtolower(trim($colorCode)), $productColorCodeList)) {
                $this->addError($attribute, \Yii::t('common', 'Код цвета не найден в карте цветов'), '\n' . CVarDumper::dumpAsString($productColorCodes->getLabels()));
            }
        }
    }

    public function validSection($attribute)
    {
        $sectionString = $this->$attribute;
        $this->_sectionId = 0;
        if ($sectionString !== '' && $this->parentSection !== '') {
            $parentSectionName = lcfirst(trim($this->parentSection));
            $sectionName = lcfirst(trim($sectionString));

            if (isset(self::$_sections[$parentSectionName])) {
                $parentSection = self::$_sections[$parentSectionName];
            } else {
                $parentSection = ProductSectionRecord::model()->name($parentSectionName)->onlyParent()->find();
                self::$_sections[$parentSectionName] = $parentSection;
            }

            $sectionKey = $parentSectionName . '_' . $sectionName;
            $parentId = 0;
            if ($parentSection !== null) {
                $parentId = $parentSection->id;
            }

            if (isset(self::$_sections[$sectionKey])) {
                $section = self::$_sections[$sectionKey];
            } else {
                $section = ProductSectionRecord::model()
                    ->parentId($parentId)
                    ->name($sectionName)
                    ->onlyChild()
                    ->find();

                if ($section === null) {
                    $section = ProductSectionRecord::model()
                        ->name($sectionName)
                        ->onlyChild()
                        ->find();
                }

                self::$_sections[$sectionKey] = $section;
            }

            if ($parentSection === null) {
                $this->addError($attribute, \Yii::t('common', 'Родительский раздел в таблице "{parentSectionName}" в "{sectionString}" не существует',
                    [
                        '{parentSectionName}' => $parentSectionName,
                        '{sectionString}' => $sectionString,
                    ]));
            } elseif ($section === null) {
                $this->addError($attribute, \Yii::t('common', 'Раздел в таблице "{sectionName}" в "{sectionString}" не существует',
                    [
                        '{sectionName}' => $sectionName,
                        '{sectionString}' => $sectionString,
                    ]));
            } elseif ($section->parent_id != $parentSection->id) {
                $this->addError($attribute, \Yii::t('common', 'Родительский раздел в таблице "{parentSectionName}" в "{sectionString}" указан неверно',
                        [
                            '{parentSectionName}' => $parentSectionName,
                            '{sectionString}' => $sectionString,
                        ]) . ' ' . $parentSection->id . ' ' . $section->parent_id);
            } else {
                $this->_sectionId = $section->id;
            }
        }
    }

    public function extendedWeightValidation($attribute)
    {
        $value = $this->$attribute;
        if (!ProductWeight::instance()->isValid($value)) {
            $this->addError($attribute, \Yii::t('common', 'Значение "{value}" не получается обработать как вес товара', ['{value}' => $value]));
        }
    }

    public function extendedSupplierValidation($attribute)
    {
        $value = $this->$attribute;
        if ($value == '') {
            return; // do nothing
        }

        $supplier = SupplierRecord::model()->name($value)->find();
        if ($supplier == null) {
            $this->addError($attribute, \Yii::t('common', 'Поставщик "{value}" не найден в БД', ['{value}' => $value]));
        }
    }

    public function extendedBrandValidation($attribute)
    {
        $value = $this->$attribute;
        $brand = BrandRecord::model()->findByName($value);
        if ($brand == null) {
            $this->addError($attribute, \Yii::t('common', 'Бренд "{value}" не найден в БД', ['{value}' => $value]));
        }
    }

    public function extendedAgeValidation($attribute)
    {
        $value = $this->$attribute;
        if (!empty($value)) {
            $ageRange = AgeGroups::instance()->parseAgeStringRange($value);
            if ($ageRange[0] == 0 && $ageRange[1] == 0) {
                $this->addError($attribute, \Yii::t('common', 'Диапазон возраста "{value}" не поддерживается системой', ['{value}' => $value]));
            }
        }
    }

    public function extendedTargetValidation($attribute)
    {
        $value = $this->$attribute;
        $targetGroup = ProductTargets::instance()->parseTargetString($value);
        if (empty($targetGroup)) {
            $this->addError($attribute, \Yii::t('common', 'Пол "{value}" не поддерживается системой', ['{value}' => $value]));
        }
    }

    public function extendedSizeGuideValidation($attribute, $params)
    {
        $value = $this->$attribute;
        $targetAttr = $params['targetAttribute'];

        if (empty($value)) {
            return; // размерную сетку указывать не обязательно
        }

        $target = $this->$targetAttr;
        $targetGroup = ProductTargets::instance()->parseTargetString($target);

        $sizeGuide = SizeGuideGroups::instance();
        $sizeCategory = $sizeGuide->getCategory($value);
        $sizeTarget = $sizeGuide->getTarget($sizeCategory);

        if ($sizeCategory === false) {
            $this->addError($attribute, \Yii::t('common', 'Указанная размерная сетка "{value}" не поддерживается системой', ['{value}' => $value]));
        } elseif (!in_array($sizeTarget, $targetGroup)) {
            $this->addError($attribute, \Yii::t('common', 'Указанная размерная сетка "{value}" не для этого пола', ['{value}' => $value]));
        }
    }

    public function extendedCategoryValidation($attribute)
    {
        $value = $this->$attribute;
        $isValid = CategoryGroups::instance()->isInternalValid($value);
        if (!$isValid) {
            $this->addError($attribute, \Yii::t('common', 'Категория "{value}" не поддерживается системой', ['{value}' => $value]));
        }
    }

    public function validate($attributes = null, $clearErrors = true)
    {
        if (!parent::validate($attributes, $clearErrors)) {
            // hack
            $errors = ['lineNum' => \Yii::t('common', 'На строке #{line} произошли ошибки', ['{line}' => $this->lineNum])] + $this->getErrors();
            $this->clearErrors();
            $this->addErrors($errors);
            return false;
        }
        return true;
    }

    public function getAssortmentTable()
    {
        return $this->assortmentTable;
    }

    /**
     * @param AssortmentTable $assortmentTable
     * @param integer $lineNum
     * @param array $lineData
     *
     * @return AssortmentProduct
     * @throws InvalidColumnCountException
     */
    public static function fromLines(AssortmentTable $assortmentTable, $lineNum, array $lineData)
    {
        $model = new static();

        $model->assortmentTable = $assortmentTable;
        $model->lineNum = $lineNum;

        $model->lineColumnCount = count($lineData);
        if ($model->lineColumnCount !== static::ASSORTMENT_LINE_COLUMN_NUMBER) {
            throw new InvalidColumnCountException(
                \Yii::t('common', 'Ошибка, строка содержит {count} колонок а должна {number}, содержимое: ',
                    [
                        '{count}' => $model->lineColumnCount,
                        '{number}' => self::ASSORTMENT_LINE_COLUMN_NUMBER,
                    ]
                ) .
                CVarDumper::dumpAsString($lineData)
            );
        }

        // import columns by fucking magic-indexed offsets
        $model->supplier = $lineData[0];
        $model->article = $lineData[1];
        $model->barcode = $lineData[2];
        $model->internalCode = $lineData[3];
        $model->brand = $lineData[4];
        $model->category = $lineData[5];
        $model->parentSection = $lineData[6];
        $model->section = $lineData[7];
        $model->name = $lineData[8];
        $model->designIn = $lineData[9];
        $model->madeIn = $lineData[10];
        $model->target = $lineData[11];
        $model->number = $lineData[12];

        // resolve sizes
        $ageGroups = AgeGroups::instance();
        $age = $ageGroups->convertAgeRangeToString(
            $ageGroups->parseAgeStringRange($lineData[27])
        ); // reformat

        $sizes = [
            'eu' => $lineData[13],
            'us' => $lineData[14],
            'int' => $lineData[15],
            'ru' => $lineData[16],
            'chn' => $lineData[17],
            'uk' => $lineData[18],
            'fr' => $lineData[19],
            'it' => $lineData[20],
            'jp' => $lineData[21],
            'height' => self::getHeightNormalization($lineData[22]),
            'headwear' => $lineData[23],
            'gloves' => $lineData[24],

            'age' => $age,
        ];

        $model->sizeGuide = $lineData[25];
        $model->label = $lineData[26];
        $model->age = $age;
        $model->colorCode = $lineData[28];
        $model->color = $lineData[29];
        $model->composition = $lineData[30];
        $model->shelfLife = $lineData[31];
        $model->description = $lineData[32];
        $model->weight = $lineData[33];
        $model->dimensions = $lineData[34];
        $model->pricePurchase = $lineData[35];
        $model->priceMarket = $lineData[36];
        $model->price = $lineData[37];
        $model->photo = $lineData[38];

        $model->sizes = ArrayUtils::onlyNonEmpty($sizes, true);

        foreach ($model->_sizesPriorities as $sizeformat) {
            if (!isset($model->sizes[$sizeformat])) {
                continue;
            }

            $size = Utf8::trim($model->sizes[$sizeformat]);
            if (mb_strlen($size) > 0) {
                $model->sizeformat = $sizeformat;
                $model->size = $size;
                break;
            }
        }

        return $model;
    }

    /**
     * @param AssortmentTable $assortmentTable
     * @param EventProductRecord $product
     *
     * @return AssortmentProduct
     */
    public static function fromModel(AssortmentTable $assortmentTable, EventProductRecord $product)
    {
        $model = new static();

        $model->assortmentTable = $assortmentTable;

        // import columns by fucking magic-indexed offsets
        $model->supplier = $product->supplier->name;
        $model->article = $product->article;
        $model->barcode = $product->barcode;
        $model->internalCode = $product->internal_code;
        $model->brand = $product->brand->name;
        $model->category = $product->category;
        $model->name = $product->name;
        $model->designIn = $product->design_in;
        $model->madeIn = $product->made_in;
        $model->target = Utf8::implode(', ', ProductTargets::instance()->getLabels($product->target));
        $model->number = $product->number;

        // resolve sizes
        $ageGroups = AgeGroups::instance();

        $model->sizes = $product->getSizes(true);

        $model->sizeGuide = $product->size_guide;
        $model->label = $product->label;
        $model->age = $ageGroups->convertAgeRangeToString($product->ageRange);

        $model->colorCode = ProductColorCodes::instance()->getLabel($product->product->color_code);
        $model->color = $product->color;
        $model->composition = $product->composition;
        $model->shelfLife = $product->shelflife;
        $model->description = $product->description;
        $model->weight = $product->weightReplacement;
        $model->dimensions = $product->dimensions;
        $model->pricePurchase = $product->price_purchase;
        $model->priceMarket = $product->price_market;
        $model->price = $product->price;
        $model->photo = $product->photo;
        $parentSection = $product->product->section !== null ? $product->product->section->parent : null;
        /** @var  $parentSection ProductSectionRecord */
        $model->parentSection = $parentSection !== null ? $parentSection->name : '';
        $model->section = $product->product->section !== null ? $product->product->section->name : '';
        return $model;
    }

    /**
     * @return string[]
     */
    public function getLineData()
    {
        $sizes = $this->sizes;

        return [
            $this->supplier,
            $this->article,
            $this->barcode,
            $this->internalCode,
            $this->brand,
            $this->category,
            $this->parentSection,
            $this->section,
            $this->name,
            $this->designIn,
            $this->madeIn,
            $this->target,
            $this->number,
            isset($sizes[SizeFormats::SIZEFORMAT_EU]) ? $sizes[SizeFormats::SIZEFORMAT_EU] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_US]) ? $sizes[SizeFormats::SIZEFORMAT_US] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_INT]) ? $sizes[SizeFormats::SIZEFORMAT_INT] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_RU]) ? $sizes[SizeFormats::SIZEFORMAT_RU] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_CHN]) ? $sizes[SizeFormats::SIZEFORMAT_CHN] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_UK]) ? $sizes[SizeFormats::SIZEFORMAT_UK] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_FR]) ? $sizes[SizeFormats::SIZEFORMAT_FR] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_IT]) ? $sizes[SizeFormats::SIZEFORMAT_IT] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_JP]) ? $sizes[SizeFormats::SIZEFORMAT_JP] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_HEIGHT]) ? $sizes[SizeFormats::SIZEFORMAT_HEIGHT] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_HEADWEAR]) ? $sizes[SizeFormats::SIZEFORMAT_HEADWEAR] : '',
            isset($sizes[SizeFormats::SIZEFORMAT_GLOVES]) ? $sizes[SizeFormats::SIZEFORMAT_GLOVES] : '',
            $this->sizeGuide,
            $this->label,
            $this->age,
            $this->colorCode,
            $this->color,
            $this->composition,
            $this->shelfLife,
            $this->description,
            $this->weight,
            $this->dimensions,
            $this->pricePurchase,
            $this->priceMarket,
            $this->price,
            $this->photo,
        ];
    }

    /**
     * @return EventProductRecord
     * @throws AssortmentException
     * @throws BrandNotFoundException
     * @throws SupplierNotFoundException
     */
    public function convertToEventProduct()
    {
        $event = $this->getAssortmentTable()->getEvent();
        /* @var $event EventRecord */

        $productTargets = ProductTargets::instance();
        $ageGroups = AgeGroups::instance();
        $productWeight = ProductWeight::instance();

        $supplierName = Utf8::trim($this->supplier);

        $supplier = $supplierName == '' ?
            $event->supplier :
            SupplierRecord::model()->name($this->supplier)->find();
        if ($supplier === null) {
            throw new SupplierNotFoundException(\Yii::t('common', 'Поставщик "{value}" не существует', ['{value}' => $this->supplier]));
        }

        $brand = BrandRecord::model()->findByName(Utf8::trim($this->brand));
        if ($brand === null) {
            throw new BrandNotFoundException(\Yii::t('common', 'Бренд "{value}" не существует', ['{value}' => $this->supplier]));
        }

        $this->_colorCode = ProductRecord::bindingColorCode($this->colorCode);

        $product = null;
        /** @var EventProductRecord $product */
        if ($event->is_stock) {
            $product = EventProductRecord::model()
                ->eventId($event->id)
                ->supplierId($supplier->id)
                ->articleIs($this->article)
                ->category($this->category)
                ->size($this->size)
                ->color($this->color)
                ->brandId($brand->id)
                ->name($this->name)
                ->find();
        }

        if (null === $product) {
            $product = new EventProductRecord();
            $product->event_id = $event->id;
            $product->supplier_id = $supplier->id;
            $product->article = $this->article;
            $product->barcode = $this->barcode;
            $product->brand_id = $brand->id;
            $product->name = $this->name;
            $product->category = $this->category;
            $product->design_in = $this->designIn;
            $product->made_in = $this->madeIn;
            $product->target = $productTargets->parseTargetString($this->target);
            $product->sizeformat = $this->sizeformat;
            $product->size = $this->size;
            $product->sizes = $this->sizes;
            $product->size_guide = $this->sizeGuide;
            $product->label = $this->label;
            list($product->age_from, $product->age_to) = $ageGroups->parseAgeStringRange($this->age);
            $product->color = $this->color;
            $product->composition = $this->composition;
            $product->shelflife = $this->shelfLife;
            $product->description = $this->description;
            $product->weight = $productWeight->parseString($this->weight);
            $product->dimensions = $this->dimensions;
            $product->photo = implode("\n", array_unique(Utf8::explode(',', $this->photo)));
            $product->price_purchase = $this->pricePurchase;
            $product->price_market = $this->priceMarket;
            $product->price = $this->price;
            $product->internal_code = $this->internalCode;
            $product->sectionId = $this->_sectionId;
            $product->colorCode = $this->_colorCode;
        }

        $product->number = $this->number;

        return $product;
    }

    /**
     * @param int| string $height
     *
     * @return string
     */
    protected static function getHeightNormalization($height)
    {
        $height = Utf8::trim(Cast::toStr($height));

        if (preg_match('/^[0-9\-]+$/', $height)) {
            $height .= self::HEIGHT_DEFAULT_UNIT;
        }

        return $height;
    }
}
