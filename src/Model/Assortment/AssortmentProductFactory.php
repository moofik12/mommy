<?php

namespace MommyCom\Model\Assortment;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;

class AssortmentProductFactory
{
    /**
     * @var Regions
     */
    private $regions;

    /**
     * AssortmentProductFactory constructor.
     * @param Regions $regions
     */
    public function __construct(Regions $regions)
    {
        $this->regions = $regions;
    }

    /**
     * @param AssortmentTable $assortmentTable
     * @param $lineNum
     * @param array $lineData
     *
     * @return AssortmentProduct
     * @throws Exception\InvalidColumnCountException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function createFromLines(AssortmentTable $assortmentTable, $lineNum, array $lineData)
    {
        $regionName = $this->regions->getServerRegion()->getRegionName();

        switch ($regionName) {
            case Region::INDONESIA:
                return IndonesiaAssortmentProduct::fromLines($assortmentTable, $lineNum, $lineData);
            default:
                return AssortmentProduct::fromLines($assortmentTable, $lineNum, $lineData);
        }
    }

    /**
     * @param AssortmentTable $assortmentTable
     * @param EventProductRecord $product
     *
     * @return AssortmentProduct
     */
    public function createFromModel(AssortmentTable $assortmentTable, EventProductRecord $product)
    {
        $regionName = $this->regions->getServerRegion()->getRegionName();

        switch ($regionName) {
            case Region::INDONESIA:
                return IndonesiaAssortmentProduct::fromModel($assortmentTable, $product);
            default:
                return AssortmentProduct::fromModel($assortmentTable, $product);
        }
    }
}