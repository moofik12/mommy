<?php

namespace MommyCom\Model\Assortment;

use CMap;
use finfo;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Model\SupplierRequest\RequestTable;
use MommyCom\YiiComponent\Utf8;

/**
 * Class ProductImport
 * @method AssortmentProduct[] toArray()
 */
class AssortmentTable extends CMap
{
    const CHECKFILE_OK = '';
    const CHECKFILE_INVALID_MIME = 'mime';
    const CHECKFILE_INVALID_ENCODING = 'encoding';
    const CHECKFILE_INVALID_CSV = 'csv';

    /**
     * @var AssortmentProductFactory
     */
    private $assortmentProductFactory;

    /**
     * @var EventRecord
     */
    protected $_event;

    /**
     * @param string $filename
     *
     * @return string
     */
    public static function checkFile($filename)
    {
        $finfo = new finfo();

        if (strpos($finfo->file($filename, FILEINFO_MIME_TYPE), 'text/') !== 0) {
            return self::CHECKFILE_INVALID_MIME;
        }

        if ($finfo->file($filename, FILEINFO_MIME_ENCODING) != 'utf-8') {
            return self::CHECKFILE_INVALID_ENCODING;
        }

        return self::CHECKFILE_OK;
    }

    /**
     * @return EventRecord
     */
    public function getEvent()
    {
        return $this->_event;
    }

    /**
     * @param AssortmentProductFactory $assortmentProductFactory
     * @param EventRecord $event
     * @param $filename
     *
     * @return AssortmentTable
     * @throws Exception\InvalidColumnCountException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function fromFile(AssortmentProductFactory $assortmentProductFactory, EventRecord $event, $filename)
    {
        $instance = new static();
        /* @var $instance AssortmentTable */

        $instance->_event = $event;

        if ($instance->checkFile($filename) !== self::CHECKFILE_OK) {
            return $instance; // just exit
        }

        // here read csv
        $handle = fopen($filename, 'r');
        $lineNum = -1;

        while (($lineData = fgetcsv($handle, 0, ',', '"')) !== false) {
            $lineNum++;
            if ($lineNum == 0) { // first line is header
                continue;
            }

            $lineData = Utf8::trimArr($lineData);
            $instance[] = $assortmentProductFactory->createFromLines($instance, $lineNum, $lineData);

            if (function_exists('gc_collect_cycles')) {
                gc_collect_cycles();
            }
        }

        fclose($handle);

        $instance->setReadOnly(true);

        return $instance;
    }

    /**
     * @param AssortmentProductFactory $assortmentProductFactory
     * @param EventRecord $event
     *
     * @return AssortmentTable
     */
    public static function fromEvent(AssortmentProductFactory $assortmentProductFactory, EventRecord $event)
    {
        $instance = new static();
        /* @var $instance AssortmentTable */

        $instance->_event = $event;

        foreach ($event->products as $item) {
            $instance[] = $assortmentProductFactory->createfromModel($instance, $item);
        }

        $instance->setReadOnly(true);

        return $instance;
    }

    public function toCsvString()
    {
        $products = $this->toArray();

        $headerData = [
            'Поставщик',
            'Артикул',
            'Штрихкод',
            'Код',
            'Бренд',
            'Категория товара',
            'Раздел на сайте',
            'Подраздел на сайте',
            'Наименование',
            'Страна Дизайн',
            'Страна Производство',
            'Пол',
            'РЕЗЕРВ',
            'EU',
            'US',
            'INT',
            'UA/RU',
            'CHN',
            'UK',
            'FR',
            'IT',
            'JP',
            'РОСТ',
            'Головной Убор',
            'Перчатки',
            'Размерная Сетка',
            'Размер Этикетка',
            'ВОЗРАСТ',
            'Код Цвета',
            'Цвет',
            'Состав',
            'Срок годности',
            'Описание',
            'Вес Брутто',
            'Габариты',
            'Закупочная Цена',
            'Рыночная Цена',
            'Цена Продажи',
            'Фото',
        ];

        $contentData = [];
        foreach ($products as $product) {
            $contentData[] = $product->getLineData();
        }

        return CsvFile::loadFromArray(array_merge([$headerData], $contentData))->toString();
    }

    /**
     * @param $filename
     */
    public function toFile($filename)
    {
        file_put_contents($filename, $this->toCsvString());
    }
}

