<?php

namespace MommyCom\Model\Assortment;

use MommyCom\Model\Db\EventRecord;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;

class AssortmentTableFactory
{
    /**
     * @var Regions
     */
    private $regions;

    /**
     * @var AssortmentProductFactory
     */
    private $assortmentProductFactory;

    /**
     * AssortmentTableFactory constructor.
     * @param AssortmentProductFactory $assortmentProductFactory
     * @param Regions $regions
     */
    public function __construct(AssortmentProductFactory $assortmentProductFactory, Regions $regions)
    {
        $this->regions = $regions;
        $this->assortmentProductFactory = $assortmentProductFactory;
    }

    /**
     * @param EventRecord $event
     * @param $filename
     *
     * @return AssortmentTable
     * @throws Exception\InvalidColumnCountException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function createFromFile(EventRecord $event, $filename)
    {
        $regionName = $this->regions->getServerRegion()->getRegionName();

        switch ($regionName) {
            case Region::INDONESIA:
                return IndonesiaAssortmentTable::fromFile($this->assortmentProductFactory, $event, $filename);
            default:
                return AssortmentTable::fromFile($this->assortmentProductFactory, $event, $filename);
        }
    }

    /**
     * @param EventRecord $event
     *
     * @return AssortmentTable
     */
    public function createFromEvent(EventRecord $event)
    {
        $regionName = $this->regions->getServerRegion()->getRegionName();

        switch ($regionName) {
            case Region::INDONESIA:
                return IndonesiaAssortmentTable::fromEvent($this->assortmentProductFactory, $event);
            default:
                return AssortmentTable::fromEvent($this->assortmentProductFactory, $event);
        }
    }
}