<?php

namespace MommyCom\Model\Assortment\Exception;

class SupplierNotFoundException extends AssortmentException
{
}
