<?php

namespace MommyCom\Model\Assortment\Exception;

class InvalidSectionException extends AssortmentException
{
}
