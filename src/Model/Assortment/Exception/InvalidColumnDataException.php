<?php

namespace MommyCom\Model\Assortment\Exception;

class InvalidColumnDataException extends AssortmentException
{
}
