<?php

namespace MommyCom\Model\Assortment\Exception;

class InvalidColumnCountException extends AssortmentException
{
}
