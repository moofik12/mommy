<?php

namespace MommyCom\Model\Assortment\Exception;

class InvalidColorException extends AssortmentException
{
}
