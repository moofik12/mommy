<?php

namespace MommyCom\Model\Assortment\Exception;

class CertificateNotFoundException extends AssortmentException
{
}