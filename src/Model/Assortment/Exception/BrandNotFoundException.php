<?php

namespace MommyCom\Model\Assortment\Exception;

class BrandNotFoundException extends AssortmentException
{
}
