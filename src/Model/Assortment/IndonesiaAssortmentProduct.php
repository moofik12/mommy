<?php

namespace MommyCom\Model\Assortment;

use MommyCom\Model\Assortment\Exception\AssortmentException;
use MommyCom\Model\Assortment\Exception\BrandNotFoundException;
use MommyCom\Model\Assortment\Exception\CertificateNotFoundException;
use MommyCom\Model\Assortment\Exception\InvalidColumnCountException;
use MommyCom\Model\Assortment\Exception\SupplierNotFoundException;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\YiiComponent\Facade\Translator;

class IndonesiaAssortmentProduct extends AssortmentProduct
{
    public const ASSORTMENT_LINE_COLUMN_NUMBER = 40;

    protected const CERTIFICATION_SECTIONS = [
        'Kosmetik dan parfum',
        'Косметика и парфюмерия',
    ];

    /**
     * @var string $certificate
     */
    protected $certificate;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['certificate', 'validateCertificate'],
        ]);
    }

    /**
     * @param $attribute
     *
     * @throws CertificateNotFoundException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function validateCertificate($attribute)
    {
        if (
            (in_array($this->parentSection, self::CERTIFICATION_SECTIONS) && empty($this->{$attribute}))
            || (!in_array($this->parentSection, self::CERTIFICATION_SECTIONS) && !empty($this->{$attribute}))
        ) {
            throw new CertificateNotFoundException(Translator::t('Empty certificate. Line {line}', ['line' => $this->lineNum]));
        }
    }

    /**
     * @param AssortmentTable $assortmentTable
     * @param int $lineNum
     * @param array $lineData
     *
     * @return AssortmentProduct
     * @throws InvalidColumnCountException
     */
    public static function fromLines(AssortmentTable $assortmentTable, $lineNum, array $lineData)
    {
        /** @var self $product */
        $product = parent::fromLines($assortmentTable, $lineNum, $lineData);
        $product->certificate = $lineData[39];

        return $product;
    }

    /**
     * @param AssortmentTable $assortmentTable
     * @param EventProductRecord $product
     *
     * @return AssortmentProduct
     */
    public static function fromModel(AssortmentTable $assortmentTable, EventProductRecord $product)
    {
        /** @var self $model */
        $model = parent::fromModel($assortmentTable, $product);
        $model->certificate = $product->certificate;

        return $model;
    }

    /**
     * @return EventProductRecord
     * @throws AssortmentException
     * @throws BrandNotFoundException
     * @throws SupplierNotFoundException
     */
    public function convertToEventProduct()
    {
        $product = parent::convertToEventProduct();

        if (!empty($this->certificate)) {
            $product->certificate = $this->certificate;
        }

        return $product;
    }

    /**
     * @return array
     */
    public function getLineData()
    {
        return array_merge(parent::getLineData(), [$this->certificate]);
    }
}