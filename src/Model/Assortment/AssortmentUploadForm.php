<?php

namespace MommyCom\Model\Assortment;

use CUploadedFile;
use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class AssortmentUploadForm
 */
class AssortmentUploadForm extends \CFormModel
{
    /**
     * @var AssortmentTableFactory
     */
    private $assortmentTableFactory;

    /**
     * @var int
     */
    public $eventId;

    /**
     * @var CUploadedFile
     */
    public $file;

    public function __construct(AssortmentTableFactory $assortmentTableFactory, string $scenario = '')
    {
        parent::__construct($scenario);
        $this->assortmentTableFactory = $assortmentTableFactory;
    }

    public function rules()
    {
        return [
            ['file, eventId', 'required'],
            ['eventId', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['file', 'file', 'types' => 'csv', 'minSize' => 10, 'maxSize' => 20 * 1024 * 1024, 'allowEmpty' => false],
            ['file', 'validatorFile'],
        ];
    }

    public function validatorFile($attribute)
    {
        $value = $this->$attribute;
        /* @var $value CUploadedFile */
        if (($result = AssortmentTable::checkFile($value->tempName)) !== AssortmentTable::CHECKFILE_OK) {
            $this->addError($attribute, Translator::t('in {attribute} an unsupported file type is specified, the reason', ['{attribute}' => $attribute]) . ' ' . $result);
        }
    }

    public function attributeLabels()
    {
        return [
            'file' => 'File',
            'eventId' => 'Event',
        ];
    }

    public function getConfig()
    {
        return [
            'showErrorSummary' => true,
            'enctype' => 'multipart/form-data',
            'elements' => [
                'eventId' => [
                    'type' => 'hidden',
                ],
                'file' => [
                    'type' => 'file',
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
            ],
        ];
    }

    /**
     * @return AssortmentTable
     * @throws Exception\InvalidColumnCountException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getAssortmentTable()
    {
        $event = EventRecord::model()->findByPk($this->eventId);
        $table = $this->assortmentTableFactory->createFromFile($event, $this->file->tempName);

        return $table;
    }
}
