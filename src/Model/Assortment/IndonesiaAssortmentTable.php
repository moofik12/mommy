<?php
namespace MommyCom\Model\Assortment;

use MommyCom\Model\Misc\CsvFile;

class IndonesiaAssortmentTable extends AssortmentTable
{
    public function toCsvString()
    {
        $products = $this->toArray();

        $headerData = [
            'Поставщик',
            'Артикул',
            'Штрихкод',
            'Код',
            'Бренд',
            'Категория товара',
            'Раздел на сайте',
            'Подраздел на сайте',
            'Наименование',
            'Страна Дизайн',
            'Страна Производство',
            'Пол',
            'РЕЗЕРВ',
            'EU',
            'US',
            'INT',
            'UA/RU',
            'CHN',
            'UK',
            'FR',
            'IT',
            'JP',
            'РОСТ',
            'Головной Убор',
            'Перчатки',
            'Размерная Сетка',
            'Размер Этикетка',
            'ВОЗРАСТ',
            'Код Цвета',
            'Цвет',
            'Состав',
            'Срок годности',
            'Описание',
            'Вес Брутто',
            'Габариты',
            'Закупочная Цена',
            'Рыночная Цена',
            'Цена Продажи',
            'Фото',
            'Cертификат',
        ];

        $contentData = [];
        foreach ($products as $product) {
            $contentData[] = $product->getLineData();
        }

        return CsvFile::loadFromArray(array_merge([$headerData], $contentData))->toString();
    }
}