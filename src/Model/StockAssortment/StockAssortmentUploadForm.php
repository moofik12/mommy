<?php

namespace MommyCom\Model\StockAssortment;

use CUploadedFile;
use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class StockAssortmentUploadForm
 *
 * @author scriptbunny
 */
class StockAssortmentUploadForm extends \CFormModel
{
    public $eventId;

    /**
     * @var CUploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            ['file, eventId', 'required'],

            ['eventId', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['eventId', 'validatorEvent'],

            ['file', 'file', 'types' => 'csv', 'minSize' => 10, 'maxSize' => 20 * 1024 * 1024, 'allowEmpty' => false],
            ['file', 'validatorFile'],
        ];
    }

    public function validatorFile($attribute)
    {
        $value = $this->$attribute;
        /* @var $value CUploadedFile */
        if (StockAssortmentTable::checkFile($value->tempName) !== StockAssortmentTable::CHECKFILE_OK) {
            $this->addError($attribute, Translator::t('File type not supported at') . $attribute);
        }
    }

    public function validatorEvent($attribute)
    {
        $value = $this->$attribute;
        $event = EventRecord::model()->findByPk($value);
        if ($event !== null) {
            return;
        }

        if (!$event->is_stock) {
            $this->addError($attribute, Translator::t("Non stock flash-sale"));
        } elseif ($event->is_virtual) {
            $this->addError($attribute, Translator::t("Non virtual flash-sale"));
        }
    }

    public function attributeLabels()
    {
        return [
            'file' => Translator::t('File'),
            'eventId' => Translator::t('Flash-sale'),
        ];
    }

    public function getConfig()
    {
        return [
            'showErrorSummary' => true,
            'enctype' => 'multipart/form-data',
            'elements' => [
                'eventId' => [
                    'type' => 'hidden',
                ],
                'file' => [
                    'type' => 'file',
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
            ],
        ];
    }

    /**
     * @return StockAssortmentTable
     */
    public function getAssortmentTable()
    {
        $event = EventRecord::model()->findByPk($this->eventId);
        $table = StockAssortmentTable::fromFile($event, $this->file->tempName);
        return $table;
    }
}
