<?php

namespace MommyCom\Model\StockAssortment;

use CModel;
use CVarDumper;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\StockAssortment\Exception\StockInvalidColumnCountException;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use ReflectionClass;

class StockAssortmentProduct extends CModel
{
    const ASSORTMENT_LINE_COLUMN_NUMBER = 9;

    /*
    "№ Товара", "Поставщик", "Бренд", "Наименование", "Срок годности",
    "СтоимостьЗакупки", "РыночнаяЦена", "ЦенаПродажи",
    "Фото"
     */

    public $lineNum;
    public $lineColumnCount;

    public $productId;
    public $name;
    public $supplier;
    public $brand;

    /**
     * Срок годности
     *
     * @var string
     */
    public $shelfLife;

    public $pricePurchase;
    public $priceMarket;
    public $price;

    public $photo;

    private $_assortmentTable;
    private $stockWarehouseProducts = [];

    public function attributeNames()
    {
        $class = new ReflectionClass(get_class($this));
        $names = [];
        foreach ($class->getProperties() as $property) {
            $name = $property->getName();
            if ($property->isPublic() && !$property->isStatic()) {
                $names[] = $name;
            }
        }
        return $names;
    }

    protected function _import(array $lineData)
    {
        $this->lineColumnCount = count($lineData);
        if ($this->lineColumnCount !== self::ASSORTMENT_LINE_COLUMN_NUMBER) {
            throw new StockInvalidColumnCountException(
                "Ошибка, строка {$this->lineNum} содержит {$this->lineColumnCount} колонок а должна " . self::ASSORTMENT_LINE_COLUMN_NUMBER . ', содержимое: ' .
                CVarDumper::dumpAsString($lineData)
            );
        }

        // import columns by fucking magic-indexed offsets
        $this->productId = $lineData[0];
        $this->name = $lineData[1];
        $this->supplier = $lineData[2];
        $this->brand = $lineData[3];

        $this->shelfLife = $lineData[4];

        $this->pricePurchase = $lineData[5];
        $this->priceMarket = $lineData[6];
        $this->price = $lineData[7];

        $this->photo = $lineData[8];
    }

    /**
     * @param StockAssortmentTable $assortmentTable
     * @param integer $lineNum
     * @param array $lineData
     */
    public function __construct(StockAssortmentTable $assortmentTable, $lineNum, array $lineData)
    {
        $this->_assortmentTable = $assortmentTable;
        $this->lineNum = $lineNum;

        $this->_import($lineData);
    }

    public function rules()
    {
        return [
            ['lineColumnCount', 'compare', 'compareValue' => self::ASSORTMENT_LINE_COLUMN_NUMBER],
            ['productId, name, supplier, brand', 'required'],

            ['pricePurchase, priceMarket, price', 'required'],
            ['pricePurchase, priceMarket, price', 'filter', 'filter' => [Cast::class, 'smartStringToUFloat']],
            ['pricePurchase, priceMarket, price', 'numerical', 'min' => 1, 'max' => 1000000000], // million is enough

            ['pricePurchase', 'compare', 'compareAttribute' => 'price', 'operator' => '<=', 'message' => 'Стоимость закупки должна быть меньше чем стоимость продажи'],
            ['price', 'compare', 'compareAttribute' => 'priceMarket', 'operator' => '<=', 'message' => 'Стоимость продажи быть меньше чем рыночная стоимость'],

            ['supplier', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'name'],
            ['brand', 'extendedBrandValidation'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'lineNum' => 'Номер строки',
            'lineColumnCount' => 'Количество колонок',

            'productId' => '№ Товара',
            'name' => 'Название',
            'brand' => 'Бренд',
            'supplier' => 'Поставщик',

            'shelfLife' => 'Срок годности',

            'price' => 'Стоимость',
            'pricePurchase' => 'Стоимость закупки',
            'priceMarket' => 'Рыночная стоимость',

        ];
    }

    public function extendedBrandValidation($attribute)
    {
        $value = $this->$attribute;
        $brand = BrandRecord::model()->findByName($value);
        if ($brand == null) {
            $this->addError($attribute, "Бренд '{$value}' не найдет в БД");
            return;
        }

        $product = ProductRecord::model()->findByPk($this->productId);
        if ($product !== null && $product->brand_id != $brand->id) {
            $this->addError($attribute, "Товар принадлежит другому бренду");
        }
    }

    public function validate($attributes = null, $clearErrors = true)
    {
        if (!parent::validate($attributes, $clearErrors)) {
            // hack
            $errors = ['lineNum' => "На строке #{$this->lineNum} произошли ошибки"] + $this->getErrors();
            $this->clearErrors();
            $this->addErrors($errors);
            return false;
        }
        return true;
    }

    /**
     * @return StockAssortmentTable
     */
    public function getAssortmentTable()
    {
        return $this->_assortmentTable;
    }

    /**
     * @return EventProductRecord[]
     */
    public function convertToEventProducts()
    {
        $event = $this->getAssortmentTable()->getEvent();

        $selector = WarehouseProductRecord::model()
            ->onlyNotHasOrderConnected()
            ->status(WarehouseProductRecord::STATUS_WAREHOUSE_STOCK)
            ->productId($this->productId)
            ->orderBy('event_product_sizeformat')
            ->orderBy('event_product_size')
            ->groupBy('event_product_sizeformat')
            ->groupBy('event_product_size');

        $warehouses = $selector->findAll();
        /* @var $warehouses WarehouseProductRecord[] */

        $products = [];

        $copyAttributes = [
            'supplier_id', 'brand_id', 'article', 'barcode',
            'category', 'design_in', 'made_in', 'label',
            'target', 'sizeformat', 'size', 'sizes', 'size_guide',
            'age_from', 'age_to',
            'color', 'color_code',
            'composition',
            'description',
            'weight', 'dimensions',
        ];

        foreach ($warehouses as $index => $warehouse) {
            $stockProduct = $warehouse->eventProduct;
            $this->stockWarehouseProducts[] = $warehouse;
            $product = new EventProductRecord();

            foreach ($copyAttributes as $copyAttribute) {
                $product->$copyAttribute = $stockProduct->$copyAttribute;
            }

            $product->event_id = $event->id;
            $product->name = $this->name;
            $product->shelflife = $this->shelfLife;
            $product->photo = implode("\n", array_unique(Utf8::explode(',', $this->photo)));
            $product->price_purchase = $this->pricePurchase;
            $product->price_market = $this->priceMarket;
            $product->price = $this->price;
            $product->status = EventProductRecord::STATUS_STOCK;

            $products[] = $product;

            unset($stockProduct, $warehouses[$index]);
            gc_collect_cycles();
        }
        unset($warehouses, $event);

        return $products;
    }

    /**
     * @return bool
     */
    public function unstockItems() {
        foreach ($this->stockWarehouseProducts as $index => $product) {
            $product->status = WarehouseProductRecord::STATUS_WAREHOUSE_STOCK_RESERVED;
            if (!$product->save()) {
                return false;
            }
        }

        return true;
    }
}
