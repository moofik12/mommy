<?php

namespace MommyCom\Model\StockAssortment\Exception;

class StockInvalidColumnCountException extends StockAssortmentException
{
}
