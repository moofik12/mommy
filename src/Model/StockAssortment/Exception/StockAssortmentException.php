<?php

namespace MommyCom\Model\StockAssortment\Exception;

use CException;

class StockAssortmentException extends CException
{
}
