<?php

namespace MommyCom\Model\StockAssortment\Exception;

class StockBrandNotFoundException extends StockAssortmentException
{
}
