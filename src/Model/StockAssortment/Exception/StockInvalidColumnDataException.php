<?php

namespace MommyCom\Model\StockAssortment\Exception;

class StockInvalidColumnDataException extends StockAssortmentException
{
}
