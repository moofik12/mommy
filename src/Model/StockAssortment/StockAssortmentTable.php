<?php

namespace MommyCom\Model\StockAssortment;

use CMap;
use finfo;
use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\Utf8;

/**
 * Class StockAssortmentTable
 * @method StockAssortmentProduct[] toArray()
 */
class StockAssortmentTable extends CMap
{
    const CHECKFILE_OK = '';
    const CHECKFILE_INVALID_MIME = 'mime';
    const CHECKFILE_INVALID_ENCODING = 'encoding';
    const CHECKFILE_INVALID_CSV = 'csv';

    /**
     * @var EventRecord
     */
    protected $_event;

    /**
     * @param string $filename
     *
     * @return string
     */
    public static function checkFile($filename)
    {
        $finfo = new finfo();

        if (strpos($finfo->file($filename, FILEINFO_MIME_TYPE), 'text/') !== 0) {
            return self::CHECKFILE_INVALID_MIME;
        }

        if ($finfo->file($filename, FILEINFO_MIME_ENCODING) != 'utf-8') {
            return self::CHECKFILE_INVALID_ENCODING;
        }

        return self::CHECKFILE_OK;
    }

    /**
     * @return EventRecord
     */
    public function getEvent()
    {
        return $this->_event;
    }

    protected function _getIsAlreadyAdded(StockAssortmentProduct $product)
    {
        foreach ($this->toArray() as $item) {
            if ($item->productId == $product->productId) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param EventRecord $event
     * @param string $filename
     *
     * @return StockAssortmentTable
     */
    public static function fromFile(EventRecord $event, $filename)
    {
        $instance = new static();
        /* @var $instance StockAssortmentTable */

        $instance->_event = $event;

        if ($instance->checkFile($filename) !== self::CHECKFILE_OK) {
            return $instance; // just exit
        }

        // here read csv
        $handle = fopen($filename, 'r');
        $lineNum = -1;

        while (($lineData = fgetcsv($handle, 0, ',', '"')) !== false) {
            $lineNum++;
            if ($lineNum == 0) { // first line is header
                continue;
            }
            $lineData = Utf8::trimArr($lineData);
            //try {
            $product = new StockAssortmentProduct($instance, $lineNum, $lineData);
            if (!$instance->_getIsAlreadyAdded($product)) {
                $instance[] = $product;
            }
            //} catch(Exception $e) {
            //    continue; // do nothing with it
            //}

            if (function_exists('gc_collect_cycles')) {
                gc_collect_cycles();
            }
        }

        fclose($handle);

        $instance->setReadOnly(true);

        return $instance;
    }
}

