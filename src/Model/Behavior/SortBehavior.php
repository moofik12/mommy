<?php

namespace MommyCom\Model\Behavior;

use CActiveRecord;
use CActiveRecordBehavior;
use LogicException;

class SortBehavior extends CActiveRecordBehavior
{
    /**
     * @var string
     */
    public $sortableAttribute = 'sort';

    /**
     * @var array
     */
    public $criteria = [];

    protected function beforeSave($event)
    {
        /** @var CActiveRecord $model */
        $model = $this->owner;
        if (!$model->hasAttribute($this->sortableAttribute)) {
            throw new LogicException("Invalid sortable attribute `{$this->sortableAttribute}`.");
        }

        if (!$model->isNewRecord || !empty($$this->sortableAttribute)) {
            return;
        }

        $builder = $model->getCommandBuilder();
        $criteria = $builder->createCriteria();
        $table = $model->getTableSchema();
        if ($this->criteria !== []) {
            $criteria->mergeWith($this->criteria);
        }

        $criteria->select = "MAX({$table}. {$this->sortableAttribute}) as lastSort";
        $lastSort = $builder->createFindCommand($model->getTableSchema(), $criteria)->queryScalar();

        $model->{$this->sortableAttribute} = $lastSort + 1;
    }

    /**
     * @param bool $reverse
     *
     * @return static
     */
    public function sorted($reverse = false)
    {
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $alias = $owner->getTableAlias();
        $criteria = $owner->getDbCriteria();
        $sort = $reverse ? 'DESK' : 'ASC';
        $ordered = "$alias.{$this->sortableAttribute} $sort";

        if (empty($criteria->order)) {
            $criteria->order = $ordered;
        } else {
            $criteria->order = ', ' . $ordered;
        }

        return $owner;
    }
}
