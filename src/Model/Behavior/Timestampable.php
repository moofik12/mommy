<?php

namespace MommyCom\Model\Behavior;

use CActiveRecord;
use CActiveRecordBehavior;
use CDbExpression;
use MommyCom\YiiComponent\Type\Cast;

class Timestampable extends CActiveRecordBehavior
{
    public $creationTimeField = 'created_at';
    public $updateTimeField = 'updated_at';
    public $exceptScenarios = [];

    public function beforeSave($event)
    {
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $scenario = $owner->getScenario();

        if (in_array($scenario, $this->exceptScenarios)) {
            return parent::beforeSave($event);
        }

        if ($owner->isNewRecord) {
            $owner->{$this->creationTimeField} = new CDbExpression('UNIX_TIMESTAMP()');
        }
        $owner->{$this->updateTimeField} = new CDbExpression('UNIX_TIMESTAMP()');

        parent::beforeSave($event);
    }

    public function afterSave($event)
    {
        parent::afterSave($event);
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $creationTimeField = $this->creationTimeField;
        $updateTimeField = $this->updateTimeField;

        if ($owner->$creationTimeField instanceof CDbExpression) {
            $owner->$creationTimeField = time();
        }

        if ($owner->$updateTimeField instanceof CDbExpression) {
            $owner->$updateTimeField = time();
        }
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdAtFrom($value)
    {
        $value = Cast::toUInt($value);
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $alias = $owner->getTableAlias();
        $owner->getDbCriteria()->addCondition($alias . '.' . $this->creationTimeField . '>=' . Cast::toStr($value));
        return $owner;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdAtTo($value)
    {
        $value = Cast::toUInt($value);
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $alias = $owner->getTableAlias();
        $owner->getDbCriteria()->addCondition($alias . '.' . $this->creationTimeField . '<=' . Cast::toStr($value));
        return $owner;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function createdAtBetween($from, $to)
    {
        $this->createdAtFrom($from);
        $this->createdAtTo($to);
        return $this->getOwner();
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function updatedAtFrom($value)
    {
        $value = Cast::toUInt($value);
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $alias = $owner->getTableAlias();
        $owner->getDbCriteria()->addCondition($alias . '.' . $this->updateTimeField . '>=' . Cast::toStr($value));
        return $owner;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function updatedAtTo($value)
    {
        $value = Cast::toUInt($value);
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */

        $alias = $owner->getTableAlias();
        $owner->getDbCriteria()->addCondition($alias . '.' . $this->updateTimeField . '<=' . Cast::toStr($value));
        return $owner;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function updatedAtBetween($from, $to)
    {
        $this->createdAtFrom($from);
        $this->createdAtTo($to);
        return $this->getOwner();
    }
}
