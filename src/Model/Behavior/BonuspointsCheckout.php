<?php

namespace MommyCom\Model\Behavior;

use CActiveRecordBehavior;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;

/**
 * Class BonuspointsCheckout
 * Логика списывания бонусов для класса OrderRecord
 */
class BonuspointsCheckout extends CActiveRecordBehavior
{
    /**
     * @var string
     */
    public $successMessage = 'Оплата заказа №%s';

    public function checkout($force = false)
    {
        $owner = $this->owner;
        /* @var $owner OrderRecord */

        if ($owner->is_drop_shipping) {
            return;
        }

        $lockingStatuses = [
            OrderRecord::PROCESSING_UNMODERATED,
            OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
            OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
            OrderRecord::PROCESSING_CALLCENTER_RECALL,
            OrderRecord::PROCESSING_CALLCENTER_CALL_LATER,
//            OrderRecord::PROCESSING_STOREKEEPER_PACKAGED,
            OrderRecord::PROCESSING_CALLCENTER_PREPAY,
        ];

        $unlockingStatuses = [
            OrderRecord::PROCESSING_CANCELLED,
            OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER,
        ];

        $checkoutStatuses = [
            OrderRecord::PROCESSING_STOREKEEPER_MAILED,
        ];

        $points = new ShopBonusPoints();
        $points->init($owner->user);

        //не подтянет relation для новой записи OrderRecord, OrderRecord::getPrice будет возвращать 0
        if (!$owner->getIsNewRecord()) {
            $owner->refresh();
        }
        $status = $owner->processing_status;

        if ($status == $owner->processing_status_prev && !$force) {
            return; // do nothing
        }

        if (in_array($status, $lockingStatuses)) { // инициализация
            if ($points->getAvailableToSpend(false) > 0) {
                $price = $owner->getPrice(false, true, true);

                $points->unlock($owner->id);
                $num = $points->getBonusesCost($price);
                $types = $points->lock($owner->id, $num);
                $presentBonusValue = 0;
                $presentBonus = $types->itemAt(UserBonuspointsRecord::TYPE_PRESENT);
                if ($presentBonus) {
                    $presentBonusValue = $presentBonus->value;
                }

                if ($owner->bonuses != $num || $owner->bonuses_part_used_present != $presentBonusValue) {
                    $owner->bonuses_part_used_present = $presentBonusValue;
                    $owner->bonuses = $num;
                    $owner->updateByPk($owner->id, [
                        'bonuses' => $num,
                        'bonuses_part_used_present' => $presentBonusValue,
                    ]);
                }
            }
        } elseif (in_array($status, $unlockingStatuses)) {
            if ($owner->bonuses > 0) {
                $owner->bonuses = 0;
                $owner->updateByPk($owner->id, [
                    'bonuses' => $owner->bonuses,
                ]);
                $points->unlock($owner->id);
            }
        } elseif (in_array($status, $checkoutStatuses)) {
            $lock = $points->getLocks()->getLockAll($owner->id);

            if ($lock > 0) {
                $message = \Yii::t('common', 'Оплата заказа №') . $owner->getIdAligned();
                $types = $points->subtract($owner->bonuses, $message);

                $presentBonus = $types->itemAt(UserBonuspointsRecord::TYPE_PRESENT);
                if ($presentBonus && $presentBonus->value != $owner->bonuses_part_used_present) {
                    $owner->updateByPk($owner->id, [
                        'bonuses_part_used_present' => $presentBonus->value,
                    ]);
                }
            }

            $points->unlock($owner->id);
        }
    }

    public function afterSave($event)
    {
        parent::afterSave($event);
        $this->checkout();
    }
}
