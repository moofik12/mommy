<?php

namespace MommyCom\Model\UserInterests\Generator;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\ViewsTracking\ViewsTracking;
use MommyCom\Model\ViewsTracking\ViewTrackingByUserRecord;

class ViewsGenerator extends GeneratorAbstract
{
    const VIEW_LIMIT = 500;
    const VIEW_VALUE = 0.02; // конверт из просмотра около 2%

    // используется для оптимизации, вместо поиска по timeActive у EventRecord
    // можно приблизительно подобрать по created_at у EventProductRecord
    // правда не точно, но этого думаю будет достаточно
    const EVENT_SEARCH_PERIOD = 691200; // 8 days

    public function generate(UserRecord $user)
    {
        $selector = ViewTrackingByUserRecord::model()
            ->userId($user->id)
            ->limit(self::VIEW_LIMIT)
            ->objectType(ViewsTracking::getObjectType('ProductRecord'))
            ->orderBy('id', 'desc');

        $views = $selector->findAll();

        /* @var $views ViewTrackingByUserRecord[] */

        $ratings = [];

        foreach ($views as $view) {
            $product = EventProductRecord::model()
                ->productId($view->object_id)
                ->createdAtGreater($view->updated_at - self::EVENT_SEARCH_PERIOD)
                ->createdAtLower($view->updated_at)
                /*->with(array(
                    'event' => array(
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => array(
                            'timeActive' => $view->updated_at // акция была активна в момент последнего просмотра
                        )
                    )
                ))*/
                ->find();

            /* @var $product |null ProductRecord */

            // товар не был найден, например, просмотр был уже после завершения акции
            if ($product === null) {
                continue;
            }

            $price = $product->price * self::VIEW_VALUE;

            $ageGroups = $product->ageGroups;
            $category = $product->category;
            $targets = $product->target;

            // если не привязано ни к какой группе тогда false
            if ($ageGroups === []) {
                $ageGroups[] = false;
            }

            // если не привязано ни к какой группе тогда false
            if ($targets === []) {
                $targets[] = false;
            }

            foreach ($ageGroups as $ageGroup) {
                foreach ($targets as $target) {
                    $key = implode('_', [
                        $category,
                        $ageGroup,
                        $target,
                    ]);

                    $item = isset($ratings[$key]) ? $ratings[$key] : [
                        'category' => $category,
                        'age_group' => $ageGroup,
                        'target' => $target,
                        'rating' => 0,
                    ];

                    $item['rating'] += $price;

                    $ratings[$key] = $item;
                }
            }
        }

        $this->saveRatings($user, $ratings);
    }
}
