<?php

namespace MommyCom\Model\UserInterests\Generator;

use MommyCom\Model\Db\UserInterestRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\AgeGroups;
use ReflectionClass;
use Yii;

abstract class GeneratorAbstract implements GeneratorInterface
{
    const SAVE_BATCH_SIZE = 100;

    protected function saveRatings(UserRecord $user, array $ratings)
    {
        $chunks = array_chunk($ratings, self::SAVE_BATCH_SIZE);

        $builder = Yii::app()->db->commandBuilder;

        $ref = new ReflectionClass($builder);
        $composeMultipleInsertCommandRef = $ref->getMethod('composeMultipleInsertCommand');
        $composeMultipleInsertCommandRef->setAccessible(true); // hard hack, я не горжусь этим

        $tableName = UserInterestRecord::model()->tableName();

        $ageGroups = AgeGroups::instance();
        $time = time();

        foreach ($chunks as $data) {
            $rows = [];

            foreach ($data as $rawRow) {
                $ageRange = $rawRow['age_group'] !== false ?
                    $ageGroups->getAgeRange($rawRow['age_group']) :
                    [0, 0];
                $target = $rawRow['target'] !== false ? $rawRow['target'] : '';

                $row = [
                    'user_id' => $user->id,
                    'category' => $rawRow['category'],
                    'target' => $target,
                    'age_from' => $ageRange[0],
                    'age_to' => $ageRange[1],
                    'rating' => $rawRow['rating'],
                    'created_at' => $time,
                    'updated_at' => $time,
                ];
                $rows[] = $row;
            }

            $insertCmd = $composeMultipleInsertCommandRef->invoke($builder, $tableName, $rows, [
                'main' => '
                    INSERT INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}
                    ON DUPLICATE KEY UPDATE `rating` = `rating` + VALUES(`rating`), `updated_at` = VALUES(`updated_at`)
                ',
            ]);

            $insertCmd->execute();
        }
    }
}
