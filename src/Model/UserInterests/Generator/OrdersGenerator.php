<?php

namespace MommyCom\Model\UserInterests\Generator;

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserRecord;

class OrdersGenerator extends GeneratorAbstract
{
    const ORDER_LIMIT = 100;
    const ORDER_PERIOD = 46656000; // 18 months

    const CANCELLED_PRODUCT_VALUE = 0.1; // только 10% товаров являются отмененными

    public function generate(UserRecord $user)
    {
        $time = time();

        $selector = OrderRecord::model()
            ->userId($user->id)->limit(self::ORDER_LIMIT)
            ->createdAtBetween($time - self::ORDER_PERIOD, $time)
            ->processingStatusesNot([
                OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
                OrderRecord::PROCESSING_CANCELLED,
            ]);

        $selector->with([
            'positions',
        ]);

        $orders = $selector->findAll();

        /* @var $orders OrderRecord[] */

        $ratings = [];

        foreach ($orders as $order) {
            foreach ($order->positions as $position) {
                $price = $position->priceTotal;
                $product = $position->product;

                // товар не был найден
                if ($product === null) {
                    continue;
                }

                if ($position->callcenter_status === OrderProductRecord::CALLCENTER_STATUS_CANCELLED) {
                    $price *= self::CANCELLED_PRODUCT_VALUE;
                }

                $ageGroups = $product->ageGroups;
                $category = $product->category;
                $targets = $product->target;

                // если не привязано ни к какой группе тогда false
                if ($ageGroups === []) {
                    $ageGroups[] = false;
                }

                // если не привязано ни к какой группе тогда false
                if ($targets === []) {
                    $targets[] = false;
                }

                foreach ($ageGroups as $ageGroup) {
                    foreach ($targets as $target) {
                        $key = implode('_', [
                            $category,
                            $ageGroup,
                            $target,
                        ]);

                        $item = isset($ratings[$key]) ? $ratings[$key] : [
                            'category' => $category,
                            'age_group' => $ageGroup,
                            'target' => $target,
                            'rating' => 0,
                        ];

                        $item['rating'] += $price;

                        $ratings[$key] = $item;
                    }
                }
            }
        }

        $this->saveRatings($user, $ratings);
    }
}
