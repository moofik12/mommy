<?php

namespace MommyCom\Model\UserInterests\Generator;

use MommyCom\Model\Db\UserRecord;

interface GeneratorInterface
{
    public function generate(UserRecord $user);
}
