<?php

namespace MommyCom\Model\UserInterests;

use CComponent;
use CDbCriteria;
use MommyCom\Model\Db\UserInterestRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\UserInterests\Generator\GeneratorInterface;
use MommyCom\Model\UserInterests\Generator\OrdersGenerator;
use MommyCom\Model\UserInterests\Generator\ViewsGenerator;

class InterestsUpdater extends CComponent
{
    const DEFAULT_BATCH_SIZE = 5000;
    const DEFAULT_TABLE_FRESH_PERIOD = 259200; // перегенерация таблицы происходит не чаще этого времени
    const DEFAULT_CLEANUP_TIME = 7776000; // 90 days
    const DEFAULT_CLEANUP_RATING = 3; // min rating = 3

    public function update($count = self::DEFAULT_BATCH_SIZE, $period = self::DEFAULT_TABLE_FRESH_PERIOD)
    {
        $selector = UserRecord::model()
            ->limit($count)
            ->orderBy(['interests_generated_at', 'last_order_at', 'id'], 'desc');

        $criteria = $selector->getDbCriteria();
        $criteria->addCondition('interests_generated_at' . '<' . $period);

        $users = $selector->findAll();
        /* @var $users UserRecord[] */

        $generators = [];
        $generators[] = new OrdersGenerator();
        $generators[] = new ViewsGenerator();
        /* @var $generators GeneratorInterface[] */

        $lowRatingCriteria = new CDbCriteria();
        $lowRatingCriteria->addCondition('rating' . '<' . self::DEFAULT_CLEANUP_RATING);

        foreach ($users as $index => $user) {
            // очистка данных
            $user->interests_amount = 0;
            $user->saveAttributes(['interests_amount']);

            // удаляем старую рейтинговую таблицу
            UserInterestRecord::model()->deleteAllByAttributes([
                'user_id' => $user->id,
            ]);

            // подсчитываем новую таблицу
            foreach ($generators as $generator) {
                $generator->generate($user);
            }

            // удаляем рейтинги ниже минимального порога
            UserInterestRecord::model()->deleteAllByAttributes([
                'user_id' => $user->id,
            ], $lowRatingCriteria);

            // сохраняем информацию о том когда было последнее обновление
            $user->interests_generated_at = time();
            $user->interests_amount = UserInterestRecord::model()->userId($user->id)->count();
            $user->saveAttributes(['interests_amount', 'interests_generated_at']);
        }
    }

    public function cleanup($period = self::DEFAULT_CLEANUP_TIME)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('updated_at' . '<' . $period);
        UserInterestRecord::model()->deleteAll($criteria);
    }
}
