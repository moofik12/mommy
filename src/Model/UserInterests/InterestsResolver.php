<?php

namespace MommyCom\Model\UserInterests;

use CComponent;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use Yii;

class InterestsResolver extends CComponent
{
    const DEFAULT_LIMIT = 60;
    const DEFAULT_PRICE_FROM = 60;
    const DEFAULT_PRICE_TO = 500;
    const DEFAULT_EVENT_ACTIVE_UNTIL = 86400; // акция будет еще активна этот период

    const DEFAULT_GROUPED_PRODUCT_SORT_AMOUNT = 10; // сколько товаров используется для сортировки

    const PRODUCT_RATING_BOOST = 10; // используется в вычислении рейтинга, не ставить меньше 2

    /**
     * Возвращает таблицу с рейтингом товаров
     *
     * @param UserRecord $user
     * @param array $options
     *
     * @return array
     */
    public function resolve(UserRecord $user, array $options = [])
    {
        $onlyActive = isset($options['onlyActive']) ? (bool)$options['onlyActive'] : true;
        $activeAt = isset($options['activeAt']) ? (int)$options['activeAt'] : time();
        $activeUntil = isset($options['activeUntil']) ? (int)$options['activeUntil'] : self::DEFAULT_EVENT_ACTIVE_UNTIL;
        $onlyAvailable = isset($options['onlyAvailable']) ? (bool)$options['onlyAvailable'] : true;
        $limit = isset($options['limit']) ? (int)$options['limit'] : self::DEFAULT_LIMIT;
        $priceFrom = isset($options['priceFrom']) ? (int)$options['priceFrom'] : self::DEFAULT_PRICE_FROM;
        $priceTo = isset($options['priceTo']) ? (int)$options['priceTo'] : self::DEFAULT_PRICE_TO;
        $onlyOwnProducts = isset($options['onlyOwnProducts']) ? (bool)$options['onlyOwnProducts'] : false;

        $where = [];
        $where[] = "t.user_id = {$user->id}";

        if ($onlyActive) {
            $where[] = "e.start_at <= $activeAt AND e.end_at >= $activeAt";

            $activeUntilAt = $activeAt + $activeUntil;
            $where[] = "e.end_at >= $activeUntilAt";
        }
        if ($onlyAvailable) {
            $where[] = "ep.is_sold_out = 0";
        }
        //$where[] = "ep.number_sold_real > 0"; // товары которые продавались хотябы один раз
        if ($priceFrom > 0) {
            $where[] = "ep.price >= $priceFrom";
        }
        if ($priceTo > 0) {
            $where[] = "ep.price <= $priceTo";
        }
        if ($onlyOwnProducts) {
            $where[] = "e.is_drop_shipping = 0";
        }

        $whereQuery = implode(' AND ', $where);

        $booster = self::PRODUCT_RATING_BOOST;

        $query = "
            SELECT
              e.id AS event_id, e.name AS event_name,

              ep.id AS event_product_id, ep.product_id AS product_id,
              ep.name AS product_name, ep.category AS product_category, ep.target AS product_targets,

              (LOG(SUM(ep.number_sold_real) / COUNT(ep.id) + $booster) * MAX(t.rating)) AS score
            FROM
                events AS e
                INNER JOIN
                events_products AS ep
                INNER JOIN
                users_interests AS t
                ON (
                    e.id = ep.event_id AND
                    ep.category = t.category AND
                    FIND_IN_SET(t.target, ep.target) AND
                    (
                        (t.age_to = 0 AND ep.age_to = 0) OR
                        (t.age_to > 0 AND ep.age_to > 0 AND t.age_from >= ep.age_from AND t.age_from <= ep.age_to)
                    )
                )
            WHERE $whereQuery
            GROUP BY ep.product_id
            ORDER BY score DESC
            LIMIT $limit
        ";

        $db = Yii::app()->db;
        $table = $db->createCommand($query)->queryAll();

        return $table;
    }

    /**
     * Возвращает таблицу с товарами сгруппированными по группам категорий (CategoryGroups)
     * Чем выше категория в списке тем она интереснее пользователю
     *
     * @param UserRecord $user
     * @param array $options
     *
     * @return array
     */
    public function resolveGrouped(UserRecord $user, array $options = [])
    {
        $sortAmount = isset($options['sortAmount']) ?
            (int)$options['sortAmount'] : self::DEFAULT_GROUPED_PRODUCT_SORT_AMOUNT;

        $categoryGroups = CategoryGroups::instance();
        $tableRaw = $this->resolve($user, $options);
        $table = [];

        // группировка по категории
        foreach ($tableRaw as $row) {
            $targets = explode(',', $row['product_targets']);

            $categories = $categoryGroups->getInternalCategories(
                $row['product_category'], $targets
            );

            foreach ($categories as $category) {
                if (!isset($table[$category])) {
                    $table[$category] = [];
                }

                $table[$category][] = $row;
            }
        }

        // сортировка
        $scores = [];
        foreach ($table as $category => $rows) {
            $score = 0;
            foreach ($rows as $index => $row) {
                if ($index > $sortAmount - 1) {
                    break;
                }
                $score += $row['score'];
            }
            $scores[$category] = $score;
        }

        uksort($table, function ($a, $b) use ($scores) {
            if ($scores[$a] == $scores[$b]) {
                return 0;
            }

            return $scores[$a] > $scores[$b] ? -1 : 1;
        });

        return $table;
    }
}
