<?php

namespace MommyCom\Model\Statistic;

use CModel;
use MommyCom\Model\Db\UserRecord;

class StatisticUsersOfferModel extends CModel
{
    /** @var  string offer id */
    public $id;

    public $offerProvider;

    public $ordersConfirmed = 0;

    public $ordersCanceled = 0;

    public $ordersMerged = 0;

    public $ordersNotConfirmed = 0;

    public $totalOrders = 0;

    public $ordersAmount = 0;

    public $countUsers = 0;

    public $countEmailVerified = 0;

    public function __construct($id, $offerProvider)
    {
        $this->id = $id;
        $this->offerProvider = $offerProvider;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return [];
    }

    public function addUser($user)
    {
        /** @var UserRecord $user */
        $user = is_array($user) ? (object)$user : $user;

        $this->countUsers++;

        if ($user->is_email_verified) {
            $this->countEmailVerified++;
        }
    }
}
