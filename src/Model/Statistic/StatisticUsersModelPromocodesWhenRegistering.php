<?php

namespace MommyCom\Model\Statistic;

use ArrayObject;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Type\Cast;

final class StatisticUsersModelPromocodesWhenRegistering extends ArrayObject
{
    /** @var float */
    public $ordersAmount = 0.0;

    /** @var int */
    public $countOrders = 0;

    /** @var float */
    public $promocodeUsedAmount = 0.0;

    /** @var array */
    private $_ordersPromocodes = [];

    /* @var array */
    private $_userPromocodes = [];

    public function getCountOrdersPromocodes()
    {
        return count($this->_ordersPromocodes);
    }

    /**
     * @return array
     */
    public function getOrdersPromocodes()
    {
        return $this->_ordersPromocodes;
    }

    /**
     * @return array
     */
    public function getUserPromocodes()
    {
        return $this->_userPromocodes;
    }

    /**
     * @return array
     */
    public function getCountUserPromocodes()
    {
        return count($this->_userPromocodes);
    }

    /**
     * @param OrderRecord $order
     *
     * @return bool
     */
    public function addFromOrder(OrderRecord $order)
    {
        if (!$order->promocode) {
            return false;
        }

        $this->ordersAmount += Cast::toFloat($order->price_total);
        $this->countOrders++;
        $this->_ordersPromocodes[] = $order->promocode;
        $this->promocodeUsedAmount += Cast::toFloat($order->discount); //для нового пользователя нет больше дисконта чем промокод

        return true;
    }

    /**
     * @param $promocode
     * @param $orderAmount
     * @param $discountAmount
     */
    public function addOrderData($promocode, $orderAmount, $discountAmount)
    {
        $this->ordersAmount += Cast::toFloat($orderAmount);
        $this->countOrders++;
        $this->_ordersPromocodes[] = $promocode;
        $this->promocodeUsedAmount += Cast::toFloat($discountAmount); //для нового пользователя нет больше дисконта чем промокод
    }

    /**
     * @param string $promocode
     */
    public function addPromocode($promocode)
    {
        $this->_userPromocodes[$promocode] = $promocode;
    }
}
