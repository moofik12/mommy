<?php

namespace MommyCom\Model\Statistic;

use CModel;

/**
 * Class StatisticOrdersBuyoutProductModel
 *
 * @property-read float $amountSold
 * @property-read float $amountReturned
 * @property-read float $amountNotBought
 * @property-read float|int $ratioSold
 * @property-read float|int $ratioReturned
 * @property-read float|int $ratioNotBought
 * @property-read array $notBoughtOrders
 */
class StatisticOrdersBuyoutProductModel extends CModel
{
    /** @var int key of EventProductRecord */
    public $productID;

    /** @var int */
    public $eventID;

    /** @var int key of ProductRecord */
    public $baseProductID;

    /** @var  string */
    public $photoFileID;

    /** @var  string */
    public $name;

    /** @var  int key of AdminUserRecord */
    public $brandManagerID;

    /** @var  string */
    public $brandManagerName;

    /** @var float */
    public $price = 0.0;

    /** @var float */
    public $pricePurchase = 0.0;

    /** @var int */
    public $count = 0;

    /** @var int */
    public $countReturned = 0;

    /** @var int */
    public $countNotBought = 0;

    /** @var array */
    private $_notBoughtOrders = [];

    /**
     * @param int $productID
     * @param float $price
     * @param float $pricePurchase
     */
    public function __construct($productID, $price, $pricePurchase)
    {
        $this->productID = $productID;
        $this->price = $price;
        $this->pricePurchase = $pricePurchase;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return ['productID', 'eventID', 'price', 'pricePurchase', 'count', 'countReturned', 'countNotBought', 'amountSold', 'amountReturned', 'amountNotBought', 'notBoughtOrders'];
    }

    /**
     * @return float
     */
    public function getAmountSold()
    {
        return $this->price * $this->count;
    }

    /**
     * @return float
     */
    public function getAmountReturned()
    {
        return $this->price * $this->countReturned;
    }

    /**
     * @return float
     */
    public function getAmountNotBought()
    {
        return $this->price * $this->countNotBought;
    }

    /**
     * @return float|int
     */
    public function getRatioSold()
    {
        if ($this->count) {
            return round(100 - (($this->countReturned + $this->countNotBought) / $this->count) * 100);
        }

        return 100;
    }

    /**
     * @return float|int
     */
    public function getRatioReturned()
    {
        if ($this->count) {
            return round(($this->countReturned / $this->count) * 100);
        }

        return 0;
    }

    /**
     * @return float|int
     */
    public function getRatioNotBought()
    {
        if ($this->count) {
            return round(($this->countNotBought / $this->count) * 100);
        }

        return 0;
    }

    /**
     * @param int $orderID
     */
    public function addNotBoughtOrder($orderID)
    {
        if (isset($this->_notBoughtOrders[$orderID])) {
            return;
        }

        $this->_notBoughtOrders[$orderID] = $orderID;
    }

    /**
     * @return array
     */
    public function getNotBoughtOrders()
    {
        return array_values($this->_notBoughtOrders);
    }
}
