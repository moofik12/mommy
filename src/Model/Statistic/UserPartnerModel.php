<?php

namespace MommyCom\Model\Statistic;

/**
 * Class UserPartnerModel
 */
class UserPartnerModel
{
    public $id;
    /**
     * @var string
     */
    public $day;
    /**
     * @var int
     */
    public $countUser;
    /**
     * @var int
     */
    public $commonCount = 0;

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'day' => 'Дата',
            'countUser' => 'количество подавших заявку',
        ];
    }
}
