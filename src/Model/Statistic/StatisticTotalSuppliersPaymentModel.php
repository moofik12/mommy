<?php

namespace MommyCom\Model\Statistic;

use MommyCom\YiiComponent\Statistic\StatisticModelInterface;
use MommyCom\YiiComponent\Statistic\StatisticTotalModelInterface;

final class StatisticTotalSuppliersPaymentModel implements StatisticTotalModelInterface
{
    protected $_countSuppliers = 0;
    protected $_countContracts = 0;
    protected $_countEvents = 0;
    protected $_countPaid = 0;
    protected $_countPayments = 0;

    protected $_payAmount = 0.0; //к оплате
    protected $_requestAmount = 0.0; //сумма запроса
    protected $_deliveredAmount = 0.0; //сумма доставленных
    protected $_deliveredAmountWrongTime = 0.0; //сумма доставленных после окончания времен доставки
    protected $_returnedAmount = 0.0; //сумма возвратов
    protected $_penaltyAmount = 0.0; //сумма штрафов
    protected $_paidAmount = 0.0; //сумма оплат
    protected $_customPenaltyAmount = 0.0; //сумма доп. штрафов

    protected $_payAmountAllRequest = 0.0; //сумма к оплате если все оплаты выплачены
    protected $_paidAmountAllRequest = 0.0; //сумма выплат если все оплаты выплачены

    /**
     * Statistic only page
     *
     * @var bool
     */
    public $totalOnlyPage = false;

    /**
     * give item only visible on page
     *
     * @return bool
     */
    public function isUseOlyPageItem()
    {
        return !!$this->totalOnlyPage;
    }

    /**
     * Сan be used many times
     *
     * @param StatisticModelInterface $model
     */
    public function countingModel(StatisticModelInterface $model)
    {
        $this->_counting($model);
    }

    /**
     * @param StatisticSuppliersPaymentModel $model
     */
    protected function _counting(StatisticSuppliersPaymentModel $model)
    {
        $this->_countSuppliers += $model->countPayments;
        $this->_countContracts += $model->countContracts;
        $this->_countEvents += $model->countEvents;
        $this->_countPaid += $model->countPaid;
        $this->_countPayments += $model->countPayments;

        $this->_payAmount += $model->payAmount;
        $this->_paidAmount += $model->paidAmount;
        $this->_requestAmount += $model->requestAmount;
        $this->_deliveredAmount += $model->deliveredAmount;
        $this->_deliveredAmountWrongTime += $model->deliveredAmountWrongTime;
        $this->_returnedAmount += $model->returnedAmount;
        $this->_penaltyAmount += $model->penaltyAmount;
        $this->_customPenaltyAmount += $model->customPenaltyAmount;

        if ($model->isFullRequestProcessed()) {
            $this->_payAmountAllRequest += $model->payAmount;
            $this->_paidAmountAllRequest += $model->paidAmount;
        }
    }


    //GETTERS

    /**
     * @return int
     */
    public function getCountSuppliers()
    {
        return $this->_countSuppliers;
    }

    /**
     * @return int
     */
    public function getCountContracts()
    {
        return $this->_countContracts;
    }

    /**
     * @return int
     */
    public function getCountEvents()
    {
        return $this->_countEvents;
    }

    /**
     * @return int
     */
    public function getCountPaid()
    {
        return $this->_countPaid;
    }

    /**
     * @return int
     */
    public function getCountPayments()
    {
        return $this->_countPayments;
    }

    /**
     * @return float
     */
    public function getPayAmount()
    {
        return $this->_payAmount;
    }

    /**
     * @return float
     */
    public function getRequestAmount()
    {
        return $this->_requestAmount;
    }

    /**
     * @return float
     */
    public function getDeliveredAmount()
    {
        return $this->_deliveredAmount;
    }

    /**
     * @return float
     */
    public function getDeliveredAmountWrongTime()
    {
        return $this->_deliveredAmountWrongTime;
    }

    /**
     * @return float
     */
    public function getReturnedAmount()
    {
        return $this->_returnedAmount;
    }

    /**
     * @return float
     */
    public function getPenaltyAmount()
    {
        return $this->_penaltyAmount;
    }

    /**
     * @return float
     */
    public function getPaidAmount()
    {
        return $this->_paidAmount;
    }

    /**
     * @return float
     */
    public function getCustomPenaltyAmount()
    {
        return $this->_customPenaltyAmount;
    }

    /**
     * @return float
     */
    public function getPayAmountAllRequest()
    {
        return $this->_payAmountAllRequest;
    }

    /**
     * @return float
     */
    public function getPaidAmountAllRequest()
    {
        return $this->_paidAmountAllRequest;
    }

    /**
     * @return float
     */
    public function getDiffPaymentsFullRequestProcessedAmount()
    {
        return $this->_paidAmountAllRequest - $this->_payAmountAllRequest;
    }

}
