<?php

namespace MommyCom\Model\Statistic;

use MommyCom\YiiComponent\Statistic\StatisticModelInterface;
use MommyCom\YiiComponent\Statistic\StatisticTotalModelInterface;

/**
 * Class StatisticTotalOrdersBuyoutModel
 *
 * @property-read int $totalAverageProducts;
 * @property-read int $totalAverageCheck;
 * @property-read int $totalConfirmed;
 * @property-read int $totalNotConfirmed;
 * @property-read int $totalCanceled;
 * @property-read int $totalMerged;
 * @property-read int $totalPrice;
 * @property-read int $totalOrders;
 * @property-read float $percentConfirmed;
 * @property-read float $percentNotConfirmed;
 * @property-read float $percentCanceled;
 * @property-read float $usedBonuses
 * @property-read float $usedDiscounts
 * @property-read float $profit //маржа
 * @property-read float $netProfit //возможная чистая прибыль
 * @property-read float $percentUsedBonuses
 * @property-read float $percentUsedDiscount
 * @property-read float $returnOrdersProducts
 * @property-read float $expensesShopFromOrders
 * @property-read float $payed
 * @property-read float $realNetProfit //чистая прибыль
 * @property-read float $realUsedDiscounts
 * @property-read float $realUsedBonuses
 * @property-read float $expensesShop
 */
class StatisticTotalOrdersBuyoutModel implements StatisticTotalModelInterface
{
    public $orders;
    public $ordersAmount;
    public $ordersAmountToPay;
    public $ordersAmountPaid;
    public $notBoughtOrders;
    public $notBoughtOrdersAmount;
    public $returnedOrders;
    public $returnedOrdersAmount;

    public $countProducts = 0;
    public $countNotBoughtProducts = 0;
    public $countReturnedProducts = 0;

    /**
     * Statistic only page
     *
     * @var bool
     */
    public $totalOnlyPage = false;

    /** @var array */
    private $_ordersCities = [];
    private $_ordersCitiesSorted = false;

    /**
     * Сan be used many times
     *
     * @param StatisticModelInterface $model
     */
    public function countingModel(StatisticModelInterface $model)
    {
        $this->_counting($model);
    }

    /**
     * give item only visible on page
     *
     * @return bool
     */
    public function isUseOlyPageItem()
    {
        return !!$this->totalOnlyPage;
    }

    /**
     * @return float
     */
    public function getOrdersAmountPaidPercent()
    {
        $divider = $this->ordersAmountToPay > 0 ? $this->ordersAmountToPay : 1;

        return round($this->ordersAmountPaid / $divider, 3) * 100;
    }

    /**
     * @return float
     */
    public function getOrdersAmountNeedPay()
    {
        return $this->ordersAmountToPay - $this->ordersAmountPaid;
    }

    /**
     * @return float
     */
    public function getOrdersAmountNeedPayPercent()
    {
        return 100 - $this->getOrdersAmountPaidPercent();
    }

    /**
     * @return int
     */
    public function getOrdersBought()
    {
        return $this->orders - $this->notBoughtOrders - $this->returnedOrders;
    }

    /**
     * @return float
     */
    public function getOrdersBoughtAmount()
    {
        return $this->ordersAmount - $this->notBoughtOrdersAmount - $this->returnedOrdersAmount;
    }

    /**
     * @return int
     */
    public function getOrdersBoughtProducts()
    {
        return $this->countProducts - $this->countNotBoughtProducts - $this->countReturnedProducts;
    }

    /**
     * @param bool $limit
     *
     * @return array [cityID => countDelivery]
     */
    public function getOrdersCities($limit = false)
    {
        if ($this->_ordersCitiesSorted === false) {
            arsort($this->_ordersCities);
            $this->_ordersCitiesSorted = true;
        }

        if ($limit > 0) {
            return array_slice($this->_ordersCities, 0, $limit, true);
        }

        return $this->_ordersCities;
    }

    /**
     * @param StatisticOrdersBuyoutModel $model
     */
    protected function _counting(StatisticOrdersBuyoutModel $model)
    {
        $this->_ordersCitiesSorted = false;
        foreach ($model->getOrdersCities() as $cityID => $countDelivery) {
            if (!isset($this->_ordersCities[$cityID])) {
                $this->_ordersCities[$cityID] = 0;
            }

            $this->_ordersCities[$cityID] += $countDelivery;
        }

        $this->orders += $model->orders;
        $this->ordersAmount += $model->ordersAmount;
        $this->ordersAmountPaid += $model->ordersAmountPaid;
        $this->ordersAmountToPay += $model->ordersAmountToPay;
        $this->notBoughtOrders += $model->notBoughtOrders;
        $this->notBoughtOrdersAmount += $model->notBoughtOrdersAmount;
        $this->returnedOrders += $model->returnedOrders;
        $this->returnedOrdersAmount += $model->returnedOrdersAmount;

        $this->countProducts = $model->getCountProducts();
        $this->countReturnedProducts = $model->getCountReturnedProducts();
        $this->countNotBoughtProducts = $model->getCountNotBoughtProducts();
    }
}
