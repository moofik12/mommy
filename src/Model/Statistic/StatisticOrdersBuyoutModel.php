<?php

namespace MommyCom\Model\Statistic;

use LogicException;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderReturnProductRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Statistic\StatisticModelAbstract;
use MommyCom\YiiComponent\Type\Cast;

/**
 * @property-read int $ordersBought
 * @property-read float $ordersBoughtAmount
 * @property-read int $ordersBoughtProducts
 * @property-read float $ordersAmountPaidPercent
 * @property-read float $ordersAmountNeedPay
 * @property-read float $ordersAmountNeedPayPercent
 */
class StatisticOrdersBuyoutModel extends StatisticModelAbstract
{
    public $id;
    public $day;

    public $orders;
    public $ordersAmount;
    public $ordersAmountToPay;
    public $ordersAmountPaid;
    public $notBoughtOrders;
    public $notBoughtOrdersAmount;
    public $returnedOrders;
    public $returnedOrdersAmount;

    /** @var StatisticOrdersBuyoutSupplierModel[] */
    private $_suppliers = [];

    /** @var array */
    private $_ordersCities = [];

    /**
     * Инициализация, иные вычисления на основе дат
     */
    protected function afterCreate()
    {
        $this->id = $this->day = $this->dateTimeFrom->getTimestamp();
    }

    /**
     * Сan be used many times
     *
     * @param array $data
     *
     * @return mixed
     */
    public function convertData(array $data)
    {
        $this->_countingProducts($data);
        $this->_countingReturned($data);
    }

    /**
     * Returns the list of attribute names.
     * By default, this method returns all public properties of the class.
     * You may override this method to change the default.
     *
     * @return array list of attribute names. Defaults to all public properties of the class.
     */
    public function attributeNames()
    {
        return array_merge(parent::attributeNames(),
            ['ordersBought', 'ordersBoughtAmount', 'ordersBoughtProducts']
        );
    }

    /**
     * @return StatisticOrdersBuyoutSupplierModel[]
     */
    public function getSuppliers()
    {
        return array_values($this->_suppliers);
    }

    /**
     * @return StatisticOrdersBuyoutProductModel[]
     */
    public function getProducts()
    {
        $products = [];
        foreach ($this->getSuppliers() as $supplier) {
            foreach ($supplier->getProducts() as $product) {
                $products[] = $product;
            }
        }

        return $products;
    }

    /**
     * @return array StatisticOrdersBuyoutBrandManagerModel[]
     */
    public function getBrandManagersProducts()
    {
        $results = [];

        foreach ($this->getProducts() as $product) {
            $model = isset($results[$product->brandManagerID]) ? $results[$product->brandManagerID] : null;
            if ($model === null) {
                $model = new StatisticOrdersBuyoutBrandManagerModel($product->brandManagerID);
                $model->fullname = $product->brandManagerName;
                $results[$product->brandManagerID] = $model;
            }

            $model->setProduct($product);
        }

        return array_values($results);
    }

    /**
     * @return float
     */
    public function getOrdersAmountPaidPercent()
    {
        $divider = $this->ordersAmountToPay > 0 ? $this->ordersAmountToPay : 1;

        return round($this->ordersAmountPaid / $divider, 3) * 100;
    }

    /**
     * @return float
     */
    public function getOrdersAmountNeedPay()
    {
        return $this->ordersAmountToPay - $this->ordersAmountPaid;
    }

    /**
     * @return float
     */
    public function getOrdersAmountNeedPayPercent()
    {
        return 100 - $this->getOrdersAmountPaidPercent();
    }

    /**
     * @return int
     */
    public function getOrdersBought()
    {
        return $this->orders - $this->notBoughtOrders - $this->returnedOrders;
    }

    /**
     * @return float
     */
    public function getOrdersBoughtAmount()
    {
        return $this->ordersAmount - $this->notBoughtOrdersAmount - $this->returnedOrdersAmount;
    }

    /**
     * @return int
     */
    public function getOrdersBoughtProducts()
    {
        return $this->getCountProducts() - $this->getCountNotBoughtProducts() - $this->getCountReturnedProducts();
    }

    /**
     * @return int
     */
    public function getCountProducts()
    {
        return array_reduce($this->getSuppliers(), function ($carry, $item) {
            /* @var $item StatisticOrdersBuyoutSupplierModel */
            return $carry + $item->countSold;
        }, 0);
    }

    /**
     * @return int
     */
    public function getCountNotBoughtProducts()
    {
        return array_reduce($this->getSuppliers(), function ($carry, $item) {
            /* @var $item StatisticOrdersBuyoutSupplierModel */
            return $carry + $item->countNotBought;
        }, 0);
    }

    /**
     * @return int
     */
    public function getCountReturnedProducts()
    {
        return array_reduce($this->getSuppliers(), function ($carry, $item) {
            /* @var $item StatisticOrdersBuyoutSupplierModel */
            return $carry + $item->countReturned;
        }, 0);
    }

    /**
     * @return array [cityID => countDelivery]
     */
    public function getOrdersCities()
    {
        return $this->_ordersCities;
    }

    /**
     * @param $supplierId
     *
     * @return null|StatisticOrdersBuyoutSupplierModel
     */
    private function _getSupplier($supplierId)
    {
        if (isset($this->_suppliers[$supplierId])) {
            return $this->_suppliers[$supplierId];
        }

        return null;
    }

    /**
     * @param StatisticOrdersBuyoutSupplierModel $model
     */
    private function _setSupplier(StatisticOrdersBuyoutSupplierModel $model)
    {
        $this->_suppliers[$model->supplierID] = $model;
    }

    private function _countingProducts($data)
    {
        $ordersID = ArrayUtils::getColumn($data, 'id');

        $queue = array_chunk($ordersID, 1000);
        foreach ($queue as $heap) {
            $aliases = [];
            $ordersProducts = OrderProductRecord::model()
                ->orderIds($heap)
                ->callcenterStatuses([OrderProductRecord::CALLCENTER_STATUS_ACCEPTED])
                ->with(['product.product' => ['alias' => 'baseProduct'], 'product', 'product.supplier', 'event.brandManager'])
                ->together()
                ->getSqlCommand($aliases)->queryAll();

            if (!isset($aliases['product.supplier_id']) || !isset($aliases['product.price_purchase'])) {
                throw new LogicException('Aliases product.{name} not found for work');
            }

            foreach ($ordersProducts as $ordersProduct) {
                $supplierID = Cast::toUInt($ordersProduct[$aliases['product.supplier_id']]);
                $supplierName = $ordersProduct[$aliases['supplier.name']];
                $eventID = Cast::toUInt($ordersProduct[$aliases['product.event_id']]);
                $productID = Cast::toUInt($ordersProduct[$aliases['product_id']]);
                $pricePurchase = Cast::toUFloat($ordersProduct[$aliases['product.price_purchase']]);
                $price = Cast::toFloat($ordersProduct[$aliases['price']]);
                $productCount = Cast::toUInt($ordersProduct[$aliases['number']]);
                $baseProductID = Cast::toUInt($ordersProduct[$aliases['product.product_id']]);
                $photoFileID = $ordersProduct[$aliases['baseProduct.logo_fileid']];
                $productName = $ordersProduct[$aliases['product.name']];
                $brandManagerID = $ordersProduct[$aliases['brandManager.id']];
                $brandManagerName = $ordersProduct[$aliases['brandManager.fullname']];

                $model = $this->_getSupplier($supplierID);
                if ($model === null) {
                    $model = new StatisticOrdersBuyoutSupplierModel($supplierID);
                    $model->supplierName = $supplierName;
                }

                $supplierProduct = $model->getProduct($productID);
                if ($supplierProduct === null) {
                    $supplierProduct = new StatisticOrdersBuyoutProductModel($productID, $price, $pricePurchase);
                    $supplierProduct->eventID = $eventID;
                    $supplierProduct->baseProductID = $baseProductID;
                    $supplierProduct->photoFileID = $photoFileID;
                    $supplierProduct->name = $productName;
                    $supplierProduct->brandManagerID = $brandManagerID;
                    $supplierProduct->brandManagerName = $brandManagerName;
                }

                $supplierProduct->count += $productCount;

                $model->setProduct($supplierProduct);
                $this->_setSupplier($model);
            }
        }

        $ordersAmounts = array_sum(ArrayUtils::getColumn($data, 'price_total'));
        $ordersBonuses = array_sum(ArrayUtils::getColumn($data, 'bonuses'));
        $orderDiscount = array_sum(ArrayUtils::getColumn($data, 'discount'));

        $this->orders += count($data);
        $this->ordersAmount += $ordersAmounts;
        $this->ordersAmountPaid += array_sum(ArrayUtils::getColumn($data, 'card_payed'));
        $this->ordersAmountToPay += $ordersAmounts - $ordersBonuses - $orderDiscount;
    }

    /**
     * возвраты заказов
     *
     * @param array $data
     */
    private function _countingReturned(array $data)
    {
        $ordersID = ArrayUtils::getColumn($data, 'id');

        $queue = array_chunk($ordersID, 1000);
        foreach ($queue as $heap) {
            $aliases = [];
            $ordersReturns = OrderReturnRecord::model()
                ->orderIdIn($heap)
                ->statusNotIn([OrderReturnRecord::STATUS_UNCONFIGURED])
                ->with([
                    'positions' => [
                        'scopes' => [
                            'status' => [OrderReturnProductRecord::STATUS_OK],
                        ],
                    ],
                    'positions.eventProduct',
                ])
                ->together()
                ->getSqlCommand($aliases)->queryAll();

            $ordersReturns = ArrayUtils::groupBy($ordersReturns, $aliases['order_id']);

            foreach ($ordersReturns as $orderID => $ordersPositionsReturn) {
                $isReturned = false;
                $orderAmount = 0;

                foreach ($ordersPositionsReturn as $ordersPositionReturn) {
                    if ($ordersPositionReturn[$aliases['positions.status']] != OrderReturnProductRecord::STATUS_OK) {
                        throw new LogicException('Not valid data! OrderReturnProductRecord::status not is STATUS_OK');
                    }

                    if ($orderAmount == 0) {
                        $orderAmount = $ordersPositionReturn[$aliases['price_products']];
                    }

                    if ($ordersPositionReturn[$aliases['price_refund']] > 0) {
                        $isReturned = true;
                    }

                    $supplierID = Cast::toUInt($ordersPositionReturn[$aliases['eventProduct.supplier_id']]);
                    $productID = Cast::toUInt($ordersPositionReturn[$aliases['eventProduct.id']]);
                    $pricePurchase = Cast::toUFloat($ordersPositionReturn[$aliases['eventProduct.price_purchase']]);
                    $price = Cast::toUFloat($ordersPositionReturn[$aliases['positions.price']]);
                    $orderID = Cast::toUFloat($ordersPositionReturn[$aliases['order_id']]);

                    $model = $this->_getSupplier($supplierID);
                    if ($model === null) {
                        $model = new StatisticOrdersBuyoutSupplierModel($supplierID);
                    }

                    $supplierProduct = $model->getProduct($productID);
                    if ($supplierProduct === null) {
                        $supplierProduct = new StatisticOrdersBuyoutProductModel($productID, $price, $pricePurchase);
                    }

                    if ($isReturned) {
                        $supplierProduct->countReturned++;
                    } else {
                        $supplierProduct->countNotBought++;
                        $supplierProduct->addNotBoughtOrder($orderID);
                    }

                    $model->setProduct($supplierProduct);
                    $this->_setSupplier($model);
                }

                if ($isReturned) {
                    $this->returnedOrders++;
                    $this->returnedOrdersAmount += $orderAmount;
                } else {
                    $this->notBoughtOrders++;
                    $this->notBoughtOrdersAmount += $orderAmount;
                }
            }
        }
    }
}
