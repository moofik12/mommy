<?php

namespace MommyCom\Model\Statistic;

use CHttpException;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Statistic\StatisticModelAbstract;
use MommyCom\YiiComponent\Type\Cast;

class StatisticOrdersModel extends StatisticModelAbstract
{
    const ORDER_TYPE_ALL = 'all';
    const ORDER_TYPE_OWN = 'own';
    const ORDER_TYPE_DROP_SHIPPING = 'dropShipping';

    /**
     * to grid
     *
     * @var
     */
    public $id;

    /**
     * @var integer timestamp начало дня
     */
    public $day;

    private $_orderType = self::ORDER_TYPE_ALL;

    private $_countConfirmed = 0;
    private $_countCanceled = 0;
    private $_countNotConfirmed = 0;
    private $_priceTotal = 0;
    private $_priceTotalUnconfirmedIncludes = 0;
    private $_countMerged = 0;
    private $_countRedeliveryNpCard = 0;
    private $_amountRedeliveryNpCard = 0.0;
    private $_countPaidQuickly = 0;
    private $_amountPaidQuickly = 0.0;

    private $_productsTotalConfirmed = 0;
    private $_usedBonuses = 0.0;
    private $_usedDiscounts = 0.0;
    private $_count = 0;
    private $_profit = 0.0;
    private $_countDropShipping = 0;
    private $_amountDropShipping = 0.0;
    private $_amountDropShippingOrdersForCommission = 0.0;
    private $_amountDropShippingOrdersCommission = 0.0;
    private $_dropShippingOrdersForCommission = []; //масив ID заказов

    /** @var float возможная прибыль */
    private $_netProfit = 0.0;

    /** @var float Возвраты сделанных заказов */
    private $_returnOrdersProducts = 0.0;

    /** @var float издержки магазина по заказам которые были сделаны в этот день */
    private $_expensesShopFromOrder = 0.0;

    /**
     * @var float издержки магазина за период
     */
    private $_expensesShop = 0.0;

    /**
     * @var StatisticOrdersPaymentModel
     */
    protected $_paid;

    /** @var float Возвраты сделанных за период */
    private $_returnProducts = 0.0;

    /** @var float прибыль от оплаченных заказов */
    private $_realProfit = 0.0;

    /** @var float использована скидка от оплаченных заказов */
    private $_realUsedDiscount = 0.0;

    /** @var float использованы бонусы от оплаченных заказов */
    private $_realUsedBonuses = 0.0;

    /** @var array */
    private $_userOffersStorage = [];

    /** @var array */
    private $_brandsManagerStorage = [];

    /** @var array */
    private $_ordersData = [];

    /** @var array */
    private $_userEmails = [];

    /**
     * @param array $data
     *
     * @throws CHttpException
     * @return null
     */
    public function convertData(array $data)
    {
        $this->_counting($data);
    }

    /**
     * Инициализация, иные вычисления на основе дат
     */
    protected function afterCreate()
    {
        $this->id = $this->day = $this->dateTimeFrom->getTimestamp();
        $this->_paid = new StatisticOrdersPaymentModel();

        $filterOrderType = $this->getStatistic()->getFilter('orderType');
        if ($filterOrderType) {
            $this->setOrderType($filterOrderType->value);
        }

        $this->_countingProfit();
    }

    /**
     * @return string
     */
    public function getOrderType()
    {
        return $this->_orderType;
    }

    public function setOrderType($value)
    {
        if (in_array($value, [
            self::ORDER_TYPE_ALL,
            self::ORDER_TYPE_OWN,
            self::ORDER_TYPE_DROP_SHIPPING,
        ])) {
            $this->_orderType = $value;
        }
    }

    /**
     * @return array
     */
    public function getUserEmails()
    {
        return $this->_userEmails;
    }

    /**
     * общая сумма в заказе
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->_priceTotal;
    }

    /**
     * @return float
     */
    public function getPriceUnconfirmedIncludes()
    {
        return $this->_priceTotalUnconfirmedIncludes;
    }

    /**
     * @return integer
     */
    public function getCountConfirmed()
    {
        return $this->_countConfirmed;
    }

    /**
     * @return integer
     */
    public function getCountCanceled()
    {
        $countMerged = $this->getCountMerged();
        return $this->_countCanceled - $countMerged;
    }

    /**
     * @return int
     */
    public function getCountMerged()
    {
        return $this->_countMerged;
    }

    /**
     * @return integer
     */
    public function getCountNotConfirmed()
    {
        return $this->_countNotConfirmed;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->_count;
    }

    /**
     * @return float
     */
    public function getUsedBonuses()
    {
        return $this->_usedBonuses;
    }

    /**
     * @return float
     */
    public function getUsedDiscounts()
    {
        return $this->_usedDiscounts;
    }

    /**
     * @return float
     */
    public function getProfit()
    {
        return $this->_profit;
    }

    /**
     * @return float
     */
    public function getNetProfit()
    {
        return $this->_netProfit;
    }

    /**
     * @return float
     */
    public function getPercentUsedBonuses()
    {
        $usedBonuses = $this->_usedBonuses;
        $priceTotal = $this->_priceTotal > 0 ? $this->_priceTotal : 1;

        return round(($usedBonuses * 100) / $priceTotal, 2);
    }

    /**
     * @return float
     */
    public function getPercentUsedDiscounts()
    {
        $usedDiscounts = $this->_usedDiscounts;
        $priceTotal = $this->_priceTotal > 0 ? $this->_priceTotal : 1;

        return round(($usedDiscounts * 100) / $priceTotal, 2);
    }

    /**
     * @return float
     */
    public function getAverageCheck()
    {
        $countConfirmed = $this->getCountConfirmed();

        if ($countConfirmed <= 0) {
            $countConfirmed = 1;
        }

        return round($this->_priceTotal / $countConfirmed, 2);
    }

    /**
     * @return float
     */
    public function getAverageCheckUnconfirmedIncludes()
    {
        $count = $this->_count - $this->_countCanceled;

        if ($count <= 0) {
            $count = 1;
        }

        return round($this->_priceTotalUnconfirmedIncludes / $count, 2);
    }

    /**
     * @return float
     */
    public function getAverageProducts()
    {
        $countProductsConfirmed = $this->getProductsTotalConfirmed();
        $countConfirmed = $this->getCountConfirmed();

        if ($countConfirmed <= 0) {
            $countConfirmed = 1;
        }

        return round($countProductsConfirmed / $countConfirmed, 1);
    }

    /**
     * @return float
     */
    public function getRealProfit()
    {
        return $this->_realProfit;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getRealUsedDiscount()
    {
        return $this->_realUsedDiscount;
    }

    /**
     * @return float
     */
    public function getRealUsedBonuses()
    {
        return $this->_realUsedBonuses;
    }

    /**
     * реальная прибыль за период
     */
    public function getNetRealProfit()
    {
        return $this->getRealProfit() + $this->getDropShippingCommission() - $this->getExpensesShop() - $this->getRealUsedDiscount() - $this->getRealUsedBonuses();
    }

    /**
     * @return float
     */
    public function getReturnOrdersProducts()
    {
        return $this->_returnOrdersProducts;
    }

    /**
     * @return float
     */
    public function getExpensesShopFromOrder()
    {
        return $this->_expensesShopFromOrder;
    }

    /**
     * @return float
     */
    public function getExpensesShop()
    {
        return $this->_expensesShop;
    }

    /**
     * @return StatisticOrdersPaymentModel
     */
    public function getPaid()
    {
        return $this->_paid;
    }

    /**
     * @return float
     */
    public function getReturnProducts()
    {
        return $this->_returnProducts;
    }

    /**
     * @return int
     */
    public function getProductsTotalConfirmed()
    {
        return $this->_productsTotalConfirmed;
    }

    /**
     * @return int
     */
    public function getCountRedeliveryNpCard()
    {
        return $this->_countRedeliveryNpCard;
    }

    /**
     * @return float
     */
    public function getAmountRedeliveryNpCard()
    {
        return $this->_amountRedeliveryNpCard;
    }

    /**
     * @return int
     */
    public function getCountPaidQuickly()
    {
        return $this->_countPaidQuickly;
    }

    /**
     * @return float
     */
    public function getAmountPaidQuickly()
    {
        return $this->_amountPaidQuickly;
    }

    /**
     * @return float
     */
    public function getDropShippingCommission()
    {
        return $this->_amountDropShippingOrdersCommission;
    }

    /**
     * @return array
     */
    public function getDropShippingOrdersForCommission()
    {
        return $this->_dropShippingOrdersForCommission;
    }

    /**
     * @return float  Сумма дропшиппинговых заказов
     */
    public function getAmountDropShipping()
    {
        return $this->_amountDropShipping;
    }

    /**
     * @return int
     */
    public function getCountDropShipping()
    {
        return $this->_countDropShipping;
    }

    /**
     * @param array $filterOfferProvider array(UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN)
     * @param bool $preserve_keys
     *
     * @return array
     */
    public function getUserOffersHistory(array $filterOfferProvider = [], $preserve_keys = false)
    {
        $filters = array_filter($this->_userOffersStorage, function ($offer) use ($filterOfferProvider) {
            /** @var  StatisticOrdersUserOffersModel $offer */
            return in_array($offer->offerProvider, $filterOfferProvider) || empty($filterOfferProvider);
        });
        return $preserve_keys ? $filters : array_values($filters);
    }

    /**
     * @param bool $preserve_keys
     *
     * @return StatisticOrdersBrandsManagersModel[]
     */
    public function getBrandsManagerHistory($preserve_keys = false)
    {
        return $preserve_keys ? $this->_brandsManagerStorage : array_values($this->_brandsManagerStorage);
    }

    /**
     * @return array [['id' => 12, 'userID' => 155], ['id' => 14, 'userID' => 157]]
     */
    public function getOrdersData()
    {
        return $this->_ordersData;
    }

    /**
     * @param array $orders
     *
     * @throws CHttpException
     */
    protected function _counting(array $orders)
    {
        $countConfirmed = $this->_countConfirmed;
        $countCanceled = $this->_countCanceled;
        $countNotConfirmed = $this->_countNotConfirmed;
        $countProductsConfirmed = $this->_productsTotalConfirmed;
        $countMerged = $this->_countMerged;
        $priceTotal = $this->_priceTotal;
        $priceTotalUnconfirmedIncludes = $this->_priceTotalUnconfirmedIncludes;
        $usedBonuses = $this->_usedBonuses;
        $usedDiscounts = $this->_usedDiscounts;
        $count = $this->_count + count($orders);
        $profit = $this->_profit;

        $dropShippingOrdersCount = 0;
        $dropShippingOrdersAmount = 0.0;

        /** @var $orders OrderRecord[] object stdClass */
        foreach ($orders as $order) {
            /** @var OrderRecord $order */ //условно
            $order = is_array($order) ? (object)$order : $order;
            $orderArray = (array)$order;

            if (!in_array($order->processing_status, [
                OrderRecord::PROCESSING_CANCELLED,
                OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER,
                OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE,
            ])) {
                $priceTotalUnconfirmedIncludes += $order->price_total;
            }

            $this->_ordersData[] = ['id' => $order->id, 'userID' => $order->user_id];

            if (in_array($order->processing_status, [
                OrderRecord::PROCESSING_CANCELLED,
                OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER,
                OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE,
            ])) {
                $countCanceled++;
            } elseif (in_array($order->processing_status, [
                OrderRecord::PROCESSING_CALLCENTER_CALL_LATER,
                OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
                OrderRecord::PROCESSING_UNMODERATED,

            ])) {
                $countNotConfirmed++;
            } else {
                $countConfirmed++;

                // результат только для подтвержденных
                $priceTotal += $order->price_total;
                $usedBonuses += $order->bonuses;
                $usedDiscounts += $order->discount;

                if ($order->is_drop_shipping) {
                    $dropShippingOrdersCount++;
                    $dropShippingOrdersAmount += $order->price_total;
                }

                if (!$order->is_drop_shipping && $order->card_payed > 0) {
                    $this->_countPaidQuickly++;
                    $this->_amountPaidQuickly += $order->card_payed;
                }
            }

            if ($order->processing_status == OrderRecord::PROCESSING_CANCELLED
                && strpos($order->callcenter_comment, 'Отменено. По причине объединения с заказом №') !== false
            ) {
                $countMerged++;
            }

            //add statistic offer
            $offerId = isset($orderArray['user.offer_id']) ? $orderArray['user.offer_id'] : $order->offer_id;
            $offerProvider = isset($orderArray['user.offer_provider']) ? $orderArray['user.offer_provider'] : $order->offer_provider;
            $offer = isset($this->_userOffersStorage[$offerId])
                ? $this->_userOffersStorage[$offerId] : new StatisticOrdersUserOffersModel($offerId, $offerProvider);
            $offer->addOrder($order);
            $this->_userOffersStorage[$offerId] = $offer;
        }

        $queue = array_chunk($this->_ordersData, 400);
        foreach ($queue as $part) {
            $ids = ArrayUtils::getColumn($part, 'id');
            $usersId = ArrayUtils::getColumn($part, 'userID');

            if (count($usersId) > 0) {
                $emails = UserRecord::model()->idIn($usersId)->cache(60)->findColumnDistinct('email');
                $this->_userEmails += array_combine($emails, $emails);
            }

            /** @var  OrderProductRecord $productsSelect */
            $productsSelect = OrderProductRecord::model()
                ->with('product', 'event', 'event.brandManager', 'order')
                ->orderIds($ids)
                ->callcenterStatuses([OrderProductRecord::CALLCENTER_STATUS_ACCEPTED]);

            $productsSelectBrandManagers = $productsSelect->copy();
            $products = $productsSelectBrandManagers->findAll();
            $groupBrands = ArrayUtils::groupBy($products, 'event.brand_manager_id');

            foreach ($groupBrands as $brandId => $products) {
                //add statistic brand manager
                $brandManager = isset($this->_brandsManagerStorage[$brandId])
                    ? $this->_brandsManagerStorage[$brandId] : new StatisticOrdersBrandsManagersModel($brandId);

                $brandManager->addProducts($products);
                $this->_brandsManagerStorage[$brandId] = $brandManager;
            }

            $productsSelectProfit = $productsSelect->copy();
            $productsSelectProfit->getDbCriteria()->select = 'SUM((t.price - product.price_purchase)*t.number) as profit';
            $productsSelectProfit->getDbCriteria()->addCondition('order.is_drop_shipping=0');
            $row = $productsSelectProfit->getSqlCommand()->query()->read();

            if (!$row) {
                throw new CHttpException(400, 'Ошибка в расчетах маржи, среднее кол-во единиц в заказе!!!');
            }

            $profit += Cast::toFloat($row['profit']);

            $productsSelect->copy();
            $productsSelect->getDbCriteria()->select = 'SUM((t.price - product.price_purchase)*t.number) as sum, COUNT(t.id) as counts';
            $row = $productsSelect->getSqlCommand()->query()->read();

            if (!$row) {
                throw new CHttpException(400, 'Ошибка в расчетах маржи, среднее кол-во единиц в заказе!!!');
            }

            $countProductsConfirmed += Cast::toUInt($row['counts']);

            //возвраты
            $ordersReturnsSelector = OrderReturnRecord::model()
                ->orderIdIn($ids);

            $ordersReturnsSelector->getDbCriteria()->select = 'SUM(t.price_products) as products, SUM(t.exps_shop) as expensesTransportation';
            $row = $ordersReturnsSelector->getSqlCommand()->query()->read();

            if (!$row) {
                throw new CHttpException(400, 'Ошибка в расчетах убытков связанные с возвратами заказов!!!');
            }

            $this->_returnOrdersProducts += Cast::toFloat($row['products']);
            $this->_expensesShopFromOrder += Cast::toFloat($row['expensesTransportation']);
        }

        natcasesort($this->_userEmails);
        $this->_countMerged = $countMerged;
        $this->_usedBonuses = $usedBonuses;
        $this->_productsTotalConfirmed = $countProductsConfirmed;
        $this->_priceTotal = $priceTotal;
        $this->_priceTotalUnconfirmedIncludes = $priceTotalUnconfirmedIncludes;
        $this->_countCanceled = $countCanceled;
        $this->_countConfirmed = $countConfirmed;
        $this->_countNotConfirmed = $countNotConfirmed;
        $this->_count = $count;
        $this->_profit = $profit;
        $this->_usedDiscounts = $usedDiscounts;
        $this->_netProfit = $profit - $usedBonuses - $usedDiscounts;
        $this->_amountDropShipping = $dropShippingOrdersAmount;
        $this->_countDropShipping = $dropShippingOrdersCount;
    }

    protected function _countingProfitDropShipping()
    {
        //доставленные заказы для расчёта комиссии по дропшиппингу
        $orderTrackingSelector = OrderRecord::model()
            ->paidCommissionAtGreater($this->getDateTimeFrom()->getTimestamp())
            ->paidCommissionAtLower($this->getDateTimeTo()->getTimestamp())
            ->processingStatuses([OrderRecord::PROCESSING_STOREKEEPER_MAILED]);

        $orderTrackingSelector->getDbCriteria()->select = 'GROUP_CONCAT(t.id) AS orders, SUM(t.price_total) as total, SUM(t.drop_shipping_commission) as commission';
        $rowOrderTracking = $orderTrackingSelector->getSqlCommand()->query()->read();

        if (!$rowOrderTracking) {
            throw new CHttpException(400, 'Ошибка в расчетах комиссии по дропшиппингу!!!');
        }

        if ($rowOrderTracking['total'] > 0) {
            $this->_amountDropShippingOrdersForCommission = $rowOrderTracking['total'];
        }

        if ($rowOrderTracking['commission'] > 0) {
            $this->_amountDropShippingOrdersCommission = $rowOrderTracking['commission'];
        }

        if ($rowOrderTracking['orders']) {
            $this->_dropShippingOrdersForCommission = explode(',', $rowOrderTracking['orders']);
        }
    }

    protected function _countingProfitOwn()
    {
        //заказы которые оплатили
        $orders = [];
        //доход
        $realProfit = 0.0;
        $realUsedBonuses = 0.0;
        $realUsedDiscounts = 0.0;

        //возвраты заказов
        $ordersReturnsSelector = OrderReturnRecord::model()
            ->createdAtBetween($this->getDateTimeFrom()->getTimestamp(), $this->getDateTimeTo()->getTimestamp());

        $ordersReturnsSelector->getDbCriteria()->select = 'SUM(t.price_products) as products, SUM(t.exps_shop) as expensesTransportation';
        $row = $ordersReturnsSelector->getSqlCommand()->query()->read();

        if (!$row) {
            throw new CHttpException(400, 'Ошибка в расчетах убытков связанные с возвратами заказов!!!');
        }

        $productsReturn = Cast::toFloat($row['products']);
        $expensesShop = Cast::toFloat($row['expensesTransportation']);

        //возвраты оплаченные картой
        $orderBankRefundSelector = OrderBankRefundRecord::model()
            ->with(['order' => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => [
                    'isDropShipping' => false,
                ],
            ]])
            ->statusChangedAtAtGreater($this->getDateTimeFrom()->getTimestamp())
            ->statusChangedAtAtLower($this->getDateTimeTo()->getTimestamp())
            ->status(OrderBankRefundRecord::STATUS_PAYED);

        $orderBankRefundSelector->getDbCriteria()->select = 'SUM(t.price_refund) as total_refund';
        $row = $orderBankRefundSelector->getSqlCommand()->query()->read();

        if (!$row) {
            throw new CHttpException(400, 'Ошибка в расчетах возврата денежных средств оплеченных картой!!!');
        }

        $totalBankRefunds = Cast::toFloat($row['total_refund']);

        //оплаты
        $orderBuyoutSelector = OrderBuyoutRecord::model()
            ->statusChangedAtGreater($this->getDateTimeFrom()->getTimestamp())
            ->statusChangedAtLower($this->getDateTimeTo()->getTimestamp())
            ->status(OrderBuyoutRecord::STATUS_PAYED);

        $orderBuyoutSelector->getDbCriteria()->select = 'GROUP_CONCAT(t.order_id) AS orders, SUM(t.price) as total';
        $row = $orderBuyoutSelector->getSqlCommand()->query()->read();

        if (!$row) {
            throw new CHttpException(400, 'Ошибка в расчетах оплаченных заказов!!!');
        }

        $orders = array_merge($orders, explode(',', $row['orders']));
        $payed = Cast::toFloat($row['total']);

        //оплаты картой
        $payGatewaySelector = PayGatewayRecord::model()
            ->with(['order' => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => [
                    'isDropShipping' => false,
                ],
            ]])
            ->createdAtGreater($this->getDateTimeFrom()->getTimestamp())
            ->createdAtLower($this->getDateTimeTo()->getTimestamp());

        $payGatewaySelector->getDbCriteria()->select = 'GROUP_CONCAT(t.invoice_id) AS orders, SUM(t.amount) as total';
        $rowPayGateway = $payGatewaySelector->getSqlCommand()->query()->read();

        if (!$rowPayGateway) {
            throw new CHttpException(400, 'Ошибка в расчетах оплаченных заказов!!!');
        }

        $ordersBankPayedId = explode(',', $rowPayGateway['orders']);
        $orders = array_merge($orders, $ordersBankPayedId);
        $bankPayed = Cast::toFloat($rowPayGateway['total']);

        /** MYSQL FUNCTION "IN" SUPPORT ONLY 1000 ITEMS */
        $queueOrders = array_chunk($orders, 2000);
        foreach ($queueOrders as $ids) {
            //маржа
            /** @var  OrderProductRecord $productsSelect */
            $productsSelect = OrderProductRecord::model()
                ->with('product', 'order')
                ->orderIds($ids)
                ->callcenterStatuses([OrderProductRecord::CALLCENTER_STATUS_ACCEPTED]);

            $productsSelect->getDbCriteria()->select = 'SUM((t.price - product.price_purchase)*t.number) as profit';
            $productsSelect->getDbCriteria()->addCondition('order.is_drop_shipping=0');
            $row = $productsSelect->getSqlCommand()->query()->read();

            if (!$row) {
                throw new CHttpException(400, 'Ошибка в расчетах маржи, среднее кол-во единиц в заказе!!!');
            }

            $realProfit += Cast::toFloat($row['profit']);

            //скидки
            $ordersSelect = OrderRecord::model()->idIn($ids);

            $ordersSelect->getDbCriteria()->select = 'SUM(t.bonuses) as bonuses, SUM(t.discount) as discounts';
            $row = $ordersSelect->getSqlCommand()->query()->read();

            if (!$row) {
                throw new CHttpException(400, 'Ошибка в расчетах маржи, среднее кол-во единиц в заказе!!!');
            }

            $realUsedBonuses += Cast::toFloat($row['bonuses']);
            $realUsedDiscounts += Cast::toFloat($row['discounts']);
        }

        $this->_realUsedBonuses = $realUsedBonuses;
        $this->_realUsedDiscount = $realUsedDiscounts;
        $this->_realProfit = $realProfit;
        $this->_expensesShop = $expensesShop + $totalBankRefunds;
        $this->_returnProducts = $productsReturn;

        $this->_paid->amount = $payed + $bankPayed;
        $this->_paid->cardsAmount = $bankPayed;
    }

    /**
     * Данные для получения прибыли
     *
     * @throws CHttpException
     */
    protected function _countingProfit()
    {
        switch ($this->_orderType) {
            case self::ORDER_TYPE_OWN:
                $this->_countingProfitOwn();
                break;

            case self::ORDER_TYPE_DROP_SHIPPING:
                $this->_countingProfitDropShipping();
                break;

            default:
                $this->_countingProfitOwn();
                $this->_countingProfitDropShipping();
        }
    }
}
