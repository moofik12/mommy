<?php

namespace MommyCom\Model\Statistic;

use CException;
use CMap;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Statistic\StatisticModelAbstract;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class StatisticUsersModel
 *
 * @property-read int $count
 * @property-read int $countCreatedOrganic
 * @property-read int $countCreatedOffer
 * @property-read int $countEmailVerified
 * @property-read int $countCreatedOrganicEmailVerified
 * @property-read int $countCreatedOfferEmailVerified
 * @property-read int $countMovedSearchEngine
 * @property-read array $statisticCreatedOffers
 * @property-read array $statisticCreatedOffersEmailVerified
 * @property-read float $balance
 * @property-read float $countReceivedBonusesWhenRegistering
 * @property-read float $totalReceivedBonusesWhenRegistering
 * @property-read float $totalSpentBonusesWhenRegistering
 * @property-read float $totalBurnedBonusesWhenRegistering
 * @property-read int $countOrdersUsedPresentBonus
 * @property-read float $ordersAmountUsedPresentBonus
 */
class StatisticUsersModel extends StatisticModelAbstract
{
    /**
     * to grid
     *
     * @var
     */
    public $id;

    /**
     * @var integer timestamp начало дня
     */
    public $day;

    /**
     * @var string
     */
    const MESSAGE_BONUS_WHEN_REGISTERING = 'Приветствуем вас в шоппинг-клубе МАМАМ.UA';

    private $_count = 0;
    private $_countCreatedOrganic = 0;
    private $_countCreatedOffer = 0;
    private $_countEmailVerified = 0;
    private $_countCreatedOrganicEmailVerified = 0;
    private $_countCreatedOfferEmailVerified = 0;
    private $_countMovedSearchEngine = 0;

    private $_userOffersStorage = [];

    /* STATISTIC RECEIVED BONUSES WHEN REGISTERING */
    /**
     * @var float Баланс пользоватлея не включая заблокированные
     */
    private $_balance = 0.0;
    /**
     * @var int кол-во пользователей которые получили бонусы при регистрации
     */
    private $_countReceivedBonusesWhenRegistering = 0;
    /**
     * @var float сумма бонусов которые получили при регистрации
     */
    private $_totalReceivedBonusesWhenRegistering = 0.0;
    /**
     * @var float кол-во бонусов полученные при регистрации которые были потрачены
     */
    private $_totalSpentBonusesWhenRegistering = 0.0;
    /**
     * @var float кол-во списанных бонусов полученные при регистрации
     */
    private $_totalBurnedBonusesWhenRegistering = 0.0;

    /** @var CMap */
    private $__detailReceivedBonusesWhenRegistering;

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffers = [];

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffersEmailVerified = [];

    private $__promocodeModel = null;

    /** @var int */
    private $_countOrdersUsedPresentBonus = 0;

    /** @var float */
    private $_ordersAmountUsedPresentBonus = 0.0;

    /** @var array */
    private $_userEmails = [];

    /**
     * @param array $data
     *
     * @return null
     */
    public function convertData(array $data)
    {
        $this->_counting($data);
    }

    /**
     * Инициализация, иные вычисления на основе дат
     */
    protected function afterCreate()
    {
        $this->__detailReceivedBonusesWhenRegistering = new CMap();
        $this->id = $this->day = $this->dateTimeFrom->getTimestamp();
    }

    /**
     * @return array
     */
    public function getUserEmails()
    {
        return $this->_userEmails;
    }

    /**
     * @return StatisticUsersModelBonusWhenRegistering[]
     */
    public function getDetailReceivedBonusesWhenRegistering()
    {
        return $this->__detailReceivedBonusesWhenRegistering->toArray();
    }

    /**
     * @param array $filterOfferProvider array(UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN)
     *
     * @return array
     */
    public function getUserOffersHistory(array $filterOfferProvider = [])
    {
        $filters = array_filter($this->_userOffersStorage, function ($offer) use ($filterOfferProvider) {
            /** @var  StatisticOrdersUserOffersModel $offer */
            return in_array($offer->offerProvider, $filterOfferProvider) || empty($filterOfferProvider);
        });
        return array_values($filters);
    }

    /**
     * вычисления
     *
     * @param array $users
     */
    protected function _counting(array $users)
    {
        $ordersId = [];

        $countCreatedOrganic = $this->_countCreatedOrganic;
        $countCreatedOffer = $this->_countCreatedOffer;
        $count = $this->_count + count($users);
        $countEmailVerified = $this->_countEmailVerified;
        $countCreatedOrganicEmailVerified = $this->_countCreatedOrganicEmailVerified;
        $countCreatedOfferEmailVerified = $this->_countCreatedOfferEmailVerified;
        $countMovedSearchEngine = $this->_countMovedSearchEngine;

        /** @var $users UserRecord[] object */
        foreach ($users as $user) {
            if (is_array($user)) {
                $user = (object)$user;
            }

            $offer = isset($this->_userOffersStorage[$user->offer_id])
                ? $this->_userOffersStorage[$user->offer_id] : new StatisticUsersOfferModel($user->offer_id, $user->offer_provider);

            if ($user->is_email_verified) {
                $countEmailVerified++;

                if ($user->offer_provider == UserRecord::OFFER_PROVIDER_NONE) {
                    $countCreatedOrganicEmailVerified++;
                } else {
                    $countCreatedOfferEmailVerified++;
                    $this->_statisticOfferEmailVerifedCounters($user->offer_provider, 1);
                }
            }

            if ($user->offer_provider == UserRecord::OFFER_PROVIDER_NONE) {
                $countCreatedOrganic++;
            } else {
                $countCreatedOffer++;
                $this->_statisticOfferCounters($user->offer_provider, 1);
            }

            if ($user->moved_search_engine) {
                $countMovedSearchEngine++;
            }

            $offer->addUser($user);
            $this->_userOffersStorage[$user->offer_id] = $offer;

            $findOrdersId = $this->_countingUserBonusWhenRegistering($user->id);

            if ($findOrdersId) {
                $ordersId = $ordersId + $findOrdersId;
            }
        }

        $usersEmail = ArrayUtils::getColumn($users, 'email');
        $this->_userEmails += array_combine($usersEmail, $usersEmail);
        natcasesort($this->_userEmails);

        //обновление данных
        if ($ordersId) {
            $ordersDataSelector = OrderRecord::model()
                ->idIn($ordersId);
            $ordersDataSelector->getDbCriteria()->select = 'SUM(price_total) as sum, COUNT(t.id) as counts';
            $row = $ordersDataSelector->getSqlCommand()->query()->read();

            $this->_countOrdersUsedPresentBonus += Cast::toUInt($row['counts']);
            $this->_ordersAmountUsedPresentBonus += Cast::toUFloat($row['sum']);
        }

        $this->_updatePromocodeStatistic($users);

        $this->_countCreatedOffer = $countCreatedOffer;
        $this->_countCreatedOrganic = $countCreatedOrganic;
        $this->_count = $count;
        $this->_countCreatedOfferEmailVerified = $countCreatedOfferEmailVerified;
        $this->_countCreatedOrganicEmailVerified = $countCreatedOrganicEmailVerified;
        $this->_countEmailVerified = $countEmailVerified;
        $this->_countMovedSearchEngine = $countMovedSearchEngine;
    }

    private function _statisticOfferCounters($offerProvider, $count)
    {
        if (!isset($this->_statisticCreatedOffers[$offerProvider])) {
            $this->_statisticCreatedOffers[$offerProvider] = 0;
        }

        $this->_statisticCreatedOffers[$offerProvider] += $count;
    }

    private function _statisticOfferEmailVerifedCounters($offerProvider, $count)
    {
        if (!isset($this->_statisticCreatedOffersEmailVerified[$offerProvider])) {
            $this->_statisticCreatedOffersEmailVerified[$offerProvider] = 0;
        }

        $this->_statisticCreatedOffersEmailVerified[$offerProvider] += $count;
    }

    private function _updatePromocodeStatistic(array $users)
    {
        $promocodes = ArrayUtils::getColumn($users, 'present_promocode');
        $promocodes = array_filter($promocodes);

        $model = $this->getPromocodesHelper();

        foreach ($promocodes as $promocode) {
            $model->addPromocode($promocode);
        }

        if ($promocodes) {
            $ordersSelect = OrderRecord::model()
                ->processingStatusNotCancelled()
                ->promocodeIn($promocodes);

            $tableAlias = $ordersSelect->tableAlias;
            $ordersSelect->getDbCriteria()->select = "$tableAlias.price_total, $tableAlias.promocode, $tableAlias.discount";
            $rows = $ordersSelect->getSqlCommand()->queryAll();

            foreach ($rows as $row) {
                $model->addOrderData($row['promocode'], $row['price_total'], $row['discount']);
            }
        }
    }

    /**
     * @param int $userID
     *
     * @return array[int] list of orders id
     * @throws CException
     */
    private function _countingUserBonusWhenRegistering($userID)
    {
        /* @var UserBonuspointsRecord[] $points */
        $points = UserBonuspointsRecord::model()->userId($userID)->orderBy('created_at', 'desc')->findAll();

        $balance = 0.0;
        $bonusesWhenRegistering = 0;
        $bonusWhenRegistering = 0; //по идее должен получить бонус 1 раз
        $burnedBonuses = 0.0;
        $spendBonuses = 0.0;
        $countReceivedBonusesWhenRegistering = 0.0;
        $history = $this->__detailReceivedBonusesWhenRegistering;
        $ordersId = [];

        foreach ($points as $point) {
            if ($point->message == self::MESSAGE_BONUS_WHEN_REGISTERING) {
                $bonusesWhenRegistering += $point->points;
                $bonusWhenRegistering = $point->points;
                $countReceivedBonusesWhenRegistering++;
            } elseif ($point->message == 'Списание недействительных бонусов') {
                $burnedBonuses += abs($point->points);
            } elseif (mb_stripos($point->message, 'Оплата заказа') !== false) {
                $spendBonuses += abs($point->points);
                preg_match('/\d*$/', $point->message, $ordersFind);
                if (isset($ordersFind[0])) {
                    $findOrder = (int)$ordersFind[0];
                    $ordersId[$findOrder] = $findOrder;
                }
            }

            $balance += $point->points;
        }

        $burnedBonusesWhenRegistering = min($bonusesWhenRegistering, $burnedBonuses);
        $spentBonusesWhenRegistering = min($bonusesWhenRegistering, $spendBonuses);

        if ($bonusWhenRegistering > 0) {
            $historyItem = $history->contains($bonusWhenRegistering) ? $history->itemAt($bonusWhenRegistering)
                : StatisticUsersModelBonusWhenRegistering::create($bonusWhenRegistering);

            $historyItem->number += $countReceivedBonusesWhenRegistering;
            $historyItem->burned += $burnedBonusesWhenRegistering;
            $historyItem->spent += $spentBonusesWhenRegistering;

            $history->add($bonusWhenRegistering, $historyItem);
        }

        $this->_balance += $balance;
        $this->_totalReceivedBonusesWhenRegistering += $bonusesWhenRegistering;
        $this->_totalBurnedBonusesWhenRegistering += $burnedBonusesWhenRegistering;
        $this->_totalSpentBonusesWhenRegistering += $spentBonusesWhenRegistering;
        $this->_countReceivedBonusesWhenRegistering += $countReceivedBonusesWhenRegistering;

        return $ordersId;
    }

    /**
     * @param $name
     *
     * @return mixed
     * @throws CException
     */
    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        throw new CException(\Yii::t('yii', 'Property "{class}.{property}" is not defined.',
            ['{class}' => get_class($this), '{property}' => $name]));
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOffer($offerProvider)
    {
        return isset($this->_statisticCreatedOffers[$offerProvider]) ? $this->_statisticCreatedOffers[$offerProvider] : 0;
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOfferEmailVerified($offerProvider)
    {
        return isset($this->_statisticCreatedOffersEmailVerified[$offerProvider])
            ? $this->_statisticCreatedOffersEmailVerified[$offerProvider] : 0;
    }

    /**
     * @return null|StatisticUsersModelPromocodesWhenRegistering
     */
    public function getPromocodesHelper()
    {
        if ($this->__promocodeModel === null) {
            $this->__promocodeModel = new StatisticUsersModelPromocodesWhenRegistering();
        }

        return $this->__promocodeModel;
    }
}
