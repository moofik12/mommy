<?php

namespace MommyCom\Model\Statistic;

use CModel;

/**
 * Class StatisticOrdersBuyoutBaseModel
 *
 * @property-read int $countSold
 * @property-read float $amountSold
 * @property-read int $countReturned
 * @property-read float $amountReturned
 * @property-read int $countNotBought
 * @property-read float $amountNotBought
 * @property-read float $ratioSold
 * @property-read float $ratioReturned
 * @property-read float $ratioNotBought
 * @property-read float $ratioSoldAmount
 */
abstract class StatisticOrdersBuyoutBaseModel extends CModel
{
    public $id;

    /** @var StatisticOrdersBuyoutProductModel[] */
    protected $_products = [];

    /**
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = intval($id);
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return ['countSold', 'amountSold', 'countReturned', 'amountReturned', 'countNotBought', 'amountNotBought'];
    }

    /**
     * @return StatisticOrdersBuyoutProductModel[]
     */
    public function getProducts()
    {
        return array_values($this->_products);
    }

    /**
     * Кол-во проданных товаров
     *
     * @return mixed
     */
    public function getCountSold()
    {
        return array_reduce($this->getProducts(), function ($carry, $item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            return $carry + $item->count;
        }, 0);
    }

    /**
     * Сумма проданных товаров
     *
     * @return mixed
     */
    public function getAmountSold()
    {
        return array_reduce($this->getProducts(), function ($carry, $item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            return $carry + $item->count * $item->price;
        }, 0);
    }

    /**
     * Кол-во товаров которые вернули
     *
     * @return mixed
     */
    public function getCountReturned()
    {
        return array_reduce($this->getProducts(), function ($carry, $item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            return $carry + $item->countReturned;
        }, 0);
    }

    /**
     * Сумма товаров которые вернули
     *
     * @return mixed
     */
    public function getAmountReturned()
    {
        return array_reduce($this->getProducts(), function ($carry, $item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            return $carry + $item->countReturned * $item->price;
        }, 0);
    }

    /**
     * Кол-во товаров которые невыкупили
     *
     * @return mixed
     */
    public function getCountNotBought()
    {
        return array_reduce($this->getProducts(), function ($carry, $item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            return $carry + $item->countNotBought;
        }, 0);
    }

    /**
     * Сумма товаров которые невыкупили
     *
     * @return mixed
     */
    public function getAmountNotBought()
    {
        return array_reduce($this->getProducts(), function ($carry, $item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            return $carry + $item->countNotBought * $item->price;
        }, 0);
    }

    /**
     * @return float|int
     */
    public function getRatioSold()
    {
        $sold = $this->getCountSold();

        if ($sold > 0) {
            return round(100 - (($this->countReturned + $this->countNotBought) / $sold) * 100);
        }

        return 100;
    }

    /**
     * % выкупа в денежном эквиваленте
     *
     * @return float|int
     */
    public function getRatioSoldAmount()
    {
        $sold = $this->getAmountSold();

        if ($sold > 0) {
            return round(100 - (($this->amountReturned + $this->amountNotBought) / $sold) * 100);
        }

        return 100;
    }

    /**
     * @return float|int
     */
    public function getRatioReturned()
    {
        $sold = $this->getCountSold();

        if ($sold > 0) {
            return round(($this->countReturned / $sold) * 100);
        }

        return 0;
    }

    /**
     * @return float|int
     */
    public function getRatioNotBought()
    {
        $sold = $this->getCountSold();

        if ($sold > 0) {
            return round(($this->countNotBought / $sold) * 100);
        }

        return 0;
    }

    /**
     * @param int $productID
     *
     * @return null|StatisticOrdersBuyoutProductModel
     */
    public function getProduct($productID)
    {
        if (isset($this->_products[$productID])) {
            return $this->_products[$productID];
        }

        return null;
    }

    /**
     * @return StatisticOrdersBuyoutProductModel[]
     */
    public function getReturnedProducts()
    {
        return array_filter($this->getProducts(), function ($item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            if ($item->countReturned > 0) {
                return true;
            }

            return false;
        });
    }

    /**
     * @return StatisticOrdersBuyoutProductModel[]
     */
    public function getNotBoughtProducts()
    {
        return array_filter($this->getProducts(), function ($item) {
            /* @var StatisticOrdersBuyoutProductModel $item */
            if ($item->countNotBought > 0) {
                return true;
            }

            return false;
        });
    }

    /**
     * @param StatisticOrdersBuyoutProductModel $model
     */
    public function setProduct(StatisticOrdersBuyoutProductModel $model)
    {
        $this->_products[$model->productID] = $model;
    }

    /**
     * @param StatisticOrdersBuyoutProductModel $model
     */
    public function removeProduct(StatisticOrdersBuyoutProductModel $model)
    {
        if (isset($this->_products[$model->productID])) {
            $storageModel = $this->_products[$model->productID];

            $storageModel->count -= $model->count;
            $storageModel->countReturned -= $model->countReturned;
            $storageModel->countNotBought -= $model->countNotBought;
            /*
            if ($storageModel->count <= 0) {
                unset($this->_products[$model->productID]);
            }
            */
        }
    }
}
