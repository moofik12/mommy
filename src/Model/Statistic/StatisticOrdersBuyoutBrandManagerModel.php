<?php

namespace MommyCom\Model\Statistic;

/**
 * Class StatisticOrdersBuyoutBrandManagerModel
 */
class StatisticOrdersBuyoutBrandManagerModel extends StatisticOrdersBuyoutBaseModel
{
    /** @var  string */
    public $fullname;

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array_merge(parent::attributeNames(), ['brandManagerName']);
    }
}
