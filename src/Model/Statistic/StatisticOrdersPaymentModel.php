<?php

namespace MommyCom\Model\Statistic;

use CModel;

/**
 * Class StatisticOrdersPaymentModel
 */
class StatisticOrdersPaymentModel extends CModel
{
    public $id;

    /** @var float */
    public $courierSenderConfirm = 0.0;

    /** @var float */
    public $courierSender = 0.0;

    /** @var float */
    public $novaPostaSenderConfirm = 0.0;

    /** @var float */
    public $novaPostaSender = 0.0;

    /** @var float */
    public $cardsAmount = 0.0;

    /** @var  float сумма */
    public $amount = 0.0;

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return [];
    }

}
