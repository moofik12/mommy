<?php

namespace MommyCom\Model\Statistic;

use ArrayObject;

final class StatisticUsersModelBonusWhenRegistering extends ArrayObject
{
    /**
     * @var float
     * Кло-во бонусов (тип бонуса)
     */
    public $points = 0.0;

    /**
     * @var int
     * Кол-во пользователей
     */
    public $number = 0;

    /**
     * @var float
     * Кол-во списанных бонусов
     */
    public $burned = 0.0;

    /**
     * @var float
     * Кол-во потраченных
     */
    public $spent = 0.0;

    /**
     * @param float $points
     *
     * @return StatisticUsersModelBonusWhenRegistering
     */
    public static function create($points = 0.0)
    {
        $self = new self();
        $self->points = $points;

        return $self;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->points * $this->number;
    }
}
