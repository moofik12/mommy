<?php

namespace MommyCom\Model\Statistic;

use CHttpException;
use MommyCom\Model\Db\MoneyReceiveStatementOrderRecord;
use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;

class StatisticOrdersPaidModel extends StatisticOrdersModel
{
    /**
     * Данные для получения прибыли
     *
     * @throws CHttpException
     */
    protected function _countingProfit()
    {
        parent::_countingProfit();

        //заказы которые оплатили
        $orders = [];

        //оплаты
        $orderBuyoutSelector = OrderBuyoutRecord::model()
            ->statusChangedAtGreater($this->getDateTimeFrom()->getTimestamp())
            ->statusChangedAtLower($this->getDateTimeTo()->getTimestamp())
            ->status(OrderBuyoutRecord::STATUS_PAYED);

        $orderBuyoutSelector->getDbCriteria()->select = 'GROUP_CONCAT(t.order_id) AS orders';
        $row = $orderBuyoutSelector->getSqlCommand()->query()->read();

        if ($row) {
            $orders = array_merge($orders, explode(',', $row['orders']));
        }

        /** MYSQL FUNCTION "IN" SUPPORT ONLY 1000 ITEMS */
        $queueOrders = array_chunk($orders, 2000);
        foreach ($queueOrders as $ids) {
            //сделаны оплаты
            $ordersSelect = OrderRecord::model()
                ->with([
                    'buyout' => ['together' => true],
                ])
                ->idIn($ids);

            $ordersSelect->getDbCriteria()->select = 'SUM(buyout.price) as amount, t.delivery_type as deliveryType';
            $ordersSelect->getDbCriteria()->group = 'delivery_type';
            $rows = $ordersSelect->getSqlCommand()->query()->readAll();

            foreach ($rows as $row) {
                if ($row['deliveryType'] == DeliveryTypeUa::DELIVERY_TYPE_COURIER) {
                    $this->_paid->courierSender += $row['amount'];
                } elseif ($row['deliveryType'] == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA) {
                    $this->_paid->novaPostaSender += $row['amount'];
                }
            }

            //подтверждены оплаты
            $ordersSelect = MoneyReceiveStatementOrderRecord::model()
                ->with([
                    'statement' => ['together' => true],
                ])
                ->orderIdIn($ids);

            $ordersSelect->getDbCriteria()->select = 'SUM(t.order_price) as amount, statement.delivery_type as deliveryType';
            $ordersSelect->getDbCriteria()->group = 'statement.delivery_type';
            $rows = $ordersSelect->getSqlCommand()->query()->readAll();

            foreach ($rows as $row) {
                if ($row['deliveryType'] == DeliveryTypeUa::DELIVERY_TYPE_COURIER) {
                    $this->_paid->courierSenderConfirm += $row['amount'];
                } elseif ($row['deliveryType'] == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA) {
                    $this->_paid->novaPostaSenderConfirm += $row['amount'];
                }
            }
        }
    }
}
