<?php

namespace MommyCom\Model\Statistic;

/**
 * Class UserPartnerOrderModel
 */
class UserPartnerOrderModel
{
    public $id;
    /**
     * @var int
     */
    public $startTime;
    /**
     * @var int
     */
    public $endTime;
    /**
     * @var int
     */
    public $countRegPartnerInvitedUser = 0;
    /**
     * @var int
     */
    public $countOrder = 0;
    /**
     * @var int
     */
    public $sumOrder = 0;
    /**
     * @var int
     */
    public $countProduct = 0;
    /**
     * @var int
     */
    public $countOutput = 0;
    /**
     * @var int
     */
    public $sumOutput = 0;
    /**
     * @var int
     */
    public $countConfirmedOutput = 0;
    /**
     * @var int
     */
    public $sumConfirmedOutput = 0;
    /**
     * @var int
     */
    public $countUsedPromocode = 0;
    /**
     * @var int
     */
    public $commonCount = 0;
    public $admissions = [];

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'day' => 'Дата',
            'countRegPartnerInvitedUser' => 'Регистраций',
            'countOrder' => 'Кол-во заказов',
            'sumOrder' => 'Сумма заказов',
            'countProduct' => 'Кол-во товаров',
            'countOutput' => 'Кол-во потенциальных выплат',
            'sumOutput' => 'Потенциальная сумма',
            'countConfirmedOutput' => 'Кол-во подтвержденных выплат',
            'sumConfirmedOutput' => 'Подтвержденная сумма',
            'countUsedPromocode' => 'Использовано промокодов',
            'admissions' => 'Начисления подробно',
        ];
    }

    public function getCommonCount()
    {
        $this->commonCount = $this->countRegPartnerInvitedUser + $this->countOrder + $this->countOutput;
        return $this->commonCount;
    }
}
