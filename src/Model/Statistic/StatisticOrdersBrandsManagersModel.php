<?php

namespace MommyCom\Model\Statistic;

use CModel;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class StatisticOrdersBrandsManagersModel
 *
 * @property-read string $displayName
 */
class StatisticOrdersBrandsManagersModel extends CModel
{
    /** @var  string brand manager id */
    public $id;

    /** @var  string */
    public $login = 'нет';

    /** @var string ФИО */
    public $fullname;

    /** @var int кол-во товаров */
    public $countProducts = 0;

    /** @var  float сумма */
    public $amount = 0.0;

    /** @var float прибыль (возможная) */
    public $netProfit = 0.0;

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return [];
    }

    public function __construct($brandManagerId)
    {
        $this->id = $brandManagerId;
        /** @var AdminUserRecord|null $model */
        $model = AdminUserRecord::model()->cache(300)->findByPk($brandManagerId);//ничего страшного

        if ($model) {
            $this->login = $model->login;
            $this->fullname = $model->fullname;
        }
    }

    /**
     * @param array OrderProductRecord[]
     */
    public function addProducts(array $products)
    {
        /** @var OrderProductRecord[] $products */
        foreach ($products as $product) {
            $this->amount += $product->price * $product->number;
            $this->netProfit += ($product->price - $product->product->price_purchase) * $product->number;
        }

        $this->countProducts += count($products);
    }

    /**
     * @param int $count
     */
    public function addCountProducts($count)
    {
        $count = Cast::toUInt($count);
        $this->countProducts += $count;
    }

    /**
     * @param float $amount
     */
    public function addNetProfit($amount)
    {
        $amount = Cast::toUFloat($amount);
        $this->netProfit += $amount;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        $text = $this->login;
        if ($this->fullname) {
            $text .= " ({$this->fullname})";
        }

        return $text;
    }
}
