<?php

namespace MommyCom\Model\Statistic;

use CException;
use MommyCom\YiiComponent\Statistic\StatisticModelInterface;
use MommyCom\YiiComponent\Statistic\StatisticTotalModelInterface;

/**
 * StatisticTotalOrdersModel
 *
 * @property-read int $totalAverageProducts;
 * @property-read int $totalAverageCheck;
 * @property-read int $totalAverageCheckUnconfirmedIncludes;
 * @property-read int $totalConfirmed;
 * @property-read int $totalNotConfirmed;
 * @property-read int $totalCanceled;
 * @property-read int $totalMerged;
 * @property-read int $totalPrice;
 * @property-read int $totalPriceUnconfirmedIncludes;
 * @property-read int $totalOrders;
 * @property-read float $percentConfirmed;
 * @property-read float $percentNotConfirmed;
 * @property-read float $percentCanceled;
 * @property-read float $usedBonuses
 * @property-read float $usedDiscounts
 * @property-read float $profit //маржа
 * @property-read float $netProfit //возможная чистая прибыль
 * @property-read float $percentUsedBonuses
 * @property-read float $percentUsedDiscount
 * @property-read float $dropShippingCommission
 * @property-read float $dropShipping
 * @property-read int $countRedeliveryNpCard кол-во заказов которые должны прийти на Р\C от НП
 * @property-read float $amountRedeliveryNpCard сумма заказов которые должны прийти на Р\C от НП
 * @property-read int $countPaidQuickly кол-во заказов которые оплатили сразу
 * @property-read float $amountPaidQuickly сумма заказов которые оплатили сразу
 * @property-read float $percentPaidQuickly
 * @property-read float $percentRedeliveryNpCard
 * @property-read float $returnOrdersProducts
 * @property-read float $expensesShopFromOrders
 * @property-read float $payed
 * @property-read float $realNetProfit //чистая прибыль
 * @property-read float $realUsedDiscounts
 * @property-read float $realUsedBonuses
 * @property-read float $expensesShop
 */
class StatisticTotalOrdersModel implements StatisticTotalModelInterface
{
    /**
     * Statistic only page
     *
     * @var bool
     */
    public $totalOnlyPage = false;

    protected $_totalAverageProducts = 0.0;
    protected $_totalAverageCheck = 0.0;
    protected $_totalAverageCheckUnconfirmedIncludes = 0.0;
    protected $_totalConfirmed = 0;
    protected $_totalNotConfirmed = 0;
    protected $_totalMerged = 0;
    protected $_totalCanceled = 0;
    protected $_totalPrice = 0.0;
    protected $_totalPriceUnconfirmedIncludes = 0.0;
    protected $_totalOrders = 0;
    protected $_usedBonuses = 0.0;
    protected $_usedDiscounts = 0.0;
    protected $_totalProductsConfirmed = 0;
    protected $_countRedeliveryNpCard = 0;
    protected $_amountRedeliveryNpCard = 0.0;
    protected $_countPaidQuickly = 0;
    protected $_amountPaidQuickly = 0.0;

    protected $_percentRedeliveryNpCard = 0.0;
    protected $_percentPaidQuickly = 0.0;

    protected $_percentConfirmed = 0.0;
    protected $_percentNotConfirmed = 0.0;
    protected $_percentCanceled = 0.0;
    protected $_percentUsedBonuses = 0.0;
    protected $_percentUsedDiscount = 0.0;
    protected $_profit = 0.0;
    protected $_netProfit = 0.0; //возможная прибыль

    protected $_realNetProfit = 0.0; //реальная прибыль
    protected $_expensesShop = 0.0; //издержки магазина
    protected $_realUsedBonuses = 0.0;
    protected $_realUsedDiscounts = 0.0;
    protected $_dropShippingCommission = 0.0;
    protected $_dropShipping = 0.0;

    private $_returnOrdersProducts = 0.0;

    /** @var float издержки магазина */
    private $_expensesShopFromOrders = 0.0;

    /** @var float оплачено */
    private $_payed = 0.0;

    /** @var array список ID обектов которые уже есть */
    private $_historyModel = [];

    /**
     * Сan be used many times
     *
     * @param StatisticModelInterface $model
     */
    public function countingModel(StatisticModelInterface $model)
    {
        $this->_counting($model);
    }

    /**
     * give item only visible on page
     *
     * @return bool
     */
    public function isUseOlyPageItem()
    {
        return !!$this->totalOnlyPage;
    }

    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        throw new CException(\Yii::t('yii', 'Property "{class}.{property}" is not defined.',
            ['{class}' => get_class($this), '{property}' => $name]));
    }

    /**
     * @param StatisticOrdersModel $model
     */
    protected function _counting(StatisticOrdersModel $model)
    {
        $totalConfirmed = $this->_totalConfirmed;
        $totalCanceled = $this->_totalCanceled;
        $totalNotConfirmed = $this->_totalNotConfirmed;
        $totalMerged = $this->_totalMerged;
        $totalOrders = $this->_totalOrders;
        $totalPrice = $this->_totalPrice;
        $totalPriceUnconfirmedIncludes = $this->_totalPriceUnconfirmedIncludes;
        $usedBonuses = $this->_usedBonuses;
        $usedDiscounts = $this->_usedDiscounts;
        $profit = $this->_profit;
        $totalProductsConfirmed = $this->_totalProductsConfirmed;

        //ADDED
        $totalPrice += $model->getPrice();
        $totalPriceUnconfirmedIncludes += $model->getPriceUnconfirmedIncludes();
        $totalCanceled += $model->getCountCanceled();
        $totalConfirmed += $model->getCountConfirmed();
        $totalNotConfirmed += $model->getCountNotConfirmed();
        $totalMerged += $model->getCountMerged();
        $totalOrders += $model->count();
        $usedBonuses += $model->getUsedBonuses();
        $profit += $model->getProfit();
        $usedDiscounts += $model->getUsedDiscounts();
        $totalProductsConfirmed += $model->getProductsTotalConfirmed();

        $this->_countPaidQuickly += $model->getCountPaidQuickly();
        $this->_amountPaidQuickly += $model->getAmountPaidQuickly();
        $this->_countRedeliveryNpCard += $model->getCountRedeliveryNpCard();
        $this->_amountRedeliveryNpCard += $model->getAmountRedeliveryNpCard();
        $this->_dropShippingCommission += $model->getDropShippingCommission();
        $this->_dropShipping += $model->getAmountDropShipping();

        //COUNTING
        $totalOrdersForPercent = $totalOrders > 0 ? $totalOrders : 1;

        $this->_percentPaidQuickly = ($this->_countPaidQuickly / $totalOrdersForPercent) * 100;
        $this->_percentRedeliveryNpCard = ($this->_countRedeliveryNpCard / $totalOrdersForPercent) * 100;

        $percentConfirmed = ($totalConfirmed / $totalOrdersForPercent) * 100;
        $percentCanceled = ($totalCanceled / $totalOrdersForPercent) * 100;
        $percentNotConfirmed = ($totalNotConfirmed / $totalOrdersForPercent) * 100;

        //проверка на 0
        $calculationsTotalPrice = $totalPrice > 0 ? $totalPrice : 1;
        $calculationsTotalConfirmed = $totalConfirmed > 0 ? $totalConfirmed : 1;

        $this->_returnOrdersProducts += $model->getReturnOrdersProducts();
        $this->_expensesShopFromOrders += $model->getExpensesShopFromOrder();

        $this->_totalMerged = $totalMerged;
        $this->_percentUsedBonuses = round(($usedBonuses * 100 / $calculationsTotalPrice), 2);
        $this->_percentUsedDiscount = round(($usedDiscounts * 100 / $calculationsTotalPrice), 2);
        $this->_percentConfirmed = round($percentConfirmed, 1);
        $this->_percentNotConfirmed = round($percentNotConfirmed, 1);
        $this->_percentCanceled = round($percentCanceled, 1);
        $this->_usedBonuses = $usedBonuses;
        $this->_totalPrice = $totalPrice;
        $this->_totalPriceUnconfirmedIncludes = $totalPriceUnconfirmedIncludes;
        $this->_totalOrders = $totalOrders;
        $this->_totalCanceled = $totalCanceled;
        $this->_totalConfirmed = $totalConfirmed;
        $this->_totalNotConfirmed = $totalNotConfirmed;
        $this->_totalAverageProducts = round($totalProductsConfirmed / $calculationsTotalConfirmed, 1);
        $this->_totalAverageCheck = round($totalPrice / $calculationsTotalConfirmed, 2);
        $this->_totalAverageCheckUnconfirmedIncludes = 0;

        if ($totalOrders !== $totalCanceled) {
            round($totalPriceUnconfirmedIncludes / ($totalOrders - $totalCanceled), 2);
        }

        $this->_profit = $profit;
        $this->_usedDiscounts = $usedDiscounts;
        $this->_netProfit = $profit - $usedBonuses - $usedDiscounts;
        $this->_totalProductsConfirmed = $totalProductsConfirmed;

        //свойства которые не пересчитываются
        $modelId = $model->getDateTimeFrom()->getTimestamp() + $model->getDateTimeTo()->getTimestamp();
        if (!in_array($modelId, $this->_historyModel)) {
            $this->_historyModel[] = $modelId;

            $this->_payed += $model->getPaid()->amount;
            $this->_expensesShop += $model->getExpensesShop();
            $this->_realUsedBonuses += $model->getRealUsedBonuses();
            $this->_realUsedDiscounts += $model->getRealUsedDiscount();
            $this->_realNetProfit += $model->getNetRealProfit();
        }
    }
}
