<?php

namespace MommyCom\Model\Statistic;

use CException;
use MommyCom\YiiComponent\Statistic\StatisticModelInterface;
use MommyCom\YiiComponent\Statistic\StatisticTotalModelInterface;

class StatisticTotalUsersModel implements StatisticTotalModelInterface
{
    /**
     * Statistic only page
     *
     * @var bool
     */
    public $totalOnlyPage = false;

    private $_count = 0;
    private $_countEmailVerified = 0;
    private $_countCreatedOrganic = 0;
    private $_countCreatedOffer = 0;
    private $_countCreatedOrganicEmailVerified = 0;
    private $_countCreatedOfferEmailVerified = 0;

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffers = [];

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffersEmailVerified = [];

    /**
     * Сan be used many times
     *
     * @param StatisticModelInterface $model
     */
    public function countingModel(StatisticModelInterface $model)
    {
        $this->_counting($model);
    }

    /**
     * give item only visible on page
     *
     * @return bool
     */
    public function isUseOlyPageItem()
    {
        return !!$this->totalOnlyPage;
    }

    /**
     * @param StatisticUsersModel $statistic
     */
    protected function _counting(StatisticUsersModel $statistic)
    {
        $this->_countCreatedOffer += $statistic->countCreatedOffer;
        $this->_countCreatedOrganic += $statistic->countCreatedOrganic;

        $this->_countCreatedOrganicEmailVerified += $statistic->countCreatedOrganicEmailVerified;
        $this->_countCreatedOfferEmailVerified += $statistic->countCreatedOfferEmailVerified;
        $this->_count += $statistic->count;
        $this->_countEmailVerified += $statistic->countEmailVerified;

        $this->_statisticCreatedOffers = $this->_sumArray($this->_statisticCreatedOffers, $statistic->statisticCreatedOffers);
        $this->_statisticCreatedOffersEmailVerified =
            $this->_sumArray($this->_statisticCreatedOffersEmailVerified, $statistic->statisticCreatedOffersEmailVerified);
    }

    private function _sumArray(array $a, array $b)
    {
        foreach ($b as $key => $value) {
            if (isset($a[$key])) {
                $a[$key] += $value;
            } else {
                $a[$key] = $value;
            }
        }

        return $a;
    }

    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        throw new CException(\Yii::t('yii', 'Property "{class}.{property}" is not defined.',
            ['{class}' => get_class($this), '{property}' => $name]));
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOffer($offerProvider)
    {
        return isset($this->_statisticCreatedOffers[$offerProvider]) ? $this->_statisticCreatedOffers[$offerProvider] : 0;
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOfferEmailVerified($offerProvider)
    {
        return isset($this->_statisticCreatedOffersEmailVerified[$offerProvider])
            ? $this->_statisticCreatedOffersEmailVerified[$offerProvider] : 0;
    }

}
