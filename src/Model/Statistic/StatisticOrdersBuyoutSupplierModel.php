<?php

namespace MommyCom\Model\Statistic;

/**
 * Class StatisticOrdersBuyoutSupplierModel
 *
 * @property-read int $supplierID
 */
class StatisticOrdersBuyoutSupplierModel extends StatisticOrdersBuyoutBaseModel
{
    /** @var  string */
    public $supplierName;

    /**
     * @return int
     */
    public function getSupplierID()
    {
        return $this->id;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array_merge(parent::attributeNames(), ['supplierName', 'supplierID']);
    }
}
