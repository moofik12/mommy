<?php

namespace MommyCom\Model\Statistic;

use CModel;
use MommyCom\Model\Db\OrderRecord;

class StatisticOrdersUserOffersModel extends CModel
{
    /** @var  string offer id */
    public $id;

    public $offerProvider;

    public $ordersConfirmed = 0;

    public $ordersCanceled = 0;

    public $ordersMerged = 0;

    public $ordersNotConfirmed = 0;

    public $totalOrders = 0;

    public $ordersAmount = 0;

    public function __construct($id, $offerProvider)
    {
        $this->id = $id;
        $this->offerProvider = $offerProvider;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return [];
    }

    public function addOrder($order)
    {
        $order = is_array($order) ? (object)$order : $order;

        if (in_array($order->processing_status, [
            OrderRecord::PROCESSING_CANCELLED,
            OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER,
            OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE,
        ])) {
            $this->ordersCanceled++;
        } elseif (in_array($order->processing_status, [
            OrderRecord::PROCESSING_CALLCENTER_CALL_LATER,
            OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
            OrderRecord::PROCESSING_UNMODERATED,
        ])) {
            $this->ordersNotConfirmed++;
        } else {
            $this->ordersConfirmed++;
            $this->ordersAmount += $order->price_total;
        }

        if ($order->processing_status == OrderRecord::PROCESSING_CANCELLED
            && strpos($order->callcenter_comment, 'Отменено. По причине объединения с заказом №') !== false
        ) {
            $this->ordersMerged++;
            $this->ordersCanceled--;
        }

        $this->totalOrders++;
    }
}
