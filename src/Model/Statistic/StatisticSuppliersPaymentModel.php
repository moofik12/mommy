<?php

namespace MommyCom\Model\Statistic;

use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\YiiComponent\Statistic\StatisticModelAbstract;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Модель для агрегирования статистики SupplierPaymentRecord
 *
 * @property-read int $countSuppliers
 * @property-read int $countContracts
 * @property-read int $countEvents
 * @property-read int $countPayments
 * @property-read int $countPaid
 * @property-read float $payAmount
 * @property-read float $requestAmount
 * @property-read float $deliveredAmount
 * @property-read float $deliveredAmountWrongTime
 * @property-read float $returnedAmount
 * @property-read float $penaltyAmount
 * @property-read float $paidAmount
 * @property-read float $customPenaltyAmount
 */
class StatisticSuppliersPaymentModel extends StatisticModelAbstract
{
    /**
     * to grid
     *
     * @var
     */
    public $id;

    /**
     * @var integer timestamp начало дня
     */
    public $day;

    protected $_countSuppliers = 0;
    protected $_countContracts = 0;
    protected $_countEvents = 0;
    protected $_countPaid = 0;
    protected $_countPayments = 0;

    protected $_payAmount = 0.0; //к оплате
    protected $_requestAmount = 0.0; //сумма запроса
    protected $_deliveredAmount = 0.0; //сумма доставленных
    protected $_deliveredAmountWrongTime = 0.0; //сумма доставленных после окончания времен доставки
    protected $_returnedAmount = 0.0; //сумма возвратов
    protected $_penaltyAmount = 0.0; //сумма штрафов
    protected $_paidAmount = 0.0; //сумма оплат
    protected $_customPenaltyAmount = 0.0; //сумма доп. штрафов

    /**
     * Инициализация, иные вычисления на основе дат
     */
    protected function afterCreate()
    {
        $this->id = $this->day = $this->dateTimeFrom->getTimestamp();
    }

    /**
     * Сan be used many times
     *
     * @param array $data
     *
     * @return mixed
     */
    public function convertData(array $data)
    {
        foreach ($data as $model) {
            $this->_countingData($model);
        }
    }

    /**
     * Not support relations
     *
     * @param array|object $model unit data of SupplierPaymentRecord
     */
    private function _countingData($model)
    {
        //unification
        if (is_array($model)) {
            $model = (object)$model;
        }

        /* @var SupplierPaymentRecord $model */
        //counters
        $this->_countPayments++;
        $this->_countContracts++;
        $this->_countSuppliers++;
        $this->_countEvents++;

        if ($model->paid_amount > 0) {
            $this->_countPaid++;
        }

        //amounts
        $this->_requestAmount += Cast::toFloat($model->request_amount);
        $this->_deliveredAmount += Cast::toFloat($model->delivered_amount);
        $this->_deliveredAmountWrongTime += Cast::toFloat($model->delivered_amount_wrong_time);
        $this->_returnedAmount += Cast::toFloat($model->returned_amount);
        $this->_penaltyAmount += Cast::toFloat($model->penalty_amount);
        $this->_paidAmount += Cast::toFloat($model->paid_amount);
        $this->_customPenaltyAmount += Cast::toFloat($model->custom_penalty_amount);

        //TODO вычесления эмулируются так как данные в виде массива
        $this->_payAmount +=
            Cast::toFloat($model->delivered_amount) - $model->returned_amount - $model->penalty_amount - $model->custom_penalty_amount;
    }


    //GETTERS

    /**
     * @return int
     */
    public function getCountPayments()
    {
        return $this->_countPayments;
    }

    /**
     * @return int
     */
    public function getCountPaid()
    {
        return $this->_countPaid;
    }

    /**
     * @return float
     */
    public function getPayAmount()
    {
        return $this->_payAmount;
    }

    /**
     * @return mixed
     */
    public function getCountSuppliers()
    {
        return $this->_countSuppliers;
    }

    /**
     * @return mixed
     */
    public function getCountContracts()
    {
        return $this->_countContracts;
    }

    /**
     * @return mixed
     */
    public function getCountEvents()
    {
        return $this->_countEvents;
    }

    /**
     * @return mixed
     */
    public function getRequestAmount()
    {
        return $this->_requestAmount;
    }

    /**
     * @return mixed
     */
    public function getDeliveredAmount()
    {
        return $this->_deliveredAmount;
    }

    /**
     * @return mixed
     */
    public function getDeliveredAmountWrongTime()
    {
        return $this->_deliveredAmountWrongTime;
    }

    /**
     * @return mixed
     */
    public function getReturnedAmount()
    {
        return $this->_returnedAmount;
    }

    /**
     * @return mixed
     */
    public function getPenaltyAmount()
    {
        return $this->_penaltyAmount;
    }

    /**
     * @return mixed
     */
    public function getPaidAmount()
    {
        return $this->_paidAmount;
    }

    /**
     * @return mixed
     */
    public function getCustomPenaltyAmount()
    {
        return $this->_customPenaltyAmount;
    }

    /**
     * Все оплаты были выплачены
     *
     * @return bool
     */
    public function isFullRequestProcessed()
    {
        return ($this->_countPayments == $this->_countPaid);
    }
}
