<?php

namespace MommyCom\Model\OrderNotification;

use MommyCom\Model\Db\OrderRecord;

class OrderNotificationMessageFactory
{
    public const TYPE_CREATE = 1;

    public const TYPE_CONFIRMED = 2;

    public const TYPE_AUTOMATIC_CONFIRMED = 3;

    public const TYPE_MAILED = 4;

    public const TYPE_PREPAY = 5;

    /**
     * @param OrderRecord $orderRecord
     * @param int $type
     * @return OrderNotificationMessage
     */
    public function createNotification(OrderRecord $orderRecord, int $type): OrderNotificationMessage
    {
        switch ($type) {
            case self::TYPE_CREATE:
                return new OrderNotificationMessageCreate($orderRecord);
                break;
            case self::TYPE_AUTOMATIC_CONFIRMED:
                return new OrderNotificationMessageAutomaticConfirmed($orderRecord);
                break;
            case self::TYPE_CONFIRMED:
                return new OrderNotificationMessageConfirmed($orderRecord);
                break;
            case self::TYPE_MAILED:
                return new OrderNotificationMessageMailed($orderRecord);
                break;
            case self::TYPE_PREPAY:
                return new OrderNotificationMessagePrepay($orderRecord);
                break;
            default:
                throw new \RuntimeException('Notification of type' . $type . ' not found');
        }
    }
}
