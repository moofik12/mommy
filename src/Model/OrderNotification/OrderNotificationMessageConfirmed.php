<?php

namespace MommyCom\Model\OrderNotification;

use CException;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Facade\EmailTranslator;

/**
 * Class OrderNotificationMessageConfirmed
 */
class OrderNotificationMessageConfirmed extends OrderNotificationMessage
{
    /**
     * @param bool $force
     *
     * @return bool|mixed
     * @throws CException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function send($force = false)
    {
        $user = $this->order->user;

        if (in_array(OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED, $this->getStatusHistory()) && !$force) {
            return false;
        }

        $result = $this->app()->mailer->create(
            [$this->app()->params['layoutsEmails']['confirmed'] => $this->app()->params['distributionEmailName']],
            [$user->email],
            EmailTranslator::t('Order №{order} confirmed', ['{order}' => $this->order->id]),
            [
                'view' => 'notification.order.confirmed',
                'data' => [
                    'order' => $this->order,
                    'frontendUrlManager' => $this->getFrontendUrlManager(),
                    'translator' => EmailTranslator::get(),
                ],
            ],
            EmailMessage::TYPE_SYSTEM
        );

        return $result;
    }
}
