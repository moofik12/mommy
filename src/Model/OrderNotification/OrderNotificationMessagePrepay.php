<?php

namespace MommyCom\Model\OrderNotification;

use CException;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\YiiComponent\Facade\EmailTranslator;
use Yii;

/**
 * Class OrderNotificationMessagePrepay
 */
class OrderNotificationMessagePrepay extends OrderNotificationMessage
{
    /**
     * @param bool $force
     *
     * @return bool|EmailMessage|mixed
     * @throws CException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function send($force = false)
    {
        $user = $this->order->user;
        $allowed = Yii::app()->params['newsletters'] ?? false;

        if ($allowed) {
            $result = Yii::app()->mailer->create(
                [Yii::app()->params['layoutsEmails']['prepay'] => Yii::app()->params['distributionEmailName']],
                [$user->email],
                "Заказ  №{$this->order->id} ожидает оплату",
                [
                    'view' => 'notification.order.prepay',
                    'data' => [
                        'order' => $this->order,
                        'frontendUrlManager' => $this->getFrontendUrlManager(),
                        'translator' => EmailTranslator::get(),
                    ],
                ],
                EmailMessage::TYPE_SYSTEM
            );
        }

        return $result ?? $allowed;
    }
}
