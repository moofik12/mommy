<?php

namespace MommyCom\Model\OrderNotification;

use CComponent;
use CLogger;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\ArrayUtils;
use Yii;

/**
 * Class OrderNotification
 */
class OrderNotification extends CComponent
{
    const NOTIFICATION_UNSENT_TIME = 10; // в течение 10 секунд с момента изменения статуса будет считаться, что сообщение не было отправлено

    /**
     * @var OrderRecord
     */
    protected $_order;

    /**
     * @var array
     */
    protected $_config;

    /**
     * OrderNotification constructor.
     *
     * @param OrderRecord $order
     */
    public function __construct(OrderRecord $order)
    {
        $this->_order = $order;
        $this->_config = [
            'create' => OrderRecord::PROCESSING_UNMODERATED,
            'prepay' => OrderRecord::PROCESSING_CALLCENTER_PREPAY,
            'confirmed' => OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
            'automaticConfirmed' => OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
            'mailed' => OrderRecord::PROCESSING_STOREKEEPER_MAILED,
        ];
    }

    /**
     * @param null|string $message
     * @param bool $force
     *
     * @return bool
     */
    public function send($message = null, $force = false)
    {
        $status = $this->_order->processing_status;
        $statusPrev = $this->_order->processing_status_prev;
        $isNew = $this->_order->isNewRecord;

        $result = false;
        if ($message !== null) {
            $messageStatuses = $this->_config[$message];
            $status = is_array($messageStatuses) ? reset($messageStatuses) : $messageStatuses;
        } else {
            foreach ($this->_config as $key => $value) {
                if ((is_array($value) && in_array($status, $value)) || $value == $status) {
                    $message = $key;
                    break;
                }
            }
        }

        $isAlreadySent = true;

        if ($force) {
            $isAlreadySent = false;
        } else {
            $history = $this->_order->getProcessingStatusHistory();
            $statusLog = ArrayUtils::getColumn($history, 'status');

            end($history);

            $changedAt = key($history);
            $time = time();
            $isChanged = $changedAt >= $time - self::NOTIFICATION_UNSENT_TIME;

            if ($isNew) {
                $isAlreadySent = false;
            } elseif ($isChanged) {
                end($statusLog);

                unset($statusLog[key($statusLog)]); // remove last
                $isAlreadySent = $status == $statusPrev || in_array($status, $statusLog);
            }
        }

        if (!$isAlreadySent && $message) {
            $class = 'MommyCom\Model\OrderNotification\OrderNotificationMessage' . ucfirst($message);

            if (class_exists($class)) {
                $instance = new $class($this->_order);
                /* @var $instance OrderNotificationMessage */
                $result = $instance->send($force);
            } else {
                Yii::log('Ошибка при создании уведомления для заказа. Не найден класс: ' . $class, CLogger::LEVEL_ERROR
                    , 'application.notification');
            }
        }

        return $result;
    }
} 
