<?php

namespace MommyCom\Model\OrderNotification;

use CException;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\YiiComponent\Facade\EmailTranslator;
use MommyCom\YiiComponent\Emailer;

/**
 * Class OrderNotificationMessageCreate
 */
class OrderNotificationMessageCreate extends OrderNotificationMessage
{
    /**
     * @param bool $force
     *
     * @return bool|mixed
     * @throws CException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function send($force = false)
    {
        $user = $this->order->user;

        /** @var Emailer $mailer */
        $mailer = $this->app()->mailer;

        $result = $mailer->create(
            [$this->app()->params['layoutsEmails']['create'] => $this->app()->params['distributionEmailName']],
            [$user->email],
            EmailTranslator::t('Thank you for your order') . " {$this->order->id}!",
            [
                'view' => 'notification.order.create',
                'data' => [
                    'order' => $this->order,
                    'frontendUrlManager' => $this->getFrontendUrlManager(),
                    'translator' => EmailTranslator::get(),
                ],
            ],
            EmailMessage::TYPE_SYSTEM
        );

        return $result;
    }
}
