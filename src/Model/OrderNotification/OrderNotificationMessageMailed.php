<?php

namespace MommyCom\Model\OrderNotification;

use CException;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\YiiComponent\Facade\EmailTranslator;
use Yii;

/**
 * Class OrderNotificationMessageMailed
 */
class OrderNotificationMessageMailed extends OrderNotificationMessage
{
    /**
     * @param bool $force
     *
     * @return bool
     * @throws CException
     */
    public function send($force = false)
    {
        $createSMS = $this->_createSMS();
        $createEmail = $this->_createEmail();

        return $createSMS && $createEmail;
    }

    /**
     * @return EmailMessage
     * @throws CException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function _createEmail()
    {
        $user = $this->order->user;
        $allowed = Yii::app()->params['newsletters'] ?? false;

        if ($allowed) {
            $result = Yii::app()->mailer->create(
                [Yii::app()->params['layoutsEmails']['mailed'] => Yii::app()->params['distributionEmailName']],
                [$user->email],
                "Заказ  №{$this->order->id} был передан в службу доставки",
                [
                    'view' => 'notification.order.mailed',
                    'data' => [
                        'order' => $this->order,
                        'frontendUrlManager' => $this->getFrontendUrlManager(),
                        'translator' => EmailTranslator::get(),
                    ],
                ],
                EmailMessage::TYPE_SYSTEM
            );
        }

        return $result ?? $allowed;
    }

    protected function _createSMS()
    {
        $message = 'Шоппинг-клуб МАМАМ. ';
        $order = $this->getOrder();

        $message .= 'Спасибо за заказ, ждем Вас снова!';

        return Yii::app()->sms->create($order->user_id, $order->telephone, $message);
    }
}
