<?php

namespace MommyCom\Model\OrderNotification;

use CComponent;
use CException;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\BuildUrlManager;
use Yii;

/**
 * Class OrderNotificationMessage
 *
 * @property-read OrderRecord $order
 */
abstract class OrderNotificationMessage extends CComponent
{
    use ApplicationTrait;

    /**
     * @var OrderRecord
     */
    private $_order;

    /**
     * @var array
     */
    private $_statusHistory = [];

    /**
     * OrderNotificationMessage constructor.
     *
     * @param OrderRecord $order
     */
    public function __construct(OrderRecord $order)
    {
        $this->_order = $order;
        $history = $this->_order->getProcessingStatusHistory();
        $this->_statusHistory = ArrayUtils::getColumn($history, 'status');
    }

    /**
     * @return BuildUrlManager
     * @throws CException
     */
    public function getFrontendUrlManager()
    {
        /**
         * на самом деле не очень понятно куда это засунуть.
         * дело в том, что первое сообщение будет отправляться в контексте frontend[-mobile]
         * а последующие в backend
         * потому контекст Yii:app() не совсем подходит
         */
        return Yii::createComponent([
            'class' => BuildUrlManager::class,
            'configApp' => include_once ROOT_PATH . '/config/yii/frontend/main.php', //include_once обязательно
        ]);
    }

    /**
     * @return OrderRecord
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * array([timestamp] => OrderRecordStatus);
     *
     * @return array
     */
    public function getStatusHistory()
    {
        return $this->_statusHistory;
    }

    /**
     * @param bool $force
     *
     * @return mixed
     */
    abstract public function send($force = false);
} 
