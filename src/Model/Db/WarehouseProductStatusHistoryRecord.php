<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbExpression;
use CException;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class WarehouseProductStatusHistoryRecord
 *
 * @property-read integer $id
 * @property integer $product_id
 * @property integer $status
 * @property integer $status_prev
 * @property string $comment
 * @property integer $user_id
 * @property integer $client_id
 * @property integer $time
 * @property-read string $statusReplacement
 * @property-read string $statusPrevReplacement
 * @property-read EventProductRecord $product
 * @property-read AdminUserRecord $user
 * @property-read UserRecord|null $client
 */
class WarehouseProductStatusHistoryRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return WarehouseProductStatusHistoryRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['time', 'getTime', 'setTime', 'time'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['client_id', 'getClientId', 'setClientId', 'clientId'];
    }

    public function tableName()
    {
        return 'warehouse_products_statushistory';
    }

    public function relations()
    {
        return [
            'product' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventProductRecord', 'product_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'client' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'client_id'],
        ];
    }

    public function rules()
    {
        return [
            ['product_id', 'required'],
            ['product_id', 'numerical', 'integerOnly' => true, 'min' => 0],

            ['id, product_id, status, status_prev, comment, user_id, client_id, time', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'product_id' => \Yii::t($category, 'Stock number'),
            'status' => \Yii::t($category, 'Status'),
            'status_prev' => \Yii::t($category, 'Status of previous'),
            'comment' => \Yii::t($category, 'Comment'),
            'user_id' => \Yii::t($category, 'User'),
            'client_id' => \Yii::t($category, 'Client'),
            'time' => \Yii::t($category, 'Time'),
            'product.eventProduct.name' => \Yii::t($category, 'Order'),
        ];
    }

    public function _resolveTime()
    {
        if (empty($this->time)) {
            $this->time = new CDbExpression('UNIX_TIMESTAMP()');
        }
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        $this->_resolveTime();
        try { // for console app
            if (empty($this->user_id)) {
                $this->user_id = \Yii::app()->user->id;
            }
        } catch (CException $e) {
        }

        return true;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function productId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', '=' . Utf8::ltrim($model->id, '0'));
        $criteria->compare($alias . '.product_id', '=' . $model->product_id);

        $criteria->compare($alias . '.status', '=' . $model->status);
        $criteria->compare($alias . '.status_prev', '=' . $model->status_prev);

        $criteria->compare($alias . '.user_id', '=' . $model->user_id);
        $criteria->compare($alias . '.client_id', '=' . $model->client_id);
        $criteria->compare($alias . '.time', '=' . $model->time);

        return $provider;
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = WarehouseProductRecord::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = WarehouseProductRecord::statusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }
}
