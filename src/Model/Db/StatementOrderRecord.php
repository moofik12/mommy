<?php

namespace MommyCom\Model\Db;

use CException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Delivery;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class StatementOrderRecord
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $statement_id
 * @property integer $service_ref
 * @property integer $user_id
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read OrderRecord $order
 * @property-read StatementRecord $statement
 */
class StatementOrderRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return StatementOrderRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['service_ref', 'getServiceRef', 'setServiceRef', 'serviceRef'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['statement_id', 'getStatementId', 'setStatementId', 'statementId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'statements_orders';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],

            'statement' => [self::BELONGS_TO, 'MommyCom\Model\Db\StatementRecord', 'statement_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id, order_id, statement_id', 'numerical', 'integerOnly' => true],

            ['order_id', 'unique'],

            ['statement_id', 'exist', 'className' => 'MommyCom\Model\Db\StatementRecord', 'attributeName' => 'id'],
            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],

            ['order_id', 'validatorDeliveryType'],

            ['service_ref', 'length', 'max' => 36],

            ['id, statement_id, order_id, user_id, service_ref, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function beforeDelete()
    {
        //документ нужно расформировать на сервисе
        if (!empty($this->service_ref)) {
            return false;
        }

        return parent::beforeDelete();
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        try { // for console app
            $this->user_id = \Yii::app()->user->id;
        } catch (CException $e) {
        }

        return true;
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'user_id' => \Yii::t($category, 'User'),
            'order_id' => \Yii::t($category, 'Order'),
            'service_ref' => \Yii::t($category, 'ID in the service system'),
            'statement_id' => \Yii::t($category, 'Statement ID'),
            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),
        ];
    }

    /**
     * @param $attribute
     */
    public function validatorDeliveryType($attribute)
    {
        $value = $this->$attribute;
        $order = OrderRecord::model()->findByPk($value);
        $statement = StatementRecord::model()->findByPk($value);

        if (!Delivery::available()->hasDelivery($order->delivery_type)) {
            $this->addError($attribute, \Yii::t('common', 'You can add to the act only orders that are delivered enabled service'));
        }

        if ($order !== null && $statement !== null && $order->delivery_type != $statement->delivery_type) {
            $this->addError($attribute, \Yii::t('common', 'Delivery in the order does not match the one selected in the account'));
        }
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function statementId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'statement_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }
}
