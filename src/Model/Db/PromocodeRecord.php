<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use CValidator;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Misc\StringList;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class PromocodeRecord
 *
 * @property-read integer $id
 * @property string $promocode
 * @property integer $admin_id
 * @property integer $type
 * @property integer $percent число от 0 до 100
 * @property integer $cash
 * @property integer $valid_until действителен до
 * @property integer $user_id используется для персонализированных
 * @property integer $partner_id используется для партнерских промокодов
 * @property boolean $is_deleted
 * @property integer $benefice_user_id
 * @property integer $benefice_cash
 * @property integer $benefice_percent
 * @property boolean $is_benefice_payed
 * @property integer $order_price_after
 * @property integer $reason причина по которой выдали промокод
 * @property string $reason_description
 * @property string $event_ids_json к каким акциям может применяться
 * @property string $event_product_ids_json к каким акционным товарам может применяться
 * @property string $products_ids_json к каким товарам может применяться
 * @property bool $generateShortPromocode актуально только к новым промокодам
 * @property-read UserRecord|null $user
 * @property-read OrderRecord[] $orders в персоналазированных будет максимум только однин элемент
 * @property-read integer $orderCount
 * @property-read AdminUserRecord|null $admin
 * @property-read UserPartnerRecord|null $partner
 * @property integer[] $eventIds на какие акции распостраняется промокод
 * @property integer[] $eventProductIds на какие товары акции распостраняется промокод
 * @property integer[] $productIds на какие товары распостраняется промокод
 * @property StringList $eventIdsStrings
 * @property StringList $eventProductIdsStrings
 * @property StringList $productIdsStrings
 * @property-read string $typeReplacement
 * @property-read string $reasonReplacement
 * @mixin Timestampable
 */
class PromocodeRecord extends DoctrineActiveRecord
{
    const PRIVATE_KEY = '1^asd@7ugFsvzY776cvmd';

    const TYPE_PERSONAL = 0; // привязывается к клиенту
    const TYPE_ANY = 1; // не важно
    const TYPE_UNLIMITED = 2; // бесрочные

    const REASON_NOT_SET = 0;
    const REASON_CAMPAIGN = 1;
    const REASON_UNFORMED_CART = 2;
    const REASON_ATTRACT_FRIENDS = 3;
    const REASON_INTERESTING_PRODUCTS = 4;
    const REASON_PARTNER = 5;

    const PROMOCODE_LENGTH = 21;
    const PROMOCODE_SHORT_LENGTH = 8;

    private $_generateShortPromocode = false;

    /**
     * @param string $className
     *
     * @return PromocodeRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['promocode', 'getPromocode', 'setPromocode', 'promocode'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['reason', 'getReason', 'setReason', 'reason'];
        yield ['reason_description', 'getReasonDescription', 'setReasonDescription', 'reasonDescription'];
        yield ['percent', 'getPercent', 'setPercent', 'percent'];
        yield ['cash', 'getCash', 'setCash', 'cash'];
        yield ['order_price_after', 'getOrderPriceAfter', 'setOrderPriceAfter', 'orderPriceAfter'];
        yield ['valid_until', 'getValidUntil', 'setValidUntil', 'validUntil'];
        yield ['event_ids_json', 'getEventIdsJson', 'setEventIdsJson', 'eventIdsJson'];
        yield ['event_product_ids_json', 'getEventProductIdsJson', 'setEventProductIdsJson', 'eventProductIdsJson'];
        yield ['products_ids_json', 'getProductsIdsJson', 'setProductsIdsJson', 'productsIdsJson'];
        yield ['is_deleted', 'isDeleted', 'setIsDeleted', 'isDeleted'];
        yield ['benefice_user_id', 'getBeneficeUserId', 'setBeneficeUserId', 'beneficeUserId'];
        yield ['benefice_percent', 'getBeneficePercent', 'setBeneficePercent', 'beneficePercent'];
        yield ['benefice_cash', 'getBeneficeCash', 'setBeneficeCash', 'beneficeCash'];
        yield ['is_benefice_payed', 'isBeneficePayed', 'setIsBeneficePayed', 'isBeneficePayed'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
        yield ['partner_id', 'getPartnerId', 'setPartnerId', 'partnerId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'promocodes';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'orders' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderRecord', ['promocode' => 'promocode']],
            'orderCount' => [self::STAT, 'MommyCom\Model\Db\OrderRecord', ['promocode' => 'promocode']],
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
            'partner' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserPartnerRecord', 'partner_id'],
        ];
    }

    public function rules()
    {
        return [
            ['eventIds, eventProductIds, productIds, eventIdsStrings, eventProductIdsStrings, productIdsStrings, promocode',
                'filter', 'filter' => [Utf8::class, 'trim']],

            ['promocode', 'length', 'max' => self::PROMOCODE_LENGTH],
            ['promocode', 'validatePromocode'],
            ['promocode', 'unique'],

            ['is_deleted', 'boolean'],

            ['type', 'in', 'range' => [
                self::TYPE_PERSONAL,
                self::TYPE_ANY,
                self::TYPE_UNLIMITED,
            ]],

            ['user_id', 'validateUser', 'type' => self::TYPE_PERSONAL],
            ['partner_id', 'validatePartner', 'reason' => self::REASON_PARTNER, 'type' => self::TYPE_UNLIMITED],

            ['percent', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 99,
            ],
            ['cash', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000],
            ['cash', 'validateCashOrPercent', 'percentAttributeName' => 'percent'],

            ['order_price_after', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000],

            //benefice
            ['benefice_user_id', 'validateUser', 'type' => self::TYPE_ANY, 'on' => 'benefice'],
            ['benefice_percent', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 99, 'on' => 'benefice'],
            ['benefice_cash', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000, 'on' => 'benefice'],
            ['benefice_cash', 'validateCashOrPercent', 'percentAttributeName' => 'percent', 'on' => 'benefice'],
            ['is_benefice_payed', 'boolean'],

            ['valid_until', 'numerical', 'integerOnly' => true, 'min' => 1],

            ['validUntilString, validUntilDateString', 'required'],

            ['validUntilString, validUntilDateString', 'validatorValidUntil', 'on' => 'insert, benefice'],

            ['reason', 'in', 'range' => [
                self::REASON_NOT_SET,
                self::REASON_CAMPAIGN,
                self::REASON_UNFORMED_CART,
                self::REASON_ATTRACT_FRIENDS,
                self::REASON_INTERESTING_PRODUCTS,
                self::REASON_PARTNER,
            ]],
            ['reason_description', 'type', 'type' => 'string'],

            ['generateShortPromocode', 'safe'],
            ['id, promocode, type, reason, percent, cash, admin_id, user_id, partner_id, is_deleted, benefice_user_id, benefice_percent, 
            benefice_cash, is_benefice_payed, created_at, updated_at'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', '№'),
            'admin.fullname' => \Yii::t('common', 'Operator'),
            'promocode' => \Yii::t('common', 'Promotional code'),
            'user_id' => \Yii::t('common', 'User'),
            'type' => \Yii::t('common', 'A type'),
            'cash' => \Yii::t('common', 'Discount (amount)'),
            'percent' => \Yii::t('common', 'Discount (percentage)'),
            'valid_until' => \Yii::t('common', 'Valid until'),
            'is_deleted' => \Yii::t('common', 'Deleted'),
            'reason' => \Yii::t('common', 'Reason'),
            'reason_description' => \Yii::t('common', 'Description of the reason'),
            'order_price_after' => \Yii::t('common', 'Ordering over'),
            'partner_id' => \Yii::t('common', 'Partner'),

            'typeReplacement' => \Yii::t('common', 'A type'),
            'validUntilString' => \Yii::t('common', 'Valid until'),
            'validUntilDateString' => \Yii::t('common', 'Valid until'),
            'reasonReplacement' => \Yii::t('common', 'Base'),

            'eventIds' => \Yii::t('common', 'Stocks'),
            'eventProductIds' => \Yii::t('common', 'Stock goods'),
            'productIds' => \Yii::t('common', 'Products in the catalog'),

            'eventIdsStrings' => \Yii::t('common', 'Flash-sales'),
            'eventProductIdsStrings' => \Yii::t('common', 'Stock goods'),
            'productIdsStrings' => \Yii::t('common', 'Products in the catalog'),
            'generateShortPromocode' => \Yii::t('common', 'Make a short promotional code'),

            'benefice_user_id' => \Yii::t('common', 'User (Benefits)'),
            'benefice_cash' => \Yii::t('common', 'Amount of bonuses'),
            'benefice_percent' => \Yii::t('common', 'The percentage of bonuses accrued under the loyalty program'),
            'is_benefice_payed' => \Yii::t('common', 'A payment was made'),

            'created_at' => \Yii::t('common', 'Created'),
            'updated_at' => \Yii::t('common', 'Updated'),
        ];
    }

    public function validateCashOrPercent($attribute, $params)
    {
        $value = Cast::toUInt($this->$attribute);
        $percentAttribute = $params['percentAttributeName'];
        $percent = Cast::toUInt($this->$percentAttribute);
        if ($value == 0 && $percent == 0) {
            $this->addError($attribute, \Yii::t('common', 'You must specify the discount amount (amount or percentage)'));
            $this->addError($percentAttribute, \Yii::t('common', 'You must specify the discount amount (amount or percentage)'));
        }
    }

    public function validateUser($attribute, $params)
    {
        if ($params['type'] == $this->type) {
            $validator = CValidator::createValidator('exist', $this, [$attribute], [
                'className' => 'MommyCom\Model\Db\UserRecord',
                'attributeName' => 'id',
                'allowEmpty' => false,
            ]);
            $validator->validate($this);
        }
    }

    public function validatePartner($attribute, $params)
    {
        if ($params['reason'] == $this->reason && $params['type'] == $this->type) {
            $validator = CValidator::createValidator('exist', $this, [$attribute], [
                'className' => 'MommyCom\Model\Db\UserPartnerRecord',
                'attributeName' => 'id',
                'allowEmpty' => false,
            ]);
            $validator->validate($this);
        }
    }

    public function validatePromocode($attribute)
    {
        $value = $this->$attribute;
        if (!empty($value) && !$this->checkPromocode($value)) {
            $this->addError($attribute, \Yii::t('common', 'Invalid promotional code'));
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatorValidUntil($attribute, $params)
    {
        if (!$this->isNewRecord) {
            return;
        }

        if ($this->type != self::TYPE_UNLIMITED) {
            if ($this->valid_until < time()) {
                $this->addError($attribute, \Yii::t('skip', 'Promotion code is expired'));
            }
        }
    }

    /**
     * @param int $type
     * @param bool $short
     *
     * @return string
     */
    public static function buildPromocode($type, $short = false)
    {
        static $keys = [
            self::TYPE_PERSONAL => '1',
            self::TYPE_ANY => '2',
            self::TYPE_UNLIMITED => '3',
        ];

        $key = isset($keys[$type]) ? $keys[$type] : reset($keys);

        if ($short) {
            $id = Random::alphabet(6, '1234567890');
            return sprintf('%1s-%6s', $key, $id);
        }

        $id = Random::alphabet(12, '1234567890');
        $chunkedId = str_split($id, 4);
        $protect = abs(substr(base_convert(hash('crc32', $key . $id . self::PRIVATE_KEY), 16, 10), 0, 4));

        return sprintf('%1s-%4s-%4s-%4s-%4s', $key, $chunkedId[0], $chunkedId[1], $chunkedId[2], $protect);
    }

    /**
     * @param $promocode
     *
     * @return boolean
     */
    public static function checkPromocode($promocode)
    {
        $promocodeLength = strlen($promocode);
        if ($promocodeLength != self::PROMOCODE_LENGTH && $promocodeLength != self::PROMOCODE_SHORT_LENGTH) {
            return false;
        }

        if ($promocodeLength == self::PROMOCODE_SHORT_LENGTH) {
            list($key, $id) = @sscanf($promocode, '%1s-%6s');
            return strlen($id) == 6;
        }

        list($key, $chunkedId1, $chunkedId2, $chunkedId3, $protect) = @sscanf($promocode, '%1s-%4s-%4s-%4s-%4s');

        $id = $chunkedId1 . $chunkedId2 . $chunkedId3;
        $validProtect = abs(substr(base_convert(hash('crc32', $key . $id . self::PRIVATE_KEY), 16, 10), 0, 4));

        return strlen($id) == 12 && $protect == $validProtect;
    }

    /**
     *
     */
    protected function _generatePromocode()
    {
        if (!empty($this->promocode)) {
            return;
        }

        $i = 0;
        do {
            $i++;
            $this->promocode = self::buildPromocode($this->type, $this->_generateShortPromocode);
        } while (!$this->validate(['promocode']) || $i < 100);
    }

    protected function _removeUser()
    {
        if ($this->type == self::TYPE_ANY) {
            $this->user_id = 0;
        }
    }

    protected function _resolveAdminUser()
    {
        if ($this->isNewRecord && $this->admin_id == 0) {
            try { // for console app
                $this->admin_id = \Yii::app()->user->id;
            } catch (CException $e) {
            }
        }
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        $this->_generatePromocode();
        $this->_removeUser();
        $this->_resolveAdminUser();
        return true;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if ($this->getIsUsed()) {
            $this->is_deleted = true;
            $this->save(true, ['is_deleted']);
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function getGenerateShortPromocode()
    {
        return $this->_generateShortPromocode;
    }

    /**
     * @param boolean $generateShortPromocode
     */
    public function setGenerateShortPromocode($generateShortPromocode)
    {
        $this->_generateShortPromocode = !!$generateShortPromocode;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function promocode($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'promocode' => $value,
        ]);
        return $this;
    }

    /**
     * @param $values
     *
     * @return $this
     */
    public function promocodeIn($values)
    {
        $values = Cast::toStrArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.promocode', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function type($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function reason($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'reason' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * <b>Заканчивается ДО</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function timeValidUntilTo($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'valid_until' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Заканчивается ПОСЛЕ</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function timeValidUntilFrom($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'valid_until' . '>=' . Cast::toStr($value));
        return $this;
    }

    public function onlyTimeValid()
    {
        $this->timeValidUntilTo(time());
    }

    public function onlyTimeInvalid()
    {
        $this->timeValidUntilTo(time());
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isDeleted($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_deleted' => $value,
        ]);
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsTypePersonal()
    {
        return $this->type == self::TYPE_PERSONAL;
    }

    /**
     * @return bool
     */
    public function getIsTypeUnlimited()
    {
        return $this->type == self::TYPE_UNLIMITED;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.user_id', '=' . $model->user_id);
        $criteria->compare($alias . '.admin_id', '=' . $model->admin_id);
        $criteria->compare($alias . '.type', '=' . $model->type);
        $criteria->compare($alias . '.reason', '=' . $model->reason);
        $criteria->compare($alias . '.cash', '=' . $model->cash);
        $criteria->compare($alias . '.percent', '=' . $model->percent);
        $criteria->compare($alias . '.is_deleted', '=' . $model->is_deleted);
        $criteria->compare($alias . '.order_price_after', '>' . $model->order_price_after);
        $criteria->compare($alias . '.promocode', $model->promocode, true);
        $criteria->compare($alias . '.partner_id', $model->partner_id);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    // item api

    /**
     * Заменяет указанные атрибуты у указанного времени
     *
     * @param integer $time
     * @param null|integer $minutes
     * @param null|integer $hours
     * @param null|integer $day
     * @param null|integer $month
     * @param null|integer $year
     *
     * @return integer
     */
    protected function _combineTime($time, $minutes = null, $hours = null, $day = null, $month = null, $year = null)
    {
        $timeData = getdate($time);
        return mktime(
            $hours === null ? $timeData['hours'] : $hours,
            $minutes === null ? $timeData['minutes'] : $minutes,
            0,
            $month === null ? $timeData['mon'] : $month,
            $day === null ? $timeData['mday'] : $day,
            $year === null ? $timeData['year'] : $year
        );
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getValidUntilString($format = '%d.%m.%Y %H:%M')
    {
        return $this->valid_until > 0 ? strftime($format, $this->valid_until) : '';
    }

    /**
     * @param string $datetime
     * @param string $format
     *
     * @return $this
     */
    public function setValidUntilString($datetime, $format = '%d.%m.%Y %H:%M')
    {
        $timeData = strptime($datetime, $format);
        $this->valid_until = $this->_combineTime(
            $this->valid_until,
            $timeData['tm_min'],
            $timeData['tm_hour'],
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getValidUntilDateString($format = '%d.%m.%Y')
    {
        return $this->valid_until > 0 ? strftime($format, $this->valid_until) : '';
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return $this
     */
    public function setValidUntilDateString($date, $format = '%d.%m.%Y')
    {
        $timeData = strptime($date, $format);
        $this->valid_until = $this->_combineTime(
            $this->valid_until,
            null,
            null,
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsTypeAny()
    {
        return $this->type == self::TYPE_ANY;
    }

    /**
     * @param integer $userId
     * @param int $orderId
     *
     * @return bool
     */
    public function getIsUsed($userId = 0, $orderId = 0)
    {
        $userId = Cast::toUInt($userId);
        $orderId = Cast::toUInt($orderId);

        $selector = OrderRecord::model()->promocode($this->promocode);
        $selector->processingStatusNotCancelled();

        if ($userId) {
            $selector->userId($userId);
        }
        if ($orderId) {
            $selector->idIn([$orderId]);
        }
        return $selector->find() !== null;
    }

    /**
     * Есть ли у пользователя хотя бы один заказ
     *
     * @param $userId
     *
     * @return bool
     */
    public function isExistOrder($userId)
    {
        $userId = Cast::toUInt($userId);
        $selector = OrderRecord::model()->userId($userId);
        return $selector->count() == 0;
    }

    /**
     * Может ли промокод еще быть использован
     *
     * @return bool
     */
    public function getIsTimeValid()
    {
        if ($this->type == self::TYPE_UNLIMITED) {
            return true;
        }
        return $this->valid_until >= time();
    }

    /**
     * Есть ли у пользователя хотя бы один новый заказ без промокода
     *
     * @param $userId
     *
     * @return bool
     */
    public function isExistNewOrderWithoutPromocode($userId)
    {
        $userId = Cast::toUInt($userId);
        $orders = OrderRecord::model()->userId($userId)->findAll();
        foreach ($orders as $order) {
            /** @var OrderRecord $order */
            if (!in_array($order->processing_status, [
                    OrderRecord::PROCESSING_UNMODERATED, // не обработано
                    OrderRecord::PROCESSING_CALLCENTER_RECALL, // возвращено на прозвон
                ]) || $order->promocode != '') {
                return false;
            }
        }
        return true;
    }

    /**
     * Может ли использоваться этим юзером партнерский промокод
     *
     * @param int $userId
     *
     * @return bool
     */
    public function getCanUsedPartnerPromocode($userId)
    {
        $userId = Cast::toUInt($userId);
        $inviteUser = UserPartnerInviteRecord::model()->userId($userId)->find();
        if ($inviteUser === null) {
            return true;
        }
        /** @var $inviteUser UserPartnerInviteRecord */
        return $this->partner_id == $inviteUser->partner_id;
    }

    /**
     * Может ли быть использовано этим клиентом
     *
     * @param integer $userId
     * @param boolean $checkUsed
     *
     * @return bool
     */
    public function getIsUsable($userId, $checkUsed = true)
    {
        $userId = Cast::toUInt($userId);

        $partnerUserId = 0;
        if ($this->partner_id !== 0 && $this->getIsTypeUnlimited()) {
            if ($this->partner !== null) {
                $partnerUserId = $this->partner->user_id;
            }
        }

        $result = $this->getIsTypeAny() || ($this->getIsTypePersonal() && $this->user_id == $userId); // совпадает ли тип
        $result = $result && $this->getIsTimeValid(); // активен дисконт
        $result = $result && (!$checkUsed || !$this->getIsUsed($userId)); // был ли уже ранее использован
        $result = $result && ($userId != $this->benefice_user_id); // нельзя в дальнейшем делать выплату сам себе
        $result = $result || ($this->getIsTypeUnlimited() && $partnerUserId != $userId && $this->getCanUsedPartnerPromocode($userId) && $this->isExistOrder($userId));
        $result = $result || ($this->getIsTypeUnlimited() && $partnerUserId != $userId && $this->getCanUsedPartnerPromocode($userId) && $this->isExistNewOrderWithoutPromocode($userId));
        return $result;
    }

    /**
     * Возвращает предоставляемую скидку
     *
     * @param OrderProductRecord[]|CartRecord[] $products
     *
     * @return float
     */
    public function getDiscount(array $products)
    {
        $price = 0.0;
        $discount = 0.0;

        if ($this->is_deleted) {
            return 0.0;
        }

        $eventIds = $this->eventIds;
        $eventProductIds = $this->eventProductIds;
        $productIds = $this->productIds;

        foreach ($products as $product) {
            if ($product instanceof OrderProductRecord) {
                $itemPrice = $product->priceTotal;
                $itemEventId = $product->event_id;
                $itemEventProductId = $product->product_id;
                $itemProductId = $product->catalog_product_id;
            } elseif ($product instanceof CartRecord) {
                $itemPrice = $product->totalPrice;
                $itemEventId = $product->event_id;
                $itemEventProductId = $product->product_id;
                $itemProductId = $product->product->product_id;
            } else {
                continue;
            }

            if (empty($eventIds) && empty($eventProductIds) && empty($productIds)) {
                $price += $itemPrice;
            } else {
                if (in_array($itemEventId, $eventIds) ||
                    in_array($itemEventProductId, $eventProductIds) ||
                    in_array($itemProductId, $productIds)) {
                    $price += $itemPrice;
                }
            }
        }

        if ($this->cash > 0) {
            $discount += min(Cast::toUFloat($this->cash), $price);
        }
        if ($this->percent > 0) {
            $discount += $price * Cast::toUFloat($this->percent) / 100;
        }

        $discount = min(round($discount), $price); // скидка не может быть больше цены товара

        if ($this->order_price_after > 0 && $price < $this->order_price_after) {
            $discount = 0.0;
        }

        return (int)$discount;
    }

    /**
     * Возвращает предоставляемое вознаграждение
     *
     * @param float|int|OrderRecord
     *
     * @return float
     */
    public function getBenefice($amount)
    {
        $discount = 0.0;

        if ($this->is_deleted) {
            return 0.0;
        }

        if ($amount instanceof OrderRecord) {
            $amount = Cast::toUFloat($amount->price_total);
        }

        $price = Cast::toUFloat($amount, 0.0);

        if ($this->benefice_cash > 0) {
            $discount += min(Cast::toUFloat($this->benefice_cash), $price);
        }
        if ($this->benefice_percent > 0) {
            $discount += $price * Cast::toUFloat($this->benefice_percent) / 100;
        }

        $discount = min(round($discount), $price); // скидка не может быть больше цены товара
        return (int)$discount;
    }

    /**
     * @return integer[]
     */
    public function getEventIds()
    {
        return Cast::toUIntArr(json_decode($this->event_ids_json, true, 2));
    }

    /**
     * @param integer[] $values
     *
     * @return $this
     */
    public function setEventIds($values)
    {
        $values = ArrayUtils::onlyNonZero(array_unique(Cast::toUIntArr($values)));
        $this->event_ids_json = json_encode($values);
        return $this;
    }

    /**
     * @return integer[]
     */
    public function getEventProductIds()
    {
        return Cast::toUIntArr(json_decode($this->event_product_ids_json, true, 2));
    }

    /**
     * @param integer[] $values
     *
     * @return $this
     */
    public function setEventProductIds($values)
    {
        $values = ArrayUtils::onlyNonZero(array_unique(Cast::toUIntArr($values)));
        $this->event_product_ids_json = json_encode($values);
        return $this;
    }

    /**
     * @return integer[]
     */
    public function getProductIds()
    {
        return Cast::toUIntArr(json_decode($this->products_ids_json, true, 2));
    }

    /**
     * @param integer[] $values
     *
     * @return $this
     */
    public function setProductIds($values)
    {
        $values = ArrayUtils::onlyNonZero(array_unique(Cast::toUIntArr($values)));
        $this->products_ids_json = json_encode($values);
        return $this;
    }

    /**
     * @return StringList|string
     */
    public function getEventIdsStrings()
    {
        return new StringList($this->getEventIds());
    }

    /**
     * @param StringList|string $value
     */
    public function setEventIdsStrings($value)
    {
        $value = new StringList($value);
        $this->setEventIds($value->toArray());
    }

    /**
     * @return StringList|string
     */
    public function getEventProductIdsStrings()
    {
        return new StringList($this->getEventProductIds());
    }

    /**
     * @param StringList|string $value
     */
    public function setEventProductIdsStrings($value)
    {
        $value = new StringList($value);
        $this->setEventProductIds($value->toArray());
    }

    /**
     * @return StringList|string
     */
    public function getProductIdsStrings()
    {
        return new StringList($this->getProductIds());
    }

    /**
     * @param StringList|string $value
     */
    public function setProductIdsStrings($value)
    {
        $value = new StringList($value);
        $this->setProductIds($value->toArray());
    }

    /**
     * @return string
     */
    public function getTypeReplacement()
    {
        $values = self::typeReplacements();
        return isset($values[$this->type]) ? $values[$this->type] : '';
    }

    /**
     * @return string
     */
    public function getReasonReplacement()
    {
        $values = self::reasonReplacements();
        return isset($values[$this->reason]) ? $values[$this->reason] : '';
    }

    /**
     * @param null|int $now текущее время
     *
     * @return int доступное кол-во дней действия промокода
     */
    public function getAvailableDays($now = null)
    {
        $days = 0;
        $time = time();
        $now = intval($now);

        if ($now > 0) {
            $time = $now;
        }

        if ($this->valid_until <= 0 || $this->valid_until < $time) {
            return $days;
        }

        $days = ceil(($this->valid_until - $time) / 86400);

        return $days;
    }

    /**
     * @return array
     */
    public static function typeReplacements()
    {
        return [
            self::TYPE_PERSONAL => \Yii::t('common', 'Personal'),
            self::TYPE_ANY => \Yii::t('common', 'For all'),
            self::TYPE_UNLIMITED => \Yii::t('common', 'Unlimited'),
        ];
    }

    /**
     * @return array
     */
    public static function reasonReplacements()
    {
        return [
            self::REASON_NOT_SET => \Yii::t('common', 'Not specified'),
            self::REASON_CAMPAIGN => \Yii::t('common', 'Event'),
            self::REASON_ATTRACT_FRIENDS => \Yii::t('skip', 'Lure friends'),
            self::REASON_UNFORMED_CART => \Yii::t('common', 'Newsletter - Not formed basket'),
            self::REASON_INTERESTING_PRODUCTS => \Yii::t('common', 'Newsletter - Featured Products'),
            self::REASON_PARTNER => \Yii::t('common', 'Affiliate'),
        ];
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function partnerId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'partner_id' => $value,
        ]);
        return $this;
    }
} 
