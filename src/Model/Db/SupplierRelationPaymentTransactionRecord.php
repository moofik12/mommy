<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * @property-read integer $id
 * @property integer $event_id
 * @property float $amount
 * @property string $bank_name_enc
 * @property string $bank_giro_enc
 * @property string $comment
 * @property integer $updated_at
 * @property integer $created_at
 * @property string $bankName
 * @property string $bankGiro
 * @property-read EventRecord $event
 */
class SupplierRelationPaymentTransactionRecord extends HasBankDataRecord
{
    const AVAILABLE_TIME_EDIT = 10800; //3 hours

    /**
     * @param string $className
     *
     * @return SupplierRelationPaymentTransactionRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['bank_name_enc', 'getBankNameEnc', 'setBankNameEnc', 'bankNameEnc'];
        yield ['bank_giro_enc', 'getBankGiroEnc', 'setBankGiroEnc', 'bankGiroEnc'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
    }

    public function tableName()
    {
        return 'suppliers_relations_payments_transactions';
    }

    public function relations()
    {
        return [
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],

            ['amount', 'numerical', 'min' => PHP_INT_MIN, 'max' => PHP_INT_MAX],
            ['bank_name_enc, bank_giro_enc, bankGiro, bankName', 'length', 'max' => 255, 'encoding' => false],
            ['comment', 'length', 'max' => 255],

            ['id, event_id, amount, bank_name_enc, bank_giro_enc, comment', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'event_id' => \Yii::t('common', 'Event'),
            'amount' => \Yii::t('common', 'Amount'),
            'bank_name_enc' => \Yii::t('common', 'Name of the bank'),
            'bankName' => \Yii::t('common', 'Name of the bank'),
            'bank_giro_enc' => \Yii::t('common', 'Bank account/Card Number'),
            'bankGiro' => \Yii::t('common', 'Bank account/Card Number'),
            'comment' => \Yii::t('common', 'Comment'),

            'updated_at' => \Yii::t('common', 'Created'),
            'created_at' => \Yii::t('common', 'Updated'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.event_id', $model->event_id);
        $criteria->compare($alias . '.amount', $model->amount);
        $criteria->compare($alias . '.comment', $model->comment);
        $criteria->compare($alias . '.bank_giro_enc', $model->bankGiro);
        $criteria->compare($alias . '.bank_name_enc', $model->bankName);

        return $provider;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.event_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdNotIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @return string
     */
    protected static function _bankDataPrivateKey(): string
    {
        return pack('H*', '5d0b5d233e8c3b6e8a839447392e9a82ec15dd9de2e0788e192e2292987d5cc6');
    }

    /**
     * @return string
     */
    public function getbankName(): string
    {
        return self::_bankDataDecode($this->bank_name_enc);
    }

    /**
     * @param $value
     */
    public function setbankName($value)
    {
        $this->bank_name_enc = self::_bankDataEncode($value);
    }

    /**
     * @return string
     */
    public function getbankGiro(): string
    {
        return self::_bankDataDecode($this->bank_giro_enc);
    }

    /**
     * @param $value
     */
    public function setbankGiro($value)
    {
        $this->bank_giro_enc = self::_bankDataEncode($value);
    }

    /* API */
    /**
     * @return bool
     */
    public function isAvailableEdit()
    {
        return $this->created_at + self::AVAILABLE_TIME_EDIT > time();
    }
}
