<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CHttpException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class TaskBoardAnswersRecord
 */
class TaskBoardAnswersRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['answer', 'getAnswer', 'setAnswer', 'answer'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['question_id', 'getQuestionId', 'setQuestionId', 'questionId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'task_board_answers';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'question' => [self::BELONGS_TO, 'MommyCom\Model\Db\TaskBoardRecord', 'question_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['answer, user_id , question_id', 'required'],
            ['question_id', 'exist', 'allowEmpty' => true, 'className' => 'MommyCom\Model\Db\TaskBoardRecord', 'attributeName' => 'id'],
            ['user_id', 'exist', 'allowEmpty' => true, 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => Translator::t('User'),
            'question_id' => Translator::t('Question'),
            'answer' => Translator::t('Answer'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);
        return $provider;
    }

    /**
     * @param $id
     *
     * @return static
     * @throws CHttpException
     */
    public static function loadModel($id)
    {
        $model = self::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Translator::t('Model does not exist.'));
        }
        return $model;
    }
}
