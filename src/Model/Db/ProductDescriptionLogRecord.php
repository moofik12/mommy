<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class ProductDescriptionLogRecord
 *
 * @property-description Для логов при генерации описания товаров
 * @property-read string $id
 * @property string $product_id
 * @property string $event_id
 * @property integer $is_description
 * @property string $created_at
 * @property string $updated_at
 */
class ProductDescriptionLogRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return ProductDescriptionLogRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['is_description', 'isDescription', 'setIsDescription', 'isDescription'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'products_description_logs';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['product_id', 'required'],
            ['id, product_id, event_id, is_description, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'product_id' => Translator::t('Product №'),
            'event_id' => Translator::t('Flash-sale №'),
            'is_description' => Translator::t('Has description'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare('id', $this->id);
        $criteria->compare('product_id', $this->product_id);
        $criteria->compare('event_id', $this->event_id);
        $criteria->compare('is_description', $this->is_description);
        $criteria->compare('created_at', $this->created_at);
        $criteria->compare('updated_at', $this->updated_at);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function productId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function eventId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function isDescription($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_description' => $value,
        ]);
        return $this;
    }

    public function isExistEvent()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'event_id' . '!=0'); // not empty
        return $this;
    }
}
