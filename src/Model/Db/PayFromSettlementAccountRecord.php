<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * @property-read integer $id
 * @property integer $status
 * @property integer $unique_payment
 * @property integer $possible_order_id
 * @property integer $order_id
 * @property string $provider
 * @property string $pay_comment
 * @property integer $pay_gateway_id
 * @property integer $operation_at
 * @property float $amount
 * @property-read integer $updated_at
 * @property-read integer $created_at
 * @property-read string $statusText
 * @property-read string $providerText
 * @property-read PayGatewayRecord $payGateway
 * @property-read OrderRecord $possibleOrder
 */
class PayFromSettlementAccountRecord extends DoctrineActiveRecord
{
    const STATUS_NOT_PROCESSED = 0;
    const STATUS_CREATED_PAYMENT = 1;
    const STATUS_CREATED_PAYMENT_AUTOMATIC = 2;
    const STATUS_WRONG = 3;

    /**
     * @param string $className
     *
     * @return PayGatewayRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['unique_payment', 'getUniquePayment', 'setUniquePayment', 'uniquePayment'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['provider', 'getProvider', 'setProvider', 'provider'];
        yield ['pay_comment', 'getPayComment', 'setPayComment', 'payComment'];
        yield ['operation_at', 'getOperationAt', 'setOperationAt', 'operationAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['possible_order_id', 'getPossibleOrderId', 'setPossibleOrderId', 'possibleOrderId'];
        yield ['pay_gateway_id', 'getPayGatewayId', 'setPayGatewayId', 'payGatewayId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'pay_from_settlement_account';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'payGateway' => [self::BELONGS_TO, 'MommyCom\Model\Db\PayGatewayRecord', 'pay_gateway_id'],
            'possibleOrder' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'possible_order_id'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['unique_payment, provider', 'required'],
            ['unique_payment', 'unique'],
            ['provider', 'in', 'range' => PayGatewayRecord::getProviders()],
            ['status', 'in', 'range' => [
                self::STATUS_NOT_PROCESSED,
                self::STATUS_CREATED_PAYMENT,
                self::STATUS_CREATED_PAYMENT_AUTOMATIC,
                self::STATUS_WRONG,
            ]],

            ['amount', 'numerical'],
            // хак чтобы CExistValidator считал значение "0" пустым
            ['pay_gateway_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],

            ['pay_comment', 'length', 'max' => 255],

            ['pay_gateway_id', 'exist', 'className' => 'MommyCom\Model\Db\PayGatewayRecord', 'attributeName' => 'id'],

            ['pay_comment, unique_payment, pay_gateway_id, possible_order_id, order_id, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'possible_order_id' => Translator::t('Possible order ID'),
            'provider' => Translator::t('Payment provider'),
            'order_id' => Translator::t('Order'),
            'unique_payment' => Translator::t('Payment ID (document number)'),
            'pay_comment' => Translator::t('Payment comment'),
            'pay_gateway_id' => Translator::t('Fixed payment №'),
            'status' => Translator::t('Status'),
            'operation_at' => Translator::t('Payment date'),
            'amount' => Translator::t('Amount'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.possible_order_id', $model->possible_order_id, true);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.unique_payment', $model->unique_payment, true);
        $criteria->compare($alias . '.pay_gateway_id', $model->pay_gateway_id);
        $criteria->compare($alias . '.status', $model->status);

        $criteria->compare($alias . '.operation_at', $model->operation_at);
        $criteria->compare($alias . '.amount', $model->amount);
        $criteria->compare($alias . '.unique_payment', $model->unique_payment, true);
        $criteria->compare($alias . '.pay_comment', $model->pay_comment, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function statusIs($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);

        return $this;
    }

    /* API */
    /**
     * @param string $id номер документа
     * @param int $timestamp день проведения платежа
     *
     * @return string
     */
    public static function getUniqueId($id, $timestamp)
    {
        $timestamp = Cast::toUInt($timestamp);

        return $id . '_' . date('d.m.Y', $timestamp);
    }

    /**
     * @return array
     */
    public static function getStatusesReplacement()
    {
        return [
            self::STATUS_NOT_PROCESSED => Translator::t('New'),
            self::STATUS_CREATED_PAYMENT => Translator::t('Payment created'),
            self::STATUS_CREATED_PAYMENT_AUTOMATIC => Translator::t('Payment created automatically'),
            self::STATUS_WRONG => Translator::t('Erroneous payment'),
        ];
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStatusText($default = '')
    {
        $statusesReplacements = self::getStatusesReplacement();

        return isset($statusesReplacements[$this->status]) ? $statusesReplacements[$this->status] : $default;
    }

    /**
     * Дополнительная нагрузка на БД
     *
     * @return bool
     */
    public function isPossibleAutomaticConfirmed()
    {
        if ($this->possible_order_id <= 0) {
            return false;
        }

        $order = OrderRecord::model()->findByPk($this->possible_order_id);
        if (!$order) {
            return false;
        }

        $payGatewayHasOrder = PayGatewayRecord::model()->invoiceIdIs($this->possible_order_id)->find();

        return $payGatewayHasOrder === null && $order->isAvailableForPayment() && $this->amount == $order->getPayPrice();
    }

    /**
     * @return bool
     */
    public function isWrong()
    {
        return $this->status == self::STATUS_WRONG;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return in_array($this->status, [self::STATUS_CREATED_PAYMENT, self::STATUS_CREATED_PAYMENT_AUTOMATIC]);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getProviderText($default = '')
    {
        $providers = PayGatewayRecord::getProviders(true);
        return isset($providers[$this->provider]) ? $providers[$this->provider] : $default;
    }
}
