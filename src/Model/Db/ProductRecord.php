<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CUploadedFile;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Model\ViewsTracking\ViewsTracking;
use MommyCom\Model\ViewsTracking\ViewTrackingRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class ProductRecord
 *
 * @property-read integer $id
 * @property string $unique_article sha512(article + '_' + brand_id + '_'  + color)
 * @property string $article
 * @property integer $brand_id
 * @property integer $section_id
 * @property string $color
 * @property integer $color_code код цвета
 * @property string $name
 * @property string $category
 * @property string $logo_fileid
 * @property string $images_json
 * @property integer $images_count
 * @property string $made_in
 * @property string $design_in
 * @property string $description
 * @property integer $age_from
 * @property integer $age_to
 * @property string[] $target @see ProductTargets
 * @property string $photo список файлов которые содержат фото для товара (ассортиментная таблица)
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read BrandRecord $brand
 * @property-read ViewTrackingRecord $views
 * @property-read ProductSectionRecord $section
 * @property FileStorageThumbnail[] $images изображения прикрепленные к товару, unserialized $images_json
 * @property FileStorageThumbnail $logo
 * @property-read boolean $isHasLogo
 * @property-read FileStorageThumbnail[] $photogalleryImages
 * @property-read array ageRange
 * @property-read string ageRangeReplacement
 * @property-read array ageGroups
 * @mixin Timestampable
 */
class ProductRecord extends DoctrineActiveRecord
{
    CONST MAX_IMAGES_COUNT = 4;

    /**
     * @param string $className
     *
     * @return ProductRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['unique_article', 'getUniqueArticle', 'setUniqueArticle', 'uniqueArticle'];
        yield ['article', 'getArticle', 'setArticle', 'article'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['category', 'getCategory', 'setCategory', 'category'];
        yield ['logo_fileid', 'getLogoFileid', 'setLogoFileid', 'logoFileid'];
        yield ['images_json', 'getImagesJson', 'setImagesJson', 'imagesJson'];
        yield ['images_count', 'getImagesCount', 'setImagesCount', 'imagesCount'];
        yield ['made_in', 'getMadeIn', 'setMadeIn', 'madeIn'];
        yield ['design_in', 'getDesignIn', 'setDesignIn', 'designIn'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['color', 'getColor', 'setColor', 'color'];
        yield ['color_code', 'getColorCode', 'setColorCode', 'colorCode'];
        yield ['age_from', 'getAgeFrom', 'setAgeFrom', 'ageFrom'];
        yield ['age_to', 'getAgeTo', 'setAgeTo', 'ageTo'];
        yield ['target', 'getTarget', 'setTarget', 'target'];
        yield ['photo', 'getPhoto', 'setPhoto', 'photo'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['brand_id', 'getBrandId', 'setBrandId', 'brandId'];
        yield ['section_id', 'getSectionId', 'setSectionId', 'sectionId'];
    }

    public function relations()
    {
        return [
            // here
            'brand' => [self::BELONGS_TO, 'MommyCom\Model\Db\BrandRecord', 'brand_id'],
            'views' => [self::HAS_ONE, ViewTrackingRecord::class, 'object_id', 'scopes' => [
                'objectType' => ViewsTracking::getObjectType(__CLASS__),
            ]],
            'section' => [self::BELONGS_TO, 'MommyCom\Model\Db\ProductSectionRecord', 'section_id'],
        ];
    }

    public function rules()
    {
        return [
            ['name, article, color', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name, brand_id, article, color', 'required'],

            ['color_code', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],
            ['color_code', 'in', 'range' => ProductColorCodes::instance()->getList(), 'message' => \Yii::t('common', 'Код цвета отсутствует') . ' ' . $this->color_code],
            ['section_id', 'numerical', 'integerOnly' => true],

            ['id, name, category, brand_id, section_id, article, age_from, age_to, images_count, color, color_code,
             made_in, design_in, target, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => '#',
            'name' => \Yii::t($category, 'Title'),
            'category' => \Yii::t($category, 'Category'),
            'section_id' => \Yii::t($category, 'Section'),

            'views' => \Yii::t($category, 'Views'),

            'article' => \Yii::t($category, 'Stock number'),

            'age_from' => \Yii::t($category, 'Age from'),
            'age_to' => \Yii::t($category, 'Age before'),

            'logo_fileid' => \Yii::t($category, 'Label'),
            'logo' => \Yii::t($category, 'Label'),
            'images_json' => \Yii::t($category, 'Images'),
            'images' => \Yii::t($category, 'Images'),
            'images_count' => \Yii::t($category, 'Qty. of images'),
            'description' => \Yii::t($category, 'Description'),
            'photo' => \Yii::t($category, 'Photo'),

            'color' => \Yii::t($category, 'Colour'),
            'color_code' => \Yii::t($category, 'Color code'),

            'made_in' => \Yii::t($category, 'Production'),
            'design_in' => \Yii::t($category, 'Design'),
            'target' => \Yii::t($category, 'Target audience'),

            'age' => \Yii::t($category, 'Age'),
            'ageRangeReplacement' => \Yii::t($category, 'Age'),
            'ageGroups' => \Yii::t($category, 'Age groups'),

            'created_at' => \Yii::t($category, 'Addition date'),
            'updated_at' => \Yii::t($category, 'Updating date'),
        ];
    }

    protected function _resolveUniqueArticle()
    {
        $this->unique_article = self::generateUniqueArticle($this->article, $this->brand_id, $this->color);
    }

    protected function _serializeTarget()
    {
        $this->target = !empty($this->target) && is_array($this->target) ? implode(',', $this->target) : '';
    }

    protected function _unserializeTarget()
    {
        $this->target = !empty($this->target) && is_string($this->target) ? explode(',', $this->target) : [];
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolveUniqueArticle();

        $this->_serializeTarget();
        if ($this->section_id == 0) {
            $this->section_id = self::bindingSectionByName($this->name);
        }
        if ($this->color !== '' && $this->color_code == 0) {
            $this->color_code = self::bindingColorCode($this->color);
        }
        return true;
    }

    public function afterFind()
    {
        $result = parent::afterFind();
        $this->_unserializeTarget();
        return $result;
    }

    /**
     * Генерирует уникальный внутренний артикль
     *
     * @param string $article
     * @param integer $brandId
     * @param string $color
     *
     * @return string
     */
    public static function generateUniqueArticle($article, $brandId, $color)
    {
        // fix types
        $article = Utf8::trim(Cast::toStr($article));
        $brandId = Cast::toStr(Cast::toUInt($brandId));
        $color = Utf8::trim(Cast::toStr($color));

        // hash it!
        return hash('sha256', $article . '_' . $brandId . '_' . $color);
    }

    ////////////// -- SEARCH API

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    public function onlyHasImages()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'images_count' . '>=' . Cast::toStr(0));
        return $this;
    }

    public function onlyHasLogo()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'logo_fileid' . '<>' . '""'); // not empty
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function color($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'color' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer|BrandRecord $value
     *
     * @return $this
     */
    public function brandId($value)
    {
        if ($value instanceof BrandRecord) {
            $value = $value->id;
        }

        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'brand_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function madeIn($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'made_in' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function designIn($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'design_in' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function uniqueArticle($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'unique_article' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function articleIs($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'article' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function photoIs($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'photo' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.made_in', $model->made_in, true);
        $criteria->compare($alias . '.design_in', $model->design_in, true);
        $criteria->compare($alias . '.article', $model->article, true);
        $criteria->compare($alias . '.color', $model->color, true);
        $criteria->compare($alias . '.color_code', $model->color_code);
        $criteria->compare($alias . '.section_id', $model->section_id);

        $criteria->compare($alias . '.age_from', '=' . $model->age_from);
        $criteria->compare($alias . '.age_to', '=' . $model->age_to);
        $criteria->compare($alias . '.images_count', '=' . $model->images_count);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        if (!empty($model->target)) {
            $criteria->addCondition('FIND_IN_SET(' . $model->dbConnection->quoteValue($model->target) . ', ' . $alias . '.' . 'target' . ')');
        }

        return $provider;
    }


    ////////////// -- ITEM API

    /**
     * @return FileStorageThumbnail[]
     */
    public function getImages()
    {
        $imageIds = Cast::toArr(json_decode($this->images_json, true, 2));
        $images = [];

        foreach ($imageIds as $id) {
            if (!empty($id) && is_string($id)) {
                $image = \Yii::app()->filestorage->get($id);
                if ($image instanceof Image) {
                    $images[$image->getEncodedId()] = new FileStorageThumbnail($image, [
                        // here thumbs
                    ]);
                }
            }
        }

        return $images;
    }

    /**
     * @param Image[]|FileStorageThumbnail[]|string[] $values
     *
     * @return static
     */
    public function setImages(array $values)
    {
        $sanitized = [];
        $currentImages = $this->getImages();

        foreach ($values as $index => $image) {
            if (is_string($image)) {
                $image = \Yii::app()->filestorage->get($image);
            }

            if ($image instanceof FileStorageThumbnail) {
                $image = $image->file;
            }

            if ($image instanceof Image) {
                $id = $image->getEncodedId();

                if (!$image->isNewFile() && !isset($currentImages[$id])) {
                    $image = $image->duplicate();
                    $id = $image->getEncodedId();
                }

                //файла нет тумбы есть
                if ($image->isExists()) {
                    $sanitized[$id] = $image;
                }
            }
        }
        $sanitized = array_slice($sanitized, 0, self::MAX_IMAGES_COUNT, true);

        // delete current images
        foreach ($currentImages as $id => $image) {
            if (!isset($sanitized[$id])) {
                if (!$image->isEmpty) {
                    $image->file->remove();
                }
            }
        }
        // end delete current

        $this->images_json = json_encode(array_keys($sanitized));
        $this->images_count = count($sanitized);
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsHasLogo()
    {
        return !$this->getLogo()->getIsEmpty();
    }

    /**
     * @return FileStorageThumbnail
     */
    public function getLogo()
    {
        return new FileStorageThumbnail($this->logo_fileid, [
            // here thumbs
        ]);
    }

    /**
     * @param Image|FileStorageThumbnail|string $value
     *
     * @return $this
     */
    public function setLogo($value)
    {
        if (is_string($value)) {
            $value = \Yii::app()->filestorage->get($value);
        }

        if ($value instanceof FileStorageThumbnail) {
            $value = $value->file;
        }

        if ($value instanceof CUploadedFile) {
            $value = \Yii::app()->filestorage->factoryFromLocalFile($value->tempName);
        }

        if ($value instanceof Image && $value->getEncodedId() != $this->logo_fileid) {
            if (!empty($this->logo_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->logo_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }
            }
            if (!$value->isNewFile()) {
                $value = $value->duplicate();
            }
            $this->logo_fileid = $value->getEncodedId();
        }

        return $this;
    }

    /**
     * @return FileStorageThumbnail[]
     */
    public function getPhotogalleryImages()
    {
        $images = $this->images;
        if ($this->isHasLogo) {
            $logo = $this->logo;
            $images = [$logo->getEncodedId() => $logo] + $images;
        }
        return array_slice($images, 0, self::MAX_IMAGES_COUNT + 1, true);
    }

    /**
     * @return array
     */
    public function getAgeRange()
    {
        return [$this->age_from, $this->age_to];
    }

    /**
     * @return string
     */
    public function getAgeRangeReplacement()
    {
        return AgeGroups::instance()->convertAgeRangeToString([$this->age_from, $this->age_to]);
    }

    /**
     * @return array
     */
    public function getAgeGroups()
    {
        return AgeGroups::instance()->rangeToGroups($this->getAgeRange());
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function descriptionIn($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'description' => $value,
        ]);
        return $this;
    }

    public function idFromTo($id)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'id' . '<=' . $id); // not empty
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function sectionId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'section_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $sectionIds
     *
     * @return $this
     */
    public function sectionIn($sectionIds)
    {
        $sectionIds = Cast::toUIntArr($sectionIds);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.section_id', $sectionIds);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function colorCode($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'color_code' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $colorCodes
     *
     * @return $this
     */
    public function colorCodeIn($colorCodes)
    {
        $colorCodes = Cast::toUIntArr($colorCodes);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.color_code', $colorCodes);
        return $this;
    }

    /**
     * Определение секции категории товара по кейвордсам дочерних категорий и названия товара
     *
     * @param $name
     *
     * @return int
     */
    public static function bindingSectionByName($name)
    {
        if (!empty($name)) {
            $sections = ArrayUtils::groupBy(ProductSectionRecord::model()->onlyChild()->findAll(), 'id');
            foreach ($sections as $sectionId => $productSectionItem) {
                $productSection = $productSectionItem[0];
                if (empty($productSection->keywords)) {
                    continue;
                }
                /** @var $productSection ProductSectionRecord */
                foreach ($productSection->keywords as $keyword) {
                    if (empty($keyword)) {
                        continue;
                    }
                    if (mb_stripos($name, $keyword) !== false) {
                        return $sectionId;
                    }
                }
            }
        }

        return 0;
    }

    /**
     * Определение кода цвета по названию цвета
     *
     * @param $colorName
     *
     * @return int
     */
    public static function bindingColorCode($colorName)
    {
        $wordEndingLength = 2; // чтобы убрать окончание “ый” у назв. цветов
        $colorProduct = mb_strtolower(trim($colorName));
        $colorProduct = mb_substr($colorProduct, 0, mb_strlen($colorProduct) - $wordEndingLength);
        $colorCodes = ProductColorCodes::instance()->getUnifiedLabels();
        foreach ($colorCodes as $colorCode => $color) {
            if (mb_substr($color, 0, mb_strlen($color) - $wordEndingLength) == $colorProduct) {
                return $colorCode;
            }
        }
        return 0;
    }
}
