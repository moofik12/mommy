<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class BrandAliasRecord
 *
 * @property-read integer $id
 * @property integer $brand_id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read BrandRecord $brand
 */
class BrandAliasRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return BrandRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['brand_id', 'getBrandId', 'setBrandId', 'brandId'];
    }

    public function tableName()
    {
        return 'brands_aliases';
    }

    public function relations()
    {
        return [
            'children' => [self::BELONGS_TO, 'MommyCom\Model\Db\BrandRecord', 'brand_id'],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name', 'required'],
            ['name', 'length', 'min' => 1, 'max' => 100],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function brandId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'brand_id' => $value,
        ]);
        return $this;
    }
}

