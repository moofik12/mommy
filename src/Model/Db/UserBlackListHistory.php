<?php

namespace MommyCom\Model\Db;

use ArrayAccess;
use CMapIterator;
use IteratorAggregate;

/**
 * Class UserBlackListHistory
 */
class UserBlackListHistory implements IteratorAggregate, ArrayAccess
{
    /** @var  int */
    public $timestamp;

    /** @var  int */
    public $person_id;

    /** @var  string */
    public $person_name;

    /** @var  int */
    public $status;

    /** @var  string */
    public $comment;

    /** @var  int */
    public $user_id;

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array_keys(get_class_vars(__CLASS__));
    }

    /**
     * @return array
     */
    public function getAttributeToWatch()
    {
        return ['status', 'comment', 'person_id', 'user_id'];
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStatusReplacement($default = '')
    {
        $statuses = UserBlackListRecord::statuses();
        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $default;
    }

    /**
     * Sets the attribute values in a massive way.
     *
     * @param array $values attribute values (name=>value) to be set.
     * A safe attribute is one that is associated with a validation rule in the current {@link scenario}.
     *
     * @see getSafeAttributeNames
     * @see attributeNames
     */
    public function setAttributes($values)
    {
        if (!is_array($values))
            return;
        $attributes = array_flip($this->attributeNames());
        foreach ($values as $name => $value) {
            if (isset($attributes[$name]))
                $this->$name = $value;
        }
    }

    /**
     * Returns all attribute values.
     *
     * @param array $names list of attributes whose value needs to be returned.
     * Defaults to null, meaning all attributes as listed in {@link attributeNames} will be returned.
     * If it is an array, only the attributes in the array will be returned.
     *
     * @return array attribute values (name=>value).
     */
    public function getAttributes($names = null)
    {
        $values = [];
        foreach ($this->attributeNames() as $name)
            $values[$name] = $this->$name;

        if (is_array($names)) {
            $values2 = [];
            foreach ($names as $name)
                $values2[$name] = isset($values[$name]) ? $values[$name] : null;
            return $values2;
        } else
            return $values;
    }

    /**
     * Returns an iterator for traversing the attributes in the model.
     * This method is required by the interface IteratorAggregate.
     *
     * @return CMapIterator an iterator for traversing the items in the list.
     */
    public function getIterator()
    {
        $attributes = $this->getAttributes();
        return new CMapIterator($attributes);
    }

    /**
     * Returns whether there is an element at the specified offset.
     * This method is required by the interface ArrayAccess.
     *
     * @param mixed $offset the offset to check on
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    /**
     * Returns the element at the specified offset.
     * This method is required by the interface ArrayAccess.
     *
     * @param integer $offset the offset to retrieve element.
     *
     * @return mixed the element at the offset, null if no element is found at the offset
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * Sets the element at the specified offset.
     * This method is required by the interface ArrayAccess.
     *
     * @param integer $offset the offset to set element
     * @param mixed $item the element value
     */
    public function offsetSet($offset, $item)
    {
        $this->$offset = $item;
    }

    /**
     * Unsets the element at the specified offset.
     * This method is required by the interface ArrayAccess.
     *
     * @param mixed $offset the offset to unset element
     */
    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }
}
