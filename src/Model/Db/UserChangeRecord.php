<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * class UserChangeRecord
 *
 * @param string $className
 *
 * @property-read int $id
 * @property int $user_id
 * @property string $email
 * @property string $telephone
 * @property int $updated_at
 * @property int $created_at
 */
class UserChangeRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return UserChangeRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['telephone', 'getTelephone', 'setTelephone', 'telephone'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'users_changes';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['user_id', 'numerical', 'integerOnly' => true],

            ['email', 'length', 'max' => 128],
            ['telephone', 'length', 'max' => 20],

            //grid
            ['id, user_id, email, telephone', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'user_id' => Translator::t('User'),
            'email' => Translator::t('E-Mail'),
            'telephone' => Translator::t('Phone number'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.email', $model->email, true);
        $criteria->compare($alias . '.telephone', $model->telephone, true);

        return $provider;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param $values
     *
     * @return $this
     */
    public function emailIn($values)
    {
        $alias = $this->getTableAlias();
        $values = Cast::toStrArr($values);

        $this->getDbCriteria()->addInCondition($alias . '.email', $values);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function emailLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.email', $value, true, 'OR');
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephoneLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.telephone', $value, true, 'OR');
        return $this;
    }

} 
