<?php

namespace MommyCom\Model\Db;

use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserBonuspointsRecord
 * Класс начислений и растрат бонусов пользователя
 *
 * @author scriptbunny
 * @property-read integer $id
 * @property integer $user_id
 * @property integer $type
 * @property integer $points
 * @property string $message
 * @property integer $admin_id
 * @property boolean $is_custom
 * @property string $custom_message
 * @property integer $expire_at
 * @property string $expireAtString дата\время начала строкой
 * @property integer $is_expiration_processed произведено списание бонуса
 * @property integer $is_user_notified произведено списание бонуса
 * @proprty-read string $typeReplacement
 * @property integer $created_at
 * @property integer $updated_at
 * @property UserRecord $user
 * @property AdminUserRecord $admin
 */
class UserBonuspointsRecord extends DoctrineActiveRecord
{
    //ВНИМАНИЕ!!! Логика списывания находится в классе ShopBonusPoints, перед тем как добавлять тип, убедитесь что он учтен в ShopBonusPoints
    const TYPE_INNER = 0;
    const TYPE_PRESENT = 1;

    const MAX_BONUSPOINTS_PER_TRANSFER = 10000;
    const BONUSPOINTS_BUY_PERCENTAGE = 1; // 100%

    /**
     * @param string $className
     *
     * @return UserBonuspointsRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['points', 'getPoints', 'setPoints', 'points'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['message', 'getMessage', 'setMessage', 'message'];
        yield ['is_custom', 'isCustom', 'setIsCustom', 'isCustom'];
        yield ['custom_message', 'getCustomMessage', 'setCustomMessage', 'customMessage'];
        yield ['expire_at', 'getExpireAt', 'setExpireAt', 'expireAt'];
        yield ['is_expiration_processed', 'isExpirationProcessed', 'setIsExpirationProcessed', 'isExpirationProcessed'];
        yield ['is_user_notified', 'isUserNotified', 'setIsUserNotified', 'isUserNotified'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    public function tableName()
    {
        return 'users_bonuspoints';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id, points', 'required'],
            ['user_id, points', 'numerical', 'integerOnly' => true],
            ['type', 'in', 'range' => self::getTypes(false)],

            ['is_expiration_processed', 'boolean'],
            ['is_user_notified', 'boolean'],

            ['expireAtString', 'length'],

            ['points', 'numerical', 'min' => -self::MAX_BONUSPOINTS_PER_TRANSFER, 'max' => self::MAX_BONUSPOINTS_PER_TRANSFER],
            ['points', 'compare', 'compareValue' => 0, 'operator' => '!='],

            ['expire_at', 'numerical', 'integerOnly' => true],
            ['expire_at', 'validateExpire'], //должен после валидации self::points

            ['message, custom_message', 'length', 'min' => 0, 'max' => 250],

            ['id, type, user_id, points, message, custom_message, is_custom, created_at, updated_at, is_expiration_processed, is_user_notified',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'user_id' => Translator::t('User'),
            'points' => Translator::t('Bonuses'),
            'type' => Translator::t('Bonus type'),
            'message' => Translator::t('Message'),
            'is_custom' => Translator::t('Created manually'),
            'custom_message' => Translator::t('Custom message'),
            'admin_id' => Translator::t('Administrator'),
            'expire_at' => Translator::t('Validity period '),
            'is_expiration_processed' => Translator::t('Произведено списание бонуса'),
            'is_user_notified' => Translator::t('Notification of bonus debited'),

            'expireAtString' => Translator::t('Validity period'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.type', $model->type);
        $criteria->compare($alias . '.is_custom', $model->is_custom);
        $criteria->compare($alias . '.points', $model->points);
        $criteria->compare($alias . '.message', $model->message, true);
        $criteria->compare($alias . '.custom_message', $model->custom_message, true);
        $criteria->compare($alias . '.is_user_notified', $model->is_user_notified);
        $criteria->compare($alias . '.is_expiration_processed', $model->is_expiration_processed);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateExpire($attribute, $params)
    {
        $value = $this->$attribute;
        /*if ($this->type == self::TYPE_INNER && $value > 0) {
            if (strtotime('01.09.2015') < $this->created_at) {
                $this->addError($attribute, 'Срок действия бонуса недоступен для типа: ' . $this->getTypeReplacement());
            }

        } else*/
        if ($this->type == self::TYPE_PRESENT && $value == 0 && $this->points >= 0) {
            $this->addError($attribute, Translator::t('Срок действия бонуса не может быть пустым для типа: ') . $this->getTypeReplacement());
        }
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getExpireAtString($format = '%d.%m.%Y %H:%M')
    {
        return $this->expire_at > 0 ? strftime($format, $this->expire_at) : '';
    }

    /**
     * @param string $datetime
     *
     * @return $this
     */
    public function setExpireAtString($datetime)
    {
        $time = strtotime($datetime);
        if ($time) {
            $this->setExpireAt($time);
        }
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function typeIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.type' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function userIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $values);

        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function isCustom($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_custom' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function messageLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.message', $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function customMessageLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.custom_message', $value);
        return $this;
    }

    /**
     * @return $this
     */
    public function notExpired()
    {
        $alias = $this->getTableAlias();
        $time = time();
        $this->getDbCriteria()->addCondition("$alias.expire_at=0 OR $alias.expire_at>$time");

        return $this;
    }

    /**
     * @return $this
     */
    public function onlyExpired()
    {
        $alias = $this->getTableAlias();
        $time = time();
        $this->getDbCriteria()->addCondition("$alias.expire_at>0 AND $alias.expire_at<$time");

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function isUserNotified($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_user_notified' => Cast::toUInt($value),
        ]);

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function expirationProcessed($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_expiration_processed' => (int)$value,
        ]);

        return $this;
    }

    /**
     * @param int|DateTime $time
     */
    public function setExpireAt($time)
    {
        $dataTime = new DateTime();

        if (is_integer($time)) {
            $dataTime->setTimestamp($time);
        } elseif ($time instanceof DateTime) {
            $dataTime->setTimestamp($time->getTimestamp());
        }

        $dataTime->modify('tomorrow -1 second');
        $this->expire_at = $dataTime->getTimestamp();
    }

    /**
     * @param bool|true $replacements
     *
     * @return array
     */
    public static function getTypes($replacements = true)
    {
        $types = [
            self::TYPE_INNER => Translator::t('Inner'),
            self::TYPE_PRESENT => Translator::t('Gift'),
        ];

        return $replacements ? $types : array_keys($types);
    }

    /**
     * @param string|bool|false $default
     *
     * @return string
     */
    public function getTypeReplacement($default = false)
    {
        if ($default === false) {
            $default = $this->type;
        }

        $replacements = self::getTypes();
        return isset($replacements[$this->type]) ? $replacements[$this->type] : $default;
    }

    /** API */

    /**
     * @param int|bool $time
     *
     * @return bool
     */
    public function isExpired($time = false)
    {
        $time = $time ?: time();

        return $this->expire_at > 0 && $this->expire_at < $time;
    }

    /**
     * @return bool
     */
    public function isLimitedTime()
    {
        return $this->expire_at > 0;
    }

    /**
     * @return $this
     */
    public function onlyPlusPoints()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition("$alias.points>0");

        return $this;
    }
}
