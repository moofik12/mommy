<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class AdminAuthlogRecord
 *
 * @property-read integer $id
 * @property string $login логин
 * @property integer $user_id ID администратора
 * @property boolean $is_success
 * @property string $comment
 * @property string $ip IP с которого был вход
 * @property string $user_agent дополнительная информация
 * @property integer $created_at
 * @property integer $updated_at
 */
class AdminAuthlogRecord extends DoctrineActiveRecord
{
    const SUCCESS_TRUE = 1;
    const SUCCESS_FALSE = 0;

    //для поиска в grid
    public $searchDateStart;
    public $searchDateEnd;
    //для хранения даты выбора при перезагрузке
    public $searchDateRangeInput;

    /**
     * @param string $className
     *
     * @return AdminAuthlogRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['login', 'getLogin', 'setLogin', 'login'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['is_success', 'isSuccess', 'setIsSuccess', 'isSuccess'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
        yield ['user_agent', 'getUserAgent', 'setUserAgent', 'userAgent'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    public function tableName()
    {
        return 'admins_authlogs';
    }

    public function relations()
    {
        return [];
    }

    public function rules()
    {
        return [
            //array('login', 'required'),
            ['login, ip', 'length', 'max' => 45],

            ['comment', 'length', 'max' => 100],

            ['login, ip', 'length', 'max' => 45],

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],

            ['is_success', 'boolean'],

            ['ip, login, comment, is_success, user_agent, searchDateStart, searchDateEnd, searchDateRangeInput',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'login' => Translator::t('Login'),
            'is_success' => Translator::t('Signed in'),
            'comment' => Translator::t('Comments'),
            //'user_agent' => '',

            'created_at' => Translator::t('Date'),
            'updated_at' => Translator::t('Updated'),

            'searchDateRangeInput' => Translator::t('Period'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.ip', $provider->model->ip, true);
        $criteria->compare($alias . '.login', $provider->model->login, true);
        $criteria->compare($alias . '.comment', $provider->model->comment, true);
        $criteria->compare($alias . '.user_agent', $provider->model->user_agent, true);
        $criteria->compare($alias . '.is_success', $provider->model->is_success);

        //для поиска в grid
        $criteria->compare($alias . '.created_at', '>=' . $provider->model->searchDateStart);
        $criteria->compare($alias . '.created_at', '<=' . $provider->model->searchDateEnd);

        return $provider;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function login($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'login' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function ip($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'ip' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function isSuccess($value = true)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_success' => $value,
        ]);
        return $this;
    }

}
