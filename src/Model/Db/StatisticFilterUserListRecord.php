<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

/**
 * Class StatisticFilterUserListRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property integer $total_items //кол-во элементов в списке
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read StatisticFilterUserRecord $items
 * @property-read int $countItems
 */
class StatisticFilterUserListRecord extends DoctrineActiveRecord
{
    /**
     * Подсчитать кол-во элементов в списке
     *
     * @var bool
     */
    public $refreshCountItems = false;

    /**
     * @param string $className
     *
     * @return StatisticFilterUserListRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['total_items', 'getTotalItems', 'setTotalItems', 'totalItems'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    public function tableName()
    {
        return 'statistic_filter_users_list';
    }

    public function relations()
    {
        return [
            'items' => [self::HAS_MANY, 'MommyCom\Model\Db\StatisticFilterUserRecord', 'list_id'],
            'countItems' => [self::STAT, 'MommyCom\Model\Db\StatisticFilterUserRecord', 'list_id'],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name', 'length', 'max' => 127, 'min' => 5, 'allowEmpty' => false],
            ['name', 'unique'],
            ['total_items', 'numerical', 'integerOnly' => true],

            ['name', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'name' => Translator::t('Name'),
            'total_items' => Translator::t('Qty'),

            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model StatisticFilterUserListRecord */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    protected function afterDelete()
    {
        StatisticFilterUserRecord::model()->deleteAll('list_id=:list', [':list' => $this->id]);
        parent::afterDelete();
    }

    /**
     * @return bool
     */
    protected function beforeSave()
    {
        if ($this->refreshCountItems) {
            $this->total_items = $this->countItems;
        }

        return parent::beforeSave();
    }

    public function clear()
    {
        if (!$this->id) {
            return false;
        }

        StatisticFilterUserRecord::model()->deleteAll('list_id=:list', [':list' => $this->id]);
        return true;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameIs($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.name' => $value,
        ]);
        return $this;
    }
}
