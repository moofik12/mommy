<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use CHtml;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * class CallbackRecord
 *
 * @property-read int $id
 * @property int $user_id
 * @property string $telephone
 * @property int $status
 * @property string $status_history_json
 * @property integer $processing_admin_id админ который выполняет задачу
 * @property integer $processing_update_last_at UNIXTIME последней активности
 * @property integer $next_callback_at
 * @property string $comment_callcenter комментарий при выполнении
 * @property $updated_at
 * @property $created_at
 * @property-read UserRecord $user
 * @property-read AdminUserRecord $processingAdmin
 */
class CallbackRecord extends DoctrineActiveRecord
{
    const STATUS_NOT_PROCESSED = 0,
        STATUS_PROCESSED = 1,
        STATUS_POSTPONED = 2,
        STATUS_CANCELED = 3;

    /**
     * время в сек. при котром ститается что админ редактирует заказ
     */
    const TIME_PROCESSING_VALID = 300;

    /**
     * @param string $className
     *
     * @return CallbackRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['telephone', 'getTelephone', 'setTelephone', 'telephone'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['comment_callcenter', 'getCommentCallcenter', 'setCommentCallcenter', 'commentCallcenter'];
        yield ['status_history_json', 'getStatusHistoryJson', 'setStatusHistoryJson', 'statusHistoryJson'];
        yield ['processing_update_last_at', 'getProcessingUpdateLastAt', 'setProcessingUpdateLastAt', 'processingUpdateLastAt'];
        yield ['next_callback_at', 'getNextCallbackAt', 'setNextCallbackAt', 'nextCallbackAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['processing_admin_id', 'getProcessingAdminId', 'setProcessingAdminId', 'processingAdminId'];
    }

    public function tableName()
    {
        return 'callbacks';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'processingAdmin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'processing_admin_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['user_id, telephone', 'required'],
            ['telephone', 'length', 'max' => 20],
            ['status', 'in', 'range' => [
                self::STATUS_NOT_PROCESSED,
                self::STATUS_PROCESSED,
                self::STATUS_POSTPONED,
                self::STATUS_CANCELED,
            ]],
            ['processing_admin_id, processing_update_last_at, next_callback_at', 'numerical', 'integerOnly' => true],
            ['comment_callcenter', 'length'],

            ['id, user_id, telephone, status, processing_admin_id, comment_callcenter', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'user_id' => \Yii::t($category, 'User'),
            'status' => \Yii::t($category, 'Status'),
            'telephone' => \Yii::t($category, 'Phone'),
            'comment_callcenter' => \Yii::t($category, 'Comment'),
        ];
    }

    public function beforeSave()
    {
        $result = parent::beforeSave();

        $this->_updateStatusChangeStack();

        return $result;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'));
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.status', $model->status);

        $criteria->compare($alias . '.telephone', $model->telephone, true);
        $criteria->compare($alias . '.comment_callcenter', $model->comment_callcenter, true);

        return $provider;
    }

    /**
     * @param bool $active , включать в очередь звонки в обработке (не логично но надо)
     *
     * @return $this
     */
    public function queue($active = false)
    {
        $active = Cast::toBool($active);
        $alias = $this->getTableAlias();
        $time = time();

        $this->dbCriteria
            ->addColumnCondition([
                "$alias.status" => self::STATUS_NOT_PROCESSED,
            ])
            ->addColumnCondition([
                "$alias.status" => self::STATUS_POSTPONED,
                "$alias.next_callback_at <" => $time,
            ], 'AND', 'OR')
            ->mergeWith([
                'order' => "IF($alias.next_callback_at > $alias.created_at, $alias.next_callback_at, $alias.created_at) ASC",
            ]);

        if (!$active) {
            $this->processingNotActive();
        }

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function notIdIn(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.id', $value);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephone($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.telephone' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephoneLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.telephone', $value, true);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function userIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $values);

        return $this;
    }

    /**
     * отложены добавляет ИЛИ к выражению
     *
     * @return $this
     */
    public function postponed()
    {
        $alias = $this->getTableAlias();
        $time = time();

        $this->dbCriteria
            ->addColumnCondition([
                "$alias.status" => self::STATUS_POSTPONED,
                "$alias.next_callback_at >" => $time,
            ], 'AND', 'OR');

        return $this;
    }

    /**
     * @param int $adminId
     *
     * @return bool
     */
    public function bindToAdmin(int $adminId)
    {
        $timestamp = time();

        $attributes = ['processing_admin_id' => $adminId, 'processing_update_last_at' => $timestamp];

        $condition = 'processing_admin_id = 0';
        $condition .= ' OR processing_admin_id = :processing_admin_id';
        $condition .= ' OR processing_update_last_at < :valid_time';

        $params = [
            'processing_admin_id' => $adminId,
            'valid_time' => $timestamp - self::TIME_PROCESSING_VALID,
        ];

        $result = $this->updateByPk($this->id, $attributes, $condition, $params);

        if ($result) {
            $this->refresh();
        }

        return (bool)$result;
    }

    /**
     * @return bool
     */
    public function unbindFromAdmin()
    {
        $result = $this->updateByPk($this->id, ['processing_admin_id' => 0, 'processing_update_last_at' => 0]);

        return Cast::toBool($result);
    }

    /**
     * исключаются редактируемые заказы self::admin_id > 0 и admin_update_last_at > self::TIME_PROCESSING_VALID
     *
     * @return static
     */
    public function processingNotActive()
    {
        $alias = $this->getTableAlias();
        $condition = [
            $alias . '.processing_admin_id' => 0,
            $alias . '.processing_update_last_at <' => time() - self::TIME_PROCESSING_VALID,
        ];

        $this->getDbCriteria()->addColumnCondition($condition, 'OR');
        return $this;
    }

    /**
     * $value - оператор
     *
     * @param null | integer $value , при NULL возвращает все заказы которые редактируются
     *
     * @return $this
     */
    public function processingActive($value = null)
    {
        $alias = $this->getTableAlias();

        $condition = [
            $alias . '.processing_admin_id >' => 1,
            $alias . '.processing_update_last_at >' => time() - self::TIME_PROCESSING_VALID,
        ];

        if ($value !== null) {
            $value = Cast::toUInt($value);
            $condition = [$alias . '.processing_admin_id' => $value];
        }

        $this->getDbCriteria()->addColumnCondition($condition);
        return $this;
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateStatusChangeStack()
    {
        $statusHistory = $this->getProcessingStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->status) {
            $now = time();
            $addToHistory = [
                'status' => $this->status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
        }
        ksort($statusHistory);

        $this->status_history_json = json_encode($statusHistory);
    }

    /**
     * @return array
     */
    public function getProcessingStatusHistory()
    {
        return Cast::toArr(json_decode($this->status_history_json, true, 3));
    }


    /** ITEMS API */

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_NOT_PROCESSED => \Yii::t($category, 'Not processed'),
            self::STATUS_PROCESSED => \Yii::t($category, 'Processed'),
            self::STATUS_POSTPONED => \Yii::t($category, 'Postponed'),
            self::STATUS_CANCELED => \Yii::t($category, 'Cancelled'),
        ];
    }

    /**
     * @return string|int
     */
    public function statusReplacement()
    {
        return CHtml::value(self::statusReplacements(), $this->status);
    }

    /**
     * @return bool
     */
    public function isProcessingActive()
    {
        $processingActiveTime = time() - self::TIME_PROCESSING_VALID;
        return $this->processing_admin_id > 0 && $this->processing_update_last_at > $processingActiveTime;
    }

    /**
     * позиция пользователя в очереди (не учитываются отложенные, и в обработке)
     *
     * @param $value
     *
     * @return int position in queue
     */
    public function positionUserQueue($value)
    {
        $value = Cast::toUInt($value);
        $table = $this->tableName();
        $statusNotProcessed = self::STATUS_NOT_PROCESSED;
        $statusPostponed = self::STATUS_POSTPONED;
        $time = time();
        $processActiveTime = $time - self::TIME_PROCESSING_VALID;

        $sql = "SELECT t.n FROM (SELECT @recNo:=0) init,(SELECT @recNo:=@recNo+1 n, user_id FROM `$table`
            WHERE (status='$statusNotProcessed' OR (status='$statusPostponed' AND next_callback_at<$time))
            AND (processing_admin_id=0 OR processing_update_last_at<$processActiveTime)
            ORDER BY IF(next_callback_at > created_at, next_callback_at, created_at) ASC) t WHERE t.user_id='$value'";

        $result = $this->getDbConnection()->createCommand($sql)->queryScalar();

        return Cast::toUInt($result);
    }

    /**
     * есть и активна заявка пользователя на обратный звонок
     *
     * @param int $value user_id
     *
     * @return bool
     */
    public function isRequestUser($value)
    {
        $value = Cast::toUInt($value);

        $result = $this->queue(true)->postponed()->userId($value)->find();

        return !!$result;
    }
} 
