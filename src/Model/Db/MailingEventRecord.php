<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;

/**
 * Class MailingEventRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property integer $code
 * @property integer $priority
 * @property integer $created_at
 * @property integer $updated_at
 * @mixin Timestampable
 */
class MailingEventRecord extends DoctrineActiveRecord
{
    public const UNFINISHED_REGISTRATION_REGISTER_BUTTON_CLICK = 'unfinished_registration_register_button_click';

    public const UNFINISHED_REGISTRATION_SURFING = 'unfinished_registration_surfing';

    public const UNFINISHED_REGISTRATION_FILLING_EMAIL = 'unfinished_registration_filling_email';

    public const FINISHED_REGISTRATION = 'finished_registration';

    public const LOST_CART = 'lost_cart';

    public const VIEW_SECTION = 'view_section';

    /**
     * @var string - приоритет, начиная с которого можно пытаться подписать пользователя на рассылки
     */
    private const SUBSCRIPTION_PRIORITY = 2;

    /**
     * @param string $className
     *
     * @return MailingEventRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['code', 'getCode', 'setCode', 'code'];
        yield ['priority', 'getPriority', 'setPriority', 'priority'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return bool|string
     */
    public function tableName()
    {
        return 'mailing_events';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'data' => [self::HAS_MANY, MailingDataRecord::class, 'distribution_event_id'],
        ];
    }

    public function canSubscribe()
    {
        return $this->priority >= self::SUBSCRIPTION_PRIORITY;
    }

    /**
     * @param int $eventName
     *
     * @return $this
     */
    public function withName($eventName)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "name='" . $eventName . "'",
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function getPrioritiesArray()
    {
        $events = MailingEventRecord::findAll();

        $eventsArray = [];
        foreach ($events as $event) {
            $eventsArray[$event->priority] = $event;
        }

        return $eventsArray;
    }
}
