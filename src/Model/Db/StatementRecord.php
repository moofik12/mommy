<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class StatementRecord
 *
 * @property-read integer $id
 * @property string $service_statement_id номер реестра по базе сервиса
 * @property string $service_statement_ref ID реестра по базе сервиса
 * @property integer $user_id админ создавший реестр
 * @property integer $status
 * @property integer $delivery_type
 * @property string $synchronization_error_message
 * @property integer $supplier_id
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read AdminUserRecord $user
 * @property-read SupplierRecord $supplier
 * @property-read StatementOrderRecord[] $positions
 * @property-read integer $positionCount
 * @property-read StatementOrderRecord[] $positionsMailedOnly
 * @property-read float $totalPrice
 * @property-read float $totalDeliveryPrice
 */
class StatementRecord extends DoctrineActiveRecord
{
    const STATUS_UNCONFIRMED = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_REFRESH = 2;
    const STATUS_REFRESHING = 4;
    const STATUS_SYNCHRONIZATION_ERROR = 3;

    const SUPPLIER_ID_NONE = 0;

    /**
     * @param string $className
     *
     * @return StatementRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['delivery_type', 'getDeliveryType', 'setDeliveryType', 'deliveryType'];
        yield ['service_statement_id', 'getServiceStatementId', 'setServiceStatementId', 'serviceStatementId'];
        yield ['service_statement_ref', 'getServiceStatementRef', 'setServiceStatementRef', 'serviceStatementRef'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['synchronization_error_message', 'getSynchronizationErrorMessage', 'setSynchronizationErrorMessage', 'synchronizationErrorMessage'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
    }

    public function tableName()
    {
        return 'statements';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],

            'positions' => [self::HAS_MANY, 'MommyCom\Model\Db\StatementOrderRecord', 'statement_id'],
            'positionCount' => [self::STAT, 'MommyCom\Model\Db\StatementOrderRecord', 'statement_id'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id,', 'numerical', 'integerOnly' => true],
            ['service_statement_id', 'length', 'max' => 24],
            ['service_statement_ref', 'length', 'max' => 36],

            ['status', 'in', 'range' => [
                self::STATUS_UNCONFIRMED,
                self::STATUS_CONFIRMED,
                self::STATUS_REFRESH,
                self::STATUS_SYNCHRONIZATION_ERROR,
            ]],

            ['supplier_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],

            ['synchronization_error_message', 'length'],
            ['delivery_type', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'delivery_type'],

            ['id, service_statement_id, service_statement_ref, user_id, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        if ($this->isNewRecord) {
            try { // for console app
                $this->user_id = \Yii::app()->user->id;
            } catch (CException $e) {
            }
        }

        if ($this->status == self::STATUS_REFRESH && $this->synchronization_error_message) {
            $this->synchronization_error_message = '';
        }

        return true;
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'user_id' => \Yii::t($category, 'User'),
            'service_statement_id' => \Yii::t($category, 'External id of Ret.Doc.'),
            'service_statement_ref' => \Yii::t($category, 'External id of Ret.Doc. in the system'),
            'delivery_type' => \Yii::t($category, 'Type of delivery'),
            'status' => \Yii::t($category, 'Status'),
            'synchronization_error_message' => \Yii::t($category, 'Error creating act'),
            'supplier_id' => \Yii::t($category, 'Supplier'),
            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),

            'positionCount' => \Yii::t($category, 'The orders'),
            'totalPrice' => \Yii::t($category, 'Total cost'),
            'totalDeliveryPrice' => \Yii::t($category, 'Total Shipping Cost'),
        ];
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function serviceStatementId($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.service_statement_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function deliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.delivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function deliveryTypes(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.delivery_type', $value);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierIdIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.supplier_id' => $value]);
        return $this;
    }

    /**
     * @return static
     */
    public function suppliersOwner()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.supplier_id>' . self::SUPPLIER_ID_NONE);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.service_statement_id', '=' . $model->service_statement_id);
        $criteria->compare($alias . '.service_statement_ref', '=' . $model->service_statement_id);
        $criteria->compare($alias . '.user_id', '=' . $model->user_id);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /// item api

    /**
     * @return array
     */
    public function deliveryTypeReplacements()
    {
        return OrderRecord::deliveryTypeReplacements();
    }

    /**
     * @return array
     */
    public function getPositionsMailedOnly()
    {
        $result = [];
        foreach ($this->positions as $position) {
            if ($position->order->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED) {
                $result[] = $position;
            }
        }
        return $result;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        $result = 0.0;
        foreach ($this->positions as $position) {
            $result += $position->order->price_total;
        }
        return $result;
    }

    /**
     * @return float
     */
    public function getTotalDeliveryPrice()
    {
        $result = 0.0;
        foreach ($this->positions as $position) {
            $result += $position->order->getDeliveryPrice();
        }
        return $result;
    }
}
