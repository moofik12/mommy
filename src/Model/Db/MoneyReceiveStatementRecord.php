<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Service\Deprecated\Delivery\DeliveryCountryGroups;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class MoneyReceiveStatementRecord
 *
 * @property-read int $id
 * @property int $delivery_type
 * @property int $user_id
 * @property float $money_expected
 * @property float $money_realReturn DocumentationreadyToReturn
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property-read MoneyReceiveStatementOrderRecord[] $positions
 * @property-read AdminUserRecord $user
 */
class MoneyReceiveStatementRecord extends DoctrineActiveRecord
{
    /**
     * время в сек. в течении которого возможно редактирование Акта (с начала создания)
     */
    const TIME_ENABLE_EDIT = 86400; //day

    /**
     * @param string $className
     *
     * @return MoneyReceiveStatementRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['delivery_type', 'getDeliveryType', 'setDeliveryType', 'deliveryType'];
        yield ['money_expected', 'getMoneyExpected', 'setMoneyExpected', 'moneyExpected'];
        yield ['money_real', 'getMoneyReal', 'setMoneyReal', 'moneyReal'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'money_receive_statements';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'positions' => [self::HAS_MANY, 'MommyCom\Model\Db\MoneyReceiveStatementOrderRecord', 'statement_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['money_expected, money_real', 'required'],
            ['money_expected, money_real', 'filter', 'filter' => [Cast::class, 'toUFloat']],
            ['money_expected, money_real', 'length', 'min' => 1, 'max' => 10000000000], // ten million is enough
            ['delivery_type', 'in', 'range' => DeliveryCountryGroups::instance()->getDelivery()->getList(true)],

            ['delivery_type, user_id, money_expected, money_real', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $user = \Yii::app()->user->getModel();
        if ($this->isNewRecord && $user) {
            $this->user_id = $user->id;
        }

        return $result;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->positions as $position) {
            $position->delete();
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => \Yii::t('common', 'Created'),
            'money_expected' => \Yii::t('common', 'Expected amount'),
            'money_real' => \Yii::t('common', 'Amount'),
            'delivery_type' => \Yii::t('common', 'Type of delivery'),

            'created_at' => \Yii::t('common', 'date'),
            'updated_at' => \Yii::t('common', 'Updated on'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.money_expected', $model->money_expected, true);
        $criteria->compare($alias . '.money_real', $model->money_real, true);
        $criteria->compare($alias . '.delivery_type', $model->delivery_type);

        return $provider;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /** API */

    /**
     * @return bool
     */
    public function isAvailableEditing()
    {
        $timeCreate = Cast::toUInt($this->created_at);

        return $timeCreate + self::TIME_ENABLE_EDIT > time();
    }

} 
