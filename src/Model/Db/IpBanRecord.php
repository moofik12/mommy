<?php

namespace MommyCom\Model\Db;

use CHtml;
use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class IpBanRecord
 *
 * @property-read integer $id
 * @property string $ip
 * @property string $comment
 * @property integer $section
 * @property integer $banned_to
 * @property integer $created_at
 * @property integer $updated_at
 */
class IpBanRecord extends DoctrineActiveRecord
{
    const SECTION_ALL = 0,
        SECTION_AUTH_ADMIN = 1,
        SECTION_AUTH_SITE = 2,
        SECTION_CART = 3;

    const FILTER_TIME_ALL = 0,
        FILTER_TIME_ACTIVE = 1,
        FILTER_TIME_PASSED = 2;

    /**
     * @var integer
     */
    public $filterTime = self::FILTER_TIME_ACTIVE;

    //для поиска в grid
    public $searchDateStart;
    public $searchDateEnd;
    //для хранения даты выбора при перезагрузке
    public $searchDateRangeInput;

    /**
     * @param string $className
     *
     * @return IpBanRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['section', 'getSection', 'setSection', 'section'];
        yield ['banned_to', 'getBannedTo', 'setBannedTo', 'bannedTo'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    public function tableName()
    {
        return 'ipbans';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['ip, section', 'required'],
            ['ip', 'length', 'min' => 1, 'max' => 60],

            ['comment', 'length', 'max' => 255],

            ['section', 'in', 'range' => [
                self::SECTION_ALL,
                self::SECTION_AUTH_ADMIN,
                self::SECTION_AUTH_SITE,
                self::SECTION_CART,
            ]],

            ['ip, section, comment, filterTime, searchDateStart, searchDateEnd, searchDateRangeInput',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $time = new DateTime();
        //время на которое будет заблокирован IP
        switch ($this->section) {
            case self::SECTION_ALL:
                $this->banned_to = $time->modify('+1 day')->getTimestamp();
                break;

            case self::SECTION_AUTH_ADMIN:
                $this->banned_to = $time->modify('+1 day')->getTimestamp();
                break;

            case self::SECTION_AUTH_SITE:
                $this->banned_to = $time->modify('+1 day')->getTimestamp();
                break;

            case self::SECTION_CART:
                $this->banned_to = $time->modify('+1 day')->getTimestamp();
                break;

            default:
                $this->banned_to = $time->modify('+1 day')->getTimestamp();
                break;
        }

        return true;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'section' => Translator::t('Ban type'),
            'comment' => Translator::t('Ban reason'),
            'banned_to' => Translator::t('Ban end date'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'filterTime' => Translator::t('Time filter'),
            'searchDateRangeInput' => Translator::t('Period'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.ip', $provider->model->ip, true);
        $criteria->compare($alias . '.section', $provider->model->section);
        $criteria->compare($alias . '.comment', $provider->model->comment, true);

        switch ($provider->model->filterTime) {
            case self::FILTER_TIME_ACTIVE:
                $criteria->compare($alias . '.banned_to', '>=' . time());
                break;

            case self::FILTER_TIME_PASSED:
                $criteria->compare($alias . '.banned_to', '<' . time());
                break;

            default:
                break;
        }

        //для поиска в grid
        $criteria->compare($alias . '.created_at', '>=' . $provider->model->searchDateStart);
        $criteria->compare($alias . '.created_at', '<=' . $provider->model->searchDateEnd);

        return $provider;
    }

    /**
     * @param $ip
     *
     * @return static
     */
    public function bannedIp($ip)
    {
        $ip = strval($ip);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'ip' => $ip,
            $alias . '.banned_to >' => time(),
        ]);

        return $this;
    }

    /**
     * by default section self::SECTION_ALL
     *
     * @param integer $section self::SECTION_ALL |    self::SECTION_AUTH_ADMIN | self::SECTION_AUTH_SITE | self::SECTION_CART
     *
     * @return static
     */
    public function section($section)
    {
        $section = intval($section);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.' . 'section',
            $section === 0 ? [self::SECTION_ALL] : [self::SECTION_ALL, $section]
        );

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function ip($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'ip' => $value,
        ]);
        return $this;
    }

    /**
     * @return array
     */
    public function getSectionReplacements()
    {
        return [
            self::SECTION_ALL => Translator::t('Access denied'),
            self::SECTION_AUTH_ADMIN => Translator::t('Admin access denied'),
            self::SECTION_AUTH_SITE => Translator::t('Account access denied'),
            self::SECTION_CART => Translator::t('Addding to shopping cart denied'),
        ];
    }

    /**
     * @param bool $section
     *
     * @return mixed
     */
    public function getSectionReplacement($section = false)
    {
        if ($section === false) {
            return CHtml::value($this->getSectionReplacements(), $this->section);
        }

        return CHtml::value($this->getSectionReplacements(), $section);
    }

    /**
     * @return array
     */
    public function getFilterReplacement()
    {
        return [
            self::FILTER_TIME_ALL => Translator::t('All'),
            self::FILTER_TIME_ACTIVE => Translator::t('Active'),
            self::FILTER_TIME_PASSED => Translator::t('Passed'),
        ];
    }

    /**
     * @param $ip
     *
     * @return array
     */
    public function getBannedKeys($ip)
    {
        $ip = Cast::toStr($ip);
        $result = $this->cache(60)->bannedIp($ip)->findColumnDistinct('section');

        return is_array($result) ? $result : [];
    }

    /**
     * @param string $ip
     *
     * @return array
     */
    public function getBannedKeysReplacements($ip)
    {
        $keys = $this->getBannedKeys($ip);
        $newKeysReplacements = [];

        foreach ($keys as $key) {
            $newKeysReplacements[$key] = $this->getSectionReplacement($key);
        }

        return $newKeysReplacements;
    }
}
