<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class TaskBoardUserRoleConnectionRecord
 */
class TaskBoardUserRoleConnectionRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['user_role', 'getUserRole', 'setUserRole', 'userRole'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['task_board_id', 'getTaskBoardId', 'setTaskBoardId', 'taskBoardId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'task_board_user_role_connection';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'board' => [self::BELONGS_TO, 'MommyCom\Model\Db\TaskBoardRecord', 'task_board_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['task_board_id, user_role', 'required'],
            ['user_role', 'length', 'min' => 1, 'max' => 500],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'task_board_id' => Translator::t('Board name'),
            'user_role' => Translator::t('Role'),
            'created_at' => Translator::t('Created'),
        ];
    }
}
