<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class StatisticFilterUserRecord
 *
 * @property-read integer $id
 * @property int $list_id
 * @property string $email
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read StatisticFilterUserRecord $list
 */
class StatisticFilterUserRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return StatisticFilterUserRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['list_id', 'getListId', 'setListId', 'listId'];
    }

    public function tableName()
    {
        return 'statistic_filter_users';
    }

    public function relations()
    {
        return [
            'list' => [self::BELONGS_TO, 'MommyCom\Model\Db\StatisticFilterUserListRecord', 'list_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', ['email', 'email']],
        ];
    }

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['email', 'email', 'message' => \Yii::t('common', 'E-mail is not valid')],
            ['list_id', 'exist', 'className' => 'MommyCom\Model\Db\StatisticFilterUserListRecord', 'attributeName' => 'id'],

            ['email', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'list_id' => Translator::t('List'),
            'email' => Translator::t('E-mail'),

            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model StatisticFilterUserRecord */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.email', $model->email, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function listIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.list_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function listId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.list_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function emailIs($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.email' => $value,
        ]);
        return $this;
    }
}
