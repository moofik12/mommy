<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use DateTime;
use LogicException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class CartRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property string $anonymous_id
 * @property integer $event_id
 * @property integer $product_id
 * @property integer $number
 * @property integer $added_at
 * @property integer $reserved_to
 * @property integer $reservation_bonus_time
 * @property integer $priority приоритет при сортировке (меньше - лучше)
 * @property boolean $is_sent_notification была рассылка по брошенному товару
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read EventProductRecord $product
 * @property-read EventRecord $event
 * @property-read UserRecord $user
 * @property-read boolean $isExpired
 * @property-read integer $reservedTo
 * @property-read float $price
 * @property-read float $totalPrice price * number
 * @property-read float $savings
 * @property-read float $totalSavings
 * @mixin Timestampable
 */
class CartRecord extends DoctrineActiveRecord
{
    public const RESERVATION_TIME = 1800; // 30 minutes
    public const RESERVATION_BONUS_TIME = 900; // 15 minutes
    public const VIRTUAL_EVENT_RESERVATION_TIME = 600;
    public const VIRTUAL_EVENT_RESERVATION_BONUS_TIME = 600;

    /**
     * @param string $className
     *
     * @return CartRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['anonymous_id', 'getAnonymousId', 'setAnonymousId', 'anonymousId'];
        yield ['number', 'getNumber', 'setNumber', 'number'];
        yield ['added_at', 'getAddedAt', 'setAddedAt', 'addedAt'];
        yield ['reserved_to', 'getReservedTo', 'setReservedTo', 'reservedTo'];
        yield ['reservation_bonus_time', 'getReservationBonusTime', 'setReservationBonusTime', 'reservationBonusTime'];
        yield ['priority', 'getPriority', 'setPriority', 'priority'];
        yield ['is_sent_notification', 'isSentNotification', 'setIsSentNotification', 'isSentNotification'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
    }

    public function tableName()
    {
        return 'carts';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'product' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventProductRecord', 'product_id'],
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
        ];
    }

    public function rules()
    {
        return [
            ['event_id, product_id, number', 'required'],
            ['number', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 10000],

            ['user_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым
            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
            ['event_id', 'exist', 'allowEmpty' => false, 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['product_id', 'exist', 'allowEmpty' => false, 'className' => 'MommyCom\Model\Db\EventProductRecord', 'attributeName' => 'id'],

            ['anonymous_id', 'length'],

            ['priority', 'numerical', 'min' => 0, 'max' => 255],
            ['is_sent_notification ', 'boolean'],

            ['user_id, event_id, product_id, number, anonymous_id', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestampable' => [
                'class' => Timestampable::class,
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => \Yii::t('common', 'User'),
            'anonymous_id' => \Yii::t('common', 'Anonymous user'),
            'event_id' => \Yii::t('common', 'Event'),
            'added_at' => \Yii::t('common', 'Add time'),
            'created_at' => \Yii::t('common', 'Created time'),
            'number' => \Yii::t('common', 'Quantity'),
            'priority' => \Yii::t('common', 'Priority when sorting'),
            'is_sent_notification' => \Yii::t('common', 'Notification of abandoned shopping cart'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.id', $provider->model->id);
        $criteria->compare($alias . '.user_id', $provider->model->user_id);
        $criteria->compare($alias . '.event_id', $provider->model->event_id);
        $criteria->compare($alias . '.product_id', $provider->model->product_id);
        $criteria->compare($alias . '.number', $provider->model->number);
        $criteria->compare($alias . '.anonymous_id', $provider->model->anonymous_id);
        $criteria->compare($alias . '.is_sent_notification', $provider->model->is_sent_notification);

        return $provider;
    }

    protected function _resolveEvent()
    {
        if ($this->product) {
            $this->event_id = $this->product->event_id;
        }
    }

    protected function _reserveProduct()
    {
        if ($this->added_at > 0) {
            return;
        }

        $this->added_at = time();
        $this->reserved_to = $this->added_at + self::RESERVATION_TIME;
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolveEvent();
        $this->_reserveProduct();

        return $result;
    }

    public function afterSave()
    {
        parent::afterSave();
        $this->_updateProduct();
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $this->_updateProduct();
    }

    /**
     * Обновление данных EventProductRecord
     */
    protected function _updateProduct()
    {
        if ($this->isNewRecord) {
            $eventProduct = EventProductRecord::model()->findByPk($this->product_id);
            if ($eventProduct) {
                $eventProduct->updateIsSoldOutNumber(true);
            }

            return;
        }

        if ($this->product === null) {
            return;
        }

        $this->product->updateIsSoldOutNumber(true);
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function productId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function eventIdIn(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function isSentNotification($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_sent_notification' => (int)$value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userIdNot($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition([
            $alias . '.' . 'user_id' . ' <> ' . Cast::toStr($value),
        ]);
        return $this;
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return $this
     */
    public function createdAtBetween($start, $end)
    {
        $start = Cast::toUInt($start);
        $end = Cast::toUInt($end);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addBetweenCondition($alias . '.created_at', $start, $end);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtDay($value)
    {
        $value = Cast::toUInt($value);

        $time = new DateTime();
        $time->setTimestamp($value);
        $unixTime = $time->modify('today')->getTimestamp();
        $unixTimeNextDay = $time->modify('+1 day')->getTimestamp();

        return $this->createdAtBetween($unixTime, $unixTimeNextDay);
    }

    /**
     * @return static
     */
    public function onlyReserved()
    {
        $alias = $this->getTableAlias();
        $addedAtAttr = $alias . '.' . 'reserved_to';
        $bonusAttr = $alias . '.' . 'reservation_bonus_time';
        $this->getDbCriteria()->addCondition(
            '(' . $addedAtAttr . '+' . $bonusAttr . ')' . '>=' . time()
        );
        return $this;
    }

    /**
     * @return static
     */
    public function onlyUnreserved()
    {
        $alias = $this->getTableAlias();
        $addedAtAttr = $alias . '.' . 'reserved_to';
        $bonusAttr = $alias . '.' . 'reservation_bonus_time';
        $this->getDbCriteria()->addCondition(
            '(' . $addedAtAttr . '+' . $bonusAttr . ')' . '<' . time()
        );
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function aliasName($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();

        $aliases = BrandAliasRecord::model()->name($value)->findAll();
        $ids = ArrayUtils::getColumn($aliases, 'id');
        $this->getDbCriteria()->addInCondition($alias . '.' . 'id', $ids);

        return $this;
    }

    /*** ITEM API ***/

    /**
     * Обновляет время резерва
     *
     * @return bool
     */
    public function updateReservation()
    {
        if (!$this->getIsExpired()) {
            return true;
        }

        if ($this->product->getIsPossibleToBuy($this->number)) {
            if (!$this->product->event->is_virtual) {
                $this->reserved_to = time() + self::RESERVATION_TIME;
            } else {
                $this->reserved_to = time() + self::VIRTUAL_EVENT_RESERVATION_TIME;
            }
            $this->reservation_bonus_time = 0;
            if ($this->is_sent_notification) {
                $this->is_sent_notification = 0;
            }
            return $this->save();
        }
        return false;
    }

    /**
     * @return boolean
     */
    public function giveBonusTime()
    {
        if (!$this->getIsReserved()) {
            return false;
        }

        if (!$this->product->event->is_virtual) {
            $this->reservation_bonus_time = self::RESERVATION_BONUS_TIME;
        } else {
            $this->reservation_bonus_time = self::VIRTUAL_EVENT_RESERVATION_BONUS_TIME;
        }
        return $this->save();
    }

    /**
     * @return integer время до которого резервируется товар
     */
    public function getReservedTo()
    {
        return min($this->event->end_at, $this->reserved_to + $this->reservation_bonus_time);
    }

    /**
     * @return bool
     */
    public function getIsReserved()
    {
        return $this->getReservedTo() >= time();
    }

    /**
     * @return bool
     */
    public function getIsExpired()
    {
        return !$this->getIsReserved();
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->product->price;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->product->price * $this->number;
    }

    /**
     * @return float
     */
    public function getSavings()
    {
        return $this->product->price_market - $this->product->price;
    }

    /**
     * @return float
     */
    public function getTotalSavings()
    {
        return $this->getSavings() * $this->number;
    }

    /**
     * @param bool $reverse
     *
     * @return static
     */
    public function priority($reverse = false)
    {
        $sort = 'ASC';
        $alias = $this->getTableAlias();

        if ($reverse) {
            $sort = 'DESC';
        }

        $order = $alias . '.priority ' . $sort;

        if ($this->getDbCriteria()->order) {
            $order = ', ' . $order;
        }

        $this->getDbCriteria()->order = $order;

        return $this;
    }

    /**
     * @param array $cartItems
     *
     * @return array
     */
    public static function sortPriority(array $cartItems)
    {
        uasort($cartItems, function ($a, $b) {
            /* @var CartRecord $a */
            /* @var CartRecord $b */
            if (!$a instanceof CartRecord || !$b instanceof CartRecord) {
                throw new LogicException('array must be only CartRecord');
            }

            if ($a->product == $b->priority) {
                return 0;
            }

            return ($a < $b) ? 1 : -1;
        });

        return $cartItems;
    }

    /**
     * @param array $cartItems
     *
     * @return array
     */
    public static function rsortPriority(array $cartItems)
    {
        uasort($cartItems, function ($a, $b) {
            /* @var CartRecord $a */
            /* @var CartRecord $b */
            if (!$a instanceof CartRecord || !$b instanceof CartRecord) {
                throw new LogicException('array must be only CartRecord');
            }

            if ($a->product == $b->priority) {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        });

        return $cartItems;
    }

    public static function generateAnonymousId()
    {
        return md5(uniqid('mamam'));
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function anonymousId($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'anonymous_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function anonymousIdNot($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition([
            $alias . '.' . 'anonymous_id' . ' <> ' . $value,
        ]);
        return $this;
    }
}
