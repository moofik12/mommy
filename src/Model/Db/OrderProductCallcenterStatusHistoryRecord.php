<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbExpression;
use CException;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderProductCallcenterHistoryRecord
 *
 * @property-read integer $id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $status
 * @property integer $status_prev
 * @property string $comment
 * @property integer $user_id
 * @property integer $client_id
 * @property integer $time
 * @property-read string $statusReplacement
 * @property-read string $statusPrevReplacement
 * @property-read OrderRecord $product
 * @property-read AdminUserRecord $user
 * @property-read UserRecord $client
 */
class OrderProductCallcenterStatusHistoryRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return OrderProductCallcenterStatusHistoryRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['time', 'getTime', 'setTime', 'time'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['client_id', 'getClientId', 'setClientId', 'clientId'];
    }

    public function tableName()
    {
        return 'orders_products_callcenter_statushistory';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'client' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'client_id'],
        ];
    }

    public function rules()
    {
        return [
            ['order_id', 'required'],
            ['order_id', 'numerical', 'integerOnly' => true, 'min' => 0],

            ['id, order_id, status, status_prev, user_id, time', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'order_id' => \Yii::t($category, '№ order'),
            'status' => \Yii::t($category, 'Status'),
            'status_prev' => \Yii::t($category, 'Status of previous'),
            'comment' => \Yii::t($category, 'Comment'),
            'user_id' => \Yii::t($category, 'User'),
            'time' => \Yii::t($category, 'Time'),
        ];
    }

    public function _resolveTime()
    {
        if (empty($this->time)) {
            $this->time = new CDbExpression('UNIX_TIMESTAMP()');
        }
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        $this->_resolveTime();
        try { // for console app
            if (empty($this->user_id)) {
                $this->user_id = \Yii::app()->user->id;
            }
        } catch (CException $e) {
        }

        return true;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', '=' . Utf8::ltrim($model->id, '0'));
        $criteria->compare($alias . '.order_id', '=' . $model->order_id);

        $criteria->compare($alias . '.status', '=' . $model->status);
        $criteria->compare($alias . '.status_prev', '=' . $model->status_prev);

        $criteria->compare($alias . '.user_id', '=' . $model->user_id);
        $criteria->compare($alias . '.time', '=' . $model->time);

        return $provider;
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = OrderProductRecord::callcenterStatusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = OrderProductRecord::callcenterStatusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }
}
