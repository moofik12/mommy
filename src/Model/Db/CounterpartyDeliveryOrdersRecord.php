<?php

namespace MommyCom\Model\Db;

use MommyCom\Entity\CounterpartyDeliveryOrder;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Фиксация оправок заказов по контрагентам
 *
 * @property string $id
 * @property string $counterparty_id
 * @property string $order_id
 * @property float $amount
 * @property float $toPay
 * @property string $updated_at
 * @property string $created_at
 * @property CounterpartyDeliveryRecord $counterparty
 * @mixin Timestampable
 */
class CounterpartyDeliveryOrdersRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className active record class name.
     *
     * @return CounterpartyDeliveryOrdersRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntityClassName(): string
    {
        return CounterpartyDeliveryOrder::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['toPay', 'getToPay', 'getToPay', 'toPay'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['counterparty_id', 'getCounterpartyId', 'setCounterpartyId', 'counterpartyId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'counterparties_delivery_orders';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'counterparty' => [self::BELONGS_TO, 'MommyCom\Model\Db\CounterpartyDeliveryRecord', 'counterparty_id'],
        ];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['order_id, amount, toPay', 'required'],
            ['amount, toPay', 'numerical'],
            ['updated_at, created_at', 'numerical', 'integerOnly' => true],
            ['order_id', 'length', 'max' => 20],
            ['counterparty_id', 'exist', 'className' => 'MommyCom\Model\Db\CounterpartyDeliveryRecord', 'attributeName' => 'id'],

            ['id, counterparty_id, order_id, amount, toPay, updated_at, created_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'counterparty_id' => Translator::t('Consignor'),
            'order_id' => Translator::t('Order'),
            'amount' => Translator::t('Order total'),
            'toPay' => Translator::t('Amount to be paid'),
            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var CounterpartyDeliveryOrdersRecord $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.counterparty_id', $model->counterparty_id);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.amount', $model->amount);
        $criteria->compare($alias . '.toPay', $model->toPay);
        $criteria->compare($alias . '.updated_at', $model->updated_at);
        $criteria->compare($alias . '.created_at', $model->created_at);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function counterpartyId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.counterparty_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function counterpartyIdIn(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.counterparty_id', $value);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function orderIdIn(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $value);

        return $this;
    }

}
