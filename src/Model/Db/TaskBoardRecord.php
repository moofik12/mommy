<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbCriteria;
use CHttpException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class TaskBoardRecord
 */
class TaskBoardRecord extends DoctrineActiveRecord
{
    /**
     * @var
     */
    public $answer;

    /**
     * типы досок
     */
    const
        TYPE_CALL_CENTER = 1,
        TYPE_BRAND_MANAGER = 2,
        TYPE_STORAGE = 3,
        TYPE_COORDINATION = 4;

    /**
     *
     */
    const Q_MAIN = 0; // Без типа

    /**
     * типы вопросов
     */
    const
        Q_PRODUCT = 1,
        Q_SHIPPING = 2,
        Q_COURIER = 3,
        Q_RENEWALS_FORWARDING = 4;

    /**
     * @var array
     * session constants keys
     */
    public static $sessionConstants = [
        self::TYPE_CALL_CENTER => 'callcenter',
        self::TYPE_BRAND_MANAGER => 'brandmanager',
        self::TYPE_STORAGE => 'storage',
        self::TYPE_COORDINATION => 'coordination',
    ];

    /**
     * @param $type integer
     *
     * @return array
     */
    public static function getTaskBoardTypes($type = null)
    {
        $types = [
            self::TYPE_CALL_CENTER => Translator::t('Callcenter'),
            self::TYPE_BRAND_MANAGER => Translator::t('Brand managers'),
            self::TYPE_STORAGE => Translator::t('Warehouse'),
            self::TYPE_COORDINATION => Translator::t('Coordination'),
        ];
        if ($type && isset($types[$type])) {
            return $types[$type];
        }
        return $types;
    }

    /**
     * @param $type integer
     *
     * @return array
     */
    public static function getQuestionTypes($type = null)
    {
        $types = [
            self::Q_PRODUCT => Translator::t('Question about the product'),
            self::Q_SHIPPING => Translator::t('When sending'),
            self::Q_COURIER => Translator::t('Calling the courier'),
            self::Q_RENEWALS_FORWARDING => Translator::t('Renewal, redirection'),
        ];

        if ($type && isset($types[$type])) {
            return $types[$type];
        }

        return $types;
    }

    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['task_board_type', 'getTaskBoardType', 'setTaskBoardType', 'taskBoardType'];
        yield ['question_type', 'getQuestionType', 'setQuestionType', 'questionType'];
        yield ['question_description', 'getQuestionDescription', 'setQuestionDescription', 'questionDescription'];
        yield ['answer_counter', 'getAnswerCounter', 'setAnswerCounter', 'answerCounter'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['url', 'getUrl', 'setUrl', 'url'];
        yield ['is_archive', 'isArchive', 'setIsArchive', 'isArchive'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['products', 'getProducts', 'setProducts', 'products'];
        yield ['owner_id', 'getOwnerId', 'setOwnerId', 'ownerId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'task_board';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'owner' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'owner_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'order_id'],
            'answers' => [self::HAS_MANY, 'MommyCom\Model\Db\TaskBoardAnswersRecord', 'question_id', 'order' => 'created_at DESC'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status,is_archive', 'in', 'range' => [1, 0]],
            ['task_board_type', 'in', 'range' => array_keys(self::getQuestionTypes())],
            ['status,is_archive', 'boolean'],
            ['id', 'required', 'on' => 'task_search'],
            ['task_board_type', 'required', 'on' => 'changeBoard'],
            ['task_board_type, user_id , question_description', 'required', 'on' => 'manual'],
            ['question_type, question_description', 'required', 'on' => 'productCard'],
            ['question_description', 'required', 'on' => 'update'],
            ['url', 'length', 'min' => 1, 'max' => 255],
            ['url', 'url'],
            ['owner_id', 'exist', 'allowEmpty' => true, 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['question_type, task_board_type', 'safe', 'on' => 'search'],
            ['answer,question_description,answer_counter,products', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '',
            'task_board_type' => Translator::t('Board Name'),
            'user_id' => Translator::t('Recipient'),
            'owner_id' => Translator::t('Created by'),
            'question_type' => Translator::t('Type of Question'),
            'question_description' => Translator::t('Question'),
            'url' => Translator::t('Link'),
            'answer_counter' => Translator::t('Number of Replies'),
            'answer' => Translator::t('Answer'),
            'status' => Translator::t('Status'),
            'is_archive' => Translator::t('Archive'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->isNewRecord) {
            $this->question_description = strip_tags($this->question_description);
            $this->answer = strip_tags($this->answer);
        } else {
            $this->question_description = strip_tags($this->question_description);
        }
        return parent::beforeValidate();
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;

        $criteria->compare($alias . '.task_board_type', $model->task_board_type);
        $criteria->compare($alias . '.question_type', $model->question_type);

        return $provider;
    }

    /**
     * @return $this
     */
    public function notArchived()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_archive' => 0,
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function orderId($value)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param $value
     * @param array $attr
     *
     * @return $this
     */
    public function getTasks($value, $attr = [])
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $mainUserTasks = [
            $alias . '.user_id' => $value,
            $alias . '.owner_id' => $value,
        ];

        $in = [];
        if ($attr !== []) {
            foreach ($attr as $k => $v) {
                array_push($in, (int)$v);
            }
            array_push($in, (int)$value);
        }

        if ($in !== []) {
            $this->getDbCriteria()->addCondition($alias . '.owner_id' . '=' . $value, 'OR')->addInCondition($alias . '.user_id', $in, 'OR');
        } else {
            $this->getDbCriteria()->addColumnCondition($mainUserTasks, 'OR');
        }

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function userTasks($value)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
            $alias . '.owner_id' => $value,
        ], 'OR');
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.user_id = ' . $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function getByTaskId($value)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.id' => $value,
        ]);
        return $this;
    }

    /**
     * @param $type
     *
     * @return $this
     */
    public function getByBoardType($type)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.task_board_type' => $type,
        ]);
        return $this;
    }

    /**
     * @param int $true
     *
     * @return $this
     */
    public function archived($true = 1)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_archive' => $true,
        ]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function getNewTasks($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $userTasks = [
            $alias . '.user_id' => $value,
            $alias . '.answer' => '',
            $alias . '.is_archive' => 0,
        ];

        $this->getDbCriteria()->addColumnCondition($userTasks);
        return $this;
    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getUsersByBoardId($id)
    {
        $managers = [];
        $sql = 'SELECT * FROM ' . TaskBoardUserRoleConnectionRecord::model()->tableName() . ' WHERE task_board_id = ' . $id;
        $roles = \Yii::app()->db->cache(1000, time())->createCommand($sql)->queryAll();

        $in = [];
        if ($roles !== []) {
            foreach ($roles as $k => $v) {
                array_push($in, (string)$v['user_role']);
            }
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition('name', $in, 'OR');

        $assignments = AdminRoleRecord::model()->findAll($criteria);
        foreach ($assignments as $k => $v) {
            $assignment = $v->assignment;
            foreach ($assignment as $key => $value) {
                if ($value->admin->status == AdminUserRecord::STATUS_ACTIVE) {
                    $managers[$value->admin->id] = $value->admin->fullname;
                }
            }
        }
        return $managers;
    }

    /**
     * @param $id
     *
     * @return static
     * @throws CHttpException
     */
    public static function loadModel($id)
    {
        $model = self::model()->cache(1000, time())->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Translator::t('Model does not exist.'));
        }
        return $model;
    }

    /**
     * @return array
     */
    public static function qTypes()
    {
        return TaskBoardRecord::getQuestionTypes() + [TaskBoardRecord::Q_MAIN => Translator::t('No model')];
    }
}
