<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CHtml;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class ProductSectionRecord
 *
 * @property-description Секции (разделы) товаров
 * @property-read string $id
 * @property string $name
 * @property int $parent_id
 * @property array $keywords
 * @property-read $parent self
 * @property-read $fullChainSection string
 */
class ProductSectionRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return ProductSectionRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['keywords', 'getKeywords', 'setKeywords', 'keywords'];
        yield ['parent_id', 'getParentId', 'setParentId', 'parentId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'products_sections';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'validName'],
            ['name, parent_id', 'required'],

            ['name', 'length', 'max' => 60],
            ['parent_id', 'numerical', 'integerOnly' => true],
            //array('keywords', 'length', 'max'=>250),
            ['keywords', 'safe'],

            ['id, name, parent_id, keywords', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'parent' => [self::BELONGS_TO, 'MommyCom\Model\Db\ProductSectionRecord', 'parent_id'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'name' => Translator::t('Section'),
            'parent_id' => Translator::t('Parent section'),
            'keywords' => Translator::t('Keywords'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.parent_id', $model->parent_id);
        $criteria->compare($alias . '.keywords', $model->keywords);

        return $provider;
    }

    public function validName($attr)
    {
        $name = $this->$attr;
        if (strpos($name, '->') !== false) {
            $this->addError($attr, \Yii::t('common', 'Нельзя использовать -> в названии секции'));
        }
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_serializeKeywords();

        return true;
    }

    public function afterFind()
    {
        $result = parent::afterFind();
        $this->_unserializeKeywords();
        return $result;
    }

    protected function _serializeKeywords()
    {
        if (!empty($this->keywords) && is_array($this->keywords)) {
            $this->keywords = array_map('trim', $this->keywords);
            $this->keywords = implode(',', $this->keywords);
        } else {
            $this->keywords = '';
        }
    }

    protected function _unserializeKeywords()
    {
        $this->keywords = !empty($this->keywords) && is_string($this->keywords) ? explode(',', $this->keywords) : [];
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function parentId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'parent_id' => $value,
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function onlyParent()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'parent_id' => 0,
        ]);
        return $this;
    }

    /**
     * Фильтр по названию
     *
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value, true);
        return $this;
    }

    public function getFullChainSection()
    {
        $sections = $this->name;
        if ($this->parent !== null) {
            $sections = $this->parent->name . '->' . $sections;
        }
        return $sections;
    }

    public function onlyChild()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'parent_id' . '>' . 0); // not empty
        return $this;
    }

    /**
     * @return array
     */
    public static function getGroupList()
    {
        $categories = self::model()->cache(1800)->findAll();
        $groupParentCategories = CHtml::listData($categories, 'id', 'name', 'parent_id');
        $groupedCategories1 = [];
        foreach ($groupParentCategories[0] as $id => $parentCategory) {
            $groupedCategories1[$id] = ['text' => $parentCategory, 'children' => []];
        }
        foreach ($groupParentCategories as $parentId => $categoryList) {
            if ($parentId != 0) {
                foreach ($categoryList as $categoryId => $category)
                    $groupedCategories1[$parentId]['children'][] = ['value' => $categoryId, 'text' => $category];
            }
        }
        $groupedCategories = [];
        foreach ($groupedCategories1 as $g)
            $groupedCategories[] = $g;
        return $groupedCategories;
    }

    /**
     * @return array
     */
    public static function getGroupListForFilter()
    {
        $categories = self::model()->cache(1800)->findAll();
        $groupParentCategories = CHtml::listData($categories, 'id', 'name', 'parent_id');
        $groupedCategories = [];
        foreach ($groupParentCategories[0] as $id => $parentCategory) {
            $groupedCategories[$parentCategory] = [];
            if (isset($groupParentCategories[$id])) {
                foreach ($groupParentCategories[$id] as $categoryChildId => $categoryChildName) {
                    $groupedCategories[$parentCategory][$categoryChildId] = $categoryChildName;
                }
            }
        }
        return $groupedCategories;
    }
}
