<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use Exception;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

/**
 * @property-read integer $id
 * @property string $name
 * @property-read int updated_at
 * @property-read int created_at
 * @property-read SupplierPaymentRecord[] $payments
 */
class SupplierPaymentActRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return SupplierPaymentActRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    public function tableName()
    {
        return 'suppliers_payments_acts';
    }

    public function relations()
    {
        return [
            'payments' => [self::HAS_MANY, 'MommyCom\Model\Db\SupplierPaymentRecord', 'act_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'length', 'max' => 250],

            ['id, name'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'name' => Translator::t('Name'),

            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),

            'countPayments' => Translator::t('Qty'),
            'amountPayments' => Translator::t('Amount'),
            'amountPaymentsWithoutPenalty' => Translator::t('Amount without penalty'),
        ];
    }

    /**
     * @return int
     */
    public function getCountPayments()
    {
        return count($this->payments);
    }

    /**
     * @param bool $withPenalty
     * @param int|bool $supplierId
     *
     * @return float
     */
    public function getAmountPayments($withPenalty = true, $supplierId = false)
    {
        $amount = 0.0;
        foreach ($this->payments as $payment) {
            if ($supplierId === false) {
                $amount += $payment->getPayAmount($withPenalty);
            } elseif ($payment->supplier_id == $supplierId) {
                $amount += $payment->getPayAmount($withPenalty);
            }
        }

        return $amount;
    }

    /**
     * @param int|bool $supplierId
     *
     * @return float
     */
    public function getAmountPaymentsWithoutPenalty($supplierId = false)
    {
        return $this->getAmountPayments(false, $supplierId);
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);

        return $provider;
    }

    /**
     * @return bool
     */
    protected function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $transaction = $this->dbConnection->beginTransaction();
        try {
            foreach ($this->payments as $payment) {
                $payment->act_id = '';

                if (!$payment->save()) {
                    throw new Exception(Translator::t('Error deleting act for payments by supplier') . ' Errors: ' . print_r($payment->errors, true));
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
        }

        return true;
    }

}
