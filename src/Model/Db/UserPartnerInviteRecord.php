<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserPartnerInviteRecord
 *
 * @property-description Приглашенные партнерами пользователи
 * @property-read int $id
 * @property int $partner_id
 * @property int $user_id
 * @property string $url_referer
 * @property string $utm_params
 * @property integer $type_incoming
 * @property string $created_at
 * @property string $updated_at
 * @property-read int $typeIncomingReplacement
 * @property-read UserRecord $user
 * @property-read UserPartnerRecord $partner
 */
class UserPartnerInviteRecord extends DoctrineActiveRecord
{
    const TYPE_INCOMING_UNKNOWN = 0;
    const TYPE_INCOMING_PERSONAL_PARTNER_URL = 1;
    const TYPE_INCOMING_PROMOCODE = 2;

    /**
     * @param string $className
     *
     * @return UserPartnerInviteRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['type_incoming', 'getTypeIncoming', 'setTypeIncoming', 'typeIncoming'];
        yield ['url_referer', 'getUrlReferer', 'setUrlReferer', 'urlReferer'];
        yield ['utm_params', 'getUtmParams', 'setUtmParams', 'utmParams'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['partner_id', 'getPartnerId', 'setPartnerId', 'partnerId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'users_partner_invites';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner_id, user_id', 'required'],
            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
            ['partner_id', 'exist', 'className' => 'MommyCom\Model\Db\UserPartnerRecord', 'attributeName' => 'id'],

            ['type_incoming', 'in', 'range' => array_keys(self::typeIncomingReplacements())],

            ['url_referer, utm_params', 'length'],

            [
                'id, partner_id, user_id, url_referer, utm_params, type_incoming',
                'safe', 'on' => 'searchPrivileged',
            ],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'partner' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserPartnerRecord', 'partner_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => \Yii::t('common', 'Supplier ID'),
            'user_id' => \Yii::t('common', 'User ID'),
            'url_referer' => \Yii::t('common', 'Url Referer'),
            'utm_params' => \Yii::t('common', 'Utm-marks'),
            'type_incoming' => \Yii::t('common', 'Type of invitation'),
            'created_at' => \Yii::t('common', 'Created'),
            'updated_at' => \Yii::t('common', 'Updated on'),

            'typeIncomingReplacement' => \Yii::t('common', 'Type of invitation'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.partner_id', $model->partner_id);
        $criteria->compare($alias . '.url_referer', $model->url_referer, true);
        $criteria->compare($alias . '.utm_params', $model->utm_params, true);
        $criteria->compare($alias . '.type_incoming', $model->type_incoming);

        return $provider;
    }

    /**
     * @return array
     */
    public static function typeIncomingReplacements()
    {
        return [
            self::TYPE_INCOMING_UNKNOWN => \Yii::t('common', 'unknown'),
            self::TYPE_INCOMING_PERSONAL_PARTNER_URL => \Yii::t('common', 'on the partner\'s personal link'),
            self::TYPE_INCOMING_PROMOCODE => \Yii::t('common', 'promo code of the partner'),
        ];
    }

    /**
     * @return string
     */
    public function getTypeIncomingReplacement()
    {
        $replacements = self::typeIncomingReplacements();
        return isset($replacements[$this->type_incoming]) ? $replacements[$this->type_incoming] : '';
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param $ids
     * @param int $max
     *
     * @return $this
     */
    public function userIdIn($ids, $max = -1)
    {
        if ($max > 0) {
            $ids = array_slice($ids, 0, $max);
        }
        $ids = Cast::toUIntArr($ids);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $ids);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function partnerId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.partner_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param $ids
     * @param int $max
     *
     * @return $this
     */
    public function partnerIdIn($ids, $max = -1)
    {
        if ($max > 0) {
            $ids = array_slice($ids, 0, $max);
        }
        $ids = Cast::toUIntArr($ids);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.partner_id', $ids);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function typeIncoming($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.type_incoming' => $value,
        ]);
        return $this;
    }
}
