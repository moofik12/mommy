<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserInterestRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property string $category внутреннаяя категория (текстовая)
 * @property string $target @see ProductTargets
 * @property integer $age_from с какого возраста
 * @property integer $age_to по какой возраст
 * @property float $rating
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read array ageRange
 * @property-read string ageRangeReplacement
 * @property-read array ageGroups
 * @property-read UserRecord $user
 */
class UserInterestRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return UserInterestRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['category', 'getCategory', 'setCategory', 'category'];
        yield ['target', 'getTarget', 'setTarget', 'target'];
        yield ['age_from', 'getAgeFrom', 'setAgeFrom', 'ageFrom'];
        yield ['age_to', 'getAgeTo', 'setAgeTo', 'ageTo'];
        yield ['rating', 'getRating', 'setRating', 'rating'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'users_interests';
    }

    public function relations()
    {
        return [
            // here
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id, category, target', 'required'],
            ['age_from, age_to', 'numerical', 'min' => 0, 'integerOnly' => true],
            ['rating', 'numerical', 'min' => 0, 'integerOnly' => true],
            ['category, target', 'length', 'min' => 1],

            [
                'id, user_id, category, target, age_from, age_to, ' .
                'created_at, updated_at',

                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),

            'user_id' => Translator::t('User'),
            'category' => Translator::t('Category'),
            'target' => Translator::t('Gender'),
            'age_from' => Translator::t('Age from'),
            'age_to' => Translator::t('Age to'),
            'rating' => Translator::t('Rating'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        return true;
    }

    public function afterSave()
    {
        parent::afterSave();
    }

    public function beforeDelete()
    {
        if (!$result = parent::beforeDelete()) {
            return $result;
        }

        return true;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function category($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'category' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function target($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'target' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function ageFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'age_from' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function ageTo($value)
    {
        $value = Cast::toUInt($value);
        $value = $this->dbConnection->pdoInstance->quote($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'age_to' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function ageBetween($from, $to)
    {
        $this->ageFrom($from);
        $this->ageTo($to);
        return $this;
    }

    /**
     * @param float $value
     *
     * @return static
     */
    public function ratingFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'rating' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param float $value
     *
     * @return static
     */
    public function ratingTo($value)
    {
        $value = Cast::toUInt($value);
        $value = $this->dbConnection->pdoInstance->quote($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'age_to' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param float $from
     * @param float $to
     *
     * @return static
     */
    public function ratingBetween($from, $to)
    {
        $this->ratingFrom($from);
        $this->ratingTo($to);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.age_from', '>=' . $model->age_from);
        $criteria->compare($alias . '.age_to', '<=' . $model->age_to);

        $criteria->compare($alias . '.category', $model->made_in, true);

        $criteria->compare($alias . '.target', $model->target, true);
        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }


    /// item api

    /*
     * @return array
     */
    public function getAgeRange()
    {
        return [$this->age_from, $this->age_to];
    }

    /**
     * @return string
     */
    public function getAgeRangeReplacement()
    {
        return AgeGroups::instance()->convertAgeRangeToString([$this->age_from, $this->age_to]);
    }

    /**
     * @return array
     */
    public function getAgeGroups()
    {
        return AgeGroups::instance()->rangeToGroups($this->getAgeRange());
    }
}
