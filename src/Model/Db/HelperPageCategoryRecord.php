<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class HelperPageCategoryRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read HelperPageRecord[] $pages
 */
class HelperPageCategoryRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return StaticPageCategoryRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    public function tableName()
    {
        return 'helperpages_categories';
    }

    public function relations()
    {
        return [
            'pages' => [self::HAS_MANY, 'MommyCom\Model\Db\HelperPageRecord', 'category_id', 'order' => 'pages.position ASC'],
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],

            ['name', 'length', 'min' => 3, 'max' => 45],
            ['name', 'unique'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => Translator::t('Category name'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'searchDateRangeInput' => Translator::t('Period'),
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->pages as $page) {
            $page->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.id', $provider->model->id);
        $criteria->compare($alias . '.name', $provider->model->name, true);

        return $provider;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.name' => $value,
        ]);
        return $this;
    }
}
