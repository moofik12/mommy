<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserBonuspointsLockRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property integer $operation_id код действия, например номер заказа
 * @property integer $points
 * @property integer $type look UserBonuspointsRecord types
 * @property-read integer $created_at
 * @property-read integer $updated_at
 */
class UserBonuspointsLockRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return UserBonuspointsLockRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['operation_id', 'getOperationId', 'setOperationId', 'operationId'];
        yield ['points', 'getPoints', 'setPoints', 'points'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'users_bonuspoints_locks';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id, points, operation_id', 'required'],
            ['type', 'in', 'range' => UserBonuspointsRecord::getTypes(false)],

            ['points', 'numerical', 'min' => 1, 'max' => UserBonuspointsRecord::MAX_BONUSPOINTS_PER_TRANSFER],

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', '№'),
            'user_id' => \Yii::t('common', 'User'),
            'points' => \Yii::t('common', 'Баллы'),
            'type' => \Yii::t('common', 'Тип бонуса'),
        ];
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function operationId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'operation_id' => $value,
        ]);
        return $this;
    }
} 
