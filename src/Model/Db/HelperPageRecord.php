<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Purifier\StaticPagePurifier;

/**
 * Class HelperPageRecord
 *
 * @property-read integer $id
 * @property string $title
 * @property string $body
 * @property integer $category_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read HelperPageCategoryRecord $category
 */
class HelperPageRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return StaticPageRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['title', 'getTitle', 'setTitle', 'title'];
        yield ['body', 'getBody', 'setBody', 'body'];
        yield ['position', 'getPosition', 'setPosition', 'position'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['category_id', 'getCategoryId', 'setCategoryId', 'categoryId'];
    }

    public function tableName()
    {
        return 'helperpages';
    }

    public function relations()
    {
        return [
            'category' => [self::BELONGS_TO, 'MommyCom\Model\Db\HelperPageCategoryRecord', 'category_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['title, body', 'required'],
            ['title', 'length', 'min' => 3, 'max' => 120],
            ['body', 'length', 'min' => 12],

            ['category_id', 'exist', 'className' => 'MommyCom\Model\Db\HelperPageCategoryRecord', 'attributeName' => 'id', 'allowEmpty' => false],

            ['title, body, category_id', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'category_id' => Translator::t('Category'),
            'title' => Translator::t('Title'),
            'body' => Translator::t('Text'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $purifier = new StaticPagePurifier();
        $this->body = $purifier->purify($this->body);

        return $result;
    }

    public function afterSave()
    {
        //обновление поля `position` для сортировки
        $this->updateByPk($this->id, ['position' => $this->id]);

        parent::afterSave();
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.id', $provider->model->id);
        $criteria->compare($alias . '.body', $provider->model->body, true);
        $criteria->compare($alias . '.title', $provider->model->title, true);
        $criteria->compare($alias . '.category_id', $provider->model->category_id);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function categoryId($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.category_id' => $value,
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function position()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->order = "{$alias}.position ASC";
        return $this;
    }
}
