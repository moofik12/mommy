<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class TabletWarehouseBindRecord
 *
 * @property-read integer $id
 * @property integer $warehouse_product_id
 * @property integer $user_id
 * @property integer $bind_at timestamp
 * @property-read integer $created_at
 * @property-read integer $updated_at
 */
class TabletWarehouseBindRecord extends DoctrineActiveRecord
{
    const BIND_TIME = 3600; // one hour

    /**
     * @param string $className
     *
     * @return TabletWarehouseBindRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['bind_at', 'getBindAt', 'setBindAt', 'bindAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['warehouse_product_id', 'getWarehouseProductId', 'setWarehouseProductId', 'warehouseProductId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'tablet_warehouse_products_binds';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'warehouse' => [self::BELONGS_TO, 'MommyCom\Model\Db\WarehouseProductRecord', 'warehouse_product_id'],
        ];
    }

    public function rules()
    {
        return [
            ['id, warehouse_product_id, user_id', 'numerical', 'integerOnly' => true],

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['warehouse_product_id', 'exist', 'className' => 'MommyCom\Model\Db\WarehouseProductRecord', 'attributeName' => 'id'],

            ['id, warehouse_product_id, user_id, time', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'user_id' => Translator::t('User'),
            'created_by' => Translator::t('Created by'),
            'order_id' => Translator::t('Order'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'createdBy.fullname' => Translator::t('Created by (Full Name)'),
        ];
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        if ($this->bind_at == 0) {
            $this->bind_at = time();
        }

        return true;
    }

    /**
     * <b>Зарезервированно ДО</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function bindAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'bind_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Начинается ПОСЛЕ</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function bindAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'bind_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * Только зарезервированные на данный момент
     *
     * @return static
     */
    public function onlyBinded()
    {
        $time = time();
        $this->bindAtGreater($time - self::BIND_TIME);
        $this->bindAtLower($time);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function warehouseProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.warehouse_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function warehouseProductIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.warehouse_product_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.user_id', $model->user_id, true);
        $criteria->compare($alias . '.warehouse_product_id', $model->warehouse_product_id);
        $criteria->compare($alias . '.bind_at', $model->bind_at);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }
} 
