<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class FeedbackRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property string $message
 * @property string $message_answer
 * @property integer $status
 * @property integer $type
 * @property integer $admin_id
 * @property string $created_at
 * @property string $updated_at
 * @property-read string $typeReplacement
 * @property-read string $statusReplacement
 * @property-read UserRecord $user
 * @property-read AdminUserRecord $admin
 */
class FeedbackRecord extends DoctrineActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_CANCEL = 2;

    const TYPE_HELP_PARTNER = 0;

    /**
     * @return string
     */
    public function tableName()
    {
        return 'feedback';
    }

    /**
     * @param string $className
     *
     * @return FeedbackRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['message', 'getMessage', 'setMessage', 'message'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['message_answer', 'getMessageAnswer', 'setMessageAnswer', 'messageAnswer'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['user_id, message', 'required'],

            ['type', 'in', 'range' => array_keys(self::typeReplacements())],
            ['status', 'in', 'range' => array_keys(self::statusReplacements())],
            ['message_answer', 'length'],

            ['id, user_id, message, message_answer, status, type, admin_id, created_at, updated_at', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'user_id' => \Yii::t($category, 'User'),
            'message' => \Yii::t($category, 'Message'),
            'message_answer' => \Yii::t($category, 'Message reply'),
            'status' => \Yii::t($category, 'Status'),
            'type' => \Yii::t($category, 'Type of treatment'),
            'admin_id' => \Yii::t($category, 'processed'),
            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),

            'typeReplacement' => \Yii::t('common', 'Type of treatment'),
            'statusReplacement' => \Yii::t('common', 'Status'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.message', $model->message, true);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.type', $model->type);
        $criteria->compare($alias . '.admin_id', $model->admin_id);

        return $provider;
    }

    /**
     * @return array
     */
    public static function typeReplacements()
    {
        return [
            self::TYPE_HELP_PARTNER => \Yii::t('common', 'help with partnership program'),
        ];
    }

    /**
     * @return string
     */
    public function getTypeReplacement()
    {
        $replacements = self::typeReplacements();
        return isset($replacements[$this->type]) ? $replacements[$this->type] : '';
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_NEW => \Yii::t($category, 'new!'),
            self::STATUS_PROCESSED => \Yii::t($category, 'processed'),
            self::STATUS_CANCEL => \Yii::t($category, 'cancelled!'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function status($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function notStatus($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addCondition($alias . '.status' . '!=' . $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function type($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function adminId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.admin_id' => $value,
        ]);
        return $this;
    }
}
