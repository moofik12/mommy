<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderReturnProductRecord
 *
 * @property-read integer $id
 * @property integer $return_id
 * @property integer $order_id
 * @property integer $order_product_id
 * @property integer $event_id
 * @property integer $event_product_id
 * @property integer $warehouse_id
 * @property float $price
 * @property integer $item_condition состояние
 * @property boolean $is_pay подлежит оплате (да\нет)
 * @property string $client_comment комментарий клиента при возврате
 * @property string $comment комментарий менеджера
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read OrderReturnRecord $return
 * @property-read OrderRecord $order
 * @property-read OrderProductRecord $orderProduct
 * @property-read EventRecord $event
 * @property-read EventProductRecord $eventProduct
 * @property-read WarehouseProductRecord $warehouseProduct
 * @property-read WarehouseProductRecord $returnedWarehouseProduct
 * @property string $conditionReplacement состояние
 * @property-read int|float $refundPrice
 */
class OrderReturnProductRecord extends DoctrineActiveRecord
{
    const CONDITION_UNKNOWN = 0; // пока еще не указанно
    const CONDITION_GOOD = 1; // не надевалось, либо никаких признаков
    const CONDITION_GOOD_WEARED = 2; // хорошее, но явно было надето (сбиты бирки)
    const CONDITION_DEFECTIVE = 3; // брак
    const CONDITION_BROKEN = 4; // испорчено
    const CONDITION_BROKEN_BY_DELIVERY_SERVICE = 5; // испорчено службой доставки

    const STATUS_OK = 0; // ок
    const STATUS_CANCELLED = 1; // удалено из возврата
    const STATUS_DELETED = 2; // возврат отменен

    /**
     * @param string $className
     *
     * @return OrderReturnProductRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['price', 'getPrice', 'setPrice', 'price'];
        yield ['item_condition', 'getItemCondition', 'setItemCondition', 'itemCondition'];
        yield ['is_pay', 'isPay', 'setIsPay', 'isPay'];
        yield ['client_comment', 'getClientComment', 'setClientComment', 'clientComment'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['return_id', 'getReturnId', 'setReturnId', 'returnId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['order_product_id', 'getOrderProductId', 'setOrderProductId', 'orderProductId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['event_product_id', 'getEventProductId', 'setEventProductId', 'eventProductId'];
        yield ['warehouse_id', 'getWarehouseId', 'setWarehouseId', 'warehouseId'];
    }

    public function tableName()
    {
        return 'orders_returns_positions';
    }

    public function relations()
    {
        return [
            'return' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderReturnRecord', 'return_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'orderProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderProductRecord', 'order_product_id'],
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'eventProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventProductRecord', 'event_product_id'],
            'warehouseProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\WarehouseProductRecord', 'warehouse_id'],
            'returnedWarehouseProduct' => [self::HAS_ONE, 'MommyCom\Model\Db\WarehouseProductRecord', 'return_product_id'],
        ];
    }

    public function rules()
    {
        return [
            ['return_id, order_id, order_product_id, event_id, event_product_id, warehouse_id, price', 'required'],
            ['return_id, order_id, order_product_id, event_id, event_product_id, warehouse_id', 'numerical', 'integerOnly' => true, 'min' => 1],
            ['price', 'numerical', 'min' => 0],

            ['item_condition', 'in', 'range' => [
                self::CONDITION_UNKNOWN,
                self::CONDITION_GOOD,
                self::CONDITION_GOOD_WEARED,
                self::CONDITION_DEFECTIVE,
                self::CONDITION_BROKEN,
                self::CONDITION_BROKEN_BY_DELIVERY_SERVICE,
            ]],

            ['status', 'in', 'range' => [
                self::STATUS_OK,
                self::STATUS_CANCELLED,
                self::STATUS_DELETED,
            ]],

            ['status', 'validatorStatus'],

            ['order_product_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderProductRecord', 'attributeName' => 'id'],
            ['return_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderReturnRecord', 'attributeName' => 'id'],
            ['warehouse_id', 'exist', 'className' => 'MommyCom\Model\Db\WarehouseProductRecord', 'attributeName' => 'id'],

            [
                'return_id, order_id, order_product_id, event_id, event_product_id, warehouse_id, is_pay, created_at, updated_at',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'return_id' => \Yii::t($category, '№ of return'),
            'order_id' => \Yii::t($category, '№ order'),
            'order_product_id' => \Yii::t($category, 'Order Item №'),
            'event_id' => \Yii::t($category, '№ of stock'),
            'event_product_id' => \Yii::t($category, '№ of product in stock'),
            'warehouse_id' => \Yii::t($category, 'Warehouse Item №'),
            'item_condition' => \Yii::t($category, 'Condition'),
            'price' => \Yii::t($category, 'Price'),
            'is_pay' => \Yii::t($category, 'To be compensated'),
            'client_comment' => \Yii::t($category, 'Customer comment'),
            'comment' => \Yii::t($category, 'Manager comment'),

            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),

            'conditionReplacement' => \Yii::t($category, 'Condition'),
        ];
    }

    public function validatorStatus($attribute)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        $value = $this->$attribute;

        if ($value == self::STATUS_OK) {
            $previous = self::model()
                ->warehouseId($this->warehouse_id)
                ->status(OrderReturnProductRecord::STATUS_OK)
                ->find(); // поиск такого же товара

            /* @var $previous self */

            if ($previous !== null && ($this->isNewRecord || $this->id != $previous->id)) {
                $this->addError($attribute, \Yii::t($category, 'Such goods are already in another return'));
            }
        }
    }

    protected function _resolveOrder()
    {
        if ($this->order_id > 0) {
            return;
        }

        if ($this->return !== null) {
            $this->order_id = $this->return->order_id;
            return;
        }

        if ($this->orderProduct !== null) {
            $this->order_id = $this->orderProduct->order_id;
            return;
        }

        if ($this->warehouseProduct !== null) {
            $this->order_id = $this->warehouseProduct->order_id;
            return;
        }
    }

    protected function _resolveEventProduct()
    {
        if ($this->event_product_id > 0) {
            return;
        }

        if ($this->orderProduct !== null) {
            $this->event_product_id = $this->orderProduct->product_id;
            return;
        }

        if ($this->warehouseProduct !== null) {
            $this->event_product_id = $this->warehouseProduct->event_product_id;
            return;
        }
    }

    protected function _resolvePrice()
    {
        if ($this->orderProduct === null) {
            return;
        }

        $this->price = $this->orderProduct->price;
    }

    protected function _recalculatePrice()
    {
        if ($this->return === null) {
            return;
        }

        $return = $this->return;
        $currentRefundPrice = $return->price_refund;
        $currentClientExps = $return->exps_client;
        $currentShopExps = $return->exps_shop;
        $return->forceRecalculatePrices();

        if ($return->price_refund != $currentRefundPrice ||
            $return->exps_client != $currentClientExps ||
            $return->exps_shop != $currentShopExps) {
            $return->updateByPk($this->return->id, $return->getAttributes([
                'price_products', 'price_refund', 'exps_client', 'exps_shop',
            ]));
        }
    }

    public function beforeValidate()
    {
        if (!$result = parent::beforeValidate()) {
            return $result;
        }

        // same as before save
        $this->_resolveEventProduct();
        $this->_resolveOrder();
        $this->_resolvePrice();

        return true;
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolveEventProduct();
        $this->_resolveOrder();
        $this->_resolvePrice();

        $this->_recalculatePrice();

        return true;
    }

    public function afterSave()
    {
        parent::afterSave();

        $this->_recalculatePrice();
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function returnId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'return_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'order_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function warehouseId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'warehouse_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function productCondition($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'item_condition' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isPay($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_pay' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.return_id', $model->return_id);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.order_product_id', $model->order_product_id);
        $criteria->compare($alias . '.event_id', $model->event_id);
        $criteria->compare($alias . '.event_product_id', $model->event_product_id);
        $criteria->compare($alias . '.warehouse_id', $model->warehouse_id);
        $criteria->compare($alias . '.price', $model->price);
        $criteria->compare($alias . '.item_condition', $model->item_condition);
        $criteria->compare($alias . '.is_pay', $model->is_pay);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /// item api

    public function getRefundPrice()
    {
        if (!$this->is_pay) {
            return $this->order->getCountUsedBonusesInner($this->price);
        }

        return $this->price - $this->order->getCountUsedBenefice($this->price);
    }

    /**
     * @return string
     */
    public function getConditionReplacement()
    {
        $replacements = self::conditionReplacements();
        return isset($replacements[$this->item_condition]) ? $replacements[$this->item_condition] : '';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return array
     */
    public static function conditionReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::CONDITION_UNKNOWN => \Yii::t($category, 'not specified'),
            self::CONDITION_GOOD => \Yii::t($category, 'no signs of use'),
            self::CONDITION_GOOD_WEARED => \Yii::t($category, 'good сondition, but it was in use (damaged tags on clothes)'),
            self::CONDITION_DEFECTIVE => \Yii::t($category, 'defective'),
            self::CONDITION_BROKEN => \Yii::t($category, 'broken'),
            self::CONDITION_BROKEN_BY_DELIVERY_SERVICE => \Yii::t($category, 'broken by delivery service'),
        ];
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_OK => \Yii::t($category, 'Ok'),
            self::STATUS_CANCELLED => \Yii::t($category, 'Removed from return'),
            self::STATUS_DELETED => \Yii::t($category, 'Return cancelled'),
        ];
    }
}
