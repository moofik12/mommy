<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * class CallcenterTaskOperationRecord
 *
 * @property string $title
 * @property bool $is_deleted
 * @property-read CallcenterTaskRecord $tasks
 */
class CallcenterTaskOperationRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return CallcenterTaskOperationRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['title', 'getTitle', 'setTitle', 'title'];
        yield ['is_deleted', 'isDeleted', 'setIsDeleted', 'isDeleted'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    public function tableName()
    {
        return 'callcenter_tasks_operations';
    }

    public function relations()
    {
        return [
            'tasks' => [self::HAS_MANY, 'MommyCom\Model\Db\CallcenterTaskRecord', 'operation_id'],
        ];
    }

    public function rules()
    {
        return [
            ['title', 'length', 'max' => 256, 'encoding' => false], //tinytext

            ['is_deleted', 'boolean'],

            ['id, title, is_deleted', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Translator::t('Action (operation)'),
            'is_deleted' => Translator::t('Removed'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.id', $provider->model->id);
        $criteria->compare($alias . '.title', $provider->model->title, true);
        $criteria->compare($alias . '.is_deleted', $provider->model->is_deleted);

        return $provider;
    }
}
