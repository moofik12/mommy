<?php

namespace MommyCom\Model\Db;

use CValidator;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserPartnerOutputRecord
 *
 * @property-description Заявки на вывод средств
 * @property-read int $id
 * @property int $partner_id
 * @property int $amount
 * @property integer $type_output
 * @property integer $status
 * @property string $status_reason
 * @property string $number_card
 * @property string $name_card
 * @property string $surname_card
 * @property string $unique_payment
 * @property int $admin_id
 * @property int $admin_update_last_at
 * @property int $created_at
 * @property int $updated_at
 * @property-read UserPartnerRecord $partner
 * @property-read AdminUserRecord $admin
 * @property-read string $typeOutputReplacement
 * @property-read string $statusReplacement
 * @mixin Timestampable
 */
class UserPartnerOutputRecord extends DoctrineActiveRecord
{
    const MIN_AMOUNT_OUTPUT_PRIVAT = 500; //минимальная сумма которую можно вывести на приват
    const MAX_AMOUNT_OUTPUT_MAMAM = 300; //минимальная сумма которую можно вывести автоматом на МАМАМ

    const PERCENT_OUTPUT_MAMAM = 10; //надбавка за вывод средств на счет мамам

    const TYPE_OUTPUT_MAMAM = 0; //mamam
    const TYPE_OUTPUT_PRIVAT = 1; //privat

    const STATUS_WAIT = 0; //в ожидании
    const STATUS_DONE = 1; //выполнено
    const STATUS_CANCEL = 2; //отменено

    /**
     * @param string $className
     *
     * @return UserPartnerOutputRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['type_output', 'getTypeOutput', 'setTypeOutput', 'typeOutput'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_reason', 'getStatusReason', 'setStatusReason', 'statusReason'];
        yield ['number_card', 'getNumberCard', 'setNumberCard', 'numberCard'];
        yield ['name_card', 'getNameCard', 'setNameCard', 'nameCard'];
        yield ['surname_card', 'getSurnameCard', 'setSurnameCard', 'surnameCard'];
        yield ['unique_payment', 'getUniquePayment', 'setUniquePayment', 'uniquePayment'];
        yield ['admin_update_last_at', 'getAdminUpdateLastAt', 'setAdminUpdateLastAt', 'adminUpdateLastAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['partner_id', 'getPartnerId', 'setPartnerId', 'partnerId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'users_partner_output';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner_id', 'required', 'on' => 'insert'],
            ['amount', 'required'],

            ['type_output, status', 'numerical', 'integerOnly' => true],

            ['type_output', 'in', 'range' => array_keys(self::typeOutputReplacements())],
            ['status', 'in', 'range' => array_keys(self::statusReplacements())],

            ['status_reason', 'length', 'max' => 100],
            ['number_card', 'length', 'max' => 20],
            ['name_card', 'length', 'max' => 30],
            ['surname_card', 'length', 'max' => 60],
            ['unique_payment', 'length', 'max' => 128],

            ['unique_payment', 'validateUniquePayment', 'type_output' => self::TYPE_OUTPUT_PRIVAT, 'status' => self::STATUS_DONE],

            ['id, partner_id, amount, type_output, status, status_reason, number_card, 
                name_card, surname_card, admin_id, admin_update_last_at, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
            'partner' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserPartnerRecord', 'partner_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', 'Application №'),
            'partner_id' => \Yii::t('common', 'Partner'),
            'amount' => \Yii::t('common', 'Amount for the payment'),
            'type_output' => \Yii::t('common', 'Output type'),
            'status' => \Yii::t('common', 'Status'),
            'status_reason' => \Yii::t('common', 'Reason for cancel'),
            'number_card' => \Yii::t('common', 'Card number'),
            'name_card' => \Yii::t('skip', 'Cardholder name'),
            'surname_card' => \Yii::t('common', 'Cardholder last name'),
            'unique_payment' => \Yii::t('common', 'Payment ID (document number)'),
            'admin_id' => \Yii::t('common', 'Admin'),
            'admin_update_last_at' => \Yii::t('common', 'Modified by admin'),
            'created_at' => \Yii::t('common', 'Created'),
            'updated_at' => \Yii::t('common', 'Updated on'),

            'typeOutputReplacement' => \Yii::t('common', 'Output type'),
            'statusReplacement' => \Yii::t('common', 'Status'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
                /*'globalLoggerChanges' => array(
                    'class' => UserGlobalLoggerBehavior::class,
                ),*/
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.partner_id', $model->partner_id);
        $criteria->compare($alias . '.amount', $model->amount);
        $criteria->compare($alias . '.type_output', $model->type_output);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.status_reason', $model->status_reason);

        $criteria->compare($alias . '.number_card', $model->number_card, true);
        $criteria->compare($alias . '.name_card', $model->name_card, true);
        $criteria->compare($alias . '.surname_card', $model->surname_card, true);
        $criteria->compare($alias . '.unique_payment', $model->unique_payment, true);

        $criteria->compare($alias . '.admin_id', $model->admin_id);

        return $provider;
    }

    public function validateUniquePayment($attribute, $params)
    {
        if ($params['type_output'] == $this->type_output && $params['status'] == $this->status) {
            $validator = CValidator::createValidator('unique', $this, [$attribute], [
                'className' => 'MommyCom\Model\Db\UserPartnerOutputRecord',
                'attributeName' => $attribute,
                'allowEmpty' => false,
            ]);
            $validator->validate($this);
        }
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        return [
            self::STATUS_WAIT => \Yii::t('common', 'Pending'),
            self::STATUS_DONE => \Yii::t('common', 'done'),
            self::STATUS_CANCEL => \Yii::t('common', 'сanceled!'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return array
     */
    public static function typeOutputReplacements()
    {
        return [
            self::TYPE_OUTPUT_MAMAM => \Yii::t('common', 'MOMMY'),
            self::TYPE_OUTPUT_PRIVAT => \Yii::t('common', 'Privat'),
        ];
    }

    /**
     * @return string
     */
    public function getTypeOutputReplacement()
    {
        $replacements = self::typeOutputReplacements();
        return isset($replacements[$this->type_output]) ? $replacements[$this->type_output] : '';
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function partnerId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.partner_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function type($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function status($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function statusIn($values)
    {
        $alias = $this->getTableAlias();
        $values = Cast::toIntArr($values);

        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function statusNotIn($values)
    {
        $alias = $this->getTableAlias();
        $values = Cast::toIntArr($values);

        $this->getDbCriteria()->addNotInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param $values
     * @param int $max
     *
     * @return $this
     */
    public function typeOutputIn($values, $max = -1)
    {
        if ($max > 0) {
            $values = array_slice($values, 0, $max);
        }
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.type_output', $values);
        return $this;
    }
}
