<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * @property-read integer $id
 * @property integer $event_id
 * @property integer $supplier_id
 * @property integer $order_id
 * @property integer $order_product_id
 * @property float $amount
 * @property int $state
 * @property int $cause_type
 * @property string $description
 * @property integer $updated_at
 * @property integer $created_at
 * @property-read string $stateText
 * @property-read string $causeTypeText
 * @property-read EventRecord $event
 * @property-read SupplierRecord $supplier
 * @property-read OrderRecord|null $order
 */
class SupplierRelationPaymentPenaltyRecord extends DoctrineActiveRecord
{
    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 0;

    const CAUSE_TYPE_OTHER = 0;
    const CAUSE_TYPE_DISTRIBUTION_DELAY = 1;
    const CAUSE_TYPE_NO_TRACKING = 2;
    const CAUSE_TYPE_MAILED_OVER_SITE = 3;
    const CAUSE_TYPE_NO_PRODUCT = 4;

    /**
     * @param string $className
     *
     * @return SupplierRelationPaymentPenaltyRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['order_product_id', 'getOrderProductId', 'setOrderProductId', 'orderProductId'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['state', 'getState', 'setState', 'state'];
        yield ['cause_type', 'getCauseType', 'setCauseType', 'causeType'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
    }

    public function tableName()
    {
        return 'suppliers_relations_payments_penalty';
    }

    public function relations()
    {
        return [
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['event_id, supplier_id', 'required'],

            ['order_id, order_product_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым, mysql 5.7 считается ошибкой при присвоении числовому полю строки

            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],
            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['order_product_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderProductRecord', 'attributeName' => 'id'],

            ['amount', 'numerical', 'max' => PHP_INT_MAX],
            ['description', 'length', 'max' => 255],

            ['state', 'default', 'value' => self::STATE_ACTIVE],
            ['state', 'in', 'range' => self::states(false)],

            ['cause_type', 'in', 'range' => self::causeTypes(false)],

            ['id, state, cause_type, event_id, supplier_id, order_id, amount, description', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'event_id' => Translator::t('Flash-sale'),
            'supplier_id' => Translator::t('Supplier'),
            'order_id' => Translator::t('Order'),
            'order_product_id' => Translator::t('Item in order'),
            'amount' => Translator::t('Amount'),
            'state' => Translator::t('Status (whether penalty fee has been paid)'),
            'cause_type' => Translator::t('Reason for penalty fee'),
            'description' => Translator::t('Description'),

            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.event_id', $model->event_id);
        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.state', $model->state);
        $criteria->compare($alias . '.description', $model->description, true);

        return $provider;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function causeTypes($replacements = true)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        $statuses = [
            self::CAUSE_TYPE_DISTRIBUTION_DELAY => Translator::t('Delay in supply'),
            self::CAUSE_TYPE_NO_TRACKING => Translator::t('Tracking information not available'),
            self::CAUSE_TYPE_MAILED_OVER_SITE => Translator::t('Order sent not through MOMMY system'),
            self::CAUSE_TYPE_NO_PRODUCT => Translator::t('Product not in stock'),
            self::CAUSE_TYPE_OTHER => Translator::t('Other'),
        ];

        return $replacements ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getCauseTypeText($default = '')
    {
        $types = self::causeTypes();

        return isset($types[$this->cause_type]) ? $types[$this->cause_type] : $default;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function states($replacements = true)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        $statuses = [
            self::STATE_ACTIVE => Translator::t('применён'),
            self::STATE_INACTIVE => Translator::t('отменён'),
        ];

        return $replacements ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStateText($default = '')
    {
        $statuses = self::states();

        return isset($statuses[$this->state]) ? $statuses[$this->state] : $default;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function stateIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.state', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function stateIs($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.state' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function causeTypeIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.cause_type', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function causeTypeIs($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.cause_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.event_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function orderProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_product_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdNotIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function supplierIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function descriptionLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.description', $value);

        return $this;
    }

    /**
     * @param int $causeType
     * @param float $amount
     *
     * @return float|int
     */
    public static function getPenaltyAmount($causeType, $amount)
    {
        $penalty = 0.0;

        switch ($causeType) {
            case self::CAUSE_TYPE_MAILED_OVER_SITE:
                $penalty = $amount * 10; //1000%
                break;

            case self::CAUSE_TYPE_NO_TRACKING:
                $penalty = $amount * 0.2; //20%
                break;

            case self::CAUSE_TYPE_DISTRIBUTION_DELAY:
                $penalty = $amount * 0.05; //5%
                break;

            case self::CAUSE_TYPE_NO_PRODUCT:
                $penalty = $amount * 0.1; //10%
                break;

            default:
        }

        return floor($penalty);
    }
}
