<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class AdminRoleAssignmentRecord
 *
 * @property-read integer $id
 * @property int $role_id
 * @property int $admin_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class AdminRoleAssignmentRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return AdminRoleAssignmentRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['role_id', 'getRoleId', 'setRoleId', 'roleId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'admins_roles_assignments';
    }

    public function relations()
    {
        return [
            'role' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminRoleRecord', 'role_id'],
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['role_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminRoleRecord', 'attributeName' => 'id'],
            ['admin_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],

            ['role_id, admin_id', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'role_id' => Translator::t('Role'),
            'admin_id' => Translator::t('Administrator'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        return $result;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $criteria = $provider->getCriteria();
        $criteria->compare('admin_id', $provider->model->admin_id);
        $criteria->compare('role_id', $provider->model->role_id);

        return $provider;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function roleIn(array $values)
    {
        $alias = $this->getTableAlias();
        $values = Cast::toUIntArr($values);

        $this->getDbCriteria()->addInCondition($alias . '.role_id', $values);
        return $this;
    }
}
