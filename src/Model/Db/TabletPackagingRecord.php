<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use Exception;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class TabletPackagingRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $created_by
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read AdminUserRecord $user
 * @property-read AdminUserRecord $createdBy
 * @property-read OrderRecord $order
 */
class TabletPackagingRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return TabletPackagingRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['created_by', 'getCreatedBy', 'setCreatedBy', 'createdBy'];
    }

    public function tableName()
    {
        return 'tablet_packaging';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'createdBy' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'created_by'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['id, user_id, order_id, created_by', 'numerical', 'integerOnly' => true],
            ['order_id', 'unique'],

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['created_by', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],

            ['id, user_id, order_id, created_by', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'user_id' => Translator::t('User'),
            'created_by' => Translator::t('Created by'),
            'order_id' => Translator::t('Order'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'createdBy.fullname' => Translator::t('Created by (Full Name)'),
        ];
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        if ($this->created_by == 0) {
            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                $this->created_by = $user->id;
            } catch (Exception $e) {
            }
        }

        return true;
    }

    /**
     * @return static
     */
    public function onlyPackaged()
    {
        $this->with([
            'order' => [
                'together' => true,
                'scopes' => [
                    'onlyPackaged',
                ],
                'joinType' => 'INNER JOIN',
            ],
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function onlyUnPackaged()
    {
        $this->with([
            'order' => [
                'together' => true,
                'scopes' => [
                    'onlyUnPackaged',
                ],
                'joinType' => 'INNER JOIN',
            ],
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function onlyPackagingReady()
    {
        $this->with([
            'order' => [
                'together' => true,
                'scopes' => [
                    'onlyPackagingReady',
                ],
                'joinType' => 'INNER JOIN',
            ],
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function onlyPackagingUnReady()
    {
        $this->with([
            'order' => [
                'together' => true,
                'scopes' => [
                    'onlyPackagingUnReady',
                ],
                'joinType' => 'INNER JOIN',
            ],
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderProcessingStatus($value)
    {
        $value = Cast::toUInt($value);

        $this->with([
            'order' => [
                'together' => true,
                'scopes' => [
                    'processingStatus' => $value,
                ],
            ],
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function orderProcessingStatuses(array $value)
    {
        $value = Cast::toUIntArr($value);

        $this->with([
            'order' => [
                'together' => true,
                'scopes' => [
                    'processingStatuses' => [$value],
                ],
            ],
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function userIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdBy($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.created_by' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.user_id', $model->user_id, true);
        $criteria->compare($alias . '.created_by', $model->created_by, true);
        $criteria->compare($alias . '.order_id', $model->order_id, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }
}
