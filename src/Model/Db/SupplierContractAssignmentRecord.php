<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Validator\UniqueMultiColumnValidator;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class SupplierContractAssignmentRecord
 *
 * @property-read integer $id
 * @property integer $supplier_id
 * @property integer $contract_id
 * @property-read SupplierRecord $supplier
 * @property-read SupplierContractRecord $supplierContractType
 */
class SupplierContractAssignmentRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return SupplierContractAssignmentRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['contract_type_id', 'getContractTypeId', 'setContractTypeId', 'contractTypeId'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
    }

    public function tableName()
    {
        return 'suppliers_contracts_assignments';
    }

    public function relations()
    {
        return [
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],
            ['contract_id', 'exist', 'className' => 'SupplierContractTypeRecord', 'attributeName' => 'id'],

            ['supplier_id', 'unique'],
            ['supplier_id+contract_id', UniqueMultiColumnValidator::class],

            ['id, supplier_id, contract_id', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'supplier_id' => Translator::t('Supplier'),
            'contract_id' => Translator::t('Contract'),

            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.contract_id', $model->contract_type_id);

        return $provider;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.supplier_id' => $value]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function supplierIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function contractTypeId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.contract_id' => $value]);

        return $this;
    }
}
