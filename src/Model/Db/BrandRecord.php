<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CEvent;
use CHtmlPurifier;
use CUploadedFile;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Misc\StringList;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class BrandRecord
 *
 * @property-read integer $id
 * @property string $logo_fileid
 * @property string $name
 * @property string $description supports simple html
 * @property string $description_short
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read BrandAliasRecord[] $aliases
 * @property-read integer $productCount количество наименований товара этого бренда
 * @property FileStorageThumbnail $logo
 * @property StringList|string[] $aliasesStrings алиасы в строчном виде
 */
class BrandRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return BrandRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['logo_fileid', 'getLogoFileid', 'setLogoFileid', 'logoFileid'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['description_short', 'getDescriptionShort', 'setDescriptionShort', 'descriptionShort'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    public function tableName()
    {
        return 'brands';
    }

    public function relations()
    {
        return [
            'aliases' => [self::HAS_MANY, 'MommyCom\Model\Db\BrandAliasRecord', 'brand_id'],
            'productCount' => [self::STAT, 'MommyCom\Model\Db\EventProductRecord', 'brand_id'],
        ];
    }

    public function rules()
    {
        return [
            ['name, description', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name', 'required'],
            ['name', 'length', 'min' => 1, 'max' => 100],
            ['name', 'unique'],
            ['description', 'length', 'max' => 65000],
            ['description_short', 'length', 'max' => 250],
            ['description', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],

            ['aliasesStrings', 'default', 'setOnEmpty' => true, 'value' => []],

            ['id, name, logo_fileid', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', '№'),
            'name' => \Yii::t('common', 'Brand'),
            'aliases' => \Yii::t('common', 'Aliases'),
            'aliasesStrings' => \Yii::t('common', 'Aliases'),
            'description' => \Yii::t('common', 'Description'),
            'description_short' => \Yii::t('common', 'Short description'),

            'logo_fileid' => \Yii::t('common', 'Label'),
            'logo' => \Yii::t('common', 'Label'),
        ];
    }

    public function beforeDelete()
    {
        if (!$result = parent::beforeDelete()) {
            return $result;
        }

        foreach ($this->aliases as $alias) {
            $alias->delete();
        }

        return true;
    }

    /**
     * @param string $name ищет по имени и по алиасу
     *
     * @return BrandRecord|null
     */
    public function findByName($name)
    {
        $record = static::model()->name($name)->find();
        if ($record === null) {
            $record = static::model()->aliasName($name)->find();
        }

        return $record;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameOrAliasLike($value)
    {
        $value = Cast::toStr($value);
        $alias = $this->getTableAlias();

        $this->nameLike($value);
        $aliasIds = BrandAliasRecord::model()->nameLike($value)->findColumnDistinct('brand_id');
        $this->getDbCriteria()->addInCondition($alias . '.id', $aliasIds, 'OR');

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function aliasName($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();

        $aliasIds = BrandAliasRecord::model()->name($value)->findColumnDistinct('brand_id');
        $this->getDbCriteria()->addInCondition($alias . '.' . 'id', $aliasIds);

        return $this;
    }

    /**
     * @return $this
     */
    public function hasLogo()
    {
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.logo_fileid !' => '',
        ]);

        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);
        if ($model->logo_fileid != '') {
            $criteria->compare($alias . '.logo_fileid', Cast::toBool($model->logo_fileid) ? '<> ' : ' ');
        }

        return $provider;
    }

    /**
     * @param StringList|string[] $values
     */
    public function setAliasesStrings($values)
    {
        $values = new StringList($values);
        $remove = [];
        /* @var $remove BrandAliasRecord[] */

        if (!$this->isNewRecord) {
            foreach ($this->aliases as $alias) {
                if (($index = ArrayUtils::indexOf($values, $alias->name)) >= 0) {
                    unset($values[$index]); // уже добавлен
                } else {
                    $remove[] = $alias;
                }
            }
        }

        $this->attachEventHandler('onAfterSave', function ($data) use ($values, $remove) { // правим алиасы только после того как бренд сохранен
            /* @var $data CEvent */
            $record = $data->sender;
            /* @var $record BrandRecord */
            $record->refresh();

            foreach ($remove as $alias) {
                $alias->delete(); // был удален из списка
            }

            foreach ($values as $value) {
                if (strlen($value) > 0) {
                    $alias = new BrandAliasRecord();
                    $alias->brand_id = $record->id;
                    $alias->name = $value;
                    $alias->save();
                }
            }

            return true;
        });

        return;
    }

    /**
     * @return StringList|string[]
     */
    public function getAliasesStrings()
    {
        $strings = ArrayUtils::getColumn($this->aliases, 'name');
        return new StringList($strings);
    }

    /**
     * Возвращает последние события связанные с этим брендом
     *
     * @param int $limit
     *
     * @return EventRecord[]
     */
    public function getLastEvents($limit = 10)
    {
        return EventRecord::model()->brandId($this->id)->orderBy('end_at', 'desc')->limit($limit)->findAll();
    }

    /**
     * @return FileStorageThumbnail
     */
    public function getLogo()
    {
        return new FileStorageThumbnail($this->logo_fileid, [
            // here thumbs
        ]);
    }

    /**
     * @param Image|FileStorageThumbnail|CUploadedFile|string $value |null
     *
     * @return static
     */
    public function setLogo($value)
    {
        if (is_string($value)) {
            $value = \Yii::app()->filestorage->get($value);
        }

        if ($value === null) {
            if (!empty($this->logo_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->logo_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }

                $this->logo_fileid = '';
            }
        }

        if ($value instanceof FileStorageThumbnail) {
            $value = $value->file;
        }

        if ($value instanceof CUploadedFile) {
            $value = \Yii::app()->filestorage->factoryFromLocalFile($value->tempName);
        }

        if ($value instanceof Image && $value->getEncodedId() != $this->logo_fileid) {
            if (!empty($this->logo_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->logo_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }
            }
            if (!$value->isNewFile()) {
                $value = $value->duplicate();
            }
            $this->logo_fileid = $value->getEncodedId();
        }

        return $this;
    }
}

