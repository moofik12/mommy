<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * @property-read int $id
 * @property int $order_id
 * @property int $supplier_id
 * @property int $status
 * @property bool $is_created_supplier
 * @property string $message
 * @property string $message_callcenter
 * @property string $bank_name_enc
 * @property string $bank_giro_enc
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property string $bankName
 * @property string $bankGiro
 * @property-read string $statusText
 * @property-read OrderRecord $order
 * @property-read SupplierRecord $supplier
 * @mixin Timestampable
 */
class SupplierOrderReturnRecord extends HasBankDataRecord
{
    const STATUS_NEW = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_CANCELED = 2;

    const TIME_AVAILABLE_DELETE_AFTER_CREATE = 14400; //4 часа

    /**
     * @param string $className
     *
     * @return SupplierOrderReturnRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['is_created_supplier', 'isCreatedSupplier', 'setIsCreatedSupplier', 'isCreatedSupplier'];
        yield ['message', 'getMessage', 'setMessage', 'message'];
        yield ['message_callcenter', 'getMessageCallcenter', 'setMessageCallcenter', 'messageCallcenter'];
        yield ['bank_name_enc', 'getBankNameEnc', 'setBankNameEnc', 'bankNameEnc'];
        yield ['bank_giro_enc', 'getBankGiroEnc', 'setBankGiroEnc', 'bankGiroEnc'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
    }

    public function tableName()
    {
        return 'suppliers_orders_returns';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['order_id, supplier_id', 'required'],

            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],

            ['bank_name_enc, bank_giro_enc', 'length', 'max' => 255],
            ['message, message_callcenter', 'length', 'max' => 400],
            ['status', 'in', 'range' => self::statuses(false)],
            ['status', 'default', 'value' => self::STATUS_NEW],

            ['is_created_supplier', 'boolean'],

            ['id, order_id, supplier_id, status, bank_name_enc, bank_giro_enc, message, message_callcenter', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'order_id' => \Yii::t('common', 'Order'),
            'supplier_id' => \Yii::t('common', 'Supplier'),
            'status' => \Yii::t('common', 'Status'),
            'statusText' => \Yii::t('common', 'Status'),
            'bank_name_enc' => \Yii::t('skip', 'Bank name'),
            'bankName' => \Yii::t('skip', 'Bank name'),
            'bank_giro_enc' => \Yii::t('skip', 'Settlement account number/ Bank card number'),
            'bankGiro' => \Yii::t('skip', 'Settlement account number/ Bank card number'),
            'message' => \Yii::t('common', 'Comments'),
            'message_callcenter' => \Yii::t('common', 'Callcenter comment'),
            'is_created_supplier' => \Yii::t('common', 'Created by supplier'),

            'updated_at' => \Yii::t('common', 'Updated'),
            'created_at' => \Yii::t('common', 'Created'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);
        $alias = $this->getTableAlias();
        /** @var self $model */
        $model = $provider->model;

        $criteria = $provider->getCriteria();
        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.bank_giro_enc', $model->bankGiro);
        $criteria->compare($alias . '.bank_name_enc', $model->bankName);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.message', $model->message, true);
        $criteria->compare($alias . '.message_callcenter', $model->message_callcenter, true);
        $criteria->compare($alias . '.is_created_supplier', $model->is_created_supplier);

        return $provider;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function statuses($replacements = true)
    {
        $statuses = [
            self::STATUS_NEW => Translator::t('New'),
            self::STATUS_PROCESSED => Translator::t('Processed'),
            self::STATUS_CANCELED => Translator::t('Canceled'),
        ];

        return $replacements ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string|mixed
     */
    public function getStatusText($default = null)
    {
        if (is_null($default)) {
            $default = Translator::t('unknown');
        }
        $statuses = self::statuses();

        return isset($statuses[$this->status]) ? $statuses[$this->status] : $default;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function supplierIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function statusIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function createdSupplier($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_created_supplier' => Cast::toUInt($value),
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function orderIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function canDeleted($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();
        $time = time();

        $condition = "<";
        if (!$value) {
            $condition = ">";
        }
        $condition .= $time;

        $this->getDbCriteria()->compare("$alias.created_at+" . self::TIME_AVAILABLE_DELETE_AFTER_CREATE, $condition);
        return $this;
    }

    /**
     * @return boolean whether the record should be deleted. Defaults to true.
     */
    protected function beforeDelete()
    {
        if (!$this->is_created_supplier && !$this->isPossibleCallcenterDelete()) {
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @return string
     */
    public function getbankName(): string
    {
        return self::_bankDataDecode($this->bank_name_enc);
    }

    /**
     * @param $value
     */
    public function setbankName($value)
    {
        $this->bank_name_enc = self::_bankDataEncode($value);
    }

    /**
     * @return string
     */
    public function getbankGiro(): string
    {
        return self::_bankDataDecode($this->bank_giro_enc);
    }

    /**
     * @param $value
     */
    public function setbankGiro($value)
    {
        $this->bank_giro_enc = self::_bankDataEncode($value);
    }

    /**
     * @return string
     */
    protected static function _bankDataPrivateKey(): string
    {
        return pack('H*', '5d0b5d233e8c3b6e8a839447392e9a82ec15dd9de2e0788e192e2292987d5cc6');
    }

    /* API */

    /**
     * @return bool
     */
    public function isPossibleSupplierDelete()
    {
        return $this->is_created_supplier && self::getEndDeleteDateDatetime($this->created_at) > time();
    }

    /**
     * @return bool
     */
    public function isPossibleCallcenterDelete()
    {
        return !$this->is_created_supplier && self::getEndDeleteDateDatetime($this->created_at) > time();
    }

    /**
     * @return bool
     */
    public function isPossibleCallcenterUpdate()
    {
        return $this->status == self::STATUS_NEW;
    }

    /**
     * @param int $now
     *
     * @return int
     */
    public static function getEndDeleteDateDatetime($now)
    {
        $now = Cast::toUInt($now);

        return $now + self::TIME_AVAILABLE_DELETE_AFTER_CREATE;
    }
}
