<?php

namespace MommyCom\Model\Db;

use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class PromoUserRecord
 *
 * @property-read integer $id
 * @property-read string $name
 * @property-read string $email
 * @property-read integer $status
 * @property-read string $registrationHash
 */
class PromoUserRecord extends DoctrineActiveRecord
{
    const STATUS_UNCONFIRMED = 0;
    const STATUS_CONFIRMED = 1;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['adate', 'getAdate', 'setAdate', 'adate'];
        yield ['from', 'getFrom', 'setFrom', 'from'];
        yield ['hash', 'getHash', 'setHash', 'hash'];
        yield ['actionpay', 'getActionpay', 'setActionpay', 'actionpay'];
        yield ['download', 'getDownload', 'setDownload', 'download'];
    }

    public function tableName()
    {
        return 'promo';
    }

    /**
     * @param boolean $value
     *
     * @return PromoUserRecord
     */
    public function status($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);

        return $this;
    }

    public function onlyUnregistered()
    {
        $tableName = UserRecord::model()->tableName();

        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();

        $query = "NOT EXISTS(SELECT 1 FROM $tableName WHERE email=$alias.email LIMIT 1)";

        $criteria->addCondition($query);

        return $this;
    }


    // item api

    /**
     * @return string
     */
    public function getRegistrationHash()
    {
        static $internalSalt = 'u8ashdn23hsgdyf@';
        return hash('md5', $internalSalt . '_' . $this->id);
    }

    /**
     * @return UserRecord|null
     */
    public function getRegisteredUser()
    {
        $email = Utf8::trim($this->email);
        return UserRecord::model()->emailIs($email)->find();
    }

    /**
     * @return boolean
     */
    public function getIsRegistered()
    {
        return $this->getRegisteredUser() !== null;
    }
}
