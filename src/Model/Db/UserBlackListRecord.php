<?php

namespace MommyCom\Model\Db;

use Exception;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * @property-read integer $id
 * @property integer $user_id
 * @property integer $status
 * @property integer $person_id //пользователь который добавил запись, обновил
 * @property integer $redemption_orders
 * @property string $comment
 * @property-read string $history_json
 * @property-read integer $updated_at
 * @property-read integer $created_at
 */
class UserBlackListRecord extends DoctrineActiveRecord
{
    const STATUS_ACTIVE = 0;
    const STATUS_INACTIVE = 9; //времееное исключение из списка
    const STATUS_DELETED = 10;  //исключили из списка без ограничения по времени

    public $searchUserEmail = '';

    /**
     * @param string $className
     *
     * @return UserBlackListRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['redemption_orders', 'getRedemptionOrders', 'setRedemptionOrders', 'redemptionOrders'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['history_json', 'getHistoryJson', 'setHistoryJson', 'historyJson'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['person_id', 'getPersonId', 'setPersonId', 'personId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'users_black_list';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'person' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'person_id'],
        ];
    }

    public function rules()
    {
        return [
            // хак чтобы Validators считал значение "0" пустым
            ['person_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],

            ['user_id, comment', 'required'],
            ['user_id', 'unique'],

            ['status', 'in', 'range' => self::statuses(false)],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],

            ['person_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],

            ['comment', 'length', 'max' => 255, 'encoding' => false],

            ['redemption_orders', 'numerical', 'max' => 1],

            //grid
            ['user_id, status, person_id, comment, redemption_orders, searchUserEmail'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => Translator::t('User'),
            'comment' => Translator::t('Comments'),
            'person_id' => Translator::t('Added'),
            'status' => Translator::t('Status'),
            'redemption_orders' => Translator::t('Settling an order'),

            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),

            'searchUserEmail' => 'Email',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.person_id', $model->person_id);
        $criteria->compare($alias . '.comment', $model->comment, true);
        $criteria->compare($alias . '.redemption_orders', $model->redemption_orders, true);

        $criteria->with = ['user'];
        $criteria->compare('user.email', $model->searchUserEmail, true);

        return $provider;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function statuses($replacements = true)
    {
        $statuses = [
            self::STATUS_ACTIVE => Translator::t('Active'),
            self::STATUS_INACTIVE => Translator::t('Inactive'), //возможно восстановить
            self::STATUS_DELETED => Translator::t('Deleted'),
        ];

        return $replacements ? $statuses : array_keys($statuses);
    }

    public function beforeSave()
    {
        $lastHistory = new UserBlackListHistory;
        $history = $this->getHistory();
        if ($history) {
            $lastHistory = end($history);
        }

        //for console
        $user = null;
        try {
            $user = \Yii::app()->user->model;
        } catch (Exception $e) {
        }

        $newHistory = new UserBlackListHistory();
        $newHistory->timestamp = time();
        $newHistory->person_id = $user ? $user->id : 0;
        $newHistory->person_name = $user ?
            $user->login . ' (' . $user->fullname . ')' : Translator::t('System');
        $newHistory->status = $this->status;
        $newHistory->comment = $this->comment;

        if (array_diff_assoc($newHistory->getAttributes($newHistory->getAttributeToWatch()), $lastHistory->getAttributes($lastHistory->getAttributeToWatch()))) {
            $history = array_map(function ($item) {
                /** @var  UserBlackListHistory $item */
                return $item->getAttributes();
            }, $history);

            $history[] = $newHistory->getAttributes();

            $this->history_json = json_encode(array_values($history), JSON_UNESCAPED_UNICODE, 3);
        }

        return parent::beforeSave();
    }

    /**
     * @return UserBlackListHistory[]
     */
    public function getHistory()
    {
        $history = json_decode($this->history_json, true);
        $items = [];

        if (is_array($history)) {
            foreach ($history as $item) {
                $record = new UserBlackListHistory();
                $record->setAttributes($item);

                $items[] = $record;
            }
        }

        return $items;
    }

    public function getStatusReplacement($default = '')
    {
        $statuses = UserBlackListRecord::statuses();
        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $default;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }
}
