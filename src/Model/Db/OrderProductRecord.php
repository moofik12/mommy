<?php

namespace MommyCom\Model\Db;

use CDbCriteria;
use CException;
use MommyCom\Entity\OrderProduct;
use MommyCom\Model\Behavior\BonuspointsCheckout;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class OrderProductRecord
 * ВНИМЕНИЕ! есть проблема некорректной обработки уведомлений при использовании $this->order->save() при первой обработки заказа,
 * предпочтительней использовать $this->order->updateByPk()
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $relation_order_product_id
 * @property integer $catalog_product_id ProductRecord->id
 * @property integer $event_id
 * @property integer $product_id EventProductRecord->id
 * @property string $product_sizeformat
 * @property string $product_size
 * @property integer $number количество товара
 * @property float $price стоимость ОДНОГО товара при формлении заказа
 * @property boolean $is_added_by_callcenter
 * @property integer $callcenter_status
 * @property integer $callcenter_status_prev
 * @property string $callcenter_comment
 * @property integer $storekeeper_status
 * @property integer $storekeeper_status_prev
 * @property string $storekeeper_comment
 * @property string $callcenter_status_history_json
 * @property string $storekeeper_status_history_json
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read UserRecord $user
 * @property-read EventProductRecord $product
 * @property-read ProductRecord $catalogProduct
 * @property-read EventRecord $event
 * @property-read OrderRecord $order
 * @property-read OrderProductRecord $relationOrderProduct
 * @property-read OrderProductRecord[] $relationCloneOrderProducts
 * @property-read WarehouseProductRecord[] $connectedWarehouseProducts
 * @property-read WarehouseProductRecord[] $warehouseProducts
 * @property-read integer $connectedWarehouseProductCount
 * @property-read float $priceTotal стоимость за все товары
 * @property-read boolean $isWarehouseFullyConnected
 * @property-read integer $warehouseNumberAvailable Доступное количество товаров на складе доступное для данного заказа
 */
class OrderProductRecord extends DoctrineActiveRecord
{
    public const CALLCENTER_STATUS_UNMODERATED = OrderProduct::CALLCENTER_STATUS_UNMODERATED;
    public const CALLCENTER_STATUS_CANCELLED = OrderProduct::CALLCENTER_STATUS_CANCELLED;
    public const CALLCENTER_STATUS_ACCEPTED = OrderProduct::CALLCENTER_STATUS_ACCEPTED;

    const STOREKEEPER_STATUS_UNMODERATED = 0;
    const STOREKEEPER_STATUS_ACCEPTED = 1;
    const STOREKEEPER_STATUS_UNAVAILABLE = 2;

    const VIRTUAL_STATUS_UNKNOWN = 0;
    const VIRTUAL_STATUS_WAITING_END_EVENT = 1;
    const VIRTUAL_STATUS_WAITING_DELIVERY_SUPPLIER = 2;
    const VIRTUAL_STATUS_IN_WAREHOUSE = 3;
    const VIRTUAL_STATUS_SENT = 4;
    const VIRTUAL_STATUS_CANCELED = 5;

    /**
     * @param string $className
     *
     * @return OrderProductRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['product_sizeformat', 'getProductSizeformat', 'setProductSizeformat', 'productSizeformat'];
        yield ['product_size', 'getProductSize', 'setProductSize', 'productSize'];
        yield ['product_stock_id', 'getProductStockId', 'setProductStockId', 'productStockId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['number', 'getNumber', 'setNumber', 'number'];
        yield ['price', 'getPrice', 'setPrice', 'price'];
        yield ['is_added_by_callcenter', 'isAddedByCallcenter', 'setIsAddedByCallcenter', 'isAddedByCallcenter'];
        yield ['callcenter_status', 'getCallcenterStatus', 'setCallcenterStatus', 'callcenterStatus'];
        yield ['callcenter_status_prev', 'getCallcenterStatusPrev', 'setCallcenterStatusPrev', 'callcenterStatusPrev'];
        yield ['callcenter_status_history_json', 'getCallcenterStatusHistoryJson', 'setCallcenterStatusHistoryJson', 'callcenterStatusHistoryJson'];
        yield ['callcenter_comment', 'getCallcenterComment', 'setCallcenterComment', 'callcenterComment'];
        yield ['storekeeper_status', 'getStorekeeperStatus', 'setStorekeeperStatus', 'storekeeperStatus'];
        yield ['storekeeper_status_prev', 'getStorekeeperStatusPrev', 'setStorekeeperStatusPrev', 'storekeeperStatusPrev'];
        yield ['storekeeper_status_history_json', 'getStorekeeperStatusHistoryJson', 'setStorekeeperStatusHistoryJson', 'storekeeperStatusHistoryJson'];
        yield ['storekeeper_comment', 'getStorekeeperComment', 'setStorekeeperComment', 'storekeeperComment'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['relation_order_product_id', 'getRelationOrderProductId', 'setRelationOrderProductId', 'relationOrderProductId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['catalog_product_id', 'getCatalogProductId', 'setCatalogProductId', 'catalogProductId'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
    }

    public function tableName()
    {
        return 'orders_products';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'product' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventProductRecord', 'product_id'],
            'catalogProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\ProductRecord', 'catalog_product_id'],
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'relationOrderProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderProductRecord', 'relation_order_product_id'],
            'relationCloneOrderProducts' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'relation_order_product_id'],

            'connectedWarehouseProducts' => [self::HAS_MANY, 'MommyCom\Model\Db\WarehouseProductRecord', 'order_product_id'],
            'connectedWarehouseProductCount' => [self::STAT, 'MommyCom\Model\Db\WarehouseProductRecord', 'order_product_id'],
            'warehouseProducts' => [
                self::HAS_MANY,
                'MommyCom\Model\Db\WarehouseProductRecord',
                '',
                'foreignKey' => ['product_id' => 'catalog_product_id'],
            ],
        ];
    }

    public function rules()
    {
        return [
            ['user_id, product_id, event_id, number', 'required'],
            ['number', 'numerical', 'min' => 1],

            ['callcenter_comment, storekeeper_comment', 'length', 'max' => 2048, 'encoding' => false,
                'tooLong' => Translator::t('Comment is too long')],

            ['callcenter_status', 'in', 'range' => [
                self::CALLCENTER_STATUS_UNMODERATED,
                self::CALLCENTER_STATUS_CANCELLED,
                self::CALLCENTER_STATUS_ACCEPTED,
            ]],

            ['storekeeper_status', 'in', 'range' => [
                self::STOREKEEPER_STATUS_UNMODERATED,
                self::STOREKEEPER_STATUS_ACCEPTED,
                self::STOREKEEPER_STATUS_UNAVAILABLE,
            ]],

            ['is_added_by_callcenter', 'boolean'],

            // хак чтобы CExistValidator считал значение "0" пустым
            ['relation_order_product_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],

            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['relation_order_product_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderProductRecord', 'attributeName' => 'id'],
            ['relation_order_product_id', 'relationOrderProductValidator'],
            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['product_id', 'exist', 'className' => 'MommyCom\Model\Db\EventProductRecord', 'attributeName' => 'id'],
            ['catalog_product_id', 'exist', 'className' => 'MommyCom\Model\Db\ProductRecord', 'attributeName' => 'id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'order_id' => Translator::t('Order'),
            'user_id' => Translator::t('User'),
            'catalog_product_id' => Translator::t('Order'),
            'product_id' => Translator::t('Product in flash-sale'),
            'product_sizeformat' => Translator::t('Size format'),
            'product_size' => Translator::t('Product size'),
            'event_id' => Translator::t('Special offer'),
            'number' => Translator::t('Quantity of product'),
            'is_added_by_callcenter' => Translator::t('Added via Callcenter'),
            'callcenter_status' => Translator::t('Callcenter status'),
            'storekeeper_status' => Translator::t('Warehouse status'),
            'callcenter_comment' => Translator::t('Callcenter comment'),
            'storekeeper_comment' => Translator::t('Storekeeper comment'),
            'price' => Translator::t('Price'),
            'priceTotal' => Translator::t('Price total'),
            'relation_order_product_id' => Translator::t('Copied from product of the order'), //был скопирован из товара в заказе

            'callcenterStatusReplacement' => Translator::t('Callcenter status'),
            'storekeeperStatusReplacement' => Translator::t('Warehouse status'),
        ];
    }

    public function relationOrderProductValidator($attribute, $params)
    {
        if ($this->callcenter_status == self::CALLCENTER_STATUS_CANCELLED) {
            return;
        }

        $activeRelationStatuses = [self::CALLCENTER_STATUS_UNMODERATED, self::CALLCENTER_STATUS_ACCEPTED];
        if ($this->relationOrderProduct
            && in_array($this->relationOrderProduct->callcenter_status, $activeRelationStatuses)
        ) {
            $this->addError($attribute, Translator::t('Found unclaimed duplicate of goods in order No.') . $this->relationOrderProduct->order_id);
        }

        foreach ($this->relationCloneOrderProducts as $cloneProduct) {
            if (in_array($cloneProduct->callcenter_status, $activeRelationStatuses)) {
                $this->addError($attribute, Translator::t('Found unclaimed duplicate of goods in order No.') . $cloneProduct->order_id);
            }
        }
    }

    protected function _resolveProductAdditionalInfo()
    {
        $product = $this->product;
        if ($product === null) {
            return;
        }

        $this->event_id = $product->event_id;
        $this->catalog_product_id = $product->product_id;
        $this->product_sizeformat = $product->sizeformat;
        $this->product_size = $product->size;
    }

    protected function _refreshProductNumberSold()
    {
        $product = $this->product;
        if ($product === null) {
            return;
        }

        // количество которое продали
        $sold = $product->number_sold;
        $soldReal = $product->number_sold_real;

        $product->forceRefreshSoldNumber();

        if ($sold != $product->number_sold || $soldReal != $product->number_sold_real) {
            $product->save(true, ['number_sold', 'number_sold_real']);
        }
    }

    protected function _refreshProductIsSoldOut()
    {
        $product = $this->product;
        if ($product === null) {
            return;
        }

        $product->updateIsSoldOutNumber(true);
    }

    protected function _resolvePrice()
    {
        if ($this->isNewRecord) {
            $this->price = $this->product->price;
        }
    }

    protected function _refreshOrderTotalPrice()
    {
        $order = $this->order;
        if ($order !== null && $order->price_total != $order->forceRefreshTotalPrice()->price_total) {
            //проблема отправки уведомлений перед созданием заказа
            if ($order->validate(['price_total'])) {
                $order->updateByPk($order->getPrimaryKey(), ['price_total' => $order->price_total]);
            }
        }
    }

    protected function _refreshOrderBonuses()
    {
        $order = $this->order;

        if ($order === null) {
            return;
        }

        $bonuses = $order->asa('bonuspointsCheckout');

        if ($bonuses === null || !$bonuses instanceof BonuspointsCheckout) {
            return;
        }

        $bonuses->checkout(true); // форсированно меняем бонусы
    }

    /**
     * Отсоединяет складской товар если позиция отменена
     */
    protected function _refreshWarehouseState()
    {
        if ($this->isNewRecord) {
            return;
        }

        $items = WarehouseProductRecord::model()
            ->orderProductId($this->id)
            ->orderId($this->order_id)
            ->findAll();
        /* @var $items WarehouseProductRecord[] */
        if ($this->callcenter_status == self::CALLCENTER_STATUS_CANCELLED) {
            foreach ($items as $item) {
                $item->order_product_id = 0;
                $item->save();
            }
        } else {
            foreach ($items as $item) {
                $item->forceResolveOrderState();
                $item->save();
            }
        }
    }

    /**
     * Обновляет историю смены статусов callcenter
     */
    protected function _updateStatusChangeStackCallcenter()
    {
        $statusHistory = $this->getCallcenterStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->callcenter_status) {
            $now = time();

            $addToHistory = [
                'status' => $this->callcenter_status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;

            $this->callcenter_status_prev = $prevStatus['status'];

            $logRec = new OrderProductCallcenterStatusHistoryRecord();
            $logRec->order_id = $this->order_id;
            $logRec->product_id = $this->id;
            $logRec->user_id = $addToHistory['admin_id'];
            $logRec->status = $this->callcenter_status;
            $logRec->status_prev = $this->callcenter_status_prev;
            $logRec->client_id = $this->user_id;
            $logRec->comment = $this->callcenter_comment;
            $logRec->save();
        }
        ksort($statusHistory);

        $this->callcenter_status_history_json = json_encode($statusHistory);
    }

    /**
     * Обновляет историю смены статусов склада
     */
    protected function _updateStatusChangeStackStorekeeper()
    {
        $statusHistory = $this->getStorekeeperStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->storekeeper_status) {
            $now = time();

            $addToHistory = [
                'status' => $this->storekeeper_status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $user = \Yii::app()->user->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;

            $this->storekeeper_status_prev = $prevStatus['status'];

            $logRec = new OrderProductStorekeeperStatusHistoryRecord();
            $logRec->order_id = $this->order_id;
            $logRec->product_id = $this->id;
            $logRec->user_id = $addToHistory['admin_id'];
            $logRec->status = $this->storekeeper_status;
            $logRec->status_prev = $this->storekeeper_status_prev;
            $logRec->client_id = $this->user_id;
            $logRec->comment = $this->storekeeper_comment;
            $logRec->save();
        }
        ksort($statusHistory);

        $this->storekeeper_status_history_json = json_encode($statusHistory);
    }

    /**
     * @param $products
     *
     * @return static[]
     */
    public static function getProducts($products, $order)
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('product_id', $products);
        $criteria->addCondition('order_id' . '=' . $order, 'AND');
        $data = self::model()->findAll($criteria);
        return $data;
    }

    public function beforeValidate()
    {
        if (!$result = parent::beforeValidate()) {
            return $result;
        }

        $this->_resolvePrice();
        $this->_resolveProductAdditionalInfo();

        return true;
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolvePrice();
        $this->_resolveProductAdditionalInfo();

        $this->_updateStatusChangeStackStorekeeper();
        $this->_updateStatusChangeStackCallcenter();
        return true;
    }

    public function afterSave()
    {
        parent::afterSave();

        $this->_refreshProductNumberSold();
        $this->_refreshOrderTotalPrice();
        $this->_refreshWarehouseState();
        $this->_refreshOrderBonuses();

        $this->_refreshProductIsSoldOut(); //после отработка склада
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $this->_refreshProductNumberSold();
        $this->_refreshOrderBonuses();

        $this->_refreshProductIsSoldOut(); //после отработка склада
    }

    /**
     * Только те товары которые полностью упакованы
     *
     * @return static
     */
    public function onlyPackaged()
    {
        $alias = $this->getTableAlias();
        $warehouseTable = WarehouseProductRecord::model()->tableName();

        $selectQuery = "SELECT COUNT(1) FROM $warehouseTable WHERE ";
        $selectQuery .= "$warehouseTable.order_id = $alias.order_id AND ";
        $selectQuery .= "$warehouseTable.order_product_id = $alias.id AND ";
        $selectQuery .= "$warehouseTable.status <> " . Cast::toStr(WarehouseProductRecord::STATUS_UNMODERATED);
        $query = "($selectQuery ) >= $alias.number";

        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * Только те товары которые не полностью упакованы
     *
     * @return static
     */
    public function onlyUnPackaged()
    {
        $alias = $this->getTableAlias();
        $warehouseTable = WarehouseProductRecord::model()->tableName();

        $selectQuery = "SELECT COUNT(1) FROM $warehouseTable WHERE ";
        $selectQuery .= "$warehouseTable.order_id = $alias.order_id AND ";
        $selectQuery .= "$warehouseTable.order_product_id = $alias.id";
        $query = "($selectQuery ) < $alias.number";

        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * Только те товары которые готовы к упакевке
     *
     * @return static
     */
    public function onlyPackagingReady()
    {
        $alias = $this->getTableAlias();

        /* hard hacks */

        $warehouseTable = WarehouseProductRecord::model()->tableName();

        $inWarehouseStatus = Cast::toStr(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED);
        $mailedToClientStatus = Cast::toStr(WarehouseProductRecord::STATUS_MAILED_TO_CLIENT);

        $selectQuery = "SELECT COUNT(1) FROM $warehouseTable AS wh WHERE ";
        $selectQuery .= "(";
        $selectQuery .= "(wh.order_id = 0 AND wh.status = $inWarehouseStatus) OR ";
        $selectQuery .= "(wh.order_id = $alias.order_id AND wh.status IN ($inWarehouseStatus, $mailedToClientStatus))";
        $selectQuery .= ")";
        $selectQuery .= " AND ";

        $selectQuery .= "(
            wh.product_id = $alias.catalog_product_id AND
            wh.event_product_sizeformat = $alias.product_sizeformat AND
            wh.event_product_size = $alias.product_size
        )";

        $query = "($selectQuery ) >= $alias.number";

        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * Только те товары которые не готовы к упаковке
     *
     * @return static
     */
    public function onlyPackagingUnReady()
    {
        $alias = $this->getTableAlias();

        /* hard hacks */

        $warehouseTable = WarehouseProductRecord::model()->tableName();

        $inWarehouseStatus = Cast::toStr(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED);
        $mailedToClientStatus = Cast::toStr(WarehouseProductRecord::STATUS_MAILED_TO_CLIENT);

        $selectQuery = "SELECT COUNT(1) FROM $warehouseTable AS wh WHERE ";
        $selectQuery .= "(";
        $selectQuery .= "(wh.order_id = 0 AND wh.status = " . Cast::toStr(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED) . ") OR ";
        $selectQuery .= "(wh.order_id = $alias.order_id AND wh.status IN ($inWarehouseStatus, $mailedToClientStatus))";
        $selectQuery .= ")";
        $selectQuery .= " AND ";

        $selectQuery .= "(
            wh.product_id = $alias.catalog_product_id AND
            wh.event_product_sizeformat = $alias.product_sizeformat AND
            wh.event_product_size = $alias.product_size
        )";

        $query = "($selectQuery ) < $alias.number";

        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function catalogProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'catalog_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function productId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function productSizeformat($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_sizeformat' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function productSize($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_size' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param $values
     *
     * @return $this
     */
    public function orderIds($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.order_id', $values
        );
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function callcenterStatus($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'callcenter_status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function storekeeperStatus($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'storekeeper_status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function callcenterStatuses($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.' . 'callcenter_status', $values
        );
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function storekeeperStatuses($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.' . 'storekeeper_status', $values
        );
        return $this;
    }

    /// item api

    /**
     * @return float
     */
    public function getPriceTotal()
    {
        return $this->price * $this->number;
    }

    /**
     * Принудительное обновление стоимости товара
     *
     * @return $this
     */
    public function forceRefreshPrice()
    {
        $this->price = $this->product->price;
        return $this;
    }

    /**
     * Заказ полностью выполнен
     *
     * @param boolean $confirmed
     * @param boolean $security
     *
     * @return bool
     */
    public function getIsWarehouseFullyConnected($confirmed = true, $security = true)
    {
        if ($confirmed || $security) {
            $confirmedCount = 0;
            foreach ($this->connectedWarehouseProducts as $product) {
                $valid = true;
                $valid = $valid && (!$confirmed || !in_array($product->status, [
                            WarehouseProductRecord::STATUS_UNMODERATED,
                            WarehouseProductRecord::STATUS_BROKEN,
                        ]));
                $valid = $valid && (!$security || $product->is_security_code_checked);

                if ($valid) {
                    $confirmedCount++;
                }
            }
            return $this->number == $confirmedCount;
        } else {
            return $this->number == $this->connectedWarehouseProductCount;
        }
    }

    /**
     * Все ли товары есть на складе
     *
     * @return bool
     */
    public function getIsAllInWarehouse()
    {
        return $this->number == $this->getCountReadyInWarehouse();
    }

    /**
     * Товары которые уже есть на складе
     *
     * @return int
     */
    public function getCountReadyInWarehouse()
    {
        $count = 0;

        foreach ($this->connectedWarehouseProducts as $warehouseProduct) {
            if ($warehouseProduct->status == WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED
                && $warehouseProduct->lost_status == WarehouseProductRecord::LOST_STATUS_CORRECT
            ) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Доступное количество товаров на складе доступное для данного заказа
     *
     * @param bool $possible включая возможную поставку
     *
     * @return integer количество уже зарезервированного + никем не заерезервированные товар
     */
    public function getWarehouseNumberAvailable($possible = false)
    {
        $selector = WarehouseProductRecord::model()
            ->productId($this->product->product_id)
            ->eventProductSizeformat($this->product->sizeformat)
            ->eventProductSize($this->product->size)
            ->onlyNotHasOrderConnected();

        if ($possible && $this->event && $this->event->mailing_start_at > time()) {
            $selector->statusIn([WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED, WarehouseProductRecord::STATUS_UNMODERATED]);
        } else {
            $selector->status(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED);
        }

        $result = (int)$selector->count();
        $result += $this->connectedWarehouseProductCount;
        return $result;
    }

    /**
     * @return string
     */
    public function getStorekeeperStatusReplacement()
    {
        $replacements = self::storekeeperStatusReplacements();
        return isset($replacements[$this->storekeeper_status]) ? $replacements[$this->storekeeper_status] : '';
    }

    /**
     * @return string
     */
    public function getCallcenterStatusReplacement()
    {
        $replacements = self::callcenterStatusReplacements();
        return isset($replacements[$this->callcenter_status]) ? $replacements[$this->callcenter_status] : '';
    }

    /**
     * @return array
     */
    public static function callcenterStatusReplacements()
    {
        return [
            self::CALLCENTER_STATUS_UNMODERATED => Translator::t('not processed'),
            self::CALLCENTER_STATUS_ACCEPTED => Translator::t('confirmed'),
            self::CALLCENTER_STATUS_CANCELLED => Translator::t('cancelled'),
        ];
    }

    /**
     * @return array
     */
    public static function storekeeperStatusReplacements()
    {
        return [
            self::STOREKEEPER_STATUS_UNMODERATED => Translator::t('not processed'),
            self::STOREKEEPER_STATUS_ACCEPTED => Translator::t('confirmed'),
            self::STOREKEEPER_STATUS_UNAVAILABLE => Translator::t('not available'),
        ];
    }

    /**
     * @return array
     */
    public static function virtualStatusReplacements()
    {
        static $statuses = null;

        if ($statuses === null) {
            $statuses = [
                self::VIRTUAL_STATUS_UNKNOWN => Translator::t('unknown'),
                self::VIRTUAL_STATUS_WAITING_END_EVENT => Translator::t('the delivery is expected'),
                self::VIRTUAL_STATUS_WAITING_DELIVERY_SUPPLIER => Translator::t('supplier delays delivery'),
                self::VIRTUAL_STATUS_IN_WAREHOUSE => Translator::t('at warehouse'),
                self::VIRTUAL_STATUS_SENT => Translator::t('dispatched'),
                self::VIRTUAL_STATUS_CANCELED => Translator::t('cancelled'),
            ];
        }

        return $statuses;
    }

    /**
     * Может делать нагрузку на базу
     *
     * @return int
     */
    public function getVirtualStatus()
    {
        $status = self::VIRTUAL_STATUS_UNKNOWN;

        if ($this->isNewRecord) {
            return $status;
        }

        if ($this->callcenter_status == self::CALLCENTER_STATUS_CANCELLED) {
            $status = self::VIRTUAL_STATUS_CANCELED;
        } elseif ($this->order && in_array($this->order->processing_status, [OrderRecord::PROCESSING_STOREKEEPER_PACKAGED, OrderRecord::PROCESSING_STOREKEEPER_MAILED])) {
            $status = self::VIRTUAL_STATUS_SENT;
        } elseif ($this->getIsAllInWarehouse()) {
            $status = self::VIRTUAL_STATUS_IN_WAREHOUSE;
        } elseif ($this->event && $this->event->isTimeActive) {
            $status = self::VIRTUAL_STATUS_WAITING_END_EVENT;
        } elseif ($this->event && !$this->event->isTimeActive) {
            $status = self::VIRTUAL_STATUS_WAITING_DELIVERY_SUPPLIER;
        }

        return $status;
    }

    /**
     * @return string
     */
    public function getVirtualStatusText()
    {
        $statuses = self::virtualStatusReplacements();
        $status = $this->getVirtualStatus();

        if (isset($statuses[$status])) {
            return $statuses[$status];
        }

        return '';
    }

    /**
     * @param bool $replacements
     * @param string $postfix
     *
     * @return array|null
     */
    public function getCallcenterStatusHistory($replacements = false, $postfix = '<br>(callcenter)')
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';
        $history = Cast::toArr(json_decode($this->callcenter_status_history_json, true, 3));

        if ($replacements) {
            $historyReplacement = $this->callcenterStatusReplacements();
            array_walk($history, function (&$item, $key) use ($postfix, $historyReplacement, $category) {
                $status = $item['status'];
                /** @var AdminUserRecord $admin */
                $admin = AdminUserRecord::model()->cache(300)->findByPk($item['admin_id']);

                $item['status'] = isset($historyReplacement[$status]) ? $historyReplacement[$status] . $postfix : $status;
                $item['admin_id'] = $admin === null ? Translator::t('User') : $admin->login . "<br>(" . $admin->fullname . ')';
            });
            unset($item);
        }

        return $history;
    }

    /**
     * @param bool $replacements
     * @param string $postfix
     *
     * @return array|null
     */
    public function getStorekeeperStatusHistory($replacements = false, $postfix = null)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        if (!$postfix) {
            $postfix = '<br>(' . Translator::t('warehouse') . ')';
        }
        $history = Cast::toArr(json_decode($this->storekeeper_status_history_json, true, 3));

        if ($replacements) {
            $historyReplacement = $this->storekeeperStatusReplacements();
            array_walk($history, function (&$item, $key) use ($postfix, $historyReplacement, $category) {
                $status = $item['status'];
                /** @var AdminUserRecord $admin */
                $admin = AdminUserRecord::model()->cache(300)->findByPk($item['admin_id']);

                $item['status'] = isset($historyReplacement[$status]) ? $historyReplacement[$status] . $postfix : $status;
                $item['admin_id'] = $admin === null ? Translator::t('User') : $admin->login . "<br>(" . $admin->fullname . ')';
            });
            unset($item);
        }

        return $history;
    }
}
