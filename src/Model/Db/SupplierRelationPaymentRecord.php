<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Validator\UniqueMultiColumnValidator;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * @property-read integer $id
 * @property integer $supplier_id
 * @property integer $event_id
 * @property integer $orders
 * @property integer $orders_canceled
 * @property integer $orders_hold_delivery
 * @property integer $orders_mailed
 * @property integer $orders_delivered
 * @property integer $orders_not_delivered
 * @property integer $orders_returned @deprecated
 * @property float $orders_amount
 * @property float $orders_delivered_amount
 * @property float $orders_not_delivered_amount
 * @property float $orders_prepaid_amount
 * @property float $penalty_amount
 * @property float $commission
 * @property integer $status
 * @property integer $waiting_sending_days
 * @property float $to_pay
 * @property float $paid
 * @property boolean $is_force_refresh
 * @property integer $synchronized_at
 * @property integer $updated_at
 * @property integer $created_at
 * @property-read string $statusText
 * @property-read int $ordersConfirmed
 * @property-read EventRecord $event
 * @property-read SupplierRecord $supplier
 */
class SupplierRelationPaymentRecord extends DoctrineActiveRecord
{
    const STATUS_EVENT_QUEUE = 0;           //Акция в очереди - когда акция еще не началась
    const STATUS_EVENT_ACTIVE = 1;          //Акция активна - когда акция началась
    const STATUS_EVENT_COMPLETED = 2;       //Акция завершена - когда акция закончилась, но еще не отправлен ни один заказ
    const STATUS_DISTRIBUTION_DELAY = 3;    //Задержка в рассылке - когда есть хоть один штраф с таким типом и при этом есть еще хотя бы один не отправленный заказ
    const STATUS_SHIPMENT = 4;              //Рассылка - когда есть хотя бы один отправленный заказ
    const STATUS_DELIVERY = 5;              //Доставка - когда все заказы отправлены но есть еще не полученные заказы (возвращающиеся не учитываются)
    const STATUS_RETURNS_PROCESSING = 6;    //Обработка возвратов
    const STATUS_READY_TO_PAY = 7;          //Готово к выплате - все заказы получены, а возвращенные компенсированы пока не используется
    const STATUS_PAID = 8;                  //Выплачено

    const COMMISSION_PERCENT = 20;

    /**
     * @param string $className
     *
     * @return SupplierRelationPaymentRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['orders', 'getOrders', 'setOrders', 'orders'];
        yield ['orders_canceled', 'getOrdersCanceled', 'setOrdersCanceled', 'ordersCanceled'];
        yield ['orders_mailed', 'getOrdersMailed', 'setOrdersMailed', 'ordersMailed'];
        yield ['orders_hold_delivery', 'getOrdersHoldDelivery', 'setOrdersHoldDelivery', 'ordersHoldDelivery'];
        yield ['orders_delivered', 'getOrdersDelivered', 'setOrdersDelivered', 'ordersDelivered'];
        yield ['orders_not_delivered', 'getOrdersNotDelivered', 'setOrdersNotDelivered', 'ordersNotDelivered'];
        yield ['orders_returned', 'getOrdersReturned', 'setOrdersReturned', 'ordersReturned'];
        yield ['orders_amount', 'getOrdersAmount', 'setOrdersAmount', 'ordersAmount'];
        yield ['orders_delivered_amount', 'getOrdersDeliveredAmount', 'setOrdersDeliveredAmount', 'ordersDeliveredAmount'];
        yield ['orders_not_delivered_amount', 'getOrdersNotDeliveredAmount', 'setOrdersNotDeliveredAmount', 'ordersNotDeliveredAmount'];
        yield ['orders_prepaid_amount', 'getOrdersPrepaidAmount', 'setOrdersPrepaidAmount', 'ordersPrepaidAmount'];
        yield ['penalty_amount', 'getPenaltyAmount', 'setPenaltyAmount', 'penaltyAmount'];
        yield ['commission', 'getCommission', 'setCommission', 'commission'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['waiting_sending_days', 'getWaitingSendingDays', 'setWaitingSendingDays', 'waitingSendingDays'];
        yield ['to_pay', 'getToPay', 'setToPay', 'toPay'];
        yield ['paid', 'getPaid', 'setPaid', 'paid'];
        yield ['is_force_refresh', 'isForceRefresh', 'setIsForceRefresh', 'isForceRefresh'];
        yield ['synchronized_at', 'getSynchronizedAt', 'setSynchronizedAt', 'synchronizedAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
    }

    public function tableName()
    {
        return 'suppliers_relations_payments';
    }

    public function relations()
    {
        return [
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['event_id, supplier_id', 'required'],
            ['event_id+supplier_id', UniqueMultiColumnValidator::class],

            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],

            ['orders, orders_canceled, orders_mailed, orders_hold_delivery, orders_delivered, orders_not_delivered, orders_returned, waiting_sending_days',
                'numerical', 'integerOnly' => true, 'min' => 0, 'max' => PHP_INT_MAX],

            ['orders_amount, orders_delivered_amount, orders_not_delivered_amount, orders_prepaid_amount, penalty_amount, commission',
                'numerical', 'min' => 0, 'max' => PHP_INT_MAX],

            ['to_pay, paid', 'numerical', 'min' => PHP_INT_MIN, 'max' => PHP_INT_MAX],

            ['synchronized_at', 'numerical', 'integerOnly' => true, 'min' => 0],
            ['is_force_refresh', 'boolean'],

            ['status', 'default', 'value' => self::STATUS_EVENT_QUEUE],
            ['status', 'in', 'range' => self::statuses(false)],

            ['id, event_id, status, supplier_id, orders, orders_amount, orders_delivered_amount'
                . ', orders_not_delivered_amount, orders_prepaid_amount, orders_returned, waiting_sending_days'
                . ', to_pay, paid, synchronized_at, is_force_refresh', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    protected function beforeSave()
    {
        if (parent::beforeSave() == false) {
            return false;
        }

        $this->_updatePaid();
        $this->_updatePenalty();
        $this->_updateToPay();
        return true;
    }

    protected function _updateToPay()
    {
        $this->to_pay = $this->commission + $this->penalty_amount - $this->orders_prepaid_amount;
    }

    protected function _updatePaid()
    {
        $paid = 0.0;

        if ($this->event === null) {
            return;
        }

        foreach ($this->event->supplierPaymentTransactions as $transaction) {
            $paid += $transaction->amount;
        }

        $this->paid = $paid;
    }

    protected function _updatePenalty()
    {
        $amount = 0.0;

        if ($this->event === null) {
            return;
        }

        foreach ($this->event->supplierPaymentPenaltyActive as $penalty) {
            $amount += $penalty->amount;
        }

        $this->penalty_amount = $amount;
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'supplier_id' => Translator::t('Supplier'),
            'event_id' => Translator::t('Flash-sale'),
            'orders' => Translator::t('Orders'),
            'orders_canceled' => Translator::t('Canceled orders'),
            'orders_hold_delivery' => Translator::t('Delayed dispatch'),
            'orders_mailed' => Translator::t('Dispatched orders'),
            'orders_delivered' => Translator::t('Orders delivered '),
            'orders_not_delivered' => Translator::t('Orders not delivered'),
            'orders_returned' => Translator::t('Order returns'), //устаревший
            'orders_amount' => Translator::t('Total'),
            'orders_delivered_amount' => Translator::t('Total amount of orders delivered'),
            'orders_not_delivered_amount' => Translator::t('Total amount of orders returned'),
            'orders_prepaid_amount' => Translator::t('Prepaid'),
            'penalty_amount' => Translator::t('Penatlies'),
            'commission' => Translator::t('Commission fee'),
            'status' => Translator::t('Status'),
            'waiting_sending_days' => Translator::t('Dispatch delay (days)'),
            'to_pay' => Translator::t('Amount to be paid'),
            'paid' => Translator::t('Paid out'),
            'is_force_refresh' => Translator::t('Data must be updated'),
            'synchronized_at' => Translator::t('Synchronized'),
            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.event_id', $model->event_id);
        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.orders', $model->orders);
        $criteria->compare($alias . '.orders_canceled', $model->orders_canceled);
        $criteria->compare($alias . '.orders_hold_delivery', $model->orders_hold_delivery);
        $criteria->compare($alias . '.orders_mailed', $model->orders_mailed);
        $criteria->compare($alias . '.orders_delivered', $model->orders_delivered);
        $criteria->compare($alias . '.orders_not_delivered', $model->orders_not_delivered);
        $criteria->compare($alias . '.orders_returned', $model->orders_returned);
        $criteria->compare($alias . '.orders_amount', $model->orders_amount);
        $criteria->compare($alias . '.orders_delivered_amount', $model->orders_delivered_amount);
        $criteria->compare($alias . '.orders_not_delivered_amount', $model->orders_not_delivered_amount);
        $criteria->compare($alias . '.orders_prepaid_amount', $model->orders_prepaid_amount);
        $criteria->compare($alias . '.penalty_amount', $model->penalty_amount);
        $criteria->compare($alias . '.commission', $model->commission);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.waiting_sending_days', $model->waiting_sending_days);
        $criteria->compare($alias . '.to_pay', $model->to_pay);
        $criteria->compare($alias . '.paid', $model->paid);
        $criteria->compare($alias . '.is_force_refresh', $model->is_force_refresh);
        $criteria->compare($alias . '.synchronized_at', $model->synchronized_at);

        $criteria->compare($alias . '.updated_at', $model->updated_at);
        $criteria->compare($alias . '.created_at', $model->created_at);

        return $provider;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function statuses($replacements = true)
    {
        $statuses = [
            self::STATUS_EVENT_QUEUE => Translator::t('Flash-sale scedueled'),
            self::STATUS_EVENT_ACTIVE => Translator::t('Flash-sale active'),
            self::STATUS_EVENT_COMPLETED => Translator::t('Flash-sale ended'),
            self::STATUS_DISTRIBUTION_DELAY => Translator::t('Delay in supply'),
            self::STATUS_SHIPMENT => Translator::t('Dispatch'),
            self::STATUS_DELIVERY => Translator::t('Delivery'),
            self::STATUS_RETURNS_PROCESSING => Translator::t('Processing Returns'), //пока не используется
            self::STATUS_READY_TO_PAY => Translator::t('Ready for withdrawal'),
            self::STATUS_PAID => Translator::t('Paid out'),
        ];

        return $replacements ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStatusText($default = '')
    {
        $statuses = self::statuses();

        return isset($statuses[$this->status]) ? $statuses[$this->status] : $default;
    }

    /**
     * Форсированное обновление оплаты
     */
    public function needUpdateForce()
    {
        $statusesUpdate = [
            self::STATUS_DISTRIBUTION_DELAY,
            self::STATUS_SHIPMENT,
            self::STATUS_DELIVERY,
            self::STATUS_RETURNS_PROCESSING,
            self::STATUS_READY_TO_PAY,
        ];

        if (!in_array($this->status, $statusesUpdate)) {
            return;
        }

        $this->is_force_refresh = 1;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.event_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdNotIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function supplierIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function statusIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.status', $value);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function statusNotIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addNotInCondition($alias . '.status', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function forceRefresh($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_force_refresh' => Cast::toUInt($value),
        ]);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function synchronizedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.synchronized_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function synchronizedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.synchronized_at', ">= $value");

        return $this;
    }


    /* API */

    /**
     * @return int
     */
    public function getOrdersConfirmed()
    {
        return $this->orders - $this->orders_canceled;
    }

    /**
     * @return float|int
     */
    public function getCommissionAmount()
    {
        return static::getCommission($this->orders_delivered_amount);
    }

    /**
     * @param float|int $amount
     *
     * @return float|int
     */
    public static function getCommission($amount)
    {
        $commissionAmount = Cast::toFloat($amount) * self::COMMISSION_PERCENT / 100;

        return $commissionAmount > 0 ? $commissionAmount : 0;
    }

    /**
     * @return bool
     */
    public function isPossibleAddPayment()
    {
        return $this->status == SupplierRelationPaymentRecord::STATUS_READY_TO_PAY;
    }

    /**
     * @return bool
     */
    public function isPossibleAddPenalty()
    {
        return $this->status != SupplierRelationPaymentRecord::STATUS_PAID;
    }
}
