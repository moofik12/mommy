<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserPartnerAdmissionRecord
 *
 * @property-description Партнерские зачисления по заказам
 * @property-read integer $id
 * @property integer $partner_id
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $sum_order
 * @property integer $count_product_order
 * @property integer $amount
 * @property integer $status
 * @property integer $paid_after_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read string $statusReplacement
 * @property-read UserPartnerRecord $partner
 * @property-read OrderRecord $order
 * @property-read UserRecord $user
 */
class UserPartnerAdmissionRecord extends DoctrineActiveRecord
{
    CONST PAID_DAY = 1209600; // после 14 дней можно перечислять на подтвержденный баланс партнера зачисление, если заказ оплачен и забран

    const STATUS_NEW = 0; //новые, не отправленные или не оплаченные заказы
    const STATUS_PAID_AND_WAIT = 1; //есть оплата и забран на НП, ждем 14 дней чтобы сделать вывод на баланс
    const STATUS_BALANCE = 2; //подтвержден и выведен на баланс
    const STATUS_CANCELED = 3; //отменен
    const STATUS_RETURN = 4; //был возврат по заказу

    /**
     * @return string
     */
    public function tableName()
    {
        return 'users_partner_admissions';
    }

    /**
     * @param string $className
     *
     * @return UserPartnerAdmissionRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['count_product_order', 'getCountProductOrder', 'setCountProductOrder', 'countProductOrder'];
        yield ['sum_order', 'getSumOrder', 'setSumOrder', 'sumOrder'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['paid_after_at', 'getPaidAfterAt', 'setPaidAfterAt', 'paidAfterAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['partner_id', 'getPartnerId', 'setPartnerId', 'partnerId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner_id, order_id, user_id, sum_order, count_product_order, amount', 'required'],

            ['sum_order, count_product_order, amount', 'numerical'],

            ['status', 'in', 'range' => array_keys(self::statusReplacements())],

            ['id, partner_id, order_id, user_id, sum_order, count_product_order, amount, status, paid_after_at, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'partner' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserPartnerRecord', 'partner_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', '№'),
            'partner_id' => \Yii::t('common', 'Supplier ID'),
            'order_id' => \Yii::t('common', 'Order ID'),
            'user_id' => \Yii::t('common', 'User who made an order'),
            'sum_order' => \Yii::t('common', 'Order price'),
            'count_product_order' => \Yii::t('common', 'Quantity of goods in the order'),
            'amount' => \Yii::t('common', 'Amount to be credited'),
            'status' => \Yii::t('common', 'Status'),
            'paid_after_at' => \Yii::t('common', 'Pay later'),//время, после которого готовое к выплате зачисление можно перевести на баланс
            'created_at' => \Yii::t('common', 'Created'),
            'updated_at' => \Yii::t('common', 'Updated on'),

            'statusReplacement' => \Yii::t('common', 'Status'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.partner_id', $model->partner_id);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.amount', $model->amount);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.sum_order', $model->sum_order);
        $criteria->compare($alias . '.count_product_order', $model->count_product_order);

        return $provider;
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }
        $amount = 0;
        if ($this->partner !== null && $this->sum_order > 0) {
            $partnerPercent = $this->partner->partner_percent;
            $amount = round($this->sum_order / 100 * $partnerPercent);
        }
        if ($this->status == self::STATUS_PAID_AND_WAIT) {
            $this->paid_after_at = time() + self::PAID_DAY;
        }
        $this->amount = $amount;
        return $result;
    }

    public function getIsOutputBalance()
    {
        return $this->status == self::STATUS_BALANCE;
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        return [
            self::STATUS_NEW => \Yii::t('common', 'new!'),
            self::STATUS_PAID_AND_WAIT => \Yii::t('common', 'is ready for payment to the balance'),
            self::STATUS_BALANCE => \Yii::t('common', 'put on balance'),
            self::STATUS_CANCELED => \Yii::t('common', 'cancelled!'),
            self::STATUS_RETURN => \Yii::t('common', 'return'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function partnerId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.partner_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function status($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param $values
     * @param int $max
     *
     * @return $this
     */
    public function statusIn($values, $max = -1)
    {
        if ($max > 0) {
            $values = array_slice($values, 0, $max);
        }
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function orderId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function paidAfterAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.paid_after_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function paidAfterAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.paid_after_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }
}
