<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbException;
use CException;
use CHtmlPurifier;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class WarehouseStatementReturnRecord
 *
 * @property-read int $id
 * @property int $supplier_id
 * @property int $user_id
 * @property int $status
 * @property bool $is_accounted_returns deprecated возврат учтен при ручном выставлении оплаты
 * @property bool $notify_text
 * @property bool $is_need_notify
 * @property bool $is_notified
 * @property int $supplier_payment_id
 * @property-read int $status_changed_at
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property-read string $statusReplacement
 * @property-read SupplierRecord $supplier
 * @property-read AdminUserRecord $user
 * @property-read WarehouseStatementReturnProductRecord[] $positions
 * @property-read int $positionCount
 * @property-read SupplierPaymentRecord $supplierPayment
 * @mixin Timestampable
 */
class WarehouseStatementReturnRecord extends DoctrineActiveRecord
{
    const STATUS_UNCONFIRMED = 0;
    const STATUS_READY = 1;
    const STATUS_CONFIRMED = 2;

    const ENABLE_TIME_CHANGE_STATUS = 10800; // 3 hours
    const ENABLE_TIME_CHANGE_ACCOUNTED_RETURNS = 864000; // 10 days

    private $_prevStatus;

    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['is_accounted_returns', 'isAccountedReturns', 'setIsAccountedReturns', 'isAccountedReturns'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['is_need_notify', 'isNeedNotify', 'setIsNeedNotify', 'isNeedNotify'];
        yield ['is_notified', 'isNotified', 'setIsNotified', 'isNotified'];
        yield ['notify_text', 'getNotifyText', 'setNotifyText', 'notifyText'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['supplier_payment_id', 'getSupplierPaymentId', 'setSupplierPaymentId', 'supplierPaymentId'];
    }

    public function tableName()
    {
        return 'warehouse_statements_return';
    }

    public function relations()
    {
        return [
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],

            'positions' => [self::HAS_MANY, 'MommyCom\Model\Db\WarehouseStatementReturnProductRecord', 'statement_id'],
            'positionCount' => [self::STAT, 'MommyCom\Model\Db\WarehouseStatementReturnProductRecord', 'statement_id'],

            'supplierPayment' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierPaymentRecord', 'supplier_payment_id'],
        ];
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_UNCONFIRMED],
            ['status', 'in', 'range' => self::statuses(false)],
            ['status', 'validateStatus'],

            ['is_accounted_returns', 'boolean'], //deprecated
            ['is_need_notify', 'boolean'],
            ['is_notified', 'boolean'],

            ['notify_text', 'length', 'max' => 255],
            ['notify_text', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],

            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id', 'allowEmpty' => false],
            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id', 'allowEmpty' => false],

            // хак чтобы CExistValidator считал значение "0" пустым
            ['supplier_payment_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],

            ['supplier_payment_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierPaymentRecord', 'attributeName' => 'id'],

            [
                'id, suppler_id, user_id, status, is_accounted_returns,is_need_notify, notify_text, is_notified, created_at, updated_at',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'status' => \Yii::t($category, 'Status'),
            'supplier_id' => \Yii::t($category, 'Supplier'),
            'user_id' => \Yii::t($category, 'User'),
            'is_accounted_returns' => \Yii::t($category, 'Accounted for next invoice'),
            'is_need_notify' => \Yii::t($category, 'Notify supplier'),
            'is_notified' => \Yii::t($category, 'Supplier was notified'),
            'notify_text' => \Yii::t($category, 'Notification text'),
            'supplier_payment_id' => \Yii::t($category, 'Payment to the supplier'),

            'status_changed_at' => \Yii::t($category, 'Status is set'),
            'created_at' => \Yii::t($category, 'Added'),
            'updated_at' => \Yii::t($category, 'Updated'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function validateStatus($attribute, $params)
    {
        $status = $this->{$attribute};

        if ($status == self::STATUS_CONFIRMED && $this->positionCount == 0) {
            $this->addError($attribute, \Yii::t('backend', 'No products in the act'));
        }
    }

    /**
     * @return bool
     */
    protected function beforeDelete()
    {
        $delete = true;
        if ($this->status == self::STATUS_CONFIRMED) {
            $delete = false;
        }

        foreach ($this->positions as $position) {
            $delete = $delete && $position->delete();
        }

        return $delete && parent::beforeDelete();
    }

    protected function afterSave()
    {
        parent::afterSave();

        $this->_updateWarehouseProductStatus();
        $this->_notify();
    }

    /**
     * This method is invoked before saving a record (after validation, if any).
     * The default implementation raises the {@link onBeforeSave} event.
     * You may override this method to do any preparation work for record saving.
     * Use {@link isNewRecord} to determine whether the saving is
     * for inserting or updating record.
     * Make sure you call the parent implementation so that the event is raised properly.
     *
     * @return boolean whether the saving should be executed. Defaults to true.
     */
    protected function beforeSave()
    {
        if ($this->_prevStatus != $this->status) {
            $this->status_changed_at = time();
        }

        return parent::beforeSave();
    }

    /**
     * This method is invoked after each record is instantiated by a find method.
     * The default implementation raises the {@link onAfterFind} event.
     * You may override this method to do postprocessing after each newly found record is instantiated.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterFind()
    {
        $this->_prevStatus = $this->status;
        parent::afterFind();
    }

    /**
     * @throws CException
     */
    private function _updateWarehouseProductStatus()
    {
        //force change status
        if ($this->status == self::STATUS_READY || $this->status == self::STATUS_CONFIRMED) {
            foreach ($this->positions as $position) {
                if (!$position->save()) {
                    Yii::log(\Yii::t('backend', 'An error occurred while fixing the warehouse status when issuing the act to the supplier. The record number in the {position} act', ['{position}' => $position->id]) . "\n" . print_r($position->getErrors(), true));
                }
            }
        }
    }

    /**
     * Уведомление поставщиков
     *
     * @throws CDbException
     */
    private function _notify()
    {
        if ($this->isNewRecord) {
            return;
        }

        if (!$this->is_notified && $this->is_need_notify && $this->status == self::STATUS_CONFIRMED) {
            $notified = false;

            if ($this->supplier) {
                $createEmail = false;

                if ($this->supplier->email) {
                    $allowed = \Yii::app()->params['newsletters'] ?? false;

                    if ($allowed) {
                        $createEmail = \Yii::app()->mailer->create(
                            [\Yii::app()->params['layoutsEmails']['warehouseStatementReturn'] => \Yii::app()->params['layoutsEmails']['default']],
                            [$this->supplier->email],
                            \Yii::t('common', 'Return of goods'),
                            [
                                'view' => 'warehouseStatementReturn',
                                'data' => ['statement' => $this, 'message' => $this->notify_text],
                            ],
                            EmailMessage::TYPE_SYSTEM
                        );
                    }
                }

                $notified = $notified || $createEmail;
            }

            if ($notified) {
                $this->is_notified = 1;
                $this->update('is_notified');
            }
        }
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model WarehouseStatementReturnRecord */

        $criteria->compare($alias . '.id', '=' . Utf8::ltrim($model->id, '0'));
        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.is_accounted_returns', $model->is_accounted_returns); //deprecated
        $criteria->compare($alias . '.is_need_notify', $model->is_need_notify);
        $criteria->compare($alias . '.is_notified', $model->is_notified);
        $criteria->compare($alias . '.notify_text', $model->notify_text, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function statusIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.status', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function supplierIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplier($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierPayment($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_payment_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function supplierPaymentIn(array $values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.supplier_payment_id', $values);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function supplierPaymentNotIn(array $values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addNotInCondition($alias . '.supplier_payment_id', $values);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function userIn($value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.user_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function user($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @deprecated
     *
     * @param bool $value
     *
     * @return static
     */
    public function isAccountedReturns($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_accounted_returns' => Cast::toUInt($value),
        ]);

        return $this;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function statuses($replacements = true)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        $statuses = [
            self::STATUS_UNCONFIRMED => \Yii::t($category, 'New'),
            self::STATUS_READY => \Yii::t($category, 'Created'),
            self::STATUS_CONFIRMED => \Yii::t($category, 'confirmed'),
        ];

        return $replacements ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStatusReplacement($default = '')
    {
        $statuses = self::statuses();

        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $default;
    }

    /**
     * @return WarehouseProductRecord[]
     */
    public function getProducts()
    {
        $products = array_map(function ($position) {
            /* @var WarehouseStatementReturnProductRecord $position */
            return $position->product;
        }, $this->positions);

        return $products;
    }

    public function getProductsPricePurchase()
    {
        $products = $this->getProducts();

        $price = array_reduce($products, function ($carry, $item) {
            /* @var WarehouseProductRecord $item */
            return $carry + Cast::toUFloat($item->eventProduct->price_purchase, 0.0);
        }, 0.0);

        return $price;
    }

    /**
     * @param int|null $time unix time stamp
     *
     * @return bool
     */
    public function isAvailableChangeAccountedReturns($time = null)
    {
        if ($this->status != self::STATUS_CONFIRMED) {
            return false;
        }

        if ($time === null) {
            $time = time();
        } else {
            $time = intval($time);
        }

        return $this->status_changed_at + self::ENABLE_TIME_CHANGE_ACCOUNTED_RETURNS > $time;
    }

    /* API */
    /**
     * @return float
     */
    public function positionsPurchaseAmount()
    {
        return array_reduce($this->positions, function ($carry, $item) {
            /* @var  WarehouseStatementReturnProductRecord $item */
            return $carry = $carry + Cast::toFloat($item->eventProduct->price_purchase);
        }, 0.0);
    }

    /**
     * возврат учтен при оплате поставщику
     *
     * @return bool
     */
    public function isCountedInPayment()
    {
        return $this->supplier_payment_id > 0 || $this->is_accounted_returns;
    }
}
