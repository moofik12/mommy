<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use CHtmlPurifier;
use CLogger;
use CUploadedFile;
use CValidator;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class EventRecord
 *
 * @property-read integer $id
 * @property boolean $is_deleted
 * @property boolean $is_stock Акция стоковая
 * @property boolean $is_virtual Акция которая создается автоматически на основе остатков со склада
 * @property boolean $is_drop_shipping акция дропшипинговая
 * @property boolean $is_charity Благотворительная акция
 * @property boolean $can_prepay Возможна предоплата через эквайринг
 * @property integer $supplier_id
 * @property string $logo_fileid
 * @property string $promo_fileid
 * @property string $distribution_fileid
 * @property string $size_chart_fileid
 * @property integer $status
 * @property string $name
 * @property string $description supports simple html
 * @property string $description_short
 * @property string $description_mailing текст для рассылок
 * @property int $brand_manager_id
 * @property integer $start_at дата\время акции начала
 * @property integer $end_at дата\время акции окончания
 * @property integer $mailing_start_at дата начала рассылки посылок
 * @property boolean $is_visible видима ли акция в списках
 * @property boolean $supplier_confirmation
 * @property boolean $manager_confirmation
 * @property boolean $supplier_notified
 * @property integer $priority приоритет при сортировке на главной (меньше - лучше)
 * @property integer $created_at
 * @property integer $updated_at
 * @property string startAtString дата\время начала строкой
 * @property string startAtDateString дата начала строкой
 * @property string startAtTimeString время начала строкой
 * @property string endAtString дата\время окончания строкой
 * @property string endAtDateString дата окончания строкой
 * @property string endAtTimeString время окончания строкой
 * @property string mailingStartAtDateString дата начала рассылки посылок строкой
 * @property-read SupplierRecord $supplier основной поставщик акции
 * @property-read SupplierRecord[] $productSuppliers список ВСЕХ поставщиков акции
 * @property-read EventProductRecord[] $products
 * @property-read integer $productCount
 * @property-read OrderProductRecord[] $orderProducts
 * @property-read OrderProductRecord[] $orderProductsConfirmed
 * @property-read SupplierRelationPaymentTransactionRecord[] $supplierPaymentTransactions
 * @property-read SupplierRelationPaymentPenaltyRecord[] $supplierPaymentPenalty
 * @property-read SupplierRelationPaymentPenaltyRecord[] $supplierPaymentPenaltyActive
 * @property-read boolean $isStatusEmpty
 * @property-read boolean $isStatusPublished
 * @property-read boolean $isStatusHidden
 * @property-read boolean $isTimeActive активное ли в данный момент событие
 * @property-read boolean $isTimeFuture
 * @property-read boolean $isTimeFinished
 * @property-read boolean $isSupplierAnswerOrder заказ от поставщика был загружен, абстрактное понятие
 * @property-read string $statusReplacement
 * @property-read BrandRecord[] availableBrands возвращает бренды из текущей акции
 * @property-read string[] availableSizes возвращает возможные размеры из текущей акции
 * @property-read string[] availableColors цвета из текущей акции
 * @property-read array availableAges года, массив значений из @see YearGroups
 * @property-read string[] availableTargets целевая аудитория для текущей акции. @see ProductTargets
 * @property FileStorageThumbnail $logo
 * @property FileStorageThumbnail $promo
 * @property FileStorageThumbnail $distribution
 * @property FileStorageThumbnail $sizeChart
 * @property-read integer $productsSoldNumber количество проданных товаров из акции
 * @property-read integer $productsSoldRealNumber количество проданных товаров из акции (без вычита стока)
 * @property-read GroupedProduct[] $groupedProducts
 * @property-read AdminUserRecord $brandManager
 * @property-read boolean $isDescriptionLong считается ли описание длинным
 * @property-read float $minSellingPrice
 * @property-read float $promoDiscount максимальная скидка на товар в акции (от 0.00 до 1.00)
 * @property-read integer $promoDiscountPercent максимальная скидка на товар в акции (от 1 до 100)
 * @property-read integer $maxProductDiscount максимальная скидка на товар в акции (от 0.00 до 1.00)
 */
class EventRecord extends DoctrineActiveRecord
{
    const STATUS_EMPTY = 0;
    const STATUS_PUBLISHED = 1;

    const DESCRIPTION_LONG_LEN = 300; // длинна описания после которого она считается длинной

    const VIRTUAL_EVENT_DURATION = 900; // продолжительность виртуальной акции
    const VIRTUAL_EVENT_SIZE = 6;

    const SUPPLIER_ANSWER_ORDER_PERIOD = 172800; //2 дня, возможно выходные, время за которое поставщик должен вернуть ответ на счет поставки

    const TIME_AVAILABLE_ORDERS_EDIT_AFTER_END = 25200; //7 часов

    /**
     * @param string $className
     *
     * @return EventRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_deleted', 'isDeleted', 'setIsDeleted', 'isDeleted'];
        yield ['is_stock', 'isStock', 'setIsStock', 'isStock'];
        yield ['is_virtual', 'isVirtual', 'setIsVirtual', 'isVirtual'];
        yield ['is_drop_shipping', 'isDropShipping', 'setIsDropShipping', 'isDropShipping'];
        yield ['is_charity', 'isCharity', 'setIsCharity', 'isCharity'];
        yield ['can_prepay', 'canPrepay', 'setCanPrepay', 'canPrepay'];
        yield ['logo_fileid', 'getLogoFileid', 'setLogoFileid', 'logoFileid'];
        yield ['promo_fileid', 'getPromoFileid', 'setPromoFileid', 'promoFileid'];
        yield ['distribution_fileid', 'getDistributionFileid', 'setDistributionFileid', 'distributionFileid'];
        yield ['size_chart_fileid', 'getSizeChartFileid', 'setSizeChartFileid', 'sizeChartFileid'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['description_short', 'getDescriptionShort', 'setDescriptionShort', 'descriptionShort'];
        yield ['description_mailing', 'getDescriptionMailing', 'setDescriptionMailing', 'descriptionMailing'];
        yield ['start_at', 'getStartAt', 'setStartAt', 'startAt'];
        yield ['end_at', 'getEndAt', 'setEndAt', 'endAt'];
        yield ['mailing_start_at', 'getMailingStartAt', 'setMailingStartAt', 'mailingStartAt'];
        yield ['is_visible', 'isVisible', 'setIsVisible', 'isVisible'];
        yield ['supplier_confirmation', 'isSupplierConfirmation', 'setSupplierConfirmation', 'supplierConfirmation'];
        yield ['manager_confirmation', 'isManagerConfirmation', 'setManagerConfirmation', 'managerConfirmation'];
        yield ['supplier_notified', 'isSupplierNotified', 'setSupplierNotified', 'supplierNotified'];
        yield ['priority', 'getPriority', 'setPriority', 'priority'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['brand_manager_id', 'getBrandManagerId', 'setBrandManagerId', 'brandManagerId'];
    }

    public function tableName()
    {
        return 'events';
    }

    public function relations()
    {
        return [
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
            'products' => [self::HAS_MANY, 'MommyCom\Model\Db\EventProductRecord', 'event_id'],
            'productCount' => [self::STAT, 'MommyCom\Model\Db\EventProductRecord', 'event_id'],
            'maxProductDiscount' => [self::STAT, 'MommyCom\Model\Db\EventProductRecord', 'event_id', 'select' => 'MAX(1 - ROUND(price/price_market, 2))', 'condition' => 'price_market > 0'],
            'brandManager' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'brand_manager_id'],

            'orderProducts' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'event_id'],
            'orderProductsConfirmed' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'event_id', 'scopes' => [
                'callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
            ]],

            'supplierPaymentTransactions' => [self::HAS_MANY, 'MommyCom\Model\Db\SupplierRelationPaymentTransactionRecord', 'event_id'],
            'supplierPaymentPenalty' => [self::HAS_MANY, 'MommyCom\Model\Db\SupplierRelationPaymentPenaltyRecord', 'event_id'],
            'supplierPaymentPenaltyActive' => [self::HAS_MANY, 'MommyCom\Model\Db\SupplierRelationPaymentPenaltyRecord', 'event_id', 'scopes' => [
                'stateIs' => SupplierRelationPaymentPenaltyRecord::STATE_ACTIVE,
            ]],
        ];
    }

    public function rules()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            ['name, description, description_short, description_mailing', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name, description, description_short, description_mailing, logo_fileid', 'validatorNonVirtualRequires'],

            ['is_stock, is_virtual, is_deleted, is_charity, is_visible, is_drop_shipping, can_prepay', 'boolean'],

            ['name', 'length', 'max' => 100],
            // array('name', 'unique'),
            ['description', 'length', 'max' => 65000],
            ['description_short', 'length', 'max' => 250],
            ['description, description_mailing', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],

            ['logo, promo, distribution', 'required', 'on' => 'insert'], // требуется только при создании

            ['startAtString, startAtTimeString, startAtDateString, endAtString, endAtTimeString, endAtDateString, mailingStartAtDateString', 'required'],

            ['start_at, end_at, mailing_start_at, brand_manager_id, name', 'required'],
            ['start_at, end_at, mailing_start_at', 'numerical', 'min' => 0, 'max' => PHP_INT_MAX, 'allowEmpty' => false],
            ['start_at', 'compare', 'compareAttribute' => 'end_at', 'operator' => '<', 'message' => \Yii::t($category, 'The promotion must begin before it ends')],
            ['mailing_start_at', 'compare', 'compareAttribute' => 'start_at', 'operator' => '>=', 'message' => \Yii::t($category, 'Distribution of goods must begin after the stock begins')],

            ['priority', 'numerical', 'min' => 0, 'max' => PHP_INT_MAX, 'allowEmpty' => false],

            //array('end_at', 'compare', 'compareValue' => time(), 'operator' => '>', 'message' => 'Нельзя создавать акции задним числом'),

            ['supplier_id', 'filter', 'filter' => function ($value) {
                return $value == 0 && !$this->is_drop_shipping ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id', 'message' => \Yii::t($category, 'The specified provider does not exist'), 'allowEmpty' => true],
            ['brand_manager_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id', 'allowEmpty' => true],

            ['id, is_stock, supplier_id, article, status, name, is_drop_shipping, start_at, end_at, mailing_start_at, is_visible, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function validatorNonVirtualRequires($attribute)
    {
        if (!$this->is_virtual) {
            $validator = CValidator::createValidator('required', $this, $attribute);
            $validator->validate($this, $attribute);
        }
    }

    protected function _resolveStatus()
    {
        if ($this->isNewRecord) {
            $this->status = self::STATUS_EMPTY;
        }

        $productCount = $this->getRelated('productCount', true);
        $this->status = count($productCount) > 0 ? self::STATUS_PUBLISHED : self::STATUS_EMPTY;
    }

    public function beforeDelete()
    {
        if (!$result = parent::beforeDelete()) {
            return $result;
        }

        if ($this->productsSoldRealNumber > 0 || $this->getWarehouseProductNumber() > 0) {
            $this->is_deleted = true;
            $this->save();
            return false;
        }

        foreach ($this->products as $product) {
            $product->delete();
        }

        return true;
    }

    protected function afterDelete()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';
        parent::afterDelete();

        if ($this->is_drop_shipping) {
            $deleteSupplierRelationPayment = SupplierRelationPaymentRecord::model()->eventId($this->id)->supplierId($this->supplier_id)->find();
            if ($deleteSupplierRelationPayment) {
                if (!$deleteSupplierRelationPayment->delete()) {
                    Yii::log(\Yii::t($category, 'Error removing the record for payment to the supplier, when deleting the promotion') . print_r($deleteSupplierRelationPayment->errors, true), CLogger::LEVEL_ERROR);
                }
            }
        }
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolveStatus();

        return true;
    }

    protected function afterSave()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';
        parent::afterSave();

        if ($this->isNewRecord && $this->is_drop_shipping) {
            $this->refresh();
            $existSupplierRelationPayment = SupplierRelationPaymentRecord::model()->eventId($this->id)->supplierId($this->supplier_id)->exists();
            if (!$existSupplierRelationPayment) {
                $newModel = new SupplierRelationPaymentRecord();
                $newModel->event_id = $this->id;
                $newModel->supplier_id = $this->supplier_id;

                if (!$newModel->save()) {
                    Yii::log(\Yii::t($category, 'Error creating records for payment to vendor') . print_r($newModel->errors, true), CLogger::LEVEL_ERROR);
                }
            }
        }
    }

    /**
     * @param $events
     *
     * @return array|mixed|null|static
     */
    public static function getEvents($events)
    {
        $events = self::model()->idIn($events)->findAll();
        return $events;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'is_deleted' => \Yii::t($category, 'Removed'),
            'is_stock' => \Yii::t($category, 'Stock promotion'),
            'is_virtual' => \Yii::t($category, 'Quick share'),
            'is_drop_shipping' => \Yii::t($category, 'Dropshipping flash-sale'),
            'is_visible' => \Yii::t($category, 'Visible on the mainpage'),
            'is_charity' => \Yii::t($category, 'Charity Event'),
            'can_prepay' => \Yii::t($category, 'Prepay is possible'),

            'name' => \Yii::t($category, 'Name'),
            'supplier_id' => \Yii::t($category, 'Supplier'),
            'brand_manager_id' => \Yii::t($category, 'Brand Manager'),
            'logo_fileid' => \Yii::t($category, 'Label'),
            'promo_fileid' => \Yii::t($category, 'Promo'),
            'distribution_fileid' => \Yii::t($category, 'Newsletter'),
            'size_chart_fileid' => \Yii::t($category, 'Dimension mesh'),
            'description' => \Yii::t($category, 'Description'),
            'description_short' => \Yii::t($category, 'Short description'),
            'description_mailing' => \Yii::t($category, 'Description for distribution'),
            'status' => \Yii::t($category, 'Status'),
            'start_at' => \Yii::t($category, 'Start date / time'),
            'end_at' => \Yii::t($category, 'End date / time'),
            'mailing_start_at' => \Yii::t($category, 'Shipment start date'),

            'statusReplacement' => \Yii::t($category, 'Status'),
            'logo' => \Yii::t($category, 'Label'),
            'promo' => \Yii::t($category, 'Promo'),
            'distribution' => \Yii::t($category, 'Newsletter'),
            'sizeChart' => \Yii::t($category, 'Dimension mesh'),
            'supplier.name' => \Yii::t($category, 'Supplier'),

            'productCount' => \Yii::t($category, 'Number of items'),

            'startAtString' => \Yii::t($category, 'Start date / time'),
            'startAtDateString' => \Yii::t($category, 'Start date'),
            'startAtTimeString' => \Yii::t($category, 'Start time'),
            'endAtString' => \Yii::t($category, 'End date / time'),
            'endAtDateString' => \Yii::t($category, 'End date'),
            'endAtTimeString' => \Yii::t($category, 'End time'),

            'priority' => \Yii::t($category, 'Position on the mainpage'),

            'mailingStartAtDateString' => \Yii::t($category, 'Shipment start date'),

            'productsSoldNumber' => \Yii::t($category, 'Sold out'),
            'productsSoldRealNumber' => \Yii::t($category, 'Sold out'),

            'isTimeActive' => \Yii::t($category, 'Currently active'),
            'isSupplierAnswerOrder' => \Yii::t($category, 'The order from the supplier was downloaded'),

            'created_at' => \Yii::t($category, 'Created'),
            'updated_at' => \Yii::t($category, 'Updated on'),
        ];
    }

    /**
     * @param boolean $value
     *
     * @return $this
     */
    public function isDeleted($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_deleted' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return $this
     */
    public function isVirtual($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_virtual' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function isStock($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_stock' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function isCharity($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_charity' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isDropShipping($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([$alias . '.is_drop_shipping' => Cast::toStr($value)]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function canPrepay($value)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'can_prepay' => (bool)$value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'supplier_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function supplierIdIn(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isVisible($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_visible' => $value,
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function onlyVisible()
    {
        return $this->isVisible(true);
    }

    /**
     * Только опубликованные (по статусу)
     *
     * @return static
     */
    public function onlyPublished()
    {
        return $this->status(self::STATUS_PUBLISHED);
    }

    /**
     * Только те акции у которых нет товаров
     *
     * @return static
     */
    public function onlyEmpty()
    {
        $alias = $this->getTableAlias();
        $table = EventProductRecord::model()->tableName();

        $query = "(NOT EXISTS(SELECT 1 FROM $table WHERE $table.event_id = $alias.id))";
        $this->dbCriteria->addCondition($query);
        return $this;
    }

    /**
     * Только активные
     *
     * @return static
     */
    public function onlyTimeActive()
    {
        $now = time();
        $this->timeStartsBefore($now);
        $this->timeEndsAfter($now);
        return $this;
    }

    /**
     * Только закончившиеся
     *
     * @return static
     */
    public function onlyTimeClosed()
    {
        $this->timeEndsBefore(time());
        return $this;
    }

    /**
     * Только будущие
     *
     * @return static
     */
    public function onlyTimeFuture()
    {
        $this->timeStartsAfter(time());
        return $this;
    }

    /**
     * <b>Начата ДО</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function timeStartsBefore($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'start_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Начинается ПОСЛЕ</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function timeStartsAfter($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'start_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Заканчивается ДО</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function timeEndsBefore($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'end_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Заканчивается ПОСЛЕ</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function timeEndsAfter($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'end_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Активная на дату </b> указанного времени
     *
     * @param integer $time
     *
     * @return $this
     */
    public function timeActive($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()
            ->addCondition($alias . '.start_at' . '<=' . Cast::toStr($value))
            ->addCondition($alias . '.end_at' . '>=' . Cast::toStr($value));

        return $this;
    }

    /**
     * <b>Активная между датами дату </b> указанного времени
     *
     * @param integer $start
     * @param integer $end
     *
     * @return $this
     */
    public function timeActiveBetween($start, $end)
    {
        $start = Cast::toStr(Cast::toUInt($start));
        $end = Cast::toStr(Cast::toUInt($end));

        $alias = $this->getTableAlias();
        $sql = "($alias.start_at<=$start AND $alias.end_at>=$start) OR ($alias.start_at<=$end AND $alias.end_at>=$end)";
        $this->getDbCriteria()
            ->addCondition($sql);

        return $this;
    }

    /**
     * @param integer|BrandRecord $value
     *
     * @return static
     * @throws CException
     */
    public function brandId($value)
    {
        if ($value instanceof BrandRecord) {
            $value = $value->id;
        }
        $value = Cast::toUInt($value);
        // write magic code here

        throw new CException('not implemented yet');

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value);
        return $this;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function idLike($id)
    {
        $value = strval($id);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function ageFrom($value)
    {
        // hacks

        $value = Cast::toUInt($value);

        $representativeNum = 100;
        $ids = ArrayUtils::getColumn($this->copy()->limit($representativeNum)->findAll(), 'id');
        $table = EventProductRecord::model()->tableName();

        $idsQuery = "('" . implode("', '", $ids) . "')";
        $query = "
            SELECT DISTINCT event_id
            FROM $table
            WHERE
            event_id IN $idsQuery AND
            age_from >= '$value'
        ";

        $validIds = ArrayUtils::getColumn($this->dbConnection->pdoInstance->query($query)->fetchAll(), 'event_id');

        $this->idIn($validIds);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function ageTo($value)
    {
        // hacks
        $value = Cast::toUInt($value);

        $representativeNum = 100;
        $ids = ArrayUtils::getColumn($this->copy()->limit($representativeNum)->findAll(), 'id');
        $table = EventProductRecord::model()->tableName();

        $idsQuery = "('" . implode("', '", $ids) . "')";
        $query = "
            SELECT DISTINCT event_id
            FROM $table
            WHERE
            event_id IN $idsQuery AND
            age_to <= '$value'
        ";

        $validIds = ArrayUtils::getColumn($this->dbConnection->pdoInstance->query($query)->fetchAll(), 'event_id');

        $this->idIn($validIds);

        return $this;
    }

    public function ageBetween($from, $to)
    {
        $from = Cast::toUInt($from);
        $to = Cast::toUInt($to);

        $representativeNum = 100;
        $ids = ArrayUtils::getColumn($this->copy()->limit($representativeNum)->findAll(), 'id');
        $table = EventProductRecord::model()->tableName();

        $idsQuery = "('" . implode("', '", $ids) . "')";
        $query = "
            SELECT DISTINCT event_id
            FROM $table
            WHERE
            event_id IN $idsQuery AND
            age_from >= '$from' AND
            age_to <= '$to'
        ";

        $validIds = ArrayUtils::getColumn($this->dbConnection->pdoInstance->query($query)->fetchAll(), 'event_id');

        $this->idIn($validIds);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function target($value)
    {
        // hacks
        $value = Cast::toStr($value);
        $value = $this->dbConnection->pdoInstance->query($value);

        $representativeNum = 100;
        $ids = ArrayUtils::getColumn($this->copy()->limit($representativeNum)->findAll(), 'id');
        $table = EventProductRecord::model()->tableName();

        $idsQuery = "('" . implode("', '", $ids) . "')";
        $query = "
            SELECT DISTINCT event_id
            FROM $table
            WHERE
            event_id IN $idsQuery AND
            FIND_IN_SET(target, '$value')
        ";

        $validIds = ArrayUtils::getColumn($this->dbConnection->pdoInstance->query($query)->fetchAll(), 'event_id');

        $this->idIn($validIds);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function categories($values)
    {
        // hacks
        $values = Cast::toStrArr($values);
        $values = array_map([$this->dbConnection->pdoInstance, 'quote'], $values);

        $representativeNum = 100;
        $ids = ArrayUtils::getColumn($this->copy()->limit($representativeNum)->findAll(), 'id');
        $table = EventProductRecord::model()->tableName();

        $idsQuery = "('" . implode("', '", $ids) . "')";
        $valuesQuery = "(" . implode(", ", $values) . ")";

        $query = "
            SELECT DISTINCT event_id
            FROM $table
            WHERE
            event_id IN $idsQuery AND
            category IN $valuesQuery
        ";

        $validIds = ArrayUtils::getColumn($this->dbConnection->pdoInstance->query($query)->fetchAll(), 'event_id');

        $this->idIn($validIds);

        return $this;
    }

    /**
     * @param int $cache
     *
     * @return $this
     */
    public function display($cache = 0)
    {
        if ($cache > 0) {
            $this->cache($cache);
        }

        return $this->isVirtual(false)
            ->onlyVisible()
            ->onlyPublished()
            ->onlyTimeActive()
            ->orderBy('end_at', 'desc')
            ->orderBy('priority', 'asc');
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);

        $criteria->compare($alias . '.supplier_id', '=' . $model->supplier_id);
        $criteria->compare($alias . '.status', '=' . $model->status);
        $criteria->compare($alias . '.is_drop_shipping', $model->is_drop_shipping);

        $criteria->compare($alias . '.start_at', '>=' . $model->start_at);
        $criteria->compare($alias . '.end_at', '<=' . $model->end_at);
        $criteria->compare($alias . '.mailing_start_at', '>=' . $model->mailing_start_at);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /// item api

    /**
     * @return bool
     */
    public function getIsStatusPublished()
    {
        return $this->status == self::STATUS_PUBLISHED;
    }

    /**
     * @return bool
     */
    public function getIsStatusEmpty()
    {
        return $this->status == self::STATUS_EMPTY;
    }

    /**
     * @return boolean
     */
    public function getIsTimeActive()
    {
        $now = time();
        return $this->start_at <= $now && $this->end_at >= $now;
    }

    /**
     * @return boolean
     */
    public function getIsTimeFinished()
    {
        $now = time();
        return $this->end_at <= $now;
    }

    /**
     * Поставщик уже отдал результат поставки, таблица заказа уже была загружена
     *
     * @return boolean
     */
    public function getIsSupplierAnswerOrder()
    {
        $now = time();
        return $this->end_at + self::SUPPLIER_ANSWER_ORDER_PERIOD <= $now;
    }

    /**
     * @return bool
     */
    public function getIsTimeFuture()
    {
        $now = time();
        return $this->start_at >= $now;
    }

    /**
     * возвращает возможные бренды по текущим условиям
     *
     * @return BrandRecord[]
     */
    public function getAvailableBrands()
    {
        $products = $this->products;
        $brandIds = ArrayUtils::onlyNonEmpty(array_unique(ArrayUtils::getColumn($products, 'brand_id')));
        return BrandRecord::model()->idIn($brandIds)->findAll();
    }

    /**
     * возвращает возможные размеры по текущим условиям
     *
     * @return string[]
     */
    public function getAvailableSizes()
    {
        $products = $this->products;
        $sizes = ArrayUtils::onlyNonEmpty(array_unique(ArrayUtils::getColumn($products, 'size')));
        sort($sizes, SORT_NATURAL | SORT_FLAG_CASE);
        return $sizes;
    }

    /**
     * цвета, массив значений
     *
     * @return string[]
     */
    public function getAvailableColors()
    {
        $products = $this->products;
        $colors = ArrayUtils::onlyNonEmpty(array_unique(ArrayUtils::getColumn($products, 'color')));
        sort($colors, SORT_NATURAL | SORT_FLAG_CASE);
        return $colors;
    }

    /**
     * года, массив значений из class YearGroups
     *
     * @return array
     */
    public function getAvailableAges()
    {
        $products = $this->products;

        $buildin = AgeGroups::instance()->getList();
        $buildinCount = count($buildin);
        $available = [];

        foreach ($products as $product) {
            $groups = AgeGroups::instance()->rangeToGroups($product->age_from, $product->age_to);
            foreach ($groups as $group) {
                if (ArrayUtils::indexOf($available, $group) == -1) {
                    $available[] = $group;
                }
            }

            if ($buildinCount == count($available)) {
                break;
            }
        }

        return $available;
    }

    /**
     * целевая аудитория, массив значений из class ProductTargets
     *
     * @return array @see ProductTargets
     */
    public function getAvailableTargets()
    {
        $products = $this->products;

        $buildin = ProductTargets::instance()->getList();
        $buildinCount = count($buildin);

        $available = [];
        foreach ($products as $product) {
            foreach ($product->target as $target) {
                if (!in_array($target, $available) && in_array($target, $buildin)) {
                    $available[] = $target;
                }
            }

            if ($buildinCount === count($available)) {
                break;
            }
        }

        return $available;
    }

    /**
     * @return GroupedProducts
     */
    public function getGroupedProducts()
    {
        return GroupedProducts::fromProducts($this->products);
    }

    /**
     * Возвращает группированный товар
     *
     * @param integer $productId
     *
     * @return GroupedProduct|null
     */
    public function getGroupedProduct($productId)
    {
        $product = ProductRecord::model()->findByPk($productId);
        if ($product === null) {
            return null;
        }

        $products = EventProductRecord::model()
            ->eventId($this->id)
            ->productId($productId)
            ->orderBy('id', 'asc')
            ->findAll();

        if (count($products) === 0) {
            return null;
        }

        return new GroupedProduct($this, $product, $products);
    }

    /**
     * Заменяет указанные атрибуты у указанного времени
     *
     * @param integer $time
     * @param null|integer $minutes
     * @param null|integer $hours
     * @param null|integer $day
     * @param null|integer $month
     * @param null|integer $year
     *
     * @return integer
     */
    protected function _combineTime($time, $minutes = null, $hours = null, $day = null, $month = null, $year = null)
    {
        $timeData = getdate($time);
        return mktime(
            $hours === null ? $timeData['hours'] : $hours,
            $minutes === null ? $timeData['minutes'] : $minutes,
            0,
            $month === null ? $timeData['mon'] : $month,
            $day === null ? $timeData['mday'] : $day,
            $year === null ? $timeData['year'] : $year
        );
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getStartAtString($format = '%d.%m.%Y %H:%M')
    {
        return $this->start_at > 0 ? strftime($format, $this->start_at) : '';
    }

    /**
     * @param string $datetime
     * @param string $format
     *
     * @return $this
     */
    public function setStartAtString($datetime, $format = '%d.%m.%Y %H:%M')
    {
        $timeData = strptime($datetime, $format);
        $this->start_at = $this->_combineTime(
            $this->start_at,
            $timeData['tm_min'],
            $timeData['tm_hour'],
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getStartAtDateString($format = '%d.%m.%Y')
    {
        return $this->start_at > 0 ? strftime($format, $this->start_at) : '';
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return string
     */
    public function setStartAtDateString($date, $format = '%d.%m.%Y')
    {
        $timeData = strptime($date, $format);
        $this->start_at = $this->_combineTime(
            $this->start_at,
            null,
            null,
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getStartAtTimeString($format = '%H:%M')
    {
        return $this->start_at > 0 ? strftime($format, $this->start_at) : '00:00';
    }

    /**
     * @param string $time
     * @param string $format
     *
     * @return string
     */
    public function setStartAtTimeString($time, $format = '%H:%M')
    {
        $timeData = strptime($time, $format);
        $this->start_at = $this->_combineTime(
            $this->start_at,
            $timeData['tm_min'],
            $timeData['tm_hour'],
            null,
            null
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getEndAtString($format = '%d.%m.%Y %H:%M')
    {
        return $this->end_at > 0 ? strftime($format, $this->end_at) : '';
    }

    /**
     * @param string $datetime
     * @param string $format
     *
     * @return $this
     */
    public function setEndAtString($datetime, $format = '%d.%m.%Y %H:%M')
    {
        $timeData = strptime($datetime, $format);
        $this->end_at = $this->_combineTime(
            $this->end_at,
            $timeData['tm_min'],
            $timeData['tm_hour'],
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getEndAtDateString($format = '%d.%m.%Y')
    {
        return $this->end_at > 0 ? strftime($format, $this->end_at) : '';
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return string
     */
    public function setEndAtDateString($date, $format = '%d.%m.%Y')
    {
        $timeData = strptime($date, $format);
        $this->end_at = $this->_combineTime(
            $this->end_at,
            null,
            null,
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getEndAtTimeString($format = '%H:%M')
    {
        return $this->end_at > 0 ? strftime($format, $this->end_at) : '';
    }

    /**
     * @param string $time
     * @param string $format
     *
     * @return string
     */
    public function setEndAtTimeString($time, $format = '%H:%M')
    {
        $timeData = strptime($time, $format);
        $this->end_at = $this->_combineTime(
            $this->end_at,
            $timeData['tm_min'],
            $timeData['tm_hour'],
            null,
            null
        );
        return $this;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getMailingStartAtDateString($format = '%d.%m.%Y')
    {
        return $this->mailing_start_at > 0 ? strftime($format, $this->mailing_start_at) : '';
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return string
     */
    public function setMailingStartAtDateString($date, $format = '%d.%m.%Y')
    {
        $timeData = strptime($date, $format);
        $this->mailing_start_at = $this->_combineTime(
            $this->mailing_start_at,
            null,
            null,
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * Времени в секундах до конца акции
     *
     * @return integer
     */
    public function getTimeLeft()
    {
        return $this->end_at - $this->start_at;
    }

    /**
     * @return float
     */
    public function getMinSellingPrice()
    {
        return EventProductRecord::model()->eventId($this->id)->findColumnMin('price');
    }

    /**
     * @return int
     */
    public function getProductsSoldNumber()
    {
        return (int)EventProductRecord::model()->eventId($this->id)->findColumnSum('number_sold');
    }

    /**
     * Количество проданного в этой акции товара
     *
     * @return int
     */
    public function getProductsSoldRealNumber()
    {
        return (int)OrderProductRecord::model()
            ->eventId($this->id)
            ->callcenterStatuses([
                OrderProductRecord::CALLCENTER_STATUS_UNMODERATED,
                OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
            ])->findColumnSum('number');
    }

    /**
     * Кол-во товара на складе
     *
     * @return int
     */
    public function getWarehouseProductNumber()
    {
        return (int)WarehouseProductRecord::model()
            ->eventId($this->id)->count();
    }

    /**
     * @return FileStorageThumbnail
     */
    public function getLogo()
    {
        return new FileStorageThumbnail($this->logo_fileid, [
            // here thumbs
        ]);
    }

    /**
     * @param Image|FileStorageThumbnail|CUploadedFile|string $value
     *
     * @return static
     */
    public function setLogo($value)
    {
        if (is_string($value)) {
            $value = \Yii::app()->filestorage->get($value);
        }

        if ($value instanceof FileStorageThumbnail) {
            $value = $value->file;
        }

        if ($value instanceof CUploadedFile) {
            $value = \Yii::app()->filestorage->factoryFromLocalFile($value->tempName);
        }

        if ($value instanceof Image && $value->getEncodedId() != $this->logo_fileid) {
            if (!empty($this->logo_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->logo_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }
            }
            if (!$value->isNewFile()) {
                $value = $value->duplicate();
            }
            $this->logo_fileid = $value->getEncodedId();
        }

        return $this;
    }

    /**
     * @return FileStorageThumbnail
     */
    public function getPromo()
    {
        return new FileStorageThumbnail($this->promo_fileid, [
            // here thumbs
        ]);
    }

    /**
     * @param Image|FileStorageThumbnail|CUploadedFile|string $value
     *
     * @return static
     */
    public function setPromo($value)
    {
        if (is_string($value)) {
            $value = \Yii::app()->filestorage->get($value);
        }

        if ($value instanceof FileStorageThumbnail) {
            $value = $value->file;
        }

        if ($value instanceof CUploadedFile) {
            $value = \Yii::app()->filestorage->factoryFromLocalFile($value->tempName);
        }

        if ($value instanceof Image && $value->getEncodedId() != $this->promo_fileid) {
            if (!empty($this->promo_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->promo_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }
            }
            if (!$value->isNewFile()) {
                $value = $value->duplicate();
            }
            $this->promo_fileid = $value->getEncodedId();
        }

        return $this;
    }

    /**
     * @return FileStorageThumbnail
     */
    public function getDistribution()
    {
        return new FileStorageThumbnail($this->distribution_fileid, [
            // here thumbs
        ]);
    }

    /**
     * @param Image|FileStorageThumbnail|CUploadedFile|string $value
     *
     * @return static
     */
    public function setDistribution($value)
    {
        if (is_string($value)) {
            $value = \Yii::app()->filestorage->get($value);
        }

        if ($value instanceof FileStorageThumbnail) {
            $value = $value->file;
        }

        if ($value instanceof CUploadedFile) {
            $value = \Yii::app()->filestorage->factoryFromLocalFile($value->tempName);
        }

        if ($value instanceof Image && $value->getEncodedId() != $this->distribution_fileid) {
            if (!empty($this->distribution_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->distribution_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }
            }
            if (!$value->isNewFile()) {
                $value = $value->duplicate();
            }
            $this->distribution_fileid = $value->getEncodedId();
        }

        return $this;
    }

    /**
     * @return FileStorageThumbnail
     */
    public function getSizeChart()
    {
        return new FileStorageThumbnail($this->size_chart_fileid, [
            // here thumbs
        ]);
    }

    /**
     * @param Image|FileStorageThumbnail|CUploadedFile|string $value
     *
     * @return static
     */
    public function setSizeChart($value)
    {
        if (is_string($value)) {
            $value = \Yii::app()->filestorage->get($value);
        }

        if ($value instanceof FileStorageThumbnail) {
            $value = $value->file;
        }

        if ($value instanceof CUploadedFile) {
            $value = \Yii::app()->filestorage->factoryFromLocalFile($value->tempName);
        }

        if ($value instanceof Image && $value->getEncodedId() != $this->size_chart_fileid) {
            if (!empty($this->size_chart_fileid)) {
                $currentFile = \Yii::app()->filestorage->get($this->size_chart_fileid);
                if ($currentFile instanceof File) {
                    $currentFile->remove();
                }
            }
            if (!$value->isNewFile()) {
                $value = $value->duplicate();
            }
            $this->size_chart_fileid = $value->getEncodedId();
        }

        return $this;
    }

    /**
     * Возвращает число между 0 и 1 (напр 0.3),
     * максимальная скидка в акции
     *
     * @return float
     */
    public function getPromoDiscount()
    {
        return $this->maxProductDiscount;
    }

    /**
     * @return int
     */
    public function getPromoDiscountPercent()
    {
        return (int)($this->getPromoDiscount() * 100);
    }

    /**
     * Возвращет всех поставщиков ответственных за поставку товара для данной акции
     *
     * @return SupplierRecord[]
     */
    public function getProductSuppliers()
    {
        $supplierIds = EventProductRecord::model()->cache(10)->eventId($this->id)->findColumnDistinct('supplier_id');
        return SupplierRecord::model()->cache(10)->findAllByPk($supplierIds);
    }

    /**
     * @return array
     */
    public function getProductSuppliersName()
    {
        return ArrayUtils::getColumn($this->getProductSuppliers(), 'name');
    }

    /**
     * @return bool
     */
    public function getIsDescriptionLong()
    {
        return $this->description >= self::DESCRIPTION_LONG_LEN;
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        return [
            self::STATUS_EMPTY => \Yii::t('backend', 'Empty'),
            self::STATUS_PUBLISHED => \Yii::t('backend', 'Ok'),
        ];
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = static::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return array
     */
    public static function visibilityReplacements()
    {
        return [
            false => \Yii::t('backend', 'Invisible'),
            true => \Yii::t('backend', 'Visible'),
        ];
    }

    /**
     * @return string
     */
    public function getVisibilityReplacement()
    {
        $replacements = static::visibilityReplacements();
        return isset($replacements[$this->is_visible]) ? $replacements[$this->is_visible] : '';
    }

    /**
     * Форсированно обновляет статус
     */
    public function forceResolveStatus()
    {
        $this->_resolveStatus();
    }

    /**
     * Описание акции для рассылок
     *
     * @param bool $onlyText
     *
     * @return string
     */
    public function getDescriptionMailing($onlyText = false)
    {
        $text = $this->description_mailing != '' ? $this->description_mailing : $this->description_short;
        return $onlyText ? strip_tags($text) : $text;
    }

    /**
     * @return bool
     */
    public function isAvailableOrdersEdit()
    {
        return $this->end_at + self::TIME_AVAILABLE_ORDERS_EDIT_AFTER_END > time();
    }

    /**
     * @param int $variant
     *
     * @return $this
     */
    public function splitByMadeInVariant(int $variant)
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = \Yii::app()->splitTesting;

        if (!$splitTesting->isSplitTestEnabled(SplitTesting::SPLITTEST_NAME_MADE_IN)) {
            return $this;
        }

        $madeIn = $splitTesting->getMadeInCountries()[$variant];

        $madeIn = array_map(function ($item) {
            return "'$item'";
        }, $madeIn);

        $madeInStr = implode(',', $madeIn);
        $this->getDbCriteria()->addCondition(
            $this->getTableAlias() .
            ".id IN (SELECT event_id FROM events_products WHERE made_in IN (" . $madeInStr . "))"
        );

        return $this;
    }

    /**
     * @return bool
     */
    public function getIfCanBeSaved()
    {
        if (!$this->getIsNewRecord() && $this->end_at < CurrentTime::getUnixTimestamp()) {
            return false;
        }

        return true;
    }
}
