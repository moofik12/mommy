<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use CLogger;
use DateTime;
use Exception;
use LogicException;
use MommyCom\Model\Behavior\BonuspointsCheckout;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\OrderNotification\OrderNotification;
use MommyCom\Model\Validator\CountriesMobileNumbersValidator;
use MommyCom\Service\Delivery\DeliveryDirectory;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Deprecated\Delivery\DeliveryCountryGroups;
use MommyCom\Service\Order\MommyOrder;
use MommyCom\Service\Order\OrderInterface;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Delivery;
use MommyCom\YiiComponent\Facade\MommyOrderManager;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Money\Money;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Yii;

/**
 * Class OrderRecord
 *
 * @property-read integer $id
 * @property-read string $idAligned
 * @property integer $user_id
 * @property string $promocode промокод
 * @property string $client_name
 * @property string $client_surname
 * @property integer $ordered_at
 * @property integer $confirmed_at время подтверждения заказа
 * @property integer $admin_id Оператор который обрабатывает в данный момент заказ
 * @property integer $admin_update_last_at Последнее время редактирование зараза (более 5-ти минут редактирование не валидно)
 * @property integer $mail_after дата когда можно начинать отправку заказа
 * @property integer $processing_status
 * @property integer $processing_status_prev предыдущий статус
 * @property string $processing_status_history_json стек истории статусов в json
 * @property integer $called_count
 * @property integer $next_called_at //время начала действия следующего прозвона
 * @property integer $delivery_type
 * @property string $telephone
 * @property string $country_code
 * @property string $novaposta_id
 * @property string $order_comment
 * @property string $storekeeper_comment
 * @property string $callcenter_comment
 * @property string $supplier_comment
 * @property string $trackcode код для трекинга
 * @property string $track_id id заказа в системе доставки
 * @property bool $is_inform_payment уведомить пользователя что ему доступна оплата заказа
 * @property-read string $delivery_attributes_json
 * @property string $affiliate_promocodes_json
 * @property int $offer_provider
 * @property string $payment_token
 * @property int $payment_token_expires_at
 * @property string $offer_id
 * @property bool $is_offer_success
 * @property bool $is_drop_shipping
 * @property float $drop_shipping_commission
 * @property integer $paid_commission_at дата выплаты комиссии
 * @property integer $supplier_id
 * @property string $uuid
 * @property integer $delivery_start_at
 * @property integer $shipped_at
 * @property float $price_total цена заказа (без валидации!)
 * @property float $to_pay к оплате за заказ за вычетом бонусов и скидок
 * @property float $delivery_price стоимость доставки
 * @property integer $bonuses количество использованых бонусов (общее кол-во)
 * @property integer $bonuses_part_used_present подарочные бонусы которые входят в self::bonuses
 * @property float $discount дисконт
 * @property float $user_discount_benefit скидка по дисконту которую нужно выплатить в виде бонусов
 * @property float $user_discount_benefit_paid скидка выплачена в виде бонусов
 * @property float $card_payed оплачено безналичными (без валидации!)
 * @property integer $weight_custom указанный вручную вес товара (в граммах)
 * @property integer $package_count количесво упаковок в которые упакован товар
 * @property integer $discount_campaign_id
 * @property integer $ip_long можно использовать фун-ю MySql INET_ATON
 * @property string $ip
 * @property string $url_referer
 * @property string $utm_params
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $line_account
 * @property-read array $processingStatusHistory стек истории статусов, array(time => status, time => status, .... )
 * @property-read float $price
 * @property integer $weight вес посылки в граммах
 * @property-read string $deliveryTypeReplacement
 * @property-read string $processingStatusReplacement
 * @property-read string $processingStatusPrevReplacement
 * @property-read integer $startMailingAt
 * @property-read integer $preDeliveryAt
 * @property-read integer $deliveryPrice
 * @property-read AdminUserRecord $admin
 * @property-read UserRecord $user
 * @property-read SupplierRecord $supplier
 * @property-read OrderProductRecord[] $positions товар
 * @property-read integer $positionCount позиций товаров
 * @property-read integer $positionNumber количество товаров
 * @property-read OrderProductRecord[] $positionsPackagingReady товар который готов к упаковке (он есть на складе)
 * @property-read OrderProductRecord[] $positionsCallcenterAccepted
 * @property-read OrderProductRecord[] $positionsStorekeeperAccepted
 * @property-read OrderTrackingRecord $tracking
 * @property-read OrderBuyoutRecord $buyout
 * @property-read OrderReturnRecord[] $returns
 * @property-read StatementOrderRecord $statementOrder
 * @property-read StatementRecord $statement
 * @property-read OrderRecord $relationOrders
 * @property-read $isPackagingReady
 * @property-read PromocodeRecord $promo промокод
 * @property-read OrderDiscountCampaignRecord $discountCampaign
 * @mixin Timestampable
 */
class OrderRecord extends DoctrineActiveRecord implements OrderInterface
{
    const PROCESSING_UNMODERATED = 0; // не обработано
    const PROCESSING_CALLCENTER_CONFIRMED = 1; // обработано колл центром
    const PROCESSING_STOREKEEPER_PACKAGED = 2; // упаковано кладовщиком
    const PROCESSING_CALLCENTER_RECALL = 3; // возвращено на прозвон
    const PROCESSING_CALLCENTER_NOT_ANSWER = 4; // звонили, не берет трубку (отмена, нет смысла продолжать)
    const PROCESSING_CANCELLED = 5; // отменено
    const PROCESSING_STOREKEEPER_MAILED = 6; // обработано кладовщиком и выслано по почте
    const PROCESSING_CALLCENTER_CALL_LATER = 8; //колл центр отложил звонок на N минут (внутренее ограничениее можно N раз)
    const PROCESSING_CALLCENTER_CALL_NOT_RESPONDING = 9; //пользователь не отвечал на звоноки
    const PROCESSING_CALLCENTER_PREPAY = 7; // выставлен на ожидание оплаты
    const PROCESSING_AUTOMATIC_CONFIRMED = 10; //автоматическое подтверждение
    const PROCESSING_CANCELLED_MAILED_OVER_SITE = 11; //поставщик попытался отправить не через МАМАМ

    const MIN_PACKAGE_WEIGHT = 200;

    const TIME_PROCESSING_VALID = 300; //время в сек. при котром ститается что админ редактирует заказ

    // process unmoderated -> callcenter -> [confirmed] | [not_answer] | [cancel] -> [event end] -> storekeeper -> [package] | [recall + @comment] -> [mailed]

    const DISCOUNT_USED_USER = 'user';
    const DISCOUNT_USED_PROMOCODE = 'promocode';
    const DISCOUNT_USED_CAMPAIGN = 'campaign';
    const DISCOUNT_USED_ALL = 'all';

    const DISCOUNT_STRATEGY_MAX = 'max'; //наибольшая скидка
    const DISCOUNT_STRATEGY_SUM = 'sum'; //сумма скидок

    const COUNT_AFFILIATE_PROMOCODES_USER_CREATE = 1;
    const COUNT_AFFILIATE_PROMOCODES_FRIENDS_CREATE = 3;

    const NEW_STRATEGY_USER_DISCOUNT_BENEFIT_AS_BONUS_UNTIL = 1460984400; //18.04.2016 16:00

    const MAX_TIME_WAIT_PACKING = 604800; //7 дней макс. время на которое можно отложить упаковку

    private static $ordersHoldTimeForSupplier = 21600; //6 часов, задежка для обработка новых заказов поставщиками

    /**
     * @var CartRecord[]|array
     */
    private $_cartPositions = [];

    /**
     * @var string
     */
    public $discountStrategy = self::DISCOUNT_STRATEGY_MAX;

    /**
     * @var bool уведомление пользователя об изменении статуса заказа
     */
    public $notification = true;

    /**
     * @var OrderRecord[]
     */
    private $_relationsOrders;

    /**
     * @var DeliveryModel
     */
    private $_deliveryModel;

    /**
     * @var MommyOrder|null
     */
    private $_mommyOrder = null;

    /**
     * @param string $className
     *
     * @return OrderRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_drop_shipping', 'isDropShipping', 'setIsDropShipping', 'isDropShipping'];
        yield ['drop_shipping_commission', 'getDropShippingCommission', 'setDropShippingCommission', 'dropShippingCommission'];
        yield ['paid_commission_at', 'getPaidCommissionAt', 'setPaidCommissionAt', 'paidCommissionAt'];
        yield ['is_bonuses_debited', 'isBonusesDebited', 'setIsBonusesDebited', 'isBonusesDebited'];
        yield ['ordered_at', 'getOrderedAt', 'setOrderedAt', 'orderedAt'];
        yield ['confirmed_at', 'getConfirmedAt', 'setConfirmedAt', 'confirmedAt'];
        yield ['payment_token_expires_at', 'getPaymentTokenExpiresAt', 'setPaymentTokenExpiresAt', 'paymentTokenExpiresAt'];
        yield ['payment_token', 'getPaymentToken', 'setPaymentToken', 'paymentToken'];
        yield ['delivery_start_at', 'getDeliveryStartAt', 'setDeliveryStartAt', 'deliveryStartAt'];
        yield ['shipped_at', 'getShippedAt', 'setShippedAt', 'shippedAt'];
        yield ['mail_after', 'getMailAfter', 'setMailAfter', 'mailAfter'];
        yield ['admin_update_last_at', 'getAdminUpdateLastAt', 'setAdminUpdateLastAt', 'adminUpdateLastAt'];
        yield ['processing_status', 'getProcessingStatus', 'setProcessingStatus', 'processingStatus'];
        yield ['processing_status_prev', 'getProcessingStatusPrev', 'setProcessingStatusPrev', 'processingStatusPrev'];
        yield ['processing_status_history_json', 'getProcessingStatusHistoryJson', 'setProcessingStatusHistoryJson', 'processingStatusHistoryJson'];
        yield ['called_count', 'getCalledCount', 'setCalledCount', 'calledCount'];
        yield ['next_called_at', 'getNextCalledAt', 'setNextCalledAt', 'nextCalledAt'];
        yield ['delivery_type', 'getDeliveryType', 'setDeliveryType', 'deliveryType'];
        yield ['client_name', 'getClientName', 'setClientName', 'clientName'];
        yield ['client_surname', 'getClientSurname', 'setClientSurname', 'clientSurname'];
        yield ['price_total', 'getPriceTotal', 'setPriceTotal', 'priceTotal'];
        yield ['to_pay', 'getToPay', 'setToPay', 'toPay'];
        yield ['uuid', 'getUuid', 'setUuid', 'uuid'];
        yield ['card_payed', 'getCardPayed', 'setCardPayed', 'cardPayed'];
        yield ['bonuses', 'getBonuses', 'setBonuses', 'bonuses'];
        yield ['bonuses_part_used_present', 'getBonusesPartUsedPresent', 'setBonusesPartUsedPresent', 'bonusesPartUsedPresent'];
        yield ['discount', 'getDiscount', 'setDiscount', 'discount'];
        yield ['user_discount_benefit', 'getUserDiscountBenefit', 'setUserDiscountBenefit', 'userDiscountBenefit'];
        yield ['user_discount_benefit_paid', 'getUserDiscountBenefitPaid', 'setUserDiscountBenefitPaid', 'userDiscountBenefitPaid'];
        yield ['weight_custom', 'getWeightCustom', 'setWeightCustom', 'weightCustom'];
        yield ['package_count', 'getPackageCount', 'setPackageCount', 'packageCount'];
        yield ['address', 'getAddress', 'setAddress', 'address'];
        yield ['zipcode', 'getZipcode', 'setZipcode', 'zipcode'];
        yield ['telephone', 'getTelephone', 'setTelephone', 'telephone'];
        yield ['novaposta_id', 'getNovapostaId', 'setNovapostaId', 'novapostaId'];
        yield ['trackcode', 'getTrackcode', 'setTrackcode', 'trackcode'];
        yield ['track_id', 'getTrackId', 'setTrackId', 'trackId'];
        yield ['is_inform_payment', 'isInformPayment', 'setIsInformPayment', 'isInformPayment'];
        yield ['offer_provider', 'getOfferProvider', 'setOfferProvider', 'offerProvider'];
        yield ['offer_id', 'getOfferId', 'setOfferId', 'offerId'];
        yield ['is_offer_success', 'isOfferSuccess', 'setIsOfferSuccess', 'isOfferSuccess'];
        yield ['order_comment', 'getOrderComment', 'setOrderComment', 'orderComment'];
        yield ['callcenter_comment', 'getCallcenterComment', 'setCallcenterComment', 'callcenterComment'];
        yield ['storekeeper_comment', 'getStorekeeperComment', 'setStorekeeperComment', 'storekeeperComment'];
        yield ['supplier_comment', 'getSupplierComment', 'setSupplierComment', 'supplierComment'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['url_referer', 'getUrlReferer', 'setUrlReferer', 'urlReferer'];
        yield ['utm_params', 'getUtmParams', 'setUtmParams', 'utmParams'];
        yield ['ip_long', 'getIpLong', 'setIpLong', 'ipLong'];
        yield ['delivery_attributes_json', 'getDeliveryAttributesJson', 'setDeliveryAttributesJson', 'deliveryAttributesJson'];
        yield ['affiliate_promocodes_json', 'getAffiliatePromocodesJson', 'setAffiliatePromocodesJson', 'affiliatePromocodesJson'];
        yield ['delivery_price', 'getDeliveryPrice', 'setDeliveryPrice', 'deliveryPrice'];
        yield ['promocode', 'getPromocode', 'setPromocode', 'promocode'];
        yield ['country_code', 'getCountryCode', 'setCountryCode', 'countryCode'];
        yield ['line_account', 'getLineAccount', 'setLineAccount', 'lineAccount'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
        yield ['discount_campaign_id', 'getDiscountCampaignId', 'setDiscountCampaignId', 'discountCampaignId'];
    }

    public function tableName()
    {
        return 'orders';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],

            'promo' => [self::BELONGS_TO, 'MommyCom\Model\Db\PromocodeRecord', ['promocode' => 'promocode']],

            'buyout' => [self::HAS_ONE, 'MommyCom\Model\Db\OrderBuyoutRecord', 'order_id'],

            'returns' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderReturnRecord', 'order_id'],

            'tracking' => [self::HAS_ONE, 'MommyCom\Model\Db\OrderTrackingRecord', 'order_id'],

            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],

            'discountCampaign' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderDiscountCampaignRecord', 'discount_campaign_id'],

            'statementOrder' => [self::HAS_ONE, 'MommyCom\Model\Db\StatementOrderRecord', 'order_id'],
            'statement' => [self::HAS_ONE, 'MommyCom\Model\Db\StatementRecord', ['statement_id' => 'id'], 'through' => 'statementOrder'],

            'positions' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'order_id'],
            'positionCount' => [self::STAT, 'MommyCom\Model\Db\OrderProductRecord', 'order_id'],
            'positionNumber' => [self::STAT, 'MommyCom\Model\Db\OrderProductRecord', 'order_id', 'select' => 'SUM(number)'],

            'positionsPackagingReady' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'order_id', 'scopes' => [
                'callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
                'onlyPackagingReady',
            ]],

            'positionsPackaged' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'order_id', 'scopes' => [
                'callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
                'onlyPackaged',
            ]],

            'positionsCallcenterAccepted' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'order_id', 'scopes' => [
                'callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
            ]],

            'positionsStorekeeperAccepted' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderProductRecord', 'order_id', 'scopes' => [
                'callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
                'storekeeperStatus' => OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED,
            ]],

            'questions' => [self::HAS_MANY, 'MommyCom\Model\Db\TaskBoardRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['deliveryAttributes, weight', 'safe'],

            ['user_id, processing_status, ordered_at, confirmed_at', 'numerical', 'integerOnly' => true],
            ['mail_after', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
            ['weight_custom', 'numerical', 'integerOnly' => true, 'allowEmpty' => true, 'min' => 0],

            //более строгий сценария для коллцентра
            ['user_id, client_name, client_surname, delivery_type, telephone',
                'required', 'on' => 'callcenter'],
            ['delivery_type', 'validateDeliveryCallcenter', 'on' => 'callcenter'],
            ['mail_after', 'validatorMailAfter', 'on' => 'callcenter'],

            ['client_name, client_surname, telephone', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['order_comment, storekeeper_comment, callcenter_comment, supplier_comment', 'filter', 'filter' => [Utf8::class, 'trim']],

            ['client_name, client_surname', 'length', 'max' => 60],

            ['telephone, trackcode', 'length', 'max' => 45],
            ['telephone', CountriesMobileNumbersValidator::class, 'on' => 'callcenter, automaticConfirmed', 'allowEmpty' => false],

            ['delivery_type', 'in', 'range' => [
                DeliveryDirectory::DUMMY,
                DeliveryDirectory::FEEDR,
            ]],

            ['processing_status', 'in', 'range' => [
                self::PROCESSING_UNMODERATED,
                self::PROCESSING_CALLCENTER_CONFIRMED,
                self::PROCESSING_CALLCENTER_RECALL,
                self::PROCESSING_CALLCENTER_NOT_ANSWER,
                self::PROCESSING_CANCELLED,
                self::PROCESSING_STOREKEEPER_PACKAGED,
                self::PROCESSING_STOREKEEPER_MAILED,
                self::PROCESSING_CALLCENTER_CALL_LATER,
                self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
                self::PROCESSING_CALLCENTER_PREPAY,
                self::PROCESSING_AUTOMATIC_CONFIRMED,
                self::PROCESSING_CANCELLED_MAILED_OVER_SITE,
            ]],

            ['processing_status', 'validatorProcessingStatus'],

            ['order_comment, storekeeper_comment, callcenter_comment, supplier_comment', 'length'
                , 'min' => 0, 'max' => 255, 'encoding' => false], //encoding=>false для strlen()

            ['mail_after, discount_campaign_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
            ['promocode', 'exists', 'className' => 'MommyCom\Model\Db\PromocodeRecord', 'attributeName' => 'promocode', 'allowEmpty' => true],
            ['discount_campaign_id', 'exists', 'className' => 'MommyCom\Model\Db\OrderDiscountCampaignRecord', 'attributeName' => 'discount_campaign_id'],

            ['promocode', 'validatorPromocode'],

            ['user_discount_benefit, user_discount_benefit_paid', 'numerical', 'min' => 0, 'max' => 10000],
            ['offer_provider', 'numerical', 'min' => 0, 'max' => 999],
            ['offer_id', 'type', 'type' => 'string'],
            ['offer_id', 'filter', 'filter' => [Utf8::class, 'trim']],

            ['is_offer_success', 'boolean'],
            ['is_inform_payment', 'boolean'],
            ['is_drop_shipping', 'boolean'],

            ['to_pay, drop_shipping_commission', 'numerical', 'min' => 0, 'max' => PHP_INT_MAX],
            ['uuid', 'length', 'max' => 40],

            ['supplier_id', 'filter', 'filter' => function ($value) {
                return $value == 0 && !$this->is_drop_shipping ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],

            ['delivery_start_at, shipped_at, paid_commission_at', 'numerical', 'min' => 0, 'max' => PHP_INT_MAX],

            ['ip_long', 'numerical', 'min' => 0, 'max' => PHP_INT_MAX],
            ['url_referer, utm_params', 'length', 'max' => 255],

            ['id, processing_status, user_id, processing_status, ordered_at, confirmed_at, zipcode, trackcode'
                . ', telephone, mail_after, price_total, to_pay, client_name, client_surname, promocode, is_drop_shipping, drop_shipping_commission',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'bonuspointsCheckout' => [
                    'class' => BonuspointsCheckout::class,
                ],
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function attributeLabels()
    {
        $tr = Translator::get();

        return [
            'id' => $tr('№ of Order'),
            'user_id' => $tr('User'),
            'promocode' => $tr('Promotional code'),
            'client_name' => $tr('Name'),
            'client_surname' => $tr('Last name'),
            'ordered_at' => $tr('Order from'),
            'confirmed_at' => $tr('Confirmed'),
            'admin_id' => $tr('Operator'),
            'bonuses' => $tr('Bonuses'),
            'bonuses_part_used_present' => $tr('Gift bonuses'),
            'discount' => $tr('A discount'),

            'is_drop_shipping' => $tr('Dropshipping order'),
            'drop_shipping_commission' => $tr('Commission'),
            'paid_commission_at' => $tr('Date of comission transfer'),
            'supplier_id' => $tr('Supplier'),
            'to_pay' => $tr('To pay'), //не включается оплата по безналу
            'delivery_start_at' => $tr('Start sending'),
            'shipped_at' => $tr('Sending date'),

            'processing_status' => $tr('Order status'),
            'processing_status_prev' => $tr('Previous order status'),
            'processing_status_history_json' => $tr('Order Status History'),
            'called_count' => $tr('Unanswered calls'),

            'delivery_type' => $tr('Type of delivery'),
            'mail_after' => $tr('Send after'),
            'mailAfterString' => $tr('Send after'),

            'address' => $tr('Address'),
            'zipcode' => $tr('Postcode'),
            'telephone' => $tr('Phone'),
            'trackcode' => $tr('Tracking Number / Consignment Note'),
            'is_inform_payment' => $tr('Notify me of availability of payment'),

            'deliveryPrice' => $tr('Cost of delivery'),
            'addBonusPoints' => $tr('Accrue bonus'),

            'weight_custom' => $tr('Weight (user)'),
            'weight' => $tr('Weight'),
            'address.name' => $tr('Address'),
            'novaposta.name' => $tr('Branch of New Post'),

            'order_comment' => $tr('Comment'),
            'callcenter_comment' => $tr('Call Center Comments'),
            'storekeeper_comment' => $tr('Storekeeper\'s comment'),
            'supplier_comment' => $tr('Supplier Comments'),

            'deliveryTypeReplacement' => $tr('Type of delivery'),
            'processingStatusReplacement' => $tr('Order status'),
            'processingStatusPrevReplacement' => $tr('Previous order status'),
            'processingStatusHistory' => $tr('History of status changes'),

            'positionCount' => $tr('Quantity of goods'),
            'positionsPackagingReadyCount' => $tr('Quantity of goods'),
            'positionsCallcenterAcceptedCount' => $tr('Quantity of goods'),
            'positionsStorekeeperAcceptedCount' => $tr('Quantity of goods'),

            'positions' => $tr('Goods'),
            'price' => $tr('Cost'),
            'price_total' => $tr('Order price'),
            'card_payed' => $tr('Paid cashless'),
            'payPriceHistory' => $tr('To pay'),
            'payPrice' => $tr('To pay'),

            'ip_long' => 'IP',
            'ip' => 'IP',
            'updated_at' => $tr('Updated on'),
        ];
    }

    /**
     * @param string $attribute
     *
     * @throws ContainerExceptionInterface
     * @throws \CDbException
     * @throws NotFoundExceptionInterface
     */
    public function validatorProcessingStatus($attribute)
    {
        $tr = Translator::get();

        $value = $this->$attribute;
        switch ($value) {
            case self::PROCESSING_STOREKEEPER_PACKAGED:
                $positions = $this->getRelated('positions');
                /* @var $positions OrderProductRecord[] */

                foreach ($positions as $position) {
                    if (!in_array($position->callcenter_status, [OrderProductRecord::CALLCENTER_STATUS_ACCEPTED, OrderProductRecord::CALLCENTER_STATUS_CANCELLED])) {
                        $this->addError($attribute, $tr('You cannot change the status while there is at least one product item unconfirmed by the call center'));
                        break;
                    }

                    if ($position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED
                        && $position->storekeeper_status != OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED
                    ) {
                        $this->addError($attribute, $tr('You cannot change the status of the order until you have confirmed the commodity positions by the storekeeper. If there is no product in the warehouse, you need to set the status of the product to "Out of stock" in front of the product and "Send to recall" to cancel the position with the call center'));
                        break;
                    }

                    if (!$this->is_drop_shipping) {
                        if ($position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED && !$position->isWarehouseFullyConnected) {
                            $this->addError($attribute, $tr('You cannot change the status while there is at least one unprepared commodity item'));
                            break;
                        }

                        $availableStatuses = [
                            WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                            WarehouseProductRecord::STATUS_MAILED_TO_CLIENT,
                            WarehouseProductRecord::STATUS_WAREHOUSE_STOCK_RESERVED,
                        ];
                        foreach ($position->connectedWarehouseProducts as $warehouse) {
                            if (!in_array($warehouse->status, $availableStatuses, true)) {
                                $this->addError($attribute, $tr('You cannot change the status while there is at least one warehouse position that has not been confirmed by the storekeeper') . ' (' . $warehouse->id . ')');
                                break;
                            }
                        }
                    }
                }
                break;
        }

        if (!$this->isCanceled()) {
            //создана позиция для возврата денег
            if ($this->isPreviewStatusCanceled() && $this->card_payed > 0) {
                $this->addError($attribute, $tr('You can not restore an order until you return money for goods'));
            }
        }

        if (!$this->is_drop_shipping && $this->processing_status == self::PROCESSING_CANCELLED_MAILED_OVER_SITE) {
            $this->addError($attribute, $tr('It is impossible to change the order status to "the vendor tried to send it not through the site" for own orders'));
        }
    }

    /**
     * @param string $attribute
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function validatorMailAfter($attribute)
    {
        if ($this->mail_after > $this->getMailAfterEndAt()) {
            $date = date('d.m.Y', $this->getMailAfterEndAt());
            $this->addError($attribute, Translator::t('Postponing the order is only possible up to') . ' ' . $date);
        }
    }

    /**
     * @param string $attribute
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function validateDeliveryCallcenter($attribute)
    {
        try {
            $delivery = Delivery::get($this->delivery_type);

            $deliveryModel = $delivery->createFormModel();
            $deliveryModel->setAttributes($this->findDeliveryModel()->getAttributes());
            $deliveryModel->setScenario($this->getScenario());

            if (!$deliveryModel->validate()) {
                $errors = current($deliveryModel->getErrors());
                $this->addError($attribute, $errors[0]);
            }
        } catch (ContainerExceptionInterface $e) {
            $this->addError($attribute, Translator::t('This delivery type cannot be used'));
        }
    }

    /**
     * @param string $attribute
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function validatorPromocode($attribute)
    {
        $value = $this->$attribute;
        if (empty($value)) {
            return;
        }

        $promocode = PromocodeRecord::model()->promocode($value)->find();
        /* @var $promocode PromocodeRecord */

        if ($promocode === null) {
            $this->addError($attribute, Translator::t('Promotional code not found'));
            return;
        }

        if (!$promocode->getIsUsable($this->user_id) &&
            ($this->id > 0 && !$promocode->getIsUsed($this->user_id, $this->id))) {
            $this->addError($attribute, Translator::t('You can not use this promotional code'));
        }
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateStatusChangeStack()
    {
        $statusHistory = $this->getProcessingStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->processing_status) {
            $now = time();
            $addToHistory = [
                'status' => $this->processing_status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $webUser = Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
            $this->processing_status_prev = $prevStatus['status'];

            $logRec = new OrderProcessingStatusHistoryRecord();
            $logRec->order_id = $this->id;
            $logRec->user_id = $addToHistory['admin_id'];
            $logRec->status = $this->processing_status;
            $logRec->status_prev = $this->processing_status_prev;
            $logRec->client_id = $this->user_id;
            if (!empty($this->callcenter_comment) || !empty($this->storekeeper_comment)) {
                $logRec->comment = implode("\n\n", ArrayUtils::onlyNonEmpty([
                    $this->callcenter_comment,
                    $this->storekeeper_comment,
                ]));
            }
            $logRec->save();
        }
        ksort($statusHistory);

        $this->processing_status_history_json = json_encode($statusHistory);
    }

    /**
     * возвращает время следующего прозвона если пользователь не отвечает
     * устанавливает статус в self::PROCESSING_CANCELLED если продолжать нет смысла
     *
     * @return int
     */
    protected function _recallTimeNotResponding()
    {
        /** @var DateTime $timeOrder */
        $timeOrder = new DateTime();
        $timeOrder->setTimestamp($this->ordered_at);

        $timeCall = new DateTime('now +1 hour');
        $nextCall = $timeCall->getTimestamp();

        $days = $timeOrder->modify('today +3 days')->diff($timeCall)->d;

        //каждый час до 3-х дней
        if ($days >= 3) {
            $this->processing_status = self::PROCESSING_CALLCENTER_NOT_ANSWER;
        }

        $this->next_called_at = $nextCall;
        $this->called_count++;
    }

    /**
     * возвращает время следующего прозвона
     *
     * @return int
     */
    protected function _recallTime()
    {
        $nextTime = date_create('now +15 minute')->getTimestamp();
        $oldTime = Cast::toUInt($this->next_called_at);

        if ($oldTime > $nextTime) {
            $nextTime = $oldTime;
        }

        $this->next_called_at = $nextTime;
    }

    /**
     * возвращает время следующего прозвона
     *
     * @return int
     */
    protected function _recallTimePrepay()
    {
        $nextTime = date_create('now +12 hours')->getTimestamp();

        $this->next_called_at = $nextTime;
    }

    protected function _refreshPrices()
    {
        $this->price_total = $this->getPrice();
        $this->delivery_price = $this->getDeliveryPrice();

        $statusesNotUpdatedPrice = [self::PROCESSING_STOREKEEPER_MAILED];

        if (!in_array($this->processing_status, $statusesNotUpdatedPrice)) {
            $this->to_pay = $this->getPayPrice(false);
        }
    }

    /**
     * Создает запись хранения состояния оплаты за товар
     */
    protected function _createBuyoutStub()
    {
        if ($this->isNewRecord) {
            return;
        }

        if ($this->processing_status != self::PROCESSING_STOREKEEPER_MAILED) {
            return;
        }

        if (OrderBuyoutRecord::model()->orderId($this->id)->exists()) {
            return;
        }

        $model = new OrderBuyoutRecord();
        $model->order_id = $this->id;

        if ($this->getPayPriceHistory() == 0) {
            $model->status = OrderBuyoutRecord::STATUS_PAYED;
            return;
        }

        $model->save();
    }

    /**
     * Создает запись хранения состояния доставки посылки
     */
    protected function _createTrackingStub()
    {
        if ($this->getIsNewRecord()) {
            return;
        }

        if ($this->processing_status != self::PROCESSING_STOREKEEPER_MAILED) {
            return;
        }

        if (OrderTrackingRecord::model()->orderId($this->id)->exists()) {
            return;
        }

        $model = new OrderTrackingRecord();
        $model->status = OrderTrackingRecord::STATUS_TRANSITION;
        $model->order_id = $this->id;
        $model->is_drop_shipping = $this->is_drop_shipping;
        $model->supplier_id = $this->supplier_id;
        $model->save();
    }

    protected function _refreshWarehouseState()
    {
        if ($this->isNewRecord) {
            return;
        }

        if ($this->processing_status == $this->processing_status_prev) {
            return;
        }

        $items = WarehouseProductRecord::model()
            ->orderId($this->id)
            ->findAll();
        /* @var $items WarehouseProductRecord[] */

        foreach ($items as $item) {
            $item->forceResolveOrderState();
            $item->save();
        }
    }

    protected function _refreshDiscount()
    {
        $this->discount = $this->getCountDiscount();
        $this->user_discount_benefit = $this->getUserDiscountAsBonus();
    }

    protected function _refreshTime()
    {
        if ($this->shipped_at == 0 && $this->processing_status == self::PROCESSING_STOREKEEPER_MAILED) {
            $this->shipped_at = time();
        }

        $this->delivery_start_at = $this->getStartMailingAt();
    }

    protected function _changeCommissionDatetime()
    {
        if ($this->drop_shipping_commission && $this->paid_commission_at == 0) {
            $this->paid_commission_at = time();
        }
    }

    protected function _sendOrderNotification()
    {
        if ($this->notification) {
            $this->getNotification()->send();
        }
    }

    protected function _updateUserLastOrderTime()
    {
        if (!$this->isNewRecord) {
            return;
        }

        $user = $this->user;
        $user->last_order_at = time();
        $user->saveAttributes(['last_order_at']);
    }

    /**
     * @return bool
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->delivery_attributes_json = json_encode($this->findDeliveryModel() ?: [], JSON_UNESCAPED_UNICODE);

        if ($this->isNewRecord) {
            if (!$this->ordered_at) {
                $this->ordered_at = time();
            }

            if (empty($this->ip_long) && Yii::app()->request) {
                $this->ip = Yii::app()->request->getUserHostAddress();
            }

            if (empty($this->uuid)) {
                $this->uuid = self::generateUid($this->user_id, $this->ordered_at);
            }

            try { // for console app
                $webUser = Yii::app()->user;
                $this->url_referer = $webUser->getUrlReferer();
                $this->setUtmParams($webUser->getUtmParams());
            } catch (CException $e) {
                Yii::log(Translator::t('Failed to assign properties when creating an order') . ' url_referer, utm_params. ' . $e, CLogger::LEVEL_ERROR);
            }
        }

        $this->_updateUserLastOrderTime();
        $this->_updateStatusChangeStack();

        switch ($this->processing_status) {
            case self::PROCESSING_CALLCENTER_CALL_LATER:
                $this->_recallTime();
                break;

            case self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING:
                $this::_recallTimeNotResponding();
                break;

            case self::PROCESSING_CALLCENTER_PREPAY:
                $this::_recallTimePrepay();
                break;

            default:
                break;
        }

        $this->_updateBankRefund();
        $this->_refreshPrices();
        $this->_createBuyoutStub();
        $this->_createTrackingStub();
        $this->_refreshWarehouseState();
        $this->_refreshDiscount();
        $this->_refreshTime();
        $this->_changeCommissionDatetime();

        if ($this->isConfirmed() && $this->confirmed_at == 0) {
            $this->confirmed_at = time();
        }

        return $result;
    }

    /**
     * @return bool|void
     */
    public function afterSave()
    {
        //создание товаров в заказе
        $this->_createPositionsFromCart();

        if ($this->isNewRecord) {
            $this->getRelated('positions', true);
        }

        //должен идти после добавления товаров, запуск BonuspointsCheckout
        parent::afterSave();

        //обновление скидки должно быть после обновления бонусов
        if ($this->isNewRecord) {
            //force update discount
            $this->_sendOrderNotification();
            $this->_refreshDiscount();
            $this->updateByPk($this->id, [
                'discount' => $this->discount,
                'user_discount_benefit' => $this->user_discount_benefit,
            ]);
        }

        $this->_updateOrderProductForCallcenterStatus();
        $this->unbindFromAdmin();
        $this->_splitTestTracking();
        $this->_assignPartnerFromPromocode();
    }

    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->positions as $position) {
            $position->callcenter_status = OrderProductRecord::CALLCENTER_STATUS_CANCELLED;
            $position->delete();
        }
    }

    protected function _assignPartnerFromPromocode()
    {
        //обновление суммы партнерского зачисления, если оно есть для этого заказа
        $partnerAdmission = UserPartnerAdmissionRecord::model()->orderId($this->id)->find();
        if ($partnerAdmission !== null) {
            $partnerAdmission->count_product_order = count($this->positions);
            $partnerAdmission->sum_order = $this->getPrice(true, true);
            $partnerAdmission->save();
        } elseif ($this->user->partnerInvitedBy !== null) {
            $partnerAdmission = new UserPartnerAdmissionRecord();
            $partnerAdmission->partner_id = $this->user->partnerInvitedBy->partner_id;
            $partnerAdmission->order_id = $this->id;
            $partnerAdmission->user_id = $this->user_id;
            $partnerAdmission->status = UserPartnerAdmissionRecord::STATUS_NEW;
            $partnerAdmission->count_product_order = count($this->positions);
            $partnerAdmission->sum_order = $this->getPrice(true, true);
            $partnerAdmission->save();
        }

        if ($this->promocode !== ''
            && in_array($this->processing_status,
                [
                    self::PROCESSING_UNMODERATED, // не обработано
                    self::PROCESSING_CALLCENTER_CONFIRMED, // обработано колл центром
                    self::PROCESSING_CALLCENTER_RECALL, // возвращено на прозвон
                    self::PROCESSING_AUTOMATIC_CONFIRMED //автоматическое подтверждение
                ]
            )
        ) {
            $this->refresh();
            $promocode = $this->promo;
            /** @var $promocode PromocodeRecord */
            if ($promocode !== null) {
                if ($promocode->type == PromocodeRecord::TYPE_UNLIMITED && $promocode->reason == PromocodeRecord::REASON_PARTNER) {
                    //если введен партнерский промокод, автоматом присоединяем к партнеру
                    $this->user->assignToPartner($promocode->partner_id, '', '', UserPartnerInviteRecord::TYPE_INCOMING_PROMOCODE);
                    if ($this->user->partnerInvitedBy !== null) {
                        if ($this->user->partnerInvitedBy->partner_id == $promocode->partner_id) {
                            //добавление к парнерскому зачислению по заказу, если он еще не был создан
                            $partnerAdmission = UserPartnerAdmissionRecord::model()->orderId($this->id)->find();
                            if ($partnerAdmission === null) {
                                $partnerAdmission = new UserPartnerAdmissionRecord();
                                $partnerAdmission->partner_id = $promocode->partner_id;
                                $partnerAdmission->order_id = $this->id;
                                $partnerAdmission->user_id = $this->user_id;
                                $partnerAdmission->status = UserPartnerAdmissionRecord::STATUS_NEW;
                            }
                            $partnerAdmission->count_product_order = count($this->positions);
                            $partnerAdmission->sum_order = $this->getPrice(true, true);

                            $partnerAdmission->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * только для новых заказов!
     * добавление позиций товаров Корзины
     *
     * @param CartRecord[] $cartPositions
     *
     * @return bool
     */
    public function setCartPositions(array $cartPositions)
    {
        if (!$this->isNewRecord) {
            return false;
        }

        $item = current($cartPositions);
        if (!$item instanceof CartRecord) {
            return false;
        }

        $this->_cartPositions = $cartPositions;
        return true;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function _splitTestTracking()
    {
        if ($this->isNewRecord) {
            try {
                $splitTestTracking = new SplitTestTrackingRecord();
                $splitTestTracking->target = SplitTestTrackingRecord::TARGET_CREATE_ORDER;
                $splitTestTracking->user_id = $this->user_id;
                $splitTestTracking->order_id = $this->id;
                $splitTestTracking->testParams = Yii::app()->splitTesting->getAvailable();

                $splitTestTracking->save();
            } catch (Exception $e) {
                Yii::log(Translator::t('Error while creating test tracking') . $e->getTraceAsString(), CLogger::LEVEL_ERROR);
            }
        }
    }

    /**
     * создание позиций товаров на основе Корзины
     */
    protected function _createPositionsFromCart()
    {
        if (!$this->isNewRecord || empty($this->_cartPositions)) {
            return;
        }

        $user = Yii::app()->user;
        $somethingNotSaved = false;
        $cart = $user->cart;

        foreach ($this->_cartPositions as $key => $position) {
            $orderItem = new OrderProductRecord();
            $orderItem->order_id = $this->id;
            $orderItem->user_id = $user->id;
            $orderItem->product_id = $position->product_id;
            $orderItem->event_id = $position->event_id;
            $orderItem->number = $position->number;

            if (!$orderItem->save()) {
                $somethingNotSaved = true;
                $this->addErrors($orderItem->getErrors()); // bubble errors
                break;
            }
        }

        if (!$somethingNotSaved) {
            foreach ($this->_cartPositions as $position) {
                // remove from cart
                $cart->remove($position->product_id);
            }
        } else {
            Yii::log(Translator::t('ERROR ADDING ITEMS TO ORDER') . ' Errors: ' . print_r($this->getErrors(), true), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * возврат денег оплаченные по б/н
     * создание возврата только если изменение статусов больше не предвидится
     */
    protected function _updateBankRefund()
    {
        $priceRefund = 0.0;

        if ($this->card_payed <= 0) {
            return;
        }

        if ($this->isCanceled()) {
            $priceRefund = $this->card_payed;
        } elseif ($this->processing_status == self::PROCESSING_STOREKEEPER_MAILED) {
            $priceRefund = $this->card_payed - $this->getPayPrice(false);
        }

        if ($priceRefund > 0) {
            OrderBankRefundRecord::refundOrder($this, $priceRefund);
        }
    }

    /**
     * действия с товаром при изменении заказа
     * при отмене перевести все товары заказа в статус отмены
     */
    protected function _updateOrderProductForCallcenterStatus()
    {
        if ($this->isCanceled()) {
            foreach ($this->positions as $item) {
                $item->callcenter_status = OrderProductRecord::CALLCENTER_STATUS_CANCELLED;
                $item->save();
            }
        }
    }

    /**
     * @param array $value
     */
    public function setUtmParams(array $value)
    {
        $this->utm_params = json_encode($value, JSON_UNESCAPED_UNICODE, 3);
    }

    /**
     * @return string
     */
    public function getUtmParams()
    {
        if ($this->utm_params) {
            $values = json_decode($this->utm_params, true);
            return is_array($values) ? $values : [];
        }

        return [];
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function userIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $values);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephone($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.telephone' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephoneLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.telephone', $value, true);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function adminId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.admin_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function notId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition(
            $alias . '.id', [$value]
        );
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function deliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.delivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function deliveryTypes(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.delivery_type', $value);

        return $this;
    }

    /**
     * @param integer $value
     * @param bool $filterTime
     *
     * @return static
     */
    public function processingStatus($value, $filterTime = true)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $condition = [
            $alias . '.processing_status' => $value,
        ];

        if ($filterTime) {
            //статусы зависящие от времени
            if ($value === self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING
                || $value === self::PROCESSING_CALLCENTER_CALL_LATER
                || $value === self::PROCESSING_CALLCENTER_PREPAY
            ) {
                $condition += [$alias . '.next_called_at <' => time()];
                //$condition += array($alias .'.next_called_at >' => 1);
            }
        }

        $this->getDbCriteria()->addColumnCondition($condition, 'AND', 'OR');
        return $this;
    }

    /**
     * @param integer[] $values
     * @param string $operator 'AND'|'OR'
     *
     * @return $this
     */
    public function processingStatuses(array $values, $operator = "AND")
    {
        $values = Cast::toUIntArr($values);
        $validOperators = [
            'and', 'or', 'AND', 'OR',
        ];

        if (!in_array($operator, $validOperators)) {
            throw new LogicException('Operator not support');
        }

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.processing_status', $values, $operator);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function processingStatusesNot(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.processing_status', $values);
        return $this;
    }

    /**
     * Заказы у которых события закончились
     */
    public function eventsFinished()
    {
        $time = time();

        // fucking awesome code
        $this->with([
            'positions' => ['together' => true],
            'positions.event' => ['together' => true],
        ]);

        $table = EventRecord::model()->tableName();
        $query = "(SELECT MAX(end_at) FROM $table WHERE id IN (positions.event_id)) < $time";

        $this->getDbCriteria()->addCondition($query);

        return $this;
    }

    /**
     * Только те заказы которые МОЖНО упаковать (товар есть в полной комплеции на складе)
     *
     * @return static
     */
    public function onlyPackagingReady()
    {
        // fucking awesome code

        $aliasPrefix = '__order_onlyPackagingReady_';
        $positionsAlias = $aliasPrefix . 'positions';
        $packagingReadyAlias = $aliasPrefix . 'positionsPackagingReady';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($positionsAlias, $relations['positions']);
        $metadata->addRelation($packagingReadyAlias, $relations['positionsPackagingReady']);

        // it's magic ;)
        $this->with([
            $positionsAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
            $packagingReadyAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
        ]);

        $alias = $this->tableAlias;

        $query = "COUNT(DISTINCT $positionsAlias.id) = COUNT(DISTINCT $packagingReadyAlias.id)";
        $this->getDbCriteria()->mergeWith([
            'group' => $alias . '.id',
            'having' => $query,
        ]);
        return $this;
    }

    /**
     * Только те заказы которые НЕЛЬЗЯ упаковать (товар нет в полной комплеции на складе)
     *
     * @return static
     */
    public function onlyPackagingUnready()
    {
        // fucking awesome code

        $aliasPrefix = '__order_onlyPackagingUnready_';
        $positionsAlias = $aliasPrefix . 'positions';
        $packagingReadyAlias = $aliasPrefix . 'positionsPackagingReady';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($positionsAlias, $relations['positions']);
        $metadata->addRelation($packagingReadyAlias, $relations['positionsPackagingReady']);

        // it's magic ;)
        $this->with([
            $positionsAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
            $packagingReadyAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
        ]);

        $alias = $this->tableAlias;

        $query = "COUNT(DISTINCT $positionsAlias.id) <> COUNT(DISTINCT $packagingReadyAlias.id)";
        $this->getDbCriteria()->mergeWith([
            'group' => $alias . '.id',
            'having' => $query,
        ]);
        return $this;
    }

    /**
     * Только те заказы которые уже полностью упакованы
     *
     * @return static
     */
    public function onlyPackaged()
    {
        // fucking awesome code

        // добавляем еще relation для того, чтобы yii думал, что по ним еще выборки не было
        $aliasPrefix = '__order_onlyPackaged_';
        $positionsAlias = $aliasPrefix . 'positions';
        $packagedAlias = $aliasPrefix . 'positionsPackaged';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($positionsAlias, $relations['positions']);
        $metadata->addRelation($packagedAlias, $relations['positionsPackaged']);

        // it's magic ;)
        $this->with([
            $positionsAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
            $packagedAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
        ]);

        $alias = $this->tableAlias;

        $query = "COUNT(DISTINCT $positionsAlias.id) = COUNT(DISTINCT $packagedAlias.id)";
        $this->getDbCriteria()->mergeWith([
            'group' => $alias . '.id',
            'having' => $query,
        ]);
        return $this;
    }

    /**
     * Только те заказы которые уже полностью упакованы
     *
     * @return static
     */
    public function onlyUnPackaged()
    {
        // fucking awesome code

        // добавляем еще relation для того, чтобы yii думал, что по ним еще выборки не было
        $aliasPrefix = '__order_onlyPackaged_';
        $positionsAlias = $aliasPrefix . 'positions';
        $packagedAlias = $aliasPrefix . 'positionsPackaged';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($positionsAlias, $relations['positions']);
        $metadata->addRelation($packagedAlias, $relations['positionsPackaged']);

        // it's magic ;)
        $this->with([
            $positionsAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
            $packagedAlias => [
                'together' => true,
                'scopes' => ['callcenterStatus' => OrderProductRecord::CALLCENTER_STATUS_ACCEPTED],
            ],
        ]);

        $alias = $this->tableAlias;

        $query = "COUNT(DISTINCT $positionsAlias.id) <> COUNT(DISTINCT $packagedAlias.id)";
        $this->getDbCriteria()->mergeWith([
            'group' => $alias . '.id',
            'having' => $query,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function positionCountCallcenterAccepted($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->tableAlias;
        $productsTable = OrderProductRecord::model()->tableName();

        $query = "(SELECT COUNT(1) FROM $productsTable WHERE order_id = $alias.id) = " . Cast::toStr($value);

        $this->getDbCriteria()->mergeWith([
            'condition' => $query,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function positionNumberCallcenterAccepted($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->tableAlias;
        $productsTable = OrderProductRecord::model()->tableName();

        $query = "(SELECT SUM(number) FROM $productsTable WHERE order_id = $alias.id) = " . Cast::toStr($value);

        $this->getDbCriteria()->mergeWith([
            'condition' => $query,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function positionCount($value)
    {
        $value = Cast::toUInt($value);
        $this->with([
            'positionCount' => ['together' => true],
        ]);

        $this->getDbCriteria()->addColumnCondition([
            'positionCount' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function positionNumber($value)
    {
        $value = Cast::toUInt($value);
        $this->with([
            'positionNumber' => ['together' => true],
        ]);

        $this->getDbCriteria()->addColumnCondition([
            'positionNumber' => $value,
        ]);
        return $this;
    }

    /**
     * исключаются редактируемые заказы self::admin_id > 0 и admin_update_last_at > 5 мин.
     *
     * @return static
     */
    public function processingNotActive()
    {
        $alias = $this->getTableAlias();
        $condition = [
            $alias . '.admin_id' => 0,
            $alias . '.admin_update_last_at <' => time() - self::TIME_PROCESSING_VALID, //более 5-ти минут
        ];

        $this->getDbCriteria()->addColumnCondition($condition, 'OR');
        return $this;
    }

    /**
     * @param null | integer $value , при NULL возвращает все заказы которые редактируются
     *
     * @return $this
     */
    public function processingActive($value = null)
    {
        $alias = $this->getTableAlias();

        $condition = [
            $alias . '.admin_id >' => 1,
            $alias . '.admin_update_last_at >' => time() - self::TIME_PROCESSING_VALID, //более 5-ти минут
        ];

        if ($value !== null) {
            $value = Cast::toUInt($value);
            $condition = [$alias . '.admin_id' => $value];
        }

        $this->getDbCriteria()->addColumnCondition($condition);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function processingStatusPrev($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'processing_status_prev' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return $this
     */
    public function orderedAtBetween($start, $end)
    {
        $start = Cast::toUInt($start);
        $end = Cast::toUInt($end);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addBetweenCondition($alias . '.ordered_at', $start, $end);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function orderedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.ordered_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function orderedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.ordered_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function confirmedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.confirmed_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function confirmedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.confirmed_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function paidCommissionAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.paid_commission_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function paidCommissionAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.paid_commission_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function updatedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.updated_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function updatedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.updated_at', ">= $value");

        return $this;
    }

    /**
     * @param int $start
     * @param int $end
     *
     * @return $this
     */
    public function mailAfterBetween($start, $end)
    {
        $start = Cast::toUInt($start);
        $end = Cast::toUInt($end);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addBetweenCondition($alias . '.mail_after', $start, $end);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function mailAfterLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.mail_after', "<= $value");

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function mailAfterGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.mail_after', ">= $value");

        return $this;
    }

    /**
     * @return $this
     */
    public function onlyMailReady()
    {
        $this->mailAfterGreater(0);
        $this->mailAfterLower(time());

        return $this;
    }

    /**
     * @return $this
     */
    public function onlyMailUnReady()
    {
        $this->mailAfterGreater(time());
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function promocode($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'promocode' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isPromocode($value)
    {
        $value = Cast::toBool($value);
        $sign = $value ? '<>' : '=';

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . " .promocode $sign ''");

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isDropShipping($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([$alias . '.is_drop_shipping' => Cast::toStr($value)]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function uuidIs($value)
    {
        $value = Cast::toStr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([$alias . '.uuid' => $value]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierIdIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.supplier_id' => $value]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function promocodeIn(array $value)
    {
        $value = Cast::toStrArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.promocode', $value);
        return $this;
    }

    /**
     * @param array $value
     * @param $max
     *
     * @return $this
     */
    public function eventIdIn(array $value, $max = -1)
    {
        if ($max > 0) {
            $value = array_slice($value, 0, $max);
        }
        $value = Cast::toUIntArr($value);

        $criteria = $this->getDbCriteria();
        $criteria->with = [
            'positions' => ['together' => true],
        ];
        $criteria->addInCondition('positions.event_id', $value, 'OR');
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function productId($value)
    {
        $value = Cast::toUInt($value);

        $criteria = $this->getDbCriteria();
        $criteria->with = [
            'positions' => ['together' => true],
        ];

        $criteria->addInCondition('positions.product_id', [$value]);

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $criteria = $this->getDbCriteria();
        $criteria->with = [
            'positions' => ['together' => true],
        ];

        $criteria->addInCondition('positions.event_id', [$value]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function trackcode($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'trackcode' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function offerProvider($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.offer_provider' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function isOfferSuccess($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_offer_success' => $value,
        ]);

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isInformPayment($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_inform_payment' => $value,
        ]);

        return $this;
    }

    /**
     * Только те заказы у которых уже есть трек код
     *
     * @return static
     */
    public function onlyHasTrackcode()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . ' .trackcode <> ""');
        return $this;
    }

    /**
     * Только те заказы у которых еще нет треккода
     *
     * @return static
     */
    public function onlyNotHasTrackcode()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . ' .trackcode = ""');
        return $this;
    }

    /**
     * Нужно выплать скидку в виде бонуса
     *
     * @return static
     */
    public function needPayUserDiscountBenefit()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . ' .user_discount_benefit > 0');
        return $this;
    }

    /**
     * Уже выплачена скидка в виде бонуса
     *
     * @param bool $value
     *
     * @return static
     */
    public function isPaidUserDiscountBenefit($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $sql = $alias . ' .user_discount_benefit_paid = 0';

        if ($value) {
            $sql = $alias . ' .user_discount_benefit_paid > 0';
        }

        $this->getDbCriteria()->addCondition($sql);
        return $this;
    }

    /**
     * helper
     *
     * @return static
     */
    public function orderNew()
    {
        return $this->processingStatus(self::PROCESSING_UNMODERATED)
            ->processingStatus(self::PROCESSING_CALLCENTER_CALL_LATER)
            ->processingStatus(self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING)
            ->processingStatus(self::PROCESSING_CALLCENTER_PREPAY);
    }

    /**
     * helper
     *
     * @return static
     */
    public function orderRepeat()
    {
        return $this->processingStatus(self::PROCESSING_CALLCENTER_RECALL);
    }

    /**
     * helper
     *
     * @return static
     */
    public function orderCancelled()
    {
        return $this->processingStatus(self::PROCESSING_CANCELLED)
            ->processingStatus(self::PROCESSING_CALLCENTER_NOT_ANSWER)
            ->processingStatus(self::PROCESSING_CANCELLED_MAILED_OVER_SITE);
    }

    /**
     * helper
     *
     * @return static
     */
    public function orderNotConfirmed()
    {
        return $this->processingStatus(self::PROCESSING_CALLCENTER_CALL_LATER, false)
            ->processingStatus(self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING, false)
            ->processingStatus(self::PROCESSING_CALLCENTER_PREPAY, false);
    }

    /**
     * @return static
     */
    public function processingStatusNotCancelled()
    {
        return $this->processingStatusesNot([
            self::PROCESSING_CANCELLED,
            self::PROCESSING_CALLCENTER_NOT_ANSWER,
            self::PROCESSING_CANCELLED_MAILED_OVER_SITE,
        ]);
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
//        $criteria->compare($alias . '.zipcode', $model->zipcode, true);
        $criteria->compare($alias . '.trackcode', $model->trackcode, true);
        $criteria->compare($alias . '.telephone', $model->telephone, true);

        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.delivery_type', $model->delivery_type);
        $criteria->compare($alias . '.processing_status', $model->processing_status);
        $criteria->compare($alias . '.promocode', $model->promocode);

        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.is_drop_shipping', $model->is_drop_shipping);

        $criteria->compare($alias . '.price_total', $model->price_total);
        $criteria->compare($alias . '.to_pay', $model->to_pay);

        $criteria->compare($alias . '.client_name', $model->client_name, true);
        $criteria->compare($alias . '.client_surname', $model->client_surname, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * Обьединение заказов в один (в текущий)
     *
     * @param OrderRecord $orderMerge
     *
     * @return int
     */
    public function orderMerge(OrderRecord $orderMerge)
    {
        $countAdd = 0;
        $orderMergeCardPayed = Cast::toFloat($orderMerge->card_payed);
        foreach ($orderMerge->positions as $position) {
            $moveResult = $this->movePosition($position, $position->callcenter_status);
            if ($moveResult) {
                $countAdd++;
            }
        }

        if ($this->promo === null && $orderMerge->promo !== null) {
            $this->promocode = $orderMerge->promocode;
            $orderMerge->promocode = '';

            $orderMerge->callcenter_comment .= ' ' . Translator::t('The promocode {promocode} has been moved to the {id} order.', ['{promocode}' => $this->promocode, '{id}' => $this->id]);
        }

        if ($this->discount_campaign_id == 0 && $orderMerge->discount_campaign_id > 0) {
            $this->discount_campaign_id = $orderMerge->discount_campaign_id;
        }

        if ($orderMergeCardPayed > 0) {
            $this->card_payed = Cast::toFloat($this->card_payed) + $orderMergeCardPayed;
            $currencyName = Yii::app()->countries->getCurrency()->getName('short');
            if ($orderMerge->callcenter_comment) {
                if (!preg_match('/\.$/ui', $orderMerge->callcenter_comment)) {
                    $orderMerge->callcenter_comment .= '.';
                }
                $orderMerge->callcenter_comment .= ' ';
            }
            $orderMerge->callcenter_comment .= Translator::t('Transfer {orderMergeCardPayed} {currencyName} of paid funds to the order {id}.',
                [
                    '{orderMergeCardPayed}' => $orderMergeCardPayed,
                    '{currencyName}' => $currencyName,
                    '{id}' => $this->id]);
        }

        $this->updateByPk($this->id, [
            'promocode' => $this->promocode,
            'card_payed' => $this->card_payed,
            'discount_campaign_id' => $this->discount_campaign_id,
        ]);

        $orderMerge->processing_status = self::PROCESSING_CANCELLED;
        $orderMerge->card_payed = 0;
        $orderMerge->save(false);

        return $countAdd;
    }

    /**
     * Перенос товара в этот заказ
     *
     * @param OrderProductRecord $position
     * @param int $callCenterStatus
     *
     * @return bool
     * @throws CException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function movePosition(OrderProductRecord $position, $callCenterStatus = OrderProductRecord::CALLCENTER_STATUS_UNMODERATED)
    {
        if ($this->isNewRecord) {
            throw new CException('must be not new');
        }

        $positionOldCallcenterStatus = $position->callcenter_status;
        $positionOldStorekeeperStatus = $position->storekeeper_status;

        $attributes = $position->getAttributes();
        unset($attributes['id']);

        $newPosition = null;
        foreach ($this->positions as $positionInOrder) {
            if ($positionInOrder->product_id == $position->product_id) {
                $newPosition = $positionInOrder;
                break;
            }
        }

        if (!$newPosition) {
            $newPosition = new OrderProductRecord();
            $newPosition->setAttributes($attributes, false);
            $newPosition->order_id = $this->id;
            $newPosition->number = $position->number;

            $newPosition->storekeeper_status = OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED;
            $newPosition->callcenter_status = $callCenterStatus;
            $newPosition->callcenter_comment = Translator::t('Copied from order №') . $position->order_id . ' ' . $position->callcenter_comment;
            $newPosition->relation_order_product_id = $position->id;
        } else {
            $newPosition->number += $position->number;
        }

        //отмена товара при копировании
        $position->storekeeper_status = OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED;
        $position->callcenter_status = OrderProductRecord::CALLCENTER_STATUS_CANCELLED;

        if ($position->save(false) && $newPosition->save()) {
            return true;
        } else {
            //возврат старых значений
            $position->storekeeper_status = $positionOldStorekeeperStatus;
            $position->callcenter_status = $positionOldCallcenterStatus;
            $position->save(false);

            if (!$newPosition->isNewRecord) {
                $newPosition->number -= $position->number;
                $newPosition->save();
            }

            $msg = Translator::t('An error occurred while transferring the promotion')
                . ' '
                . $newPosition->product_id
                . ' Errros: '
                . print_r($newPosition->getErrors(), true);

            Yii::log($msg, CLogger::LEVEL_ERROR, 'application.order.movePosition');
        }

        return false;
    }
    /// item api

    /**
     * @param int $userId
     * @param int $timestamp
     *
     * @return string
     */
    public static function generateUid(int $userId, int $timestamp): string
    {
        $result = pack('VV', $userId, $timestamp) . random_bytes(7);

        $result = base64_encode($result);
        $result = rtrim($result, '=');
        $result = strtr($result, '+/', '*_');

        return $result;
    }

    /**
     * @param string|int $uid
     *
     * @return bool
     */
    public static function isUid($uid): bool
    {
        if (strpos($uid, '-')) {
            $parts = explode('-', $uid);
            return (2 === count($parts) && is_numeric($parts[0]) && is_numeric($parts[1]));
        }

        return (1 === preg_match('/[a-zA-Z\d_-]{20}/', $uid));
    }

    /**
     * @param SupplierRecord|integer $supplier
     *
     * @return bool
     * @throws LogicException
     */
    public function isBelongsToSupplier($supplier)
    {
        if ($supplier instanceof SupplierRecord) {
            return $supplier->id == $this->supplier_id;
        } elseif (is_numeric($supplier)) {
            return $supplier == $this->supplier_id;
        }

        throw new LogicException('Supplier isn`t instance of SupplierRecord and numeric');
    }

    /**
     * Заказ обрабатывается оператором
     *
     * @return bool
     */
    public function isProcessingActive()
    {
        return $this->admin_id > 0 && $this->admin_update_last_at >= time() - self::TIME_PROCESSING_VALID;
    }

    /**
     * Заказ редактировали, но бросили
     *
     * @return bool
     */
    public function isProcessingLost()
    {
        return (
            OrderRecord::PROCESSING_UNMODERATED === $this->processing_status &&
            $this->admin_id > 0 &&
            $this->admin_update_last_at < time() - self::TIME_PROCESSING_VALID
        );
    }

    /**
     * @deprecated
     * @return bool
     */
    public function isMyProcessingActive()
    {
        $webUser = Yii::app()->user;

        return $webUser ? $webUser->id == $this->admin_id : false;
    }

    /**
     * @return int
     */
    public function getPositionsPackagingReadyCount()
    {
        return count($this->positionsPackagingReady);
    }

    /**
     * @return int
     */
    public function getPositionsCallcenterAcceptedCount()
    {
        return count($this->positionsCallcenterAccepted);
    }

    /**
     * @return mixed
     */
    public function getPositionsStorekeeperAcceptedCount()
    {
        return count($this->positionsStorekeeperAccepted);
    }

    /**
     * @return int
     */
    public function getPositionsPackagingReadyNumber()
    {
        $result = 0;
        foreach ($this->positionsPackagingReady as $position) {
            $result += $position->number;
        }
        return $result;
    }

    /**
     * @return int
     */
    public function getPositionsCallcenterAcceptedNumber()
    {
        $result = 0;
        foreach ($this->positionsCallcenterAccepted as $position) {
            $result += $position->number;
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getPositionsStorekeeperAcceptedNumber()
    {
        $result = 0;
        foreach ($this->positionsStorekeeperAccepted as $position) {
            $result += $position->number;
        }
        return $result;
    }

    /**
     * @return OrderProductRecord[]
     */
    public function getPositionsAvailableForPay()
    {
        return array_filter($this->positions, function ($position) {
            /* @var  $position OrderProductRecord */
            return $position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_UNMODERATED
                || $position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED;
        });
    }

    /**
     * @return int
     */
    public function getPositionsNumberAvailableForPay()
    {
        $number = 0;
        foreach ($this->getPositionsAvailableForPay() as $positions) {
            $number += $positions->number;
        }

        return $number;
    }

    /**
     * @return int
     */
    public function getCountNotDeletedPositions()
    {
        $countOrderProductsConfirmed = 0;

        foreach ($this->positions as $orderPosition) {
            if ($orderPosition->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_CANCELLED) {
                $countOrderProductsConfirmed++;
            }
        }

        return $countOrderProductsConfirmed;
    }

    /**
     * @return string
     */
    public function getIdAligned()
    {
        return str_pad($this->id, 10, '0', STR_PAD_LEFT);
    }

    /**
     * @return OrderNotification
     */
    public function getNotification()
    {
        return new OrderNotification($this);
    }

    /**
     * Заменяет указанные атрибуты у указанного времени
     *
     * @param integer $time
     * @param null|integer $minutes
     * @param null|integer $hours
     * @param null|integer $day
     * @param null|integer $month
     * @param null|integer $year
     *
     * @return integer
     */
    protected function _combineTime($time, $minutes = null, $hours = null, $day = null, $month = null, $year = null)
    {
        $timeData = getdate($time);
        return mktime(
            $hours === null ? $timeData['hours'] : $hours,
            $minutes === null ? $timeData['minutes'] : $minutes,
            0,
            $month === null ? $timeData['mon'] : $month,
            $day === null ? $timeData['mday'] : $day,
            $year === null ? $timeData['year'] : $year
        );
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getMailAfterString($format = '%d.%m.%Y')
    {
        return strftime($format, max($this->mail_after, $this->ordered_at));
    }

    /**
     * @param string $datetime
     * @param string $format
     *
     * @return $this
     */
    public function setMailAfterString($datetime, $format = '%d.%m.%Y')
    {
        $timeData = strptime($datetime, $format);
        $this->mail_after = $this->_combineTime(
            $this->mail_after,
            $timeData['tm_min'],
            $timeData['tm_hour'],
            $timeData['tm_mday'],
            $timeData['tm_mon'] + 1,
            $timeData['tm_year'] + 1900 // see docs of strptime
        );
        return $this;
    }

    /**
     * @return float
     */
    public function getPromocodeDiscount()
    {
        if ($this->promo === null) {
            return 0;
        }

        if ($this->is_drop_shipping) {
            return 0;
        }

        $positions = [];
        foreach ($this->positions as $position) {
            if ($position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED
                || $position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_UNMODERATED
            ) {
                $positions[] = $position;
            }
        }

        return $this->promo->getDiscount($positions);
    }

    /**
     * @return float
     */
    public function getUserDiscount()
    {
        $discount = 0.0;

        if ($this->is_drop_shipping) {
            return $discount;
        }

        if ($this->user === null || $this->isEnableUserDiscountAsBonus($this->ordered_at)) {
            return $discount;
        }

        $percentDiscount = Cast::toUInt($this->user->discount_percent);
        $discount = $this->getPrice() * $percentDiscount / 100;

        return ceil($discount);
    }

    /**
     * Сумма бонусов которые долны выплатиться пользователю на основе его персональной скидки
     *
     * @return int
     */
    public function getUserDiscountAsBonus()
    {
        $discount = 0;

        if ($this->is_drop_shipping) {
            return $discount;
        }

        if (!$this->isEnableUserDiscountAsBonus($this->ordered_at) || $this->getCountDiscount() > 0) {
            return $discount;
        }

        $percentDiscount = Cast::toUInt($this->user->discount_percent);
        $discount = $this->getPrice(true) * $percentDiscount / 100;

        return ceil($discount);
    }

    /**
     * Доступна ли выплата скидки в виде бонусов
     *
     * @param int $orderAt timestamp создания заказа
     *
     * @return bool
     */
    public static function isEnableUserDiscountAsBonus($orderAt)
    {
        if ($orderAt < self::NEW_STRATEGY_USER_DISCOUNT_BENEFIT_AS_BONUS_UNTIL) {
            return false;
        }

        return true;
    }

    /**
     * @return int
     */
    public function getCampaignDiscount()
    {
        $discount = 0;

        if ($this->is_drop_shipping) {
            return $discount;
        }

        if ($this->discountCampaign === null) {
            return $discount;
        }

        $discount = $this->discountCampaign->getDiscount($this->getPrice());

        return $discount;
    }

    /**
     * @param bool $withBonuses вычесть из стоимости бонусы
     * @param bool $withDiscounts вычесть из стоимости дисконты
     * @param bool $withCard вычесть из стоимости безналичную оплату
     *
     * @return float
     */
    public function getPrice($withBonuses = false, $withDiscounts = false, $withCard = false)
    {
        $price = 0.0;

        foreach ($this->positions as $position) {
            if ($position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED
                || $position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_UNMODERATED
            ) {
                $price += $position->getPriceTotal();
            }
        }

        if ($withBonuses) {
            $price -= $this->bonuses;
        }

        if ($withCard) {
            $price -= $this->card_payed;
        }

        if ($withDiscounts) {
            $price -= $this->getCountDiscount();
        }

        return Cast::toUFloat($price);
    }

    /**
     * Стоимость которую должен заплатить пользователь
     *
     * @param bool $withCard вычесть из стоимости безналичную оплату
     *
     * @return float
     */
    public function getPayPrice($withCard = true)
    {
        return max($this->getPrice(true, true, $withCard), 0.0);
    }

    /**
     * Стоимость заказа которая была отправлена
     *
     * @param bool $withCard вычесть из стоимости безналичную оплату
     *
     * @return float
     */
    public function getPayPriceHistory($withCard = true)
    {
        //в случае если картой оплачено больше (отменяли товар)
        return $this->getPriceHistory(true, true, $withCard);
    }

    /**
     * Стоимость заказа которая была
     *
     * @param bool $withBonuses
     * @param bool $withDiscounts
     * @param bool $withCard
     *
     * @return mixed
     */
    public function getPriceHistory($withBonuses = false, $withDiscounts = false, $withCard = false)
    {
        $price = $this->price_total;

        if ($withBonuses) {
            $price -= $this->bonuses;
        }

        if ($withDiscounts) {
            if ($this->discount) {
                $price -= $this->discount;
            }
        }

        if ($withCard) {
            $price -= $this->card_payed;
        }

        //в случае если картой оплачено больше (отменяли товар)
        return max($price, 0.0);
    }

    /**
     * @return bool
     */
    public function isActiveSuppliersNewOrdersHoldTime()
    {
        if ($this->processing_status == self::PROCESSING_UNMODERATED) {
            return true;
        }

        return $this->confirmed_at > self::getSuppliersNewOrdersShowAt();
    }

    /**
     * @return int
     */
    public static function getSuppliersNewOrdersShowAt()
    {
        return time() - self::$ordersHoldTimeForSupplier;
    }

    /**
     * количество бонусов которые были использованы
     *
     * @return float
     */
    public function getUsedDiscounts()
    {
        return Cast::toUFloat($this->discount);
    }

    /**
     * Скидка которая была использована для определенной суммы, части заказа
     *
     * @param $price
     *
     * @return int
     */
    public function getCountUsedDiscounts($price)
    {
        $price = Cast::toFloat($price);
        if ($price < 0 || $this->getPriceHistory() <= 0) {
            return 0;
        }

        $usedDiscounts = floor($price * $this->getUsedDiscounts() / $this->getPriceHistory());

        return min($usedDiscounts, $this->getUsedDiscounts());
    }

    /**
     * Подарочные бонусы которые были использованы для определенной суммы, части заказа
     *
     * @param $price
     *
     * @return int
     */
    public function getCountUsedBonusesPresent($price)
    {
        $price = Cast::toFloat($price);
        if ($price < 0 || $this->getPriceHistory() <= 0) {
            return 0;
        }

        $usedBonusesPresent = floor($price * $this->bonuses_part_used_present / $this->getPriceHistory());

        return min($usedBonusesPresent, $this->bonuses_part_used_present);
    }

    /**
     * @param $price
     *
     * @return int|mixed
     */
    public function getCountUsedBonusesInner($price)
    {
        $price = Cast::toFloat($price);
        if ($price < 0 || $this->getPriceHistory() <= 0) {
            return 0;
        }

        $bonusesInner = $this->bonuses - $this->bonuses_part_used_present;
        $usedBonusesPresent = floor($price * $bonusesInner / $this->getPriceHistory());

        return min($usedBonusesPresent, $bonusesInner);
    }

    /**
     * Сколько подарочных/не возвращаемых денег было использовано для определенной суммы, части заказа
     *
     * @param $price
     *
     * @return int
     */
    public function getCountUsedBenefice($price)
    {
        $benefice = $this->getCountUsedDiscounts($price) + $this->getCountUsedBonusesPresent($price);
        return $benefice;
    }

    /**
     * {@inheritdoc}
     */
    public function getWeight(): int
    {
        $weight = $this->getMommyOrder()->getWeight();

        return max($weight, self::MIN_PACKAGE_WEIGHT);
    }

    public function setWeight($value)
    {
        $this->weight_custom = Cast::toUInt($value);
    }

    /**
     * @return int
     */
    public function getMailAfterEndAt()
    {
        return $this->getStartMailingAt() + self::MAX_TIME_WAIT_PACKING;
    }

    /**
     * время предварительной отгрузки
     *
     * @return integer
     */
    public function getStartMailingAt()
    {
        $mailingStartAt = [0];
        foreach ($this->positions as $orderProduct) {
            $mailingStartAt[] = $orderProduct->event->mailing_start_at;
        }
        $lastMailingStartAt = max($mailingStartAt);

        return $lastMailingStartAt;
    }

    /**
     * время окончания последней акции
     *
     * @return integer
     */
    public function getLastEventEndAt()
    {
        $lastEventsEndAt = [0];
        foreach ($this->positions as $orderProduct) {
            $lastEventsEndAt[] = $orderProduct->event->end_at;
        }
        $lastEventEndAt = max($lastEventsEndAt);

        return $lastEventEndAt;
    }

    /**
     * время окончания последней акции
     *
     * @return integer
     */
    public function getLastEventStartAt()
    {
        $lastEventsStartAt = [0];
        foreach ($this->positions as $orderProduct) {
            $lastEventsStartAt[] = $orderProduct->event->start_at;
        }
        $lastEventStartAt = max($lastEventsStartAt);

        return $lastEventStartAt;
    }

    /**
     * @return DeliveryModel|null
     */
    private function findDeliveryModel(): ?DeliveryModel
    {
        if (null === $this->_deliveryModel) {
            if (!$this->delivery_attributes_json) {
                return null;
            }

            if (!Delivery::has($this->delivery_type)) {
                return null;
            }

            try {
                $this->_deliveryModel = Delivery::get($this->delivery_type)->createFormModel();
            } catch (ContainerExceptionInterface $e) {
                return null;
            }
            $this->_deliveryModel->setAttributes(json_decode($this->delivery_attributes_json, true, 4));
        }

        return $this->_deliveryModel;
    }

    /**
     * @param DeliveryModel $deliveryModel
     */
    public function setDeliveryModel(DeliveryModel $deliveryModel): void
    {
        $this->_deliveryModel = $deliveryModel;
    }

    /**
     * @param string $name
     * @param string|null $default
     *
     * @return string|null
     * @deprecated
     */
    public function getDeliveryAttribute(string $name, $default = null)
    {
        $deliveryModel = $this->findDeliveryModel();

        return $deliveryModel->{$name} ?? $default;
    }

    /**
     * @param string $name
     * @param string $value
     *
     * @deprecated
     */
    public function setDeliveryAttribute(string $name, $value)
    {
        $deliveryModel = $this->findDeliveryModel();

        $deliveryModel->{$name} = $value;
    }

    /**
     * @return array
     */
    public function getDeliveryAttributes(): array
    {
        $deliveryModel = $this->findDeliveryModel();

        return $deliveryModel ? $deliveryModel->getAttributes() : [];
    }

    /**
     * @param array $deliveryAttributes
     */
    public function setDeliveryAttributes(array $deliveryAttributes): void
    {
        $this->findDeliveryModel()->setAttributes($deliveryAttributes);
    }

    /**
     * @return PromocodeRecord[] массив с промокодами
     * @throws CException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getAffiliatePromocodes()
    {
        if ($this->isNewRecord) {
            throw new CException(Translator::t('Promotional codes are not available for new orders!'));
        }

        if (empty($this->affiliate_promocodes_json)) {
            $promocodesUntil = strtotime('tomorrow +3 weeks');
            $listNumberUserCreate = range(1, min(self::COUNT_AFFILIATE_PROMOCODES_USER_CREATE, 10)); //ограниечение на всякий случай
            $listNumberFriendsCreate = range(1, min(self::COUNT_AFFILIATE_PROMOCODES_FRIENDS_CREATE, 30)); //ограниечение на всякий случай

            /** @var PromocodeRecord[] $modelsToSave */
            $modelsToSave = [];

            foreach ($listNumberUserCreate as $number) {
                /** @var PromocodeRecord $promo */
                $promo = new PromocodeRecord('benefice');
                $promo->type = PromocodeRecord::TYPE_PERSONAL;
                $promo->reason = PromocodeRecord::REASON_ATTRACT_FRIENDS;
                $promo->user_id = $this->user_id;
                $promo->valid_until = $promocodesUntil;
                $promo->percent = 6;

                $modelsToSave[] = $promo;
            }

            foreach ($listNumberFriendsCreate as $number) {
                $promo = new PromocodeRecord('benefice');
                $promo->type = PromocodeRecord::TYPE_ANY;
                $promo->reason = PromocodeRecord::REASON_ATTRACT_FRIENDS;
                $promo->benefice_user_id = $this->user_id;
                $promo->benefice_percent = 5;
                $promo->user_id = $this->user_id;
                $promo->valid_until = $promocodesUntil;
                $promo->percent = 5;

                $modelsToSave[] = $promo;
            }

            $saved = static::saveMany($modelsToSave);
            if (!$saved) {
                $error = array_reduce($modelsToSave, function ($error, $item) {
                    /** @var PromocodeRecord $item */
                    if (!$error && $item->hasErrors()) {
                        $error = reset($item->getErrors());
                    }

                    return $error;
                });

                $msg = Translator::t('Error while creating promotional codes!') . ' Error: ' . print_r($error, true);
                Yii::log($msg, CLogger::LEVEL_ERROR);
                throw new CException(Translator::t('Error while creating promotional codes!'));
            }

            $pomocodesValues = ArrayUtils::getColumn($modelsToSave, 'promocode');

            $this->affiliate_promocodes_json = json_encode($pomocodesValues, JSON_UNESCAPED_UNICODE, 3);
            $this->updateByPk($this->id, [
                'affiliate_promocodes_json' => $this->affiliate_promocodes_json,
            ]);
        }

        /** @var array|null $promocodes */
        $promocodes = json_decode($this->affiliate_promocodes_json, true, 3);

        return $promocodes ? $promocodes : [];
    }

    /**
     * время доставки товара (предварительно)
     *
     * @return integer
     */
    public function getPreDeliveryAt()
    {
        /** @var $timeMailing $timeMailing */
        $timeMailing = new DateTime();
        $lastEventEndAt = $this->getLastEventEndAt();
        $timeMailing->setTimestamp($lastEventEndAt);
        $timeMailing->modify("+5 days");

        //отправка товара не осуществляется в выходные дни
        $weekDay = $timeMailing->format('N');
        if ($weekDay == 7) {
            $timeMailing->modify('+1 day');
        }

        return $timeMailing->getTimestamp();
    }

    /**
     * @return array
     */
    public function getProcessingStatusHistory()
    {
        return Cast::toArr(json_decode($this->processing_status_history_json, true, 3));
    }

    /**
     * @param string $ip
     */
    public function setIP($ip)
    {
        $long = self::ip2long($ip);
        if ($long > 0 && $long < PHP_INT_MAX) {
            $this->ip_long = $long;
        }
    }

    /**
     * @return string
     */
    public function getIP()
    {
        return self::long2ip($this->ip_long);
    }

    /**
     * @param $ip
     *
     * @return string
     */
    public static function ip2long($ip)
    {
        return sprintf("%u", ip2long($ip));
    }

    /**
     * @param $long
     *
     * @return string
     */
    public static function long2ip($long)
    {
        return long2ip($long);
    }

    /**
     * @param int $adminId
     *
     * @return bool
     */
    public function bindToAdmin(int $adminId)
    {
        $timestamp = time();

        $attributes = ['admin_id' => $adminId, 'admin_update_last_at' => $timestamp];

        $condition = 'admin_id = 0';
        $condition .= ' OR admin_id = :admin_id';
        $condition .= ' OR admin_update_last_at < :valid_time';

        $params = [
            'admin_id' => $adminId,
            'valid_time' => $timestamp - self::TIME_PROCESSING_VALID,
        ];

        $result = $this->updateByPk($this->id, $attributes, $condition, $params);

        if ($result) {
            $this->refresh();
        }

        return (bool)$result;
    }

    /**
     * @param int $adminId
     *
     * @return bool
     */
    public function forceBindToAdmin(int $adminId)
    {
        return (bool)$this->updateByPk($this->id, ['admin_id' => $adminId, 'admin_update_last_at' => time()]);
    }

    /**
     * @return bool
     */
    public function unbindFromAdmin()
    {
        return (bool)$this->updateByPk($this->id, ['admin_id' => 0, 'admin_update_last_at' => 0]);
    }

    /**
     * Принудительное обновление полной стоимости
     *
     * @return $this
     */
    public function forceRefreshTotalPrice()
    {
        $this->_refreshPrices();
        return $this;
    }

    /**
     * @return array
     */
    public function getDeliveryPrices()
    {
        $result = [];

        foreach (Delivery::available()->getDeliveries() as $delivery) {
            $result[$delivery->getId()] = $this->getDeliveryPrice();
        }

        return $result;
    }

    /**
     * @return float|null
     */
    public function getDeliveryPrice()
    {
        if (!Delivery::has($this->delivery_type)) {
            return null;
        }

        if (null !== $this->delivery_price) {
            return $this->delivery_price;
        }

        $positions = $this->positionsCallcenterAccepted;

        return $this->findDeliveryModel()->calcDeliveryCost($positions);
    }

    /**
     * @return bool
     */
    public function hasInformPayment()
    {
        return $this->isAvailableForPayment() && $this->is_inform_payment;
    }

    /**
     * @return bool
     */
    public function toldInformPayment()
    {
        $this->is_offer_success = 0;
        return !!$this->updateByPk($this->id, ['is_inform_payment' => $this->is_offer_success]);
    }

    /**
     * @return string
     */
    public function getDeliveryTypeReplacement()
    {
        $type = $this->delivery_type;
        $availableDeliveries = Delivery::available();

        if (!$availableDeliveries->hasDelivery($type)) {
            return '';
        }

        return $availableDeliveries->getDelivery($type)->getName();
    }

    /**
     * @param bool $onlyAvailable
     *
     * @return array
     */
    public static function deliveryTypeReplacements($onlyAvailable = false)
    {
        return DeliveryCountryGroups::instance()->getDelivery()->getList();
    }

    /**
     * @return string
     */
    public function getProcessingStatusReplacement()
    {
        $replacements = self::processingStatusReplacements();
        return isset($replacements[$this->processing_status]) ? $replacements[$this->processing_status] : '';
    }

    /**
     * @return string
     */
    public function getProcessingStatusPrevReplacement()
    {
        $replacements = self::processingStatusReplacements();
        return isset($replacements[$this->processing_status_prev]) ? $replacements[$this->processing_status_prev] : '';
    }

    /**
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function processingStatusReplacements()
    {
        $tr = Translator::get();

        return [
            self::PROCESSING_UNMODERATED => $tr('Not processed'),
            self::PROCESSING_CALLCENTER_CONFIRMED => $tr('processed by call center'),
            self::PROCESSING_CALLCENTER_RECALL => $tr('return call'),
            self::PROCESSING_CALLCENTER_NOT_ANSWER => $tr('canceled (not responding)'),
            self::PROCESSING_CANCELLED => $tr('canceled'),
            self::PROCESSING_STOREKEEPER_PACKAGED => $tr('packed by a storekeeper'),
            self::PROCESSING_STOREKEEPER_MAILED => $tr('packaged and shipped'),
            self::PROCESSING_CALLCENTER_CALL_LATER => $tr('deferred call'),
            self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING => $tr('does not answer (call back)'),
            self::PROCESSING_CALLCENTER_PREPAY => $tr('waiting for payment (w / n)'),
            self::PROCESSING_AUTOMATIC_CONFIRMED => $tr('automatically confirmed'),
            self::PROCESSING_CANCELLED_MAILED_OVER_SITE => $tr('the vendor tried not to send via the site'),
        ];
    }

    public static function storekeeperStatuses()
    {
        return [
            self::PROCESSING_STOREKEEPER_PACKAGED,
            self::PROCESSING_STOREKEEPER_MAILED,
        ];
    }

    public static function callcenterStatuses()
    {
        return [
            self::PROCESSING_CALLCENTER_CONFIRMED,
            self::PROCESSING_CALLCENTER_RECALL,
            self::PROCESSING_CALLCENTER_NOT_ANSWER,
            self::PROCESSING_CALLCENTER_CALL_LATER,
            self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
            self::PROCESSING_CALLCENTER_PREPAY,
            self::PROCESSING_AUTOMATIC_CONFIRMED,
        ];
    }

    /**
     * Возвращает текущую доступную скидку
     *
     * @return float|mixed
     */
    public function getCountDiscount()
    {
        $discount = 0.0;

        if ($this->is_drop_shipping) {
            return $discount;
        }

        switch ($this->discountStrategy) {
            case self::DISCOUNT_STRATEGY_MAX:
                $discount = max([$this->getPromocodeDiscount(), $this->getUserDiscount(), $this->getCampaignDiscount(), $discount]);
                break;

            case self::DISCOUNT_STRATEGY_SUM:
                $discount += $this->getPromocodeDiscount();
                $discount += max($this->getUserDiscount(), $this->getCampaignDiscount());
                break;

            default:
                break;
        }

        return $discount;
    }

    /**
     * @return bool|string
     */
    public function getNameDiscountUsed()
    {
        $name = false;

        switch ($this->discountStrategy) {
            case self::DISCOUNT_STRATEGY_MAX:
                $name = self::DISCOUNT_USED_USER;
                $discount = $this->getUserDiscount();

                if ($this->getPromocodeDiscount() > $discount) {
                    $name = self::DISCOUNT_USED_PROMOCODE;
                } elseif ($this->getCampaignDiscount() > $discount) {
                    $name = self::DISCOUNT_USED_CAMPAIGN;
                }

                break;

            case self::DISCOUNT_STRATEGY_SUM:
                $name = self::DISCOUNT_USED_ALL;
                break;

            default:
                break;
        }

        return $name;
    }

    /**
     * @return bool
     */
    public function isDeliveryAddressRight()
    {
        return $this->findDeliveryModel() ? $this->findDeliveryModel()->validate() : false;
    }

    /**
     * @return string
     */
    public function getDeliveryAddress()
    {
        return $this->findDeliveryModel()->getDeliveryAddress();
    }

    /**
     * @param bool $refresh
     *
     * @return static[]
     */
    public function getRelationOrders($refresh = false)
    {
        if ($this->_relationsOrders === null || $refresh) {
            $this->_relationsOrders = OrderRecord::model()->uuidIs($this->uuid)->idInNot([$this->id])->findAll();
        }

        return $this->_relationsOrders;
    }

    /**
     * если ли возможность оплатить заказ
     *
     * @return bool
     */
    public function isAvailableForPayment(): bool
    {
        if (!in_array($this->processing_status, [
            self::PROCESSING_UNMODERATED,
            self::PROCESSING_CALLCENTER_CALL_LATER,
            self::PROCESSING_CALLCENTER_CONFIRMED,
            self::PROCESSING_AUTOMATIC_CONFIRMED,
            self::PROCESSING_CALLCENTER_PREPAY,
        ])) {
            return false;
        }

        foreach ($this->getPositionsAvailableForPay() as $product) {
            if (!$product->event->can_prepay) {
                return false;
            }
        }

        return $this->getPayPrice() > 0;
    }

    /**
     * Некоторые данные не должны быть показаны
     *
     * @return bool
     */
    public function isShowFullInfoForSupplier()
    {
        return $this->processing_status == self::PROCESSING_STOREKEEPER_MAILED;
    }

    /**
     * нужен прозвон по заказу
     *
     * @return bool
     */
    public function isNeedCall()
    {
        return $this->processing_status == self::PROCESSING_CALLCENTER_CALL_LATER
            || $this->processing_status == self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING
            || $this->processing_status == self::PROCESSING_CALLCENTER_PREPAY;
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        $result = false;

        if ($this->processing_status == self::PROCESSING_CANCELLED
            || $this->processing_status == self::PROCESSING_CALLCENTER_NOT_ANSWER
            || $this->processing_status == self::PROCESSING_CANCELLED_MAILED_OVER_SITE
        ) {
            $result = true;
        }
        return $result;
    }

    /**
     * @return bool
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function isMerged()
    {
        return (
            $this->processing_status == self::PROCESSING_CANCELLED &&
            false !== strpos($this->callcenter_comment, Translator::t('Canceled. Due to merge with order №'))
        );
    }

    /**
     * @return bool
     */
    public function isNotConfirmed()
    {
        $result = false;

        if ($this->processing_status == self::PROCESSING_CALLCENTER_CALL_LATER
            || $this->processing_status == self::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING
            || $this->processing_status == self::PROCESSING_UNMODERATED
        ) {
            $result = true;
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        $result = false;

        if (!$this->isCanceled() && !$this->isNotConfirmed()) {
            $result = true;
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function isPreviewStatusCanceled()
    {
        $result = false;

        if ($this->processing_status_prev == self::PROCESSING_CANCELLED
            || $this->processing_status_prev == self::PROCESSING_CALLCENTER_NOT_ANSWER
            || $this->processing_status_prev == self::PROCESSING_CANCELLED_MAILED_OVER_SITE
        ) {
            $result = true;
        }
        return $result;
    }

    /**
     * Атрибуты которые должны копироваться при разделении заказа
     *
     * @return array
     */
    public static function getSeparateAttributes()
    {
        return ['user_id', 'admin_id', 'admin_update_last_at', 'delivery_type', 'zipcode',
            'novaposta_id', 'client_name', 'client_surname', 'discount_campaign_id', 'address', 'telephone', 'offer_provider',
            'offer_id', 'order_comment', 'callcenter_comment', 'storekeeper_comment', 'supplier_comment', 'delivery_attributes_json',
            'is_drop_shipping', 'supplier_id', 'url_referer', 'utm_params'];
    }

    /**
     * @param string $uid
     *
     * @return OrderRecord[]
     */
    public function findByUid(string $uid): array
    {
        if ($this->isUid($uid)) {
            return $this->uuidIs($uid)->findAll();
        }

        $order = $this->findByPk($uid);

        return $order ? [$order] : [];
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getTrackId(): string
    {
        return $this->track_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getCost(): Money
    {
        return $this->getMommyOrder()->getCost();
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoice(): Money
    {
        return $this->getMommyOrder()->getInvoice();
    }

    /**
     * {@inheritdoc}
     */
    public function getDeliveryModel(): DeliveryModel
    {
        return $this->getMommyOrder()->getDeliveryModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerName(): string
    {
        return $this->getMommyOrder()->getCustomerName();
    }

    /**
     * {@inheritdoc}
     */
    public function getPhoneNumber(): string
    {
        return $this->getMommyOrder()->getPhoneNumber();
    }

    /**
     * Возвращает статус заказа в системе заказа
     *
     * @return int
     */
    public function getFulfilmentStatus(): int
    {
        return $this->getMommyOrder()->getFulfilmentStatus();
    }

    /**
     * Возвращает описание заказа
     *
     * @return string
     */
    public function getOrderDescription(): string
    {
        return $this->getMommyOrder()->getOrderDescription();
    }

    /**
     * @return MommyOrder
     */
    private function getMommyOrder(): MommyOrder
    {
        if (null === $this->_mommyOrder) {
            $this->_mommyOrder = MommyOrderManager::find($this->getId());
        }

        return $this->_mommyOrder;
    }
}
