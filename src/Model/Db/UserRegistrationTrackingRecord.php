<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Form\RegistrationForm;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use Yii;

/**
 * Class UserRegistrationTrackingRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $ip
 * @property string $uuid
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserRegistrationTrackingRecord extends DoctrineActiveRecord
{
    public function tableName()
    {
        return 'users_registration_tracking';
    }

    /**
     * @param string $className
     *
     * @return UserRegistrationTrackingRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['phone', 'getPhone', 'setPhone', 'phone'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
        yield ['uuid', 'getUuid', 'setUuid', 'uuid'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    /**
     * @return bool
     */
    public static function fillWithRequestData()
    {
        $app = Yii::app();
        $request = $app->request;
        $form = new RegistrationForm('simple');
        $dataForm = $request->getPost('RegistrationForm');
        $anonymousCookie = Yii::app()->request->cookies['anonymousId'];
        $anonymousId = isset($anonymousCookie->value) ? $anonymousCookie->value : '';

        if (!empty($dataForm) && !empty($anonymousId)) {
            $form->setAttributes($dataForm);
            $registrationTracker = UserRegistrationTrackingRecord::model();

            $registrations = $registrationTracker
                ->findAll('uuid=:uuid', [':uuid' => $anonymousId]);

            if (count($registrations) < 3) {
                $registrationTracker->name = $form->name;
                $registrationTracker->phone = $form->telephone;
                $registrationTracker->email = $form->email;
                $registrationTracker->ip = $_SERVER['REMOTE_ADDR'];
                $registrationTracker->uuid = $anonymousId;
                $registrationTracker->created_at = time();
                $registrationTracker->updated_at = time();
                $registrationTracker->save();

                return true;
            }
        }

        return false;
    }
}
