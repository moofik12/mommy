<?php

namespace MommyCom\Model\Db;

use MommyCom\Entity\AdminSupplier;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class AdminSuppliers
 *
 * @property-read int $id
 * @property int $admin_id
 * @property int $supplier_id
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property array $suppliersId
 * @property-read AdminUserRecord $user
 * @mixin Timestampable
 */
class AdminSuppliers extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return AdminSuppliers
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntityClassName(): string
    {
        return AdminSupplier::class;
    }

    public function tableName()
    {
        return 'admins_suppliers';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
            'supplierData' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
        ];
    }

    public function rules()
    {
        return [
            ['admin_id', 'required'],
            ['admin_id', 'unique'],
            ['admin_id', 'exist', 'allowEmpty' => false,
                'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],

            ['supplier_id', 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'admin_id' => Translator::t('Login'),
            'suppliers_json' => Translator::t('Password'),

            'created_at' => Translator::t('Updated'),
            'updated_at' => Translator::t('Created'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        /** @var self $model */
        $model = $provider->model;
        $criteria = $provider->getCriteria();
        $criteria->compare('admin_id', $model->admin_id);
        $criteria->compare('suppliers_json', $model->suppliers_json, true);

        return $provider;
    }
}
