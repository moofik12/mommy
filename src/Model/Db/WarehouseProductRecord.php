<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use InvalidArgumentException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class WarehouseProductRecord
 *
 * @property integer $id
 * @property integer $packaging_security_code число от 10 до 99 (случайное)
 * @property integer $supplier_id поставщик
 * @property integer $event_product_id товар
 * @property integer $event_product_sizeformat тип размера
 * @property integer $event_product_size размер
 * @property integer $event_product_price стоимость товара в закупке
 * @property integer $event_id акция
 * @property integer $product_id базовая информация о товаре
 * @property string $request_fileid
 * @property integer $status статус
 * @property string $status_comment комментарий к текущему статусу
 * @property string $status_history_json
 * @property integer $status_prev
 * @property integer $status_changed_at
 * @property string $lost_status статус пропажи
 * @property string $lost_status_comment
 * @property string $lost_status_history_json
 * @property integer $lost_status_prev
 * @property integer $lost_status_changed_at
 * @property integer $requestnum номер запроса на поставку
 * @property integer $user_id кто создал
 * @property integer $order_user_id пользователь которому отправлен (или будет) товар
 * @property integer $order_id
 * @property integer $order_product_id
 * @property integer $order_product_price стоимость товара при продаже клиенту
 * @property boolean $is_security_code_checked был ли подтвержден код
 * @property boolean $is_return создано на основе возврата
 * @property integer $return_id номер возврата
 * @property integer $return_product_id номер товара возврата
 * @property integer $prev_warehouse_id предыдущий складской номер
 * @property integer $prev_order_id предыдущий номер заказа
 * @property integer $prev_order_product_id предыдущий номер товара
 * @property integer $is_supplier_informed поставщик извещен
 * @property-read $created_at
 * @property-read $updated_at
 * @property-read string $statusReplacement
 * @property-read SupplierRecord $supplier поставщик
 * @property-read EventProductRecord $eventProduct товар
 * @property-read EventRecord $event акция
 * @property-read ProductRecord $product базовая информация о товаре
 * @property-read OrderRecord $order к какому заказу относится (когда у заказа будет PROCESSING_CALLCENTER_CONFIRMED)
 * @property-read OrderProductRecord $orderProduct к какому товару из заказа относится (будет доступно только когда у заказа будет статус PROCESSING_CALLCENTER_CONFIRMED)
 * @property-read UserRecord $orderClient к какому клиенту относится (когда у заказа будет PROCESSING_CALLCENTER_CONFIRMED)
 * @property-read OrderReturnRecord|null $return
 * @property-read OrderReturnProductRecord|null $returnProduct
 * @property-read WarehouseProductRecord|null $prevWarehouse
 * @property-read OrderRecord|null $prevOrder
 * @property-read OrderProductRecord|null $prevOrderProduct
 * @property-read boolean $isOrderProductConnected ассоциирован ли с данным складским товаром товар из корзины
 * @mixin Timestampable
 */
class WarehouseProductRecord extends DoctrineActiveRecord
{
    use ApplicationTrait;

    private $_isCurrentStatusesResolved = false;
    private $_currentStatus = null;
    private $_currentStatusComment = null;
    private $_currentLostStatus = null;
    private $_currentLostStatusComment = null;

    const STATUS_UNMODERATED = 0; // практически тоже самое что виртуальное
    const STATUS_IN_WAREHOUSE_RESERVED = 1; // на складе
    const STATUS_MAILED_TO_CLIENT = 2; // отправлено заказчику
    const STATUS_READY_SHIPPING_TO_SUPPLIER = 3; //готов к отправке поставщику
    const STATUS_MAILED_TO_SUPPLIER = 4; // отправлено поставщику
    const STATUS_BROKEN = 6; // испорчено (далее нужно выставить статус причины)
    const STATUS_WAREHOUSE_STOCK = 7;
    const STATUS_WAREHOUSE_STOCK_RESERVED = 8;

    const LOST_STATUS_CORRECT = 0; // все ок
    const LOST_STATUS_LOST = 1; // утеряно
    const LOST_STATUS_BROKEN = 2; // испорчено
    const LOST_STATUS_DEFECTIVE = 4; // брак
    const LOST_STATUS_SOLD = 5; // продано в обход системы

    /**
     * @param string $className
     *
     * @return WarehouseProductRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['packaging_security_code', 'getPackagingSecurityCode', 'setPackagingSecurityCode', 'packagingSecurityCode'];
        yield ['event_product_sizeformat', 'getEventProductSizeformat', 'setEventProductSizeformat', 'eventProductSizeformat'];
        yield ['event_product_size', 'getEventProductSize', 'setEventProductSize', 'eventProductSize'];
        yield ['event_product_price', 'getEventProductPrice', 'setEventProductPrice', 'eventProductPrice'];
        yield ['order_product_price', 'getOrderProductPrice', 'setOrderProductPrice', 'orderProductPrice'];
        yield ['is_security_code_checked', 'isSecurityCodeChecked', 'setIsSecurityCodeChecked', 'isSecurityCodeChecked'];
        yield ['is_return', 'isReturn', 'setIsReturn', 'isReturn'];
        yield ['is_supplier_informed', 'isSupplierInformed', 'setIsSupplierInformed', 'isSupplierInformed'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_comment', 'getStatusComment', 'setStatusComment', 'statusComment'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['status_history_json', 'getStatusHistoryJson', 'setStatusHistoryJson', 'statusHistoryJson'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['lost_status', 'getLostStatus', 'setLostStatus', 'lostStatus'];
        yield ['lost_status_comment', 'getLostStatusComment', 'setLostStatusComment', 'lostStatusComment'];
        yield ['lost_status_prev', 'getLostStatusPrev', 'setLostStatusPrev', 'lostStatusPrev'];
        yield ['lost_status_history_json', 'getLostStatusHistoryJson', 'setLostStatusHistoryJson', 'lostStatusHistoryJson'];
        yield ['lost_status_changed_at', 'getLostStatusChangedAt', 'setLostStatusChangedAt', 'lostStatusChangedAt'];
        yield ['requestnum', 'getRequestnum', 'setRequestnum', 'requestnum'];
        yield ['request_fileid', 'getRequestFileid', 'setRequestFileid', 'requestFileid'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['event_product_id', 'getEventProductId', 'setEventProductId', 'eventProductId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
        yield ['order_user_id', 'getOrderUserId', 'setOrderUserId', 'orderUserId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['order_product_id', 'getOrderProductId', 'setOrderProductId', 'orderProductId'];
        yield ['return_id', 'getReturnId', 'setReturnId', 'returnId'];
        yield ['return_product_id', 'getReturnProductId', 'setReturnProductId', 'returnProductId'];
        yield ['prev_warehouse_id', 'getPrevWarehouseId', 'setPrevWarehouseId', 'prevWarehouseId'];
        yield ['prev_order_id', 'getPrevOrderId', 'setPrevOrderId', 'prevOrderId'];
        yield ['prev_order_product_id', 'getPrevOrderProductId', 'setPrevOrderProductId', 'prevOrderProductId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'warehouse_products';
    }

    public function relations()
    {
        return [
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
            'eventProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventProductRecord', 'event_product_id'],
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'product' => [self::BELONGS_TO, 'MommyCom\Model\Db\ProductRecord', 'product_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],

            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'orderProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderProductRecord', 'order_product_id'],
            'orderClient' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'order_user_id'],

            'return' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderReturnRecord', 'return_id'],
            'returnProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderReturnProductRecord', 'return_product_id'],
            'prevWarehouse' => [self::BELONGS_TO, 'MommyCom\Model\Db\WarehouseProductRecord', 'prev_warehouse_id'],
            'prevOrder' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'prev_order_id'],
            'prevOrderProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderProductRecord', 'prev_order_product_id'],
        ];
    }

    public function rules()
    {
        return [
            ['event_product_id, event_id, product_id', 'required'],
            ['event_product_id, event_id, product_id', 'numerical', 'integerOnly' => true, 'min' => 0],
            ['status', 'in', 'range' => [
                self::STATUS_UNMODERATED,
                self::STATUS_IN_WAREHOUSE_RESERVED,
                self::STATUS_MAILED_TO_CLIENT,
                self::STATUS_READY_SHIPPING_TO_SUPPLIER,
                self::STATUS_MAILED_TO_SUPPLIER,
                self::STATUS_BROKEN,
                self::STATUS_WAREHOUSE_STOCK,
                self::STATUS_WAREHOUSE_STOCK_RESERVED
            ]],

            ['lost_status', 'in', 'range' => [
                self::LOST_STATUS_CORRECT,
                self::LOST_STATUS_LOST,
                self::LOST_STATUS_BROKEN,
                self::LOST_STATUS_DEFECTIVE,
                self::LOST_STATUS_SOLD,
            ]],

            ['status', 'validatorStatus'],
            ['lost_status', 'validatorLostStatus'],

            ['user_id, order_id, order_product_id, order_user_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],
            ['event_product_id', 'exist', 'className' => 'MommyCom\Model\Db\EventProductRecord', 'attributeName' => 'id'],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['product_id', 'exist', 'className' => 'MommyCom\Model\Db\ProductRecord', 'attributeName' => 'id'],

            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['order_product_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderProductRecord', 'attributeName' => 'id'],
            ['order_user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
            ['order_product_id', 'validatorOrderProduct'],

            ['is_supplier_informed', 'boolean'],

            [
                'id, event_product_id, event_id, user_id, product_id, order_id, order_product_id, order_user_id' .
                ', status, lost_status, is_return, return_id, return_product_id, prev_order_id, prev_warehouse_id, is_supplier_informed' .
                ', created_at, updated_at',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function validatorOrderProduct($attribute)
    {
        $value = $this->$attribute;

        if (empty($value)) {
            return;
        }

        $orderProduct = OrderProductRecord::model()->findByPk($value);
        /* @var $orderProduct OrderProductRecord */

        $eventProduct = $orderProduct->product;
        $product = $eventProduct->product;

        $isSame = $this->product_id == $product->id;
        $isSame = $isSame && $this->event_product_sizeformat == $eventProduct->sizeformat;
        $isSame = $isSame && $this->event_product_size == $eventProduct->size;

        if (!$isSame) {
            $this->addError($attribute, \Yii::t('backend', 'Can not be associated with this product'));
        }
    }

    public function validatorStatus($attribute)
    {
        if ($this->isNewRecord) {
            return; // разрешается любой статус когда запись является новой
        }

        $value = $this->$attribute;
        $currentStatus = $this->_currentStatus;

        if ($currentStatus != $this->status) {
            $chains = self::statusChains();
            if (!isset($chains[$currentStatus]) || !in_array($value, $chains[$currentStatus])) {
                $this->addError($attribute, \Yii::t('backend', 'Can not be changed to the specified status'));
            }
        }
    }

    public function validatorLostStatus($attribute)
    {
        $value = $this->$attribute;
        if ($value != self::LOST_STATUS_CORRECT && $this->status != self::STATUS_BROKEN) {
            $this->addError($attribute, \Yii::t('backend', 'You can set the type of write-off only after you set the status in "Canceled"'));
        } elseif ($value == self::LOST_STATUS_CORRECT && $this->status == self::STATUS_BROKEN) {
            $this->addError($attribute, \Yii::t('backend', 'Enter the reason for wirte-off'));
        }
    }

    /**
     * Сохраняет текущий статус
     */
    protected function _resolveCurrentStatuses()
    {
        $this->_isCurrentStatusesResolved = true;
        $this->_currentStatus = $this->status;
        $this->_currentStatusComment = $this->status_comment;
        $this->_currentLostStatus = $this->lost_status;
        $this->_currentLostStatusComment = $this->lost_status_comment;
    }

    /**
     * Обновляет количество поставленного товара у ProductEventRecord
     */
    protected function _updateEventProduct()
    {
        $count = static::model()
            ->eventId($this->event_id)
            ->eventProductId($this->event_product_id)
            ->productId($this->product_id)
            ->lostStatus(self::LOST_STATUS_CORRECT)
            ->count();

        $product = $this->eventProduct;

        if ($product !== null && $product->number_arrived != $count) {
            $product->number_arrived = $count;
            $product->save(true, ['number_arrived']);
        }
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateStatusChangeStack()
    {
        $statusHistory = $this->getStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->status) {
            $now = time();

            $addToHistory = [
                'status' => $this->status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
            $this->status_prev = $prevStatus['status'];

            $this->status_changed_at = $now;

            $statusRec = new WarehouseProductStatusHistoryRecord();
            $statusRec->product_id = $this->id;
            $statusRec->status = $this->status;
            $statusRec->status_prev = $this->status_prev;
            $statusRec->comment = $this->status_comment;
            $statusRec->client_id = $this->order_user_id;
            $statusRec->save();
        }
        ksort($statusHistory);

        $this->status_history_json = json_encode($statusHistory);
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateLostStatusChangeStack()
    {
        $statusHistory = $this->getLostStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->lost_status) {
            $now = time();

            $addToHistory = [
                'status' => $this->lost_status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
            $this->lost_status_prev = $prevStatus['status'];

            $this->lost_status_changed_at = $now;

            $lostStatusRec = new WarehouseProductLostStatusHistoryRecord();
            $lostStatusRec->product_id = $this->id;
            $lostStatusRec->status = $this->lost_status;
            $lostStatusRec->status_prev = $this->status_prev;
            $lostStatusRec->comment = $this->lost_status_comment;
            $lostStatusRec->client_id = $this->order_user_id;
            $lostStatusRec->save();
        }
        ksort($statusHistory);

        $this->lost_status_history_json = json_encode($statusHistory);
    }

    protected function _resolveOrderAdditionalInfo()
    {
        $orderProduct = $this->getRelated('orderProduct', true);
        /* @var $orderProduct OrderProductRecord|null */
        if ($orderProduct === null) { // товар не присоединен к заказу
            $this->order_id = 0;
            $this->order_product_id = 0;
            $this->order_user_id = 0;
            $this->is_security_code_checked = false; // если товар не подключен к заказу то и подтвержденным он не может

            // видимо отконектили от заказа, потому товар теперь на складе
            if ($this->status == WarehouseProductRecord::STATUS_MAILED_TO_CLIENT) {
                $this->status = WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED;
            }
        } else {
            $this->order_id = $orderProduct->order_id;
            $this->order_user_id = $orderProduct->user_id;
            $this->order_product_price = $orderProduct->price;

            // после определенного статуса заказа товар переходит в статус отправленных
            if ($this->status == WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED) {
                if ($orderProduct->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED &&
                    $orderProduct->storekeeper_status == OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED) {
                    $this->status = WarehouseProductRecord::STATUS_MAILED_TO_CLIENT;
                }
            } elseif ($this->status == WarehouseProductRecord::STATUS_MAILED_TO_CLIENT) {
                if ($orderProduct->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_ACCEPTED ||
                    $orderProduct->storekeeper_status != OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED) {
                    $this->status = WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED;
                }
            }
        }
    }

    protected function _resolveEventAdditionalInfo()
    {
        $eventProduct = $this->eventProduct;
        if ($eventProduct === null) {
            return;
        }

        $this->supplier_id = $eventProduct->supplier_id;
        $this->event_id = $eventProduct->event_id;
        $this->event_product_sizeformat = $eventProduct->sizeformat;
        $this->event_product_size = $eventProduct->size;
        $this->event_product_price = $eventProduct->price;
        $this->product_id = $eventProduct->product_id;

        $this->event_product_price = $eventProduct->price_purchase;
    }

    protected function _resolveReturnAdditionalInfo()
    {
        if ($this->return_product_id == 0) { // это не возврат, обнуляем
            $this->is_return = false;
            $this->return_id = 0;
            $this->prev_order_id = 0;
            $this->prev_order_product_id = 0;
            $this->prev_warehouse_id = 0;
            return;
        }

        $product = $this->returnProduct;

        if ($product === null) {
            return;
        }

        $this->is_return = true;
        $this->return_id = $product->return_id;
        $this->prev_order_id = $product->order_id;
        $this->prev_order_product_id = $product->order_product_id;
        $this->prev_warehouse_id = $product->warehouse_id;
    }

    protected function _resolveAdminUser()
    {
        if ($this->isNewRecord && $this->user_id == 0) {
            try { // for console app
                $this->user_id = \Yii::app()->user->id;
            } catch (CException $e) {
            }
        }
    }

    protected function _resolvePackagingSecurityCode()
    {
        if (!$this->isNewRecord) {
            return;
        }

        $minimalSecurityCodeArray = $this->app()->db
            ->createCommand()
            ->select('MIN(packaging_security_code)')
            ->from('warehouse_products')
            ->where('status <> :status', [':status' => 1])
            ->queryAll();
        $minimalSecurityCode = current(current($minimalSecurityCodeArray));

        if (null === $minimalSecurityCode) {
            $minimalSecurityCodeArray = $this->app()->db
                ->createCommand()
                ->select('MAX(packaging_security_code)')
                ->from('warehouse_products')
                ->where('status <> :status', [':status' => 0])
                ->queryAll();
            $minimalSecurityCode = (int)current(current($minimalSecurityCodeArray)) + 1;
        }

        $this->packaging_security_code = $minimalSecurityCode;
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolveOrderAdditionalInfo();
        $this->_resolveEventAdditionalInfo();
        $this->_resolveReturnAdditionalInfo();
        $this->_resolveAdminUser();
        $this->_resolvePackagingSecurityCode();

        $this->_updateStatusChangeStack();
        $this->_updateLostStatusChangeStack();

        return $result;
    }

    public function afterSave()
    {
        parent::afterSave();

        $this->_updateEventProduct();
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $this->_updateEventProduct();
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->_resolveCurrentStatuses();
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'supplier_id' => \Yii::t($category, '№ of supplier'),
            'event_product_id' => \Yii::t($category, '№ product in flash-sale'),
            'event_id' => \Yii::t($category, '№ Flash-sale'),
            'product_id' => \Yii::t($category, '№ product'),
            'order_id' => \Yii::t($category, '№ order'),

            'status' => \Yii::t($category, 'Status'),
            'status_comment' => \Yii::t($category, 'Comment on the status'),

            'lost_status' => \Yii::t($category, 'Dispose'),
            'lost_status_comment' => \Yii::t($category, 'Reason for write-off'),

            'is_return' => \Yii::t($category, 'Return'),
            'return_id' => \Yii::t($category, '№ of return'),
            'prev_order_id' => \Yii::t($category, '№ of previous order'),
            'prev_warehouse_id' => \Yii::t($category, '№ of previous warehouse'),
            'is_supplier_informed' => \Yii::t($category, 'Supplier is notified'),

            'packaging_security_code' => \Yii::t($category, 'Verification code'),
            'is_security_code_checked' => \Yii::t($category, 'Verified code'),

            'supplier.name' => \Yii::t($category, 'Supplier'),
            'eventProduct.name' => \Yii::t($category, 'Order'),
            'event.name' => \Yii::t($category, 'Special offer'),
            'product.name' => \Yii::t($category, 'Order'),

            'user.fullname' => \Yii::t($category, 'User'),

            'event_product_price' => \Yii::t($category, 'Order Total'),
            'order_product_price' => \Yii::t($category, 'Cost of sale'),

            'statusReplacement' => \Yii::t($category, 'Status'),
            'lostStatusReplacement' => \Yii::t($category, 'Dispose'),

            'created_at' => \Yii::t($category, 'Added'),
            'updated_at' => \Yii::t($category, 'Updated'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * Только тот товар который был продан как сток
     *
     * @param integer $eventId в какой акции он на самом деле был продан
     *
     * @return static
     */
    public function onlySoldAsStock($eventId)
    {
        $alias = $this->getTableAlias();
        $this->eventIdNot($eventId);

        $aliasPrefix = '__onlySoldAsStock_';
        $orderProductAlias = $aliasPrefix . 'orderProduct';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($orderProductAlias, $relations['orderProduct']);

        $this->with([
            $orderProductAlias => [
                'together' => true,
                'joinType' => 'INNER JOIN',
            ],
        ]);

        $criteria = $this->getDbCriteria();
        $criteria->addCondition($alias . '.' . 'event_id' . '<>' . $orderProductAlias . '.' . 'event_id');
        $criteria->addColumnCondition([
            $orderProductAlias . '.' . 'event_id' => $eventId,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function lostStatus($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'lost_status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function lostStatusIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'supplier_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function productId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function productIdIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.product_id', $values);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventIdNot($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'event_id' . '<>' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function eventProductSizeformat($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_product_sizeformat' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function eventProductSize($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_product_size' => $value,
        ]);
        return $this;
    }

    /**
     * @param float|int $value
     *
     * @return $this
     */
    public function eventProductPriceFrom($value)
    {
        $value = Cast::toFloat($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.event_product_price >=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param float|int $value
     *
     * @return $this
     */
    public function eventProductPriceTo($value)
    {
        $value = Cast::toFloat($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.event_product_price <' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function requestNum($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'requestnum' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function securityCodeChecked(bool $value)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_security_code_checked' => $value,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderUserId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function isReturn($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_return' => Cast::toUInt($value),
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function returnId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'return_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function returnProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'return_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function prevOrderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'prev_order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function prevOrderProductId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'prev_order_product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function prevWarehouseId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'prev_warehouse_id' => $value,
        ]);
        return $this;
    }

    /**
     * Только те складские товары которые УЖЕ связанны с какой-либо покупкой
     *
     * @return static
     */
    public function onlyHasOrderConnected()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.order_product_id > 0');
        return $this;
    }

    /**
     * Только те складские товары которые ЕЩЕ связанны с какой-либо покупкой
     *
     * @return static
     */
    public function onlyNotHasOrderConnected()
    {
        $this->orderProductId(0);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        //    array('id, event_product_id, event_id, product_id, status, lost_status', 'safe', 'on' => 'searchPrivileged')

        $criteria->compare($alias . '.id', '=' . Utf8::ltrim($model->id, '0'));
        $criteria->compare($alias . '.event_product_id', '=' . $model->event_product_id);

        $criteria->compare($alias . '.event_id', '=' . $model->event_id);
        $criteria->compare($alias . '.product_id', '=' . $model->product_id);

        $criteria->compare($alias . '.order_id', '=' . $model->order_id);
        $criteria->compare($alias . '.order_product_id', '=' . $model->order_product_id);
        $criteria->compare($alias . '.order_user_id', '=' . $model->order_user_id);

        $criteria->compare($alias . '.status', '=' . $model->status);
        $criteria->compare($alias . '.lost_status', '=' . $model->lost_status);
        $criteria->compare($alias . '.is_return', '=' . $model->is_return);
        $criteria->compare($alias . '.return_id', '=' . $model->return_id);
        $criteria->compare($alias . '.return_product_id', '=' . $model->return_product_id);
        $criteria->compare($alias . '.prev_order_id', '=' . $model->prev_order_id);
        $criteria->compare($alias . '.prev_warehouse_id', '=' . $model->prev_warehouse_id);
        $criteria->compare($alias . '.is_supplier_informed', $model->is_supplier_informed);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    // item api

    /**
     * Устанавливает принадлежность товара на складе к заказу
     *
     * @param OrderProductRecord $product
     *
     * @throws InvalidArgumentException
     * @return static
     */
    public function bindProduct(OrderProductRecord $product)
    {
        $eventProduct = $product->product;

        if ($eventProduct == null) {
            return $this;
        }

        if ($this->order_product_id != $product->id) {
            $this->is_security_code_checked = false; // сбрасываем состояние проверки кода
        }

        $this->order_id = $product->order_id;
        $this->order_product_id = $product->id;
        $this->order_user_id = $product->user_id;
        return $this;
    }

    /**
     * Очищает связку с заказом
     *
     * @param OrderProductRecord|null $product
     *
     * @return $this
     */
    public function unbindProduct(OrderProductRecord $product = null)
    {
        if ($product !== null && $product->id == $this->order_product_id) {
            $this->order_id = 0;
            $this->order_product_id = 0;
            $this->order_user_id = 0;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getPackagingSecurityCode()
    {
        return $this->packaging_security_code;
    }

    /**
     * Проверка кода
     *
     * @param integer $code
     *
     * @return bool
     */
    public function compareSecurityCode($code)
    {
        return strcmp($this->getPackagingSecurityCode(), $code) == 0;
    }

    /**
     * @return $this
     */
    public function forceResolveOrderState()
    {
        $this->_resolveOrderAdditionalInfo();
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsOrderProductConnected()
    {
        return $this->order_product_id > 0;
    }

    /**
     * @return File|null
     */
    public function getRequestFile()
    {
        return \Yii::app()->filestorage->get($this->request_fileid);
    }

    /**
     * @return array
     */
    public function getStatusHistory()
    {
        return Cast::toArr(json_decode($this->status_history_json, true, 3));
    }

    /**
     * @param integer $status
     * @param bool $last
     *
     * @return int|false
     */

    public function getChangeStatusFromHistory($status, $last = false)
    {
        $timestamp = false;
        $history = $this->getStatusHistory();

        if ($last) {
            krsort($history);
        }

        foreach ($history as $time => $item) {
            if ($status == $item['status']) {
                $timestamp = $time;
                break;
            }
        }

        return $timestamp;
    }

    /**
     * @return array
     */
    public function getLostStatusHistory()
    {
        return Cast::toArr(json_decode($this->lost_status_history_json, true, 3));
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getLostStatusReplacement()
    {
        $replacements = self::lostStatusReplacements();
        return isset($replacements[$this->lost_status]) ? $replacements[$this->lost_status] : '';
    }

    /**
     * @return array
     */
    public function getNextStatuses()
    {
        $chains = self::statusChains();
        return isset($chains[$this->status]) ? $chains[$this->status] : [];
    }

    /**
     * Проверяет возможность смены статуса с текущего на указанный
     *
     * @param integer $newStatus
     *
     * @return boolean
     */
    public function getIsPossibleChangeStatusTo($newStatus)
    {
        $nextStatuses = $this->getNextStatuses();
        return isset($nextStatuses[$newStatus]);
    }

    /**
     * Товар был доставлен поставщиков
     *
     * @return bool
     */
    public function getIsSupplierDelivered()
    {
        return $this->status != self::STATUS_UNMODERATED;
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_UNMODERATED => \Yii::t($category, 'not processed'),
            self::STATUS_IN_WAREHOUSE_RESERVED => \Yii::t($category, 'on warehouse'),
            self::STATUS_MAILED_TO_CLIENT => \Yii::t($category, 'sent to the client'),
            self::STATUS_READY_SHIPPING_TO_SUPPLIER => \Yii::t($category, 'ready to be sent to the supplier'),
            self::STATUS_MAILED_TO_SUPPLIER => \Yii::t($category, 'sent to the supplier'),
            self::STATUS_BROKEN => \Yii::t($category, 'Dispose'),
            self::STATUS_WAREHOUSE_STOCK => Translator::t('In stock (not reserved)'),
            self::STATUS_WAREHOUSE_STOCK_RESERVED => Translator::t('In stock (reserved)'),
        ];
    }

    /**
     * @return array
     */
    public static function lostStatusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::LOST_STATUS_CORRECT => \Yii::t($category, 'no'),
            self::LOST_STATUS_LOST => \Yii::t($category, 'lost'),
            self::LOST_STATUS_BROKEN => \Yii::t($category, 'broken'),
            self::LOST_STATUS_DEFECTIVE => \Yii::t($category, 'defective'),
            self::LOST_STATUS_SOLD => \Yii::t($category, 'sold bypass system'),
        ];
    }

    /**
     * Цепочки смены статуса формат статус => array(на, какие, статусы, можно, сменить)
     *
     * @return array
     */
    public static function statusChains()
    {
        //const STATUS_UNMODERATED = 0; // практически тоже самое что виртуальное
        //const STATUS_IN_WAREHOUSE = 1; // на складе
        //const STATUS_MAILED_TO_CLIENT = 2; // отправлено заказчику
        //const STATUS_IN_TRANSFER = 3; // currently not used, перемещается между складами
        //const STATUS_MAILED_TO_SUPPLIER = 4; // отправлено поставщику
        //const STATUS_PHOTO_SESSION = 5; // отправлено на фотосессию
        //const STATUS_BROKEN = 6; // испорчено (далее нужно выставить статус причины)

        return [
            self::STATUS_UNMODERATED => [
                self::STATUS_IN_WAREHOUSE_RESERVED,
                self::STATUS_BROKEN,
            ],
            self::STATUS_IN_WAREHOUSE_RESERVED => [
                self::STATUS_UNMODERATED,
                self::STATUS_MAILED_TO_CLIENT,
                self::STATUS_READY_SHIPPING_TO_SUPPLIER,
                self::STATUS_BROKEN,
                self::STATUS_WAREHOUSE_STOCK
            ],
            self::STATUS_MAILED_TO_CLIENT => [
                self::STATUS_IN_WAREHOUSE_RESERVED,
                self::STATUS_BROKEN,
            ],
            self::STATUS_BROKEN => [
                self::STATUS_IN_WAREHOUSE_RESERVED,
                self::STATUS_READY_SHIPPING_TO_SUPPLIER,
            ],
            self::STATUS_READY_SHIPPING_TO_SUPPLIER => [
                self::STATUS_IN_WAREHOUSE_RESERVED,
                self::STATUS_BROKEN,
                self::STATUS_MAILED_TO_SUPPLIER,
            ],
            self::STATUS_WAREHOUSE_STOCK => [
                self::STATUS_WAREHOUSE_STOCK_RESERVED
            ],
            //еще неизвестно как это будет
            self::STATUS_MAILED_TO_SUPPLIER => [
//                self::STATUS_IN_WAREHOUSE,
//                self::STATUS_BROKEN,
            ],
        ];
    }
}
