<?php

namespace MommyCom\Model\Db;

use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class HasBankDataRecord
 * @package MommyCom\Model\Db
 */
abstract class HasBankDataRecord extends DoctrineActiveRecord
{
    /**
     * @var string
     */
    public const ENCRYPT_METHOD = 'AES-256-CBC';

    /**
     * @param $value
     *
     * @return string
     */
    protected static function _bankDataEncode($value): string
    {
        if ($value == '') {
            return '';
        }

        $key = static::_bankDataPrivateKey();
        $value = trim(Cast::toStr($value));
        $ivSize = openssl_cipher_iv_length(self::ENCRYPT_METHOD);
        $iv = openssl_random_pseudo_bytes($ivSize);
        $encrypted = openssl_encrypt($value, self::ENCRYPT_METHOD, $key, OPENSSL_RAW_DATA, $iv);
        $encrypted = base64_encode($iv . $encrypted);

        return $encrypted;
    }

    /**
     * @param $value
     *
     * @return string
     */
    protected static function _bankDataDecode($value): string
    {
        if ($value == '') {
            return '';
        }

        $key = static::_bankDataPrivateKey();
        $encrypted = base64_decode($value);
        $ivSize = openssl_cipher_iv_length(self::ENCRYPT_METHOD);
        $iv = substr($encrypted, 0, $ivSize);
        $encrypted = substr($encrypted, $ivSize);
        $encrypted = openssl_decrypt($encrypted, self::ENCRYPT_METHOD, $key, OPENSSL_RAW_DATA, $iv);
        $encrypted = rtrim($encrypted, chr(0));

        return $encrypted;
    }

    /**
     * @return string
     */
    abstract protected static function _bankDataPrivateKey(): string;

    /**
     * @return string
     */
    abstract public function getBankGiro(): string;

    /**
     * @param mixed $value
     */
    abstract public function setBankGiro($value);
}
