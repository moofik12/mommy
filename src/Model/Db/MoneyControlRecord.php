<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Service\Deprecated\Delivery\DeliveryCountryGroups;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * class MoneyControlRecord
 *
 * @property-read int $id
 * @property int $status
 * @property int $delivery_type
 * @property string $trackcode
 * @property int $order_id
 * @property float $order_price
 * @property-read int $updated_at
 * @property-read int $created_at
 */
class MoneyControlRecord extends DoctrineActiveRecord
{
    const STATUS_WAITING_CONFIRMATION = 0, //не подтвержден
        STATUS_CONFIRMATION = 1, //подтверждена полата (деньги получены)
        STATUS_ERROR_TRACKCODE = 2; //ошибка в получении обратной ТТН

    /**
     * @param string $className
     *
     * @return MoneyControlRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['delivery_type', 'getDeliveryType', 'setDeliveryType', 'deliveryType'];
        yield ['trackcode', 'getTrackcode', 'setTrackcode', 'trackcode'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['order_price', 'getOrderPrice', 'setOrderPrice', 'orderPrice'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    public function tableName()
    {
        return 'money_control';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['order_id', 'unique'],
            ['order_id, order_price', 'required'],
            ['order_price', 'filter', 'filter' => [Cast::class, 'toUFloat']],
            ['order_price', 'numerical', 'min' => 0, 'max' => 1000000000], // million is enough
            ['delivery_type', 'in', 'range' => DeliveryCountryGroups::instance()->getDelivery()->getList(true)],
            ['status', 'in', 'range' => [
                self::STATUS_WAITING_CONFIRMATION,
                self::STATUS_CONFIRMATION,
                self::STATUS_ERROR_TRACKCODE,
            ]],
            ['trackcode', 'length', 'max' => 45],

            ['trackcode, order_id, order_price', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'status' => Translator::t('Status'),
            'trackcode' => Translator::t('Tracking number'),
            'order_id' => Translator::t('Order'),
            'order_price' => Translator::t('Order total'),
            'delivery_type' => Translator::t('Delivery type'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.delivery_type', $model->delivery_type);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.order_price', $model->order_price, true);
        $criteria->compare($alias . '.trackcode', $model->trackcode, true);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function deliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.delivery_type' => $value,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function statusIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * magic error validator CRangeValidator for name statusIn
     *
     * @param $values
     *
     * @return $this
     */
    public function statusIsIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.status', $values
        );
        return $this;
    }

    /**
     * @param $values
     *
     * @return $this
     */
    public function orderIds($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.order_id', $values
        );
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function trackcode($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.trackcode' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function trackcodes($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.trackcode', $values
        );
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function updateAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.updated_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function updatedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.updated_at', ">= $value");

        return $this;
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function statusReplacement($default = '')
    {
        $replacements = self::statusReplacements();

        return isset($replacements[$this->status]) ? $replacements[$this->status] : $default;
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        return [
            self::STATUS_WAITING_CONFIRMATION => Translator::t('pending confirmation'),
            self::STATUS_CONFIRMATION => Translator::t('confirmed'),
            self::STATUS_ERROR_TRACKCODE => Translator::t('error obtaining return tracking number'),
        ];
    }
}
