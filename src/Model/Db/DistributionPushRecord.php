<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CHtml;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class DistributionPushRecord
 *
 * @property integer $id
 * @property integer $start_at
 * @property integer $status
 * @property string $subject
 * @property string $body
 * @property string $url
 * @property string $url_desktop
 * @property integer $created_at
 * @property integer $update_at
 */
class DistributionPushRecord extends DoctrineActiveRecord
{
    const STATUS_PENDING = 0, //в ожидании
        STATUS_START = 1, //рассылка началась
        STATUS_END = 2, //рассылка закончилась
        STATUS_ERROR = 3; //рассылка завершилась неудачей

    /**
     * @param string $className
     *
     * @return DistributionPushRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['subject', 'getSubject', 'setSubject', 'subject'];
        yield ['body', 'getBody', 'setBody', 'body'];
        yield ['url', 'getUrl', 'setUrl', 'url'];
        yield ['url_desktop', 'getUrlDesktop', 'setUrlDesktop', 'urlDesktop'];
        yield ['start_at', 'getStartAt', 'setStartAt', 'startAt'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    public function tableName()
    {
        return 'distributions_push';
    }

    public function rules()
    {
        return [
            ['status', 'in', 'range' => [
                self::STATUS_PENDING,
                self::STATUS_START,
                self::STATUS_END,
            ]],
            ['start_at', 'numerical', 'min' => 0, 'integerOnly' => true],
            ['subject, body, url, url_desktop', 'length', 'max' => 255, 'encoding' => false],

            ['id, status, subject, url', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'start_at' => Translator::t('Date of dispatch'),
            'status' => Translator::t('Status'),
            'subject' => Translator::t('Title'),
            'body' => Translator::t('Description'),
            'url' => Translator::t('Link for applications'),
            'url_desktop' => Translator::t('Link for browsers'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @return array
     */
    public function statusReplacements()
    {
        return [
            self::STATUS_PENDING => Translator::t('Pending'),
            self::STATUS_START => Translator::t('Started'),
            self::STATUS_END => Translator::t('Finished'),
        ];
    }

    /**
     * @return mixed
     */
    public function statusReplacement()
    {
        return CHtml::value($this->statusReplacements(), $this->status);
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        /* @var $model self */
        $model = $provider->model;
        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.subject', $model->subject, true);
        $criteria->compare($alias . '.body', $model->body, true);
        $criteria->compare($alias . '.url', $model->url, true);

        return $provider;
    }

    /**
     * @return $this
     */
    public function mustBeSigned()
    {
        $time = time();

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.status' . '=' . self::STATUS_PENDING);
        $this->getDbCriteria()->addCondition($alias . '.start_at' . '<=' . Cast::toStr($time));
        return $this;
    }

    /**
     * @return boolean
     */
    public function setStartMailing()
    {
        if (!($this->status == self::STATUS_PENDING)) {
            return false;
        }

        $this->status = self::STATUS_START;
        return $this->save();
    }

}
