<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class MoneyReceiveStatementOrderRecord
 *
 * @property-read int $id
 * @property int $statement_id
 * @property int $order_id
 * @property float $order_price
 * @property string $trackcode
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property-read MoneyReceiveStatementRecord $statement
 * @mixin Timestampable
 */
class MoneyReceiveStatementOrderRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return MoneyReceiveStatementOrderRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['trackcode', 'getTrackcode', 'setTrackcode', 'trackcode'];
        yield ['order_price', 'getOrderPrice', 'setOrderPrice', 'orderPrice'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['statement_id', 'getStatementId', 'setStatementId', 'statementId'];
    }

    public function tableName()
    {
        return 'money_receive_statements_orders';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'statement' => [self::BELONGS_TO, 'MommyCom\Model\Db\MoneyReceiveStatementRecord', 'statement_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['statement_id, order_id, order_price', 'required'],
            ['order_id', 'unique'],
            ['order_price', 'filter', 'filter' => [Cast::class, 'toUFloat']],
            ['order_price', 'numerical', 'min' => 0, 'max' => 1000000000], // million is enough
            ['trackcode', 'length', 'max' => 45],

            ['statement_id, order_id, order_price, trackcode', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_updateMoneyControl();

        return $result;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        //update link
        MoneyControlRecord::model()->updateAll(['status' => MoneyControlRecord::STATUS_WAITING_CONFIRMATION], [
            'condition' => "order_id={$this->order_id}",
        ]);
    }

    protected function _updateMoneyControl()
    {
        $moneyControls = MoneyControlRecord::model()->orderId($this->order_id)->findAll();

        foreach ($moneyControls as $moneyControl) {
            if ($moneyControl->status != MoneyControlRecord::STATUS_CONFIRMATION) {
                $moneyControl->status = MoneyControlRecord::STATUS_CONFIRMATION;

                $moneyControl->save();
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'statement_id' => Translator::t('Act'),
            'order_id' => Translator::t('Order'),
            'order_price' => Translator::t('Order total'),
            'trackcode' => Translator::t('Tracking number'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.statement_id', $model->statement_id);
        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.order_price', $model->order_price, true);
        $criteria->compare($alias . '.trackcode', $model->trackcode, true);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'order_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function statementId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.statement_id' => $value,
        ]);
        return $this;
    }
}
