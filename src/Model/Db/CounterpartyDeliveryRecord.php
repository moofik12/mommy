<?php

namespace MommyCom\Model\Db;

use CDbCriteria;
use CSort;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Service\Deprecated\Delivery\DeliveryCountryGroups;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Управление доверенными лицами для отправки посылок
 *
 * @property integer $id
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property integer $delivery_type
 * @property integer $is_active есть возможность отправить от него
 * @property integer $is_enabled доступна возможность отправлять посылки на сервисе
 * @property string $synchronized_at
 * @property string $updated_at
 * @property string $created_at
 * @property-read string $fullName
 * @property-read CounterpartyDeliveryOrdersRecord[] $deliveryOrdersInfo
 * @property-read int $countDeliveryOrdersThisMonth
 * @property-read float $sumDeliveryOrdersThisMonth
 * @property-read float $sumDeliveryOrdersToPayThisMonth
 * @property-read int $countDeliveryOrdersPreviousMonth
 * @property-read float $sumDeliveryOrdersPreviousMonth
 * @property-read float $sumDeliveryOrdersToPayPreviousMonth
 * @mixin Timestampable
 */
class CounterpartyDeliveryRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className active record class name.
     *
     * @return CounterpartyDeliveryRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['firstName', 'getFirstName', 'setFirstName', 'firstName'];
        yield ['middleName', 'getMiddleName', 'setMiddleName', 'middleName'];
        yield ['lastName', 'getLastName', 'setLastName', 'lastName'];
        yield ['delivery_type', 'getDeliveryType', 'setDeliveryType', 'deliveryType'];
        yield ['is_active', 'isActive', 'setIsActive', 'isActive'];
        yield ['is_enabled', 'isEnabled', 'setIsEnabled', 'isEnabled'];
        yield ['synchronized_at', 'getSynchronizedAt', 'setSynchronizedAt', 'synchronizedAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'counterparties_delivery';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'deliveryOrdersInfo' => [self::HAS_MANY, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id'],
            'countDeliveryOrdersThisMonth' => [self::STAT, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id',
                'scopes' => [
                    'createdAtFrom' => strtotime('midnight first day of this month'),
                    'createdAtTo' => strtotime('midnight first day of next month'),
                ]],
            'sumDeliveryOrdersThisMonth' => [self::STAT, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id',
                'select' => 'SUM(t.amount)',
                'scopes' => [
                    'createdAtFrom' => strtotime('midnight first day of this month'),
                    'createdAtTo' => strtotime('midnight first day of next month'),
                ]],
            'sumDeliveryOrdersToPayThisMonth' => [self::STAT, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id',
                'select' => 'SUM(t.toPay)',
                'scopes' => [
                    'createdAtFrom' => strtotime('midnight first day of this month'),
                    'createdAtTo' => strtotime('midnight first day of next month'),
                ]],
            'countDeliveryOrdersPreviousMonth' => [self::STAT, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id',
                'scopes' => [
                    'createdAtFrom' => strtotime('midnight first day of previous month'),
                    'createdAtTo' => strtotime('midnight first day of this month'),
                ]],
            'sumDeliveryOrdersPreviousMonth' => [self::STAT, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id',
                'select' => 'SUM(t.amount)',
                'scopes' => [
                    'createdAtFrom' => strtotime('midnight first day of previous month'),
                    'createdAtTo' => strtotime('midnight first day of this month'),
                ]],
            'sumDeliveryOrdersToPayPreviousMonth' => [self::STAT, 'MommyCom\Model\Db\CounterpartyDeliveryOrdersRecord', 'counterparty_id',
                'select' => 'SUM(t.toPay)',
                'scopes' => [
                    'createdAtFrom' => strtotime('midnight first day of previous month'),
                    'createdAtTo' => strtotime('midnight first day of this month'),
                ]],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['firstName, middleName, lastName, delivery_type', 'required'],
            ['delivery_type', 'numerical', 'integerOnly' => true],
            ['delivery_type', 'in', 'range' => DeliveryCountryGroups::instance()->getDelivery()->getList(true)],

            ['is_active, is_enabled', 'boolean'],
            ['is_active, is_enabled', 'default', 'value' => 0],
            ['firstName, middleName, lastName', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['firstName, middleName, lastName', 'length', 'max' => 128],

            ['updated_at, created_at, synchronized_at', 'numerical', 'integerOnly' => true],

            ['id, firstName, middleName, lastName, delivery_type, is_active, is_enabled, synchronized_at, updated_at, created_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'firstName' => \Yii::t('common', 'Name'),
            'middleName' => \Yii::t('common', 'Middle name'),
            'lastName' => \Yii::t('common', 'Last name'),
            'fullName' => \Yii::t('common', 'Full Name'),
            'delivery_type' => \Yii::t('common', 'Type of delivery'),
            'is_active' => \Yii::t('common', 'Delivery included'),
            'is_enabled' => \Yii::t('common', 'Shipping service available'),
            'synchronized_at' => \Yii::t('common', 'Synced'),
            'updated_at' => \Yii::t('common', 'Updated on'),
            'created_at' => \Yii::t('common', 'Created'),

            //relation
            'countDeliveryOrdersThisMonth' => \Yii::t('common', 'Sent this month'),
            'sumDeliveryOrdersThisMonth' => \Yii::t('common', 'Total orders this month'),
            'sumDeliveryOrdersToPayThisMonth' => \Yii::t('common', 'Amount to be paid (cover) this month'),
            'countDeliveryOrdersPreviousMonth' => \Yii::t('common', 'Sent last month'),
            'sumDeliveryOrdersPreviousMonth' => \Yii::t('common', 'Total orders last month'),
            'sumDeliveryOrdersToPayPreviousMonth' => 'Amount to be paid (cash on delivery) last month',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var CounterpartyDeliveryRecord $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.firstName', $model->firstName, true);
        $criteria->compare($alias . '.middleName', $model->middleName, true);
        $criteria->compare($alias . '.lastName', $model->lastName, true);

        $criteria->compare($alias . '.delivery_type', $model->delivery_type);
        $criteria->compare($alias . '.is_active', $model->is_active);
        $criteria->compare($alias . '.is_enabled', $model->is_enabled);
        $criteria->compare($alias . '.updated_at', $model->updated_at);
        $criteria->compare($alias . '.created_at', $model->created_at);

        return $provider;
    }

    /**
     * @param string $lastName
     * @param string $firstName
     * @param string $middleName
     *
     * @return static
     */
    public function fullNameIs($lastName, $firstName, $middleName)
    {
        $firstName = Utf8::trim(Cast::toStr($firstName));
        $middleName = Utf8::trim(Cast::toStr($middleName));
        $lastName = Utf8::trim(Cast::toStr($lastName));

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            "CONCAT($alias.firstName, $alias.middleName, $alias.lastName)" => implode('', [$firstName, $middleName, $lastName]),
        ]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function fullNameLike($value)
    {
        $value = Cast::toStr($value);
        $alias = $this->getTableAlias();

        $criteria = new CDbCriteria();
        $criteria->addSearchCondition("$alias.firstName", $value, true, 'OR');
        $criteria->addSearchCondition("$alias.middleName", $value, true, 'OR');
        $criteria->addSearchCondition("$alias.lastName", $value, true, 'OR');

        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isActive($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_active' => Cast::toUInt($value),
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isEnabled($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_enabled' => Cast::toUInt($value),
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function deliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.delivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function deliveryTypes(array $value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.delivery_type', $value);

        return $this;
    }

    /**
     * @see DeliveryTypeUa types
     *
     * @param int $deliveryType
     * @param int $cache
     *
     * @return static|null
     */
    public static function getCounterpartyForDelivery($deliveryType, $cache = 5)
    {
        $counterparties = self::model()
            ->cache($cache)
            ->isActive(true)
            ->isEnabled(true);

        $tableNameCounterpartyOrder = CounterpartyDeliveryOrdersRecord::model()->tableName();
        $criteria = $counterparties->getDbCriteria();
        $criteria->select = ['t.id as counterpartyId', 'SUM(info.toPay) as allToPay'];
        $criteria->join = "LEFT JOIN $tableNameCounterpartyOrder as info ON t.id=info.counterparty_id AND info.created_at >="
            . strtotime('midnight first day of this month') . ' AND info.created_at <=' . strtotime('midnight first day of next month');
        $criteria->group = 't.id';
        $counterparties->orderBy('allToPay', CSort::SORT_ASC);;

        $row = $counterparties->getSqlCommand()->query()->read();

        if ($row === false) {
            return null;
        }

        return self::model()->findByPk($row['counterpartyId']);
    }

    /* API */
    /**
     * @return string
     */
    public function getFullName()
    {
        return implode(' ', [$this->lastName, $this->firstName, $this->middleName]);
    }
}
