<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class PayGatewayLogRecord
 *
 * @property-read integer $id
 * @property string $provider
 * @property integer $invoice_id
 * @property string $unique_payment ID платежа
 * @property float $amount
 * @property bool $is_custom создан вручную
 * @property integer $status (@see PayGatewayReceiveResult::RECEIVE_STATUS*)
 * @property string $message
 * @property string $user_id
 * @property string $user_ip
 * @property string $user_browser
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read UserRecord $user|null
 */
class PayGatewayLogRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return PayGatewayLogRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['provider', 'getProvider', 'setProvider', 'provider'];
        yield ['invoice_id', 'getInvoiceId', 'setInvoiceId', 'invoiceId'];
        yield ['unique_payment', 'getUniquePayment', 'setUniquePayment', 'uniquePayment'];
        yield ['is_custom', 'isCustom', 'setIsCustom', 'isCustom'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['message', 'getMessage', 'setMessage', 'message'];
        yield ['user_ip', 'getUserIp', 'setUserIp', 'userIp'];
        yield ['user_browser', 'getUserBrowser', 'setUserBrowser', 'userBrowser'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'pay_gateway_log';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    public function rules()
    {
        return [
            ['provider', 'type', 'type' => 'string'],
            ['amount, invoice_id, status, user_id', 'numerical', 'min' => 0],
            ['message', 'type', 'type' => 'string'],
            ['message, user_ip, user_browser', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['is_custom', 'boolean'],
            [
                'id, provider, invoice_category, invoice_id, amount, status, message, user_id, user_ip, user_browser, ' .
                'created_at, updated_at',
                'safe', 'on' => 'searchPrivileged',
            ],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'provider' => \Yii::t('common', 'Провайдер'),
            'invoice_id' => \Yii::t('common', 'Order'),
            'unique_payment' => \Yii::t('common', 'Payment ID'),
            'amount' => \Yii::t('common', 'Amount'),
            'status' => \Yii::t('common', 'Status'),
            'message' => \Yii::t('common', 'Description'),
            'user_id' => \Yii::t('common', 'User'),
            'user_ip' => \Yii::t('common', 'IP'),
            'user_browser' => \Yii::t('common', 'User-Agent'),
            'is_custom' => \Yii::t('common', 'Created manually'),

            'created_at' => \Yii::t('common', 'Created'),
            'updated_at' => \Yii::t('common', 'Updated'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.provider', $model->provider, true);
        $criteria->compare($alias . '.unique_payment', $model->unique_payment, true);
        $criteria->compare($alias . '.invoice_id', $model->invoice_id, true);
        $criteria->compare($alias . '.amount', $model->amount, true);
        $criteria->compare($alias . '.message', $model->message, true);

        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.user_ip', $model->user_ip, true);
        $criteria->compare($alias . '.user_browser', $model->user_browser, true);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.is_custom', $model->is_custom);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        if (!empty($model->target)) {
            $criteria->addCondition('FIND_IN_SET(' . $model->dbConnection->quoteValue($model->target) . ', ' . $alias . '.' . 'target' . ')');
        }

        return $provider;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function uniquePayment($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.unique_payment' => $value,
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function uniquePaymentLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.unique_payment', $value);

        return $this;
    }
} 
