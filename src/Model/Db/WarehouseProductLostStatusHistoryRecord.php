<?php

namespace MommyCom\Model\Db;

/**
 * Class WarehouseProductLostStatusHistoryRecord
 */
class WarehouseProductLostStatusHistoryRecord extends WarehouseProductStatusHistoryRecord
{
    /**
     * @param string $className
     *
     * @return WarehouseProductLostStatusHistoryRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'warehouse_products_lost_statushistory';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = WarehouseProductRecord::lostStatusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = WarehouseProductRecord::lostStatusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }
}
