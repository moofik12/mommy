<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbException;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use LogicException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class SupplierPaymentRecord
 *
 * @property-read integer $id
 * @property integer $status
 * @property integer $supplier_id
 * @property integer $contract_id
 * @property integer $act_id
 * @property integer $event_id
 * @property integer $event_end_at
 * @property integer $delivery_start_at
 * @property integer $delivery_end_at
 * @property integer $payment_after_at
 * @property integer $payment_at
 * @property integer $count_products_problem_delivery
 * @property float $request_amount
 * @property float $delivered_amount
 * @property float $delivered_amount_wrong_time сумма товаров которые были доставлены не вовремя
 * @property float $returned_amount
 * @property float $penalty_amount
 * @property float $unpaid_amount задолженность поставщика
 * @property float $unpaid_fixed_amount задолженность поставщика которая учтена при оплате
 * @property int $unpaid_in_payment_id
 * @property float $paid_amount
 * @property float $custom_penalty_amount
 * @property string $comment
 * @property int $order_table_loaded_at
 * @property int $is_order_positions_worked
 * @property string bank_name_enc Название банка <b>(зашифровано)</b>
 * @property string bank_giro_enc расчетный счет (20 цифр) <b>(зашифровано)</b>
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property-read string $statusReplacement
 * @property-read float $payAmount
 * @property-read string $bankName Название банка
 * @property-read string $bankGiro расчетный счет (20 цифр)
 * @property-read DateInterval $stayPayment
 * @property-read SupplierRecord $supplier
 * @property-read EventRecord $event
 * @property-read SupplierContractRecord $contract
 * @property-read WarehouseStatementReturnRecord[] $statementReturns
 * @property-read int $statementReturnsCount
 * @property-read int $statementReturnsPossibleCount
 * @property-read int $unpaidPaymentPossibleCount
 * @property-read SupplierPaymentRecord[] $includeUnpaidPayments в оплату включены задолженности
 * @property-read SupplierPaymentRecord $unpaidFixedPayment задолженность зафиксирована в оплате
 * @property-read int $includeUnpaidPaymentsCount
 * @property-read SupplierPaymentActRecord $act
 */
class SupplierPaymentRecord extends HasBankDataRecord
{
    const STATUS_NOT_ANALYZED = 0;
    const STATUS_ANALYSIS = 1;
    const STATUS_ANALYZED = 2;
    const STATUS_CONFIRMED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_WAIT_DELIVERY = 5;

    /**
     * @param string $className
     *
     * @return SupplierPaymentRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['event_end_at', 'getEventEndAt', 'setEventEndAt', 'eventEndAt'];
        yield ['delivery_start_at', 'getDeliveryStartAt', 'setDeliveryStartAt', 'deliveryStartAt'];
        yield ['delivery_end_at', 'getDeliveryEndAt', 'setDeliveryEndAt', 'deliveryEndAt'];
        yield ['payment_after_at', 'getPaymentAfterAt', 'setPaymentAfterAt', 'paymentAfterAt'];
        yield ['payment_at', 'getPaymentAt', 'setPaymentAt', 'paymentAt'];
        yield ['request_amount', 'getRequestAmount', 'setRequestAmount', 'requestAmount'];
        yield ['delivered_amount', 'getDeliveredAmount', 'setDeliveredAmount', 'deliveredAmount'];
        yield ['count_products_problem_delivery', 'getCountProductsProblemDelivery', 'setCountProductsProblemDelivery', 'countProductsProblemDelivery'];
        yield ['delivered_amount_wrong_time', 'getDeliveredAmountWrongTime', 'setDeliveredAmountWrongTime', 'deliveredAmountWrongTime'];
        yield ['returned_amount', 'getReturnedAmount', 'setReturnedAmount', 'returnedAmount'];
        yield ['penalty_amount', 'getPenaltyAmount', 'setPenaltyAmount', 'penaltyAmount'];
        yield ['unpaid_amount', 'getUnpaidAmount', 'setUnpaidAmount', 'unpaidAmount'];
        yield ['unpaid_fixed_amount', 'getUnpaidFixedAmount', 'setUnpaidFixedAmount', 'unpaidFixedAmount'];
        yield ['paid_amount', 'getPaidAmount', 'setPaidAmount', 'paidAmount'];
        yield ['custom_penalty_amount', 'getCustomPenaltyAmount', 'setCustomPenaltyAmount', 'customPenaltyAmount'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['is_order_positions_worked', 'isOrderPositionsWorked', 'setIsOrderPositionsWorked', 'isOrderPositionsWorked'];
        yield ['order_table_loaded_at', 'getOrderTableLoadedAt', 'setOrderTableLoadedAt', 'orderTableLoadedAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['act_id', 'getActId', 'setActId', 'actId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['contract_id', 'getContractId', 'setContractId', 'contractId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['unpaid_in_payment_id', 'getUnpaidInPaymentId', 'setUnpaidInPaymentId', 'unpaidInPaymentId'];
    }

    public function tableName()
    {
        return 'suppliers_payments';
    }

    public function relations()
    {
        return [
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'contract' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierContractRecord', 'contract_id'],
            'includeUnpaidPayments' => [self::HAS_MANY, __CLASS__, 'unpaid_in_payment_id'],
            'includeUnpaidPaymentsCount' => [self::STAT, __CLASS__, 'unpaid_in_payment_id'],
            'unpaidFixedPayment' => [self::BELONGS_TO, __CLASS__, 'unpaid_in_payment_id'],

            'statementReturns' => [self::HAS_MANY, 'MommyCom\Model\Db\WarehouseStatementReturnRecord', 'supplier_payment_id'],
            'statementReturnsCount' => [self::STAT, 'MommyCom\Model\Db\WarehouseStatementReturnRecord', 'supplier_payment_id'],
            'act' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierPaymentActRecord', 'act_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NOT_ANALYZED],
            ['status', 'in', 'range' => self::statuses(false)],
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],
            ['contract_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierContractRecord', 'attributeName' => 'id'],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],

            ['delivery_start_at, delivery_end_at, payment_after_at, event_end_at, payment_at', 'numerical', 'integerOnly' => true],
            ['request_amount, delivered_amount, penalty_amount, returned_amount, paid_amount, custom_penalty_amount, delivered_amount_wrong_time', 'numerical', 'min' => 0],

            ['count_products_problem_delivery', 'numerical', 'integerOnly' => true, 'min' => 0],

            ['comment', 'length', 'max' => 200],

            ['bankName', 'length', 'max' => 250],
            ['bankGiro', 'length', 'max' => 20],

            ['unpaid_in_payment_id, act_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым

            ['act_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierPaymentActRecord', 'attributeName' => 'id'],
            ['unpaid_amount, unpaid_fixed_amount, unpaid_fixed_amount', 'numerical', 'min' => 0],
            ['unpaid_in_payment_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierPaymentRecord', 'attributeName' => 'id'],

            ['order_table_loaded_at', 'numerical', 'integerOnly' => true, 'min' => 0],
            ['is_order_positions_worked', 'boolean'],

            //СЦЕНАРИИ
            //подтверждение
            ['bankName, bankGiro, paid_amount', 'required', 'on' => 'confirm'],
            ['paid_amount', 'numerical', 'min' => 1, 'on' => 'confirm'],
            //отмена
            ['unpaid_amount', 'required', 'on' => 'cancel'],
            ['unpaid_fixed_amount', 'compare', 'compareValue' => 0, 'on' => 'cancel', 'message' => Translator::t('``')],

            ['id, status, supplier_id, contract_id, act_id, event_id, request_amount, delivered_amount, penalty_amount, returned_amount, delivered_amount_wrong_time'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * This method is invoked before saving a record (after validation, if any).
     * The default implementation raises the {@link onBeforeSave} event.
     * You may override this method to do any preparation work for record saving.
     * Use {@link isNewRecord} to determine whether the saving is
     * for inserting or updating record.
     * Make sure you call the parent implementation so that the event is raised properly.
     *
     * @return boolean whether the saving should be executed. Defaults to true.
     */
    protected function beforeSave()
    {
        if (in_array($this->status, [self::STATUS_ANALYSIS, self::STATUS_ANALYZED])
            && $this->contract
        ) {
            $notDeliveredAmount = Cast::toUFloat($this->request_amount - $this->delivered_amount + $this->delivered_amount_wrong_time);
            $penaltyAmount = $this->contract->getPenaltyAmount($notDeliveredAmount, $this->count_products_problem_delivery);
            if ($penaltyAmount != $this->penalty_amount) {
                $this->penalty_amount = $penaltyAmount;
            }
        }

        $this->_commitPaymentTime();
        $this->_updateUnpaidFixedAmount();
        return parent::beforeSave();
    }

    protected function afterSave()
    {
        parent::afterSave();
    }

    /**
     * Change payment_at
     */
    protected function _commitPaymentTime()
    {
        if ($this->payment_at == 0 && $this->status == self::STATUS_CONFIRMED) {
            $this->payment_at = time();
        }
    }

    protected function _updateUnpaidFixedAmount()
    {
        if (in_array($this->status, [self::STATUS_ANALYSIS, self::STATUS_ANALYZED])) {
            $amount = array_reduce($this->includeUnpaidPayments, function ($carry, $record) {
                /* @var SupplierPaymentRecord $record */
                return $carry + $record->unpaid_amount;
            }, 0);

            if ($amount != $this->unpaid_fixed_amount) {
                $this->unpaid_fixed_amount = $amount;
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'status' => Translator::t('Status'),
            'event_id' => Translator::t('Flash-sale'),
            'supplier_id' => Translator::t('Supplier'),
            'contract_id' => Translator::t('Type of contract'),
            'event_end_at' => Translator::t('Flash-sale end date'),
            'delivery_start_at' => Translator::t('Start of supply'),
            'delivery_end_at' => Translator::t('End of supplies'),
            'payment_after_at' => Translator::t('Payment'),
            'payment_at' => Translator::t('Payment time'),
            'count_products_problem_delivery' => Translator::t('Qty of products under problem delivery'),
            'request_amount' => Translator::t('Request amount'),
            'delivered_amount' => Translator::t('Delivery amount'),
            'delivered_amount_wrong_time' => Translator::t('Delivery amount after timeout'),
            'returned_amount' => Translator::t('Refund amount'),
            'penalty_amount' => Translator::t('Penalty'),
            'unpaid_amount' => Translator::t('Debt of the supplier'),
            'unpaid_fixed_amount' => Translator::t('Debt repaid'),
            'unpaid_in_payment_id' => Translator::t('Arrears recorded in payment'),
            'paid_amount' => Translator::t('Paid'),
            'custom_penalty_amount' => Translator::t('Other penalties'),
            'comment' => Translator::t('Comment'),
            'act_id' => Translator::t('Act ID'),
            'is_order_positions_worked' => Translator::t('Analysis of order position'),

            'payAmount' => Translator::t('To pay'),
            'payAmountWithoutPenalty' => Translator::t('For payment without penalty'),
            'statusReplacement' => Translator::t('Status'),

            'bankName' => Translator::t('Bank name'),
            'bankGiro' => Translator::t('Settlement account number/ Bank card number'),

            'order_table_loaded_at' => Translator::t('Order table upload time'),
            'updated_at' => Translator::t('Updated'),
            'created_at' => Translator::t('Created'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.event_id', $model->event_id);
        $criteria->compare($alias . '.supplier_id', $model->supplier_id);
        $criteria->compare($alias . '.contract_id', $model->contract_id);
        $criteria->compare($alias . '.act_id', $model->act_id);

        $criteria->compare($alias . '.request_amount', $model->request_amount);
        $criteria->compare($alias . '.delivered_amount', $model->delivered_amount);
        $criteria->compare($alias . '.returned_amount', $model->returned_amount);
        $criteria->compare($alias . '.penalty_amount', $model->penalty_amount);
        $criteria->compare($alias . '.unpaid_amount', $model->unpaid_amount);
        $criteria->compare($alias . '.unpaid_fixed_amount', $model->unpaid_fixed_amount);
        $criteria->compare($alias . '.delivered_amount_wrong_time', $model->delivered_amount_wrong_time);
        $criteria->compare($alias . '.count_products_problem_delivery', $model->count_products_problem_delivery);

        return $provider;
    }

    /**
     * @param bool|true $replacements
     * @param array $filter только те статусы которые переданы в фильтр
     *
     * @return array
     */
    public static function statuses($replacements = true, $filter = [])
    {
        $statuses = [
            self::STATUS_NOT_ANALYZED => Translator::t('Waiting for unloading'),
            self::STATUS_WAIT_DELIVERY => Translator::t('Waiting for supply'),
            self::STATUS_ANALYSIS => Translator::t('Waiting for post-supply'),
            self::STATUS_ANALYZED => Translator::t('Is ready for payment'),
            self::STATUS_CONFIRMED => Translator::t('Confirmed'),
            self::STATUS_CANCELED => Translator::t('Canceled'),
        ];

        if ($filter && is_array($filter)) {
            $statusesFiltered = [];
            foreach ($filter as $key) {
                if (isset($statuses[$key])) {
                    $statusesFiltered[$key] = $statuses[$key];
                }
            }

            return $replacements ? $statusesFiltered : array_keys($statusesFiltered);
        }

        return $replacements ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStatusReplacement($default = '')
    {
        $statuses = self::statuses();

        return isset($statuses[$this->status]) ? $statuses[$this->status] : $default;
    }

    /**
     * Сумма к оплате поставщику
     *
     * @param bool $withPenalty включить в оплату неустойку
     *
     * @return float
     */
    public function getPayAmount($withPenalty = true)
    {
        $amount = $this->delivered_amount - $this->returned_amount - $this->custom_penalty_amount - $this->unpaid_fixed_amount;
        if ($withPenalty) {
            $amount -= $this->penalty_amount;
        }

        return $amount;
    }

    /**
     * Сумма к оплате поставщику без штрофов
     *
     * @return float
     */
    public function getPayAmountWithoutPenalty()
    {
        return $this->getPayAmount(false);
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function deliveredStartAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.delivery_start_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function deliveredStartAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.delivery_start_at', "> $value");

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function hasDeliveredEnd($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $condition = ">0";
        if (!$value) {
            $condition = "=0";
        }

        $this->getDbCriteria()->compare($alias . '.delivery_end_at', $condition);

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function deliveredEndAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.delivery_end_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function deliveredEndAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.delivery_end_at', "> $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function eventEndAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.event_end_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function eventEndAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.event_end_at', "> $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function paymentAfterAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.payment_after_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function paymentAfterAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.payment_after_at', "> $value");

        return $this;
    }

    /**
     * @return static
     */
    public function hasPaymentAfterAt()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.payment_after_at', "> 0");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function paymentAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.payment_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function paymentAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.payment_at', "> $value");

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusNotIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.status', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdNotIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.event_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function supplierIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.supplier_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isOrderPositionsWorked($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_order_positions_worked' => (int)$value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function actIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.act_id', $values);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function actId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.act_id' => $value,
        ]);

        return $this;
    }

    /**
     * @return $this
     */
    public function hasUnpaidAmount()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.unpaid_amount', '> 0');

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function unpaidInPaymentId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.unpaid_in_payment_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function unpaidInPaymentIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.unpaid_in_payment_id', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function contractIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.contract_id', $values);

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function hasInAct($value)
    {
        $value = Cast::toBool($value);
        $condition = $value ? '<>' : '=';

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.act_id' . $condition . '0');
        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function idLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.id', $value, true);
        return $this;
    }

    /**
     * @param array $warehouseStatementReturnRecords
     *
     * @throws CDbException
     * @throws LogicException|Exception
     */
    public function setWarehouseStatementReturns(array $warehouseStatementReturnRecords)
    {
        $transaction = $this->dbConnection->beginTransaction();
        try {
            WarehouseStatementReturnRecord::model()->updateAll(['supplier_payment_id' => null], 'supplier_payment_id=:ID', [':ID' => $this->id]);
            foreach ($warehouseStatementReturnRecords as $record) {
                $this->setWarehouseStatementReturn($record);
            }

            $transaction->commit();
            $this->getRelated('statementReturns', true);
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    /**
     * @param WarehouseStatementReturnRecord $record
     *
     * @throw LogicException
     */
    public function setWarehouseStatementReturn(WarehouseStatementReturnRecord $record)
    {
        if ($this->supplier_id != $record->supplier_id) {
            throw new LogicException(Translator::t('Billing error for supplier № {id} to return № {record}, ​​different vendors', ['{id}' => $this->id, '{record}' => $record->id]));
        }

        $record->supplier_payment_id = $this->id;

        if (!$record->save()) {
            throw new LogicException(Translator::t('Billing error with vendor № {id} for return № {record}', ['{id}' => $this->id, '{record}' => $record->id]));
        };
    }

    /**
     * @param SupplierPaymentRecord[] $unpaidPayments
     *
     * @throws CDbException
     * @throws LogicException|Exception
     */
    public function setUnpaidPayments(array $unpaidPayments)
    {
        $transaction = $this->dbConnection->beginTransaction();
        try {
            SupplierPaymentRecord::model()->updateAll(['unpaid_in_payment_id' => null], 'unpaid_in_payment_id=:ID', [':ID' => $this->id]);
            foreach ($unpaidPayments as $record) {
                $this->setUnpaidPayment($record);
            }

            $transaction->commit();
            $this->getRelated('includeUnpaidPayments', true);
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    /**
     * @param SupplierPaymentRecord $record
     *
     * @throw LogicException
     */
    public function setUnpaidPayment(SupplierPaymentRecord $record)
    {
        if ($record->unpaid_in_payment_id > 0 && $record->unpaid_in_payment_id != $this->id) {
            throw new LogicException(Translator::t('Failed to add a record of payment arrears {record} to payment No. {id}'), ['{id}' => $this->id, '{record}' => $record->id]);
        }

        $record->unpaid_in_payment_id = $this->id;

        if (!$record->save()) {
            throw new LogicException(Translator::t('Error of account merge for debt on payment № {record} to payment № {id}'), ['{id}' => $this->id, '{record}' => $record->id]);
        };
    }

    /* API */
    /**
     * Кол-во доступных актов возвратов которые можно прикрепить к платежу
     *
     * @param int $cache
     *
     * @return int
     */
    public function getStatementReturnsPossibleCount($cache = 30)
    {
        return WarehouseStatementReturnRecord::model()
            ->cache($cache)
            ->isAccountedReturns(false)
            ->supplier($this->supplier_id)
            ->supplierPayment(0)
            ->status(WarehouseStatementReturnRecord::STATUS_CONFIRMED)
            ->count();
    }

    /**
     * @param int $supplierId
     * @param int $supplierPaymentId
     * @param int $cache
     *
     * @return $this
     */
    public function getUnpaidPaymentPossibleConditions($supplierId, $supplierPaymentId = 0, $cache = 30)
    {
        $payments = [0];
        if ($supplierPaymentId > 0) {
            $payments[] = $supplierPaymentId;
        }

        return self::model()
            ->cache($cache)
            ->supplierId($supplierId)
            ->unpaidInPaymentIdIn($payments)
            ->statusIn([self::STATUS_CANCELED])
            ->hasUnpaidAmount();
    }

    /**
     * @param int $cache
     *
     * @return string
     */
    public function getUnpaidPaymentPossibleCount($cache = 30)
    {
        return $this->getUnpaidPaymentPossibleConditions($this->supplier_id)->count();
    }

    /**
     * @return array
     */
    public function getStatementReturnsID()
    {
        return ArrayUtils::getColumn($this->statementReturns, 'id');
    }

    /**
     * @return string
     */
    public function getBankName(): string
    {
        return self::_bankDataDecode($this->bank_name_enc);
    }

    /**
     * @return string
     */
    public function getBankGiro(): string
    {
        return self::_bankDataDecode($this->bank_giro_enc);
    }

    /**
     * @param $value
     */
    public function setBankName($value)
    {
        $this->bank_name_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankGiro($value)
    {
        $this->bank_giro_enc = self::_bankDataEncode($value);
    }

    // bank account

    /**
     * @return string
     */
    protected static function _bankDataPrivateKey(): string
    {
        return pack('H*', '55104c459b7310e8bd7a8004a0d2cc2e30954a22aa8e8b7fd25d58ca8f24983e');
    }

    //API

    /**
     * @return bool
     */
    public function isPossibleConfirm()
    {
        return ($this->status == self::STATUS_ANALYZED && $this->payment_after_at > 0 && $this->payment_after_at < time());
    }

    /**
     * начало поставок для поставщиков считается со следующего дня после окончания акции
     *
     * @return int
     */
    public function getMustDeliveryStartAt()
    {
        return strtotime('tomorrow', $this->event_end_at);
    }

    /**
     * Интервал отсрочки платежа
     *
     * @return DateInterval
     */
    public function getStayPayment()
    {
        $interval = new DateInterval('PT0S');

        if ($this->payment_after_at > 0 && $this->delivery_end_at > 0) {
            $deliveryDateTime = new DateTime();
            $deliveryDateTime->setTimestamp($this->getDeliveryEndTime());

            $paymentDateTime = new DateTime();
            $paymentDateTime->setTimestamp($this->payment_after_at);

            $interval = $deliveryDateTime->diff($paymentDateTime);
        }

        return $interval;
    }

    /**
     * Интервал отсрочки платежа
     *
     * @return DateInterval
     */
    public function getStayPaymentWorkDays()
    {
        $days = 0;

        if ($this->payment_after_at > 0 && $this->delivery_end_at > 0) {
            $deliveryDateTime = new DateTime();
            $deliveryDateTime->setTimestamp($this->getDeliveryEndTime());

            $paymentDateTime = new DateTime();
            $paymentDateTime->setTimestamp($this->payment_after_at);

            $period = new DatePeriod($deliveryDateTime, new DateInterval('P1D'), $paymentDateTime);
            foreach ($period as $dt) {
                if (in_array($dt->format('N'), [6, 7])) { //'N' = ISO-8601 от 1 (понедельник) до 7 (воскресенье)
                    continue;
                }

                $days++;
            }
        }

        return $days;
    }

    /**
     * Нормализованный timestamp времени доставки
     *
     * @return int
     */
    public function getDeliveryEndTime()
    {
        if ($this->delivery_end_at == 0) {
            return $this->delivery_end_at;
        }

        return strtotime('today', $this->delivery_end_at);
    }

    /**
     * если возможность проапрувить доставку, прервать поставку в лучае если не будет допоставки/поставки
     */
    public function isCanApplyFullDelivery()
    {
        $availableDeliveryDays = $this->contract->getAvailableDeliveryDays($this->event_end_at, false);
        if ($availableDeliveryDays > 0) {
            return false;
        }
        $result = false;

        if ($this->status == self::STATUS_ANALYSIS) {
            $result = true;
        } elseif (
            $this->status == self::STATUS_WAIT_DELIVERY
            && $this->delivered_amount == 0
            && $this->delivered_amount_wrong_time == 0
        ) {
            $result = true;
        }

        return $result;
    }
}
