<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderBankRefundRecord
 * Возврат денежных средств которые были оплачены катрой
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $order_user_id
 * @property float $price_order
 * @property float $discount
 * @property float $price_refund
 * @property int $payment_after_at
 * @property float $order_card_payed
 * @property string $transaction
 * @property bool $is_refunded успешное списание денег с заказа
 * @property integer $status
 * @property integer $status_prev
 * @property integer $type_refund
 * @property string $status_history_json
 * @property-read integer $status_changed_at
 * @property boolean $is_custom
 * @property integer $admin_id
 * @property string $comment комментарий менеджера
 * @property int $actionOrderPayed
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read string $statusReplacement
 * @property-read string $statusPrevReplacement
 * @property-read OrderRecord $order
 * @property-read UserRecord $orderUser
 * @property-read AdminUserRecord $user
 * @property-read array $statusHistory
 */
class OrderBankRefundRecord extends DoctrineActiveRecord
{
    const ENABLE_EDIT_TIME = 10800; // 3 hours

    const STATUS_UNCONFIGURED = 0; // создано, но не подтверждено
    const STATUS_CONFIGURED = 1; // подтверждено и зафиксированно
    const STATUS_NEED_PAY = 2; // в очереди на оплату
    const STATUS_PAYED = 3; // выплачено
    const STATUS_CANCELLED = 4; // отменено

    const ACTION_ORDER_PAYED_NOTHING = 0,
        ACTION_ORDER_PAYED_WRITTEN_OFF = 1,
        ACTION_ORDER_PAYED_RETURNED = 2;

    const TYPE_REFUND_INNER = 0, //возврат на внутренний счет
        TYPE_REFUND_PRIVATE_BANK = 1; //возврат через Приват Банк

    const NOT_PAYMENT_TIME_INNER = 0; //2 дня
    const NOT_PAYMENT_TIME_BANK = 0; //12 дней, обязаны в течении 14 дней выплатить

    /**
     * действия которые произведены с казазом
     *
     * @var int
     */
    private $_actionOrderPayed = self::ACTION_ORDER_PAYED_NOTHING;

    /**
     * @param string $className
     *
     * @return OrderBankRefundRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['price_order', 'getPriceOrder', 'setPriceOrder', 'priceOrder'];
        yield ['discount', 'getDiscount', 'setDiscount', 'discount'];
        yield ['price_refund', 'getPriceRefund', 'setPriceRefund', 'priceRefund'];
        yield ['payment_after_at', 'getPaymentAfterAt', 'setPaymentAfterAt', 'paymentAfterAt'];
        yield ['order_card_payed', 'getOrderCardPayed', 'setOrderCardPayed', 'orderCardPayed'];
        yield ['transaction', 'getTransaction', 'setTransaction', 'transaction'];
        yield ['is_refunded', 'isRefunded', 'setIsRefunded', 'isRefunded'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['type_refund', 'getTypeRefund', 'setTypeRefund', 'typeRefund'];
        yield ['status_history_json', 'getStatusHistoryJson', 'setStatusHistoryJson', 'statusHistoryJson'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['is_custom', 'isCustom', 'setIsCustom', 'isCustom'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['order_user_id', 'getOrderUserId', 'setOrderUserId', 'orderUserId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    public function tableName()
    {
        return 'orders_banks_refunds';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'orderUser' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'order_user_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
        ];
    }

    public function rules()
    {
        return [
            ['order_id', 'required'],
            ['order_id, order_user_id, price_order, price_refund, order_card_payed', 'numerical'],

            ['is_custom', 'boolean'],

            ['status, status_prev', 'in', 'range' => [
                self::STATUS_UNCONFIGURED,
                self::STATUS_CONFIGURED,
                self::STATUS_NEED_PAY,
                self::STATUS_CANCELLED,
                self::STATUS_PAYED,
            ]],

            ['type_refund', 'in', 'range' => [
                self::TYPE_REFUND_INNER,
                self::TYPE_REFUND_PRIVATE_BANK,
            ]],

            ['status', 'default', 'value' => self::NOT_PAYMENT_TIME_INNER > 0 || self::NOT_PAYMENT_TIME_BANK > 0
                ? self::STATUS_UNCONFIGURED : self::STATUS_CONFIGURED,
            ],
            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['comment, transaction', 'length', 'max' => 250],

            ['payment_after_at', 'numerical', 'integerOnly' => true],

            [
                'id, order_id, order_user_id, is_custom, is_refunded, ' .
                'price_order, price_refund, order_card_payed, transaction, ' .
                'status, status_prev, type_refund',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.order_id', $model->order_id, true);
        $criteria->compare($alias . '.order_user_id', $model->order_user_id, true);
        $criteria->compare($alias . '.price_refund', $model->price_refund);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.status_prev', $model->status_prev);
        $criteria->compare($alias . '.is_custom', $model->is_custom);
        $criteria->compare($alias . '.is_refunded', $model->is_refunded);
        $criteria->compare($alias . '.price_order', $model->price_order, true);
        $criteria->compare($alias . '.price_refund', $model->price_refund, true);
        $criteria->compare($alias . '.order_card_payed', $model->order_card_payed);
        $criteria->compare($alias . '.transaction', $model->transaction, true);
        $criteria->compare($alias . '.type_refund', $model->type_refund);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'order_id' => \Yii::t($category, 'Order'),
            'order_user_id' => \Yii::t($category, 'User'),

            'price_order' => \Yii::t($category, 'Order cost'),
            'discount' => \Yii::t($category, 'A discount'),
            'price_refund' => \Yii::t($category, 'Refund amount'),
            'payment_after_at' => \Yii::t($category, 'Compensate after'),
            'order_card_payed' => \Yii::t($category, 'Paid by wire'),
            'transaction' => \Yii::t($category, 'Transaction'),

            'status' => \Yii::t($category, 'Status'),
            'status_prev' => \Yii::t($category, 'Previous status'),
            'status_history_json' => \Yii::t($category, 'Status History'),
            'is_refunded' => \Yii::t($category, 'Refund order'),
            'type_refund' => \Yii::t($category, 'Return type'),

            'is_custom' => \Yii::t($category, 'Created by hand'),
            'admin_id' => \Yii::t($category, 'User'),
            'comment' => \Yii::t($category, 'Manager comments'),

            'created_at' => \Yii::t($category, 'Created'),
            'updated_at' => \Yii::t($category, 'Updated on'),

            'statusHistory' => \Yii::t($category, 'Status History'),
            'statusReplacement' => \Yii::t($category, 'Status'),
            'statusPrevReplacement' => \Yii::t($category, 'Preliminary status'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_updateOrder(); //должен быть первым в очереди
        $this->_updateStatusChangeStack();
        $this->_checkPaymentAfterTime();

        return $result;
    }

    public function _updateOrder()
    {
        $order = $this->order;

        if ($this->isNewRecord || $order === null) {
            return;
        }

        if ($this->status == self::STATUS_CANCELLED && $this->is_refunded) {
            $order->card_payed = $order->card_payed + $this->price_refund;

            if ($order->update(['card_payed'])) {
                $this->_actionOrderPayed = self::ACTION_ORDER_PAYED_RETURNED;
                $this->is_refunded = 0;
            }
        }
    }

    /**
     * Обновление даты выплаты, если дата выпадает на выходные, выплата назначается на пятницу
     */
    protected function _checkPaymentAfterTime()
    {
        if ($this->status == self::STATUS_CONFIGURED && $this->payment_after_at == 0) {
            $time = time();
            switch ($this->type_refund) {
                case self::TYPE_REFUND_INNER:
                    $time += self::NOT_PAYMENT_TIME_INNER;
                    break;

                case self::TYPE_REFUND_PRIVATE_BANK:
                    $time += self::NOT_PAYMENT_TIME_BANK;
                    break;
            }

            $dataTime = new DateTime();
            $dataTime->setTimestamp($time);
            $dataTime->modify('today +8 hours');

            $dateDayNumber = $dataTime->format('N');
            if (in_array($dateDayNumber, [6, 7])) {
                $dataTime->modify('previous friday +8 hours');
            }

            $this->payment_after_at = $dataTime->getTimestamp();
        }
    }

    /**
     * @return int
     */
    public function getActionOrderPayed()
    {
        return $this->_actionOrderPayed;
    }

    /**
     * @param int $actionOrderPayed
     */
    public function setActionOrderPayed($actionOrderPayed)
    {
        $validAction = [
            self::ACTION_ORDER_PAYED_NOTHING,
            self::ACTION_ORDER_PAYED_RETURNED,
            self::ACTION_ORDER_PAYED_WRITTEN_OFF,
        ];
        if (in_array($actionOrderPayed, $validAction)) {
            $this->_actionOrderPayed = $actionOrderPayed;
        }
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateStatusChangeStack()
    {
        $statusHistory = $this->getStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->status) {
            $now = time();

            $addToHistory = [
                'status' => $this->status,
                'admin_id' => 0,
                'user_id' => 0,
                'actionOrderPayed' => $this->_actionOrderPayed, //списались деньги
            ];

            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
            $this->status_prev = $prevStatus['status'];

            if (!$this->isNewRecord) {
                $this->status_changed_at = time();
            }
        }
        ksort($statusHistory);

        $this->status_history_json = json_encode($statusHistory);
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function orderIdIn($value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $value);
        return $this;
    }

    /**
     * @return static
     */
    public function notCancelled()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.status', [
            self::STATUS_CANCELLED,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderUserId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function orderUserIdIn($value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_user_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isCustom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_custom' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function refundType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.refund_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function priceRefundFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.price_refund' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function priceRefundTo($value)
    {
        $value = Cast::toUInt($value);
        $value = $this->dbConnection->pdoInstance->quote($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.price_refund' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return $this
     */
    public function refundPriceBetween($from, $to)
    {
        $this->priceRefundFrom($from);
        $this->priceRefundTo($to);
        return $this;
    }

    /**
     * @param int $value unix timestamp
     *
     * @return $this
     */
    public function paymentAfterAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.payment_after_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param int $value unix timestamp
     *
     * @return $this
     */
    public function paymentAfterAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.payment_after_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function statusPrev($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status_prev' => $value,
        ]);
        return $this;
    }

    /**
     * Готовые к оплате
     *
     * @return static
     */
    public function onlyPayReady()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $criteria->addCondition(
            $alias . '.status = ' . self::STATUS_CONFIGURED . ' AND ' .
            $alias . '.' . 'payment_after_at < ' . $time
        );
        return $this;
    }

    /**
     * Не готовые к оплате
     *
     * @return static
     */
    public function onlyPayUnready()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $criteria->addCondition(
            $alias . '.status <> ' . self::STATUS_CONFIGURED . ' OR ' .
            $alias . '.status_changed_at >= ' . ($time - self::ENABLE_EDIT_TIME)
        );
        return $this;
    }

    /**
     * Редактировать нельзя
     *
     * @return static
     */
    public function unavailableEdit()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $statusChangedAt = $this->status_changed_at > 0 ? $this->status_changed_at : $time;

        $criteria->addCondition(
            $alias . '.status_changed_at <= ' . ($statusChangedAt - self::ENABLE_EDIT_TIME)
        );

        return $this;
    }

    /**
     * Редактировать нельзя
     *
     * @return static
     */
    public function availableEdit()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $statusChangedAt = $this->status_changed_at > 0 ? $this->status_changed_at : $time;

        $criteria->addCondition(
            $alias . '.status_changed_at > ' . ($statusChangedAt - self::ENABLE_EDIT_TIME)
        );

        return $this;
    }

    /**
     * произошло списание
     *
     * @param bool $value
     *
     * @return $this
     */
    public function refunded($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toBool($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_refunded' => Cast::toUInt($value),
        ]);

        return $this;
    }

    /**
     * возвраты денег за заказ которые сделаны + которые возможно будут сделаны
     *
     * @return $this
     */
    public function refundRealTime()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $statusChangedAt = $this->status_changed_at > 0 ? $this->status_changed_at : $time;

        $this->notCancelled();
        $criteria->addCondition(
            $alias . '.status_changed_at > ' . ($statusChangedAt - self::ENABLE_EDIT_TIME)
            , 'OR');

        return $this;
    }

    /** API */

    /**
     * @return bool
     */
    public function isAvailableEdit()
    {
        $result = false;
        $time = time();

        $statusChangedAt = $this->status_changed_at > 0 ? $this->status_changed_at : $time;
        /** @var bool $isEditingTimeLeft время для редактирования закончилось */
        $isAvailableEditingTime = $time < $statusChangedAt + self::ENABLE_EDIT_TIME;

        if ($isAvailableEditingTime) {
            $result = true;
        }

        return $result;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function statusChangedAtAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.status_changed_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function statusChangedAtAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.status_changed_at', ">= $value");

        return $this;
    }

    /**
     * @return $this
     */
    public function statusChangedAtOut()
    {
        $value = time() - self::ENABLE_EDIT_TIME;

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition("$alias.status_changed_at < $value AND NOT $alias.status_changed_at=0");
        return $this;
    }

    /**
     * @return array
     */
    public function getStatusHistory()
    {
        return Cast::toArr(json_decode($this->status_history_json, true, 3));
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_UNCONFIGURED => \Yii::t($category, 'Created but not verified'),
            self::STATUS_CONFIGURED => \Yii::t($category, 'confirmed'),
            self::STATUS_NEED_PAY => \Yii::t($category, 'In the queue for payment'),
            self::STATUS_PAYED => \Yii::t($category, 'Was paid'),
            self::STATUS_CANCELLED => \Yii::t($category, 'Canceled'),
        ];
    }

    /**
     * @return string
     */
    public function actionOrderPayedReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->_actionOrderPayed]) ? $replacements[$this->_actionOrderPayed] : '';
    }

    /**
     * @return array
     */
    public static function actionOrderPayedReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::ACTION_ORDER_PAYED_NOTHING => \Yii::t($category, 'Refund was not completed'),
            self::ACTION_ORDER_PAYED_RETURNED => \Yii::t($category, 'Money returned from the order'),
            self::ACTION_ORDER_PAYED_WRITTEN_OFF => \Yii::t($category, 'Money was charged from the order'),
        ];
    }

    /**
     * @return array
     */
    public static function typeRefundReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::TYPE_REFUND_INNER => \Yii::t($category, 'Return to mommy account'),
            self::TYPE_REFUND_PRIVATE_BANK => \Yii::t($category, 'Refund through the bank'),
        ];
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function typeRefundReplacement($default = '')
    {
        $replacements = self::typeRefundReplacements();
        return isset($replacements[$this->type_refund]) ? $replacements[$this->type_refund] : $default;
    }

    /**
     * Создание возврата на основе заказа, учитывая предыдущие возвраты
     *
     * @param OrderRecord $order
     * @param float $amount
     *
     * @return bool
     */
    public static function refundOrder(OrderRecord $order, $amount)
    {
        $priceRefund = $amount;
        $result = false;

        $bankRefundOrders = self::model()
            ->refundRealTime()
            ->orderId($order->id)
            ->findAll();

        if ($bankRefundOrders) {
            $masPriceRefunds = ArrayUtils::getColumn($bankRefundOrders, 'price_refund');
            $priceRefund -= array_sum($masPriceRefunds);
        }

        if ($priceRefund > 0) {
            $bankRefundOrder = new self();
            $bankRefundOrder->order_id = $order->id;
            $bankRefundOrder->is_custom = false;
            $bankRefundOrder->order_user_id = $order->user_id;
            $bankRefundOrder->price_order = $order->getPrice();
            $bankRefundOrder->discount = $order->getPrice() - $order->getPayPrice(false);
            $bankRefundOrder->order_card_payed = $order->card_payed;

            $bankRefundOrder->price_refund = $priceRefund;

            $result = $bankRefundOrder->save();
        }

        return $result;
    }
}
