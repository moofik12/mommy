<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class InviteRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property string $email
 * @property integer $status 0 - В ожидании; 1 - Покупка совершена; 2 - Активировано
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read UserRecord $user
 */
class InviteRecord extends DoctrineActiveRecord
{
    const STATUS_WAITING = 0,
        STATUS_ACTIVATED = 1,
        STATUS_PURCHASE = 2;

    /**
     * @param string $className
     *
     * @return InviteRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    public function tableName()
    {
        return 'invites';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['user_id, email', 'required'],
            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'id'],
            ['email', 'email'],

            ['status', 'in', 'range' => [self::STATUS_WAITING, self::STATUS_ACTIVATED, self::STATUS_PURCHASE]],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => Translator::t('User'),
            'email' => Translator::t('E-mail'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function emailIn($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.email' => $value,
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function statusReplacement()
    {
        return [
            self::STATUS_WAITING => Translator::t('Pending'),
            self::STATUS_ACTIVATED => Translator::t('Purchase completed'),
            self::STATUS_PURCHASE => Translator::t('Activated'),
        ];
    }
}
