<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class UserPartnerRecord
 *
 * @property-description Участиники партнерской программы
 * @property-read integer $id
 * @property integer $user_id
 * @property string $source
 * @property integer $partner_percent
 * @property integer $is_sent_unisender
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read UserRecord $user
 * @property-read UserPartnerInviteRecord[] $invitedUsers
 */
class UserPartnerRecord extends DoctrineActiveRecord
{
    /** @var int */
    public $balance = 0; //Всего денег

    const PARTNER_PERCENT = 5;

    /**
     * @return string
     */
    public function tableName()
    {
        return 'users_partner';
    }

    /**
     * @param string $className
     *
     * @return UserPartnerRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['source', 'getSource', 'setSource', 'source'];
        yield ['partner_percent', 'getPartnerPercent', 'setPartnerPercent', 'partnerPercent'];
        yield ['is_sent_unisender', 'isSentUnisender', 'setIsSentUnisender', 'isSentUnisender'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['is_sent_unisender, partner_percent', 'numerical', 'integerOnly' => true],
            ['source', 'length', 'max' => 16],
            ['id, user_id, source, is_sent_unisender, partner_percent', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'invitedUsers' => [self::HAS_MANY, 'MommyCom\Model\Db\UserPartnerInviteRecord', 'partner_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('Partner ID'),
            'user_id' => Translator::t('User'),
            'source' => Translator::t('Source'),
            'partner_percent' => Translator::t('Affiliate commission'),
            'is_sent_unisender' => Translator::t('Sent to Unisender'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.source', $model->source);
        $criteria->compare($alias . '.partner_percent', $model->partner_percent);

        return $provider;
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }
        $this->partner_percent = self::PARTNER_PERCENT;
        return $result;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function partnerPercent($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.partner_percent' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function source($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toStr($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.source' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function isSentUnisender($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_sent_unisender' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param $ids
     * @param int $max
     *
     * @return $this
     */
    public function userIdIn($ids, $max = -1)
    {
        if ($max > 0) {
            $ids = array_slice($ids, 0, $max);
        }
        $ids = Cast::toUIntArr($ids);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $ids);
        return $this;
    }

    /**
     * Всего на потенциальном балансе
     *
     * @return number
     */
    public function getPotentialBalance()
    {
        $amount = UserPartnerAdmissionRecord::model()
            ->statusIn([UserPartnerAdmissionRecord::STATUS_NEW, UserPartnerAdmissionRecord::STATUS_PAID_AND_WAIT])
            ->partnerId($this->id)
            ->findColumnSum('amount');
        return $amount !== false ? Cast::toInt($amount) : 0;
    }

    /**
     * Всего на балансе (подтвержденном)
     *
     * @return number
     */
    public function getBalance()
    {
        $amount = UserPartnerBalanceRecord::model()->partnerId($this->id)->findColumnSum('amount');
        return $amount !== false ? Cast::toInt($amount) : 0;
    }

    /**
     * Возращено при отмене при выводе средств
     *
     * @return number
     */
    public function getSumCancel()
    {
        $amount = UserPartnerBalanceRecord::model()->partnerId($this->id)->type(UserPartnerBalanceRecord::TYPE_CANCEL_PLUS)->findColumnSum('amount');
        return $amount !== false ? (Cast::toInt($amount)) : 0;
    }

    /**
     * Сумма, вычитаемая из баланса по возвратам заказа
     *
     * @return number
     */
    public function getSumReturn()
    {
        $amount = UserPartnerBalanceRecord::model()->partnerId($this->id)->type(UserPartnerBalanceRecord::TYPE_RETURN_MINUS)->findColumnSum('amount');
        return $amount !== false ? (Cast::toInt($amount)) : 0;
    }

    /**
     * Всего на выведено
     *
     * @var $typeOutput string all | mamam | privat
     * @return number
     */
    public function getSumOutput($typeOutput = 'all')
    {
        $types = [];
        if ($typeOutput == 'all') {
            $types = [UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM, UserPartnerOutputRecord::TYPE_OUTPUT_PRIVAT];
        } elseif ($typeOutput == 'mamam') {
            $types = [UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM];
        } elseif ($typeOutput == 'privat') {
            $types = [UserPartnerOutputRecord::TYPE_OUTPUT_PRIVAT];
        }
        $balances = UserPartnerOutputRecord::model()
            ->partnerId($this->id)
            ->typeOutputIn($types)
            ->findAll();
        $result = array_reduce($balances, function ($carry, $item) {
            /* @var UserPartnerOutputRecord $item */

            return $carry + $item->amount;
        }, 0);
        return abs($result);
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function idLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.id', $value, true, 'OR');
        return $this;
    }
}
