<?php

namespace MommyCom\Model\Db;

use CJSON;
use MommyCom\Entity\EmailMessage as EmailMessageEntity;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use Swift_Message;

/**
 * This is the model class for table "email_message".
 * The followings are the available columns in table 'email':
 *
 * @property string $id
 * @property string $from
 * @property string $to
 * @property string $cc
 * @property string $bcc
 * @property string $subject
 * @property string $body
 * @property string $headers
 * @property string $contentType
 * @property string $charset
 * @property integer $status 0 - не отправлено; 1 - отправлено; 2 - ошибка в отправке
 * @property integer $type 0-системные сообщения; 1-пользовательские
 * @property integer $sent_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class EmailMessage extends DoctrineActiveRecord
{
    const STATUS_WAITING = 0, //default
        STATUS_SENT = 1,
        STATUS_ERROR = 2;

    const TYPE_SYSTEM = 0, //default
        TYPE_CUSTOM = 1;

    /**
     * @var Swift_Message the swift message.
     */
    private $_message;

    //для поиска в grid
    public $searchDateStart;
    public $searchDateEnd;
    //для хранения даты выбора при перезагрузке
    public $searchDateRangeInput;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return EmailMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['from', 'getFrom', 'setFrom', 'from'];
        yield ['to', 'getTo', 'setTo', 'to'];
        yield ['cc', 'getCc', 'setCc', 'cc'];
        yield ['bcc', 'getBcc', 'setBcc', 'bcc'];
        yield ['subject', 'getSubject', 'setSubject', 'subject'];
        yield ['body', 'getBody', 'setBody', 'body'];
        yield ['headers', 'getHeaders', 'setHeaders', 'headers'];
        yield ['contentType', 'getContentType', 'setContentType', 'contentType'];
        yield ['charset', 'getCharset', 'setCharset', 'charset'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['sent_at', 'getSentAt', 'setSentAt', 'sentAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntityClassName(): string
    {
        return EmailMessageEntity::class;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'email_message';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['from, to, subject, body, headers, contentType, charset', 'required'],
            ['status', 'in', 'range' => [
                self::STATUS_WAITING,
                self::STATUS_SENT,
                self::STATUS_ERROR,
            ]],
            ['type', 'in', 'range' => [
                self::TYPE_SYSTEM,
                self::TYPE_CUSTOM,
            ]],
            //array('headers', 'length', 'max' => 500),
            ['subject', 'length', 'max' => 255],
            ['body', 'length', 'max' => 64000, 'encoding' => false],
            ['contentType', 'length', 'max' => 60],
            ['charset', 'length', 'max' => 30],
            ['cc, bcc, sent_at, headers', 'safe'],

            // The following rule is used by search().
            ['id, from, to, cc, bcc, subject, body, headers, status, sent_at, searchDateStart, searchDateEnd, searchDateRangeInput'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function afterFind()
    {
        $this->from = CJSON::decode($this->from);
        $this->to = CJSON::decode($this->to);
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->from = is_array($this->from) ? json_encode($this->from, JSON_UNESCAPED_UNICODE) : $this->from;
        $this->to = is_array($this->to) ? json_encode($this->to, JSON_UNESCAPED_UNICODE) : $this->to;

        return $result;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'from' => Translator::t('From'),
            'to' => Translator::t('To whom'),
            'subject' => Translator::t('Subject'),
            'body' => Translator::t('Text'),
            'sent_at' => Translator::t('Time of dispatch'),
            'status' => Translator::t('Status'),
            'type' => Translator::t('Type'),

            'created_at' => Translator::t('Created'),
            'update_at' => Translator::t('Updated'),

            'searchDateRangeInput' => Translator::t('Period'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.from', $provider->model->from, true);
        $criteria->compare($alias . '.to', $provider->model->to, true);
        $criteria->compare($alias . '.subject', $provider->model->subject, true);
        $criteria->compare($alias . '.status', $provider->model->status);
        $criteria->compare($alias . '.type', $provider->model->type);
        $criteria->compare($alias . '.body', $provider->model->body, true);

        //для поиска в grid
        $criteria->compare($alias . '.created_at', '>=' . $provider->model->searchDateStart);
        $criteria->compare($alias . '.created_at', '<=' . $provider->model->searchDateEnd);

        return $provider;
    }

    /**
     * @return array
     */
    public function statusReplacement()
    {
        return [
            self::STATUS_WAITING => Translator::t('Waiting to send'),
            self::STATUS_SENT => Translator::t('Sent'),
            self::STATUS_ERROR => Translator::t('Error sending'),
        ];
    }

    /**
     * @return array
     */
    public function typeReplacement()
    {
        return [
            self::TYPE_SYSTEM => Translator::t('System'),
            self::TYPE_CUSTOM => Translator::t('User\'s'),
        ];
    }

    public function defaultScope()
    {
        return [
            'limit' => 50,
        ];
    }

    /**
     * @param integer $status (self::STATUS_WAITING |     self::STATUS_SENT | self::STATUS_ERROR)
     *
     * @return static
     */
    public function status($status)
    {
        $status = intval($status);

        if (!in_array($status, [
            self::STATUS_WAITING,
            self::STATUS_SENT,
            self::STATUS_ERROR,
        ])) {
            $status = self::STATUS_WAITING;
        }

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.status',
            [$status]
        );

        return $this;
    }

    /**
     * @param integer $type (self::TYPE_SYSTEM,    self::TYPE_CUSTOM)
     *
     * @return $this
     */
    public function type($type)
    {
        $type = intval($type);

        if (!in_array($type, [
            self::TYPE_SYSTEM,
            self::TYPE_CUSTOM,
        ])) {
            $type = self::TYPE_SYSTEM;
        }

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.type' => $type]
        );

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function sendToLike($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition(
            $alias . '.to',
            $value
        );

        return $this;
    }

    /**
     * Returns the number of recipients for this message.
     *
     * @return integer the count.
     */
    public function getRecipientCount()
    {
        $message = $this->getMessage();
        return $message instanceof Swift_Message ? count($message->getTo()) : -1;
    }

    /**
     * Creates a Swift_Message instance for this model.
     *
     * @return Swift_Message the message.
     */
    public function createMessage()
    {
        $from = is_array($this->from) ? $this->from : json_decode($this->from);
        $to = is_array($this->to) ? $this->to : json_decode($this->to);
        $subject = $this->subject;
        $body = $this->body;
        $contentType = $this->contentType;
        $charset = $this->charset;

        /** @var Swift_Message $message */
        $message = new Swift_Message($subject);

        // Set variables to message
        $message->setFrom($from)
            ->setTo($to)
            ->setBody($body, $contentType, $charset);

        return $message;
    }

    /**
     * Returns the swift message instance.
     *
     * @return Swift_Message the instance.
     */
    public function getMessage()
    {
        if (isset($this->_message)) {
            return $this->_message;
        } else {
            return $this->_message = $this->createMessage();
        }
    }

    /**
     * Converts this model to a string.
     *
     * @return string the text.
     */
    public function __toString()
    {
        $message = $this->getMessage();
        $from = implode(', ', array_keys($message->getFrom()));
        $to = implode(', ', array_keys($message->getTo()));
        $headers = implode('', $message->getHeaders()->getAll());
        $body = $message->getBody();
        return 'From: ' . $from . PHP_EOL
            . 'To: ' . $to . PHP_EOL
            . 'Headers: ' . PHP_EOL . $headers
            . 'Body: ' . PHP_EOL . $body;
    }
}
