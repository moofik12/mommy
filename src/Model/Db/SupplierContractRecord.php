<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\RegionalTranslator;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class SupplierContractTypeRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property bool $is_default
 * @property integer $not_delivered_penalty_percent
 * @property integer $delivery_working_days
 * @property integer $add_delivery_working_days
 * @property string $agreement
 * @property string $paid_by
 * @property integer $days_to_pay
 * @property integer $discount_from_market
 * @property integer $discount_from_wholesale
 * @property-read int updated_at
 * @property-read int created_at
 * @property-read SupplierPaymentRecord[] $supplierPayments
 * @property-read SupplierRecord[] $suppliers
 * @property-read integer $suppliersCount
 */
class SupplierContractRecord extends DoctrineActiveRecord
{
    use ApplicationTrait;

    const STRATEGY_MAX_PENALTY = 'max';
    const STRATEGY_MIN_PENALTY = 'min';
    const STRATEGY_ALL_PENALTY = 'all';

    protected $_strategy = self::STRATEGY_MAX_PENALTY;

    /**
     * @return string
     */
    public function getStrategy()
    {
        return $this->_strategy;
    }

    /**
     * @param $strategy
     *
     * @return $this
     */
    public function setStrategy($strategy)
    {
        if (in_array($strategy, self::getStrategies())) {
            $this->_strategy = $strategy;
        }

        return $this;
    }

    public static function getStrategies()
    {
        return [
            self::STRATEGY_MAX_PENALTY,
            self::STRATEGY_MIN_PENALTY,
            self::STRATEGY_ALL_PENALTY,
        ];
    }

    /**
     * @param string $className
     *
     * @return SupplierContractRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['is_default', 'isDefault', 'setIsDefault', 'isDefault'];
        yield ['not_delivered_penalty_percent', 'getNotDeliveredPenaltyPercent', 'setNotDeliveredPenaltyPercent', 'notDeliveredPenaltyPercent'];
        yield ['delivery_working_days', 'getDeliveryWorkingDays', 'setDeliveryWorkingDays', 'deliveryWorkingDays'];
        yield ['add_delivery_working_days', 'getAddDeliveryWorkingDays', 'setAddDeliveryWorkingDays', 'addDeliveryWorkingDays'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['agreement', 'getAgreement', 'setAgreement', 'agreement'];
        yield ['paid_by', 'getPaidBy', 'setPaidBy', 'paidBy'];
        yield ['days_to_pay', 'getDaysToPay', 'setDaysToPay', 'daysToPay'];
        yield ['discount_from_market', 'getDiscountFromMarket', 'setDiscountFromMarket', 'discountFromMarket'];
        yield ['discount_from_wholesale', 'getDiscountFromWholesale', 'setDiscountFromWholesale', 'discountFromWholesale'];
    }

    public function tableName()
    {
        return 'suppliers_contracts';
    }

    public function relations()
    {
        return [
            'supplierPayments' => [self::HAS_MANY, 'MommyCom\Model\Db\SupplierPaymentRecord', 'contract_id'],
            'suppliers' => [self::HAS_MANY, 'MommyCom\Model\Db\SupplierRecord', 'contract_id'],
            'suppliersCount' => [self::STAT, 'MommyCom\Model\Db\SupplierRecord', 'contract_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'length', 'min' => 5, 'max' => 255, 'allowEmpty' => false],
            ['is_default', 'boolean'],
            ['is_default', 'validateDefault'],
            ['not_delivered_penalty_percent', 'numerical', 'min' => 0, 'max' => 100],
            ['delivery_working_days', 'numerical', 'min' => 1, 'max' => 100, 'allowEmpty' => false],
            ['add_delivery_working_days', 'numerical', 'min' => 0, 'max' => 100, 'allowEmpty' => false],
            [
                'name, not_delivered_penalty_percent, agreement, delivery_working_days, add_delivery_working_days, paid_by, days_to_pay',
                'required',
            ],
        ];
    }

    /**
     * @return array
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function attributeLabels()
    {
        $translator = Translator::get();

        if ($this->app()->user->getModel()->is_supplier) {
            $translator = RegionalTranslator::get();
        }

        return [
            'id' => $translator->t('№'),
            'name' => $translator->t('Name'),
            'is_default' => $translator->t('Default'),
            'not_delivered_penalty_percent' => $translator->t('Return is included into agreement (%)'),
            'delivery_working_days' => $translator->t('Delivery time (working days)'),
            'add_delivery_working_days' => $translator->t('Return period'),
            'updated_at' => $translator->t('Updated'),
            'created_at' => $translator->t('Created'),
            'suppliers' => $translator->t('Suppliers'),
            'suppliersCount' => $translator->t('Suppliers'),
            'agreement' => $translator->t('Agreement type'),
            'paid_by' => $translator->t('Delivery paid by'),
            'days_to_pay' => $translator->t('How many days to pay for supply'),
            'discount_from_market' => $translator->t('Discount from market price (%)'),
            'discount_from_wholesale' => $translator->t('Discount from wholesale price (%)'),
        ];
    }

    public function validateDefault($attribute, $params)
    {
        $isDefault = $this->{$attribute};

        if (!$isDefault) {
            $hasDefault = self::model()->isDefault(true)->idInNot([$this->id])->exists();

            if (!$hasDefault) {
                $this->addError($attribute, Translator::t('No other default contracts found. Choose another default contract or choose an existing one'));
            }
        }
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model SupplierContractRecord */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name);
        $criteria->compare($alias . '.is_default', $model->is_default);
        $criteria->compare($alias . '.not_delivered_penalty_percent', $model->not_delivered_penalty_percent);
        $criteria->compare($alias . '.delivery_working_days', $model->delivery_working_days);
        $criteria->compare($alias . '.add_delivery_working_days', $model->add_delivery_working_days);

        return $provider;
    }

    /**
     * This method is invoked after saving a record successfully.
     * The default implementation raises the {@link onAfterSave} event.
     * You may override this method to do postprocessing after record saving.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterSave()
    {
        //update default options
        if ($this->is_default) {
            $hasOthersDefaults = self::model()->idInNot([$this->id])->isDefault(true)->exists();

            if ($hasOthersDefaults) {
                $this->updateAll(['is_default' => 0], 'id!=:ID', [':ID' => $this->id]);
            }
        }

        parent::afterSave();
    }

    /**
     * @param bool|int $value
     *
     * @return $this
     */
    public function isDefault($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.is_default' => $value]);

        return $this;
    }


    /* API */
    /**
     * @param float $priceNotDelivery сумма недоставленных товаров
     * @param int $countProblemProducts кол-во товаров по которым необходим штраф
     *
     * @return float
     */
    public function getPenaltyAmount($priceNotDelivery, $countProblemProducts)
    {
        throw new \RuntimeException('Penalty cost in deprecated');

        $priceNotDelivery = Cast::toFloat($priceNotDelivery);
        $countProblemProducts = Cast::toUInt($countProblemProducts);

        $penaltyPercent = floor(($this->not_delivered_penalty_percent / 100) * $priceNotDelivery);
        $penaltyProducts = $countProblemProducts * $this->not_delivered_penalty_cost;

        switch ($this->_strategy) {
            case self::STRATEGY_ALL_PENALTY:
                $penaltyAmount = $penaltyPercent + $penaltyProducts;
                break;
            case self::STRATEGY_MIN_PENALTY:
                $amount = min(array_filter([$penaltyPercent, $penaltyProducts]));
                $penaltyAmount = $amount > 0 ? $amount : 0;
                break;

            //DEFAULT STRATEGY MAX PENALTY
            default:
                $penaltyAmount = max($penaltyPercent, $penaltyProducts);
        }

        return $penaltyAmount;
    }

    /**
     * Конечная дата поставки
     *
     * @param int $startDelivery unix timestamp
     *
     * @return int unix timestamp
     */
    public function getLastDeliveryDayTimestamp($startDelivery)
    {
        $startDelivery = Cast::toUInt($startDelivery);
        $deliveryWorkingDays = Cast::toUInt($this->delivery_working_days);

        $endDateTime = new DateTime();
        $endDateTime->setTimestamp($startDelivery);
        $endDateTime->modify("today");
        $endDateTime->modify("+$deliveryWorkingDays weekdays");

        return $endDateTime->getTimestamp();
    }

    /**
     * Конечная дата допоставки, включая саму поставку
     *
     * @param int $startDelivery unix timestamp
     *
     * @return int unix timestamp
     */
    public function getLastAddDeliveryDayTimestamp($startDelivery)
    {
        $endDeliveryDayTimestamp = $this->getLastDeliveryDayTimestamp($startDelivery);

        if ($this->add_delivery_working_days == 0) {
            return $endDeliveryDayTimestamp;
        }

        $deliveryWorkingDays = Cast::toUInt($this->add_delivery_working_days);

        $endDateTime = new DateTime();
        $endDateTime->setTimestamp($endDeliveryDayTimestamp);
        $endDateTime->modify("+$deliveryWorkingDays weekdays");

        return $endDateTime->getTimestamp();
    }

    /**
     * @param int $startDelivery начало поставки
     * @param bool $addDeliverySupplier включая допоставку
     * @param int|null $currentTime текущая дата
     *
     * @return int days available to delivery
     */
    public function getAvailableDeliveryDays($startDelivery, $addDeliverySupplier = true, $currentTime = null)
    {
        if ($addDeliverySupplier) {
            $endDeliveryDayTimestamp = $this->getLastAddDeliveryDayTimestamp($startDelivery);
        } else {
            $endDeliveryDayTimestamp = $this->getLastDeliveryDayTimestamp($startDelivery);
        }

        $dateTime = new DateTime();

        if (is_scalar($currentTime)) {
            $dateTime->setTimestamp(intval($currentTime));
        }
        $dateTime->modify('today');

        if ($dateTime->getTimestamp() >= $endDeliveryDayTimestamp) {
            return 0;
        }

        $endDateTime = new DateTime();
        $endDateTime->setTimestamp($endDeliveryDayTimestamp);
        $endDateTime->modify('today');
        $interval = $dateTime->diff($endDateTime);

        return $interval ? $interval->d : 0;
    }

    /**
     * Возврат даты оплаты поставщику при полной поставке
     *
     * @param int $endDelivery unix timestamp
     *
     * @return int
     */
    public function getPaymentFullDeliveryTimestamp($endDelivery)
    {
        $startDelivery = Cast::toUInt($endDelivery);
        $workingDays = Cast::toUInt($this->payment_after_full_delivery_working_days);

        $endDateTime = new DateTime();
        $endDateTime->setTimestamp($startDelivery);
        $endDateTime->modify("today");
        $endDateTime->modify("+$workingDays weekdays");

        return $endDateTime->getTimestamp();
    }

    /**
     * Возврат даты оплаты поставщику при НЕ полной поставке
     *
     * @param int $endDelivery unix timestamp
     *
     * @return int
     */
    public function getPaymentNotFullDeliveryTimestamp($endDelivery)
    {
        $startDelivery = Cast::toUInt($endDelivery);
        $workingDays = Cast::toUInt($this->payment_after_not_full_delivery_working_days);

        $endDateTime = new DateTime();
        $endDateTime->setTimestamp($startDelivery);
        $endDateTime->modify("today");
        $endDateTime->modify("+$workingDays weekdays");

        return $endDateTime->getTimestamp();
    }
}
