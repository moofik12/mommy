<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;

/**
 * Class UserDistributionRecord
 *
 * @property-read string $id
 * @property string $user_id
 * @property string $type
 * @property integer $is_subscribe
 * @property integer $is_update
 * @property string $created_at
 * @property string $updated_at
 * @property-read $typeReplacement string
 * @property-read $user UserRecord
 * @mixin Timestampable
 */
class UserDistributionRecord extends DoctrineActiveRecord
{
    const TYPE_EVERY_DAY = 0; //каждый день
    const TYPE_EVERY_WEEK = 1; //каждую неделю

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['is_subscribe', 'isSubscribe', 'setIsSubscribe', 'isSubscribe'];
        yield ['is_update', 'isUpdate', 'setIsUpdate', 'isUpdate'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users_distributions';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['is_subscribe, is_update', 'numerical', 'integerOnly' => true],
            ['type', 'in', 'range' => array_keys(self::typeReplacements())],
            ['id, user_id, type, is_subscribe, is_update, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            'user' => [self::HAS_ONE, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('common', '№'),
            'user_id' => \Yii::t('common', 'User'),
            'type' => \Yii::t('common', 'Newsletter'),
            'is_subscribe' => \Yii::t('common', 'Signed'),
            'is_update' => \Yii::t('common', 'You must update on the service'),

            //replacements
            'typeReplacement' => \Yii::t('common', 'Newsletter'),

            'created_at' => \Yii::t('common', 'Created'),
            'updated_at' => \Yii::t('common', 'Updated on'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('type', $this->type);
        $criteria->compare('is_subscribe', $this->is_subscribe);
        $criteria->compare('is_update', $this->is_update);
        $criteria->compare('created_at', $this->created_at);
        $criteria->compare('updated_at', $this->updated_at);

        return $provider;
    }

    /**
     * @return array
     */
    public static function typeReplacements()
    {
        return [
            self::TYPE_EVERY_DAY => \Yii::t('common', 'Daily dispatch, individual offers, gifts and bonuses'),
            self::TYPE_EVERY_WEEK => \Yii::t('common', 'Individual offers'),
        ];
    }

    /**
     * @param string $default
     *
     * @return mixed|string
     */
    public function getTypeReplacement($default = '')
    {
        $replacements = self::typeReplacements();
        return isset($replacements[$this->type]) ? $replacements[$this->type] : $default;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function type($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.type' => $value,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isSubscribe($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_subscribe' => $value,
        ]);

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isUpdate($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_update' => $value,
        ]);

        return $this;
    }
}
