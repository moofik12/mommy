<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\MailerServices\MailerServiceUserModel;

/**
 * Class MailingDataRecord
 *
 * @property-read integer $id
 * @property integer $distribution_event_id
 * @property string $anonymous_id
 * @property string $user_id
 * @property string $name
 * @property string $email
 * @property string $source
 * @property string $country
 * @property string $product_ids
 * @property string $section_ids
 * @property boolean $is_registered
 * @property boolean $is_subscribed
 * @property string $ip_address
 * @property string $last_login_at
 * @property integer $created_at
 * @property integer $updated_at
 * @mixin Timestampable
 */
class MailingDataRecord extends DoctrineActiveRecord
{
    /**
     * @return string
     */
    public function tableName()
    {
        return 'mailing_data';
    }

    /**
     * @param string $className
     *
     * @return MailingDataRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['anonymous_id', 'getAnonymousId', 'setAnonymousId', 'anonymousId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['source', 'getSource', 'setSource', 'source'];
        yield ['country', 'getCountry', 'setCountry', 'country'];
        yield ['products_ids', 'getProductsIds', 'setProductsIds', 'productsIds'];
        yield ['categories_ids', 'getCategoriesIds', 'setCategoriesIds', 'categoriesIds'];
        yield ['is_registered', 'isRegistered', 'setIsRegistered', 'isRegistered'];
        yield ['is_subscribed', 'isSubscribed', 'setIsSubscribed', 'isSubscribed'];
        yield ['ip_address', 'getIpAddress', 'setIpAddress', 'ipAddress'];
        yield ['last_login_date', 'getLastLoginDate', 'setLastLoginDate', 'lastLoginDate'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['distribution_event_id', 'getDistributionEventId', 'setDistributionEventId', 'distributionEventId'];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'event' => [self::BELONGS_TO, MailingEventRecord::class, 'distribution_event_id'],
        ];
    }

    /**
     * Отмечает конкретного пользователя с конкретным евентом, как подписанного на группу рассылок
     *
     * @param MailingEventRecord $eventRecord
     *
     * @return bool
     * @throws \CException
     */
    public function markUserAsSubscribed(MailingEventRecord $eventRecord)
    {
        /**
         * @var MailingDataRecord[] $result
         */
        $result = $this->findAll('email=:email AND distribution_event_id=:event_id', [
            ':email' => $this->email,
            ':event_id' => $eventRecord->id,
        ]);

        foreach ($result as $record) {
            $record->is_subscribed = true;
            $record->save();
        }

        return true;
    }

    /**
     * Отмечает пользователей подписанными на группу рассылок
     *
     * @param MailerServiceUserModel[] $mailerUsers
     * @param int $eventId
     *
     * @throws \CException
     */
    public function markMailerUsersAsSubscribed(array $mailerUsers, int $eventId)
    {
        if (count($mailerUsers) === 0) {
            return;
        }

        $emailString = $this->extractEmailsFromMailerUsers($mailerUsers);

        \Yii::app()->db->createCommand()->update(
            $this->getTableSchema()->name,
            [
                'is_subscribed' => true,
            ],
            'email IN (' . $emailString . ') AND distribution_event_id=' . $eventId
        );
    }

    /**
     * Отписывает конкретного пользователя от всех групп рассылок
     *
     * @return bool
     * @throws \CException
     */
    public function markUserAsUnsubscribed()
    {
        /**
         * @var MailingEventRecord[] $result
         */
        $result = $this->findAll('email=:email', [
            ':email' => $this->email,
        ]);

        foreach ($result as $record) {
            $record->is_subscribed = false;
            $record->save();
        }

        return true;
    }

    /**
     * Проверяет подписан ли пользователь с конкретным евентом на группу рассылок
     *
     * @param MailingEventRecord $eventRecord
     *
     * @return bool
     * @throws \CException
     */
    public function checkUserWithEventSubscribed(MailingEventRecord $eventRecord)
    {
        $result = $this->find('user_id=:user_id AND distribution_event_id=:event_id', [
            ':email' => $this->email,
            ':distribution_event_id' => $eventRecord->id,
        ]);

        return (boolean)($result->is_subscribed ?? false);
    }

    /**
     * Проверяет подписан ли конкретный пользователь на какие-либо группы рассылок
     *
     * @param string email
     *
     * @return bool
     * @throws \CException
     */
    public function checkUserSubscribed()
    {
        $result = $this->find('email=:email', [
            ':email' => $this->email,
        ]);

        return (boolean)($result->is_subscribed ?? false);
    }

    /**
     * Возвращает список пользователей с заполненным email, которые не подписаны на рассылки
     *
     * @return MailingDataRecord[]
     */
    public function getUnsubscribedUsers()
    {
        return $this->findAll('TRIM(email) <> "" AND is_subscribed IS NOT TRUE');
    }

    /**
     * @param string $email
     *
     * @return MailingDataRecord[]
     */
    public function getUserRecordsByEmail(string $email)
    {
        return $this->findAll('email=:email', [
            ':email' => $email,
        ]);
    }

    /**
     * @param MailingEventRecord $event
     * @param int $userId
     * @param string $email
     * @param string $anonymousId
     *
     * @return MailingDataRecord
     */
    public function getModelFromParams(
        MailingEventRecord $event,
        $userId,
        $email = '#',
        $anonymousId = '#'
    ): MailingDataRecord
    {
        return MailingDataRecord::model()
                ->withId($userId)
                ->find() ??
            MailingDataRecord::model()
                ->withEmail($email)
                ->withEvent($event->id)
                ->find() ??
            MailingDataRecord::model()
                ->withAnonymousId($anonymousId)
                ->withEvent($event->id)
                ->find() ??
            new MailingDataRecord();
    }

    /**
     * @return mixed
     */
    public function getHighPriorityRecords()
    {
        return \Yii::app()->db->createCommand()
            ->select('email, MAX(priority) AS max_priority')
            ->from('mailing_data')
            ->join('mailing_events m', 'distribution_event_id = m.id')
            ->where('email IS NOT null')
            ->group('email')
            ->having('COUNT(email) > 1')
            ->queryAll();
    }

    /**
     * Оставляет в БД записи с наиболее высоким приоритетом для каждого пользователя
     */
    public function deleteLowPriorityRecords()
    {
        $dataRecords = MailingDataRecord::model()->getHighPriorityRecords();
        $events = MailingEventRecord::model()->getPrioritiesArray();

        foreach ($dataRecords as $dataRecord) {
            MailingDataRecord::model()
                ->deleteAll('email=:email AND distribution_event_id<>:event_id', [
                    ':email' => $dataRecord['email'],
                    ':event_id' => $events[$dataRecord['max_priority']]->id,
                ]);
        }
    }

    /**
     * @param $eventId
     *
     * @return $this
     */
    public function withEvent($eventId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "distribution_event_id='" . $eventId . "'",
        ]);

        return $this;
    }

    /**
     * @param $email
     *
     * @return $this
     */
    public function withEmail($email)
    {
        if (null !== $email) {
            $this->getDbCriteria()->mergeWith([
                'condition' => "email LIKE '" . $email . "%'",
            ]);
        }

        return $this;
    }

    /**
     * @param $email
     *
     * @return $this
     */
    public function withEmailNotNull()
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "email IS NOT NULL",
        ]);

        return $this;
    }

    /**
     * @param int $userId
     *
     * @return $this
     */
    public function withId($userId = 0)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "user_id='" . $userId . "'",
        ]);

        return $this;
    }

    /**
     * @param $anonymousId
     *
     * @return $this
     */
    public function withAnonymousId($anonymousId)
    {
        if (null !== $anonymousId) {
            $this->getDbCriteria()->mergeWith([
                'condition' => "anonymous_id='" . $anonymousId . "'",
            ]);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function notSubscribed()
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => "is_subscribed IS NULL",
        ]);

        return $this;
    }

    /**
     * @param MailerServiceUserModel[] $mailerUsers
     * @param string $keyCol
     *
     * @return array
     */
    public function fromMailerUserArray(array $mailerUsers, string $keyCol = ''): array
    {
        $emailsString = $this->extractEmailsFromMailerUsers($mailerUsers);

        $criteria = $this->getDbCriteria();
        $criteria->mergeWith([
            'condition' => "email IN (" . $emailsString . ")",
        ]);

        $results = $this->findAll();

        if (!empty($keyCol)) {
            $arrayResults = [];
            foreach ($results as $result) {
                $arrayResults[$result[$keyCol]] = $result;
            }

            return $arrayResults;
        }

        return $results;
    }

    /**
     * @return MailerServiceUserModel[]
     */
    public function toMailerUserModels(): array
    {
        /**
         * @var MailingDataRecord[] $records
         */
        $records = $this->findAll();
        $result = [];

        foreach ($records as $record) {
            $mailerServiceUser = new MailerServiceUserModel();

            if (is_null($record->email)) {
                continue;
            }

            $mailerServiceUser->email = $record->email;
            $mailerServiceUser->isSubscribed = $record->is_subscribed;
            $result[] = $mailerServiceUser;
        }

        return $result;
    }

    /**
     * @param array $mailerUsers
     *
     * @return bool|string
     */
    private function extractEmailsFromMailerUsers(array $mailerUsers)
    {
        $emailsString = "";
        foreach ($mailerUsers as $user) {
            $emailsString .= "'" . $user->email . "',";
        }

        return substr($emailsString, 0, -1);
    }
}
