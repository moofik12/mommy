<?php

namespace MommyCom\Model\Db;

/**
 * Class OrderProductStorekeeperHistoryRecord
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $status
 * @property integer $status_prev
 * @property string $comment
 * @property integer $user_id
 * @property integer $client_id
 * @property integer $time
 * @property-read string $statusReplacement
 * @property-read string $statusPrevReplacement
 * @property-read OrderRecord $product
 * @property-read AdminUserRecord $user
 * @property-read UserRecord $client
 */
class OrderProductStorekeeperStatusHistoryRecord extends OrderProductCallcenterStatusHistoryRecord
{
    /**
     * @param string $className
     *
     * @return OrderProductStorekeeperStatusHistoryRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'orders_products_storekeeper_statushistory';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = OrderProductRecord::storekeeperStatusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = OrderProductRecord::storekeeperStatusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }
}
