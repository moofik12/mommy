<?php

namespace MommyCom\Model\Db;

use CDbExpression;
use CException;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

/**
 * Class OrderProcessingStatusHistoryRecord
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $user_id admins->id
 * @property integer $status
 * @property integer $status_prev
 * @property integer $comment
 * @property integer $client_id
 * @property integer $time
 * @property-read AdminUserRecord $user
 * @property-read UserRecord $client
 * @property-read OrderRecord $order
 * @property-read string $statusReplacement
 * @property-read string $statusPrevReplacement
 */
class OrderProcessingStatusHistoryRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return OrderProcessingStatusHistoryRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['time', 'getTime', 'setTime', 'time'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['client_id', 'getClientId', 'setClientId', 'clientId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
    }

    public function tableName()
    {
        return 'orders_processing_statushistory';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'client' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'client_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['status, status_prev, order_id', 'required'],
            ['user_id, status, status_prev, client_id, order_id', 'numerical', 'integerOnly' => true],
            ['comment', 'type', 'type' => 'string'],
        ];
    }

    public function _resolveTime()
    {
        if (empty($this->time)) {
            $this->time = new CDbExpression('UNIX_TIMESTAMP()');
        }
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        $this->_resolveTime();
        try { // for console app
            if (empty($this->user_id)) {
                $this->user_id = Yii::app()->user->id;
            }
        } catch (CException $e) {
        }

        return true;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function statusPrev($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status_prev' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusPrevIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'status_prev', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function clientId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'client_id' => $value,
        ]);
        return $this;
    }

    // item api

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        return OrderRecord::processingStatusReplacements();
    }
}
