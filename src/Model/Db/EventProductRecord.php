<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use CLogger;
use Exception;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Model\Product\SizeFormats;
use MommyCom\Model\ViewsTracking\ViewsTracking;
use MommyCom\Model\ViewsTracking\ViewTrackingByUserRecord;
use MommyCom\Model\ViewsTracking\ViewTrackingRecord;
use MommyCom\Model\ViewsTracking\ViewTrackingSimilarRecord;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class EventProductRecord
 *
 * @property-read integer $id
 * @property integer $event_id
 * @property integer $view_count
 * @property integer $max_per_buy
 * @property integer $supplier_id
 * @property integer $product_id
 * @property-read string $unique_article
 * @property float $price стоимость продажи
 * @property float $price_market среднерыночная цена
 * @property float $price_purchase стоимость закупки
 * @property $number - ожидаемое количество на продажу
 * @property $number_arrived - количество которое прибыло на склад
 * @property $number_sold - количество проданного товара (сток вычитается)
 * @property $number_sold_real - количество проданного товара (без вычита стока)
 * @property bool $is_sold_out - проданы ли все товары включая забронированные в корзине
 * @property integer $status
 * @property string $article
 * @property integer $brand_id
 * @property string $name
 * @property string $category
 * @property string $description
 * @property string $barcode
 * @property string $internal_code внутренний код поставщика
 * @property integer $age_from
 * @property integer $age_to
 * @property string $size
 * @property string $sizeformat
 * @property string $size_guide размерная сетка
 * @property string $sizes_json другие возможные интерпретации размеров
 * @property array[] $sizes другие возможные интерпретации размеров
 * @property string $label размер на этикетке
 * @property string $composition состав
 * @property string $shelflife срок годности
 * @property string $color
 * @property integer $color_code
 * @property string $made_in
 * @property string $design_in
 * @property string $weight
 * @property string $dimensions
 * @property integer $number_supplied
 * @property string $photo список файлов которые содержат фото для товара (ассортиментная таблица)
 * @property array $target @see ProductTargets
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $certificate
 * @property-read string $idAligned
 * @property-read BrandRecord $brand
 * @property-read SupplierRecord $supplier
 * @property-read EventRecord $event
 * @property-read ProductRecord $product
 * @property-read ViewTrackingRecord $views
 * @property-read integer $numberAvailable
 * @property-read integer $stockNumber количество такого-же товара из других акций которое просто пылиться на складе
 * @property-read integer $numberSold
 * @property-read integer $numberSoldReal
 * @property-read string $sizeformatReplacement
 * @property-read string $statusReplacement
 * @property-read string $weightReplacement
 * @property-read array ageRange
 * @property-read string ageRangeReplacement
 * @property-read array ageGroups
 * @property-read float $promoDiscount скидка на товар (от 0.00 до 1.00)
 * @property-read integer $promoDiscountPercent скидка на товар в процентах (от 1 до 100)
 */
class EventProductRecord extends DoctrineActiveRecord
{
    /** @var  int */
    public $sectionId;

    /** @var  int */
    public $colorCode;

    const STATUS_VIRTUAL = 0; // товар есть может быть продан
    const STATUS_ARRIVED = 1; // товар отгружен на склад и скоро будет оправлен клиентам
    const STATUS_STOCK = 2;

    const MAX_PER_BUY_DEFAULT = 10;
    const MAX_PER_BUY = 30;

    /**
     * @param string $className
     *
     * @return EventProductRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['stock_id', 'getStockId', 'setStockId', 'stockId'];
        yield ['view_count', 'getViewCount', 'setViewCount', 'viewCount'];
        yield ['max_per_buy', 'getMaxPerBuy', 'setMaxPerBuy', 'maxPerBuy'];
        yield ['unique_article', 'getUniqueArticle', 'setUniqueArticle', 'uniqueArticle'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['category', 'getCategory', 'setCategory', 'category'];
        yield ['price', 'getPrice', 'setPrice', 'price'];
        yield ['price_market', 'getPriceMarket', 'setPriceMarket', 'priceMarket'];
        yield ['price_purchase', 'getPricePurchase', 'setPricePurchase', 'pricePurchase'];
        yield ['number', 'getNumber', 'setNumber', 'number'];
        yield ['number_arrived', 'getNumberArrived', 'setNumberArrived', 'numberArrived'];
        yield ['number_sold', 'getNumberSold', 'setNumberSold', 'numberSold'];
        yield ['number_sold_real', 'getNumberSoldReal', 'setNumberSoldReal', 'numberSoldReal'];
        yield ['is_sold_out', 'isSoldOut', 'setIsSoldOut', 'isSoldOut'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['article', 'getArticle', 'setArticle', 'article'];
        yield ['barcode', 'getBarcode', 'setBarcode', 'barcode'];
        yield ['internal_code', 'getInternalCode', 'setInternalCode', 'internalCode'];
        yield ['label', 'getLabel', 'setLabel', 'label'];
        yield ['images', 'getImages', 'setImages', 'images'];
        yield ['age_to', 'getAgeTo', 'setAgeTo', 'ageTo'];
        yield ['age_from', 'getAgeFrom', 'setAgeFrom', 'ageFrom'];
        yield ['sizeformat', 'getSizeformat', 'setSizeformat', 'sizeformat'];
        yield ['size', 'getSize', 'setSize', 'size'];
        yield ['sizes_json', 'getSizesJson', 'setSizesJson', 'sizesJson'];
        yield ['size_guide', 'getSizeGuide', 'setSizeGuide', 'sizeGuide'];
        yield ['color', 'getColor', 'setColor', 'color'];
        yield ['color_code', 'getColorCode', 'setColorCode', 'colorCode'];
        yield ['made_in', 'getMadeIn', 'setMadeIn', 'madeIn'];
        yield ['design_in', 'getDesignIn', 'setDesignIn', 'designIn'];
        yield ['composition', 'getComposition', 'setComposition', 'composition'];
        yield ['shelflife', 'getShelflife', 'setShelflife', 'shelflife'];
        yield ['weight', 'getWeight', 'setWeight', 'weight'];
        yield ['dimensions', 'getDimensions', 'setDimensions', 'dimensions'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['target', 'getTarget', 'setTarget', 'target'];
        yield ['photo', 'getPhoto', 'setPhoto', 'photo'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['number_supplied', 'getNumberSupplied', 'setNumberSupplied', 'numberSupplied'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
        yield ['brand_id', 'getBrandId', 'setBrandId', 'brandId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['certificate', 'getCertificate', 'setCertificate', 'certificate'];
    }

    public function tableName()
    {
        return 'events_products';
    }

    public function relations()
    {
        return [
            // here
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'product' => [self::BELONGS_TO, 'MommyCom\Model\Db\ProductRecord', 'product_id'],
            'brand' => [self::BELONGS_TO, 'MommyCom\Model\Db\BrandRecord', 'brand_id'],
            'supplier' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierRecord', 'supplier_id'],

            'views' => [self::HAS_ONE, ViewTrackingRecord::class, ['object_id' => 'product_id'], 'scopes' => [ // просмотры связываются с моделью твоара а не товаром акции
                'objectType' => ViewsTracking::getObjectType('MommyCom\Model\Db\ProductRecord'),
            ]],
            'similar' => [self::HAS_ONE, ViewTrackingSimilarRecord::class, ['object_id' => 'product_id'], 'scopes' => [ // просмотры связываются с моделью твоара а не товаром акции
                'objectType' => ViewsTracking::getObjectType('MommyCom\Model\Db\ProductRecord'),
            ]],
            'viewsByUser' => [self::HAS_ONE, ViewTrackingByUserRecord::class, ['object_id' => 'product_id'], 'scopes' => [ // просмотры связываются с моделью твоара а не товаром акции
                'objectType' => ViewsTracking::getObjectType('MommyCom\Model\Db\ProductRecord'),
            ]],
        ];
    }

    public function rules()
    {
        return [
            // connection rules
            ['event_id', 'required'],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            // array('product_id', 'exist', 'className' => 'ProductRecord', 'attributeName' => 'product_id'),- no need, automatically created

            ['max_per_buy', 'default', 'value' => self::MAX_PER_BUY_DEFAULT],
            ['max_per_buy', 'numerical', 'min' => 1, 'max' => self::MAX_PER_BUY],

            ['view_count', 'numerical', 'min' => 0, 'max' => PHP_INT_MAX],

            // amount rules

            ['number', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => PHP_INT_MAX],
            ['number_arrived, number_sold, number_sold_real', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => PHP_INT_MAX],

            ['is_sold_out', 'boolean'],

            // product rules

            ['name, article, article, category', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name, brand_id, article, category', 'required'],
            ['article', 'length', 'max' => 45],
            ['name', 'length', 'max' => 250],

            ['price_purchase, price_market, price', 'required'],
            ['price_purchase, price_market, price', 'filter', 'filter' => [Cast::class, 'toUFloat']],
            ['price_purchase, price_market, price', 'numerical', 'min' => 1, 'max' => 1000000000], // million is enough

            ['price_purchase', 'compare', 'compareAttribute' => 'price_purchase', 'operator' => '<='],
            ['price', 'compare', 'compareAttribute' => 'price_market', 'operator' => '<='],

            //array('sizeformat, size', 'required'),
            ['sizeformat', 'in', 'range' => SizeFormats::instance()->getList()],
            ['size', 'length', 'max' => 15],

            ['size_guide', 'type', 'type' => 'string'],

            ['shelflife, dimensions, weight', 'length', 'min' => 0, 'max' => 255, 'encoding' => '8bit'],

            [
                'id, product_id, event_id, name, category, status, color, color_code, ' .
                'sizeformat, size, size_guide, target, article, made_in, design_in, ' .
                'created_at, updated_at',

                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'product_id' => \Yii::t($category, '№ of product in the catalog'),
            'name' => \Yii::t($category, 'Name'),
            'brand.name' => \Yii::t($category, 'Brand'),
            'supplier.name' => \Yii::t($category, 'Supplier'),
            'category' => \Yii::t($category, 'Category'),
            'max_per_buy' => \Yii::t($category, 'Max. for purchase'),

            'event_id' => \Yii::t($category, 'Event'),
            'supplier_id' => \Yii::t($category, 'Supplier'),

            'price' => \Yii::t($category, 'Cost'),
            'price_market' => \Yii::t($category, 'Market price'),
            'price_purchase' => \Yii::t($category, 'Basic price'),

            'number' => \Yii::t($category, 'amount'),
            'number_arrived' => \Yii::t($category, 'Quantity (supplied)'),
            'number_sold' => \Yii::t($category, 'Sold out'),
            'number_sold_real' => \Yii::t($category, 'Sold out with stock'),
            'is_sold_out' => \Yii::t($category, 'All products are sold out'),

            'status' => \Yii::t($category, 'Status'),

            'article' => \Yii::t($category, 'Stock number'),

            'color' => \Yii::t($category, 'Colour'),
            'color_code' => \Yii::t($category, 'Color code'),
            'target' => \Yii::t($category, 'Target audience'),

            'age_from' => \Yii::t($category, 'Age from'),
            'age_to' => \Yii::t($category, 'Age before'),

            'sizeformat' => \Yii::t($category, 'Size type'),
            'size' => \Yii::t($category, 'Size'),
            'size_guide' => \Yii::t($category, 'Dimension mesh'),
            'label' => \Yii::t($category, 'Label'),

            'made_in' => \Yii::t($category, 'Production'),
            'design_in' => \Yii::t($category, 'Design'),

            'composition' => \Yii::t($category, 'Composition'),
            'shelflife' => \Yii::t($category, 'Shelf life'),
            'weight' => \Yii::t($category, 'Weight'),
            'dimensions' => \Yii::t($category, 'Dimensions of'),

            'weightReplacement' => \Yii::t($category, 'Weight'),

            'numberAvailable' => \Yii::t($category, 'Available for purchase'),
            'sizeformatReplacement' => \Yii::t($category, 'Size type'),
            'statusReplacement' => \Yii::t($category, 'Stock'),
            'photo' => \Yii::t($category, 'Photo'),

            'age' => \Yii::t($category, 'Age'),
            'ageRangeReplacement' => \Yii::t($category, 'Age'),
            'ageGroups' => \Yii::t($category, 'Age groups'),

            'created_at' => \Yii::t($category, 'Addition date'),
            'updated_at' => \Yii::t($category, 'Updating date'),
        ];
    }

    protected function _serializeTarget()
    {
        $this->target = !empty($this->target) && is_array($this->target) ? implode(',', $this->target) : '';
    }

    protected function _unserializeTarget()
    {
        $this->target = !empty($this->target) && is_string($this->target) ? explode(',', $this->target) : [];
    }

    protected function _resolveStatus()
    {
        if (self::STATUS_STOCK === $this->status) {
            return;
        }

        $this->status = $this->number_arrived == 0 ? self::STATUS_VIRTUAL : self::STATUS_ARRIVED;
    }

    protected function _resolveUniqueArticle()
    {
        $this->unique_article = ProductRecord::generateUniqueArticle($this->article, $this->brand_id, $this->color);
    }

    protected function _resolveProduct()
    {
        if (!$this->isNewRecord) {
            $product = $this->getRelated('product', true);
            /* @var $product ProductRecord|null */

            if ($product !== null && $product->unique_article == $this->unique_article) {
                return;
            }
        }

        static $productsCache = [];
        if (!isset($productsCache[$this->unique_article])) {
            $productsCache[$this->unique_article] = ProductRecord::model()->uniqueArticle($this->unique_article)->find();
        }

        $product = $productsCache[$this->unique_article];
        $productChanged = false;
        if ($product === null) { // copy base data
            $product = new ProductRecord();

            $product->brand_id = $this->brand_id;
            $product->article = $this->article;
            $product->color = $this->color;

            $product->name = $this->name;
            $product->category = $this->category;
            $product->made_in = $this->made_in;
            $product->design_in = $this->design_in;
            $product->description = $this->description;
            $product->age_from = $this->age_from;
            $product->age_to = $this->age_to;
            $product->target = $this->target;
            $product->photo = $this->photo;
            $product->color_code = $this->color_code;
            $product->section_id = $this->sectionId;
            try {
                if (!$product->save()) {
                    throw new CException(print_r($product->getErrors(), true));
                }
            } catch (Exception $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, 'application.db.EventProductRecord._resolveProduct');
                throw new CException($e->getMessage());
            }
        } else {
            if ($product->color_code == 0) {
                $productChanged = true;
                if ($this->colorCode > 0) {
                    $product->color_code = $this->colorCode;
                } else {
                    $product->color_code = ProductRecord::bindingColorCode($this->color);
                }
            } else {
                if ($this->colorCode > 0) {
                    $productChanged = true;
                    $product->color_code = $this->colorCode;
                } elseif ($product->color != $this->color) {
                    $productChanged = true;
                    $product->color_code = ProductRecord::bindingColorCode($this->color);
                }
            }
            $product->color = $this->color;

            if ($product->section_id == 0) {
                $productChanged = true;
                if ($this->sectionId > 0) {
                    $product->section_id = $this->sectionId;
                } else {
                    $product->section_id = ProductRecord::bindingSectionByName($product->name);
                }
            } else {
                if ($this->sectionId > 0) {
                    $productChanged = true;
                    $product->section_id = $this->sectionId;
                } elseif ($product->name) {
                    $productChanged = true;
                    $product->section_id = ProductRecord::bindingSectionByName($product->name);
                }
            }
        }

        $this->color_code = 0;
        $this->product_id = $product->id;

        // update product ages
        if (static::model()->productId($product->id)->count() > 0) {
            $ageFrom = min(
                $this->age_from,
                static::model()->productId($product->id)->findColumnMin('age_from') // у YII глупость, что нельзя реюзать цепочки
            );
            $ageTo = max(
                $this->age_to,
                static::model()->productId($product->id)->findColumnMax('age_to')
            );
        } else {
            $ageFrom = $this->age_from;
            $ageTo = $this->age_to;
        }

        if ($product->age_from != $ageFrom || $product->age_to != $ageTo) {
            $product->age_from = $ageFrom;
            $product->age_to = $ageTo;
            $productChanged = true;
        }

        // need to be fixed, buggy
        $targets = ProductTargets::instance()->mergeTargets($this->target, $product->target);
        if (count(array_diff($targets, (array)$product->target)) > 0) {
            $product->target = $targets;
            $productChanged = true;
        }

        if (!empty($this->photo) && $product->photo !== $this->photo) {
            $product->photo = $this->photo;
            $productChanged = true;
        }

        // now save it all!
        if ($productChanged) {
            if (!$product->save()) {
                //var_dump($product->getErrors()); exit;
            };
        }
    }

    protected function _refreshProductNumberSold()
    {
        if ($this->isNewRecord) {
            return;
        }

        $this->forceRefreshSoldNumber();
    }

    protected function _resolveEventStatus()
    {
        $status = $this->event->status;
        $this->event->forceResolveStatus(); // резолвим статус
        if ($status !== $this->event->status) {
            $this->event->save();
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->_unserializeTarget();
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_resolveUniqueArticle();
        $this->_resolveProduct();
        $this->_refreshProductNumberSold();
        $this->_resolveStatus();

        $this->_serializeTarget();
        $this->updateIsSoldOutNumber(false);
        return true;
    }

    public function afterSave()
    {
        parent::afterSave();

        $this->_resolveEventStatus();
    }

    public function beforeDelete()
    {
        if (!$result = parent::beforeDelete()) {
            return $result;
        }

        if ($this->number_sold_real > 0 || $this->number_sold > 0) {
            return false;
        }

        return true;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function sizeformat($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'sizeformat' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function productId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'product_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function productIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'product_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function productIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.' . 'product_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function productIdNot($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'product_id' . '<>' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function eventId($value)
    {
        $value = Cast::toInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'event_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function eventIdIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.event_id', $values);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function category($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'category' => $value,
        ]);
        return $this;
    }

    /**
     * @param string[] $values
     *
     * @return static
     */
    public function categories($values)
    {
        $values = Cast::toStrArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.category', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function brandId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'brand_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param int[] $values
     *
     * @return static
     */
    public function brandIds(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.' . 'brand_id', $values);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function size($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'size' => $value,
        ]);
        return $this;
    }

    /**
     * @param string[] $values
     *
     * @return static
     */
    public function sizes(array $values)
    {
        $values = Cast::toStrArr($values);

        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.' . 'size', $values);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function color($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'color' => $value,
        ]);

        return $this;
    }

    /**
     * @param string[] $values
     *
     * @return static
     */
    public function colors($values)
    {
        $values = Cast::toStrArr($values);

        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.' . 'color', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function ageFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'age_from' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function ageTo($value)
    {
        $value = Cast::toUInt($value);
        $value = $this->dbConnection->pdoInstance->quote($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'age_to' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function ageBetween($from, $to)
    {
        $this->ageFrom($from);
        $this->ageTo($to);
        return $this;
    }

    /**
     * Фильтрация по группе возраста
     *
     * @param string $value
     *
     * @return static
     */
    public function ageGroup($value)
    {
        $value = Cast::toStr($value);
        $range = AgeGroups::instance()->getAgeRange($value);
        return $this->ageBetween($range[0], $range[1]);
    }

    /**
     * Фильтрация по группам возрастов
     *
     * @param string[] $values
     *
     * @return static
     */
    public function ageGroups($values)
    {
        $values = Cast::toStrArr($values);
        foreach ($values as $value) {
            $this->ageGroup($value);
        }
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function target($value)
    {
        $value = Cast::toStr($value);
        $value = $this->dbConnection->pdoInstance->quote($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition('FIND_IN_SET(' . $value . ', ' . $alias . '.' . 'target' . ')');
        return $this;
    }

    /**
     * Только те товары которые доступны для продажи
     * в случае если акция стоковая то возвращает все ее товары
     *
     * @return static
     */
    public function onlyStockAvailable()
    {
        $alias = $this->getTableAlias();

        $query = '(';
        $query .= '(' . $alias . '.number' . '-' . $alias . '.number_sold_real' . ')' . '>=' . '0';
        $query .= ' OR ';
        $query .= '(' . $alias . '.number' . '=' . '0' . ')'; // стоковая акция
        $query .= ')';

        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * Только те товары в которых не было продано ни одной единицы
     * Может потребоваться проверка поединично уже после выборки (а вдруг что-то зарезервировано?)
     *
     * @return static
     */
    public function onlyUnSold()
    {
        $alias = $this->getTableAlias();
        $query = '(' . $alias . '.number_sold_real' . ' = ' . '0' . ')';
        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * @param string[] $values
     *
     * @return static
     */
    public function targets($values)
    {
        $values = Cast::toStrArr($values);

        $alias = $this->getTableAlias();

        $criteria = $this->getDbCriteria();
        $pdo = $this->dbConnection->pdoInstance;
        $conditions = [];

        foreach ($values as $value) {
            $value = $pdo->quote($value);
            $conditions[] = 'FIND_IN_SET(' . $value . ', ' . $alias . '.' . 'target' . ')';
        }

        $criteria->mergeWith([
            'condition' => implode(' OR ', $conditions),
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value, true, 'OR');
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function idLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.id', $value, true, 'OR');
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function uniqueArticle($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'unique_article' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function articleIs($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.article' => $value,
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function articleLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addSearchCondition($alias . '.article', $value);

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @return $this
     */
    public function hasNumberSold()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.number_sold', "> 0");

        return $this;
    }

    /**
     * @return $this
     */
    public function hasNumberSoldReal()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.number_sold_real', "> 0");

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function supplierId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.supplier_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.event_id', '=' . $model->event_id);
        $criteria->compare($alias . '.product_id', '=' . $model->product_id);
        $criteria->compare($alias . '.status', '=' . $model->status);

        $criteria->compare($alias . '.article', $model->article, true);
        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.category', $model->made_in, true);
        $criteria->compare($alias . '.sizeformat', $model->sizeformat, true);
        $criteria->compare($alias . '.size', $model->size, true);
        $criteria->compare($alias . '.size_guide', $model->size_guide, true);
        $criteria->compare($alias . '.color', $model->color, true);
        $criteria->compare($alias . '.color_code', $model->color_code, true);
        $criteria->compare($alias . '.made_in', $model->made_in, true);
        $criteria->compare($alias . '.design_in', $model->design_in, true);
        $criteria->compare($alias . '.is_sold_out', $model->is_sold_out);

        $criteria->compare($alias . '.target', $model->target, true);
        if (!empty($model->target)) {
            $criteria->addCondition('FIND_IN_SET(' . $model->dbConnection->quoteValue($model->target) . ', ' . $alias . '.' . 'target' . ')');
        }
        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }


    /// item api

    /**
     * @return string
     */
    public function getIdAligned()
    {
        return str_pad($this->id, 10, '0', STR_PAD_LEFT);
    }

    /**
     * @param int $amount
     * @param boolean $checkActive
     *
     * @return bool
     */
    public function getIsPossibleToBuy($amount = 1, $checkActive = true)
    {
        $amount = intval($amount);

        if ($checkActive && !$this->event->getIsTimeActive()) {
            return false;
        }

        if ($amount < 1 || $amount > $this->max_per_buy) {
            return false;
        }

        $this->refresh();

        $available = $this->getNumberAvailable();

        return $available >= $amount;
    }

    /**
     * Количество доступных для покупки пользователями
     *
     * @param boolean $subtractReserved
     *
     * @return integer
     */
    public function getNumberAvailable($subtractReserved = true)
    {
        $available = 0;

        if ($this->event->is_stock) {
            $available += $this->getStockNumber();
        }

        $available += $this->number_arrived == 0 ? $this->number : $this->number_arrived;
        $available -= $this->number_sold;

        // также вычитаем зарезервированные
        if ($subtractReserved) {
            $reserved = CartRecord::model()
                ->eventId($this->event_id)
                ->productId($this->id)
                ->onlyReserved()
                ->findColumnSum('number');
            $available -= $reserved;
        }

        $available = Cast::toUInt($available);

        return $available;
    }

    /**
     * Количество такого-же товара которое пылится на складе
     *
     * @param boolean $excludeCurrentEvent исключить товар из текущей акции (он ведь не складской)
     *
     * @return integer
     */
    public function getStockNumber($excludeCurrentEvent = true)
    {
        $selector = WarehouseProductRecord::model()
            ->productId($this->product_id)
            ->eventProductSizeformat($this->sizeformat)
            ->eventProductSize($this->size)
            ->onlyNotHasOrderConnected()
            ->status(WarehouseProductRecord::STATUS_WAREHOUSE_STOCK)
            ->lostStatus(WarehouseProductRecord::LOST_STATUS_CORRECT);

        if ($excludeCurrentEvent) {
            $selector->eventIdNot($this->event_id);
        }

        $available = $selector->count();

        return $available;
    }

    /**
     * Количество такого-же товара которое поставщик должен доставить, заказ на него есть но возможно был отменен до поставки
     *
     * @return integer
     */
    public function getWaitingDeliveryNumber()
    {
        if (!$this->event) {
            return 0;
        } elseif ($this->event->mailing_start_at < time()) {
            return 0;
        }

        $selector = WarehouseProductRecord::model()
            ->productId($this->product_id)
            ->eventProductSizeformat($this->sizeformat)
            ->eventProductSize($this->size)
            ->onlyNotHasOrderConnected()
            ->eventId($this->event_id)
            ->status(WarehouseProductRecord::STATUS_UNMODERATED)
            ->lostStatus(WarehouseProductRecord::LOST_STATUS_CORRECT);

        $available = $selector->count();
        return $available;
    }

    /**
     * Количество которое фактически продали
     *
     * @return int
     */
    public function getNumberSoldReal()
    {
        return (int)OrderProductRecord::model()
            ->callcenterStatuses([OrderProductRecord::CALLCENTER_STATUS_UNMODERATED, OrderProductRecord::CALLCENTER_STATUS_ACCEPTED])
            ->productId($this->id)
            ->findColumnSum('number');
    }

    /**
     * Количество проданного без учета проданного напрямую со склада
     *
     * @return int
     */
    public function getNumberSold()
    {
        $sold = (int)($this->getNumberSoldReal() - WarehouseProductRecord::model()
                ->onlySoldAsStock($this->event_id)
                ->productId($this->product_id)
                ->eventProductSizeformat($this->sizeformat)
                ->eventProductSize($this->size)
                ->count());
        return max($sold, 0);
    }

    /**
     * @return $this
     */
    public function forceRefreshSoldNumber()
    {
        // количество которое продали
        $this->number_sold_real = $this->getNumberSoldReal();

        // вычитаем количество которое отправили напрямую со склада
        $this->number_sold = $this->getNumberSold();

        return $this;
    }

    /**
     * @param bool $save If true - update now
     */
    public function updateIsSoldOutNumber($save = false)
    {
        $isSoldOut = $this->getNumberAvailable() == 0;

        if ($save && $this->is_sold_out != $isSoldOut) {
            $this->is_sold_out = $isSoldOut;
            $this->updateByPk($this->id, ['is_sold_out' => Cast::toUInt($this->is_sold_out)]);
            return;
        }

        $this->is_sold_out = $isSoldOut;
    }

    /**
     * @return string
     */
    public function getSizeformatReplacement()
    {
        return SizeFormats::instance()->getLabel($this->sizeformat);
    }

    /**
     * @param boolean $includeCurrent
     *
     * @return array
     */
    public function getSizes($includeCurrent = false)
    {
        $sizes = Cast::toArr(json_decode($this->sizes_json, true, 2));

        if ($includeCurrent) {
            $sizes[$this->sizeformat] = $this->size; // обновляем значение в списке
        } else {
            unset($sizes[$this->sizeformat]); // он может быть в списке
        }

        return $sizes;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function setSizes(array $values)
    {
        $sanitized = [];
        foreach ($values as $name => $value) {
            if (SizeFormats::instance()->isValid($name)) {
                $sanitized[$name] = Cast::toStr($value);
            }
        }
        $this->sizes_json = json_encode($sanitized);
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $values = self::statusReplacements();
        return isset($values[$this->status]) ? $values[$this->status] : '';
    }

    /**
     * @return array
     */
    public function getAgeRange()
    {
        return [$this->age_from, $this->age_to];
    }

    /**
     * Возвращает вес в удобочитаемом виде
     *
     * @return string
     */
    public function getWeightReplacement()
    {
        return ProductWeight::instance()->convertToString($this->weight);
    }

    /**
     * @return string
     */
    public function getAgeRangeReplacement()
    {
        return AgeGroups::instance()->convertAgeRangeToString([$this->age_from, $this->age_to]);
    }

    /**
     * @return array
     */
    public function getAgeGroups()
    {
        return AgeGroups::instance()->rangeToGroups($this->getAgeRange());
    }

    public static function statusReplacements()
    {
        return [
            self::STATUS_VIRTUAL => \Yii::t('backend', 'Virtually'),
            self::STATUS_ARRIVED => \Yii::t('backend', 'Real'),
        ];
    }

    /**
     * Возвращает число между 0 и 1 (напр 0.3),
     * скидка в акции
     *
     * @return float
     */
    public function getPromoDiscount()
    {
        $result = 1 - $this->price / $this->price_market;

        return round($result, 2);
    }

    /**
     * @return int
     */
    public function getPromoDiscountPercent()
    {
        return (int)($this->getPromoDiscount() * 100);
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function priceAtLower($value)
    {
        $value = Cast::toFloat($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.price', "<= $value");

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function priceAtGreater($value)
    {
        $value = Cast::toFloat($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.price', ">= $value");

        return $this;
    }

    public static function replaceLangAgeSize($size)
    {
        if (strpos($size, 'months')) {
            $size = str_replace('months', \Yii::t('common', 'months'), $size);
        } elseif (strpos($size, 'month')) {
            $size = str_replace('month', \Yii::t('common', 'month'), $size);
        } elseif (strpos($size, 'years')) {
            $size = str_replace('years', \Yii::t('common', 'years'), $size);
        } elseif (strpos($size, 'year')) {
            $size = str_replace('year', \Yii::t('common', 'year'), $size);
        }

        return $size;
    }

    /**
     * @param int $variant
     *
     * @return $this
     */
    public function splitByMadeInVariant(int $variant)
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = \Yii::app()->splitTesting;

        if (!$splitTesting->isSplitTestEnabled(SplitTesting::SPLITTEST_NAME_MADE_IN)) {
            return $this;
        }

        $madeIn = $splitTesting->getMadeInCountries()[$variant];

        $this->getDbCriteria()->addInCondition($this->getTableAlias() . '.made_in', $madeIn);

        return $this;
    }
}
