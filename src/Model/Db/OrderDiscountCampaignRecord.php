<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CHtmlPurifier;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderDiscountCampaignRecord
 *
 * @property-read int $id
 * @property int $start_at
 * @property int $end_at
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property int $percent
 * @property int $cash
 * @property int $min_amount
 * @property int $max_amount
 * @property-read int $updated_at
 * @property-read int $created_at
 * @mixin Timestampable
 */
class OrderDiscountCampaignRecord extends DoctrineActiveRecord
{
    const USED_PERCENT = 'percent';
    const USED_CASH = 'cash';
    const USED_NONE = 'none';

    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['start_at', 'getStartAt', 'setStartAt', 'startAt'];
        yield ['end_at', 'getEndAt', 'setEndAt', 'endAt'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['text', 'getText', 'setText', 'text'];
        yield ['short_text', 'getShortText', 'setShortText', 'shortText'];
        yield ['percent', 'getPercent', 'setPercent', 'percent'];
        yield ['cash', 'getCash', 'setCash', 'cash'];
        yield ['min_amount', 'getMinAmount', 'setMinAmount', 'minAmount'];
        yield ['max_amount', 'getMaxAmount', 'setMaxAmount', 'maxAmount'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    public function tableName()
    {
        return 'order_discount_campaigns';
    }

    public function relations()
    {
        return [
        ];
    }

    public function rules()
    {
        return [
            ['percent', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 99],
            ['cash', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000],

            ['min_amount, max_amount', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100000],

            ['start_at, end_at', 'validateCashOrPercent', 'percentAttributeName' => 'percent'],

            ['name, text, short_text', 'length'],
            ['name, short_text', 'length', 'max' => 255, 'encoding' => false],
            ['text', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],

            ['cash', 'numerical', 'integerOnly' => true, 'min' => 0],

            ['id, type, start_at, end_at, name, text, short_text, percent, cash, min_amount, max_amount, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'start_at' => \Yii::t($category, 'Campaign starts at'),
            'end_at' => \Yii::t($category, 'Campaign ends at'),
            'name' => \Yii::t($category, 'Name'),
            'text' => \Yii::t($category, 'Text'),
            'short_text' => \Yii::t($category, 'Short description'),
            'percent' => \Yii::t($category, 'Size of discount in percents'),
            'cash' => \Yii::t($category, 'discount amount'),
            'min_amount' => \Yii::t($category, 'мин. заказ'),
            'max_amount' => \Yii::t($category, 'макс. заказ'),
            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.start_at', '>=' . $model->start_at);
        $criteria->compare($alias . '.end_at', '<=' . $model->end_at);

        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.text', $model->text, true);
        $criteria->compare($alias . '.short_text', $model->short_text, true);
        $criteria->compare($alias . '.percent', $model->percent);
        $criteria->compare($alias . '.cash', $model->cash);
        $criteria->compare($alias . '.min_amount', $model->min_amount);
        $criteria->compare($alias . '.max_amount', $model->max_amount);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateCashOrPercent($attribute, $params)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';
        $value = Cast::toUInt($this->$attribute);
        $percentAttribute = $params['percentAttributeName'];
        $percent = Cast::toUInt($this->$percentAttribute);

        if ($value == 0 && $percent == 0) {
            $this->addError($attribute, \Yii::t($category, 'You must specify the discount amount (amount or percentage)'));
            $this->addError($percentAttribute, \Yii::t($category, 'You must specify the discount amount (amount or percentage)'));
        }
    }

    /**
     * Недостающая сумма для использования компании
     *
     * @param float|int $orderPrice
     *
     * @return float
     */
    public function getAmountNotEnoughUseCampaign($orderPrice)
    {
        $amount = 0.0;
        $orderPrice = Cast::toUFloat($orderPrice);

        if ($this->min_amount > 0 && $this->min_amount > $orderPrice) {
            $amount = $this->min_amount - $orderPrice;
        }

        return ceil($amount);
    }

    /**
     * @param $orderPrice
     *
     * @return string see self::USED_...
     */
    public function getUsedType($orderPrice)
    {
        $orderPrice = Cast::toUFloat($orderPrice);
        $amount = 0.0;
        $usedType = self::USED_NONE;

        if ($orderPrice <= 0
            || $this->min_amount > $orderPrice
            || ($this->max_amount > 0 && $this->max_amount < $orderPrice)) {
            return $usedType;
        }

        if ($this->percent > 0) {
            $amount = $this->percent / 100 * $orderPrice;
            $usedType = self::USED_PERCENT;
        }

        if ($this->cash > 0 && $this->cash > $amount) {
            $usedType = self::USED_CASH;
        }

        return $usedType;
    }

    /**
     * @param $orderPrice
     *
     * @return string see self::USED_...
     */
    public function getMayBeUsedType($orderPrice)
    {
        $orderPrice = Cast::toUFloat($orderPrice);
        $amount = 0.0;
        $usedType = self::USED_NONE;

        if ($this->percent > 0) {
            $amount = $this->percent / 100 * $orderPrice;
            $usedType = self::USED_PERCENT;
        }

        if ($this->cash > 0 && $this->cash > $amount) {
            $usedType = self::USED_CASH;
        }

        return $usedType;
    }

    /**
     * Сумма скидки
     *
     * @param float|int $orderPrice
     *
     * @return float
     */
    public function getDiscount($orderPrice)
    {
        $orderPrice = Cast::toUFloat($orderPrice);
        $amount = 0.0;

        $userType = $this->getUsedType($orderPrice);

        switch ($userType) {
            case self::USED_PERCENT:
                $amount = $this->percent / 100 * $orderPrice;
                break;

            case self::USED_CASH:
                $amount = $this->cash;
                break;

            default:
                break;
        }

        return ceil($amount);
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function timeStartAt($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.start_at' . '=>' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function timeEndAt($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.end_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer|null $time Unix timestamp
     *
     * @return $this
     */
    public function active($time = null)
    {
        if ($time === null) {
            $time = time();
        } else {
            $time = Cast::toInt($time);
        }

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition("$alias.start_at <=" . Cast::toStr($time));
        $this->getDbCriteria()->addCondition("$alias.end_at >" . Cast::toStr($time), 'AND');

        return $this;
    }

    /**
     * @param float|int $orderPrice
     * @param float|int $cache
     *
     * @return null|OrderDiscountCampaignRecord
     */
    public static function findBestCampaign($orderPrice, $cache = 10)
    {
        $campaigns = self::model()->cache($cache)->active()->findAll();

        $campaign = array_reduce($campaigns, function ($carry, $item) use ($orderPrice) {
            /* @var self $item */
            /* @var self $carry */
            $minOrderPriceCarry = max($orderPrice, $carry->min_amount);
            $minOrderPriceItem = max($orderPrice, $item->min_amount);

            return $carry->getDiscount($minOrderPriceCarry) < $item->getDiscount($minOrderPriceItem) ? $item : $carry;
        }, reset($campaigns));

        return $campaign;
    }
}
