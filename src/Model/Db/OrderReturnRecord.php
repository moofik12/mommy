<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbException;
use CException;
use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Service\Deprecated\Delivery\DeliveryCountryGroups;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class OrderReturnRecord
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $order_user_id
 * @property integer $delivery_type
 * @property string $trackcode
 * @property string $trackcode_return
 * @property integer $refund_type
 * @property float $price_delivery
 * @property float $price_redelivery
 * @property float $price_products
 * @property integer $refund_delivery_type
 * @property integer $refund_redelivery_type
 * @property float $price_refund
 * @property int $payment_after_at
 * @property float $exps_client
 * @property float $exps_shop
 * @property integer $status
 * @property integer $status_prev
 * @property string $status_history_json
 * @property-read integer $status_changed_at
 * @property boolean $is_custom
 * @property integer $admin_id
 * @property string $comment комментарий менеджера
 * @property string $client_comment комментарий клиента
 * @property string bank_fullname_enc ФИО <b>(зашифровано)</b>
 * @property string bank_vatin_enc ИНН <b>(зашифровано)</b>
 * @property string bank_name_enc Название банка <b>(зашифровано)</b>
 * @property string bank_giro_enc расчетный счет (20 цифр) <b>(зашифровано)</b>
 * @property string bank_cardnum_enc Номер карты (16 цифр) <b>(зашифровано)</b>
 * @property string bank_correspondent_acc_enc к\с банка (20 цифр) <b>(зашифровано)</b>
 * @property string bank_idcode_enc БИК банка (9 цифр) <b>(зашифровано)</b>
 * @property string bank_mfocode_enc МФО банка (6 цифр) <b>(зашифровано)</b>
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read OrderRecord $order
 * @property-read OrderReturnProductRecord[] $positions
 * @property-read integer $positionCount
 * @property-read UserRecord $orderUser
 * @property-read AdminUserRecord $user
 * @property-read array $statusHistory
 * @property-read string $deliveryTypeReplacement
 * @property-read string $refundTypeReplacement
 * @property-read string $refundDeliveryTypeReplacement
 * @property-read string $refundRedeliveryTypeReplacement
 * @property-read string $statusReplacement
 * @property-read string $statusPrevReplacement
 * @property-read string $bankFullname ФИО
 * @property-read string $bankVatin ИНН
 * @property-read string $bankName Название банка
 * @property-read string $bankGiro расчетный счет (20 цифр)
 * @property-read string $bankCardnum Номер карты (16 цифр)
 * @property-read string $bankCorrespondentAcc к\с банка (20 цифр)
 * @property-read string $bankIdCode БИК банка (9 цифр)
 * @property-read string $bankMFOCode МФО банка (6 цифр)
 * @mixin Timestampable
 */
class OrderReturnRecord extends HasBankDataRecord
{
    const PAY_READY_TIME = 10800; // 3 hours

    const STATUS_UNCONFIGURED = 0; // создано, но не подтверждено
    const STATUS_PARTIAL_CONFIGURED = 1; // подтвержден только тип оплаты, товар не указан
    const STATUS_CONFIGURED = 2; // подтверждено и зафиксированно
    const STATUS_NEED_PAY = 3; // оплата подтверждена
    const STATUS_PAYED = 5; // выплачено
    const STATUS_CANCELLED = 4; // отменено

    const DELIVERY_PAYER_CLIENT_CLIENT = 0; // уплатил клиент, плачивает клиент
    const DELIVERY_PAYER_CLIENT_SHOP = 1; // уплатил клиент, компенсирует интернет-магазин
    const DELIVERY_PAYER_SHOP_SHOP = 2; // уплатил магазин, плачивает магазин
    const DELIVERY_PAYER_SHOP_CLIENT = 3; // уплатил магазин, плачивает клиент

    const REFUND_TYPE_BONUSPOINTS = 0; // внутренний счет
    const REFUND_TYPE_BANK = 1; // на расчетный счет в банке

    const NOT_PAYMENT_TIME_BONUSPOINTS = 7200; //2 часа
    const NOT_PAYMENT_TIME_BANK = 14400; //4 часа

    /**
     * @param string $className
     *
     * @return OrderReturnRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['trackcode', 'getTrackcode', 'setTrackcode', 'trackcode'];
        yield ['trackcode_return', 'getTrackcodeReturn', 'setTrackcodeReturn', 'trackcodeReturn'];
        yield ['delivery_type', 'getDeliveryType', 'setDeliveryType', 'deliveryType'];
        yield ['refund_type', 'getRefundType', 'setRefundType', 'refundType'];
        yield ['price_delivery', 'getPriceDelivery', 'setPriceDelivery', 'priceDelivery'];
        yield ['price_redelivery', 'getPriceRedelivery', 'setPriceRedelivery', 'priceRedelivery'];
        yield ['price_products', 'getPriceProducts', 'setPriceProducts', 'priceProducts'];
        yield ['refund_delivery_type', 'getRefundDeliveryType', 'setRefundDeliveryType', 'refundDeliveryType'];
        yield ['refund_redelivery_type', 'getRefundRedeliveryType', 'setRefundRedeliveryType', 'refundRedeliveryType'];
        yield ['price_refund', 'getPriceRefund', 'setPriceRefund', 'priceRefund'];
        yield ['payment_after_at', 'getPaymentAfterAt', 'setPaymentAfterAt', 'paymentAfterAt'];
        yield ['exps_client', 'getExpsClient', 'setExpsClient', 'expsClient'];
        yield ['exps_shop', 'getExpsShop', 'setExpsShop', 'expsShop'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['status_history_json', 'getStatusHistoryJson', 'setStatusHistoryJson', 'statusHistoryJson'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['is_custom', 'isCustom', 'setIsCustom', 'isCustom'];
        yield ['client_comment', 'getClientComment', 'setClientComment', 'clientComment'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['bank_fullname_enc', 'getBankFullnameEnc', 'setBankFullnameEnc', 'bankFullnameEnc'];
        yield ['bank_vatin_enc', 'getBankVatinEnc', 'setBankVatinEnc', 'bankVatinEnc'];
        yield ['bank_name_enc', 'getBankNameEnc', 'setBankNameEnc', 'bankNameEnc'];
        yield ['bank_giro_enc', 'getBankGiroEnc', 'setBankGiroEnc', 'bankGiroEnc'];
        yield ['bank_cardnum_enc', 'getBankCardnumEnc', 'setBankCardnumEnc', 'bankCardnumEnc'];
        yield ['bank_correspondent_acc_enc', 'getBankCorrespondentAccEnc', 'setBankCorrespondentAccEnc', 'bankCorrespondentAccEnc'];
        yield ['bank_idcode_enc', 'getBankIdcodeEnc', 'setBankIdcodeEnc', 'bankIdcodeEnc'];
        yield ['bank_mfocode_enc', 'getBankMfocodeEnc', 'setBankMfocodeEnc', 'bankMfocodeEnc'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['order_user_id', 'getOrderUserId', 'setOrderUserId', 'orderUserId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    public function tableName()
    {
        return 'orders_returns';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'positions' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderReturnProductRecord', 'return_id'],
            'positionCount' => [self::STAT, 'MommyCom\Model\Db\OrderReturnProductRecord', 'return_id'],
            'orderUser' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'order_user_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
        ];
    }

    public function rules()
    {
        return [
            ['order_id', 'required'],
            ['trackcode, trackcode_return', 'length', 'max' => 250],

            ['order_id, refund_type, price_delivery, price_redelivery, price_products, price_refund, exps_client, exps_shop', 'numerical'],
            ['payment_after_at', 'numerical', 'integerOnly' => true],

            ['refund_delivery_type, refund_redelivery_type', 'in', 'range' => [
                self::DELIVERY_PAYER_CLIENT_CLIENT,
                self::DELIVERY_PAYER_CLIENT_SHOP,
                self::DELIVERY_PAYER_SHOP_SHOP,
                self::DELIVERY_PAYER_SHOP_CLIENT,
            ]],

            ['is_custom', 'boolean'],

            ['status, status_prev', 'in', 'range' => [
                self::STATUS_UNCONFIGURED,
                self::STATUS_PARTIAL_CONFIGURED,
                self::STATUS_CONFIGURED,
                self::STATUS_NEED_PAY,
                self::STATUS_CANCELLED,
                self::STATUS_PAYED,
            ]],

            ['delivery_type', 'in', 'range' => DeliveryCountryGroups::instance()->getDelivery()->getList(true)],

            ['refund_type', 'in', 'range' => [
                self::REFUND_TYPE_BONUSPOINTS,
                self::REFUND_TYPE_BANK,
            ]],

            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['order_id', 'validatorOrderStatus'],
            ['order_id', 'validatorOrderIsFullyReturned'],
            ['status', 'validatorStatus'],
            ['comment, client_comment', 'length', 'max' => 250],

            ['bankFullname, bankVatin, bankName, bankGiro, bankCardnum, bankCorrespondentAcc, bankIdCode, bankMFOCode', 'type', 'type' => 'string'],
            ['bankFullname', 'length', 'max' => 250],
            ['bankVatin', 'length', 'min' => 8, 'max' => 10], // ИНН-ом у некоторых людей может выступать код паспорта (8 символов)
            ['bankName', 'length', 'max' => 250],
            ['bankGiro', 'length', 'max' => 20],
            ['bankCardnum', 'length', 'min' => 7, 'max' => 19],
            ['bankCorrespondentAcc', 'length', 'is' => 20],
            ['bankIdCode', 'length', 'is' => 9],
            ['bankMFOCode', 'length', 'is' => 6],
            [
                'bankGiro, bankCardnum, bankCorrespondentAcc, bankGiro, bankIdCode, bankMFOCode',
                'match',
                'pattern' => '/^[0-9]+$/',
            ],

            [
                'id, order_id, order_user_id, delivery_type, is_custom, trackcode, trackcode_return, refund_type, ' .
                'price_delivery, price_redelivery, price_products, price_refund, exps_client, exps_shop, ' .
                'status, status_prev',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => '№',
            'order_id' => \Yii::t($category, 'Order'),
            'order_user_id' => \Yii::t($category, 'Client'),

            'delivery_type' => \Yii::t($category, 'Delivery type'),
            'refund_type' => \Yii::t($category, 'Refund type'),

            'price_delivery' => \Yii::t($category, 'Shipping сost'),
            'price_redelivery' => \Yii::t($category, 'Cost of return delivery'),
            'price_products' => \Yii::t($category, 'Cost of products'),
            'refund_delivery_type' => \Yii::t($category, 'Type of delivery compensation'),
            'refund_redelivery_type' => \Yii::t($category, 'Type of return delivery compensation'),
            'price_refund' => \Yii::t($category, 'Cost of compensation'),
            'payment_after_at' => \Yii::t($category, 'Compenstation payment after'),

            'trackcode' => \Yii::t($category, 'Tracking code / Consignment note'),
            'trackcode_return' => \Yii::t($category, 'Return tracking code / Consignment note'),

            'status' => \Yii::t($category, 'Status'),
            'status_prev' => \Yii::t($category, 'Previous status'),
            'status_history_json' => \Yii::t($category, 'Status history'),

            'is_custom' => \Yii::t($category, 'Created manually'),
            'admin_id' => \Yii::t($category, 'User'),
            'comment' => \Yii::t($category, 'Manager comment'),
            'client_comment' => \Yii::t($category, 'Customer comment'),

            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),

            'exps_client' => \Yii::t($category, 'Expenses of the client'),
            'exps_shop' => \Yii::t($category, 'Expenses of the shop'),

            'bankFullname' => \Yii::t($category, 'Full name of the Recipient'),
            'bankVatin' => \Yii::t($category, 'Beneficiary\'s ID'),
            'bankName' => \Yii::t($category, 'Name of the bank'),
            'bankGiro' => \Yii::t($category, 'Bank account'),
            'bankCardnum' => \Yii::t($category, 'Bankcard number'),
            'bankCorrespondentAcc' => \Yii::t($category, 'Correspondent Bank Account'),
            'bankIdCode' => \Yii::t($category, 'Bank Id Code'),
            'bankMFOCode' => \Yii::t($category, 'Bank Id Code'),

            'refundTypeReplacement' => \Yii::t($category, 'Refund type'),

            'refundDeliveryTypeReplacement' => \Yii::t($category, 'Delivery refund type'),
            'refundRedeliveryTypeReplacement' => \Yii::t($category, 'Return delivery refund type'),

            'statusHistory' => \Yii::t($category, 'Status history'),
            'positionCount' => \Yii::t($category, 'Number of positions'),
            'statusReplacement' => \Yii::t($category, 'Status'),
            'statusPrevReplacement' => \Yii::t($category, 'Пред. статус'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function validatorStatus($attribute)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        $value = $this->$attribute;

        if ($value == self::STATUS_CONFIGURED) {
            $positions = $this->positions;

            if (empty($positions)) {
                $this->addError($attribute, 'Can not confirm empty return');
                return;
            }

            foreach ($positions as $position) {
                if ($position->status != OrderReturnProductRecord::STATUS_OK) {
                    continue;
                }

                if ($position->item_condition == OrderReturnProductRecord::CONDITION_UNKNOWN) {
                    $this->addError($attribute, \Yii::t($category, 'Can not confirm a return in which at least one of the goods from the return is not indicated by the specific state'));
                    return;
                }
            }
        }
    }

    public function validatorOrderIsFullyReturned($attribute)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        if (!$this->isNewRecord) {
            return;
        }

        $value = $this->$attribute;
        $order = OrderRecord::model()->findByPk($value);
        if ($order === null) {
            return;
        }

        /* @var $order OrderRecord */

        $warehouseCount = WarehouseProductRecord::model()->orderId($order->id)->count();
        $returnedWarehouseCount = OrderReturnProductRecord::model()
            ->orderId($order->id)
            ->status(OrderReturnProductRecord::STATUS_OK)
            ->count();

        if ($warehouseCount == $returnedWarehouseCount) {
            $this->addError($attribute, \Yii::t($category, 'All items from this order have already been returned'));
        }
    }

    public function validatorOrderStatus($attribute)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        $value = $this->$attribute;
        $order = OrderRecord::model()->findByPk($value);
        if ($order === null) {
            return;
        }

        /* @var $order OrderRecord */

        if ($order->processing_status != OrderRecord::PROCESSING_STOREKEEPER_MAILED) {
            $this->addError($attribute, \Yii::t($category, 'You can not make a refund for an order that was not sent'));
        }

        if ($order->is_drop_shipping) {
            $this->addError($attribute, \Yii::t($category, 'You can not make a return for a dropshipping order'));
        }
    }

    protected function _resolveOrder()
    {
        if (!$this->isNewRecord) {
            return;
        }

        if ($this->order === null) {
            return;
        }

        $this->order_user_id = $this->order->user_id;
        $this->price_delivery = $this->order->deliveryPrice;
        $this->trackcode = $this->order->trackcode;
        $this->delivery_type = $this->order->delivery_type;
    }

    protected function _createStubPositions()
    {
        $order = $this->order;

        if ($this->status != self::STATUS_UNCONFIGURED) {
            return;
        }

        if ($order === null) {
            return;
        }

        $orderPositions = $order->positions;

        foreach ($orderPositions as $orderPosition) {
            $warehouseProducts = $orderPosition->connectedWarehouseProducts;

            foreach ($warehouseProducts as $warehouseProduct) {
                $position = new OrderReturnProductRecord();
                $position->return_id = $this->id;
                $position->price = $orderPosition->price;
                $position->order_product_id = $orderPosition->id;
                $position->order_id = $orderPosition->order_id;
                $position->event_id = $orderPosition->event_id;
                $position->event_product_id = $orderPosition->product->id;
                $position->warehouse_id = $warehouseProduct->id;

                $position->save();
            }
        }
    }

    /**
     * @param OrderReturnProductRecord $position
     */
    protected function _createWarehousePosition($position)
    {
        switch ($position->item_condition) {
            case OrderReturnProductRecord::CONDITION_BROKEN_BY_DELIVERY_SERVICE:
            case OrderReturnProductRecord::CONDITION_BROKEN:
                $warehouseStatus = WarehouseProductRecord::STATUS_BROKEN;
                $warehouseLostStatus = WarehouseProductRecord::LOST_STATUS_BROKEN;
                break;
            case OrderReturnProductRecord::CONDITION_DEFECTIVE:
                $warehouseStatus = WarehouseProductRecord::STATUS_BROKEN;
                $warehouseLostStatus = WarehouseProductRecord::LOST_STATUS_DEFECTIVE;
                break;
            case OrderReturnProductRecord::CONDITION_GOOD_WEARED:
                $warehouseStatus = WarehouseProductRecord::STATUS_BROKEN;
                $warehouseLostStatus = WarehouseProductRecord::LOST_STATUS_DEFECTIVE;
                break;

            case OrderReturnProductRecord::CONDITION_GOOD:
            default:
                $warehouseStatus = WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED;
                $warehouseLostStatus = WarehouseProductRecord::LOST_STATUS_CORRECT;
                break;
        }

        $warehouseStatusNotChange = [
            WarehouseProductRecord::STATUS_READY_SHIPPING_TO_SUPPLIER,
            WarehouseProductRecord::STATUS_MAILED_TO_SUPPLIER,
        ];
        $warehouse = $position->returnedWarehouseProduct;
        $changed = false;

        $needDelete = $position->return->status == self::STATUS_CANCELLED ||
            $position->status != OrderReturnProductRecord::STATUS_OK ||
            $position->item_condition == OrderReturnProductRecord::CONDITION_UNKNOWN;

        if ($needDelete) {
            if ($warehouse !== null) {
                $warehouse->delete();
            }
            return;
        }

        if ($warehouse === null) {
            $warehouse = new WarehouseProductRecord();
            $warehouse->return_product_id = $position->id;

            $attributes = $position->warehouseProduct->getAttributes([
                'supplier_id',
                'event_product_id',
                'event_id',
                'product_id',
            ]);

            $warehouse->setAttributes($attributes, false);
        }

        if ($warehouse->status != $warehouseStatus || $warehouse->lost_status != $warehouseLostStatus ||
            $warehouse->status_comment != $this->comment || $warehouse->lost_status_comment != $this->comment) {
            if (!in_array($warehouse->status, $warehouseStatusNotChange)) {
                $warehouse->status = $warehouseStatus;
                $warehouse->lost_status = $warehouseLostStatus;
            }

            $warehouse->status_comment = $warehouse->lost_status_comment = $this->comment;
            $changed = true;
        }

        if ($changed) {
            if (!$warehouse->save()) {
                Yii::log(print_r($warehouse->getErrors(), true));
            }
        }
    }

    protected function _createWarehousePositions()
    {
        $positions = $this->getRelated('positions', true);
        foreach ($positions as $position) {
            $this->_createWarehousePosition($position);
        }
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateStatusChangeStack()
    {
        $statusHistory = $this->getStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->status) {
            $now = time();

            $addToHistory = [
                'status' => $this->status,
                'admin_id' => 0,
                'user_id' => 0,
            ];

            try { // for console app
                $webUser = \Yii::app()->user;
                $user = $webUser->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }

                if ($user instanceof UserRecord) {
                    $addToHistory['user_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
            $this->status_prev = $prevStatus['status'];

            $this->status_changed_at = time();
        }
        ksort($statusHistory);

        $this->status_history_json = json_encode($statusHistory);
    }

    protected function _recalculatePrices()
    {
        $this->forceRecalculatePrices();
    }

    protected function _updatePositionsStatus()
    {
        $positions = $this->positions;

        if (count($positions) == 0) {
            return;
        }

        $positionStatus = OrderReturnProductRecord::STATUS_OK;
        if ($this->status == self::STATUS_CANCELLED) {
            $positionStatus = OrderReturnProductRecord::STATUS_CANCELLED;
        }

        foreach ($positions as $position) {
            if ($position->status != $positionStatus && $position->status != OrderReturnProductRecord::STATUS_DELETED) {
                $position->status = $positionStatus;
                $position->save();
            }
        }
    }

    protected function _resolvePreviousBankInfo()
    {
        if (!$this->isNewRecord) {
            return;
        }

        $prevRefund = OrderReturnRecord::model()
            ->orderUserId($this->order_user_id)
            ->refundType(self::REFUND_TYPE_BANK)
            ->orderBy('id', 'desc')
            ->find();

        if ($prevRefund === null) {
            return;
        }

        /* @var $prevRefund OrderReturnRecord */

        $attrs = $prevRefund->getAttributes(null);
        foreach ($attrs as $name => $value) {
            if (mb_strpos($name, 'bank_') === 0) {
                $this->$name = $value;
            }
        }
    }

    /**
     * Обновление даты выплаты, если дата выпадает на выходные, выплата назначается на пятницу
     */
    protected function _checkPaymentAfterTime()
    {
        if ($this->status == self::STATUS_CONFIGURED && $this->payment_after_at == 0) {
            $time = time();
            switch ($this->refund_type) {
                case self::REFUND_TYPE_BANK:
                    $time += self::NOT_PAYMENT_TIME_BANK;
                    break;

                case self::REFUND_TYPE_BONUSPOINTS:
                    $time += self::NOT_PAYMENT_TIME_BONUSPOINTS;
                    break;
            }

            $dataTime = new DateTime();
            $dataTime->setTimestamp($time);
            $dataTime->modify('today +8 hours');

            $dateDayNumber = $dataTime->format('N');
            if (in_array($dateDayNumber, [6, 7])) {
                $dataTime->modify('previous friday +8 hours');
            }

            $this->payment_after_at = $dataTime->getTimestamp();
        }
    }

    public function beforeValidate()
    {
        if (!$result = parent::beforeValidate()) {
            return $result;
        }

        $this->_resolveOrder();
        $this->_recalculatePrices();

        return true;
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->_updateStatusChangeStack();
        $this->_resolveOrder();
        $this->_recalculatePrices();
        $this->_updatePositionsStatus();
        $this->_resolvePreviousBankInfo();
        $this->_checkPaymentAfterTime();

        return true;
    }

    protected function afterSave()
    {
        parent::afterSave();

        $this->_createStubPositions();
        $this->_createWarehousePositions();
        $this->_createDeductionPartnerBalance();
    }

    /**
     * создание отчисления в балансе партнера (минусуем)
     */
    protected function _createDeductionPartnerBalance()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        if ($this->status === self::STATUS_PAYED && $this->price_refund > 0) {
            $partnerAdmission = UserPartnerAdmissionRecord::model()->orderId($this->order_id)->find();
            if ($partnerAdmission !== null) {
                if ($partnerAdmission->status == UserPartnerAdmissionRecord::STATUS_BALANCE) {
                    $balance = new UserPartnerBalanceRecord();
                    $balance->partner_id = $partnerAdmission->partner_id;
                    $balance->type = UserPartnerBalanceRecord::TYPE_RETURN_MINUS;
                    $balance->amount = -$this->price_refund;
                    $balance->order_id = $partnerAdmission->order_id;
                    $balance->description = \Yii::t($category, 'Return to the order №') . $partnerAdmission->order_id . \Yii::t($category, ' in the amount ') . $this->price_refund;
                    $balance->save();
                }
            }
        }
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function idNotIn($value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function orderIdIn($value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'order_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderUserId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $value
     *
     * @return static
     */
    public function orderUserIdIn($value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'order_user_id', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function isCustom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'is_custom' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function refundDeliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'refund_delivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function refundRedeliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'refund_redelivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function refundType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'refund_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function priceRefundFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'price_refund' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function priceRefundTo($value)
    {
        $value = Cast::toUInt($value);
        $value = $this->dbConnection->pdoInstance->quote($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'price_refund' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param int $value unix timestamp
     *
     * @return $this
     */
    public function paymentAfterAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.payment_after_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param int $value unix timestamp
     *
     * @return $this
     */
    public function paymentAfterAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.payment_after_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return $this
     */
    public function refundPriceBetween($from, $to)
    {
        $this->priceRefundFrom($from);
        $this->priceRefundTo($to);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'status', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusNotIn($values)
    {
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function statusPrev($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status_prev' => $value,
        ]);
        return $this;
    }

    /**
     * Готовые к оплате
     *
     * @return static
     */
    public function onlyPayReady()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $criteria->addCondition(
            $alias . '.' . 'status = ' . self::STATUS_CONFIGURED . ' AND ' .
            $alias . '.' . 'status_changed_at < ' . ($time - self::PAY_READY_TIME) . ' AND ' .
            $alias . '.' . 'payment_after_at < ' . $time
        );
        return $this;
    }

    /**
     * Не готовые к оплате
     *
     * @return static
     */
    public function onlyPayUnready()
    {
        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();
        $time = time();
        $criteria->addCondition(
            $alias . '.' . 'status <> ' . self::STATUS_CONFIGURED . ' OR ' .
            $alias . '.' . 'status_changed_at >= ' . ($time - self::PAY_READY_TIME)
        );
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.order_user_id', $model->order_user_id);
        $criteria->compare($alias . '.delivery_type', $model->delivery_type);
        $criteria->compare($alias . '.price_delivery', $model->price_delivery);
        $criteria->compare($alias . '.price_redelivery', $model->price_redelivery);
        $criteria->compare($alias . '.price_products', $model->price_products);
        $criteria->compare($alias . '.refund_type', $model->refund_type);
        $criteria->compare($alias . '.refund_delivery_type', $model->refund_delivery_type);
        $criteria->compare($alias . '.refund_redelivery_type', $model->refund_redelivery_type);
        $criteria->compare($alias . '.price_refund', $model->price_refund);
        $criteria->compare($alias . '.exps_client', $model->exps_client);
        $criteria->compare($alias . '.exps_shop', $model->exps_shop);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.status_prev', $model->status_prev);
        $criteria->compare($alias . '.is_custom', $model->is_custom);

        $criteria->compare($alias . '.trackcode', $model->trackcode, true);
        $criteria->compare($alias . '.trackcode_return', $model->trackcode_return, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /// item api

    /**
     * @return array
     */
    public function getStatusHistory()
    {
        return Cast::toArr(json_decode($this->status_history_json, true, 3));
    }

    /**
     * готов ли возврат к оплате
     *
     * @return bool
     */
    public function getIsPayReady()
    {
        $time = time();
        return $this->status == self::STATUS_CONFIGURED &&
            $this->status_changed_at < $time - self::PAY_READY_TIME;
    }

    /**
     * Стоимость всех товаров
     *
     * @param integer|null $condition фильтрация по состоянию позиции
     *
     * @return float
     */
    public function getProductsPrice($condition = null)
    {
        $result = 0.0;
        $positions = $this->getRelated('positions', true);
        foreach ($positions as $position) {
            if ($position->status != OrderReturnProductRecord::STATUS_OK) {
                continue;
            }

            if ($condition === null || $position->item_condition == $condition) {
                $result += $position->price;
            }
        }
        return $result;
    }

    /**
     * Сумма возврата пользователю УЧИТЫВАЯ бонусы которые он потратил
     *
     * @return float|int
     * @throws CDbException
     */
    public function getProductsPriceRefund()
    {
        $result = 0.0;
        /** @var OrderReturnProductRecord[] $positions */
        $positions = $this->getRelated('positions', true);
        foreach ($positions as $position) {
            $result += $position->refundPrice;
        }

        return $result;
    }

    /**
     * Стоимость всех товаров которые ПОДЛЕЖАТ компенсации клиенту
     *
     * @param integer|null $condition фильтрация по состоянию позиции
     * @param bool $subtractionDiscount вычесть скидку которая были для этой суммы
     * @param bool $subtractionBonusesPresent вычесть подарочные бонусы которые были для этой суммы
     *
     * @return float
     */
    public function getProductsPricePayable($condition = null, $subtractionDiscount = true, $subtractionBonusesPresent = true)
    {
        $result = 0.0;
        $positions = $this->getRelated('positions', true);
        foreach ($positions as $position) {
            if ($position->status != OrderReturnProductRecord::STATUS_OK) {
                continue;
            }

            if ($condition === null || $position->item_condition == $condition) {
                if ($position->is_pay) {
                    $result += $position->price;
                }
            }
        }

        if ($subtractionDiscount) {
            $result -= $this->order->getCountUsedDiscounts($result);
        }

        if ($subtractionBonusesPresent) {
            $result -= $this->order->getCountUsedBonusesPresent($result);
        }

        return $result;
    }

    /**
     * Стоимость всех товаров которые НЕ ПОДЛЕЖАТ компенсации клиенту
     *
     * @param integer|null $condition фильтрация по состоянию позиции
     * @param bool $subtractionDiscount вычесть скидку которая былы для этой суммы
     * @param bool $subtractionBonusesPresent вычесть подарочные бонусы которые были для этой суммы
     *
     * @return float
     */
    public function getProductsPriceUnPayable($condition = null, $subtractionDiscount = false, $subtractionBonusesPresent = false)
    {
        $result = 0.0;
        $positions = $this->getRelated('positions', true);
        foreach ($positions as $position) {
            if ($position->status != OrderReturnProductRecord::STATUS_OK) {
                continue;
            }

            if ($condition === null || $position->item_condition == $condition) {
                if (!$position->is_pay) {
                    $result += $position->price;
                }
            }
        }

        if ($subtractionDiscount) {
            $result -= $this->order->getCountUsedDiscounts($result);
        }

        if ($subtractionBonusesPresent) {
            $result -= $this->order->getCountUsedBonusesPresent($result);
        }

        return $result;
    }

    /**
     * Сумма к компенсации клиенту
     *
     * @return float
     */
    public function getRefundPrice()
    {
        $result = 0.0;
        $result += $this->getProductsPriceRefund();
        //проверяем доступные средства для списания
        $result = min($result, $this->getAvailableRefund());

        if ($this->refund_delivery_type == self::DELIVERY_PAYER_CLIENT_SHOP) {
            $result += $this->price_delivery;
        } elseif ($this->refund_delivery_type == self::DELIVERY_PAYER_SHOP_CLIENT) {
            $result -= $this->price_delivery;
        }

        if ($this->refund_redelivery_type == self::DELIVERY_PAYER_CLIENT_SHOP) {
            $result += $this->price_redelivery;
        } elseif ($this->refund_redelivery_type == self::DELIVERY_PAYER_SHOP_CLIENT) {
            $result -= $this->price_redelivery;
        }

        return $result;
    }

    /**
     * Издержки которые понес клиент
     *
     * @return float
     */
    public function getClientExps()
    {
        $result = 0.0;

        $statuses = [self::DELIVERY_PAYER_CLIENT_CLIENT, self::DELIVERY_PAYER_SHOP_CLIENT];

        if (in_array($this->refund_delivery_type, $statuses)) {
            $result += $this->price_delivery;
        }

        if (in_array($this->refund_redelivery_type, $statuses)) {
            $result += $this->price_redelivery;
        }

        return $result;
    }

    /**
     * Издержки которые понес интернет-магазин
     *
     * @return float
     */
    public function getShopExps()
    {
        $result = 0.0;
        $result += $this->getProductsPricePayable();

        $statuses = [self::DELIVERY_PAYER_SHOP_SHOP, self::DELIVERY_PAYER_CLIENT_SHOP];

        if (in_array($this->refund_delivery_type, $statuses)) {
            $result += $this->price_delivery;
        }

        if (in_array($this->refund_redelivery_type, $statuses)) {
            $result += $this->price_redelivery;
        }

        return $result;
    }

    /**
     * Денежные средства которые заплатил пользователь
     *
     * @return float
     */
    public function getOrderPayed()
    {
        $orderBuyOut = OrderBuyoutRecord::model()
            ->orderId($this->order->id)
            ->status(OrderBuyoutRecord::STATUS_PAYED)
            ->findColumnSum('price');

        $payed = Cast::toFloat($orderBuyOut) + $this->order->card_payed;
        return min($payed, $this->order->getPriceHistory());
    }

    /**
     * Денежные средства которые уже были возвращены
     *
     * @return false|int
     */
    public function getCountReturned()
    {
        $returned = self::model()
            ->idNotIn($this->id)
            ->orderId($this->order->id)
            ->statusIn([OrderReturnRecord::STATUS_PAYED, OrderReturnRecord::STATUS_NEED_PAY, OrderReturnRecord::STATUS_CONFIGURED])
            ->findColumnSum('price_refund');

        return Cast::toFloat($returned);
    }

    /**
     * @param bool $whichDelivery
     *
     * @return mixed
     */
    public function getAvailableRefund($whichDelivery = false)
    {
        $count = $this->getProductsPriceRefund();
        $orderBalance = $this->order->getPrice(false, true) - $this->getCountReturned();

        if ($whichDelivery) {
            $count += $this->getClientExps();
            $count -= $this->getShopExps();
        }

        $refund = min($count, $this->getProductsPrice(), $orderBalance);
        return $refund > 0 ? $refund : 0;
    }

    /**
     * Форсированно пересчитывает стоимость возврата
     *
     * @return static
     */
    public function forceRecalculatePrices()
    {
        $this->price_products = $this->getProductsPrice();
        $this->price_refund = $this->getRefundPrice();
        $this->exps_client = $this->getClientExps();
        $this->exps_shop = $this->getShopExps();
        return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryTypeReplacement()
    {
        $replacements = OrderRecord::deliveryTypeReplacements();
        return isset($replacements[$this->delivery_type]) ? $replacements[$this->delivery_type] : '';
    }

    /**
     * @return string
     */
    public function getRefundDeliveryTypeReplacement()
    {
        $replacements = self::refundDeliveryReplacements();
        return isset($replacements[$this->refund_delivery_type]) ? $replacements[$this->refund_delivery_type] : '';
    }

    /**
     * @return string
     */
    public function getRefundRedeliveryTypeReplacement()
    {
        $replacements = self::refundDeliveryReplacements();
        return isset($replacements[$this->refund_redelivery_type]) ? $replacements[$this->refund_redelivery_type] : '';
    }

    /**
     * @return string
     */
    public function getRefundTypeReplacement()
    {
        $replacements = self::refundTypeReplacements();
        return isset($replacements[$this->refund_type]) ? $replacements[$this->refund_type] : '';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_UNCONFIGURED => \Yii::t($category, 'Created, but not confirmed'),
            self::STATUS_PARTIAL_CONFIGURED => \Yii::t($category, 'Confirmed only the type of payment, product is not specified'),
            self::STATUS_CONFIGURED => \Yii::t($category, 'confirmed'),
            self::STATUS_NEED_PAY => \Yii::t($category, 'In the queue for payment'),
            self::STATUS_PAYED => \Yii::t($category, 'Paid'),
            self::STATUS_CANCELLED => \Yii::t($category, 'Cancelled'),
        ];
    }

    /**
     * @return array
     */
    public static function refundTypeReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::REFUND_TYPE_BONUSPOINTS => \Yii::t($category, 'internal account'),
            self::REFUND_TYPE_BANK => \Yii::t($category, 'to a bank account'),
        ];
    }

    /**
     * @return array
     */
    public static function refundDeliveryReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::DELIVERY_PAYER_CLIENT_CLIENT => \Yii::t($category, 'customer pays for all'),
            self::DELIVERY_PAYER_CLIENT_SHOP => \Yii::t($category, 'customer pays, online-shop compensates'),
            self::DELIVERY_PAYER_SHOP_SHOP => \Yii::t($category, 'shop pays for all'),
            self::DELIVERY_PAYER_SHOP_CLIENT => \Yii::t($category, 'shop pays, customer compensates'),
        ];
    }

    /**
     * @return string
     */
    protected static function _bankDataPrivateKey(): string
    {
        return pack('H*', '3c699c459b7310e8bd7a8004a0d2cc2e30954a18aa8e8b7fd25d58ca8f24983e');
    }

    /**
     * @return string
     */
    public function getBankFullname()
    {
        return self::_bankDataDecode($this->bank_fullname_enc);
    }

    /**
     * @return string
     */
    public function getBankVatin()
    {
        return self::_bankDataDecode($this->bank_vatin_enc);
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return self::_bankDataDecode($this->bank_name_enc);
    }

    /**
     * @return string
     */
    public function getBankGiro(): string
    {
        return self::_bankDataDecode($this->bank_giro_enc);
    }

    /**
     * @return string
     */
    public function getBankCardnum()
    {
        return self::_bankDataDecode($this->bank_cardnum_enc);
    }

    /**
     * @return string
     */
    public function getBankCorrespondentAcc()
    {
        return self::_bankDataDecode($this->bank_correspondent_acc_enc);
    }

    /**
     * @return string
     */
    public function getBankIdCode()
    {
        return self::_bankDataDecode($this->bank_idcode_enc);
    }

    /**
     * @return string
     */
    public function getBankMFOCode()
    {
        return self::_bankDataDecode($this->bank_mfocode_enc);
    }

    /**
     * @param $value
     */
    public function setBankFullname($value)
    {
        $this->bank_fullname_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankVatin($value)
    {
        $this->bank_vatin_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankName($value)
    {
        $this->bank_name_enc = self::_bankDataEncode($value);
    }

    /**
     * @param mixed $value
     */
    public function setBankGiro($value)
    {
        $this->bank_giro_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankCardnum($value)
    {
        $this->bank_cardnum_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankCorrespondentAcc($value)
    {
        $this->bank_correspondent_acc_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankIdCode($value)
    {
        $this->bank_idcode_enc = self::_bankDataEncode($value);
    }

    /**
     * @param $value
     */
    public function setBankMFOCode($value)
    {
        $this->bank_mfocode_enc = self::_bankDataEncode($value);
    }
    // end bank account
}
