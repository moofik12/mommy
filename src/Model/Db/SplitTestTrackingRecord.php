<?php

namespace MommyCom\Model\Db;

use CHttpSession;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\UserAgent;
use Yii;

/**
 * Class SplitTestTrackingRecord
 *
 * @property-read int $id
 * @property int $target
 * @property int $user_id
 * @property int $order_id
 * @property string $test_params
 * @property string $host
 * @property string $url
 * @property string $path_from
 * @property string $path_to
 * @property string $session_id
 * @property int $device_type
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property array|string $testParams
 */
class SplitTestTrackingRecord extends DoctrineActiveRecord
{
    const TARGET_CREATE_ORDER = 1;
    const TARGET_PRESENT_THING = 2;
    const TARGET_SHOW_CONSOLE_ALIEN = 3;
    const TARGET_SHOW_CONFIRM_BUTTON_TOP = 4;
    const TARGET_VIEW_PAGE = 5;
    const TARGET_USER_REGISTER = 6;

    const DEVICE_TYPE_UNKNOWN = 0;
    const DEVICE_TYPE_DESKTOP = 1;
    const DEVICE_TYPE_MOBILE = 2;
    const DEVICE_TYPE_TABLET = 3;

    /**
     * @param string $className
     *
     * @return StatementOrderRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['target', 'getTarget', 'setTarget', 'target'];
        yield ['test_params', 'getTestParams', 'setTestParams', 'testParams'];
        yield ['host', 'getHost', 'setHost', 'host'];
        yield ['url', 'getUrl', 'setUrl', 'url'];
        yield ['path_from', 'getPathFrom', 'setPathFrom', 'pathFrom'];
        yield ['path_to', 'getPathTo', 'setPathTo', 'pathTo'];
        yield ['session_id', 'getSessionId', 'setSessionId', 'sessionId'];
        yield ['device_type', 'getDeviceType', 'setDeviceType', 'deviceType'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
    }

    public function tableName()
    {
        return 'split_tests_tracking';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['target', 'required'],

            ['target', 'in', 'range' => self::targets()],
            ['device_type', 'in', 'range' => [
                self::DEVICE_TYPE_UNKNOWN,
                self::DEVICE_TYPE_DESKTOP,
                self::DEVICE_TYPE_MOBILE,
                self::DEVICE_TYPE_TABLET,
            ]],
            ['device_type', 'default', 'value' => self::DEVICE_TYPE_UNKNOWN],

            ['test_params', 'length', 'max' => 1000],
            ['host', 'length', 'max' => 127],
            ['url', 'length'],
            ['path_from, path_to', 'length', 'max' => 127],
            ['session_id', 'length', 'max' => 64],

            //скорость предпочтительнее
            ['order_id', 'numerical', 'integerOnly' => true],
            ['user_id', 'numerical', 'integerOnly' => true],

//            array('order_id', 'exist', 'className' => 'OrderRecord', 'attributeName' => 'id'),
//            array('user_id', 'exist', 'className' => 'UserRecord', 'attributeName' => 'id'),

            ['id, target, device_type, test_params, url, order_id, user_id, created_at, updated_at',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (empty($this->url) && isset($_SERVER['REQUEST_URI'])) {
            $this->url = $_SERVER['REQUEST_URI'];
        }

        if (empty($this->host) && isset($_SERVER['HTTP_HOST'])) {
            $this->host = $_SERVER['HTTP_HOST'];
        }

        /* @var UserAgent $userAgent */
        $userAgent = Yii::app()->userAgent;
        if ($userAgent->getIsDesktop()) {
            $this->device_type = SplitTestTrackingRecord::DEVICE_TYPE_DESKTOP;
        } elseif ($userAgent->getIsMobile()) {
            $this->device_type = SplitTestTrackingRecord::DEVICE_TYPE_MOBILE;
        } elseif ($userAgent->getIsMobileTablet()) {
            $this->device_type = SplitTestTrackingRecord::DEVICE_TYPE_TABLET;
        }

        /* @var CHttpSession $session */
        $session = Yii::app()->session;
        if (empty($this->session_id) && $session) {
            $this->session_id = $session->getSessionID();
        }

        $request = Yii::app()->request;
        if (empty($this->path_to) && $request) {
            $this->path_to = '/' . $request->getPathInfo();
        }

        if (empty($this->path_from)) {
            if (isset($_SERVER['HTTP_REFERER']) && isset($_SERVER['HTTP_HOST'])) {
                $thisHost = $_SERVER['HTTP_HOST'];
                $parseReferer = parse_url($_SERVER['HTTP_REFERER']);

                if (isset($parseReferer['host']) && $thisHost == $parseReferer['host']) {
                    $this->path_from = isset($parseReferer['path']) ? $parseReferer['path'] : '/';
                    if (mb_strlen($this->path_from) > 1) {
                        $this->path_from = rtrim($this->path_from, '/');
                    }
                }

                if ($this->path_from == '/index.php') {
                    $this->path_from = '/';
                }
            }
        }

        return parent::beforeSave();
    }

    /**
     * @param $value
     */
    public function setTestParams($value)
    {
        if (is_array($value)) {
            $this->test_params = http_build_query($value);
        } elseif (is_scalar($value)) {
            $this->test_params = $value;
        }
    }

    /**
     * @return string
     */
    public function getTestParams()
    {
        return $this->test_params;
    }

    /**
     * @return array
     */
    public static function targets()
    {
        return [
            self::TARGET_CREATE_ORDER,
            self::TARGET_PRESENT_THING,
            self::TARGET_SHOW_CONSOLE_ALIEN,
            self::TARGET_SHOW_CONFIRM_BUTTON_TOP,
            self::TARGET_VIEW_PAGE,
            self::TARGET_USER_REGISTER,
        ];
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function targetIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.target', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function userIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.user_id', $values);
        return $this;
    }
}
