<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class WarehouseStatementReturnProductRecord
 *
 * @property-read int $id
 * @property int $statement_id
 * @property int $user_id
 * @property int $product_id
 * @property int $event_id
 * @property int $event_product_id
 * @property int $changed_product_status Какой статус был выставлен складскому товару
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property WarehouseStatementReturnRecord $statement
 * @property AdminUserRecord $user
 * @property EventRecord $event
 * @property EventProductRecord $eventProduct
 * @property WarehouseProductRecord $product
 * @mixin Timestampable
 */
class WarehouseStatementReturnProductRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_change_product_status', 'isChangeProductStatus', 'setIsChangeProductStatus', 'isChangeProductStatus'];
        yield ['changed_product_status', 'getChangedProductStatus', 'setChangedProductStatus', 'changedProductStatus'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['statement_id', 'getStatementId', 'setStatementId', 'statementId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['product_id', 'getProductId', 'setProductId', 'productId'];
        yield ['event_id', 'getEventId', 'setEventId', 'eventId'];
        yield ['event_product_id', 'getEventProductId', 'setEventProductId', 'eventProductId'];
    }

    public function tableName()
    {
        return 'warehouse_statements_return_products';
    }

    public function relations()
    {
        return [
            'statement' => [self::BELONGS_TO, 'MommyCom\Model\Db\WarehouseStatementReturnRecord', 'statement_id'],
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'event' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventRecord', 'event_id'],
            'eventProduct' => [self::BELONGS_TO, 'MommyCom\Model\Db\EventProductRecord', 'event_product_id'],
            'product' => [self::BELONGS_TO, 'MommyCom\Model\Db\WarehouseProductRecord', 'product_id'],
        ];
    }

    public function rules()
    {
        return [
            ['product_id', 'exist', 'className' => 'MommyCom\Model\Db\WarehouseProductRecord', 'attributeName' => 'id', 'allowEmpty' => false],
            ['statement_id', 'exist', 'className' => 'MommyCom\Model\Db\WarehouseStatementReturnRecord', 'attributeName' => 'id', 'allowEmpty' => false],
            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id', 'allowEmpty' => false],
            ['event_id', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id', 'allowEmpty' => false],
            ['event_product_id', 'exist', 'className' => 'MommyCom\Model\Db\EventProductRecord', 'attributeName' => 'id', 'allowEmpty' => false],

            ['changed_product_status', 'in', 'range' => array_keys(WarehouseProductRecord::statusReplacements())],

            [
                'id, statement_id, user_id, event_id, event_product_id, created_at, updated_at',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'statement_id' => \Yii::t($category, 'Statement(act)'),
            'product_id' => \Yii::t($category, 'Warehouse goods'),
            'user_id' => \Yii::t($category, 'User'),
            'event_id' => \Yii::t($category, 'Event'),
            'event_product_id' => \Yii::t($category, 'Item in stock event'),
            'changed_product_status' => \Yii::t($category, 'Changed stock status'),
            'created_at' => \Yii::t($category, 'Added'),
            'updated_at' => \Yii::t($category, 'Updated on'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    protected function afterValidate()
    {
        if ($this->product && $this->statement && $this->product->supplier_id != $this->statement->supplier_id) {
            $this->addError('event_id', \Yii::t('backend', 'The {event} action in statement №{statement} is not from the supplier!',
                [
                    '{event}' => $this->event->id,
                    '{statement}' => $this->statement->supplier_id,
                ]));
        }

        parent::afterValidate();
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model WarehouseStatementReturnProductRecord */

        $criteria->compare($alias . '.id', '=' . Utf8::ltrim($model->id, '0'));
        $criteria->compare($alias . '.statement_id', $model->statement_id);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.event_id', $model->event_id);
        $criteria->compare($alias . '.event_product_id', $model->event_product_id);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @return bool
     * @throws CException
     */
    protected function beforeDelete()
    {
        $delete = true;
        if ($this->changed_product_status == WarehouseProductRecord::STATUS_READY_SHIPPING_TO_SUPPLIER) {
            if ($this->product->status == WarehouseProductRecord::STATUS_READY_SHIPPING_TO_SUPPLIER) {
                $this->product->status = $this->product->status_prev;
                $delete = $this->product->save();

                if ($delete == false) {
                    $lastError = [];
                    $errors = $this->product->getErrors();
                    if ($errors) {
                        $lastError = reset($errors);
                    }
                    throw new CException(\Yii::t('backend', 'Error updating warehouse goods!') . ' Errors: ' . print_r($lastError, true));
                }
            }
        } elseif ($this->changed_product_status > 0) {
            $delete = false;
        }

        return $delete && parent::beforeDelete();
    }

    /**
     * @return bool
     */
    protected function beforeSave()
    {
        $save = $this->_changeProductStatus();
        return $save && parent::beforeSave();
    }

    /**
     * @return bool
     * @throws CException
     */
    protected function _changeProductStatus()
    {
        $save = true;
        if ($this->statement) {
            if ($this->statement->status == WarehouseStatementReturnRecord::STATUS_READY
                && $this->changed_product_status != WarehouseProductRecord::STATUS_READY_SHIPPING_TO_SUPPLIER
            ) {
                $this->changed_product_status = $this->product->status = WarehouseProductRecord::STATUS_READY_SHIPPING_TO_SUPPLIER;
                $save = $this->product->save();
            } elseif ($this->statement->status == WarehouseStatementReturnRecord::STATUS_CONFIRMED
                && $this->changed_product_status != WarehouseProductRecord::STATUS_MAILED_TO_SUPPLIER
            ) {
                $this->changed_product_status = $this->product->status = WarehouseProductRecord::STATUS_MAILED_TO_SUPPLIER;
                $save = $this->product->save();
            }

            if (!$save) {
                $errors = $this->product->getErrors();
                $error = reset($errors);
                throw new CException(\Yii::t('backend', 'Error while changing the status of the warehouse product №') . ' ' . $this->product->id . ' Error: ' . print_r($error, true));
            }
        }

        return $save;
    }

    /**
     * @param array $value
     * @param $max
     *
     * @return $this
     */
    public function statementIdIn(array $value, $max = -1)
    {
        if ($max > 0) {
            $value = array_slice($value, 0, $max);
        }

        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition("$alias.statement_id", $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function statementId($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.statement_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $value
     * @param $max
     *
     * @return $this
     */
    public function productIdIn(array $value, $max = -1)
    {
        if ($max > 0) {
            $value = array_slice($value, 0, $max);
        }

        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition("$alias.product_id", $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function productId($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.product_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $value
     * @param $max
     *
     * @return $this
     */
    public function eventIdIn(array $value, $max = -1)
    {
        if ($max > 0) {
            $value = array_slice($value, 0, $max);
        }

        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition("$alias.event_id", $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function eventId($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.event_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $value
     * @param $max
     *
     * @return $this
     */
    public function eventProductIdIn(array $value, $max = -1)
    {
        if ($max > 0) {
            $value = array_slice($value, 0, $max);
        }

        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition("$alias.event_product_id", $value);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function eventProductId($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.event_product_id' => $value,
        ]);

        return $this;
    }
}
