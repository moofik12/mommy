<?php

namespace MommyCom\Model\Db;

use CException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * class CallcenterTaskRecord
 *
 * @property integer $status in self::STATUS_
 * @property integer $operation_id
 * @property integer $order_id
 * @property integer $admin_id админ который создал задачу (если не системная задача)
 * @property integer $performed_admin_id админ который выполнил задачу
 * @property string $comment_task комментарий к задаче
 * @property string $comment_callcenter комментарий при выполнении
 * @property integer $processing_admin_id админ который выполняет задачу
 * @property integer $processing_update_last_at UNIXTIME последней активности
 * @property integer $next_task_at
 * @property bool $is_system
 * @property bool $is_callcenter_answer
 * @property bool $is_callcenter_answer_read
 * @property-read integer $performed_at
 * @property-read integer $updated_at
 * @property-read integer $created_at
 * @property-read array $orderIds
 * @property-read AdminUserRecord $admin
 * @property-read AdminUserRecord performedAdmin
 * @property-read AdminUserRecord $processingAdmin
 * @property-read OrderRecord $order
 * @property-read CallcenterTaskOperationRecord $operation
 */
class CallcenterTaskRecord extends DoctrineActiveRecord
{
    const STATUS_NEW = 0, //новая
        STATUS_REPEAT = 1, //повторная
        STATUS_POSTPONED = 2, //отложена
        STATUS_COMPLETED = 3, //завершена
        STATUS_FAILED = 4; //не возможно выполнить

    /**
     * время в сек. при котром ститается что админ редактирует заказ
     */
    const TIME_PROCESSING_VALID = 300;

    /**
     * статус выполнения в процентах
     *
     * @var array
     */
    private $_statusPercents = [
        self::STATUS_NEW => 0,
        self::STATUS_REPEAT => 30,
        self::STATUS_POSTPONED => 50,
        self::STATUS_COMPLETED => 100,
        self::STATUS_FAILED => 100,
    ];

    /**
     * @var bool
     */
    private $_previousStatus = false;

    /**
     * @param string $className
     *
     * @return CallcenterTaskRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['comment_task', 'getCommentTask', 'setCommentTask', 'commentTask'];
        yield ['comment_callcenter', 'getCommentCallcenter', 'setCommentCallcenter', 'commentCallcenter'];
        yield ['processing_update_last_at', 'getProcessingUpdateLastAt', 'setProcessingUpdateLastAt', 'processingUpdateLastAt'];
        yield ['next_task_at', 'getNextTaskAt', 'setNextTaskAt', 'nextTaskAt'];
        yield ['is_system', 'isSystem', 'setIsSystem', 'isSystem'];
        yield ['is_callcenter_answer', 'isCallcenterAnswer', 'setIsCallcenterAnswer', 'isCallcenterAnswer'];
        yield ['is_callcenter_answer_read', 'isCallcenterAnswerRead', 'setIsCallcenterAnswerRead', 'isCallcenterAnswerRead'];
        yield ['performed_at', 'getPerformedAt', 'setPerformedAt', 'performedAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['operation_id', 'getOperationId', 'setOperationId', 'operationId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
        yield ['processing_admin_id', 'getProcessingAdminId', 'setProcessingAdminId', 'processingAdminId'];
        yield ['performed_admin_id', 'getPerformedAdminId', 'setPerformedAdminId', 'performedAdminId'];
    }

    public function tableName()
    {
        return 'callcenter_tasks';
    }

    public function relations()
    {
        return [
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
            'operation' => [self::BELONGS_TO, 'MommyCom\Model\Db\CallcenterTaskOperationRecord', 'operation_id'],
            'processingAdmin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'processing_admin_id'],
            'performedAdmin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'performed_admin_id'],
        ];
    }

    public function rules()
    {
        return [
            ['comment_callcenter', 'required', 'on' => 'callcenter'],

            ['status', 'in', 'range' => [
                self::STATUS_NEW,
                self::STATUS_REPEAT,
                self::STATUS_POSTPONED,
                self::STATUS_COMPLETED,
                self::STATUS_FAILED,
            ]],

            ['comment_task, comment_callcenter', 'length'],
            ['next_task_at', 'numerical', 'integerOnly' => true],
            ['is_system, is_callcenter_answer, is_callcenter_answer_read', 'boolean'],

            ['order_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым

            ['operation_id', 'exist', 'allowEmpty' => false,
                'className' => 'MommyCom\Model\Db\CallcenterTaskOperationRecord', 'attributeName' => 'id'],

            ['status, order_id, operation_id, admin_id, is_system, comment_task', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function afterFind()
    {
        $this->_previousStatus = $this->status;
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'status' => \Yii::t($category, 'Status'),
            'comment_task' => \Yii::t($category, 'Comment to the task'),
            'comment_callcenter' => \Yii::t($category, 'Callcenter comment'),
            'is_system' => \Yii::t($category, 'System'),
            'operation_id' => \Yii::t($category, 'Action'),
            'order_id' => \Yii::t($category, 'Order'),
            'admin_id' => \Yii::t($category, 'Created by'),
            'performed_admin_id' => \Yii::t($category, 'Performed by'),
            'is_callcenter_answer' => \Yii::t($category, 'Return result'),

            'next_task_at' => \Yii::t($category, 'Next task time'),
            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),
            'performed_at' => \Yii::t($category, 'Performed at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $criteria->compare($alias . '.status', $provider->model->status);
        $criteria->compare($alias . '.operation_id', $provider->model->operation_id);
        $criteria->compare($alias . '.admin_id', $provider->model->admin_id);
        $criteria->compare($alias . '.is_system', $provider->model->is_system);
        $criteria->compare($alias . '.order_id', $provider->model->order_id, true);
        $criteria->compare($alias . '.comment_task', $provider->model->comment_task, true);

        return $provider;
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $scenario = $this->scenario;
        $adminId = 0;
        try { // for console app
            $adminId = \Yii::app()->user->id;
        } catch (CException $e) {
        }

        if ($scenario == 'repeat'
            && ($this->_previousStatus == self::STATUS_COMPLETED || $this->_previousStatus == self::STATUS_FAILED)
        ) {
            $this->status = self::STATUS_REPEAT;
            $this->created_at = time();
            $this->performed_at = 0;
        } elseif ($scenario == 'insert' && !$this->is_system) {
            $this->admin_id = $adminId;
            $this->created_at = time();
        } elseif ($scenario == 'callcenter') {
            $this->performed_admin_id = $adminId;
            $this->performed_at = time();
        }

        return $result;
    }

    /**
     * @param int $adminId
     *
     * @return bool
     */
    public function bindToAdmin(int $adminId)
    {
        $timestamp = time();

        $attributes = ['processing_admin_id' => $adminId, 'processing_update_last_at' => $timestamp];

        $condition = 'processing_admin_id = 0';
        $condition .= ' OR processing_admin_id = :processing_admin_id';
        $condition .= ' OR processing_update_last_at < :valid_time';

        $params = [
            'processing_admin_id' => $adminId,
            'valid_time' => $timestamp - self::TIME_PROCESSING_VALID,
        ];

        $result = $this->updateByPk($this->id, $attributes, $condition, $params);

        if ($result) {
            $this->refresh();
        }

        return (bool)$result;
    }

    /**
     * @return bool
     */
    public function unbindFromAdmin()
    {
        $result = $this->updateByPk($this->id, ['processing_admin_id' => 0, 'processing_update_last_at' => 0]);

        return Cast::toBool($result);
    }

    /**
     * исключаются редактируемые заказы self::admin_id > 0 и admin_update_last_at > self::TIME_PROCESSING_VALID
     *
     * @return static
     */
    public function processingNotActive()
    {
        $alias = $this->getTableAlias();
        $condition = [
            $alias . '.processing_admin_id' => 0,
            $alias . '.processing_update_last_at <' => time() - self::TIME_PROCESSING_VALID,
        ];

        $this->getDbCriteria()->addColumnCondition($condition, 'OR');
        return $this;
    }

    /**
     * @param null | integer $value , при NULL возвращает все заказы которые редактируются
     *
     * @return $this
     */
    public function processingActive($value = null)
    {
        $alias = $this->getTableAlias();

        $condition = [
            $alias . '.processing_admin_id >' => 1,
            $alias . '.processing_update_last_at >' => time() - self::TIME_PROCESSING_VALID,
        ];

        if ($value !== null) {
            $value = Cast::toUInt($value);
            $condition = [$alias . '.processing_admin_id' => $value];
        }

        $this->getDbCriteria()->addColumnCondition($condition);
        return $this;
    }

    /**
     * @return bool
     */
    public function isProcessingActive()
    {
        return $this->processing_admin_id > 0 && $this->processing_update_last_at > time() - self::TIME_PROCESSING_VALID;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function notIds(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function adminId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.admin_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function orderIds($value)
    {
        $value = Cast::toUIntArr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function orderNameSearch($value)
    {
        $value = Cast::toStr($value);

        $this->with([
            'order' => ['together' => true],
        ]);

        $this->getDbCriteria()->addSearchCondition('order.client_name', $value);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function orderSurnameSearch($value)
    {
        $value = Cast::toStr($value);
        $this->with([
            'order' => ['together' => true],
        ]);

        $this->getDbCriteria()->addSearchCondition('order.client_surname', $value);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function operationId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.operation_id' => $value,
        ]);
        return $this;
    }

    /**
     * ID наболее востребованного действия
     *
     * @return int
     */
    public function operationIdDemandedStories()
    {
        $adminId = isset(\Yii::app()->user) ? \Yii::app()->user->id : 0;
        $alias = $this->getTableAlias();
        /** @var $model CallcenterTaskRecord */
        $model = $this->adminId($adminId)->find([
                'select' => '*, COUNT(*) as counts',
                'group' => "$alias.operation_id",
                'order' => "counts DESC"]
        );

        return $model === null ? 0 : Cast::toUInt($model->operation_id);
    }

    /**
     * @param $value
     * @param bool $filterTime
     *
     * @return $this
     */
    public function statusIs($value, $filterTime = true)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $condition = [
            $alias . '.status' => $value,
        ];

        if ($filterTime) {
            //статусы зависящие от времени
            if ($value === self::STATUS_POSTPONED) {
                $condition += [$alias . '.next_task_at <' => time()];
            }
        }

        $this->getDbCriteria()->addColumnCondition($condition, 'AND', 'OR');

        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function answerRead($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_callcenter_answer_read' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function system($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_system' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function callcenterAnswer($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_callcenter_answer' => $value,
        ]);
        return $this;
    }

    /**
     * чтение ответа
     */
    public function readCallcenterAnswer()
    {
        if (($this->status == self::STATUS_COMPLETED || self::STATUS_FAILED)
            && !$this->is_callcenter_answer_read) {
            $this->is_callcenter_answer_read = 1;
        }

        $this->save(false, ['is_callcenter_answer_read']);
    }

    /**
     * helper for admin
     *
     * @return $this
     */
    public function inbox()
    {
        return $this->statusIs(self::STATUS_COMPLETED)
            ->statusIs(self::STATUS_FAILED)
            ->callcenterAnswer(true);
    }

    /**
     * helper for admin
     *
     * @return $this
     */
    public function outbox()
    {
        return $this->statusIs(self::STATUS_NEW)
            ->statusIs(self::STATUS_POSTPONED, false)
            ->statusIs(self::STATUS_REPEAT, false);
    }

    public function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_NEW => \Yii::t($category, 'New'),
            self::STATUS_REPEAT => \Yii::t($category, 'Repeated'),
            self::STATUS_POSTPONED => \Yii::t($category, 'Postponed'),
            self::STATUS_COMPLETED => \Yii::t($category, 'Completed'),
            self::STATUS_FAILED => \Yii::t($category, 'Impossible'),
        ];
    }

    public function statusReplacement()
    {
        $statusReplacements = $this->statusReplacements();

        return isset($statusReplacements[$this->status]) ? $statusReplacements[$this->status] : ' ';
    }

    public function statusPercent()
    {
        return isset($this->_statusPercents[$this->status]) ? $this->_statusPercents[$this->status] : 0;
    }
}
