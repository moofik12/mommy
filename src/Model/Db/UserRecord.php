<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class UserRecord
 *
 * @property-read integer $id
 * @property integer $invited_by //id пользователя который пригласил
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $password_hash
 * @property string $password_salt
 * @property string $telephone
 * @property string $address
 * @property integer $password_recovery_requested_at время последнего запроса на изменение пароля
 * @property integer $status
 * @property string $category
 * @property integer $psychotype
 * @property integer $discount_percent
 * @property boolean $is_email_verified 0-не подтверждено; 1-подтверждено:
 * @property string $present_promocode
 * @property integer $offer_provider рекламная площадка
 * @property string $offer_id id рекламодателя
 * @property string $offer_target_url ссылка на какую попал пользователь по офферу
 * @property boolean $offer_success выполнено ли действие по офферу
 * @property boolean $moved_search_engine с которого поисковика пришел пользователь
 * @property string $email_unsubscribe
 * @property boolean $is_system_subscribe пользователь желает получать системную рассылку
 * @property boolean $is_subscribe пользователь желает получать рассылку
 * @property boolean $is_subscribed статус рассылки
 * @property boolean $is_service_unsubscribed отписался на сервисе
 * @property boolean $is_service_possibly_subscribe возможна ли подписка
 * @property integer $landing_num по какому лендингу зарегистрировался
 * @property string $ip последний вход выполнен с ip
 * @property integer $last_order_at когда был последний заказ, если 0 тогда никогда
 * @property integer $interests_generated_at когда в последний раз обновлялась таблица интересов
 * @property integer $interests_amount Количество записей рейтингов сгенерированных для данного пользователя
 * @property integer $last_interests_mailed_at Когда было отправлено последнее письмо с интересами
 * @property integer $last_visit_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property-write $password установка нового пароля
 * @property-read $statusReplacement
 * @property-read UserRecord|null $invitedBy
 * @property-read InviteRecord $invitations
 * @property-read OrderRecord[] $orders
 * @property-read UserBlackListRecord $blackList
 * @property-read UserPartnerInviteRecord $partnerInvitedBy
 * @property-read string $fullname
 * @mixin Timestampable
 */
class UserRecord extends DoctrineActiveRecord
{
    const STATUS_ACTIVE = 0,
        STATUS_BANNED = 1,
        STATUS_DELETE = 2;

    const CATEGORY_RESELLER = 'reseller';

    const OFFER_PROVIDER_NONE = 0;
    const OFFER_PROVIDER_ACTIONPAY = 1;
    const OFFER_PROVIDER_KLUMBA = 2;
    const OFFER_PROVIDER_KIDSTUFF = 3;
    const OFFER_PROVIDER_EXPO = 4; // выставка на 2 апреля 2014 года
    const OFFER_PROVIDER_CUSTOM_CAMPAIGN = 5; // кампании с разных сайтов
    const OFFER_PROVIDER_ADMITAD = 6;
    const OFFER_PROVIDER_SSP = 7;

    const PSYCHOTYPE_UNDEFINED = 0;
    const PSYCHOTYPE_ISTEROID = 1;
    const PSYCHOTYPE_SCHIZOID = 2;
    const PSYCHOTYPE_EPILEPTOID = 3;
    const PSYCHOTYPE_ASTENIK = 4;

    /** лимит на обновление последнего запроса */
    const LAST_VISIT_LIMIT_UPDATE_PERIOD = 1800;

    /**
     * @var string
     */
    protected $_oldEmail;

    /**
     * @var string
     */
    protected $_oldTelephone;

    /**
     * @param string $className
     *
     * @return UserRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['landing_num', 'getLandingNum', 'setLandingNum', 'landingNum'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['surname', 'getSurname', 'setSurname', 'surname'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['telephone', 'getTelephone', 'setTelephone', 'telephone'];
        yield ['password_hash', 'getPasswordHash', 'setPasswordHash', 'passwordHash'];
        yield ['password_salt', 'getPasswordSalt', 'setPasswordSalt', 'passwordSalt'];
        yield ['address', 'getAddress', 'setAddress', 'address'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['category', 'getCategory', 'setCategory', 'category'];
        yield ['psychotype', 'getPsychotype', 'setPsychotype', 'psychotype'];
        yield ['discount_percent', 'getDiscountPercent', 'setDiscountPercent', 'discountPercent'];
        yield ['is_email_verified', 'isEmailVerified', 'setIsEmailVerified', 'isEmailVerified'];
        yield ['present_promocode', 'getPresentPromocode', 'setPresentPromocode', 'presentPromocode'];
        yield ['offer_provider', 'getOfferProvider', 'setOfferProvider', 'offerProvider'];
        yield ['offer_id', 'getOfferId', 'setOfferId', 'offerId'];
        yield ['offer_target_url', 'getOfferTargetUrl', 'setOfferTargetUrl', 'offerTargetUrl'];
        yield ['offer_success', 'isOfferSuccess', 'setOfferSuccess', 'offerSuccess'];
        yield ['moved_search_engine', 'getMovedSearchEngine', 'setMovedSearchEngine', 'movedSearchEngine'];
        yield ['password_recovery_requested_at', 'getPasswordRecoveryRequestedAt', 'setPasswordRecoveryRequestedAt', 'passwordRecoveryRequestedAt'];
        yield ['email_unsubscribe', 'getEmailUnsubscribe', 'setEmailUnsubscribe', 'emailUnsubscribe'];
        yield ['is_system_subscribe', 'isSystemSubscribe', 'setIsSystemSubscribe', 'isSystemSubscribe'];
        yield ['is_subscribe', 'isSubscribe', 'setIsSubscribe', 'isSubscribe'];
        yield ['is_subscribed', 'isSubscribed', 'setIsSubscribed', 'isSubscribed'];
        yield ['is_service_unsubscribed', 'isServiceUnsubscribed', 'setIsServiceUnsubscribed', 'isServiceUnsubscribed'];
        yield ['is_service_possibly_subscribe', 'isServicePossiblySubscribe', 'setIsServicePossiblySubscribe', 'isServicePossiblySubscribe'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
        yield ['last_order_at', 'getLastOrderAt', 'setLastOrderAt', 'lastOrderAt'];
        yield ['interests_generated_at', 'getInterestsGeneratedAt', 'setInterestsGeneratedAt', 'interestsGeneratedAt'];
        yield ['interests_amount', 'getInterestsAmount', 'setInterestsAmount', 'interestsAmount'];
        yield ['last_interests_mailed_at', 'getLastInterestsMailedAt', 'setLastInterestsMailedAt', 'lastInterestsMailedAt'];
        yield ['returned_at', 'getReturnedAt', 'setReturnedAt', 'returnedAt'];
        yield ['last_visit_at', 'getLastVisitAt', 'setLastVisitAt', 'lastVisitAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['invited_by', 'getInvitedBy', 'setInvitedBy', 'invitedBy'];
    }

    public function tableName()
    {
        return 'users';
    }

    public function relations()
    {
        return [
            'invitedBy' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'invited_by'],
            'invitations' => [self::HAS_MANY, 'MommyCom\Model\Db\InviteRecord', 'user_id'],
            'orders' => [self::HAS_MANY, 'MommyCom\Model\Db\OrderRecord', 'user_id'],
            'blackList' => [self::HAS_ONE, 'MommyCom\Model\Db\UserBlackListRecord', 'user_id'],
            'partnerInvitedBy' => [self::HAS_ONE, 'MommyCom\Model\Db\UserPartnerInviteRecord', 'user_id'],
        ];
    }

    public function rules()
    {
        return [
            ['email, name, surname, telephone', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['email', 'required'],

            ['name', 'length', 'min' => 2, 'max' => 30,
                'tooShort' => \Yii::t('common', 'Имя слишком короткое'), 'tooLong' => \Yii::t('common', '30 character limit')],
            //array('name', 'match', 'pattern' => '/^([А-яA-z -]+)$/iu', 'message' => 'В имени недопустимые символы!'),

            ['surname', 'length', 'min' => 2, 'max' => 60,
                'tooShort' => \Yii::t('common', 'Фамилия слишком короткая'), 'tooLong' => \Yii::t('common', '60 character limit')],
            //array('surname', 'match', 'pattern' => '/^([А-яA-z -]+)$/iu', 'message' => 'В фамилии недопустимые символы!'),

            ['email', 'email', 'message' => \Yii::t('common', 'E-mail is not valid')],
            ['email, email_unsubscribe', 'length', 'max' => 128, 'tooLong' => \Yii::t('common', '128 character limit')],

            ['email', 'unique'],

            ['invited_by', 'exist', 'attributeName' => 'id', 'on' => 'invitedBy'],

            ['status', 'in', 'range' => [
                self::STATUS_ACTIVE,
                self::STATUS_BANNED,
                self::STATUS_DELETE,
            ]],

            ['category', 'in', 'range' => [
                self::CATEGORY_RESELLER,
            ], 'allowEmpty' => true],

            ['psychotype', 'in', 'range' => self::psychotypes(false)],
            ['psychotype', 'default', 'value' => self::PSYCHOTYPE_UNDEFINED],

            ['offer_provider', 'in', 'range' => [
                self::OFFER_PROVIDER_NONE,
                self::OFFER_PROVIDER_ACTIONPAY,
                self::OFFER_PROVIDER_KLUMBA,
                self::OFFER_PROVIDER_KIDSTUFF,
                self::OFFER_PROVIDER_EXPO,
                self::OFFER_PROVIDER_CUSTOM_CAMPAIGN,
                self::OFFER_PROVIDER_ADMITAD,
                self::OFFER_PROVIDER_SSP,
            ]],
            ['offer_id, offer_target_url', 'length'],
            ['offer_id, offer_target_url', 'filter', 'filter' => [Utf8::class, 'trim']],

            ['moved_search_engine', 'length', 'max' => 32],

            ['is_email_verified', 'boolean'],

            ['password_recovery_requested_at', 'numerical', 'integerOnly' => true],

            ['telephone', 'length', 'min' => 6, 'max' => 20,
                'tooShort' => \Yii::t('common', 'Телефон слишком короткий'), 'tooLong' => \Yii::t('common', '20 character limit')],

            ['address', 'length', 'min' => 6, 'max' => 20000, 'tooLong' => \Yii::t('common', 'Address is too long')],

            ['is_system_subscribe, is_subscribe', 'default', 'value' => 1],
            ['is_system_subscribe, is_subscribe, is_subscribed, is_service_possibly_subscribe, is_service_unsubscribed', 'boolean'],

            ['landing_num', 'numerical', 'integerOnly' => true],

            ['discount_percent', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 15],

            ['present_promocode', 'exists', 'className' => 'MommyCom\Model\Db\PromocodeRecord', 'attributeName' => 'promocode', 'allowEmpty' => true],

            //grid
            ['id, name, surname, email, telephone, status, address, ip'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => '#',
            'name' => \Yii::t($category, 'Your name'),
            'surname' => \Yii::t($category, 'Your surname'),
            'invited_by' => \Yii::t($category, 'Invitation from'),
            'telephone' => \Yii::t($category, 'Phone'),
            'address' => \Yii::t($category, 'Address'),

            'status' => \Yii::t($category, 'Status'),
            'is_email_verified' => \Yii::t($category, 'E-mail confirmed'),

            'created_at' => \Yii::t($category, 'Created'),
            'updated_at' => \Yii::t($category, 'Updated on'),

            'discount_percent' => \Yii::t($category, 'A discount %'),

            'moved_search_engine' => \Yii::t($category, 'Сame from a search engine'),

            'present_promocode' => \Yii::t($category, 'Promotional code'),

            'fullname' => \Yii::t($category, 'Full name of user'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        //по умолчанию 0 и срабатывает валидатор exist
        if ($this->scenario == 'invitedBy' && Cast::toUInt($this->invited_by) === 0) {
            $this->invited_by = '';
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        //вынесено в контроллер auth/registration
        /*
        $user = \Yii::app()->user;
        if ($user->hasState('invitedBy') && $user->isGuest && $this->scenario == 'register') {
            $this->invited_by = intval($user->getState('invitedBy'));
        }*/

        //отписка предыдущего email
        if ($this->_oldEmail != $this->email && $this->email_unsubscribe === null) {
            $this->email_unsubscribe = $this->_oldEmail;
        }

        if ($this->invited_by < 0 || $this->invitedBy === null) {
            $this->invited_by = 0;
        }

        if ($this->isNewRecord) {
            if (empty($this->ip) && isset($_SERVER['REMOTE_ADDR'])) {
                $this->ip = $_SERVER['REMOTE_ADDR'];
            }
        }

        return $result;
    }

    public function afterSave()
    {
        $email = $this->_oldEmail && $this->_oldEmail != $this->email ? Utf8::trim($this->_oldEmail) : null;
        $telephone = $this->_oldTelephone && $this->_oldTelephone != $this->telephone ? Utf8::trim($this->_oldTelephone) : null;

        if ($email || $telephone) {
            $model = new UserChangeRecord();
            $model->user_id = $this->id;
            $model->email = $email;
            $model->telephone = $telephone;

            $model->save();
        }

        parent::afterSave();
    }

    public function afterFind()
    {
        $this->_oldEmail = $this->email;
        $this->_oldTelephone = $this->telephone;

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.surname', $model->surname, true);
        $criteria->compare($alias . '.email', $model->email, true);
        $criteria->compare($alias . '.telephone', $model->telephone, true);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.category', $model->category);

        return $provider;
    }

    /**
     * @param bool $isNeedSave Нужно ли сохранить состояние или только обновить
     *
     * @return bool
     */
    public function updateLastVisit($isNeedSave = false)
    {
        if ($this->last_visit_at + self::LAST_VISIT_LIMIT_UPDATE_PERIOD < time()) {
            $this->last_visit_at = time();
            if (isset($_SERVER['REMOTE_ADDR'])) {
                $this->ip = $_SERVER['REMOTE_ADDR'];
            }

            if ($isNeedSave) {
                $this->save(false, ['ip', 'last_visit_at']);
            }

            return true;
        }

        return false;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephone($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.telephone' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function telephoneLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.telephone', $value, true);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value, true, 'OR');
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function surnameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.surname', $value, true, 'OR');
        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function emailIs($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toStr($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.email' => $value,
        ]);
        return $this;
    }

    /**
     * @param $values
     *
     * @return static
     */
    public function emailIn($values)
    {
        $alias = $this->getTableAlias();
        $values = Cast::toStrArr($values);

        $this->getDbCriteria()->addInCondition($alias . '.email', $values);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function emailLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.email', $value, true, 'OR');
        return $this;
    }

    /**
     * @return static
     */
    public function onlyHasTelephone()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.telephone <> ' . '""');
        return $this;
    }

    /**
     * @return static
     */
    public function onlyNotHasTelephone()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.telephone = ' . '""');
        return $this;
    }

    /**
     * @return static
     */
    public function onlyEmailUnsubscribe()
    {
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addCondition($alias . '.email_unsubscribe IS NOT NULL');
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function emailVerified($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toBool($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_email_verified' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function status($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function psyhotypeIs($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.psychotype' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function psyhotypeIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.psychotype', $values);

        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return $this
     */
    public function systemSubscribe($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toBool($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_system_subscribe' => Cast::toStr($value),
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return $this
     */
    public function subscribe($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toBool($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_subscribe' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return $this
     */
    public function subscribed($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toBool($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_subscribed' => $value,
        ]);
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return $this
     */
    public function servicePossiblySubscribe($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toBool($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_service_possibly_subscribe' => $value,
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function subscribeDiffer()
    {
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->compare(
            $alias . '.is_subscribe',
            '<>' . $alias . '.is_subscribed'
        );
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function lastOrderAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.last_order_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function lastOrderAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.last_order_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function lastVisitAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.last_visit_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function lastVisitAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.last_visit_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function lastInterestsMailedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.last_interests_mailed_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function lastInterestsMailedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.last_interests_mailed_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function interestsGeneratedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.interests_generated_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return static
     */
    public function interestsGeneratedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.interests_generated_at', ">= $value");

        return $this;
    }

    /**
     * @param int $value count
     *
     * @return static
     */
    public function interestsAmountLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.interests_amount', "<= $value");

        return $this;
    }

    /**
     * @param int $value count
     *
     * @return static
     */
    public function interestsAmountGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.interests_amount', ">= $value");

        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function offerProvider($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.offer_provider' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function offerProviderIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.offer_provider', $values);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return $this
     */
    public function offerIdIn(array $values)
    {
        $values = Cast::toStrArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.offer_id', $values);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function ipIs($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toStr($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.ip' => $value,
        ]);
        return $this;
    }

    /// item api

    /**
     * @param string $password
     * @param string $salt
     *
     * @return string
     */
    public static function encodePassword(string $password, string $salt)
    {
        static $internalSalt = 'd21903hnkjfghs7dfQW#Q@#Rasdfsd23';
        $passwordHash = hash('sha512', $internalSalt . '_' . $password . '_' . $salt);

        return $passwordHash;
    }

    /**
     * @return string
     */
    public function generateHashEmailToVerify()
    {
        return md5($this->email);
    }

    /**
     * @return string
     */
    public function generateHashPasswordRecovery()
    {
        return hash('sha512', $this->id . '_' . $this->password_recovery_requested_at . '_' . $this->password_hash);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        if (empty($this->password_salt)) {
            $this->password_salt = Random::alphabet(20);
        }

        //обновление hash на сервисах при изменении пароля
        if ($this->is_subscribe && $this->is_service_possibly_subscribe) {
            $this->is_subscribed = 0;
        }

        $this->password_hash = $this->encodePassword($password, $this->password_salt);
    }

    /**
     * Возвращает хеш который нужен для авторизации по хешу
     *
     * @return string
     */
    public function getLoginHash()
    {
        static $internalSalt = '!@%@zfadl;faFz';
        return hash('md5', $internalSalt . '_' . $this->id . '_' . $this->password_hash . '_' . $this->password_salt);
    }

    /**
     * @param string $route
     * @param string|array $redirectTo
     *
     * @return string
     */
    public function buildLoginUrl($route, $redirectTo = '')
    {
        $id = $this->id;
        $hash = $this->getLoginHash();

        $app = \Yii::app();

        if (is_array($redirectTo)) {
            $redirectRoute = $redirectTo[0];
            $redirectParams = $redirectTo;
            unset($redirectParams[0]);
            $redirectTo = $app->createAbsoluteUrl($redirectRoute, $redirectParams);
        }

        $url = $app->createAbsoluteUrl($route, ['id' => $id, 'hash' => $hash, 'redirectTo' => $redirectTo]);

        if (strpos($url, 'http') === 0) {
            return $url;
        } else {
            return $app->getRequest()->getHostInfo('') . $url;
        }
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * @return array
     */
    public function getBuyStatistics()
    {
        /* @var $data OrderRecord */
        $telephones = [$this->telephone];
        $telephones = array_merge($telephones, OrderRecord::model()->userId($this->id)->findColumnDistinct('telephone'));
        $telephones = ArrayUtils::onlyNonEmpty(array_unique(Utf8::trimArr($telephones)));

        $users = [$this];
        foreach ($telephones as $telephone) {
            if (strlen($telephone) < 7) {
                continue;
            }
            $users = array_merge($users, UserRecord::model()->telephoneLike($telephone)->limit(10)->findAll());
            $users = array_merge($users, ArrayUtils::getColumn(
                OrderRecord::model()->telephoneLike($telephone)->limit(10)->with('user')->findAll(), 'user')
            );
        }
        $users = ArrayUtils::changeKeyColumn($users, 'id');

        $additionalOrders = OrderRecord::model()->userIdIn(array_keys($users))->findAll();
        /* @var $additionalOrders OrderRecord[] */
        $additionalTelephones = ArrayUtils::getColumn($additionalOrders, 'telephone');

        foreach ($additionalOrders as $order) {
            $users[$order->user->id] = $order->user;

            $additionalTelephones[] = $order->telephone;
            $additionalTelephones[] = $order->user->telephone;
        }

        $telephones = array_merge($telephones, $additionalTelephones);
        $telephones = ArrayUtils::onlyNonEmpty(array_unique(Utf8::trimArr($telephones)));

        $userIds = array_keys($users);

        $result = [];
        $result['orderCount'] = OrderRecord::model()->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_MAILED)->userIdIn($userIds)->count();

        $result['buyoutUnPayedCount'] = OrderBuyoutRecord::model()->userIdIn($userIds)->status(OrderBuyoutRecord::STATUS_UNPAYED)->count();
        $result['buyoutPayedCount'] = OrderBuyoutRecord::model()->userIdIn($userIds)->status(OrderBuyoutRecord::STATUS_PAYED)->count();
        $result['buyoutCancelledCount'] = OrderBuyoutRecord::model()->userIdIn($userIds)->status(OrderBuyoutRecord::STATUS_CANCELLED)->count();

        $possibleReturnIds = OrderBuyoutRecord::model()->userIdIn($userIds)->status(OrderBuyoutRecord::STATUS_PAYED)->findColumnDistinct('order_id');

        $result['returns'] = OrderReturnRecord::model()->orderIdIn($possibleReturnIds)->statusIn([
            OrderReturnRecord::STATUS_UNCONFIGURED,
            OrderReturnRecord::STATUS_PARTIAL_CONFIGURED,
            OrderReturnRecord::STATUS_CONFIGURED,
            OrderReturnRecord::STATUS_NEED_PAY,
            OrderReturnRecord::STATUS_PAYED,
        ])->findColumnDistinctCount('order_id');

        $result['telephones'] = $telephones;
        $result['userIds'] = $userIds;
        $result['userEmails'] = array_unique(ArrayUtils::getColumn($users, 'email'));

        return $result;
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function psychotypes($replacements = true)
    {
        $psychotypes = [
            self::PSYCHOTYPE_UNDEFINED => \Yii::t('skip', 'Unknown'),
            self::PSYCHOTYPE_ISTEROID => \Yii::t('skip', 'hysteroid'),
            self::PSYCHOTYPE_SCHIZOID => \Yii::t('skip', 'schizoid'),
            self::PSYCHOTYPE_EPILEPTOID => \Yii::t('skip', 'epileptoid'),
            self::PSYCHOTYPE_ASTENIK => \Yii::t('skip', 'asthenic'),
        ];

        return $replacements ? $psychotypes : array_keys($psychotypes);
    }

    /**
     * @return array
     */
    public function statusReplacements()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('backend', 'Active'),
            self::STATUS_BANNED => \Yii::t('backend', 'Blocked'),
            self::STATUS_DELETE => \Yii::t('backend', 'Deleted'),
        ];
    }

    /**
     * @return array
     */
    public function categoryReplacements()
    {
        return [
            self::CATEGORY_RESELLER => \Yii::t('backend', 'Reseller'),
        ];
    }

    /**
     * @return array
     */
    public function getCategoryReplacement()
    {
        $replacements = $this->categoryReplacements();
        return isset($replacements[$this->category]) ? $replacements[$this->category] : '';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @param $cash
     * @param bool $ceil
     *
     * @return float|int
     */
    public function getDiscountCost($cash, $ceil = true)
    {
        $cost = $cash * $this->discount_percent / 100;

        return $ceil ? Cast::toUInt($cost) : Cast::toUFloat(round($cost, 2));
    }

    /**
     * Подписан ли пользователь к промо рассылкам
     *
     * @return bool
     */
    public function getIsDistributionSubscribe()
    {
        return $this->is_subscribe && $this->is_subscribed;
    }

    /**
     * Закрепить за партнером
     *
     * @param $partnerId
     * @param int $typeIncoming
     * @param string $urlReferer
     * @param string $utmParams
     */
    public function assignToPartner(
        $partnerId,
        $urlReferer = '',
        $utmParams = '',
        $typeIncoming = UserPartnerInviteRecord::TYPE_INCOMING_PERSONAL_PARTNER_URL
    ) {
        $inviteUser = UserPartnerInviteRecord::model()->userId($this->id)->typeIncoming($typeIncoming)->find();
        if ($inviteUser === null) {
            $inviteUser = new UserPartnerInviteRecord();
            $inviteUser->partner_id = $partnerId;
            $inviteUser->user_id = $this->id;
            $inviteUser->url_referer = $urlReferer;
            $inviteUser->utm_params = $utmParams;
            $inviteUser->type_incoming = $typeIncoming;
            if (!$inviteUser->save()) {
                //var_dump($inviteUser->getErrors());
            }
        }
    }
}
