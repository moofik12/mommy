<?php

namespace MommyCom\Model\Db;

use CHtml;
use CJSON;
use Exception;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class AdminUserRecord
 *
 * @property-read integer $id
 * @property string $login логин
 * @property string $password пароль
 * @property string $password_salt затравка пароля
 * @property string $rules сериал-ный массив доступа (старое)
 * @property string $fullname Ф. И. О.
 * @property string $email
 * @property bool $is_root
 * @property bool $is_supplier пользователь является поставщиком
 * @property integer $status 0 - активный, 1 - забанен, 2 - удален
 * @property string $ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read string $displayName
 * @property-read AdminRoleRecord[] $roles
 * @property-read AdminRoleAssignmentRecord[] $assignments
 * @property-read AdminSuppliers $supplier
 */
class AdminUserRecord extends DoctrineActiveRecord
{
    const STATUS_ACTIVE = 0;
    const STATUS_BAN = 1;
    const STATUS_HIDE = 2;

    public $newPassword;
    public $confirmPassword;

    public $searchDateStart;
    public $searchDateEnd;
    //для хранения даты выбора при перезагрузке
    public $searchDateRangeInput;

    /**
     * @param string $className
     *
     * @return AdminUserRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['login', 'getLogin', 'setLogin', 'login'];
        yield ['password', 'getPassword', 'setPassword', 'password'];
        yield ['password_salt', 'getPasswordSalt', 'setPasswordSalt', 'passwordSalt'];
        yield ['rules', 'getRules', 'setRules', 'rules'];
        yield ['fullname', 'getFullname', 'setFullname', 'fullname'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['is_root', 'isRoot', 'setIsRoot', 'isRoot'];
        yield ['is_supplier', 'isSupplier', 'setIsSupplier', 'isSupplier'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
    }

    public function tableName()
    {
        return 'admins_users';
    }

    public function relations()
    {
        return [
            'roles' => [self::MANY_MANY, 'MommyCom\Model\Db\AdminRoleRecord', 'admins_roles_assignments(admin_id, role_id)', 'together' => true],
            'assignments' => [self::HAS_MANY, AdminRoleAssignmentRecord::class, 'admin_id'],
            'supplier' => [self::HAS_ONE, 'MommyCom\Model\Db\AdminSuppliers', 'admin_id'],
        ];
    }

    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'unique'],
            ['login', 'length', 'min' => 4, 'max' => 32],

            ['fullname', 'length', 'max' => 100, 'allowEmpty' => true],
            ['status', 'numerical', 'min' => 0, 'max' => 9],
            ['is_root', 'boolean'],
            ['is_supplier', 'boolean'],

            ['email', 'email', 'allowEmpty' => true],
            ['ip', 'default', 'value' => \Yii::app()->request->getUserHostAddress(), 'on' => 'login'],

            ['login, fullname, email, status, is_root, is_supplier, searchDateStart, searchDateEnd, searchDateRangeInput',
                'safe', 'on' => 'searchPrivileged'],

            //смена пароля
            ['newPassword', 'required', 'on' => 'insert'],
            ['newPassword', 'length', 'min' => 6, 'max' => 20, 'on' => 'insert, update', 'allowEmpty' => true],
            ['confirmPassword', 'compare', 'compareAttribute' => 'newPassword', 'on' => 'insert, update'],
        ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'login' => \Yii::t($category, 'Login'),
            'password' => \Yii::t($category, 'Password'),
            'fullname' => \Yii::t($category, 'Full name'),
            'roles' => \Yii::t($category, 'Role'),
            'status' => \Yii::t($category, 'Status'),
            'is_root' => \Yii::t($category, 'Superuser'),
            'is_supplier' => \Yii::t($category, 'Supplier'),

            'newPassword' => \Yii::t($category, 'Password'),
            'confirmPassword' => \Yii::t($category, 'Password confirmation'),

            'created_at' => \Yii::t($category, 'Registered'),
            'updated_at' => \Yii::t($category, 'Last Activity'),

            'searchDateRangeInput' => \Yii::t($category, 'Activity period'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $criteria = $provider->getCriteria();
        $criteria->compare('id', $provider->model->id);
        $criteria->compare('is_root', $provider->model->is_root);
        $criteria->compare('is_supplier', $provider->model->is_supplier);
        $criteria->compare('login', $provider->model->login, true);
        $criteria->compare('fullname', $provider->model->fullname, true);
        $criteria->compare('email', $provider->model->email, true);
        $criteria->compare('status', $provider->model->status);

        //для поиска в grid
        $criteria->compare('updated_at', '>=' . $provider->model->searchDateStart);
        $criteria->compare('updated_at', '<=' . $provider->model->searchDateEnd);

        return $provider;
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $scenario = $this->getScenario();

        if ($scenario === 'insert' || $scenario === 'update') {
            if (!empty($this->newPassword)) {
                $this->password_salt = self::_generateSalt();
                $this->password = self::encodePassword($this->newPassword, $this->password_salt);
            }

            //for console
            try {
                //только супер админ может создавать Супер Пользователей
                $this->is_root = \Yii::app()->user->isRoot() ? $this->is_root : false;
            } catch (Exception $e) {
            }
        }

        return true;
    }

    /**
     * @param array $roles
     */
    public function assignmentsRoles(array $roles)
    {
        if ($this->isNewRecord) {
            return;
        }

        foreach ($this->assignments as $assignment) {
            $assignment->delete();
        }

        foreach ($roles as $role) {
            if ($role instanceof AdminRoleRecord) {
                $model = new AdminRoleAssignmentRecord();
                $model->admin_id = $this->id;
                $model->role_id = $role->id;
                $model->save();
            }
        }
    }

    /**
     * Солит и хеширует пароль
     *
     * @param string $password
     * @param $salt
     *
     * @return string хэш пароля
     */
    public static function encodePassword($password, $salt)
    {
        static $privateKey = '@aisHomd8ts';
        return rtrim(base64_encode(hash('sha256', $privateKey . $password . $salt, true)), '=');
    }

    /**
     * @return string
     */
    protected static function _generateSalt()
    {
        $salt = '';
        $arrVal = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $arrKey = array_rand($arrVal, 5);

        foreach ($arrKey as $key) {
            $salt .= $arrVal[$key];
        }

        return $salt;
    }

    /**
     * Возвращает серриализарованый массив с ролями
     *
     * @param type $roles
     *
     * @return type
     */
    protected function _serializeRoles($roles)
    {
        return CJSON::encode($roles);
    }

    /**
     * @param $roles
     *
     * @return mixed
     */
    protected function _unserializeRoles($roles)
    {
        return CJSON::decode($roles);
    }

    /**
     * @return array
     */
    public function statusReplacements()
    {
        return [
            self::STATUS_ACTIVE => \Yii::t('common', 'Active'),
            self::STATUS_BAN => \Yii::t('common', 'Locked'),
            self::STATUS_HIDE => \Yii::t('common', 'Have been deleted'),
        ];
    }

    /**
     * @return mixed (array || integer)
     */
    public function getStatusText()
    {
        return CHtml::value($this->statusReplacements(), $this->status);
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return "{$this->login} ({$this->fullname})";
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function fullnameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.fullname', $value, true);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function loginLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.login', $value, true);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function loginOrFullnameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.login', $value, true);
        $this->getDbCriteria()->addSearchCondition($alias . '.fullname', $value, true, 'OR');
        return $this;
    }

    /**
     * @param int $value
     *
     * @return static
     */
    public function statusIs($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([$alias . '.status' => $value]);
        return $this;
    }
}
