<?php

namespace MommyCom\Model\Db;

use CException;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * SmsMessageRecord class file.
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $telephone
 * @property string $text
 * @property integer $status 0 - не отправлено; 1 - отправлено; 2 - ошибка в отправке
 * @property integer $type 0-системные сообщения; 1-пользовательские
 * @property bool $is_need_to_translit
 * @property integer $admin_id администратор который оставил сообщение
 * @property integer $sent_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read string $textToSend
 * @property-read $user UserRecord
 */
class SmsMessageRecord extends DoctrineActiveRecord
{
    const STATUS_WAITING = 0;
    const STATUS_SENT = 1;
    const STATUS_ERROR = 2;

    const TYPE_SYSTEM = 0; //default
    const TYPE_CUSTOM = 1;

    //для поиска в grid
    public $userLike;
    public $searchDateStart;
    public $searchDateEnd;

    //для хранения даты выбора при перезагрузке
    public $searchDateRangeInput;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return SmsMessageRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['telephone', 'getTelephone', 'setTelephone', 'telephone'];
        yield ['text', 'getText', 'setText', 'text'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['is_need_to_translit', 'isNeedToTranslit', 'setIsNeedToTranslit', 'isNeedToTranslit'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['sent_at', 'getSentAt', 'setSentAt', 'sentAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sms_message';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
        ];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['user_id, telephone, text', 'required'],
            ['status', 'in', 'range' => [
                self::STATUS_WAITING,
                self::STATUS_SENT,
                self::STATUS_ERROR,
            ]],
            ['type', 'in', 'range' => [
                self::TYPE_SYSTEM,
                self::TYPE_CUSTOM,
            ]],
            ['text', 'length', 'max' => 240],
            ['telephone', 'length', 'max' => 20],
            ['telephone', 'validateTelephone'],

            ['sent_at', 'numerical'],
            ['is_need_to_translit', 'default', 'value' => 1],
            ['is_need_to_translit', 'boolean'],

            // The following rule is used by search().
            ['user_id, userLike, telephone, status, text, searchDateStart, searchDateEnd, searchDateRangeInput'
                , 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'user_id' => \Yii::t('common', 'Addressee'),
            'telephone' => \Yii::t('common', 'Phone number'),
            'text' => \Yii::t('common', 'Message'),
            'sent_at' => \Yii::t('common', 'Send at'),
            'status' => \Yii::t('common', 'Status'),
            'type' => \Yii::t('common', 'Message type'),
            'is_need_to_translit' => \Yii::t('common', 'Translate text'),

            'created_at' => \Yii::t('common', 'Created'),
            'update_at' => \Yii::t('common', 'Updated on'),

            'searchDateRangeInput' => \Yii::t('common', 'period'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * валидация телефонных номеров Украины!
     *
     * @param string $attribute
     * @param string $params
     */
    public function validateTelephone($attribute, $params)
    {
        $phone = $this->$attribute;

        if (preg_match('/[^+0-9- ()]/', $phone)) {
            $this->addError($attribute, \Yii::t('common', 'The phone number contains prohibited characters'));
        } else {
            $phone = preg_replace("/[^0-9]/", '', $phone);
            $phoneNumber = mb_substr($phone, -9);

            if (mb_strlen($phone) > 12) {
                $this->addError($attribute, \Yii::t('common', 'The number of digits in the phone is more than allowed'));
            } elseif (mb_strlen($phoneNumber) < 9) {
                $this->addError($attribute, \Yii::t('common', 'The phone number must contain the operator code'));
            }
        }
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        try { // for console app
            $scenario = $this->getScenario();
            if (intval($this->type) === self::TYPE_CUSTOM && $scenario === 'insert') {
                $this->admin_id = \Yii::app()->user->id;
            }
        } catch (CException $e) {
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        $criteria->with = ['user'];
        $model = $provider->model;

        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.telephone', $model->telephone, true);
        $criteria->compare($alias . '.text', $model->text, true);
        $criteria->compare($alias . '.type', $model->type);
        $criteria->compare($alias . '.is_need_to_translit', $model->is_need_to_translit);

        //для поиска в grid
        $criteria->compare('user.name', $model->userLike, true, 'OR');
        $criteria->compare('user.surname', $model->userLike, true, 'OR');
        $criteria->compare('user.email', $model->userLike, true, 'OR');

        $criteria->compare($alias . '.created_at', '>=' . $model->searchDateStart);
        $criteria->compare($alias . '.created_at', '<=' . $model->searchDateEnd);

        return $provider;
    }

    /**
     * @return array
     */
    public function statusReplacement()
    {
        return [
            self::STATUS_WAITING => \Yii::t('common', 'Waiting to send'),
            self::STATUS_SENT => \Yii::t('common', 'Sent'),
            self::STATUS_ERROR => \Yii::t('common', 'Error sending'),
        ];
    }

    /**
     * @return array
     */
    public function typeReplacement()
    {
        return [
            self::TYPE_SYSTEM => \Yii::t('common', 'System'),
            self::TYPE_CUSTOM => \Yii::t('common', 'User\'s'),
        ];
    }

    /**
     * @return array
     */
    public function defaultScope()
    {
        return [
            'limit' => 30,
        ];
    }

    /**
     * @param integer $status (self::STATUS_WAITING |    self::STATUS_SENT | self::STATUS_ERROR)
     *
     * @return static
     */
    public function status($status)
    {
        $status = intval($status);

        if (!in_array($status, [
            self::STATUS_WAITING,
            self::STATUS_SENT,
            self::STATUS_ERROR,
        ])) {
            $status = self::STATUS_WAITING;
        }

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition(
            $alias . '.status',
            [$status]
        );

        return $this;
    }

    /**
     * @param integer $type (self::TYPE_SYSTEM,    self::TYPE_CUSTOM)
     *
     * @return $this
     */
    public function type($type)
    {
        $type = intval($type);

        if (!in_array($type, [
            self::TYPE_SYSTEM,
            self::TYPE_CUSTOM,
        ])) {
            $type = self::TYPE_SYSTEM;
        }

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.type' => $type]
        );

        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.user_id' => $value]
        );

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function textIs($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.text' => $value]
        );

        return $this;
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function translit($string)
    {
        $converter = [
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e', 'є' => 'ye',
            'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'і' => 'i',
            'и' => 'i', 'й' => 'j', 'к' => 'k', 'ї' => 'yi',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'kh', 'ц' => 'ts',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '"',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Є' => 'Ye',
            'Ё' => 'Yo', 'Ж' => 'Zh', 'З' => 'Z', 'І' => 'I',
            'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Ї' => 'Yi',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'Kh', 'Ц' => 'Ts',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Shch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '"',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        ];
        $result = strtr($string, $converter);

        //upper case if needed
        if (mb_strtoupper($string) == $string) {
            $result = mb_strtoupper($result);
        }

        return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $result);
    }

    /**
     * @return string
     */
    public function getTextToSend()
    {
        return $this->is_need_to_translit ? self::translit($this->text) : $this->text;
    }
}
