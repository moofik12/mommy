<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * class ApplicationSubscriberRecord
 *
 * @property-read int $id
 * @property int $type
 * @property string $client_id
 * @property string $client_gcm_id
 * @property int $version_code
 * @property float $version_name
 * @property string last_subject
 * @property string last_body
 * @property string last_url
 * @property int $updated_at
 * @property int $created_at
 * @property-read string $typeText
 */
class ApplicationSubscriberRecord extends DoctrineActiveRecord
{
    const TYPE_ANDROID = 1,
        TYPE_IOS = 2,
        TYPE_WEB = 3;

    /**
     * @param string $className
     *
     * @return ApplicationSubscriberRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['client_id', 'getClientId', 'setClientId', 'clientId'];
        yield ['client_gcm_id', 'getClientGcmId', 'setClientGcmId', 'clientGcmId'];
        yield ['version_code', 'getVersionCode', 'setVersionCode', 'versionCode'];
        yield ['version_name', 'getVersionName', 'setVersionName', 'versionName'];
        yield ['last_subject', 'getLastSubject', 'setLastSubject', 'lastSubject'];
        yield ['last_body', 'getLastBody', 'setLastBody', 'lastBody'];
        yield ['last_url', 'getLastUrl', 'setLastUrl', 'lastUrl'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'application_subscribers';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['client_id', 'required'],
            ['client_id', 'unique'],
            ['client_id', 'length', 'max' => 40],

            ['type', 'in', 'range' => self::types(false)],

            ['version_code', 'numerical', 'min' => 0, 'max' => 999, 'integerOnly' => true],
            ['version_name', 'length', 'max' => 10],
            ['last_subject, last_body, last_url', 'length', 'max' => 255, 'encoding' => false],

            ['client_gcm_id', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function client($value)
    {
        $value = Cast::toStr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.client_id' => $value]
        );

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function device($value)
    {
        $value = Cast::toStr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.client_gcm_id' => $value]
        );

        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function typeIs($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition(
            [$alias . '.type' => $value]
        );

        return $this;
    }

    /**
     * @return string
     */
    public static function generateWebClientId()
    {
        return 'WEB-' . microtime();
    }

    /**
     * @param bool $replacements
     *
     * @return array
     */
    public static function types($replacements = true)
    {
        static $types = [
            self::TYPE_ANDROID => 'ANDROID',
            self::TYPE_IOS => 'IOS',
            self::TYPE_WEB => 'WEB',
        ];

        return $replacements ? $types : array_keys($types);
    }

    /**
     * @param integer $type
     * @param string $default
     *
     * @return string
     */
    public static function typeReplacement($type, $default = '')
    {
        $types = static::types();
        return isset($types[$type]) ? $types[$type] : $default;
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function typeText($default = '')
    {
        return static::typeReplacement($this->type, $default);
    }
}
