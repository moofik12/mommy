<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CDbCriteria;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Validator\CountriesMobileNumbersValidator;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\RegionalTranslator;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class SupplierRecord
 *
 * @property-read integer $id
 * @property int $contract_id
 * @property string $name
 * @property string $full_name
 * @property string $phone
 * @property string $email
 * @property int $is_inform включена возможность уведомления разного рода событий
 * @property boolean $is_approved
 * @property string $login
 * @property string $password
 * @property string $password_salt
 * @property string $display_name
 * @property string $logo
 * @property string $address
 * @property integer $status
 * @property string $ip
 * @property string $settings_json
 * @property integer $last_activity_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $messenger
 * @property string $certifiable_sections
 * @property-read string $displayName
 * @property-read string $statusText
 * @property-read integer $eventCount количество событий у этого поставщика
 * @property-read EventRecord[] $events
 * @property-read SupplierContractRecord $contract
 * @property-read SupplierContractRecord $contractConnected
 */
class SupplierRecord extends DoctrineActiveRecord
{
    use ApplicationTrait;

    public const ACCESS_RULE = '["SUPPLIEREVENTCONTROLLER.*"]';

    private const STATUS_ACTIVE = 0; //возможен вход в админку, запуск его акций
    private const STATUS_BAN = 1; //вход в админку закрыт, запуск его акций разрешён
    private const STATUS_DELETED = 2; //вход и запуск акций запрещён

    public const TYPE_LEGAL_ENTITY = 0;
    public const TYPE_INDIVIDUAL = 1;

    public $newPassword;
    public $confirmPassword;

    /**
     * @param string $className
     *
     * @return SupplierRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['phone', 'getPhone', 'setPhone', 'phone'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['full_name', 'getFullName', 'setFullName', 'fullName'];
        yield ['login', 'getLogin', 'setLogin', 'login'];
        yield ['password', 'getPassword', 'setPassword', 'password'];
        yield ['password_salt', 'getPasswordSalt', 'setPasswordSalt', 'passwordSalt'];
        yield ['display_name', 'getDisplayName', 'setDisplayName', 'displayName'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['settings_json', 'getSettingsJson', 'setSettingsJson', 'settingsJson'];
        yield ['is_inform', 'isInform', 'setIsInform', 'isInform'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
        yield ['last_activity_at', 'getLastActivityAt', 'setLastActivityAt', 'lastActivityAt'];
        yield ['address', 'getAddress', 'setAddress', 'address'];
        yield ['logo', 'getLogo', 'setLogo', 'logo'];
        yield ['is_approved', 'isApproved', 'setIsApproved', 'isApproved'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['contract_id', 'getContractId', 'setContractId', 'contractId'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['registration_date', 'getRegistrationDate', 'setRegistrationDate', 'registrationDate'];
        yield ['chief_name', 'getChiefName', 'setChiefName', 'chiefName'];
        yield ['chief_passport', 'getChiefPassport', 'setChiefPassport', 'chiefPassport'];
        yield ['license', 'getLicense', 'setLicense', 'license'];
        yield ['npwp', 'getNpwp', 'setNpwp', 'npwp'];
        yield ['tdp', 'getTdp', 'setTdp', 'tdp'];
        yield ['bank_name', 'getBankName', 'setBankName', 'bankName'];
        yield ['bank_address', 'getBankAddress', 'setBankAddress', 'bankAddress'];
        yield ['bank_account', 'getBankAccount', 'setBankAccount', 'bankAccount'];
        yield ['messenger', 'getMessenger', 'setMessenger', 'messenger'];
        yield ['certifiable_sections', 'getCertifiableSections', 'setCertifiableSections', 'certifiableSections'];
    }

    public function tableName()
    {
        return 'suppliers';
    }

    public function relations()
    {
        return [
            'events' => [self::HAS_MANY, 'MommyCom\Model\Db\EventRecord', 'supplier_id'],
            'eventCount' => [self::STAT, 'MommyCom\Model\Db\EventRecord', 'supplier_id'],
            'adminSupplier' => [self::HAS_ONE, 'MommyCom\Model\Db\AdminSuppliers', 'supplier_id'],
            'contract' => [self::BELONGS_TO, 'MommyCom\Model\Db\SupplierContractRecord', 'contract_id'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name', 'required'],
            ['name', 'length', 'min' => 1, 'max' => 45],
            ['name', 'unique'],

            ['email', 'email'],
            ['email', 'length', 'max' => 45],
            ['address', 'length', 'max' => 4096],
            ['phone', 'length', 'max' => 25],
            ['phone', CountriesMobileNumbersValidator::class],

            ['status', 'in', 'range' => self::statuses(false)],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],

            // хак чтобы CExistValidator считал значение "0" пустым
            ['contract_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],

            ['full_name', 'length', 'max' => 250],
            ['display_name', 'length', 'max' => 64],

            ['contract_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierContractRecord', 'attributeName' => 'id'],
            ['is_inform', 'boolean'],
            ['is_approved', 'boolean'],
            ['ip', 'length', 'max' => 40],

            ['id, name, email, phone, contract_id, is_inform', 'safe', 'on' => 'searchPrivileged'],
            /* Встроенный в Yii валидатор файлов ломается при попытке присвоить файл в свойство модели вручную
             * (такой баг только с файлами, неправильно резолвится имя свойства - CUploadedFile, строка 59),
             * поэтому валидируем через callback */
            ['logo', 'validateImage'],
        ];
    }

    /**
     * @return array
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function attributeLabels()
    {
        $translator = Translator::get();

        if ($this->app()->user->getModel()->is_supplier) {
            $translator = RegionalTranslator::get();
        }

        return [
            'id' => $translator->t('№'),
            'name' => $translator->t('Name'),
            'phone' => $translator->t('Phone'),
            'events' => $translator->t('Flash-sales'),
            'eventCount' => $translator->t('Qty. of flash-sales'),
            'contract_id' => $translator->t('Contract'),
            'is_inform' => $translator->t('Notifications is switched on'),
            'is_approved' => $translator->t('Verified supplier'),
            'full_name' => $translator->t('Full Name'),
            'login' => $translator->t('Login'),
            'password' => $translator->t('Password'),
            'status' => $translator->t('Status'),
            'statusText' => $translator->t('Status'),
            'display_name' => $translator->t('Name to show on website'),
            'displayName' => $translator->t('Supplier'),
            'newPassword' => $translator->t('Password'),
            'confirmPassword' => $translator->t('Password confirmation'),
            'created_at' => $translator->t('Registered'),
            'updated_at' => $translator->t('Updated'),
            'last_activity_at' => $translator->t('Last Activity'),
            'address' => $translator->t('Address'),
            'registration_date' => $translator->t('Registration Date'),
            'chief_name' => $translator->t('Chief Name'),
            'chief_passport' => $translator->t('Chief Passport'),
            'license' => $translator->t('License'),
            'bank_name' => $translator->t('Bank Name'),
            'bank_address' => $translator->t('Bank Address'),
            'email' => $translator->t('Email'),
            'bank_account' => $translator->t('Bank Account'),
            'messenger' => $translator->t('Messenger'),
            'certifiable_sections' => $translator->t('Certifiable sections'),
            'logo' => $translator->t('Logo'),
        ];
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /**
     * Фильтр по названию
     *
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value, true);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function phoneLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.phone', $value, true);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function emailLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.email', $value, true);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function contractId($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.contract_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function approved(bool $value)
    {
        $this->getDbCriteria()->addColumnCondition([
            $this->getTableAlias() . '.is_approved' => $value,
        ]);

        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function contractIdIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.contract_id', $values);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function hasEnableAccess($value = true)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria();
        if ($value) {
            $criteria->addCondition($alias . '.status=' . self::STATUS_ACTIVE);
            $criteria->addCondition($alias . '.login<>""');
        } else {
            $criteria->addCondition($alias . '.status<>' . self::STATUS_ACTIVE, 'OR');
            $criteria->addCondition($alias . '.login=""', 'OR');
        }

        $this->getDbCriteria()->mergeWith($criteria);

        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.phone', $model->phone, true);
        $criteria->compare($alias . '.email', $model->email, true);
        $criteria->compare($alias . '.contract_id', $model->contract_id);
        $criteria->compare($alias . '.is_inform', $model->is_inform);
        $criteria->compare($alias . '.full_name', $model->full_name, true);

        return $provider;
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $scenario = $this->getScenario();
        if ($scenario === 'createAccess' || $scenario === 'changePassword') {
            if (!empty($this->newPassword)) {
                $this->password_salt = self::_generateSalt();
                $this->password = self::encodePassword($this->newPassword, $this->password_salt);
            }
        }

        return true;
    }

    /**
     * @return string
     */
    protected static function _generateSalt()
    {
        $salt = '';
        $arrVal = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $arrKey = array_rand($arrVal, 5);

        foreach ($arrKey as $key) {
            $salt .= $arrVal[$key];
        }

        return $salt;
    }

    /**
     * Солит и хеширует пароль
     *
     * @param string $password
     * @param $salt
     *
     * @return string хэш пароля
     */
    public static function encodePassword($password, $salt)
    {
        static $privateKey = '@aisHomd8ts';
        return rtrim(base64_encode(hash('sha256', $privateKey . $password . $salt, true)), '=');
    }

    /**
     * @param bool $withReplace
     *
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function statuses($withReplace = true)
    {
        $statuses = [
            self::STATUS_ACTIVE => Translator::t('Active'),
            self::STATUS_BAN => Translator::t('Locked'),
            self::STATUS_DELETED => Translator::t('Deleted'),
        ];

        return $withReplace ? $statuses : array_keys($statuses);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getStatusText($default = '')
    {
        $statuses = self::statuses();

        return isset($statuses[$this->status]) ? $statuses[$this->status] : $default;
    }

    /**
     * @return SupplierContractRecord|null
     */
    public function getContractConnected()
    {
        if ($this->contract) {
            return $this->contract;
        }

        return SupplierContractRecord::model()->cache(60)->isDefault(true)->find();
    }

    /**
     * @return bool
     */
    public function isInformEnable()
    {
        return !!$this->is_inform;
    }

    /**
     * @return bool
     */
    public function isInformPossible()
    {
        return (!empty($this->email) && $this->isInformEnable());
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        if (empty($this->settings_json)) {
            return [];
        }

        /** @var array|null $deliveryAttributes */
        $deliveryAttributes = json_decode($this->settings_json, true, 3);

        return $deliveryAttributes ? $deliveryAttributes : [];
    }

    /**
     * @param $name
     *
     * @return array
     */
    public function getSetting($name)
    {
        $settings = $this->getSettings();
        if (isset($settings[$name])) {
            return $settings[$name];
        }

        return [];
    }

    /**
     * @param string $name
     */
    public function removeSetting($name)
    {
        $settings = $this->getSettings();

        if (isset($settings[$name])) {
            unset($settings[$name]);
        }

        $this->settings_json = json_encode($settings, JSON_UNESCAPED_UNICODE);
    }

    /* API */
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name ? $this->display_name : $this->name;
    }

    /**
     * @return bool
     */
    public function isHasAccess()
    {
        return !empty($this->login) && !empty($this->password);
    }

    /**
     * @return bool
     */
    public function isEnableAccess()
    {
        return $this->isHasAccess() && $this->status == self::STATUS_ACTIVE;
    }

    /**
     * @param $attribute
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function validateImage($attribute)
    {
        if ($this->$attribute instanceof Image || is_string($this->$attribute)) {
            return;
        }

        $this->addError($attribute, Translator::t('Incorrect file type'));
    }
}
