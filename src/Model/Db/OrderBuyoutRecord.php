<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderBuyoutRecord
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $order_delivery_type copy from order->delivery_type
 * @property float $price
 * @property float $delivery_price
 * @property integer $status
 * @property boolean $is_auto_synchronizable
 * @property integer $synchronized_at
 * @property string $status_history_json стек истории статусов в json
 * @property integer $status_changed_at
 * @property integer $admin_id админ
 * @property bool $is_drop_shipping
 * @property-read array $statusHistory
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read OrderRecord $order
 * @property-read string $statusReplacement
 * @mixin Timestampable
 */
class OrderBuyoutRecord extends DoctrineActiveRecord
{
    const STATUS_UNPAYED = 0; // не оплачено
    const STATUS_PAYED = 1; // оплачено
    const STATUS_CANCELLED = 2; // не оплачено и отменено

    /**
     * @param string $className
     *
     * @return OrderBuyoutRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_drop_shipping', 'isDropShipping', 'setIsDropShipping', 'isDropShipping'];
        yield ['order_delivery_type', 'getOrderDeliveryType', 'setOrderDeliveryType', 'orderDeliveryType'];
        yield ['price', 'getPrice', 'setPrice', 'price'];
        yield ['delivery_price', 'getDeliveryPrice', 'setDeliveryPrice', 'deliveryPrice'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['status_history_json', 'getStatusHistoryJson', 'setStatusHistoryJson', 'statusHistoryJson'];
        yield ['is_auto_synchronizable', 'isAutoSynchronizable', 'setIsAutoSynchronizable', 'isAutoSynchronizable'];
        yield ['synchronized_at', 'getSynchronizedAt', 'setSynchronizedAt', 'synchronizedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
    }

    public function tableName()
    {
        return 'orders_buyouts';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['order_id', 'required'],
            ['order_id', 'unique'],

            ['price, delivery_price', 'numerical', 'min' => 0],

            ['status', 'in', 'range' => [
                self::STATUS_UNPAYED,
                self::STATUS_PAYED,
                self::STATUS_CANCELLED,
            ]],

            ['is_drop_shipping', 'boolean'],
            ['supplier_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],

            ['id, order_id, price, delivery_price, status, order_delivery_type', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'order_id' => \Yii::t($category, 'Order'),
            'order_delivery_type' => \Yii::t($category, 'Delivery type'),
            'status' => \Yii::t($category, 'Status'),
            'is_auto_synchronizable' => \Yii::t($category, 'Auto synchronizable'),
            'synchronized_at' => \Yii::t($category, 'Updated'),
            'price' => \Yii::t($category, 'Price'),
            'delivery_price' => \Yii::t($category, 'Shipping сost'),
            'is_drop_shipping' => \Yii::t($category, 'Dropshipping flash-sale'),
            'supplier_id' => \Yii::t($category, 'Supplier'),

            'created_at' => \Yii::t($category, 'Created at'),
            'updated_at' => \Yii::t($category, 'Updated'),
        ];
    }

    protected function _resolveOrderInfo()
    {
        $this->order_delivery_type = $this->order->delivery_type;

        if ($this->isNewRecord) {
            $this->price = $this->order->getPayPriceHistory(true);

            if ($this->order->tracking && $this->order->tracking->delivery_price > 0) {
                $this->delivery_price = $this->order->tracking->delivery_price;
            } else {
                $this->delivery_price = $this->order->getDeliveryPrice();
            }
        }
    }

    public function beforeValidate()
    {
        if (!parent::beforeValidate()) {
            return false;
        }

        $this->_resolveOrderInfo();

        return true;
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        $this->_resolveOrderInfo();
        $this->_updateStatusChangeStack();

        return true;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function statusIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.order_id', $values);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isDropShipping($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([$alias . '.is_drop_shipping', Cast::toStr($value)]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierIdIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.supplier_id' => $value]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderDeliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_delivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function statusChangedAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'status_changed_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function statusChangedAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'status_changed_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param boolean $value
     *
     * @return static
     */
    public function isAutoSynchronizable($value)
    {
        $value = Cast::toBool($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_auto_synchronizable' => $value,
        ]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function synchronizedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'synchronized_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function synchronizedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'synchronized_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @return static
     */
    public function onlyStatusChangeable()
    {
        $alias = $this->getTableAlias();

        $query = '(';
        $query .= $alias . '.' . 'status_changed_at' . '>=' . (time() - 86400);
        $query .= ' OR ';
        $query .= $alias . '.' . 'status' . '=' . Cast::toStr(self::STATUS_UNPAYED);
        $query .= ')';

        $this->getDbCriteria()->addCondition($query);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $aliasPrefix = '__buyout_UserId_';
        $ordersAlias = $aliasPrefix . 'order';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($ordersAlias, $relations['order']);

        // it's magic ;)
        $this->with([
            $ordersAlias => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => ['userId' => $value],
            ],
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function userIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $aliasPrefix = '__buyout_UserId_';
        $ordersAlias = $aliasPrefix . 'order';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($ordersAlias, $relations['order']);

        // it's magic ;)
        $this->with([
            $ordersAlias => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => ['userIdIn' => [$values]],
            ],
        ]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.order_delivery_type', $model->order_delivery_type);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.is_auto_synchronizable', $model->is_auto_synchronizable);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @return array
     */
    public function getStatusHistory()
    {
        return Cast::toArr(json_decode($this->status_history_json, true, 3));
    }

    /**
     * Обновляет историю смены статусов
     * записывает время изменения статуса
     */
    protected function _updateStatusChangeStack()
    {
        $statusHistory = $this->getStatusHistory();

        $prevStatus = end($statusHistory);
        if ($prevStatus === false || $prevStatus['status'] != $this->status) {
            $now = time();
            $addToHistory = [
                'status' => $this->status,
                'is_auto_synchronizable' => $this->is_auto_synchronizable,
                'admin_id' => 0,
            ];

            try { // for console app
                $user = \Yii::app()->user->getModel();
                if ($user instanceof AdminUserRecord) {
                    $addToHistory['admin_id'] = \Yii::app()->user->id;
                }
            } catch (CException $e) {
            }

            $statusHistory[$now] = $addToHistory;
            $this->status_changed_at = time();
        }
        ksort($statusHistory);

        $this->status_history_json = json_encode($statusHistory);
    }

    /// item api

    /**
     * можно ли редактировать статус
     *
     * @return bool
     */
    public function getIsStatusChangingAllowed()
    {
        if ($this->is_auto_synchronizable) {
            return false;
        }

        if ($this->status == self::STATUS_UNPAYED) {
            return true;
        }

        $statusChangedAt = $this->status_changed_at > 0 ? $this->status_changed_at : time();

        $time = new DateTime();
        $time->setTimestamp($statusChangedAt);

        $days = $time->diff(new DateTime())->d;

        return $days == 0;
    }

    /**
     * @return string
     */
    public function getOrderDeliveryTypeReplacement()
    {
        $replacements = OrderRecord::deliveryTypeReplacements();
        return isset($replacements[$this->order_delivery_type]) ? $replacements[$this->order_delivery_type] : '';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_UNPAYED => \Yii::t($category, 'Waiting for payment'),
            self::STATUS_PAYED => \Yii::t($category, 'Payed'),
            self::STATUS_CANCELLED => \Yii::t($category, 'cancelled'),
        ];
    }
}
