<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CLogger;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\PayGateway\PayGatewayReceiveResult;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class PayGatewayRecord Оплаты заказов с разных источников
 *
 * @property-read integer $id
 * @property string $provider
 * @property integer $invoice_id
 * @property string $unique_payment ID платежа
 * @property bool $is_custom создан вручную
 * @property float $amount
 * @property integer $status (@see PayGatewayReceiveResult::RECEIVE_STATUS*)
 * @property string $message
 * @property int $admin_id
 * @property int $user_id
 * @property string $user_ip
 * @property string $user_browser
 * @property int $pay_status
 * @property int $order_bank_refund_id
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read string $payStatusText
 * @property-read string $providerText
 * @property-read OrderRecord|null $order
 * @property-read OrderBankRefundRecord|null $orderBankRefund
 */
class PayGatewayRecord extends DoctrineActiveRecord
{
    private const REWARD_BONUS_PAYMENT = 0; //кол-во бонусов которые пользователь получит при оплате картой

    private const PROVIDER_PRIVATE_24 = 'privat24'; // интернет-эквайринг, полученние данных через api.privatbank.ua
    private const PROVIDER_PRIVATE_SA = 'privatSA'; // р/с в приват банке
    private const PROVIDER_INTERKASSA = 'interkassa';
    private const PROVIDER_WAYFORPAY = 'wayforpay'; //wiki.wayforpay.com/display/AD/Api+documentation
    private const PROVIDER_ESPAY = 'espay';

    public const PAY_STATUS_NOT_PROCESSED = 0;
    public const PAY_STATUS_OK = 1;
    public const PAY_STATUS_ERROR_PAY_OVER_COST = 2;
    public const PAY_STATUS_ERROR_ORDER_NOT_AVAILABLE_FOR_PAYMENT = 3;
    public const PAY_STATUS_ERROR_ORDER_SAVE = 4;
    public const PAY_STATUS_CREATED_RETURN = 5;
    public const PAY_STATUS_CANCELED_ORDER_PAY = 6;
    public const PAY_STATUS_WRONG = 7;
    public const PAY_STATUS_ERROR_ORDER_FULL_PAID = 8;

    /**
     * @param string $className
     *
     * @return PayGatewayRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['provider', 'getProvider', 'setProvider', 'provider'];
        yield ['unique_payment', 'getUniquePayment', 'setUniquePayment', 'uniquePayment'];
        yield ['is_custom', 'isCustom', 'setIsCustom', 'isCustom'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['pay_status', 'getPayStatus', 'setPayStatus', 'payStatus'];
        yield ['order_bank_refund_id', 'getOrderBankRefundId', 'setOrderBankRefundId', 'orderBankRefundId'];
        yield ['message', 'getMessage', 'setMessage', 'message'];
        yield ['user_ip', 'getUserIp', 'setUserIp', 'userIp'];
        yield ['user_browser', 'getUserBrowser', 'setUserBrowser', 'userBrowser'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['invoice_id', 'getInvoiceId', 'setInvoiceId', 'invoiceId'];
        yield ['admin_id', 'getAdminId', 'setAdminId', 'adminId'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'pay_gateway';
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', 'user_id'],
            'admin' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'admin_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'invoice_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['unique_payment, provider, invoice_id', 'required'],
            ['provider', 'type', 'type' => 'string'],
            ['provider', 'in', 'range' => self::getProviders()],
            ['pay_status', 'in', 'range' => self::getLockedPayStatuses()],
            ['status', 'in', 'range' => PayGatewayReceiveResult::getStatuses()],
            ['amount, invoice_id, user_id', 'numerical', 'min' => 0],
            ['invoice_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],

            ['message', 'type', 'type' => 'string'],
            ['message, user_ip, user_browser', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['unique_payment', 'unique'],
            ['is_custom', 'boolean'],

            // хак чтобы CExistValidator считал значение "0" пустым
            ['admin_id, order_bank_refund_id', 'filter', 'filter' => function ($value) {
                return $value == 0 ? '' : $value;
            }],

            ['admin_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['order_bank_refund_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderBankRefundRecord', 'attributeName' => 'id'],

            ['pay_status', 'payStatusValidator'],

            [
                'id, provider, invoice_category, unique_payment, invoice_id, amount, status, message, user_id, user_ip, user_browser, pay_status' .
                'created_at, updated_at',
                'safe', 'on' => 'searchPrivileged',
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function payStatusValidator($attribute, $params)
    {
        if ($this->pay_status == self::PAY_STATUS_CREATED_RETURN && empty($this->order_bank_refund_id)) {
            $this->addError($attribute, Translator::t('Невозможно выставить {attribute} в статус "{status}" если не зафиксирована выплата', ['{attribute}' => $attribute, '{status}' => $this->getPayStatusText()]));
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'provider' => Translator::t('Payment provider'),
            'invoice_id' => Translator::t('Order'),
            'unique_payment' => Translator::t('Payment ID'),
            'amount' => Translator::t('Amount'),
            'status' => Translator::t('Payment status'),
            'message' => Translator::t('Description'),
            'user_id' => Translator::t('User'),
            'is_custom' => Translator::t('Сreated manually'),
            'admin_id' => Translator::t('Operator'),
            'pay_status' => Translator::t('Payment status'),
            'payStatusText' => Translator::t('Payment status'),
            'order_bank_refund_id' => Translator::t('Refund №'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.provider', $model->provider, true);
        $criteria->compare($alias . '.unique_payment', $model->unique_payment, true);
        $criteria->compare($alias . '.invoice_id', $model->invoice_id, true);
        $criteria->compare($alias . '.amount', $model->amount, true);
        $criteria->compare($alias . '.message', $model->message, true);

        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.admin_id', $model->admin_id);
        $criteria->compare($alias . '.user_ip', $model->user_ip, true);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.is_custom', $model->is_custom);
        $criteria->compare($alias . '.pay_status', $model->pay_status);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function invoiceIdIs($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.invoice_id' => $value,
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function uniquePayment($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.unique_payment' => $value,
        ]);

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function statusIs($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => "$value",
        ]);

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function statusIn(array $value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.status', $value);

        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function payStatusIs($value)
    {
        $value = Cast::toUInt($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.pay_status' => "$value",
        ]);

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function payStatusIn(array $value)
    {
        $value = Cast::toUIntArr($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addInCondition($alias . '.pay_status', $value);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function messageIs($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.message' => $value,
        ]);

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function messageLike($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.message', $value, true);

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }

    /**
     * @return boolean whether the saving should be executed. Defaults to true.
     */
    protected function beforeSave()
    {
        $beforeSave = parent::beforeSave();
        if (!$beforeSave) {
            return $beforeSave;
        }

        $this->_addPaymentToOrder();

        return $beforeSave;
    }

    protected function afterSave()
    {
        $this->_rewardBonusPayment();
        parent::afterSave();
    }

    /**
     * получение вознаграждения при оплате заказа
     */
    private function _rewardBonusPayment()
    {
        if ($this->isNewRecord && self::REWARD_BONUS_PAYMENT > 0) {
            $userId = $this->user_id;
            $message = Translator::t('Получение бонуса за оплату заказа №') . $this->invoice_id;

            if (!$userId) {
                $order = OrderRecord::model()->idIn([$this->invoice_id])->find();

                if ($order && $order->id == $this->invoice_id) {
                    $userId = $order->user_id;
                }
            }

            $bonusPoint = new UserBonuspointsRecord();
            $bonusPoint->user_id = $userId;
            $bonusPoint->points = PayGatewayRecord::REWARD_BONUS_PAYMENT;
            $bonusPoint->message = $message;
            $bonusPoint->is_custom = false;
            $bonusPoint->type = UserBonuspointsRecord::TYPE_INNER;

            if (!$bonusPoint->save()) {
                Yii::log(Translator::t('Ошибка начисления бонуса при оплате заказа №{id}, номер платежа {payment} через {provider} на сумму {amount}',
                        [
                            '{id}' => $this->invoice_id,
                            '{payment}' => $this->unique_payment,
                            '{provider}' => $this->provider,
                            '{amount}' => $this->amount,
                        ]
                    ) . ' Errors: ' . print_r($bonusPoint->getErrors(), true),
                    CLogger::LEVEL_ERROR, 'paygateway');
            }
        }
    }

    /**
     * Зачисление оплаты в заказ при создании успешной оплаты
     */
    private function _addPaymentToOrder()
    {
        $order = OrderRecord::model()->findByPk($this->invoice_id);

        if ($order) {
            if ($this->isNewRecord && $this->status == PayGatewayReceiveResult::RECEIVE_STATUS_OK) {
                $allPayment = Cast::toUFloat($order->card_payed) + Cast::toUFloat($this->amount);

                if ($order->getPayPrice() == 0) {
                    $this->pay_status = self::PAY_STATUS_ERROR_ORDER_FULL_PAID;
                    return;
                }

                if (!$order->isAvailableForPayment()) {
                    $this->pay_status = self::PAY_STATUS_ERROR_ORDER_NOT_AVAILABLE_FOR_PAYMENT;
                    return;
                }

                $order->card_payed = $allPayment;

                if (in_array($order->processing_status, [
                    OrderRecord::PROCESSING_CALLCENTER_PREPAY,
                ])) {
                    $order->processing_status = OrderRecord::PROCESSING_CALLCENTER_CONFIRMED;
                }

                if (!$order->save(true, ['card_payed', 'processing_status', 'updated_at'])) {
                    $this->pay_status = self::PAY_STATUS_ERROR_ORDER_SAVE;
                    Yii::log(Translator::t('Ошибка записи оплаты {unique_payment} в заказ №{id} на сумму {amount}.',
                            [
                                '{unique_payment}' => $this->unique_payment,
                                '{id}' => $order->id,
                                '{amount}' => $this->amount,
                            ]
                        ) . ' Errors :' . print_r($order->getErrors(), true),
                        CLogger::LEVEL_ERROR);
                } else {
                    $this->pay_status = self::PAY_STATUS_OK;
                }
            } elseif (
                $this->status == PayGatewayReceiveResult::RECEIVE_STATUS_OK
                && $this->pay_status == self::PAY_STATUS_CANCELED_ORDER_PAY
                && $order->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED
                && $order->card_payed == 0
            ) {
                $order->card_payed = $this->amount;
                $order->updateByPk($order->id, ['card_payed' => $order->card_payed]);
            }
        }
    }

    /* API */
    /**
     * @param bool $withReplacements
     *
     * @return array
     */
    public static function getProviders($withReplacements = false)
    {
        $mas = [
            self::PROVIDER_ESPAY => Translator::t('skip', 'Espay'),
        ];

        return $withReplacements ? $mas : array_keys($mas);
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getProviderText($default = '')
    {
        $providers = self::getProviders(true);
        return isset($providers[$this->provider]) ? $providers[$this->provider] : $default;
    }

    /**
     * @param bool $withReplacements
     *
     * @return array
     */
    public static function getAvailableProvidersToPay($withReplacements = false)
    {
        static $available = [
            self::PROVIDER_ESPAY,
        ];

        return $withReplacements ?
            array_intersect_key(self::getProviders(true), array_combine($available, $available)) : $available;
    }

    /**
     * @return array
     */
    public static function getLockedPayStatuses()
    {
        return [
            self::PAY_STATUS_NOT_PROCESSED,
            self::PAY_STATUS_OK,
            self::PAY_STATUS_ERROR_PAY_OVER_COST,
            self::PAY_STATUS_ERROR_ORDER_SAVE,
            self::PAY_STATUS_ERROR_ORDER_NOT_AVAILABLE_FOR_PAYMENT,
            self::PAY_STATUS_CREATED_RETURN,
            self::PAY_STATUS_CANCELED_ORDER_PAY,
            self::PAY_STATUS_WRONG,
            self::PAY_STATUS_ERROR_ORDER_FULL_PAID,
        ];
    }

    /**
     * @return array
     */
    public static function getPayStatusesReplacement()
    {
        return [
            self::PAY_STATUS_NOT_PROCESSED => Translator::t('оплата не была добавлена в заказ'),
            self::PAY_STATUS_OK => Translator::t('оплата добавлена в заказ'),
            self::PAY_STATUS_ERROR_PAY_OVER_COST => Translator::t('сумма оплаты превышает сумму заказа'),
            self::PAY_STATUS_ERROR_ORDER_SAVE => Translator::t('произошла ошибка при добавлении оплаты в заказ'),
            self::PAY_STATUS_ERROR_ORDER_NOT_AVAILABLE_FOR_PAYMENT => Translator::t('заказ недоступен для оплаты'),
            self::PAY_STATUS_CREATED_RETURN => Translator::t('создан возврат средств'),
            self::PAY_STATUS_CANCELED_ORDER_PAY => Translator::t('отмена оплаты при получении заказа'),
            self::PAY_STATUS_WRONG => Translator::t('ошибочная оплата'),
            self::PAY_STATUS_ERROR_ORDER_FULL_PAID => Translator::t('заказ полностью оплачен'),
        ];
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getPayStatusText($default = '')
    {
        $statuses = self::getPayStatusesReplacement();

        return isset($statuses[$this->pay_status]) ? $statuses[$this->pay_status] : $default;
    }

    /**
     * @return bool
     */
    public function isErrorPayStatus()
    {
        return in_array($this->pay_status, self::getErrorPayStatuses());
    }

    /**
     * @return array
     */
    public static function getErrorPayStatuses()
    {
        static $statusesError = [
            self::PAY_STATUS_ERROR_ORDER_SAVE,
            self::PAY_STATUS_ERROR_PAY_OVER_COST,
            self::PAY_STATUS_ERROR_ORDER_NOT_AVAILABLE_FOR_PAYMENT,
            self::PAY_STATUS_ERROR_ORDER_FULL_PAID,
        ];

        return $statusesError;
    }

    /**
     * @return bool
     */
    public function isNeedReturnPayment()
    {
        return $this->status == PayGatewayReceiveResult::RECEIVE_STATUS_OK
            && $this->order_bank_refund_id == 0 && $this->isErrorPayStatus();
    }
}
