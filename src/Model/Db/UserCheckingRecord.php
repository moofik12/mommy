<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserCheckingRecord
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $is_send_we_worry
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserCheckingRecord extends DoctrineActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users_checking';
    }

    /**
     * @param string $className
     *
     * @return UserCheckingRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_send_we_worry', 'isSendWeWorry', 'setIsSendWeWorry', 'isSendWeWorry'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['is_send_we_worry, user_id', 'numerical', 'integerOnly' => true],
            ['id, user_id, is_send_we_worry, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'user_id' => Translator::t('User'),
            'is_send_we_worry' => Translator::t('Sent "We worry"'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;
        $criteria->compare($alias . 'id', $this->id, true);
        $criteria->compare($alias . 'user_id', $this->user_id, true);
        $criteria->compare($alias . 'is_send_we_worry', $this->is_send_we_worry);
        $criteria->compare($alias . 'created_at', $this->created_at);
        $criteria->compare($alias . 'updated_at', $this->updated_at);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function isSentWeWorry($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_send_we_worry' => $value,
        ]);
        return $this;
    }
}
