<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Purifier\StaticPagePurifier;

/**
 * Class StaticPageRecord
 *
 * @property-read integer $id
 * @property string $url
 * @property bool $is_visible
 * @property string $title
 * @property string $body
 * @property integer $category_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read StaticPageCategoryRecord $category
 */
class StaticPageRecord extends DoctrineActiveRecord
{
    //для поиска в grid
    public $searchDateStart;
    public $searchDateEnd;
    //для хранения даты выбора при перезагрузке
    public $searchDateRangeInput;
    protected $staticPageCategoryRecord;

    /**
     * @param string $className
     *
     * @return StaticPageRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_visible', 'isVisible', 'setIsVisible', 'isVisible'];
        yield ['url', 'getUrl', 'setUrl', 'url'];
        yield ['title', 'getTitle', 'setTitle', 'title'];
        yield ['body', 'getBody', 'setBody', 'body'];
        yield ['position', 'getPosition', 'setPosition', 'position'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['category_id', 'getCategoryId', 'setCategoryId', 'categoryId'];
    }

    public function tableName()
    {
        return 'staticpages';
    }

    public function relations()
    {
        return [
            'category' => [self::BELONGS_TO, 'MommyCom\Model\Db\StaticPageCategoryRecord', 'category_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['url, title, body', 'required'],
            ['is_visible', 'boolean'],
            ['url', 'length', 'min' => 3, 'max' => 45],
            ['url', 'match', 'pattern' => '/^([a-z0-9-]+)$/', 'message' => \Yii::t('common', 'Field must contain only letters, digits and  "-"')],

            ['title', 'length', 'min' => 3, 'max' => 120],

            ['body', 'length', 'min' => 12],

            ['category_id', 'exist', 'className' => 'MommyCom\Model\Db\StaticPageCategoryRecord', 'attributeName' => 'id', 'allowEmpty' => false],

            ['title, body, url, category_id, searchDateStart, searchDateEnd, searchDateRangeInput',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'is_visible' => Translator::t('Show'),
            'category_id' => Translator::t('Category'),
            'url' => Translator::t('URL (page title)'),
            'title' => Translator::t('Title'),
            'body' => Translator::t('Text'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'searchDateRangeInput' => Translator::t('Period'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $purifier = new StaticPagePurifier([
            'Attr.EnableID' => true,
        ]);

        $this->body = $purifier->purify($this->body);

        return true;
    }

    public function afterSave()
    {
        //обновление поля `position` для сортировки
        $this->updateByPk($this->id, ['position' => $this->id]);

        parent::afterSave();
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        /* @var static $model */
        $model = $provider->model;
        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.url', $model->url, true);
        $criteria->compare($alias . '.url', $model->is_visible, true);
        $criteria->compare($alias . '.body', $model->body, true);
        $criteria->compare($alias . '.title', $model->title, true);
        $criteria->compare($alias . '.category_id', $model->category_id);

        //для поиска в grid
        $criteria->compare($alias . '.created_at', '>=' . $model->searchDateStart);
        $criteria->compare($alias . '.created_at', '<=' . $model->searchDateEnd);

        return $provider;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function categoryId($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.category_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function visibleIs($value)
    {
        $value = intval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.is_visible' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function url($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.url' => $value,
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function position()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->order = "{$alias}.position ASC";
        return $this;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function getFullPageLink(string $url): string
    {
        $staticPage = $this->url($url)->find();

        if (!$staticPage) {
            return '#';
        }

        if (empty($this->staticPageCategoryRecord)) {
            $this->staticPageCategoryRecord = new StaticPageCategoryRecord();
        }

        $categoryUrl = $this->staticPageCategoryRecord->findByPk($staticPage->category_id)->url;

        return DIRECTORY_SEPARATOR . 'static/' . $categoryUrl . DIRECTORY_SEPARATOR . $staticPage->url;
    }
}
