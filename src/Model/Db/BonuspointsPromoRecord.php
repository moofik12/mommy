<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * @property-read int $id
 * @property string $name
 * @property string $hash
 * @property int $points кол-во бонусов
 * @property int $bonus_available_at timestamp период действия бонуса
 * @property string $log_message
 * @property string $user_message
 * @property bool $email_confirm
 * @property bool $is_stopped
 * @property int $start_at
 * @property int $end_at
 * @property-read int $updated_at
 * @property-read int $created_at
 * @property-read string $ulrForUnisender
 * @property string $startAsString
 * @property string $endAsString
 * @property int $bonusAvailableDays
 * @mixin Timestampable
 */
class BonuspointsPromoRecord extends DoctrineActiveRecord
{
    const AVAILABLE_DELETE_PERIOD_AFTER_CREATE = 10800; //3 hours

    /**
     * @param string $className
     *
     * @return BonuspointsPromoRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['hash', 'getHash', 'setHash', 'hash'];
        yield ['points', 'getPoints', 'setPoints', 'points'];
        yield ['bonus_available_at', 'getBonusAvailableAt', 'setBonusAvailableAt', 'bonusAvailableAt'];
        yield ['is_stopped', 'isStopped', 'setIsStopped', 'isStopped'];
        yield ['log_message', 'getLogMessage', 'setLogMessage', 'logMessage'];
        yield ['user_message', 'getUserMessage', 'setUserMessage', 'userMessage'];
        yield ['email_confirm', 'isEmailConfirm', 'setEmailConfirm', 'emailConfirm'];
        yield ['start_at', 'getStartAt', 'setStartAt', 'startAt'];
        yield ['end_at', 'getEndAt', 'setEndAt', 'endAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
    }

    public function tableName()
    {
        return 'bonuspoints_promo';
    }

    public function relations()
    {
        return [];
    }

    public function rules()
    {
        return [
            ['name, log_message, user_message', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name, log_message, user_message, start_at, end_at', 'required'],

            ['name', 'length', 'max' => 64],
            ['name', 'unique'],
            ['log_message', 'length', 'max' => 255, 'encoding' => false],
            ['user_message', 'length', 'max' => 255, 'encoding' => false],

            ['email_confirm, is_stopped', 'boolean'],
            ['start_at, end_at', 'numerical', 'integerOnly' => true],
            ['points', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 200],

            ['bonus_available_at', 'numerical', 'integerOnly' => true, 'min' => 0],
            ['start_at', 'validatorActivityPeriod'],

            //custom property
            ['startAsString, endAsString', 'length'],
            ['bonusAvailableDays', 'numerical', 'min' => 0],

            ['id, name, points, log_message, user_message, start_at, end_at, email_confirm, is_stopped', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Translator::t('Capmaign ID'),
            'log_message' => Translator::t('System message'),
            'user_message' => Translator::t('Message to user'),
            'email_confirm' => Translator::t('Confirm e-mail'),
            'is_stopped' => Translator::t('Operation aborted'),
            'start_at' => Translator::t('Started'),
            'startAsString' => Translator::t('Started'),
            'end_at' => Translator::t('Finished'),
            'endAsString' => Translator::t('Finished'),
            'points' => Translator::t('Bonuses qty'),
            'bonus_available_at' => Translator::t('Bonus end date'),
            'bonusAvailableDays' => Translator::t('Bonus validity period'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'ulrForUnisender' => Translator::t('Link to Unisender mail-out'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.name', $model->name, true);
        $criteria->compare($alias . '.points', $model->points);
        $criteria->compare($alias . '.log_message', $model->log_message, true);
        $criteria->compare($alias . '.user_message', $model->user_message, true);
        $criteria->compare($alias . '.email_confirm', $model->email_confirm, true);
        $criteria->compare($alias . '.is_stopped', $model->is_stopped, true);

        return $provider;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatorActivityPeriod($attribute, $params)
    {
        if ($this->start_at >= $this->end_at) {
            $this->addError($attribute, Translator::t('Flash-sale start time is later (or the same) as its end time.'));
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->log_message)) {
            $this->log_message = $this->user_message;
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        $this->hash = $this->generateHash($this->name);

        if (empty($this->log_message)) {
            $this->log_message = $this->user_message;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->created_at < time() - self::AVAILABLE_DELETE_PERIOD_AFTER_CREATE) {
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function generateHash($name = '')
    {
        static $salt = 'oC*}B89($3>m';

        if (empty($name)) {
            $name = $this->name;
        }

        $name = Utf8::trim(Cast::toStr($name));
        return md5($salt . '_' . $name);
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function hash($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'hash' => $value,
        ]);
        return $this;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function name($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'name' => $value,
        ]);
        return $this;
    }

    /** API */

    /**
     * @return bool
     */
    public function isEnable()
    {
        $time = time();

        return !$this->is_stopped && $this->start_at < $time && $this->end_at > $time;
    }

    /**
     * @return string
     */
    public function getUniqueLogMessage()
    {
        return $this->log_message . " hash:{$this->hash}";
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function startAtFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.start_at>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function startAtTo($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.start_at<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function endAtFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.end_at>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function endAtTo($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.end_at<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @return string
     */
    public function getUlrForUnisender()
    {
        $frontendUrlManager = \Yii::app()->frontendUrlManager;

        return '{{hash}}' . urlencode(urlencode($frontendUrlManager->createAbsoluteUrl('promoBonus/activation', ['hash' => $this->hash, 'utm_source' => 'newsletter', 'utm_medium' => 'email', 'utm_campaign' => 'mamam'])));
    }

    /**
     * @link http://php.net/manual/en/function.date.php supports format
     *
     * @param string $format
     *
     * @return bool|string
     */
    public function getStartAsString($format = 'd.m.Y')
    {
        return date($format, $this->start_at);
    }

    /**
     * @link http://php.net/manual/en/function.strtotime.php supports format
     *
     * @param string $timeString
     */
    public function setStartAsString($timeString)
    {
        $time = strtotime($timeString);

        if ($time > 0) {
            $this->start_at = $time;
        }
    }

    /**
     * @link http://php.net/manual/en/function.date.php supports format
     *
     * @param string $format
     *
     * @return bool|string
     */
    public function getEndAsString($format = 'd.m.Y')
    {
        return date($format, $this->end_at);
    }

    /**
     * @link http://php.net/manual/en/function.strtotime.php supports format
     *
     * @param string $timeString
     */
    public function setEndAsString($timeString)
    {
        $time = strtotime($timeString);

        if ($time > 0) {
            $this->end_at = $time;
        }
    }

    /**
     * @param int $days
     */
    public function setBonusAvailableDays($days)
    {
        $days = Cast::toUInt($days);

        $this->bonus_available_at = $days * 86400;
    }

    /**
     * @return integer
     */
    public function getBonusAvailableDays()
    {
        return (int)$this->bonus_available_at / 86400;
    }
}
