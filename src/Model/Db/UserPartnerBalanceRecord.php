<?php

namespace MommyCom\Model\Db;

use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class UserPartnerBalanceRecord
 *
 * @property-description Потенциальный баланс
 * @property-read integer $id
 * @property integer $partner_id
 * @property integer $type
 * @property integer $amount
 * @property string $description
 * @property integer $order_id
 * @property string $created_at
 * @property string $updated_at
 * @property-read $typeReplacement string
 * @property-read $partner UserPartnerRecord
 * @property-read $order OrderRecord
 */
class UserPartnerBalanceRecord extends DoctrineActiveRecord
{
    const TYPE_ORDER_PLUS = 0; //зачисление с заказа
    const TYPE_MAMAM_MINUS = 1; //выплата на счет мамам
    const TYPE_PRIVAT_MINUS = 2; //выплата на Приват
    const TYPE_CANCEL_PLUS = 3; //зачисление при отклонении заявки
    const TYPE_RETURN_MINUS = 4; //возврат по заказу

    /**
     * @return string
     */
    public function tableName()
    {
        return 'users_partner_balance';
    }

    /**
     * @param string $className
     *
     * @return UserPartnerBalanceRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['type', 'getType', 'setType', 'type'];
        yield ['amount', 'getAmount', 'setAmount', 'amount'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['partner_id', 'getPartnerId', 'setPartnerId', 'partnerId'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['partner_id, amount', 'required'],

            ['type', 'in', 'range' => array_keys(self::typeReplacements())],
            ['type', 'numerical', 'integerOnly' => true],
            ['amount', 'numerical', 'integerOnly' => true],

            ['description', 'length', 'max' => 150],
            ['id, partner_id, type, amount, description, order_id, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function relations()
    {
        return [
            'partner' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserPartnerRecord', 'partner_id'],
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('Account record No.'),
            'partner_id' => Translator::t('Partner'),
            'type' => Translator::t('Type'),
            'amount' => Translator::t('Amount'),
            'description' => Translator::t('Description'),
            'order_id' => Translator::t('Order ID'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),

            'typeReplacement' => Translator::t('Type'),
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @inheritdoc
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();
        /** @var self $model */
        $model = $provider->model;

        $criteria->compare($alias . '.id', $model->id);
        $criteria->compare($alias . '.partner_id', $model->partner_id);
        $criteria->compare($alias . '.type', $model->type);
        $criteria->compare($alias . '.amount', $model->amount);
        $criteria->compare($alias . '.description', $model->description);
        $criteria->compare($alias . '.order_id', $model->order_id);

        return $provider;
    }

    /**
     * @return array
     */
    public static function typeReplacements()
    {
        return [
            self::TYPE_ORDER_PLUS => Translator::t('Crediting from order'),
            self::TYPE_MAMAM_MINUS => Translator::t('Withdrawal to MOMMY account'),
            self::TYPE_PRIVAT_MINUS => Translator::t('Withdrawal to PRIVAT account'),
            self::TYPE_CANCEL_PLUS => Translator::t('Refund in case of cancellation'),
            self::TYPE_RETURN_MINUS => Translator::t('Refund on order'),
        ];
    }

    /**
     * @return string
     */
    public function getTypeReplacement()
    {
        $replacements = self::typeReplacements();
        return isset($replacements[$this->type]) ? $replacements[$this->type] : '';
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function partnerId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.partner_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function type($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function orderId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param $values
     * @param int $max
     *
     * @return $this
     */
    public function typeIn($values, $max = -1)
    {
        if ($max > 0) {
            $values = array_slice($values, 0, $max);
        }
        $values = Cast::toUIntArr($values);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.type', $values);
        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', "<= $value");

        return $this;
    }

    /**
     * @param int $value timestamp
     *
     * @return $this
     */
    public function createdAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->compare($alias . '.created_at', ">= $value");

        return $this;
    }
}
