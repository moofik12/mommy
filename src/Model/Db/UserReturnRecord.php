<?php

namespace MommyCom\Model\Db;

use MommyCom\Entity\User;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class UserReturnRecord
 * Пользователь который был каким-либо свобом возвращен.
 * Например, при переходе по рекламе на наш сайт пытался повторно зарегистрироваться
 *
 * @property-read integer $id
 * @property string $email
 * @property integer $returned_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read UserRecord $user
 */
class UserReturnRecord extends DoctrineActiveRecord
{
    /**
     * @param string $className
     *
     * @return UserReturnRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['landing_num', 'getLandingNum', 'setLandingNum', 'landingNum'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['surname', 'getSurname', 'setSurname', 'surname'];
        yield ['email', 'getEmail', 'setEmail', 'email'];
        yield ['telephone', 'getTelephone', 'setTelephone', 'telephone'];
        yield ['password_hash', 'getPasswordHash', 'setPasswordHash', 'passwordHash'];
        yield ['password_salt', 'getPasswordSalt', 'setPasswordSalt', 'passwordSalt'];
        yield ['address', 'getAddress', 'setAddress', 'address'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['psychotype', 'getPsychotype', 'setPsychotype', 'psychotype'];
        yield ['discount_percent', 'getDiscountPercent', 'setDiscountPercent', 'discountPercent'];
        yield ['is_email_verified', 'isEmailVerified', 'setIsEmailVerified', 'isEmailVerified'];
        yield ['present_promocode', 'getPresentPromocode', 'setPresentPromocode', 'presentPromocode'];
        yield ['offer_provider', 'getOfferProvider', 'setOfferProvider', 'offerProvider'];
        yield ['offer_id', 'getOfferId', 'setOfferId', 'offerId'];
        yield ['offer_target_url', 'getOfferTargetUrl', 'setOfferTargetUrl', 'offerTargetUrl'];
        yield ['offer_success', 'isOfferSuccess', 'setOfferSuccess', 'offerSuccess'];
        yield ['moved_search_engine', 'getMovedSearchEngine', 'setMovedSearchEngine', 'movedSearchEngine'];
        yield ['password_recovery_requested_at', 'getPasswordRecoveryRequestedAt', 'setPasswordRecoveryRequestedAt', 'passwordRecoveryRequestedAt'];
        yield ['email_unsubscribe', 'getEmailUnsubscribe', 'setEmailUnsubscribe', 'emailUnsubscribe'];
        yield ['is_system_subscribe', 'isSystemSubscribe', 'setIsSystemSubscribe', 'isSystemSubscribe'];
        yield ['is_subscribe', 'isSubscribe', 'setIsSubscribe', 'isSubscribe'];
        yield ['is_subscribed', 'isSubscribed', 'setIsSubscribed', 'isSubscribed'];
        yield ['is_service_unsubscribed', 'isServiceUnsubscribed', 'setIsServiceUnsubscribed', 'isServiceUnsubscribed'];
        yield ['is_service_possibly_subscribe', 'isServicePossiblySubscribe', 'setIsServicePossiblySubscribe', 'isServicePossiblySubscribe'];
        yield ['ip', 'getIp', 'setIp', 'ip'];
        yield ['last_order_at', 'getLastOrderAt', 'setLastOrderAt', 'lastOrderAt'];
        yield ['interests_generated_at', 'getInterestsGeneratedAt', 'setInterestsGeneratedAt', 'interestsGeneratedAt'];
        yield ['interests_amount', 'getInterestsAmount', 'setInterestsAmount', 'interestsAmount'];
        yield ['last_interests_mailed_at', 'getLastInterestsMailedAt', 'setLastInterestsMailedAt', 'lastInterestsMailedAt'];
        yield ['returned_at', 'getReturnedAt', 'setReturnedAt', 'returnedAt'];
        yield ['last_visit_at', 'getLastVisitAt', 'setLastVisitAt', 'lastVisitAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['invited_by', 'getInvitedBy', 'setInvitedBy', 'invitedBy'];
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntityClassName(): string
    {
        return User::class;
    }

    public function tableName()
    {
        return 'users';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\UserRecord', ['email' => 'email']],
        ];
    }

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['email, returned_at', 'required'],
            ['returned_at', 'numerical', 'min' => 1, 'integerOnly' => true],

            ['email', 'email'],
            ['email', 'unique'],

            //grid
            ['id, email, returned_at, created_at, updated_at', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => '#',
            'email' => Translator::t('Email'),
            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function email($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toStr($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.email' => $value,
        ]);
        return $this;
    }

    /**
     * <b>Возвращен ДО</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function returnedAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'returned_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * <b>Возвращен ПОСЛЕ</b> указанного времени
     *
     * @param integer $time
     *
     * @return static
     */
    public function returnedAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'returned_at' . '>=' . Cast::toStr($value));
        return $this;
    }
} 
