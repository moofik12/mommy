<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CJSON;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class AdminRoleRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property string $description
 * @property string $rules
 * @property array $rulesArray
 * @property integer $created_at
 * @property integer $updated_at
 * @property-read AdminRoleAssignmentRecord[] $assignment
 */
class AdminRoleRecord extends DoctrineActiveRecord
{
    protected $_rulesArray = false;

    /**
     * @param string $className
     *
     * @return AdminRoleRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['name', 'getName', 'setName', 'name'];
        yield ['description', 'getDescription', 'setDescription', 'description'];
        yield ['rules', 'getRules', 'setRules', 'rules'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'admins_roles';
    }

    public function relations()
    {
        return [
            'assignment' => [self::HAS_MANY, AdminRoleAssignmentRecord::class, 'role_id'],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name, description', 'required'],
            ['name', 'unique'],
            ['name', 'length', 'min' => 3, 'max' => 100],
            ['description', 'length', 'max' => 255, 'encoding' => false],

            ['rulesArray', 'safe'],
            ['name, description', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Translator::t('Role'),
            'description' => Translator::t('Description'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return $result;
        }

        return $result;
    }

    /**
     * @param $rules
     *
     * @return array
     */
    protected function _normalizationRules($rules)
    {
        $normalRules = [];
        $separator = '.';

        if (is_array($rules)) {
            foreach ($rules as $controller => $value) {
                $buff = [];
                $countAccess = 0;

                if (is_array($value)) {
                    foreach ($value as $action => $access) {
                        if (Cast::toBool($access)) {
                            $countAccess++;
                            $buff[] = $controller . $separator . $action;
                        }
                    }

                    if (count($value) == $countAccess) {
                        $buff = [$controller . $separator . '*'];
                    }
                } elseif (is_numeric($controller) && is_string($value)) {
                    $buff = [$value];
                }

                $normalRules = ArrayUtils::merge($normalRules, $buff);
            }
        }

        return $normalRules;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $criteria = $provider->getCriteria();
        $criteria->compare('name', $provider->model->name, true);
        $criteria->compare('description', $provider->model->description, true);

        return $provider;
    }

    /**
     * @return array|mixed
     */
    public function getRulesArray()
    {
        if ($this->_rulesArray === false) {
            $rules = [];
            if (!empty($this->rules)) {
                $rules = $this->unserializeRules($this->rules);
            }
            $this->_rulesArray = $rules;
        }

        return $this->_rulesArray;
    }

    /***
     * @param array $rules
     */
    public function setRulesArray(array $rules)
    {
        $this->_rulesArray = $this->_normalizationRules($rules);
        $this->rules = $this->serializeRules($this->_rulesArray);
    }

    /**
     * Возвращает серриализарованый массив с правами админа
     *
     * @param $rules $rules
     *
     * @return string
     */
    public static function serializeRules($rules)
    {
        $newRules = [];
        foreach ($rules as $key => $val) {
            $newRules[mb_strtoupper($key)] = mb_strtoupper($val);
        }

        return CJSON::encode($newRules);
    }

    /**
     * @param string $rules
     *
     * @return mixed
     */
    public static function unserializeRules($rules)
    {
        return CJSON::decode($rules);
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function name($value)
    {
        $value = strval($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.name' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public function nameLike($value)
    {
        $alias = $this->getTableAlias();
        $value = Utf8::trim($value);

        $this->getDbCriteria()->addSearchCondition($alias . '.name', $value, true);
        return $this;
    }

    /**
     * @return static[]
     */
    public static function getRolesList()
    {
        return self::model()->cache(3600, time())->findAll();
    }
}
