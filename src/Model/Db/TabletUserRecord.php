<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class TabletUserRecord
 *
 * @property-read integer $id
 * @property integer $user_id
 * @property integer $created_by
 * @property integer $last_activity_at
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read AdminUserRecord $user
 * @property-read AdminUserRecord $createdBy
 */
class TabletUserRecord extends DoctrineActiveRecord
{
    const ACTIVITY_ONLINE_PERIOD = 3600; // one hour

    /**
     * @param string $className
     *
     * @return TabletUserRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['last_activity_at', 'getLastActivityAt', 'setLastActivityAt', 'lastActivityAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['user_id', 'getUserId', 'setUserId', 'userId'];
        yield ['created_by', 'getCreatedBy', 'setCreatedBy', 'createdBy'];
    }

    public function tableName()
    {
        return 'tablet_users';
    }

    public function relations()
    {
        return [
            'user' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'user_id'],
            'createdBy' => [self::BELONGS_TO, 'MommyCom\Model\Db\AdminUserRecord', 'created_by'],
        ];
    }

    public function rules()
    {
        return [
            ['user_id', 'required'],
            ['id, user_id, last_activity_at, created_by', 'numerical', 'integerOnly' => true],

            ['user_id', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],
            ['created_by', 'exist', 'className' => 'MommyCom\Model\Db\AdminUserRecord', 'attributeName' => 'id'],

            ['id, user_id, created_by', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Translator::t('№'),
            'user_id' => Translator::t('User'),
            'created_by' => Translator::t('Created by'),
            'last_activity_at' => Translator::t('Last activity'),

            'created_at' => Translator::t('Created'),
            'updated_at' => Translator::t('Updated'),
        ];
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userIdNot($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.user_id' . ' <> ' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdBy($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.created_by' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function lastActivityAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'last_activity_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function lastActivityAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'last_activity_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @return static
     */
    public function onlyOnline()
    {
        return $this->lastActivityAtGreater(time() - self::ACTIVITY_ONLINE_PERIOD);
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.user_id', $model->user_id, true);
        $criteria->compare($alias . '.created_by', $model->created_by, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }
} 
