<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use CException;
use MommyCom\Entity\OrderTracking;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Service\CurrentTime;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderTrackingRecord
 *
 * @property-read integer $id
 * @property integer $order_id
 * @property integer $order_delivery_type
 * @property float $delivery_price
 * @property string $trackcode
 * @property string $trackcode_return
 * @property integer $status
 * @property integer $status_prev
 * @property string $status_history_json
 * @property integer $status_changed_at
 * @property bool $is_drop_shipping
 * @property integer $supplier_id
 * @property integer $deadline_for_delivery
 * @property integer $delivered_date
 * @property integer $synchronized_at
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read OrderRecord $order
 * @property-read array $statusHistory
 * @property-read string $statusReplacement
 * @property-read string $statusPrevRep
 * @mixin Timestampable
 */
class OrderTrackingRecord extends DoctrineActiveRecord
{
    public const STATUS_UNKNOWN = OrderTracking::STATUS_UNKNOWN;
    public const STATUS_LOST = OrderTracking::STATUS_LOST;
    public const STATUS_DELIVERED = OrderTracking::STATUS_DELIVERED;
    public const STATUS_PROCESSING = OrderTracking::STATUS_PROCESSING;
    public const STATUS_SHOP_WAREHOUSE = OrderTracking::STATUS_SHOP_WAREHOUSE;
    public const STATUS_TRANSITION = OrderTracking::STATUS_TRANSITION;
    public const STATUS_CLIENT_WAREHOUSE = OrderTracking::STATUS_CLIENT_WAREHOUSE;
    public const STATUS_COURIER = OrderTracking::STATUS_COURIER;
    public const STATUS_RETURNING = OrderTracking::STATUS_RETURNING;
    public const STATUS_RETURNING_WAREHOUSE = OrderTracking::STATUS_RETURNING_WAREHOUSE;
    public const STATUS_RETURNED = OrderTracking::STATUS_RETURNED;
    public const STATUS_INIT = OrderTracking::STATUS_INIT;
    public const STATUS_FORWARDING = OrderTracking::STATUS_FORWARDING;
    public const STATUS_CLIENT_REFUSAL = OrderTracking::STATUS_CLIENT_REFUSAL;
    public const STATUS_CLIENT_WAREHOUSE_AND_PENALTY = OrderTracking::STATUS_CLIENT_WAREHOUSE_AND_PENALTY;
    public const STATUS_CLIENT_WAREHOUSE_AND_LOST = OrderTracking::STATUS_CLIENT_WAREHOUSE_AND_LOST;
    public const STATUS_RETURNING_LOST = OrderTracking::STATUS_RETURNING_LOST;

    /**
     * @param string $className
     *
     * @return OrderTrackingRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['is_drop_shipping', 'isDropShipping', 'setIsDropShipping', 'isDropShipping'];
        yield ['order_delivery_type', 'getOrderDeliveryType', 'setOrderDeliveryType', 'orderDeliveryType'];
        yield ['delivery_price', 'getDeliveryPrice', 'setDeliveryPrice', 'deliveryPrice'];
        yield ['trackcode', 'getTrackcode', 'setTrackcode', 'trackcode'];
        yield ['trackcode_return', 'getTrackcodeReturn', 'setTrackcodeReturn', 'trackcodeReturn'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['status_history_json', 'getStatusHistoryJson', 'setStatusHistoryJson', 'statusHistoryJson'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['deadline_for_delivery', 'getDeadlineForDelivery', 'setDeadlineForDelivery', 'deadlineForDelivery'];
        yield ['delivered_date', 'getDeliveredDate', 'setDeliveredDate', 'deliveredDate'];
        yield ['synchronized_at', 'getSynchronizedAt', 'setSynchronizedAt', 'synchronizedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
        yield ['supplier_id', 'getSupplierId', 'setSupplierId', 'supplierId'];
    }

    public function tableName()
    {
        return 'orders_tracking';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['order_id', 'required'],
            ['order_id', 'unique'],
            ['order_id', 'numerical', 'integerOnly' => true, 'min' => 1],
            ['delivery_price', 'numerical'],

            ['status, status_prev', 'in', 'range' => [
                self::STATUS_UNKNOWN,
                self::STATUS_LOST,
                self::STATUS_PROCESSING,
                self::STATUS_DELIVERED,
                self::STATUS_SHOP_WAREHOUSE,
                self::STATUS_TRANSITION,
                self::STATUS_CLIENT_WAREHOUSE,
                self::STATUS_COURIER,
                self::STATUS_RETURNING,
                self::STATUS_RETURNING_WAREHOUSE,
                self::STATUS_RETURNED,
                self::STATUS_INIT,
                self::STATUS_FORWARDING,
                self::STATUS_CLIENT_REFUSAL,
                self::STATUS_CLIENT_WAREHOUSE_AND_PENALTY,
                self::STATUS_CLIENT_WAREHOUSE_AND_LOST,
                self::STATUS_RETURNING_LOST,
            ]],

            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['is_drop_shipping', 'boolean'],

            ['supplier_id', 'filter', 'filter' => function ($value) {
                return $value == 0 && !$this->is_drop_shipping ? '' : $value;
            }], // хак чтобы CExistValidator считал значение "0" пустым
            ['supplier_id', 'exist', 'className' => 'MommyCom\Model\Db\SupplierRecord', 'attributeName' => 'id'],

            [
                'id, order_id, order_delivery_type, trackcode, trackcode_return, status, status_prev, status_changed_at, created_at, updated_at',
                'safe',
                'on' => 'searchPrivileged',
            ],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $tr = Translator::get();

        return [
            'id' => $tr('№'),
            'order_id' => $tr('Order'),
            'order_delivery_type' => $tr('Delivery type'),
            'delivery_price' => $tr('Shipping сost (real)'),
            'trackcode' => $tr('Tracking code / Consignment note'),
            'trackcode_return' => $tr('Return tracking code / Consignment note'),
            'status' => $tr('Status'),
            'status_history_json' => $tr('Status history'),
            'status_changed_at' => $tr('Статус изменен'),
            'is_drop_shipping' => $tr('Dropshipping flash-sale'),
            'supplier_id' => $tr('Supplier'),

            'deadline_for_delivery' => $tr('Deadline for delivery'),
            'delivered_date' => $tr('Delivered date'),
            'synchronized_at' => $tr('Updated'),
            'created_at' => $tr('Sent'),
            'updated_at' => $tr('Updated'),
        ];
    }

    /**
     * Обновляет историю смены статусов
     */
    protected function _updateStatusChangeStack()
    {
        /** @var OrderTracking $orderTracking */
        $orderTracking = $this->getEntity();

        $statusHistory = $orderTracking->getStatusHistory();
        $prevStatus = end($statusHistory);
        $currentStatus = $orderTracking->getStatus();

        if (false !== $prevStatus && $prevStatus['status'] !== $currentStatus) {
            return;
        }

        $adminId = $userId = 0;

        try { // for console app
            $webUser = \Yii::app()->user;
            $user = $webUser->getModel();
            if ($user instanceof AdminUserRecord) {
                $adminId = $webUser->id;
            }

            if ($user instanceof UserRecord) {
                $userId = $webUser->id;
            }
        } catch (CException $e) {
            // можно забить
        }

        $orderTracking->addStatusToHistory($currentStatus, $adminId, $userId);
        $orderTracking->setStatusPrev((int)$prevStatus['status']);
        $orderTracking->setStatusChangedAt(CurrentTime::getUnixTimestamp());
    }

    protected function _resolveOrderInfo()
    {
        $order = $this->order;

        if ($order === null) {
            return;
        }

        $this->order_delivery_type = $order->delivery_type;
        $this->trackcode = $order->trackcode;
    }

    public function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }

        $this->_resolveOrderInfo();
        $this->_updateStatusChangeStack();

        return true;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'order_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderDeliveryType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'order_delivery_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function trackcode($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'trackcode' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function trackcodeReturn($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'trackcode_return' => $value,
        ]);
        return $this;
    }

    /**
     * @return static
     */
    public function onlyHasTrackcode()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'trackcode' . '<>' . '""');
        return $this;
    }

    /**
     * @return static
     */
    public function onlyHasTrackcodeReturn()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'trackcode_return' . '<>' . '""');
        return $this;
    }

    /**
     * @return static
     */
    public function onlyNotHasTrackcode()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'trackcode' . '=' . '""');
        return $this;
    }

    /**
     * @return static
     */
    public function onlyNotHasTrackcodeReturn()
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'trackcode_return' . '=' . '""');
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toStr($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.' . 'status' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusIn(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusInNot(array $values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function statusChangedAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'status_changed_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function statusChangedAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'status_changed_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function synchronizedAtLower($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'synchronized_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param $value
     *
     * @return static
     */
    public function synchronizedAtGreater($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'synchronized_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $aliasPrefix = '__buyout_UserId_';
        $ordersAlias = $aliasPrefix . 'order';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($ordersAlias, $relations['order']);

        // it's magic ;)
        $this->with([
            $ordersAlias => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => ['userId' => $value],
            ],
        ]);
        return $this;
    }

    /**
     * @param integer $values
     *
     * @return static
     */
    public function userIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $aliasPrefix = '__buyout_UserId_';
        $ordersAlias = $aliasPrefix . 'order';

        $relations = $this->relations();
        $metadata = $this->metaData;
        $metadata->addRelation($ordersAlias, $relations['order']);

        // it's magic ;)
        $this->with([
            $ordersAlias => [
                'together' => true,
                'joinType' => 'INNER JOIN',
                'scopes' => ['userIdIn' => [$values]],
            ],
        ]);
        return $this;
    }

    /**
     * @param bool $value
     *
     * @return static
     */
    public function isDropShipping($value)
    {
        $value = Cast::toBool($value);
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addColumnCondition([$alias . '.is_drop_shipping' => Cast::toStr($value)]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function supplierIdIs($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([$alias . '.supplier_id' => $value]);
        return $this;
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);

        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.order_delivery_type', $model->order_delivery_type);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.status_prev', $model->status_prev);
        $criteria->compare($alias . '.trackcode', $model->trackcode);
        $criteria->compare($alias . '.trackcode_return', $model->trackcode_return);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }


    /// item api

    /**
     * @return array
     */
    public function getStatusHistory()
    {
        return Cast::toArr(json_decode($this->status_history_json, true, 3));
    }

    /**
     * @return string
     */
    public function getOrderDeliveryTypeReplacement()
    {
        $replacements = OrderRecord::deliveryTypeReplacements();
        return isset($replacements[$this->order_delivery_type]) ? $replacements[$this->order_delivery_type] : '';
    }

    /**
     * @return string
     */
    public function getStatusReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : '';
    }

    /**
     * @return string
     */
    public function getStatusPrevReplacement()
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status_prev]) ? $replacements[$this->status_prev] : '';
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        $tr = Translator::get();

        return [
            self::STATUS_UNKNOWN => $tr('information unknown'),
            self::STATUS_LOST => $tr('lost'),
            self::STATUS_DELIVERED => $tr('delivered'),
            self::STATUS_PROCESSING => $tr('reserved, but not sent'),
            self::STATUS_SHOP_WAREHOUSE => $tr('on the mail warehouse for sending'),
            self::STATUS_TRANSITION => $tr(' in transit '),
            self::STATUS_CLIENT_WAREHOUSE => $tr('at the client\'s post office'),
            self::STATUS_COURIER => $tr('at the courier'),
            self::STATUS_RETURNING => $tr('is returning'),
            self::STATUS_RETURNING_WAREHOUSE => $tr('returned to the warehouse'),
            self::STATUS_RETURNED => $tr('returned and was received'),
            self::STATUS_INIT => $tr('created'),
            self::STATUS_FORWARDING => $tr('forwarding'),
            self::STATUS_CLIENT_REFUSAL => $tr('refusal of parcel'),
            self::STATUS_CLIENT_WAREHOUSE_AND_PENALTY => $tr('Is in the warehouse and charged penalties'),
            self::STATUS_CLIENT_WAREHOUSE_AND_LOST => $tr('Is in the warehouse and storage is stopped'),
            self::STATUS_RETURNING_LOST => $tr('lost on return'),
        ];
    }

    /**
     * @return bool
     */
    public function canChangeTrackcode()
    {
        static $availableStatuses = [
            self::STATUS_UNKNOWN,
            self::STATUS_PROCESSING,
            self::STATUS_INIT,
            self::STATUS_LOST,
        ];

        return in_array($this->status, $availableStatuses);
    }
}
