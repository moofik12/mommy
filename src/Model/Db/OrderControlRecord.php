<?php

namespace MommyCom\Model\Db;

use CActiveDataProvider;
use DateTime;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\YiiComponent\Database\DoctrineActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class OrderControlRecord
 *
 * @property-read int $id
 * @property int $order_id
 * @property int $status
 * @property int $status_prev
 * @property int $operator_status
 * @property int $operator_status_prev
 * @property string $comment
 * @property string $system_comment
 * @property $order_mailed_at
 * @property-read $status_changed_at
 * @property-read $operator_status_changed_at
 * @property-read $updated_at
 * @property-read $created_at
 * @property-read OrderRecord $order
 */
class OrderControlRecord extends DoctrineActiveRecord
{
    /** время на доставку и оплату, значения см. http://nl3.php.net/manual/ru/datetime.formats.php */
    const ACTUAL_MAILED_ORDER = '16 days';

    /** время в течении которого оператор может изменить статус */
    const OPERATOR_CHANGE_STATUS = '1 day';

    const STATUS_NEW = 0,
        STATUS_NOT_DELIVERED = 1,
        STATUS_DELIVERED = 2,
        STATUS_NOT_PAYED = 3,
        STATUS_PAYED = 4,
        STATUS_RETURNED = 5,  //подтвержден складом
        STATUS_LOST = 6,
        STATUS_RETURNED_PART = 7;

    const OPERATOR_STATUS_NEW = 0,
        OPERATOR_STATUS_FOUND = 1,
        OPERATOR_STATUS_NOT_FOUND = 2;

    /**
     * @param string $className
     *
     * @return OrderControlRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    protected function entityAttributes(): iterable
    {
        yield ['id', 'getId', false, 'id'];
        yield ['status', 'getStatus', 'setStatus', 'status'];
        yield ['status_prev', 'getStatusPrev', 'setStatusPrev', 'statusPrev'];
        yield ['operator_status', 'getOperatorStatus', 'setOperatorStatus', 'operatorStatus'];
        yield ['operator_status_prev', 'getOperatorStatusPrev', 'setOperatorStatusPrev', 'operatorStatusPrev'];
        yield ['comment', 'getComment', 'setComment', 'comment'];
        yield ['system_comment', 'getSystemComment', 'setSystemComment', 'systemComment'];
        yield ['order_mailed_at', 'getOrderMailedAt', 'setOrderMailedAt', 'orderMailedAt'];
        yield ['status_changed_at', 'getStatusChangedAt', 'setStatusChangedAt', 'statusChangedAt'];
        yield ['operator_status_changed_at', 'getOperatorStatusChangedAt', 'setOperatorStatusChangedAt', 'operatorStatusChangedAt'];
        yield ['updated_at', 'getUpdatedAt', 'setUpdatedAt', 'updatedAt'];
        yield ['created_at', 'getCreatedAt', 'setCreatedAt', 'createdAt'];
        yield ['order_id', 'getOrderId', 'setOrderId', 'orderId'];
    }

    public function tableName()
    {
        return 'orders_control';
    }

    public function relations()
    {
        return [
            'order' => [self::BELONGS_TO, 'MommyCom\Model\Db\OrderRecord', 'order_id'],
        ];
    }

    public function rules()
    {
        return [
            ['status, status_prev', 'default', 'value' => self::STATUS_NEW],
            ['operator_status, operator_status_prev', 'default', 'value' => self::OPERATOR_STATUS_NEW],

            ['order_id, order_mailed_at', 'required'],
            ['order_id', 'unique'],
            ['order_mailed_at', 'numerical', 'min' => 0],
            ['status', 'in', 'range' => array_keys(self::statusReplacements())],
            ['operator_status', 'in', 'range' => array_keys(self::operatorStatusReplacements())],

            ['comment, system_comment', 'length', 'max' => 255, 'encoding' => false],
            ['system_comment', 'length'],
            ['order_id', 'unique'],

            ['id, order_id, status, operator_status, comment', 'safe', 'on' => 'searchPrivileged'],
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    public function attributeLabels()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            'id' => \Yii::t($category, '№'),
            'order_id' => \Yii::t($category, 'Order'),
            'status' => \Yii::t($category, 'Status'),
            'status_prev' => \Yii::t($category, 'Previous status'),
            'operator_status' => \Yii::t($category, 'condition'),
            'operator_status_prev' => \Yii::t($category, 'Previous state'),
            'comment' => \Yii::t($category, 'Comment'),
            'system_comment' => \Yii::t($category, 'System comment'),
            'order_mailed_at' => \Yii::t($category, 'Sending date'),

            'created_at' => \Yii::t($category, 'Created'),
            'updated_at' => \Yii::t($category, 'Updated on'),
        ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model self */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'));

        $criteria->compare($alias . '.order_id', $model->order_id);
        $criteria->compare($alias . '.status', $model->status);
        $criteria->compare($alias . '.operator_status', $model->operator_status);
        $criteria->compare($alias . '.comment', $model->comment, true);
        $criteria->compare($alias . '.system_comment', $model->system_comment, true);

        $criteria->compare($alias . '.created_at', '>=' . $model->created_at);
        $criteria->compare($alias . '.updated_at', '<=' . $model->updated_at);

        return $provider;
    }

    public static function statusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::STATUS_NEW => \Yii::t($category, 'Not processed'),
            self::STATUS_NOT_DELIVERED => \Yii::t($category, 'Not delivered'),
            self::STATUS_DELIVERED => \Yii::t($category, 'Delivered'),
            self::STATUS_NOT_PAYED => \Yii::t($category, 'Payment not verified'),
            self::STATUS_PAYED => \Yii::t($category, 'Paid'),
            self::STATUS_RETURNED_PART => \Yii::t($category, 'Partially returned to the warehouse'),
            self::STATUS_RETURNED => \Yii::t($category, 'Returned to the warehouse'),
            self::STATUS_LOST => \Yii::t($category, 'Lost'),
        ];
    }

    public static function operatorStatusReplacements()
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';

        return [
            self::OPERATOR_STATUS_NEW => \Yii::t($category, 'Not processed'),
            self::OPERATOR_STATUS_FOUND => \Yii::t($category, 'Not found'),
            self::OPERATOR_STATUS_NOT_FOUND => \Yii::t($category, 'Found'),
        ];
    }

    public function statusReplacement($default = '')
    {
        $replacements = self::statusReplacements();
        return isset($replacements[$this->status]) ? $replacements[$this->status] : $default;
    }

    public function operatorStatusReplacement($default = '')
    {
        $replacements = self::operatorStatusReplacements();
        return isset($replacements[$this->operator_status]) ? $replacements[$this->operator_status] : $default;
    }

    public function beforeSave()
    {
        if (!$result = parent::beforeSave()) {
            return false;
        }

        if ($this->status_prev != $this->status) {
            $this->status_prev = $this->status;
            $this->status_changed_at = time();
        }

        if ($this->operator_status_prev != $this->operator_status && $this->operator_status != self::OPERATOR_STATUS_NEW) {
            $this->operator_status_prev = $this->operator_status;
            $this->operator_status_changed_at = time();
        }

        return $result;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function orderId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.order_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.order_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function orderIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.order_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function status($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.status' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function statusIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.status', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function operatorStatus($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.operator_status' => $value,
        ]);
        return $this;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function operatorStatusIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.operator_status', $values);
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function statusChangedAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.status_changed_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function statusChangedAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.status_changed_at' . '>=' . Cast::toStr($value));

        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function orderMailedAtLower($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.order_mailed_at' . '<' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $time
     *
     * @return static
     */
    public function orderMailedAtGreater($time)
    {
        $value = Cast::toUInt($time);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.order_mailed_at' . '>=' . Cast::toStr($value));

        return $this;
    }

    /**
     * @return static
     */
    public function mailedOrderEnded()
    {
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addCondition($alias . '.order_mailed_at + ' . strtotime(self::ACTUAL_MAILED_ORDER, 0) . '<=' . time());
        return $this;
    }

    /**
     * @return static
     */
    public function mailedOrderActual()
    {
        $alias = $this->getTableAlias();

        $this->getDbCriteria()->addCondition($alias . '.order_mailed_at + ' . strtotime(self::ACTUAL_MAILED_ORDER, 0) . '>' . time());
        return $this;
    }

    /** API */
    /**
     * @return DateTime
     */
    public function actualSendOrderDate()
    {
        $date = date_create();
        $date->setTimestamp($this->order_mailed_at);
        $date->modify('+' . self::ACTUAL_MAILED_ORDER);

        return $date;
    }

    /**
     * @return bool
     */
    public function isMailedOrderEnded()
    {
        return $this->order_mailed_at + strtotime(self::ACTUAL_MAILED_ORDER, 0) < time();
    }

    /**
     * @return bool
     */
    public function isAvailableChangeOperatorStatus()
    {
        return $this->operator_status == self::OPERATOR_STATUS_NEW || $this->operator_status_changed_at + strtotime(self::OPERATOR_CHANGE_STATUS, 0) > time();
    }
}
