<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class UserPasswordForm
 */
class UserPasswordForm extends \CFormModel
{
    public $password;

    public function rules()
    {
        return [
            ['password', 'required'],
            [
                'password', 
                'length', 
                'min' => 4, 
                'max' => 20,
                'tooShort' => Translator::t('This password is too short'),
                'tooLong' => Translator::t('Limit 20 characters')],
            ];
    }

    public function attributeLabels()
    {
        return [
            'password' => Translator::t('New password'),
        ];
    }
}
