<?php

namespace MommyCom\Model\Backend;

use MommyCom\Model\Db\OrderControlRecord;
use MommyCom\YiiComponent\Facade\Translator;

class OrderControlForm extends \CFormModel
{
    /**
     * @var int
     */
    public $operatorStatus;

    /**
     * @var string
     */
    public $comment;

    public function rules()
    {
        return [
            ['operatorStatus, comment', 'required'],
            ['operatorStatus', 'in', 'range' => array_keys(OrderControlRecord::operatorStatusReplacements())],
            ['comment', 'length', 'min' => 5, 'max' => 255, 'encoding' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'operatorStatus' => Translator::t('Order status'),
            'comment' => Translator::t('Comments'),
        ];
    }
}
