<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

class DistributionSmsForm extends \CFormModel
{
    /**
     * @var string
     */
    public $text;

    /**
     * @var array массив городов
     */
    public $cities;

    /**
     * @var integer
     */
    public $region;

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'length', 'max' => 240], //max 2 смс in translit
            ['cities, region', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => Translator::t('Text'),
            'cities' => Translator::t('City'),
            'region' => Translator::t('Region'),
        ];
    }

    public function afterValidate()
    {
        $this->text = Utf8::trim($this->text);
    }

}
