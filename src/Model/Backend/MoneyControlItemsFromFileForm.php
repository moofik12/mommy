<?php

namespace MommyCom\Model\Backend;

use CUploadedFile;
use LogicException;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;

class MoneyControlItemsFromFileForm extends \CFormModel
{
    const DELIMITER = '|';
    const ENCLOSURE = '"';
    const FILE_CSV = 'csv';
    const FILE_PDF = 'pdf';
    const FILE_TXT = 'txt';
    const FILE_UNKNOWN = 'unknown';

    /**
     * @var int
     */
    public $deliveryType;

    /**
     * @var string csv file
     */
    public $file;

    /**
     * @var int
     */
    public $numColumn;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['file', 'file', 'types' => [self::FILE_CSV, self::FILE_PDF, self::FILE_TXT], 'minSize' => 10, 'maxSize' => 20 * 1024 * 1024, 'allowEmpty' => false],
            ['deliveryType', 'in', 'range' => [
                DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA,
            ], 'message' => Translator::t('Import from file for Act is not supported yet')],
            ['numColumn', 'numerical', 'integerOnly' => true, 'min' => 1],
            ['numColumn', 'default', 'value' => 7, 'setOnEmpty' => true],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'file' => Translator::t('Load from file'),
            'numColumn' => Translator::t('No. column in CSV file'),
        ];
    }

    /**
     * @return string[]
     */
    public function getTextFile()
    {
        $rows = [];
        $file = CUploadedFile::getInstance($this, 'file');

        switch ($this->getFileType()) {
            case self::FILE_CSV:
                $dataColumn = $this->numColumn - 1;
                $dataCsv = CsvFile::load($file->tempName, self::DELIMITER, self::ENCLOSURE);
                $rows = ArrayUtils::getColumn($dataCsv->toArray(), $dataColumn);
                break;

            case self::FILE_PDF:
                $parser = new \Smalot\PdfParser\Parser();
                $pdf = $parser->parseFile($file->tempName);
                $text = $pdf->getText();
                $rows = explode("\n", $text);
                break;
            case self::FILE_TXT:
                $text = file_get_contents($file->tempName);
                $rows = explode("\n", $text);
                break;
            default:
                throw new LogicException(Translator::t('Type file not support to extract trackcodes!'));
        }

        return $rows;
    }

    /**
     * Возвращает тип файла из доступных констант FILE_...
     *
     * @return string
     */
    public function getFileType()
    {
        $type = self::FILE_UNKNOWN;
        $file = CUploadedFile::getInstance($this, 'file');

        switch ($file->type) {
            case 'text/csv':
                $type = self::FILE_CSV;
                break;

            case 'application/pdf':
            case 'application/x-pdf':
                $type = self::FILE_PDF;
                break;
            case 'text/plain' :
                $type = self::FILE_TXT;
                break;
        }
        return $type;
    }

}
