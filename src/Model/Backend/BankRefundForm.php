<?php

namespace MommyCom\Model\Backend;

use CValidator;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

class BankRefundForm extends \CFormModel
{
    /**
     * @var string
     */
    public $comment;

    /**
     * @var string
     */
    public $transaction;

    /**
     * для показа
     *
     * @var float
     */
    public $amount;

    /**
     * @var int
     */
    public $order;

    /**
     * @var float
     */
    public $order_card_payed;

    /**
     * @var int
     */
    public $type_refund;

    public function rules()
    {
        return [
            ['comment, transaction', 'length', 'max' => 250],
            ['order', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['amount', 'validateAmount'],
            ['transaction', 'validateTransaction'],
            ['type_refund', 'in', 'range' => [
                OrderBankRefundRecord::TYPE_REFUND_INNER,
                OrderBankRefundRecord::TYPE_REFUND_PRIVATE_BANK,
            ]],
        ];
    }

    public function validateTransaction($attribute)
    {
        if ($this->type_refund != OrderBankRefundRecord::TYPE_REFUND_INNER) {
            $validator = CValidator::createValidator('required', $this, $attribute);
            $validator->validate($this, $attribute);
        }
    }

    public function validateAmount($attribute)
    {
        $amount = Cast::toUFloat($this->{$attribute});
        /** @var OrderRecord|null $order */
        $order = OrderRecord::model()->findByPk($this->order);

        if ($order === null) {
            $this->addError($attribute, Translator::t('No order was found for money withdrawal'));
        } elseif ($order->card_payed - $amount < 0) {
            if ($order->isMerged()) {
                $this->addError($attribute, Translator::t('Money was transferred to another order'));
            } elseif (!$order->isCanceled()) {
                $this->addError($attribute, Translator::t('Insufficient amount for withdrawal'));
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'comment' => Translator::t('Comments'),
            'transaction' => Translator::t('Transaction №'),
            'order' => Translator::t('Order'),
            'amount' => Translator::t('Refund amount'),
            'order_card_payed' => Translator::t('Paid by non-cash method'),
            'type_refund' => Translator::t('Purpose'),
        ];
    }
}
