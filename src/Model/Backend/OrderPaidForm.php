<?php

namespace MommyCom\Model\Backend;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\YiiComponent\Facade\Translator;

class OrderPaidForm extends \CFormModel
{
    /**
     * @var string
     */
    public $provider;

    /**
     * @var
     */
    public $unique_payment;

    /**
     * для показа
     *
     * @var float
     */
    public $amount;

    /**
     * @var int
     */
    public $order;

    /**
     * @var string
     */
    public $comment;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['provider, unique_payment, amount, order, comment', 'required'],
            ['comment', 'length', 'max' => 250],
            ['provider', 'in', 'range' => PayGatewayRecord::getAvailableProvidersToPay()],

            ['unique_payment', 'unique', 'className' => 'MommyCom\Model\Db\PayGatewayRecord', 'attributeName' => 'unique_payment'],
            ['amount', 'numerical', 'min' => 0, 'max' => 9000],
            ['order', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['amount', 'validateAmount'],
        ];
    }

    /**
     * @param $attribute
     */
    public function validateAmount($attribute)
    {
        /** @var OrderRecord|null $order */
        $order = OrderRecord::model()->findByPk($this->order);

        if ($order === null) {
            $this->addError($attribute, Translator::t('No order was found to credit money to'));
            return;
        }

        $hasOrderPayment = PayGatewayRecord::model()->invoiceIdIs($order->id)->find();
        if ($hasOrderPayment) {
            if ($hasOrderPayment->amount + $this->amount > $order->getPrice()) {
                $this->addError($attribute, Translator::t('Payment #') . $hasOrderPayment->id .
                    Translator::t('for this order has been found. Previous and current payments exceed the order amount'));
            }
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'provider' => Translator::t('Payment provider'),
            'comment' => Translator::t('Comment'),
            'unique_payment' => Translator::t('Payment ID'),
            'order' => Translator::t('Order'),
            'amount' => Translator::t('Amount paid'),
        ];
    }
}
