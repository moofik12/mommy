<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Backend\AdmAuthManager\AdmUserIdentity;
use MommyCom\YiiComponent\Facade\Translator;

class AuthForm extends \CFormModel
{
    public $login;
    public $password;
    public $remember = false;
    private $_identity;

    public function rules()
    {
        return [
            ['login, password', 'required'],
            ['password', 'authenticate'],
        ];
    }

    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new AdmUserIdentity($this->login, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', $this->_identity->errorMessage);
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'login' => Translator::t('Login'),
            'password' => Translator::t('Password'),
        ];
    }

    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new AdmUserIdentity($this->login, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === AdmUserIdentity::ERROR_NONE) {
            //$duration = $this->remember ? Yii::app()->params['remember'] : 0;
            $duration = 12 * 60 * 60;
            \Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        return false;
    }
}
