<?php

namespace MommyCom\Model\Backend;

use CHtmlPurifier;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

class BrandForm extends \CFormModel
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $description_short;

    /**
     * @var string
     */
    public $aliasesStrings;

    /**
     * @var bool
     */
    public $isDeleteLogo;

    public $logo;

    public function rules()
    {
        return [
            ['name, description', 'filter', 'filter' => [Utf8::class, 'trim']],
            ['name', 'required'],
            ['name', 'length', 'min' => 1, 'max' => 100],
            ['name', 'unique', 'className' => 'MommyCom\Model\Db\BrandRecord', 'attributeName' => 'id'],
            ['description', 'length', 'max' => 65000],
            ['description_short', 'length', 'max' => 250],
            ['description', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],

            ['isDeleteLogo', 'boolean'],
            ['logo', 'file', 'mimeTypes' => 'image/gif, image/jpeg, image/jpg, image/png', 'allowEmpty' => true],

            ['aliasesStrings', 'length'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Translator::t('Brand'),
            'aliasesStrings' => Translator::t('Aliases'),
            'description' => Translator::t('Description'),
            'description_short' => Translator::t('Short description'),

            'logo' => Translator::t('Logotype'),
            'isDeleteLogo' => Translator::t('Delete logotype'),
        ];
    }
}
