<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;

class DistributionPushForm extends \CFormModel
{
    /** @var  string */
    public $subject;

    /** @var  string */
    public $body;

    /** @var  integer */
    public $start_at;

    /**
     * @var integer
     */
    public $start_time;

    /** @var  string */
    public $url;

    /** @var  string */
    public $url_desktop;

    /** @var  integer */
    public $id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['subject, start_at, body, url, url_desktop, start_time', 'required'],
            ['subject', 'length', 'max' => 255],
            ['subject, body, url, url_desktop', 'length', 'min' => 3, 'max' => 255, 'encoding' => false],
            ['start_at', 'safe'],
            ['url, url_desktop', 'url'],
            ['start_at', 'date', 'format' => 'dd.MM.yyyy'],
            ['start_time', 'type', 'type' => 'time'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'start_at' => Translator::t('Date of mail-out'),
            'start_time' => Translator::t('Start a mail-out'),
            'status' => Translator::t('Status'),
            'subject' => Translator::t('Title'),
            'body' => Translator::t('Description'),
            'url' => Translator::t('Link for applications'),
            'url_desktop' => Translator::t('Link for browsers'),
        ];
    }

    /**
     * @return int
     */
    public function getStartAt()
    {
        $time = strtotime("$this->start_at $this->start_time");

        return $time;
    }
}
