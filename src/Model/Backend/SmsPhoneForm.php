<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

class SmsPhoneForm extends \CFormModel
{
    /**
     * @var integer
     */
    public $phone;

    /**
     * @var string
     */
    public $text;

    /**
     * @var boolean
     */
    public $saveText = false;

    public function rules()
    {
        return [
            ['text, phone', 'required'],
            ['text', 'length', 'max' => 240], //2 sms
            ['saveText', 'boolean'],

            ['phone', 'validateTelephone'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => Translator::t('Phone number'),
            'text' => Translator::t('Text'),
            'saveText' => Translator::t('Save message text'),
        ];
    }

    public function afterValidate()
    {
        $this->text = Utf8::trim($this->text);
    }

    /**
     * валидация телефонных номеров Украины!
     *
     * @param string $attribute
     * @param string $params
     */
    public function validateTelephone($attribute, $params)
    {
        $phone = $this->$attribute;

        if (preg_match('/[^+0-9- ()]/', $phone)) {
            $this->addError($attribute, Translator::t('Invalid characters in phone number!'));
        } else {
            $phone = preg_replace("/[^0-9]/", '', $phone);
            $phoneNumber = mb_substr($phone, -9);

            if (mb_strlen($phone) > 12) {
                $this->addError($attribute, Translator::t('The quantity of digits in the phone number exceeds allowed'));
            } elseif (mb_strlen($phoneNumber) < 9) {
                $this->addError($attribute, Translator::t('The phone number must contain the operator code'));
            }
        }

        if (!\Yii::app()->sms->isValidNumber($phone)) {
            $this->addError($attribute, Translator::t('Invalid mobile phone number'));
        }
    }
}
