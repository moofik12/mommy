<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class ProductImageZipForm
 * Загрузка пакета изображений для товаров
 */
class ProductImageZipForm extends \CFormModel
{
    const MAX_COUNT_FILES = 10;
    public $eventId;

    public $zipes;

    public $forceUpdate = false;

    public function rules()
    {
        return [
            ['eventId', 'required'],

            ['eventId', 'exist', 'className' => 'MommyCom\Model\Db\EventRecord', 'attributeName' => 'id'],
            ['forceUpdate', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'zipes' => Translator::t('Zip-files'),
            'eventId' => Translator::t('Flash-sale'),
            'forceUpdate' => Translator::t('Update products with images'),
        ];
    }

    public function getConfig()
    {
        return [
            'showErrorSummary' => true,
            'enctype' => 'multipart/form-data',
            'elements' => [
                'eventId' => [
                    'type' => 'hidden',
                ],
                'forceUpdate' => [
                    'type' => 'checkbox',
                ],
                'zipes' => [
                    'type' => 'CMultiFileUpload',
                    'max' => self::MAX_COUNT_FILES,
                    'accept' => 'zip',
                    'remove' => Translator::t('Delete'),
                    'denied' => Translator::t('Possible format for uploading is *.zip'),
                    'selected' => Translator::t('Will be uploaded'),
                    'duplicate' => Translator::t('This file has already been selected for upload'),
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
            ],
        ];
    }
}
