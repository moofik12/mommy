<?php

namespace MommyCom\Model\Backend;

use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnProductRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class WarehouseStatementReturnForm
 *
 * @property array $productsID
 */
class WarehouseStatementReturnForm extends \CFormModel
{
    /**
     * @var array Список складских товаров
     */
    private $_productsID;

    /**
     * @var int ID поставщика
     */
    private $_supplierID;

    /**
     * @var WarehouseProductRecord[]
     */
    private $_products = [];

    public function rules()
    {
        return [
            ['productsID', 'required', 'message' => Translator::t('You need to add warehouse products')],
            ['productsID', 'warehouseProductValidator'],
        ];
    }

    /**
     * @return array
     */
    public function getProductsID()
    {
        return $this->_productsID;
    }

    /**
     * @param array $productsID
     * @param string $delimiter
     */
    public function setProductsID($productsID, $delimiter = ',')
    {
        if (is_scalar($productsID)) {
            $productsID = explode($delimiter, Cast::toStr($productsID));
        }

        if (is_array($productsID)) {
            $this->_productsID = $productsID;
        }
    }

    /**
     * @return int
     */
    public function getSupplierID()
    {
        return $this->_supplierID;
    }

    /**
     * @return WarehouseProductRecord[]
     */
    public function getProducts()
    {
        return $this->_products;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function warehouseProductValidator($attribute, $params)
    {
        $products = $this->getProductsID();
        $supplierID = (int)$this->getSupplierID();
        $warehouseProducts = $this->getProducts();
        $allowedStatuses = [
            WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
            WarehouseProductRecord::STATUS_BROKEN,
        ];

        if (empty($products)) {
            $this->addError($attribute, Translator::t('There are no items to add to the statement'));
            return;
        }

        $warehouseProductsNotFound = array_diff($products, ArrayUtils::getColumn($warehouseProducts, 'id'));

        if ($warehouseProductsNotFound) {
            $this->addError($attribute, Translator::t('No warehouse products found') . ': ' . implode(',', $warehouseProductsNotFound));
            return;
        }

        $warehouseStatementsProducts = WarehouseStatementReturnProductRecord::model()->productIdIn($products)->findColumnDistinct('product_id');

        if ($warehouseStatementsProducts) {
            $this->addError($attribute, Translator::t('Warehouse products')
                . ': ' . implode(',', $warehouseStatementsProducts) . ' ' .
                Translator::t('found in other statements'));
            return;
        }

        $supplierProductsNotAllowed = [];
        foreach ($warehouseProducts as $product) {
            /* @var  WarehouseProductRecord $product */
            if ($product->order_id > 0) {
                $this->addError($attribute, Translator::t('Product') . ":" . $product->id .
                    Translator::t('is already tied to the order:') . ' ' . $product->order_id);
            }

            if ($supplierID != $product->supplier_id) {
                $supplierProductsNotAllowed[] = $product->supplier_id;
            }

            if (!in_array($product->status, $allowedStatuses)) {
                $this->addError($attribute, Translator::t('Product') . $product->id .
                    Translator::t('can not be added because of it\'s status:') . ' ' . $product->getStatusReplacement());
            }
        }

        if ($supplierProductsNotAllowed) {
            $this->addError($attribute, "It is allowed to create a statement for only one supplier");
        }
    }

    /**
     * @return bool
     */
    protected function beforeValidate()
    {
        //INIT PRIVATE ATTRIBUTES
        $products = $this->getProductsID();
        if ($products) {
            /* @var  $warehouseProducts [] */
            $warehouseProducts = WarehouseProductRecord::model()->idIn($products)->findAll();
            $warehouseProduct = reset($warehouseProducts);

            $this->_products = $warehouseProducts;
            if ($warehouseProduct) {
                /* @var WarehouseProductRecord $warehouseProduct */
                $this->_supplierID = $warehouseProduct->supplier_id;
            }
        }

        return parent::beforeValidate();
    }

    /**
     * Clear attributes
     */
    public function clear()
    {
        $this->_products = [];
        $this->_productsID = null;
        $this->_supplierID = null;
    }

}
