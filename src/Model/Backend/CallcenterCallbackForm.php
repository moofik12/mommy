<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;

class CallcenterCallbackForm extends \CFormModel
{
    /**
     * @var string
     */
    public $callcenterComment;

    /**
     * @var string
     */
    public $timeString;

    public function rules()
    {
        return [
            ['callcenterComment', 'required', 'except' => ['postponed']],
            ['timeString', 'required', 'on' => ['postponed']],
            ['timeString, callcenterComment', 'length'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'callcenterComment' => Translator::t('Comment'),
            'timeString' => Translator::t('Time'),
        ];
    }

    /**
     * @return bool|string
     */
    public function getNextCallTime()
    {
        return strtotime("today $this->timeString");
    }
}
