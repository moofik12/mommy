<?php

namespace MommyCom\Model\Backend;

use CJavaScriptExpression;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class ProductForm
 */
class ProductForm extends \CFormModel
{
    public $logo;
    public $images;
    public $imagesUpload;
    public $description;
    public $name;
    public $madeIn;
    public $designIn;
    public $sectionId;
    public $colorCode;

    public function attributeLabels()
    {
        return [
            'logo' => Translator::t('Logotype'),
            'imagesUpload' => Translator::t('Images'),
            'description' => Translator::t('Description'),
            'name' => Translator::t('Name'),
            'madeIn' => Translator::t('Production'),
            'designIn' => Translator::t('Design'),
            'sectionId' => Translator::t('Product section'),
            'colorCode' => Translator::t('Color'),
        ];
    }

    public function rules()
    {
        return [
            ['description', 'validDescription'],
            ['logo, images, description, name, madeIn, designIn, sectionId, colorCode', 'safe'],
        ];
    }

    public function validDescription($attr)
    {
        $description = $this->$attr;
        $description = strip_tags(trim($description), '<br><p><a><i><b><ul><li><ol>');
        $description = str_replace('&#182;', '', $description);
        $description = str_replace('¶', '', $description);
        $this->$attr = $description;
    }

    public function getConfig()
    {
        return [
            'showErrorSummary' => true,
            'enctype' => 'multipart/form-data',
            'elements' => [
                'logo' => [
                    'type' => 'file',
                ],
                'imagesForDelete' => [
                    'type' => 'checkboxlist',
                ],
                /*
                'images' => array(
                    'type' => 'CMultiFileUpload',
                    'max' => ProductRecord::MAX_IMAGES_COUNT,
                ),
                */
                'imagesUpload' => [
                    'type' => 'bootstrap.widgets.TbFileUpload',
                    'url' => \Yii::app()->createUrl('product/photoUpload', ['class' => 'ProductForm', 'attribute' => 'imagesUpload']),
                    'multiple' => true,
                    'formView' => 'widgets.views.fileupload.product.form',
                    'uploadView' => 'widgets.views.fileupload.product.upload',
                    'downloadView' => 'widgets.views.fileupload.product.download',
                    'options' => [
                        'filesContainer' => '#current-image-views',
                        'submit' => new CJavaScriptExpression('function (e, data) {
                            var $photo = $("input[class*=\"delete-photo\"]");
                            var $maxPhoto = ' . ProductRecord::MAX_IMAGES_COUNT . ';
                            console.log($photo.length + " >=  " + $maxPhoto);
                            if ($photo.length >= $maxPhoto) {
								alert("' . Translator::t('The maximum number of photos should not exceed') . ' "  + $maxPhoto);
                                return false;
                            }

                            return true;
                        }'),
                    ],
                ],
                'description' => [
                    'type' => 'bootstrap.widgets.TbRedactorJs',
                    'editorOptions' => [
                        'buttons' => ['bold', 'italic', '|', 'unorderedlist', 'orderedlist', '|', 'html'],
                    ],
                ],
                'name' => [
                    'type' => 'text',
                    'class' => 'span6',
                ],
                'madeIn' => [
                    'type' => 'text',
                    'class' => 'span4',
                ],
                'designIn' => [
                    'type' => 'text',
                    'class' => 'span4',
                ],
                'sectionId' => [
                    'type' => 'select2',
                    'data' => ProductSectionRecord::getGroupListForFilter(),
                    'asDropDownList' => true,
                ],
                'colorCode' => [
                    'type' => 'select2',
                    'data' => ProductColorCodes::instance()->getLabels(),
                    'asDropDownList' => true,
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
                'reset' => [
                    'type' => 'reset',
                    'label' => Translator::t('Cancel'),
                ],
            ],
        ];
    }
}
