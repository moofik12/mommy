<?php

namespace MommyCom\Model\Backend;

use CActiveDataProvider;
use CHtml;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class WarehouseReceptionForm
 */
class WarehouseReceptionForm extends \CFormModel
{
    public $warehouseIds = [];
    private $_warehouseId = 0;
    public $status;
    public $comment = '';

    public $statusResolved = false;
    public $currentStatus;

    public function rules()
    {
        return [
            ['warehouseIds, status, warehouseId', 'safe'],

            ['warehouseIds, status', 'required'],
            ['warehouseIds', 'type', 'type' => 'array'],

            ['warehouseId', 'exist', 'allowEmpty' => true, 'className' => 'MommyCom\Model\Db\WarehouseProductRecord', 'attributeName' => 'id'],
            ['warehouseId', 'extendedWarehouseValidator'],
            ['warehouseIds', 'extendedWarehousesListValidator'],

            ['status', 'extendedStatusValidator'],
        ];
    }

    public function extendedWarehousesListValidator($attribute)
    {
        $values = array_values($this->$attribute);

        $statusResolved = false;
        $firstStatus = null;
        foreach ($values as $index => $value) {
            if (!is_numeric($value)) {
                $this->addError($attribute, Translator::t('Must be a number'));
                break;
            }

            $warehouse = WarehouseProductRecord::model()->findByPk($value);
            /* @var $warehouse WarehouseProductRecord */

            if ($warehouse === null) {
                $this->addError($attribute, Translator::t('Not found in database'));
                break;
            }

            if (!$statusResolved) {
                $firstStatus = $warehouse->status;
                $statusResolved = true;
            }

            if ($warehouse->status != $firstStatus) {
                $this->addError($attribute, Translator::t('While massive changing of statuses for all products, the statuses of the products should be the same'));
                break;
            }
        }

        if ($statusResolved) {
            $this->statusResolved = true;
            $this->currentStatus = $firstStatus;
        }
    }

    public function extendedStatusValidator($attribute)
    {
        $value = $this->$attribute;

        if (!$this->statusResolved) {
            return;
        }

        $currentStatus = $this->currentStatus;

        if (!in_array($currentStatus, [WarehouseProductRecord::STATUS_UNMODERATED, WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED])) {
            $this->addError($attribute, Translator::t('Can not edit current product status'));
        }

        $chains = WarehouseProductRecord::statusChains();
        if ($value != $currentStatus && (!isset($chains[$currentStatus]) || !in_array($value, $chains[$currentStatus]))) {
            $this->addError($attribute, Translator::t('Can not be changed to this status'));
        }
    }

    public function extendedWarehouseValidator($attribute)
    {
        $value = $this->$attribute;

        if (empty($value)) {
            return;
        }

        $warehouse = WarehouseProductRecord::model()->findByPk($value);
        /* @var $warehouse WarehouseProductRecord */
        if ($warehouse !== null && $this->statusResolved && $warehouse->status != $this->currentStatus) {
            $this->addError($attribute, Translator::t('While massive changing of statuses for all products, the statuses of the products should be the same'));
        }

        if (!$this->hasErrors($attribute)) {
            $this->$attribute = ''; // clear

        }
    }

    public function attributeLabels()
    {
        return [
            'status' => Translator::t('Status'),
            'comment' => Translator::t('Dispose'),
            'warehouseId' => Translator::t('Product'),
        ];
    }

    public function setWarehouseId($value)
    {
        $this->_warehouseId = $value;
        $sanitized = Cast::toUInt($value);
        if (!empty($sanitized) && !in_array($sanitized, $this->warehouseIds)) {
            $this->warehouseIds[] = Cast::toUInt($sanitized);
        }
    }

    public function getWarehouseId()
    {
        return $this->_warehouseId;
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $name = 'WarehouseReceptionForm' . '[warehouseIds][]';
        $currentWarehouse = '';
        $ids = Cast::toUIntArr($this->warehouseIds);
        foreach ($ids as $id) {
            $currentWarehouse .= CHtml::hiddenField($name, $id);
        }

        return [
            'showErrorSummary' => true,
            'elements' => [
                'warehouseIds' => [
                    'required' => false,
                    'type' => 'string',
                    'content' => $currentWarehouse,
                ],
                'status' => [
                    'type' => 'dropdownlist',
                    'items' => ArrayUtils::valuesByKeys(
                        WarehouseProductRecord::statusReplacements(),
                        [
                            WarehouseProductRecord::STATUS_UNMODERATED,
                            WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                        ],
                        true
                    ),
                ],
                'warehouseId' => [
                    'type' => 'text',
                ],
                'addProduct' => [
                    'type' => 'string',
                    'content' => '<button type="submit" name="addProduct" class="btn btn-success">' . Translator::t('Add') . '</button>',
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
                'reset' => [
                    'type' => 'reset',
                    'label' => Translator::t('Cancel'),
                ],
            ],
        ];
    }

    /**
     * @return CActiveDataProvider
     */
    public function getConnectedWarehousesProvider()
    {
        return WarehouseProductRecord::model()->idIn($this->warehouseIds)->getDataProvider(false, [
            'pagination' => false,
        ]);
    }

    /**
     * @return WarehouseProductRecord[]
     */
    public function getConnectedWarehouses()
    {
        return WarehouseProductRecord::model()->idIn($this->warehouseIds)->findAll();
    }

    /**
     * @param bool $validate
     *
     * @return bool
     */
    public function save($validate = true)
    {
        if ($validate && !$this->validate()) {
            return false;
        }

        $warehouses = $this->getConnectedWarehouses();

        foreach ($warehouses as $item) {
            $item->status = $this->status;
            $item->status_comment = $this->comment;
        }

        if (!UnShardedActiveRecord::saveMany($warehouses)) {
            foreach ($warehouses as $item) {
                if ($item->hasErrors()) {
                    $this->addErrors($item->getErrors());
                    return false;
                }
            }
        }

        return true;
    }
}
