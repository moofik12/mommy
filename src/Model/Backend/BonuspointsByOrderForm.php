<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;

class BonuspointsByOrderForm extends \CFormModel
{
    public $orderId;
    public $points;
    public $customMessage;
    public $expireAtString;

    public function rules()
    {
        return [
            ['orderId, customMessage, points', 'required'],
            ['customMessage', 'length', 'min' => 1],
            ['points', 'numerical', 'integerOnly' => true],
            ['points', 'compare', 'compareValue' => 0, 'operator' => '!='],
            ['orderId', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['expireAtString', 'length'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'orderId' => Translator::t('Order No.'),
            'points' => Translator::t('Bonuses'),
            'customMessage' => Translator::t('Message'),
            'expireAtString' => Translator::t('Validity period'),
        ];
    }
}
