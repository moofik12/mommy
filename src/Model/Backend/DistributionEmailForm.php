<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Purifier\StaticPagePurifier;
use MommyCom\YiiComponent\Utf8;

class DistributionEmailForm extends \CFormModel
{
    /**
     * @var array массив городов
     */
    public $cities;

    /**
     * @var integer
     */
    public $region;

    /**
     * @var
     */
    public $subject;

    /**
     * @var string
     */
    public $body;

    public function rules()
    {
        return [
            ['body, subject', 'required'],
            ['subject', 'length', 'max' => 255],
            ['body', 'length', 'max' => 30000],
            ['cities, region', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'body' => Translator::t('Text'),
            'subject' => Translator::t('Subject (Title)'),
            'cities' => Translator::t('City'),
            'region' => Translator::t('Region'),
        ];
    }

    public function afterValidate()
    {
        $purifier = new StaticPagePurifier();
        $this->body = $purifier->purify(Utf8::trim($this->body));
    }
}
