<?php

namespace MommyCom\Model\Backend;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Facade\Translator;

class SupplierOrderReturnForm extends \CFormModel
{
    public $order_id;

    public $message_callcenter;

    public $bankName;

    public $bankGiro;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['order_id', 'exist', 'className' => 'MommyCom\Model\Db\OrderRecord', 'attributeName' => 'id'],
            ['order_id', 'validatorOrder'],
            ['message_callcenter', 'length', 'max' => 400],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Translator::t('Order №'),
            'message_callcenter' => Translator::t('Comments'),
            'bankName' => Translator::t('Bank name'),
            'bankGiro' => Translator::t('Settlement account number/ Bank card number'),
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatorOrder($attribute, $params)
    {
        $orderId = $this->$attribute;
        $order = OrderRecord::model()->findByPk($orderId);

        if ($order === null) {
            $this->addError($attribute, Translator::t('Order not found'));
            return;
        }

        if (!$order->is_drop_shipping) {
            $this->addError($attribute, Translator::t('The order was made not from flash-sale with dropshipping'));
        }
    }
}
