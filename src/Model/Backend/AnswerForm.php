<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

/**
 * Class AnswerForm
 */
class AnswerForm extends \CFormModel
{
    public $fromEmail;
    public $answerMessage;

    public function rules()
    {
        return [
            ['answerMessage', 'filter', 'filter' => [Utf8::class, 'trim'], 'on' => 'email'],
            ['answerMessage', 'required', 'on' => 'email'],
            ['fromEmail', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'fromEmail' => Translator::t('Sender e-mail'),
            'answerMessage' => Translator::t('Request response'),
        ];
    }
}
