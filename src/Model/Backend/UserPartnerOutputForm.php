<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class UserPartnerOutputForm
 * Форма для редактирования данных заявке на вывод средств парнера
 */
class UserPartnerOutputForm extends \CFormModel
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $reason;
    /**
     * @var int
     */
    public $requisite;

    /**
     * @return array
     */
    public function rules()
    {
        return [

            ['id', 'required', 'on' => 'setReason, setRequisite'],
            ['id', 'exist', 'className' => 'MommyCom\Model\Db\UserPartnerOutputRecord', 'attributeName' => 'id', 'on' => 'setReason, setRequisite'],
            ['reason', 'required', 'on' => 'setReason'],
            ['reason', 'length', 'max' => 100],

            ['requisite', 'required', 'on' => 'setRequisite'],
            ['requisite', 'length', 'max' => 128],
            ['requisite', 'unique', 'className' => 'MommyCom\Model\Db\UserPartnerOutputRecord', 'attributeName' => 'unique_payment', 'on' => 'setRequisite'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Translator::t('Request ID'),
            'reason' => Translator::t('Reason for canceling request'),
            'requisite' => Translator::t('Payment ID (document number)'),
        ];
    }
}
