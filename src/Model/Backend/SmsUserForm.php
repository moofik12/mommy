<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Utf8;

class SmsUserForm extends \CFormModel
{
    /**
     * @var array
     */
    public $userIds;

    /**
     * @var string
     */
    public $text;

    public function rules()
    {
        return [
            ['text, userIds', 'required'],
            ['text', 'length', 'max' => 240],
        ];
    }

    public function attributeLabels()
    {
        return [
            'userIds' => Translator::t('Consignee'),
            'text' => Translator::t('Text'),
        ];
    }

    public function afterValidate()
    {
        $this->text = Utf8::trim($this->text);
    }
}
