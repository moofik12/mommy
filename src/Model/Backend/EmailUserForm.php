<?php

namespace MommyCom\Model\Backend;

use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Purifier\StaticPagePurifier;
use MommyCom\YiiComponent\Utf8;

class EmailUserForm extends \CFormModel
{
    /**
     * @var array
     */
    public $userIds;

    /**
     * @var
     */
    public $subject;

    /**
     * @var string
     */
    public $body;

    public function rules()
    {
        return [
            ['body, userIds, subject', 'required'],
            ['subject', 'length', 'max' => 255],
            ['body', 'length', 'max' => 30000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'userIds' => Translator::t('Consignee'),
            'body' => Translator::t('Text'),
            'subject' => Translator::t('Subject (Title)'),
        ];
    }

    public function afterValidate()
    {
        $purifier = new StaticPagePurifier();
        $this->body = $purifier->purify(Utf8::trim($this->body));
    }
}
