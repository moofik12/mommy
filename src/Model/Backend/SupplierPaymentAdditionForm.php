<?php

namespace MommyCom\Model\Backend;

use InvalidArgumentException;
use LogicException;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class SupplierPaymentAdditionForm
 *
 * @property-read array $warehouseStatementReturnsID
 * @property-read string $warehouseStatementReturnsIDAsString
 * @property-read array $unpaidPaymentsID
 * @property-read string $unpaidPaymentsIDAsString
 */
class SupplierPaymentAdditionForm extends \CFormModel
{
    public $custom_penalty_amount = 0.0;

    /**
     * @var WarehouseStatementReturnRecord[]
     */
    private $_warehouseStatementReturnRecords = [];

    /**
     * @var SupplierPaymentRecord[]
     */
    private $_unpaidPayments = [];

    /**
     * некоторые действия разрешены только перед валидацией
     *
     * @var bool
     */
    private $_validated = false;

    /**
     * @var SupplierPaymentRecord
     */
    private $_record;

    /**
     * @param SupplierPaymentRecord $record
     * @param string $scenario
     */
    public function __construct(SupplierPaymentRecord $record, $scenario = '')
    {
        $this->_record = $record;
        parent::__construct($scenario);
    }

    /**
     * @return array
     */
    public function attributeNames()
    {
        return parent::attributeNames() + [
                'warehouseStatementReturnsIDAsString',
                'warehouseStatementReturnsID',
                'unpaidPaymentsID',
                'unpaidPaymentsIDAsString',
            ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['custom_penalty_amount', 'numerical', 'min' => 0, 'max' => 10000],

            ['warehouseStatementReturnsID', 'safe'],
            ['warehouseStatementReturnsIDAsString', 'validatorWarehouseStatementReturns'],

            ['unpaidPaymentsID', 'safe'],
            ['unpaidPaymentsIDAsString', 'validatorUnpaidPayments'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'warehouseStatementReturnsIDAsString' => Translator::t('Returns to supplier'),
            'warehouseStatementReturnsID' => Translator::t('Returns to supplier'),
            'unpaidPaymentsID' => Translator::t('Payment of outstanding debt'),
            'unpaidPaymentsIDAsString' => Translator::t('Payment of outstanding debt'),
            'custom_penalty_amount' => $this->_record->getAttributeLabel('custom_penalty_amount'),
        ];
    }

    /**
     * This method is invoked after validation ends.
     * The default implementation calls {@link onAfterValidate} to raise an event.
     * You may override this method to do postprocessing after validation.
     * Make sure the parent implementation is invoked so that the event can be raised.
     */
    protected function afterValidate()
    {
        parent::afterValidate();
        $this->_validated = true;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatorWarehouseStatementReturns($attribute, $params)
    {
        $supplierPaymentID = $this->_record->id;
        $haveSupplierPayments = WarehouseStatementReturnRecord::model()
            ->supplierPayment($supplierPaymentID)->findAll();

        $haveSupplierPaymentsID = ArrayUtils::getColumn($haveSupplierPayments, 'supplier_payment_id', 'id');

        $haveInOtherSupplierPayments = array_filter($haveSupplierPaymentsID, function ($supplier_payment_id) use ($supplierPaymentID) {
            if ($supplier_payment_id != 0 && $supplier_payment_id != $supplierPaymentID) {
                return true;
            }

            return false;
        });

        if ($haveInOtherSupplierPayments) {
            $error = '';
            foreach ($haveInOtherSupplierPayments as $warehouseStatementReturnID => $supplierPaymentID) {
                $error .= Translator::t("The refund Act No.") .
                    $warehouseStatementReturnID . ' ' . Translator::t('was found in bill No.') . $supplierPaymentID;
            }

            $this->addError($attribute, $error);
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatorUnpaidPayments($attribute, $params)
    {
        foreach ($this->_unpaidPayments as $record) {
            if ($record->unpaid_in_payment_id > 0 && $record->unpaid_in_payment_id != $this->_record->id) {
                $this->addError($attribute, Translator::t("It is impossible to add the debt of the supplier No.") .
                    $record->id . ' ' .
                    Translator::t('into the bill, since this debt was already recorded in the bill No.') .
                    $record->unpaid_in_payment_id);

                break;
            }
        }
    }

    /**
     * @return SupplierPaymentRecord
     */
    public function getRecord()
    {
        return $this->_record;
    }

    /**
     * @return string
     */
    public function getWarehouseStatementReturnsIDAsString()
    {
        return implode(',', $this->getWarehouseStatementReturnsID());
    }

    /**
     * @param $value
     *
     * @throw InvalidArgumentException
     */
    public function setWarehouseStatementReturnsIDAsString($value)
    {
        if (!is_scalar($value)) {
            throw new InvalidArgumentException(Translator::t('Values in function') . ' ' . __FUNCTION__ . ' ' . Translator::t('must be scalar'));
        }
        $ids = explode(',', $value);
        $this->setWarehouseStatementReturnsID($ids);
    }

    /**
     * @return array
     */
    public function getWarehouseStatementReturnsID()
    {
        return array_keys($this->_warehouseStatementReturnRecords);
    }

    /**
     * @param array $ids
     */
    public function setWarehouseStatementReturnsID(array $ids)
    {
        $model = WarehouseStatementReturnRecord::model();

        $records = [];
        foreach ($ids as $id) {
            $record = $model->findByPk($id);
            if ($record) {
                $records[] = $record;
            }
        }

        if (count($records) != count($ids)) {
            throw new LogicException(Translator::t('Some return Acts have not been found'));
        }

        $this->setWarehouseStatementReturns($records);
    }

    /**
     * @param array $ids
     */
    public function setUnpaidPaymentsID(array $ids)
    {
        $supplierPaymentRecord = SupplierPaymentRecord::model();

        $records = [];
        foreach ($ids as $id) {
            $record = $supplierPaymentRecord->findByPk($id);
            if ($record) {
                $records[] = $record;
            }
        }

        if (count($records) != count($ids)) {
            throw new LogicException(Translator::t('Some payments for suppliers have not been found'));
        }

        $this->setUnpaidPayments($records);
    }

    /**
     * @return array
     */
    public function getUnpaidPaymentsID()
    {
        return array_keys($this->_unpaidPayments);
    }

    /**
     * @param $value
     */
    public function setUnpaidPaymentsIDAsString($value)
    {
        if (!is_scalar($value)) {
            throw new InvalidArgumentException(Translator::t('Values in function') . ' ' . __FUNCTION__ . ' ' . Translator::t('must be scalar'));
        }

        $ids = explode(',', $value);
        $this->setUnpaidPaymentsID($ids);
    }

    /**
     * @return string
     */
    public function getUnpaidPaymentsIDAsString()
    {
        return implode(',', $this->getUnpaidPaymentsID());
    }

    /**
     * @return SupplierPaymentRecord[]
     */
    public function getUnpaidPayments()
    {
        return array_values($this->_unpaidPayments);
    }

    /**
     * @return array
     */
    public function getWarehouseStatementReturns()
    {
        return array_values($this->_warehouseStatementReturnRecords);
    }

    /**
     * @param array $warehouseStatementReturnsID
     */
    public function setWarehouseStatementReturns(array $warehouseStatementReturnsID)
    {
        $this->_warehouseStatementReturnRecords = [];

        foreach ($warehouseStatementReturnsID as $record) {
            $this->setWarehouseStatementReturn($record);
        }
    }

    /**
     * @param WarehouseStatementReturnRecord $record
     */
    public function setWarehouseStatementReturn(WarehouseStatementReturnRecord $record)
    {
        if ($this->_validated) {
            throw new LogicException(Translator::t('Add WarehouseStatementReturnRecord in form after validation'));
        }

        if ($this->_record->supplier_id != $record->supplier_id) {
            throw new LogicException(Translator::t("Different suppliers specified in payment and return. In payment supplier No.") .
                $this->_record->supplier_id . ',' . Translator::t('in the return  supplier No.') . $record->supplier_id);
        }

        $this->_warehouseStatementReturnRecords[$record->id] = $record;
    }

    /**
     * @param SupplierPaymentRecord[] $unpaidPayments
     */
    public function setUnpaidPayments(array $unpaidPayments)
    {
        $this->_unpaidPayments = [];

        foreach ($unpaidPayments as $record) {
            $this->setUnpaidPayment($record);
        }
    }

    /**
     * @param SupplierPaymentRecord $payment
     */
    public function setUnpaidPayment(SupplierPaymentRecord $payment)
    {
        $this->_unpaidPayments[$payment->id] = $payment;
    }

    /**
     * @return float|int
     */
    public function getWarehouseStatementReturnsAmount()
    {
        return array_reduce($this->getWarehouseStatementReturns(), function ($carry, $record) {
            /* @var WarehouseStatementReturnRecord $record */
            return $carry + $record->positionsPurchaseAmount();
        }, 0.0);
    }

    /**
     * @return static
     */
    public function loadDataFromRecord()
    {
        $this->setWarehouseStatementReturns($this->_record->statementReturns);
        $this->setUnpaidPayments($this->_record->includeUnpaidPayments);
        $this->custom_penalty_amount = $this->_record->custom_penalty_amount;

        return $this;
    }

}
