<?php

namespace MommyCom\Model\Backend;

use MommyCom\Model\Db\MoneyReceiveStatementOrderRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class MoneyControlForm
 *
 * @property array $items
 * @property-read float $moneyExpected
 */
class MoneyControlForm extends \CFormModel
{
    /**
     * @var int
     */
    public $deliveryType;

    /**
     * @var float
     */
    public $moneyReal;

    /**
     * @var array array('order_id' => array ('trackcode', 'price'))
     */
    private $_items = [];

    /**
     * @var float
     */
    private $_moneyExpected = false;

    /**
     * @var OrderRecord[]
     */
    private $_orders = false;

    public function rules()
    {
        return [
            ['items', 'validateItems'],
            ['moneyReal', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'deliveryType' => Translator::t('Delivery type'),
            'moneyReal' => Translator::t('Amount'),
            'moneyExpected' => Translator::t('Expected amount'),
        ];
    }

    public function validateItems($attribute, $param)
    {
        $ordersIds = Cast::toUIntArr($this->getOrderIds());
        $orders = $this->getOrders();
        $ordersHas = Cast::toUIntArr(ArrayUtils::getColumn($orders, 'id'));

        if (empty($orders)) {
            $this->addError('moneyReal', Translator::t('No orders'));
        }

        $notOrders = array_diff($ordersHas, $ordersIds);
        if ($notOrders) {
            $this->addError('moneyReal', Translator::t('Orders not found') . ': ', implode(',', $notOrders));
        }

        $this->_orders = $orders;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $newItems = [];
        if (current($items) instanceof MoneyReceiveStatementOrderRecord) {
            /** @var MoneyReceiveStatementOrderRecord[] $items */
            foreach ($items as $item) {
                $newItems[$item->order_id] = $item->trackcode;
            }
        } else {
            $newItems = $items;
        }

        $this->_items = $newItems;
    }

    /**
     * @return array(order_id => 'trackcode');
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * @return array
     */
    public function getOrderIds()
    {
        return array_keys($this->_items);
    }

    /**
     * @return array
     */
    public function getTrackcodes()
    {
        return array_values($this->_items);
    }

    /**
     * @return float
     */
    public function getMoneyExpected()
    {
        if ($this->_moneyExpected === false) {
            $ordersIds = $this->getOrderIds();
            $orders = OrderRecord::model()->idIn($ordersIds)->with('positions')->findAll();
            $sum = 0.0;

            /* @var $orders OrderRecord[] */
            foreach ($orders as $order) {
                $sum += $order->getPayPriceHistory();
            }

            $this->_moneyExpected = $sum;
        }

        return $this->_moneyExpected;
    }

    public function getOrders()
    {
        if ($this->_orders === false) {
            $normalizeOrders = [];
            $ordersIds = $this->getOrderIds();
            /** @var OrderRecord[] $orders */
            $orders = OrderRecord::model()->idIn($ordersIds)->findAll();
            foreach ($orders as $order) {
                $normalizeOrders[$order->id] = $order;
            }

            $this->_orders = $normalizeOrders;
        }

        return $this->_orders;
    }
}
