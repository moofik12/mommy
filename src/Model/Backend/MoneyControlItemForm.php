<?php

namespace MommyCom\Model\Backend;

use MommyCom\Model\Db\MoneyControlRecord;
use MommyCom\Model\Db\MoneyReceiveStatementOrderRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Deprecated\Delivery\DeliveryCountryGroups;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class MoneyControlItemForm
 *
 * @property-read OrderRecord|null $_order
 * @property-read float $price
 */
class MoneyControlItemForm extends \CFormModel
{
    /**
     * @var string
     */
    public $trackcode;

    /**
     * @var array
     */
    public $order;

    /**
     * @var int
     */
    public $deliveryType;

    /**
     * @var OrderRecord|null
     */
    private $_order = false;

    /**
     * @var int|false
     */
    private $_deliveryType = false;

    /**
     * @var int
     */
    private $_price = 0.0;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['deliveryType', 'in', 'range' => DeliveryCountryGroups::instance()->getDelivery()->getList(true)],
            ['trackcode', 'validateTrackcode'],
            ['order', 'validateOrder'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'trackcode' => Translator::t('Tracking number'),
            'order' => Translator::t('Order'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->order) && empty($this->trackcode)) {
            $this->addError('trackcode', Translator::t('Tracking number/order No. required!'));
        }

        return parent::beforeValidate();
    }

    /**
     * дополнительные проверки
     */
    public function afterValidate()
    {
        parent::afterValidate();

        $deliveryType = Cast::toUInt($this->deliveryType);

        if (!$this->hasErrors()) {
            if ($deliveryType !== $this->_deliveryType) {
                $this->addError('order', Translator::t('Delivery type in order and in Act doesn\'t match'));
            }

            if (!$this->_order) {
                $this->addError('order', Translator::t('No order found'));
            } elseif ($this->_order->isCanceled()) {
                $this->addError('order', Translator::t("Order No.") . $this->_order->id . " " . Translator::t("canceled!"));
            }

            /** @var MoneyReceiveStatementOrderRecord $model */
            $model = MoneyReceiveStatementOrderRecord::model()->orderId($this->getOrderId())->find();
            if ($model) {
                $this->addError('order', Translator::t("Order No.") . $model->order_id .
                    Translator::t("is already registered in Act No.") .
                    $model->statement_id);
            }
        }
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function validateTrackcode($attribute, $param)
    {
        $trackcode = Utf8::trim($this->{$attribute});
        $order = 0;
        $deliveryType = $this->_deliveryType;
        $price = $this->_price;

        if ($trackcode) {
            /** @var MoneyControlRecord $trackcodeMoneyControl */
            $trackcodeMoneyControl = MoneyControlRecord::model()->trackcode($trackcode)->find();

            if ($trackcodeMoneyControl) {
                $orderId = $trackcodeMoneyControl->order_id;
                $deliveryType = Cast::toUInt($trackcodeMoneyControl->delivery_type);
                $price = Cast::toFloat($trackcodeMoneyControl->order_price);
                $order = OrderRecord::model()->findByPk($orderId);
            } else {
                /** @var OrderRecord $trackcodeOrder */
                $trackcodeOrder = OrderRecord::model()->trackcode($trackcode)->find();
                if ($trackcodeOrder) {
                    $order = $trackcodeOrder;
                    $deliveryType = Cast::toUInt($trackcodeOrder->delivery_type);
                    $price = Cast::toFloat($trackcodeOrder->getPayPriceHistory());
                } else {
                    $this->addError($attribute, Translator::t('No order with this tracking number is found!'));
                }
            }
        }

        $this->_deliveryType = $deliveryType;
        $this->_order = $order;
        $this->_price = $price;
    }

    /**
     * @param $attribute
     * @param $param
     */
    public function validateOrder($attribute, $param)
    {
        if ($this->_order) {
            return;
        }

        $order = Cast::toUInt($this->{$attribute});

        if ($order) {
            /** @var OrderRecord $model */
            $model = OrderRecord::model()->findByPk($order);

            if ($model === null) {
                $this->addError($attribute, Translator::t('No order found!'));
                return;
            }

            $this->_deliveryType = Cast::toUInt($model->delivery_type);
            $this->_order = $model;
            $this->_price = $model->getPayPriceHistory();
        }
    }

    /**
     * @return OrderRecord|null
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->_order ? $this->_order->id : 0;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->_price;
    }
}
