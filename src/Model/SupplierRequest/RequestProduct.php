<?php

namespace MommyCom\Model\SupplierRequest;

use CModel;
use CVarDumper;
use MommyCom\Model\Assortment\Exception\BrandNotFoundException;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\SupplierRequest\Exception\HashMismatchException;
use MommyCom\Model\SupplierRequest\Exception\InvalidColumnCountRequestException;
use MommyCom\Model\SupplierRequest\Exception\ProductNotFoundException;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

class RequestProduct extends CModel
{
    const LINE_COLUMN_NUMBER = 38;
    const CHECKSUM_PRIVATE_KEY = '286f5933110bbbde9f73';

    public $requestNum;
    public $lineNum;
    public $lineColumnCount;

    public $id;
    public $supplier;
    public $article;
    public $barcode;
    public $internalCode;

    public $name;
    public $category;
    public $brand;
    public $color;
    public $colorCode;

    public $sizeEu;
    public $sizeUs;
    public $sizeInt;
    public $sizeRu;
    public $sizeChn;
    public $sizeUk;
    public $sizeFr;
    public $sizeIt;
    public $sizeJp;
    public $sizeHeight;
    public $sizeHeadWear;
    public $sizeGloves;

    public $weight;

    public $dimensions;

    public $age;

    public $pricePurchase;

    public $priceSell;

    /**
     * Количество товара предполагаемое к продаже
     *
     * @var int
     */
    public $number = 0;

    /**
     * @var int
     */
    public $numberSold = 0;

    /**
     * Продано с учетом стока
     *
     * @var int
     */
    public $numberSoldReal = 0;

    /**
     * Количество товара уже поставлено
     *
     * @var int
     */
    public $numberCurrentlySupplied = 0;

    /**
     * Количество товара заказанно
     *
     * @var int
     */
    public $numberCurrentlyRequested = 0;

    /**
     * Реальное количество товара к поставке
     *
     * @var int
     */
    public $numberRequest = 0;

    /**
     * Сумма на которую будет заказ
     *
     * @var float
     */
    public $requestPurchasePrice = 0;

    /**
     * Количество товара которое будет поставлено
     *
     * @var int
     */
    public $numberSupplied = 0;

    /**
     * Ссылка на изображение
     *
     * @var int
     */
    public $logoUrl = '';

    /**
     * @var RequestTable
     */
    private $_requestTable;

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return [
            'lineNum',

            'id',
            'supplier',
            'article',
            'barcode',
            'internalCode',
            'name',
            'category',
            'brand',
            'color',
            'colorCode',

            'sizeEu',
            'sizeUs',
            'sizeInt',
            'sizeRu',
            'sizeChn',
            'sizeUk',
            'sizeFr',
            'sizeIt',
            'sizeJp',
            'sizeHeight',
            'sizeHeadWear',
            'sizeGloves',

            'weight',
            'dimensions',
            'age',

            'pricePurchase',
            'priceSell',

            'number',
            'numberSold',
            'numberSoldReal',
            'numberCurrentlySupplied',
            'numberCurrentlyRequested',
            'numberRequest',
            'numberSupplied',

            'logoUrl',
        ];
    }

    public function attributeLabels()
    {
        return [
            'lineNum' => '№',
            'hash' => 'Контроль',
            'id' => 'Код',
            'supplier' => 'Поставщик',
            'article' => 'Артикул',
            'barcode' => 'Штрихкод',
            'internalCode' => 'Внутренний код поставщика',

            'name' => 'Название',
            'category' => 'Категория',
            'brand' => 'Бренд',
            'color' => 'Цвет',
            'colorCode' => 'Код цвета',

            'sizeEu' => 'Eu',
            'sizeUs' => 'Us',
            'sizeInt' => 'Int',
            'sizeRu' => 'Ru/Ua',
            'sizeChn' => 'Chn',
            'sizeUk' => 'Uk',
            'sizeFr' => 'Fr',
            'sizeIt' => 'It',
            'sizeJp' => 'Jp',
            'sizeHeight' => 'Рост',
            'sizeHeadWear' => 'Головной убор',
            'sizeGloves' => 'Перчатки',

            'weight' => 'Вес',

            'dimensions' => 'Габариты',

            'age' => 'Возраст',

            'pricePurchase' => 'Стоимость',

            'priceSell' => 'Стоимость продажи',

            'number' => 'Вирт',
            'numberSoldReal' => 'Продано (реал)',
            'numberSold' => 'Продано',
            'numberCurrentlyRequested' => 'Уже запрошено',
            'numberCurrentlySupplied' => 'Уже поставлено',
            'numberRequest' => 'Запрос',
            'requestPurchasePrice' => 'Сумма запроса',
            'numberSupplied' => 'Выполнено',

            'logoUrl' => 'Изображение',
        ];
    }

    protected function __construct()
    {
    }

    public function rules()
    {
        return [
            ['lineNum, id, supplier, article, brand, number, hash, logoUrl', 'required'],
            ['lineNum, id', 'numerical', 'integerOnly' => true],
            ['logoUrl', 'url'],
            ['pricePurchase, priceSell', 'required'],
            ['number, numberSold, numberSoldReal, numberCurrentlySupplied, numberCurrentlyRequested, numberRequest, numberSupplied', 'numerical', 'integerOnly' => true, 'min' => 0],

            ['id', 'exist', 'className' => 'MommyCom\Model\Db\EventProductRecord', 'attributeName' => 'id'],
            ['id', 'extendedProductEventValidator'],
            ['numberCurrentlySupplied', 'validatorCurrentlySupplied'],
            ['numberCurrentlyRequested', 'validatorCurrentlyRequested'],
        ];
    }

    /**
     * @return RequestTable
     */
    public function getRequestTable()
    {
        return $this->_requestTable;
    }

    public function setRequestTable($table)
    {
        $this->_requestTable = $table;
    }

    public function extendedProductEventValidator($attribute)
    {
        $value = $this->$attribute;
        $product = $this->getModel();
        /* @var $product EventProductRecord */
        if ($product !== null && $product->event->id !== $this->getRequestTable()->event->id) {
            $this->addError($attribute, "Товар не из этой акции");
        }
    }

    /**
     * @param $attribute
     */
    public function validatorCurrentlySupplied($attribute)
    {
        $value = Cast::toUInt($this->$attribute);
        $product = $this->getModel();
        /* @var $product EventProductRecord */

        $supplied = WarehouseProductRecord::model()
            ->eventProductId($product->id)
            ->isReturn(false)
            ->statusIn([
                WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                WarehouseProductRecord::STATUS_MAILED_TO_CLIENT,
                WarehouseProductRecord::STATUS_BROKEN,
            ])->count();

        if ($value != $supplied) {
            $this->addError($attribute, "Количество уже поставленного не соотвествует указанному в таблице");
        }
    }

    /**
     * @param $attribute
     */
    public function validatorCurrentlyRequested($attribute)
    {
        $value = Cast::toUInt($this->$attribute);
        $product = $this->getModel();
        /* @var $product EventProductRecord */

        $requested = WarehouseProductRecord::model()
            ->eventProductId($product->id)
            ->isReturn(false)
            ->count();

        if ($value != $requested) {
            $this->addError($attribute, "Количество уже запрошенного не соотвествует указанному в таблице");
        }
    }

    /**
     * @param RequestTable $requestTable
     * @param integer $lineNum
     * @param EventProductRecord $product
     *
     * @return RequestProduct
     */
    public static function fromModel(RequestTable $requestTable, $lineNum, EventProductRecord $product)
    {
        $model = new self;

        $model->setRequestTable($requestTable);
        $model->requestNum = $requestTable->timestamp;
        $model->lineNum = $lineNum;

        $model->id = $product->id;
        $model->supplier = $product->supplier->name;
        $model->article = $product->article;
        $model->barcode = $product->barcode;
        $model->internalCode = $product->internal_code;

        $model->name = $product->name;
        $model->category = $product->category;
        $model->brand = $product->brand->name;
        $model->color = $product->color;
        $model->colorCode = $product->color_code;

        $sizes = $product->getSizes(true);

        $model->sizeEu = isset($sizes['eu']) ? $sizes['eu'] : '';
        $model->sizeUs = isset($sizes['us']) ? $sizes['us'] : '';
        $model->sizeInt = isset($sizes['int']) ? $sizes['int'] : '';
        $model->sizeRu = isset($sizes['ru']) ? $sizes['ru'] : '';
        $model->sizeChn = isset($sizes['chn']) ? $sizes['chn'] : '';
        $model->sizeUk = isset($sizes['uk']) ? $sizes['uk'] : '';
        $model->sizeFr = isset($sizes['fr']) ? $sizes['fr'] : '';
        $model->sizeIt = isset($sizes['it']) ? $sizes['it'] : '';
        $model->sizeJp = isset($sizes['jp']) ? $sizes['jp'] : '';
        $model->sizeIt = isset($sizes['it']) ? $sizes['it'] : '';
        $model->sizeHeight = isset($sizes['height']) ? $sizes['height'] : '';
        $model->sizeHeadWear = isset($sizes['headwear']) ? $sizes['headwear'] : '';
        $model->sizeGloves = isset($sizes['gloves']) ? $sizes['gloves'] : '';

        $model->weight = $product->weight;
        $model->dimensions = $product->dimensions;
        $model->age = $product->ageRangeReplacement;

        $model->pricePurchase = (int)$product->price_purchase;
        $model->priceSell = (int)$product->price;

        $model->number = $product->number;
        $model->numberSold = $product->getNumberSold();
        $model->numberSoldReal = $product->getNumberSoldReal();
        //$model->numberCurrentlySupplied = $product->number_arrived;
        $model->numberCurrentlyRequested = WarehouseProductRecord::model()
            ->eventProductId($product->id)
            ->isReturn(false)
            ->count();
        $model->numberCurrentlySupplied = WarehouseProductRecord::model()
            ->eventProductId($product->id)
            ->isReturn(false)
            ->statusIn([
                WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                WarehouseProductRecord::STATUS_MAILED_TO_CLIENT,
                WarehouseProductRecord::STATUS_BROKEN,
            ])->count();
        $model->numberRequest = max($model->numberSold - $model->numberCurrentlyRequested, 0);
        $model->requestPurchasePrice = (int)$model->numberRequest * (int)$product->price_purchase;
        $model->logoUrl = $product->product->logo->getUrl();

        return $model;
    }

    /**
     * @param RequestTable $requestTable
     * @param array $lineData
     *
     * @throws HashMismatchException
     * @throws ProductNotFoundException
     * @throws BrandNotFoundException
     * @throws InvalidColumnCountRequestException
     * @return RequestProduct
     */
    public static function fromLines(RequestTable $requestTable, array $lineData)
    {
        // I like magic offsets

        $model = new self;

        $model->lineColumnCount = count($lineData);
        if ($model->lineColumnCount !== self::LINE_COLUMN_NUMBER) {
            throw new InvalidColumnCountRequestException(
                "Ошибка, строка содержит {$model->lineColumnCount} колонок а должна " . self::LINE_COLUMN_NUMBER . ', содержимое: ' .
                CVarDumper::dumpAsString($lineData)
            );
        }

        $model->setRequestTable($requestTable);
        $model->requestNum = $requestTable->timestamp;
        $model->lineNum = $lineData[0];

        $hash = $lineData[1];
        sscanf($hash, '%u', $hash);

        $model->id = $lineData[2];
        $model->supplier = $lineData[3];
        $model->article = $lineData[4];
        $model->barcode = $lineData[5];
        $model->internalCode = $lineData[6];

        $model->name = $lineData[7];
        $model->category = $lineData[8];
        $model->brand = $lineData[9];
        $model->color = $lineData[10];
        $model->colorCode = $lineData[11];

        $model->sizeEu = $lineData[12];
        $model->sizeUs = $lineData[13];
        $model->sizeInt = $lineData[14];
        $model->sizeRu = $lineData[15];
        $model->sizeChn = $lineData[16];
        $model->sizeUk = $lineData[17];
        $model->sizeFr = $lineData[18];
        $model->sizeIt = $lineData[19];
        $model->sizeJp = $lineData[20];
        $model->sizeHeight = $lineData[21];
        $model->sizeHeadWear = $lineData[22];
        $model->sizeGloves = $lineData[23];

        $model->weight = $lineData[24];
        $model->dimensions = $lineData[25];
        $model->age = $lineData[26];

        $model->pricePurchase = $lineData[27];
        $model->priceSell = $lineData[28];

        $model->number = $lineData[29];
        $model->numberSoldReal = $lineData[30];
        $model->numberSold = $lineData[31];
        $model->numberCurrentlyRequested = $lineData[32];
        $model->numberCurrentlySupplied = $lineData[33];
        $model->numberRequest = $lineData[34];
        $model->requestPurchasePrice = $lineData[35];
        $model->numberSupplied = $lineData[36];

        $model->logoUrl = $lineData[37];

        if ($model->getHash() != $hash) {
            throw new HashMismatchException("Контроль '$hash' не соответствует сожидаемому");
        }

        return $model;
    }

    /**
     * @return array
     */
    public function getLineData()
    {
        return [
            $this->lineNum,
            $this->getHash(),
            $this->id,
            $this->supplier,
            $this->article,
            $this->barcode,
            $this->internalCode,
            $this->name,
            $this->category,
            $this->brand,
            $this->color,
            $this->colorCode,
            $this->sizeEu,
            $this->sizeUs,
            $this->sizeInt,
            $this->sizeRu,
            $this->sizeChn,
            $this->sizeUk,
            $this->sizeFr,
            $this->sizeIt,
            $this->sizeJp,
            $this->sizeHeight,
            $this->sizeHeadWear,
            $this->sizeGloves,
            $this->weight,
            $this->dimensions,
            $this->age,
            $this->pricePurchase,
            $this->priceSell,
            $this->number,
            $this->numberSoldReal,
            $this->numberSold,
            $this->numberCurrentlyRequested,
            $this->numberCurrentlySupplied,
            $this->numberRequest,
            $this->requestPurchasePrice,
            $this->numberSupplied,
            $this->logoUrl,
        ];
    }

    /**
     * Хэш строки
     *
     * @return string
     */
    public function getHash()
    {
        $attrNames = [
            'id', 'supplier', 'article', 'category', 'brand', 'color', 'colorCode', 'priceSell', 'pricePurchase',

            'sizeEu', 'sizeUs', 'sizeInt',
            'sizeRu', 'sizeChn', 'sizeUk',
            'sizeFr', 'sizeIt', 'sizeJp',
            'sizeHeight', 'sizeHeadWear',
            'sizeGloves',

            'logoUrl',
        ];

        $attributes = $this->getAttributes($attrNames);
        $attributes['priceSell'] = Cast::toUFloat($attributes['priceSell']);
        $attributes['pricePurchase'] = Cast::toUFloat($attributes['pricePurchase']);

        $attributes = array_map([Cast::class, 'toStr'], $attributes);
        $attributes = array_map([Utf8::class, 'trim'], $attributes);

        $hash = hash('crc32b', self::CHECKSUM_PRIVATE_KEY . "\n" . implode("\n", $attributes), true);
        return sprintf("%u", unpack('L', $hash)[1]);
    }

    /**
     * @return EventProductRecord
     */
    public function getModel()
    {
        return EventProductRecord::model()->findByPk($this->id);
    }

    /**
     * @return WarehouseProductRecord[]
     */
    public function convertToWarehouseProducts()
    {
        $result = [];
        $supplied = Cast::toUInt($this->numberSupplied);

        if ($supplied === 0) {
            return [];
        }

        $model = $this->getModel();
        $requestNum = $this->requestNum;

        $file = $this->getRequestTable()->file;
        $fileid = '';
        if ($fileid !== null) {
            $fileid = $file->getEncodedId();
        }

        for ($i = 0; $i < $supplied; $i++) {
            $product = new WarehouseProductRecord();

            $product->supplier_id = $model->supplier_id;
            $product->event_id = $model->event_id;
            $product->event_product_id = $model->id;
            $product->event_product_sizeformat = $model->sizeformat;
            $product->event_product_size = $model->size;
            $product->event_product_price = $model->price;
            $product->product_id = $model->product_id;
            $product->requestnum = $requestNum;
            $product->request_fileid = $fileid;

            /* @var $orderProducts OrderProductRecord[] */

            $result[] = $product;
        }

        return $result;
    }
}
