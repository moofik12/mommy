<?php

namespace MommyCom\Model\SupplierRequest;

use CUploadedFile;
use MommyCom\Model\Assortment\AssortmentTable;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class RequestUploadForm
 */
class RequestUploadForm extends \CFormModel
{
    /**
     * @var CUploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            ['file', 'required'],

            ['file', 'file', 'types' => 'csv', 'minSize' => 10, 'maxSize' => 20 * 1024 * 1024, 'allowEmpty' => false],
            ['file', 'extendedFileValidation'],
        ];
    }

    public function extendedFileValidation($attribute)
    {
        $value = $this->$attribute;
        /* @var $value CUploadedFile */
        if (AssortmentTable::checkFile($value->tempName) !== AssortmentTable::CHECKFILE_OK) {
            $this->addError($attribute, Translator::t('File type not supported at') . ' ' . $attribute);
        }
    }

    public function attributeLabels()
    {
        return [
            'file' => Translator::t('File'),
            'eventId' => Translator::t('Flash-sale'),
        ];
    }

    public function getConfig()
    {
        return [
            'showErrorSummary' => true,
            'enctype' => 'multipart/form-data',
            'elements' => [
                'file' => [
                    'type' => 'file',
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t(    'Save'),
                ],
            ],
        ];
    }

    public function getRequestTable()
    {
        $table = RequestTable::fromFile($this->file->tempName);
        return $table;
    }
}
