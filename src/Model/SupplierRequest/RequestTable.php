<?php

namespace MommyCom\Model\SupplierRequest;

use CMap;
use finfo;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class RequestTable
 * @method RequestProduct[] toArray()
 */
class RequestTable extends CMap
{
    const CHECKSUM_PRIVATE_KEY = 'j192hjaksdh89@f;kg12asd';

    const CHECKFILE_OK = '';
    const CHECKFILE_INVALID_MIME = 'mime';
    const CHECKFILE_INVALID_ENCODING = 'encoding';
    const CHECKFILE_INVALID_CSV = 'csv';
    const CHECKFILE_INVALID_DATA = 'data_error';
    const CHECKFILE_INVALID_CHECKSUM = 'checksum';

    const IMPORT_OK = '';
    const IMPORT_ALREADY_IMPORTED = 'already_imported';

    /**
     * @var EventRecord
     */
    public $event;

    public $timestamp;

    /**
     * @var \MommyCom\YiiComponent\FileStorage\File|null
     */
    public $file = null;

    protected $_errors = [];

    /**
     * @param string $filename
     *
     * @return string
     */
    public static function checkFile($filename)
    {
        $finfo = new finfo();

        if (strpos($finfo->file($filename, FILEINFO_MIME_TYPE), 'text/') !== 0) {
            return self::CHECKFILE_INVALID_MIME;
        }

        if ($finfo->file($filename, FILEINFO_MIME_ENCODING) != 'utf-8') {
            return self::CHECKFILE_INVALID_ENCODING;
        }

        return self::CHECKFILE_OK;
    }

    public function __construct()
    {
        $this->timestamp = time();
    }

    /**
     * @param string $filename
     *
     * @return RequestTable
     */
    public static function fromFile($filename)
    {
        $instance = new static();
        /* @var $instance RequestTable */

        if ($instance->checkFile($filename) !== self::CHECKFILE_OK) {
            return $instance; // just exit
        }

        $file = Yii::app()->filestorage->factoryFromLocalFile($filename);

        if ($file instanceof File) {
            $instance->file = $file;
        }

        $csv = CsvFile::load($filename)->toArray();

        $timestamp = $csv[3][1]; // Маркер времени
        $hash = $csv[4][1]; // Контроль
        $eventId = $csv[7][1]; // Акция

        $productHashes = [];

        $content = array_slice($csv, 19); // 19 is content offset (without header)
        foreach ($content as &$lineData) {
            $lineData = Utf8::trimArr($lineData);

            $product = RequestProduct::fromLines($instance, $lineData);
            $instance[] = $product;
            $productHashes[] = $product->getHash();
        }
        unset($lineData);

        $instance->event = EventRecord::model()->findByPk($eventId);

        if ($hash != $instance->_generateHash($timestamp, $eventId, $productHashes)) {
            $instance->addError('hash', 'Контроль всей таблицы не соответствует ожидаемому');
            $instance->clear();
        }

        unset($productHashes);

        return $instance;
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function addError($attribute, $message)
    {
        if (!isset($this->_errors[$attribute])) {
            $this->_errors[$attribute] = [];
        }
        $this->_errors[$attribute][] = $message;
    }

    public function clearErrors()
    {
        $this->_errors = [];
    }

    /**
     * @param EventRecord $event
     * @param integer $supplierId
     * @param bool $addZeroSoldItems
     *
     * @return RequestTable
     */
    public static function fromEvent(EventRecord $event, $supplierId = 0, $addZeroSoldItems = true)
    {
        $instance = new static();
        /* @var $instance RequestTable */

        $lineNum = 1;
        foreach ($event->products as $item) {
            if ($supplierId > 0 && $item->supplier_id != $supplierId) {
                continue;
            }

            if (!$addZeroSoldItems && ($item->getNumberSold() == 0 && $item->number_arrived == 0)) {
                continue;
            }
            $instance[] = RequestProduct::fromModel($instance, $lineNum++, $item);
        }

        $instance->event = $event;
        $instance->setReadOnly(true);

        return $instance;
    }

    /**
     * @param $timestamp
     * @param $eventId
     * @param array $productHashes
     *
     * @return string
     * @todo создать из этого статического метода свойство
     */
    protected function _generateHash($timestamp, $eventId, array $productHashes)
    {
        $timestamp = Cast::toUInt($timestamp);
        $eventId = Cast::toUInt($eventId);

        $productHashes = Cast::toStrArr($productHashes);

        $raw = json_encode([$timestamp, $eventId, $productHashes]);

        $hash = hash('crc32b', self::CHECKSUM_PRIVATE_KEY . '_' . $raw, true);
        return sprintf("%u", unpack('L', $hash)[1]);
    }

    /**
     * @return string
     */
    public function toCsvString()
    {
        $products = $this->toArray();
        uasort($products, function ($model, $modelPrev) {
            /* @var RequestProduct $model */
            /* @var RequestProduct $modelPrev */
            return strncasecmp($model->article, $modelPrev->article, 4); //2 байта рус. буква
        });

        $timestamp = time();

        $productHashes = [];
        foreach ($products as $product) {
            $productHashes[] = $product->getHash();
        }

        $dateFormatter = Yii::app()->getDateFormatter();

        // content
        $contentData = [];

        $contentHeader = [];
        foreach ($products as $item) {
            $contentData[] = $item->getLineData();
            if (empty($contentHeader)) {
                $contentHeader = $item->attributeLabels();
            }
        }

        $contentData = array_merge([$contentHeader], $contentData);

        // header

        $event = $this->event;
        $totalHash = $this->_generateHash($timestamp, $event->id, $productHashes);

        $headerData = [];

        $eventAttrs = ['id', 'name', 'startAtDateString', 'endAtDateString', 'mailingStartAtDateString'];

        $headerData[] = ['Таблица заказа', $dateFormatter->formatDateTime($timestamp)];
        $headerData[] = []; // empty line
        $headerData[] = ['Служебная информация'];
        $headerData[] = ['Маркер времени', $timestamp];
        $headerData[] = ['Контроль', $totalHash];
        $headerData[] = []; // empty line
        $headerData[] = ['Информация об акции'];

        foreach ($eventAttrs as $name) {
            $label = $event->getAttributeLabel($name);
            $value = ArrayUtils::getValueByPath($event, $name);

            $headerData[] = [
                $label, $value,
            ];
        }

        $totalPurchased = 0.0;
        $totalRequested = 0.0;
        $totalForPurchase = 0.0;
        foreach ($products as $product) {
            $totalPurchased += Cast::smartStringToUFloat($product->pricePurchase) * $product->numberCurrentlySupplied;
            $totalRequested += Cast::smartStringToUFloat($product->pricePurchase) * $product->numberCurrentlyRequested;
            $totalForPurchase += Cast::smartStringToUFloat($product->pricePurchase) * $product->numberRequest;
        }

        $headerData[] = []; // empty line

        $headerData[] = ['Закуплено на сумму', $totalPurchased];
        $headerData[] = ['Заказано на сумму', $totalRequested];
        $headerData[] = ['Для закупки', $totalForPurchase];

        $headerData[] = []; // empty line
        $headerData[] = []; // empty line

        return CsvFile::loadFromArray(array_merge($headerData, $contentData))->toString();
    }

    /**
     * @param $filename
     */
    public function toFile($filename)
    {
        file_put_contents($filename, $this->toCsvString());
    }
}
