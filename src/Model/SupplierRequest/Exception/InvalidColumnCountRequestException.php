<?php

namespace MommyCom\Model\SupplierRequest\Exception;

class InvalidColumnCountRequestException extends RequestProductException
{
}
