<?php

namespace MommyCom\Model\SupplierRequest\Exception;

class ProductNotFoundException extends RequestProductException
{
}
