<?php

namespace MommyCom\Model\SupplierRequest\Exception;

class BrandNotFoundException extends RequestProductException
{
}
