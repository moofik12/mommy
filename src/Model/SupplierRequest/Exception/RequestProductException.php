<?php

namespace MommyCom\Model\SupplierRequest\Exception;

use CException;

class RequestProductException extends CException
{
}
