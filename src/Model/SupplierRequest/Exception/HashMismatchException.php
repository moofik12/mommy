<?php

namespace MommyCom\Model\SupplierRequest\Exception;

class HashMismatchException extends RequestProductException
{
}
