<?php

namespace MommyCom\Model\PayGateway;

/**
 * Class PayGatewayReceiveResult
 *
 * @deprecated
 */
class PayGatewayReceiveResult
{
    const RECEIVE_STATUS_OK = 0; // все хорошо, платеж отправлен
    const RECEIVE_STATUS_ERROR_UNKNOWN = 1; // ошибка, причина не известна
    const RECEIVE_STATUS_ERROR_SERVICE_UNAVAILABLE = 2; // ошибка, сервис не доступен
    const RECEIVE_STATUS_ERROR_NOT_ENOUGH_MONEY = 3; // ошибка, недостаточно средств
    const RECEIVE_STATUS_ERROR_SERVICE_SECURITY = 4; // ошибка, сервис отклонил оплату по каким-то причинам связанным с защитой
    const RECEIVE_STATUS_ERROR_SECURITY = 5; // ошибка, ответ с сервера не прошел защиту
    const RECEIVE_STATUS_ERROR_DATA = 6; // сервис не предоставил данные для обработки
    const RECEIVE_STATUS_ERROR_RESPONSE_URL = 7; // ответ пришел из неразрешенного URL
    const RECEIVE_STATUS_ERROR_REJECTED = 8; // ошибка, платеж отклонен
    const RECEIVE_STATUS_WAIT = 9; // сервис перевел обработку в ожидание по каким-то причинам
    const RECEIVE_STATUS_FORMAT_ERROR = 10; // Ошибка на стороне мерчанта — неверно сформирована транзакция

    public $invoiceId;
    public $amount = 0.0;
    public $status;
    public $uniquePayment;
    public $description;
    public $currency;

    /**
     * @var mixed
     */
    public $raw = null;

    /**
     * @param string $default
     *
     * @return string
     */
    public function statusText($default = '')
    {
        $replacements = static::statusReplacements();

        return isset($replacements[$this->status]) ? $replacements[$this->status] : $default;
    }

    /**
     * @return array
     */
    public static function statusReplacements()
    {
        return [
            self::RECEIVE_STATUS_OK => 'платеж прошел успешно',
            self::RECEIVE_STATUS_ERROR_UNKNOWN => 'ошибка, причина не известна',
            self::RECEIVE_STATUS_ERROR_SERVICE_UNAVAILABLE => 'ошибка, сервис не доступен',
            self::RECEIVE_STATUS_ERROR_NOT_ENOUGH_MONEY => 'ошибка, недостаточно средств',
            self::RECEIVE_STATUS_ERROR_SERVICE_SECURITY => 'ошибка, сервис отклонил оплату по каким-то причинам связанным с защитой',
            self::RECEIVE_STATUS_ERROR_SECURITY => 'ошибка, ответ с сервера не прошел внутреннюю защиту',
            self::RECEIVE_STATUS_ERROR_DATA => 'сервис не предоставил данные для обработки',
            self::RECEIVE_STATUS_ERROR_RESPONSE_URL => 'ответ пришел из неразрешенного Url',
            self::RECEIVE_STATUS_WAIT => 'сервис перевел обработку в ожидание по каким-то причинам',
            self::RECEIVE_STATUS_ERROR_REJECTED => 'ошибка, платеж отклонен',
            self::RECEIVE_STATUS_FORMAT_ERROR => 'ошибка, неверно сформирована транзакция',
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::RECEIVE_STATUS_OK,
            self::RECEIVE_STATUS_ERROR_UNKNOWN,
            self::RECEIVE_STATUS_ERROR_SERVICE_UNAVAILABLE,
            self::RECEIVE_STATUS_ERROR_NOT_ENOUGH_MONEY,
            self::RECEIVE_STATUS_ERROR_SERVICE_SECURITY,
            self::RECEIVE_STATUS_ERROR_SECURITY,
            self::RECEIVE_STATUS_ERROR_DATA,
            self::RECEIVE_STATUS_ERROR_RESPONSE_URL,
            self::RECEIVE_STATUS_WAIT,
            self::RECEIVE_STATUS_ERROR_REJECTED,
            self::RECEIVE_STATUS_FORMAT_ERROR,
        ];
    }
} 
