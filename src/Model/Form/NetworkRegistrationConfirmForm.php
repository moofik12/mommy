<?php

namespace MommyCom\Model\Form;

use CFormModel;
use MommyCom\YiiComponent\AuthManager\UserIdentity;

/**
 * Class NetworkRegistrationConfirmForm
 */
class NetworkRegistrationConfirmForm extends CFormModel
{
    public $email;
    public $password;
    public $remember = true;
    private $_identity;

    public function rules()
    {
        return [
            ['email, password', 'required'],

            ['email', 'email', 'message' => \Yii::t('common', 'E-mail is not valid')],
            ['password', 'authenticate'],
        ];
    }

    public function authenticate()
    {
        $result = false;
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', $this->_identity->errorMessage);
            }

            $result = true;
        }

        return $result;
    }

    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('common', 'Your E-mail'),
            'password' => \Yii::t('common', 'Your password'),
        ];
    }

    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->remember ? \Yii::app()->params['remember'] : 0;
            \Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        return false;
    }

}
