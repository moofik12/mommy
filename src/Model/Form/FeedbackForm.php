<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class FeedbackForm
 * Форма обратной связи
 */
class FeedbackForm extends CFormModel
{
    /**
     * @var string
     */
    public $message;

    public function rules()
    {
        return [
            ['message', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'message' => \Yii::t('common', 'Текст обращения'),
        ];
    }

}
