<?php

namespace MommyCom\Model\Form;

use CFormModel;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\Facade\Translator;

class RegistrationOfferForm extends CFormModel
{
    const MIN_BENEFICE_RANDOM = 50;
    const MAX_BENEFICE_RANDOM = 100;

    const BENEFICE_0 = 'none';
    const BENEFICE_30 = 'standard';
    const BENEFICE_35 = 'standard-plus';
    const BENEFICE_40 = 'standard-perfect';
    const BENEFICE_50 = 'comfort';
    const BENEFICE_100 = 'max';
    const BENEFICE_RANDOM = 'random';

    const BENEFICE_PERCENT_10 = 'percent_full';

    const STRATEGY_BONUSES = 'bonuses';
    const STRATEGY_PROMOCODE = 'promocode';

    const STRATEGY_PAYMENTS_MONEY = 'money';
    const STRATEGY_PAYMENTS_PERCENT = 'percent';

    const PROMOCODE_PAYMENTS_MONEY_AFTER_ORDER_AMOUNT = 400;

    const SECRET_CODES = ['201016', '022817'];
    const SECRET_CODE_NOT_PRESENT = 'not_present';

    const LIMIT_REGISTRATION_FROM_IP = 3;
    const LIMIT_TIME_REGISTRATION_FROM_IP = 3600; //1 hour

    /**
     * Стратегия куда именно начислять бонус
     *
     * @var string
     */
    public $strategy = self::STRATEGY_BONUSES;

    /**
     * Стратегия получения бонуса
     *
     * @var string
     */
    public $strategyPayments = self::STRATEGY_PAYMENTS_MONEY;

    /**
     * @var string
     */
    public $email;

    /**
     * @var bool
     */
    public $isConfirmPrivacy = true;

    /**
     * @var
     */
    public $benefice = self::BENEFICE_0;

    /**
     * @var string
     */
    public $secretCode = '';

    /**
     * Имя баннера при регистрации
     *
     * @var string
     */
    public $bannerName = '';

    public function rules()
    {
        $tr = Translator::get();

        return [
            ['email, isConfirmPrivacy', 'required'],
            ['isConfirmPrivacy', 'boolean'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email', 'message' => $tr('E-mail is not valid')],
            ['email', 'length', 'max' => 128, 'tooLong' => $tr('128 character limit')],

            ['strategy', 'in', 'range' => [
                self::STRATEGY_BONUSES,
                self::STRATEGY_PROMOCODE,
            ]],

            ['strategyPayments', 'in', 'range' => [
                self::STRATEGY_PAYMENTS_MONEY,
                self::STRATEGY_PAYMENTS_PERCENT,
            ]],

            ['benefice', 'in', 'range' => [
                self::BENEFICE_0,
                self::BENEFICE_30,
                self::BENEFICE_35,
                self::BENEFICE_40,
                self::BENEFICE_50,
                self::BENEFICE_100,
                self::BENEFICE_RANDOM,

                self::BENEFICE_PERCENT_10,
            ]],

            ['email', 'unique', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'email', 'message' => $tr('E-mail is already in use')],
            ['email', 'validateEmail'],
            ['secretCode', 'length', 'max' => 14],
            ['secretCode', 'validateSecretCode'],

            ['bannerName', 'length', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        $tr = Translator::get();

        return [
            'email' => $tr('E-Mail'),
            'isConfirmPrivacy' => $tr('Согласен с правилами'),
            'secretCode' => \Yii::t('skip', 'Секретный код с флаера'),
        ];
    }

    public function validateEmail($attribute)
    {
        $remoteIp = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
        if ($remoteIp == false) {
            $this->addError($attribute, Translator::t('Не найден ip пользователя'));
        }

        if (self::LIMIT_REGISTRATION_FROM_IP > 0 && self::LIMIT_TIME_REGISTRATION_FROM_IP > 0) {
            $timeForm = time() - self::LIMIT_TIME_REGISTRATION_FROM_IP;
            $countUsersRegisteredFromIp = UserRecord::model()->createdAtFrom($timeForm)->ipIs($remoteIp)->count();
            if ($countUsersRegisteredFromIp > self::LIMIT_REGISTRATION_FROM_IP) {
                $this->addError($attribute, Translator::t('Превышен лимит регистраций с Вашего ip'));
            }
        }
    }

    public function validateSecretCode($attribute)
    {
        $secretCode = $this->$attribute;
        //пока только для рандомного бонуса
        if ($this->benefice != self::BENEFICE_RANDOM) {
            return;
        }

        if ($secretCode == self::SECRET_CODE_NOT_PRESENT) {
            return;
        }

        if (!in_array($secretCode, self::SECRET_CODES)) {
            $this->addError($attribute, Translator::t('Неверный секретный код'));
        }
    }

    /**
     * @return int
     */
    public function getBeneficeAmount()
    {
        $count = 0;

        switch ($this->strategyPayments) {
            case self::STRATEGY_PAYMENTS_MONEY:
                $count = $this->_amountBeneficeMoney();
                break;

            case self::STRATEGY_PAYMENTS_PERCENT:
                $count = $this->_amountBeneficePercent();
                break;

            default:
                break;
        }

        return $count;
    }

    /**
     * @return int
     */
    protected function _amountBeneficeMoney()
    {
        $amount = 0;

        switch ($this->benefice) {
            case self::BENEFICE_30:
                $amount = 30;
                break;

            case self::BENEFICE_35:
                $amount = 35;
                break;

            case self::BENEFICE_40:
                $amount = 40;
                break;

            case self::BENEFICE_50:
                $amount = 50;
                break;

            case self::BENEFICE_100:
                $amount = 100;
                break;

            case self::BENEFICE_RANDOM:
                $amount = $this->_getRandomBenefice();
                break;

            default:
                break;
        }

        return $amount;
    }

    /**
     * @return int
     */
    protected function _amountBeneficePercent()
    {
        $percent = 0;

        switch ($this->benefice) {
            case self::BENEFICE_PERCENT_10:
                $percent = 10;
                break;

            default:
                break;
        }

        return $percent;
    }

    /**
     * @param int $min
     * @param int $max
     * @param int $step
     *
     * @return int
     */
    private function _getRandomBenefice($min = self::MIN_BENEFICE_RANDOM, $max = self::MAX_BENEFICE_RANDOM, $step = 5)
    {
        $benefice = function_exists('random_int') ? random_int($min, $max) : mt_rand($min, $max);
        $beneficeRound = $benefice - $benefice % $step;

        return $beneficeRound < $min ? $min : $beneficeRound;
    }
}
