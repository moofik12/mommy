<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class GeneratorUrlForm
 */
class GeneratorUrlForm extends CFormModel
{
    /**
     * @var string
     */
    public $url;

    public function rules()
    {
        return [
            ['url', 'required'],
            ['url', 'url'],
            ['url', 'validDomainUrl'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'url' => \Yii::t('common', 'Ссылка'),
        ];
    }

    public function validDomainUrl($attr)
    {
        $url = $this->$attr;
        if (mb_strpos($url, $_SERVER['SERVER_NAME']) === false) {
            $this->addError($attr, \Yii::t('common', 'Не верная ссылка сайта'));
        }
    }

    /**
     * @param string $redirectToUrl
     *
     * @return string
     */
    public static function getPersonalInviteUrl($id, $redirectToUrl = '')
    {
        if ($redirectToUrl === '') {
            $personalUrl = \Yii::app()->createAbsoluteUrl('partner/invite', ['id' => $id]);
        } else {
            $personalUrl = \Yii::app()->createAbsoluteUrl('partner/invite', ['id' => $id, 'redirectTo' => base64_encode($redirectToUrl)]);
        }
        return $personalUrl;
    }

}
