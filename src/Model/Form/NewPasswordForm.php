<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class NewPasswordForm
 */
class NewPasswordForm extends CFormModel
{
    public $password;

    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'length', 'min' => 4, 'max' => 20,
                'tooShort' => \Yii::t('common', 'Пароль слишком короткий'), 'tooLong' => \Yii::t('common', '20 character limit')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => \Yii::t('common', 'Новый пароль'),
        ];
    }

}
