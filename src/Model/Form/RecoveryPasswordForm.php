<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class RecoveryPassword
 */
class RecoveryPasswordForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'email'
                , 'message' => \Yii::t('common', 'Your E-mail is not registered in the system')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('common', 'Your E-mail'),
        ];
    }

}
