<?php

namespace MommyCom\Model\Form;

use CFormModel;
use CValidator;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * Class UserSettingsForm
 */
class UserSettingsForm extends CFormModel
{
    public $password;
    public $newPassword;
    public $confirmPassword;
    public $isWishSystemMail = 1;
    public $isWishPromoMail = 1;
    public $distribution;

    public function rules()
    {
        return [
            ['password', 'validPassword'],
            ['newPassword', 'length', 'min' => 4, 'max' => 20,
                'tooShort' => \Yii::t('common', 'Минимум 4 символа'), 'tooLong' => \Yii::t('common', '60 character limit')],
            ['confirmPassword', 'compare', 'compareAttribute' => 'newPassword',
                'message' => \Yii::t('common', 'Пароль должен быть повторен в точности')],

            ['isWishSystemMail, isWishPromoMail', 'boolean'],
            ['distribution', 'numerical', 'integerOnly' => true],
            ['distribution', 'default', 'value' => UserDistributionRecord::TYPE_EVERY_DAY],
        ];
    }

    public function validPassword($attribute)
    {
        $value = $this->$attribute;
        if (empty($value)) {
            return;
        }

        $validator = CValidator::createValidator('required', $this, ['password', 'newPassword'], ['message' => \Yii::t('common', 'Обязательное поле')]);
        $validator->validate($this, ['password', 'newPassword']);
        $modelUser = \Yii::app()->user->getModel();
        if ($modelUser->password_hash !== UserRecord::encodePassword($this->password, $modelUser->password_salt)) {
            $this->addError('password', \Yii::t('common', 'Неверный пароль'));
        }
    }

    public function attributeLabels()
    {
        return [
            'password' => \Yii::t('common', 'Текущий пароль'),
            'newPassword' => \Yii::t('common', 'Новый пароль'),
            'confirmPassword' => \Yii::t('common', 'Подтвердите новый пароль'),
            'isWishSystemMail' => \Yii::t('common', 'Получать информационную рассылку'),
            'isWishPromoMail' => \Yii::t('common', 'Получать ежедневную рассылку товаров'),
            'distribution' => \Yii::t('common', 'Рассылка'),
        ];
    }
}
