<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class PrivatOutputForm
 * Форма для создания заявки на вывод
 */
class PrivatOutputForm extends CFormModel
{
    public $amount = 0;
    public $cardNumber;
    public $nameCard;
    public $surnameCard;

    public function attributeLabels()
    {
        return [
            'amount' => \Yii::t('common', 'Сумма вывода средств'),
            'cardNumber' => \Yii::t('common', 'Номер карты'),
            'nameCard' => \Yii::t('common', 'Ваше имя'),
            'surnameCard' => \Yii::t('common', 'Ваша фамилия'),
        ];
    }

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['cardNumber', 'required', 'message' => \Yii::t('common', 'Заполните Номер карты')],
            ['nameCard', 'required', 'message' => \Yii::t('common', 'Заполните Имя')],
            ['surnameCard', 'required', 'message' => \Yii::t('common', 'Заполните Фамилию')],

            ['amount', 'numerical', 'integerOnly' => true, 'min' => 1],

            ['nameCard', 'length', 'max' => 30],
            ['surnameCard', 'length', 'max' => 30],
            ['cardNumber', 'length', 'min' => 16, 'max' => 16,
                'tooShort' => \Yii::t('common', 'Недостаточное кол-во цифр в номере карты'), 'tooLong' => \Yii::t('common', 'Должно быть 16 цифр')],

            ['cardNumber', 'match', 'pattern' => '/^\d+$/', 'message' => \Yii::t('common', 'Только цифры!')],
            ['surnameCard', 'match', 'pattern' => '/^([А-яA-z -]+)$/iu', 'message' => \Yii::t('common', 'В фамилии недопустимые символы!')],
            ['nameCard', 'match', 'pattern' => '/^([А-яA-z -]+)$/iu', 'message' => \Yii::t('common', 'В имени недопустимые символы!')],
        ];
    }
}
