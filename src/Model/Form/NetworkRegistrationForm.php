<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class NetworkRegistrationForm
 */
class NetworkRegistrationForm extends CFormModel
{
    /**
     * @var string
     */
    public $email;

    public function rules()
    {
        return [
            ['email', 'required'],

            ['email', 'email', 'message' => \Yii::t('common', 'E-mail is not valid')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('common', 'Введите свою электронную почту'),
        ];
    }
}
