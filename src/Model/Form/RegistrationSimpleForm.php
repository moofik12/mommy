<?php

namespace MommyCom\Model\Form;

use CFormModel;
use MommyCom\YiiComponent\Facade\Translator;

class RegistrationSimpleForm extends CFormModel
{
    /**
     * @var string
     */
    public $email;

    public function rules()
    {
        $tr = Translator::get();

        return [
            ['email', 'required'],
            ['email', 'filter', 'filter' => 'trim'],

            ['email', 'email', 'message' => $tr('E-mail is not valid e-mail.')],
            ['email', 'length', 'max' => 128, 'tooLong' => $tr('128 character limit')],

            ['email', 'unique', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'email', 'message' => $tr('E-mail is already in use')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Translator::t('E-Mail'),
        ];
    }
}
