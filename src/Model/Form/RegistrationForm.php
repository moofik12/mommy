<?php

namespace MommyCom\Model\Form;

use CFormModel;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\Facade\Translator;

class RegistrationForm extends CFormModel
{
    use ApplicationTrait;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var boolean
     */
    public $agreeToTerms;

    /**
     * @var string
     */
    public $telephone;

    public function rules()
    {
        $tr = Translator::get();

        return [
            //обязательно
            ['email', 'required', 'message' => $tr('Email can not be blank')],
            ['name', 'required', 'except' => ['landing', 'simple']],
            ['password', 'required', 'except' => ['landing', 'simple', 'checkout']],
            ['agreeToTerms', 'required', 'except' => ['landing', 'simple']],

            ['email, name, surname', 'filter', 'filter' => 'trim'],

            ['telephone', 'validateTelephone'],

            ['name', 'length', 'min' => 2, 'max' => 30,
                'tooShort' => $tr('Name is too short'), 'tooLong' => $tr('60 character limit'),
                'except' => ['simple'],
            ],
            //array('name', 'match', 'pattern' => '/^([А-яA-z -]+)$/iu', 'message' => 'В имени недопустимые символы!'),

            ['surname', 'length', 'min' => 2, 'max' => 60,
                'tooShort' => $tr('Surname is too short'), 'tooLong' => $tr('60 character limit'),
                'except' => ['simple'],
            ],
            //array('surname', 'match', 'pattern' => '/^([А-яA-z -]+)$/iu', 'message' => 'В фамилии недопустимые символы!'),

            ['email', 'validateEmail'],
            ['email', 'length', 'max' => 128, 'tooLong' => $tr('128 characters limit')],

            ['email', 'unique', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'email', 'message' => $tr('E-mail is already in use')],
        ];
    }

    public function attributeLabels()
    {
        $tr = Translator::get();

        return [
            'name' => $tr('Your name'),
            'surname' => $tr('Your surname'),
            'email' => $tr('E-Mail'),
            'password' => $tr('Password'),
            'telephone' => $tr('Phone'),
            'agreeToTerms' => $tr('Agree to terms of service'),
        ];
    }

    /**
     * валидация телефонных номеров текущей страны
     *
     * @param string $attribute
     */
    public function validateTelephone($attribute)
    {
        if (!empty($this->telephone)) {
            $phone = str_replace(' ', '', $this->telephone);
            $regex = \Yii::app()->params['validatorRegex']['phone'][\Yii::app()->language]['pattern'];

            if (!preg_match('/' . $regex . '/', $phone)) {
                $this->addError($attribute, Translator::t('Phone number is incorrect'));
            }
        }
    }

    /**
     * Валидация e-mail адресса по паттерну из конфига
     *
     * @param string $attribute
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function validateEmail(string $attribute)
    {
        $email = str_replace(' ', '', $this->email);
        $regex = $this->app()->params['validatorRegex']['email']['default']['pattern'];
        $flags = $this->app()->params['validatorRegex']['email']['default']['flags'];

        if (!preg_match('/' . $regex . '/' . $flags, $email)) {
            $this->addError($attribute, Translator::t('E-mail is not valid'));
        }
    }

    public static function getMarkupVariant()
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = \Yii::app()->splitTesting;
        $variant = $splitTesting->getRegistrationFieldsVariant();

        return $variant;
    }
}
