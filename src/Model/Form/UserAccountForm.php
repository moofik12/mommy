<?php

namespace MommyCom\Model\Form;

use CFormModel;
use MommyCom\Model\Db\UserRecord;

/**
 * Class UserAccountForm
 */
class UserAccountForm extends CFormModel
{
    public $name;
    public $surname;
    public $email;
    public $telephone;
    public $address;

    public function rules()
    {
        return [
            ['name, surname, email, telephone, address', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => \Yii::t('common', 'Заполните поле')],
            ['email', 'email'],
            ['email', 'validEmail'],

            ['name, surname, email, telephone, address', 'safe'],
        ];
    }

    public function validEmail($attribute)
    {
        $email = $this->$attribute;
        $user = \Yii::app()->user->getModel();
        /** @var  $user UserRecord */
        $userWithEmail = UserRecord::model()->emailIs($email)->find();
        /** @var  $userWithEmail UserRecord */
        if ($userWithEmail !== null) {
            if ($userWithEmail->id !== $user->id) {
                $this->addError($attribute, \Yii::t('common', 'Email {email} уже используется', ['{email}' => $email]));
            }
        }
    }
} 
