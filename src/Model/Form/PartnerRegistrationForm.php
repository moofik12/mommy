<?php

namespace MommyCom\Model\Form;

use CFormModel;

/**
 * Class PartnerRegistrationForm
 * Регистрации участиков партнерской программы
 */
class PartnerRegistrationForm extends CFormModel
{
    public $email;
    public $isConfirmPartner = true;
    public $isConfirmPrivacy = true;

    public function rules()
    {
        return [
            ['isConfirmPartner', 'boolean'],
            ['isConfirmPartner', 'required'],

            ['email', 'required', 'message' => 'Заполните Email', 'on' => 'newUser'],
            ['isConfirmPrivacy', 'required', 'on' => 'newUser'],
            ['isConfirmPrivacy', 'boolean', 'on' => 'newUser'],
            ['email', 'email', 'on' => 'newUser'],
            ['email', 'unique', 'className' => 'MommyCom\Model\Db\UserRecord', 'attributeName' => 'email', 'message' => \Yii::t('common', 'Email уже используется'), 'on' => 'newUser'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'isConfirmPartner' => \Yii::t('common', 'Согласие с правилами партнерской программы'),
            'isConfirmPrivacy' => \Yii::t('common', 'Согласие с правилами сайта'),
        ];
    }
}
