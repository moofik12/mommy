<?php

namespace MommyCom\Model\Form;

use CFormModel;
use MommyCom\Model\Db\CallbackRecord;

class CallbackForm extends CFormModel
{
    /**
     * @var string
     */
    public $telephone;

    public function rules()
    {
        return [
            ['telephone', 'required', 'message' => 'Необходимо заполнить поле «{attribute}»'],
            //min без кода города
            ['telephone', 'length', 'min' => 7, 'max' => 20, 'tooLong' => 'Ваш номер слишком длинный (максимум: {max} симв.)',
                'tooShort' => 'Ваш номер слишком короткий (минимум: {min} симв.)',
            ],
            //если в очереди заявка
            ['telephone', 'validatorQueue', 'userId' => \Yii::app()->user->id],
        ];
    }

    public function attributeLabels()
    {
        return [
            'telephone' => \Yii::t('common', 'Ваш номер телефона'),
        ];
    }

    public function validatorQueue($attribute, $params)
    {
        $userId = isset($params['userId']) ? $params['userId'] : \Yii::app()->user->id;

        /*
        $positionQueue = CallbackRecord::model()->positionUserQueue($userId);
        if ($positionQueue) {
            $this->addError($attribute, \Yii::t('common', 'Скоро вам позвонят. Ваш звонок {n} в очереди', $positionQueue));
        }
        */

        $isRequest = CallbackRecord::model()->isRequestUser($userId);

        if ($isRequest) {
            $this->addError($attribute, \Yii::t('common', 'Дождитесь окончания обработки текущей заявки'));
        }
    }
}
