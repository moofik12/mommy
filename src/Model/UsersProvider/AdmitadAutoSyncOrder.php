<?php

namespace MommyCom\Model\UsersProvider;

use CModel;
use LogicException;
use MommyCom\Model\Db\OrderRecord;

class AdmitadAutoSyncOrder extends CModel
{
    const STATUS_CONFIRM = 1;
    const STATUS_CANCEL = 2;

    /**
     * Номер заказа
     *
     * @var int
     */
    public $OrderID;

    /**
     * Статус заказа
     *
     * @var int
     */
    public $Status;

    /**
     * Стоимость заказа
     * (опционально если стоимость изменилась)
     *
     * @var float
     */
    public $OrderAmount;

    /**
     * Вознаграждение
     * (опционально)
     *
     * @var float
     */
    public $Reward;

    /**
     * @var OrderRecord
     */
    private $_order;

    /**
     * @param OrderRecord $order
     * @param int $status
     */
    public function __construct(OrderRecord $order, $status)
    {
        if (!in_array($status, [
            self::STATUS_CANCEL,
            self::STATUS_CONFIRM,
        ])) {
            throw new LogicException("Status '$status' not support");
        }

        $this->_order = $order;
        $this->_init($status);
    }

    /**
     * @param int $status
     */
    private function _init($status)
    {
        $order = $this->_order;

        $this->OrderID = $order->id;
        $this->Status = $status;
        $this->OrderAmount = $order->getPrice();
    }

    public function attributeNames()
    {
        return ['OrderID', 'Status', 'OrderAmount', 'Reward'];
    }
}
