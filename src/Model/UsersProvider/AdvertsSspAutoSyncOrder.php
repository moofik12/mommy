<?php

namespace MommyCom\Model\UsersProvider;

use CModel;
use MommyCom\Model\Db\OrderRecord;

class AdvertsSspAutoSyncOrder extends CModel
{
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_REJECTED = 'rejected';
    const STATUS_WAITING = 'waiting';

    /**
     * Идентификатор перехода от advertssp
     *
     * @var string
     */
    public $waylogId;

    /**
     * Номер заказа в нашей системе
     * Идентификатор выполненного действия, это может быть номер заказа или любой другой не повторящуйся номер
     *
     * @var int|string
     */
    public $actionId;

    /**
     * @var string
     */
    public $status;

    /**
     * Cумма заказа
     *
     * @var float
     */
    public $amount;

    /**
     * @param OrderRecord $order
     * @param string $status @see self statuses
     */
    public function __construct(OrderRecord $order, $status)
    {
        $this->waylogId = $order->offer_id;
        $this->actionId = $order->id;
        $this->status = $status;
        $this->amount = $order->getPrice();
    }

    public function attributeNames()
    {
        return ['waylogId', 'actionId', 'status', 'amount'];
    }
}
