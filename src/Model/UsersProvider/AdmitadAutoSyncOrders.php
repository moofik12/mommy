<?php

namespace MommyCom\Model\UsersProvider;

use DOMElement;
use DOMImplementation;
use LogicException;

class AdmitadAutoSyncOrders
{
    /** @var AdmitadAutoSyncOrder[] */
    protected $_models = [];

    /**
     * @param AdmitadAutoSyncOrder $model
     */
    public function addModel(AdmitadAutoSyncOrder $model)
    {
        $this->_models[$model->OrderID] = $model;
    }

    /**
     * @param AdmitadAutoSyncOrder[] $models
     *
     * @throws LogicException
     */
    public function addModels(array $models)
    {
        if (!reset($orders) instanceof AdmitadAutoSyncOrder) {
            throw new LogicException('Array not has AdmitadAutoSyncOrder object');
        }

        foreach ($models as $model) {
            $this->_models[$model->OrderID] = $model;
        }
    }

    public function toXml()
    {
        $implementation = new DOMImplementation();
        $dom = $implementation->createDocument();
        $dom->encoding = 'utf-8';
        $dom->formatOutput = true;

        $domTree = $dom->createElement('Payments');
        $domTree->setAttribute('xmlns', 'http://admitad.com/payments-revision');

        foreach ($this->_models as $model) {
            $nodePayment = new DOMElement('Payment');
            $domTree->appendChild($nodePayment);

            foreach ($model->getAttributes() as $name => $value) {
                if ($value) {
                    $node = new DOMElement($name);
                    $node->nodeValue = $value;
                    $nodePayment->appendChild($node);
                }
            }
        }

        $dom->appendChild($domTree);

        return $dom->saveXML();
    }
}
