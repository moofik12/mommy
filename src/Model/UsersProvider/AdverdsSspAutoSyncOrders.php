<?php

namespace MommyCom\Model\UsersProvider;

use DOMElement;
use DOMImplementation;
use LogicException;

class AdverdsSspAutoSyncOrders
{
    /** @var AdvertsSspAutoSyncOrder[] */
    protected $_models = [];

    /**
     * @param AdvertsSspAutoSyncOrder $model
     */
    public function addModel(AdvertsSspAutoSyncOrder $model)
    {
        $this->_models[$model->waylogId] = $model;
    }

    /**
     * @param AdvertsSspAutoSyncOrder[] $models
     *
     * @throws LogicException
     */
    public function addModels(array $models)
    {
        if (!reset($orders) instanceof AdvertsSspAutoSyncOrder) {
            throw new LogicException('Array not has AdvertsSspAutoSyncOrder object');
        }

        foreach ($models as $model) {
            $this->_models[$model->waylogId] = $model;
        }
    }

    /**
     * @return string
     */
    public function toXml()
    {
        $implementation = new DOMImplementation();
        $dom = $implementation->createDocument();
        $dom->encoding = 'utf-8';
        $dom->formatOutput = true;

        $domTree = $dom->createElement('cpa');

        foreach ($this->_models as $model) {
            $nodePayment = new DOMElement('lead');
            $domTree->appendChild($nodePayment);

            foreach ($model->getAttributes() as $name => $value) {
                $node = new DOMElement($name);
                $node->nodeValue = $value;
                $nodePayment->appendChild($node);
            }
        }

        $dom->appendChild($domTree);

        return $dom->saveXML();
    }
}
