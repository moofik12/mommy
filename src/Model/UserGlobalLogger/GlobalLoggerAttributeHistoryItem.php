<?php

namespace MommyCom\Model\UserGlobalLogger;

final class GlobalLoggerAttributeHistoryItem
{
    public $before;
    public $after;
}
