<?php

namespace MommyCom\Model\UserGlobalLogger;

/**
 * Class GlobalLoggerAttributeHistory
 *
 * @property-ready GlobalLoggerAttributeHistoryItem $value
 */
final class GlobalLoggerAttributeHistory
{
    /**
     * @var string name attribute
     */
    public $id;

    /**
     * @var GlobalLoggerAttributeHistoryItem
     */
    private $_value;

    public function __construct()
    {
        $this->_value = new GlobalLoggerAttributeHistoryItem();
    }

    /**
     * @return GlobalLoggerAttributeHistoryItem
     */
    public function getValue()
    {
        return $this->_value;
    }
}
