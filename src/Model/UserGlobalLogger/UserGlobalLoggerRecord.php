<?php

namespace MommyCom\Model\UserGlobalLogger;

use CActiveDataProvider;
use CActiveRecord;
use MommyCom\Model\Behavior\Timestampable;
use MommyCom\Model\Db\AdminRoleAssignmentRecord;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

/**
 * Class UserGlobalLoggerRecord
 *
 * @property-read int $id
 * @property int $action
 * @property int $user_id
 * @property string $user_class
 * @property string $class_object_id
 * @property string $class
 * @property string $controller
 * @property string $attributes_before_json
 * @property string $attributes_after_json
 * @property string $description
 * @property-read string $actionReplacement
 * @property-read string $controllerReplacement
 * @property int updated_at
 * @property int created_at
 * @mixin Timestampable
 */
class UserGlobalLoggerRecord extends UnShardedActiveRecord
{
    const ACTION_UNKNOWN = 0;
    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_DELETE = 3;
    const ACTION_VIEW = 4;

    private $_cacheUserModel = null;
    private $_cacheModel = null;

    /**
     * @param string $className
     *
     * @return UserGlobalLoggerRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return 'user_global_logger';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['user_class, class', 'classValidator'],
            ['user_id, class_object_id', 'numerical', 'integerOnly' => true],
            ['action', 'in', 'range' => self::actions(false)],

            //not required
            ['controller', 'length', 'max' => 128],
            ['description', 'length', 'max' => 255, 'encoding' => false],

            ['attributes_before_json, attributes_after_json', 'length'],

            ['user_class, class, user_id, class_object_id, action, controller, description, attributes_before_json, attributes_after_json',
                'safe', 'on' => 'searchPrivileged'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'action' => 'Действие',
            'actionReplacement' => 'Действие',
            'user_id' => 'Пользователь',
            'userClassReplacement' => 'Тип пользователя',
            'user_class' => 'Класс пользователя',
            'class_object_id' => 'Объект',
            'class' => 'Класс объекта',
            'classReplacement' => 'Назначение',
            'controller' => 'Модуль',
            'controllerReplacement' => 'Модуль',
            'attributes_before_json' => 'До изменения',
            'attributesBefore' => 'До изменения',
            'attributes_after_json' => 'После изменения',
            'attributesAfter' => 'После изменения',
            'description' => 'Описание',

            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    public function behaviors()
    {
        return parent::behaviors() + [
                'timestampable' => [
                    'class' => Timestampable::class,
                ],
            ];
    }

    /**
     * @param bool $resetScopes
     * @param array $providerOptions
     *
     * @return CActiveDataProvider
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $provider = parent::getDataProvider($resetScopes, $providerOptions);

        $alias = $this->getTableAlias();
        $criteria = $provider->getCriteria();

        $model = $provider->model;
        /* @var $model UserGlobalLoggerRecord */

        $criteria->compare($alias . '.id', Utf8::ltrim($model->id, '0'), true);
        $criteria->compare($alias . '.action', $model->action);
        $criteria->compare($alias . '.user_id', $model->user_id);
        $criteria->compare($alias . '.user_class', $model->user_class);
        $criteria->compare($alias . '.class', $model->class);
        $criteria->compare($alias . '.class_object_id', $model->class_object_id);
        $criteria->compare($alias . '.controller', $model->controller);
        $criteria->compare($alias . '.description', $model->description, true);
        $criteria->compare($alias . '.attributes_before_json', $model->attributes_before_json, true);
        $criteria->compare($alias . '.attributes_after_json', $model->attributes_after_json, true);

        return $provider;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function classValidator($attribute, $params)
    {
        $value = $this->$attribute;
        if (empty($value)) {
            return;
        }

        if (!class_exists($value)) {
            $this->addError($attribute, "Class $value not found");
        }
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param int $value
     *
     * @return $this
     */
    public function classObjectId($value)
    {
        $alias = $this->getTableAlias();
        $value = Cast::toUInt($value);

        $this->getDbCriteria()->addColumnCondition([
            $alias . '.class_object_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getActionReplacement($default = 'не найдено')
    {
        $replacements = self::actions();

        return isset($replacements[$this->action]) ? $replacements[$this->action] : $default;
    }

    /**
     * @param bool|true $replacement
     *
     * @return array
     */
    public static function actions($replacement = true)
    {
        static $statuses = [
            self::ACTION_UNKNOWN => 'Неизвестно',
            self::ACTION_CREATE => 'Создание',
            self::ACTION_UPDATE => 'Редактирование',
            self::ACTION_DELETE => 'Удаление',
            self::ACTION_VIEW => 'Просмотр',
        ];

        return $replacement ? $statuses : array_keys($statuses);
    }

    /**
     * @return array
     */
    public function getAttributesBefore()
    {
        if (!$this->attributes_before_json) {
            return [];
        }

        $resultParse = json_decode($this->attributes_before_json, true, 4);
        return $resultParse ? $resultParse : [];
    }

    /**
     * @param $value
     */
    public function setAttributesBefore($value)
    {
        $this->attributes_before_json = json_encode($value, JSON_UNESCAPED_UNICODE, 4);
    }

    /**
     * @return array
     */
    public function getAttributesAfter()
    {
        if (!$this->attributes_after_json) {
            return [];
        }

        $resultParse = json_decode($this->attributes_after_json, true, 4);
        return $resultParse ? $resultParse : [];
    }

    /**
     * @param $value
     */
    public function setAttributesAfter($value)
    {
        $this->attributes_after_json = json_encode($value, JSON_UNESCAPED_UNICODE, 4);
    }

    /**
     * @return string
     */
    public function getUserClassReplacement()
    {
        $replacements = self::classDescriptions();

        return isset($replacements[$this->user_class]) ? $replacements[$this->user_class] : $this->user_class;
    }

    /**
     * @return string
     */
    public function getClassReplacement()
    {
        $replacements = self::classDescriptions();

        return isset($replacements[$this->class]) ? $replacements[$this->class] : $this->class;
    }

    /**
     * @return string
     */
    public function getControllerReplacement()
    {
        $replacements = self::controllerDescriptions();

        return isset($replacements[$this->controller]) ? $replacements[$this->controller] : $this->controller;
    }

    /**
     * @param bool|false $onlySkeleton
     * @param int $cache
     *
     * @return CActiveRecord|null
     */
    public function getUserModel($onlySkeleton = false, $cache = 60)
    {
        if ($this->_cacheUserModel !== null) {
            return $this->_cacheUserModel;
        }

        $model = null;
        $class = $this->user_class;
        if ($this->_isModel($class)) {
            /* @var CActiveRecord $model */
            $model = $class::model();

            if (!$onlySkeleton) {
                $model = $model->cache($cache)->findByPk($this->user_id);
                $this->_cacheUserModel = $model;
            }
        }

        return $model;
    }

    /**
     * @param bool|false $onlySkeleton
     * @param int $cache
     *
     * @return CActiveRecord|null
     */
    public function getModel($onlySkeleton = false, $cache = 60)
    {
        if ($this->_cacheModel !== null) {
            return $this->_cacheModel;
        }

        $model = null;
        $class = $this->class;
        if ($this->_isModel($class)) {
            /* @var CActiveRecord $model */
            $model = $class::model();

            if (!$onlySkeleton) {
                $model = $model->cache($cache)->findByPk($this->user_id);
                $this->_cacheModel = $model;
            }
        }

        return $model;
    }

    /**
     * @return GlobalLoggerAttributeHistory[]
     */
    public function getAttributesHistory()
    {
        $data = [];
        foreach ($this->getAttributesBefore() as $attribute => $value) {
            $model = new GlobalLoggerAttributeHistory();
            $model->id = $attribute;
            $model->getValue()->before = $value;

            $data[$attribute] = $model;
        }

        foreach ($this->getAttributesAfter() as $attribute => $value) {
            if (isset($data[$attribute])) {
                $model = $data[$attribute];
            } else {
                $model = new GlobalLoggerAttributeHistory();
                $model->id = $attribute;
            }
            $model->getValue()->after = $value;

            $data[$attribute] = $model;
        }

        return array_values($data);
    }

    /**
     * @param $className
     *
     * @return bool
     */
    private function _isModel($className)
    {
        if (is_subclass_of($className, 'CActiveRecord')) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public static function classDescriptions()
    {
        static $descriptions = null;
        if ($descriptions === null) {
            $descriptions = [
                    //records
                    AdminRoleAssignmentRecord::class => 'Назначение роли',
                    'MommyCom\Model\Db\AdminRoleRecord' => 'Роль администраторов',
                    'MommyCom\Model\Db\AdminSuppliers' => 'Поставщик в админке',
                    'MommyCom\Model\Db\AdminUserRecord' => 'Администратор',
                    'MommyCom\Model\Db\BonuspointsPromoRecord' => 'Бонусная акция',
                    'MommyCom\Model\Db\BrandAliasRecord' => 'Псевдоним поставщиков',
                    'MommyCom\Model\Db\BrandRecord' => 'Бренд',
                    'MommyCom\Model\Db\CallbackRecord' => 'Обратный звонок',
                    'MommyCom\Model\Db\CallcenterTaskOperationRecord' => 'Действие задачи для коллцентра',
                    'MommyCom\Model\Db\CallcenterTaskRecord' => 'Задача для коллцентра',
                    'MommyCom\Model\Db\DistributionPushRecord' => 'Рассылка push сообщений',
                    'MommyCom\Model\Db\EventProductRecord' => 'Товар в акции',
                    'MommyCom\Model\Db\EventRecord' => 'Акция',
                    'MommyCom\Model\Db\HelperPageCategoryRecord' => 'Категория инф. стр. в админке',
                    'MommyCom\Model\Db\HelperPageRecord' => 'Информационная стр. админке',
                    'MommyCom\Model\Db\InviteRecord' => 'Приглашения пользователя',
                    'MommyCom\Model\Db\IpBanRecord' => 'Блокировка по IP',
                    'MommyCom\Model\Db\MoneyControlRecord' => 'Контроль денег',
                    'MommyCom\Model\Db\MoneyReceiveStatementOrderRecord' => 'Заказ в акте пол. денег',
                    'MommyCom\Model\Db\MoneyReceiveStatementRecord' => 'Акт пол. денег',
                    'MommyCom\Model\Db\OrderBankRefundRecord' => 'Возврат денег оплаченный картой',
                    'MommyCom\Model\Db\OrderBuyoutRecord' => 'Оплата заказа',
                    'MommyCom\Model\Db\OrderControlRecord' => 'Контроль состояния заказа',
                    'MommyCom\Model\Db\OrderDiscountCampaignRecord' => 'Скидочные акции',
                    'MommyCom\Model\Db\OrderProductRecord' => 'Товар в заказе',
                    'MommyCom\Model\Db\OrderRecord' => 'Заказ',
                    'MommyCom\Model\Db\OrderReturnRecord' => 'Возврат заказа',
                    'MommyCom\Model\Db\OrderReturnProductRecord' => 'Возврат товара',
                    'MommyCom\Model\Db\OrderTrackingRecord' => 'ТТН заказа',
                    'MommyCom\Model\Db\PayGatewayLogRecord' => 'Лог оплаты по безналу',
                    'MommyCom\Model\Db\PayGatewayRecord' => 'Оплата по безналу',
                    'MommyCom\Model\Db\ProductRecord' => 'Товар (базовый)',
                    'MommyCom\Model\Db\PromocodeRecord' => 'Промокод',
                    'MommyCom\Model\Db\StatementOrderRecord' => 'Заказ в акте отправки',
                    'MommyCom\Model\Db\StatementRecord' => 'Акт отправки',
                    'MommyCom\Model\Db\StaticPageCategoryRecord' => 'Категории стат. страниц',
                    'MommyCom\Model\Db\StaticPageRecord' => 'Статическая страница',
                    'MommyCom\Model\Db\SupplierContractAssignmentRecord' => 'Принадлежность контр. к поставщику',
                    'SupplierContractTypeRecord' => 'Контракт поставщика',
                    'MommyCom\Model\Db\SupplierPaymentRecord' => 'Выплата поставщику',
                    'MommyCom\Model\Db\SupplierRecord' => 'Поставщик',
                    'MommyCom\Model\Db\TabletUserRecord' => 'Аккаунт на планшете',
                    'MommyCom\Model\Db\UserBlackListRecord' => 'Чёрный список пользователя',
                    'MommyCom\Model\Db\UserBonuspointsRecord' => 'Бонусный счет пользователя',
                    'MommyCom\Model\Db\UserRecord' => 'Пользователь',
                    'MommyCom\Model\Db\WarehouseProductRecord' => 'Складской товар',
                    'MommyCom\Model\Db\WarehouseStatementReturnProductRecord' => 'Складской товар в акте возврата поставщику',
                    'MommyCom\Model\Db\WarehouseStatementReturnRecord' => 'Акт возврата поставщику',

                    ShopWebUser::class => 'Пользователь',
                ] + self::controllerDescriptions();
        }

        return $descriptions;
    }

    /**
     * @return array
     */
    public static function controllerDescriptions()
    {
        static $descriptions = null;
        if ($descriptions === null) {
            $descriptions = [
                //Controllers
                'MommyCom\Controller\Backend\CallcenterOrderController' => 'Управление заказами',
                'MommyCom\Controller\Backend\UserController' => 'Управление пользователями',
                'MommyCom\Controller\Backend\BonuspointsController' => 'Управление Бонусами',
                'MommyCom\Controller\Backend\EventController' => 'Управление Акциями',
                'MommyCom\Controller\Backend\EventProductController' => 'Управление Товарами акции',
                'MommyCom\Controller\Backend\StatisticController' => 'Просмотр Статистики (старая)',
                'MommyCom\Controller\Backend\StatisticCustomController' => 'Просмотр Статистики',
                'MommyCom\Controller\Backend\EmailController' => 'Управление E-mail сообщениями',
                'MommyCom\Controller\Backend\SmsController' => 'Управление SMS сообщениями',
                'MommyCom\Controller\Backend\DistributionController' => 'Управление Рассылками',
                'MommyCom\Controller\Backend\OrderReturnController' => 'Управление возвратами',
                'MommyCom\Controller\Backend\SupplierController' => 'Управление Поставщиками',
                'MommyCom\Controller\Backend\SupplierPaymentController' => 'Управление Оплатами Поставщикам',
                'MommyCom\Controller\Backend\IpBanController' => 'Управление Блокировкой по IP',
                'MommyCom\Controller\Backend\MoneyControlController' => 'Управление Актами возврата денег',
                'MommyCom\Controller\Backend\CourierStatementController' => 'Управление Актами приема-передачи курьеров',
                'MommyCom\Controller\Backend\OrderControlController' => 'Контроль заказов',
                'MommyCom\Controller\Backend\ProductController' => 'Управление Товаром',
                'MommyCom\Controller\Backend\PromocodeController' => 'Управление Промокодами',
                'MommyCom\Controller\Backend\StaticPageController' => 'Управление Статическими страницами',
                'MommyCom\Controller\Backend\StaticPageCategoryController' => 'Управление Категориями статических страниц',
                'MommyCom\Controller\Backend\StorekeeperOrderController' => 'Управление Заказами на упаковку',
                'MommyCom\Controller\Backend\UserBlackListController' => 'Управление Черным списком пользователей',
                'MommyCom\Controller\Backend\UserUnformedCartController' => 'Просмотр неоформленных корзин пользователей',
                'MommyCom\Controller\Backend\WarehouseController' => 'Управление Складом',
                'MommyCom\Controller\Backend\WarehouseDownloadController' => 'Скачивание отчета по складу',
                'MommyCom\Controller\Backend\WarehouseStatementReturnController' => 'Управление возвратами поставщикам',
                'MommyCom\Controller\Backend\OrderPayController' => 'Управление оплатами заказов (безнал)',
                'MommyCom\Controller\Backend\CallcenterCallbackController' => 'Управление обратными звонками (коллцентр)',
                'MommyCom\Controller\Backend\DistributionPushController' => 'Управление PUSH рассылкой',
                'MommyCom\Controller\Backend\AuthController' => 'Вход/выход в админку',
                'MommyCom\Controller\Backend\BrandController' => 'Управление Брендами',
                'MommyCom\Controller\Backend\CallcenterTaskController' => 'Управление задачами для Call центра',
                'MommyCom\Controller\Backend\AdminController' => 'Управление Администраторами',
                'MommyCom\Controller\Backend\AdminRoleController' => 'Управление Ролями в админке',
            ];
        }

        return $descriptions;
    }
}
