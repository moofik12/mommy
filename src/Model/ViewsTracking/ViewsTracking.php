<?php

namespace MommyCom\Model\ViewsTracking;

use CActiveRecord;
use CComponent;
use CDbConnection;
use CException;
use MommyCom\YiiComponent\Type\Cast;
use ReflectionClass;
use Yii;

/**
 * Class ViewsTracking
 * Более-менее оптимизированное решение по отслеживанию количества просмотров элементов
 * Использует MySQL специфичные расширения (DELAYED, ON DUPLICATE)
 * В данный момент оптимизированно только на такие задачи:
 * быстрое получение количества просмотров, редкие table locks
 * редикие table locks за счет:
 * (views_tracking_incoming) отсутвия autoinc поля, отсутствия чтения из этой таблицы, нет индексов
 */
class ViewsTracking extends CComponent
{
    CONST STATISTICS_LIFETIME = 2592000; // one month

    /**
     * @var CDbConnection
     */
    public $db;

    public function __construct()
    {
        $this->db = Yii::app()->db;
    }

    public static function getObjectType($class)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }

        if (!is_string($class)) {
            throw new CException('$class must be of string type or object');
        }

        static $cache = [];

        if (!isset($cache[$class])) {
            list($cache[$class]) = sscanf(base_convert(hash('crc32', $class), 16, 10), '%u'); // convert to uint
        }

        return $cache[$class];
    }

    /**
     * @param CActiveRecord $object
     * @param integer $userId
     *
     * @return boolean
     */
    public function add(CActiveRecord $object, $userId)
    {
        $cache = Yii::app()->cache;

        $objectType = self::getObjectType($object);
        $objectId = Cast::toUInt($object->primaryKey);
        $userId = Cast::toUInt($userId);

        if ($object->getIsNewRecord() || $objectId == 0) {
            return false;
        }

        $cacheKey = __CLASS__ . '_' . $objectType . '_' . $objectId . '_' . $userId;
        if ($cache->get($cacheKey)) { // already added
            return true;
        }

        $query = "
            INSERT DELAYED INTO `views_tracking_incoming`(`object_type`, `object_id`, `user_id`, `time`)
            VALUES(:objectType, :objectId, :userId, UNIX_TIMESTAMP())
        ";

        $cmd = $this->db->createCommand($query);
        $cmd->bindValues([
            'objectType' => $objectType,
            'objectId' => $objectId,
            'userId' => $userId,
        ]);

        if ($cmd->execute() <= 0) {
            return false;
        }

        $cache->set($cacheKey, true, 180);

        return true;
    }

    /**
     * @param CActiveRecord $object
     *
     * @return integer
     */
    public function get($object)
    {
        $objectType = self::getObjectType($object);
        $objectId = Cast::toUInt($object->primaryKey);

        if ($object->getIsNewRecord() || $objectId == 0) {
            return false;
        }

        $query = "
            SELECT `number`
            FROM `views_tracking`
            WHERE `object_type` = :objectType AND `object_id` = :objectId
        ";

        $cmd = $this->db->createCommand($query);
        $cmd->bindValues([
            'objectType' => $objectType,
            'objectId' => $objectId,
        ]);

        return Cast::toUInt($cmd->queryScalar());
    }

    protected function _mergeViews($databaseTime)
    {
        $builder = $this->db->commandBuilder;

        $ref = new ReflectionClass($builder);
        $composeMultipleInsertCommandRef = $ref->getMethod('composeMultipleInsertCommand');
        $composeMultipleInsertCommandRef->setAccessible(true); // hard hack, я не горжусь этим

        $pageSize = 1000;

        $query = "
            SELECT
                `object_type`,
                `object_id`,
                COUNT(DISTINCT `user_id`) AS 'number',
                MAX(`time`) AS 'created_at',
                MAX(`time`) AS 'updated_at'
            FROM `views_tracking_incoming`
            WHERE `time` < :time
            GROUP BY `object_type`, `object_id`
            LIMIT :page, :pageSize
        ";

        $i = 0;
        do {
            $selectCmd = $this->db->createCommand($query);
            $selectCmd->bindValues([
                'time' => $databaseTime,
                'pageSize' => $pageSize,
                'page' => $i * $pageSize,
            ]);

            $allViews = $selectCmd->queryAll();
            $hasViews = !empty($allViews);

            if ($hasViews) {
                $insertCmd = $composeMultipleInsertCommandRef->invoke($builder, 'views_tracking', $allViews, [
                    'main' => /** @lang text */
                        '
                    INSERT INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}
                    ON DUPLICATE KEY UPDATE `number` = `number` + VALUES(`number`), `updated_at` = VALUES(`updated_at`)
                ',
                ]);
                unset($allViews);

                $insertCmd->execute();
            }

            $i++;
        } while ($hasViews);

        return true;
    }

    protected function _mergeViewsByUser($databaseTime)
    {
        $builder = $this->db->commandBuilder;

        $ref = new ReflectionClass($builder);
        $composeMultipleInsertCommandRef = $ref->getMethod('composeMultipleInsertCommand');
        $composeMultipleInsertCommandRef->setAccessible(true); // hard hack, я не горжусь этим

        $pageSize = 1000;

        $query = "
            SELECT
                `user_id`,
                `object_type`,
                `object_id`,
                SUM(1) AS 'number',
                MAX(`time`) AS 'created_at',
                MAX(`time`) AS 'updated_at'
            FROM `views_tracking_incoming`
            WHERE `time` < :time
            GROUP BY `user_id`, `object_type`, `object_id`
            LIMIT :page, :pageSize
        ";

        $i = 0;
        do {
            $selectCmd = $this->db->createCommand($query);
            $selectCmd->bindValues([
                'time' => $databaseTime,
                'pageSize' => $pageSize,
                'page' => $i * $pageSize,
            ]);

            $allViews = $selectCmd->queryAll();
            $hasViews = !empty($allViews);

            if ($hasViews) {
                $insertCmd = $composeMultipleInsertCommandRef->invoke($builder, 'views_tracking_by_users', $allViews, [
                    'main' => /** @lang text */
                        '
                    INSERT INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}
                    ON DUPLICATE KEY UPDATE `number` = `number` + VALUES(`number`), `updated_at` = VALUES(`updated_at`)
                ',
                ]);
                unset($allViews);

                $insertCmd->execute();
            }

            $i++;
        } while ($hasViews);

        return true;
    }

    protected function _mergeViewsSimilarity($databaseTime)
    {
        $builder = $this->db->commandBuilder;

        $ref = new ReflectionClass($builder);
        $composeMultipleInsertCommandRef = $ref->getMethod('composeMultipleInsertCommand');
        $composeMultipleInsertCommandRef->setAccessible(true); // hard hack, я не горжусь этим

        $pageSize = 5000;

        $query = "
            SELECT
                `vti1`.`object_type` AS object_type,
                `vti1`.`object_id` AS object_id,
                `vti2`.`object_id` AS similar_object_id,
                COUNT(DISTINCT `vti2`.`user_id`) AS 'number',
                MAX(`vti2`.`time`) AS 'created_at',
                MAX(`vti2`.`time`) AS 'updated_at'
            FROM `views_tracking_incoming` AS vti1
            JOIN `views_tracking_incoming` AS vti2
                ON (`vti1`.`object_type` = `vti2`.`object_type` AND `vti1`.`user_id` = `vti2`.`user_id` AND `vti1`.`object_id` <> `vti2`.`object_id`)
            WHERE `vti1`.`time` < :time AND `vti2`.`time` < :time
            GROUP BY vti1.`object_type`, vti1.`object_id`, vti2.`object_id`
            LIMIT :page, :pageSize
        ";

        $i = 0;
        do {
            $selectCmd = $this->db->createCommand($query);
            $selectCmd->bindValues([
                'time' => $databaseTime,
                'pageSize' => $pageSize,
                'page' => $i * $pageSize,
            ]);

            $allViews = $selectCmd->queryAll();
            $hasViews = !empty($allViews);

            if ($hasViews) {
                $insertCmd = $composeMultipleInsertCommandRef->invoke($builder, 'views_tracking_similar', $allViews, [
                    'main' => /** @lang text */
                        '
                    INSERT INTO {{tableName}} ({{columnInsertNames}}) VALUES {{rowInsertValues}}
                    ON DUPLICATE KEY UPDATE `number` = `number` + VALUES(`number`), `updated_at` = VALUES(`updated_at`)
                ',
                ]);
                unset($allViews);

                $insertCmd->execute();
            }

            $i++;
        } while ($hasViews);

        return true;
    }

    /**
     * Переносит данные из временной таблицы в статическую
     *
     * @return bool
     */
    public function mergeAll()
    {
        $timeCmd = $this->db->createCommand("SELECT UNIX_TIMESTAMP()");
        $time = Cast::toUInt($timeCmd->queryScalar());

        gc_enable();
        $success = $this->_mergeViews($time);
        gc_collect_cycles();
        $success = $success && $this->_mergeViewsByUser($time);
        gc_collect_cycles();
        $success = $success && $this->_mergeViewsSimilarity($time);
        gc_collect_cycles();

        if ($success) {
            $clearCmd = $this->db->createCommand("DELETE FROM `views_tracking_incoming` WHERE `time` < '$time'");

            if ($clearCmd->execute() < 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Удаляет старые просмотры и подобия
     *
     * @return bool
     */
    public function removeOutdated()
    {
        $timeCmd = $this->db->createCommand("SELECT UNIX_TIMESTAMP()");
        $time = Cast::toUInt($timeCmd->queryScalar());
        $outdatedTime = $time - self::STATISTICS_LIFETIME;

        // удаление старых просмотров
        $clearCmd = $this->db->createCommand("DELETE FROM `views_tracking` WHERE `updated_at` < :time");
        $clearCmd->bindValue('time', $outdatedTime);

        // удаление старых просмотров
        $clearCmd = $this->db->createCommand("DELETE FROM `views_tracking_by_users` WHERE `updated_at` < :time");
        $clearCmd->bindValue('time', $outdatedTime);

        // удаление старых подобий
        $clearCmd = $this->db->createCommand("DELETE FROM `views_tracking_similar` WHERE `updated_at` < :time");
        $clearCmd->bindValue('time', $outdatedTime);

        return true;
    }
}
