<?php

namespace MommyCom\Model\ViewsTracking;

use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class ViewTrackingSimilarityRecord
 *
 * @property-read integer $id
 * @property-read integer $object_type
 * @property-read integer $object_id
 * @property-read integer $similar_object_id
 * @property-read integer $number
 * @property-read integer $created_at
 * @property-read integer $updated_at
 * @property-read ViewTrackingSimilarRecord[] $similar
 */
class ViewTrackingSimilarRecord extends UnShardedActiveRecord
{
    /**
     * @param string $className
     *
     * @return ViewTrackingSimilarRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'views_tracking_similar';
    }

    public function relations()
    {
        return [
            'similar' => [self::HAS_MANY, 'MommyCom\Model\ViewsTracking\ViewTrackingSimilarRecord', ['similar_object_id' => 'object_id', 'object_type' => 'object_type']],
        ];
    }

    public function beforeSave()
    {
        return false; // table is read-only
    }

    public function beforeDelete()
    {
        return false; // table is read-only
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function objectType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.object_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function objectTypeIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'object_type', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function objectId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.object_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function objectIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'object_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function objectIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.' . 'object_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function similarObjectId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.similar_object_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function similarObjectIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'similar_object_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function similarObjectIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.' . 'similar_object_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdAtFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'created_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdAtTo($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'created_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function createdAtBetween($from, $to)
    {
        $this->createdAtFrom($from);
        $this->createdAtTo($to);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function updatedAtFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'updated_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function updatedAtTo($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'updated_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function updatedAtBetween($from, $to)
    {
        $this->createdAtFrom($from);
        $this->createdAtTo($to);
        return $this;
    }
} 
