<?php

namespace MommyCom\Model\ViewsTracking;

use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class ViewTrackingRecord
 * !!!!! ОСТОРОЖНО - ЗАПИСИ ИСПОЛЬЗУЮТСЯ ТОЛЬКО ДЛЯ ЧТЕНИЯ
 * !!!! ЗАПИСЬ ОСУЩЕСТВЛЯЕТСЯ ТОЛЬКО ЧЕРЕЗ КОМПОНТЕНТ КОТОРЫЙ НЕ ИСПОЛЬЗУЕТ ACTIVERECORD ПРОСЛОЙКУ
 *
 * @property-read integer $id
 * @property-read integer $object_type
 * @property-read integer $object_id
 * @property-read integer $number
 * @property-read integer $created_at
 * @property-read integer $updated_at
 */
class ViewTrackingRecord extends UnShardedActiveRecord
{
    /**
     * @param string $className
     *
     * @return ViewTrackingRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'views_tracking';
    }

    public function beforeSave()
    {
        return false; // table is read-only
    }

    public function beforeDelete()
    {
        return false; // table is read-only
    }

    /**
     * @param integer $value
     *
     * @return ViewTrackingRecord
     */
    public function objectType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.object_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return ViewTrackingRecord
     */
    public function objectTypeIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'object_type', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return ViewTrackingRecord
     */
    public function objectId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.object_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return ViewTrackingRecord
     */
    public function objectIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'object_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return ViewTrackingRecord
     */
    public function objectIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.' . 'object_id', $values);
        return $this;
    }
} 
