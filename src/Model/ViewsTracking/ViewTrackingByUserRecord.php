<?php

namespace MommyCom\Model\ViewsTracking;

use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class ViewTrackingByUserRecord
 * !!!!! ОСТОРОЖНО - ЗАПИСИ ИСПОЛЬЗУЮТСЯ ТОЛЬКО ДЛЯ ЧТЕНИЯ
 * !!!! ЗАПИСЬ ОСУЩЕСТВЛЯЕТСЯ ТОЛЬКО ЧЕРЕЗ КОМПОНТЕНТ КОТОРЫЙ НЕ ИСПОЛЬЗУЕТ ACTIVERECORD ПРОСЛОЙКУ
 *
 * @author scriptbunny
 * @property-read integer $id
 * @property-read integer $user_id
 * @property-read integer $object_type
 * @property-read integer $object_id
 * @property-read integer $number
 * @property-read integer $created_at
 * @property-read integer $updated_at
 */
class ViewTrackingByUserRecord extends UnShardedActiveRecord
{
    /**
     * @param string $className
     *
     * @return ViewTrackingByUserRecord
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'views_tracking_by_users';
    }

    public function beforeSave()
    {
        return false; // table is read-only
    }

    public function beforeDelete()
    {
        return false; // table is read-only
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function userId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.user_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function objectType($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.object_type' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function objectTypeIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'object_type', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return $this
     */
    public function objectId($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addColumnCondition([
            $alias . '.object_id' => $value,
        ]);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function objectIdIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.' . 'object_id', $values);
        return $this;
    }

    /**
     * @param integer[] $values
     *
     * @return static
     */
    public function objectIdNotIn($values)
    {
        $values = Cast::toUIntArr($values);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.' . 'object_id', $values);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdAtFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'created_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function createdAtTo($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'created_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function createdAtBetween($from, $to)
    {
        $this->createdAtFrom($from);
        $this->createdAtTo($to);
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function updatedAtFrom($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'updated_at' . '>=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $value
     *
     * @return static
     */
    public function updatedAtTo($value)
    {
        $value = Cast::toUInt($value);

        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addCondition($alias . '.' . 'updated_at' . '<=' . Cast::toStr($value));
        return $this;
    }

    /**
     * @param integer $from
     * @param integer $to
     *
     * @return static
     */
    public function updatedAtBetween($from, $to)
    {
        $this->createdAtFrom($from);
        $this->createdAtTo($to);
        return $this;
    }
} 
