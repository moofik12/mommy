<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\CategoryGroups;

/**
 * Class TargetFilter
 * Фильтр товаров по ЦА
 */
class TargetFilter extends ProductFilter
{
    /**
     * Значение фильтра, типа ProductTargets::TARGET_WOMAN
     *
     * @var string|null
     **/
    public $value;

    /**
     * @return string
     */
    public function name()
    {
        return 'target';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Categories');
    }

    /**
     * @inheritdoc
     */
    public function isApplicable()
    {
        return true;
    }

    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable()) {
            return $selector;
        }
        $this->validValue();

        if ($this->value === null) {
            return $selector;
        }
        $categoryGroups = CategoryGroups::instance();
        $internalCategories = $categoryGroups->getCategories($this->value);

        $selector->categories($internalCategories);

        $targets = $categoryGroups->getTarget($this->value);
        if ($targets !== false) {
            $selector->targets($targets);
        }
        return $selector;
    }

    /**
     * @return bool
     */
    public function validValue()
    {
        if (in_array($this->value, array_keys($this->getItems()))) {
            return true;
        }
        $this->value = null;
        return false;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        $categoryGroups = CategoryGroups::instance();
        if (!$this->isApplicable() && !$categoryGroups->isValid($this->value)) {
            return 0;
        }
        $selector = EventProductRecord::model();
        if ($onlyActive) {
            $selector->eventIdIn($this->getActiveEventIds());
        }
        $categoryGroups = CategoryGroups::instance();
        $internalCategories = $categoryGroups->getCategories($this->value);
        $selector->categories($internalCategories);

        $targets = $categoryGroups->getTarget($this->value);
        if ($targets !== false) {
            $selector->targets($targets);
        }
        return $selector->count();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }
        return [
            CategoryGroups::CATEGORY_MOMS => \Yii::t('common', 'For moms'),
            CategoryGroups::CATEGORY_DADS => \Yii::t('common', 'For dads'),
            CategoryGroups::CATEGORY_GIRLS => \Yii::t('common', 'Girls'),
            CategoryGroups::CATEGORY_BOYS => \Yii::t('common', 'Boys'),
            CategoryGroups::CATEGORY_HOME => \Yii::t('common', 'Household products'),
            CategoryGroups::CATEGORY_TOYS => \Yii::t('common', 'Toys and books'),
        ];
    }
}
