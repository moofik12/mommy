<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Product\CategoryGroups;

/**
 * Class SizeFilter
 * Фильтр товаров по размерам
 */
class SizeFilter extends DerivativeFilterAbstract
{
    const ITEMS_CACHE = 80;
    public $values;

    /**
     * @return string
     */
    public function name()
    {
        return 'sizes';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Dimensions');
    }

    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return $selector;
        }
        $selector->sizes($this->values);
        return $selector;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return 0;
        }

        $selector = EventProductRecord::model();
        if ($onlyActive) {
            $selector->eventIdIn($this->getActiveEventIds());
        }
        $selector->sizes($this->values);
        return $selector->count();
    }

    /**
     * @return array|false
     */
    public function getItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }

        $target = $this->superFilter->targetFilter->value;
        $categoryGroups = CategoryGroups::instance();
        $internalCategories = $categoryGroups->getCategories($target);

        $targets = $categoryGroups->getTarget($target);

        $sectionFilter = $this->superFilter->sectionFilter;

        if ($sectionFilter->isParent) {
            $sectionIds = $sectionFilter->childValues;
        } else {
            $sectionIds = $sectionFilter->values;
        }

        $productIds = ProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->sectionIn($sectionIds)
            ->findColumnDistinct('id');

        $activeEventIds = $this->getActiveEventIds();

        $selector = EventProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->productIdIn($productIds)
            ->categories($internalCategories)
            ->eventIdIn($activeEventIds);

        if ($targets !== false) {
            $selector->targets($targets);// фильтр по ЦА
        }

        $sizes = $selector->findColumnDistinct('size');
        if ($sizes === false || count($sizes) === 1) {
            // если размеров нет, или есть только один размер тогда скрываем фильтр
            return [];
        }

        $sizes = self::sortSizes($sizes);

        return $sizes;
    }

    protected static function sortSizes($sizes)
    {
        // так чтобы часть размеров пошла в одну категорию

        \Yii::t('common', 'year');
        \Yii::t('common', 'years');
        \Yii::t('common', 'years');

        static $replacements = [
            'год' => 'years',
            'года' => 'years',
        ];

        // группы
        $sizeGroups = [
            'ungrouped' => [],
        ];

        foreach ($sizes as $size) {
            $key = explode(' ', $size, 2);

            if (count($key) !== 2) {
                $sizeGroups['ungrouped'][] = $size;
                continue;
            }

            $key = substr(strtr($key[1], $replacements), 0, 3);

            if (!isset($sizeGroups[$key])) {
                $sizeGroups[$key] = [];
            }
            $sizeGroups[$key][] = $size;
        }

        $sizes = [];
        foreach ($sizeGroups as $values) {
            natsort($values);
            $sizes = array_merge($sizes, $values);
        }

        return $sizes;
    }
}
