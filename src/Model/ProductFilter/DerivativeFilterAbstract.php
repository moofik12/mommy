<?php

namespace MommyCom\Model\ProductFilter;

/**
 * Class DerivativeFilterAbstract
 * Класс для специального подтипа фильтров которые применимы только после того как выбрана ЦА а так же раздел
 */
abstract class DerivativeFilterAbstract extends ProductFilter
{
    /**
     * @inheritdoc
     */
    public function isApplicable()
    {
        $superFilter = $this->superFilter;
        $targetFilter = $superFilter->targetFilter;
        $sectionFilter = $superFilter->sectionFilter;

        // если в данный момент фильтры по категориям не доступны то данный фильтр так же не доступны
        if (!$targetFilter->isApplicable() || !$sectionFilter->isApplicable()) {
            return false;
        }

        // фильтр становится доступным только после того как указан ЦА и раздел
        return !empty($targetFilter->value) && !empty($sectionFilter->values);
    }
}
