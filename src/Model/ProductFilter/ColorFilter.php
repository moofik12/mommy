<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductColorCodes;

/**
 * Class ColorFilter
 * Фильтр товаров по цвету
 */
class ColorFilter extends DerivativeFilterAbstract
{
    const ITEMS_CACHE = 80;
    const APPLY_CACHE = 80;
    public $values;

    /**
     * @return string
     */
    public function name()
    {
        return 'color';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Colour');
    }

    /**
     * @inheritdoc
     */
    public function isApplicable()
    {
        if (!parent::isApplicable()) {
            return false;
        }

        return true;
    }

    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return $selector;
        }
        $productIds = ProductRecord::model()
            ->cache(self::APPLY_CACHE)
            ->colorCodeIn($this->values)
            ->findColumnDistinct('id');
        $selector->eventIdIn($this->getActiveEventIds())->productIdIn($productIds);
        return $selector;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        if ($this->isApplicable() && !empty($this->values)) {
            $productIds = ProductRecord::model()
                ->colorCodeIn($this->values)
                ->findColumnDistinct('id');
            $selector = EventProductRecord::model();
            if ($onlyActive) {
                $selector->eventIdIn($this->getActiveEventIds());
            }
            $selector->productIdIn($productIds);
            return $selector->count();
        }
        return 0;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }

        $sectionFilter = $this->superFilter->sectionFilter;

        $target = $this->superFilter->targetFilter->value;

        $categoryGroups = CategoryGroups::instance();
        $internalCategories = $categoryGroups->getCategories($target);

        $targets = $categoryGroups->getTarget($target);

        $selector = EventProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->eventIdIn($this->getActiveEventIds())
            ->categories($internalCategories);

        if ($targets !== false) {
            $selector->targets($targets);// фильтр по ЦА
        }

        $productIds = $selector->findColumnDistinct('product_id');

        if ($sectionFilter->isParent) {
            $sectionIds = $sectionFilter->childValues;
        } else {
            $sectionIds = $sectionFilter->values;
        }

        $activeProductIds = ProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->idIn($productIds)
            ->sectionIn($sectionIds)
            ->findColumnDistinct('id');

        $colorCodes = ProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->idIn($activeProductIds)
            ->findColumnDistinct('color_code');

        return $colorCodes === false ? [] : ProductColorCodes::instance()->getLabels($colorCodes);
    }

    /**
     * @return array
     */
    public function getSelectedValues()
    {
        return ProductColorCodes::instance()->getLabels($this->values);
    }
}
