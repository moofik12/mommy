<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;

/**
 * Interface ProductFilterInterface
 */
interface ProductFilterInterface
{
    /**
     * Название фильтра для использования в GET-параметрах
     *
     * @return string
     */
    public function name();

    /**
     * Название фильтра
     *
     * @return string
     */
    public function label();

    /**
     * Доступен ли фильтр в данный момент
     *
     * @return boolean
     */
    public function isApplicable();

    /**
     * Применить фильтр
     *
     * @param EventProductRecord $selector
     *
     * @return mixed
     */
    public function apply(EventProductRecord $selector);

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return int
     */
    public function totalCount($refresh = false, $onlyActive = true);

}
