<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;

/**
 * Class PriceFilter
 * Фильтр товаров по цене
 */
class PriceFilter extends ProductFilter
{
    const PLACEHOLDER_CACHE = 80;

    /**
     * GET-param
     * @var  int
     */
    public $valueMin;

    /**
     * GET-param
     * @var  int
     */
    public $valueMax;

    /**
     * @return string
     */
    public function name()
    {
        return 'price';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Price list');
    }

    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable()) {
            return $selector;
        }

        $min = (int)max($this->valueMin, 0);
        $max = (int)max($this->valueMax, $min);

        $this->valueMin = $min;
        $this->valueMax = $max;

        if ($min > 0) {
            $selector->priceAtGreater($this->valueMin);
        }

        if ($max > $min) {
            $selector->priceAtLower($this->valueMax);
        }

        return $selector;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        if (!$this->isApplicable()) {
            return 0;
        }

        $selector = EventProductRecord::model();
        if ($onlyActive) {
            $selector->eventIdIn($this->getActiveEventIds());
        }

        $min = (int)max($this->valueMin, 0);
        $max = (int)max($this->valueMax, $min);

        if ($min > 0) {
            $selector->priceAtGreater($this->valueMin);
        }

        if ($max > $min) {
            $selector->priceAtLower($this->valueMax);
        }

        return $selector->count();
    }

    /**
     * @return int
     */
    public function getAvailableFrom()
    {
        $eventProducts = EventProductRecord::model()
            ->eventIdIn($this->getActiveEventIds());

        return (int)$eventProducts->cache(self::PLACEHOLDER_CACHE)->findColumnMin('price');
    }

    /**
     * @return int
     */
    public function getAvailableTo()
    {
        $eventProducts = EventProductRecord::model()
            ->eventIdIn($this->getActiveEventIds());

        return (int)$eventProducts->cache(self::PLACEHOLDER_CACHE)->findColumnMax('price');
    }
}
