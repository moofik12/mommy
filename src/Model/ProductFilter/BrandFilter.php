<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductBrands;

/**
 * Class BrandFilter
 * Фильтр товаров по бренду
 */
class BrandFilter extends DerivativeFilterAbstract
{
    const ITEMS_CACHE = 80;

    public $values;

    /**
     * @return string
     */
    public function name()
    {
        return 'brands';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Brand');
    }

    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return $selector;
        }

        $selector->brandIds($this->values);

        return $selector;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return 0;
        }

        $selector = EventProductRecord::model();
        if ($onlyActive) {
            $selector->eventIdIn($this->getActiveEventIds());
        }

        $selector->brandIds($this->values);

        return $selector->count();
    }

    /**
     * @return array|ProductBrands
     */
    public function getItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }
        $target = $this->superFilter->targetFilter->value;

        $categoryGroups = CategoryGroups::instance();
        $internalCategories = $categoryGroups->getCategories($target);

        $targets = $categoryGroups->getTarget($target);

        $sectionFilter = $this->superFilter->sectionFilter;

        if ($sectionFilter->isParent) {
            $sectionIds = $sectionFilter->childValues;
        } else {
            $sectionIds = $sectionFilter->values;
        }

        $selector = EventProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->eventIdIn($this->getActiveEventIds())
            ->categories($internalCategories);

        if ($targets !== false) {
            $selector->targets($targets);// фильтр по ЦА
        }

        $productIds = $selector->findColumnDistinct('product_id');

        $productIds = ProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->idIn($productIds)
            ->sectionIn($sectionIds)
            ->findColumnDistinct('id');

        $eventProductSelector = EventProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->eventIdIn($this->getActiveEventIds())
            ->productIdIn($productIds);

        $productBrandIds = $eventProductSelector->findColumnDistinct('brand_id');
        $productBrands = BrandRecord::model()->cache(self::ITEMS_CACHE)->idIn($productBrandIds)->orderBy('name')->findAll();;
        return $productBrands;
    }

    /**
     * @return array
     */
    public function getSelectedValues()
    {
        $brands = $this->getItems();
        $selectedBrands = [];
        foreach ($brands as $brand) {
            /** @var $brand BrandRecord */
            if (in_array($brand->id, $this->values)) {
                $selectedBrands[] = $brand;
            }
        }
        return $selectedBrands;
    }
}
