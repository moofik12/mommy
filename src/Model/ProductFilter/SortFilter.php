<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;

/**
 * Class SortFilter
 */
class SortFilter extends ProductFilter
{
    const SORT_ASC = 'price_asc';
    const SORT_DESC = 'price_desc';
    const SORT_NONE = 'none';
    public $value = self::SORT_ASC;

    /**
     * @return string
     */
    public function name()
    {
        return 'sort';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Sort');
    }

    public function isApplicable()
    {
        return true;
    }

    public function apply(EventProductRecord $selector)
    {
        if ($this->isApplicable() && $this->isValid($this->value)) {
            if ($this->value == self::SORT_ASC) {
                $selector->orderBy('price', 'ASC', 'start');
            } elseif ($this->value == self::SORT_DESC) {
                $selector->orderBy('price', 'DESC', 'start');
            } elseif ($this->value == self::SORT_NONE) {
                $selector->orderBy('number_sold_real', 'DESC', 'start');
            }
        }
        return $selector;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    protected function isValid($value)
    {
        return in_array($value, array_keys($this->getItems()));
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if ($this->isApplicable()) {
            return [
                self::SORT_ASC => \Yii::t('common', 'Price low to high'),
                self::SORT_DESC => \Yii::t('common', 'Price high to low'),
                self::SORT_NONE => \Yii::t('common', 'Recommended'),
            ];
        }
        return [];
    }

    public function totalCount($refresh = false, $onlyActive = true)
    {
        return 0;
    }

    public function getValueName()
    {
        $items = $this->getItems();
        return isset($items[$this->value]) ? $items[$this->value] : '';
    }

}
