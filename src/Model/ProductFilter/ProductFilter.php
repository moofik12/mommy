<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventRecord;

/**
 * Class ProductFilter
 */
abstract class ProductFilter implements ProductFilterInterface
{
    /** @var ProductsFilter */
    public $superFilter;

    protected static $activeEventIds = [];

    public function __construct(ProductsFilter $superFilter)
    {
        $this->superFilter = $superFilter;
    }

    /**
     * @inheritdoc
     */
    public function isApplicable()
    {
        return true;
    }

    /**
     * Ид активных акций
     *
     * @return array|false|null
     */
    public function getActiveEventIds()
    {
        $activeEventIds = self::$activeEventIds;
        if ($activeEventIds === []) {
            $activeEventIds = EventRecord::model()
                ->cache(300)
                ->isVirtual(false)
                ->onlyVisible()
                ->onlyPublished()
                ->onlyTimeActive()
                ->isDeleted(false)
                ->findColumnDistinct('id');
            self::$activeEventIds = $activeEventIds;
        }
        return $activeEventIds;
    }
}
