<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductTargets;

/**
 * Class AgeFilter
 * Фильтр товаров по возрасту
 */
class AgeFilter extends DerivativeFilterAbstract
{
    public $values;

    /**
     * @inheritdoc
     */
    public function name()
    {
        return 'ages';
    }

    /**
     * @inheritdoc
     */
    public function label()
    {
        return \Yii::t('common', 'By age');
    }

    /**
     * @inheritdoc
     */
    public function isApplicable()
    {
        if (!parent::isApplicable()) {
            return false;
        }

        $targetFilter = $this->superFilter->targetFilter;
        $targets = CategoryGroups::instance()->getTarget((string)$targetFilter->value);

        // Данный фильтр неприменим когда выбрана ЦА которая не поддерживает возраст
        if (empty($targets)) {
            return false;
        }

        $targetProvider = ProductTargets::instance();

        foreach ($targets as $target) {
            // если один из $target поддерживает фильтрацию по возрасту тогда можно применять фильтр
            if ($targetProvider->getIsSupportsAge($target)) {
                return true;
            }
        }

        // небыло ни одного ЦА который поддерживает фильтрацию по возрасту
        return false;
    }

    /**
     * @param EventProductRecord $selector
     *
     * @return EventProductRecord
     */
    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return $selector;
        }

        $ageMin = 0;
        $ageMax = 0;
        foreach ($this->values as $value) {
            $ageRange = AgeGroups::instance()->getAgeRange($value);
            if ($ageRange[0] < $ageMin) {
                $ageMin = $ageRange[0];
            }

            if ($ageRange[1] > $ageMax || $ageMax == 0) {
                $ageMax = $ageRange[1];
            }
        }

        if ($ageMax > 0) {
            $selector->ageBetween($ageMin, $ageMax);
        }

        return $selector;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return 0;
        }

        $selector = EventProductRecord::model();
        $ageMin = 0;
        $ageMax = 0;
        foreach ($this->values as $value) {
            $ageRange = AgeGroups::instance()->getAgeRange($value);

            if ($ageRange[0] < $ageMin) {
                $ageMin = $ageRange[0];
            }

            if ($ageRange[1] > $ageMax || $ageMax == 0) {
                $ageMax = $ageRange[1];
            }
        }

        if ($ageMax > 0) {
            $selector->ageBetween($ageMin, $ageMax);
        }

        if ($onlyActive) {
            $selector->eventIdIn($this->getActiveEventIds());
        }
        return $selector->count();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }
        return AgeGroups::instance()->getLabels();
    }
}
