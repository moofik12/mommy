<?php

namespace MommyCom\Model\ProductFilter;

use CDbDataReader;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\GroupedProducts;

/**
 * Class ProductsFilter
 * Фильтр товаров
 */
class ProductsFilter implements ProductFilterInterface
{
    const DEFAULT_CACHE = 80;

    /** @var PriceFilter */
    public $priceFilter;

    /** @var SectionFilter */
    public $sectionFilter;

    /** @var TargetFilter */
    public $targetFilter;

    /** @var SizeFilter */
    public $sizeFilter;

    /** @var ColorFilter */
    public $colorFilter;

    /** @var BrandFilter */
    public $brandFilter;

    /** @var AgeFilter */
    public $ageFilter;

    /** @var  SortFilter */
    public $sortFilter;

    public function __construct()
    {
        $this->sectionFilter = new SectionFilter($this);
        $this->priceFilter = new PriceFilter($this);
        $this->targetFilter = new TargetFilter($this);
        $this->sizeFilter = new SizeFilter($this);
        $this->colorFilter = new ColorFilter($this);
        $this->brandFilter = new BrandFilter($this);
        $this->ageFilter = new AgeFilter($this);
        $this->sortFilter = new SortFilter($this);
    }

    public function name()
    {
    }

    public function label()
    {
    }

    /**
     * Доступен ли фильтр в данный момент
     *
     * @return boolean
     */
    public function isApplicable()
    {
        return true;
    }

    /**
     * @param EventProductRecord $selector
     * @param bool $refresh
     *
     * @return EventProductRecord|mixed
     */
    public function apply(EventProductRecord $selector, $refresh = false)
    {
        if (!$refresh) {
            $selector->cache(self::DEFAULT_CACHE);
        }
        $selector->eventIdIn($this->sectionFilter->getActiveEventIds());
        $selector = $this->targetFilter->apply($selector);
        $selector = $this->sectionFilter->apply($selector);
        $selector = $this->priceFilter->apply($selector);
        $selector = $this->brandFilter->apply($selector);
        $selector = $this->colorFilter->apply($selector);
        $selector = $this->sizeFilter->apply($selector);
        $selector = $this->ageFilter->apply($selector);
        $selector = $this->sortFilter->apply($selector);

        return $selector;
    }

    /**
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return CDbDataReader|mixed|string
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        $selector = EventProductRecord::model();
        $eventProductSelector = $this->apply($selector, $refresh);

        $productsIds = $eventProductSelector->findColumnDistinct('product_id');

        $eventProducts = $eventProductSelector->productIdIn($productsIds)->cache(self::DEFAULT_CACHE)->findAll();
        $groupedProducts = GroupedProducts::fromProducts($eventProducts);

        return count($groupedProducts);
    }
}
