<?php

namespace MommyCom\Model\ProductFilter;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Product\CategoryGroups;

/**
 * Class SectionFilter
 * Фильтр продуктов по секциям
 */
class SectionFilter extends ProductFilter
{
    const ITEMS_CACHE = 80;
    const APPLY_CACHE = 80;

    /**
     * @var array
     */
    public $values;

    /** @var bool */
    public $isParent = false;

    /**
     * @var array
     */
    public $childValues = [];

    /** @var null|ProductSectionRecord */
    public $parent = null;

    /**
     * @return string
     */
    public function name()
    {
        return 'sections';
    }

    /**
     * @return string
     */
    public function label()
    {
        return \Yii::t('common', 'Categories');
    }

    /**
     * @inheritdoc
     */
    public function isApplicable()
    {
        // данный фильтр становится доступным только после того как указан ЦА

        $targetFilter = $this->superFilter->targetFilter;
        return !empty($targetFilter->value);
    }

    public function apply(EventProductRecord $selector)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return $selector;
        }

        $sectionIds = $this->values;
        $this->isParent = false;
        if (count($this->values) == 1) {
            $this->parent = ProductSectionRecord::model()->cache(self::APPLY_CACHE)->findByPk($this->values[0]);
            if ($this->parent !== null) {
                if ($this->parent->parent_id == 0) {
                    $this->isParent = true;
                    $sectionIds = ProductSectionRecord::model()
                        ->cache(self::APPLY_CACHE)
                        ->parentId($this->parent->id)
                        ->findColumnDistinct('id');
                    $this->childValues = $sectionIds;
                }
            }
        }

        $productIds = ProductRecord::model()
            ->cache(self::APPLY_CACHE)
            ->sectionIn($sectionIds)
            ->findColumnDistinct('id');

        $selector->eventIdIn($this->getActiveEventIds())->productIdIn($productIds);

        return $selector;
    }

    /**
     * Возвращает общее количество товаров
     * Другие фильтры не учитываются
     *
     * @param bool $refresh
     * @param bool $onlyActive
     *
     * @return mixed
     */
    public function totalCount($refresh = false, $onlyActive = true)
    {
        if (!$this->isApplicable() || empty($this->values)) {
            return 0;
        }

        $productIds = ProductRecord::model()
            ->cache(self::APPLY_CACHE)
            ->sectionIn($this->values)
            ->findColumnDistinct('id');

        $selector = EventProductRecord::model();
        if ($onlyActive) {
            $selector->eventIdIn($this->getActiveEventIds());
        }

        $selector->productIdIn($productIds);

        return $selector->count();
    }

    /**
     * @return ProductSectionRecord[]
     */
    public function getItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }

        $target = $this->superFilter->targetFilter->value;

        $categoryGroups = CategoryGroups::instance();
        $internalCategories = $categoryGroups->getCategories($target);

        $targets = $categoryGroups->getTarget($target);

        $activeEventIds = $this->getActiveEventIds();

        $selector = EventProductRecord::model()
            ->cache(self::ITEMS_CACHE)
            ->with('product')
            ->categories($internalCategories)
            ->eventIdIn($activeEventIds);

        if ($targets !== false) {
            $selector->targets($targets);// фильтр по ЦА
        }

        $eventProducts = $selector->findAll();

        $sectionIds = [];
        foreach ($eventProducts as $eventProduct) {
            if ((!in_array($eventProduct->product->section_id, $sectionIds)) && $eventProduct->product->section_id > 0) {
                $sectionIds[] = $eventProduct->product->section_id;
            }
        }
        unset($eventProduct);

        return ProductSectionRecord::model()->cache(self::ITEMS_CACHE)->idIn($sectionIds)->onlyChild()->orderBy('name')->findAll();
    }

    /**
     * @return array
     */
    public function getGroupItems()
    {
        if (!$this->isApplicable()) {
            return [];
        }

        static $parents = [];
        $items = $this->getItems();
        $groupItems = [];
        foreach ($items as $section) {
            /** @var $section ProductSectionRecord */

            if (isset($parents[$section->parent_id])) {
                $parent = $parents[$section->parent_id];
            } else {
                $parents[$section->parent_id] = $section->parent;
                $parent = $section->parent;
            }

            if (isset($groupItems[$parent->name])) {
                $groupItems[$parent->name]['children'][$section->id] = $section;
                if (in_array($section->id, $this->values)) {
                    $groupItems[$parent->name]['isSelectedChild'] = true;
                }
            } else {
                $isSelectedChild = in_array($section->id, $this->values);
                if ($this->isParent) {
                    if ($this->parent->id == $parent->id) {
                        $isSelectedChild = true;
                    }
                }
                $groupItems[$parent->name] = [
                    'parent' => $parent,
                    'children' => [$section->id => $section],
                    'isSelectedChild' => $isSelectedChild,
                ];
            }
        }
        unset($section);
        ksort($groupItems);
        return $groupItems;
    }

    /**
     * @return array
     */
    public function getSelectedValues()
    {
        $items = $this->getItems();
        $selectedList = [];
        foreach ($items as $item) {
            /** @var $item ProductSectionRecord */
            if (in_array($item->id, $this->values)) {
                $selectedList[] = $item;
            }
        }
        return $selectedList;
    }
}
