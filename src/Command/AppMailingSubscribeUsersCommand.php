<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Service\Distribution\Handler\SubscribeUserHandler;
use MommyCom\YiiComponent\MailerServices\UniSender;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppMailingSubscribeUsersCommand extends Command
{
    protected static $defaultName = 'app:mailing:subscribe-users';

    /**
     * @var UniSender
     */
    private $mailingService;

    public function __construct(UniSender $mailingService)
    {
        $this->mailingService = $mailingService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Для каждого события получает пользователей, которые не подписаны на списки рассылок' .
            ' и подписывает их на соответсвующие списки рассылок');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        try {
            /** @var MailingDataRecord[] $result */
            $result = MailingDataRecord::model()->getUnsubscribedUsers();
            $handler = new SubscribeUserHandler();

            foreach ($result as $dataRecord) {
                $eventRecord = MailingEventRecord::model()->findByPk($dataRecord->distribution_event_id);

                $this->initMailingService($dataRecord, $eventRecord);
                $handler->process($dataRecord, $eventRecord, $this->mailingService);
            }
        } catch (\CException $e) {
            $io->error((string)$e);
        }
    }

    /**
     * Инициализация сервиса рассылок
     *
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     */
    private function initMailingService(MailingDataRecord $dataRecord, MailingEventRecord $eventRecord)
    {
        $this->mailingService->init($this->mailingService->getListId($dataRecord->country, $eventRecord->name));
    }
}
