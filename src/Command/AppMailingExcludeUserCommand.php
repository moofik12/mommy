<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Service\Distribution\Handler\ExcludeUserHandler;
use MommyCom\YiiComponent\MailerServices\UniSender;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppMailingExcludeUserCommand extends Command
{
    private const ARG_EMAIL = 'email';

    protected static $defaultName = 'app:mailing:exclude-user';
    /**
     * @var UniSender
     */
    private $mailingService;

    public function __construct(UniSender $mailingService)
    {
        $this->mailingService = $mailingService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Ищет все записи о пользователе с указаным email, где пользователь подписан на группу' .
                ' рассылок и исключает пользователя из соответсвующих списков рассылок')
            ->addArgument(self::ARG_EMAIL, InputArgument::REQUIRED, 'Argument description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument(self::ARG_EMAIL);

        try {
            $result = MailingDataRecord::model()->getUserRecordsByEmail($email);
            $handler = new ExcludeUserHandler();

            foreach ($result as $dataRecord) {
                $eventRecord = MailingEventRecord::model()->findByPk($dataRecord->distribution_event_id);

                $this->initMailingService($dataRecord, $eventRecord);
                $handler->process($dataRecord, $eventRecord, $this->mailingService);
            }
        } catch (\CException $e) {
            $io->error((string)$e);
        }
    }

    /**
     * Инициализация сервиса рассылок
     *
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     */
    private function initMailingService(MailingDataRecord $dataRecord, MailingEventRecord $eventRecord)
    {
        $this->mailingService->init($this->mailingService->getListId($dataRecord->country, $eventRecord->name));
    }
}
