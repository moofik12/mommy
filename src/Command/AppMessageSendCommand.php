<?php

namespace MommyCom\Command;

use MommyCom\YiiComponent\Application\ApplicationTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\StyleInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppMessageSendCommand extends Command
{
    use ApplicationTrait;

    private const OPTION_TYPE = 'type';
    private const TYPE_EMAIL = 'email';
    private const TYPE_SMS = 'sms';
    private const TYPE_ALL = 'all';
    private const TYPE_DEFAULT = self::TYPE_ALL;

    protected static $defaultName = 'app:message:send';

    private static $types = [
        self::TYPE_EMAIL => true,
        self::TYPE_SMS => true,
        self::TYPE_ALL => true,
    ];

    protected function configure()
    {
        $this
            ->setDescription('Рассылает письма и/или СМС сообщения')
            ->addOption(self::OPTION_TYPE, null, InputOption::VALUE_OPTIONAL, 'Тип отправляемых сообщений (email|sms|all)', self::TYPE_DEFAULT);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $type = $input->getOption(self::OPTION_TYPE);
        $this->ensureValidType($type);

        foreach ($this->getHandlers($type, $io) as $handler) {
            $handler();
        }
    }

    /**
     * @param string $type
     *
     * @throws \InvalidArgumentException if type is invalid
     */
    private function ensureValidType(string $type)
    {
        if (!isset(self::$types[$type])) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid type %s. Allowed types: %s',
                $type,
                implode(', ', self::$types)
            ));
        }
    }

    private function getHandlers(string $type, StyleInterface $io): iterable
    {
        if ('all' === $type || 'email' === $type) {
            yield function () use ($io) {
                $errorMessage = '';

                try {
                    $result = $this->app()->mailer->sendWaiting();
                    if ($result['errors']) {
                        $errorMessage = 'Ошибки в отправке E-mail, ошибок: ' . $result['errors'];
                    }
                } catch (\Exception $e) {
                    $errorMessage = 'Ошибка при отправке E-mail: ' . $e->getMessage();
                }

                if ($errorMessage) {
                    \Yii::log($errorMessage, \CLogger::LEVEL_ERROR, 'sending.email');
                    $io->error($errorMessage);
                }
            };
        }

        if ('all' === $type || 'sms' === $type) {
            yield function () use ($io) {
                $errorMessage = '';

                try {
                    $result = $this->app()->sms->sendWaiting();
                    if ($result['errors']) {
                        $errorMessage = 'Ошибки в отправке SMS, ошибок: ' . $result['errors'];
                    }
                } catch (\Exception $e) {
                    $errorMessage = 'Ошибка при отправке SMS: ' . $e->getMessage();
                }

                if ($errorMessage) {
                    \Yii::log($errorMessage, \CLogger::LEVEL_ERROR, 'sending.sms');
                    $io->error($errorMessage);
                }
            };
        }
    }
}
