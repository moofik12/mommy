<?php

namespace MommyCom\Command;

use MommyCom\Model\ViewsTracking\ViewsTracking;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppViewsStatisticsRemoveOutdatedCommand extends Command
{
    protected static $defaultName = 'app:views-statistics:remove-outdated';

    protected function configure()
    {
        $this->setDescription('Удаляет устаревшую статистику');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tracker = new ViewsTracking();
        $tracker->removeOutdated();
    }
}
