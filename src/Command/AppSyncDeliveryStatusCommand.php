<?php

namespace MommyCom\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderTracking;
use MommyCom\Repository\OrderRepository;
use MommyCom\Repository\OrderTrackingRepository;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Order\MommyOrderManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppSyncDeliveryStatusCommand extends Command
{
    private const IGNORE_OLDER_THEN_DAYS = 30; // игрорировать заказы старше 30 дней

    protected static $defaultName = 'app:sync-delivery-status';

    /**
     * @var AvailableDeliveries
     */
    private $availableDeliveries;

    /**
     * @var MommyOrderManager
     */
    private $mommyOrderManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderTrackingRepository
     */
    private $orderTrackingRepository;

    /**
     * AppSyncDeliveryStatusCommand constructor.
     *
     * @param AvailableDeliveries $availableDeliveries
     * @param MommyOrderManager $mommyOrderManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        AvailableDeliveries $availableDeliveries,
        MommyOrderManager $mommyOrderManager,
        EntityManagerInterface $entityManager
    ) {
        $this->availableDeliveries = $availableDeliveries;
        $this->mommyOrderManager = $mommyOrderManager;
        $this->entityManager = $entityManager;

        $this->orderRepository = $entityManager->getRepository(Order::class);
        $this->orderTrackingRepository = $entityManager->getRepository(OrderTracking::class);

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Fetch delivery statuses be API and save it locally');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $currentTime = CurrentTime::getUnixTimestamp();
        $threshold = CurrentTime::getDateTimeImmutable()->modify(sprintf('-%d days', self::IGNORE_OLDER_THEN_DAYS));

        try {
            $notSynchronized = $this->orderTrackingRepository->findNotSynchronized($threshold);

            foreach ($notSynchronized as $tracking) {
                $deliveryType = $tracking->getOrderDeliveryType();

                if (!$this->availableDeliveries->hasDelivery($deliveryType)) {
                    $io->warning(sprintf('Tracking #%d has not available delivery type.', $tracking->getId()));

                    continue;
                }

                $tracking = $this->synchronizeStatus($tracking, $deliveryType, $currentTime);

                $this->entityManager->flush($tracking);
            }

            $io->success('Done.');
        } catch (ORMException $e) {
            if (isset($tracking)) {
                $io->error(sprintf('Cannot save tracking #%d, because: %s.', $tracking->getId(), $e->getMessage()));
            } else {
                $io->error($e->getMessage());
            }
        }
    }

    private function synchronizeStatus(OrderTracking $tracking, int $deliveryType, int $currentTime): OrderTracking
    {
        $mommyOrder = $this->mommyOrderManager->find($tracking->getOrderId());
        $deliveryApi = $this->availableDeliveries->getDelivery($deliveryType)->getApi();
        $deliveryStatus = $deliveryApi->getDeliveryStatus($mommyOrder);

        if ($tracking->getStatus() !== $deliveryStatus) {
            if (OrderTracking::STATUS_DELIVERED === $deliveryStatus) {
                $tracking->setDeliveredDate($currentTime);
            }

            $tracking->addStatusToHistory($deliveryStatus, 0, 0);
            $tracking->setStatusPrev($tracking->getStatus());
            $tracking->setStatus($deliveryStatus);
            $tracking->setStatusChangedAt($currentTime);
        }

        $tracking->setSynchronizedAt($currentTime);

        return $tracking;
    }
}
