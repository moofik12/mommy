<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\EventRecord;
use MommyCom\Service\CurrentTime;
use MommyCom\YiiComponent\SupplierOrder\NotificationDataFactory;
use MommyCom\YiiComponent\SupplierOrder\SupplierOrderNotification;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppMessageSendSupplierRequestCommand extends Command
{
    protected static $defaultName = 'app:message:send-supplier-request';

    private $notification;

    /**
     * @var NotificationDataFactory
     */
    private $notificationDataFactory;

    public function __construct(
        NotificationDataFactory $notificationDataFactory,
        SupplierOrderNotification $supplierOrderNotification
    ) {
        $this->notificationDataFactory = $notificationDataFactory;
        $this->notification = $supplierOrderNotification;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Отправляет ссылку поставщику на интерфейс подтверждения заказа');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $time = CurrentTime::getDateTimeImmutable()
            ->modify('-7 day')
            ->getTimestamp();

        $events = EventRecord::model()->onlyTimeClosed()
            ->timeEndsAfter($time)
            ->findAll();

        /**
         * Не обернуто в транзакцию за ненадобностью, т.к. акции, по которым не будут отправлены письма
         * попадут в список для отправки во время следующего вызова комманды.
         *
         * @var EventRecord $event
         */
        foreach ($events as $event) {
            if (!$event->supplier_notified) {
                $notificationData = $this->notificationDataFactory->create($event->id);
                $this->notification->send($notificationData);
                $event->supplier_notified = true;
                $event->save();
            }
        }
    }
}
