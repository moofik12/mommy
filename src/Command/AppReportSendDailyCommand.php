<?php

namespace MommyCom\Command;

use Doctrine\ORM\EntityManager;
use MommyCom\Entity\Order;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Repository\OrderRepository;
use MommyCom\Service\TelegramBot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class AppReportSendDailyCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:report:send-daily';

    protected function configure()
    {
        $this->setDescription('Sends orders statistics for the last 24 hours to telegram');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var TelegramBot $telegramBot */
        $telegramBot = $this->getContainer()->get(TelegramBot::class);

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var OrderRepository $orderRepository */
        $orderRepository = $em->getRepository(Order::class);

        $begin = new \Datetime("-24 hours");
        $end = new \DateTime("now");

        $allStats = [];

        if ($stats = $orderRepository->getDeliveredStatsForInterval($begin, $end)) {
            $allStats['Доставлено заказов за сутки'] = $stats['countTotal'];
            $allStats['Доставлено за сутки на сумму'] =  $stats['priceTotal'];
        }

        if ($stats = $orderRepository->getDeliveredStatsForInterval(new \DateTime("01 January 1970"), $end)) {
            $allStats['Доставлено заказов всего'] = $stats['countTotal'];
            $allStats['Доставлено всего на сумму'] = $stats['priceTotal'];
        }

        if ($stats = $orderRepository->getCreatedStatsForInterval($begin, $end)) {
            $allStats['Создано новых заказов за сутки'] = $stats['countTotal'];
            $allStats['Создано за сутки на сумму'] = $stats['priceTotal'];
        }

        if ($stats = $orderRepository->getCreatedStatsForInterval(new \DateTime("01 January 1970"), $end)) {
            $allStats['Создано заказов всего'] = $stats['countTotal'];
            $allStats['Создано всего на сумму'] = $stats['priceTotal'];
        }

        if ($stats = $orderRepository->getStatusStatsForInterval($begin, $end, OrderRecord::PROCESSING_CANCELLED)) {
            $allStats['Отменено заказов за сутки'] = $stats['countTotal'];
            $allStats['Отменено за сутки на сумму'] = $stats['priceTotal'];
        }

        if ($stats = $orderRepository->getShippedStatsForInterval($begin, $end)) {
            $allStats['Отправлено заказов за сутки'] = $stats['countTotal'];
            $allStats['Отправлено за сутки на сумму'] = $stats['priceTotal'];
        }

        $message = "";
        foreach ($allStats as $k => $v) {
            $message .= "$k: $v\n";
        }

        $baseUrl = getenv('BASE_URL');

        if ($telegramBot->getToken()) {
            $output->writeln("Sending the message $message");
            $telegramBot->sendMessage(
                getenv('DAILY_REPORT_TELEGRAM_CHAT_ID'),
                "Ежедневные показатели ($baseUrl):\n\n$message"
            );
        } else {
            $output->writeln("Couldn't send to telegram )-:");
        }

    }
}
