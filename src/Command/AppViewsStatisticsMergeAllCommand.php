<?php

namespace MommyCom\Command;

use MommyCom\Model\ViewsTracking\ViewsTracking;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppViewsStatisticsMergeAllCommand extends Command
{
    protected static $defaultName = 'app:views-statistics:merge-all';

    protected function configure()
    {
        $this->setDescription('Переносит статистику из временной таблицы');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        (new ViewsTracking())->mergeAll();
    }
}
