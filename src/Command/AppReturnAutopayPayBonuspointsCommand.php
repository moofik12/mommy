<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppReturnAutopayPayBonuspointsCommand extends Command
{
    protected static $defaultName = 'app:return-autopay:pay-bonuspoints';

    protected function configure()
    {
        $this->setDescription('Отмечает возврат и добавляет бонусов');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // начисление средств на внутренний счет
        $returns = OrderReturnRecord::model()
            ->refundType(OrderReturnRecord::REFUND_TYPE_BONUSPOINTS)
            ->status(OrderReturnRecord::STATUS_NEED_PAY)
            ->with('order')
            ->findAll();
        /** @var $returns OrderReturnRecord[] */

        foreach ($returns as $return) {
            $return->status = OrderReturnRecord::STATUS_PAYED;
            $return->save();

            $user = UserRecord::model()->findByPk($return->order_user_id);

            /** @noinspection PhpUnhandledExceptionInspection */
            $bonuspoints = new ShopBonusPoints();
            $bonuspoints->init($user);
            $bonuspoints->sendInner($return->price_refund, "Возврат товара из заказа №" . $return->order->idAligned);
        }
    }
}
