<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\OrderReturnRecord;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppReturnAutopayPayBankCommand extends Command
{
    protected static $defaultName = 'app:return-autopay:pay-bank';

    protected function configure()
    {
        $this->setDescription('т.к. у нас нет интеграции с клиент-банком то оплата производится в ручную,' .
            ' здесь просто отмечается как оплаченное');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var $returns OrderReturnRecord[] */
        $returns = OrderReturnRecord::model()
            ->refundType(OrderReturnRecord::REFUND_TYPE_BANK)
            ->status(OrderReturnRecord::STATUS_NEED_PAY)
            ->findAll();

        foreach ($returns as $return) {
            $return->status = OrderReturnRecord::STATUS_PAYED;
            $return->save();
        }
    }
}
