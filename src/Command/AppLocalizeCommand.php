<?php

namespace MommyCom\Command;

use MommyCom\YiiComponent\LocalizationTool\LocalizationTool;
use MommyCom\YiiComponent\LocalizationTool\PathException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppLocalizeCommand extends Command
{
    private const ARG_PATH = 'path';

    protected static $defaultName = 'app:localize';

    protected function configure()
    {
        $this
            ->setDescription('Переводит тексты по указанному пути')
            ->addArgument(self::ARG_PATH, InputArgument::REQUIRED, 'Путь к messages');
    }

    /**
     * {@inheritdoc}
     * @throws PathException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument(self::ARG_PATH);

        (new LocalizationTool($path))->run();
    }
}
