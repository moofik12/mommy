<?php

namespace MommyCom\Command;

use CLogger;
use DateTime;
use Exception;
use InvalidArgumentException;
use LogicException;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yii;

class AppSupplierPaymentCommand extends Command
{
    private const PERIOD_FIND_NEW_EVENTS = '2 weeks'; //добавление поставщиков для оплаты
    private const SUPPLIER_SEND_INFO_AFTER = 1443646800; //отправка писем при получении товаров складом после 01.10.2015 (1443646800)

    protected static $defaultName = 'app:supplier-payment';

    protected function configure()
    {
        $this->setDescription('Обеспечивает логику слежения оплаты поставщикам');
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->_findSuppliersToPay();
            UnShardedActiveRecord::clearModelsCache();

            $this->_updateStatusToPay();
            UnShardedActiveRecord::clearModelsCache();

            $this->_updateSuppliersDelivery();
            UnShardedActiveRecord::clearModelsCache();

            $this->_cancelOrderPositionsNotDelivered();
            UnShardedActiveRecord::clearModelsCache();
        } catch (Exception $e) {
            Yii::log("Ошибка при обновлении полаты поставщикам, на строке: {$e->getLine()}", CLogger::LEVEL_ERROR);
            throw $e;
        }

        Yii::log("Обновление данных об оплатах поставщикам", CLogger::LEVEL_INFO);
    }

    /**
     * Добавление новых данных для оплаты
     *
     * @throws \CException
     */
    private function _findSuppliersToPay()
    {
        $endFindDateTime = new DateTime('today');
        $endFindDateTime->modify('-' . self::PERIOD_FIND_NEW_EVENTS);

        $startFindDateTime = new DateTime();

        $eventsID = EventRecord::model()
            ->timeEndsAfter($endFindDateTime->getTimestamp())
            ->timeEndsBefore($startFindDateTime->getTimestamp())
            ->isDropShipping(false)
            ->findColumnDistinct('id');

        $uniqueEventsId = array_chunk($eventsID, 100);
        foreach ($uniqueEventsId as $heapEvents) {
            $supplierPaymentToSave = [];

            $warehouseProductsSelector = WarehouseProductRecord::model()->eventIdIn($heapEvents)->isReturn(false);
            $warehouseProductsSelector->getDbCriteria()->distinct = true;
            $warehouseProductsSelector->getDbCriteria()->select = 'event_id, supplier_id';
            /* @var WarehouseProductRecord[] $warehouseProducts */
            $warehouseProducts = $warehouseProductsSelector->findAll();

            foreach ($warehouseProducts as $warehouseProduct) {
                $hasSupplierPayment = SupplierPaymentRecord::model()->supplierIdIn([$warehouseProduct->supplier_id])
                    ->eventIdIn([$warehouseProduct->event_id])->count();

                if ($hasSupplierPayment) {
                    continue;
                }

                $supplierPayment = new SupplierPaymentRecord();
                $supplierPayment->supplier_id = $warehouseProduct->supplier_id;
                $supplierPayment->event_id = $warehouseProduct->event_id;
                $supplierPayment->event_end_at = $warehouseProduct->event->end_at;
                $supplierPayment->status = SupplierPaymentRecord::STATUS_NOT_ANALYZED;
                $this->_bindSupplierContractType($supplierPayment);

                $supplierPaymentToSave[] = $supplierPayment;
            }

            $result = UnShardedActiveRecord::saveMany($supplierPaymentToSave);
            if (!$result) {
                $errors = [];
                /** @var SupplierPaymentRecord[] $supplierPaymentToSave */
                foreach ($supplierPaymentToSave as $item) {
                    if ($item->hasErrors()) {
                        $errors[] = $item->errors;
                    }
                }

                Yii::log('Ошибка при добавлении записи в контроле оплаты поставщикам. Ошибки:' . print_r($errors, true), CLogger::LEVEL_ERROR, 'supplier.payment.findSuppliersToPay');
            }
        }
    }

    /**
     * Обновление данных для оплаты
     */
    private function _updateStatusToPay()
    {
        $supplierPaymentsID = SupplierPaymentRecord::model()
            ->statusIn([
                SupplierPaymentRecord::STATUS_NOT_ANALYZED,
                SupplierPaymentRecord::STATUS_ANALYSIS,
                SupplierPaymentRecord::STATUS_WAIT_DELIVERY,
            ])
            ->findColumnDistinct('id');

        $queue = array_chunk($supplierPaymentsID, 500);
        foreach ($queue as $heap) {
            $this->_updateSupplierPayments(SupplierPaymentRecord::model()->idIn($heap)->with('contract')->together()->findAll());
        }
    }

    /**
     * Обновление данных о поставленных товарах
     */
    private function _updateSuppliersDelivery()
    {
        $supplierPaymentsID = SupplierPaymentRecord::model()
            ->statusIn([SupplierPaymentRecord::STATUS_ANALYZED])
            ->paymentAfterAtGreater(strtotime('today -1 second'))
            ->findColumnDistinct('id');

        $queue = array_chunk($supplierPaymentsID, 500);
        foreach ($queue as $heap) {
            $this->_updateSupplierPayments(SupplierPaymentRecord::model()->idIn($heap)->with('contract')->together()->findAll(), true);
        }
    }

    /**
     * @param SupplierPaymentRecord[] $payments
     * @param bool $informSupplier default false
     */
    private function _updateSupplierPayments(array $payments, $informSupplier = false)
    {
        foreach ($payments as $supplierPayment) {
            /* @var SupplierPaymentRecord $supplierPayment */

            /* @var WarehouseProductRecord[] $warehouseProducts */
            $warehouseProducts = WarehouseProductRecord::model()
                ->eventId($supplierPayment->event_id)
                ->supplierId($supplierPayment->supplier_id)
                ->isReturn(false)
                ->with('eventProduct')
                ->together()
                ->findAll();

            $supplierDeliveryStartAt = $supplierPayment->getMustDeliveryStartAt();

            $helperSupplier = new SupplierPaymentCommandSupplierInfo($supplierPayment->supplier, $supplierPayment->event_id);
            foreach ($warehouseProducts as $warehouseProduct) {
                $warehouseProductDeliveredAt = 0;

                //обновление времени получения товаров
                if ($warehouseProduct->status == WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED) {
                    $warehouseProductDeliveredAt = $warehouseProduct->status_changed_at;
                } else {
                    $history = $warehouseProduct->getStatusHistory();
                    ksort($history);
                    foreach ($history as $historyTimestamp => $historyItem) {
                        if ($historyItem > 0 && isset($historyItem['status']) && $historyItem['status'] == WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED) {
                            $warehouseProductDeliveredAt = $historyTimestamp;
                            break;
                        }
                    }
                }

                //кол-во дней которые можно доставить товар
                $availableDeliveryDaysForWarehouseProduct = $warehouseProductDeliveredAt > 0 && $supplierPayment->contract
                    ? $supplierPayment->contract->getAvailableDeliveryDays($supplierDeliveryStartAt, true, $warehouseProductDeliveredAt) : 0;

                $helperWarehouseProduct = SupplierPaymentCommandWarehouseProductInfo::loadFromModel($warehouseProduct, $availableDeliveryDaysForWarehouseProduct > 0);
                $helperWarehouseProduct->deliveredAt = $warehouseProductDeliveredAt;
                $helperSupplier->addWarehouseProduct($helperWarehouseProduct);
            }

            $supplierPayment->delivery_start_at = $helperSupplier->getDeliveryStartAt();
            $supplierPayment->delivery_end_at = $helperSupplier->getDeliveryEndAt();
            $supplierPayment->request_amount = $helperSupplier->getRequestAmount();
            $supplierPayment->delivered_amount = $helperSupplier->getDeliveredAmount();
            $supplierPayment->delivered_amount_wrong_time = $helperSupplier->getDeliveredAmountWrongTime();
            $supplierPayment->count_products_problem_delivery = $helperSupplier->getCountProductsNotDelivered();

            $isFullDelivered = $supplierPayment->request_amount == $supplierPayment->delivered_amount;

            //доступные дни доставки
            $availableDeliveryDays = $supplierPayment->contract
                ? $supplierPayment->contract->getAvailableDeliveryDays($supplierDeliveryStartAt, false) : 0;

            //на случай если вообще не было поставки
            $supplierDeliveryEndAt = max($supplierPayment->delivery_end_at, $supplierDeliveryStartAt);

            if ($supplierPayment->order_table_loaded_at == 0) {
                $supplierPayment->order_table_loaded_at = $helperSupplier->getOrderTableLoadedAt();
            }

            if ($isFullDelivered || $availableDeliveryDays == 0) {
                //назначение даты оплаты
                if (in_array($supplierPayment->status, [
                    SupplierPaymentRecord::STATUS_NOT_ANALYZED, SupplierPaymentRecord::STATUS_ANALYSIS, SupplierPaymentRecord::STATUS_WAIT_DELIVERY,
                ])) {
                    $supplierPayment->status = SupplierPaymentRecord::STATUS_ANALYZED;

                    $paymentAfterAt = $supplierPayment->contract->getPaymentNotFullDeliveryTimestamp($supplierDeliveryEndAt);

                    //приведение актуализации времени поставки товаров на склад для старых акций
                    $availableDeliveryDaysForOldEvents = $supplierPayment->contract
                        ? $supplierPayment->contract->getAvailableDeliveryDays($supplierDeliveryStartAt, true, $supplierDeliveryEndAt) : 0;

                    if ($isFullDelivered && $availableDeliveryDaysForOldEvents > 0) {
                        $paymentAfterAt = $supplierPayment->contract->getPaymentFullDeliveryTimestamp($supplierDeliveryEndAt);
                    }

                    if ($supplierPayment->payment_after_at == 0) {
                        $supplierPayment->payment_after_at = $paymentAfterAt;
                    }
                }

                //send inform supplier
                if ($informSupplier) {
                    $this->_notifierSupplier($helperSupplier);
                }
            } elseif ($supplierPayment->status == SupplierPaymentRecord::STATUS_NOT_ANALYZED
                || $supplierPayment->status == SupplierPaymentRecord::STATUS_WAIT_DELIVERY) {
                if ($supplierPayment->delivery_end_at == 0 && $supplierPayment->delivery_start_at == 0) {
                    $supplierPayment->status = SupplierPaymentRecord::STATUS_WAIT_DELIVERY;
                } else {
                    $supplierPayment->status = SupplierPaymentRecord::STATUS_ANALYSIS;
                }
            }

            if (!$supplierPayment->save()) {
                $errors = $supplierPayment->getErrors();

                Yii::log('Ошибка при обновлении записи в контроле оплаты поставщикам. Ошибки:' . print_r($errors, true), CLogger::LEVEL_ERROR, 'supplier.payment.updateStatusToPay');
            }
        }
    }

    /**
     * отмена недоставленных позиций
     *
     * @throws \CException
     */
    private function _cancelOrderPositionsNotDelivered()
    {
        $startTime = strtotime('today 08:30');
        $endTime = strtotime('today 22:00');
        $time = time();

        if ($time < $startTime || $time > $endTime) {
            return;
        }

        $supplierPaymentsID = SupplierPaymentRecord::model()
            ->statusIn([SupplierPaymentRecord::STATUS_ANALYZED])
            ->isOrderPositionsWorked(false)
            ->paymentAfterAtGreater(strtotime('today -1 second'))
            ->findColumnDistinct('id');

        $queue = array_chunk($supplierPaymentsID, 500);
        foreach ($queue as $heap) {
            /* @var $models SupplierPaymentRecord[] */
            $models = SupplierPaymentRecord::model()->idIn($heap)->with('contract')->together()->findAll();

            foreach ($models as $model) {
                if ($model->status != SupplierPaymentRecord::STATUS_ANALYZED || $model->is_order_positions_worked || $model->event == null) {
                    continue;
                }

                //доп. время по просьбе бренд менеджеров
                $startDayToCanceled = strtotime('+2 days', $model->event->mailing_start_at);
                if ($startDayToCanceled > time()) {
                    continue;
                }

                $warehouseProductsNotInStock = WarehouseProductRecord::model()
                    ->eventId($model->event_id)
                    ->supplierId($model->supplier_id)
                    ->status(WarehouseProductRecord::STATUS_UNMODERATED)
                    ->onlyHasOrderConnected()
                    ->with('orderProduct')
                    ->findAll();

                $warehouseProductsByOrder = ArrayUtils::groupBy($warehouseProductsNotInStock, 'order_id');

                foreach ($warehouseProductsByOrder as $orderId => $warehouseProducts) {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $cancelPositions = [];

                        /* @var $warehouseProducts WarehouseProductRecord[] */
                        foreach ($warehouseProducts as $warehouseProduct) {
                            $orderProduct = $warehouseProduct->orderProduct;
                            if ($orderProduct->storekeeper_status != OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED) {
                                continue;
                            }

                            $orderProduct->callcenter_status = OrderProductRecord::CALLCENTER_STATUS_CANCELLED;
                            if (!$orderProduct->save()) {
                                throw new LogicException("Ошибка отмены позиции №{$orderProduct->id} в заказе $orderId, ошибки: "
                                    . print_r($orderProduct->getErrors(), true));
                            }

                            $cancelPositions[] = $orderProduct;
                        }

                        if (!$cancelPositions) {
                            $transaction->commit();
                            continue;
                        }

                        $order = OrderRecord::model()->idIn([$orderId])->with('user')->find();

                        if ($order == null) {
                            throw new LogicException("Не найден заказ $orderId для отмены позиций при недоставке товоров поставщиком");
                        }

                        if ($order->getCountNotDeletedPositions() == 0) {
                            $order->processing_status = OrderRecord::PROCESSING_CANCELLED;

                            if (!$order->save()) {
                                throw new LogicException("Ошибка обновления статуса заказа {$order->id}, ошибки: "
                                    . print_r($order->getErrors(), true));
                            }
                        }

                        //send mail
                        $viewPath = Yii::getPathOfAlias(Yii::app()->mailer->viewPath);
                        $viewFile = $viewPath . DIRECTORY_SEPARATOR . 'orderCancelPositionsConsole.php';
                        $emailTitle = "На склад MAMAM не поступили товары";

                        $body = $this->renderFile($viewFile, [
                            'title' => $emailTitle,
                            'order' => $order,
                            'cancelPositions' => $cancelPositions,
                        ], true);

                        $allowed = Yii::app()->params['newsletters'] ?? false;

                        if ($allowed) {
                            $createEmail = Yii::app()->mailer->create(
                                [Yii::app()->params['layoutsEmails']['default'] => Yii::app()->params['distributionEmailName']]
                                , [$order->user->email]
                                , $emailTitle
                                , ['body' => $body]
                                , EmailMessage::TYPE_SYSTEM
                            );

                            if (!$createEmail) {
                                throw new LogicException("Не удалось создать email для пользователя {$order->user->email} с уведомлением об отмене позиций в заказе");
                            }
                        }
                        //send SMS
                        $smsText = "Поставщик не смог поствить некоторые товары в заказ №{$order->id}";

                        if ($order->processing_status == OrderRecord::PROCESSING_CANCELLED) {
                            $smsText .= '. Ваш заказ отменён';
                        }

                        if ($order->card_payed > 0) {
                            $smsText .= '. Предоплата за них будет возвращена';
                        }

                        Yii::app()->sms->create($order->user_id, $order->telephone, $smsText);

                        $model->is_order_positions_worked = 1;
                        $model->updateByPk($model->id, ['is_order_positions_worked' => 1]);

                        $transaction->commit();
                    } catch (Exception $e) {
                        $transaction->rollback();

                        Yii::log($e, CLogger::LEVEL_ERROR);
                    }
                }
            }
        }
    }

    /**
     * Привязка контраката к поставщику
     *
     * @param SupplierPaymentRecord $supplierPayment
     */
    private function _bindSupplierContractType(SupplierPaymentRecord $supplierPayment)
    {
        $contract = $supplierPayment->supplier->contractConnected;

        if ($contract === null) {
            Yii::log("Ошибка при находжении контракта для поставщика {$supplierPayment->supplier_id}", CLogger::LEVEL_ERROR, 'supplier.payment.updateStatusToPay');
            throw new LogicException('Supplier contract not found');
        }

        $supplierPayment->contract_id = $contract->id;
    }

    /**
     * Отправка уведомления поставщику о принятых товарах
     *
     * @param SupplierPaymentCommandSupplierInfo $info
     */
    private function _notifierSupplier(SupplierPaymentCommandSupplierInfo $info)
    {
        if (!$info->getIsSupplierInformPossible() || !$info->isNeedInformSupplier()) {
            return;
        }

        if ($info->getDeliveryStartAt() < self::SUPPLIER_SEND_INFO_AFTER) {
            return;
        }

        $viewPath = Yii::getPathOfAlias(Yii::app()->mailer->viewPath);
        $viewFile = $viewPath . DIRECTORY_SEPARATOR . 'supplierDeliveredInfoConsole.php';

        $newDeliveredProductsIDs = array_map(function ($item) {
            /* @var SupplierPaymentCommandWarehouseProductInfo $item */
            return $item->id;
        }, $info->getProductsWhenNeedsSupplierInform());

        //send notifier
        if ($newDeliveredProductsIDs) {
            $notDeliveredProductsIDs = array_map(function ($item) {
                /* @var SupplierPaymentCommandWarehouseProductInfo $item */
                return $item->id;
            }, $info->getProductsNotDelivered());

            $newDeliveredProducts = WarehouseProductRecord::model()->idIn($newDeliveredProductsIDs)->findAll();
            $notDeliveredProducts = WarehouseProductRecord::model()->idIn($notDeliveredProductsIDs)->findAll();

            try {
                $body = $this->renderFile($viewFile, [
                    'title' => "На склад MAMAM поступили товары из акции {$info->getEventID()}",
                    'deliveredWarehouseProducts' => $newDeliveredProducts,
                    'needDeliveryWarehouseProducts' => $notDeliveredProducts,
                ], true);

                $createEmail = Yii::app()->mailer->create(
                    [Yii::app()->params['layoutsEmails']['default'] => Yii::app()->params['distributionEmailName']]
                    , [$info->getSupplierEmail()]
                    , "На склад MAMAM поступили товары из акции {$info->getEventID()}"
                    , ['body' => $body]
                    , EmailMessage::TYPE_SYSTEM
                );

                if (!$createEmail) {
                    throw new LogicException("Не удаллось создать email для поставщика {$info->getSupplierID()} в акции {$info->getEventID()}");
                }

                $countNewDeliveredProducts = count($newDeliveredProducts);
                $countUpdate = WarehouseProductRecord::model()->updateAll(['is_supplier_informed' => 1], 'id IN(' . implode(',', $newDeliveredProductsIDs) . ')');
                if ($countUpdate !== $countNewDeliveredProducts) {
                    throw new LogicException("НЕ все данные обновлены об информировании доставки товаров для поставщика {$info->getSupplierID()} в акции {$info->getEventID()}"
                        . "Обновлено $countUpdate позиция(й) складского товара из $countNewDeliveredProducts."
                    );
                }
            } catch (Exception $e) {
                Yii::log('Ошибка при создании письма для поставщика о доставленных товарах. ' . $e, CLogger::LEVEL_ERROR);
            }
        }
    }

    private function renderFile($_viewFile_, $_data_ = null, $_return_ = false)
    {
        if (is_array($_data_))
            extract($_data_, EXTR_PREFIX_SAME, 'data');
        else
            $data = $_data_;
        if ($_return_) {
            ob_start();
            ob_implicit_flush(false);
            require($_viewFile_);
            return ob_get_clean();
        } else
            require($_viewFile_);
    }
}

class SupplierPaymentCommandSupplierInfo
{
    /** @var  int */
    private $_eventID;
    /** @var  int */
    private $_supplierID;
    /** @var  bool */
    private $_isSupplierInformPossible;
    /** @var  string email */
    private $_supplierEmail;

    /** @var SupplierPaymentCommandWarehouseProductInfo[] */
    private $_products = [];

    public function __construct(SupplierRecord $supplier, $eventID)
    {
        $this->_eventID = $eventID;
        $this->_supplierID = $supplier->id;
        $this->_isSupplierInformPossible = $supplier->isInformPossible();
        $this->_supplierEmail = $supplier->email;
    }

    /**
     * @return int
     */
    public function getEventID()
    {
        return $this->_eventID;
    }

    /**
     * @return int
     */
    public function getSupplierID()
    {
        return $this->_supplierID;
    }

    /**
     * @return int
     */
    public function getIsSupplierInformPossible()
    {
        return $this->_isSupplierInformPossible;
    }

    /**
     * @return string
     */
    public function getSupplierEmail()
    {
        return $this->_supplierEmail;
    }

    public function addWarehouseProduct(SupplierPaymentCommandWarehouseProductInfo $record)
    {
        if ($record->supplierID != $this->_supplierID || $record->eventID != $this->_eventID) {
            throw new InvalidArgumentException('WarehouseProductRecord::supplier_id and WarehouseProductRecord::event_id must be mach in this attributes');
        }

        if ($record->isReturn) {
            throw new LogicException('WarehouseProductRecord::is_return not support in this logic');
        }

        $this->_products[$record->id] = $record;
    }

    public function removeWarehouseProduct(SupplierPaymentCommandWarehouseProductInfo $record)
    {
        if (isset($this->_products[$record->id])) {
            unset($this->_products[$record->id]);
        }
    }

    /**
     * Начало поставки товаров
     *
     * @return int
     */
    public function getDeliveryStartAt()
    {
        return array_reduce($this->_products, function ($carry, $item) {
            /** @var SupplierPaymentCommandWarehouseProductInfo $item */
            if ($item->deliveredAt > 0) {
                return min($carry, $item->deliveredAt);
            }

            return $carry;
        }, $this->getDeliveryEndAt());
    }

    /**
     * Конец поставки товаров
     *
     * @return mixed
     */
    public function getDeliveryEndAt()
    {
        return array_reduce($this->_products, function ($carry, $item) {
            /** @var SupplierPaymentCommandWarehouseProductInfo $item */

            return max($carry, $item->deliveredAt);
        }, 0);
    }

    /**
     * Сумма запрошенных товаров
     *
     * @return float
     */
    public function getRequestAmount()
    {
        return array_reduce($this->_products, function ($carry, $item) {
            /** @var SupplierPaymentCommandWarehouseProductInfo $item */

            return ($carry + $item->pricePurchase);
        }, 0.0);
    }

    /**
     * Сумма поставленных товаров
     *
     * @return float
     */
    public function getDeliveredAmount()
    {
        return array_reduce($this->_products, function ($carry, $item) {
            /** @var SupplierPaymentCommandWarehouseProductInfo $item */
            if ($item->isDelivered) {
                return ($carry + $item->pricePurchase);
            }

            return $carry;
        }, 0.0);
    }

    /**
     * Сумма товаров поставленных после срока поставки по контракту
     *
     * @return float
     */
    public function getDeliveredAmountWrongTime()
    {
        return array_reduce($this->_products, function ($carry, $item) {
            /** @var SupplierPaymentCommandWarehouseProductInfo $item */
            if ($item->isDelivered && !$item->isDeliveredOnTime) {
                return ($carry + $item->pricePurchase);
            }

            return $carry;
        }, 0.0);
    }

    /**
     * Поставщик сделал полную доставку
     *
     * @return bool
     */
    public function isFullDelivered()
    {
        $countDelivered = array_reduce($this->_products, function ($carry, $item) {
            /* @var SupplierPaymentCommandWarehouseProductInfo $item */
            if ($item->isDelivered) {
                ++$carry;
            }

            return $carry;
        }, 0);

        return (count($this->_products) === $countDelivered);
    }

    /**
     * @return SupplierPaymentCommandWarehouseProductInfo[]
     */
    public function getProducts()
    {
        return $this->_products;
    }

    /**
     * Необходимо проинформировать поставщика
     *
     * @return bool
     */
    public function isNeedInformSupplier()
    {
        return !!$this->getProductsWhenNeedsSupplierInform();
    }

    /**
     * @return SupplierPaymentCommandWarehouseProductInfo[]
     */
    public function getProductsWhenNeedsSupplierInform()
    {
        $products = array_filter($this->_products, function ($item) {
            /* @var SupplierPaymentCommandWarehouseProductInfo $item */
            if ($item->isDelivered && !$item->isSupplierInformed) {
                return true;
            }

            return false;
        });

        return $products;
    }

    /**
     * @return SupplierPaymentCommandWarehouseProductInfo[]
     */
    public function getProductsNotDelivered()
    {
        $products = array_filter($this->_products, function ($item) {
            /* @var SupplierPaymentCommandWarehouseProductInfo $item */
            if (!$item->isDelivered) {
                return true;
            }

            return false;
        });

        return $products;
    }

    /**
     * @return int
     */
    public function getCountProductsNotDelivered()
    {
        return array_reduce($this->_products, function ($carry, $item) {
            /* @var SupplierPaymentCommandWarehouseProductInfo $item */
            if (!$item->isDelivered) {
                return ++$carry;
            }

            return $carry;
        }, 0);
    }

    /**
     * @return int
     */
    public function getOrderTableLoadedAt()
    {
        $time = time();

        $timeTableLoaded = array_reduce($this->_products, function ($carry, $item) {
            /* @var SupplierPaymentCommandWarehouseProductInfo $item */

            return min($carry, $item->getCreatedAt());
        }, $time);

        if ($timeTableLoaded == $time) {
            return 0;
        }

        return $timeTableLoaded;
    }
}

/**
 * Class SupplierPaymentCommandWarehouseProductInfo
 *
 * @property int $id ID WarehouseProductRecord
 * @property int $deliveredAt unix timestamp delivered product
 * @property float $pricePurchase
 * @property int $supplierID
 * @property int $eventID
 * @property bool $isReturn
 * @property bool $isDelivered был ли товар поставлен
 * @property bool $isDeliveredOnTime был ли товар поставлен вовремя
 * @property bool $isSupplierInformed поставщик уже уведомлен о поставленном товаре
 */
class SupplierPaymentCommandWarehouseProductInfo extends \CComponent
{
    /** @var  int */
    private $id;
    /** @var  int */
    private $supplierID;
    /** @var  int */
    private $eventID;
    /** @var bool */
    private $isDelivered;
    /** @var  int */
    private $deliveredAt = 0;
    /** @var  bool */
    private $isDeliveredOnTime;
    /** @var  float */
    private $pricePurchase = 0.0;
    /** @var  bool */
    private $isReturn;
    /** @var  bool */
    private $isInformSupplier;
    /** @var  int */
    private $created_at = 0;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    /**
     * @param int $deliveredAt
     */
    public function setDeliveredAt($deliveredAt)
    {
        $this->deliveredAt = Cast::toInt($deliveredAt);
    }

    /**
     * @return boolean
     */
    public function isIsDeliveredOnTime()
    {
        return $this->isDeliveredOnTime;
    }

    /**
     * @param boolean $isDeliveredOnTime
     */
    public function setIsDeliveredOnTime($isDeliveredOnTime)
    {
        $this->isDeliveredOnTime = !!$isDeliveredOnTime;
    }

    /**
     * @return float
     */
    public function getPricePurchase()
    {
        return $this->pricePurchase;
    }

    /**
     * @param float $pricePurchase
     */
    public function setPricePurchase($pricePurchase)
    {
        $this->pricePurchase = Cast::toFloat($pricePurchase);
    }

    /**
     * @return int
     */
    public function getSupplierID()
    {
        return $this->supplierID;
    }

    /**
     * @return mixed
     */
    public function getEventID()
    {
        return $this->eventID;
    }

    /**
     * @return boolean
     */
    public function getIsDeliveredOnTime()
    {
        return $this->isDeliveredOnTime;
    }

    /**
     * @return boolean
     */
    public function getIsDelivered()
    {
        return $this->isDelivered;
    }

    /**
     * @return boolean
     */
    public function getIsReturn()
    {
        return $this->isReturn;
    }

    /**
     * @param boolean $isReturn
     */
    public function setIsReturn($isReturn)
    {
        $this->isReturn = !!$isReturn;
    }

    /**
     * @return boolean
     */
    public function getIsSupplierInformed()
    {
        return $this->isSupplierInformed;
    }

    /**
     * @param boolean $isInformSupplier
     */
    public function setIsSupplierInformed($isInformSupplier)
    {
        $this->isSupplierInformed = !!$isInformSupplier;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param WarehouseProductRecord $record
     * @param bool $isDeliveredOnTime товар доставили вовремя
     *
     * @return SupplierPaymentCommandWarehouseProductInfo
     */
    public static function loadFromModel(WarehouseProductRecord $record, $isDeliveredOnTime)
    {
        $model = new self();
        $model->supplierID = $record->supplier_id;
        $model->eventID = $record->event_id;
        $model->id = $record->id;
        $model->pricePurchase = $record->eventProduct->price_purchase;
        $model->isReturn = $record->is_return;
        $model->isDeliveredOnTime = !!$isDeliveredOnTime;
        $model->isDelivered = $record->getIsSupplierDelivered();
        $model->isSupplierInformed = !!$record->is_supplier_informed;
        $model->created_at = $record->created_at;

        return $model;
    }
}
