<?php

namespace MommyCom\Command;

use Minishlink\WebPush\WebPush;
use MommyCom\Model\Db\ApplicationSubscriberRecord;
use MommyCom\Model\Db\DistributionPushRecord;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppMessagePushCommand extends Command
{
    protected static $defaultName = 'app:message:push';

    protected function configure()
    {
        $this->setDescription('Отправляет пуш уведомления');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var DistributionPushRecord $distributionPush */
        $distributionPush = DistributionPushRecord::model()
            ->mustBeSigned()
            ->find();

        if ($distributionPush) {
            $distributionPush->status = DistributionPushRecord::STATUS_START;
            $distributionPush->save();

            $distributionPush->status = DistributionPushRecord::STATUS_END;

            try {
                $this->browserPush($distributionPush);
            } catch (\Exception $e) {
                //$distributionPush->status = DistributionPushRecord::STATUS_ERROR;
                \Yii::log('Ошибка при отправлении push сообщений на Web!' . $e, \CLogger::LEVEL_ERROR, 'pushMessages');
            }

            $distributionPush->save();
        }
    }

    private function browserPush(DistributionPushRecord $distributionPush)
    {
        $auth = [
            'GCM' => 'AIzaSyAGUrnNvxXVzlCUAQXQ3COgQ53O7rYytXk',
        ];
        $webPush = new WebPush($auth);

        $pageSize = 100; //максимальное кол-во ID котрое можно отправить за 1 раз - 1000
        $model = ApplicationSubscriberRecord::model()->typeIs(ApplicationSubscriberRecord::TYPE_WEB);
        $count = $model->copy()->count();

        $dataProvider = $model->getDataProvider(false, [
                'pagination' => ['pageSize' => $pageSize, 'itemCount' => $count],
                'criteria' => ['order' => 'id ASC'], //для последовательности
            ]
        );
        $countPage = $dataProvider->pagination->getPageCount();

        for ($j = 0; $j < $countPage; $j++) {
            usleep(1000);
            $dataProvider->getPagination()->setCurrentPage($j);
            /* @var ApplicationSubscriberRecord[] $items */
            $items = $dataProvider->getData(true);

            foreach ($items as $item) {
                $item->last_subject = $distributionPush->subject;
                $item->last_body = $distributionPush->body;
                $item->last_url = $distributionPush->url_desktop;

                $webPush->sendNotification(
                    $item->client_gcm_id
                );
            }

            UnShardedActiveRecord::saveMany($items);
            $webPush->flush();
            unset($items);
        }
    }
}
