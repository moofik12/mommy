<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\Distribution\DistributionEventRecorder;
use MommyCom\Service\Distribution\DistributionUserSynchronizer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppMailingSynchronizeCommand extends Command
{
    protected static $defaultName = 'app:mailing:synchronize';

    /**
     * @var DistributionUserSynchronizer
     */
    private $distributionUserSynchronizer;

    public function __construct(DistributionUserSynchronizer $distributionUserSynchronizer)
    {
        $this->distributionUserSynchronizer = $distributionUserSynchronizer;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Синхронизиует БД сайта с группами рассылок почтового сервиса');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $distributionEventRecorder = new DistributionEventRecorder();
            $distributionEventRecorder->makeRecordsForLostCarts(
                UserRecord::model(),
                CartRecord::model(),
                MailingEventRecord::model(),
                MailingDataRecord::model()
            );

            $this->distributionUserSynchronizer->sync();
        } catch (\CException $e) {
            $io->error((string)$e);
        }
    }
}
