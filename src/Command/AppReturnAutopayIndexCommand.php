<?php

namespace MommyCom\Command;

use MommyCom\Model\Db\OrderReturnRecord;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppReturnAutopayIndexCommand extends Command
{
    protected static $defaultName = 'app:return-autopay:index';

    protected function configure()
    {
        $this->setDescription('Автоматически отмечает как оплаченые возвраты стоимость которых равна 0');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var $returns OrderReturnRecord[] */
        $returns = OrderReturnRecord::model()
            ->status(OrderReturnRecord::STATUS_CONFIGURED)
            ->onlyPayReady()
            ->refundPriceBetween(0, 0)
            ->findAll();

        foreach ($returns as $return) {
            $return->status = OrderReturnRecord::STATUS_PAYED;
            $return->save();
        }
    }
}
