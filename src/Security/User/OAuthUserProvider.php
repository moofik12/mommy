<?php

namespace MommyCom\Security\User;

use Doctrine\ORM\EntityManagerInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use MommyCom\Security\Authentication\Encoder\UserPasswordEncoder;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use MommyCom\Entity\User;

class OAuthUserProvider implements UserProviderInterface, OAuthAwareUserProviderInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var UserPasswordEncoder
     */
    protected $encoder;

    /**
     * @var UserBuilder
     */
    protected $userBuilder;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * OAuthUserProvider constructor.
     *
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoder $factory
     * @param UserBuilder $userBuilder
     * @param SessionInterface $session
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoder $factory,
        UserBuilder $userBuilder,
        SessionInterface $session
    ) {
        $this->em = $em;
        $this->encoder = $factory;
        $this->userBuilder = $userBuilder;
        $this->session = $session;
    }

    /**
     * @param UserResponseInterface $response
     *
     * @return UserInterface
     *
     * @throws UserBuilderException
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        /** @var UserInterface $user */
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy(['email' => $response->getEmail()]);

        if ($user) {
            return $user;
        }

        $user = $this
            ->userBuilder
            ->setName($response->getFirstName())
            ->setSurname($response->getLastName())
            ->setEmail($response->getEmail())
            ->build();

        $this->em->persist($user);
        $this->em->flush();

        $this->session->set('justRegistered', true);

        return $user;
    }

    /**
     * @param string $username
     *
     * @return UserInterface|null
     */
    public function loadUserByUsername($username)
    {
        return $this
            ->em
            ->getRepository(User::class)
            ->findOneBy(['email' => $username]);
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === User::class;
    }
}
