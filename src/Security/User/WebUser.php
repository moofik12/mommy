<?php

namespace MommyCom\Security\User;

use MommyCom\Entity\AdminUser;
use MommyCom\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

abstract class WebUser extends \CWebUser
{
    /**
     * @var TokenInterface
     */
    private $token;

    /**
     * WebUser constructor.
     *
     * @param TokenInterface|null $token
     */
    public function __construct(?TokenInterface $token)
    {
        $this->token = $token;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        if (!$this->getToken()) {
            return null;
        }

        $user = $this->getToken()->getUser();
        if (!($user instanceof User) && !($user instanceof AdminUser)) {
            return null;
        }

        return $user->getId();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->token ? $this->token->getUsername() : $this->guestName;
    }

    /**
     * @return bool
     */
    public function getIsGuest()
    {
        return !$this->token || $this->token instanceof AnonymousToken || !$this->token->isAuthenticated();
    }

    /**
     * @param string $operation
     * @param array $params
     * @param bool $allowCaching
     *
     * @return bool
     */
    public function checkAccess($operation, $params = [], $allowCaching = true)
    {
        /* Для фронтенд юзера права доступа настраиваются через security.yaml */
        return true;
    }

    /**
     * @return TokenInterface
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param TokenInterface $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @param array $identityCookie
     */
    public function setIdentityCookie(array $identityCookie)
    {
        $this->identityCookie = $identityCookie;
    }
}
