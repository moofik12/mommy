<?php

namespace MommyCom\Security\User;

use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Backend\AdmAuthManager\AdmWebUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class WebUserFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return ShopWebUser|null
     */
    public static function createFrontendUser(ContainerInterface $container): ?ShopWebUser
    {
        if (!$container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }

        /** @var TokenInterface $token */
        $token = $container->get('security.token_storage')->getToken();

        if (null !== $token) {
            $user = $token->getUser();

            if (!is_object($user) && !is_string($user)) {
                $token = null;
            }
        }

        $user = new ShopWebUser($token);

        $user->setIdentityCookie(isset($_SERVER['SERVER_NAME'])
            ? ['domain' => '.' . implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2))]
            : []);
        $user->init();

        return $user;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return AdmWebUser|null
     */
    public static function createBackendUser(ContainerInterface $container): ?AdmWebUser
    {
        $user = new AdmWebUser();

        $userParameters = $container->getParameter('user.backend');

        $user->init();
        $user->setAllowController($userParameters['allowed_controller']);
        $user->setLoginUrl($userParameters['login_url']);
        $user->allowAutoLogin = true;

        return $user;
    }
}
