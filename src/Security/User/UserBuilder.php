<?php

namespace MommyCom\Security\User;

use MommyCom\Entity\User;
use MommyCom\Security\Authentication\Encoder\UserPasswordEncoder;
use MommyCom\Service\CurrentTime;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserBuilder
{
    /**
     * @var string|null
     */
    private $password;

    /**
     * @var boolean
     */
    private $hasDefaultPassword;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $surname;

    /**
     * @var int|null
     */
    private $landingNum;

    /**
     * @var UserPasswordEncoder
     */
    private $encoder;

    /**
     * UserBuilder constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->hasDefaultPassword = false;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        $this->hasDefaultPassword = true;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $surname
     *
     * @return $this
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @param int $landingNum
     *
     * @return $this
     */
    public function setLandingNum(int $landingNum)
    {
        $this->landingNum = $landingNum;

        return $this;
    }

    /**
     * @return User
     *
     * @throws UserBuilderException
     */
    public function build(): User
    {
        if (!$this->email) {
            throw new UserBuilderException('E-mail is undefined. Missed setEmail() in builder configuration?');
        }

        $user = new User();

        if (!$this->hasDefaultPassword && null !== $this->password) {
            $this->password = null;
        }

        if (!$this->hasDefaultPassword) {
            $this->password = rtrim(base64_encode(random_bytes(10)), '=');
        }

        if ($this->name) {
            $user->setName($this->name);
        }

        if ($this->surname) {
            $user->setSurname($this->surname);
        }

        if ($this->landingNum) {
            $user->setLandingNum($this->landingNum);
        }

        $timestamp = CurrentTime::getUnixTimestamp();

        $user->setUpdatedAt($timestamp);
        $user->setCreatedAt($timestamp);
        $user->setEmail($this->email);
        $user->setPasswordHash($this->encoder->encodePassword($user, $this->password));

        return $user;
    }
}