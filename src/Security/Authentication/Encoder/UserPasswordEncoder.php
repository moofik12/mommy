<?php

namespace MommyCom\Security\Authentication\Encoder;


use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Entity\User;
use MommyCom\Model\Db\UserRecord;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserPasswordEncoder implements UserPasswordEncoderInterface
{
    private const INTERNAL_SALT = 'd21903hnkjfghs7dfQW#Q@#Rasdfsd23';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UserPasswordEncoder constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param UserInterface $user
     * @param string $plainPassword
     *
     * @return string
     */
    public function encodePassword(UserInterface $user, $plainPassword)
    {
        /** @var User $user */
        $passwordHash = hash('sha512', self::INTERNAL_SALT . '_' . $plainPassword . '_' . $user->getPasswordSalt());

        return $passwordHash;
    }

    /**
     * @param UserInterface $user
     * @param string $raw
     *
     * @return bool
     */
    public function isPasswordValid(UserInterface $user, $raw)
    {
        if ($user instanceof User) {
            $passwordHash = $this->encodePassword($user, $raw);

            return $passwordHash === $user->getPasswordHash();
        }

        return false;
    }
}