<?php

namespace MommyCom\Security\Authentication;

use MommyCom\YiiComponent\Facade\Translator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    private const HOME_PAGE_URL = '/';
    private const YII_SUBMIT_PARAMETER = 'yt0';

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * AuthenticationHandler constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($request->isXmlHttpRequest()) {
            $message = ['success' => true];
            $response = new Response(json_encode($message));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }

        $url = $request->getSession()->get('_security.main.target_path') ?: self::HOME_PAGE_URL;

        return new RedirectResponse($url);
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $yiiSubmitParameter = $request->get(self::YII_SUBMIT_PARAMETER);

        if (null !== $yiiSubmitParameter) {
            $message = [
                'AuthForm_email' => [Translator::t('Wrong e-mail or password')],
            ];
        } else {
            $message = [
                'success' => false,
                'message' => $exception->getMessage(),
            ];
        }

        $response = new Response(json_encode($message));
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        return $response;
    }
}
