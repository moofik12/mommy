<?php

namespace MommyCom\Repository;

use Doctrine\ORM\EntityRepository;
use MommyCom\Entity\EventProduct;
use MommyCom\Entity\WarehouseProduct;
use MommyCom\Model\Db\WarehouseProductRecord;

class WarehouseProductRepository extends EntityRepository
{
    /**
     * @param int $eventProductId
     *
     * @return WarehouseProduct|null
     */
    public function findLatestReservedProduct(int $eventProductId)
    {
        return $this->createQueryBuilder('wp')
            ->andWhere('wp.eventProductId = :eventProductId')
            ->andWhere('wp.status = :warehouseStatus')
            ->setParameter('eventProductId', $eventProductId)
            ->setParameter('warehouseStatus', WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED)
            ->orderBy('wp.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param EventProduct $eventProduct
     * @param int $securityCode
     * @param int $status
     * @param int $timestamp
     *
     * @return WarehouseProduct
     */
    public function createFromEventProduct(EventProduct $eventProduct, int $securityCode, int $status, int $timestamp) {
        $warehouseProduct = new WarehouseProduct();
        $warehouseProduct->setSupplierId($eventProduct->getSupplierId());
        $warehouseProduct->setEventId($eventProduct->getEventId());
        $warehouseProduct->setEventProductId($eventProduct->getId());
        $warehouseProduct->setEventProductSizeformat($eventProduct->getSizeformat());
        $warehouseProduct->setEventProductSize($eventProduct->getSize());
        $warehouseProduct->setEventProductPrice($eventProduct->getPrice());
        $warehouseProduct->setProductId($eventProduct->getProductId());
        $warehouseProduct->setPackagingSecurityCode($securityCode);
        $warehouseProduct->setStatus($status);
        $warehouseProduct->setRequestnum($timestamp);
        $warehouseProduct->setCreatedAt($timestamp);
        $warehouseProduct->setUpdatedAt($timestamp);

        return $warehouseProduct;
    }
}
