<?php

namespace MommyCom\Repository;

use Doctrine\ORM\EntityRepository;
use MommyCom\Entity\OrderTracking;

class OrderTrackingRepository extends EntityRepository
{
    /**
     * @param \DateTimeInterface $threshold
     *
     * @return OrderTracking[]
     */
    public function findNotSynchronized(\DateTimeInterface $threshold): array
    {
        return $this
            ->createQueryBuilder('ot')
            ->where('ot.deliveredDate = 0')
            ->andWhere('ot.createdAt > :threshold')
            ->setParameter('threshold', $threshold->getTimestamp())
            ->getQuery()
            ->getResult();
    }
}
