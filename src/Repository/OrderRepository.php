<?php

namespace MommyCom\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderProcessingStatusHistory;
use MommyCom\Entity\OrderProduct;
use MommyCom\Entity\OrderTracking;
use MommyCom\Model\Db\OrderRecord;

class OrderRepository extends EntityRepository
{
    /**
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOrdersCountForTest(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $magicFix = getenv('REGION') == 'Indonesia' ? -3 : 0;

        return (int)$qb->select('count(o.id)')
                ->from(Order::class, 'o')
                ->where($qb->expr()->gt('o.createdAt', strtotime('2018-06-05')))
                ->andWhere($qb->expr()->neq('o.processingStatus', OrderRecord::PROCESSING_CANCELLED))
                ->getQuery()
                ->getSingleScalarResult() + $magicFix;
    }

    /**
     * @param \DateTime $begin
     * @param \DateTime $end
     * @return array ['priceTotal' => .., 'countTotal' => ..]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCreatedStatsForInterval(\DateTime $begin, \DateTime $end): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        try {
            return $qb->select('sum(o.priceTotal) as priceTotal, count(o.id) as countTotal')
                ->from(Order::class, 'o')
                ->where($qb->expr()->gte('o.orderedAt', $begin->getTimestamp()))
                ->andWhere($qb->expr()->lte('o.orderedAt', $end->getTimestamp()))
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return ['priceTotal' => 0, 'countTotal' => 0];
        }
    }

    /**
     * @param \DateTime $begin
     * @param \DateTime $end
     * @return array ['priceTotal' => .., 'countTotal' => ..]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getShippedStatsForInterval(\DateTime $begin, \DateTime $end): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        try {
            return $qb->select('sum(o.priceTotal) as priceTotal, count(o.id) countTotal')
                ->from(Order::class, 'o')
                ->join(OrderTracking::class, 'ot', Join::WITH, "ot.orderId = o.id")
                ->where($qb->expr()->gte('ot.createdAt', $begin->getTimestamp()))
                ->andWhere($qb->expr()->lte('ot.createdAt', $end->getTimestamp()))
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return ['priceTotal' => 0, 'countTotal' => 0];
        }
    }

    /**
     * @param \DateTime $begin
     * @param \DateTime $end
     * @return array ['priceTotal' => .., 'countTotal' => ..]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDeliveredStatsForInterval(\DateTime $begin, \DateTime $end): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        try {
            return $qb->select('sum(o.priceTotal) as priceTotal, count(o.id) countTotal')
                ->from(Order::class, 'o')
                ->join(OrderTracking::class, 'ot', Join::WITH, "ot.orderId = o.id")
                ->where($qb->expr()->gte('ot.deliveredDate', $begin->getTimestamp()))
                ->andWhere($qb->expr()->lte('ot.deliveredDate', $end->getTimestamp()))
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return ['priceTotal' => 0, 'countTotal' => 0];
        }
    }

    /**
     * @param \DateTime $begin
     * @param \DateTime $end
     * @param $status
     * @return array ['priceTotal' => .., 'countTotal' => ..]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getStatusStatsForInterval(\DateTime $begin, \DateTime $end, $status): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        try {
            // внимание, эта штука будет завышать статистику, если за заданный интервал заказ более одного раза менял статус на указанный
            return $qb->select('sum(o.priceTotal) as priceTotal, count(o.id) countTotal')
                ->from(Order::class, 'o')
                ->leftJoin(OrderProcessingStatusHistory::class, 'h', Join::WITH, "h.orderId = o.id")
                ->where($qb->expr()->gte('h.time', $begin->getTimestamp()))
                ->andWhere($qb->expr()->lte('h.time', $end->getTimestamp()))
                ->andWhere("h.status = :status")
                ->setParameter("status", $status)
                ->andWhere("h.id is not null")
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return ['priceTotal' => 0, 'countTotal' => 0];
        }
    }

    /**
     * @param int $eventProductId
     *
     * @return null|Order
     */
    public function getCheapestOrderWithProduct(int $eventProductId): ?Order
    {
        try {
            return $this
                ->getEntityManager()
                ->createQueryBuilder()
                ->select('o')
                ->from(OrderProduct::class, 'op')
                ->join(Order::class, 'o', Join::WITH, 'o.id = op.orderId')
                ->where('op.productId = :productId')
                ->setParameter('productId', $eventProductId)
                ->groupBy('op.orderId')
                ->orderBy('SUM(op.price)', 'ASC')
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
