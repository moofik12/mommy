<?php

namespace MommyCom\Repository;

use Doctrine\ORM\EntityRepository;
use MommyCom\Entity\User;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserRepository extends EntityRepository implements UserProviderInterface, UserLoaderInterface
{
    /**
     * @param string $username
     *
     * @throws UsernameNotFoundException
     * @return UserInterface
     */
    public function loadUserByUsername($username)
    {
        /** @var UserInterface $user */
        $user = $this
            ->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy(['email' => $username]);

        if (null === $user) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return User
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }

        $username = $user->getUsername();

        /** @var User $refreshedUser */
        $refreshedUser = $this->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy(['email' => $username]);

        if (!$refreshedUser) {
            throw new UsernameNotFoundException(sprintf('User with username %s not found', $user->getUsername()));
        }

        return $refreshedUser;
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
