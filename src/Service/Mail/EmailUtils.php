<?php

namespace MommyCom\Service\Mail;

use MommyCom\Entity\User;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\Emailer;

class EmailUtils
{
    use ApplicationTrait;

    /**
     * @var Emailer
     */
    public $mailer;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    /**
     * EmailUtils constructor.
     * @param Emailer $emailer
     * @param TranslatorInterface $translator
     */
    public function __construct(Emailer $emailer, TranslatorInterface $translator)
    {
        $this->mailer = $emailer;
        $this->translator = $translator;
    }

    /**
     * @param User $user
     * @param string $password
     * @param string $fromAddress
     * @param string $fromName
     *
     * @throws \CException
     */
    public function sendRegistrationEmail(User $user, string $password, string $fromAddress, string $fromName)
    {
        $this->mailer->create(
            [$fromAddress => $fromName],
            [$user->getEmail()],
            $this->translator->t('IMPORTANT INFORMATION for a new club member'),
            [
                'view' => 'welcome2',
                'data' => ['model' => $user, 'password' => $password],
            ]
        );
    }

    /**
     * @param User $user
     * @param int $orderId
     * @param string $trackLink
     * @param string $fromAddress
     * @param string $fromName
     *
     * @throws \CException
     */
    public function sendTrackOrderEmail(User $user, int $orderId, string $trackLink, string $fromAddress, string $fromName)
    {
        $frontendUrl = $this
            ->app()
            ->frontendUrlManager
            ->createAbsoluteUrl('index/index');

        $this->mailer->create(
            [$fromAddress => $fromName],
            [$user->getEmail()],
            $this->translator->t('Your order №{order} has been shipped!', ['order' => $orderId]),
            [
                'view' => 'notification.order.shipped',
                'data' => [
                    'user' => $user,
                    'translator' => $this->translator,
                    'link' => $trackLink,
                    'baseUrl' => $this->app()->getBaseUrl(true),
                    'frontendUrl' => $frontendUrl,
                ],
            ]
        );
    }

    public function sendCustomEmail()
    {

    }
}