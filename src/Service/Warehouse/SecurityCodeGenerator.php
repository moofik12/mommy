<?php

namespace MommyCom\Service\Warehouse;

use MommyCom\Entity\EventProduct;

class SecurityCodeGenerator
{
    /**
     * Генерит секьюрити код для товара на складе
     *
     * @param EventProduct $product
     *
     * @return int
     */
    public function generate(EventProduct $product): int
    {
        $result = sha1($product->getProductId() . $product->getColor() . $product->getSize());
        $result = substr($result, 0, 4);
        $result = hexdec($result);
        $result = $result % 10000;

        return $result;
    }
}