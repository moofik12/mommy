<?php

namespace MommyCom\Service\Translator;

use MommyCom\Service\Region\Regions;

class RegionalTranslator implements TranslatorInterface
{
    /**
     * @var string $category
     */
    private $category;

    /**
     * @var string $language
     */
    private $language = null;

    /**
     * RegionTranslator constructor.
     *
     * @param Regions $regions
     * @param string $category
     */
    public function __construct(Regions $regions, string $category)
    {
        $this->category = $category;
        $this->language = $regions->getServerRegion()->getLocale();
    }

    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     */
    public function __invoke(string $sourceText, $params = []): string
    {
        return \Yii::t(
            $this->category,
            $sourceText,
            $params,
            null,
            $this->language
        );
    }

    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     */
    public function t(string $sourceText, $params = []): string
    {
        return $this($sourceText, $params);
    }
}
