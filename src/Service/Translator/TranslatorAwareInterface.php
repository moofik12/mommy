<?php

namespace MommyCom\Service\Translator;

interface TranslatorAwareInterface
{
    /**
     * @param TranslatorInterface|null $translator
     */
    public function setTranslator(TranslatorInterface $translator = null);
}
