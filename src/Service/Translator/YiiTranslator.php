<?php

namespace MommyCom\Service\Translator;

class YiiTranslator implements TranslatorInterface
{
    /**
     * @var string
     */
    private $category;

    /**
     * YiiTranslator constructor.
     *
     * @param string $category
     */
    public function __construct(string $category)
    {
        $this->category = $category;
    }

    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     */
    public function __invoke(string $sourceText, $params = []): string
    {
        return \Yii::t($this->category, $sourceText, $params);
    }

    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     */
    public function t(string $sourceText, $params = []): string
    {
        return $this($sourceText, $params);
    }
}
