<?php

namespace MommyCom\Service\Translator;

interface TranslatorInterface
{
    /**
     * Переводит фразу с исходного языка на целевой
     *
     * @param string $sourceText исходный текст
     * @param mixed $params опциональные параметры
     *
     * @return string
     */
    public function __invoke(string $sourceText, $params = []): string;

    /**
     * Переводит фразу с исходного языка на целевой (синоним для __invoke)
     *
     * @param string $sourceText
     * @param array $params
     *
     * @return string
     */
    public function t(string $sourceText, $params = []): string;
}
