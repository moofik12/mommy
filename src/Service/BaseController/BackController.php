<?php

namespace MommyCom\Service\BaseController;

use CAssetManager;
use CClientScript;
use CException;
use CHttpException;
use Doctrine\ORM\EntityManagerInterface;
use Yii;

abstract class BackController extends Controller
{
    const COOKIE_HIDE_ME_NAME = 'hideMe';
    const COOKIE_HIDE_ME_LIFETIME = 30 * 24 * 3600; // 30 days

    /**
     * @var string
     */
    public $assetsUrl = 'assets.';

    /**
     * @var array
     */
    protected $_scriptArray = [];

    /**
     * @var array
     */
    protected $_cssArray = [];

    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function accessRules()
    {
        return [
            // доступ только авторизированым пользователям
            [
                'allow',
                'users' => ['@'],
            ],
            // всем остальным разрешаем посмотреть только на страницу авторизации
            [
                'allow',
                'actions' => ['login'],
                'users' => ['*'],
            ],
            // запрещаем все остальное
            [
                'deny',
                'users' => ['*'],
            ],
        ];
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * @param \CAction $action
     *
     * @return bool
     * @throws CHttpException
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!\Yii::app()->user->checkAccess()) {
            \Yii::app()->user->setFlash('error', '<strong>' . \Yii::t('common', 'Access Denied!') . '</strong> '
                . \Yii::t('common', 'You are not allowed to do that action'));
            $url = \Yii::app()->request->getUrlReferrer() ?: \Yii::app()->createUrl('/index');
            $this->redirect($url);

            throw new CHttpException(403, \Yii::t('common', 'Access Denied!'));
        }

        if (!\Yii::app()->user->isGuest) {
            \Yii::app()->request->cookies[self::COOKIE_HIDE_ME_NAME] = new \CHttpCookie(self::COOKIE_HIDE_ME_NAME, 1, [
                'expire' => time() + self::COOKIE_HIDE_ME_LIFETIME,
            ]);
        }

        return true;
    }

    public function actions()
    {
        return [
            'page' => [
                'class' => 'CViewAction',
            ],
        ];
    }

    /**
     * Подключение js и css файлов перед рендерингом страницы
     *
     * @param string $view
     *
     * @return boolean
     */
    public function beforeRender($view)
    {
        parent::beforeRender($view);

        $jsFile = \Yii::app()->controller->action->id . '.js';
        $actionScriptFile = $this->getAssetsUrl() . '/' . $jsFile;
        $actionFile = Yii::getPathOfAlias($this->assetsUrl . 'js' . '/' . \Yii::app()->controller->id) . '/' . $jsFile;

        if (is_array($this->_scriptArray) && !empty($this->_scriptArray)) {
            foreach ($this->_scriptArray as $value) {
                \Yii::app()->clientScript->registerScriptFile($value, CClientScript::POS_BEGIN);
            }
        }
        if (is_array($this->_cssArray) && !empty($this->_cssArray)) {
            foreach ($this->_cssArray as $value) {
                \Yii::app()->getClientScript()->registerCssFile($value);
            }
        }
        if (file_exists($actionFile)) {
            \Yii::app()->clientScript->registerScriptFile($actionScriptFile, CClientScript::POS_BEGIN);
        }

        return true;
    }

    /**
     * Возвращает путь к папке с скриптами в public'е
     *
     * @return string
     */
    protected function getAssetsUrl()
    {
        $assetsPath = Yii::getPathOfAlias($this->assetsUrl);

        if (YII_DEBUG) {
            $assetsUrl = \Yii::app()->assetManager->publish($assetsPath, false, -1, true);
        } else {
            $assetsUrl = \Yii::app()->assetManager->publish($assetsPath);
        }

        return $assetsUrl . '/' . \Yii::app()->controller->id;
    }

    /**
     * Подключение дополнительных js файлов.
     *
     * @example
     * $this->requiredScript('libs/textAreaMaxLength.js');
     *
     * @param string $fileName
     *
     * @throws CException
     */
    public function requiredScript($fileName)
    {
        $actionFile = Yii::getPathOfAlias($this->assetsUrl) . DIRECTORY_SEPARATOR . $fileName;
        if (file_exists($actionFile)) {
            /** @var CAssetManager $assetManager */
            $assetManager = \Yii::app()->assetManager;
            $this->_scriptArray[] = $assetManager->publish(Yii::getPathOfAlias($this->assetsUrl)) . '/' . $fileName;
        }
    }

    /**
     * Подключение дополнительных css файлов.
     *
     * @example
     * $this->requiredScript('modal/Alert.css');
     *
     * @param string $fileName
     *
     * @throws CException
     */
    public function requiredCss($fileName)
    {
        $actionFile = Yii::getPathOfAlias($this->assetsUrl) . DIRECTORY_SEPARATOR . $fileName;
        if (file_exists($actionFile)) {
            /** @var CAssetManager $assetManager */
            $assetManager = \Yii::app()->assetManager;
            $this->_cssArray[] = $assetManager->publish(Yii::getPathOfAlias($this->assetsUrl)) . '/' . $fileName;
        }
    }

    /**
     * @deprecated более не используется
     *
     * @param string $descriptions описание данных просмотра
     * @param object $object объект который просматривался
     */
    public function commitView($object = null, $descriptions = '')
    {
        // заглушка
    }
}
