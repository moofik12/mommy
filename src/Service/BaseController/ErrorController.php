<?php

namespace MommyCom\Service\BaseController;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ErrorController extends Controller
{
    /**
     * @var bool
     */
    private $debug;

    /**
     * @var int[]
     */
    private $supportedErrorCodes;

    public function __construct(bool $debug, array $supportedErrorCodes)
    {
        $this->debug = $debug;
        $this->supportedErrorCodes = $supportedErrorCodes;

        parent::__construct('error');
    }

    public function showError(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $showException = $request->attributes->get('showException', $this->debug);

        if ($showException) {
            return $this->delegateToTwig($request, $exception, $logger);
        }

        $statusCode = $exception->getStatusCode();
        $message = $exception->getMessage();

        if ($request->isXmlHttpRequest()) {
            return new Response($message, (int)$statusCode);
        }

        $this->setAction(new class($this, 'index') extends \CAction
        {
            // because \CAction is abstract
        });

        return new Response(
            $this->renderError((string)$statusCode, $message),
            (int)$statusCode
        );
    }

    private function renderError(string $statusCode, string $message): string
    {
        $view = (in_array($statusCode, $this->supportedErrorCodes)) ? $statusCode : 'error';

        $data = [
            'errorCode' => $statusCode,
            'errorMessage' => $message,
        ];

        return parent::render($view, $data, true);
    }

    private function delegateToTwig(Request $request, FlattenException $exception, ?DebugLoggerInterface $logger)
    {
        $controller = $this->container->get('twig.controller.exception');

        return $controller->showAction($request, $exception, $logger);
    }

    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (\CException $e) {
            return null;
        }
    }
}
