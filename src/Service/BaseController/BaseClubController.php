<?php

namespace MommyCom\Service\BaseController;

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionDetector;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\AuthManager\AuthenticationTrait;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Security\Authentication\Encoder\UserPasswordEncoder;
use MommyCom\Security\User\UserBuilder;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\Facade\Doctrine;
use Yii;

/**
 * Class ClubController
 * @method void renderJson(array $params, $terminate = true)
 */
abstract class BaseClubController extends Controller
{
    use AuthenticationTrait;

    const LANDING_CLUB = 30;
    const LANDING_MOMMY = 31;
    const LANDING_TRANSFER = 32;
    const LANDING_WHITE = 33;
    const LANDING_MOON = 34;

    /**
     * @var string
     */
    public $layout = '//layouts/club';

    /**
     * @var string
     */
    public $bodyClass = '';

    /**
     * @var \CClientScript|null
     */
    private $clientScript = null;

    /**
     * @var string|null
     */
    private $assetsBaseUrl = null;

    /**
     * @var string[]
     */
    private $googleFonts = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        /** @var MommyWebApplication $app */
        $app = Yii::app();
        $this->clientScript = $app->getClientScript();
        $this->assetsBaseUrl = $app->getAssetManager()->publish(Yii::getPathOfAlias('assets.club'));

        parent::init();
    }

    /**
     * @param string $suffix
     *
     * @return string
     */
    public function assetPath(string $suffix): string
    {
        return $this->assetsBaseUrl . '/' . ltrim($suffix, '/');
    }

    /**
     * @param \CAction $action
     *
     * @return bool
     */
    protected function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if ('thanks' === $action->getId()) {
            return true;
        }

        $this->clientScript->reset();

        $clubCssFile = '/' . $action->getId() . '/css/club.css';

        if (is_file(Yii::getPathOfAlias('assets.club') . $clubCssFile)) {
            $this->clientScript->registerCssFile($this->assetPath($clubCssFile));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function beforeRender($view)
    {
        if (!parent::beforeRender($view)) {
            return false;
        }

        $this->renderGoogleFonts($this->googleFonts);

        return true;
    }

    /**
     * @param string[] $fonts
     */
    private function renderGoogleFonts(array $fonts): void
    {
        if (!$fonts) {
            return;
        }

        ksort($fonts);

        $href = 'https://fonts.googleapis.com/css?family=' . implode('|', $fonts);

        $this->clientScript->registerLinkTag('stylesheet', 'text/css', $href);
    }

    /**
     * @param string $font
     */
    protected function addGoogleFont(string $font): void
    {
        $font = strtr($font, ' ', '+');

        $this->googleFonts[$font] = $font;
    }

    protected function setRegionCookie()
    {
        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);
        $request = $this->app()->request;

        $cookie = new \CHttpCookie(RegionDetector::getCookieName(), $regions->getServerRegion()->getRegionName());
        $cookie->domain = $regions->getHostname(Region::DEFAULT);
        $cookie->expire = 2147483647; // много лет, степень двойки

        $request->cookies[RegionDetector::getCookieName()] = $cookie;
    }
}
