<?php

namespace MommyCom\Service\BaseController;

use CController;
use CException;
use CHtml;
use CJSON;
use CWebLogRoute;
use MommyCom\Service\Translator\TranslatorAwareInterface;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Yii;

abstract class Controller extends CController implements ContainerAwareInterface, TranslatorAwareInterface, TranslatorInterface
{
    use ApplicationTrait;

    /**
     * @var ContainerInterface|null
     */
    protected $container = null;

    /**
     * @var TranslatorInterface|null
     */
    protected $translator = null;

    /**
     * {@inheritdoc}
     */
    public function __construct($id = 'index', $module = null)
    {
        parent::__construct($id, $module);
    }

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param TranslatorInterface|null $translator
     */
    public function setTranslator(TranslatorInterface $translator = null)
    {
        $this->translator = $translator;
    }

    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     */
    public function __invoke(string $sourceText, $params = []): string
    {
        return $this->translator->t($sourceText, $params);
    }

    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     */
    public function t(string $sourceText, $params = []): string
    {
        return $this->translator->t($sourceText, $params);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        try {
            CHtml::setModelNameConverter(function ($model) {
                $className = is_object($model) ? get_class($model) : (string)$model;
                $className = trim($className, '\\');

                if (strpos($className, 'MommyCom\\') !== 0) {
                    return str_replace('\\', '_', $className);
                }

                $slashPos = strrpos($className, '\\');

                if (false !== $className) {
                    return substr($className, $slashPos + 1);
                }

                return $className;
            });
        } catch (CException $e) {
            // nothing to do
        }
    }

    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    public function actions()
    {
        return [
            'page' => [
                'class' => 'CViewAction',
            ],
        ];
    }

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = [];

    public function cache()
    {
        return [];
    }

    /**
     * @param string $name action name
     *
     * @return ActionOutputCache|null
     * @throws CException
     */
    protected function getActionCache($name)
    {
        static $instances = [];
        /* @var ActionOutputCache[] $instances */

        $configs = $this->cache();
        $path = $this->id . '/' . $name;
        if (!isset($instances[$path]) && isset($configs[$name])) {
            $config = $configs[$name];
            $instances[$path] = Yii::createComponent([
                    'class' => ActionOutputCache::class,
                    'controller' => $this,
                ] + $config);
        }

        return isset($instances[$path]) ? $instances[$path] : null;
    }

    /**
     * @inheritdoc
     */
    protected function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if ($cache = $this->getActionCache($action->id)) {
            return $cache->beginCache();
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function render($view, $data = null, $return = false)
    {
        if (!$this->beforeRender($view)) {
            return null;
        }

        $output = $this->renderPartial($view, $data, true);

        $cache = $this->getActionCache($this->action->id);
        if ($cache) {
            $cache->endCache($output);
        }

        if (($layoutFile = $this->getLayoutFile($this->layout)) !== false) {
            $output = $this->renderFile($layoutFile, ['content' => $output], true);
        }

        $this->afterRender($view, $output);

        $output = $this->processOutput($output);

        if ($return) {
            return $output;
        }

        echo $output;

        return null;
    }

    /**
     * @param array $params
     * @param bool $terminate
     */
    public function renderJson(array $params, $terminate = true)
    {
        $this->getCachingStack();
        @ob_clean(); // clear current output

        foreach (Yii::app()->log->routes as $route) { // disable additional routes like Log
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false;
            }
        }

        header('Content-type: application/json');

        echo CJSON::encode($params);

        if ($terminate) {
            Yii::app()->end();
        }
    }

    /**
     * @noinspection PhpDocMissingThrowsInspection
     *
     * @param string $view
     * @param array $parameters
     *
     * @return string
     */
    protected function renderTwig(string $view, array $parameters = []): string
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "renderTwig" method if the Templating Component or the Twig Bundle are not available. Try running "composer require symfony/twig-bundle".');
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->container->get('twig')->render($view, $parameters);
    }

    /**
     * @param string $view
     * @param array $parameters
     *
     * @return Response
     */
    protected function responseTwig(string $view, array $parameters = []): Response
    {
        return new Response($this->renderTwig($view, $parameters));
    }

    /**
     * Либо редиректит на предыдующую страницу либо по указанному урлу
     * Внимание: подклбчите кастомный HttpRequest который возвращает реферер только если он локальный
     *
     * @param $url
     * @param bool $terminate
     */
    public function redirectBackOr($url, $terminate = true)
    {
        $app = Yii::app();
        $returnUrl = $app->user->returnUrl;
        if ($returnUrl == $app->request->requestUri || empty($returnUrl)) {
            $this->redirect($url, $terminate);
        } else {
            $this->redirect($returnUrl, $terminate);
        }
    }

    /**
     * @return void
     */
    public function saveBackUrl($saveCurrent = false)
    {
        $request = Yii::app()->request;
        $user = Yii::app()->user;
        if (!$request->isPostRequest && !$request->isAjaxRequest) {
            $backUrl = $saveCurrent ? $request->requestUri : $request->urlReferrer;
            $user->setReturnUrl($backUrl);
        }
    }

    /**
     * @return string
     */
    public function getSavedBackUrl()
    {
        return Yii::app()->user->getReturnUrl(['index/index']);
    }

    /**
     * @param string|array $item
     */
    public function addToBreadcrumbs($item)
    {
        $this->breadcrumbs = array_merge($this->breadcrumbs, (array)$item);
    }
}
