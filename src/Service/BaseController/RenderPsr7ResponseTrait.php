<?php

namespace MommyCom\Service\BaseController;

use Psr\Http\Message\ResponseInterface;

trait RenderPsr7ResponseTrait
{
    /**
     * Выводит содержимое ответа и останавливает выполнение
     *
     * @param ResponseInterface $response
     */
    protected function renderPsr7Response(ResponseInterface $response): void
    {
        $app = \Yii::app();

        $app->getController()->getCachingStack();
        @ob_clean(); // clear current output

        foreach ($app->log->routes as $route) { // disable additional routes like Log
            if ($route instanceof \CWebLogRoute) {
                $route->enabled = false;
            }
        }

        (new SapiEmitter())->emit($response);

        $app->end();
    }
}
