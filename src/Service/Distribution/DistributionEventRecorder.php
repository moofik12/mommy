<?php

namespace MommyCom\Service\Distribution;

use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\CurrentTime;

class DistributionEventRecorder
{
    use DistributionValidatorTrait;

    /**
     * Создание записи о пользователе и событии на основании данных полученых в запросе
     *
     * @param array $params
     * @param array $serverParams
     * @param int $userId
     */
    public function createRecordFromArray(array $params, array $serverParams, int $userId)
    {
        $eventCode = $params['event'];
        $anonymousId = $params['anonymousId'];
        $email = $params['email'] ?? '';

        $event = MailingEventRecord::model()->find([
            'condition' => 'code=:code',
            'params' => [':code' => $eventCode],
        ]);

        $eventData = MailingDataRecord::model()->getModelFromParams($event, $userId, $email, $anonymousId);
        $oldEventId = $eventData->distribution_event_id;
        $eventChangeAllowed = true;

        if (null !== $oldEventId) {
            $eventChangeAllowed = $this->isEventChangeAllowed($event, $oldEventId);
        }

        $eventData->distribution_event_id = $eventChangeAllowed ? $event->id : $oldEventId;
        $eventData->ip_address = $serverParams['REMOTE_ADDR'];
        $eventData->country = $params['country'] ?? null;
        $eventData->source = $params['source'] ?? null;

        if ($userId === 0) {
            $eventData->anonymous_id = $anonymousId;
            $eventData->is_registered = false;
            $eventData->user_id = 0;
        } else {
            $user = UserRecord::model()->findByPk($userId);
            $email = $user->email;
            $eventData->is_registered = true;
            $eventData->user_id = $userId;
        }

        if ($event->canSubscribe()) {
            if (null !== $email && $this->validateEmail($email)) {
                $eventData->email = $email;
            } else {
                $eventData->distribution_event_id = $oldEventId;
            }
        }

        $eventData->save();
    }

    /**
     * @param UserRecord $userRecord
     * @param CartRecord $cartRecord
     * @param MailingEventRecord $eventRecord
     * @param MailingDataRecord $dataRecord
     */
    public function makeRecordsForLostCarts(
        UserRecord $userRecord,
        CartRecord $cartRecord,
        MailingEventRecord $eventRecord,
        MailingDataRecord $dataRecord
    ) {
        $time = CurrentTime::getDateTimeImmutable()
            ->modify('-1 day')
            ->getTimestamp();

        $userIds = $cartRecord
            ->createdAtLower($time)
            ->isSentNotification(false)
            ->findColumnDistinct('user_id');

        $event = $eventRecord
            ->withName(MailingEventRecord::LOST_CART)
            ->find();

        $users = $userRecord
            ->idIn($userIds)
            ->findAll();

        foreach ($users as $user) {
            $eventData = $dataRecord->getModelFromParams($event, (int)$user->id);

            if ($eventData->distribution_event_id === $event->id) {
                continue;
            }

            $eventData->user_id = $user->id;
            $eventData->email = $user->email;
            $eventData->is_registered = true;
            $eventData->distribution_event_id = $event->id;
            $eventData->save();
        }
    }

    /**
     * @param $event
     * @param int $oldEventId
     *
     * @return bool
     */
    private function isEventChangeAllowed($event, int $oldEventId)
    {
        $previousPriority = $event->findByPk($oldEventId)->priority;
        $currentPriority = $event->priority;
        $eventChangeAllowed = false;

        if ($currentPriority > $previousPriority) {
            $eventChangeAllowed = true;
        }

        return $eventChangeAllowed;
    }
}
