<?php

namespace MommyCom\Service\Distribution\Handler;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\YiiComponent\MailerServices\MailerServiceAbstract;

class SubscribeUserHandler implements DistributionHandler
{
    use MommyCom\Service\Distribution\DistributionValidatorTrait;

    /**
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     * @param MailerServiceAbstract $mailerService
     *
     * @throws \CException
     */
    public function process(
        MailingDataRecord $dataRecord,
        MailingEventRecord $eventRecord,
        MailerServiceAbstract $mailerService
    ): void {
        if ($eventRecord->canSubscribe() &&
            !$dataRecord->checkUserSubscribed() &&
            $this->validateEmail($dataRecord->email)) {
            $result = $mailerService->subscribe($dataRecord);

            if ($mailerService->isErrorResult($result)) {
                throw new \RuntimeException(self::API_ERROR);
            }

            $dataRecord->markUserAsSubscribed($eventRecord);
        }
    }
}
