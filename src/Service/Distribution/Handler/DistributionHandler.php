<?php

namespace MommyCom\Service\Distribution\Handler;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\YiiComponent\MailerServices\MailerServiceAbstract;

interface DistributionHandler
{
    /**
     * @var string
     */
    public const EVENT_NOT_SPECIFIED_ERROR = 'Event not specified.';

    /**
     * @var string
     */
    public const QUERY_ERROR = 'Query error';

    /**
     * @var string
     */
    public const API_ERROR = 'Invalid request data';

    /**
     * @var string
     */
    public const SUCCESS = 'Success';

    /**
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     * @param MailerServiceAbstract $mailerService
     *
     * @throws \CException
     */
    public function process(
        MailingDataRecord $dataRecord,
        MailingEventRecord $eventRecord,
        MailerServiceAbstract $mailerService
    ): void;
}
