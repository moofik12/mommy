<?php

namespace MommyCom\Service\Distribution\Handler;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Service\Distribution\DistributionUserSynchronizer;
use MommyCom\YiiComponent\MailerServices\MailerServiceAbstract;
use MommyCom\YiiComponent\MailerServices\MailerServiceUserModel;

class MassImportHandler implements DistributionHandler
{
    /**
     * @var string $region
     */
    private $region;

    /**
     * @var array
     */
    private $userEventCache = [];

    /**
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     * @param MailerServiceAbstract $mailingService
     *
     * @throws \CException
     */
    public function process(
        MailingDataRecord $dataRecord,
        MailingEventRecord $eventRecord,
        MailerServiceAbstract $mailingService
    ): void {
        $priorityArray = array_reverse($eventRecord->getPrioritiesArray());
        $lists = $mailingService->getLists();
        $list = $lists[$this->region];

        foreach ($priorityArray as $event) {
            $page = 0;

            do {
                $mailerUsers = $this->getUsersForSubscription($dataRecord, $event, $page);

                if (count($mailerUsers) !== 0) {
                    $this->updateUserMap($mailerUsers);
                    $this->subscribeUsers($mailingService, $mailerUsers, $dataRecord, $event, $list);
                }

                $page++;
            } while (count($mailerUsers) >= DistributionUserSynchronizer::CHUNK_LIMIT);
        }
    }

    /**
     * Отсеивает записи с низкими приоритетом на основании
     * кеша пользователей, хранящего подписанные email-ы
     *
     * @param array $mailerUsers
     *
     * @return array
     */
    private function getActualUserArray(array $mailerUsers)
    {
        $users = [];

        foreach ($mailerUsers as $mailerUser) {
            if (!isset($this->userEventCache[$mailerUser->email])) {
                $users[] = $mailerUser;
            }
        }

        return $users;
    }

    /**
     * Записывает в переменную-кэш email-ы уже подписанных на рассылки пользователей
     *
     * @param array $mailerUsers
     */
    private function updateUserMap(array $mailerUsers)
    {
        /**
         * @var MailerServiceUserModel $mailerUser
         */
        foreach ($mailerUsers as $mailerUser) {
            $this->userEventCache[$mailerUser->email] = true;
        }
    }

    /**
     * Получить список пользователей для подписки
     * Пользователи фильтруются - если до этого, пользователь с данным email был подписан на
     * более приоритетное событие, то он не включается в список
     *
     * @param $event
     * @param $page
     * @param MailingDataRecord $dataRecord
     *
     * @return array
     */
    private function getUsersForSubscription(MailingDataRecord $dataRecord, $event, $page)
    {
        $mailerUsers = $dataRecord->withEvent($event->id)
            ->withEmailNotNull()
            ->limit(500, $page)
            ->toMailerUserModels();

        $mailerUsers = $this->getActualUserArray($mailerUsers);

        return $mailerUsers;
    }

    /**
     * Подписывает юзеров на рассылки порционно, после этого делает пометки в нашей БД что юзеры с конкретным событием подписаны
     *
     * @param MailerServiceAbstract $mailingService
     * @param array $mailerUsers
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $event
     * @param $list
     *
     * @throws \CException
     */
    private function subscribeUsers(
        MailerServiceAbstract $mailingService,
        array $mailerUsers,
        MailingDataRecord $dataRecord,
        MailingEventRecord $event,
        $list)
    {
        $mailingService->init($list[$event->name]);
        $mailingService->importContacts($mailerUsers, [
            'overwrite' => true,
        ]);
        $dataRecord->markMailerUsersAsSubscribed($mailerUsers, $event->id);
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region): void
    {
        $this->region = $region;
    }
}
