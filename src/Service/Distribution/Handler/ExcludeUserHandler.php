<?php

namespace MommyCom\Service\Distribution\Handler;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\YiiComponent\MailerServices\MailerServiceAbstract;

class ExcludeUserHandler implements DistributionHandler
{
    /**
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     * @param MailerServiceAbstract $mailerService
     *
     * @throws \CException
     */
    public function process(
        MailingDataRecord $dataRecord,
        MailingEventRecord $eventRecord,
        MailerServiceAbstract $mailerService
    ): void {
        if (!$dataRecord->checkUserSubscribed()) {
            return;
        }

        $result = $mailerService->exclude($dataRecord);

        if ($mailerService->isErrorResult($result)) {
            throw new \RuntimeException(self::API_ERROR);
        }

        $dataRecord->markUserAsUnsubscribed();
    }
}
