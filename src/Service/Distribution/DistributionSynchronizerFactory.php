<?php

namespace MommyCom\Service\Distribution;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Service\Distribution\Handler\MassImportHandler;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\MailerServices\MailerServiceAbstract;

class DistributionSynchronizerFactory
{
    public static function createUserSynchronizer(
        MailerServiceAbstract $mailerService,
        Regions $regions,
        MassImportHandler $massImportHandler
    ) {
        return new DistributionUserSynchronizer(
            MailingDataRecord::model(),
            MailingEventRecord::model(),
            $mailerService,
            $massImportHandler,
            $regions->getServerRegion()->getRegionName()
        );
    }
}
