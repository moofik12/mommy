<?php

namespace MommyCom\Service\Distribution;

use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\MailingEventRecord;
use MommyCom\Service\Distribution\Handler\MassImportHandler;
use MommyCom\YiiComponent\MailerServices\MailerServiceAbstract;
use MommyCom\YiiComponent\MailerServices\MailerServiceUserModel;

class DistributionUserSynchronizer
{
    public const CHUNK_LIMIT = 500;

    /**
     * @var MailingDataRecord $dataRecord
     */
    private $dataRecord;

    /**
     * @var MailingEventRecord $eventRecord
     */
    private $eventRecord;

    /**
     * @var MailerServiceAbstract $mailingService
     */
    private $mailingService;

    /**
     * @var MassImportHandler $massImportAction
     */
    private $massImportAction;

    /**
     * @var string $country
     */
    private $region;

    /**
     * @var MailerServiceUserModel[] $intersectionUserRecord
     */
    private $intersectionMailerUserRecords = [];

    /**
     * DistributionSynchronizer constructor.
     *
     * @param MailingDataRecord $dataRecord
     * @param MailingEventRecord $eventRecord
     * @param MailerServiceAbstract $mailingService
     * @param MassImportHandler $distributionMassImport
     * @param string $region
     */
    public function __construct(
        MailingDataRecord $dataRecord,
        MailingEventRecord $eventRecord,
        MailerServiceAbstract $mailingService,
        MassImportHandler $distributionMassImport,
        string $region
    ) {
        $this->dataRecord = $dataRecord;
        $this->eventRecord = $eventRecord;
        $this->mailingService = $mailingService;
        $this->region = $region;
        $this->massImportAction = $distributionMassImport;
        $this->massImportAction->setRegion($region);
    }

    /**
     * Синхронизирует БД сайта с БД Unisender
     *
     * @throws \CException
     */
    public function sync()
    {
        $lists = $this->mailingService->getLists();

        foreach ($lists[$this->region] as $eventName => $listId) {
            $this->mailingService->init($listId);

            $offset = 0;
            do {
                $externalRecords = $this->mailingService->exportContacts($offset, self::CHUNK_LIMIT);

                $distributionEvent = $this->eventRecord
                    ->withName($eventName)
                    ->find();

                if (count($externalRecords) > 0) {
                    $intersection = $this->dataRecord->fromMailerUserArray($externalRecords, 'email');
                    $this->deleteExternalAbsentUsers($intersection, $externalRecords);
                    $this->deleteInternalAbsentUsers($distributionEvent);
                }

                $offset += self::CHUNK_LIMIT;
            } while (count($externalRecords) >= self::CHUNK_LIMIT);
        }

        $this->massImportAction->process($this->dataRecord, $this->eventRecord, $this->mailingService);
    }

    /**
     * Подготовливает список тех пользователей, которые отстутствуют в нашей БД
     *
     * @param array $intersection
     * @param MailerServiceUserModel[] $externalRecords
     */
    private function deleteExternalAbsentUsers(array $intersection, array $externalRecords)
    {
        $externalRecords = array_filter($externalRecords, function ($value) use ($intersection) {
            if (array_key_exists($value->email, $intersection)) {
                $this->intersectionMailerUserRecords[] = $value;
                return false;
            }

            return true;
        });

        $this->mailingService->importContacts(array_values($externalRecords), [
            'delete' => true,
        ]);
    }

    /**
     * Удаляет записи об отписавшихся с конкретных рассылок пользователей из нашей БД
     *
     * @param MailingEventRecord $distributionEvent
     *
     * @throws \CDbException
     */
    private function deleteInternalAbsentUsers(MailingEventRecord $distributionEvent)
    {
        foreach ($this->intersectionMailerUserRecords as $record) {
            if (!$record->isSubscribed && !$record->isPossiblySubscribe) {
                /**
                 * @var MailingDataRecord $dataRecord
                 */
                $dataRecord = $this->dataRecord
                    ->withEvent($distributionEvent->id)
                    ->withEmail($record->email)
                    ->find();

                if (!is_null(!$dataRecord)) {
                    $dataRecord->delete();
                }
            }
        }
    }
}
