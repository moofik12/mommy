<?php

namespace MommyCom\Service\Distribution;

trait DistributionValidatorTrait
{
    /**
     * Валидирует e-mail
     *
     * @param string $email
     *
     * @return bool
     */
    private function validateEmail(string $email): bool
    {
        if (empty($email)) {
            return false;
        }

        $email = str_replace(' ', '', $email);
        $regex = \Yii::app()->params['validatorRegex']['email']['default']['pattern'];

        return (bool)preg_match('/' . $regex . '/', $email);
    }
}
