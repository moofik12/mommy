<?php

namespace MommyCom\Service\Delivery\FormModel;

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Service\Translator\TranslatorInterface;

abstract class DeliveryModel extends \CFormModel implements \JsonSerializable
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var string|null
     */
    private $modelName = null;

    /**
     * DeliveryModel constructor.
     *
     * @param TranslatorInterface $translator
     * @param string $scenario
     */
    public function __construct(TranslatorInterface $translator, string $scenario = '')
    {
        $this->translator = $translator;

        parent::__construct($scenario);
    }

    /**
     * @return string
     */
    public function getModelName(): string
    {
        if ($this->modelName) {
            return $this->modelName;
        }

        $name = get_class($this);
        $slashPos = strrpos($name, '\\');

        if (false !== $slashPos) {
            $name = substr($name, $slashPos + 1);
        }

        return $this->modelName = $name;
    }

    /**
     * @param \CHttpRequest $request
     *
     * @return $this
     */
    public function setAttributesFromRequest(\CHttpRequest $request): self
    {
        $attributes = $request->getPost($this->getModelName(), []);
        $this->setAttributes($attributes);

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->getAttributes();
    }

    /**
     * @return AttributesDefinition[]
     */
    abstract public function getAttributesDefinition(): array;

    /**
     * @return string
     */
    abstract public function getDeliveryAddress(): string;

    /**
     * @param OrderProductRecord[] $products
     *
     * @return float|null
     */
    abstract public function calcDeliveryCost(array $products): ?float;

    /**
     * @return bool
     */
    abstract public function isCashOnDelivery(): bool;
}
