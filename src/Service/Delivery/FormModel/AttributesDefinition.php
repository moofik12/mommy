<?php

namespace MommyCom\Service\Delivery\FormModel;

class AttributesDefinition
{
    public const TYPE_HIDDEN = 0;
    public const TYPE_TEXT_AREA = 1;
    public const TYPE_SELECT = 2;
    public const TYPE_YES_NO = 3;

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var int
     */
    public $type = self::TYPE_TEXT_AREA;

    /**
     * @var string
     */
    public $label = '';

    /**
     * @var string
     */
    public $ajaxUrl = '';

    /**
     * @var string|\Closure
     */
    public $initialValue = '';

    /**
     * @var string[]
     */
    public $dependencies = [];

    /**
     * @var string[]
     */
    public $dependents = [];

    /**
     * @param array $properties
     *
     * @return AttributesDefinition
     */
    public static function fromArray(array $properties): self
    {
        $definition = new self();

        foreach ($properties as $property => $value) {
            if (!property_exists($definition, $property)) {
                throw new \LogicException('Invalid property name ' . $property);
            }

            $definition->{$property} = $value;
        }

        return $definition;
    }

    /**
     * @return string
     */
    public function resolveInitialValue(): string
    {
        if ($this->initialValue instanceof \Closure) {
            $closure = $this->initialValue;
            $this->initialValue = $closure();
        }

        return $this->initialValue;
    }
}
