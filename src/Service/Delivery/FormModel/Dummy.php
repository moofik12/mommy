<?php

namespace MommyCom\Service\Delivery\FormModel;

use MommyCom\Model\Db\OrderProductRecord;

class Dummy extends DeliveryModel
{
    /**
     * @var string
     */
    public $address;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['address', 'filter', 'filter' => 'trim'],
            ['address', 'required', 'on' => 'callcenter'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'address' => $this->translator->t('Address'),
        ];
    }

    /**
     * @return AttributesDefinition[]
     */
    public function getAttributesDefinition(): array
    {
        return [
            'address' => AttributesDefinition::fromArray([
                'name' => 'address',
                'type' => AttributesDefinition::TYPE_TEXT_AREA,
            ]),
        ];
    }

    /**
     * @return string
     */
    public function getDeliveryAddress(): string
    {
        return (string)$this->address;
    }

    /**
     * @param OrderProductRecord[] $products
     *
     * @return float
     */
    public function calcDeliveryCost(array $products): float
    {
        return 0.0;
    }

    /**
     * @return bool
     */
    public function isCashOnDelivery(): bool
    {
        return false;
    }
}
