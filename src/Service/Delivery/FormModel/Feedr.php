<?php

namespace MommyCom\Service\Delivery\FormModel;

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Service\Delivery\Api\ApiException;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Translator\TranslatorInterface;

class Feedr extends DeliveryModel
{
    /**
     * @var int
     */
    public $provinceId;

    /**
     * @var int
     */
    public $cityId;

    /**
     * @var int
     */
    public $suburbId;

    /**
     * @var int
     */
    public $areaId;

    /**
     * @var int
     */
    public $zipCode;

    /**
     * @var string
     */
    public $address;

    /**
     * @var bool
     */
    public $cashOnDelivery;

    /**
     * @var int
     */
    public $rateId;

    /**
     * @var string
     */
    public $rateVisualName;

    /**
     * @var FeedrApi
     */
    private $feedrApi;

    /**
     * Feedr constructor.
     *
     * @param FeedrApi $feedrApi
     * @param TranslatorInterface $translator
     * @param string $scenario
     */
    public function __construct(FeedrApi $feedrApi, TranslatorInterface $translator, string $scenario = '')
    {
        parent::__construct($translator, $scenario);

        $this->feedrApi = $feedrApi;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['provinceId, cityId, suburbId, areaId, zipCode, address, cashOnDelivery, rateId, rateVisualName', 'safe'],

            ['provinceId, cityId, suburbId, areaId, address', 'required', 'on' => 'step1'],
            ['address', 'filter', 'filter' => 'trim', 'on' => 'step1'],
            ['provinceId, cityId, suburbId, areaId, zipCode', 'numerical', 'integerOnly' => true, 'on' => 'step1'],
            ['provinceId, cityId, suburbId, areaId', 'filter', 'filter' => 'intval', 'on' => 'step1'],
            ['provinceId, cityId, suburbId, areaId, address', 'requiredWhenProvince', 'on' => 'step1'],

            ['rateId', 'required', 'on' => 'step2'],
            ['rateVisualName', 'filter', 'filter' => 'trim', 'on' => 'step2'],
            ['rateId', 'numerical', 'integerOnly' => true, 'on' => 'step2'],
            ['rateId', 'requiredWhenProvince', 'on' => 'step2'],
            ['rateId', 'filter', 'filter' => 'intval', 'on' => 'step2'],

            ['address, rateVisualName, cashOnDelivery', 'filter', 'filter' => 'trim', 'on' => 'step3'],
            ['provinceId, cityId, suburbId, areaId, rateId, zipCode', 'numerical', 'integerOnly' => true, 'on' => 'step3'],
            ['provinceId, cityId, suburbId, areaId, rateId, address', 'requiredWhenProvince', 'on' => 'step3'],
            ['provinceId, cityId, suburbId, areaId, rateId', 'filter', 'filter' => 'intval', 'on' => 'step3'],
            ['cashOnDelivery', 'filter', 'filter' => 'boolval', 'on' => 'step3'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'provinceId' => $this->translator->t('Province'),
            'cityId' => $this->translator->t('City'),
            'suburbId' => $this->translator->t('Suburb'),
            'areaId' => $this->translator->t('Area'),
            'zipCode' => $this->translator->t('ZIP code'),
            'address' => $this->translator->t('Address'),
            'rateId' => $this->translator->t('Courier delivery'),
            'cashOnDelivery' => $this->translator->t('Cash on delivery'),
        ];
    }

    /**
     * @return AttributesDefinition[]
     */
    public function getAttributesDefinition(): array
    {
        return [
            'provinceId' => AttributesDefinition::fromArray([
                'name' => 'provinceId',
                'type' => AttributesDefinition::TYPE_SELECT,
                'label' => $this->translator->t('Province'),
                'ajaxUrl' => \Yii::app()->createUrl('feedr/ajaxProvinces'),
                'initialValue' => function () {
                    return $this->provinceId ? $this->feedrApi->getProvinceName($this->provinceId) : '';
                },
                'dependencies' => [],
                'dependents' => ['cityId', 'suburbId', 'areaId', 'rateId'],
            ]),
            'cityId' => AttributesDefinition::fromArray([
                'name' => 'cityId',
                'type' => AttributesDefinition::TYPE_SELECT,
                'label' => $this->translator->t('City'),
                'ajaxUrl' => \Yii::app()->createUrl('feedr/ajaxCities'),
                'initialValue' => function () {
                    return ($this->provinceId && $this->cityId)
                        ? $this->feedrApi->getCityName($this->provinceId, $this->cityId) : '';
                },
                'dependencies' => ['provinceId'],
                'dependents' => ['suburbId', 'areaId', 'rateId'],
            ]),
            'suburbId' => AttributesDefinition::fromArray([
                'name' => 'suburbId',
                'type' => AttributesDefinition::TYPE_SELECT,
                'label' => $this->translator->t('Suburb'),
                'ajaxUrl' => \Yii::app()->createUrl('feedr/ajaxSuburbs'),
                'initialValue' => function () {
                    return ($this->cityId && $this->suburbId)
                        ? $this->feedrApi->getSuburbName($this->cityId, $this->suburbId) : '';
                },
                'dependencies' => ['provinceId', 'cityId'],
                'dependents' => ['areaId', 'rateId'],
            ]),
            'areaId' => AttributesDefinition::fromArray([
                'name' => 'areaId',
                'type' => AttributesDefinition::TYPE_SELECT,
                'label' => $this->translator->t('Area'),
                'ajaxUrl' => \Yii::app()->createUrl('feedr/ajaxAreas'),
                'initialValue' => function () {
                    return ($this->suburbId && $this->areaId)
                        ? $this->feedrApi->getAreaName($this->suburbId, $this->areaId) : '';
                },
                'dependencies' => ['provinceId', 'cityId', 'suburbId'],
                'dependents' => ['rateId'],
            ]),
            'address' => AttributesDefinition::fromArray([
                'name' => 'address',
                'type' => AttributesDefinition::TYPE_TEXT_AREA,
                'label' => $this->translator->t('Address'),
                'initialValue' => function () {
                    return (string)$this->address;
                },
            ]),
            'cashOnDelivery' => AttributesDefinition::fromArray([
                'name' => 'cashOnDelivery',
                'type' => AttributesDefinition::TYPE_YES_NO,
                'label' => $this->translator->t('Cash on delivery'),
                'dependencies' => ['provinceId', 'cityId', 'suburbId', 'areaId'],
                'dependents' => ['rateId'],
            ]),
            'rateId' => AttributesDefinition::fromArray([
                'name' => 'rateId',
                'type' => AttributesDefinition::TYPE_SELECT,
                'label' => $this->translator->t('Courier delivery'),
                'ajaxUrl' => \Yii::app()->createUrl('feedr/ajaxRates'),
                'initialValue' => function () {
                    return (string)$this->rateVisualName;
                },
                'dependencies' => ['provinceId', 'cityId', 'suburbId', 'areaId', 'cashOnDelivery'],
                'dependents' => [],
            ]),
            'rateVisualName' => AttributesDefinition::fromArray([
                'name' => 'rateVisualName',
                'type' => AttributesDefinition::TYPE_HIDDEN,
            ]),
        ];
    }

    /**
     * @return string
     * @throws ApiException
     */
    public function getDeliveryAddress(): string
    {
        $parts = [];

        $parts[] = $this->feedrApi->getProvinceName($this->provinceId);
        $parts[] = $this->feedrApi->getCityName($this->provinceId, $this->cityId);
        $parts[] = $this->feedrApi->getSuburbName($this->cityId, $this->suburbId);
        $parts[] = $this->feedrApi->getAreaName($this->suburbId, $this->areaId);
        $parts[] = $this->zipCode;

        $parts = array_filter($parts);

        return implode(', ', $parts);
    }

    /**
     * @param OrderProductRecord[] $products
     *
     * @return float|null
     */
    public function calcDeliveryCost(array $products): ?float
    {
        $weight = 0;
        $cost = 0.0;

        foreach ($products as $product) {
            $weight += (int)$product->product->weight * $product->number;
            $cost += $product->getPriceTotal();
        }

        $weight /= 1000;
        $areaId = (int)$this->areaId;
        $cashOnDelivery = (bool)$this->cashOnDelivery;

        try {
            $domesticRates = $this->feedrApi->getDomesticRates($areaId, $weight, 30, 20, 10, $cost, $cashOnDelivery);

            $rates = array_merge($domesticRates['regular'] ?? [], $domesticRates['express'] ?? []);

            foreach ($rates as $rate) {
                if ((int)$this->rateId === (int)$rate['rate_id']) {
                    return (float)$rate['finalRate'];
                }
            }
        } catch (ApiException $e) {
        } catch (\RuntimeException $e) {
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isCashOnDelivery(): bool
    {
        return (bool)$this->cashOnDelivery;
    }

    public function requiredWhenProvince(string $attribute): void
    {
        if (!$this->provinceId) {
            return;
        }

        $requiredValidator = new \CRequiredValidator();
        $requiredValidator->on = [];
        $requiredValidator->except = [];
        $requiredValidator->attributes = [$attribute];

        $requiredValidator->validate($this);
    }

    public function setAttributes($values, $safeOnly = true)
    {
        if (!isset($values['rateId'])) {
            return parent::setAttributes($values, $safeOnly);
        }

        if ($values['rateId'] < 0) {
            $values['rateId'] *= -1;
            $values['cashOnDelivery'] = false;
        }

        if (!isset($values['cashOnDelivery'])) {
            $values['cashOnDelivery'] = true;
        }

        return parent::setAttributes($values, $safeOnly);
    }
}
