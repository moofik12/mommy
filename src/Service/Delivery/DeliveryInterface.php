<?php

namespace MommyCom\Service\Delivery;

use MommyCom\Service\Delivery\FormModel\DeliveryModel;

interface DeliveryInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return DeliveryApiInterface
     */
    public function getApi(): DeliveryApiInterface;

    /**
     * @return DeliveryModel
     */
    public function createFormModel(): DeliveryModel;

    /**
     * @return bool
     */
    public function isFulfilment(): bool;
}
