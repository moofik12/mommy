<?php

namespace MommyCom\Service\Delivery;

use MommyCom\Service\Delivery\Api\DummyApi;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Delivery\FormModel\Dummy;
use MommyCom\Service\Translator\TranslatorInterface;

class DummyDelivery implements DeliveryInterface
{
    private const NAME = 'Dummy delivery';

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * DummyDelivery constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return DeliveryDirectory::DUMMY;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return DeliveryApiInterface
     */
    public function getApi(): DeliveryApiInterface
    {
        return new DummyApi();
    }

    /**
     * @return DeliveryModel
     */
    public function createFormModel(): DeliveryModel
    {
        return new Dummy($this->translator);
    }

    /**
     * @return bool
     */
    public function isFulfilment(): bool
    {
        return true;
    }
}
