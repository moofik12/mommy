<?php

namespace MommyCom\Service\Delivery;

use MommyCom\Service\Order\OrderInterface;

interface DeliveryApiInterface
{
    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function createOrder(OrderInterface $order): string;

    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function getTrackingCode(OrderInterface $order): string;

    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function getLabelUrl(OrderInterface $order): string;

    /**
     * @param OrderInterface $order
     *
     * @return int
     */
    public function getDeliveryPrice(OrderInterface $order): int;

    /**
     * @param OrderInterface $order
     *
     * @return \DateTimeImmutable
     */
    public function getDeadlineForDelivery(OrderInterface $order): \DateTimeImmutable;

    /**
     * @param OrderInterface $order
     *
     * @return int
     */
    public function getDeliveryStatus(OrderInterface $order): int;

    /**
     * @param OrderInterface $order
     */
    public function confirmOrder(OrderInterface $order): void;

    /**
     * @param string $trackingCode
     *
     * @return null|string
     */
    public function findDeliveryId(string $trackingCode): ?string;

    /**
     * @param string $trackingCode
     *
     * @return string
     */
    public function getTrackUrl(string $trackingCode): string;
}
