<?php

namespace MommyCom\Service\Delivery;

use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\Service\Translator\TranslatorInterface;

class FeedrDelivery implements DeliveryInterface
{
    private const NAME = 'Feedr';

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var FeedrApi
     */
    private $feedrApi;

    /**
     * FeedrDelivery constructor.
     *
     * @param TranslatorInterface $translator
     * @param FeedrApi $feedrApi
     */
    public function __construct(TranslatorInterface $translator, FeedrApi $feedrApi)
    {
        $this->translator = $translator;
        $this->feedrApi = $feedrApi;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return DeliveryDirectory::FEEDR;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return DeliveryModel
     */
    public function createFormModel(): DeliveryModel
    {
        return new Feedr($this->feedrApi, $this->translator);
    }

    /**
     * @return DeliveryApiInterface
     */
    public function getApi(): DeliveryApiInterface
    {
        return $this->feedrApi;
    }

    /**
     * @return bool
     */
    public function isFulfilment(): bool
    {
        return true;
    }
}
