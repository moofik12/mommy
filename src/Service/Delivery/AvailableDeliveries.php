<?php

namespace MommyCom\Service\Delivery;

use ArrayIterator;
use Countable;
use IteratorAggregate;

class AvailableDeliveries implements IteratorAggregate, Countable
{
    /**
     * @var DeliveryInterface[]
     */
    private $deliveries = [];

    /**
     * DeliveryDirectory constructor.
     *
     * @param DeliveryInterface[] $deliveries
     */
    public function __construct(array $deliveries)
    {
        foreach ($deliveries as $delivery) {
            $this->addDelivery($delivery);
        }
    }

    /**
     * @param DeliveryInterface $delivery
     */
    private function addDelivery(DeliveryInterface $delivery): void
    {
        $this->deliveries[$delivery->getId()] = $delivery;
    }

    /**
     * @return DeliveryInterface[]
     */
    public function getDeliveries(): array
    {
        return $this->deliveries;
    }

    /**
     * @return DeliveryInterface
     */
    public function getFirstDelivery(): DeliveryInterface
    {
        return $this->getDelivery(key($this->deliveries));
    }

    /**
     * @param int $id
     *
     * @return DeliveryInterface
     */
    public function getDelivery(int $id): DeliveryInterface
    {
        if (!isset($this->deliveries[$id])) {
            throw new \RuntimeException(sprintf('Delivery with id %d is unavailable', $id));
        }

        return $this->deliveries[$id];
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function hasDelivery(int $id): bool
    {
        return isset($this->deliveries[$id]);
    }

    /**
     * @return ArrayIterator|DeliveryInterface[]
     */
    public function getIterator()
    {
        return new ArrayIterator($this->deliveries);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->deliveries);
    }
}
