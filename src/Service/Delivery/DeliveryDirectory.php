<?php

namespace MommyCom\Service\Delivery;

class DeliveryDirectory
{
    public const DUMMY = 1;
    public const FEEDR = 2;

    public const CLASSES = [
        self::DUMMY => DummyDelivery::class,
        self::FEEDR => FeedrDelivery::class,
    ];
}
