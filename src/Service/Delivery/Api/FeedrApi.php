<?php

namespace MommyCom\Service\Delivery\Api;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use MommyCom\Entity\OrderTracking;
use MommyCom\Service\Delivery\DeliveryApiInterface;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\Service\Order\OrderInterface;
use Psr\Log\LoggerInterface;

class FeedrApi implements DeliveryApiInterface
{
    private const DELIVERY_STATUS_MAP = [
        1 => OrderTracking::STATUS_INIT,
        2 => OrderTracking::STATUS_PROCESSING,
        3 => OrderTracking::STATUS_PROCESSING,
        4 => OrderTracking::STATUS_SHOP_WAREHOUSE,
        5 => OrderTracking::STATUS_TRANSITION,
        6 => OrderTracking::STATUS_TRANSITION,
        7 => OrderTracking::STATUS_TRANSITION,
        8 => OrderTracking::STATUS_CLIENT_WAREHOUSE,
        9 => OrderTracking::STATUS_DELIVERED,
        10 => OrderTracking::STATUS_FORWARDING,
        11 => OrderTracking::STATUS_FORWARDING,
        12 => OrderTracking::STATUS_CLIENT_REFUSAL,
        13 => OrderTracking::STATUS_RETURNING,
        14 => OrderTracking::STATUS_UNKNOWN,
        15 => OrderTracking::STATUS_UNKNOWN,
        99 => OrderTracking::STATUS_LOST,
    ];

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $labelUrl;

    /**
     * @var int
     */
    private $warehouseAreaId;

    /**
     * @var string
     */
    private $warehouseAddress;

    /**
     * @var string
     */
    private $trackUrl;

    /**
     * FeedrApi constructor.
     *
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param string $apiKey
     * @param string $labelUrl
     * @param int $warehouseAreaId
     * @param string $warehouseAddress
     * @param string $trackUrl
     */
    public function __construct(
        ClientInterface $client,
        LoggerInterface $logger,
        string $apiKey,
        string $labelUrl,
        int $warehouseAreaId,
        string $warehouseAddress,
        string $trackUrl
    ) {
        $this->client = $client;
        $this->logger = $logger;
        $this->apiKey = $apiKey;
        $this->labelUrl = $labelUrl;
        $this->warehouseAreaId = $warehouseAreaId;
        $this->warehouseAddress = $warehouseAddress;
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return array
     * @throws ApiException
     */
    public function getProvinces(): array
    {
        $provinces = $this->call('GET', 'public/v1/provinces');

        if (!isset($provinces['rows']) || !is_array($provinces['rows'])) {
            throw $this->makeError('Provinces data is invalid.', 'GET', 'public/v1/provinces', [], $provinces);
        }

        return $provinces['rows'];
    }

    /**
     * @param int|null $provinceId
     *
     * @return array
     * @throws ApiException
     */
    public function getCities(int $provinceId = null): array
    {
        $params = (null === $provinceId) ? ['origin' => 'all'] : ['province' => $provinceId];
        $cities = $this->call('GET', 'public/v1/cities', $params);

        if (!isset($cities['rows']) || !is_array($cities['rows'])) {
            throw $this->makeError('Cities data is invalid.', 'GET', 'public/v1/cities', $params, $cities);
        }

        return $cities['rows'];
    }

    /**
     * @param int $cityId
     *
     * @return array
     * @throws ApiException
     */
    public function getSuburbs(int $cityId): array
    {
        $params = ['city' => $cityId];
        $suburbs = $this->call('GET', 'public/v1/suburbs', $params);

        if (!isset($suburbs['rows']) || !is_array($suburbs['rows'])) {
            throw $this->makeError('Suburbs data is invalid.', 'GET', 'public/v1/suburbs', $params, $suburbs);
        }

        return $suburbs['rows'];
    }

    /**
     * @param int $suburbId
     *
     * @return array
     * @throws ApiException
     */
    public function getAreas(int $suburbId): array
    {
        $params = ['suburb' => $suburbId];
        $areas = $this->call('GET', 'public/v1/areas', $params);

        if (!isset($areas['rows']) || !is_array($areas['rows'])) {
            throw $this->makeError('Areas data is invalid.', 'GET', 'public/v1/areas', $params, $areas);
        }

        return $areas['rows'];
    }

    /**
     * @param int $destinationAreaId
     * @param float $weight kilograms
     * @param int $length centimeters
     * @param int $width centimeters
     * @param int $height centimeters
     * @param int $value IDR
     * @param bool $cashOnDelivery
     *
     * @return array
     * @throws ApiException
     */
    public function getDomesticRates(
        int $destinationAreaId,
        float $weight,
        int $length,
        int $width,
        int $height,
        int $value,
        bool $cashOnDelivery
    ): array {
        $params = [
            'o' => $this->warehouseAreaId,
            'd' => $destinationAreaId,
            'wt' => $weight,
            'l' => $length,
            'w' => $width,
            'h' => $height,
            'v' => $value,
            'cod' => (int)$cashOnDelivery,
            'order' => 1,
        ];
        $rates = $this->call('GET', 'public/v1/domesticRates', $params);

        if (isset($rates['statusCode']) && 404 === (int)$rates['statusCode']) {
            return [];
        }

        if (!isset($rates['rates']['logistic']) || !is_array($rates['rates']['logistic'])) {
            throw $this->makeError('Domestic rates data is invalid.', 'GET', 'public/v1/domesticRates', $params, $rates);
        }

        return $rates['rates']['logistic'];
    }

    /**
     * @param int $destinationAreaId
     * @param string $destinationAddress
     * @param float $weight
     * @param int $length
     * @param int $width
     * @param int $height
     * @param int $value
     * @param bool $cashOnDelivery
     * @param int $rateId
     * @param bool $isExpress
     * @param string $itemName
     * @param string $consigneeName
     * @param string $consigneePhoneNumber
     * @param int $orderId
     * @param string $orderDescription
     *
     * @return string
     * @throws ApiException
     */
    public function createDomesticOrder(
        int $destinationAreaId,
        string $destinationAddress,
        float $weight,
        int $length,
        int $width,
        int $height,
        int $value,
        bool $cashOnDelivery,
        int $rateId,
        bool $isExpress,
        string $itemName,
        string $consigneeName,
        string $consigneePhoneNumber,
        int $orderId,
        string $orderDescription
    ): string {
        $params = [
            'o' => $this->warehouseAreaId,
            'originAddress' => $this->warehouseAddress,
            'd' => $destinationAreaId,
            'destinationAddress' => $destinationAddress,
            'wt' => $weight,
            'l' => $length,
            'w' => $width,
            'h' => $height,
            'v' => $value,
            'cod' => (int)$cashOnDelivery,
            'rateID' => $rateId,
            'logistic' => $isExpress ? 2 : 1,
            'packageType' => 2,
            'itemName' => $itemName,
            'consigneeName' => $consigneeName,
            'consigneePhoneNumber' => $consigneePhoneNumber,
            'externalID' => $orderId,
            'contents' => $orderDescription,
        ];
        $result = $this->call('POST', 'public/v1/orders/domestics', $params);

        if (empty($result['id'])) {
            throw $this->makeError('Order was not created.', 'POST', 'public/v1/orders/domestics', $params, $result);
        }

        return $result['id'];
    }

    /**
     * @param string $orderId
     *
     * @return array
     * @throws ApiException
     */
    public function getOrderDetails(string $orderId): array
    {
        $details = $this->call('GET', 'public/v1/orders/' . $orderId);

        if (empty($details['order']['detail']) || !is_array($details['order']['detail'])) {
            throw $this->makeError('Cannot get order details', 'GET', 'public/v1/orders/' . $orderId, [], $details);
        }

        return $details['order']['detail'];
    }

    /**
     * @param OrderInterface $order
     *
     * @return string
     * @throws ApiException
     */
    public function createOrder(OrderInterface $order): string
    {
        /** @var Feedr $deliveryModel */
        $deliveryModel = $order->getDeliveryModel();

        $price = $order->getInvoice()->getAmount();
        $price = max($price, 0.0);
        $cashOnDelivery = ($price > 0.0);

        $price = $order->getCost()->getAmount();

        if ($cashOnDelivery) {
            $price = $price / 1.04;
        }

        return $this->createDomesticOrder(
            $deliveryModel->areaId,
            $deliveryModel->address,
            $order->getWeight() / 1000,
            30,
            20,
            10,
            round($price),
            $cashOnDelivery,
            $deliveryModel->rateId,
            false,
            'Package',
            $order->getCustomerName(),
            $order->getPhoneNumber(),
            $order->getId(),
            $order->getOrderDescription()
        );
    }

    /**
     * @param OrderInterface $order
     *
     * @return string
     * @throws ApiException
     */
    public function getTrackingCode(OrderInterface $order): string
    {
        $details = $this->getOrderDetails($order->getTrackId());

        if (empty($details['id'])) {
            throw $this->makeError('Cannot get track id');
        }

        return (string)$details['id'];
    }

    /**
     * @param OrderInterface $order
     *
     * @return string
     * @throws ApiException
     */
    public function getLabelUrl(OrderInterface $order): string
    {
        $details = $this->getOrderDetails($order->getTrackId());

        if (empty($details['id'])) {
            throw $this->makeError('Cannot get track id');
        }

        if (empty($details['labelChecksum'])) {
            throw $this->makeError('Cannot get label checksum');
        }

        $trackId = $details['id'];
        $labelChecksum = $details['labelChecksum'];

        $labelUrl = $this->labelUrl;
        $labelUrl = str_replace('{oid}', $trackId, $labelUrl);
        $labelUrl = str_replace('{uid}', $labelChecksum, $labelUrl);

        return $labelUrl;
    }

    /**
     * @param string $trackingCode
     *
     * @return string
     */
    public function getTrackUrl(string $trackingCode): string
    {
        return $this->trackUrl . $trackingCode;
    }

    /**
     * @param OrderInterface $order
     *
     * @return int
     * @throws ApiException
     */
    public function getDeliveryPrice(OrderInterface $order): int
    {
        $details = $this->getOrderDetails($order->getTrackId());

        if (!isset($details['courier']['rate']['value'])) {
            throw $this->makeError('Cannot get delivery price value');
        }

        if (!isset($details['courier']['rate']['UoM'])) {
            throw $this->makeError('Cannot get delivery price currency');
        }

        if ('IDR' != strtoupper($details['courier']['rate']['UoM'])) {
            throw $this->makeError('Invalid delivery price currency');
        }

        return (int)round($details['courier']['rate']['value']);
    }

    /**
     * @param OrderInterface $order
     *
     * @return \DateTimeImmutable
     * @throws ApiException
     */
    public function getDeadlineForDelivery(OrderInterface $order): \DateTimeImmutable
    {
        $details = $this->getOrderDetails($order->getTrackId());

        if (empty($details['creationDate'])) {
            throw $this->makeError('Cannot get delivery creation date');
        }
        if (empty($details['courier']['max_day'])) {
            throw $this->makeError('Cannot get delivery interval');
        }

        try {
            $created = new \DateTimeImmutable($details['creationDate']);
            $created = $created->setTimezone((new \DateTimeImmutable)->getTimezone());

            return $created->add(new \DateInterval(sprintf('P%dD', $details['courier']['max_day'])));
        } catch (\Exception $e) {
            throw $this->makeError(sprintf(
                'Invalid delivery interval with date %s and interval %s',
                $details['creationDate'],
                $details['courier']['max_day']
            ));
        }
    }

    /**
     * @param OrderInterface $order
     *
     * @return int
     * @throws ApiException
     */
    public function getDeliveryStatus(OrderInterface $order): int
    {
        $details = $this->getOrderDetails($order->getTrackId());

        if (empty($details['shipmentStatus']['statusCode'])) {
            throw  $this->makeError('Cannot get status code');
        }
        if (empty($details['shipmentStatus']['updateDate'])) {
            throw  $this->makeError('Cannot get status date');
        }

        return self::DELIVERY_STATUS_MAP[$details['shipmentStatus']['statusCode']] ?? OrderTracking::STATUS_UNKNOWN;
    }

    /**
     * @param OrderInterface $order
     *
     * @throws ApiException
     */
    public function confirmOrder(OrderInterface $order): void
    {
        $this->call('PUT', 'public/v1/activations/' . $order->getTrackId(), [
            'active' => 1,
        ]);
    }

    /**
     * @param string $trackingCode
     *
     * @return null|string
     */
    public function findDeliveryId(string $trackingCode): ?string
    {
        try {
            $details = $this->getOrderDetails($trackingCode);

            if (empty($details['creationDate'])) {
                throw $this->makeError('Cannot get delivery creation date');
            }

            if (empty($details['_id'])) {
                throw $this->makeError('Cannot get delivery ID');
            }

            return $details['_id'];

        } catch (ApiException $e) {
            return null;
        }
    }

    /**
     * @param int $provinceId
     *
     * @return string
     * @throws ApiException
     */
    public function getProvinceName(int $provinceId): string
    {
        $provinces = $this->getProvinces();

        foreach ($provinces as $province) {
            if ((int)$province['id'] === $provinceId) {
                return $province['name'];
            }
        }

        throw $this->makeError("Province #{$provinceId} was not found.");
    }

    /**
     * @param int $provinceId
     * @param int $cityId
     *
     * @return string
     * @throws ApiException
     */
    public function getCityName(int $provinceId, int $cityId): string
    {
        $cities = $this->getCities($provinceId);

        foreach ($cities as $city) {
            if ((int)$city['id'] === $cityId) {
                return $city['name'];
            }
        }

        throw $this->makeError("City #{$cityId} in province #{$provinceId} was not found.");
    }

    /**
     * @param int $cityId
     * @param int $suburbId
     *
     * @return string
     * @throws ApiException
     */
    public function getSuburbName(int $cityId, int $suburbId): string
    {
        $suburbs = $this->getSuburbs($cityId);

        foreach ($suburbs as $suburb) {
            if ((int)$suburb['id'] === $suburbId) {
                return $suburb['name'];
            }
        }

        throw $this->makeError("Suburb #{$suburbId} in city #{$cityId} was not found.");
    }

    /**
     * @param int $suburbId
     * @param int $areaId
     *
     * @return string
     * @throws ApiException
     */
    public function getAreaName(int $suburbId, int $areaId): string
    {
        $areas = $this->getAreas($suburbId);

        foreach ($areas as $area) {
            if ((int)$area['id'] === $areaId) {
                return $area['name'];
            }
        }

        throw $this->makeError("Area #{$areaId} in suburb #{$suburbId} was not found.");
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $params
     *
     * @return array
     * @throws ApiException
     */
    private function call(string $method, string $uri, array $params = []): array
    {
        $method = strtoupper($method);

        if ('GET' === $method) {
            $params = array_replace(['apiKey' => $this->apiKey], $params);
            $uri .= '?' . http_build_query($params);
            $params = [];
        } else {
            $uri .= '?apiKey=' . $this->apiKey;
            $params = ['form_params' => $params];
        }

        try {
            $this->logger->info(sprintf('Sending %s request to Feedr: %s.', $method, $uri), [
                'method' => $method,
                'uri' => $uri,
                'params' => $params,
            ]);
            $response = $this->client->request($method, $uri, $params);
            $response = json_decode($response->getBody(), true);
            $this->logger->debug(sprintf('Received response from Feedr API'), ['response' => $response]);
        } catch (GuzzleException $e) {
            $this->logger->error(sprintf('Feedr API problem: %s.', $e->getMessage()), ['exception' => $e]);

            throw new ApiException(
                sprintf('Communication error: %s.', $e->getMessage()), $method, $uri, $params, null, $e
            );
        }

        if (!isset($response['status']) || !isset($response['data'])) {
            throw $this->makeError('Invalid response data', $method, $uri, $params, $response);
        }

        return $response['data'];
    }

    /**
     * @param string $message
     * @param string|null $method
     * @param string|null $uri
     * @param array|null $params
     * @param array|null $response
     *
     * @return ApiException
     */
    private function makeError(
        string $message,
        string $method = null,
        string $uri = null,
        array $params = null,
        array $response = null
    ): ApiException {
        $this->logger->error('Feedr API problem: ' . $message, [
            'method' => $method,
            'uri' => $uri,
            'params' => $params,
            'response' => $response,
        ]);

        return new ApiException($message, $method, $uri, $params, $response);
    }
}
