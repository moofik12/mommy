<?php

namespace MommyCom\Service\Delivery\Api;

use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class FeedrCachedApi extends FeedrApi
{
    private const CACHE_EXPIRE = 3600; // час
    private const CACHE_EXPIRE_SHORT = 300; // 5 минут

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * FeedrCachedApi constructor.
     *
     * @param CacheInterface $cache
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param string $apiKey
     * @param string $labelUrl
     * @param int $warehouseAreaId
     * @param string $warehouseAddress
     * @param string $trackUrl
     */
    public function __construct(
        CacheInterface $cache,
        ClientInterface $client,
        LoggerInterface $logger,
        string $apiKey,
        string $labelUrl,
        int $warehouseAreaId,
        string $warehouseAddress,
        string $trackUrl
    ) {
        parent::__construct($client, $logger, $apiKey, $labelUrl, $warehouseAreaId, $warehouseAddress, $trackUrl);

        $this->cache = $cache;
    }

    /**
     * @return array
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getProvinces(): array
    {
        $key = 'feedr.provinces';
        $provinces = $this->cache->get($key, false);

        if (false === $provinces) {
            $provinces = parent::getProvinces();
            $this->cache->set($key, $provinces, self::CACHE_EXPIRE);
        }

        return $provinces;
    }

    /**
     * @param int|null $provinceId
     *
     * @return array
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCities(int $provinceId = null): array
    {
        $key = 'feedr.cities.' . ((null === $provinceId) ? 'all' : $provinceId);
        $cities = $this->cache->get($key, false);

        if (false === $cities) {
            $cities = parent::getCities($provinceId);
            $this->cache->set($key, $cities, self::CACHE_EXPIRE);
        }

        return $cities;
    }

    /**
     * @param int $cityId
     *
     * @return array
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getSuburbs(int $cityId): array
    {
        $key = 'feedr.suburbs.' . $cityId;
        $suburbs = $this->cache->get($key, false);

        if (false === $suburbs) {
            $suburbs = parent::getSuburbs($cityId);
            $this->cache->set($key, $suburbs, self::CACHE_EXPIRE);
        }

        return $suburbs;
    }

    /**
     * @param int $suburbId
     *
     * @return array
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getAreas(int $suburbId): array
    {
        $key = 'feedr.areas.' . $suburbId;
        $areas = $this->cache->get($key, false);

        if (false === $areas) {
            $areas = parent::getAreas($suburbId);
            $this->cache->set($key, $areas, self::CACHE_EXPIRE);
        }

        return $areas;
    }

    /**
     * @param int $destinationAreaId
     * @param float $weight
     * @param int $length
     * @param int $width
     * @param int $height
     * @param int $value
     * @param bool $cashOnDelivery
     *
     * @return array
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getDomesticRates(
        int $destinationAreaId,
        float $weight,
        int $length,
        int $width,
        int $height,
        int $value,
        bool $cashOnDelivery
    ): array {
        $key = 'feedr.domesticRates.' . implode('.', [
                $destinationAreaId,
                $weight,
                $length,
                $width,
                $height,
                $value,
                (int)$cashOnDelivery,
            ]);
        $domesticRates = $this->cache->get($key, false);

        if (false === $domesticRates) {
            $domesticRates = parent::getDomesticRates(
                $destinationAreaId, $weight, $length, $width, $height, $value, $cashOnDelivery
            );
            $this->cache->set($key, $domesticRates, self::CACHE_EXPIRE_SHORT);
        }

        return $domesticRates;
    }

    /**
     * @param string $orderId
     *
     * @return array
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getOrderDetails(string $orderId): array
    {
        $key = 'feedr.orderDetails.' . $orderId;

        $details = $this->cache->get($key, false);

        if (false === $details) {
            $details = parent::getOrderDetails($orderId);
            $this->cache->set($key, $details, self::CACHE_EXPIRE_SHORT);
        }

        return $details;
    }
}
