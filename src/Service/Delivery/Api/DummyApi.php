<?php

namespace MommyCom\Service\Delivery\Api;

use MommyCom\Service\Delivery\DeliveryApiInterface;
use MommyCom\Service\Order\OrderInterface;

class DummyApi implements DeliveryApiInterface
{
    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function createOrder(OrderInterface $order): string
    {
        return (string)time();
    }

    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function getTrackingCode(OrderInterface $order): string
    {
        return (string)$order->getTrackId();
    }

    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function getLabelUrl(OrderInterface $order): string
    {
        return 'ya.ru';
    }

    /**
     * @param OrderInterface $order
     *
     * @return int
     */
    public function getDeliveryPrice(OrderInterface $order): int
    {
        return 0;
    }

    /**
     * @param OrderInterface $order
     *
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function getDeadlineForDelivery(OrderInterface $order): \DateTimeImmutable
    {
        return new \DateTimeImmutable('tomorrow');
    }

    /**
     * @param OrderInterface $order
     *
     * @return int
     */
    public function getDeliveryStatus(OrderInterface $order): int
    {
        return 0;
    }

    /**
     * @param OrderInterface $order
     */
    public function confirmOrder(OrderInterface $order): void
    {
        // it is dummy
    }

    /**
     * @param string $trackingCode
     *
     * @return null|string
     */
    public function findDeliveryId(string $trackingCode): ?string
    {
        return $trackingCode;
    }

    /**
     * @param string $trackingCode
     *
     * @return string
     */
    public function getTrackUrl(string $trackingCode): string
    {
        return 'http://example.com/' . $trackingCode;
    }
}
