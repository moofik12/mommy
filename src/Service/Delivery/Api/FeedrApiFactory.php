<?php

namespace MommyCom\Service\Delivery\Api;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class FeedrApiFactory
{
    private const DEFAULT_TRACK_URL = 'https://shipper.id/track/';

    private const BASE_TEST_URL = 'https://api.shipper.id/sandbox/';
    private const BASE_PROD_URL = 'https://api.shipper.id/prod/';

    private const LABEL_TEST_URL = 'https://shipper.id/label-dev/sticker.php?oid={oid}&uid={uid}';
    private const LABEL_PROD_URL = 'https://shipper.id/label/sticker.php?oid={oid}&uid={uid}';

    // Jl. Empang Bahagia 6 Blok B2 No 611B, Jelambar,  Grogol Petamburan, Jakarta Barat 11460.
    private const WAREHOUSE_AREA_ID = 4802;
    private const WAREHOUSE_ADDRESS = 'Jl. Empang Bahagia 6 Blok B2 No 611B, Barat 11460';

    /**
     * @param CacheInterface $cache
     * @param LoggerInterface $logger
     * @param string $apiKey
     * @param bool $test
     *
     * @return FeedrApi
     */
    public static function createFeedrApi(
        CacheInterface $cache,
        LoggerInterface $logger,
        string $apiKey,
        bool $test
    ): FeedrApi {
        $userAgent = 'Mozilla/5.0 (Linux x86_64) GuzzleHttp/' . Client::VERSION;

        $client = new Client([
            'base_uri' => $test ? static::BASE_TEST_URL : static::BASE_PROD_URL,
            'headers' => ['User-Agent' => $userAgent],
        ]);

        $labelUrl = $test ? static::LABEL_TEST_URL : static::LABEL_PROD_URL;

        return new FeedrCachedApi(
            $cache,
            $client,
            $logger,
            $apiKey,
            $labelUrl,
            self::WAREHOUSE_AREA_ID,
            self::WAREHOUSE_ADDRESS,
            self::DEFAULT_TRACK_URL
        );
    }
}
