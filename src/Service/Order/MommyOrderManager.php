<?php

namespace MommyCom\Service\Order;

use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Entity\EventProduct;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderProduct;
use MommyCom\Repository\EventProductRepository;
use MommyCom\Repository\OrderProductRepository;
use MommyCom\Repository\OrderRepository;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Money\MoneyFactory;

class MommyOrderManager
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderProductRepository
     */
    private $orderProductRepository;

    /**
     * @var EventProductRepository
     */
    private $eventProductRepository;

    /**
     * @var MoneyFactory
     */
    private $moneyFactory;

    /**
     * @var AvailableDeliveries
     */
    private $availableDeliveries;

    /**
     * @var MommyOrder[]
     */
    private $orders = [];

    /**
     * MommyOrderManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param MoneyFactory $moneyFactory
     * @param AvailableDeliveries $availableDeliveries
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        MoneyFactory $moneyFactory,
        AvailableDeliveries $availableDeliveries
    ) {
        $this->orderRepository = $entityManager->getRepository(Order::class);
        $this->orderProductRepository = $entityManager->getRepository(OrderProduct::class);
        $this->eventProductRepository = $entityManager->getRepository(EventProduct::class);
        $this->moneyFactory = $moneyFactory;
        $this->availableDeliveries = $availableDeliveries;
    }

    /**
     * @param int $id
     *
     * @return MommyOrder|null
     */
    public function find(int $id): ?MommyOrder
    {
        if (array_key_exists($id, $this->orders)) {
            return $this->orders[$id];
        }

        /** @var Order|null $order */
        $order = $this->orderRepository->find($id);

        if (null === $order) {
            $this->orders[$id] = null;
        } else {
            $this->orders[$id] = new MommyOrder(
                $order,
                $this->orderProductRepository,
                $this->eventProductRepository,
                $this->moneyFactory,
                $this->availableDeliveries
            );
        }

        return $this->orders[$id];
    }
}
