<?php

namespace MommyCom\Service\Order;

use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use Money\Money;

interface OrderInterface
{
    /**
     * Возвращает внутренний идентификатор заказа
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Возвращает идентификатор заказа в системе доставки
     *
     * @return string
     */
    public function getTrackId(): string;

    /**
     * Возвращает полную стоимость заказа
     *
     * @return Money
     */
    public function getCost(): Money;

    /**
     * Возвращает сколько осталось заплатить за заказ
     *
     * @return Money
     */
    public function getInvoice(): Money;

    /**
     * Возвращает вес в граммах
     *
     * @return int
     */
    public function getWeight(): int;

    /**
     * Возвращает информацию о доставке
     *
     * @return DeliveryModel
     */
    public function getDeliveryModel(): DeliveryModel;

    /**
     * Возвращает полное имя покупателя
     *
     * @return string
     */
    public function getCustomerName(): string;

    /**
     * Возвращает номер телефона покупателя
     *
     * @return string
     */
    public function getPhoneNumber(): string;

    /**
     * Возвращает описание заказа
     *
     * @return string
     */
    public function getOrderDescription(): string;
}
