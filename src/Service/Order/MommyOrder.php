<?php

namespace MommyCom\Service\Order;

use MommyCom\Entity\EventProduct;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderProduct;
use MommyCom\Repository\EventProductRepository;
use MommyCom\Repository\OrderProductRepository;
use MommyCom\Service\Delivery\Api\ApiException;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Money\MoneyFactory;
use Money\Money;

class MommyOrder implements OrderInterface
{
    public const STATUS_FULFILMENT_INITIAL = 0;
    public const STATUS_FULFILMENT_CREATED = 1;
    public const STATUS_FULFILMENT_READY = 2;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderProductRepository
     */
    private $orderProductRepository;

    /**
     * @var EventProductRepository
     */
    private $eventProductRepository;

    /**
     * @var MoneyFactory
     */
    private $moneyFactory;

    /**
     * @var AvailableDeliveries
     */
    private $availableDeliveries;

    /**
     * @var OrderProduct[]|null
     */
    private $actualOrderProducts = null;

    /**
     * @var int[]
     */
    private $eventProductWeights = [];

    public function __construct(
        Order $order,
        OrderProductRepository $orderProductRepository,
        EventProductRepository $eventProductRepository,
        MoneyFactory $moneyFactory,
        AvailableDeliveries $availableDeliveries
    ) {
        $this->order = $order;
        $this->orderProductRepository = $orderProductRepository;
        $this->eventProductRepository = $eventProductRepository;
        $this->moneyFactory = $moneyFactory;
        $this->availableDeliveries = $availableDeliveries;
    }

    public function getId(): int
    {
        return (int)$this->order->getId();
    }

    public function getTrackId(): string
    {
        return (string)$this->order->getTrackId();
    }

    public function getCost(): Money
    {
        $cost = 0.0;

        foreach ($this->getActualOrderPositions() as $product) {
            $cost += $product->getPriceTotal();
        }

        $cost -= $this->order->getBonuses();
        $cost -= $this->order->getDiscount();
        $cost = max(0.0, $cost);

        return $this->moneyFactory->makeMoney($cost);
    }

    public function getInvoice(): Money
    {
        $cost = 0.0;

        foreach ($this->getActualOrderPositions() as $product) {
            $cost += $product->getPriceTotal();
        }

        $cost -= $this->order->getBonuses();
        $cost -= $this->order->getDiscount();
        $cost -= $this->order->getCardPayed();
        $cost = max(0.0, $cost);

        return $this->moneyFactory->makeMoney($cost);
    }

    public function getWeight(): int
    {
        if ($this->order->getWeightCustom()) {
            return $this->order->getWeightCustom();
        }

        $weight = 0;

        foreach ($this->getActualOrderPositions() as $product) {
            $weight += $this->getEventProductWeight($product->getProductId()) * $product->getNumber();
        }

        return $weight;
    }

    public function getDeliveryModel(): DeliveryModel
    {
        $deliveryType = $this->order->getDeliveryType();
        $delivery = $this->availableDeliveries->getDelivery($deliveryType);

        $deliveryModel = $delivery->createFormModel();

        $deliveryAttributesJson = $this->order->getDeliveryAttributesJson();
        $deliveryModel->setAttributes(json_decode($deliveryAttributesJson, true, 4));

        return $deliveryModel;
    }

    public function getCustomerName(): string
    {
        return trim($this->order->getClientName() . ' ' . $this->order->getClientSurname());
    }

    public function getPhoneNumber(): string
    {
        return (string)$this->order->getTelephone();
    }

    public function getOrderDescription(): string
    {
        return (string)$this->order->getOrderComment();
    }

    /**
     * @return int
     */
    public function getFulfilmentStatus(): int
    {
        $deliveryApi = $this->availableDeliveries->getDelivery($this->order->getDeliveryType())->getApi();

        $isFulfilmentOrderGenerated = true;
        try {
            $deliveryApi->getLabelUrl($this);
        } catch (ApiException $exception) {
            $isFulfilmentOrderGenerated = false;
        }

        $isFulfilmentOrderCreated = !empty($this->getTrackId());

        if (!$isFulfilmentOrderCreated) {
            return self::STATUS_FULFILMENT_INITIAL;
        } elseif ($isFulfilmentOrderGenerated) {
            return self::STATUS_FULFILMENT_READY;
        } else {
            return self::STATUS_FULFILMENT_CREATED;
        }
    }

    /**
     * @return OrderProduct[]
     */
    private function getActualOrderPositions(): array
    {
        if (null !== $this->actualOrderProducts) {
            return $this->actualOrderProducts;
        }

        /** @var OrderProduct[] $products */
        $products = $this->orderProductRepository->findBy(['orderId' => $this->order->getId()]);
        $this->actualOrderProducts = [];

        foreach ($products as $product) {
            if (OrderProduct::CALLCENTER_STATUS_CANCELLED === $product->getCallcenterStatus()) {
                continue;
            }

            $this->actualOrderProducts[] = $product;
        }

        return $this->actualOrderProducts;
    }

    private function getEventProductWeight($id): int
    {
        if (!isset($this->eventProductWeights[$id])) {
            /** @var EventProduct $product */
            $product = $this->eventProductRepository->find($id);

            $this->eventProductWeights[$id] = $product ? (int)$product->getWeight() : 0;
        }

        return $this->eventProductWeights[$id];
    }
}
