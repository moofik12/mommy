<?php

namespace MommyCom\Service;

class CurrentTime
{
    /**
     * @var \DateTimeImmutable|null
     */
    private static $dateTime = null;

    /**
     * @return \DateTimeImmutable
     */
    public static function getDateTimeImmutable(): \DateTimeImmutable
    {
        if (null === self::$dateTime) {
            $dateTime = new \DateTimeImmutable();

            if (!empty($_SERVER['REQUEST_TIME'])) {
                $dateTime = $dateTime->setTimestamp($_SERVER['REQUEST_TIME']);
            }

            self::$dateTime = $dateTime;
        }

        return self::$dateTime;
    }

    /**
     * @return \DateTime
     */
    public static function getDateTimeMutable(): \DateTime
    {
        $dateTimeImmutable = self::getDateTimeImmutable();
        $dateTime = new \DateTime(null, $dateTimeImmutable->getTimezone());
        $dateTime->setTimestamp($dateTimeImmutable->getTimestamp());

        return $dateTime;
    }

    /**
     * @return int
     */
    public static function getUnixTimestamp(): int
    {
        return self::getDateTimeImmutable()->getTimestamp();
    }
}
