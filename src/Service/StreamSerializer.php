<?php

namespace MommyCom\Service;

use Psr\Http\Message\StreamInterface;

class StreamSerializer implements \IteratorAggregate
{
    /**
     * @var StreamInterface
     */
    private $stream;

    /**
     * StreamSerializer constructor.
     *
     * @param StreamInterface $stream
     */
    public function __construct(StreamInterface $stream)
    {
        $this->stream = $stream;
    }

    /**
     * @param object $object
     *
     * @return int сколько байт записано в поток
     */
    public function append($object): int
    {
        $serialized = serialize($object);

        return $this->stream->write(pack('V', strlen($serialized)) . $serialized);
    }

    public function readNext()
    {
        if ($this->stream->eof()) {
            return null;
        }

        $data = $this->stream->read(4);
        if (!$data) {
            return null;
        }

        $length = current(unpack('V', $data));

        return unserialize($this->stream->read($length));
    }

    public function rewind()
    {
        $this->stream->rewind();
    }

    public function getIterator(): \Generator
    {
        while ($object = $this->readNext()) {
            yield $object;
        }
    }
}
