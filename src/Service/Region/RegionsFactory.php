<?php

namespace MommyCom\Service\Region;

use MommyCom\Service\Region\Exception\UnknownRegionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RegionsFactory
{
    /**
     * @param RequestStack $requestStack
     * @param string[][] $regions
     * @param string $serverRegion
     *
     * @return Regions
     */
    public static function createRegions(RequestStack $requestStack, array $regions, string $serverRegion): Regions
    {
        $request = $requestStack->getCurrentRequest() ?: new Request();
        $host = $request->headers->get('Host', '');

        $regions = self::getRegionsByRequest($regions, $host);
        $serverRegion = self::createServerRegion($serverRegion);
        $userRegion = RegionDetector::detectRegion($request);

        return new Regions($regions, $serverRegion, $userRegion);
    }

    /**
     * @param RequestStack $requestStack
     * @param string[][] $regions
     *
     * @return Regions
     */
    public static function createDevRegions(RequestStack $requestStack, array $regions): Regions
    {
        $request = $requestStack->getCurrentRequest() ?: new Request();
        $host = $request->headers->get('Host', '');

        $regions = self::getRegionsByRequest($regions, $host);
        $serverRegion = self::detectServerRegion($regions, $host);
        $userRegion = RegionDetector::detectRegion($request);

        return new Regions($regions, $serverRegion, $userRegion);
    }

    private static function getRegionsByRequest(array $regions, string $host): array
    {
        $requestType = ('m' === strstr($host, '.', true)) ? 'mobile' : 'desktop';

        if (empty($regions[$requestType]) || !is_array($regions[$requestType])) {
            throw new \LogicException('Regions is not configured.');
        }

        return $regions[$requestType];
    }

    private static function createServerRegion(string $serverRegion): Region
    {
        if ($serverRegion && !Region::regionIsValid($serverRegion)) {
            throw UnknownRegionException::regionIsUnknown($serverRegion);
        }

        return new Region($serverRegion, Region::DETECT_LEVEL_SERVER, false);
    }

    private static function detectServerRegion(array $regions, string $host): Region
    {
        $regions = array_flip($regions);
        $serverRegion = $regions[$host] ?? '';

        if (!$serverRegion || !Region::regionIsValid($serverRegion)) {
            $serverRegion = Region::DEFAULT;
        }

        return new Region($serverRegion, Region::DETECT_LEVEL_SERVER, false);
    }
}
