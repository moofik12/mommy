<?php

namespace MommyCom\Service\Region;

use Symfony\Component\HttpFoundation\Request;

class RegionDetector
{
    /**
     * @param Request $request
     *
     * @return Region
     */
    public static function detectRegion(Request $request): Region
    {
        $isBot = (
            false !== strpos(strtolower($request->headers->get('User-Agent')), 'bot') ||
            false !== strpos(strtolower($request->headers->get('Referer')), '//webvisor.com')
        );

        $region = $request->cookies->get(self::getCookieName(), '');

        if (Region::regionIsValid($region)) {
            return new Region($region, Region::DETECT_LEVEL_CONFIRMED, $isBot);
        }

        $region = self::findRegion($request->getLanguages());

        if (Region::regionIsValid($region)) {
            return new Region($region, Region::DETECT_LEVEL_UNCONFIRMED, $isBot);
        }

        return new Region(Region::DEFAULT, Region::DETECT_LEVEL_UNKNOWN, $isBot);
    }

    public static function getCookieName()
    {
        return getenv('REGION_COOKIE_NAME');
    }

    /**
     * @param string[] $codes
     *
     * @return string
     */
    private static function findRegion(array $codes): string
    {
        foreach ($codes as $code) {
            $region = Region::findRegion(self::normalizeCode($code));

            if ('' !== $region) {
                return $region;
            }
        }

        return '';
    }

    /**
     * @param string $code
     *
     * @return string
     */
    private static function normalizeCode(string $code): string
    {
        $code = ltrim($code, '-_');
        $code = substr($code, 0, 3);
        $code = rtrim($code, '-_');

        return $code;
    }
}

