<?php

namespace MommyCom\Service\Region\Exception;

class UnknownDetectLevelException extends \InvalidArgumentException
{
    public static function detectLevelIsUnknown($level)
    {
        return new static(sprintf('Detect level "%s" is unknown', $level));
    }
}
