<?php

namespace MommyCom\Service\Region\Exception;

class UnknownRegionException extends \InvalidArgumentException
{
    public static function regionIsUnknown($region)
    {
        return new static(sprintf('Region "%s" is unknown.', $region));
    }
}
