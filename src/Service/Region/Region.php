<?php

namespace MommyCom\Service\Region;

use MommyCom\Service\Money\CurrencyDirectory;
use MommyCom\Service\Region\Exception\UnknownDetectLevelException;
use MommyCom\Service\Region\Exception\UnknownRegionException;

class Region
{
    public const GLOBAL = 'Global';
    public const INDONESIA = 'Indonesia';
    public const VIETNAM = 'Vietnam';
    public const ROMANIA = 'Romania';
    public const COLOMBIA = 'Colombia';

    public const DEFAULT = self::GLOBAL;

    public const COUNTRY_CODES = [
        self::GLOBAL => [],
        self::INDONESIA => ['id', 'ind'],
        self::VIETNAM => ['vn', 'vi', 'vie'],
        self::ROMANIA => ['ro', 'ron', 'rum'],
        self::COLOMBIA => ['co'],
    ];

    public const CURRENCY_CODES = [
        self::GLOBAL => CurrencyDirectory::CODE_GLOBAL,
        self::INDONESIA => CurrencyDirectory::CODE_INDONESIA,
        self::VIETNAM => CurrencyDirectory::CODE_VIETNAM,
        self::ROMANIA => CurrencyDirectory::CODE_ROMANIA,
        self::COLOMBIA => CurrencyDirectory::CODE_COLOMBIA,
    ];

    public const LANGUAGE_CODES = [
        self::GLOBAL => 'en',
        self::INDONESIA => 'in',
        self::VIETNAM => 'vi',
        self::ROMANIA => 'ro',
        self::COLOMBIA => 'co',
    ];

    public const DETECT_LEVEL_UNKNOWN = 0;
    public const DETECT_LEVEL_UNCONFIRMED = 1;
    public const DETECT_LEVEL_CONFIRMED = 2;
    public const DETECT_LEVEL_SERVER = 3;

    /**
     * @var string
     */
    private $regionName;

    /**
     * @var int
     */
    private $detectLevel;

    /**
     * @var bool
     */
    private $detectBot;

    /**
     * Region constructor.
     *
     * @param string $region
     * @param int $detectLevel
     * @param bool $detectBot
     */
    public function __construct(string $region, int $detectLevel, bool $detectBot)
    {
        if (!static::regionIsValid($region)) {
            throw UnknownRegionException::regionIsUnknown($region);
        }

        if ($detectLevel < 0 || $detectLevel > static::DETECT_LEVEL_SERVER) {
            throw UnknownDetectLevelException::detectLevelIsUnknown($detectLevel);
        }

        $this->regionName = $region;
        $this->detectLevel = $detectLevel;
        $this->detectBot = $detectBot;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        if (!isset(self::CURRENCY_CODES[$this->regionName])) {
            throw new \LogicException(sprintf('Region %s has no currency code.', $this->regionName));
        }

        return self::CURRENCY_CODES[$this->regionName];
    }

    /**
     * @return string
     */
    public function getRegionName(): string
    {
        return $this->regionName;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return self::LANGUAGE_CODES[$this->getRegionName()];
    }

    /**
     * @return int
     */
    public function getDetectLevel(): int
    {
        return $this->detectLevel;
    }

    /**
     * @return bool
     */
    public function isDetectBot(): bool
    {
        return $this->detectBot;
    }

    /**
     * @param string $region
     *
     * @return bool
     */
    public static function regionIsValid(string $region): bool
    {
        return isset(static::COUNTRY_CODES[$region]);
    }

    /**
     * @param string $code
     *
     * @return string
     */
    public static function findRegion(string $code): string
    {
        foreach (static::COUNTRY_CODES as $region => $codes) {
            if (in_array($code, $codes, true)) {
                return $region;
            }
        }

        return '';
    }
}
