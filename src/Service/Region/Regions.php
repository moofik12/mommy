<?php

namespace MommyCom\Service\Region;

use MommyCom\Service\Region\Exception\UnknownRegionException;

class Regions
{
    /**
     * @var string[]
     */
    private $regions;

    /**
     * @var Region
     */
    private $serverRegion;

    /**
     * @var Region
     */
    private $userRegion;

    /**
     * Regions constructor.
     *
     * @param array $regions
     * @param Region $serverRegion
     * @param Region $userRegion
     */
    public function __construct(array $regions, Region $serverRegion, Region $userRegion)
    {
        $this->setRegions($regions);

        $this->serverRegion = $serverRegion;
        $this->userRegion = $userRegion;
    }

    /**
     * @param array $regions
     */
    private function setRegions(array $regions)
    {
        foreach ($regions as $region => $host) {
            if (!Region::regionIsValid($region)) {
                throw UnknownRegionException::regionIsUnknown($region);
            }

            if (!is_string($host)) {
                throw new \LogicException('Region hostname must be a string.');
            }
        }

        $this->regions = $regions;
    }

    /**
     * @return string[]
     */
    public function getRegions(): array
    {
        return $this->regions;
    }

    /**
     * @param string $region
     *
     * @return string
     */
    public function getHostname(string $region): string
    {
        if (!isset($this->regions[$region])) {
            throw new \RuntimeException(sprintf('Hostname of region "%s" is not configured.', $region));
        }

        return $this->regions[$region];
    }

    /**
     * @return Region
     */
    public function getUserRegion(): Region
    {
        return $this->userRegion;
    }

    /**
     * @return Region
     */
    public function getServerRegion(): Region
    {
        return $this->serverRegion;
    }
}
