<?php

namespace MommyCom\Service;

use CHtml;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use Psr\SimpleCache\CacheInterface;
use Yii;

class SplitTesting
{
    const SPLITTEST_PREFIX = 'splittest-';

    const SPLITTEST_NAME_MADE_IN = 'made-in';
    const SPLITTEST_NAME_REG_FIELDS = 'reg-fields';
    const SPLITTEST_NAME_DB = 'db';

    /**
     * @var Regions
     */
    private $regions;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var bool новое значение на основе предыдущих данных, более достовреный результат
     */
    private $useBalancer;

    /**
     * SplitTesting constructor.
     *
     * @param Regions $regions
     * @param CacheInterface $cache
     * @param bool $useBalancer
     */
    public function __construct(Regions $regions, CacheInterface $cache, bool $useBalancer)
    {
        $this->regions = $regions;
        $this->cache = $cache;
        $this->useBalancer = $useBalancer;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function isSplitTestEnabled(string $name): bool
    {
        $regionName = $this->regions->getServerRegion()->getRegionName();

        switch ($name) {
            case self::SPLITTEST_NAME_MADE_IN:
                return !in_array($regionName, [
                    Region::INDONESIA,
                    Region::GLOBAL,
                    Region::VIETNAM,
                    Region::ROMANIA,
                    Region::COLOMBIA,
                ], true);
            case self::SPLITTEST_NAME_DB:
                return !in_array($regionName, [
                    Region::GLOBAL,
                    Region::INDONESIA,
                    Region::VIETNAM,
                    Region::ROMANIA,
                    Region::COLOMBIA,
                ], true);
            case self::SPLITTEST_NAME_REG_FIELDS:
                return false;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getDbVariant(): int
    {
        return $this->getNum(self::SPLITTEST_NAME_DB, count(Yii::app()->params['dbVariants']));
    }

    /**
     * @return int
     */
    public function getMadeInVariant(): int
    {
        return $this->getNum(self::SPLITTEST_NAME_MADE_IN, count($this->getMadeInCountries()));
    }

    /**
     * @return int
     */
    public function getRegistrationFieldsVariant(): int
    {
        return $this->getNum(self::SPLITTEST_NAME_REG_FIELDS, count(Yii::app()->params['registrationFieldsVariants']));
    }

    /**
     * @param string $name Название сплит-теста
     * @param integer $variationNum количество вариаций в сплит-тесте
     *
     * @return integer
     */
    public function getNum($name, $variationNum)
    {
        $cookieName = self::SPLITTEST_PREFIX . CHtml::getIdByName($name);
        $startNumber = 0;
        $endNumber = $variationNum - 1;

        if (isset($_COOKIE[$cookieName])) {
            $value = intval($_COOKIE[$cookieName]);
        } else {
            $cache = $this->cache;
            $endTimeTest = time() + 30 * 86400;
            $value = mt_rand($startNumber, $endNumber);

            if ($this->useBalancer && $cache) {
                $cacheValue = $cache->get($cookieName, false);

                if ($cacheValue === false) {
                    $cache->set($cookieName, $value, $endTimeTest);
                } else {
                    $newValue = ++$cacheValue;

                    if ($newValue < $startNumber || $newValue > $endNumber) {
                        $newValue = $startNumber;
                    }

                    //на случай заглушки
                    if ($cache->set($cookieName, $newValue)) {
                        $value = $newValue;
                    }
                }
            }

            if ($this->isSplitTestEnabled($name)) {
                setCookie($cookieName, $value, $endTimeTest, '/');
                $_COOKIE[$cookieName] = $value;
            }
        }

        return max(min($value, $variationNum), $startNumber);
    }

    /**
     * @return array
     */
    public function getAvailable()
    {
        $prefix = self::SPLITTEST_PREFIX;
        $result = [];

        foreach ($_COOKIE as $name => $value) {
            if (substr($name, 0, strlen($prefix)) !== $prefix) {
                continue;
            }

            if (!$this->isSplitTestEnabled(str_replace(self::SPLITTEST_PREFIX, '', $name))) {
                continue;
            }

            $result[$name] = $value;
        }

        return $result;
    }

    /**
     * @return string[]
     */
    public function getMadeInCountries()
    {
        return Yii::app()->params['madeInCountries'][$this->regions->getServerRegion()->getRegionName()];
    }
}
