<?php

namespace MommyCom\Service\RegionalValidator;

use MommyCom\Service\RegionalValidator\Filter\FilterInterface;

class FilterRunner implements SanitizerInterface
{
    /**
     * @var FilterInterface[]
     */
    private $filters;

    /**
     * RegionalValidatorService constructor.
     *
     * @param FilterInterface[] $filters
     */
    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function sanitize(string $value): string
    {
        foreach ($this->filters as $filter) {
            $value = $filter->filter($value);
        }

        return $value;
    }
}
