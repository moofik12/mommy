<?php

namespace MommyCom\Service\RegionalValidator\Filter;

class SpecialCharFilter implements FilterInterface
{
    public const REMOVE_ALL_CHARACTERS = 0;
    public const REMOVE_FIRST_CHARACTER = 1;

    private const MODES = [
        self::REMOVE_ALL_CHARACTERS => true,
        self::REMOVE_FIRST_CHARACTER => true,
    ];

    private const SPECIAL_CHARACTERS = [
        "+", "/", "[", "]", "#", "@", "$", "%",
        "^", "&", "*", "(", ")", "=", "-", "'",
        ";", "\\", ".", "{", "}", "|", "\"", ":",
        "<", ">", "?", "~", ",", "!", '_',
    ];

    /**
     * @var int $removeMode
     */
    protected $removeMode;

    /**
     * @var array|null
     */
    private static $specialCharacters = null;

    /**
     * SpecialCharFilter constructor.
     *
     * @param int $removeMode
     */
    public function __construct(int $removeMode)
    {
        if (!isset(self::MODES[$removeMode])) {
            throw new \InvalidArgumentException('Unknown filter mode.');
        }

        $this->removeMode = $removeMode;
    }

    /**
     * @param string $param
     *
     * @return string
     */
    public function filter(string $param): string
    {
        if (self::REMOVE_FIRST_CHARACTER === $this->removeMode) {
            $firstCharacter = substr($param, 0, 1);

            if (in_array($firstCharacter, self::SPECIAL_CHARACTERS, true)) {
                return substr($param, 1);
            }

            return $param;
        }

        return str_replace(self::SPECIAL_CHARACTERS, '', $param);
    }
}
