<?php

namespace MommyCom\Service\RegionalValidator\Filter;


interface FilterInterface
{
    /**
     * @param string $param
     *
     * @return string
     */
    public function filter(string $param): string;
}