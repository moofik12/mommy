<?php

namespace MommyCom\Service\RegionalValidator\Filter;

class CharFilter implements FilterInterface
{
    /**
     * @var string[]
     */
    private $chars;

    /**
     * CharFilter constructor.
     *
     * @param array $chars
     */
    public function __construct(array $chars)
    {
        $this->chars = $chars;
    }

    /**
     * @param string $param
     *
     * @return string
     */
    public function filter(string $param): string
    {
        return str_replace($this->chars, '', $param);
    }
}
