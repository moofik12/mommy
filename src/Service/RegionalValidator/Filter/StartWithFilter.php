<?php

namespace MommyCom\Service\RegionalValidator\Filter;

class StartWithFilter implements FilterInterface
{
    /**
     * @var string[]
     */
    private $charSets;

    /**
     * FirstCharFilter constructor.
     *
     * @param array $charSets
     */
    public function __construct(array $charSets)
    {
        $this->charSets = $charSets;
    }

    /**
     * @param string $param
     *
     * @return string
     */
    public function filter(string $param): string
    {
        foreach ($this->charSets as $charSet) {
            if (0 === strpos($param, $charSet)) {
                return substr($param, strlen($charSet));
            }
        }

        return $param;
    }
}
