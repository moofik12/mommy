<?php

namespace MommyCom\Service\RegionalValidator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface as Validator;

class ValidatorRunner implements ValidatorInterface
{
    /**
     * @var Constraint[]
     */
    private $constraints;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * RegionalValidatorService constructor.
     *
     * @param Constraint[] $constraints
     * @param Validator $validator
     */
    public function __construct(array $constraints, Validator $validator)
    {
        $this->constraints = $constraints;
        $this->validator = $validator;
    }

    /**
     * @param string $value
     *
     * @return ConstraintViolationListInterface|ConstraintViolationInterface[]
     */
    public function validate(string $value): ConstraintViolationListInterface
    {
        $violations = new ConstraintViolationList();

        foreach ($this->constraints as $constraint) {
            $violations->addAll($this->validator->validate($value, $constraint));

            if (0 !== $violations->count()) {
                break;
            }
        }

        return $violations;
    }
}
