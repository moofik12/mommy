<?php

namespace MommyCom\Service\RegionalValidator;

use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ValidatorInterface
{
    /**
     * @param string $value
     *
     * @return ConstraintViolationListInterface
     */
    public function validate(string $value): ConstraintViolationListInterface;
}
