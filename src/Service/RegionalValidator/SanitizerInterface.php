<?php

namespace MommyCom\Service\RegionalValidator;

interface SanitizerInterface
{
    /**
     * @param string $value
     *
     * @return string
     */
    public function sanitize(string $value): string;
}
