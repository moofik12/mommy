<?php

namespace MommyCom\Service\PaymentGateway;

class AvailablePaymentGateways
{
    /**
     * @var PaymentGatewayInterface[]
     */
    private $paymentGateways;

    /**
     * AvailablePaymentGateways constructor.
     *
     * @param PaymentGatewayInterface[] $paymentGateways
     */
    public function __construct(array $paymentGateways)
    {
        $this->paymentGateways = [];

        foreach ($paymentGateways as $gateway) {
            $this->addGateway($gateway);
        }
    }

    /**
     * @param PaymentGatewayInterface $gateway
     */
    private function addGateway(PaymentGatewayInterface $gateway): void
    {
        $this->paymentGateways[$gateway->getId()] = $gateway;
    }

    /**
     * @return PaymentGatewayInterface[]
     */
    public function getPaymentGateways(): array
    {
        return $this->paymentGateways;
    }

    /**
     * @param string $id
     *
     * @return PaymentGatewayInterface
     */
    public function getPaymentGateway(string $id): PaymentGatewayInterface
    {
        return $this->paymentGateways[$id];
    }

    /**
     * @return \Generator|PaymentMethodInterface[]
     */
    public function getPaymentMethods(): \Generator
    {
        foreach ($this->paymentGateways as $paymentGateway) {
            yield from $paymentGateway->getPaymentMethods();
        }
    }
}
