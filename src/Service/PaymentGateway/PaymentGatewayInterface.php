<?php

namespace MommyCom\Service\PaymentGateway;

use MommyCom\Service\BaseController\Controller;
use Psr\Http\Server\RequestHandlerInterface;

interface PaymentGatewayInterface extends RequestHandlerInterface
{
    /**
     * Возвращает уникальный идентификатор платёжного шлюза
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Возвращает список доступных способов оплаты для платёжного шлюза
     *
     * @return iterable|PaymentMethodInterface[]
     */
    public function getPaymentMethods(): iterable;

    /**
     * Рендерит часть страницы для корректной работы данного платежного шлюза
     *
     * @param \MommyCom\Service\BaseController\Controller $controller
     * @param string $uid
     *
     * @return string
     */
    public function render(Controller $controller, string $uid): string;
}
