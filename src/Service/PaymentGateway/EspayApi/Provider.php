<?php

namespace MommyCom\Service\PaymentGateway\EspayApi;

use MommyCom\Service\PaymentGateway\PaymentMethodInterface;

class Provider implements PaymentMethodInterface
{
    /**
     * @var string
     */
    public $bankCode;

    /**
     * @var string
     */
    public $productCode;

    /**
     * @var string
     */
    public $productName;

    /**
     * Provider constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        foreach ($attributes as $name => $value) {
            if (!property_exists($this, $name)) {
                throw new \RuntimeException(sprintf('Invalid property name "%"', $name));
            }

            $this->{$name} = $value;
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->bankCode . '_' . $this->productCode;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->productName;
    }
}
