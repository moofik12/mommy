<?php

namespace MommyCom\Service\PaymentGateway\EspayApi;

use GuzzleHttp\ClientInterface;
use Psr\SimpleCache\CacheInterface;

class EspayApi
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var string
     */
    private $baseKitUrl;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Provider[]|null
     */
    private $providers = null;

    /**
     * EspayApi constructor.
     *
     * @param CacheInterface $cache
     * @param ClientInterface $client
     * @param string $baseKitUrl
     * @param string $apiKey
     */
    public function __construct(CacheInterface $cache, ClientInterface $client, string $baseKitUrl, string $apiKey)
    {
        $this->cache = $cache;
        $this->client = $client;
        $this->baseKitUrl = $baseKitUrl;
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return Provider[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getProviders(): array
    {
        if (null !== $this->providers) {
            return $this->providers;
        }

        $key = 'espay.providers';

        $data = $this->cache->get($key);

        if (!is_array($data)) {
            $response = $this->client->request('GET', 'merchant/merchantinfo?key=' . $this->apiKey);

            $body = json_decode($response->getBody(), true);

            $errorCode = $body['error_code'] ?? null;
            $data = $body['data'] ?? null;

            if ('0000' !== $errorCode || !is_array($data) || !$data) {
                throw new \RuntimeException('Invalid response');
            }

            $this->cache->set($key, $data);
        }

        $this->providers = [];

        foreach ($data as $item) {
            $this->providers[] = new Provider($item);
        }

        return $this->providers;
    }

    /**
     * @return string
     */
    public function getJsUrl(): string
    {
        return rtrim($this->baseKitUrl, '/') . '/public/signature/js';
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function getImageUrl(string $name): string
    {
        return rtrim($this->baseKitUrl, '/') . '/images/products/' . $name . '.png';
    }
}
