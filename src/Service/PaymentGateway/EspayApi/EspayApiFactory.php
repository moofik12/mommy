<?php

namespace MommyCom\Service\PaymentGateway\EspayApi;

use GuzzleHttp\Client;
use Psr\SimpleCache\CacheInterface;

class EspayApiFactory
{
    /**
     * @param CacheInterface $cache
     * @param string $baseKitUrl
     * @param string $baseApiUrl
     * @param string $apiKey
     *
     * @return EspayApi
     */
    public static function createApi(
        CacheInterface $cache,
        string $baseKitUrl,
        string $baseApiUrl,
        string $apiKey
    ): EspayApi {
        $client = new Client([
            'base_uri' => $baseApiUrl,
        ]);

        return new EspayApi($cache, $client, $baseKitUrl, $apiKey);
    }
}
