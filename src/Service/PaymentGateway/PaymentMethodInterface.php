<?php

namespace MommyCom\Service\PaymentGateway;

interface PaymentMethodInterface
{
    /**
     * Возвращает уникальный идентификатор способа оплаты
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Возвращает отображаемое имя способа оплаты
     *
     * @return string
     */
    public function getName(): string;
}
