<?php

namespace MommyCom\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Service\StreamSerializer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SaveRequestToStreamMiddleware implements MiddlewareInterface
{
    /**
     * @var StreamSerializer
     */
    private $streamSerializer;

    /**
     * SaveRequestToStreamMiddleware constructor.
     *
     * @param StreamSerializer $streamSerializer
     */
    public function __construct(StreamSerializer $streamSerializer)
    {
        $this->streamSerializer = $streamSerializer;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->streamSerializer->append($request);

        return $handler->handle($request);
    }
}
