<?php

namespace MommyCom\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Service\PaymentGateway\Model\AbstractEspayModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class EspayValidateRequestMiddleware extends AbstractEspayMiddleware
{
    /**
     * @var AbstractEspayModel
     */
    private $espayModelPrototype;

    /**
     * @var string
     */
    private $errorContents;

    /**
     * EspayValidateRequestMiddleware constructor.
     *
     * @param AbstractEspayModel $espayModelPrototype
     * @param string $errorContents
     */
    public function __construct(AbstractEspayModel $espayModelPrototype, string $errorContents)
    {
        $this->espayModelPrototype = $espayModelPrototype;
        $this->errorContents = $errorContents;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $espayModel = clone $this->espayModelPrototype;
        $espayModel->setAttributes($request->getParsedBody());

        if (!$espayModel->validate()) {
            return $this->response($this->errorContents);
        }

        $request = $request->withAttribute(static::ATTRIBUTE_MODEL, $espayModel);

        return $handler->handle($request);
    }
}

