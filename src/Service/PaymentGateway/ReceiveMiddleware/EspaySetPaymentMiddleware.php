<?php

namespace MommyCom\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PayGatewayLogRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\PaymentGateway\Model\EspayPaymentModel;
use MommyCom\Service\PaymentGateway\PaymentGatewayDirectory;
use Money\Currency;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\UuidFactory;

final class EspaySetPaymentMiddleware extends AbstractEspayMiddleware
{
    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var OrderRecord
     */
    private $orderRecordPrototype;

    /**
     * @var PayGatewayLogRecord
     */
    private $payGatewayLogRecordPrototype;

    /**
     * @var PayGatewayRecord
     */
    private $payGatewayRecordPrototype;

    /**
     * @var UuidFactory
     */
    private $uuidFactory;

    /**
     * EspayInquireMiddleware constructor.
     *
     * @param Currency $currency
     * @param OrderRecord $orderRecordPrototype
     * @param PayGatewayLogRecord $payGatewayLogRecordPrototype
     * @param PayGatewayRecord $payGatewayRecordPrototype
     * @param UuidFactory $uuidFactory
     */
    public function __construct(
        Currency $currency,
        OrderRecord $orderRecordPrototype,
        PayGatewayLogRecord $payGatewayLogRecordPrototype,
        PayGatewayRecord $payGatewayRecordPrototype,
        UuidFactory $uuidFactory
    ) {
        $this->currency = $currency;
        $this->orderRecordPrototype = $orderRecordPrototype;
        $this->payGatewayLogRecordPrototype = $payGatewayLogRecordPrototype;
        $this->payGatewayRecordPrototype = $payGatewayRecordPrototype;
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $espayModel = $this->extractEspayModel($request);
        $orders = $this->orderRecordPrototype->findByUid($espayModel->order_id);

        if (!$orders) {
            return $this->response('1,Invalid Order Id,,,');
        }

        $amount = 0.0;
        foreach ($orders as $order) {
            $amount += $order->getPayPrice(false) + $order->getDeliveryPrice() - $order->card_payed;
        }

        if (abs((float)$espayModel->amount - $amount) > 0.01) {
            return $this->response('2,Invalid amount,,,');
        }

        if (strtoupper($espayModel->ccy) !== $this->currency->getCode()) {
            return $this->response('3,Invalid currency,,,');
        }

        try {
            $this->savePayments($request, ...$orders);
        } catch (\Exception $e) {
            return $this->response('4,' . $this->quote($e->getMessage()) . ',,,');
        }

        $dateTime = CurrentTime::getDateTimeImmutable()->format('Y-m-d H:i:s');

        $message = sprintf(
            '0,Success,%s,%s,%s',
            crc32($espayModel->order_id . $dateTime),
            $this->quote($espayModel->order_id),
            $dateTime
        );

        return $this->response($message);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return EspayPaymentModel
     */
    private function extractEspayModel(ServerRequestInterface $request): EspayPaymentModel
    {
        return $request->getAttribute(static::ATTRIBUTE_MODEL);
    }

    /**
     * @param ServerRequestInterface $request
     * @param OrderRecord ...$orders
     *
     * @throws \Exception
     */
    private function savePayments(ServerRequestInterface $request, OrderRecord ...$orders): void
    {
        $logs = $pays = [];

        foreach ($orders as $order) {
            $attributes = [
                'invoice_id' => $order->id,
                'unique_payment' => $this->uuidFactory->uuid4(),
                'provider' => PaymentGatewayDirectory::ESPAY,
                'amount' => $order->getPayPrice(false) + $order->getDeliveryPrice() - $order->card_payed,
                'message' => '',
                'status' => 0,
                'user_id' => 0,
                'user_ip' => $request->getServerParams()['REMOTE_ADDR'] ?? '',
                'user_browser' => $request->getServerParams()['HTTP_USER_AGENT'] ?? '',
            ];

            $payGatewayLog = clone $this->payGatewayLogRecordPrototype;
            $payGatewayLog->setAttributes($attributes);

            $payGateway = clone $this->payGatewayRecordPrototype;
            $payGateway->setAttributes($attributes);

            $logs[] = $payGatewayLog;
            $pays[] = $payGateway;
        }

        $this->saveModels(...$logs, ...$pays);
    }

    /**
     * @param \CActiveRecord ...$models
     *
     * @throws \Exception
     */
    private function saveModels(\CActiveRecord ...$models): void
    {
        foreach ($models as $model) {
            if (!$model->validate()) {
                throw new \Exception('Invalid attributes');
            }
        }

        foreach ($models as $model) {
            if (!$model->save(false)) {
                throw new \Exception('Unknown error');
            }
        }
    }
}
