<?php

namespace MommyCom\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Service\PsrHttpMessage\ResponsePrototypeAwareInterface;
use MommyCom\Service\PsrHttpMessage\ResponsePrototypeAwareTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use function GuzzleHttp\Psr7\stream_for;

abstract class AbstractEspayMiddleware implements MiddlewareInterface, ResponsePrototypeAwareInterface
{
    use ResponsePrototypeAwareTrait;

    protected const ATTRIBUTE_MODEL = 'espayModel';

    /**
     * @param string $content
     *
     * @return ResponseInterface
     */
    protected function response(string $content): ResponseInterface
    {
        return $this
            ->getResponse()
            ->withHeader('Content-Type', 'text/plain')
            ->withBody(stream_for($content));
    }

    /**
     * @param string $filed
     *
     * @return string
     */
    protected function quote(string $filed): string
    {
        if (false !== strpos($filed, '"') || false !== strpos($filed, ';')) {
            $filed = str_replace('"', '""', $filed);
            $filed = '"' . $filed . '"';
        }

        return $filed;
    }
}
