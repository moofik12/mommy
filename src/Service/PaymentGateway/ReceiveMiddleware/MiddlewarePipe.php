<?php

namespace MommyCom\Service\PaymentGateway\ReceiveMiddleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SplQueue;

class MiddlewarePipe implements RequestHandlerInterface
{
    /**
     * @var SplQueue
     */
    private $pipeline;

    /**
     * Pipe constructor.
     */
    public function __construct()
    {
        $this->pipeline = new SplQueue();
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($this->pipeline->isEmpty()) {
            throw new \RuntimeException('Empty middleware pipe');
        }

        /** @var MiddlewareInterface $middleware */
        $middleware = $this->pipeline->dequeue();

        return $middleware->process($request, $this);
    }

    /**
     * @param MiddlewareInterface $middleware
     *
     * @return $this
     */
    public function pipe(MiddlewareInterface $middleware): self
    {
        $this->pipeline->enqueue($middleware);

        return $this;
    }
}
