<?php

namespace MommyCom\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\PaymentGateway\Model\EspayInquireModel;
use Money\Currency;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class EspayInquireMiddleware extends AbstractEspayMiddleware
{
    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var OrderRecord
     */
    private $orderRecordPrototype;

    /**
     * EspayInquireMiddleware constructor.
     *
     * @param Currency $currency
     * @param OrderRecord $orderRecordPrototype
     */
    public function __construct(Currency $currency, OrderRecord $orderRecordPrototype)
    {
        $this->currency = $currency;
        $this->orderRecordPrototype = $orderRecordPrototype;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $espayModel = $this->getEspayModel($request);
        $orders = $this->orderRecordPrototype->findByUid($espayModel->order_id);

        if (!$orders) {
            return $this->response('1;Invalid Order Id;;;;;');
        }

        $amount = 0;
        foreach ($orders as $order) {
            $amount += $order->getPayPrice(false) + $order->getDeliveryPrice() - $order->card_payed;
        }

        $message = sprintf(
            '0;Success;%s;%.2F;%s;Pay for %d order(s);%s',
            $this->quote($espayModel->order_id),
            $amount,
            $this->currency->getCode(),
            count($orders),
            CurrentTime::getDateTimeImmutable()->format('d/m/Y H:i:s')
        );

        return $this->response($message);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return EspayInquireModel
     */
    private function getEspayModel(ServerRequestInterface $request): EspayInquireModel
    {
        return $request->getAttribute(static::ATTRIBUTE_MODEL);
    }
}
