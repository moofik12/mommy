<?php

namespace MommyCom\Service\PaymentGateway\Model;

use CFormModel;

abstract class AbstractEspayModel extends CFormModel
{
    /**
     * @var string
     */
    public $rq_datetime;

    /**
     * @var
     */
    public $order_id;

    /**
     * @var string
     */
    public $comm_code;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $signature;

    /**
     * @var string
     */
    private $signatureKey;

    /**
     * AbstractEspayModel constructor.
     *
     * @param $signatureKey
     */
    public function __construct($signatureKey)
    {
        $this->signatureKey = $signatureKey;

        parent::__construct();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['rq_datetime, order_id, comm_code, signature', 'required'],
            ['signature', 'validateSignature'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateSignature($attribute): void
    {
        if ($this->generateSignature() === $this->signature) {
            return;
        }

        $this->addError($attribute, 'Invalid signature');
    }

    /**
     * @return string
     */
    protected function generateSignature(): string
    {
        $string = implode('##', [
            '',
            $this->signatureKey,
            $this->rq_datetime,
            $this->order_id,
            $this->getMode(),
            '',
        ]);

        return hash('sha256', strtoupper($string));
    }

    /**
     * @return string
     */
    abstract protected function getMode(): string;
}
