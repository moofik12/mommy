<?php

namespace MommyCom\Service\PaymentGateway\Model;

class EspayPaymentModel extends AbstractEspayModel
{
    public $rq_uuid;
    public $member_id;
    public $ccy;
    public $amount;
    public $debit_from;
    public $debit_from_name;
    public $credit_to;
    public $credit_to_name;
    public $message;
    public $payment_datetime;
    public $payment_ref;
    public $debit_from_bank;
    public $credit_to_bank;
    public $approval_code_full_bca;
    public $approval_code_installment_bca;

    public function rules()
    {
        $rules = [
            ['rq_uuid, ccy, amount, payment_datetime, payment_ref, debit_from_bank, credit_to_bank', 'required'],
            ['ccy', 'length', 'is' => 3],
            ['amount', 'numerical'],
        ];

        return array_merge($rules, parent::rules());
    }

    /**
     * @return string
     */
    protected function getMode(): string
    {
        return 'PAYMENTREPORT';
    }
}
