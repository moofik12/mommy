<?php

namespace MommyCom\Service\PaymentGateway\Model;

class EspayInquireModel extends AbstractEspayModel
{
    public $rq_uuid;
    public $member_id;

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            ['rq_uuid', 'required'],
        ];

        return array_merge($rules, parent::rules());
    }

    /**
     * @return string
     */
    protected function getMode(): string
    {
        return 'INQUIRY';
    }
}
