<?php

namespace MommyCom\Service\PaymentGateway;

use GuzzleHttp\Psr7\Response;
use MommyCom\Service\BaseController\Controller;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DummyPaymentGateway implements PaymentGatewayInterface
{
    /**
     * @return string
     */
    public function getId(): string
    {
        return PaymentGatewayDirectory::DUMMY;
    }

    /**
     * @return array
     */
    public function getPaymentMethods(): array
    {
        return [];
    }

    /**
     * @param Controller $controller
     * @param string $uid
     *
     * @return string
     */
    public function render(Controller $controller, string $uid): string
    {
        return '';
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new Response(200, ['Content-Type' => 'text/plain'], 'It is dummy!');
    }
}
