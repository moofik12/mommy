<?php

namespace MommyCom\Service\PaymentGateway;

use GuzzleHttp\Exception\GuzzleException;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PayGatewayLogRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\PaymentGateway\EspayApi\EspayApi;
use MommyCom\Service\PaymentGateway\EspayApi\Provider;
use MommyCom\Service\PaymentGateway\Model\EspayInquireModel;
use MommyCom\Service\PaymentGateway\Model\EspayPaymentModel;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\EspayInquireMiddleware;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\EspaySetPaymentMiddleware;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\EspayValidateRequestMiddleware;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\MiddlewarePipe;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\SaveRequestToStreamMiddleware;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\SaveResponseToStreamMiddleware;
use MommyCom\Service\StreamSerializer;
use MommyCom\YiiComponent\TokenManager;
use Money\Currency;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Ramsey\Uuid\UuidFactory;
use function GuzzleHttp\Psr7\stream_for;

class EspayPaymentGateway implements PaymentGatewayInterface
{
    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var UuidFactory
     */
    private $uuidFactory;

    /**
     * @var TokenManager
     */
    private $tokenManager;

    /**
     * @var EspayApi
     */
    private $espayApi;

    /**
     * @var string
     */
    private $signatureKey;

    /**
     * @var string
     */
    private $saveRequestsPath;

    /**
     * EspayPaymentGateway constructor.
     *
     * @param Currency $currency
     * @param UuidFactory $uuidFactory
     * @param TokenManager $tokenManager
     * @param EspayApi $espayApi
     * @param string $signatureKey
     * @param string $saveRequestsPath
     */
    public function __construct(
        Currency $currency,
        UuidFactory $uuidFactory,
        TokenManager $tokenManager,
        EspayApi $espayApi,
        string $signatureKey,
        string $saveRequestsPath
    ) {
        $this->currency = $currency;
        $this->uuidFactory = $uuidFactory;
        $this->tokenManager = $tokenManager;
        $this->espayApi = $espayApi;
        $this->signatureKey = $signatureKey;
        $this->saveRequestsPath = $saveRequestsPath;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return PaymentGatewayDirectory::ESPAY;
    }

    /**
     * @return Provider[]
     * @throws InvalidArgumentException
     */
    public function getPaymentMethods(): array
    {
        try {
            return $providers = $this->espayApi->getProviders();
        } catch (GuzzleException $e) {
            return [];
        }
    }

    /**
     * @param Controller $controller
     * @param string $uid
     *
     * @return string
     * @throws InvalidArgumentException
     * @throws \CException
     */
    public function render(Controller $controller, string $uid): string
    {
        try {
            $providers = $this->espayApi->getProviders();
        } catch (GuzzleException $e) {
            return '';
        }

        $backUrl = $controller->createAbsoluteUrl('pay/result', [
            'uid' => $uid,
            'token' => $this->tokenManager->getToken('resultAction'),
        ]);

        return $controller->renderPartial('/pay/_espayPaymentGateway', [
            'uid' => $uid,
            'backUrl' => $backUrl,
            'providers' => $providers,
            'espayApi' => $this->espayApi,
        ], true);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $type = $request->getQueryParams()['type'] ?? '';

        switch (strtolower($type)) {
            case 'inquiry':
                return $this->getInquiryPipe()->handle($request);
            case 'payment':
                return $this->getPaymentPipe()->handle($request);
            default:
                throw new \RuntimeException(sprintf('Unknown handler type "%s"', $type));
        }
    }

    /**
     * @return MiddlewarePipe
     */
    private function getInquiryPipe(): MiddlewarePipe
    {
        return (new MiddlewarePipe())
            ->pipe(new SaveRequestToStreamMiddleware($this->getLogStreamSerializer('espay_inquiry.log')))
            ->pipe(new SaveResponseToStreamMiddleware($this->getLogStreamSerializer('espay_inquiry_response.log')))
            ->pipe(new EspayValidateRequestMiddleware(
                new EspayInquireModel($this->signatureKey),
                '1;Invalid request;;;;;'
            ))
            ->pipe(new EspayInquireMiddleware(
                $this->currency,
                OrderRecord::model()
            ));
    }

    /**
     * @return MiddlewarePipe
     */
    private function getPaymentPipe(): MiddlewarePipe
    {
        return (new MiddlewarePipe())
            ->pipe(new SaveRequestToStreamMiddleware($this->getLogStreamSerializer('espay_payment.log')))
            ->pipe(new SaveResponseToStreamMiddleware($this->getLogStreamSerializer('espay_payment_response.log')))
            ->pipe(new EspayValidateRequestMiddleware(
                new EspayPaymentModel($this->signatureKey),
                '1,Invalid request,,,'
            ))
            ->pipe(new EspaySetPaymentMiddleware(
                $this->currency,
                OrderRecord::model(),
                PayGatewayLogRecord::model(),
                PayGatewayRecord::model(),
                $this->uuidFactory
            ));
    }

    /**
     * @param string $fileName
     *
     * @return StreamSerializer
     */
    private function getLogStreamSerializer(string $fileName): StreamSerializer
    {
        $runtimePath = rtrim($this->saveRequestsPath, DIRECTORY_SEPARATOR);
        $fileName = ltrim($fileName, DIRECTORY_SEPARATOR);

        $stream = stream_for(fopen($runtimePath . DIRECTORY_SEPARATOR . $fileName, 'a'));

        return new StreamSerializer($stream);
    }
}
