<?php

namespace MommyCom\Service\Money;

use MommyCom\Service\Region\Regions;
use Money\Currency;
use Psr\Container\ContainerInterface;

class DefaultCurrencyFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return Currency
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function getDefaultCurrency(ContainerInterface $container): Currency
    {
        $currencyCode = self::getDefaultCurrencyCode($container);

        return new Currency($currencyCode);
    }

    /**
     * @param ContainerInterface $container
     *
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function getDefaultCurrencyCode(ContainerInterface $container): string
    {
        /** @var Regions $regions */
        $regions = $container->get(Regions::class);

        return $regions->getServerRegion()->getCurrencyCode();
    }
}
