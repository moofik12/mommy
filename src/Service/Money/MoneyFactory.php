<?php

namespace MommyCom\Service\Money;

use Money\Currencies;
use Money\Currency;
use Money\Money;

class MoneyFactory
{
    /**
     * @var Currencies
     */
    private $currencies;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var int|null
     */
    private $valueMultiplier = null;

    /**
     * @var int[]
     */
    private $currencyMultipliers = [
        'IDR' => 0,
    ];

    /**
     * MoneyFactory constructor.
     *
     * @param Currencies $currencies
     * @param Currency $currency
     */
    public function __construct(Currencies $currencies, Currency $currency)
    {
        $this->currencies = $currencies;
        $this->currency = $currency;
    }

    /**
     * @param float $amount
     *
     * @return Money
     */
    public function makeMoney(float $amount): Money
    {
        $amount = $amount * $this->getValueMultiplier();
        $amount = (int)round($amount);

        return new Money($amount, $this->currency);
    }

    /**
     * @return int
     */
    private function getValueMultiplier(): int
    {
        $code = $this->currency->getCode();

        if (key_exists($code, $this->currencyMultipliers)) {
            $this->valueMultiplier = $this->currencyMultipliers[$code];
        }

        if (null === $this->valueMultiplier) {
            $this->valueMultiplier = 10 ** $this->currencies->subunitFor($this->currency);
        }

        return $this->valueMultiplier;
    }
}
