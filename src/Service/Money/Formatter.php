<?php

namespace MommyCom\Service\Money;

use Money\Currencies;
use Money\Money;
use Money\MoneyFormatter;

class Formatter implements MoneyFormatter
{
    private static $formats = [
        CurrencyDirectory::CODE_GLOBAL => [
            'template' => '%s %s',
            'decimals' => 2,
            'decimalPoint' => '.',
            'thousandsSeparator' => ' ',
            'symbol' => CurrencyDirectory::SYMBOL_GLOBAL,
        ],
        CurrencyDirectory::CODE_INDONESIA => [
            'template' => '%2$s %1$s',
            'decimals' => 0,
            'decimalPoint' => ',',
            'thousandsSeparator' => '.',
            'symbol' => CurrencyDirectory::SYMBOL_INDONESIA,
        ],
        CurrencyDirectory::CODE_VIETNAM => [
            'template' => '%s %s',
            'decimals' => 0,
            'decimalPoint' => ',',
            'thousandsSeparator' => ' ',
            'symbol' => CurrencyDirectory::SYMBOL_VIETNAM,
        ],
        CurrencyDirectory::CODE_ROMANIA => [
            'template' => '%s %s',
            'decimals' => 0,
            'decimalPoint' => ',',
            'thousandsSeparator' => '.',
            'symbol' => CurrencyDirectory::SYMBOL_ROMANIA,
        ],
        CurrencyDirectory::CODE_COLOMBIA => [
            'template' => '%s %s',
            'decimals' => 0,
            'decimalPoint' => ',',
            'thousandsSeparator' => ' ',
            'symbol' => CurrencyDirectory::SYMBOL_COLOMBIA,
        ],
    ];

    /**
     * @var Currencies
     */
    private $currencies;

    /**
     * Formatter constructor.
     *
     * @param Currencies $currencies
     */
    public function __construct(Currencies $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * @param Money $money
     *
     * @return string
     */
    public function format(Money $money)
    {
        list(
            'template' => $template,
            'decimals' => $decimals,
            'decimalPoint' => $decimalPoint,
            'thousandsSeparator' => $thousandsSeparator,
            'symbol' => $symbol
            ) = $this->getFormat($money->getCurrency()->getCode());

        $amount = $money->getAmount() / 10 ** $this->currencies->subunitFor($money->getCurrency());
        $number = $this->numberFormat($amount, $decimals, $decimalPoint, $thousandsSeparator);

        return sprintf($template, $number, $symbol);
    }

    /**
     * @param string $code
     *
     * @return array
     */
    private function getFormat(string $code)
    {
        return self::$formats[$code] ?? self::$formats[CurrencyDirectory::CODE_GLOBAL];
    }

    /**
     * @param float $number
     * @param int $decimals
     * @param string $decimalPoint
     * @param string $thousandsSeparator
     *
     * @return string
     */
    private function numberFormat(float $number, int $decimals, string $decimalPoint, string $thousandsSeparator): string
    {
        $result = number_format($number, $decimals, $decimalPoint, $thousandsSeparator);

        if ($decimals > 0) {
            $result = rtrim($result, '0');
            $result = rtrim($result, $decimalPoint);
        }

        return $result;
    }
}
