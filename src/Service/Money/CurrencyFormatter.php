<?php

namespace MommyCom\Service\Money;

use Money\Currencies;
use Money\Currency;
use Money\Money;
use Money\MoneyFormatter;

class CurrencyFormatter
{
    /**
     * @var Currency
     */
    private $defaultCurrency;

    /**
     * @var Currencies
     */
    private $currencies;

    /**
     * @var MoneyFormatter
     */
    private $formatter;

    /**
     * @var int|null
     */
    private $valueMultiplier = null;

    /**
     * CurrencyFormatter constructor.
     *
     * @param Currencies $currencies
     * @param Currency $defaultCurrency
     * @param MoneyFormatter $moneyFormatter
     */
    public function __construct(Currencies $currencies, Currency $defaultCurrency, MoneyFormatter $moneyFormatter)
    {
        $this->defaultCurrency = $defaultCurrency;
        $this->currencies = $currencies;
        $this->formatter = $moneyFormatter;
    }

    /**
     * @param Money|string|null $value
     *
     * @return string
     */
    public function format($value): string
    {
        if (null === $value) {
            return '';
        }

        if (!$value instanceof Money) {
            $value = $this->makeMoney($value);
        }

        return $this->formatter->format($value);
    }

    /**
     * @param string|int|float $value
     *
     * @return Money
     */
    private function makeMoney($value): Money
    {
        if (is_string($value) && $value !== '0') {
            $value = ltrim($value, '0');
        }

        $value = (float)$value * $this->getValueMultiplier();
        $value = (int)round($value);

        return new Money($value, $this->defaultCurrency);
    }

    /**
     * @return int
     */
    private function getValueMultiplier(): int
    {
        if (null === $this->valueMultiplier) {
            $this->valueMultiplier = 10 ** $this->currencies->subunitFor($this->defaultCurrency);
        }

        return $this->valueMultiplier;
    }
}
