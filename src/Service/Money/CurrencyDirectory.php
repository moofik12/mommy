<?php

namespace MommyCom\Service\Money;

class CurrencyDirectory
{
    const CODE_GLOBAL = 'USD';
    const CODE_INDONESIA = 'IDR';
    const CODE_VIETNAM = 'VND';
    const CODE_ROMANIA = 'RON';
    const CODE_COLOMBIA = 'COP';

    const SYMBOL_GLOBAL = '$';
    const SYMBOL_INDONESIA = 'idr';
    const SYMBOL_VIETNAM = '₫';
    const SYMBOL_ROMANIA = 'lei';
    const SYMBOL_COLOMBIA = '$';

    const SYMBOLS = [
        self::CODE_GLOBAL => self::SYMBOL_GLOBAL,
        self::CODE_INDONESIA => self::SYMBOL_INDONESIA,
        self::CODE_VIETNAM => self::SYMBOL_VIETNAM,
        self::CODE_ROMANIA => self::SYMBOL_ROMANIA,
        self::CODE_COLOMBIA => self::SYMBOL_COLOMBIA,
    ];
}
