<?php

namespace MommyCom\Service;

class TelegramBot
{
    const TELEGRAM_API = 'https://api.telegram.org';

    /**
     * @var string
     */
    private $token;

    /**
     * TelegramBot constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @param string $chatId
     * @param string $message
     */
    public function sendMessage(string $chatId, string $message)
    {
        if (!$this->token) {
            throw new \RuntimeException('Telegram bot token is empty');
        }

        $url = self::TELEGRAM_API .
            '/bot' .
            $this->token .
            '/sendMessage?chat_id=' .
            $chatId .
            '&text=' .
            urlencode($message);

        $ch = curl_init();
        $optArray = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        ];
        curl_setopt_array($ch, $optArray);
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
