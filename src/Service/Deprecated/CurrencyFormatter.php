<?php

namespace MommyCom\Service\Deprecated;

use MommyCom\Service\Money\CurrencyDirectory;
use MommyCom\Service\Money\CurrencyFormatter as BaseCurrencyFormatter;
use MommyCom\Service\Region\Regions;

/**
 * Class CurrencyFormatter
 * Класс прокси вместо старого компонента
 *
 * @deprecated use Money\CurrencyFormatter
 * @see \MommyCom\Service\Money\CurrencyFormatter
 */
class CurrencyFormatter
{
    /**
     * @var BaseCurrencyFormatter
     */
    private $formatter;

    /**
     * @var Regions
     */
    private $regions;

    /**
     * CurrencyFormatter constructor.
     *
     * @param BaseCurrencyFormatter $formatter
     * @param Regions $regions
     */
    public function __construct(BaseCurrencyFormatter $formatter, Regions $regions)
    {
        $this->formatter = $formatter;
        $this->regions = $regions;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function format($value): string
    {
        return $this->formatter->format($value);
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        $currencyCode = $this->regions->getServerRegion()->getCurrencyCode();

        return CurrencyDirectory::SYMBOLS[$currencyCode] ?? CurrencyDirectory::SYMBOL_GLOBAL;
    }

    /**
     * @param float $value
     * @param string $language
     *
     * @return string
     */
    public function convertToWords($value, $language)
    {
        return Currency2Words::instance($language)->convert($value);
    }
}
