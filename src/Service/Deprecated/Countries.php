<?php

namespace MommyCom\Service\Deprecated;

use MommyCom\Service\Region\Regions;

/**
 * Class Countries
 *
 * @deprecated use Regions
 * @see \MommyCom\Service\Region\Regions
 * @see \MommyCom\Service\Region\Region
 */
class Countries
{
    /**
     * @var Regions
     */
    private $regions;

    /**
     * Countries constructor.
     *
     * @param Regions $regions
     */
    public function __construct(Regions $regions)
    {
        $this->regions = $regions;
    }

    /**
     * @return \MommyCom\Service\Deprecated\CurrencyName
     */
    public function getCurrency()
    {
        return CurrencyName::instance($this->regions->getServerRegion()->getCurrencyCode());
    }
}
