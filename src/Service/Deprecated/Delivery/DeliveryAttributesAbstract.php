<?php

namespace MommyCom\Service\Deprecated\Delivery;

/**
 * Class DeliveryAttributesAbstract
 *
 * @deprecated
 */
abstract class DeliveryAttributesAbstract extends \CModel
{
    /**
     * @var string[][]
     */
    private static $_names = [];

    /**
     * Copy from CFormModel
     *
     * @return string[]
     * @throws \ReflectionException
     */
    public function attributeNames()
    {
        $className = get_class($this);

        if (!isset(self::$_names[$className])) {
            $class = new \ReflectionClass(get_class($this));
            $names = [];
            foreach ($class->getProperties() as $property) {
                $name = $property->getName();
                if ($property->isPublic() && !$property->isStatic())
                    $names[] = $name;
            }
            self::$_names[$className] = $names;
        }

        return self::$_names[$className];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }
}
