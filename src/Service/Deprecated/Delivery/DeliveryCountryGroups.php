<?php

namespace MommyCom\Service\Deprecated\Delivery;

/**
 * Class DeliveryCountryGroups
 *
 * @deprecated
 */
class DeliveryCountryGroups
{
    /**
     * @var static|null
     */
    private static $instance = null;

    /**
     * @var DeliveryTypeUa|null
     */
    private static $deliveryInstance = null;

    protected function __construct()
    {
        // singleton
    }

    /**
     * @return DeliveryCountryGroups
     */
    public static function instance()
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * @return DeliveryTypeUa
     */
    public function getDelivery()
    {
        return self::$deliveryInstance ?: self::$deliveryInstance = new DeliveryTypeUa();
    }
} 
