<?php

namespace MommyCom\Service\Deprecated\Delivery;

use MommyCom\YiiComponent\Facade\Delivery;

/**
 * Class DeliveryTypeUa
 *
 * @deprecated
 */
class DeliveryTypeUa
{
    /**
     * @deprecated
     */
    const DELIVERY_TYPE_UKRPOST = 1;

    /**
     * @deprecated
     */
    const DELIVERY_TYPE_NOVAPOSTA = 2;

    /**
     * @deprecated
     */
    const DELIVERY_TYPE_COURIER = 3;

    /**
     * @param bool $onlyKey
     *
     * @return array
     */
    public function getList($onlyKey = false)
    {
        $result = [];
        foreach (Delivery::available()->getDeliveries() as $delivery) {
            $result[$delivery->getId()] = $delivery->getName();
        }

        return $onlyKey ? array_keys($result) : $result;
    }
}
