<?php

namespace MommyCom\Service\Deprecated;

use MommyCom\Service\Money\CurrencyDirectory;

/**
 * Class CurrencyName
 * Класс прокси вместо старого компонента
 *
 * @deprecated use CurrencyFormatter
 * @see \MommyCom\Service\Deprecated\CurrencyFormatter
 */
class CurrencyName
{
    /**
     * @var CurrencyName[]
     */
    private static $instances = [];

    /**
     * @var string
     */
    private $code;

    /**
     * @param string $code
     *
     * @return CurrencyName
     */
    public static function instance(string $code): self
    {
        if (!isset(self::$instances[$code])) {
            self::$instances[$code] = new self($code);
        }

        return self::$instances[$code];
    }

    /**
     * CurrencyName constructor.
     *
     * @param string $code
     */
    private function __construct(string $code)
    {
        if (!isset(CurrencyDirectory::SYMBOLS[$code])) {
            throw new \InvalidArgumentException('Unknown currency code');
        }

        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return CurrencyDirectory::SYMBOLS[$this->code];
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function declination(): string
    {
        return CurrencyDirectory::SYMBOLS[$this->code];
    }
}
