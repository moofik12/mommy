<?php

namespace MommyCom\Service\Deprecated;

class Currency2Words
{
    /**
     * @var self[]
     */
    private static $instances = [];

    /**
     * @var \NumberFormatter
     */
    private $formatter;

    /**
     * Currency2Words constructor.
     *
     * @param string $locale
     */
    private function __construct(string $locale)
    {
        $this->formatter = new \NumberFormatter($locale, \NumberFormatter::SPELLOUT);
    }

    /**
     * @param string $locale
     *
     * @return Currency2Words
     */
    public static function instance($locale)
    {
        if (!isset(self::$instances[$locale])) {
            self::$instances[$locale] = new self($locale);
        }

        return self::$instances[$locale];
    }

    /**
     * @param integer|float $value
     *
     * @return string
     */
    public function convert($value)
    {
        return $this->formatter->format($value);
    }
} 
