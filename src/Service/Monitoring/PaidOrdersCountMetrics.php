<?php

namespace MommyCom\Service\Monitoring;

use CDbCriteria;
use MommyCom\Model\Db\OrderRecord;

class PaidOrdersCountMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'card_payed > 0';
        $criteria->addBetweenCondition('created_at', time() - 86400, time());

        return OrderRecord::model()->count($criteria);
    }
}
