<?php

namespace MommyCom\Service\Monitoring;

use Symfony\Component\DomCrawler\Crawler;

class ProductsExistsMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $baseUrl = getenv('BASE_MONITORING_URL') . '/catalog';
        $html = file_get_contents($baseUrl);
        $crawler = new Crawler($html);

        return count($crawler->filter('.goods-item'));
    }
}
