<?php

namespace MommyCom\Service\Monitoring;

class PhoneExistsMobileMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $baseUrl = getenv('BASE_MONITORING_URL_MOBILE');
        $html = file_get_contents($baseUrl);

        preg_match('~href="tel:.+"~', $html, $matches);

        return count($matches);
    }
}
