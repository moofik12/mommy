<?php

namespace MommyCom\Service\Monitoring;

use CDbCriteria;
use MommyCom\Model\Db\OrderRecord;

class OrdersCountMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('created_at', time() - 86400, time());

        return OrderRecord::model()->count($criteria);
    }
}
