<?php

namespace MommyCom\Service\Monitoring;

use CDbCriteria;
use MommyCom\Model\Db\UserRecord;

class RegistrationCountMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('created_at', time() - 86400, time());

        return UserRecord::model()->count($criteria);
    }
}
