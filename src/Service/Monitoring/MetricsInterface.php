<?php

namespace MommyCom\Service\Monitoring;

interface MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue();
}
