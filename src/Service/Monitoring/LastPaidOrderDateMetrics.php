<?php

namespace MommyCom\Service\Monitoring;

use CDbCriteria;
use MommyCom\Model\Db\OrderRecord;

class LastPaidOrderDateMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $model = OrderRecord::model();
        $criteria = new CDbCriteria;
        $criteria->order = 'id desc';
        $criteria->condition = 'card_payed > 0';
        $row = $model->find($criteria);

        return time() - $row['created_at'];
    }
}
