<?php

namespace MommyCom\Service\Monitoring;

use CDbCriteria;
use MommyCom\Model\Db\UserRecord;

class LastRegistrationDateMetrics implements MetricsInterface
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        $model = UserRecord::model();
        $criteria = new CDbCriteria;
        $criteria->order = 'id desc';
        $row = $model->find($criteria);

        return time() - $row['created_at'];
    }
}
