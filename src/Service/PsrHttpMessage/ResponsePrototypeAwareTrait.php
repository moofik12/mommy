<?php

namespace MommyCom\Service\PsrHttpMessage;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

trait ResponsePrototypeAwareTrait
{
    /**
     * @var ResponseInterface|null
     */
    private $responsePrototype = null;

    /**
     * @param ResponseInterface $response
     */
    public function setResponsePrototype(ResponseInterface $response): void
    {
        $this->responsePrototype = $response;
    }

    /**
     * @return ResponseInterface
     */
    protected function getResponse(): ResponseInterface
    {
        return $this->responsePrototype ?: $this->responsePrototype = new Response();
    }
}
