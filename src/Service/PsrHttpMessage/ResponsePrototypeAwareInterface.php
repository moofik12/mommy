<?php

namespace MommyCom\Service\PsrHttpMessage;

use Psr\Http\Message\ResponseInterface;

interface ResponsePrototypeAwareInterface
{
    /**
     * @param ResponseInterface $response
     */
    public function setResponsePrototype(ResponseInterface $response): void;
}
