<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use MommyCom\YiiComponent\Frontend\FrontController;

class MaintenanceController extends FrontController
{
    /**
     * @var string
     */
    public $layout = '//layouts/maintenance';

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    /**
     * Информационное сообщение при обслуживании сайта
     */
    public function actionIndex()
    {
        if (!file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . 'maintenance')) {
            throw new CHttpException(404, $this->t('Page not found'));
        }

        $this->render('index');
    }
}
