<?php

namespace MommyCom\Controller\Frontend;

use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\Frontend\FrontController;

class PastController extends FrontController
{
    const DEFAULT_PAGE_SIZE = 45;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    /**
     * @return array
     */
    public function cache()
    {
        return [];
    }

    public function actionIndex()
    {
        $provider = EventRecord::model()
            ->isVirtual(false)
            ->onlyVisible()
            ->onlyPublished()
            ->onlyTimeClosed()
            ->isDeleted(false)
            ->orderBy('end_at', 'desc')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => self::DEFAULT_PAGE_SIZE,
                    'pageVar' => 'page',
                ],
            ]);
        $events = $provider->getData();

        $this->render('index', [
            'events' => $events,
            'provider' => $provider,
        ]);
    }
}
