<?php

namespace MommyCom\Controller\Frontend;

use CException;
use CLogger;
use CMap;
use DateTime;
use Exception;
use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\UsersProvider\AdmitadAutoSyncOrder;
use MommyCom\Model\UsersProvider\AdmitadAutoSyncOrders;
use MommyCom\Model\UsersProvider\AdverdsSspAutoSyncOrders;
use MommyCom\Model\UsersProvider\AdvertsSspAutoSyncOrder;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class SyncProvidersController extends FrontController
{

    const OFFER_PROVIDER_ADMITAD = 'admitad';
    const OFFER_PROVIDER_SSP = 'advertssp';

    const RESPONSE_TYPE_FILE_XML = 'xml';
    const RESPONSE_TYPE_FILE_JSON = 'json';

    private $syncAdmitadLogin;
    private $syncAdmitadPassword;

    private $syncAdvertsspLogin;
    private $syncAdvertsspPassword;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    public function init()
    {
        $initParams = [
            'syncAdmitadLogin', 'syncAdmitadPassword',
            'syncAdvertsspLogin', 'syncAdvertsspPassword',
        ];

        foreach ($initParams as $param) {
            if ($this->app()->params->contains($param)) {
                $this->$param = $this->app()->params->itemAt($param);
            }
        }

        parent::init();
    }

    public function actionAdmitadPayOrders()
    {
        $this->_checkAccess($this->syncAdmitadLogin, $this->syncAdmitadPassword, self::OFFER_PROVIDER_ADMITAD, true);

        $this->_admitadPayOrders();
    }

    public function actionAdvertsspPayOrders()
    {
        $this->_checkAccess($this->syncAdvertsspLogin, $this->syncAdvertsspPassword, self::OFFER_PROVIDER_SSP, true);

        $this->_advertsspPayOrders();
    }

    /**
     * @param string $login
     * @param string $password
     * @param bool|true $end закончить работу скрипта при не верной авторизации
     * @param string $provider
     *
     * @return bool
     */
    private function _checkAccess($login, $password, $provider, $end = true)
    {
        $connectUser = isset($_SERVER['PHP_AUTH_USER']) ? Cast::toStr($_SERVER['PHP_AUTH_USER']) : '';
        $connectPass = isset($_SERVER['PHP_AUTH_PW']) ? Cast::toStr($_SERVER['PHP_AUTH_PW']) : '';

        if ($connectUser || $connectPass) {
            Yii::log($this->t('Authorization for synchronizing orders with') . ' ' . $provider . ' Login: ' . $connectUser, CLogger::LEVEL_INFO, 'sync.orders');
        }

        if ($this->generateSecurityHash($connectUser, $connectPass) === $this->generateSecurityHash($login, $password)) {
            return true;
        }

        if ($end) {
            header('WWW-Authenticate: Basic');
            header('HTTP/1.0 401 Unauthorized');
            $this->app()->end();
        }

        return false;
    }

    /**
     * @param string $login
     * @param string $pass
     *
     * @return string
     */
    private function generateSecurityHash(string $login, string $pass)
    {
        static $salt = '7t/ou3~xLfP.Gb';

        return md5($login . $salt . $pass);
    }

    /**
     * отдача заказов для Адмита
     */
    private function _admitadPayOrders()
    {
        $offerProvider = self::OFFER_PROVIDER_ADMITAD;
        $dataTime = new DateTime('-4 weeks'); //начало обновлений
        $response = new AdmitadAutoSyncOrders();
        $countResponseItems = 0;

        //CONFIRMED
        $ordersPayedIn = OrderBuyoutRecord::model()
            ->statusChangedAtGreater($dataTime->getTimestamp())
            ->status(OrderBuyoutRecord::STATUS_PAYED)
            ->findColumnDistinct('order_id');

        $countToUpdate = 0;
        $countUpdated = 0;

        $ordersPayedIdQueue = array_chunk($ordersPayedIn, 100);
        foreach ($ordersPayedIdQueue as $heapOrder) {
            $update = [];
            /** @var OrderRecord[] $ordersPayed */
            $ordersPayed = OrderRecord::model()
                ->offerProvider(UserRecord::OFFER_PROVIDER_ADMITAD)
                ->idIn($heapOrder)
                //            ->isOfferSuccess(false)
                ->findAll();

            $countResponseItems += count($ordersPayed);
            foreach ($ordersPayed as $order) {
                $model = new AdmitadAutoSyncOrder($order, AdmitadAutoSyncOrder::STATUS_CONFIRM);
                $response->addModel($model);

                if (!$order->is_offer_success) {
                    $order->is_offer_success = true;
                    $update[] = $order;
                }
            }

            if ($update) {
                $countToUpdate += count($update);

                try {
                    $updatesResult = UnShardedActiveRecord::saveMany($update, false, ['is_offer_success']);
                    if ($updatesResult) {
                        $countUpdated += count($update);
                    }
                } catch (CException $e) {
                }
            }
        }

        //CANCEL
        $ordersCanceledIn = OrderBuyoutRecord::model()
            ->statusChangedAtGreater($dataTime->getTimestamp())
            ->status(OrderBuyoutRecord::STATUS_CANCELLED)
            ->findColumnDistinct('order_id');

        $countResponseItems += count($ordersCanceledIn);
        $ordersCanceledInQueue = array_chunk($ordersCanceledIn, 100);

        foreach ($ordersCanceledInQueue as $heapOrder) {
            $update = [];
            /** @var OrderRecord[] $ordersCanceled */
            $ordersCanceledBuyout = OrderRecord::model()
                ->offerProvider(UserRecord::OFFER_PROVIDER_ADMITAD)
                ->idIn($heapOrder)
//            ->isOfferSuccess(false)
                ->findAll();

            $ordersCanceledOther = OrderRecord::model()
                ->offerProvider(UserRecord::OFFER_PROVIDER_ADMITAD)
                ->updatedAtGreater($dataTime->getTimestamp())
                ->processingStatuses([OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER, OrderRecord::PROCESSING_CANCELLED])
                ->idInNot($heapOrder)
//            ->isOfferSuccess(false)
                ->findAll();

            $ordersCanceled = CMap::mergeArray($ordersCanceledBuyout, $ordersCanceledOther);

            $countResponseItems += count($ordersCanceled);
            foreach ($ordersCanceled as $order) {
                $model = new AdmitadAutoSyncOrder($order, AdmitadAutoSyncOrder::STATUS_CANCEL);
                $response->addModel($model);

                if (!$order->is_offer_success) {
                    $order->is_offer_success = true;
                    $update[] = $order;
                }
            }

            if ($update) {
                $countToUpdate += count($update);

                try {
                    $updatesResult = UnShardedActiveRecord::saveMany($update, false, ['is_offer_success']);
                    if ($updatesResult) {
                        $countUpdated += count($update);
                    }
                } catch (CException $e) {
                }
            }
        }

        Yii::log($this->t('Synchronization of orders with {offerProvider}, orders for processing - {count}, orders must be renewed - {countToUpdate}, orders updated - {countUpdated}',
            [
                '{offerProvider}' => $offerProvider,
                '{count}' => $countResponseItems,
                '{countToUpdate}' => $countToUpdate,
                '{countUpdated}' => $countUpdated,
            ]), CLogger::LEVEL_INFO, 'sync.orders');

        $this->_saveDuplicateFile($offerProvider, $response->toXml(), self::RESPONSE_TYPE_FILE_XML);

        header('Content-Type: text/xml');
        echo $response->toXml();
        $this->app()->end();
    }

    /**
     * отдача заказов для SSP
     */
    private function _advertsspPayOrders()
    {
        $offerProvider = self::OFFER_PROVIDER_SSP;
        $dataTime = new DateTime('-4 days'); //начало обновлений

        $ordersPayedIn = OrderBuyoutRecord::model()
            ->statusChangedAtGreater($dataTime->getTimestamp())
            ->status(OrderBuyoutRecord::STATUS_PAYED)
            ->findColumnDistinct('order_id');

        $ordersCanceledIn = OrderBuyoutRecord::model()
            ->statusChangedAtGreater($dataTime->getTimestamp())
            ->status(OrderBuyoutRecord::STATUS_CANCELLED)
            ->findColumnDistinct('order_id');

        /** @var OrderRecord[] $ordersPayed */
        $ordersPayed = OrderRecord::model()
            ->offerProvider(UserRecord::OFFER_PROVIDER_SSP)
            ->idIn($ordersPayedIn)
//            ->isOfferSuccess(false)
            ->findAll();

        /** @var OrderRecord[] $ordersCanceled */
        $ordersCanceledBuyout = OrderRecord::model()
            ->offerProvider(UserRecord::OFFER_PROVIDER_SSP)
            ->idIn($ordersCanceledIn)
//            ->isOfferSuccess(false)
            ->findAll();

        $ordersCanceledOther = OrderRecord::model()
            ->offerProvider(UserRecord::OFFER_PROVIDER_SSP)
            ->updatedAtGreater($dataTime->getTimestamp())
            ->processingStatuses([OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER, OrderRecord::PROCESSING_CANCELLED])
            ->idInNot($ordersCanceledIn)
//            ->isOfferSuccess(false)
            ->findAll();

        $ordersCanceled = CMap::mergeArray($ordersCanceledBuyout, $ordersCanceledOther);

        $response = new AdverdsSspAutoSyncOrders();

        $update = [];
        $ordersIDNotWaiting = [];

        foreach ($ordersPayed as $order) {
            $model = new AdvertsSspAutoSyncOrder($order, AdvertsSspAutoSyncOrder::STATUS_CONFIRMED);
            $response->addModel($model);

            if (!$order->is_offer_success) {
                $order->is_offer_success = true;
                $update[] = $order;
            }

            $ordersIDNotWaiting[] = $order->id;
        }

        foreach ($ordersCanceled as $order) {
            $model = new AdvertsSspAutoSyncOrder($order, AdvertsSspAutoSyncOrder::STATUS_REJECTED);
            $response->addModel($model);

            if (!$order->is_offer_success) {
                $order->is_offer_success = true;
                $update[] = $order;
            }

            $ordersIDNotWaiting[] = $order->id;
        }

        $ordersWaiting = OrderRecord::model()
            ->offerProvider(UserRecord::OFFER_PROVIDER_SSP)
            ->updatedAtGreater($dataTime->getTimestamp())
            ->idInNot($ordersIDNotWaiting)
//            ->isOfferSuccess(false)
            ->findAll();

        foreach ($ordersWaiting as $order) {
            $model = new AdvertsSspAutoSyncOrder($order, AdvertsSspAutoSyncOrder::STATUS_WAITING);
            $response->addModel($model);
        }

        $countResponseItems = count($ordersPayed) + count($ordersCanceled);
        $updateCount = count($update);
        $updatesResult = false;

        try {
            if ($updateCount > 0) {
                $updatesResult = UnShardedActiveRecord::saveMany($update, false, ['is_offer_success']);
            } else {
                $updatesResult = true;
            }
        } catch (CException $e) {
            Yii::log($this->t('Error while saving order update from') . ' ' . $offerProvider, CLogger::LEVEL_ERROR, 'sync.orders');
        }

        Yii::log($this->t('Synchronization of orders with {offerProvider}, orders for processing were given - {count}, orders were updated - {updateCount}, order statuses were updated',
                [
                    '{offerProvider}' => $offerProvider,
                    '{count}' => $countResponseItems,
                    '{updateCount}' => $updateCount,
                ]
            ) . ' - '
            . ($updatesResult ? $this->t('Yes') : $this->t('no')), CLogger::LEVEL_INFO, 'sync.orders');

        $this->_saveDuplicateFile($offerProvider, $response->toXml(), self::RESPONSE_TYPE_FILE_XML);

        header('Content-Type: text/xml');
        echo $response->toXml();
        $this->app()->end();
    }

    /**
     * @param string $provider @see self
     * @param string $data
     * @param $responseTypeFile
     */
    private function _saveDuplicateFile($provider, $data, $responseTypeFile)
    {
        try {
            $dataTimeLogFile = date('d.m.Y_H:s');
            $logFile = $this->_getPathToFolder($provider) . DIRECTORY_SEPARATOR . "$dataTimeLogFile.$responseTypeFile";

            $fp = fopen($logFile, 'a');
            fwrite($fp, $data);
            fclose($fp);
        } catch (Exception $e) {
            Yii::log($this->t('Error creating duplicate file for Admitad'), CLogger::LEVEL_ERROR, 'sync.orders.admintad');
        }
    }

    /**
     * @param $folder
     *
     * @return string
     */
    protected function _getPathToFolder($folder)
    {
        $path = $this->app()->getRuntimePath() . DIRECTORY_SEPARATOR . 'offers' . DIRECTORY_SEPARATOR . $folder;

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }
}
