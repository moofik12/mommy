<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\PromoUserRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\AuthManager\MailingUserIdentity;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class UsersProviderController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    public function actionIndex()
    {
        /** @var ShopWebUser $user */
        $user = $this->app()->user;

        if (!$user->isGuest) {
            $this->redirect(['index/index']);
        }

        $user->setOfferProvider(UserRecord::OFFER_PROVIDER_NONE);
        $user->setOfferId(0);
        $this->redirect(['index/index', 'promoLogin' => true]);
    }

    public function actionActionpay($actionpay, $redirectTo = '')
    {
        $user = $this->app()->user;

        if (!$user->isGuest) {
            $this->redirect(['index/index']);
        }

        if ($redirectTo == '') {
            $redirectTo = $this->app()->createAbsoluteUrl('index/index');
        }

        $user->setOfferProvider(UserRecord::OFFER_PROVIDER_ACTIONPAY);
        $user->setOfferId($actionpay);

        $this->app()->request->redirectInternal($redirectTo);
    }

    public function actionAt($cpamit_uid, $redirectTo = '')
    {
        $user = $this->app()->user;

        if (!$user->isGuest) {
            $this->redirect(['index/index']);
        }

        if ($redirectTo == '') {
            $redirectTo = $this->app()->createAbsoluteUrl('index/index');
        }

        $user->setOfferProvider(UserRecord::OFFER_PROVIDER_ADMITAD);
        $user->setOfferId($cpamit_uid);

        $this->app()->request->redirectInternal($redirectTo);
    }

    /**
     * Новое регистрация по admitad_uid
     *
     * @param $admitad_uid
     * @param string $redirectTo
     * @param bool $balance
     */
    public function actionAtu($admitad_uid, $redirectTo = '')
    {
        $user = $this->app()->user;

        if (!$user->isGuest) {
            $this->redirect(['index/index']);
        }

        if ($redirectTo == '') {
            $redirectTo = $this->app()->createAbsoluteUrl('index/index');
        }

        $user->setOfferProvider(UserRecord::OFFER_PROVIDER_ADMITAD);
        $user->setOfferId($admitad_uid);

        $this->app()->request->redirectInternal($redirectTo);
    }

    public function actionSsp($waylog_id, $redirectTo = '')
    {
        $user = $this->app()->user;

        if (!$user->isGuest) {
            $this->redirect(['index/index']);
        }

        if ($redirectTo == '') {
            $redirectTo = $this->app()->createAbsoluteUrl('index/index');
        }

        $user->setOfferProvider(UserRecord::OFFER_PROVIDER_SSP);
        $user->setOfferId($waylog_id);

        $this->app()->request->redirectInternal($redirectTo);
    }

    public function actionCustomCampaign($name, $redirectTo = '')
    {
        $user = $this->app()->user;

        if (!$user->isGuest) {
            $this->redirect(['index/index']);
        }

        if ($redirectTo == '') {
            $redirectTo = $this->app()->createAbsoluteUrl('index/index');
        }
        /** @var $user ShopWebUser */
        $user->setOfferProvider(UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN);
        $user->setOfferId($name);
        $user->setOfferTargetUrl($redirectTo);

        //бенефисы для 1kr и яндекс директ
        if ($name === '1kr.ua' || mb_stripos($name, 'direct-') !== false) {
            $user->setEnableOfferBenefice(true); //выдача бонусов
            $psychotype = $this->app()->request->getParam('ps', false);
            $user->setPsychotype($psychotype, true);
            $offerBannerNumber = $this->app()->request->getParam('bn', $psychotype);
            $user->setOfferBannerNumber($offerBannerNumber, true);
        }

        $this->app()->request->redirectInternal($redirectTo);
    }

    /**
     * @param $id
     * @param $hash
     * @param $redirectTo
     *
     * @throws CHttpException
     */
    public function actionMailingPromo($id, $hash, $redirectTo = '')
    {
        $id = Cast::toUInt($id);
        $hash = Cast::toStr($hash);
        $redirectTo = Cast::toStr($redirectTo);

        if (empty($redirectTo)) {
            $redirectTo = $this->createUrl('index/index');
        }

        $this->app()->user->logout();

        $promoUser = PromoUserRecord::model()->findByPk($id);
        /* @var $promoUser PromoUserRecord */
        if ($promoUser === null) {
            throw new CHttpException(404, $this->t('User is not found'));
        }

        if ($promoUser->registrationHash == $hash) {
            $user = $promoUser->getRegisteredUser();
            if (!$promoUser->getIsRegistered()) {
                $password = Random::alphabet('6', Random::ALPHABET_SIMPLIFIED); // generate password

                $user = new UserRecord('register');
                $user->name = $promoUser->name;
                $user->email = Utf8::trim($promoUser->email);
                $user->password = $password;
                $user->is_email_verified = true;

                if ($user->save()) {
                    // here send password to email
                    $this->app()->mailer->create(
                        [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                        [$user->email],
                        $this->t('IMPORTANT INFORMATION for a new club member'),
                        [
                            'view' => 'welcome2',
                            'data' => ['user' => $user, 'password' => $password]]
                    );
                } else {
                    $user = null;
                }
            }

            if ($user !== null) {
                $identity = new MailingUserIdentity($user->id, $user->getLoginHash());
                if ($identity->authenticate()) {
                    $duration = isset($this->app()->params['remember']) ? $this->app()->params['remember'] : 0;
                    $this->app()->user->login($identity, $duration);
                }
            }
        }

        $this->redirect($redirectTo);
    }

    /**
     * @param integer $id
     * @param string $hash
     * @param string $redirectTo
     *
     * @throws CHttpException
     */
    public function actionMailingRegistered($id, $hash, $redirectTo = '')
    {
        $id = Cast::toUInt($id);
        $hash = Cast::toStr($hash);
        $redirectTo = Cast::toStr($redirectTo);

        if (empty($redirectTo)) {
            $redirectTo = $this->createUrl('index/index');
        } else {
            $redirectTo = urldecode($redirectTo);
        }

        $this->app()->user->logout();

        $user = UserRecord::model()->findByPk($id);
        /* @var $user UserRecord */
        if ($user === null) {
            throw new CHttpException(404, $this->t('User is not found'));
        }

        $identity = new MailingUserIdentity($user->id, $hash);
        if ($identity->authenticate()) {
            $duration = isset($this->app()->params['remember']) ? $this->app()->params['remember'] : 0;
            $this->app()->user->login($identity, $duration);

            if (!$user->is_email_verified) {
                $user->is_email_verified = true;
                $user->save();
            }
        }

        $redirectTo = $this->_additionUtmParams($redirectTo);

        //пока только при переходе с письма "Брошенная корзина"
        if (mb_stripos($redirectTo, 'utm_campaign=cart') !== false) {
            $this->app()->user->cart->updateReservations();
        }

        $this->redirect($redirectTo);
    }

    /**
     * Добавление в ссылку UTM переменных которые попали в этот запрос
     * актуально для Unisender так, как он в цикле приводит ссылку в нормальный вид (urldecode)
     * в итоге в параметр redirectTo не доходят UTM переменные
     *
     * @param $url
     *
     * @return mixed
     */
    protected function _additionUtmParams($url)
    {
        $utmAvailableParams = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content', 'mode'];

        $utmParams = array_map(function ($param) {
            return $this->app()->request->getQuery($param);
        }, array_combine($utmAvailableParams, $utmAvailableParams));

        //$hasUtmParams = array_filter($utmParams);
        function is_not_null_nol($val)
        {
            return !is_null($val) || $val === 0;
        }

        $hasUtmParams = array_filter($utmParams, 'is_not_null_nol');

        parse_str(parse_url($url, PHP_URL_QUERY), $paramsInUrl);

        $mainParams = $paramsInUrl + $hasUtmParams;

        if (mb_stripos($url, '?') !== false) {
            $url = preg_replace('/\?.[^#]*/u', '?' . urldecode(http_build_query($mainParams)), $url);
        } elseif ($mainParams) {
            $url .= '?' . urldecode(http_build_query($mainParams));
        }

        return $url;
    }

    /**
     * @property-description Принудительное обновление резервирования товаров в корзине (если их они доступны для покупки)
     */
    public function actionCartUpdate()
    {
        $redirectTo = $this->createUrl('index/index');
        $user = $this->app()->user;
        /** @var $user ShopWebUser */
        if (!$user->isGuest) {
            $redirectTo = $this->createUrl('order/index');
            //$user->cart->updateReservations();
            //принудительное обновление резервирования товаров в корзине (если их они доступны для покупки)
            foreach ($user->cart->getPositions() as $position) {
                if ($position->product->getIsPossibleToBuy($position->number)) {
                    if (!$position->product->event->is_virtual) {
                        $position->reserved_to = time() + CartRecord::RESERVATION_TIME;
                    } else {
                        $position->reserved_to = time() + CartRecord::VIRTUAL_EVENT_RESERVATION_TIME;
                    }
                    $position->reservation_bonus_time = 0;
                    if ($position->is_sent_notification) {
                        $position->is_sent_notification = 0;
                    }
                    $position->save();
                }
            }
        }

        $redirectTo = $this->_additionUtmParams($redirectTo);

        $this->redirect($redirectTo);
    }
}
