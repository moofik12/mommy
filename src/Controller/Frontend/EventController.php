<?php

namespace MommyCom\Controller\Frontend;

use CArrayDataProvider;
use CAssetManager;
use CClientScript;
use CException;
use CHttpException;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class EventController extends FrontController
{
    const DEFAULT_CACHE = 30;
    const DEFAULT_PAGE_SIZE = 48;

    /**
     * @var bool
     */
    public $showPromoLogin = false;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    public function cache()
    {
        return [];
    }

    /**
     * @param mixed $sort
     *
     * @return array
     */
    protected function _sanitizeSort($sort)
    {
        if ($sort == 'price_desc') {
            return ['price', 'desc'];
        } elseif ($sort == 'price_asc') {
            return ['price', 'asc'];
        }
        return []; // sort is invalid or empty
    }

    /**
     * @param mixed $size
     *
     * @return string[]
     */
    protected function _sanitizeSize($size)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toStrArr($size))), 0, 20);
    }

    /**
     * @param mixed $color
     *
     * @return string[]
     */
    protected function _sanitizeColor($color)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toStrArr($color))), 0, 20);
    }

    /**
     * @param mixed $target
     *
     * @return string[]
     */
    protected function _sanitizeTarget($target)
    {
        return array_slice(array_unique(
            ProductTargets::instance()->sanitizeTargets(
                ArrayUtils::onlyNonEmpty(Cast::toStrArr($target))
            )
        ), 0, 20);
    }

    /**
     * @param mixed $ageGroup
     *
     * @return string[]
     */
    protected function _sanitizeAgeGroup($ageGroup)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toStrArr($ageGroup))), 0, 20);
    }

    /**
     * @param mixed $brand
     *
     * @return int[]
     */
    protected function _sanitizeBrand($brand)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toUIntArr($brand))), 0, 20);
    }

    /**
     * @param EventProductRecord $selector
     *
     * @throws CException
     */
    protected function _applyOrderBySoldOut(EventProductRecord $selector)
    {
        $selector->orderBy('is_sold_out', 'asc', 'start');
    }

    protected function _applyFilter(EventProductRecord $selector)
    {
        $request = $this->app()->request;
        $sort = $this->_sanitizeSort($request->getParam('sort', []));
        $size = $this->_sanitizeSize($request->getParam('size', []));
        $color = $this->_sanitizeColor($request->getParam('color', []));
        $target = $this->_sanitizeTarget($request->getParam('target', []));
        $brand = $this->_sanitizeBrand($request->getParam('brand', []));
        $ageGroup = $this->_sanitizeAgeGroup($request->getParam('ageGroup', []));

        /* @var EventProductRecord $selector */
        if ($sort !== []) {
            $selector->orderBy($sort[0], $sort[1], 'start');
        }

        if (count($brand) > 0) {
            $selector->brandIds($brand);
        }

        if (count($size) > 0) {
            $selector->sizes($size);
        }

        if (count($color) > 0) {
            $selector->colors($color);
        }

        if (count($target) > 0) {
            $selector->targets($target);
        }

        if (count($ageGroup) > 0) {
            $selector->ageGroups($ageGroup);
        }
    }

    public function actionIndex($id)
    {
        /** @var \MommyCom\Service\SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $this->saveBackUrl(true);
        $model = EventRecord::model()->findByPk($id);
        /* @var $model EventRecord */

        if ($model === null || $model->is_deleted) {
            throw new CHttpException(404, $this->t('This promotion was not found'));
        }

        if ($model->is_virtual) {
            throw new CHttpException(400, $this->t('Sorry, you cannot view this promotion'));
        }

        if (!$model->isStatusPublished) {
            throw new CHttpException(400, $this->t('Sorry, this promotion is temporarily unavailable'));
        }

        $selector = EventProductRecord::model()
            ->eventId($model->id)
            ->splitByMadeInVariant($variant)
            ->orderBy('id', 'asc')
            ->cache(self::DEFAULT_CACHE)
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->findColumnDistinct('product_id');

        $this->description = trim(str_replace(["\n", "\r\n", '"'], "", strip_tags($model->description)));
        $this->pageTitle = $model->name;
        /** @var CClientScript $cs */
        $cs = $this->app()->clientScript;
        $baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.front.actions'));
        /** @var $as CAssetManager */
        $basePath = realpath(Yii::getPathOfAlias('assets.front.actions'));

        if ($model->end_at <= time()) { //ДЛЯ ПРОШЛЫХ АКЦИЙ
            $modifiedAt = @filemtime($basePath . '/eventPast.js');
            $cs->registerScriptFile($baseUrl . "/eventPast.js?v=" . $modifiedAt, CClientScript::POS_END);
            $provider = new CArrayDataProvider($productsIds, [
                'pagination' => [
                    'pageSize' => self::DEFAULT_PAGE_SIZE,
                    'pageVar' => 'page',
                ],
            ]);
            $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(self::DEFAULT_CACHE)->findAll());

            $filterProducts = GroupedProductsFilter::fromEvent($model);

            if (!$this->app()->request->isAjaxRequest) {
                $this->render('past', [
                    'model' => $model,
                    'groupedProducts' => $groupedProducts,
                    'provider' => $provider,
                    'filterProducts' => $filterProducts,
                ]);
            } else {
                $this->renderPartial('past', [
                    'model' => $model,
                    'groupedProducts' => $groupedProducts,
                    'provider' => $provider,
                    'filterProducts' => $filterProducts,
                    'showEvent' => false,
                    'showTimer' => false,
                    'enableItems' => true,
                ]);
            }
        } else { // ДЛЯ АКТИВНЫХ АКЦИЙ
            $modifiedAt = @filemtime($basePath . '/eventIndex.js');
            $cs->registerScriptFile($baseUrl . "/eventIndex.js?v=" . $modifiedAt, CClientScript::POS_END);
            $provider = new CArrayDataProvider($productsIds, [
                'pagination' => [
                    'pageSize' => self::DEFAULT_PAGE_SIZE,
                ],
            ]);

            $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(self::DEFAULT_CACHE)->findAll());

            $filterProducts = GroupedProductsFilter::fromEvent($model);

            if (!$this->app()->request->isAjaxRequest) {
                $this->render('index', [
                    'model' => $model,
                    'groupedProducts' => $groupedProducts,
                    'provider' => $provider,
                    'filterProducts' => $filterProducts,
                ]);
            } else {
                $this->renderPartial('index', [
                    'model' => $model,
                    'groupedProducts' => $groupedProducts,
                    'provider' => $provider,
                    'filterProducts' => $filterProducts,
                    'showEvent' => false,
                    'showTimer' => false,
                    'enableItems' => true,
                ]);
            }
        }
    }

    public function actionCategory($category)
    {
        /** @var \MommyCom\Service\SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $this->saveBackUrl(true);
        $categoryGroups = CategoryGroups::instance();
        /* @var $categoryGroups CategoryGroups */

        if (!$categoryGroups->isValid($category)) {
            throw new CHttpException(404, $this->t('Category not found'));
        }

        $internalCategories = $categoryGroups->getCategories($category);

        $targets = CategoryGroups::instance()->getTarget($category);

        // extract current events
        $activeEventIds = EventRecord::model()
            ->isVirtual(false)
            ->onlyPublished()
            ->onlyTimeActive()
            ->onlyVisible()
            ->cache(self::DEFAULT_CACHE)
            ->findColumnDistinct('id');

        // extract unique products
        $productIdsSelector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->splitByMadeInVariant($variant)
            ->categories($internalCategories);

        /* @var $productIdsSelector EventProductRecord */
        if ($targets !== false) {
            $productIdsSelector->targets($targets);
        }

        $productIds = $productIdsSelector
            ->cache(self::DEFAULT_CACHE)
            ->findColumnDistinct('product_id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->productIdIn($productIds)
            ->cache(self::DEFAULT_CACHE)
            ->with('brand', 'event', 'product');
        /* @var $selector EventProductRecord */

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => self::DEFAULT_PAGE_SIZE,
            ],
        ]);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(self::DEFAULT_CACHE)->findAll());
        $groupedProducts->sortBy('isSoldOut');

        $filterProducts = GroupedProductsFilter::fromEventsId($activeEventIds, $productIds);

        $subCategories = [];
        foreach ($groupedProducts->toArray() as $product) {
            $subCategories[$product->categoryId] = [
                'id' => $product->categoryId,
                'name' => $product->category,
            ];
        }

//        if ($groupedProducts->count == 0) {
//            throw new CHttpException(200, $this->t('There are currently no products that match'));
//        }
        /** @var CClientScript $cs */
        $cs = $this->app()->clientScript;
        $baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.front.actions'));
        $cs->registerScriptFile($baseUrl . "/eventIndex.js", CClientScript::POS_END);

        if (!$this->app()->request->isAjaxRequest) {
            $this->render('category', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'filterProducts' => $filterProducts,
                'category' => $category,
                'targets' => $targets,
                'subCategories' => $subCategories,
            ]);
        } else {
            $this->renderPartial('category', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'filterProducts' => $filterProducts,
                'category' => $category,
                'targets' => $targets,
                'subCategories' => $subCategories,
                'showEvent' => true,
                'showTimer' => true,
                'enableItems' => true,
            ]);
        }
    }

    public function actionAge($age, $target)
    {
        $this->saveBackUrl(true);
        $isAgesSupported = ProductTargets::instance()->getIsSupportsAge($target);
        $ageRange = AgeGroups::instance()->getAgeRange($age);

        /** @var \MommyCom\Service\SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        if (!ProductTargets::instance()->isValid($target)) {
            throw new CHttpException(404, $this->t('Sex is not supported'));
        }

        if ($ageRange === false) {
            throw new CHttpException(400, $this->t('Age not supported'));
        }

        if (!$isAgesSupported) {
            throw new CHttpException(400, $this->t('Age filter is not supported for this gender'));
        }

        // extract current events
        $activeEventIds = EventRecord::model()
            ->isVirtual(false)
            ->onlyPublished()
            ->onlyTimeActive()
            ->onlyVisible()
            ->cache(self::DEFAULT_CACHE)
            ->findColumnDistinct('id');

        // extract unique products
        $productIds = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->splitByMadeInVariant($variant)
            ->ageBetween($ageRange[0], $ageRange[1])
            ->target($target)
            ->cache(self::DEFAULT_CACHE)
            ->findColumnDistinct('product_id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->productIdIn($productIds)
            ->cache(self::DEFAULT_CACHE)
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => self::DEFAULT_PAGE_SIZE,
            ],
        ]);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(self::DEFAULT_CACHE)->findAll());
        $groupedProducts->sortBy('isSoldOut');

        $filterProducts = GroupedProductsFilter::fromEventsId($activeEventIds, $productIds);

//        if ($groupedProducts->count == 0) {
//            throw new CHttpException(200, $this->t('There are currently no products that match'));
//        }

        /** @var CClientScript $cs */
        $cs = $this->app()->clientScript;
        $baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.front.actions'));
        $cs->registerScriptFile($baseUrl . "/eventIndex.js", CClientScript::POS_END);

        if (!$this->app()->request->isAjaxRequest) {
            $this->render('age', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'filterProducts' => $filterProducts,
                'target' => $target,
                'age' => $age,
                'ageRange' => $ageRange,
            ]);
        } else {
            $this->renderPartial('age', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'filterProducts' => $filterProducts,
                'target' => $target,
                'age' => $age,
                'showEvent' => true,
                'showTimer' => true,
                'enableItems' => true,
            ]);
        }
    }

    public function actionBrand($name)
    {
        /** @var \MommyCom\Service\SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $this->saveBackUrl(true);

        /** @var BrandRecord $brand */
        $brand = BrandRecord::model()->name($name)->find();

        if ($brand === null) {
            throw new CHttpException(200, $this->t('Brand not supported'));
        }

        // extract current events
        $eventRecord = EventRecord::model();

        $activeEventIds = $eventRecord
            ->display(self::DEFAULT_CACHE)
            ->splitByMadeInVariant($variant)
            ->findColumnDistinct($eventRecord->getTableAlias() . '.id');

        // extract unique products
        $productIds = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->brandId($brand->id)
            ->splitByMadeInVariant($variant)
            ->cache(self::DEFAULT_CACHE)
            ->findColumnDistinct('product_id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->productIdIn($productIds)
            ->cache(self::DEFAULT_CACHE)
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => self::DEFAULT_PAGE_SIZE,
            ],
        ]);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(self::DEFAULT_CACHE)->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $activeEventIds = (array)$activeEventIds;
        $filterProducts = GroupedProductsFilter::fromEventsId($activeEventIds, $productIds);

//        if ($groupedProducts->count == 0) {
//            throw new CHttpException(200, $this->t('There are currently no products that match'));
//        }

        /** @var CClientScript $cs */
        $cs = $this->app()->clientScript;
        $baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.front.actions'));
        $cs->registerScriptFile($baseUrl . "/eventIndex.js", CClientScript::POS_END);

        if (!$this->app()->request->isAjaxRequest) {
            $this->render('brand', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'filterProducts' => $filterProducts,
                'brand' => $brand,

            ]);
        } else {
            $this->renderPartial('brand', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'filterProducts' => $filterProducts,
                'brand' => $brand,
                'showEvent' => true,
                'showTimer' => true,
                'enableItems' => true,
            ]);
        }
    }

    public function actionFinalSell()
    {
        throw new CHttpException(200, $this->t('The promotion is currently not available'));

        $this->saveBackUrl(true);

        // extract current events
        $activeEventIds = EventRecord::model()
            ->isVirtual(false)
            ->onlyPublished()
            ->onlyTimeActive()
            ->onlyVisible()
            ->isStock(true)
            ->cache(self::DEFAULT_CACHE)
            ->findColumnDistinct('id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->cache(self::DEFAULT_CACHE)
            ->with('brand', 'event', 'product');
        /* @var $selector EventProductRecord */

        $this->_applyFilter($selector);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $provider = new CArrayDataProvider(
            $groupedProducts->toArray(),
            [
                'pagination' => false,
            ]
        );

//        if ($groupedProducts->count == 0) {
//            throw new CHttpException(200, $this->t('There are currently no products that match'));
//        }

        /** @var CClientScript $cs */
        $cs = $this->app()->clientScript;
        $baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.front.actions'));
        $cs->registerScriptFile($baseUrl . "/eventIndex.js", CClientScript::POS_END);

        if (!$this->app()->request->isAjaxRequest) {
            $this->render('finalSell', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
            ]);
        } else {
            $this->renderPartial('_products', [
                'groupedProducts' => $groupedProducts,
                'provider' => $provider,
                'showEvent' => true,
                'showTimer' => true,
                'enableItems' => true,
            ]);
        }
    }

    public function actionFast()
    {
        throw new CHttpException(200, $this->t('The promotion is currently not available'));

        $event = EventRecord::model()
            ->isVirtual(true)
            ->onlyTimeActive()
            ->onlyVisible()
            ->orderBy('id', 'asc')
            ->find();

        $futureEvent = EventRecord::model()
            ->isVirtual(true)
            ->onlyTimeFuture()
            ->orderBy('id', 'asc')
            ->find();

        $eventId = isset($event->id) ? $event->id : 0;
        $futureEventId = isset($futureEvent->id) ? $futureEvent->id : 0;

        $productsSelector = EventProductRecord::model()->eventId($eventId);
        $futureProductsSelector = EventProductRecord::model()->eventId($futureEventId);

        $groupedProducts = GroupedProducts::fromProducts($productsSelector->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $provider = new CArrayDataProvider(
            $groupedProducts->toArray(),
            [
                'pagination' => false,
            ]
        );

//        if ($groupedProducts->count == 0) {
//            throw new CHttpException(200, $this->t('There are currently no products that match'));
//        }

        $groupedProducts = GroupedProducts::fromProducts($futureProductsSelector->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $futureProvider = new CArrayDataProvider(
            $groupedProducts->toArray(),
            [
                'pagination' => false,
            ]
        );
        /** @var CClientScript $cs */
        $cs = $this->app()->clientScript;
        $baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.front.actions'));
        $cs->registerScriptFile($baseUrl . "/eventIndex.js", CClientScript::POS_END);

        if (!$this->app()->request->isAjaxRequest) {
            $this->render('fast', [
                'event' => $event,
                'futureEvent' => $futureEvent,
                'provider' => $provider,
                'futureProvider' => $futureProvider,
            ]);
        } else {
            $this->renderPartial('fast', [ // all is correct
                'event' => $event,
                'futureEvent' => $futureEvent,
                'provider' => $provider,
                'futureProvider' => $futureProvider,
            ]);
        }
    }

}
