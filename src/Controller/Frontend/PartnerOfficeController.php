<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CArrayDataProvider;
use CHttpException;
use CSort;
use DateInterval;
use DatePeriod;
use DateTime;
use MommyCom\Model\Db\FeedbackRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserPartnerAdmissionRecord;
use MommyCom\Model\Db\UserPartnerBalanceRecord;
use MommyCom\Model\Db\UserPartnerInviteRecord;
use MommyCom\Model\Db\UserPartnerOutputRecord;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Form\FeedbackForm;
use MommyCom\Model\Form\GeneratorUrlForm;
use MommyCom\Model\Form\PrivatOutputForm;
use MommyCom\Model\Statistic\UserPartnerOrderModel;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Frontend\ReplyResult;
use MommyCom\YiiComponent\Type\Cast;

class PartnerOfficeController extends FrontController
{
    const REPORT_CACHE = 0;

    /**
     * @var string
     */
    public $defaultAction = 'report';

    /**
     * @var array
     */
    public $menu = [];

    public function init()
    {
        $this->menu = [
            ['label' => $this->t('Summary of cases'), 'url' => ['partnerOffice/report']],
            ['label' => $this->t('Finance'), 'url' => ['partnerOffice/finance'],
                'items' => [
                    ['label' => '', 'url' => ['partnerOffice/return']],
                    ['label' => '', 'url' => ['partnerOffice/output']],
                ]],
            ['label' => $this->t('Promotional materials'), 'url' => ['partnerOffice/promo']],
            ['label' => $this->t('Statistics'), 'url' => ['partnerOffice/statistic']],
            ['label' => $this->t('Help'), 'url' => ['partnerOffice/help']],
            ['label' => $this->t('Return to My Account'), 'url' => ['account/orders']],
        ];
        parent::init();
    }

    /**
     * @property-description Сводка дел
     */
    public function actionReport()
    {
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            throw new CHttpException('400', $this->t('Access is allowed only to program partners'));
        }
        $timeNow = mktime(date('H'), date('i'), 0, date('m'), date('d'), date('Y'));
        $startTodayDay = mktime(0, 0, 0, date('m', $timeNow), date('d', $timeNow), date('Y', $timeNow));

        //заработано
        $statusPotentialBalance = [
            UserPartnerAdmissionRecord::STATUS_NEW,
            UserPartnerAdmissionRecord::STATUS_PAID_AND_WAIT,
            UserPartnerAdmissionRecord::STATUS_BALANCE,
        ];
        $countAmountOrderToday = Cast::toInt(UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            //->statusIn($statusPotentialBalance)
            ->createdAtGreater($startTodayDay)
            ->createdAtLower($timeNow)
            ->findColumnSum('amount'));
        $countAmountOrderYesterday = Cast::toInt(UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->statusIn($statusPotentialBalance)
            ->createdAtGreater(strtotime('- 1 day', $startTodayDay))
            ->createdAtLower($startTodayDay)
            ->findColumnSum('amount'));
        $countAmountOrderSevenDay = Cast::toInt(UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->statusIn($statusPotentialBalance)
            ->createdAtGreater(strtotime('- 7 day', $startTodayDay))
            ->createdAtLower($timeNow)
            ->findColumnSum('amount'));
        $countAmountOrderThirtyDay = Cast::toInt(UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->statusIn($statusPotentialBalance)
            ->createdAtGreater(strtotime('- 30 day', $startTodayDay))
            ->createdAtLower($timeNow)
            ->findColumnSum('amount'));

        //регистрации
        $distinctUsers = UserPartnerInviteRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater($startTodayDay)
            ->createdAtLower($timeNow)
            ->findColumnDistinct('user_id');
        $countInvitedUserToday = $distinctUsers === false ? 0 : count($distinctUsers);

        $distinctUsers = UserPartnerInviteRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater(strtotime('- 1 day', $startTodayDay))
            ->createdAtLower($startTodayDay)
            ->findColumnDistinct('user_id');
        $countInvitedUserYesterday = $distinctUsers === false ? 0 : count($distinctUsers);

        $distinctUsers = UserPartnerInviteRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater(strtotime('- 7 day', $startTodayDay))
            ->createdAtLower($timeNow)
            ->findColumnDistinct('user_id');
        $countInvitedUserSevenDay = $distinctUsers === false ? 0 : count($distinctUsers);

        $distinctUsers = UserPartnerInviteRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater(strtotime('- 30 day', $startTodayDay))
            ->createdAtLower($timeNow)
            ->findColumnDistinct('user_id');
        $countInvitedUserThirtyDay = $distinctUsers === false ? 0 : count($distinctUsers);

        //заказы
        $countOrderToday = UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater($startTodayDay)
            ->createdAtLower($timeNow)
            ->count();
        $countOrderYesterday = UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater(strtotime('- 1 day', $startTodayDay))
            ->createdAtLower($startTodayDay)
            ->count();
        $countOrderSevenDay = UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater(strtotime('- 7 day', $startTodayDay))
            ->createdAtLower($timeNow)
            ->count();
        $countOrderThirtyDay = UserPartnerAdmissionRecord::model()
            ->cache(self::REPORT_CACHE)
            ->partnerId($partner->id)
            ->createdAtGreater(strtotime('- 30 day', $startTodayDay))
            ->createdAtLower($timeNow)
            ->count();

        $this->render('report', [
            'partner' => $partner,
            'reportCounts' => [
                'invited' => [
                    'today' => $countInvitedUserToday,
                    'yesterday' => $countInvitedUserYesterday,
                    'seven' => $countInvitedUserSevenDay,
                    'thirty' => $countInvitedUserThirtyDay,
                ],
                'orders' => [
                    'today' => $countOrderToday,
                    'yesterday' => $countOrderYesterday,
                    'seven' => $countOrderSevenDay,
                    'thirty' => $countOrderThirtyDay,
                ],
                'amountOrders' => [
                    'today' => $countAmountOrderToday,
                    'yesterday' => $countAmountOrderYesterday,
                    'seven' => $countAmountOrderSevenDay,
                    'thirty' => $countAmountOrderThirtyDay,
                ],
            ],
        ]);
    }

    /**
     * @property-description Рекламные материалы
     */
    public function actionPromo()
    {
        $app = $this->app();
        /** @var ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            throw new CHttpException('400', $this->t('Access is allowed only to program partners'));
        }
        $partnerPromoCodes = PromocodeRecord::model()->partnerId($partner->id)->orderBy('cash')->findAll();
        if (empty($partnerPromoCodes)) {
            //скидка 10 грн на заказ свыше 150
            $partnerPromocode10 = new PromocodeRecord();
            $partnerPromocode10->partner_id = $partner->id;
            $partnerPromocode10->type = PromocodeRecord::TYPE_UNLIMITED;
            $partnerPromocode10->reason = PromocodeRecord::REASON_PARTNER;
            $partnerPromocode10->cash = 10;
            $partnerPromocode10->valid_until = strtotime('+ 1 year');
            $partnerPromocode10->reason_description = $this->t('Affiliate discount');
            $partnerPromocode10->order_price_after = 150;
            $partnerPromocode10->save();

            //скидка 30 грн на заказ свыше 250
            $partnerPromocode30 = new PromocodeRecord();
            $partnerPromocode30->partner_id = $partner->id;
            $partnerPromocode30->type = PromocodeRecord::TYPE_UNLIMITED;
            $partnerPromocode30->reason = PromocodeRecord::REASON_PARTNER;
            $partnerPromocode30->cash = 30;
            $partnerPromocode30->valid_until = strtotime('+ 1 year');
            $partnerPromocode30->reason_description = $this->t('Affiliate discount');
            $partnerPromocode30->order_price_after = 250;
            $partnerPromocode30->save();

            //скидка 50 грн на заказ свыше 350
            $partnerPromocode50 = new PromocodeRecord();
            $partnerPromocode50->partner_id = $partner->id;
            $partnerPromocode50->type = PromocodeRecord::TYPE_UNLIMITED;
            $partnerPromocode50->reason = PromocodeRecord::REASON_PARTNER;
            $partnerPromocode50->cash = 50;
            $partnerPromocode50->valid_until = strtotime('+ 1 year');
            $partnerPromocode50->reason_description = $this->t('Affiliate discount');
            $partnerPromocode50->order_price_after = 350;
            $partnerPromocode50->save();

            //скидка 75 грн на заказ свыше 500
            $partnerPromocode75 = new PromocodeRecord();
            $partnerPromocode75->partner_id = $partner->id;
            $partnerPromocode75->type = PromocodeRecord::TYPE_UNLIMITED;
            $partnerPromocode75->reason = PromocodeRecord::REASON_PARTNER;
            $partnerPromocode75->cash = 75;
            $partnerPromocode75->valid_until = strtotime('+ 1 year');
            $partnerPromocode75->reason_description = $this->t('Affiliate discount');
            $partnerPromocode75->order_price_after = 500;
            $partnerPromocode75->save();

            $partnerPromoCodes = [$partnerPromocode10, $partnerPromocode30, $partnerPromocode50, $partnerPromocode75];
        }

        $this->render('promo', [
            'partner' => $partner,
            'partnerPromoCodes' => $partnerPromoCodes,
        ]);
    }

    /**
     * @property-description Генератор ссылки-приглашения от партнера
     *
     * @param $partnerId
     */
    public function actionGenerateUrl($partnerId)
    {
        $request = $this->app()->request;
        if (!$request->isAjaxRequest) {
            $this->redirect($this->createUrl('index/index'));
        }
        $response = new ReplyResult();
        $generatorForm = new GeneratorUrlForm();
        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($generatorForm);
            $this->app()->end();
        }

        $dataAuthForm = $request->getPost('GeneratorUrlForm');
        if (!empty($dataAuthForm)) {
            $generatorForm->setAttributes($dataAuthForm);
            if ($generatorForm->validate()) {
                $generatedLink = GeneratorUrlForm::getPersonalInviteUrl($partnerId, $generatorForm->url);
                $response->setSuccess(true);
                $response->add('generatedLink', $generatedLink);
            }
        }
        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }
    }

    /**
     * @property-description Статистика
     * @throws CHttpException
     */
    public function actionStatistic()
    {
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            throw new CHttpException('400', $this->t('Access is allowed only to program partners'));
        }
        $partnerId = $partner->id;
        $fromDateForm = $this->app()->request->getParam('fromDate', '');
        $toDateForm = $this->app()->request->getParam('toDate', '');

        if ($fromDateForm !== '' && $toDateForm !== '') {
            $fromDate = strtotime($fromDateForm);
            $toDate = strtotime($toDateForm);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 7 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
        }

        $interval = new DateInterval('P1D');
        $dataTimeStart = new DateTime();
        $dataTimeEnd = new DateTime();
        $dataTimeStart->setTimestamp($fromDate);
        $dataTimeEnd->setTimestamp($toDate);

        $dateRange = new DatePeriod($dataTimeStart, $interval, $dataTimeEnd);
        $items = [];
        foreach ($dateRange as $dateTimeInterval) {
            /** @var DateTime $dateTimeInterval */
            $dateTimeIntervalEnd = clone $dateTimeInterval;
            $dateTimeIntervalEnd = $dateTimeIntervalEnd->add($interval);

            $startInterval = $dateTimeInterval->getTimestamp();
            $endInterval = $dateTimeIntervalEnd->getTimestamp();

            $itemReport = new UserPartnerOrderModel();
            $from = new DateTime();
            $to = new DateTime();
            $from->setTimestamp($startInterval);
            $to->setTimestamp($endInterval);
            $itemReport->startTime = $startInterval;
            $itemReport->endTime = $endInterval;

            //Кол-во регистраций приглашенных пользователей
            $modelPartnerInvitedUser = UserPartnerInviteRecord::model()
                ->createdAtGreater($startInterval)
                ->createdAtLower($endInterval);
            if ($partnerId > 0) {
                $modelPartnerInvitedUser->partnerId($partnerId);
            }
            $distinctUsers = $modelPartnerInvitedUser->findColumnDistinct('user_id');

            $itemReport->countRegPartnerInvitedUser = $distinctUsers === false ? 0 : count($distinctUsers);

            //Использовано промокодов
            $modelPartnerInvitedUser = UserPartnerInviteRecord::model()
                ->createdAtGreater($startInterval)
                ->createdAtLower($endInterval)
                ->typeIncoming(UserPartnerInviteRecord::TYPE_INCOMING_PROMOCODE);
            if ($partnerId > 0) {
                $modelPartnerInvitedUser->partnerId($partnerId);
            }

            $itemReport->countUsedPromocode = $modelPartnerInvitedUser->count();

            //Кол-во заказов, Сумма заказов, Кол-во товаров, Кол-во выплат, Сумма выплат
            $itemReport->countOrder = 0;
            $itemReport->sumOrder = 0;
            $itemReport->countProduct = 0;
            $itemReport->countOutput = 0;
            $itemReport->sumOutput = 0;
            $itemReport->countConfirmedOutput = 0;
            $itemReport->sumConfirmedOutput = 0;
            $modelAdmission = UserPartnerAdmissionRecord::model();

            $modelAdmission->createdAtGreater($startInterval);
            $modelAdmission->createdAtLower($endInterval);
            if ($partnerId > 0) {
                $modelAdmission->partnerId($partnerId);
            }
            $admissions = $modelAdmission->findAll();

            foreach ($admissions as $admission) {
                /** @var $admission UserPartnerAdmissionRecord */
                $itemReport->countOrder++;
                $itemReport->sumOrder += $admission->sum_order;
                $itemReport->countProduct += $admission->count_product_order;
                //if ($admission->getIsOutputBalance()) {
                $itemReport->countOutput++;
                $itemReport->sumOutput += $admission->amount;
                //}
                if ($admission->status == UserPartnerAdmissionRecord::STATUS_BALANCE) {
                    $itemReport->countConfirmedOutput++;
                    $itemReport->sumConfirmedOutput += $admission->amount;
                }
            }

            $items[] = $itemReport;
        }
        $dataProviderReport = new CArrayDataProvider($items, [
            'keyField' => 'startTime',
            'pagination' => false,
        ]);
        $this->render('statistic', [
            'partner' => $partner,
            'dataProviderReport' => $dataProviderReport,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ]);
    }

    /**
     * @property-description Финансы: история зачислений
     * @throws CHttpException
     */
    public function actionFinance()
    {
        $app = $this->app();
        /** @var ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            throw new CHttpException('400', $this->t('Access is allowed only to program partners'));
        }

        $fromDateForm = $this->app()->request->getParam('fromDate', '');
        $toDateForm = $this->app()->request->getParam('toDate', '');

        if ($fromDateForm !== '' && $toDateForm !== '') {
            $fromDate = strtotime($fromDateForm);
            $toDate = strtotime($toDateForm);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 7 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
        }

        $interval = new DateInterval('P1D');
        $dataTimeStart = new DateTime();
        $dataTimeEnd = new DateTime();
        $dataTimeStart->setTimestamp($fromDate);
        $dataTimeEnd->setTimestamp($toDate);

        $dateRange = new DatePeriod($dataTimeStart, $interval, $dataTimeEnd);
        $items = [];
        foreach ($dateRange as $dateTimeInterval) {
            /** @var DateTime $dateTimeInterval */
            $dateTimeIntervalEnd = clone $dateTimeInterval;
            $dateTimeIntervalEnd = $dateTimeIntervalEnd->add($interval);

            $startInterval = $dateTimeInterval->getTimestamp();
            $endInterval = $dateTimeIntervalEnd->getTimestamp();

            $itemReport = new UserPartnerOrderModel();
            $from = new DateTime();
            $to = new DateTime();
            $from->setTimestamp($startInterval);
            $to->setTimestamp($endInterval);
            $itemReport->startTime = $startInterval;
            $itemReport->endTime = $endInterval;

            //Кол-во заказов, Сумма заказов, Кол-во товаров, Кол-во выплат, Сумма выплат
            $itemReport->countOrder = 0;
            $itemReport->sumOrder = 0;
            $itemReport->countProduct = 0;
            $itemReport->countOutput = 0;
            $itemReport->sumOutput = 0;
            $modelAdmission = UserPartnerAdmissionRecord::model();

            $modelAdmission->createdAtGreater($startInterval);
            $modelAdmission->createdAtLower($endInterval);
            $modelAdmission->partnerId($partner->id);
            $admissions = $modelAdmission->findAll();

            foreach ($admissions as $admission) {
                /** @var $admission UserPartnerAdmissionRecord */
                $itemReport->countOrder++;//*
                $itemReport->sumOrder += $admission->sum_order;
                $itemReport->countProduct += $admission->count_product_order;
                //if ($admission->getIsOutputBalance()) {
                $itemReport->countOutput++;
                $itemReport->sumOutput += $admission->amount;//*
                //}
            }
            $itemReport->admissions = $admissions;
            //if ($itemReport->countOrder > 0 || !empty($items))
            $items[] = $itemReport;
        }
        $dataProviderReport = new CArrayDataProvider($items, [
            'keyField' => 'startTime',
            'pagination' => false,
        ]);

        $this->render('finance', [
            'partner' => $partner,
            'dataProviderReport' => $dataProviderReport,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ]);
    }

    /**
     * @property-description Финансы: история заявок
     * @throws CHttpException
     */
    public function actionOutput()
    {
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            throw new CHttpException('400', $this->t('Access is allowed only to program partners'));
        }

        $fromDateForm = $this->app()->request->getParam('fromDate', '');
        $toDateForm = $this->app()->request->getParam('toDate', '');

        if ($fromDateForm !== '' && $toDateForm !== '') {
            $fromDate = strtotime($fromDateForm);
            $toDate = strtotime($toDateForm);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 7 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
        }

        $modelOutput = UserPartnerOutputRecord::model();

        $modelOutput->createdAtGreater($fromDate);
        $modelOutput->createdAtLower($toDate);
        $modelOutput->partnerId($partner->id);

        $dataProvider = $modelOutput->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('output', [
            'partner' => $partner,
            'dataProvider' => $dataProvider,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ]);
    }

    /**
     * @property-description Заявка вывода на баланс Мамам
     */
    public function actionOutputMamam()
    {
        $request = $this->app()->request;
        if (!$request->isAjaxRequest) {
            $this->redirect($this->createUrl('index/index'));
        }
        $response = new ReplyResult();
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            if ($request->isAjaxRequest) {
                $this->renderJson($response->toArray());
            }
        }
        $amount = abs(Cast::toInt($this->app()->request->getParam('amount', 0)));
        if ($amount > 0) {
            if ($amount <= $partner->getBalance()) {
                $requestOutput = new UserPartnerOutputRecord();
                $requestOutput->partner_id = $partner->id;
                $requestOutput->amount = $amount;
                $requestOutput->type_output = UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM;
                $requestOutput->status = UserPartnerOutputRecord::STATUS_WAIT;
                if ($requestOutput->save()) {
                    $balance = new UserPartnerBalanceRecord();
                    $balance->partner_id = $partner->id;
                    $balance->amount = -$amount;
                    $balance->type = UserPartnerBalanceRecord::TYPE_MAMAM_MINUS;
                    $balance->description = $this->t('Application for payment to mommy account');
                    if (!$balance->save()) {
                        $response->addError($this->t('There was an error in the withdrawal of funds to your balance'));
                    } else {
                        $response->setSuccess(true);
                        if ($amount <= UserPartnerOutputRecord::MAX_AMOUNT_OUTPUT_MAMAM) { //автоматический вывод средств
                            $canChangeStatus = false;
                            $sumPoints = ceil($requestOutput->amount * UserPartnerOutputRecord::PERCENT_OUTPUT_MAMAM / 100); // с надбавкой за вывод средств на счет Мамам
                            $bonusPoint = new UserBonuspointsRecord();
                            $bonusPoint->user_id = $requestOutput->partner->user_id;
                            $bonusPoint->points = $requestOutput->amount + $sumPoints;
                            $bonusPoint->message = $this->t('Automatic transfer from the partner account from application № {number} for the withdrawal of funds', ['{number}' => $requestOutput->id]);
                            $bonusPoint->custom_message = $bonusPoint->message;
                            $bonusPoint->is_custom = false;
                            $bonusPoint->type = UserBonuspointsRecord::TYPE_INNER;
                            if ($bonusPoint->save()) {
                                $canChangeStatus = true;
                            } else {
                                $response->addError($this->t('Error in calculating bonuses on the partner\'s personal account'));
                            }
                            if ($canChangeStatus) {
                                $requestOutput->status = UserPartnerOutputRecord::STATUS_DONE;
                                if ($requestOutput->save()) {
                                    $response->setSuccess(true);
                                    $balance->description = $this->t('Автоматическая заявка на выплату на бонусный счет Мамам');
                                    $balance->save(false, ['description']);
                                } else {
                                    $response->addError($this->t('Error saving data'));
                                }
                            }
                        }
                    }
                }
            } else {
                $response->addError($this->t('The sum of the output exceeds the allowable value'));
            }
        } else {
            $response->addError($this->t('Invalid amount'));
        }

        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }
    }

    /**
     * @property-description Валидация суммы вывода на счет Приват
     */
    public function actionValidOutputPrivat()
    {
        $request = $this->app()->request;
        if (!$request->isAjaxRequest) {
            $this->redirect($this->createUrl('index/index'));
        }
        $response = new ReplyResult();
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            if ($request->isAjaxRequest) {
                $this->renderJson($response->toArray());
            }
        }

        $amount = abs(Cast::toInt($this->app()->request->getParam('amount', 0)));
        if ($amount > 0) {
            if ($amount >= UserPartnerOutputRecord::MIN_AMOUNT_OUTPUT_PRIVAT) {
                if ($amount <= $partner->getBalance()) {
                    $response->setSuccess(true);
                } else {
                    $response->addError($this->t('The sum of the output exceeds the allowable value {sum} USD'), ['{sum}' => $partner->getBalance()]);
                }
            } else {
                $response->addError($this->t('The minimum amount for withdrawal is {sum}$', ['{sum}' => UserPartnerOutputRecord::MIN_AMOUNT_OUTPUT_PRIVAT]));
            }
        } else {
            $response->addError($this->t('Invalid amount'));
        }

        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }
    }

    /**
     * @property-description Заявка вывода на баланс Privat
     */
    public function actionOutputPrivat()
    {
        $request = $this->app()->request;
        if (!$request->isAjaxRequest) {
            $this->redirect($this->createUrl('index/index'));
        }
        $response = new ReplyResult();
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            if ($request->isAjaxRequest) {
                $this->renderJson($response->toArray());
            }
        }
        $privatOutputForm = new PrivatOutputForm();
        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($privatOutputForm);
            $this->app()->end();
        }

        $dataForm = $request->getPost('PrivatOutputForm');
        if (!empty($dataForm)) {
            $privatOutputForm->setAttributes($dataForm);
            if ($privatOutputForm->validate()) {
                $amount = abs(Cast::toInt($privatOutputForm->amount));
                if ($amount > 0) {
                    if ($amount >= UserPartnerOutputRecord::MIN_AMOUNT_OUTPUT_PRIVAT) {
                        if ($amount <= $partner->getBalance()) {
                            $requestOutput = new UserPartnerOutputRecord();
                            $requestOutput->partner_id = $partner->id;
                            $requestOutput->amount = $amount;
                            $requestOutput->type_output = UserPartnerOutputRecord::TYPE_OUTPUT_PRIVAT;
                            $requestOutput->status = UserPartnerOutputRecord::STATUS_WAIT;
                            $requestOutput->name_card = $privatOutputForm->nameCard;
                            $requestOutput->surname_card = $privatOutputForm->surnameCard;
                            $requestOutput->number_card = $privatOutputForm->cardNumber;
                            if ($requestOutput->save()) {
                                $balance = new UserPartnerBalanceRecord();
                                $balance->partner_id = $partner->id;
                                $balance->amount = -$amount;
                                $balance->type = UserPartnerBalanceRecord::TYPE_PRIVAT_MINUS;
                                $balance->description = $this->t('Application for payment to Privat account');
                                if (!$balance->save()) {
                                    $response->addError(print_r($balance->getErrors(), true));
                                } else {
                                    $response->setSuccess(true);
                                }
                            }
                        } else {
                            $response->addError($this->t('The sum of the output exceeds the allowable value {sum} USD'), ['{sum}' => $partner->getBalance()]);
                        }
                    } else {
                        $response->addError($this->t('The minimum amount for withdrawal is {sum}$', ['{sum}' => UserPartnerOutputRecord::MIN_AMOUNT_OUTPUT_PRIVAT]));
                    }
                } else {
                    $response->addError($this->t('Invalid amount'));
                }
            }
        }

        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }
    }

    /**
     * @property-description Финансы: возвраты
     * @throws CHttpException
     */
    public function actionReturn()
    {
        $app = $this->app();
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $user */
        $user = $app->user;
        $partner = UserPartnerRecord::model()->userId($user->id)->find();
        if ($partner === null) {
            throw new CHttpException('400', $this->t('Access is allowed only to program partners'));
        }

        $fromDateForm = $this->app()->request->getParam('fromDate', '');
        $toDateForm = $this->app()->request->getParam('toDate', '');

        if ($fromDateForm !== '' && $toDateForm !== '') {
            $fromDate = strtotime($fromDateForm);
            $toDate = strtotime($toDateForm);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 7 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
        }

        $modelOutput = UserPartnerBalanceRecord::model();
        $modelOutput->type(UserPartnerBalanceRecord::TYPE_RETURN_MINUS);
        $modelOutput->createdAtGreater($fromDate);
        $modelOutput->createdAtLower($toDate);
        $modelOutput->partnerId($partner->id);

        $dataProvider = $modelOutput->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('return', [
            'partner' => $partner,
            'dataProvider' => $dataProvider,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
        ]);
    }

    /**
     * @property-description Помощь
     */
    public function actionHelp()
    {
        $model = new FeedbackForm();
        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        $formData = $this->app()->request->getPost('FeedbackForm');
        $model->setAttributes($formData);

        if (!empty($formData)) {
            if ($model->validate()) {
                $feedback = new FeedbackRecord();
                $feedback->user_id = $this->app()->user->id;
                $feedback->message = $model->message;
                $feedback->status = FeedbackRecord::STATUS_NEW;
                $feedback->type = FeedbackRecord::TYPE_HELP_PARTNER;
                if ($feedback->save()) {
                    $this->app()->user->setFlash('message', $this->t('Your question has been submitted.'));
                } else {
                    $this->app()->user->setFlash('message', $this->t('Error. Your question has not been submitted. Contact the site hotline'));
                }
                $model->message = '';
            }
        }
        $this->render('help', [
            'feedbackForm' => $model,
        ]);
    }

}
