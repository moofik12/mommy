<?php

namespace MommyCom\Controller\Frontend;

use CException;
use CHttpException;
use GuzzleHttp\Psr7\ServerRequest;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\RenderPsr7ResponseTrait;
use MommyCom\Service\PaymentGateway\AvailablePaymentGateways;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\TokenManager;

class PayController extends FrontController
{
    use RenderPsr7ResponseTrait;

    /**
     * @return array
     */
    public function accessRules()
    {
        $rules = [
            [
                'allow',
                'actions' => ['receive'],
                'users' => ['*'],
            ],
        ];

        return array_merge($rules, parent::accessRules());
    }

    /**
     * Выбор провайдера для оплаты
     *
     * @param string $uid
     *
     * @throws CHttpException
     */
    public function actionIndex($uid)
    {
        $orders = OrderRecord::model()->findByUid(trim($uid));
        $this->validateOrders($orders);

        $ordersPrice = 0;
        $deliveryPrice = 0;
        $bonusesAmount = 0;
        $discountAmount = 0;
        $countProductsNumber = 0;

        foreach ($orders as $calcOrder) {
            $countProductsNumber += $calcOrder->getPositionsNumberAvailableForPay();
            $ordersPrice += $calcOrder->getPayPrice();
            $deliveryPrice += $calcOrder->getDeliveryPrice();
            $bonusesAmount += $calcOrder->bonuses;
            $discountAmount += $calcOrder->getCountDiscount();
        }

        /** @var AvailablePaymentGateways $availablePaymentGateways */
        $availablePaymentGateways = $this->container->get(AvailablePaymentGateways::class);

        $this->render('allProvider', [
            'uid' => trim($uid),
            'orders' => $orders,
            'amount' => $ordersPrice + $deliveryPrice,
            'deliveryPrice' => $deliveryPrice,
            'bonusesAmount' => $bonusesAmount,
            'discountAmount' => $discountAmount,
            'countProductsNumber' => $countProductsNumber,
            'availableGateways' => $availablePaymentGateways->getPaymentGateways(),
        ]);
    }

    /**
     * Страница с результатами выполнения платежа
     *
     * @param string $uid
     * @param string $token
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionResult($uid, $token)
    {
        /** @var TokenManager $tokenManager */
        $tokenManager = $this->container->get(TokenManager::class);

        if (!$tokenManager->isValid('resultAction', $token)) {
            $this->redirect(['order/index']);
        }

        $orderFirst = current(OrderRecord::model()->findByUid(trim($uid)));
        if (!$orderFirst instanceof OrderRecord) {
            throw new CHttpException(404, $this->t('No order for payment found'));
        }

        $status = (int)$orderFirst->getPayPrice()
            ? $this->t('Awaiting payment confirmation')
            : $this->t('Payment successful');

        $this->render('result', ['status' => $status]);
    }

    /**
     * Обработка ответов от платежных сервисов
     *
     * @param string $provider
     */
    public function actionReceive($provider)
    {
        /** @var AvailablePaymentGateways $availablePaymentGateways */
        $availablePaymentGateways = $this->container->get(AvailablePaymentGateways::class);
        $paymentGateway = $availablePaymentGateways->getPaymentGateway($provider);

        $response = $paymentGateway->handle(ServerRequest::fromGlobals());

        $this->renderPsr7Response($response);
    }

    /**
     * @param OrderRecord[] $orders
     *
     * @throws CHttpException
     */
    private function validateOrders(array $orders)
    {
        if (!count($orders)) {
            throw new CHttpException(404, $this->t('No order for payment found'));
        }

        $userId = $this->app()->user->id;

        foreach ($orders as $order) {
            if ($order->user_id !== $userId) {
                $errorMessage = $this->t('Order № {number} was made by another user', ['{number}' => $order->id]);
                throw new CHttpException(400, $errorMessage);
            }

            if (!$order->isAvailableForPayment()) {
                $errorMessage = $this->t('Order number {number} is not available for payment', ['{number}' => $order->id]);
                throw new CHttpException(400, $errorMessage);
            }

            if ($order->isProcessingActive()) {
                $errorMessage = $this->t('Your order № {number} is currently being processed by our manager. Please try again later.', ['{number}' => $order->id]);
                throw new CHttpException(400, $errorMessage);
            }
        }
    }
} 
