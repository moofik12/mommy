<?php

namespace MommyCom\Controller\Frontend;

use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\Monitoring\EventsCountMetrics;
use MommyCom\Service\Monitoring\EventsCountMobileMetrics;
use MommyCom\Service\Monitoring\LastOrderDateMetrics;
use MommyCom\Service\Monitoring\LastPaidOrderDateMetrics;
use MommyCom\Service\Monitoring\LastRegistrationDateMetrics;
use MommyCom\Service\Monitoring\MetricsInterface;
use MommyCom\Service\Monitoring\OrdersCountMetrics;
use MommyCom\Service\Monitoring\PaidOrdersCountMetrics;
use MommyCom\Service\Monitoring\PhoneExistsMetrics;
use MommyCom\Service\Monitoring\PhoneExistsMobileMetrics;
use MommyCom\Service\Monitoring\ProductsExistsMetrics;
use MommyCom\Service\Monitoring\RegistrationCountMetrics;

class MonitoringController extends Controller
{
    private const METRICS_MAP = [
        'events_count' => EventsCountMetrics::class,
        'events_count_mobile' => EventsCountMobileMetrics::class,
        'registration_count' => RegistrationCountMetrics::class,
        'last_registration_date' => LastRegistrationDateMetrics::class,
        'products_exists' => ProductsExistsMetrics::class,
        'phone_exists' => PhoneExistsMetrics::class,
        'phone_exists_mobile' => PhoneExistsMobileMetrics::class,
        'last_order_date' => LastOrderDateMetrics::class,
        'orders_count' => OrdersCountMetrics::class,
        'last_paid_order_date' => LastPaidOrderDateMetrics::class,
        'paid_orders_count' => PaidOrdersCountMetrics::class,
    ];

    /**
     * @param string $metrics
     */
    public function actionIndex(string $metrics)
    {
        if (!array_key_exists($metrics, self::METRICS_MAP)) {
            $this->renderJson([
                'success' => false,
                'message' => 'Bad metrics name',
            ]);
        }

        /** @var MetricsInterface $metricsService */
        $metricsService = $this->container->get(self::METRICS_MAP[$metrics]);

        $this->renderJson([
            'success' => true,
            'data' => [
                'value' => $metricsService->getValue(),
            ],
        ]);
    }
}
