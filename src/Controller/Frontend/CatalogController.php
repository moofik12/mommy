<?php

namespace MommyCom\Controller\Frontend;

use CArrayDataProvider;
use CHttpRequest;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\ProductFilter\ProductsFilter;
use MommyCom\Model\ProductFilter\SortFilter;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;

class CatalogController extends FrontController
{

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    const DEFAULT_CACHE = 80;
    const DEFAULT_PAGE_SIZE = 48;

    public function actionIndex()
    {
        $selector = EventProductRecord::model();

        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $selector->splitByMadeInVariant($variant);

        $filter = new ProductsFilter();

        $request = $this->app()->request;
        /** @var $request CHttpRequest */

        $filter->targetFilter->value = $request->getParam($filter->targetFilter->name(), null);
        $filter->priceFilter->valueMin = $request->getParam($filter->priceFilter->name() . '_min', null);
        $filter->priceFilter->valueMax = $request->getParam($filter->priceFilter->name() . '_max', null);
        $filter->sectionFilter->values = $request->getParam($filter->sectionFilter->name(), []);
        $filter->sizeFilter->values = $request->getParam($filter->sizeFilter->name(), []);
        $filter->colorFilter->values = $request->getParam($filter->colorFilter->name(), []);
        $filter->brandFilter->values = $request->getParam($filter->brandFilter->name(), []);
        $filter->ageFilter->values = $request->getParam($filter->ageFilter->name(), []);
        $filter->sortFilter->value = $request->getParam($filter->sortFilter->name(), SortFilter::SORT_NONE);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderJson(['count' => $filter->totalCount()]);
        }

        $eventProductSelector = $filter->apply($selector);

        $productsIds = $eventProductSelector->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => self::DEFAULT_PAGE_SIZE,
            ],
        ]);
        $eventProductSelector->cache(self::DEFAULT_CACHE);
        $eventProductSelector->with('brand', 'event', 'product');
        $eventProducts = $eventProductSelector->productIdIn($provider->getData())->findAll();

        $groupedProducts = GroupedProducts::fromProducts($eventProducts);

        $this->render('index', [
            'groupedProducts' => $groupedProducts,
            'provider' => $provider,
            'title' => $this->t('Catalog of products'),
            'filter' => $filter,
        ]);
    }
}
