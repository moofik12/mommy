<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CHtml;
use CHttpException;
use CLogger;
use Doctrine\ORM\EntityManagerInterface;
use MommyCom\YiiComponent\AuthManager\AuthenticationTrait;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\SplitTestTrackingRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Db\UserRegistrationTrackingRecord;
use MommyCom\Model\Db\UserReturnRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\NewPasswordForm;
use MommyCom\Model\Form\RecoveryPasswordForm;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\Security\User\UserBuilder;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\AuthManager\MailingUserIdentity;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Frontend\ReplyResult;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Yii;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends FrontController
{
    use AuthenticationTrait;

    const SHOW_MODAL_SIGN_IN = 'auth-enter';
    const SHOW_MODAL_SIGN_UP = 'auth-enter';
    const SHOW_MODAL_SOCIAL_ENTER_EMAIL = 'auth-social-end';
    const SHOW_MODAL_SOCIAL_CONFIRM_PASSWORD = 'auth-social-confirm';

    const SOCIAL_AUTH_SERVICE_NOT_AVAILABLE = 'SERVICE_NOT_AVAILABLE';
    const SOCIAL_AUTH_SERVICE_FORM_ERROR = 'FORM_ERROR';
    const SOCIAL_AUTH_NOT_EMAIL = 'NOT_EMAIL';
    const SOCIAL_AUTH_CONFIRM_PASSWORD = 'CONFIRM_PASSWORD';

    const SPLIT_TEST_NAME_FOR_BENEFICE = 'present-reg-promocode-box';
    const SPLIT_TEST_DEFAULT_NUMBER_FOR_BENEFICE = 2;

    /**
     * @var string
     */
    public $defaultAction = 'auth';

    /**
     * @var string
     */
    public $layout = '//layouts/login';

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['postOnly + recovery, registration, offerRegistration']);
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['auth', 'registration'],
                'users' => ['?'],
            ],
            [
                'allow',
                'actions' => ['logout'],
                'users' => ['@'],
            ],
            [
                'allow',
                'actions' => [
                    'recovery',
                    'finishRegistration',
                    'passwordRecovery',
                    'offerRegistration',
                    'track',
                ],
            ],
            [
                'deny',
            ],
        ];
    }

    public function actionRecovery()
    {
        $request = $this->app()->request;
        $recoveryForm = new RecoveryPasswordForm();
        $response = new ReplyResult();

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($recoveryForm);
            $this->app()->end();
        }

        $dataForm = $this->app()->getRequest()->getPost('RecoveryPasswordForm');
        if (!empty($dataForm)) {
            $recoveryForm->setAttributes($dataForm);
            if ($recoveryForm->validate()) {
                /** @var UserRecord $model */
                $model = UserRecord::model()->emailIs($recoveryForm->email)->find();

                if ($model === null || Cast::toUInt($model->status) !== UserRecord::STATUS_ACTIVE) {
                    if ($request->isAjaxRequest) {
                        $response->addError($this->t('Profile not found or blocked'));
                    } else {
                        $this->app()->user->setFlash('message', $this->t('Profile not found or blocked'));
                    }
                } else {
                    $model->password_recovery_requested_at = time();

                    if ($model->save(true, ['password_recovery_requested_at'])) {
                        $mail = $this->app()->mailer;
                        $mail->create(
                            [$this->app()->params['layoutsEmails']['recoveryPassword'] => $this->app()->params['distributionEmailName']],
                            [$model->email],
                            $this->t('Password recovery'),
                            ['view' => 'recoveryPassword', 'data' => ['user' => $model]]
                        );

                        if ($request->isAjaxRequest) {
                            $response->setSuccess(true);
                            $response->setInfo($this->t('An email for password recovery was sent to {email}', ['{email}' => CHtml::encode($recoveryForm->email)]));
                        } else {
                            $this->app()->user->setFlash('message', $this->t('An email for password recovery was sent to {email}', ['{email}' => CHtml::encode($recoveryForm->email)]));
                            $this->redirect($this->app()->request->urlReferrer);
                        }
                    } else {
                        if ($request->isAjaxRequest) {
                            $response->addError($this->t('Error retrieving password'));
                        } else {
                            $this->app()->user->setFlash('message', $this->t('Error retrieving password'));
                        }
                    }
                }
            }
        }

        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }

        $this->redirect(['index/index', '#' => self::SHOW_MODAL_SIGN_IN]);
    }

    /**
     * @throws \CException
     */
    public function actionRegistration()
    {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $form = new RegistrationForm('simple');
        $response = new ReplyResult();

        if ($request->isXmlHttpRequest() && $request->get('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->get('RegistrationForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                /** @var EntityManagerInterface $entityManager */
                $entityManager = $this->container->get('doctrine')->getManager();
                /** @var UserBuilder $userBuilder */
                $userBuilder = $this->container->get(UserBuilder::class);
                /** @var EmailUtils $emailUtils */
                $emailUtils = $this->container->get(EmailUtils::class);

                $user = $userBuilder->setEmail($form->email)->build();
                $entityManager->persist($user);
                $entityManager->flush();

                $senderEmail = $this->app()->params['layoutsEmails']['welcome2'];
                $senderName = $this->app()->params['distributionEmailName'];
                $emailUtils->sendRegistrationEmail($user, $userBuilder->getPassword(), $senderEmail, $senderName);

                /* Если нам не нужно трекать все попытки регистрации пользователей в отдельную таблицу, то это можно выпилить */
                UserRegistrationTrackingRecord::fillWithRequestData();

                $splitTestTracking = new SplitTestTrackingRecord();
                $splitTestTracking->target = SplitTestTrackingRecord::TARGET_USER_REGISTER;
                $splitTestTracking->user_id = $user->getId();
                $splitTestTracking->order_id = 0;
                $splitTestTracking->testParams = $this->app()->splitTesting->getAvailable();
                $splitTestTracking->save();

                /** @var TokenStorageInterface $tokenStorage */
                $tokenStorage = $this->container->get('security.token_storage');
                $providerKey = $this->container->getParameter('frontend.firewall_key');
                if ($this->authenticateUser($tokenStorage, $user, $providerKey)) {
                    if ($request->isXmlHttpRequest()) {
                        $response->setSuccess(true);
                    } else {
                        $this->redirect(['index/index']);
                    }
                }
            }
        }


        if ($request->isXmlHttpRequest()) {
            $this->renderJson($response->toArray());
        }

        $this->redirect(['index/index', '#' => self::SHOW_MODAL_SIGN_UP]);
    }

    /**
     * @property-description Регистрация с бонусами
     */
    public function actionOfferRegistration()
    {
        $request = $this->app()->request;
        $webUser = $this->app()->user;
        $form = new RegistrationOfferForm();
        $message = false;
        $password = Random::alphabet(8, Random::DIGIT);
        $response = new ReplyResult();
        /** @var  $user  UserRecord */
        /** @var  $webUser  ShopWebUser */

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->getPost('RegistrationOfferForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                $user = new UserRecord();
                $user->email = $form->email;
                $user->password = $password;
                $user->moved_search_engine = $webUser->getLastSearchEngine(null);
                $user->psychotype = $webUser->getPsychotype(UserRecord::PSYCHOTYPE_UNDEFINED);
                $user->landing_num = $webUser->getLandingNum();

                if ($user->save()) {
                    $this->_saveDistribution($user->id);

                    $specialPsychotypeBanner = $webUser->getPsychotype(false);
                    $bannerName = $form->bannerName;

                    $invitedBy = $webUser->getState('invitedBy', 0);

                    if ($invitedBy && $webUser->isGuest) {
                        $user->setScenario('invitedBy');
                        $user->invited_by = $invitedBy;
                        $user->save(true, ['invited_by']);
                    }

                    $offerProvider = $webUser->getOfferProvider();
                    if ($offerProvider != UserRecord::OFFER_PROVIDER_NONE) {
                        $offerId = $webUser->getOfferId();
                        $offerTargetUrl = $webUser->getOfferTargetUrl();

                        $user->landing_num = $webUser->getLandingNum();
                        $user->offer_provider = $offerProvider;
                        $user->offer_id = $offerId;
                        $user->offer_target_url = $offerTargetUrl;
                        $user->save(true, ['offer_provider', 'offer_id', 'offer_target_url', 'landing_num']);
                    }
                    //закрепление за партнером
                    $partnerId = $webUser->getPartnerId();
                    if ($partnerId > 0) {
                        $partnerRefererUrl = $webUser->getPartnerRefererUrl();
                        $partnerUtmParams = $webUser->getPartnerUtmParams();
                        $user->assignToPartner($partnerId, $partnerRefererUrl, json_encode($partnerUtmParams, JSON_UNESCAPED_UNICODE, 3));
                    }
                    //получение бенефитов
                    if ($form->strategy == RegistrationOfferForm::STRATEGY_BONUSES) {
                        $bonusPoints = new UserBonuspointsRecord();
                        $bonusPoints->user_id = $user->id;
                        $bonusPoints->points = $form->getBeneficeAmount();
                        $bonusPoints->message = $this->t('Welcome to the shopping club mommy.com');
                        $bonusPoints->is_custom = false;
                        $bonusPoints->type = UserBonuspointsRecord::TYPE_PRESENT;
                        $bonusPoints->setExpireAtString('tomorrow +6 days');

                        if ($bonusPoints->save()) {
                            $allowed = $this->app()->params['newsletters'] ?? false;
                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                                    [$user->email],
                                    $this->t('Welcome to the shopping club mommy.com'),
                                    [
                                        'view' => 'welcome2',
                                        'data' => ['model' => $user, 'password' => $password, 'bonus' => $bonusPoints],
                                    ]
                                );
                                $response->setSuccess(true);
                                $message = $this->t('On your bonus account, you have accrued {bonus} USD. Have fun shopping!', ['{bonus}' => $bonusPoints->points]);
                            }
                        } else {
                            Yii::log($this->t('An error occurred while calculating bonuses to the new user {email}. Error type:', ['{email}' => $form->email]) . print_r($bonusPoints->errors, true), CLogger::LEVEL_ERROR);
                        }
                    } elseif ($form->strategy == RegistrationOfferForm::STRATEGY_PROMOCODE) {
                        $promo = new PromocodeRecord();
                        $promo->type = PromocodeRecord::TYPE_PERSONAL;
                        $promo->reason = PromocodeRecord::REASON_CAMPAIGN;
                        $promo->user_id = $user->id;

                        if ($form->strategyPayments == RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY) {
                            $promo->cash = $form->getBeneficeAmount();
                            $promo->order_price_after = RegistrationOfferForm::PROMOCODE_PAYMENTS_MONEY_AFTER_ORDER_AMOUNT;
                        } elseif ($form->strategyPayments == RegistrationOfferForm::STRATEGY_PAYMENTS_PERCENT) {
                            $promo->percent = $form->getBeneficeAmount();
                        }
                        $promo->valid_until = strtotime('tomorrow +3 days');

                        if ($promo->save()) {
                            $user->present_promocode = $promo->promocode;
                            if (!$user->save(true, ['present_promocode'])) {
                                Yii::log($this->t('Error writing gift voucher code to new user {email}. Error type:', ['{email}' => $form->email]) . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                            }

                            $allowed = $this->app()->params['newsletters'] ?? false;
                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['welcomeOfferWithPromocode2'] => $this->app()->params['distributionEmailName']],
                                    [$user->email],
                                    $this->t('Important information + Discount for new club member'),
                                    [
                                        'view' => 'welcomeOfferWithPromocode2',
                                        'data' => ['user' => $user, 'password' => $password, 'promocode' => $promo],
                                    ]
                                );
                                $response->setSuccess(true);
                                $message = $this->t('Thank you for registering! A letter with a coupon has been sent to your mailing address!');
                            }
                        } else {
                            Yii::log($this->t('An error occurred while calculating bonuses to the new user {email}. Error type:', ['{email}' => $form->email]) . print_r($promo->errors, true), CLogger::LEVEL_ERROR);
                        }
                    }

                    $authForm = new AuthForm();
                    $authForm->email = $form->email;
                    $authForm->password = $password;

                    //при авторизации подымается новая сессия, сообщение нужно записать в актуальную
                    if ($authForm->login()) {
                        if ($message) {
                            if ($request->isAjaxRequest) {
                                $this->renderJson($response->toArray());
                            }
                            $this->redirect(['message/info',
                                'title' => $this->t($this->t('Thank you for registering!')),
                                'message' => $message,
                                'hash' => $this->app()->tokenManager->getToken($message),
                                "present-registration-box-registered" => "success-$bannerName",
                            ]);
                        }
                        if ($request->isAjaxRequest) {
                            $this->renderJson($response->toArray());
                        }
                        $this->redirect(['index/index', "present-registration-box-registered" => "success-$bannerName"]);
                    } else {
                        if ($message) {
                            $this->app()->user->setFlash('message', $message);
                        }
                        if ($request->isAjaxRequest) {
                            $this->renderJson($response->toArray());
                        }
                        $this->redirect(['index/index', "present-registration-box-registered" => "success-$bannerName", 'login' => 'error']);
                    }
                } else {
                    Yii::log($this->t('An error occurred while registering a new user {email}. Error type:', ['{email}' => $form->email]) . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                    $message = $this->t('Error while registering. Contact Support!');
                }
            } else {
                Yii::log($this->t('An error occurred while registering a new user {email}. Error type:', ['{email}' => $form->email]) . print_r($form->errors, true), CLogger::LEVEL_ERROR);
                $message = $this->t('Error while registering. Contact Support!');

                $user = UserRecord::model()->emailIs($form->email)->find();
                /** @var  $user  UserRecord */
                if ($user !== null) {
                    $userReturned = UserReturnRecord::model()->email($form->email)->find() ?: new UserReturnRecord();
                    $userReturned->email = $user->email;
                    $userReturned->returned_at = time();
                    $userReturned->save();
                }
            }
        }

        if ($message) {
            $this->app()->user->setFlash('message', $message);
        }
        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }
        $this->redirect(['index/index']);
    }

    public function actionFinishRegistration($h, $uid)
    {
        $uid = Cast::toUInt($uid);
        $h = Cast::toStr($h);

        /* @var $user UserRecord */
        $user = UserRecord::model()->findByPk($uid);

        $this->app()->user->logout();

        if ($user === null) {
            throw new CHttpException('404', 'Data not found');
        }

        if ($user->is_email_verified) {
            $this->redirect(['index/index']);
        }

        if ($h === $user->generateHashEmailToVerify() && !Cast::toBool($user->is_email_verified)) {
            $user->is_email_verified = true;
            if ($user->save(true, ['is_email_verified'])) {
                $this->app()->user->setFlash('message', $this->t('Registration is complete. You can log in using your e-mail and password'));
            }

            $identity = new MailingUserIdentity($user->id, $user->getLoginHash());
            if ($identity->authenticate()) {
                $duration = isset($this->app()->params['remember']) ? $this->app()->params['remember'] : 0;
                $this->app()->user->login($identity, $duration);
            }
        }

        if ($user->is_email_verified) {
            $this->layout = '//layouts/main'; // hack
            $this->render('finishRegistration');
        } else {
            $this->redirect(['index/index']);
        }
    }

    /**
     * @param integer $uid userId
     * @param string $t token
     *
     * @throws CHttpException
     */
    public function actionPasswordRecovery($uid, $t)
    {
        $uid = Cast::toUInt($uid);
        $token = Cast::toStr($t);

        /** @var UserRecord $model */
        $model = UserRecord::model()->findByPk($uid);

        if ($model === null) {
            throw new CHttpException('404', 'Error');
        }

        $accessToken = $model->generateHashPasswordRecovery();

        if ($model->password_recovery_requested_at > time() - 86400 && $token === $accessToken) {
            //create form
            $formPassword = new NewPasswordForm();
            $data = $this->app()->request->getParam('NewPasswordForm');

            if ($data) {
                if ($this->app()->request->isAjaxRequest) {
                    echo CActiveForm::validate($formPassword);
                    $this->app()->end();
                }

                $formPassword->setAttributes($data);
                if ($formPassword->validate($data)) {
                    // ставим новый пароль и генерируем новую соль
                    $model->password_recovery_requested_at = 0;
                    $model->password_salt = '';
                    $model->password = $formPassword->password;
                    if (!$model->save()) {
                        $this->app()->user->setFlash('message', $this->t('Error retrieving password!'));
                        $this->redirect(['index/index', '#' => self::SHOW_MODAL_SIGN_IN]);
                    }

                    //входим от имени пользователя
                    $authForm = new AuthForm();
                    $authForm->email = $model->email;
                    $authForm->password = $formPassword->password;
                    if ($authForm->login()) {
                        $this->redirect(['index/index']);
                    }

                    $this->redirect(['index/index', '#' => self::SHOW_MODAL_SIGN_IN]);
                }
            }

            $this->render('newPassword', ['formPassword' => $formPassword]);
            $this->app()->end();
        }

        throw new CHttpException('404', 'Error');
    }

    public function actionLogout()
    {
        $this->app()->user->logout();
        $this->redirect(['index/index']);
    }

    public function actionTrack()
    {
        UserRegistrationTrackingRecord::fillWithRequestData();
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    private function _saveDistribution($userId)
    {
        $distribution = new UserDistributionRecord();
        $distribution->user_id = $userId;
        $distribution->type = UserDistributionRecord::TYPE_EVERY_DAY;
        $distribution->is_subscribe = 1;
        $distribution->is_update = 1;
        return $distribution->save();
    }
}
