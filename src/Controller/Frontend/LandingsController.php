<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CLogger;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Db\UserReturnRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Frontend\ReplyResult;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class LandingsController extends FrontController
{
    const SOCIAL_AUTH_SERVICE_NOT_AVAILABLE = 'SERVICE_NOT_AVAILABLE';
    const SOCIAL_AUTH_SERVICE_FORM_ERROR = 'FORM_ERROR';
    const SOCIAL_AUTH_NOT_EMAIL = 'NOT_EMAIL';
    const SOCIAL_AUTH_CONFIRM_PASSWORD = 'CONFIRM_PASSWORD';

    /**
     * @var string
     */
    public $layout = '//layouts/landings';

    /**
     * @var string
     */
    public $bodyClass = '';

    /**
     * @return array
     */
    public function accessRules(): array
    {
        return [
            ['deny'],
        ];
    }

    /**
     * список автивных Promo
     *
     * @var array array( id => name );
     */
    protected $_promoEnabled = [
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',

        //new
        8 => 'eight',
        9 => 'nine',
        10 => 'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
//        17 => 'seventeen', // mamsy like landing, russian lang
//        18 => 'eighteen', // mamsy like landing, ukrainian lang
//        19 => 'nineteen', // mamsy like landing, russian lang
        20 => 'twenty',
    ];

    protected $_promoDisableBalancer = [
        1 => 'one',
        2 => 'two',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        8 => 'eight',
        9 => 'nine',
        10 => 'ten',
        12 => 'twelve',
        13 => 'thirteen',
        15 => 'fifteen',
        16 => 'sixteen',
        17 => 'seventeen',
        18 => 'eighteen',
        19 => 'nineteen',
        20 => 'twenty',
    ];

    protected $_promoOffer = [
        20 => 'twenty',
    ];

    /**
     * @param string $name
     * @param string $social модальное окно завершение регистрации
     * @param string $promo
     */
    public function actionIndex($name = 'registration', $social = '', $promo = '')
    {
        $validPromo = $this->_promoEnabled;
        $user = $this->app()->user;
        $tab = Cast::toStr($name);
        $promoId = array_search(Cast::toStr($promo), $validPromo);

        if (!$user->isGuest || empty($validPromo)) {
            $this->redirect(['index/index']);
        }

        if ($promoId === false) {
            $promoId = array_rand(array_diff($validPromo, $this->_promoDisableBalancer));
            $promo = $validPromo[$promoId];
            $this->redirect(['index', 'promo' => $promo]);
        }

        $user->setLandingNum($promoId);

        $form = new RegistrationForm();

        $tabs = [
            'success',
            'registration',
        ];

        $socialIn = [
            'socialNotEmail',
            'socialConfirm',
        ];

        if (!in_array($social, $socialIn)) {
            $social = '';
        }

        if (!in_array($tab, $tabs)) {
            $tab = 'registration';
        }

        //processing     static::actionNetworkRegistration()
        $registrationSocial = new NetworkRegistrationForm();

        //processing     static::actionNetworkRegistration()
        $registrationSocialConfirm = new NetworkRegistrationConfirmForm();

        $this->render("$promoId/index", [
            'promoId' => $promoId,
            'tab' => $tab,
            'social' => $social,
            'registrationForm' => $form,
            'registrationSocial' => $registrationSocial,
            'registrationSocialConfirm' => $registrationSocialConfirm,
        ]);
    }

    public function actionRegistration($id)
    {
        $password = Random::alphabet(8, Random::DIGIT);
        $request = $this->app()->request;
        $webUser = $this->app()->user;

        $id = Cast::toUInt($id);
        $tab = 'registration';

        //на всякий случай
        $promo = isset($this->_promoEnabled[$id]) ? $this->_promoEnabled[$id] : '';

        $form = new RegistrationForm('landing');

        //частный случай (верстка)
        $form->attachEventHandler('onBeforeValidate', function ($event) {
            /** @var UserRecord $model */
            $model = $event->sender;

            if ($model->name == $this->t('Your name')) {
                $model->addError('name', $this->t('Enter your name'));
            }
        });

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->getPost('RegistrationForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                $user = new UserRecord();
                $user->name = $form->name;
                $user->email = $form->email;
                $user->password = $password;

                $offerProvider = $webUser->getOfferProvider();
                if ($offerProvider != UserRecord::OFFER_PROVIDER_NONE) {
                    $user->offer_provider = $offerProvider;
                    $user->offer_id = $webUser->getOfferId();
                    $user->offer_target_url = $webUser->getOfferTargetUrl();
                }
                $user->landing_num = $webUser->getLandingNum();

                if ($user->save()) {
                    $this->_saveDistribution($user->id);

                    $this->app()->mailer->create(
                        [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                        [$user->email],
                        $this->t('IMPORTANT INFORMATION for a new club member'),
                        [
                            'view' => 'welcome2',
                            'data' => ['user' => $user, 'password' => $password,
                            ],
                        ]
                    );

                    $tab = 'success';
                } else {
                    Yii::log($this->t('Error while registering new user') . ' ' . $form->email . ' Errors: ' . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                    $this->app()->user->setFlash('message', $this->t('Error while registering. Contact Support!'));
                }
            } else {
                $user = UserRecord::model()->emailIs($form->email)->find();
                /** @var  $user  UserRecord */
                if ($user !== null) {
                    $userReturned = UserReturnRecord::model()->email($form->email)->find() ?: new UserReturnRecord();
                    $userReturned->email = $user->email;
                    $userReturned->returned_at = time();
                    $userReturned->save();
                }
            }
        }

        $this->redirect(['index', 'name' => $tab, 'promo' => $promo]);
    }


    /**
     * @property-description Регистрация пользователей с начилением бонусов или созданием промокодов на покупку
     *
     * @param string $promo
     * @param string $social
     * @param int $fon
     * @param string $name
     * @param string $redirectTo
     */
    public function actionOfferRegistration($promo = 'twenty', $social = '', $fon = 1, $name = '', $redirectTo = '')
    {
        $fon = Cast::toInt($fon);
        if (!in_array($fon, range(1, 23))) {
            $fon = 1;
        }
        $this->bodyClass = 'land bg' . $fon;
        $validPromo = $this->_promoOffer;
        $request = $this->app()->request;
        $webUser = $this->app()->user;
        /** @var  $webUser \MommyCom\YiiComponent\AuthManager\ShopWebUser */
        $promoId = array_search(Cast::toStr($promo), $validPromo);
        $socialIn = [
            'socialNotEmail',
            'socialConfirm',
            'success',
        ];

        if (!in_array($social, $socialIn)) {
            $social = '';
        }
        $defaultRedirectTo = $this->createAbsoluteUrl('index/index');
        $nameStateRedirectTo = 'offer_reg_lg_redirectTo';
        if ($redirectTo !== '') {
            $redirectToDecode = base64_decode($redirectTo);
            if (mb_strpos($redirectToDecode, 'mommy.com') !== false) {
                $redirectTo = $redirectToDecode;
                if ($webUser->getState($nameStateRedirectTo) === null) {
                    $webUser->setState($nameStateRedirectTo, $redirectTo);
                }
            } else {
                $redirectTo = $defaultRedirectTo;
            }
        } else {
            $redirectTo = $webUser->getState($nameStateRedirectTo, $defaultRedirectTo);
        }

        if ($social !== 'success') {
            if (!$webUser->isGuest || empty($validPromo)) {
                $this->redirect($redirectTo);
            }
        }

        $message = false;
        $password = Random::alphabet(8, Random::DIGIT);
        $response = new ReplyResult();
        /** @var  $user  UserRecord */

        if ($promoId === false) {
            $this->redirect(['offerRegistration', 'promo' => 'twenty']);
        }

        $webUser->setLandingNum($promoId);

        if ($webUser->offerProvider == UserRecord::OFFER_PROVIDER_NONE) {
            $webUser->setOfferProvider(UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN);
            if ($name !== '') {
                $webUser->setOfferId($name);
            }
        }
        $webUser->setOfferTargetUrl($this->app()->createAbsoluteUrl('index/index'));

        $form = new RegistrationOfferForm();
        $form->benefice = RegistrationOfferForm::BENEFICE_100;
        $form->strategy = RegistrationOfferForm::STRATEGY_PROMOCODE;
        $form->strategyPayments = RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY;
        $form->bannerName = 'landings-' . $promo;
        $form->isConfirmPrivacy = 1;

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->getPost('RegistrationOfferForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                $user = new UserRecord();
                $user->email = $form->email;
                $user->password = $password;
                $user->moved_search_engine = $webUser->getLastSearchEngine(null);
                $user->psychotype = $webUser->getPsychotype(UserRecord::PSYCHOTYPE_UNDEFINED);
                $user->landing_num = $webUser->getLandingNum();

                if ($user->save()) {
                    $bannerName = $form->bannerName;
                    $this->_saveDistribution($user->id);

                    $invitedBy = $webUser->getState('invitedBy', 0);

                    if ($invitedBy && $webUser->isGuest) {
                        $user->setScenario('invitedBy');
                        $user->invited_by = $invitedBy;
                        $user->save(true, ['invited_by']);
                    }

                    $offerProvider = $webUser->getOfferProvider();
                    if ($offerProvider != UserRecord::OFFER_PROVIDER_NONE) {
                        $offerId = $webUser->getOfferId();
                        $offerTargetUrl = $webUser->getOfferTargetUrl();

                        $user->landing_num = $webUser->getLandingNum();
                        $user->offer_provider = $offerProvider;
                        $user->offer_id = $offerId;
                        $user->offer_target_url = $offerTargetUrl;
                        $user->save(true, ['offer_provider', 'offer_id', 'offer_target_url', 'landing_num']);
                    }

                    //получение бенефитов
                    if ($form->strategy == RegistrationOfferForm::STRATEGY_BONUSES) {
                        $bonusPoints = new UserBonuspointsRecord();
                        $bonusPoints->user_id = $user->id;
                        $bonusPoints->points = $form->getBeneficeAmount();
                        $bonusPoints->message = $this->t('Welcome to the shopping club mommy.com');
                        $bonusPoints->is_custom = false;
                        $bonusPoints->type = UserBonuspointsRecord::TYPE_PRESENT;
                        $bonusPoints->setExpireAtString('tomorrow +6 days');

                        if ($bonusPoints->save()) {
                            $allowed = $this->app()->params['newsletters'] ?? false;

                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                                    [$user->email],
                                    $this->t('Welcome to the shopping club mommy.com'),
                                    [
                                        'view' => 'welcome2',
                                        'data' => ['model' => $user, 'password' => $password, 'bonus' => $bonusPoints],
                                    ]
                                );
                                $response->setSuccess(true);
                                $message = $this->t('On your bonus account, you have accrued {bonus} USD. Have fun shopping!', ['{bonus}' => $bonusPoints->points]);
                            }
                        } else {
                            Yii::log($this->t('An error occurred while calculating bonuses to the new user') . ' ' . $form->email . ' Errors: ' . print_r($bonusPoints->errors, true), CLogger::LEVEL_ERROR);
                        }
                    } elseif ($form->strategy == RegistrationOfferForm::STRATEGY_PROMOCODE) {
                        $promo = new PromocodeRecord();
                        $promo->type = PromocodeRecord::TYPE_PERSONAL;
                        $promo->reason = PromocodeRecord::REASON_CAMPAIGN;
                        $promo->user_id = $user->id;

                        if ($form->strategyPayments == RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY) {
                            $promo->cash = $form->getBeneficeAmount();
                            $promo->order_price_after = RegistrationOfferForm::PROMOCODE_PAYMENTS_MONEY_AFTER_ORDER_AMOUNT;
                        } elseif ($form->strategyPayments == RegistrationOfferForm::STRATEGY_PAYMENTS_PERCENT) {
                            $promo->percent = $form->getBeneficeAmount();
                        }
                        $promo->valid_until = strtotime('tomorrow +3 days');

                        if ($promo->save()) {
                            $user->present_promocode = $promo->promocode;
                            if (!$user->save(true, ['present_promocode'])) {
                                Yii::log($this->t('Error writing gift voucher to new user') . ' ' . $form->email . ' Errors: ' . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                            }

                            $allowed = $this->app()->params['newsletters'] ?? false;

                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['welcomeOfferWithPromocode2'] => $this->app()->params['distributionEmailName']],
                                    [$user->email],
                                    $this->t('Important information + Discount for new club member'),
                                    [
                                        'view' => 'welcomeOfferWithPromocode2',
                                        'data' => ['user' => $user, 'password' => $password, 'promocode' => $promo],
                                    ]
                                );
                                $response->setSuccess(true);
                                $message = $this->t('Thank you for registering! A letter with a coupon has been sent to your mailing address!');
                            }
                        } else {
                            Yii::log($this->t('An error occurred while calculating bonuses to the new user') . ' ' . $form->email . ' Errors: ' . print_r($promo->errors, true), CLogger::LEVEL_ERROR);
                        }
                    }

                    $authForm = new AuthForm();
                    $authForm->email = $form->email;
                    $authForm->password = $password;

                    //при авторизации подымается новая сессия, сообщение нужно записать в актуальную
                    if ($authForm->login()) {
                        if ($message) {
                            if ($request->isAjaxRequest) {
                                $this->renderJson($response->toArray());
                            }
                            $this->redirect(['message/info',
                                'title' => $this->t('Thank you for registering!'),
                                'message' => $message,
                                'hash' => $this->app()->tokenManager->getToken($message),
                                "present-registration-box-registered" => "success-$bannerName",
                            ]);
                        }
                        if ($request->isAjaxRequest) {
                            $this->renderJson($response->toArray());
                        }
                        $this->redirect(['index/index', "present-registration-box-registered" => "success-$bannerName"]);
                    } else {
                        if ($message) {
                            $this->app()->user->setFlash('message', $message);
                        }
                        if ($request->isAjaxRequest) {
                            $this->renderJson($response->toArray());
                        }
                        $this->redirect(['index/index', "present-registration-box-registered" => "success-$bannerName", 'login' => 'error']);
                    }
                } else {
                    Yii::log($this->t('Error while registering new user') . ' ' . $form->email . ' Erorrs: ' . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                    $message = $this->t('Error while registering. Contact Support!');
                }
            } else {
                Yii::log($this->t('Error while registering new user') . ' ' . $form->email . ' Erorrs: ' . print_r($form->errors, true), CLogger::LEVEL_ERROR);
                $message = $this->t('Error while registering. Contact Support!');

                $user = UserRecord::model()->emailIs($form->email)->find();
                /** @var  $user  UserRecord */
                if ($user !== null) {
                    $userReturned = UserReturnRecord::model()->email($form->email)->find() ?: new UserReturnRecord();
                    $userReturned->email = $user->email;
                    $userReturned->returned_at = time();
                    $userReturned->save();
                }
            }
        }

        if ($message) {
            $this->app()->user->setFlash('message', $message);
        }
        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }

        $this->render("$promoId/offerRegistration", [
            'promoId' => $promoId,
            'formOfferRegistration' => $form,
            'social' => $social,
            'fon' => $fon,
            'nameStateRedirectTo' => $nameStateRedirectTo,
        ]);
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    private function _saveDistribution($userId)
    {
        $distribution = new UserDistributionRecord();
        $distribution->user_id = $userId;
        $distribution->type = UserDistributionRecord::TYPE_EVERY_DAY;
        $distribution->is_subscribe = 1;
        $distribution->is_update = 1;
        return $distribution->save();
    }

    public function actionTest()
    {
        $emails = ['var3107@ukr.net']; /*'leonextra@gmail.com','testln@yandex.ru', 'vshyvkova.arina@gmail.com', 'var3107@mail.ru');*/
        $users = UserRecord::model()->emailIn($emails)->findAll();
        foreach ($users as $user) {
            if ($user !== null) {
                echo $user->email . '<br>';
                $order = OrderRecord::model()->findByPk(168044);

                $allowed = $this->app()->params['newsletters'] ?? false;
                if ($allowed) {
                    @$this->app()->mailer->create(
                        [$this->app()->params['layoutsEmails']['automaticConfirmed'] => $this->app()->params['distributionEmailName']],
                        [$user->email],
                        $this->t('Your order № {number} is accepted!', ['{number}' => $order->id]),
                        [
                            'view' => 'notification.order.automaticConfirmed',
                            'data' => [
                                'order' => $order,
                                'frontendUrlManager' => $this->app(),
                            ],
                        ],
                        EmailMessage::TYPE_SYSTEM
                    );
                }

                /*@$this->app()->mailer->create(
                    array($this->app()->params['distributionEmail'] => $this->app()->params['distributionEmailName']),
                    array($user->email),
                    "Заказ  №{$order->id} подтвержден",
                    array(
                        'view' => 'notification.order.confirmed',
                        'data' => array(
                            'order' => $order,
                            'frontendUrlManager' => $this->app()
                        )
                    ),
                    EmailMessage::TYPE_SYSTEM
                );*/
                /*$this->app()->mailer->create(
                    array($this->app()->params['distributionEmail'] => $this->app()->params['distributionEmailName']),
                    array($user->email),
                    "Благодарим за заказ {$order->id}!",
                    array(
                        'view' => 'notification.order.create',
                        'data' => array(
                            'order' => $order,
                            'frontendUrlManager' => $this->app()
                        )
                    ),
                    EmailMessage::TYPE_SYSTEM
                );*/

                /*$this->app()->mailer->create(
                    array($this->app()->params['distributionEmail'] => $this->app()->params['distributionEmailName']),
                    array($user->email),
                    "Заказ  №{$order->id} был передан в службу доставки",
                    array(
                        'view' => 'notification.order.mailed',
                        'data' => array(
                            'order' => $order,
                            'frontendUrlManager' =>  $this->app()
                        )
                    ),
                    EmailMessage::TYPE_SYSTEM
                );*/

                /*$this->app()->mailer->create(
                    array($this->app()->params['distributionEmail'] => $this->app()->params['distributionEmailName']),
                    array($user->email),
                    "Заказ  №{$order->id} ожидает оплату",
                    array(
                        'view' => 'notification.order.prepay',
                        'data' => array(
                            'order' => $order,
                            'frontendUrlManager' =>  $this->app()
                        )
                    ),
                    EmailMessage::TYPE_SYSTEM
                );*/
                /*$viewPath = Yii::getPathOfAlias($this->app()->mailer->viewPath);
                $viewFile = $viewPath . DIRECTORY_SEPARATOR . 'weWorry.php';
                $body = $this->renderFile($viewFile, array(
                    'user' => $user,
                ), true);
                var_dump($this->app()->mailer->create(
                    array($this->app()->params['distributionEmail'] => $this->app()->params['distributionEmailName']),
                    array($user->email),
                    "Мы волнуемся",
                    array(
                        'body' => $body, //сommon.views.email-second.weWorry',
                        'data' => array(
                            'user' => $user,
                        )
                    ),
                    EmailMessage::TYPE_SYSTEM
                ));*/
                echo 'done' . '<br>';
            }
        }
        exit;
    }
}
