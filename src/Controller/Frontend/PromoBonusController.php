<?php

namespace MommyCom\Controller\Frontend;

use CLogger;
use DateTime;
use MommyCom\Model\Db\BonuspointsPromoRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

class PromoBonusController extends FrontController
{
    const BONUS_AVAILABLE_STRING = '1 month';

    /**
     * @param $hash
     */
    public function actionActivation($hash)
    {
        $shopUser = $this->app()->user;
        $hash = Utf8::trim(Cast::toStr($hash));
        $messageTitle = $this->t('Calculation of bonuses on dispatch');
        $message = '';
        $description = '';

        /** @var BonuspointsPromoRecord $bonuspointsPromo */
        $bonuspointsPromo = BonuspointsPromoRecord::model()->hash($hash)->find();

        if ($bonuspointsPromo === null || $shopUser->isGuest) {
            $this->redirect(['index/index']);
        } elseif (!$bonuspointsPromo->isEnable()) {
            $message = $this->t('The accrual of bonuses for this newsletter ended');
        }

        if (empty($message)) {
            $bonus = UserBonuspointsRecord::model()->userId($shopUser->id)->customMessageLike($bonuspointsPromo->getUniqueLogMessage())->find();
            if ($bonus === null) {
                $newBonus = new UserBonuspointsRecord();
                $newBonus->message = $bonuspointsPromo->log_message;
                $newBonus->points = $bonuspointsPromo->points;
                $newBonus->user_id = $shopUser->id;
                $newBonus->custom_message = $bonuspointsPromo->getUniqueLogMessage();
                $newBonus->type = UserBonuspointsRecord::TYPE_INNER;

                if ($bonuspointsPromo->bonus_available_at > 0) {
                    $newBonus->type = UserBonuspointsRecord::TYPE_PRESENT;
                    $newBonus->setExpireAt($bonuspointsPromo->bonus_available_at + time());
                }

                if ($newBonus->save()) {
                    $message = $bonuspointsPromo->user_message;
                } else {
                    $message = $this->t('An error occurred while calculating the bonus. Contact Support!');
                    Yii::log('Ошибка при начислении бонуса пользователю по промоакции для пользователя ID:' . $shopUser->id . print_r($newBonus->getErrors(), true), CLogger::LEVEL_ERROR);
                }
            } else {
                $message = "You already received a bonus! Pleasant shopping!";
            }
        }

        $this->redirect(['message/bonus',
                'title' => $messageTitle,
                'message' => $message,
                'description' => $description,
                'hash' => $this->app()->tokenManager->getToken($message),
            ]
        );
    }

    /**
     * Бонус в ежедневной рассылке
     *
     * @param $hash
     */
    public function actionDaily($hash)
    {
        $shopUser = $this->app()->user;
        $hash = Utf8::trim(Cast::toStr($hash));
        $dateTime = new DateTime('+' . self::BONUS_AVAILABLE_STRING);
        $messageTitle = $this->t('Calculation of bonuses on dispatch');
        $message = '';
        $description = '';

        /** @var BonuspointsPromoRecord $bonuspointsPromo */
        $bonuspointsPromo = BonuspointsPromoRecord::model()->hash($hash)->find();

        if ($bonuspointsPromo === null || $shopUser->isGuest) {
            $this->redirect(['index/index']);
        } elseif (!$bonuspointsPromo->isEnable()) {
            $message = $this->t('The accrual of bonuses for this newsletter ended');
            $description = $this->t('Bonuses are credited within a day after receiving the letter');
        }

        if (empty($message)) {
            $bonus = UserBonuspointsRecord::model()->userId($shopUser->id)->customMessageLike($bonuspointsPromo->getUniqueLogMessage())->find();
            if ($bonus === null) {
                $bonusPoints = $this->_generateRandomDailyBonus($bonuspointsPromo);

                $newBonus = new UserBonuspointsRecord();
                $newBonus->message = $bonuspointsPromo->log_message;
                $newBonus->points = $bonusPoints;
                $newBonus->user_id = $shopUser->id;
                $newBonus->custom_message = $bonuspointsPromo->getUniqueLogMessage();
                $newBonus->type = UserBonuspointsRecord::TYPE_PRESENT;
                $newBonus->setExpireAt($dateTime);

                if ($newBonus->save()) {
                    $message = $this->t('Congratulations! You are credited {bonusPoints} USD to your bonus account! Have fun shopping!', ['{bonusPoints}' => $bonusPoints]);
                } else {
                    $message = $this->t('An error occurred while calculating the bonus. Contact Support!');
                    Yii::log($this->t('An error occurred while calculating the bonus for the user\'s promotion') . ' ID:' . $shopUser->id . print_r($newBonus->getErrors(), true), CLogger::LEVEL_ERROR);
                }
            } else {
                $message = $this->t('You already received a bonus! Pleasant shopping!');
            }
        }

        $this->redirect(['message/bonus',
                'title' => $messageTitle,
                'message' => $message,
                'description' => $description,
                'hash' => $this->app()->tokenManager->getToken($message),
            ]
        );
    }

    /**
     * @param BonuspointsPromoRecord $bonuspointsPromo
     *
     * @return int
     */
    private function _generateRandomDailyBonus(BonuspointsPromoRecord $bonuspointsPromo)
    {
        $minBonus = 5;
        //соотношение % - money
        $bonusPercent = [
            $minBonus => 85,
//            10 => 10,
//            20 => 2,
//            25 => 1,
//            35 => 1,
//            50 => 1,
        ];

        $bonuspointsRecordTable = UserBonuspointsRecord::model()->tableName();
        $dbBuilder = $this->app()->db->createCommand();
        $dbBuilder->select('points as points, COUNT(points) as amount');
        $dbBuilder->from($bonuspointsRecordTable);
        $dbBuilder->group('points');
        $dbBuilder->where(['like', 'custom_message', '%' . $bonuspointsPromo->getUniqueLogMessage() . '%']);

        $data = $dbBuilder->queryAll();
        $countItems = array_sum(array_map(function ($item) {
            return $item['amount'];
        }, $data));
        $dataPercent = [];
        $bonusPercentActive = [];

        foreach ($data as $row) {
            $dataPercent[$row['points']] = ceil(($row['amount'] / $countItems) * 100);
        }

        foreach ($bonusPercent as $bonus => $percent) {
            if (isset($dataPercent[$bonus]) && $dataPercent[$bonus] > $percent) {
                continue;
            }

            $bonusPercentActive[$bonus] = $bonus;
        }

        if (empty($bonusPercentActive)) {
            $bonusPercentActive[$minBonus] = $minBonus;
        }

        return array_rand($bonusPercentActive);
    }
}
