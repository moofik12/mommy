<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CArrayDataProvider;
use CSort;
use MommyCom\Model\Db\InviteRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\UserAccountForm;
use MommyCom\Model\Form\UserSettingsForm;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Frontend\ReplyResult;

class AccountController extends FrontController
{
    /**
     * @var string
     */
    public $defaultAction = 'orders';

    /**
     * @var array
     */
    public $menu = [];

    public function init()
    {
        $this->menu = [
            ['label' => $this->t('My orders'), 'url' => ['account/orders'], 'itemOptions' => ['class' => 'bold']],
            ['label' => $this->t('Balance'), 'url' => ['account/bonuses'], 'itemOptions' => ['class' => 'bold']],
            ['label' => $this->t('Personal data'), 'url' => ['account/about']],
            ['label' => $this->t('account settings'), 'url' => ['account/settings']],
//            ['label' => $this->t('Invitations'), 'url' => ['account/invite']],
//            ['label' => $this->t('Exit'), 'url' => ['auth/logout']],
        ];
        parent::init();
    }

    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + inviteByEmail']);
    }

    public function actionAbout()
    {
        $model = new UserAccountForm();
        $user = $this->app()->user->getModel();
        /** @var  $user UserRecord */
        // prefill form
        $model->name = $user->name;
        $model->surname = $user->surname;
        $model->email = $user->email;
        $model->address = $user->address;
        $model->telephone = $user->telephone;
        // end prefill

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        $formData = $this->app()->request->getPost('UserAccountForm');
        $model->setAttributes($formData);

        if ($formData && $model->validate()) {
            // fill model
            $user->name = $model->name;
            $user->surname = $model->surname;
            $user->email = $model->email;
            $user->address = $model->address;
            $user->telephone = $model->telephone;
            // end fill

            if ($user->save()) {
                //$this->app()->user->setFlash('message', $this->t('Data saved'));
            } else {
                //$this->app()->user->setFlash('message', $this->t('Error while saving'));
                $model->addErrors($user->errors);
            }
        }

        $this->render('about', ['model' => $model]);
    }

    /**
     * Смена пароля, системные уведомления, проморассылка
     * Подписка на рассылку
     */
    public function actionSettings()
    {
        $form = new UserSettingsForm();
        $shopWebUser = $this->app()->user;
        $user = $this->app()->user->getModel();
        /** @var $shopWebUser \MommyCom\YiiComponent\AuthManager\ShopWebUser */
        /** @var $user UserRecord */
        //feed form
        $form->isWishSystemMail = $user->is_system_subscribe;
        $form->isWishPromoMail = $user->is_subscribe;
        $distribution = UserDistributionRecord::model()
            ->userId($user->id)
            ->isSubscribe(1)
            ->find();
        $form->distribution = UserDistributionRecord::TYPE_EVERY_DAY;
        if ($distribution !== null) {
            $form->distribution = $distribution->type;
        }

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $formData = $this->app()->request->getPost('UserSettingsForm');
        $form->setAttributes($formData);

        if ($formData && $form->validate()) {
            $user->password = $form->newPassword;
            $user->is_system_subscribe = 1;
            $user->is_subscribe = $form->isWishPromoMail;

            if ($user->save()) {
                $form->unsetAttributes(['password', 'newPassword', 'confirmPassword']);
                //записываем юзера в выбранный список рассылок
                $shopWebUser->_saveDistribution($form->distribution);
                $this->app()->user->setFlash('message', $this->t('Data changed'));
            } else {
                $this->app()->user->setFlash('message', $this->t('Error updating data'));
            }
        }

        $this->render('settings', ['form' => $form]);
    }

    public function actionInvite()
    {
        $invite = new InviteRecord();
        $model = $this->app()->user->getModel();
        $provider = InviteRecord::model()->userId($model->id)->getDataProvider(false, [
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('invite', ['invite' => $invite, 'model' => $model, 'provider' => $provider]);
    }

    /**
     * filter ajaxOnly
     */
    public function actionInviteByEmail()
    {
        $invite = new InviteRecord();
        $model = $this->app()->user->getModel();

        $inviteData = $this->app()->request->getPost('InviteRecord');
        $invite->setAttributes($inviteData);
        $invite->user_id = $model->getPrimaryKey();
        //TODO: notification recover

        if ($invite->save()) {
            $mail = $this->app()->mailer;
            $mail->create(
                [$this->app()->params['layoutsEmails']['inviteByEmail'] => $this->app()->params['distributionEmailName']],
                $invite->email,
                $this->t('Invitation to {siteName} from {emailFrom}', ['{siteName}' => $this->app()->name, '{emailFrom}' => $model->email]),
                ['view' => 'inviteByEmail', 'data' => ['inviteEmail' => $model->email, 'user' => $model]]
            );
            $this->app()->end();
        }

        echo CActiveForm::validate($invite);
    }

    public function actionOrders()
    {
        $model = $this->app()->user->getModel();
        $provider = new CArrayDataProvider(
            OrderRecord::model()
                ->userId($model->id)
                ->limit(20)
                ->orderBy('id', 'desc')
                ->cache(10)
                ->with('positions', 'positions.event', 'positions.product')
                ->findAll()
        );

        $this->render('orders', [
            'model' => $model,
            'provider' => $provider,
        ]);
    }

    public function actionBonuses()
    {
        $model = $this->app()->user->getModel();
        $totalBonuses = $this->app()->user->bonuspoints->getAvailableToSpend();

        $provider = new CArrayDataProvider(
            UserBonuspointsRecord::model()
                ->userId($model->id)
                ->limit(20)
                ->orderBy('id', 'desc')
                ->cache(10)
                ->findAll()
        );

        $this->render('bonuses', [
            'model' => $model,
            'provider' => $provider,
            'totalBonuses' => $totalBonuses,
        ]);
    }

    public function actionUnsubscribe()
    {
        $user = $this->app()->user->getModel();
        $user->is_system_subscribe = 0;
        if ($user->save(true, ['is_system_subscribe'])) {
            $this->redirect(['message/unsubscribe']);
        }

        $message = $this->t('An error occurred while unsubscribing from newsletters');
        $this->redirect(['message/info',
                'title' => $this->t('Unsubscribe from newsletters'),
                'message' => $message,
                'description' => $this->t('An unknown error occurred while unsubscribing from the mailing list'),
                'hash' => $this->app()->tokenManager->getToken($message),
            ]
        );
    }

    public function actionUpdateDistribution($type)
    {
        $response = new ReplyResult();

        if (!in_array($type, array_keys(UserDistributionRecord::typeReplacements()))) {
            $response->addError($this->t('Error selecting subscription type'));
        }
        $user = $this->app()->user;
        /** @var $user \MommyCom\YiiComponent\AuthManager\ShopWebUser */
        $user->_saveDistribution($type);
        $response->setSuccess(true);
        $response->setInfo($this->t('Subscription completed successfully'));
        $this->renderJson($response->toArray());
    }
}
