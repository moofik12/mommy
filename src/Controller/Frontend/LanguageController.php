<?php

namespace MommyCom\Controller\Frontend;

use MommyCom\Service\BaseController\Controller;

class LanguageController extends Controller
{
    const FRONTEND_TEXTS = [
        'Adding items to the shopping cart',
        'Adding products is blocked.',
        'Adding to the shopping cart on the product page',
        'An error occurred, loading the list of products failed. Please refresh the page.',
        'An unknown error occurred. Try refresh the page and add again.',
        'Choose a different type of delivery',
        'Dear Customer, you have expired time for order confirmation. Please re-add the item to the shopping cart.',
        'Enter promotional code',
        'Go to "People who are viewing this product were also interested"',
        'In Fast delivery orders, the promotional code is not active',
        'Incorrect code',
        'Invitation have been sent to specified address',
        'Invitation sent to specified address',
        'Network error',
        'Network error. Check the internet connection and try adding the item again.',
        'Network error. Check your internet connection and try adding again.',
        'Not exist',
        'Not suitable for this item',
        'Now we will redirect you to the registration page. Registration takes only 20 seconds of your time!',
        'Opening a modal authorization window with a promo image',
        'Opening a modal window for entering E-mail when attempting to authorize via social network',
        'Opening a modal window for entering a E-mail when attempting to authorize via social network',
        'Opening a modal window for entering a password when attempting to authorize via social network',
        'Opening the modal authorization window during pre-registration',
        'Opening the modal authorization window when adding to the cart',
        'Please make another order if you want to buy more than 20 items.',
        'Please make another order if you want to buy more than 20 items..',
        'Prolongation of the reserve',
        'Promotional code is out of date',
        'Promotional code is too old',
        'Remove all',
        'Select another tab:',
        'Sorry, adding items has been blocked.',
        'Sorry, but the stock is over.',
        'Sorry, the item has just been reserved for another member.',
        'Sorry, the item was purchased by another member.',
        'Sorry, there was an unknown error. Try refreshing the page and try adding again.',
        'Sorry, you can not add a product with this quantity.',
        'Sorry, you need to log in to make purchases.',
        'Sorry, your shopping cart is full. One order can contain only 20 items.',
        'The request can not be processed. Contact Support.',
        'Time out on the order page',
        'To make purchases you need to register.',
        'Unknown error',
        'You can not use it',
        'You have already use it',
        'Your shopping cart is full. One order can contain a maximum of 20 items.',
        'day',
        'days',
        'ended',
        'hour',
        'hours',
        'minute',
        'minutes',
        'more than a week',
        'prolong the reservation',
        'second',
        'seconds',
    ];

    /**
     * @return array
     */
    public function accessRules()
    {
        return ['allow'];
    }

    public function actionDictionary()
    {
        header('Content-Type: application/javascript');
        echo 'I18n.dic = ' . json_encode($this->prepareDictionary(), JSON_UNESCAPED_UNICODE) . ';';
    }

    private function prepareDictionary(): array
    {
        $result = [];

        foreach (self::FRONTEND_TEXTS as $text) {
            $result[$text] = $this->t($text);
        }

        return $result;
    }
}
