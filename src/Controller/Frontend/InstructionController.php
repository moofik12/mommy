<?php

namespace MommyCom\Controller\Frontend;

use MommyCom\YiiComponent\Frontend\FrontController;

class InstructionController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules(): array
    {
        return [
            'allow',
        ];
    }

    /**
     * @return array
     */
    public function cache(): array
    {
        return [];
    }

    public function actionIndex()
    {
        $this->render('index', []);
    }
}
