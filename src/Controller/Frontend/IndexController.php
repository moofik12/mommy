<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CCache;
use CDbCacheDependency;
use MommyCom\Model\Db\CallbackRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Form\CallbackForm;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\SplitTesting;
use MommyCom\Service\TelegramBot;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;

class IndexController extends FrontController
{
    /**
     * Доступ открыт для actionInvite (реферальная ссылка)
     *
     * @return array
     */
    public function accessRules()
    {
        return ArrayUtils::merge([
            [
                'allow',
                'actions' => ['invite', 'index', 'callback'],
                'users' => ['?'],
            ],
        ], parent::accessRules());
    }

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(
            ['ajaxOnly + header'],
            parent::filters()
        );
    }

    /**
     * @return array
     */
    public function cache()
    {
        return [
            'index' => [
                'partialOnAjax' => false,
                'duration' => 0,
                'dependency' => new CDbCacheDependency("SELECT MAX(updated_at) FROM events"),
                'varyByExpression' => function () {
                    if ($this->app()->user->hasFlash('message')) {
                        $userId = $this->app()->user->isGuest ? '' : $this->app()->user->id;
                        return $this->app()->user->getFlash('message', false, false) . $userId . microtime(true);
                    }

                    /** @var Regions $regions */
                    $regions = $this->container->get(Regions::class);
                    $region = $regions->getUserRegion();

                    return $region->getRegionName() . $region->getDetectLevel();
                },
                'varyByLanguage' => true,
            ],
        ];
    }

    /**
     * @param string $act
     *
     * @throws \CException
     */
    public function actionIndex($act = '')
    {
        if ($act == 'update-mode-subscribe') {
            if (isset($_REQUEST['mode'])) {
                $this->updateModeSubscribe($_REQUEST['mode']);
            }
        }

        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        /** @var CCache $cache */
        $cache = $this->app()->cache;

        /** @var TelegramBot $telegramBot */
        $telegramBot = $this->container->get(TelegramBot::class);

        $variant = $splitTesting->getMadeInVariant();

        $provider = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->isVirtual(false)
            ->onlyVisible()
            ->onlyPublished()
            ->onlyTimeActive()
            ->isDeleted(false)
            ->orderBy('end_at', 'desc')
            ->orderBy('priority', 'asc')
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        /** @var array $telegramEventsParams */
        $telegramParams = $this->app()->params['telegram'];
        $telegramEventsParams = $telegramParams['events'];

        if ($telegramBot->getToken() &&
            false === $cache->get($telegramEventsParams['cache_key_telegram_bot_few_events']) &&
            $provider->getItemCount() < $telegramEventsParams['min_critical_events_count']
        ) {
            $telegramBot->sendMessage(
                $telegramParams['dream_team_telegram_chat_id'],
                sprintf('На сайте %s меньше %d акций!',
                    $this->app()->getRequest()->getHostInfo(),
                    $telegramEventsParams['min_critical_events_count']
                )
            );
            $cache->set(
                $telegramEventsParams['cache_key_telegram_bot_few_events'],
                '',
                $telegramEventsParams['cache_key_telegram_bot_few_events_ttl']
            );
        }

        $futureProvider = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->isVirtual(false)
            ->onlyVisible()
            ->onlyPublished()
            ->onlyTimeFuture()
            ->orderBy('start_at', 'asc')
            ->limit(15)
            ->cache(30)
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $events = $provider->getData();

        $this->render('index', [
            'events' => $events,
            'provider' => $provider,
            'futureProvider' => $futureProvider,
        ]);
    }

    public function actionInvite($id)
    {
        $id = intval($id);

        if (!$this->app()->user->isGuest) {
            $this->redirect(['index/index']);
        }

        $this->app()->user->setState('invitedBy', $id);
        $url = $this->createUrl('auth/auth', ['name' => 'registration']);
        $this->redirect($url);
    }

    /**
     * обратный звонок, поддержка только ajax
     */
    public function actionCallback()
    {
        if (!$this->app()->request->isAjaxRequest) {
            $this->app()->end();
        }

        $form = new CallbackForm();
        $result = ['success' => false, 'message' => ''];

        if ($this->app()->request->getPost('ajax', false)) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $data = $this->app()->request->getPost('CallbackForm', []);
        $form->attributes = $data;

        if ($form->validate()) {
            $model = new CallbackRecord();
            $model->telephone = $form->telephone;
            $model->user_id = $this->app()->user->id;
            $model->status = CallbackRecord::STATUS_NOT_PROCESSED;

            if ($model->save()) {
                $result['success'] = true;
            } else {
                $result['message'] = $model->getErrors();
            }
        }

        $this->renderJson($result);
    }

    /**
     * @throws \CException
     */
    public function actionHeader()
    {
        $this->renderPartial('//layouts/_mainHeader');
    }

    /**
     * @param $codeMessage
     * @param string $default
     *
     * @return mixed|string
     */
    private function getMessage($codeMessage, $default = '')
    {
        $messages = [
            UserDistributionRecord::TYPE_EVERY_DAY => $this->t('Now you will receive newsletters on new promotions as they are launched'),
            UserDistributionRecord::TYPE_EVERY_WEEK => $this->t('Now you will receive our newsletters much less often'),
        ];

        return isset($messages[$codeMessage]) ? $messages[$codeMessage] : $default;
    }

    private function updateModeSubscribe($mode)
    {
        $mode = Cast::toInt($mode);
        $user = $this->app()->user;
        /** @var $user ShopWebUser */
        if (in_array($mode, array_keys(UserDistributionRecord::typeReplacements())) && !$user->isGuest) {
            if ($user->_saveDistribution($mode)) {
                $message = $this->getMessage($mode);
                $user->setFlash('message', $message);
            }
        }
    }
}
