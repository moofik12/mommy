<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\YiiComponent\Frontend\FrontController;

class StaticController extends FrontController
{

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    public function actionIndex($category, $page)
    {
        $category = StaticPageCategoryRecord::model()->url($category)->find();
        $pages = StaticPageRecord::model()->url($page)->findAll(['order' => 'created_at ASC']);

        if (empty($pages) || $category === null) {
            throw new CHttpException(404, $this->t('Page not found'));
        }

        $this->render('index', [
            'pages' => $pages,
            'category' => $category,
        ]);
    }

}
