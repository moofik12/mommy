<?php

namespace MommyCom\Controller\Frontend;

use CHtml;
use CHttpException;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\TokenManager;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class CartController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + QuickCard, AjaxUpdate, AjaxAdd, AjaxDelete, AjaxList, AjaxPromocodeDiscount']);
    }

    public function actionIndex()
    {
        $this->redirect(['order/index', '#' => 'content'] + $this->app()->splitTesting->getAvailable());
    }

    public function actionDelete($eventId, $productId, $size)
    {
        $product = EventProductRecord::model()->eventId($eventId)->productId($productId)->size($size)->find();
        /* @var $product EventProductRecord */

        if ($product !== null) {
            $this->app()->user->cart->remove($product->id);
        }

        $this->redirect(['cart/index']);
    }

    public function actionAjaxUpdate($token, $eventId, $productId, $size)
    {
        $this->app()->tokenManager->validateToken($productId, $token);
        $user = $this->app()->user;
        /* @var ShopShoppingCart $cart */
        $cart = $user->cart;

        $product = EventProductRecord::model()->eventId($eventId)->productId($productId)->size($size)->find();
        /* @var $product EventProductRecord */

        if ($product == null) {
            throw new CHttpException(404, 'Product not found');
        }

        $position = $this->app()->user->cart->itemAt($product->id);

        if (!$position instanceof CartRecord) {
            return;
        }
        /* @var $position CartRecord */

        $result = ['message' => '', 'success' => false];

        if (!$product->event->isTimeActive || !$product->event->isStatusPublished) {
            $result['message'] = 'inactive_event';
        } elseif ($cart->isFull) {
            $result['message'] = 'cart_is_full';
        } elseif ($user->isBanned('cart')) {
            $result['message'] = 'cart_block';
        } else {
            $result['success'] = $position->updateReservation();
            if (!$result['success']) {
                $result['message'] = 'low_warehouse';
            }

            foreach ($cart->positions as $relativePosition) {
                $relativePosition->updateReservation();
            }
        }

        $this->renderJson($result);
    }

    public function actionQuickCart()
    {
        $this->renderPartial('//layouts/_mainHeader');
    }

    public function actionNumberDown($eventId, $productId, $size)
    {
        $product = EventProductRecord::model()->eventId($eventId)->productId($productId)->size($size)->find();
        /* @var $product EventProductRecord */

        if ($product !== null) {
            $this->app()->user->cart->put($product, -1);
        }

        $this->redirect(['cart/index']);
    }

    public function actionAjaxAdd($token, $eventId, $productId, $size = '', $number = 1)
    {
        $this->app()->tokenManager->validateToken($productId, $token);

        $eventId = Cast::toUInt($eventId);
        $productId = Cast::toUInt($productId);
        $size = Cast::toStr($size);
        $number = Cast::toUInt($number, 1, 1, 100);
        $wishNumber = $number;

        $result = ['message' => '', 'success' => false];
        $product = EventProductRecord::model()->eventId($eventId)->productId($productId)->size($size)->find();
        /* @var $product EventProductRecord */

        $user = $this->app()->user;

        if ($user->isGuest) {
            $user->setShowComebacker();
        }

        /* @var ShopShoppingCart $cart */
        $cart = $user->cart;
        $cartOrderIsFull = false;
        if ($product !== null) {
            $cartOrder = null;
            $position = $cart->getFromEventProduct($product);
            if ($position instanceof CartRecord) {
                $wishNumber = $position->number + $number;
            }

            if ($product->event->is_drop_shipping) {
                $cartOrder = $cart->getPossibleOrderFromSupplierId($product->event->supplier_id);
            } else {
                $cartOrder = $cart->getPossibleOwnOrder();
            }

            if ($cartOrder) {
                $cartOrderIsFull = $cartOrder->isFull;
            }
        }

        if ($size == '' && $product === null) {
            $result['message'] = 'wrong_size';
        } elseif ($product === null) {
            $result['message'] = 'product_not_exist';
        } elseif (!$product->event->isTimeActive || !$product->event->isStatusPublished) {
            $result['message'] = 'inactive_event';
        } elseif ($number == 0 || $wishNumber > $product->max_per_buy) {
            $result['message'] = 'wrong_number';
        } elseif ($cartOrderIsFull) {
            $result['message'] = 'cart_is_full';
        } elseif (!$product->getIsPossibleToBuy($number)) {
            $result['message'] = 'low_warehouse';
        } elseif ($user->isBanned('cart')) {
            $result['message'] = 'cart_block';
        } else {
            $result['success'] = $cart->put($product, $number);
        }

        if (!$result['success'] && empty($result['message'])) {
            $result['message'] = 'unknown_error';
        }

        $this->renderJson($result);
    }

    public function actionAjaxDelete($eventId, $productId, $size)
    {
        $result = ['message' => '', 'success' => false];

        $product = EventProductRecord::model()->eventId($eventId)->productId($productId)->size($size)->find();
        /* @var $product EventProductRecord */

        if ($product !== null) {
            $this->app()->user->cart->remove($product->id);
            $result['success'] = true;
        }

        if (!$result['success'] && empty($result['message'])) {
            $result['message'] = 'unknown_error';
        }

        $this->renderJson($result);
    }

    public function actionAjaxList()
    {
        $app = $this->app();
        $tf = $app->timerFormatter;
        $cart = $app->user->cart;
        $cn = $app->countries->getCurrency();

        /** @var TokenManager $tokenManager */
        $tokenManager = $this->container->get(TokenManager::class);

        $result = ['success' => true, 'message' => '', 'data' => []];

        foreach ($cart->getPositions() as $position) {
            $item = [
                'id' => $position->product->product_id,
                'eventId' => $position->event_id,
                'size' => $position->product->size,
                'name' => CHtml::encode($position->product->product->name),
                'number' => $position->number,
                'price' => $position->product->price,
                'currency' => $cn->getName('short'),
                'reservedTo' => $position->getReservedTo(),
                'reservedToW3C' => $tf->formatMachine($position->getReservedTo()),
                'thumbnails' => $position->product->product->logo->getAvailableThumbnailUrls(),
                'token' => $tokenManager->getToken($position->product->product_id),
                'urls' => [
                    'product' => $app->createUrl('product/index', ['id' => $position->product->product_id, 'eventId' => $position->event_id]),
                    'event' => $app->createUrl('event/index', ['id' => $position->event_id]),
                ],
            ];
            $result['data'][] = $item;
        }

        $this->renderJson($result);
    }

    public function actionAjaxPromocodeDiscount($token, $promocode)
    {
        $this->app()->tokenManager->validateToken('promocode', $token);

        $promocode = Utf8::trim(Cast::toStr($promocode));
        $result = ['message' => '', 'promocode' => $promocode, 'validUntil' => 0, 'discount' => 0, 'success' => false];

        $user = $this->app()->user;
        $cart = $user->cart;
        /* @var ShopShoppingCart $cart */
        if (!$cart->hasPossibleOwnOrder()) {
            $result['message'] = 'not_own_order';
            $this->renderJson($result);
        }

        if (!PromocodeRecord::checkPromocode($promocode)) {
            $result['message'] = 'invalid_promocode';
        } else {
            $promo = PromocodeRecord::model()->promocode($promocode)->find();
            /* @var $promo PromocodeRecord */

            if ($promo === null) {
                $result['message'] = 'not_exists';
            } elseif (!$promo->getIsTimeValid()) {
                $result['message'] = 'inactive';
            } elseif (!$promo->getIsUsable($user->id, false)) { // может ли быть использовано этим юзером
                $result['message'] = 'not_usable';
            } elseif ($promo->getIsUsed($user->id)) { // было ли использовано этим юзеров
                $result['message'] = 'already_used';
            } else {
                $positions = [];
                foreach ($cart->getPositionsAsOrders() as $cartOrder) {
                    if ($cartOrder->isOwn) {
                        $positions = array_merge($positions, $cartOrder->getReservedPositions());
                    }
                }

                $result['validUntil'] = $promo->valid_until;
                $result['discount'] = $promo->getDiscount($positions);
                if ($result['discount'] == 0) {
                    $result['message'] = 'not_suitable';
                } else {
                    $result['success'] = true;
                }
            }
        }

        if (!$result['success'] && empty($result['message'])) {
            $result['message'] = 'unknown_error';
        }

        $this->renderJson($result);
    }
}
