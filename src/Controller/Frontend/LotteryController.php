<?php

namespace MommyCom\Controller\Frontend;

use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\Frontend\FrontController;

class LotteryController extends FrontController
{
    /**
     * @var string
     */
    public $layout = '//layouts/lottery';

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['index', 'now'],
                'users' => ['?'],
            ],
            [
                'deny',
            ],
        ];
    }

    /**
     * у пользовтелей есть флаера
     */
    public function actionIndex()
    {
        $minBonusAvailable = 30;
        $maxBonusAvailable = 300;
        $this->app()->user->setLandingNum(21);

        $lotteryAvailableFrom = strtotime('01.01.2017');
        $lotteryAvailableTo = strtotime('28.02.2017 23:59:59');
        $time = time();

        if ($time < $lotteryAvailableFrom) {
            $this->app()->user->setFlash('message', $this->t('The lottery has not started yet, but we can always find you something else interesting'));
            $this->redirect($this->app()->homeUrl);
        } elseif ($time > $lotteryAvailableTo) {
            $this->app()->user->setFlash('message', $this->t('The lottery has already ended, but we can always find something else interesting'));
            $this->redirect($this->app()->homeUrl);
        }

        $formOfferRegistration = new RegistrationOfferForm();
        $formOfferRegistration->benefice = RegistrationOfferForm::BENEFICE_100;
        $formOfferRegistration->strategy = RegistrationOfferForm::STRATEGY_PROMOCODE;
        $formOfferRegistration->strategyPayments = RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY;

        $this->render('index', [
            "minBonus" => $minBonusAvailable,
            "maxBonus" => $maxBonusAvailable,
            "startTime" => $lotteryAvailableFrom,
            "endTime" => $lotteryAvailableTo,
            "formOfferRegistration" => $formOfferRegistration,
        ]);
    }

    /**
     * пользовтелей нет флаеров, реклама
     */
    public function actionNow()
    {
        $minBonusAvailable = 30;
        $maxBonusAvailable = 100;
        $this->app()->user->setLandingNum(20);

        $lotteryAvailableFrom = strtotime('-2 days');
        $lotteryAvailableTo = strtotime('+3 days');
        $time = time();

        if ($time < $lotteryAvailableFrom) {
            $this->app()->user->setFlash('message', $this->t('The lottery has not started yet, but we can always find you something else interesting'));
            $this->redirect($this->app()->homeUrl);
        } elseif ($time > $lotteryAvailableTo) {
            $this->app()->user->setFlash('message', $this->t('The lottery has already ended, but we can always find something else interesting'));
            $this->redirect($this->app()->homeUrl);
        }

        $formOfferRegistration = new RegistrationOfferForm();
        $formOfferRegistration->benefice = RegistrationOfferForm::BENEFICE_100;
        $formOfferRegistration->strategy = RegistrationOfferForm::STRATEGY_PROMOCODE;
        $formOfferRegistration->strategyPayments = RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY;
        $formOfferRegistration->secretCode = RegistrationOfferForm::SECRET_CODE_NOT_PRESENT;
        $formOfferRegistration->bannerName = 'lottery';

        $this->render('index', [
            "minBonus" => $minBonusAvailable,
            "maxBonus" => $maxBonusAvailable,
            "startTime" => $lotteryAvailableFrom,
            "endTime" => $lotteryAvailableTo,
            "formOfferRegistration" => $formOfferRegistration,
        ]);
    }

}
