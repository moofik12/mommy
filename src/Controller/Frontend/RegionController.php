<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use MommyCom\Service\BaseController\Controller;

class RegionController extends Controller
{
    public $pageTitle = 'Region and Language';
    public $description;
    public $showRegionConfirmPopup;
    public $showRegionChooseModal;

    /**
     * @param string $secret
     *
     * @throws CHttpException
     */
    public function actionIndex($secret = '')
    {
        if ('435e686ad91563578ee6d8519d41810e' !== $secret) {
            throw new CHttpException(404);
        }

        $this->render('index');
    }
}
