<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CArrayDataProvider;
use CHttpException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use MommyCom\YiiComponent\AuthManager\AuthenticationTrait;
use MommyCom\Entity\Order;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Form\RegistrationSimpleForm;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\SimilarProducts;
use MommyCom\Repository\OrderRepository;
use MommyCom\Security\Authentication\Encoder\UserPasswordEncoder;
use MommyCom\Security\User\UserBuilder;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\Service\PaymentGateway\AvailablePaymentGateways;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\TelegramBot;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Facade\Doctrine;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PromoCode;
use MommyCom\YiiComponent\ShopLogic\ShopOrderFactory;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\TokenManager;
use MommyCom\YiiComponent\Type\Cast;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OrderController extends FrontController
{
    use AuthenticationTrait;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['auth', 'index'],
                'users' => ['?'],
            ],
            [
                'allow',
                'actions' => ['index', 'delivery', 'success',
                    'ajaxFeedrProvinces', 'ajaxFeedrCities', 'ajaxFeedrSuburbs', 'ajaxFeedrAreas', 'ajaxFeedrRates',
                ],
                'users' => ['@'],
            ],
            [
                'deny',
            ],
        ];
    }

    public function actionIndex($promocode = '')
    {
        $user = $this->app()->user;
        $cart = $user->cart;
        /** @var  $cart ShopShoppingCart */
        $bonuspoints = $user->bonuspoints;
        /** @var  $bonuspoints \MommyCom\YiiComponent\ShopLogic\ShopBonuspoints */

        $promocode = Cast::toStr($promocode);
        $promo = PromocodeRecord::model()->promocode($promocode)->find();
        /* @var $promo PromocodeRecord */
        if ($promo === null || !$promo->getIsUsable($user->id)) {
            $promocode = '';
        }

        $positions = $cart->getPositions();
        $cartProducts = ArrayUtils::getColumn($positions, 'product');
        $similar = SimilarProducts::fromEventProducts($cartProducts);

        // получаем "другие товары"
        $similarEventProducts = $similar->getEventProducts();
        $otherProducts = GroupedProducts::fromProducts($similarEventProducts);

        $othersProvider = new CArrayDataProvider($otherProducts->toArray());

        $cost = $cart->getCost(false);
        $costTotal = $cart->getCost(true, $promocode);
        $promocodeSavings = $cart->getPromocodeSavings($promocode);
        $costWithPromocode = $cost - $promocodeSavings;
        $discountAmount = $cart->getUserDiscountSavings();
        $bonusAmount = $cart->getBonusesCost();
        $view = $cart->count == 0 ? 'indexEmpty' : 'index';

        if (!$this->app()->request->isAjaxRequest) {
            $this->render($view, [
                    'cart' => $cart,
                    'bonuspoints' => $bonuspoints,
                    'othersProvider' => $othersProvider,
                    'remainToFreeDelivery' => false,
                    'returnUrl' => $this->getSavedBackUrl(),
                    'promocode' => $promocode,
                    'promocodeSavings' => $promocodeSavings,
                    'costWithPromocode' => $costWithPromocode,
                    'bonusAmount' => $bonusAmount,
                    'discountAmount' => $discountAmount,
                    'costTotal' => $costTotal,
                ] + $this->app()->splitTesting->getAvailable());
        } else {
            $this->renderPartial($view, [
                    'cart' => $cart,
                    'bonuspoints' => $bonuspoints,
                    'othersProvider' => $othersProvider,
                    'remainToFreeDelivery' => false,
                    'returnUrl' => $this->getSavedBackUrl(),
                    'promocode' => $promocode,
                    'promocodeSavings' => $promocodeSavings,
                    'costWithPromocode' => $costWithPromocode,
                    'bonusAmount' => $bonusAmount,
                    'discountAmount' => $discountAmount,
                    'costTotal' => $costTotal,
                ] + $this->app()->splitTesting->getAvailable());
        }
    }

    public function actionDelivery($promocode = '')
    {
        $promocode = Cast::toStr($promocode);

        $request = $this->app()->request;
        /** @var ShopWebUser $user */
        $user = $this->app()->user;
        $cart = $user->cart;
        /** @var  $cart ShopShoppingCart */
        $bonuspoints = $user->bonuspoints;

        $cart->giveBonusTime();

        if (!$cart->isAvailableToBuy()) {
            $this->redirect(['cart/index']);
        }

        $orderAttributes = $request->getPost('ShopOrder', []);
        $orderAttributes[PromoCode::NAME] = $promocode;

        $order = ShopOrderFactory::createShopOrder($this->container);
        $order->setAttributes($orderAttributes);

        $isPostRequest = $request->isPostRequest;

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if ($request->isAjaxRequest) {
            $delivery = $availableDeliveries->getDelivery($order->deliveryType);
            $deliveryModel = $delivery->createFormModel()->setAttributesFromRequest($request);

            echo \CActiveForm::validate([$order, $deliveryModel]);
            $this->app()->end();
        }

        if ($isPostRequest && $order->validate()) {
            $delivery = $availableDeliveries->getDelivery($order->deliveryType);
            $deliveryModel = $delivery->createFormModel()->setAttributesFromRequest($request);

            $record = $deliveryModel->validate() ? $order->makeOrder($user, $deliveryModel) : null;

            if (null !== $record) {
                /** @var TelegramBot $telegramBot */
                $telegramBot = $this->container->get(TelegramBot::class);

                /** @var EntityManager $em */
                $em = $this->container->get('doctrine')->getManager();

                /** @var OrderRepository $orderRepository */
                $orderRepository = $em->getRepository(Order::class);

                if ($telegramBot->getToken()) {
                    $telegramBot->sendMessage(
                        getenv('DAILY_REPORT_TELEGRAM_CHAT_ID'),
                        sprintf(
                            'Поступил новый заказ! (%s), всего поступило заказов: %d',
                            getenv('REGION'),
                            $orderRepository->getOrdersCountForTest()
                        )
                    );
                }

                /** @var TokenManager $tokenManager */
                $tokenManager = $this->container->get(TokenManager::class);
                $successToken = $tokenManager->getToken('successAction');
                $this->redirect(['order/success', 'id' => $record->id, 'token' => $successToken, '#' => 'content'] + $this->app()->splitTesting->getAvailable());
            }
        }

        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);
        $region = $regions->getServerRegion()->getRegionName();
        $countryCode = (string)current(Region::COUNTRY_CODES[$region]);

        $deliveryType = $order->deliveryType ?: $availableDeliveries->getFirstDelivery()->getId();

        $this->render('delivery', [
            'cart' => $cart,
            'bonuspoints' => $bonuspoints,
            'promocode' => $promocode,
            'order' => $order,
            'deliveryType' => $deliveryType,
            'availableDeliveries' => $availableDeliveries,
            'countryCode' => $countryCode,
        ]);
    }

    /**
     * @param string $id
     * @param string $token
     *
     * @throws CHttpException
     */
    public function actionSuccess($id, $token)
    {
        /** @var TokenManager $tokenManager */
        $tokenManager = $this->container->get(TokenManager::class);

        if (!$tokenManager->isValid('successAction', $token)) {
            $this->redirect(['order/index']);
        }

        $uid = trim($id);
        $order = current(OrderRecord::model()->findByUid($uid));

        if (!$order instanceof OrderRecord) {
            throw new CHttpException(404, $this->t('The specified order was not found'));
        }

        if ($order->user_id !== $this->app()->user->id) {
            throw new CHttpException(400);
        }

        /** @var AvailablePaymentGateways $availablePaymentGateways */
        $availablePaymentGateways = $this->container->get(AvailablePaymentGateways::class);

        $this->render('success', [
            'order' => $order,
            'uid' => $uid,
            'canPrepay' => $order->isAvailableForPayment(),
            'availableGateways' => $availablePaymentGateways->getPaymentGateways(),
        ]);
    }

    /**
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrProvinces($filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $provinces = $feedrApi->getProvinces();
        $provinces = $this->filterByAttribute($provinces, 'name', trim($filter));

        $this->renderJson($provinces);
    }

    /**
     * @param string $provinceId
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrCities($provinceId, $filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $cities = $feedrApi->getCities((int)$provinceId);
        $cities = $this->filterByAttribute($cities, 'name', trim($filter));

        $this->renderJson($cities);
    }

    /**
     * @param string $cityId
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrSuburbs($cityId, $filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $suburbs = $feedrApi->getSuburbs((int)$cityId);
        $suburbs = $this->filterByAttribute($suburbs, 'name', trim($filter));

        $this->renderJson($suburbs);
    }

    /**
     * @param string $suburbId
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrAreas($suburbId, $filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $areas = $feedrApi->getAreas((int)$suburbId);
        $areas = $this->filterByAttribute($areas, 'name', trim($filter));

        $this->renderJson($areas);
    }

    /**
     * @param $areaId
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrRates($areaId)
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        /** @var ShopWebUser $user */
        $user = $this->app()->user;
        $cart = $user->cart;

        $weight = $cart->getWeight() / 1000;
        $cost = $cart->getCost();

        $domesticRates = $feedrApi->getDomesticRates((int)$areaId, $weight, 30, 20, 10, $cost, false);
        $domesticRatesCod = $feedrApi->getDomesticRates((int)$areaId, $weight, 30, 20, 10, $cost, true);

        $this->renderJson([
            'regularWithoutCod' => $this->prepareDomesticRates($domesticRates['regular'] ?? []),
            'expressWithoutCod' => $this->prepareDomesticRates($domesticRates['express'] ?? []),
            'regularWithCod' => $this->prepareDomesticRates($domesticRatesCod['regular'] ?? []),
            'expressWithCod' => $this->prepareDomesticRates($domesticRatesCod['express'] ?? []),
        ]);
    }

    /**
     * @param array $rates
     *
     * @return array
     */
    private function prepareDomesticRates(array $rates): array
    {
        usort($rates, function (array $a, array $b) {
            return $a['finalRate'] - $b['finalRate'];
        });

        array_walk($rates, function (array &$item) {
            $item = [
                'id' => $item['rate_id'],
                'name' => implode(' - ', [$item['name'], $item['rate_name'], $item['finalRate'] . ' IDR']),
            ];
        });

        return $rates;
    }

    /**
     * @param array $data
     * @param string $attribute
     * @param string $filter
     *
     * @return array
     */
    private function filterByAttribute(array $data, string $attribute, string $filter): array
    {
        if ('' === $filter) {
            return $data;
        }

        $result = [];
        foreach ($data as $item) {
            if ('' === $filter || false !== strpos(mb_strtolower($item[$attribute]), mb_strtolower($filter))) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @throws \CException
     */
    public function actionAuth()
    {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $form = new RegistrationSimpleForm();

        if ($request->isXmlHttpRequest() && $request->get('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->get('RegistrationSimpleForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                /** @var EntityManagerInterface $entityManager */
                $entityManager = $this->container->get('doctrine')->getManager();
                /** @var UserBuilder $userBuilder */
                $userBuilder = $this->container->get(UserBuilder::class);
                /** @var EmailUtils $emailUtils */
                $emailUtils = $this->container->get(EmailUtils::class);

                $user = $userBuilder
                    ->setEmail($form->email)
                    ->build();
                $entityManager->persist($user);
                $entityManager->flush();

                $senderEmail = $this->app()->params['layoutsEmails']['welcome2'];
                $senderName = $this->app()->params['distributionEmailName'];
                $emailUtils->sendRegistrationEmail($user, $userBuilder->getPassword(), $senderEmail, $senderName);

                /** @var TokenStorageInterface $tokenStorage */
                $tokenStorage = $this->container->get('security.token_storage');
                $providerKey = $this->container->getParameter('frontend.firewall_key');

                if ($this->authenticateUser($tokenStorage, $user, $providerKey)) {
                    $this->redirect($this->createUrl('order/delivery'));
                }
            }
        }

        $this->render('auth', [
            'registrationForm' => $form,
            'percent' => 0,
        ]);
    }
}
