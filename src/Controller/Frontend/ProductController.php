<?php

namespace MommyCom\Controller\Frontend;

use CArrayDataProvider;
use CHttpException;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\LastViewedProducts;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;

class ProductController extends FrontController
{
    const MIN_LAST_VIEWED_FOR_SIMILAR = 3;
    const OTHER_PRODUCT_SIZE = 6;

    /**
     * @var bool
     */
    public $showPromoLogin = false;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    public function actionIndex($eventId, $id)
    {
        $request = $this->app()->request;
        $user = $this->app()->user->model;
        $eventId = Cast::toUInt($eventId);
        $id = Cast::toUInt($id);
        $size = $this->app()->request->getParam('size', '');

        $event = EventRecord::model()->findByPk($eventId);
        /* @var $event EventRecord */

        if ($event === null || $event->is_deleted) {
            throw new CHttpException(404, $this->t('This promotion was not found'));
        }

        if (!$event->isStatusPublished) {
            throw new CHttpException(400, $this->t('Sorry, this promotion is temporarily unavailable'));
        }

        if (!$event->is_virtual) {
            $this->app()->user->setReturnUrl($this->createUrl('event/index', ['id' => $eventId]));
        }

        $returnUrl = $this->app()->createUrl('event/index', ['id' => $event->id]);
        $lastRoute = $this->app()->request->getRouteFromReferrer($request->urlReferrer);

        if ($lastRoute !== false) {
            if ($lastRoute === 'event/index') {
                $returnUrl = $request->urlReferrer;
            }
        }

        $productGroup = $event->getGroupedProduct($id);

        if ($productGroup === null) {
            throw new CHttpException(404, $this->t('This product was not found'));
        }

        if ($user !== null) {
            $productGroup->trackView(); // отслеживание просмотров
        }

        // получаем "другие товары акции"
        $otherEventProducts = [];
        $otherProductIds = [];
        $othersIsSimilar = false;

        if ($user !== null) {
            $otherEventProducts = LastViewedProducts::fromUser($user, true, true, 10)->getEventProducts();
            foreach ($otherEventProducts as $index => $otherEventProduct) {
                if ($otherEventProduct->product_id == $productGroup->product->id) {
                    unset($otherEventProducts[$index]);
                    continue;
                }
                $otherProductIds[$otherEventProduct->product_id] = $otherEventProduct->product_id;
            }
        }

        $otherProductCount = count($otherProductIds);
        if ($user === null || $otherProductCount < self::MIN_LAST_VIEWED_FOR_SIMILAR) {
            $othersIsSimilar = true;

            if ($otherProductCount < self::OTHER_PRODUCT_SIZE) {
                $otherEventProducts = array_merge(
                    $otherEventProducts,
                    $productGroup->getSimilarProducts(true, self::OTHER_PRODUCT_SIZE - $otherProductCount, 60)->getEventProducts()
                );
            }
        }
        $otherProducts = GroupedProducts::fromProducts($otherEventProducts);

        /*
        if ($othersIsSimilar && count($otherProducts) < self::OTHER_PRODUCT_SIZE) { // magic
            $otherProducts->addProducts(
                EventProductRecord::model()
                    ->eventId($eventId)
                    ->orderByRand()
                    ->limit(50)
                    ->cache(120)
                    ->findAll()
            );
        }
        // end
        */

        $othersProvider = new CArrayDataProvider($otherProducts->toArray());

        $this->description = str_replace('"', "'", $productGroup->product->name . ' ' . $productGroup->product->brand->name
            . ' - ' . $this->app()->params['distributionEmailName'] . ' ' . strip_tags($productGroup->product->description));

        $this->render('index', [
            'event' => $event,
            'productGroup' => $productGroup,
            'othersProvider' => $othersProvider,
            'othersIsSimilar' => $othersIsSimilar,
            'size' => $size,
            'returnUrl' => $returnUrl,
        ]);
    }

    public function actionByArticle($id)
    {
        $id = Cast::toUInt($id);
        $product = EventProductRecord::model()->findByPk($id);

        /* @var $product EventProductRecord */

        if ($product === null) {
            throw new CHttpException(404, $this->t('This promotion was not found'));
        }

        $this->redirect(['product/index', 'id' => $product->product_id, 'eventId' => $product->event_id]);
    }
}
