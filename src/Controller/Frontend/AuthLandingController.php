<?php

namespace MommyCom\Controller\Frontend;

use CActiveForm;
use CArrayDataProvider;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Frontend\FrontController;

class AuthLandingController extends FrontController
{
    /**
     * @var string
     */
    public $layout = '//layouts/login';

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            /*array(
                'allow',
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array(
                'deny',
            ),*/
            'allow',
        ];
    }

    /**
     * @param string $name
     */
    public function actionIndex($name = 'registration')
    {
        $request = $this->app()->request;

        if (!$this->app()->user->isGuest) {
            $this->redirect(['index/index']);
        }

        //$this->saveBackUrl();

        $tabs = [
            'login',
            'registration',
        ];

        if (!in_array($name, $tabs)) {
            $name = 'login';
        }

        $authForm = new AuthForm();

        //processing     AuthController::actionRegistration()
        $registrationForm = new UserRecord('register');

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($authForm);
            $this->app()->end();
        }

        $dataAuthForm = $request->getPost('AuthForm');
        if (!empty($dataAuthForm)) {
            $authForm->setAttributes($dataAuthForm);
            if ($authForm->validate() && $authForm->login()) {
                $this->redirectBackOr(['index/index']);
            }
        }
        $authForm->unsetAttributes(['password']);

        // extract products
        $activeEvents = EventRecord::model()
            ->limit(10)
            ->onlyTimeActive()
            ->onlyPublished()
            ->orderByRand()
            ->cache(60)
            ->findAll();

        $products = GroupedProducts::fromProducts(
            EventProductRecord::model()
                ->limit(100)
                ->eventIdIn(ArrayUtils::getColumn($activeEvents, 'id'))
                ->orderByRand()
                ->with('event', 'product')
                ->cache(60)
                ->findAll()
        );

        $productsArray = $products->toArray();
        shuffle($productsArray);
        $productsProvider = new CArrayDataProvider($productsArray);

        $this->render('auth', [
            'tab' => $name,
            'authForm' => $authForm,
            'registrationForm' => $registrationForm,
            'productsProvider' => $productsProvider,
        ]);
    }
}
