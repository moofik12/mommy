<?php

namespace MommyCom\Controller\Frontend;

use CException;
use CHttpCookie;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Frontend\FrontController;

class SplitTestingController extends FrontController
{
    const TEST_UNFORMED_CART = 'unformed-cart-alien';

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + unformedCart']);
    }

    /**
     * Установка параметра показа баннера
     *
     * @throws CException
     */
    public function actionUnformedCart()
    {
        $cookiesName = self::TEST_UNFORMED_CART;
        $cookies = $this->app()->request->getCookies();
        if ($cookies->itemAt($cookiesName) === null) {
            $cookiesParams = new CHttpCookie($cookiesName, 1, [
                'expire' => strtotime('+3 days'),
            ]);

            $cookies->add($cookiesName, $cookiesParams);
        }
    }

    /**
     * Добавление метки для ГА
     *
     * @param $redirectUrl
     */
    public function actionUnformedCartActivate($redirectUrl)
    {
        //метка чтобы показать что он переходил
        $this->app()->splitTesting->getNum('unformed-cart-active', 1);

        $this->_redirect($redirectUrl);
    }

    /**
     * @param $redirectUrl
     */
    public function actionProductViewDetailActivate($redirectUrl)
    {
        //метка чтобы показать что он нажал при просмотре сделанных заказов
        $this->app()->splitTesting->getNum('product-detail-active', 1);

        $this->_redirect($redirectUrl);
    }

    /**
     * Редирект с добавлением параметров сплит тестов
     *
     * @param $redirectUrl
     */
    protected function _redirect($redirectUrl)
    {
        $url = urldecode($redirectUrl);
        $urlMas = parse_url($url);

        $path = 'index/index';
        $params = [];

        if (isset($urlMas['query'])) {
            parse_str($urlMas['query'], $params);
        }

        if (isset($urlMas['path'])) {
            $path = $urlMas['path'];
        } else {
            //что-то пошло не так на страницу рендера
            if (empty($url)) {
                $url = $this->createUrl($path);
            }

            $this->redirect($url);
        }

        $this->redirect($path . '?' . http_build_query(array_merge($params, $this->app()->splitTesting->getAvailable())));
    }
}
