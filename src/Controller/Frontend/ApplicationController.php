<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use CLogger;
use MommyCom\Model\Db\ApplicationSubscriberRecord;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class ApplicationController extends FrontController
{
    /**
     * @var int валидное время в секудах при котором действует хеш
     */
    public $validTime = 60;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    public function actionWebToken($endpoint)
    {
        $time = time();
        $result = [
            'token' => $this->_getWebHash($endpoint, $time),
            'time' => $time,
        ];

        $this->renderJson($result);
    }

    public function actionSubscribeWeb($endpoint, $token, $time)
    {
        $time = Cast::toUInt($time);
        $result = ['success' => false, 'message' => ''];

        if (!$this->_validateWebHash($endpoint, $token, $time)) {
            $result['message'] = 'incorrect token';
        }

        /** @var ApplicationSubscriberRecord $model */
        $model = ApplicationSubscriberRecord::model()->
        device($endpoint)->
        typeIs(ApplicationSubscriberRecord::TYPE_WEB)->
        find();

        if ($model) {
            //update
            $model->client_gcm_id = $endpoint;
        } else {
            $model = new ApplicationSubscriberRecord();
            $model->client_id = ApplicationSubscriberRecord::generateWebClientId();
            $model->type = ApplicationSubscriberRecord::TYPE_WEB;
            $model->client_gcm_id = $endpoint;
            $model->version_code = 1;
            $model->version_name = '1.0.0';
        }

        if ($model->save()) {
            $result['success'] = true;
        } else {
            Yii::log($this->t('Web application subscription error') . ": \n" . print_r($result), CLogger::LEVEL_ERROR, 'system.subscribeWeb');
        }

        Yii::log($this->t('Web application subscription') . ": \n" . print_r($result), CLogger::LEVEL_WARNING, 'system.subscribeWeb');
        $this->renderJson($result);
    }

    public function actionWebMessage($endpoint)
    {
        /** @var ApplicationSubscriberRecord $model */
        $model = ApplicationSubscriberRecord::model()->
        device($endpoint)->
        typeIs(ApplicationSubscriberRecord::TYPE_WEB)->
        find();

        $result['subject'] = '';
        $result['body'] = '';
        $result['url'] = '';

        if ($model === null) {
            throw new CHttpException(404, 'Not found');
        }

        $result['subject'] = $model->last_subject;
        $result['body'] = $model->last_body;
        $result['url'] = $model->last_url;

        $this->renderJson($result);
    }

    protected function _getWebHash($endpoint, $time)
    {
        static $_internalSalt = "jd034l;dknd";

        $protectKey = $time . $_internalSalt;
        return hash('sha256', $endpoint . $protectKey);
    }

    protected function _validateWebHash($endpoint, $hash, $timeGenerate)
    {
        $time = time();
        if (abs($time - $timeGenerate) > $this->validTime) {
            return false;
        }

        $hash1 = $this->_getWebHash($endpoint, $timeGenerate);
        return $hash == $hash1;
    }
}
