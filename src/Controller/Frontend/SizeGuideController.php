<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use MommyCom\Model\Product\SizeGuideGroups;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;

class SizeGuideController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules(): array
    {
        return [
            [
                'allow',
            ],
        ];
    }

    /**
     * @param mixed $name
     *
     * @throws CHttpException
     */
    public function actionTarget($name)
    {
        $target = Cast::toStr($name);
        $sizeGuide = SizeGuideGroups::instance();
        $category = $sizeGuide->getCategory($target);
        $targetGroup = $sizeGuide->getGroupTarget($category);

        $this->_redirect($targetGroup);
    }

    /**
     * @param mixed $name
     *
     * @throws CHttpException
     */
    public function actionGroup($name)
    {
        $group = Cast::toStr($name);

        $this->_redirect($group);
    }

    public function actionChildren()
    {
        $this->pageTitle = $this->t('Dimension mesh for children\'s clothes');

        $this->render('children');
    }

    public function actionMan()
    {
        $this->pageTitle = $this->t('Dimension mesh for men\'s clothing');

        $this->render('man');
    }

    public function actionWoman()
    {
        $this->pageTitle = $this->t('Dimension mesh for women\'s clothing');

        $this->render('woman');
    }

    public function actionShoes()
    {
        $this->pageTitle = $this->t('Shoe mesh size');

        $this->render('shoes');
    }

    /**
     * @param string $targetGroup
     *
     * @throws CHttpException
     */
    private function _redirect(string $targetGroup)
    {
        if ($targetGroup == SizeGuideGroups::TARGET_GROUP_CLOTH_MAN) {
            $this->redirect(['man']);
        } elseif ($targetGroup == SizeGuideGroups::TARGET_GROUP_CLOTH_WOMAN) {
            $this->redirect(['woman']);
        } elseif ($targetGroup == SizeGuideGroups::TARGET_GROUP_CLOTH_CHILDREN) {
            $this->redirect(['children']);
        } elseif ($targetGroup == SizeGuideGroups::TARGET_GROUP_FOOTWEAR) {
            $this->redirect(['shoes']);
        }

        throw new CHttpException(200, $this->t('No tariff scale found'));
    }
}
