<?php

namespace MommyCom\Controller\Frontend;

use CHttpException;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Type\Cast;

class MessageController extends FrontController
{
    /**
     * @param string $hash
     * @param string $title
     * @param string $message
     * @param string $description
     *
     * @throws CHttpException
     */
    public function actionInfo($hash, $title, $message, $description = '')
    {
        $this->app()->tokenManager->validateToken($message, $hash);

        $title = Cast::toStr($title);
        $message = Cast::toStr($message);
        $description = Cast::toStr($description);

        $this->render('info', [
            'title' => $title,
            'message' => $message,
            'description' => $description,
        ]);
    }

    /**
     * @param string $hash
     * @param string $title
     * @param string $message
     * @param string $description
     *
     * @throws CHttpException
     */
    public function actionBonus($hash, $title, $message, $description = '')
    {
        $this->app()->tokenManager->validateToken($message, $hash);

        $title = Cast::toStr($title);
        $message = Cast::toStr($message);
        $description = Cast::toStr($description);

        $this->render('bonus', [
            'title' => $title,
            'message' => $message,
            'description' => $description,
        ]);
    }

    public function actionUnsubscribe()
    {
        $user = $this->app()->user->getModel();

        $isSystemSubscribe = !!$user->is_system_subscribe;
        $isDistributionSubscribe = !!$user->is_subscribe;

        $this->render('unsubscribe', [
            'isSystemSubscribe' => $isSystemSubscribe,
            'isDistributionSubscribe' => $isDistributionSubscribe,
        ]);
    }
}
