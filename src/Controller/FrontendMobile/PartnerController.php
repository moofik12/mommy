<?php

namespace MommyCom\Controller\FrontendMobile;

use CActiveForm;
use CLogger;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\PartnerRegistrationForm;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\FrontendMobile\ReplyResult;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class PartnerController extends FrontController
{
    /**
     * @var string
     */
    public $layout = '//layouts/partner';

    /**
     * @var string
     */
    public $bodyClass = '';

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    /**
     * @param string $source
     *
     * @throws \CException
     */
    public function actionLanding($source = 'default')
    {
        if (mb_strlen($source) > 16) {
            $this->redirect($this->createUrl('index/index'));
        }
        $request = $this->app()->request;
        $webUser = $this->app()->user;
        /** @var $webUser ShopWebUser */
        $response = new ReplyResult();

        $model = new PartnerRegistrationForm($webUser->getIsGuest() ? 'newUser' : '');
        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        $dataForm = $request->getPost('PartnerRegistrationForm');
        if (!empty($dataForm)) {
            $model->setAttributes($dataForm);
            if ($model->validate()) {
                if ($webUser->isGuest) {
                    //нового регистрируем
                    $newPassword = Random::alphabet(8, Random::DIGIT);

                    $user = new UserRecord();
                    $user->email = $model->email;
                    $user->password = $newPassword;

                    if ($user->save()) {
                        //добавление пользователя который пригласил
                        $user->refresh();

                        $webUser->_saveDistribution();

                        $invitedBy = $webUser->getState('invitedBy', 0);
                        if ($invitedBy && $webUser->isGuest) {
                            $user->setScenario('invitedBy');
                            $user->invited_by = $invitedBy;
                            $user->save(true, ['invited_by']);
                        }

                        $offerProvider = $webUser->getOfferProvider();
                        if ($offerProvider != UserRecord::OFFER_PROVIDER_NONE) {
                            $offerId = $webUser->getOfferId();
                            $offerTargetUrl = $webUser->getOfferTargetUrl();

                            $user->landing_num = $webUser->getLandingNum();
                            $user->offer_provider = $offerProvider;
                            $user->offer_id = $offerId;
                            $user->offer_target_url = $offerTargetUrl;
                            $user->save(true, ['offer_provider', 'offer_id', 'offer_target_url', 'landing_num']);
                        }
                        $user->refresh();

                        $this->app()->mailer->create(
                            [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                            [$user->email],
                            $this->t('Welcome to the shopping club mommy.com'),
                            [
                                'view' => 'welcome2',
                                'data' => ['model' => $user, 'password' => $newPassword],
                            ]
                        );

                        $authForm = new AuthForm();
                        $authForm->email = $model->email;
                        $authForm->password = $newPassword;

                        if (!$authForm->login()) {
                            $response->addError('Unsuccessful attempt at registration. Contact the technical support site.');
                            Yii::log("PartnerController: Ошибка авторизации нового пользователя {$model->email} . Ошибки модели: " . implode($user->errors), CLogger::LEVEL_ERROR);
                        }
                    } else {
                        $response->addError('Could not register. Contact the technical support site.');
                        Yii::log("PartnerController: Ошибка при регистрации нового пользователя {$model->email} . Ошибки модели: " . implode($user->errors), CLogger::LEVEL_ERROR);
                    }
                }
                if (!$webUser->isGuest) {
                    $partner = UserPartnerRecord::model()->userId($webUser->getModel()->id)->find();
                    if ($partner === null) {
                        $partner = new UserPartnerRecord();
                        $partner->user_id = $webUser->getModel()->id;
                        $partner->is_sent_unisender = 0;
                        $partner->source = $source;

                        $allowed = $this->app()->params['newsletters'] ?? false;

                        if ($partner->save()) {
                            $response->setSuccess('true');

                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['newPartner'] => $this->app()->params['distributionEmailName']],
                                    [$webUser->getModel()->email],
                                    'Your application for participation in the affiliate program was successfully registered',
                                    [
                                        'view' => 'newPartner',
                                        'data' => ['user' => $webUser->getModel()],
                                    ]
                                );
                            }
                        }
                    } else {
                        $response->addError('You are already registered in the affiliate program!');
                    }
                }
            }
        }

        if ($request->isAjaxRequest) {
            $this->renderJson($response->toArray());
        }

        $this->render('landing', [
            'source' => $source,
            'model' => $model,
        ]);
    }

    public function actionSuccess()
    {
        $this->bodyClass = 'body-finish';
        $this->render('success');
    }

    public function actionMailingType()
    {
        $this->layout = '//layouts/mailingType';
        $this->render('mailingType');
    }

    /**
     * @property-description Персональная закрепляющая за партнером пользователя ссылка
     *
     * @param int $id
     * @param string $redirectTo
     * @param int $promo
     */
    public function actionInvite($id, $redirectTo = '', $promo = 0)
    {
        $id = Cast::toInt($id);
        $promo = Cast::toInt($promo);
        $user = $this->app()->user;
        $partner = UserPartnerRecord::model()->findByPk($id);
        $defaultRedirectTo = $this->createUrl('index/index');
        if ($promo > 0) {
            $defaultRedirectTo = $this->createUrl('index/index', ['promo-partner' => $promo]);
        }
        if ($partner === null) {
            $this->redirect($defaultRedirectTo);
        }
        if ($redirectTo == '') {
            $redirectTo = $defaultRedirectTo;
        } else {
            $redirectToDecode = base64_decode($redirectTo);
            if (mb_strpos($redirectToDecode, $_SERVER['SERVER_NAME']) !== false) {
                $redirectTo = $redirectToDecode;
            } else {
                $redirectTo = $defaultRedirectTo;
            }
        }
        if (!$user->isGuest) {
            if ($user->id == $partner->user_id) {
                $this->redirect($redirectTo);
            }
        }
        $urlReferer = $this->app()->request->urlReferrer;
        $user->setPartnerRefererUrl($urlReferer);
        $user->setPartnerId($partner->id);
        $user->setPartnerUtmParams($this->_getUtmParams($urlReferer));
        $this->redirect($redirectTo);
    }

    /**
     * @param $url
     *
     * @return array
     */
    protected function _getUtmParams($url)
    {
        $utmAvailableParams = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content', 'mode'];

        $utmParams = array_map(function ($param) {
            return $this->app()->request->getQuery($param);
        }, array_combine($utmAvailableParams, $utmAvailableParams));

        return $utmParams;
    }
}
