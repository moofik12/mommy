<?php

namespace MommyCom\Controller\FrontendMobile;

use GuzzleHttp\Psr7\ServerRequest;
use MommyCom\Service\Distribution\DistributionEventRecorder;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

class TrackerController extends FrontController
{
    /**
     * @var DistributionEventRecorder $distributionEventRecorder
     */
    private $distributionEventRecorder;

    public function init()
    {
        $this->distributionEventRecorder = new DistributionEventRecorder();

        return parent::init();
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    /**
     * Запись инфы о пользователе и произошедшем событии в БД
     *
     * @return string
     */
    public function actionRecord()
    {
        $request = ServerRequest::fromGlobals();
        $user = $this->app()->getUser();
        $userId = !is_null($user) && !is_null($user->getId()) ? $user->getId() : 0;

        $this->distributionEventRecorder->createRecordFromArray(
            $request->getParsedBody(),
            $request->getServerParams(),
            $userId
        );
    }
}
