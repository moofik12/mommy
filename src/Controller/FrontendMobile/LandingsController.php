<?php

namespace MommyCom\Controller\FrontendMobile;

use CActiveForm;
use CLogger;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Db\UserReturnRecord;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class LandingsController extends FrontController
{
    /**
     * идентификация пользователей зарегистрировавшихся с мобильной версии
     */
    const MOBILE_PROMO_START = 100;

    /**
     * @var string
     */
    public $layout = '//layouts/landings';

    /**
     * @return array
     */
    public function accessRules(): array
    {
        return [
            ['deny'],
        ];
    }

    /**
     * список автивных Promo
     *
     * @var array array( id => name );
     */
    protected $_promoEnabled = [
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
    ];

    protected $_promoDisableBalancer = [
        1 => 'one',
        2 => 'two',
        4 => 'four',
        5 => 'five',
        6 => 'six',
    ];

    /**
     * @param string $name
     * @param string $promo
     */
    public function actionIndex($name = 'registration', $promo = '')
    {
        $validPromo = $this->_promoEnabled;

        if (!$this->app()->user->isGuest || empty($validPromo)) {
            $this->redirect(['index/index']);
        }

        $validPromo = $this->_promoEnabled;
        $user = $this->app()->user;
        $tab = Cast::toStr($name);
        $promoId = array_search(Cast::toStr($promo), $validPromo);

        if ($promoId === false) {
            $promoId = array_rand(array_diff($validPromo, $this->_promoDisableBalancer));
            $promo = $validPromo[$promoId];
            $user->setLandingNum($promoId + self::MOBILE_PROMO_START);
            $this->redirect(['index', 'promo' => $promo]);
        }

        $form = new RegistrationForm();

        $tabs = [
            'success',
            'registration',
        ];

        if (!in_array($tab, $tabs)) {
            $tab = 'registration';
        }

        $this->render("$promoId/index", [
            'promoId' => $promoId,
            'tab' => $tab,
            'registrationForm' => $form,
        ]);
    }

    public function actionRegistration($id)
    {
        $password = Random::alphabet(6, Random::DIGIT);
        $request = $this->app()->request;
        $webUser = $this->app()->user;

        $id = Cast::toUInt($id);
        $tab = 'registration';

        //на всякий случай
        $promo = isset($this->_promoEnabled[$id]) ? $this->_promoEnabled[$id] : '';

        $form = new RegistrationForm('landing');

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->getPost('RegistrationForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                $user = new UserRecord();
                $user->name = $form->name;
                $user->email = $form->email;
                $user->password = $password;

                $offerProvider = $webUser->getOfferProvider();
                if ($offerProvider != UserRecord::OFFER_PROVIDER_NONE) {
                    $user->offer_provider = $offerProvider;
                    $user->offer_id = $webUser->getOfferId();
                    $user->offer_target_url = $webUser->getOfferTargetUrl();
                }
                $user->landing_num = $webUser->getLandingNum();

                if ($user->save()) {
                    $this->app()->mailer->create(
                        [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                        [$user->email],
                        $this->t('Welcome to the shopping club mommy.com'),
                        [
                            'view' => 'welcome2',
                            'data' => ['model' => $user, 'password' => $password]]
                    );

                    $tab = 'success';
                } else {
                    Yii::log("Ошибка при регистрации нового пользователя c лендинга {$form->email}. Ошибки модели: " . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                    $this->app()->user->setFlash('message', 'Error while registering. Contact Support!');
                }
            } else {
                $user = UserRecord::model()->emailIs($form->email)->find();
                /** @var  $user  UserRecord */
                if ($user !== null) {
                    $userReturned = UserReturnRecord::model()->email($form->email)->find() ?: new UserReturnRecord();
                    $userReturned->email = $user->email;
                    $userReturned->returned_at = time();
                    $userReturned->save();
                }
            }
        }

        $this->redirect(['index', 'name' => $tab, 'promo' => $promo]);
    }
}
