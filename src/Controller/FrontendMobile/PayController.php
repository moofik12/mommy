<?php

namespace MommyCom\Controller\FrontendMobile;

use CException;
use CHtml;
use CHttpException;
use GuzzleHttp\Psr7\ServerRequest;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\PaymentGateway\AvailablePaymentGateways;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\Service\BaseController\RenderPsr7ResponseTrait;
use MommyCom\YiiComponent\ShopLogic\ChoosePaymentMethod;
use MommyCom\YiiComponent\TokenManager;

class PayController extends FrontController
{
    use RenderPsr7ResponseTrait;

    /**
     * @return array
     */
    public function accessRules()
    {
        $rules = [
            [
                'allow',
                'actions' => ['receive', 'index'],
                'users' => ['*'],
            ],
        ];

        return array_merge($rules, parent::accessRules());
    }

    /**
     * Выбор провайдера для оплаты
     *
     * @param string $uid
     *
     * @throws CHttpException
     */
    public function actionIndex($uid)
    {
        $request = $this->app()->request;
        /** @var OrderRecord $order */
        $order = OrderRecord::model()->findByPk(trim($uid));
        $this->validateOrder($order);

        $orderPrice = $order->getPayPrice();
        $deliveryPrice = $order->getDeliveryPrice();
        $bonusesAmount = 0;
        $discountAmount = $order->bonuses;
        $countProductsNumber = $order->getCountDiscount();

        $model = new ChoosePaymentMethod();
        $model->setAttributes($request->getPost('ChoosePaymentMethod', []));

        if ($request->isAjaxRequest) {
            echo \CActiveForm::validate($model);
            $this->app()->end();
        }

        $model->cashOnDelivery = $order->getDeliveryModel()->isCashOnDelivery();

        $this->productHeader = CHtml::tag(
            'span',
            ['class' => 'table'],
            '<span class="back-container-link"><span class="back-container-content">' . $this->t('Choose a payment type') . '</span></span>'
        );

        /** @var AvailablePaymentGateways $availablePaymentGateways */
        $availablePaymentGateways = $this->container->get(AvailablePaymentGateways::class);

        $paymentMethods = [];
        foreach ($availablePaymentGateways->getPaymentMethods() as $paymentMethod) {
            $paymentMethods[] = [
                'id' => $paymentMethod->getId(),
                'name' => $paymentMethod->getName(),
            ];
        }

        $this->render('allProvider', [
            'uid' => trim($uid),
            'order' => $order,
            'model' => $model,
            'amount' => $orderPrice + $deliveryPrice,
            'deliveryPrice' => $deliveryPrice,
            'bonusesAmount' => $bonusesAmount,
            'discountAmount' => $discountAmount,
            'countProductsNumber' => $countProductsNumber,
            'availableGateways' => $availablePaymentGateways->getPaymentGateways(),
            'availableMethods' => $paymentMethods,
        ]);
    }

    /**
     * Страница с результатами выполнения платежа
     *
     * @param string $uid
     * @param string $token
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionResult($uid, $token)
    {
        /** @var TokenManager $tokenManager */
        $tokenManager = $this->container->get(TokenManager::class);

        if (!$tokenManager->isValid('resultAction', $token)) {
            $this->redirect(['order/index']);
        }

        $orderFirst = current(OrderRecord::model()->findByUid(trim($uid)));
        if (!$orderFirst instanceof OrderRecord) {
            throw new CHttpException(404, $this->t('No order for payment found'));
        }

        $status = (int)$orderFirst->getPayPrice()
            ? $this->t('Awaiting payment confirmation')
            : $this->t('Payment successful');

        $this->render('result', ['status' => $status]);
    }

    /**
     * @param OrderRecord $order
     *
     * @throws CHttpException
     */
    private function validateOrder(OrderRecord $order)
    {
        $userId = $this->app()->user->id;

        if ($order->user_id !== $userId && $order->payment_token !== $this->app()->request->getParam('token')) {
            $errorMessage = $this->t('Order № {number}: login or valid payment link required', ['{number}' => $order->id]);
            throw new CHttpException(400, $errorMessage);
        }

        if (!$order->isAvailableForPayment()) {
            $errorMessage = $this->t('Order number {number} is not available for payment', ['{number}' => $order->id]);
            throw new CHttpException(400, $errorMessage);
        }

        if ($order->isProcessingActive()) {
            $errorMessage = $this->t('Your order № {number} is currently being processed by our manager. Please try again later.', ['{number}' => $order->id]);
            throw new CHttpException(400, $errorMessage);
        }
    }

    /**
     * Обработка ответов от платежных сервисов
     *
     * @param string $provider
     */
    public function actionReceive($provider)
    {
        /** @var AvailablePaymentGateways $availablePaymentGateways */
        $availablePaymentGateways = $this->container->get(AvailablePaymentGateways::class);
        $paymentGateway = $availablePaymentGateways->getPaymentGateway($provider);

        $response = $paymentGateway->handle(ServerRequest::fromGlobals());

        $this->renderPsr7Response($response);
    }
} 
