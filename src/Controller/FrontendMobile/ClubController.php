<?php

namespace MommyCom\Controller\FrontendMobile;

use CException;
use CHttpCookie;
use CHttpException;
use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Model\Product\ViewedProducts;
use MommyCom\Security\User\UserBuilder;
use MommyCom\Service\BaseController\BaseClubController;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\Service\Money\CurrencyFormatter;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionDetector;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\FrontendMobile\ClickIdConstantsInterface;
use MommyCom\YiiComponent\FrontendMobile\ClickIdSetterTrait;
use MommyCom\YiiComponent\FrontendMobile\ReplyResult;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ClubController extends BaseClubController implements ClickIdConstantsInterface
{
    use ClickIdSetterTrait;

    const PRODUCT_CACHE = 60;

    public $description;
    public $visibleOnlyLogo;
    public $showRegionConfirmPopup;
    public $showRegionChooseModal;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => [
                    'index',
                    'mommy',
                    'registration',
                    'transfer',
                    'white',
                    'moon',
                ],
                'users' => ['?'],
            ],
            [
                'allow',
                'actions' => ['thanks'],
                'users' => ['@'],
            ],
            [
                'deny',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $this->setClickId();

        return true;
    }

    /**
     * @param string $sub_id
     */
    public function actionIndex($sub_id = '')
    {
        $this->setPageTitle('MOMMY.COM - ' . $this->t('Shopping club for loving mothers'));
        $this->addGoogleFont('Roboto');

        $this->render('index', [
            'registrationForm' => new RegistrationForm(),
            'landingNum' => self::LANDING_CLUB,
            'subId' => $sub_id,
        ]);
    }

    /**
     * @param string $sub_id
     */
    public function actionMommy($sub_id = '')
    {
        $this->setPageTitle('MOMMY.COM - ' . $this->t('Shopping club for loving mothers'));
        $this->addGoogleFont('Roboto');

        $this->render('mommy', [
            'registrationForm' => new RegistrationForm(),
            'landingNum' => self::LANDING_MOMMY,
            'subId' => $sub_id,
        ]);
    }

    /**
     * @param string $sub_id
     */
    public function actionTransfer($sub_id = '')
    {
        $this->setPageTitle('MOMMY.COM - ' . $this->t('Shopping club for loving mothers'));
        $this->addGoogleFont('Roboto Slab');
        $this->addGoogleFont('Source Sans Pro');

        $this->render('transfer', [
            'registrationForm' => new RegistrationForm(),
            'landingNum' => self::LANDING_TRANSFER,
            'subId' => $sub_id,
        ]);
    }

    /**
     * @param string $sub_id
     *
     * @throws CException
     */
    public function actionWhite($sub_id = '')
    {
        $this->setPageTitle('MOMMY.COM - ' . $this->t('Shopping club for loving mothers'));
        $this->addGoogleFont('Baloo Tamma');
        $this->addGoogleFont('Noto Sans');

        /** @var CurrencyFormatter $currencyFormatter */
        $currencyFormatter = $this->container->get(CurrencyFormatter::class);
        $groupedProducts = (new ViewedProducts())->getMostViewedProducts(3);

        $this->setRegionCookie();

        $this->render('white', [
            'registrationForm' => new RegistrationForm(),
            'landingNum' => self::LANDING_WHITE,
            'subId' => $sub_id,
            'groupedProducts' => $groupedProducts,
            'currencyFormatter' => $currencyFormatter,
        ]);
    }

    /**
     * @param string $sub_id
     */
    public function actionMoon($sub_id = '')
    {
        $this->setPageTitle('MOMMY.COM - ' . $this->t('Shopping club for loving mothers'));
        $this->addGoogleFont('Baloo');
        $this->addGoogleFont('Roboto');

        $this->render('moon', [
            'registrationForm' => new RegistrationForm(),
            'landingNum' => self::LANDING_MOON,
            'subId' => $sub_id,
        ]);
    }

    /**
     * @param int $landing
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionRegistration(int $landing = 0)
    {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if (!$request->isXmlHttpRequest()) {
            throw new CHttpException(404);
        }

        $form = new RegistrationForm('simple');
        $response = new ReplyResult();

        $dataForm = $request->get('RegistrationForm');
        if (!$dataForm) {
            throw new CHttpException(400);
        }

        $form->setAttributes($dataForm);
        if ($form->validate()) {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->container->get('doctrine')->getManager();
            /** @var UserBuilder $userBuilder */
            $userBuilder = $this->container->get(UserBuilder::class);
            /** @var EmailUtils $emailUtils */
            $emailUtils = $this->container->get(EmailUtils::class);


            $user =  $userBuilder
                ->setEmail($form->email)
                ->setLandingNum($landing)
                ->build();
            $entityManager->persist($user);
            $entityManager->flush();

            $senderEmail = $this->app()->params['layoutsEmails']['welcome2'];
            $senderName = $this->app()->params['distributionEmailName'];
            $emailUtils->sendRegistrationEmail($user, $userBuilder->getPassword(), $senderEmail, $senderName);

            /** @var TokenStorageInterface $tokenStorage */
            $tokenStorage = $this->container->get('security.token_storage');
            $providerKey = $this->container->getParameter('frontend.firewall_key');
            if ($this->authenticateUser($tokenStorage, $user, $providerKey)) {
                $response->setSuccess(true);
            }
        }

        $this->renderJson($response->toArray());
    }

    public function actionThanks()
    {
        $this->pageTitle = $this->t('Thank you for registering!');

        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);
        $request = $this->app()->request;
        $user = $this->app()->user;

        $cookie = new CHttpCookie(RegionDetector::getCookieName(), $regions->getServerRegion()->getRegionName());
        $cookie->domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);
        $cookie->expire = 2147483647; // много лет, степень двойки

        $request->cookies[RegionDetector::getCookieName()] = $cookie;

        $this->render('thanks');
    }
}
