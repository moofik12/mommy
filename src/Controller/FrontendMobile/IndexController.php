<?php

namespace MommyCom\Controller\FrontendMobile;

use CCache;
use CDbCacheDependency;
use CMap;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Misc\ModelsShuffle;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\SplitTesting;
use MommyCom\Service\TelegramBot;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

class IndexController extends FrontController
{
    /**
     * Доступ открыт для actionInvite (реферальная ссылка)
     *
     * @return array
     */
    public function accessRules()
    {
        return ArrayUtils::merge([
            [
                'allow',
                'actions' => ['invite', 'index'],
                'users' => ['?'],
            ],
        ], parent::accessRules());
    }

    /**
     * @return array
     */
    public function cache()
    {
        return [
            'index' => [
                'partialOnAjax' => false,
                'duration' => 0,
                'dependency' => new CDbCacheDependency("SELECT MAX(updated_at) FROM events"),
                'varyByExpression' => function () {
                    if ($this->app()->user->hasFlash('message')) {
                        $userId = $this->app()->user->isGuest ? '' : $this->app()->user->id;
                        return $this->app()->user->getFlash('message', false, false) . $userId . microtime(true);
                    }

                    /** @var Regions $regions */
                    $regions = $this->container->get(Regions::class);
                    $region = $regions->getServerRegion();

                    return $region->getRegionName() . $region->getDetectLevel();
                },
                'varyByLanguage' => true,
            ],
        ];
    }

    /**
     * @throws \CException
     */
    public function actionIndex()
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        /** @var CCache $cache */
        $cache = $this->app()->cache;

        /** @var TelegramBot $telegramBot */
        $telegramBot = $this->container->get(TelegramBot::class);

        $variant = $splitTesting->getMadeInVariant();

        $provider = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->onlyVisible()
            ->onlyPublished()
            ->onlyTimeActive()
            ->isVirtual(false)
            ->orderBy('end_at', 'desc')
            ->orderBy('priority', 'asc')
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        /** @var array $telegramEventsParams */
        $telegramParams = $this->app()->params['telegram'];
        $telegramEventsParams = $telegramParams['events'];

        if ($telegramBot->getToken() &&
            false === $cache->get($telegramEventsParams['cache_key_telegram_bot_few_events']) &&
            $provider->getItemCount() < $telegramEventsParams['min_critical_events_count']
        ) {
            $telegramBot->sendMessage(
                $telegramParams['dream_team_telegram_chat_id'],
                sprintf('На сайте %s меньше %d акций!',
                    $this->app()->getRequest()->getHostInfo(),
                    $telegramEventsParams['min_critical_events_count']
                )
            );
            $cache->set(
                $telegramEventsParams['cache_key_telegram_bot_few_events'],
                '',
                $telegramEventsParams['cache_key_telegram_bot_few_events_ttl']
            );
        }

        $futureProvider = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->onlyPublished()
            ->onlyTimeFuture()
            ->isVirtual(false)
            ->orderBy('start_at', 'asc')
            ->limit(15)
            ->cache(30)
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $notShuffleEventsTime = strtotime('+1 day', 0);
        $time = time();
        $eventsNotShuffle = [];
        $eventsShuffle = [];

        foreach ($provider->getData() as $event) {
            /** @var EventRecord $event */
            if ($event->start_at + $notShuffleEventsTime > $time) {
                $eventsNotShuffle[] = $event;
                continue;
            }

            $eventsShuffle[] = $event;
        }

        $eventsShuffle = new ModelsShuffle($eventsShuffle, ['id'], true, 21600);//cache 6 hours
        $events = CMap::mergeArray($eventsNotShuffle, $eventsShuffle->getData());

        $this->render('index', [
            'events' => $events,
            'provider' => $provider,
            'futureProvider' => $futureProvider,
        ]);
    }
}
