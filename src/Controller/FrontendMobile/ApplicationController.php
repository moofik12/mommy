<?php

namespace MommyCom\Controller\FrontendMobile;

use CLogger;
use MommyCom\Model\Db\ApplicationSubscriberRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class ApplicationController extends FrontController
{
    /**
     * @var int валидное время в секудах при котором действует хеш
     */
    public $validTime = 60;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    protected function _getHash($signature, $telephoneId, $time)
    {
        static $_internalSalt = ",=9ax9%QZC(R";

        $protectKey = $time . $_internalSalt;
        return hash('sha256', $signature . $telephoneId . $protectKey);
    }

    protected function _getSignature($telephoneId)
    {
        static $_publicKey = "921197500";
        return hash('sha256', $_publicKey . $telephoneId);
    }

    protected function _validateSignature($signature, $telephoneId)
    {
        return $signature == $this->_getSignature($telephoneId);
    }

    protected function _validateHash($signature, $telephoneId, $hash, $timeGenerate)
    {
        $time = time();
        if (abs($time - $timeGenerate) > $this->validTime) {
            return false;
        }

        $hash1 = $this->_getHash($signature, $telephoneId, $timeGenerate);
        return $this->_validateSignature($signature, $telephoneId) && $hash == $hash1;
    }

    public function actionToken($signature, $telephoneId)
    {
        $time = time();
        $result = [
            'token' => $this->_getHash($signature, $telephoneId, $time),
            'time' => $time,
        ];

        $this->renderJson($result);
    }

    public function actionSubscribeAndroid($signature, $token, $time, $telephoneId, $gcmId = null, $versionCode = 1, $versionName = 1)
    {
        $time = Cast::toUInt($time);
        $result = ['success' => false, 'message' => ''];

        if (!$this->_validateHash($signature, $telephoneId, $token, $time)) {
            $result['message'] = 'incorrect token';
        }

        /** @var ApplicationSubscriberRecord $model */
        $model = ApplicationSubscriberRecord::model()->
        client($telephoneId)->
        typeIs(ApplicationSubscriberRecord::TYPE_ANDROID)->
        find();

        if ($model) {
            //update
            $model->client_gcm_id = $gcmId;
            $model->version_code = $versionCode;
            $model->version_name = $versionName;
        } else {
            $model = new ApplicationSubscriberRecord();
            $model->client_id = $telephoneId;
            $model->type = ApplicationSubscriberRecord::TYPE_ANDROID;
            $model->client_gcm_id = $gcmId;
            $model->version_code = $versionCode;
            $model->version_name = $versionName;
        }

        if ($model->save()) {
            $result['success'] = true;
        } else {
            Yii::log("Ошибка подписки андроид приложения: \n" . print_r($result), CLogger::LEVEL_ERROR, 'system.subscribeAndroid');
        }

        Yii::log("Подписка андроид приложения: \n" . print_r($result), CLogger::LEVEL_WARNING, 'system.subscribeAndroid');
        $this->renderJson($result);
    }

    /**
     * @param string $signature
     * @param string $token
     * @param int $time
     * @param string $telephoneId
     * @param null $gcmId
     * @param int $versionCode
     * @param int $versionName
     */
    public function actionSubscribeIos($signature, $token, $time, $telephoneId, $gcmId = null, $versionCode = 1, $versionName = 1)
    {
        $time = Cast::toUInt($time);
        $result = ['success' => false, 'message' => ''];

        if (!$this->_validateHash($signature, $telephoneId, $token, $time)) {
            $result['message'] = 'incorrect token';
        }

        /** @var ApplicationSubscriberRecord $model */
        $model = ApplicationSubscriberRecord::model()->
        device($gcmId)->
        typeIs(ApplicationSubscriberRecord::TYPE_IOS)->
        find();

        if ($model) {
            //update
            $model->client_id = $telephoneId;
            $model->version_code = $versionCode;
            $model->version_name = $versionName;
        } else {
            $model = new ApplicationSubscriberRecord();
            $model->client_id = $telephoneId;
            $model->type = ApplicationSubscriberRecord::TYPE_IOS;
            $model->client_gcm_id = $gcmId;
            $model->version_code = $versionCode;
            $model->version_name = $versionName;
        }

        if ($model->save()) {
            $result['success'] = true;
        }

        $this->renderJson($result);
    }
}
