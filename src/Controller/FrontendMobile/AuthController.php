<?php

namespace MommyCom\Controller\FrontendMobile;

use CActiveForm;
use CHtml;
use CHttpException;
use CLogger;
use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use MommyCom\YiiComponent\AuthManager\AuthenticationTrait;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Security\User\UserBuilder;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\YiiComponent\Emailer;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\SplitTestTrackingRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Db\UserRegistrationTrackingRecord;
use MommyCom\Model\Db\UserReturnRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\NewPasswordForm;
use MommyCom\Model\Form\RecoveryPasswordForm;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\AuthManager\MailingUserIdentity;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\Random;
use MommyCom\YiiComponent\Type\Cast;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Yii;

class AuthController extends FrontController
{
    use AuthenticationTrait;

    const SHOW_MODAL_SOCIAL_ENTER_EMAIL = 'auth-social-end';
    const SHOW_MODAL_SOCIAL_CONFIRM_PASSWORD = 'auth-social-confirm';

    const SOCIAL_AUTH_SERVICE_NOT_AVAILABLE = 'SERVICE_NOT_AVAILABLE';
    const SOCIAL_AUTH_SERVICE_FORM_ERROR = 'FORM_ERROR';
    const SOCIAL_AUTH_NOT_EMAIL = 'NOT_EMAIL';
    const SOCIAL_AUTH_CONFIRM_PASSWORD = 'CONFIRM_PASSWORD';

    /**
     * @var string
     */
    public $defaultAction = 'auth';

    /**
     * @var string
     */
    public $layout = '//layouts/login';

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['postOnly + recovery, registration']);
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['auth', 'registration'],
                'users' => ['?'],
            ],
            [
                'allow',
                'actions' => ['logout'],
                'users' => ['@'],
            ],
            [
                'allow',
                'actions' => [
                    'recovery',
                    'finishRegistration',
                    'passwordRecovery',
                    'networkLogin',
                    'NetworkRegistration',
                    'offerRegistration',
                    'track',
                ],
            ],
            [
                'deny',
            ],
        ];
    }

    /**
     * @param string $name
     * @param string $social
     */
    public function actionAuth($name = 'registration', $social = '')
    {
        $request = $this->app()->request;
        $this->saveBackUrl();

        $tabs = [
            'login',
            'registration',
            'forgot',
        ];

        $socialIn = [
            'socialNotEmail',
            'socialConfirm',
        ];

        if (!in_array($social, $socialIn)) {
            $social = '';
        }

        if (!in_array($name, $tabs)) {
            $name = 'login';
        }

        $session = $this->app()->session;
        if (!$session->isStarted) {
            $session->open();
        }

        if ('registration' === $name) {
            $session->add('realReferrer', $this->app()->request->urlReferrer);
        }

        $authForm = new AuthForm();

        //processing     static::actionRecovery()
        $recoveryForm = new RecoveryPasswordForm();

        //processing     static::actionRegistration()
        $registrationForm = new RegistrationForm();
        $registrationForm->setScenario('simple');

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($authForm);
            $this->app()->end();
        }

        $dataAuthForm = $request->getPost('AuthForm');
        if (!empty($dataAuthForm)) {
            $authForm->setAttributes($dataAuthForm);
            if ($authForm->validate() && $authForm->login()) {
                $this->redirectBackOr(['index/index']);
            }
        }
        $authForm->unsetAttributes(['password']);
        $status = $this->app()->request->getParam('status');

        $this->render('auth', [
            'tab' => $name,
            'social' => $social,
            'authForm' => $authForm,
            'recoveryForm' => $recoveryForm,
            'registrationForm' => $registrationForm,
            'tokenExpired' => 'tokenExpired' === $status,
        ]);
    }

    /**
     * @property-description Восстановление пароля
     */
    public function actionRecovery()
    {
        $recoveryForm = new RecoveryPasswordForm();

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($recoveryForm);
            $this->app()->end();
        }

        $dataForm = $this->app()->getRequest()->getPost('RecoveryPasswordForm');

        if (!empty($dataForm)) {
            $recoveryForm->setAttributes($dataForm);
            if ($recoveryForm->validate()) {
                /** @var UserRecord $model */
                $model = UserRecord::model()->emailIs($recoveryForm->email)->find();

                if ($model === null || Cast::toUInt($model->status) !== UserRecord::STATUS_ACTIVE) {
                    $this->app()->user->setFlash('message', 'Profile not found or blocked');
                } else {
                    $model->password_recovery_requested_at = time();

                    if ($model->save(true, ['password_recovery_requested_at'])) {
                        $mail = $this->app()->mailer;
                        /** @var $mailer Emailer */
                        $mail->create(
                            [$this->app()->params['layoutsEmails']['recoveryPassword'] => $this->app()->params['distributionEmailName']],
                            [$model->email],
                            'Password recovery',
                            ['view' => 'recoveryPassword', 'data' => ['user' => $model]]
                        );

                        $this->app()->user->setFlash('message', 'На ' . CHtml::encode($recoveryForm->email)
                            . ' выслано письмо для восстановления пароля');
                    } else {
                        $this->app()->user->setFlash('message', 'Error retrieving password');
                    }
                }
            }
        }

        $this->redirect(['auth/auth', 'name' => 'login']);
    }

    /**
     * @throws \CException
     * @throws \MommyCom\Security\User\UserBuilderException
     */
    public function actionRegistration() {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $form = new RegistrationForm();
        $form->setScenario('simple');

        if ($request->isXmlHttpRequest() && $request->get('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->get('RegistrationForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                /** @var EntityManagerInterface $entityManager */
                $entityManager = $this->container->get('doctrine')->getManager();
                /** @var UserBuilder $userBuilder */
                $userBuilder = $this->container->get(UserBuilder::class);
                /** @var EmailUtils $emailUtils */
                $emailUtils = $this->container->get(EmailUtils::class);

                $user = $userBuilder
                    ->setEmail($form->email)
                    ->build();

                $entityManager->persist($user);
                $entityManager->flush();

                $senderEmail = $this->app()->params['layoutsEmails']['welcome2'];
                $senderName = $this->app()->params['distributionEmailName'];
                $emailUtils->sendRegistrationEmail($user, $userBuilder->getPassword(), $senderEmail, $senderName);

                $splitTestTracking = new SplitTestTrackingRecord();
                $splitTestTracking->target = SplitTestTrackingRecord::TARGET_USER_REGISTER;
                $splitTestTracking->user_id = $user->getId();
                $splitTestTracking->order_id = 0;
                $splitTestTracking->testParams = $this->app()->splitTesting->getAvailable();
                $splitTestTracking->save();

                UserRegistrationTrackingRecord::fillWithRequestData();

                /** @var TokenStorageInterface $tokenStorage */
                $tokenStorage = $this->container->get('security.token_storage');
                $providerKey = $this->container->getParameter('frontend.firewall_key');
                if ($this->authenticateUser($tokenStorage, $user, $providerKey)) {
                    if ($this->fillCart()) {
                        $this->redirect($this->createUrl('cart/index'));
                    } else {
                        $this->redirect($this->createUrl('index/index'));
                    }
                }
            }
        }
    }

    public function actionOfferRegistration()
    {
        $request = $this->app()->request;
        /** @var \MommyCom\YiiComponent\AuthManager\ShopWebUser $webUser */
        $webUser = $this->app()->user;
        $form = new RegistrationOfferForm();
        $password = Random::alphabet(8, Random::DIGIT);
        /** @var  $user  UserRecord */
        $message = false;

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->getPost('RegistrationOfferForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                $user = new UserRecord();
                $user->email = $form->email;
                $user->password = $password;
                $user->moved_search_engine = $webUser->getLastSearchEngine(null);
                $user->psychotype = $webUser->getPsychotype(UserRecord::PSYCHOTYPE_UNDEFINED);
                $user->landing_num = $webUser->getLandingNum();

                if ($user->save()) {
                    $this->_saveDistribution($user->id);

                    $specialPsychotypeBanner = $webUser->getPsychotype(false);
                    $bannerName = $specialPsychotypeBanner == false ? "PS:default" : "PS:" . $specialPsychotypeBanner;
                    if ($specialPsychotypeBanner == false) {
                        $bannerName = 'cloud-before';
                    }

                    $invitedBy = $webUser->getState('invitedBy', 0);

                    if ($invitedBy && $webUser->isGuest) {
                        $user->setScenario('invitedBy');
                        $user->invited_by = $invitedBy;
                        $user->save(true, ['invited_by']);
                    }

                    $offerProvider = $webUser->getOfferProvider();
                    if ($offerProvider != UserRecord::OFFER_PROVIDER_NONE) {
                        $offerId = $webUser->getOfferId();
                        $offerTargetUrl = $webUser->getOfferTargetUrl();

                        $user->landing_num = $webUser->getLandingNum();
                        $user->offer_provider = $offerProvider;
                        $user->offer_id = $offerId;
                        $user->offer_target_url = $offerTargetUrl;
                        $user->save(true, ['offer_provider', 'offer_id', 'offer_target_url', 'landing_num']);
                    }

                    //закрепление за партнером
                    $partnerId = $webUser->getPartnerId();
                    if ($partnerId > 0) {
                        $partnerRefererUrl = $webUser->getPartnerRefererUrl();
                        $partnerUtmParams = $webUser->getPartnerUtmParams();
                        $user->assignToPartner($partnerId, $partnerRefererUrl, json_encode($partnerUtmParams, JSON_UNESCAPED_UNICODE, 3));
                    }
                    $user->refresh();

                    //получение бенефитов
                    if ($form->strategy == RegistrationOfferForm::STRATEGY_BONUSES) {
                        $bonusPoints = new UserBonuspointsRecord();
                        $bonusPoints->user_id = $user->id;
                        $bonusPoints->points = $form->getBeneficeAmount();
                        $bonusPoints->message = 'Welcome to the shopping club mommy.com';
                        $bonusPoints->is_custom = false;
                        $bonusPoints->type = UserBonuspointsRecord::TYPE_PRESENT;
                        $bonusPoints->setExpireAtString('tomorrow +6 days');

                        if ($bonusPoints->save()) {
                            //mail disabled
                            $allowed = $this->app()->params['newsletters'] ?? false;

                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['welcome2'] => $this->app()->params['distributionEmailName']],
                                    [$user->email],
                                    $this->t('Welcome to the shopping club mommy.com'),
                                    [
                                        'view' => 'welcome2',
                                        'data' => ['model' => $user, 'password' => $password, 'bonus' => $bonusPoints],
                                    ]
                                );
                            }

                            $message = "На ваш бонусный счет начислено {$bonusPoints->points} гривен. Приятных покупок!";
                        } else {
                            Yii::log("Ошибка при начислении бонусов новому пользователю {$form->email}. Ошибки модели: " . print_r($bonusPoints->errors, true), CLogger::LEVEL_ERROR);
                        }
                    } elseif ($form->strategy == RegistrationOfferForm::STRATEGY_PROMOCODE) {
                        $promo = new PromocodeRecord();
                        $promo->type = PromocodeRecord::TYPE_PERSONAL;
                        $promo->reason = PromocodeRecord::REASON_CAMPAIGN;
                        $promo->user_id = $user->id;

                        if ($form->strategyPayments == RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY) {
                            $promo->cash = $form->getBeneficeAmount();
                            $promo->order_price_after = RegistrationOfferForm::PROMOCODE_PAYMENTS_MONEY_AFTER_ORDER_AMOUNT;
                        } elseif ($form->strategyPayments == RegistrationOfferForm::STRATEGY_PAYMENTS_PERCENT) {
                            $promo->percent = $form->getBeneficeAmount();
                        }
                        $promo->valid_until = strtotime('tomorrow +3 days');

                        if ($promo->save()) {
                            $user->present_promocode = $promo->promocode;
                            if (!$user->save(true, ['present_promocode'])) {
                                Yii::log("Ошибка записи подарочного промокода новому пользователю {$form->email}. Ошибки модели: " . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                            }
                            //mail disabled
                            $allowed = $this->app()->params['newsletters'] ?? false;

                            if ($allowed) {
                                $this->app()->mailer->create(
                                    [$this->app()->params['layoutsEmails']['welcomeOfferWithPromocode2'] => $this->app()->params['distributionEmailName']],
                                    [$user->email],
                                    'Important information + Discount for new club member',
                                    [
                                        'view' => 'welcomeOfferWithPromocode2',
                                        'data' => ['user' => $user, 'password' => $password, 'promocode' => $promo],
                                    ]
                                );
                                $message = "Thank you for registering! A letter with a coupon has been sent to your mailing address!";
                            }
                        } else {
                            Yii::log("Ошибка при начислении бонусов новому пользователю {$form->email}. Ошибки модели: " . print_r($promo->errors, true), CLogger::LEVEL_ERROR);
                        }
                    }

                    $authForm = new AuthForm();
                    $authForm->email = $form->email;
                    $authForm->password = $password;

                    if ($authForm->login()) {
                        if ($message) {
                            $this->redirect(['message/info',
                                'title' => 'Thank you for registering!',
                                'message' => $message,
                                'hash' => $this->app()->tokenManager->getToken($message),
                                "present-registration-box-registered" => "success-$bannerName",
                            ]);
                        }

                        $this->redirect(['index/index', "present-registration-box-registered" => "success-$bannerName"]);
                    } else {
                        if ($message) {
                            $this->app()->user->setFlash('message', $message);
                        }
                        $this->redirect(['index/index', "present-registration-box-registered" => "success-$bannerName", 'login' => 'error']);
                    }
                } else {
                    Yii::log("Ошибка при регистрации нового пользователя {$form->email}. Ошибки модели: " . print_r($user->errors, true), CLogger::LEVEL_ERROR);
                    $message = 'Error while registering. Contact Support!';
                }
            } else {
                Yii::log("Ошибка при регистрации нового пользователя {$form->email}. Ошибки формы: " . print_r($form->errors, true), CLogger::LEVEL_ERROR);
                $message = 'Error while registering. Contact Support!';

                $user = UserRecord::model()->emailIs($form->email)->find();
                /** @var  $user  UserRecord */
                if ($user !== null) {
                    $userReturned = UserReturnRecord::model()->email($form->email)->find() ?: new UserReturnRecord();
                    $userReturned->email = $user->email;
                    $userReturned->returned_at = time();
                    $userReturned->save();
                }
            }
        }

        if ($message) {
            $this->app()->user->setFlash('message', $message);
        }
        $this->redirect(['index/index']);
    }

    public function actionFinishRegistration($h, $uid)
    {
        $uid = Cast::toUInt($uid);
        $h = Cast::toStr($h);

        $user = UserRecord::model()->findByPk($uid);
        /* @var $user UserRecord */

        $this->app()->user->logout();

        if ($user === null) {
            throw new CHttpException('404', 'Data not found');
        }

        if ($user->is_email_verified) {
            $this->redirect(['index/index']);
        }

        if ($h === $user->generateHashEmailToVerify() && !Cast::toBool($user->is_email_verified)) {
            $user->is_email_verified = true;
            if ($user->save(true, ['is_email_verified'])) {
                $this->app()->user->setFlash('message', 'Registration is complete. You can log in using your e-mail and password');
            }

            $identity = new MailingUserIdentity($user->id, $user->getLoginHash());
            if ($identity->authenticate()) {
                $duration = isset($this->app()->params['remember']) ? $this->app()->params['remember'] : 0;
                $this->app()->user->login($identity, $duration);
            }
        }

        if ($user->is_email_verified) {
            $this->layout = '//layouts/main'; // hack
            $this->render('finishRegistration');
        } else {
            $this->redirect(['index/index']);
        }
    }

    /**
     * @property-description Сброс пароля
     *
     * @param integer $uid userId
     * @param string $t token
     *
     * @throws CHttpException
     */
    public function actionPasswordRecovery($uid, $t)
    {
        $uid = Cast::toUInt($uid);
        $token = Cast::toStr($t);

        /** @var UserRecord $model */
        $model = UserRecord::model()->findByPk($uid);

        if ($model === null) {
            throw new CHttpException('404', 'Error');
        }

        $accessToken = $model->generateHashPasswordRecovery();

        if ($model->password_recovery_requested_at > time() - 86400 && $token === $accessToken) {
            //create form
            $formPassword = new NewPasswordForm();
            $data = $this->app()->request->getParam('NewPasswordForm');

            if ($data) {
                if ($this->app()->request->isAjaxRequest) {
                    echo CActiveForm::validate($formPassword);
                    $this->app()->end();
                }

                $formPassword->setAttributes($data);
                if ($formPassword->validate($data)) {
                    // ставим новый пароль и генерируем новую соль
                    $model->password_recovery_requested_at = 0;
                    $model->password_salt = '';
                    $model->password = $formPassword->password;
                    if (!$model->save()) {
                        $this->app()->user->setFlash('message', 'Error retrieving password!');
                        $this->redirect(['auth/auth', 'name' => 'login']);
                    }

                    /** @var UserInterface $user */
                    $user = $model->getEntity();
                    /** @var TokenStorageInterface $tokenStorage */
                    $tokenStorage = $this->container->get('security.token_storage');
                    $providerKey = $this->container->getParameter('frontend.firewall_key');
                    if ($this->authenticateUser($tokenStorage, $user, $providerKey)) {
                        $this->redirect($this->createUrl('index/index', []));
                    }

                    $this->redirect(['auth/auth', 'name' => 'login']);
                }
            }

            $this->render('newPassword', ['formPassword' => $formPassword]);
            $this->app()->end();
        }

        $this->redirect(['auth/auth', 'name' => 'forgot', 'status' => 'tokenExpired']);
    }

    public function actionLogout()
    {
        $this->app()->user->logout();
        $this->redirect(['index/index']);
    }

    public function actionTrack()
    {
        UserRegistrationTrackingRecord::fillWithRequestData();
    }

    /**
     * @param $userId
     *
     * @return bool
     */
    private function _saveDistribution($userId)
    {
        $distribution = new UserDistributionRecord();
        $distribution->user_id = $userId;
        $distribution->type = UserDistributionRecord::TYPE_EVERY_DAY;
        $distribution->is_subscribe = 1;
        $distribution->is_update = 1;
        return $distribution->save();
    }

    private function fillCart()
    {
        $cartCreated = false;

        /** @var SessionInterface $session */
        $session = $this->container->get('session');

        if ($session->has('eventId') &&
            $session->has('productId') &&
            $session->has('size') &&
            $session->has('number')
        ) {
            /* @var EventProductRecord $product */
            $product = EventProductRecord::model()
                ->eventId($session->get('eventId'))
                ->productId($session->get('productId'))
                ->size($session->get('size'))
                ->find();

            if (null !== $product) {
                $this->app()->user->cart->put($product, $session->get('number'));
                $cartCreated = true;
            }

            $sessionGarbage = ['eventId', 'productId', 'size', 'number'];
            foreach ($sessionGarbage as $sessionKey) {
                $session->remove($sessionKey);
            }
        }

        return $cartCreated;
    }
}
