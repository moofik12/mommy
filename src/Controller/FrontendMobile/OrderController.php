<?php

namespace MommyCom\Controller\FrontendMobile;

use CHtml;
use CHttpException;
use Doctrine\ORM\EntityManager;
use MommyCom\Entity\Order;
use MommyCom\Entity\User;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\SplitTestTrackingRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Db\UserRegistrationTrackingRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\RecoveryPasswordForm;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Repository\OrderRepository;
use MommyCom\Security\User\UserBuilder;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Delivery\FormModel\Dummy;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\Service\PaymentGateway\AvailablePaymentGateways;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\TelegramBot;
use MommyCom\YiiComponent\AuthManager\AuthenticationTrait;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\ShopLogic\ChoosePaymentMethod;
use MommyCom\YiiComponent\ShopLogic\ShopOrderFactory;
use MommyCom\YiiComponent\TokenManager;
use MommyCom\YiiComponent\Type\Cast;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OrderController extends FrontController
{
    use AuthenticationTrait;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['auth', 'index', 'registration', 'delivery1', 'delivery2', 'recovery', 'passwordRecovery'],
                'users' => ['?'],
            ],
            [
                'allow',
                'actions' => ['auth', 'index', 'delivery1', 'delivery2', 'success',
                    'ajaxFeedrProvinces', 'ajaxFeedrCities', 'ajaxFeedrSuburbs', 'ajaxFeedrAreas',
                ],
                'users' => ['@'],
            ],
            [
                'deny',
            ],
        ];
    }

    public function actionIndex()
    {
        $cart = $this->app()->user->cart;
        $bonuspoints = $this->app()->user->bonuspoints;

        $this->productHeader = CHtml::tag(
            'span',
            ['class' => 'table'],
            '<span class="back-container-link"><span class="back-container-content">' . $this->t('Cart') . '</span></span>'
        );
        $view = $cart->count == 0 ? 'indexEmpty' : 'index';

        if (!$this->app()->request->isAjaxRequest) {
            $this->render($view, [
                    'cart' => $cart,
                    'bonuspoints' => $bonuspoints,
                    'returnUrl' => $this->getSavedBackUrl(),
                ] + $this->app()->splitTesting->getAvailable());
        } else {
            $this->renderPartial($view, [
                    'cart' => $cart,
                    'bonuspoints' => $bonuspoints,
                    'returnUrl' => $this->getSavedBackUrl(),
                ] + $this->app()->splitTesting->getAvailable());
        }
    }

    /**
     * @param string $name
     * @param string $social
     */
    public function actionAuth($name = 'registration', $social = '')
    {
        $request = $this->app()->request;

        $this->productHeader = CHtml::tag(
            'span',
            ['class' => 'table'],
            '<span class="back-container-link"><span class="back-container-content">' . $this->t('Checkout') . '</span></span>'
        );

        $this->saveBackUrl();

        $tabs = [
            'login',
            'registration',
            'forgot',
        ];

        $socialIn = [
            'socialNotEmail',
            'socialConfirm',
        ];

        if (!in_array($social, $socialIn)) {
            $social = '';
        }

        if (!in_array($name, $tabs)) {
            $name = 'login';
        }

        $authForm = new AuthForm();

        //processing     static::actionRecovery()
        $recoveryForm = new RecoveryPasswordForm();

        //processing     static::actionRegistration()
        $registrationForm = new RegistrationForm();
        $registrationForm->setScenario('simple');

        if ($request->isAjaxRequest && $request->getPost('ajax')) {
            echo \CActiveForm::validate($authForm);
            $this->app()->end();
        }

        $dataAuthForm = $request->getPost('AuthForm');

        if (!empty($dataAuthForm)) {
            $authForm->setAttributes($dataAuthForm);
            if ($authForm->validate() && $authForm->login()) {
                $this->redirect(['order/delivery1']);
            }
        }

        $authForm->unsetAttributes(['password']);

        $symfonySession = $this->container->get('session');
        $symfonySession->set('_security.main.target_path', $this->createUrl('order/delivery1'));

        $this->render('auth', [
            'tab' => $name,
            'social' => $social,
            'authForm' => $authForm,
            'recoveryForm' => $recoveryForm,
            'registrationForm' => $registrationForm,
        ]);
    }

    public function actionDelivery1()
    {
        $request = $this->app()->request;

        /** @var ShopWebUser $user */
        $user = $this->app()->user;

        if ($user->isGuest) {
            $this->redirect(["order/auth"]);
        }

        $cart = $user->cart;

        $cart->giveBonusTime();

        if (!$cart->isAvailableToBuy()) {
            $this->redirect(['cart/index']);
        }

        $orderAttributes = $request->getPost('ShopOrder', []);

        $order = ShopOrderFactory::createShopOrder($this->container);
        $order->setAttributes($orderAttributes);

        $orderSessionData = $this->getSessionOrderData()[get_class($order)]['form_attributes'] ?? [];
        $order->setAttributes($orderSessionData);

        $isPostRequest = $request->isPostRequest;

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        $deliveryModels = [];
        foreach ($availableDeliveries as $delivery) {
            $deliveryModel = $delivery->createFormModel();
            $orderSessionData = $this->getSessionOrderData()[get_class($deliveryModel)]['form_attributes'] ?? [];
            $deliveryModel->setAttributes($orderSessionData);

            $deliveryModels[$delivery->getId()] = $deliveryModel;
        }

        if ($request->isAjaxRequest) {
            $delivery = $availableDeliveries->getDelivery($order->deliveryType);
            $deliveryModel = $delivery->createFormModel();
            $deliveryModel->setScenario('step1');
            $deliveryModel->setAttributesFromRequest($request);

            echo \CActiveForm::validate([$order, $deliveryModel]);
            $this->app()->end();
        }

        if ($isPostRequest && $order->validate()) {
            $delivery = $availableDeliveries->getDelivery($order->deliveryType);
            $deliveryModel = $delivery->createFormModel();
            $deliveryModel->setScenario('step1');
            $deliveryModel->setAttributesFromRequest($request);

            if ($deliveryModel->validate()) {
                $this->setSessionOrderData([
                    get_class($order) => [
                        'form_attributes' => $order->getAttributes(),
                    ],
                    get_class($deliveryModel) => [
                        'form_attributes' => $deliveryModel->getAttributes(),
                        'special_data' => $this->getDeliverySpecialData($deliveryModel),
                    ],
                ]);
                $this->redirect(['order/delivery2']);
            }
        }

        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);
        $region = $regions->getServerRegion()->getRegionName();
        $countryCode = (string)current(Region::COUNTRY_CODES[$region]);

        $deliveryType = $order->deliveryType ?: $availableDeliveries->getFirstDelivery()->getId();

        $this->render('delivery1', [
            'cart' => $cart,
            'order' => $order,
            'deliveryType' => $deliveryType,
            'availableDeliveries' => $availableDeliveries,
            'countryCode' => $countryCode,
            'deliveryModels' => $deliveryModels,
        ]);
    }

    /**
     * @param DeliveryModel $deliveryModel
     *
     * @return array
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    private function getDeliverySpecialData(DeliveryModel $deliveryModel)
    {
        switch (get_class($deliveryModel)) {
            case Feedr::class:
                $feedrRates = $this->getFeedrRates($deliveryModel->areaId);
                return [
                    'rates' => array_merge($feedrRates['regularWithoutCod'], $feedrRates['regularWithCod']),
                ];
            case Dummy::class:
                return [];
            default:
                return [];
        }
    }

    public function actionDelivery2()
    {
        $request = $this->app()->request;

        /** @var ShopWebUser $user */
        $user = $this->app()->user;

        if ($user->isGuest) {
            $this->redirect(["order/auth"]);
        }

        $cart = $user->cart;

        $cart->giveBonusTime();

        if (!$cart->isAvailableToBuy()) {
            $this->redirect(['cart/index']);
        }

        $orderAttributes = $request->getPost('ShopOrder', []);

        $order = ShopOrderFactory::createShopOrder($this->container);
        $sessionOrderData = $this->getSessionOrderData();
        $order->setAttributes(array_merge(
            $orderAttributes,
            $sessionOrderData[get_class($order)]['form_attributes'] ?? []
        ));

        $isPostRequest = $request->isPostRequest;

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        $deliveryModels = [];
        foreach ($availableDeliveries as $delivery) {
            $deliveryModel = $delivery->createFormModel();
            $orderSessionData = $this->getSessionOrderData()[get_class($deliveryModel)]['form_attributes'] ?? [];
            $deliveryModel->setAttributes($orderSessionData);

            $deliveryModels[$delivery->getId()] = $deliveryModel;
        }

        if ($request->isAjaxRequest) {
            $delivery = $availableDeliveries->getDelivery($order->deliveryType);
            $deliveryModel = $delivery->createFormModel();
            $deliveryModel->setScenario('step2');
            $deliveryModel->setAttributesFromRequest($request);

            echo \CActiveForm::validate([$order, $deliveryModel]);
            $this->app()->end();
        }

        if ($isPostRequest && $order->validate()) {
            $delivery = $availableDeliveries->getDelivery($order->deliveryType);
            $deliveryModel = $delivery->createFormModel();
            $orderSessionData = $this->getSessionOrderData()[get_class($deliveryModel)]['form_attributes'] ?? [];
            $deliveryModel->setAttributes($orderSessionData);
            $deliveryModel->setScenario('step2');
            $deliveryModel->setAttributesFromRequest($request);

            $record = null;
            if ($deliveryModel->validate()) {
                $this->setSessionOrderData(array_replace_recursive($this->getSessionOrderData(), [
                    get_class($deliveryModel) => [
                        'form_attributes' => $deliveryModel->getAttributes(),
                    ],
                ]));

                $record = $order->makeOrder($user, $deliveryModel);
            }

            if (null !== $record) {
                $this->setSessionOrderData([]);

                /** @var TelegramBot $telegramBot */
                $telegramBot = $this->container->get(TelegramBot::class);

                /** @var EntityManager $em */
                $em = $this->container->get('doctrine')->getManager();

                /** @var OrderRepository $orderRepository */
                $orderRepository = $em->getRepository(Order::class);

                if ($telegramBot->getToken()) {
                    $telegramBot->sendMessage(
                        getenv('DAILY_REPORT_TELEGRAM_CHAT_ID'),
                        sprintf(
                            'Поступил новый заказ! (%s), всего поступило заказов: %d',
                            getenv('REGION'),
                            $orderRepository->getOrdersCountForTest()
                        )
                    );
                }

                /** @var TokenManager $tokenManager */
                $tokenManager = $this->container->get(TokenManager::class);
                $successToken = $tokenManager->getToken('successAction');
                $this->redirect(['order/success', 'id' => $record->id, 'token' => $successToken]);
            }
        }

        $deliveryType = $order->deliveryType ?: $availableDeliveries->getFirstDelivery()->getId();

        $this->render('delivery2', [
            'cart' => $cart,
            'order' => $order,
            'deliveryType' => $deliveryType,
            'availableDeliveries' => $availableDeliveries,
            'sessionOrderData' => $sessionOrderData,
            'deliveryModels' => $deliveryModels,
        ]);
    }

    /**
     * @param string $id
     * @param string $token
     *
     * @throws CHttpException
     */
    public function actionSuccess($id, $token)
    {
        /** @var TokenManager $tokenManager */
        $tokenManager = $this->container->get(TokenManager::class);

        if (!$tokenManager->isValid('successAction', $token)) {
            $this->redirect(['order/index']);
        }

        $uid = trim($id);
        $order = current(OrderRecord::model()->findByUid($uid));

        if (!$order instanceof OrderRecord) {
            throw new CHttpException(404, $this->t('The specified order was not found'));
        }

        if ($order->user_id !== $this->app()->user->id) {
            throw new CHttpException(400);
        }

        $request = $this->app()->request;
        $model = new ChoosePaymentMethod();
        $model->setAttributes($request->getPost('ChoosePaymentMethod', []));

        if ($request->isAjaxRequest) {
            echo \CActiveForm::validate($model);
            $this->app()->end();
        }

        $deliveryModel = $order->getDeliveryModel();
        $deliveryModel->setScenario('step3');
        $model->cashOnDelivery = $deliveryModel->isCashOnDelivery();

        $this->productHeader = CHtml::tag(
            'span',
            ['class' => 'table'],
            '<span class="back-container-link"><span class="back-container-content">' . $this->t('Choose a payment type') . '</span></span>'
        );

        /** @var AvailablePaymentGateways $availablePaymentGateways */
        $availablePaymentGateways = $this->container->get(AvailablePaymentGateways::class);

        $paymentMethods = [];
        foreach ($availablePaymentGateways->getPaymentMethods() as $paymentMethod) {
            $paymentMethods[] = [
                'id' => $paymentMethod->getId(),
                'name' => $paymentMethod->getName(),
            ];
        }

        $this->render('success', [
            'model' => $model,
            'order' => $order,
            'uid' => $uid,
            'canPrepay' => $order->isAvailableForPayment(),
            'availableGateways' => $availablePaymentGateways->getPaymentGateways(),
            'availableMethods' => $paymentMethods,
        ]);
    }

    /**
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrProvinces($filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $provinces = $feedrApi->getProvinces();
        $provinces = $this->filterByAttribute($provinces, 'name', trim($filter));

        $this->renderJson($provinces);
    }

    /**
     * @param string $provinceId
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrCities($provinceId, $filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $cities = $feedrApi->getCities((int)$provinceId);
        $cities = $this->filterByAttribute($cities, 'name', trim($filter));

        $this->renderJson($cities);
    }

    /**
     * @param string $cityId
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrSuburbs($cityId, $filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $suburbs = $feedrApi->getSuburbs((int)$cityId);
        $suburbs = $this->filterByAttribute($suburbs, 'name', trim($filter));

        $this->renderJson($suburbs);
    }

    /**
     * @param string $suburbId
     * @param string $filter
     *
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxFeedrAreas($suburbId, $filter = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $areas = $feedrApi->getAreas((int)$suburbId);
        $areas = $this->filterByAttribute($areas, 'name', trim($filter));

        $this->renderJson($areas);
    }

    /**
     * @property-description Восстановление пароля
     */
    public function actionRecovery()
    {
        $recoveryForm = new RecoveryPasswordForm();

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo \CActiveForm::validate($recoveryForm);
            $this->app()->end();
        }

        $dataForm = $this->app()->getRequest()->getPost('RecoveryPasswordForm');

        if (!empty($dataForm)) {
            $recoveryForm->setAttributes($dataForm);
            if ($recoveryForm->validate()) {
                /** @var UserRecord $model */
                $model = UserRecord::model()->emailIs($recoveryForm->email)->find();

                if ($model === null || Cast::toUInt($model->status) !== UserRecord::STATUS_ACTIVE) {
                    $this->app()->user->setFlash('message', 'Profile not found or blocked');
                } else {
                    $model->password_recovery_requested_at = time();

                    if ($model->save(true, ['password_recovery_requested_at'])) {
                        $mail = $this->app()->mailer;
                        /** @var $mailer Emailer */
                        $mail->create(
                            [$this->app()->params['layoutsEmails']['recoveryPassword'] => $this->app()->params['distributionEmailName']],
                            [$model->email],
                            'Password recovery',
                            ['view' => 'recoveryPasswordCheckout', 'data' => ['user' => $model]]
                        );

                        $this->app()->user->setFlash('message', 'На ' . CHtml::encode($recoveryForm->email)
                            . ' выслано письмо для восстановления пароля');
                    } else {
                        $this->app()->user->setFlash('message', 'Error retrieving password');
                    }
                }
            }
        }

        $this->redirect(['order/auth', 'name' => 'login']);
    }

    /**
     * @property-description Сброс пароля
     *
     * @param integer $uid userId
     * @param string $t token
     *
     * @throws CHttpException
     */
    public function actionPasswordRecovery($uid, $t)
    {
        $uid = Cast::toUInt($uid);
        $token = Cast::toStr($t);

        $em = $this->container->get('doctrine')->getEntityManager();

        $userEntity = $em->getRepository(User::class)->find($uid);

        /** @var UserRecord $model */
        $model = UserRecord::model()->findByPk($uid);

        if ($model === null) {
            throw new CHttpException('404', 'Error');
        }

        $accessToken = $model->generateHashPasswordRecovery();

        if ($model->password_recovery_requested_at > time() - 86400 && $token === $accessToken) {
            $tokenStorage = $this->container->get('security.token_storage');
            $providerKey = $this->container->getParameter('frontend.firewall_key');
            if ($this->authenticateUser($tokenStorage, $userEntity, $providerKey)) {
                $this->redirect($this->createUrl('order/delivery1'));
            }
            $this->redirect(['order/auth']);
        }

        throw new CHttpException('404', 'Error');
    }

    /**
     * @throws \CException
     * @throws \MommyCom\Security\User\UserBuilderException
     */
    public function actionRegistration()
    {
        /** @var Request $request */
        $request = $this->container->get('request_stack')->getCurrentRequest();

        $form = new RegistrationForm();
        $form->setScenario('checkout');

        if ($request->isXmlHttpRequest() && $request->get('ajax')) {
            echo \CActiveForm::validate($form);
            $this->app()->end();
        }

        $dataForm = $request->get('RegistrationForm');
        if (!empty($dataForm)) {
            $form->setAttributes($dataForm);

            if ($form->validate()) {
                /** @var EntityManagerInterface $entityManager */
                $entityManager = $this->container->get('doctrine')->getManager();
                /** @var UserBuilder $userBuilder */
                $userBuilder = $this->container->get(UserBuilder::class);
                /** @var EmailUtils $emailUtils */
                $emailUtils = $this->container->get(EmailUtils::class);

                $user = $userBuilder
                    ->setEmail($form->email)
                    ->setName($form->name)
                    ->build();

                $entityManager->persist($user);
                $entityManager->flush();

                $senderEmail = $this->app()->params['layoutsEmails']['welcome2'];
                $senderName = $this->app()->params['distributionEmailName'];
                $emailUtils->sendRegistrationEmail($user, $userBuilder->getPassword(), $senderEmail, $senderName);

                $splitTestTracking = new SplitTestTrackingRecord();
                $splitTestTracking->target = SplitTestTrackingRecord::TARGET_USER_REGISTER;
                $splitTestTracking->user_id = $user->getId();
                $splitTestTracking->order_id = 0;
                $splitTestTracking->testParams = $this->app()->splitTesting->getAvailable();
                $splitTestTracking->save();

                UserRegistrationTrackingRecord::fillWithRequestData();

                /** @var TokenStorageInterface $tokenStorage */
                $tokenStorage = $this->container->get('security.token_storage');
                $providerKey = $this->container->getParameter('frontend.firewall_key');
                if ($this->authenticateUser($tokenStorage, $user, $providerKey)) {
                    $this->redirect($this->createUrl('order/delivery1'));
                }
            } else {
                $this->redirect($this->createUrl('order/auth/login'));
            }
        }
    }

    /**
     * @param array $rates
     *
     * @param bool $isCod
     *
     * @return array
     */
    private function prepareDomesticRates(array $rates, bool $isCod): array
    {
        usort($rates, function (array $a, array $b) {
            return $a['finalRate'] - $b['finalRate'];
        });

        array_walk($rates, function (array &$item) use ($isCod) {
            $item = [
                'id' => $item['rate_id'] * ($isCod ? 1 : -1),
                'name' => implode(' - ', $isCod ?
                    [$item['name'], $item['rate_name'], $item['finalRate'] . ' IDR', 'COD'] :
                    [$item['name'], $item['rate_name'], $item['finalRate'] . ' IDR']
                ),
            ];
        });

        return $rates;
    }

    /**
     * @param array $data
     * @param string $attribute
     * @param string $filter
     *
     * @return array
     */
    private function filterByAttribute(array $data, string $attribute, string $filter): array
    {
        if ('' === $filter) {
            return $data;
        }

        $result = [];
        foreach ($data as $item) {
            if ('' === $filter || false !== strpos(mb_strtolower($item[$attribute]), mb_strtolower($filter))) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param int $areaId
     *
     * @return array
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    private function getFeedrRates($areaId): array
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        /** @var ShopWebUser $user */
        $user = $this->app()->user;
        $cart = $user->cart;

        $weight = $cart->getWeight() / 1000;
        $cost = $cart->getCost();

        $domesticRates = $feedrApi->getDomesticRates((int)$areaId, $weight, 30, 20, 10, $cost, false);
        $domesticRatesCod = $feedrApi->getDomesticRates((int)$areaId, $weight, 30, 20, 10, $cost, true);

        return [
            'regularWithoutCod' => $this->prepareDomesticRates($domesticRates['regular'] ?? [], false),
            'expressWithoutCod' => $this->prepareDomesticRates($domesticRates['express'] ?? [], false),
            'regularWithCod' => $this->prepareDomesticRates($domesticRatesCod['regular'] ?? [], true),
            'expressWithCod' => $this->prepareDomesticRates($domesticRatesCod['express'] ?? [], true),
        ];
    }

    /**
     * @return array
     */
    private function getSessionOrderData()
    {
        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $this->container->get('security.token_storage');

        $token = $tokenStorage->getToken();

        if (!$token->hasAttribute('order_data')) {
            $token->setAttribute('order_data', []);
        }

        return $token->getAttribute('order_data');
    }

    /**
     * @param mixed $data
     */
    private function setSessionOrderData($data)
    {
        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $this->container->get('security.token_storage');

        $tokenStorage->getToken()->setAttribute('order_data', $data);
    }
}
