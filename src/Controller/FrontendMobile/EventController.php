<?php

namespace MommyCom\Controller\FrontendMobile;

use CArrayDataProvider;
use CException;
use CHtml;
use CHttpException;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class EventController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    /**
     * @return array
     */
    public function cache()
    {
        return [];
    }

    /**
     * @param mixed $sort
     *
     * @return array
     */
    protected function _sanitizeSort($sort)
    {
        if ($sort == 'price_desc') {
            return ['price', 'desc'];
        } elseif ($sort == 'price_asc') {
            return ['price', 'asc'];
        }
        return []; // sort is invalid or empty
    }

    /**
     * @param mixed $size
     *
     * @return string[]
     */
    protected function _sanitizeSize($size)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toStrArr($size))), 0, 20);
    }

    /**
     * @param mixed $color
     *
     * @return string[]
     */
    protected function _sanitizeColor($color)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toStrArr($color))), 0, 20);
    }

    /**
     * @param mixed $target
     *
     * @return string[]
     */
    protected function _sanitizeTarget($target)
    {
        return array_slice(array_unique(
            ProductTargets::instance()->sanitizeTargets(
                ArrayUtils::onlyNonEmpty(Cast::toStrArr($target))
            )
        ), 0, 20);
    }

    /**
     * @param mixed $ageGroup
     *
     * @return string[]
     */
    protected function _sanitizeAgeGroup($ageGroup)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toStrArr($ageGroup))), 0, 20);
    }

    /**
     * @param mixed $brand
     *
     * @return int[]
     */
    protected function _sanitizeBrand($brand)
    {
        return array_slice(array_unique(ArrayUtils::onlyNonEmpty(Cast::toUIntArr($brand))), 0, 20);
    }

    /**
     * @param EventProductRecord $selector
     *
     * @throws CException
     */
    protected function _applyOrderBySoldOut(EventProductRecord $selector)
    {
        $selector->orderBy('is_sold_out', 'asc', 'start');
    }

    /**
     * @param EventProductRecord $selector
     *
     * @throws CException
     */
    protected function _applyFilter(EventProductRecord $selector)
    {
        $request = $this->app()->request;
        $sort = $this->_sanitizeSort($request->getParam('sort', []));
        $size = $this->_sanitizeSize($request->getParam('size', []));
        $color = $this->_sanitizeColor($request->getParam('color', []));
        $target = $this->_sanitizeTarget($request->getParam('target', []));
        $brand = $this->_sanitizeBrand($request->getParam('brand', []));
        $ageGroup = $this->_sanitizeAgeGroup($request->getParam('ageGroup', []));

        /* @var EventProductRecord $selector */
        if ($sort !== []) {
            $selector->orderBy($sort[0], $sort[1], 'start');
        }

        if (count($brand) > 0) {
            $selector->brandIds($brand);
        }

        if (count($size) > 0) {
            $selector->sizes($size);
        }

        if (count($color) > 0) {
            $selector->colors($color);
        }

        if (count($target) > 0) {
            $selector->targets($target);
        }

        if (count($ageGroup) > 0) {
            $selector->ageGroups($ageGroup);
        }
    }

    /**
     * @param string $id
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionIndex($id)
    {
        $this->saveBackUrl(true);
        $model = EventRecord::model()->findByPk($id);
        $request = $this->app()->request;

        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        /* @var $model EventRecord */

        if ($model === null || $model->is_deleted) {
            throw new CHttpException(404, 'This promotion was not found');
        }

        if ($model->is_virtual) {
            throw new CHttpException(400, 'Sorry, you cannot view this promotion');
        }

        if (!$model->isStatusPublished) {
            throw new CHttpException(400, 'Sorry, this promotion is temporarily unavailable');
        }

        $selector = EventProductRecord::model()
            ->eventId($model->id)
            ->splitByMadeInVariant($variant)
            ->cache(30)
            ->orderBy('id', 'asc')
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => 21,
            ],
        ]);

        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(30)->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $filterProducts = GroupedProductsFilter::fromEvent($model);

        $iconSpeedDelivery = '';
        if ($model->is_drop_shipping) {
            $iconSpeedDelivery = CHtml::link('',
                $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']),
                ['title' => "What is 'Fast Delievery'?", 'class' => 'icon-fast-delivery']
            );
        }
        $this->productHeader = CHtml::tag(
            'div',
            ['class' => 'table'],
            '<div class="back-container-link"><h1 class="back-container-content">' . CHtml::encode($model->name) . "</h1>$iconSpeedDelivery</div>"
        );

        if (!$request->isAjaxRequest) {
            $this->_composeFilterHeaderHtml($groupedProducts, $filterProducts);
        }
        $this->description = trim(str_replace(["\n", "\r\n", '"'], "", strip_tags($model->description)));

        $renderMethod = $request->isAjaxRequest ? 'renderPartial' : 'render';
        $this->{$renderMethod}('index', [
            'groupedProducts' => $groupedProducts,
            'filterProducts' => $filterProducts,
            'provider' => $provider,
            'title' => $model->name,
            'showEvent' => false,
            'returnParams' => [],
        ]);
    }

    /**
     * @param string $category
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionCategory($category)
    {
        /** @var \MommyCom\Service\SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $this->saveBackUrl(true);
        $categoryGroups = CategoryGroups::instance();
        /* @var $categoryGroups CategoryGroups */
        $request = $this->app()->request;

        if (!$categoryGroups->isValid($category)) {
            throw new CHttpException(404, 'Category not found');
        }

        $internalCategories = $categoryGroups->getCategories($category);

        $targets = CategoryGroups::instance()->getTarget($category);

        // extract current events
        $activeEventIds = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->isVirtual(false)
            ->onlyPublished()
            ->onlyTimeActive()
            ->onlyVisible()
            ->cache(30)
            ->findColumnDistinct('id');

        // extract unique products
        $productIdsSelector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->splitByMadeInVariant($variant)
            ->categories($internalCategories);
        /* @var $productIdsSelector EventProductRecord */
        if ($targets !== false) {
            $productIdsSelector->targets($targets);
        }

        $productIds = $productIdsSelector
            ->cache(30)
            ->findColumnDistinct('product_id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->productIdIn($productIds)
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->cache(30)->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(30)->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $filterProducts = GroupedProductsFilter::fromEventsId($activeEventIds, $productIds);

        $returnParams = [
            'returnType' => 'category',
            'category' => $category,
        ];

        $this->productHeader = CHtml::tag(
            'div',
            ['class' => 'table'],
            '<div class="back-container-link"><h1 class="back-container-content">' . Utf8::ucfirst($categoryGroups->getLabel($category)) . '</h1></div>'
        );

        if (!$request->isAjaxRequest) {
            $this->_composeFilterHeaderHtml(
                $groupedProducts,
                $filterProducts,
                [
                    'showTargetFilter' => false,
                ]
            );
        }

        $renderMethod = $request->isAjaxRequest ? 'renderPartial' : 'render';
        $this->{$renderMethod}('index', [
            'groupedProducts' => $groupedProducts,
            'filterProducts' => $filterProducts,
            'provider' => $provider,
            'title' => Utf8::ucfirst($categoryGroups->getLabel($category)),
            'showEvent' => true,
            'returnParams' => $returnParams,
        ]);
    }

    /**
     * @param string $age
     * @param string $target
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionAge($age, $target)
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $this->saveBackUrl(true);
        $request = $this->app()->request;
        $isAgesSupported = ProductTargets::instance()->getIsSupportsAge($target);
        $ageRange = AgeGroups::instance()->getAgeRange($age);

        if (!ProductTargets::instance()->isValid($target)) {
            throw new CHttpException(404, 'Sex is not supported');
        }

        if ($ageRange === false) {
            throw new CHttpException(400, 'Age not supported');
        }

        if (!$isAgesSupported) {
            throw new CHttpException(400, 'Age filter is not supported for this gender');
        }

        // extract current events
        $activeEventIds = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->isVirtual(false)
            ->onlyPublished()
            ->onlyTimeActive()
            ->onlyVisible()
            ->cache(30)
            ->findColumnDistinct('id');

        // extract unique products
        $productIds = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->splitByMadeInVariant($variant)
            ->ageBetween($ageRange[0], $ageRange[1])
            ->target($target)
            ->findColumnDistinct('product_id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->productIdIn($productIds)
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        // group the products
        $productsIds = $selector->copy()->cache(30)->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(30)->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $filterProducts = GroupedProductsFilter::fromEventsId($activeEventIds, $productIds);

        $productTargets = ProductTargets::instance();
        $ageGroups = AgeGroups::instance();

        $returnParams = [
            'returnType' => 'age',
            'target' => $target,
            'age' => $age,
        ];

        $this->productHeader = CHtml::tag(
            'div',
            ['class' => 'table'],
            '<div class="back-container-link"><h1 class="back-container-content">' . Utf8::ucfirst($productTargets->getLabel($target)) . ' ' . $ageGroups->getLabel($age) . '</h1></div>'
        );

        if (!$request->isAjaxRequest) {
            $this->_composeFilterHeaderHtml(
                $groupedProducts,
                $filterProducts,
                [
                    'showAgeGroupFilter' => false,
                    'showTargetFilter' => false,
                ]
            );
        }

        $renderMethod = $request->isAjaxRequest ? 'renderPartial' : 'render';
        $pageLabel = Utf8::ucfirst($productTargets->getLabel($target)) . ' ' . $ageGroups->getLabel($age);
        $this->description = $this->t('Goods') . ' ' . $pageLabel . ' - ' . $this->t('MOMMY.COM: Shopping club for moms and children');

        $this->{$renderMethod}('index', [
            'groupedProducts' => $groupedProducts,
            'filterProducts' => $filterProducts,
            'provider' => $provider,
            'title' => $pageLabel,
            'showEvent' => true,
            'returnParams' => $returnParams,
        ]);
    }

    /**
     * @param string $name
     *
     * @throws CException
     * @throws CHttpException
     */
    public function actionBrand($name)
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $this->saveBackUrl(true);
        $request = $this->app()->request;

        /** @var BrandRecord $brand */
        $brand = BrandRecord::model()->name($name)->find();

        if ($brand === null) {
            throw new CHttpException(200, 'Brand not supported');
        }

        // extract current events
        $eventRecord = EventRecord::model();

        $activeEventIds = $eventRecord
            ->splitByMadeInVariant($variant)
            ->display(30)
            ->findColumnDistinct($eventRecord->getTableAlias() . '.id');

        // extract unique products
        $productIds = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->splitByMadeInVariant($variant)
            ->brandId($brand->id)
            ->cache(30)
            ->findColumnDistinct('product_id');

        // now extract products
        $selector = EventProductRecord::model()
            ->eventIdIn($activeEventIds)
            ->productIdIn($productIds)
            ->cache(30)
            ->with('brand', 'event', 'product');

        $this->_applyFilter($selector);
        $this->_applyOrderBySoldOut($selector);

        $productsIds = $selector->copy()->findColumnDistinct('product_id');

        $provider = new CArrayDataProvider($productsIds, [
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        // group the products
        $groupedProducts = GroupedProducts::fromProducts($selector->productIdIn($provider->getData())->cache(30)->findAll());
        $groupedProducts->sortBy('isSoldOut');
        $filterProducts = GroupedProductsFilter::fromEventsId($activeEventIds, $productIds);

        $returnParams = [
            'returnType' => 'brand',
            'name' => $brand->name,
        ];

        $this->productHeader = CHtml::tag(
            'div',
            ['class' => 'table'],
            '<div class="back-container-link"><h1 class="back-container-content">' . CHtml::encode(Utf8::ucfirst($brand->name)) . '</h1></div>'
        );

        if (!$request->isAjaxRequest) {
            $this->_composeFilterHeaderHtml(
                $groupedProducts,
                $filterProducts,
                [
                    'showBrandFilter' => false,
                ]
            );
        }
        $this->description = $this->t('Brand products {pageLabel} - MOMMY.COM The first shopping club in USA for mothers and children. Products for the whole family and at home. Daily discounts of up to 90%.', ['{pageLabel}' => Utf8::ucfirst($brand->name)]);

        $renderMethod = $request->isAjaxRequest ? 'renderPartial' : 'render';
        $this->{$renderMethod}('index', [
            'groupedProducts' => $groupedProducts,
            'filterProducts' => $filterProducts,
            'provider' => $provider,
            'title' => 'Товары бренда ' . Utf8::ucfirst($brand->name),
            'showEvent' => true,
            'returnParams' => $returnParams,
        ]);
    }

    /**
     * @throws CHttpException
     */
    public function actionFinalSell()
    {
        throw new CHttpException(404, 'The promotion is currently not available');
    }

    /**
     * @param GroupedProducts $groupedProducts
     * @param GroupedProductsFilter $filterProducts
     * @param array $otherParams
     *
     * @throws CException
     */
    public function _composeFilterHeaderHtml(GroupedProducts $groupedProducts, GroupedProductsFilter $filterProducts, array $otherParams = [])
    {
        $this->filterHeaderHtml = $this->renderPartial('_filterHeaderHtml', [
                'groupedProducts' => $groupedProducts,
                'filterProducts' => $filterProducts,
            ] + $otherParams, true);
    }
}
