<?php

namespace MommyCom\Controller\FrontendMobile;

use CActiveForm;
use CArrayDataProvider;
use CSort;
use MommyCom\Model\Db\InviteRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\UserAccountForm;
use MommyCom\Model\Form\UserSettingsForm;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\FrontendMobile\ReplyResult;

class AccountController extends FrontController
{
    /**
     * @var string
     */
    public $defaultAction = 'orders';

    /**
     * @var array
     */
    public $menu;

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + inviteByEmail']);
    }

    public function actionAbout()
    {
        $this->translateMenuLabels();
        $model = new UserAccountForm();
        $user = $this->app()->user->getModel();

        // prefill form
        $model->name = $user->name;
        $model->surname = $user->surname;
        $model->email = $user->email;
        $model->address = $user->address;
        $model->telephone = $user->telephone;
        // end prefill

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        $formData = $this->app()->request->getPost('UserAccountForm');
        $model->setAttributes($formData);

        if ($formData && $model->validate()) {
            // fill model
            $user->name = $model->name;
            $user->surname = $model->surname;
            $user->email = $model->email;
            $user->address = $model->address;
            $user->telephone = $model->telephone;
            // end fill

            if ($user->save()) {
                //$this->app()->user->setFlash('message', 'Data saved');
            } else {
                //$this->app()->user->setFlash('message', 'Error while saving');
                $model->addErrors($user->errors);
            }
        }

        $body = $this->renderPartial('_about', ['model' => $model], true);
        $this->menu = ArrayUtils::merge($this->menu, ['about' => ['template' => "{menu}$body"]]);

        $this->render('main');
    }

    public function actionSettings()
    {
        $this->translateMenuLabels();
        $form = new UserSettingsForm();
        $user = $this->app()->user->getModel();

        //feed form
        $form->isWishSystemMail = $user->is_system_subscribe;
        $form->isWishPromoMail = $user->is_subscribe;

        $form->distribution = UserDistributionRecord::TYPE_EVERY_DAY;
        $distribution = UserDistributionRecord::model()
            ->userId($user->id)
            ->isSubscribe(1)
            ->find();
        if ($distribution !== null) {
            $form->distribution = $distribution->type;
        }

        if ($this->app()->request->isAjaxRequest && $this->app()->request->getPost('ajax')) {
            echo CActiveForm::validate($form);
            $this->app()->end();
        }

        $formData = $this->app()->request->getPost('UserSettingsForm');
        $form->setAttributes($formData);

        if ($formData && $form->validate()) {
            $model = $this->app()->user->getModel();
            $model->password = $form->newPassword;
            if ($model->save()) {
                $form->unsetAttributes(['password', 'newPassword', 'confirmPassword']);
                $this->_saveDistribution($form->distribution);
                $this->app()->user->setFlash('message', 'Data changed');
            } else {
                $this->app()->user->setFlash('message', 'Error updating data');
            }
        }

        $body = $this->renderPartial('_settings', ['form' => $form], true);
        $this->menu = ArrayUtils::merge($this->menu, ['settings' => ['template' => "{menu}$body"]]);

        $this->render('main');
    }

    public function actionInvite()
    {
        $this->translateMenuLabels();
        $invite = new InviteRecord();
        $model = $this->app()->user->getModel();
        $provider = InviteRecord::model()->userId($model->id)->getDataProvider(false, [
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);
        $body = $this->renderPartial('_invite', ['invite' => $invite, 'model' => $model, 'provider' => $provider], true);
        $this->menu = ArrayUtils::merge($this->menu, ['invite' => ['template' => "{menu}$body"]]);

        $this->render('main');
    }

    /**
     * filter ajaxOnly
     */
    public function actionInviteByEmail()
    {
        $this->translateMenuLabels();
        $invite = new InviteRecord();
        $model = $this->app()->user->getModel();

        $inviteData = $this->app()->request->getPost('InviteRecord');
        $invite->setAttributes($inviteData);
        $invite->user_id = $model->getPrimaryKey();

        if ($invite->save()) {
            //create mail
            $mail = $this->app()->mailer;
            $mail->create(
                [$this->app()->params['layoutsEmails']['inviteByEmail'] => $this->app()->params['distributionEmailName']],
                $invite->email,
                'Приглашение на сайт ' . $this->app()->name . ' от ' . $model->email,
                ['view' => 'inviteByEmail', 'data' => ['inviteEmail' => $model->email, 'user' => $model]]
            );

            $this->app()->end();
        }

        echo CActiveForm::validate($invite);
    }

    public function actionOrders()
    {
        $this->translateMenuLabels();
        $model = $this->app()->user->getModel();
        $provider = new CArrayDataProvider(
            OrderRecord::model()
                ->userId($model->id)
                ->limit(20)
                ->orderBy('id', 'desc')
                ->cache(10)
                ->with('positions', 'positions.event', 'positions.product')
                ->findAll()
        );

        $body = $this->renderPartial('_orders', ['model' => $model, 'provider' => $provider], true);
        $this->menu = ArrayUtils::merge($this->menu, ['orders' => ['template' => "{menu}$body"]]);

        $this->render('main');
    }

    public function actionBonuses()
    {
        $this->translateMenuLabels();
        $model = $this->app()->user->getModel();
        $totalBonuses = $this->app()->user->bonuspoints->getAvailableToSpend();

        $provider = new CArrayDataProvider(
            UserBonuspointsRecord::model()
                ->userId($model->id)
                ->limit(20)
                ->orderBy('id', 'desc')
                ->cache(10)
                ->findAll()
        );

        $body = $this->renderPartial('_bonuses', [
            'model' => $model,
            'provider' => $provider,
            'totalBonuses' => $totalBonuses,
        ], true);
        $this->menu = ArrayUtils::merge($this->menu, ['bonuses' => ['template' => "{menu}$body"]]);

        $this->render('main');
    }

    public function actionUnsubscribe()
    {
        $this->translateMenuLabels();
        $user = $this->app()->user->getModel();
        $user->is_system_subscribe = 0;
        if ($user->save(true, ['is_system_subscribe'])) {
            $this->redirect(['message/unsubscribe']);
        }

        $message = 'An error occurred while unsubscribing from newsletters';
        $this->redirect(['message/info',
                'title' => 'Unsubscribe from newsletters',
                'message' => $message,
                'description' => 'An unknown error occurred while unsubscribing from the mailing list',
                'hash' => $this->app()->tokenManager->getToken($message),
            ]
        );
    }

    public function actionUpdateDistribution($type)
    {
        $this->translateMenuLabels();
        $response = new ReplyResult();

        if (!in_array($type, array_keys(UserDistributionRecord::typeReplacements()))) {
            $response->addError('Error selecting subscription type');
        }

        $this->_saveDistribution($type);
        $response->setSuccess(true);
        $response->setInfo('Subscription completed successfully');
        $this->renderJson($response->toArray());
    }

    private function _saveDistribution($type = UserDistributionRecord::TYPE_EVERY_WEEK)
    {
        $this->translateMenuLabels();
        $user = $this->app()->user->getModel();
        /** @var $user UserRecord */
        if ($type == UserDistributionRecord::TYPE_EVERY_DAY) {
            $distributionEveryDay = UserDistributionRecord::model()
                ->type(UserDistributionRecord::TYPE_EVERY_DAY)
                ->userId($user->id)
                ->find();
            if ($distributionEveryDay === null) {
                $distributionEveryDay = new UserDistributionRecord();
                $distributionEveryDay->user_id = $user->id;
                $distributionEveryDay->type = UserDistributionRecord::TYPE_EVERY_DAY;
            }
            $distributionEveryDay->is_subscribe = 1;
            $distributionEveryDay->is_update = 1;
            $distributionEveryDay->save();
            $distributionEveryWeek = UserDistributionRecord::model()
                ->type(UserDistributionRecord::TYPE_EVERY_WEEK)
                ->userId($user->id)
                ->find();
            if ($distributionEveryWeek !== null) {
                $distributionEveryWeek->is_subscribe = 0;
                $distributionEveryWeek->is_update = 1;
                $distributionEveryWeek->save();
            }
        } else {
            $distributionEveryWeek = UserDistributionRecord::model()
                ->type(UserDistributionRecord::TYPE_EVERY_WEEK)
                ->userId($user->id)
                ->find();
            if ($distributionEveryWeek === null) {
                $distributionEveryWeek = new UserDistributionRecord();
                $distributionEveryWeek->user_id = $user->id;
                $distributionEveryWeek->type = UserDistributionRecord::TYPE_EVERY_WEEK;
            }
            $distributionEveryWeek->is_subscribe = 1;
            $distributionEveryWeek->is_update = 1;
            $distributionEveryWeek->save();
            $distributionEveryDay = UserDistributionRecord::model()
                ->type(UserDistributionRecord::TYPE_EVERY_DAY)
                ->userId($user->id)
                ->find();
            if ($distributionEveryDay !== null) {
                $distributionEveryDay->is_subscribe = 0;
                $distributionEveryDay->is_update = 1;
                $distributionEveryDay->save();
            }
        }
    }

    public function translateMenuLabels()
    {
        $this->menu = [
            'orders' => ['label' => $this->t('My orders'), 'url' => ['account/orders']],
            'bonuses' => ['label' => $this->t('Balance'), 'url' => ['account/bonuses']],
            'about' => ['label' => $this->t('Personal data'), 'url' => ['account/about']],
            'settings' => ['label' => $this->t('account settings'), 'url' => ['account/settings']],
            //'invite' => array('label' => 'Invitations', 'url' => array('account/invite')),
        ];
    }
}
