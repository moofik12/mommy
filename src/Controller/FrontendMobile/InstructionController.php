<?php

namespace MommyCom\Controller\FrontendMobile;

use MommyCom\YiiComponent\FrontendMobile\FrontController;

class InstructionController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules(): array
    {
        return [
            'allow',
        ];
    }

    /**
     * @return array
     */
    public function cache(): array
    {
        return [];
    }

    public function actionIndex()
    {
        $this->render('index', []);
    }
}
