<?php

namespace MommyCom\Controller\FrontendMobile;

use CHtml;
use CHttpException;
use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

class StaticController extends FrontController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    public function actionIndex($category, $page)
    {
        $category = StaticPageCategoryRecord::model()->url($category)->find();
        $pages = StaticPageRecord::model()->url($page)->findAll(['order' => 'created_at ASC']);

        if (empty($pages) || $category === null) {
            throw new CHttpException(404, 'Page not found');
        }

        $this->productHeader = CHtml::tag('div', ['class' => 'table'],
            CHtml::tag('div', ['class' => 'back-container-link'],
                '<h1 class="back-container-content">' . CHtml::encode($pages[0]->title) . '</h1>'
            )
        );
        $this->description = $pages[0]->title . ' - ' . $this->t('MOMMY.COM: Shopping club for moms and children');

        $this->render('index', [
            'pages' => $pages,
            'category' => $category,
        ]);
    }

}
