<?php

namespace MommyCom\Controller\FrontendMobile;

use CArrayDataProvider;
use CHtml;
use CHttpException;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class ProductController extends FrontController
{
    /**
     * @var bool
     */
    public $visibleOnlyLogo = false;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    /**
     * @param EventRecord $event
     * @param string $returnType
     *
     * @return array
     */
    protected function _getReturnData($event, $returnType)
    {
        $req = $this->app()->request;

        $returnData = [
            'url' => $this->createUrl('event/index', ['id' => $event->id]),
            'title' => $event->name,
        ];

        $lastRoute = $this->app()->request->getRouteFromReferrer($req->urlReferrer);
        if ($lastRoute !== false) {
            if ($lastRoute === 'event/index') {
                $returnData['url'] = $req->urlReferrer;
            }
        }

        if ($event->is_virtual) {
            $returnData['url'] = $this->createUrl('outlet');
            $returnData['title'] = 'Outlet';

            return $returnData;
        }

        switch ($returnType) {
            case 'category':
                $category = $req->getParam('category');
                $categoryGroups = CategoryGroups::instance();

                if ($categoryGroups->isValid($category)) {
                    $returnData['url'] = $this->createUrl('event/category', ['category' => $category]);
                    $returnData['title'] = Utf8::ucfirst($categoryGroups->getLabel($category));
                }
                break;

            case 'age':
                $productTargets = ProductTargets::instance();
                $ageGroups = AgeGroups::instance();

                $target = Cast::toStr($req->getParam('target'));
                $age = Cast::toStr($req->getParam('age'));

                $isAgesSupported = $productTargets->getIsSupportsAge($target);
                $ageRange = $ageGroups->getAgeRange($age);

                $isOk = $productTargets->isValid($target);
                $isOk = $isOk && is_array($ageRange);
                $isOk = $isOk && $isAgesSupported;

                if ($isOk) {
                    $returnData['url'] = $this->createUrl('event/age', ['target' => $target, 'age' => $age]);
                    $returnData['title'] = Utf8::ucfirst($productTargets->getLabel($target)) . ' ' . $ageGroups->getLabel($age);
                }
                break;

            case 'finalSell':
                $returnData['url'] = $this->createUrl('event/finalSell');
                $returnData['title'] = 'Final sale';
                break;

            case 'brand':
                $brand = Cast::toStr($req->getParam('brand'));

                $returnData['url'] = empty($brand) ? $returnData['url'] : $this->createUrl('event/brand', ['name' => $brand]);
                $returnData['title'] = empty($brand) ? $returnData['title'] : Utf8::ucfirst($brand);
                break;
        }

        return $returnData;
    }

    public function actionIndex($eventId, $id, $returnType = '')
    {
        $user = $this->app()->user;

        $eventId = Cast::toUInt($eventId);
        $id = Cast::toUInt($id);

        $event = EventRecord::model()->findByPk($eventId);
        /* @var $event EventRecord */

        if ($event === null || $event->is_deleted) {
            throw new CHttpException(404, 'This promotion was not found');
        }

        if (!$event->isStatusPublished) {
            throw new CHttpException(400, 'Sorry, this promotion is temporarily unavailable');
        }

        $returnData = $this->_getReturnData($event, $returnType);

        $user->setReturnUrl($returnData['url']);

        $productGroup = $event->getGroupedProduct($id);

        if ($productGroup === null) {
            throw new CHttpException(404, 'This product was not found');
        }

        if (null !== $user->model) {
            $productGroup->trackView(); // отслеживание просмотров
        }

        $productGroup->trackView(); // отслеживание просмотров
        $similar = $productGroup->getSimilarProducts(true, 6, 120);

        // получаем "другие товары акции"
        $otherProducts = GroupedProducts::fromProducts($similar->getEventProducts());

        if (count($otherProducts) < 6) { // magic
            $otherProducts->addProducts(
                EventProductRecord::model()
                    ->eventId($eventId)
                    ->orderByRand()
                    ->limit(50)
                    ->cache(120)
                    ->findAll()
            );
        }
        // end

        $othersProvider = new CArrayDataProvider($otherProducts->toArray());
        $iconSpeedDelivery = '';
        if ($event->is_drop_shipping) {
            $iconSpeedDelivery = CHtml::link('',
                $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']),
                ['title' => "What is 'Fast Delievery'?", 'class' => 'icon-fast-delivery']
            );
        }

        $this->productHeader = CHtml::tag('div', ['class' => 'table'],
            CHtml::tag('div', ['class' => 'back-container-link'],
                CHtml::link('<h1 class="back-container-content icon-back">' . CHtml::encode($returnData['title']) . '</h1>', $returnData['url'] . '#' . $id, ['itemprop' => 'breadcrumb']) . $iconSpeedDelivery
            )
        );
        $this->description = str_replace('"', "'", $productGroup->product->name . ' ' . $productGroup->product->brand->name
            . ' - ' . $this->app()->params['distributionEmailName'] . ' ' . strip_tags($productGroup->product->description));
        $this->render('index', [
            'event' => $event,
            'productGroup' => $productGroup,
            'othersProvider' => $othersProvider,
            'returnData' => $returnData,
        ]);
    }

    public function actionByArticle($id)
    {
        $id = Cast::toUInt($id);
        $product = EventProductRecord::model()->findByPk($id);

        /* @var $product EventProductRecord */

        if ($product === null) {
            throw new CHttpException(404, 'This promotion was not found');
        }

        $this->redirect(['product/index', 'id' => $product->product_id, 'eventId' => $product->event_id]);
    }
}
