<?php

namespace MommyCom\Controller\FrontendMobile;

use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

class PastController extends FrontController
{
    const DEFAULT_PAGE_SIZE = 20;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    public function cache()
    {
        return [];
    }

    public function actionIndex()
    {
        $provider = EventRecord::model()
            ->isVirtual(false)
            ->onlyVisible()
            ->onlyPublished()
            ->onlyTimeClosed()
            ->isDeleted(false)
            ->orderBy('end_at', 'desc')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => self::DEFAULT_PAGE_SIZE,
                    'pageVar' => 'page',
                ],
            ]);
        $events = $provider->getData();

        $this->render('index', [
            'events' => $events,
            'provider' => $provider,
        ]);
    }
}
