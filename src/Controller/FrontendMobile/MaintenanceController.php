<?php

namespace MommyCom\Controller\FrontendMobile;

use CHttpException;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

class MaintenanceController extends FrontController
{
    /**
     * @var string
     */
    public $layout = '//layouts/maintenance';

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            'allow',
        ];
    }

    /**
     * Информационное сообщение при обслуживании сайта
     */
    public function actionIndex()
    {
        if (!file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . 'maintenance')) {
            throw new CHttpException(404, 'Page not found');
        }

        $this->render('index');
    }
}
