<?php

namespace MommyCom\Controller\FrontendMobile;

use CHtml;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductBrands;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

class GoodsController extends FrontController
{
    /**
     * @var string
     */
    public $defaultAction = 'allEvents';

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
            ],
        ];
    }

    public $menu;

    public function actionAllEvents()
    {
        /** @var \MommyCom\Service\SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $provider = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->onlyPublished()
            ->onlyTimeActive()
            ->orderBy('end_at', 'desc')
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $items = [];

        foreach ($provider->getData() as $item) {
            $items[] = ['label' => '<h3 class="label">' . CHtml::encode($item->name) . '</h3>'
                . '<p class="info">' . CHtml::encode($item->description_short) . '</p>',
                'url' => $this->app()->createUrl('event/index', ['id' => $item->id]),
            ];
        }
        $this->menu['goods']['label'] = str_replace('h2', 'h1', $this->menu['goods']['label']);

        $this->menu = ArrayUtils::merge($this->menu, ['goods' => ['items' => $items]]);
        $this->render('main', ['class' => 'product-menu-actions']);
    }

    public function actionCategories()
    {
        $items = [];

        foreach (CategoryGroups::instance()->getLabels() as $name => $label) {
            $items[] = ['label' => '<h3 class="label">' . $label . '</h3>',
                'url' => $this->app()->createUrl('event/category', ['category' => $name]),
            ];
        }
        $this->menu['categories']['label'] = str_replace('h2', 'h1', $this->menu['categories']['label']);
        $this->menu = ArrayUtils::merge($this->menu, ['categories' => ['items' => $items]]);
        $this->render('main', ['class' => 'product-menu-cat']);
    }

    public function actionAges()
    {
        $body = '';
        foreach (ProductTargets::instance()->getSupportsAges() as $target) {
            $body .= '<h3 class="menu-head">' . ProductTargets::instance()->getLabel($target) . ' </h3>';
            $body .= '<ul class="product-menu-cat">';
            foreach (AgeGroups::instance()->getLabels() as $name => $label) {
                /* @var $item EventRecord */
                $body .= '<li>';
                $body .= '<a data-access-by-login="true" href="' . $this->app()->createUrl('event/age', ['age' => $name, 'target' => $target]) . '">';
                $body .= '<h4 class="label">' . $label . '</h4>';
                $body .= '</a>';
                $body .= '</li>';
            }
            $body .= '</ul>';
        }
        $this->menu['ages']['label'] = str_replace('h2', 'h1', $this->menu['ages']['label']);
        $this->menu = ArrayUtils::merge($this->menu, ['ages' => ['template' => "{menu}$body"]]);
        $this->render('main', ['class' => '']);
    }

    public function actionBrands()
    {
        /** @var SplitTesting $splitTesting */
        $splitTesting = $this->app()->splitTesting;

        $variant = $splitTesting->getMadeInVariant();

        $eventsIds = EventRecord::model()
            ->splitByMadeInVariant($variant)
            ->display(300)
            ->findColumnDistinct('id');
        $brandsIds = EventProductRecord::model()
            ->splitByMadeInVariant($variant)
            ->eventIdIn($eventsIds)
            ->cache(300)
            ->findColumnDistinct('brand_id');
        $productBrands = ProductBrands::fromBrandsId($brandsIds);

        $items = [];

        foreach ($productBrands->sort()->toArray() as $brand) {
            $items[] = ['label' => '<h3 class="label">' . CHtml::encode($brand->name) . '</h3>',
                'url' => $this->app()->createUrl('event/brand', ['name' => $brand->name]),
            ];
        }
        $this->menu['brands']['label'] = str_replace('h2', 'h1', $this->menu['brands']['label']);
        $this->menu = ArrayUtils::merge($this->menu, ['brands' => ['items' => $items]]);
        $this->render('main', ['class' => 'product-menu-cat']);
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $this->menu = $this->menu ?: [
            'categories' => ['label' => '<h2>' . $this->t('By Category') . ' <b></b></h2>', 'url' => ['goods/categories']],
            'ages' => ['label' => '<h2>' . $this->t('By age') . ' <b></b></h2>', 'url' => ['goods/ages']],
            'goods' => ['label' => '<h2>' . $this->t('All promotions') . ' <b></b></h2>', 'url' => ['goods/allEvents']],
        ];

        return true;
    }
}
