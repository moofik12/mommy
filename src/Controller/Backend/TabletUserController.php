<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\TabletUserRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class TabletUserController extends BackController
{
    public function actionIndex()
    {
        $provider = TabletUserRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id ASC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('TabletUserRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    public function actionAdd()
    {
        $model = new TabletUserRecord();

        $form = Form::createForm('widgets.form.config.tabletUserAdd', $model, [
            'type' => 'horizontal',
        ]);
        /* @var TbForm $form */

        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect(['tabletUser/index']);
            }
        }

        $this->render('add', [
            'form' => $form,
        ]);
    }

    public function actionDelete($id)
    {
        $id = Cast::toUInt($id);

        $model = TabletUserRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /* @var $model TabletUserRecord */

        $model->delete();

        $this->redirect('tabletUser/index');
    }
} 
