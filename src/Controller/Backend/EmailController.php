<?php

namespace MommyCom\Controller\Backend;

use CHtml;
use CHttpException;
use CSort;
use MommyCom\Model\Backend\DistributionEmailForm;
use MommyCom\Model\Backend\EmailUserForm;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;

class EmailController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Managing E-mail messages';

    /**
     * Manages all models.
     *
     * @property-description Просмотр списка E-mail сообщений
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = EmailMessage::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('EmailMessage', ['status' => EmailMessage::STATUS_WAITING
                    , 'type' => EmailMessage::TYPE_SYSTEM]),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_ASC,
                ],
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param int $id
     *
     * @property-description Просмотр E-mail сообщения
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('view', ['model' => $model]);
            $this->app()->end();
        }

        $this->commitView($model, $this->t('View e-mail'));
        $this->render('view', ['model' => $model]);
    }

    /**
     * @param int $id
     *
     * @property-description Просмотр только текста E-mail сообщения
     */
    public function actionViewBody($id)
    {
        $model = $this->loadModel($id);

        $this->commitView($model, $this->t('View e-mail'));

        echo $model->body;
    }

    /**
     * @param integer $id the ID of the model to be deleted
     *
     * @throws CHttpException
     * @property-description Удаление E-mail сообщения
     */
    public function actionDelete($id)
    {
        if ($this->app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $this->commitView($model, $this->t('Delete e-mail'));
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * @property-description Создание E-mail по ID пользователя (пользователей) и постановка в очередь
     */
    public function actionCreateFromUsers()
    {
        $result = [
            'errors' => false,
        ];

        $form = new EmailUserForm();
        $data = $this->app()->getRequest()->getPost('EmailUserForm');
        $type = $this->app()->getRequest()->getParam('type', EmailMessage::TYPE_CUSTOM);

        if (empty($data)) {
            $result['errors'][] = $this->t('Cannot send empty message');
        }

        $form->setAttributes($data);

        if (!$form->validate()) {
            foreach ($form->errors as $error) {
                $result['errors'][] = $error;
            }
        }

        if (empty($result['errors'])) {
            if (is_string($form->userIds)) {
                $form->userIds = explode(',', $form->userIds);
            }

            /** @var UserRecord[] $models */
            $models = [];
            $userRecord = UserRecord::model();
            foreach ($form->userIds as $userId) {
                if ($model = $userRecord->findByPk($userId)) {
                    $models[] = $model;
                }
            }

            foreach ($models as $model) {
                if (!$this->app()->mailer->create(
                    [$this->app()->params['distributionEmail'] => $this->app()->name]
                    , [$model->email => $model->getFullname()]
                    , CHtml::encode($form->subject)
                    , ['body' => $form->body]
                    , $type
                )
                ) {
                    $result['errors'][] = "Не удалось отправить сообшение на E-mail: $model->email "
                        . " ({$model->name} {$model->surname})";
                }
            }
        }

        $this->commitView(null, $this->t('Creating e-mail'));
        $this->renderJson($result);
    }

    /**
     * @property-description Рассылка сообщений группе пользователей
     */
    public function actionCreateDistribution()
    {
        $result = new Reply();
        $form = new DistributionEmailForm();
        //get data
        $data = $this->app()->getRequest()->getPost('DistributionEmailForm');
        $type = $this->app()->getRequest()->getParam('type', EmailMessage::TYPE_CUSTOM);
        $info = $this->app()->getRequest()->getParam('info', false);

        if (empty($data)) {
            $result->addError($this->t('Cannot send empty message'));
        }

        $form->setAttributes($data);

        if (!$form->validate()) {
            foreach ($form->errors as $error) {
                $result->addError($error);
            }
        }

        if (!$result->hasErrors()) {
            $model = UserRecord::model();

            /** @var UserRecord[] $users */
            $users = $model->findAll();
            if ($info) {
                $result->addInfo('Найдено: ' . \Yii::t('app', '{n} пользователь | {n} пользователя | {n} пользователей'
                        , count($users)));
            } else {
                $countEmailCreate = 0;
                foreach ($users as $user) {
                    if (!$this->app()->mailer->create(
                        [$this->app()->params['supportEmail'] => $this->app()->name]
                        , [$user->email => $user->getFullname()]
                        , CHtml::encode($form->subject)
                        , ['body' => $form->body]
                        , $type
                    )
                    ) {
                        $result->addError("Не удалось отправить сообшение на E-mail: $model->email "
                            . " ({$user->name} {$user->surname})");
                    } else {
                        $countEmailCreate++;
                    }
                }

                $result->addInfo('Sent' . ': ' . $countEmailCreate . ' E-email' . '.');
            }
        }

        $this->commitView(null, $this->t('Bulk mail-out'));
        $this->renderJson($result->getResponse());
    }

    /**
     * @param array $id
     * @param int $status
     *
     * @property-description Обновление статуса E-mail
     */
    public function actionUpdateStatus(array $id, $status)
    {
        $id = Cast::toUIntArr($id);

        /** @var \MommyCom\Model\Db\EmailMessage[] $emails */
        $emails = EmailMessage::model()->idIn($id)->findAll();

        foreach ($emails as $email) {
            $email->status = $status;
        }

        $result = UnShardedActiveRecord::saveMany($emails, true, ['status']);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderJson(['result' => $result]);
        }

        $this->redirect(['email/index']);
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|EmailMessage
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $id = intval($id);

        $model = EmailMessage::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Model does not exist.');
        }

        return $model;
    }
}
