<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Backend\AnswerForm;
use MommyCom\Model\Db\FeedbackRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\YiiComponent\Facade\EmailTranslator;
use MommyCom\YiiComponent\Type\Cast;

class FeedbackController extends BackController
{
    /**
     * @property-description Просмотр новых обращений
     */
    public function actionNew()
    {
        $model = FeedbackRecord::model();
        $model->status(FeedbackRecord::STATUS_NEW);
        $dataProvider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 25,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('FeedbackRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('new', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @property-description Просмотр обработанных обращений
     */
    public function actionHistory()
    {
        $model = FeedbackRecord::model();
        $model->notStatus(FeedbackRecord::STATUS_NEW);
        $dataProvider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 25,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('FeedbackRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'updated_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('history', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @property-description Отмена обращения
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionCancel($id)
    {
        if (!$this->app()->getRequest()->isAjaxRequest) {
            $this->redirect($this->app()->request->urlReferrer);
        }
        $id = Cast::toInt($id);
        $model = FeedbackRecord::model()->findByPk($id);
        if ($model == null) {
            throw new CHttpException(404, 'Record not found');
        }
        $model->status = FeedbackRecord::STATUS_CANCEL;
        $model->admin_id = $this->app()->user->id;
        $success = 0;
        if ($model->save()) {
            $success = 1;
        }
        $this->renderJson(['success' => $success]);
    }

    /**
     * @param $id
     * @throws CHttpException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function actionAnswer($id)
    {
        $id = Cast::toInt($id);
        $model = FeedbackRecord::model()->findByPk($id);
        if ($model == null) {
            throw new CHttpException(404, 'Record not found');
        }

        $form = new AnswerForm('email');
        /** @var  $answerForm AnswerForm */
        $answerForm = Form::createForm('widgets.form.config.answerAdd',
            $form, [
                'type' => 'vertical',
                'id' => 'answer-form',
            ]);

        $translator = $this->container->get('translator.email');

        $data = $this->app()->request->getPost('AnswerForm', []);
        if (!empty($data)) {
            $form->setAttributes($data);
            if ($form->validate()) {
                $model->status = FeedbackRecord::STATUS_PROCESSED;
                $model->message_answer = $form->answerMessage;
                $model->admin_id = $this->app()->user->id;
                if ($model->save()) {
                    $mail = $this->app()->mailer;
                    $mail->create(
                        [$this->app()->params['layoutsEmails']['answer'] => $this->app()->params['distributionEmailName']],
                        [$model->user->email],
                        $translator->t('Response to your request'),
                        [
                            'view' => 'answer',
                            'data' => [
                                'user' => $model->user,
                                'answer' => $model->message_answer,
                                'question' => $model->message,
                            ]
                        ]
                    );
                    $this->redirect($this->app()->createUrl('feedback/history'));
                }
            }
        }
        $this->render('answer', [
            'model' => $model,
            'answerForm' => $answerForm,
        ]);
    }

}
