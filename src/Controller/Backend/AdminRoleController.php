<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\AdminRoleRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\AdmAuthManager\ActionAccess;
use TbActiveForm;

class AdminRoleController extends BackController
{
    /**
     * @property-description Просмотр списка ролей
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = AdminRoleRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('AdminRoleRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @property-description Добавление роли
     */
    public function actionAdd()
    {
        $model = new AdminRoleRecord();

        if ($this->app()->request->isAjaxRequest) {
            echo TbActiveForm::validate($model);
            $this->app()->end();
        }

        if ($this->app()->request->isPostRequest) {
            $model->attributes = $this->app()->request->getPost('AdminRoleRecord');
            if ($model->save()) {
                $this->redirect(['index']);
            }
        }

        $classList = ActionAccess::getControllerActions();
        $this->render('add', ['classList' => $classList, 'model' => $model]);
    }

    /**
     * @property-description Удаление роли
     * @throws CHttpException
     *
     * @param integer $id
     */
    public function actionDelete($id)
    {
        $id = intval($id);
        if ($id === 0 || is_null($model = AdminRoleRecord::model()->findByPk($id))) {
            throw new CHttpException('404');
        }
        $model->delete();

        if (!$this->app()->getRequest()->isAjaxRequest) {
            $this->redirect($this->app()->request->urlReferrer);
        }
    }

    /**
     * @property-description Редактирование роли
     * @throws CHttpException
     *
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $id = intval($id);
        if ($id === 0 || is_null($model = AdminRoleRecord::model()->findByPk($id))) {
            throw new CHttpException('404');
        }

        if ($this->app()->request->isAjaxRequest) {
            echo TbActiveForm::validate($model);
            $this->app()->end();
        }

        if ($this->app()->request->isPostRequest && !!$data = $this->app()->request->getPost('AdminRoleRecord')) {
            $model->setAttributes($data);
            if ($model->save()) {
                $this->redirect(['index']);
            }
        }

        $classList = ActionAccess::getControllerActions();

        $this->render('update', [
            'model' => $model,
            'classList' => $classList,
        ]);
    }
}
