<?php

namespace MommyCom\Controller\Backend;

use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Service\BaseController\BackController;

class OrderBuyoutTrackingController extends BackController
{
    public function actionIndex()
    {
        $provider = OrderBuyoutRecord::model()
            ->with('order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderBuyoutRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }
} 
