<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Db\IpBanRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class IpBanController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'IP Blocking Management';

    public $breadcrumbs = [
        'IP Block',
        'List' => ['index'],
    ];

    /**
     * If creation is successful, the browser will be redirected to the 'index' page.
     */
    public function actionCreate()
    {
        /** @var IpBanRecord $model */
        $model = new IpBanRecord();

        $form = TbForm::createForm('widgets.form.config.ipBan', $this, [
            'type' => 'horizontal',
            'id' => 'form-ip-ban',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->app()->user->setFlash('success', 'IP address added');
                $this->redirect(['index']);
            }
        }

        $this->commitView(null, 'Создание брокировки');
        $this->render('create', ['form' => $form]);
    }

    /**
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        /*
        $model=$this->loadModel($id);

        $form = TbForm::createForm('widgets.form.config.staticPage', $this, array(
            'type'=>'horizontal',
            'id' => 'form-static-page',
        ), $model);

        if($form->submitted('submit') && $form->validate()){
            $model = $form->getModel();
            if($model->save()){
                $this->app()->user->setFlash('success', 'Страница обновлена');
                $this->redirect(array('index'));
            }
        }

        $this->render('update',array(
            'form'=>$form,
        ));
        */
    }

    /**
     * @param integer $id the ID of the model to be deleted
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if ($this->app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = IpBanRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('IpBanRecord', ['filterTime' => IpBanRecord::FILTER_TIME_ACTIVE]),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->commitView(null, 'View list');
        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|IpBanRecord
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $id = intval($id);

        $model = IpBanRecord::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'Model does not exist.');
        return $model;
    }
}
