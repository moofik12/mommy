<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Backend\DistributionSmsForm;
use MommyCom\Model\Backend\SmsUserForm;
use MommyCom\Model\Db\SmsMessageRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Type\Cast;

class SmsController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Managing of SMS';

    public function actionIndex()
    {
        // grid
        $dataProvider = SmsMessageRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('SmsMessageRecord',
                    ['status' => SmsMessageRecord::STATUS_WAITING]
                ),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param integer $id the ID of the model to be deleted
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if ($this->app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * создание СМС по ID пользователя (пользователей) и постановка в очередь
     */
    public function actionCreateFromUsers()
    {
        $result = new Reply();

        $form = new SmsUserForm();
        $data = $this->app()->getRequest()->getPost('SmsUserForm');
        $type = $this->app()->getRequest()->getParam('type', SmsMessageRecord::TYPE_CUSTOM);

        if (empty($data)) {
            $result->addError($this->t('Cannot send empty message'));
        }

        $form->setAttributes($data);

        if (!$form->validate()) {
            foreach ($form->errors as $error) {
                $result->addError($error);
            }
        }

        if (!$result->hasErrors()) {
            if (is_string($form->userIds)) {
                $userIds = explode(',', $form->userIds);
                $form->userIds = Cast::toUIntArr($userIds);
            }

            /** @var UserRecord[] $models */
            $models = UserRecord::model()->findByPkAll($form->userIds);
            foreach ($models as $model) {
                if (!$this->app()->sms->create($model->id, $model->telephone, $form->text, $type)) {
                    $result->addError("Не удалось отправить сообшение на номер: $model->telephone "
                        . " ({$model->name} {$model->surname} {$model->email})");
                }
            }
        }

        $this->commitView(null, $this->t('Creating SMS and mass mailing'));
        $this->renderJson($result->getResponse());
    }

    /**
     * рассылка сообщений пользователям UserRecord
     */
    public function actionCreateDistribution()
    {
        $result = new Reply();
        $form = new DistributionSmsForm();
        //get data
        $data = $this->app()->getRequest()->getPost('DistributionSmsForm');
        $type = $this->app()->getRequest()->getParam('type', SmsMessageRecord::TYPE_CUSTOM);
        $info = $this->app()->getRequest()->getParam('info', false);

        if (empty($data)) {
            $result->addError($this->t('Cannot send empty message'));
        }

        $form->setAttributes($data);

        if (!$form->validate()) {
            foreach ($form->errors as $error) {
                $result->addError($error);
            }
        }

        if (!$result->hasErrors()) {
            $model = UserRecord::model();

            /** @var UserRecord[] $users */
            $users = $model->findAll();
            $countSmsCreate = 0;
            if ($info) {
                foreach ($users as $user) {
                    if ($this->app()->sms->isValidNumber($user->telephone)) {
                        $countSmsCreate++;
                    }
                }

                $result->addInfo('Найдено: ' . \Yii::t('app', '{n} пользователь | {n} пользователя | {n} пользователей'
                        , count($users)) . '. Будет отправлено: <span class="badge badge-important">' . $countSmsCreate . '</span> SMS.');
            } else {
                foreach ($users as $user) {
                    if (!$this->app()->sms->create($user->id, $user->telephone, $form->text, $type)) {
                        $result->addError("Не удалось отправить сообшение на номер: $model->telephone "
                            . " ({$user->name} {$user->surname} {$user->email})");
                    } else {
                        $countSmsCreate++;
                    }
                }

                $result->addInfo('Sent' . ': ' . $countSmsCreate . ' SMS.');
            }
        }

        $this->commitView(null, $this->t('Creating SMS and mass mailing'));
        $this->renderJson($result->getResponse());
    }

    /**
     * @param string|int $phone
     */
    public function actionAjaxValid($phone)
    {
        $result = [
            'result' => $this->app()->sms->isValidNumber($phone),
        ];

        $this->renderJson($result);
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|StaticPageRecord
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $id = intval($id);

        $model = SmsMessageRecord::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'Model does not exist.');
        return $model;
    }

}
