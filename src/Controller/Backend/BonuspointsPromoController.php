<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use DateTime;
use MommyCom\Model\Db\BonuspointsPromoRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class BonuspointsPromoController extends BackController
{
    /**
     * @property-description Просмотр списка бонусов
     */
    public function actionIndex()
    {
        $selector = BonuspointsPromoRecord::model();

        $createdAtRangeString = $this->app()->request->getParam('createdAtRange', false);

        if (!empty($createdAtRangeString)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $createdAtRangeString);

            $selector->createdAtFrom(strtotime('today ' . $createdAtGreater));
            $selector->createdAtTo(strtotime('tomorrow ' . $createdAtLower));
        }

        $startAtRangeString = $this->app()->request->getParam('startAtRange', false);

        if (!empty($startAtRangeString)) {
            list($startAtGreater, $startAtLower) = explode(' - ', $startAtRangeString);

            $selector->startAtFrom(strtotime('today ' . $startAtGreater));
            $selector->startAtTo(strtotime('tomorrow ' . $startAtLower));
        }

        $endAtRangeString = $this->app()->request->getParam('endAtRange', false);

        if (!empty($endAtRangeString)) {
            list($endAtGreater, $endAtLower) = explode(' - ', $endAtRangeString);

            $selector->endAtFrom(strtotime('today ' . $endAtGreater));
            $selector->endAtTo(strtotime('tomorrow ' . $endAtLower));
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('BonuspointsPromoRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View the list of mail-out subscription bonuses'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     * @property-description Просмотр бонуса для рассылки
     */
    public function actionView($id)
    {
        $model = BonuspointsPromoRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t("No data found for processing"));
        }

        $this->commitView($model, $this->t('View mail-out subscription bonus'));
        $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @throws CHttpException
     * @property-description Создание бонуса для рассылки
     */
    public function actionCreate()
    {
        $model = new BonuspointsPromoRecord();

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.bonuspointsPromoAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit')) {
            if ($form->validate() && $model->save()) {
                $this->redirect($this->createUrl('view', ['id' => $model->id]));
            }
        } else {
            $timeStamp = new DateTime();

            $model->name = $this->t('Mail-out containing gift bonuses from') . $timeStamp->format('d.m.Y');
            $model->user_message = $this->t('Congratulations! You got a bonus! Enjoy shopping with us!');
            $model->start_at = time();
            $model->end_at = time();
        }

        $this->commitView(null, $this->t('Creating mail-out bonus'));
        $this->render('create', [
            'form' => $form,
        ]);
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     * @property-description Редактирование бонуса для рассылки
     */
    public function actionUpdate($id)
    {
        $model = BonuspointsPromoRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, "Не найдены данные для обработки");
        }

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.bonuspointsPromoUpdate', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect($this->createUrl('view', ['id' => $model->id]));
            }
        }

        $this->commitView(null, $this->t('Editing mail-out bonus'));
        $this->render('update', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     * @property-description Удаление бонуса для рассылки
     */
    public function actionDelete($id)
    {
        $model = BonuspointsPromoRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t("No data found for processing"));
        }

        if (!$model->delete()) {
            throw new CHttpException(500, $this->t('Deleting disabled'));
        }

        $this->commitView($model, $this->t('Deleting mail-out bonus'));
    }
}
