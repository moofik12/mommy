<?php

namespace MommyCom\Controller\Backend;

use CAction;
use CHttpException;
use MommyCom\Model\Db\TaskBoardUserRoleConnectionRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class TaskBoardUserRoleConnectionController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Adding Roles to the Task Manager';

    /**
     * @var
     */
    protected $model;

    /**
     * @param CAction $action
     *
     * @return bool
     * @throws CHttpException
     * need to loose weight
     */
    public function beforeAction($action)
    {
        $app = $this->app();
        $request = $app->getRequest();

        if ($request->getParam('id')) {
            $this->model = $this->loadModel($request->getParam('id'));
        }

        return parent::beforeAction($action);
    }

    /**
     * @property-description Просмотр привязанный ролей к доскам
     */
    public function actionIndex()
    {
        $provider = TaskBoardUserRoleConnectionRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.created_at DESC',
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Добавление ролей к доскам
     */
    public function actionAdd()
    {
        /* @var $model TaskBoardUserRoleConnectionRecord */
        $model = new TaskBoardUserRoleConnectionRecord();

        $form = TbForm::createForm('widgets.form.config.connectUserRoleToTaskBoard', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect(['index']);
            }
        }

        $viewData = [
            'form' => $form,
            'model' => $model,
        ];

        $this->render('add', $viewData);
    }

    /**
     * @throws CHttpException
     * @property-description Удаление ролей
     */
    public function actionDelete()
    {
        if ($this->app()->request->isPostRequest) {
            /* @var $model TaskBoardUserRoleConnectionRecord */
            $model = $this->model;
            $model->delete();
            $this->redirect(['index']);
        } else {
            throw new CHttpException(404, 'Problem deleting task.');
        }
    }

    /**
     * @param $id
     *
     * @return static
     * @throws CHttpException
     */
    private function loadModel($id)
    {
        $model = TaskBoardUserRoleConnectionRecord::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Model does not exist.');
        }
        return $model;
    }
}
