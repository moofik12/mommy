<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class StaticPageCategoryController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Managing Static Page Categories';

    public $breadcrumbs = [
        'Static page categories',
        'List' => ['index'],
    ];

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = StaticPageCategoryRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('StaticPageCategoryRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        //new model
        $model = new StaticPageCategoryRecord();

        $form = TbForm::createForm('widgets.form.config.staticPageCategory', $this, [
            'type' => 'horizontal',
            'id' => 'form-static-page',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->app()->user->setFlash('success', 'Category added');
                $this->refresh();
            }
        }
        //end new model

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['dataProvider' => $dataProvider, 'form' => $form]);
    }

    /**
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $form = TbForm::createForm('widgets.form.config.staticPageCategory', $this, [
            'type' => 'horizontal',
            'id' => 'form-static-page',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->app()->user->setFlash('success', 'Страница обновлена');
                $this->redirect(['index']);
            }
        }

        $this->commitView($model, $this->t('Category update'));
        $this->render('update', [
            'form' => $form,
        ]);
    }

    /**
     * @param integer $id the ID of the model to be deleted
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        /*
        if($this->app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        */
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|StaticPageCategoryRecord
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $id = intval($id);

        $model = StaticPageCategoryRecord::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'Model does not exist.');
        return $model;
    }
}
