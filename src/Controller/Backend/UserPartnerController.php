<?php

namespace MommyCom\Controller\Backend;

use CArrayDataProvider;
use CHttpException;
use CSort;
use DateInterval;
use DatePeriod;
use DateTime;
use MommyCom\Model\Backend\UserPartnerOutputForm;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserPartnerAdmissionRecord;
use MommyCom\Model\Db\UserPartnerBalanceRecord;
use MommyCom\Model\Db\UserPartnerInviteRecord;
use MommyCom\Model\Db\UserPartnerOutputRecord;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Statistic\UserPartnerModel;
use MommyCom\Model\Statistic\UserPartnerOrderModel;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\Type\Cast;

class UserPartnerController extends BackController
{
    /**
     * @property-description Просмотр списка пользователей-партнеров по ресурсу прихода и периоду
     */
    public function actionResource()
    {
        $model = UserPartnerRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', false);

        if (!empty($rangeData)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $rangeData);
            $fromDate = strtotime($createdAtGreater);
            $toDate = strtotime($createdAtLower);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 14 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
            $rangeData = implode(' - ', [date('d.m.Y', $fromDate), date('d.m.Y', $toDate)]);
        }
        $model->createdAtGreater($fromDate);
        $model->createdAtLower($toDate);
        $getParams = $this->app()->request->getParam('UserPartnerRecord', []);
        $dataProvider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'filter' => [
                'attributes' => $getParams,
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
        ]);

        $interval = new DateInterval('P1D');
        $dataTimeStart = new DateTime();
        $dataTimeEnd = new DateTime();
        $dataTimeStart->setTimestamp($fromDate);
        $dataTimeEnd->setTimestamp($toDate);

        $dateRange = new DatePeriod($dataTimeStart, $interval, $dataTimeEnd);
        $items = [];
        $i = 0;
        foreach ($dateRange as $dateTimeInterval) {
            /** @var DateTime $dateTimeInterval */
            $dateTimeIntervalEnd = clone $dateTimeInterval;
            $dateTimeIntervalEnd = $dateTimeIntervalEnd->add($interval);

            $startInterval = $dateTimeInterval->getTimestamp();
            $endInterval = $dateTimeIntervalEnd->getTimestamp();

            $itemReport = new UserPartnerModel();
            $from = new DateTime();
            $to = new DateTime();
            $from->setTimestamp($startInterval);
            $to->setTimestamp($endInterval);
            $itemReport->day = $startInterval;
            $partner = UserPartnerRecord::model();
            if (isset($getParams['source'])) {
                if ($getParams['source'] != '') {
                    $partner->source($getParams['source']);
                }
            }
            $itemReport->countUser = $partner->createdAtGreater($startInterval)->createdAtLower($endInterval)->count();
            if ($itemReport->countUser > 0) {
                $items[] = $itemReport;
                $i++;
            }
        }
        $dataProviderReport = new CArrayDataProvider($items, [
            'keyField' => 'day',
            'pagination' => false,
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('resource', [
            'dataProvider' => $dataProvider,
            'rangeData' => $rangeData,
            'dataProviderReport' => $dataProviderReport,
        ]);
    }

    /**
     * @param string $like
     * @param $page int для Select2
     */
    public function actionAjaxSearch($like = '', $page)
    {
        /** @var UserPartnerRecord $section */
        $section = UserPartnerRecord::model()->with('user');
        $result = new Select2Reply();
        $userIds = [];
        if (!empty($like)) {
            $userIds = UserRecord::model()
                ->nameLike($like)
                ->surnameLike($like)
                ->emailLike($like)
                ->findColumnDistinct('id');
        }
        if (!empty($userIds)) {
            $section->userIdIn($userIds);
        }
        $usersProvider = $section->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 10,
                'currentPage' => intval($page) - 1,
            ],
        ]);

        /** @var UserPartnerRecord[] $users */
        $partners = $usersProvider->getData();
        $result->pageCount = $usersProvider->getPagination()->pageCount;
        $data = [];
        foreach ($partners as $partner) {
            /** @var  $partner UserPartnerRecord */
            $data[] = [
                'id' => $partner->id,
                'name' => $partner->user->name,
                'surname' => $partner->user->surname,
            ];
        }
        $result->addItems($data);

        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Поиск по ид парнера или емейлу
     *
     * @param string $like
     * @param $page int для Select2
     */
    public function actionAjaxSearchByIdOrEmail($like = '', $page)
    {
        /** @var UserPartnerRecord $section */
        $section = UserPartnerRecord::model()->with('user');
        $result = new Select2Reply();
        $userIds = $partnerIds = [];
        $likeId = Cast::toInt($like);
        if (!empty($like)) {
            if ($likeId > 0) {
                $partnerIds = UserPartnerRecord::model()->idLike($likeId)->findColumnDistinct('id');
            } else {
                $userIds = UserRecord::model()
                    ->emailLike($like)
                    ->findColumnDistinct('id');
            }
        }
        if (!empty($userIds)) {
            $section->userIdIn($userIds);
        }
        if (!empty($partnerIds)) {
            $section->idIn($partnerIds);
        }

        $usersProvider = $section->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 10,
                'currentPage' => intval($page) - 1,
            ],
        ]);

        /** @var UserPartnerRecord[] $users */
        $partners = $usersProvider->getData();

        $result->pageCount = $usersProvider->getPagination()->pageCount;
        $data = [];
        foreach ($partners as $partner) {
            /** @var  $partner UserPartnerRecord */
            $data[] = [
                'id' => $partner->id,
                'name' => $partner->user->name,
                'surname' => $partner->user->surname,
                'email' => $partner->user->email,
            ];
        }
        $result->addItems($data);

        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Поиск партнера по id
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionAjaxGetById($id)
    {
        $id = Cast::toUInt($id);

        $item = UserPartnerRecord::model()->findByPk($id);

        if ($item === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }
        /** @var  $item UserPartnerRecord */
        $this->renderJson([
            'id' => $item->id,
            'name' => $item->user->name,
            'surname' => $item->user->surname,
        ]);
    }

    /**
     * @property-description Просмотр списка заявок на вывод средств партнерам
     */
    public function actionFinance()
    {
        $model = UserPartnerOutputRecord::model();
        $model->status(UserPartnerOutputRecord::STATUS_WAIT);
        $dataProvider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 25,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserPartnerOutputRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'ordered_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $reasonForm = Form::createForm('widgets.form.config.userPartnerOutputReason',
            new UserPartnerOutputForm('setReason'), [
                'type' => 'vertical',
                'id' => 'reason-form',
                'method' => 'GET',
            ]);

        //modal form
        $requisiteForm = Form::createForm('widgets.form.config.userPartnerOutputRequisite',
            new UserPartnerOutputForm('setRequisite'), [
                'action' => ['userPartner/makeOutputPrivat'],
                'type' => 'vertical',
                'id' => 'requisite-form',

            ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('finance', [
            'dataProvider' => $dataProvider,
            'reasonForm' => $reasonForm,
            'requisiteForm' => $requisiteForm,
        ]);
    }

    /**
     * @property-description Проводка выплаты с заявки партнера на Приват
     * @throws CHttpException
     */
    public function actionMakeOutputPrivat()
    {
        $requisiteForm = new UserPartnerOutputForm('setRequisite');
        $data = $this->app()->request->getParam('UserPartnerOutputForm', []);
        $result = new Reply();
        $result->setState(false);
        if (!empty($data)) {
            $requisiteForm->setAttributes($data);
            $id = Cast::toUInt($requisiteForm->id);
            $model = UserPartnerOutputRecord::model()->findByPk($id);
            if ($model === null) {
                $result->addError('Request not found');
            }
            if ($requisiteForm->validate()) {
                $model->unique_payment = $requisiteForm->requisite;
                $model->status = UserPartnerOutputRecord::STATUS_DONE;
                $model->admin_id = $this->app()->user->id;
                $model->admin_update_last_at = time();
                if ($model->save()) {
                    $result->setState(true);
                } else {
                    $result->addError('Error saving the request for transfer to Privat');
                }
            } else {
                foreach ($requisiteForm->errors as $error) {
                    $result->addError($error);
                }
            }
        } else {
            $result->addError('Data not filled ');
        }
        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Проводка выплаты с заявки партнера на Mamam
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionMakeOutputMamam($id)
    {
        $result = new Reply();
        $result->setState(false);
        $id = Cast::toUInt($id);
        $model = UserPartnerOutputRecord::model()->findByPk($id);
        if ($model === null) {
            $result->addError('Request not found');
        }
        if (!$result->hasErrors()) {
            $canChangeStatus = false;
            if ($model->type_output == UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM) {
                $sumPoints = ceil($model->amount * UserPartnerOutputRecord::PERCENT_OUTPUT_MAMAM / 100); // с надбавкой за вывод средств на счет Мамам
                $bonusPoint = new UserBonuspointsRecord();
                $bonusPoint->user_id = $model->partner->user_id;
                $bonusPoint->points = $model->amount + $sumPoints;
                $bonusPoint->custom_message = 'Зачисление с партнерского счета с заявки № ' . $model->id . ' на вывод средств';
                $bonusPoint->message = $bonusPoint->custom_message;
                $bonusPoint->is_custom = false;
                $bonusPoint->type = UserBonuspointsRecord::TYPE_INNER;
                if ($bonusPoint->save()) {
                    $canChangeStatus = true;
                } else {
                    $result->addError('Error when crediting bonuses to the partner\'s personal account');
                }
            }
            if ($canChangeStatus) {
                $model->status = UserPartnerOutputRecord::STATUS_DONE;
                $model->admin_id = $this->app()->user->id;
                $model->admin_update_last_at = time();
                if ($model->save()) {
                    $result->setState(true);
                } else {
                    $result->addError('Error saving data');
                }
            }
        }
        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Отмена выплаты с заявки партнера и возврат средств на баланс партнера
     * @throws CHttpException
     */
    public function actionCanceledOutput()
    {
        $reasonForm = new UserPartnerOutputForm('setReason');
        $data = $this->app()->request->getParam('UserPartnerOutputForm', []);
        $result = new Reply();
        $result->setState(false);
        if (!empty($data)) {
            $reasonForm->setAttributes($data);
            if ($reasonForm->validate()) {
                $id = Cast::toUInt($reasonForm->id);
                $model = UserPartnerOutputRecord::model()->findByPk($id);
                if ($model === null) {
                    $result->addError('Request not found');
                }
                if (!$result->hasErrors()) {
                    $model->status = UserPartnerOutputRecord::STATUS_CANCEL;
                    $model->admin_id = $this->app()->user->id;
                    $model->admin_update_last_at = time();
                    $model->status_reason = $reasonForm->reason;
                    if ($model->save()) {
                        $plusBalance = new UserPartnerBalanceRecord();
                        $plusBalance->partner_id = $model->partner_id;
                        $plusBalance->type = UserPartnerBalanceRecord::TYPE_CANCEL_PLUS;
                        $plusBalance->amount = $model->amount;
                        $plusBalance->description = 'Возврат средств при отклонении заявки  № ' . $model->id . ' на вывод средств по причине "' . $reasonForm->reason . '"';
                        if ($plusBalance->save()) {
                            $result->setState(true);
                        } else {
                            $model->status = UserPartnerOutputRecord::STATUS_WAIT;
                            $model->save(false, ['status']);
                            $result->addError($this->t("Error making refund to the balance"));
                        }
                    } else {
                        $result->addError($this->t("Error changing request status"));
                    }
                }
            } else {
                foreach ($reasonForm->errors as $error) {
                    $result->addError($error);
                }
            }
        } else {
            $result->addError('Data not filled ');
        }
        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Просмотр истории заявок на вывод средств партнерам
     */
    public function actionHistoryFinance()
    {
        $rangeData = $this->app()->request->getParam('rangeData', false);

        if (!empty($rangeData)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $rangeData);
            $fromDate = strtotime($createdAtGreater);
            $toDate = strtotime($createdAtLower);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 14 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
            $rangeData = implode(' - ', [date('d.m.Y', $fromDate), date('d.m.Y', $toDate)]);
        }
        $model = UserPartnerOutputRecord::model();
        $model->createdAtGreater($fromDate);
        $model->createdAtLower($toDate);
        $model->statusIn([UserPartnerOutputRecord::STATUS_DONE, UserPartnerOutputRecord::STATUS_CANCEL]);
        $dataProvider = $model->getDataProvider(false, [
            'pagination' => false,
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserPartnerOutputRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $countOutputAmount = 0;
        $countCanceledOutputAmount = 0;
        $countOutputAmountMamam = 0;
        $countOutputAmountPrivat = 0;
        $data = $dataProvider->getData();
        foreach ($data as $itemData) {
            /**@var $itemData UserPartnerOutputRecord */
            if ($itemData->status == UserPartnerOutputRecord::STATUS_DONE) {
                $countOutputAmount += $itemData->amount;
                if ($itemData->type_output == UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM) {
                    $countOutputAmountMamam += $itemData->amount;
                } elseif ($itemData->type_output == UserPartnerOutputRecord::TYPE_OUTPUT_PRIVAT) {
                    $countOutputAmountPrivat += $itemData->amount;
                }
            } elseif ($itemData->status == UserPartnerOutputRecord::STATUS_CANCEL) {
                $countCanceledOutputAmount += $itemData->amount;
            }
        }

        $this->commitView(null, $this->t('View list'));
        $this->render('historyFinance', [
            'dataProvider' => $dataProvider,
            'summary' => [
                'countOutputAmount' => $countOutputAmount,
                'countCanceledOutputAmount' => $countCanceledOutputAmount,
                'countOutputAmountMamam' => $countOutputAmountMamam,
                'countOutputAmountPrivat' => $countOutputAmountPrivat,
            ],
            'rangeData' => $rangeData,
        ]);
    }

    /**
     * @property-description Просмотр списка партнеров
     */
    public function actionIndex()
    {
        $model = UserPartnerRecord::model();

        $getParams = $this->app()->request->getParam('UserPartnerRecord', []);
        $dataProvider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'filter' => [
                'attributes' => $getParams,
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @property-description Подтвержденный баланс и детальный просмотр информации о партнере
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionBalance($id)
    {
        $model = UserPartnerRecord::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, $this->t('No partner found'));
        }

        $balanceModel = UserPartnerBalanceRecord::model()->partnerId($model->id);
        $getParams = $this->app()->request->getParam('UserPartnerBalanceRecord', []);
        $dataProvider = $balanceModel->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'filter' => [
                'attributes' => $getParams,
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('balance', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @property-description Статистика партнерской программы
     */
    public function actionStatistic()
    {
        $partnerId = $this->app()->request->getParam('partner_id', 0);
        $rangeData = $this->app()->request->getParam('rangeData', false);

        if (!empty($rangeData)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $rangeData);
            $fromDate = strtotime($createdAtGreater);
            $toDate = strtotime($createdAtLower);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
        } else {
            $fromDate = strtotime('- 14 day ', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $toDate = mktime(23, 59, 59, date('m'), date('d'), date('Y'));
            $rangeData = implode(' - ', [date('d.m.Y', $fromDate), date('d.m.Y', $toDate)]);
        }

        $interval = new DateInterval('P1D');
        $dataTimeStart = new DateTime();
        $dataTimeEnd = new DateTime();
        $dataTimeStart->setTimestamp($fromDate);
        $dataTimeEnd->setTimestamp($toDate);

        $dateRange = new DatePeriod($dataTimeStart, $interval, $dataTimeEnd);
        $items = [];
        foreach ($dateRange as $dateTimeInterval) {
            /** @var DateTime $dateTimeInterval */
            $dateTimeIntervalEnd = clone $dateTimeInterval;
            $dateTimeIntervalEnd = $dateTimeIntervalEnd->add($interval);

            $startInterval = $dateTimeInterval->getTimestamp();
            $endInterval = $dateTimeIntervalEnd->getTimestamp();

            $itemReport = new UserPartnerOrderModel();
            $from = new DateTime();
            $to = new DateTime();
            $from->setTimestamp($startInterval);
            $to->setTimestamp($endInterval);
            $itemReport->startTime = $startInterval;
            $itemReport->endTime = $endInterval;

            //Кол-во регистраций приглашенных пользователей
            $modelPartnerInvitedUser = UserPartnerInviteRecord::model()
                ->createdAtGreater($startInterval)
                ->createdAtLower($endInterval);
            if ($partnerId > 0) {
                $modelPartnerInvitedUser->partnerId($partnerId);
            }
            $distinctUsers = $modelPartnerInvitedUser->findColumnDistinct('user_id');

            $itemReport->countRegPartnerInvitedUser = $distinctUsers === false ? 0 : count($distinctUsers);

            //Использовано промокодов
            $modelPartnerInvitedUser = UserPartnerInviteRecord::model()
                ->createdAtGreater($startInterval)
                ->createdAtLower($endInterval)
                ->typeIncoming(UserPartnerInviteRecord::TYPE_INCOMING_PROMOCODE);
            if ($partnerId > 0) {
                $modelPartnerInvitedUser->partnerId($partnerId);
            }

            $itemReport->countUsedPromocode = $modelPartnerInvitedUser->count();

            //Кол-во заказов, Сумма заказов, Кол-во товаров, Кол-во выплат, Сумма выплат
            $itemReport->countOrder = 0;
            $itemReport->sumOrder = 0;
            $itemReport->countProduct = 0;
            $itemReport->countOutput = 0;
            $itemReport->sumOutput = 0;
            $modelAdmission = UserPartnerAdmissionRecord::model();

            $modelAdmission->createdAtGreater($startInterval);
            $modelAdmission->createdAtLower($endInterval);
            if ($partnerId > 0) {
                $modelAdmission->partnerId($partnerId);
            }
            $admissions = $modelAdmission->findAll();

            foreach ($admissions as $admission) {
                /** @var $admission UserPartnerAdmissionRecord */
                $itemReport->countOrder++;
                $itemReport->sumOrder += $admission->sum_order;
                $itemReport->countProduct += $admission->count_product_order;
                //if ($admission->getIsOutputBalance()) {
                $itemReport->countOutput++;
                $itemReport->sumOutput += $admission->amount;
                //}
                if ($admission->status == UserPartnerAdmissionRecord::STATUS_BALANCE) {
                    $itemReport->countConfirmedOutput++;
                    $itemReport->sumConfirmedOutput += $admission->amount;
                }
            }
            if ($itemReport->getCommonCount() > 0) {
                $items[] = $itemReport;
            }
        }
        $dataProviderReport = new CArrayDataProvider($items, [
            'keyField' => 'startTime',
            'pagination' => false,
        ]);

        $this->render('statistic', [
            'rangeData' => $rangeData,
            'dataProviderReport' => $dataProviderReport,
        ]);
    }

    /**
     * @property-description Приглашенные партнерами пользователи
     *
     * @param int $partner_id
     * @param string $rangeDate
     */
    public function actionInvited($partner_id = 0, $rangeDate = '')
    {
        $partnerId = Cast::toInt($partner_id);
        $rangeDate = Cast::toStr($rangeDate);
        $modelPartnerInvitedUser = UserPartnerInviteRecord::model();
        if (!empty($rangeDate)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $rangeDate);
            $fromDate = strtotime($createdAtGreater);
            $toDate = strtotime($createdAtLower);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));

            $modelPartnerInvitedUser->createdAtGreater($fromDate);
            $modelPartnerInvitedUser->createdAtLower($toDate);
        }

        if ($partnerId > 0) {
            $modelPartnerInvitedUser->partnerId($partnerId);
        }

        $dataProvider = $modelPartnerInvitedUser->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserPartnerInviteRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('invited', [
            'dataProvider' => $dataProvider,
            'rangeDate' => $rangeDate,
            'partnerId' => $partnerId,
        ]);
    }

    /**
     * @property-description Заказы приглашенных партнерами пользователей
     *
     * @param int $partner_id
     * @param string $rangeDate
     */
    public function actionOrderUsers($partner_id = 0, $rangeDate = '')
    {
        $partnerId = Cast::toInt($partner_id);
        $rangeDate = Cast::toStr($rangeDate);

        $model = UserPartnerAdmissionRecord::model();
        if (!empty($rangeData)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $rangeDate);
            $fromDate = strtotime($createdAtGreater);
            $toDate = strtotime($createdAtLower);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));

            $model->createdAtGreater($fromDate);
            $model->createdAtLower($toDate);
        }

        if ($partnerId > 0) {
            $model->partnerId($partnerId);
        }

        $dataProvider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserPartnerAdmissionRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('orderUsers', [
            'dataProvider' => $dataProvider,
            'rangeDate' => $rangeDate,
            'partnerId' => $partnerId,
        ]);
    }

    /**
     * @property-description Финансы: возвраты
     *
     * @param int $partner_id
     * @param string $rangeDate
     */
    public function actionReturn($partner_id = 0, $rangeDate = '')
    {
        $partnerId = Cast::toInt($partner_id);
        $rangeDate = Cast::toStr($rangeDate);
        $modelOutput = UserPartnerBalanceRecord::model();
        $modelOutput->type(UserPartnerBalanceRecord::TYPE_RETURN_MINUS);

        if (!empty($rangeDate)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $rangeDate);
            $fromDate = strtotime($createdAtGreater);
            $toDate = strtotime($createdAtLower);
            $toDate = mktime(23, 59, 59, date('m', $toDate), date('d', $toDate), date('Y', $toDate));
            $modelOutput->createdAtGreater($fromDate);
            $modelOutput->createdAtLower($toDate);
        }

        if ($partnerId > 0) {
            $modelOutput->partnerId($partnerId);
        }

        $dataProvider = $modelOutput->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('return', [
            'partnerId' => $partnerId,
            'dataProvider' => $dataProvider,
            'rangeDate' => $rangeDate,
        ]);
    }

}
