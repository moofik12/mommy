<?php

namespace MommyCom\Controller\Backend;

use CDbCriteria;
use CLogRoute;
use CLogRouter;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Service\BaseController\BackController;

class WarehouseDownloadController extends BackController
{
    public function actionIndex($warehouseCreatedRange = '', $warehouseUpdatedRange = '')
    {
        $selector = WarehouseProductRecord::model()
            ->orderBy('event_product_id', 'desc');

        if (!empty($warehouseCreatedRange)) {
            list($createdAtFrom, $createdAtTo) = array_map('strtotime', explode(' - ', $warehouseCreatedRange)) + [0, 0];
            $selector->createdAtBetween($createdAtFrom, $createdAtTo);
        }

        if (!empty($warehouseUpdatedRange)) {
            list($updatedAtFrom, $updatedAtTo) = array_map('strtotime', explode(' - ', $warehouseUpdatedRange)) + [0, 0];
            $selector->updatedAtBetween($updatedAtFrom, $updatedAtTo);
        }

        $provider = $selector
            ->cache(10)
            ->with([
                'event',
                'supplier',
                'eventProduct',
                'product',
                'eventProduct.brand',
            ])
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 300,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $log = $this->app()->log;
        /** @var $log CLogRouter */
        foreach ($log->getRoutes() as $route) {
            /* @var $route CLogRoute */
            $route->enabled = false;
        }

        $csv = CsvFile::loadFromProvider($provider, [
            'id',
            'event_id', 'event_product_id', 'product_id',
            'statusReplacement', 'lostStatusReplacement', 'lost_status_comment',
            'event.name',
            'supplier.name',
            'product.article',
            'eventProduct.brand.name',
            'product.name',
            'eventProduct.sizeformatReplacement',
            'eventProduct.size',
            'eventProduct.color',
            'eventProduct.weightReplacement',
            'eventProduct.ageRangeReplacement',
            'event_product_price',
            'order_product_price',
            'order_id',
            'return_id',
            'prev_order_id',
            'prev_warehouse_id',
            'created_at' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return $this->app()->format->formatDate($data->created_at, 'medium');
            },
            'updated_at' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return $this->app()->format->formatDate($data->updated_at, 'medium');
            },
            'product.logo',
        ]);

        $content = $csv->toString();
        unset($csv, $provider);
        gc_collect_cycles();

        $this->commitView(null, $this->t('Loading warehouse table'));
        $app = $this->app();
        $downloadFilename = 'warehouse_table__' . date('d-m-Y_H-i') . '.csv';
        $app->request->sendFile($downloadFilename, $content, 'text/csv');
    }

    protected function _getGroupsConfig(CDbCriteria $criteria)
    {
        return [
            'simple' => [
                'group' => [
                    'event_product_id', 'status', 'lost_status',
                ],
                'columns' => [
                    'event_id',
                    'event_product_id',
                    'product_id',
                    'statusReplacement', 'lostStatusReplacement',
                    'event.name',
                    'supplier.name',
                    'product.article',
                    'product.brand.name',
                    'product.name',
                    'product.category',
                    'eventProduct.color',
                    'eventProduct.sizeformatReplacement',
                    'eventProduct.size',
                    'eventProduct.weightReplacement',
                    'eventProduct.ageRangeReplacement',
                    'event_product_price' => function ($data) use ($criteria) {
                        /* @var $data WarehouseProductRecord */
                        $selector = WarehouseProductRecord::model();
                        $selector->getDbCriteria()->mergeWith($criteria);
                        return round(
                            $selector
                                ->productId($data->product_id)
                                ->status($data->status)
                                ->lostStatus($data->lost_status)
                                ->findColumnAvg('event_product_price'),
                            2
                        );
                    },
                    'created_at' => function ($data) {
                        /* @var $data WarehouseProductRecord */
                        return $this->app()->format->formatDate($data->created_at, 'medium');
                    },
                    'updated_at' => function ($data) {
                        /* @var $data WarehouseProductRecord */
                        return $this->app()->format->formatDate($data->updated_at, 'medium');
                    },
                    'product.logo',
                ],
            ],
            'product' => [
                'group' => [
                    'product_id', 'status', 'lost_status',
                ],
                'columns' => [
                    'product_id',
                    'statusReplacement', 'lostStatusReplacement',
                    'supplier.name',
                    'product.article',
                    'product.brand.name',
                    'product.name',
                    'product.category',
                    'product.color',
                    'product.ageRangeReplacement',
                    'event_product_price' => function ($data) use ($criteria) {
                        /* @var $data WarehouseProductRecord */
                        $selector = WarehouseProductRecord::model();
                        $selector->getDbCriteria()->mergeWith($criteria);
                        return round(
                            $selector
                                ->productId($data->product_id)
                                ->status($data->status)
                                ->lostStatus($data->lost_status)
                                ->findColumnAvg('event_product_price'),
                            2
                        );
                    },
                    'count' => function ($data) use ($criteria) {
                        /* @var $data WarehouseProductRecord */
                        $selector = WarehouseProductRecord::model();
                        $selector->getDbCriteria()->mergeWith($criteria);
                        return round(
                            $selector
                                ->productId($data->product_id)
                                ->status($data->status)
                                ->lostStatus($data->lost_status)
                                ->count(),
                            2
                        );
                    },
                    'created_at' => function ($data) {
                        /* @var $data WarehouseProductRecord */
                        return $this->app()->format->formatDate($data->created_at, 'medium');
                    },
                    'product.logo',
                ],
            ],
            'productBySize' => [
                'group' => [
                    'product_id', 'event_product_sizeformat', 'event_product_size', 'status', 'lost_status',
                ],
                'columns' => [
                    'product_id',
                    'statusReplacement', 'lostStatusReplacement',
                    'event.name',
                    'supplier.name',
                    'product.article',
                    'product.brand.name',
                    'product.name',
                    'product.category',
                    'product.color',
                    'eventProduct.sizeformatReplacement',
                    'eventProduct.size',
                    'eventProduct.weightReplacement',
                    'eventProduct.ageRangeReplacement',
                    'event_product_price' => function ($data) use ($criteria) {
                        /* @var $data WarehouseProductRecord */
                        $selector = WarehouseProductRecord::model();
                        $selector->getDbCriteria()->mergeWith($criteria);
                        return round(
                            $selector
                                ->productId($data->product_id)
                                ->eventProductSizeformat($data->event_product_sizeformat)
                                ->eventProductSize($data->event_product_size)
                                ->status($data->status)
                                ->lostStatus($data->lost_status)
                                ->findColumnAvg('event_product_price'),
                            2
                        );
                    },
                    'count' => function ($data) use ($criteria) {
                        /* @var $data WarehouseProductRecord */
                        $selector = WarehouseProductRecord::model();
                        $selector->getDbCriteria()->mergeWith($criteria);
                        return round(
                            $selector
                                ->productId($data->product_id)
                                ->eventProductSizeformat($data->event_product_sizeformat)
                                ->eventProductSize($data->event_product_size)
                                ->status($data->status)
                                ->lostStatus($data->lost_status)
                                ->count(),
                            2
                        );
                    },
                    'created_at' => function ($data) {
                        /* @var $data WarehouseProductRecord */
                        return $this->app()->format->formatDate($data->created_at, 'medium');
                    },
                    'updated_at' => function ($data) {
                        /* @var $data WarehouseProductRecord */
                        return $this->app()->format->formatDate($data->updated_at, 'medium');
                    },
                    'product.logo',
                ],
            ],
        ];
    }

    public function actionGrouped($warehouseCreatedRange = '', $warehouseUpdatedRange = '', $name = 'simple')
    {
        $criteria = WarehouseProductRecord::model()->getDataProvider(false, [
            'filter' => [
                'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ])->criteria;

        $groupsConfig = $this->_getGroupsConfig($criteria);

        $groupConfig = isset($groupsConfig[$name]) ? $groupsConfig[$name] : reset($groupsConfig);

        $selector = WarehouseProductRecord::model()
            ->orderBy('product_id', 'desc')
            ->orderBy('event_product_sizeformat', 'desc')
            ->orderBy('event_product_size', 'desc')
            ->orderBy('event_product_id', 'desc');
        foreach ($groupConfig['group'] as $attribute) {
            $selector->groupBy($attribute);
        }

        if (!empty($warehouseCreatedRange)) {
            list($createdAtFrom, $createdAtTo) = array_map('strtotime', explode(' - ', $warehouseCreatedRange)) + [0, 0];
            $selector->createdAtBetween($createdAtFrom, $createdAtTo);
        }

        if (!empty($warehouseUpdatedRange)) {
            list($updatedAtFrom, $updatedAtTo) = array_map('strtotime', explode(' - ', $warehouseUpdatedRange)) + [0, 0];
            $selector->updatedAtBetween($updatedAtFrom, $updatedAtTo);
        }

        $provider = $selector
            ->cache(10)
            ->with([
                'event',
                'supplier',
                'eventProduct',
                'product',
                'eventProduct.brand',
            ])
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 300,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $log = $this->app()->log;
        /** @var $log CLogRouter */
        foreach ($log->getRoutes() as $route) {
            /* @var $route CLogRoute */
            $route->enabled = false;
        }

        $csv = CsvFile::loadFromProvider($provider, $groupConfig['columns']);

        $content = $csv->toString();
        unset($csv);
        gc_collect_cycles();

        $this->commitView(null, $this->t('Loading warehouse table'));
        $app = $this->app();
        $downloadFilename = 'warehouse_table__' . date('d-m-Y_H-i') . '.csv';
        $app->request->sendFile($downloadFilename, $content, 'text/csv');
    }
} 
