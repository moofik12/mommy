<?php

namespace MommyCom\Controller\Backend;

use GuzzleHttp\Psr7\ServerRequest;
use MommyCom\Entity\ProductSection;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Money\CurrencyFormatter;
use MommyCom\Service\Money\DefaultCurrencyFactory;
use MommyCom\Service\Warehouse\SecurityCodeGenerator;
use MommyCom\YiiComponent\Backend\AdmAuthManager\AdmWebUser;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\YiiComponent\Facade\Doctrine;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\SupplierOrder\ManagerNotification;
use MommyCom\YiiComponent\SupplierOrder\NotificationDataFactory;
use MommyCom\YiiComponent\SupplierOrder\SupplierOrderService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SupplierEventController extends BackController
{
    /**
     * @param $id
     *
     * @throws \CException
     * @throws \CHttpException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function actionIndex($id)
    {
        /** @var AdminUserRecord $user */
        $user = $this->app()->user->model;

        /* @var $event EventRecord */
        $event = EventRecord::model()->findByPk($id);
        $this->checkAccess($user, $event);

        if ($event === null) {
            throw new \CHttpException(404, 'Flash-sale not found');
        }

        if ($event->status == EventRecord::STATUS_EMPTY) {
            throw new \CHttpException(400, 'Confirmation page for an empty flash-sale cannot be loaded');
        }

        $products = EventProductRecord::model()
            ->eventId($event->id)
            ->hasNumberSoldReal()
            ->orderBy('id', 'asc')
            ->with('product', 'brand', 'event');

        $productsProvider = $products->getDataProvider(false, [
            'pagination' => false,
            'filter' => [
                'attributes' => $this->app()->request->getParam('EventProductRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $currency = DefaultCurrencyFactory::getDefaultCurrencyCode($this->container);

        $this->render('index', [
            'translator' => $this->container->get('translator.backend'),
            'event' => $event,
            'products' => $productsProvider,
            'currency' => $currency,
        ]);
    }

    /**
     * @throws \CException
     * @throws \CHttpException
     */
    public function actionUpdate()
    {
        $request = ServerRequest::fromGlobals();
        $products = $request->getQueryParams()['products'];
        $eventId = (int)$request->getQueryParams()['eventId'];

        /** @var NotificationDataFactory $notificationDataFactory */
        $notificationDataFactory = $this->container->get(NotificationDataFactory::class);

        try {
            $notificationData = $notificationDataFactory->create($eventId);
        } catch (\InvalidArgumentException $exception) {
            throw new \CHttpException($this->t($exception->getMessage()));
        }

        /** @var ManagerNotification $notification */
        $notification = $this->container->get(ManagerNotification::class);
        /** @var SupplierOrderService $supplierOrderService */
        $supplierOrderService = $this->container->get(SupplierOrderService::class);
        $supplierOrderService->confirmProducts($products);
        $supplierOrderService->sendManagerNotification($notificationData, $notification, $products);
    }

    public function actionConfirm()
    {
        $request = ServerRequest::fromGlobals();
        $products = $request->getQueryParams()['products'];

        /** @var SupplierOrderService $supplierOrderService */
        $supplierOrderService = $this->container->get(SupplierOrderService::class);
        /** @var SecurityCodeGenerator $securityCodeGenerator */
        $securityCodeGenerator = $this->container->get(SecurityCodeGenerator::class);
        $supplierOrderService->confirmWarehouse($securityCodeGenerator, $products);
        $supplierOrderService->makePartialDeliveryRecalls($products);
    }

    public function actionChangePassword()
    {
        /** @var AdmWebUser $user */
        $user = $this->app()->user;
        $userRecord = $user->getModel();

        if (!$userRecord->is_supplier) {
            $this->redirect($this->app()->request->urlReferrer);
        } else {
            /* @var $model SupplierRecord */

            $form = \TbForm::createForm('widgets.form.config.supplierChangePassword', $this, [
                'type' => 'horizontal',
            ], $userRecord);

            $form->setModel($userRecord);

            if ($form->submitted('submit') && $form->validate()) {
                $parsedBody = ServerRequest::fromGlobals()->getParsedBody()['AdminUserRecord'];

                $userRecord = $user->getModel();
                $userRecord->newPassword = $parsedBody['newPassword'];
                $userRecord->confirmPassword = $parsedBody['confirmPassword'];
                $userRecord->save();
                $user->setFlash('success', 'Password was successfully changed');
            }

            $this->commitView($userRecord, $this->t('Update supplier data'));
            $this->render('changePassword', [
                'form' => $form,
                'model' => $userRecord,
            ]);
        }
    }

    public function actionUpdateProfile()
    {
        /** @var AdmWebUser $user */
        $user = $this->app()->user;
        $userRecord = $user->getModel();

        if (!$userRecord->is_supplier) {
            $this->redirect(['/'], true);
        }

        /* @var $supplier SupplierRecord */
        $supplier = $userRecord->supplier->supplierData;

        if ($this->app()->request->isPostRequest) {
            $requestBody = ServerRequest::fromGlobals()->getParsedBody()['SupplierRecord'];
            $supplierType = (int)$requestBody['type'];
            $isFormValid = true;

            foreach ($requestBody as $property => $value) {
                if (SupplierRecord::TYPE_LEGAL_ENTITY === $supplierType && '' === $value) {
                    $user->setFlash('error', 'You have to fill all fields');
                    $isFormValid = false;
                    break;
                }

                if ('registration_date' === $property) {
                    $supplier->{$property} = strtotime($value);
                    continue;
                }

                if ('certifiable_sections' === $property) {
                    $supplier->{$property} = implode(',', $value);
                    continue;
                }

                $supplier->{$property} = $value;
            }

            if ($isFormValid) {
                $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', null, 'image/*');
                if ($logoFile instanceof Image) {
                    $supplier->logo = $logoFile;
                }

                if ($supplier->save()) {
                    $user->setFlash('success', 'Successfully saved.');
                } else {
                    $user->setFlash('error', 'Error occurred. Please, try again.');
                }
            }
        }

        $sectionEntities = Doctrine::getManager()
            ->getRepository(ProductSection::class)
            ->findAll();
        $sections = [];

        /** @var ProductSection $sectionEntity */
        foreach ($sectionEntities as $sectionEntity) {
            $sections[] = $sectionEntity->getName();
        }

        $this->commitView($userRecord, $this->t('Update supplier data'));
        $this->render('profile', [
            'translator' => $this->container->get('translator.backend'),
            'sections' => $sections,
            'model' => $supplier,
        ]);
    }

    /**
     * @param int $id
     *
     * @throws \CException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function actionDownloadInvoice(int $id)
    {
        $translator = $this->container->get('translator.backend');

        $index = 1;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A' . $index, $translator->t('№'));
        $sheet->setCellValue('B' . $index, $translator->t('Product'));
        $sheet->setCellValue('C' . $index, $translator->t('Stock number'));
        $sheet->setCellValue('D' . $index, $translator->t('Qty'));
        $sheet->setCellValue('E' . $index, $translator->t('Price'));
        $sheet->setCellValue('F' . $index, $translator->t('Amount'));

        $products = EventProductRecord::model()
            ->eventId($id)
            ->hasNumberSoldReal()
            ->orderBy('id', 'asc')
            ->with('product', 'brand', 'event')
            ->findAll();

        /** @var CurrencyFormatter $currencyFormatter */
        $currencyFormatter = $this->container->get(CurrencyFormatter::class);

        /** @var EventProductRecord $product */
        foreach ($products as $product) {
            $index++;
            $quantity = is_null($product->number_supplied) ? $product->number_sold : $product->number_supplied;
            $sheet->setCellValue('A' . $index, $index - 1);
            $sheet->setCellValue('B' . $index, $product->name);
            $sheet->setCellValue('C' . $index, $product->article);
            $sheet->setCellValue('D' . $index, $quantity);
            $sheet->setCellValue('E' . $index, $currencyFormatter->format(round($product->price_purchase)));
            $sheet->setCellValue('F' . $index, $currencyFormatter->format($quantity * round($product->price_purchase)));
        }

        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8");
        header("Content-Disposition: attachment; filename=invoice.xlsx");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);

        $writer = new Xlsx($spreadsheet);
        $writer->setOffice2003Compatibility(true);
        $writer->save('php://output');
    }

    /**
     * @throws \CHttpException
     */
    public function actionContractInformation()
    {
        /** @var AdmWebUser $user */
        $user = $this->app()->user;
        $userRecord = $user->getModel();

        if (!$userRecord->is_supplier) {
            $this->redirect(['/'], true);
        }

        /* @var $supplier SupplierRecord */
        $supplier = $userRecord->supplier->supplierData;

        $model = SupplierContractRecord::model()->findByPk($supplier->contract_id);

        if (null === $model) {
            throw new \CHttpException('404', $this->t('Data not found'));
        }

        $form = Form::createForm('widgets.form.config.supplierProfileContract', $model, [
            'type' => 'horizontal',
        ]);

        $this->render('contractInformation', ['form' => $form]);
    }

    /**
     * @param AdminUserRecord $user
     *
     * @throws \CHttpException
     */
    private function checkAccess(AdminUserRecord $user, EventRecord $event)
    {
        if ($user->is_supplier) {
            if (!$user->supplier || empty($user->supplier->supplier_id)) {
                throw new \CHttpException(403, 'No suppliers belong to account');
            }

            if ($event->supplier_id !== $user->supplier->supplier_id) {
                throw new \CHttpException(400, 'Permission denied');
            }
        }
    }

    /**
     * @param int $id
     */
    public function actionFlashSales(int $id = 0)
    {
        /** @var AdmWebUser $user */
        $user = $this->app()->user;
        $supplierId = null;
        if ($user->getModel()->is_supplier) {
            $supplierId = $user->getModel()->supplier->supplier_id;
            $id = $supplierId;
        }

        $events = EventRecord::model()
            ->supplierId($id);

        $eventsProvider = $events->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->render('flashSales', [
            'provider' => $eventsProvider,
        ]);
    }
}
