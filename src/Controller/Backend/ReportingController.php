<?php

namespace MommyCom\Controller\Backend;

use DateInterval;
use DateTime;
use MommyCom\Model\Db\OrderProcessingStatusHistoryRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Reporting\ActiveReport;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class ReportingController extends BackController
{
    public $title;
    public $loadConfig;
    public $timeColumn;
    public $report;
    public $selector;
    public $with;

    public function actionOrders($time = '', $group = '', $chartType = null)
    {
        $this->title = $this->t('Orders');
        $this->loadConfig = 'orders';
        $this->selector = OrderRecord::model()->orderBy('created_at', 'desc');

        $this->renderReport($time, $group, $chartType);
    }

    public function actionOrderProcessing($time = '', $group = '', $chartType = null)
    {
        $this->title = $this->t('Order processing');
        $this->loadConfig = 'orderProcessing';
        $this->selector = OrderProcessingStatusHistoryRecord::model()->orderBy('time', 'desc');

        $this->renderReport($time, $group, $chartType);
    }

    private function renderReport($time = '', $group = '', $chartType = null)
    {
        $config = include ROOT_PATH . '/config/yii/backend/reporting/' . $this->loadConfig . '.php';
        foreach ($config as $attribute => $data) {
            $this->$attribute = $data;
        }

        if (!empty($time)) {
            list($createdAtFrom, $createdAtTo) = array_map('strtotime', explode(' - ', $time)) + [0, 0];
            $createdAtTo += 86400 - 1; // 23:59:59
        } else {
            $date = new DateTime();
            $createdAtTo = $date->getTimestamp();
            $createdAtFrom = $date->sub(new DateInterval('P3D'))->getTimestamp();
            $time = strftime('%d.%m.%Y', $createdAtFrom) . ' - ' . strftime('%d.%m.%Y', $createdAtTo);
        }

        $group = Cast::toStr($group);
        $customGroups = Cast::toStrArr(Yii::app()->request->getParam('customGroups'));

        $report = $this->createReport($group, $customGroups, $createdAtFrom, $createdAtTo);

        $this->render('reportView', [
            'report' => $report,
            'time' => $time,
            'chartType' => $this->normalizeChartType($chartType),
        ]);
    }

    private function createReport($group, $customGroups, $createdAtFrom, $createdAtTo)
    {
        $report = Yii::createComponent([
            'class' => ActiveReport::class,
        ] + $this->report);

        /* @var $report ActiveReport */

        $report->timeFilter = [
            'column' => $this->timeColumn,
            'from' => $createdAtFrom,
            'to' => $createdAtTo,
        ];

        $report->selectedCustomGroups = $customGroups;

        if (!empty($group)) {
            $report->selectedGroup = $group;
        }

        $report->selector = $this->selector;

        return $report;
    }

    private function normalizeChartType($chartType)
    {
        $availableChartTypes = ['AreaChart', 'ColumnChart'];
        if (!in_array($chartType, $availableChartTypes)) {
            $chartType = reset($availableChartTypes);
        }

        return $chartType;
    }
} 
