<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CHttpRequest;
use CSort;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class UserUnformedCartController extends BackController
{
    /**
     * @property-description Просмотр списка неоформленных корзин
     */
    public function actionIndex()
    {
        /* @var $request CHttpRequest */
        $request = $this->app()->request;
        $hasTelephoneArray = Cast::toStrArr($request->getParam('hasTelephone'));
        $hasTelephone = end($hasTelephoneArray);
        $eventNameArray = Cast::toStrArr($request->getParam('eventName'));
        $eventName = end($eventNameArray);
        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);
        $startTime = 0;
        $endTime = 0;

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow' . Utf8::trim($rangeData[1]));
        }

        $selector = $provider = CartRecord::model()
            ->groupBy('YEAR(FROM_UNIXTIME(created_at)), MONTH(FROM_UNIXTIME(created_at)), DAY(FROM_UNIXTIME(created_at))')
            ->groupBy('user_id');

        if ($startTime > 0) {
            $selector->createdAtGreater($startTime);
        }

        if ($endTime > 0) {
            $selector->createdAtLower($endTime);
        }

        if ($hasTelephone == 'yes') {
            $selector->with([
                    'user' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'onlyHasTelephone',
                        ],
                    ]]
            );
        } elseif ($hasTelephone == 'not') {
            $selector->with([
                    'user' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'onlyNotHasTelephone',
                        ],
                    ]]
            );
        }

        $selector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'nameLike' => $eventName,
                    ],
                ]]
        );

        $dataProvider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('CartRecord', []),
                'scenario' => 'searchPrivileged',
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'dataProvider' => $dataProvider,
            'eventName' => $eventName,
            'hasTelephone' => $hasTelephone,
            'rangeData' => implode('-', $rangeData),
        ]);
    }

    public function actionView($id)
    {
        $id = Cast::toUInt($id);
        /** @var CartRecord $model */
        $model = CartRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $provider = CartRecord::model()->userId($model->user_id)->createdAtDay($model->created_at)->getDataProvider();

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('view', [
                'dataProvider' => $provider,
            ]);
            $this->app()->end();
        }

        $this->commitView($model, $this->t('View user\'s shopping cart'));
        $this->render('view', [
            'dataProvider' => $provider,
        ]);
    }

} 
