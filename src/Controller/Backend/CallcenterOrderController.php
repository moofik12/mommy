<?php

namespace MommyCom\Controller\Backend;

use CActiveForm;
use CActiveRecord;
use CArrayDataProvider;
use CHtml;
use CHttpException;
use CLogger;
use CSort;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderProduct;
use MommyCom\Entity\WarehouseProduct;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\MoneyControlRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\SmsMessageRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\OrderNotification\OrderNotificationMessageFactory;
use MommyCom\Repository\WarehouseProductRepository;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use ReflectionClass;
use Yii;

class CallcenterOrderController extends BackController
{
    const ORDER_MERGE_PERIOD = 432000; //5 days
    const ORDER_SEPARATE_PERIOD = 259200; //3 days

    /**
     * @var array
     */
    public $labelCss = [
        OrderRecord::PROCESSING_CALLCENTER_CONFIRMED => 'label-success',
        OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED => 'label-success',
        OrderRecord::PROCESSING_CALLCENTER_RECALL => 'label-info',
        OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING => 'label-warning',
        OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER => 'label-important',
        OrderRecord::PROCESSING_CANCELLED => 'label-important',
        OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE => 'label-important',
    ];

    /**
     * @param OrderRecord $order
     *
     * @return string
     * @throws LogicException
     */
    private function _createRequisitesToSms(OrderRecord $order)
    {
        if (!isset($this->app()->params['requisites']['requisitesSMS'])) {
            throw new LogicException($this->t('No text found'));
        }

        $cn = $this->app()->countries->getCurrency();
        $requisitesText = $this->app()->params['requisites']['requisitesSMS']
            . "\nНазначение платежа: Оплата заказа {order}\nК оплате: {amount}\nТел.:{phone}";

        return strtr($requisitesText, [
            '{order}' => $order->id,
            '{amount}' => $this->app()->currencyFormatter->format($order->getPayPrice(), $cn->getName('short')),
            '{phone}' => $this->app()->params['phone'],
        ]);
    }

    /**
     * @param int $id order id
     * @param bool $strictMode
     *
     * @throws Exception
     */
    public function actionSendRequisitesSmsMessage($id, $strictMode = false)
    {
        static $intervalToSend = 3600; //1 hour
        $error = '';

        $model = OrderRecord::model()->findByPk($id);
        $phone = $this->app()->request->getParam('phone');
        $timeNow = time();

        try {
            if (!$model) {
                throw  new CHttpException('404', $this->t('Order') . " №{$id} " . $this->t('not found'));
            } else if (empty($phone)) {
                throw  new CHttpException('403', $this->t('Missing phone number'));
            } else if (!$model->isAvailableForPayment()) {
                throw  new CHttpException('403', $this->t('Order not available for payment'));
            }

            $textMessage = $this->_createRequisitesToSms($model);

            $hasMessage = SmsMessageRecord::model()->textIs($textMessage)->orderBy('created_at', 'desc')->find();

            if ($hasMessage && in_array($hasMessage->status, [SmsMessageRecord::STATUS_WAITING, SmsMessageRecord::STATUS_SENT]) && ($hasMessage->created_at + $intervalToSend) > $timeNow) {
                throw  new CHttpException('403', 'В ' . $this->app()->getDateFormatter()->formatDateTime($hasMessage->created_at, null) . ' уже было отправлено СМС. Повторите чуть позже.');
            }

            $message = new SmsMessageRecord();
            $message->is_need_to_translit = 1;
            $message->user_id = $model->user_id;
            $message->telephone = $phone;
            $message->text = $textMessage;
            $message->type = SmsMessageRecord::TYPE_CUSTOM;
            $message->admin_id = $this->app()->user->id;

            if (!$message->save()) {
                $firstError = '';
                foreach ($message->getErrors() as $attribute => $errors) {
                    $firstError = $errors[0];
                    break;
                }

                throw  new CHttpException('500', 'Произошла ошибка при создании сообщения. Ошибка: ' . $firstError);
            }
        } catch (Exception $e) {
            if ($strictMode) {
                throw $e;
            } else {
                $error = $e->getMessage();
            }
        }

        $this->renderJson(['error' => $error]);
    }

    /**
     * выдача нового заказа или списка заказов для обработки
     * если root пользователь выдача списка заказов для обработки
     */
    public function actionNew()
    {
        $model = OrderRecord::model()
            ->orderNew()
            ->processingNotActive();;

        //если не супер пользователь перевести на страницу редактирования нового заказа
        if (!$this->app()->user->isRoot()) {
            /** @var OrderRecord $data */
            $data = $model->find();

            if ($data instanceof CActiveRecord) {
                if ($data->processing_status == OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED) {
                    Yii::log("Ошибочная выдача нового заказа {$data->id}", CLogger::LEVEL_ERROR);
                }
                $this->commitView($data, $this->t('Distributing new order for editing'));
                $this->redirect(['processing', 'id' => $data->getPrimaryKey()]);
            } else {
                $this->commitView(null, $this->t('Redirect to new order waiting page'));
                $this->redirect(['processing']);
            }
        }

        // grid
        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('OrderRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'ordered_at' => CSort::SORT_ASC,
                ],
            ],
        ]);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('new', ['provider' => $provider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View new orders'));
        $this->render('new', ['provider' => $provider]);
    }

    /**
     * Выдача на редактирование новых заказов, 0 - нет отображения, $id > 0 - выдача заказа
     *
     * @param int $id
     * @param int|false $deliveryType
     * @param int|false $cityId
     *
     * @throws \CException
     */
    public function actionProcessing($id = 0, $deliveryType = false, $cityId = false)
    {
        $id = Cast::toUInt($id);
        $adminUser = $this->app()->user;

        $processingStatusNotEdit = [
            OrderRecord::PROCESSING_STOREKEEPER_PACKAGED,
            OrderRecord::PROCESSING_STOREKEEPER_MAILED,
        ];

        /** @var OrderRecord $order */
        $order = OrderRecord::model()->findByPk($id);

        if (null === $order) {
            $this->render('emptyData');
            $this->app()->end();
        }

        $isSameAdmin = $order->admin_id === $adminUser->id;
        $isEditAllowed = (!$order->bindToAdmin($adminUser->id)) ? $isSameAdmin : true;

        $orderStartedFirstEditEndLostAdmin = $order->isProcessingLost()
            ? AdminUserRecord::model()->findByPk($order->admin_id)
            : null;

        $processingStatus = Cast::toUInt($order->processing_status);

        if (in_array($processingStatus, $processingStatusNotEdit, true)) {
            $this->render('notEdit', ['data' => $order]);
            $this->app()->end();
        }

        /** @var OrderRecord[] $ordersToBind */
        $ordersToBind = OrderRecord::model()
            ->processingStatus(OrderRecord::PROCESSING_UNMODERATED)
            ->processingStatus(OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING, false)
            ->processingStatus(OrderRecord::PROCESSING_CALLCENTER_CALL_LATER, false)
            ->processingStatus(OrderRecord::PROCESSING_CALLCENTER_PREPAY, false)
            ->userId($order->user_id)
            ->processingNotActive()
            ->findAll();

        if ($order->uuid) {
            $ordersUuidToBind = OrderRecord::model()
                ->uuidIs($order->uuid)
                ->findAll();

            $ordersToBind = array_merge($ordersToBind, $ordersUuidToBind);
        }

        //если есть еще необработанные заказы этого пользователя тоже привязать оператора
        foreach ($ordersToBind as $orderToBind) {
            $orderToBind->bindToAdmin($adminUser->id);
        }

        if ($deliveryType !== false) {
            $order->delivery_type = Cast::toUInt($deliveryType);
        }

        if ($promocode = $this->app()->request->getParam('promocode', false)) {
            $order->promocode = $promocode;
        }

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('processing', [
                'data' => $order,
                'enableEdit' => true && $isEditAllowed,
                'orderStartedFirstEditEndLostAdmin' => $orderStartedFirstEditEndLostAdmin,
                'availableDeliveries' => $availableDeliveries,
            ]);

            $this->app()->end();
        }

        $paymentLink = $this->app()->frontendUrlManager->createAbsoluteUrl('pay/index', ['uid' => $id, 'token' => $order->payment_token]);
        $tokenIsExpired = (CurrentTime::getUnixTimestamp() - $order->payment_token_expires_at >= 0);

        $this->commitView($order, $this->t('Order Processing'));
        $this->render('processing', [
            'data' => $order,
            'enableEdit' => true && $isEditAllowed,
            'orderStartedFirstEditEndLostAdmin' => $orderStartedFirstEditEndLostAdmin,
            'availableDeliveries' => $availableDeliveries,
            'paymentLink' => $paymentLink,
            'tokenIsExpired' => $tokenIsExpired,
        ]);
    }

    /**
     * @param $id
     */
    public function actionView($id)
    {
        $data = OrderRecord::model()->findByPk($id);

        if ($data === null) {
            $this->render('emptyData');
            $this->app()->end();
        }

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_item', [
                'data' => $data,
                'enableEdit' => false,
                'availableDeliveries' => $availableDeliveries,
            ]);
            $this->app()->end();
        }

        $this->commitView($data, $this->t('View Order Data'));
        $this->render('processing', [
            'data' => $data,
            'enableEdit' => false,
            'availableDeliveries' => $availableDeliveries,
        ]);
    }

    public function actionRepeat()
    {
        // grid
        $provider = OrderRecord::model()
            ->orderRepeat()
            ->processingNotActive()
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'scenario' => 'searchPrivileged',
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                ],
                'sort' => [
                    'attributes' => ['ordered_at' => CSort::SORT_ASC],
                ],
            ]);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('new', ['provider' => $provider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View call back list'));
        $this->render('new', ['provider' => $provider]);
    }

    public function actionCancelled()
    {
        $provider = OrderRecord::model()
            ->orderCancelled()
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'scenario' => 'searchPrivileged',
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                ],
                'sort' => [
                    'defaultOrder' => [
                        'ordered_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('history', ['provider' => $provider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View canceled orders'));
        $this->render('history', ['provider' => $provider]);
    }

    public function actionHistory()
    {
        $filterProductId = $this->app()->request->getParam('filterProductId');
        $filterEventId = $this->app()->request->getParam('filterEventId');
        $filterSupplierID = $this->app()->request->getParam('filterSupplierID');
        /* @var OrderRecord $model */
        $model = OrderRecord::model();

        if ($filterProductId) {
            $model->productId($filterProductId);
        }

        if ($filterEventId) {
            $model->eventId($filterEventId);
        }

        if ($filterSupplierID) {
            $model->supplierIdIs($filterSupplierID);
        }

        if ($model->getDbCriteria()->with) {
            //фикс запроса при использовании связей, иначе связанные эл. учитываются в лимите
            $model->groupBy($model->getTableAlias() . ".id");
        }

        $provider = $model
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'scenario' => 'searchPrivileged',
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                ],
                'sort' => [
                    'defaultOrder' => [
                        'ordered_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('history', ['provider' => $provider]);
            $this->app()->end();
        }

        if ($model->getDbCriteria()->with) {
            $relations = array_keys($model->getDbCriteria()->with);
            foreach ($provider->getData() as $order) {
                /* @var OrderRecord $order */
                foreach ($relations as $relation) {
                    if (is_string($relation)) {
                        unset($order->$relation);
                    }
                }
            }
        }

        $this->commitView(null, $this->t('View purchase history'));
        $this->render('history', ['provider' => $provider]);
    }

    /**
     * заказы которые не подтверждены но надо обработать сейчас
     */
    public function actionNotСonfirmed()
    {
        $eventStatus = $this->app()->request->getParam('eventStatus', 'all');
        $model = OrderRecord::model()
            ->orderNotConfirmed();

        if ($eventStatus == 'passed') {
            $model->eventsFinished();
        }

        $countActive = $model->copy()->processingActive()->count();

        $provider = $model
            ->processingNotActive()
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'scenario' => 'searchPrivileged',
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                ],
                'sort' => [
                    'attributes' => ['ordered_at' => CSort::SORT_ASC],
                ],
            ]);

        $this->commitView(null, $this->t('View unconfirmed orders'));
        $this->render('notConfirmed', ['provider' => $provider, 'countActive' => $countActive]);
    }

    /**
     * @param int $id
     */
    public function actionOrderSeparate($id)
    {
        $result = new Reply();
        $adminUser = $this->app()->user;

        $heedOrderProducts = $this->app()->request->getPost('products', []);
        $order = OrderRecord::model()->with('positions')->together()->findByPk($id);

        if (empty($heedOrderProducts)) {
            $result->addError($this->t('No products found for ordering'));
        }

        if ($order == null) {
            $result->addError($this->t('No order found'));
        }

        if ($order && count($order->positions) == $heedOrderProducts) {
            $result->addError($this->t('Transfering all products to a new order is not possible'));
        }

        if ($order && $heedOrderProducts) {
            $orderProductCount = OrderProductRecord::model()->idIn($heedOrderProducts)->count();

            if ($orderProductCount != count($heedOrderProducts)) {
                $result->addError($this->t('Some products not found'));
            }
        }

        if (!$result->hasErrors()) {
            $newOrder = new OrderRecord('merge');
            /* @var OrderProductRecord[] $orderProducts */
            $orderProducts = OrderProductRecord::model()->idIn($heedOrderProducts)->findAll();

            //hard copy
            foreach (OrderRecord::getSeparateAttributes() as $attribute) {
                if (isset($order->{$attribute})) {
                    $newOrder->{$attribute} = $order->{$attribute};
                }
            }

            if ($newOrder->validate() && $newOrder->save()) {
                $newOrder->refresh();
                $newOrder->forceBindToAdmin($adminUser->id);
                $countMovePosition = 0;

                foreach ($orderProducts as $orderProduct) {
                    if ($newOrder->movePosition($orderProduct)) {
                        $countMovePosition++;
                    }
                }

                if (!$countMovePosition) {
                    $result->addError($this->t("Error transferring products"));
                    $newOrder->delete();
                }

                $result->dataItem('editUrl', $this->createUrl('processing', ['id' => $newOrder->id]));
            } else {
                foreach ($newOrder->getErrors() as $errors) {
                    $first = reset($errors);
                    $result->addError($first);
                    break;
                }
            }
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @param int $id
     * @param int $mergeId
     */
    public function actionOrderMerge($id, $mergeId)
    {
        $id = Cast::toUInt($id);
        $mergeId = Cast::toUInt($mergeId);
        $result = new Reply();

        if ($id === 0 || $mergeId === 0) {
            $result->addError($this->t('No data to process'));
        }

        /** @var OrderRecord $order */
        $order = OrderRecord::model()->with('positions')->together()->findByPk($id);
        $promocodeOld = $order->promocode;

        /** @var OrderRecord $orderMerge */
        $orderMerge = OrderRecord::model()->with('positions')->together()->findByPk($mergeId);
        $countPositionOrderAvailable = 0;
        $countPositionOrderMergeAvailable = 0;

        if (abs($orderMerge->ordered_at - $order->ordered_at) > self::ORDER_MERGE_PERIOD && !$this->app()->user->isRoot()) {
            $result->addError($this->t('Time to merge orders expired'));
        }

        if ($order === null || $orderMerge === null) {
            $result->addError($this->t('No data found for processing'));
        }

        if ($order->admin_id != $this->app()->user->id) {
            $result->addError($this->t('Order editing has not be assigned to you'));
        }

        /*if ($order->admin_id != $this->app()->user->id) {
            $result->addError('Order editing has not be assigned to you');
        }*/

        //ДРОПШИППИНГ
        if ($order->is_drop_shipping || $orderMerge->is_drop_shipping) {
            if ($order->is_drop_shipping != $orderMerge->is_drop_shipping) {
                $result->addError($this->t('Merging orders from dropshipping offer and orders from .. is not possible') . $this->app()->name);
            } elseif ($order->supplier_id != $orderMerge->supplier_id) {
                $result->addError($this->t('Merging orders from different dropshipping suppliers is not possible'));
            }
        }

        //ОПЛАТЫ
        /*if (!!$order->card_payed != !!$orderMerge->card_payed) {
            $result->addError('Merging orders is not possible, due to the fact that one of the orders has been paid, and the other one has not');
        }*/

        if ($order->card_payed > 0 || $orderMerge->card_payed > 0) {
            // один из заказов оплачен безналом

            $orderPayPrice = $order->getPayPrice();
            $orderMergePayPrice = $orderMerge->getPayPrice();

            if ($orderPayPrice > 0 || $orderMergePayPrice > 0) {
                // если какой-то из заказов не полностью оплачен тогда выдаем ошибку
                $result->addError($this->t('Merging orders is not possible, due to the fact that one of the orders has been paid, and the other one has not'));
            }
        }

        //НЕЛЬЗЯ ОБЪЕДИНЯТЬ ЗАКАЗЫ ЕСЛИ СКИДКА ПО КОМПАНИИ ПЕРЕЙДЕТ В НОВЫЙ ЗАКАЗ И СТАНЕТ ДОМИНИРУЮЩЕЙ
        if ($orderMerge->discount_campaign_id > 0 || $order->discount_campaign_id > 0) {
            //REFRESH!!! cache
            UnShardedActiveRecord::clearModelsCache();

            //workaround
            $positions = array_merge($order->positions, $orderMerge->positions);
            $orderCopyReflection = new ReflectionClass('CActiveRecord');
            $orderCopyReflectionRelated = $orderCopyReflection->getProperty('_related');
            $orderCopyReflectionRelated->setAccessible(true);

            $orderCopy = OrderRecord::model()->findByPk($order->id);
            $orderCopyRelated = $orderCopyReflectionRelated->getValue($orderCopy);
            $orderCopyRelated['positions'] = $positions;
            $orderCopyReflectionRelated->setValue($orderCopy, $orderCopyRelated);

            $orderCopyMerge = OrderRecord::model()->findByPk($orderMerge->id);
            $orderCopyRelatedMerge = $orderCopyReflectionRelated->getValue($orderCopyMerge);
            $orderCopyRelatedMerge['positions'] = $positions;
            $orderCopyReflectionRelated->setValue($orderCopyMerge, $orderCopyRelatedMerge);

            if ($orderCopy->discount_campaign_id > 0 && $orderCopy->getCountDiscount() > $orderCopyMerge->getCountDiscount()) {
                $result->addError($this->t('A special offer is valid for the current order, merging with the current order is impossible'));
            } elseif ($orderCopyMerge->discount_campaign_id > 0 && $orderCopyMerge->getCountDiscount() > $orderCopy->getCountDiscount()) {
                $result->addError($this->t('A special offer is valid for the previous order, merging with the previous order is not possible'));
            }

            //СТРАХОВКА
            $order->getRelated('positions', true);
            $orderMerge->getRelated('positions', true);
        }

        if (!$result->hasErrors()) {
            foreach ($orderMerge->positions as $position) {
                if ($position->storekeeper_status != OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED) {
                    $result->addError($this->t('Not possible, there are products not confirmed by the warehouse'));
                    break;
                }

                if ($position->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_CANCELLED) {
                    $countPositionOrderMergeAvailable++;
                }
            }

            foreach ($order->positions as $position) {
                if ($position->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_CANCELLED) {
                    $countPositionOrderAvailable++;
                }
            }
        }

        if ($countPositionOrderMergeAvailable + $countPositionOrderAvailable > ShopShoppingCart::getMaxCartSize(!$order->is_drop_shipping)) {
            $result->addError(\Yii::t('application', 'Невозможно. Будет больше {n} товар в одном заказе!
                | Невозможно. Будет больше {n} товара в одном заказе! | Невозможно. Будет больше {n} товаров в одном заказе!', ShopShoppingCart::getMaxCartSize(!$order->is_drop_shipping)));
        }

        if (!$result->hasErrors()) {
            $count = $order->orderMerge($orderMerge);
            $orderMerge->callcenter_comment .= 'Отменено. По причине объединения с заказом №' . $order->id;
            $orderMerge->save(true, ['callcenter_comment']);

            $result->addInfo('В заказе было:' . \Yii::t('app', ' {n} товар | {n} товара | {n} товаров ', $orderMerge->positionCount)
                . '. Скопировано:' . \Yii::t('app', ' {n} товар | {n} товара | {n} товаров ', $count));
        }

        if ($promocodeOld != $order->promocode) {
            $result->addWarning("ВНИМАНИЕ! Необходимо вставить промокод в заказ.<br> Промокод: {$order->promocode}");
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * обновление продуктов заказа
     *
     * @param $id
     * @param $status
     *
     * @throws CHttpException
     */
    public function actionOrderProductUpdate($id, $status)
    {
        $result = new Reply();

        $validStatus = [
            OrderProductRecord::CALLCENTER_STATUS_ACCEPTED => 'confirm',
            OrderProductRecord::CALLCENTER_STATUS_CANCELLED => 'cancel',
            OrderProductRecord::CALLCENTER_STATUS_UNMODERATED => 'restore',
        ];

        /** @var OrderProductRecord $model */
        $model = OrderProductRecord::model()->findByPk($id);

        if ($model === null || !in_array($status, $validStatus)) {
            throw new CHttpException(404, 'Data or Status not found');
        }
        /*
                if($model->storekeeper_status == OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED) {
                    $result->addError('Невозможно изменить. Статус товара на складе: ' . $model->storekeeperStatusReplacement);
                }
        */
        if ($model->order->isCanceled()) {
            $result->addError($this->t('Order canceled! First, change order status to "Deferred call"'));
        }

        $hasNumber = $model->number;
        if ($model->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_CANCELLED) {
            $hasNumber = 0;
        }

        $form = $this->app()->getRequest()->getPost('OrderProductRecord');
        $model->setAttributes($form);
        $model->callcenter_status = array_search($status, $validStatus);

        //проверка возможности добавления товара
        $numberAvailable = $model->getWarehouseNumberAvailable(true);
        if (!$model->event->isSupplierAnswerOrder || $model->event->is_drop_shipping) {
            $numberAvailable = $model->product->getNumberAvailable() + $hasNumber;
        }

        if ($model->number > $numberAvailable
            && $model->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_CANCELLED

        ) {
            $result->addError($this->t("Disable to add") . ' ' . $model->number . ' ' . $this->t("products. Max:") . ' ' . $numberAvailable);
        }

        if (!$result->hasErrors()) {
            if ($model->save()) {
                $result->setState(true);
            } else {
                foreach ($model->errors as $errors) {
                    $result->addError($errors[0]);
                }
            }
        }

        $this->renderJson($result->getResponse());
    }

    public function actionOrderProductAdd()
    {
        $eventProductId = Cast::toUInt($this->app()->request->getPost('article'));
        $orderId = Cast::toUInt($this->app()->request->getPost('order'));

        $result = new Reply();

        if ($eventProductId === 0 || $orderId === 0) {
            $result->addError($this->t('Product or order not found'));
            $this->renderJson($result->getResponse());
        }
        /** @var EventProductRecord $eventProduct */
        $eventProduct = EventProductRecord::model()->findByPk($eventProductId);
        /** @var OrderRecord $order */
        $order = OrderRecord::model()->findByPk($orderId);

        if ($eventProduct === null || $order === null) {
            $result->addError($this->t('Product or order not found'));
        } else if ($eventProduct->event === null) {
            $result->addError($this->t('Flash-sale not found'));
        } else {
            if ($order->is_drop_shipping != $eventProduct->event->is_drop_shipping) {
                $result->addError($this->t('Adding products between dropshipping offers and internal ones is not allowed'));
            } else if ($order->is_drop_shipping && $eventProduct->event->is_drop_shipping && $order->supplier_id != $eventProduct->event->supplier_id) {
                $result->addError($this->t('Adding products between different dropshipping offers is not allowed'));
            }

            $countProductAccept = 0;
            //если такой товар уже есть
            foreach ($order->positions as $position) {
                /** @var $product EventProductRecord */
                if ($position->product->id == $eventProductId) {
                    $result->addError($this->t('Product already in the shopping cart'));
                }

                if ($position->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_CANCELLED) {
                    $countProductAccept++;
                }
            }

            //не больше ShopShoppingCart::getMaxCartSize() товаров в корзине
            if ($countProductAccept > ShopShoppingCart::getMaxCartSize(!$order->is_drop_shipping)) {
                $result->addError($this->t('Not more than') . ' ' . ShopShoppingCart::getMaxCartSize(!$order->is_drop_shipping) . ' ' . $this->t('products in order'));
            }

            if ($eventProduct->event->isTimeActive || $eventProduct->event->is_drop_shipping || $eventProduct->event->isAvailableOrdersEdit()) {
                $available = $eventProduct->getIsPossibleToBuy(1, false);
                if ($available == 0) {
                    $result->addError($this->t('There is no required quantity in flash-sale'));
                }
            } else {
                if ($eventProduct->getStockNumber(false) == 0 && $eventProduct->getWaitingDeliveryNumber() == 0) {
                    $result->addError($this->t('No required quantity available'));
                }
            }
        }

        if (!$result->hasErrors()) {
            $product = new OrderProductRecord();
            $product->order_id = $order->id;
            $product->user_id = $order->user_id;
            $product->product_id = $eventProduct->id;
            $product->event_id = $eventProduct->event_id;
            $product->price = $eventProduct->price;
            $product->is_added_by_callcenter = true;
            $product->number = 1;

            if (!$product->save()) {
                $result->addError($this->t('Error saving product in the database'));
            }
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionRefreshProductPrice($id)
    {
        $id = Cast::toUInt($id);
        $result = new Reply();

        $model = OrderProductRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $model->forceRefreshPrice();
        if (!$model->save(false)) {
            $result->addError($this->t('Error saving data!'));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @param $id
     */
    public function actionResendCreateMail($id)
    {
        $notificationFactory = new OrderNotificationMessageFactory();
        $notification = $notificationFactory->createNotification(
            OrderRecord::model()->findByPk($id),
            OrderNotificationMessageFactory::TYPE_CREATE
        );
        $notification->send();

        $this->redirect($this->app()->request->urlReferrer);
    }

    /**
     * @param string $id
     *
     * @throws CHttpException
     * @throws \CException
     */
    public function actionHistoryDetails($id)
    {
        $model = OrderRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        $this->commitView(null, $this->t('View Order Data'));
        $this->renderPartial('_historyItem', [
            'model' => $model,
            'availableDeliveries' => $availableDeliveries,
        ]);
    }

    /**
     * при ошибке запись в лог
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        /** @var OrderRecord $model */
        $model = OrderRecord::model()->findByPk($id);

        $status = $this->app()->request->getPost('status');
        $nextTimeCallString = $this->app()->request->getPost('nextTimeCall', '');
        $nextTimeCall = strtotime("today $nextTimeCallString");

        if ($model === null) {
            throw new CHttpException(404, $this->t('Order not found'));
        }

        if ($status === null) {
            $data = $this->app()->request->getPost('OrderRecord');
            $model->setAttributes($data);
            $model->setMailAfterString($data['mailAfterString']);
            $model->save();
        }

        $shopBonusPoints = new ShopBonusPoints();
        if (isset($model->user)) {
            $shopBonusPoints->init($model->user);
        }

        $status = intval($status);
        $model->scenario = 'callcenterNegative';

        //дополнительная проверка данных
        if ($status == OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
            || $status == OrderRecord::PROCESSING_CALLCENTER_PREPAY
        ) {
            $model->scenario = 'callcenter';
            $model->attachEventHandler('onAfterValidate', function ($event) use ($status) {
                $unmoderatorPosition = 0;
                $countConfirmedPosition = 0;
                /** @var OrderRecord $order */
                $order = $event->sender;

                /** @var OrderProductRecord $product */
                foreach ($order->positions as $product) {
                    if ($product->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_UNMODERATED) {
                        $unmoderatorPosition++;
                    } elseif ($product->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_ACCEPTED) {
                        $countConfirmedPosition++;
                    }

                    if ($product->number > EventProductRecord::MAX_PER_BUY) {
                        $order->addError('region_id', $this->t('The quantity of one product must not exceed') . ' ' . EventProductRecord::MAX_PER_BUY_DEFAULT . ' ' . $this->t('items'));
                    }
                }

                if ($unmoderatorPosition > 0) {
                    $order->addError('region_id', $this->t('Confirmation required') .
                        $this->t('{n} product in the shopping cart| {n} products in the shopping cart | {n} products in the shopping cart', $unmoderatorPosition));
                } elseif ($countConfirmedPosition === 0) {
                    $order->addError('region_id', $this->t('There are no confirmed products in the order'));
                } elseif ($countConfirmedPosition > ShopShoppingCart::getMaxCartSize(!$order->is_drop_shipping)) {
                    $order->addError('region_id', $this->t('Your order can contain a maximum of {n} item(s)', ShopShoppingCart::getMaxCartSize(!$order->is_drop_shipping)));
                }

                if ($status == OrderRecord::PROCESSING_CALLCENTER_CONFIRMED && $order->user && $order->user->blackList) {
                    if ($order->user->blackList->isActive() && $order->card_payed == 0 && $order->getPayPrice() > 0) {
                        $order->addError('region_id', $this->t('Users in the "black list", confirmation only for prepaid orders'));
                    }
                }
            });
        }

        if (OrderRecord::PROCESSING_CANCELLED === $status) {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getEntityManager();
            $orderProducts = $entityManager->getRepository(OrderProduct::class)
                ->findBy([
                    'orderId' => $model->id,
                ]);

            $this->makeStockFromOrder($orderProducts);
        }

        $data = $this->app()->request->getPost('OrderRecord');

        $model->setAttributes($data);
        $model->setMailAfterString($data['mailAfterString']);
        $model->processing_status = $status;

        if ($status == OrderRecord::PROCESSING_CALLCENTER_CALL_LATER) {
            $timeNow = time();
            $model->next_called_at = $nextTimeCall < $timeNow ? $timeNow : $nextTimeCall;
        }

        //validate form
        if ($this->app()->request->isAjaxRequest && $data) {
            $_POST[CHtml::modelName($model)]['processing_status'] = $status;
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        if ($model->save()) {
            $model->unbindFromAdmin();
            $this->redirect(['callcenterOrder/new']);
        } else {
            Yii::log('Ошибка при сохранении!!! Ошибки: ' . print_r($model->errors, true), CLogger::LEVEL_ERROR, 'db');
            throw new CHttpException(500, 'Ошибка при сохранении!!!');
        }
    }

    public function actionTask()
    {
        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()
            ->statusIs(CallcenterTaskRecord::STATUS_NEW)
            ->statusIs(CallcenterTaskRecord::STATUS_POSTPONED)
            ->statusIs(CallcenterTaskRecord::STATUS_REPEAT)
            ->processingNotActive()
            ->find(['order' => 'is_system ASC']);

        if ($model === null) {
            $this->render('taskEmptyData');
            $this->app()->end();
        }

        $this->commitView($model, $this->t('Task history'));
        $this->redirect(['callcenterOrder/processingTask', 'id' => $model->id]);
    }

    public function actionWaitingTask()
    {
        $filterOrderId = $this->app()->request->getParam('filterOrderId');
        $filterOrderClientName = $this->app()->request->getParam('filterOrderClientName');
        $filterOrderClientSurname = $this->app()->request->getParam('filterOrderClientSurname');

        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()
            ->statusIs(CallcenterTaskRecord::STATUS_POSTPONED, false)
            ->processingNotActive();

        if ($filterOrderId) {
            $model->orderId($filterOrderId);
        }

        if ($filterOrderClientName) {
            $model->orderNameSearch($filterOrderClientName);
        }

        if ($filterOrderClientSurname) {
            $model->orderSurnameSearch($filterOrderClientSurname);
        }

        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->commitView(null, $this->t('View the task queue'));
        $this->render('taskWaiting', ['provider' => $provider]);
    }

    public function actionProcessingTask($id)
    {
        $status = $this->app()->request->getPost('status');
        $status = Cast::toUInt($status);
        $nextTimeCallString = $this->app()->request->getPost('nextTimeCall', '');
        $nextTimeCall = strtotime("today $nextTimeCallString");

        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()->findByPk($id);
        $user = $this->app()->user;

        if ($model === null) {
            throw new CHttpException('Task not found');
        }

        if (!$model->bindToAdmin($user->id)) {
            $this->render('taskProcessIsBusy', ['data' => $model]);
            $this->app()->end();
        }

        $data = $this->app()->request->getPost('CallcenterTaskRecord');

        if ($data) {
            if ($status == CallcenterTaskRecord::STATUS_POSTPONED) {
                $model->next_task_at = $nextTimeCall;
            } else {
                $model->scenario = 'callcenter';
            }

            $model->setAttributes($data);
            $model->status = $status;

            if ($model->save()) {
                $model->unbindFromAdmin();
                $this->redirect(['callcenterOrder/task']);
            }
        }

        $orderProvider = OrderRecord::model()->idIn([$model->order_id])->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $taskProvider = new CArrayDataProvider([]);

        if ($model->order_id > 0) {
            $taskProvider = CallcenterTaskRecord::model()
                ->operationId($model->operation_id)
                ->orderId($model->order_id)
                ->notIds([$model->id])
                ->getDataProvider(false, [
                    'pagination' => [
                        'pageSize' => 50,
                    ],
                ]);
        }

        $this->render('task', ['model' => $model, 'orderProvider' => $orderProvider, 'taskProvider' => $taskProvider]);
    }

    /**
     * @throws CHttpException
     */
    public function actionProductEditable()
    {
        $name = $this->app()->request->getPost('name');
        $value = $this->app()->request->getPost('value');
        $pk = $this->app()->request->getPost('pk');

        if ($name && $value && $pk) {
            $pk = Cast::toUInt($pk);

            $model = OrderProductRecord::model()->findByPk($pk);

            if ($model === null) {
                throw new CHttpException(404, $this->t('Data not found'));
            } elseif (!$model->hasAttribute($name)) {
                throw new CHttpException(500, 'Не найден атрибут для сохранения!!!');
            }

            $model->{$name} = Utf8::trim($value);

            if (!$model->save()) {
                $error = $model->getError($name);
                throw new CHttpException(500, $error);
            }
        } else {
            throw new CHttpException(500, 'Нет данных для сохранения!!!');
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionOrderProductReplacementSize()
    {
        $eventReplacementId = $this->app()->request->getPost('value');
        $eventReplacementId = Cast::toUInt($eventReplacementId);
        $id = $this->app()->request->getPost('pk');

        /** @var OrderProductRecord $model */
        $model = OrderProductRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('The item in need of replacement not found'));
        }

        /** @var  EventProductRecord $eventReplacement */
        $eventReplacement = EventProductRecord::model()->findByPk($eventReplacementId);

        if ($eventReplacement === null) {
            throw new CHttpException('404', $this->t('No replacement product found'));
        } elseif ($model->storekeeper_status != OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED) {
            throw new CHttpException('500', $this->t('Not possible! Warehouse status: ') . $model->storekeeperStatusReplacement);
        }

        foreach ($model->order->positions as $position) {
            /** @var $position OrderProductRecord */
            if ($position->product_id == $eventReplacementId) {
                throw new CHttpException('500', $this->t('This product already exists in the shopping cart'));
            }
        }

        $model->product_id = $eventReplacement->id;
        $model->event_id = $eventReplacement->event_id;
        $model->price = $eventReplacement->price;
        $model->is_added_by_callcenter = true;
        $model->callcenter_status = OrderProductRecord::CALLCENTER_STATUS_UNMODERATED;
        $model->storekeeper_status = OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED;

        if ($model->product->event->isTimeActive && $model->product->getIsPossibleToBuy() == 0) {
            throw new CHttpException('500', $this->t('There is no available quantity to replace in flash-sale'));
        } elseif (!$model->product->event->isTimeActive && $model->product->getStockNumber(false) == 0) {
            throw new CHttpException('500', $this->t('There is no available quantity to replace'));
        }

        $model->getRelated('product', true);
        $model->getRelated('event', true);

        if (!$model->save()) {
            throw new CHttpException('500', $this->t('Error while replacing the item'));
        }
    }

    /**
     * @param int $id
     *
     * @throws \CException
     */
    public function actionAjaxCopyDelivery($id)
    {
        $result = new Reply();
        $orderAvailableStatusesToUpdate = [
            OrderRecord::PROCESSING_UNMODERATED,
            OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
            OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
        ];

        $order = OrderRecord::model()->findByPk($id);
        if ($order === null) {
            $result->addError($this->t('Order') . " №$id " . $this->t('not found'));
            $result->setState(false);

            $this->renderJson($result->getResponse());
        }

        $relatedOrders = OrderRecord::model()
            ->uuidIs($order->uuid)
            ->idInNot([$order->id])
            ->findAll();

        if (empty($relatedOrders)) {
            $result->addError($this->t('No linked orders'));
            $result->setState(false);
        }

        $data = $this->app()->request->getPost('OrderRecord');
        if (empty($data)) {
            $result->addError($this->t('No data to process'));
            $result->setState(false);
        }

        $deliveryType = isset($data['delivery_type']) ? $data['delivery_type'] : '';
        $deliveryAttributes = isset($data['deliveryAttributes']) ? $data['deliveryAttributes'] : [];

        if (empty($deliveryType) || empty($deliveryAttributes)) {
            $error = '';
            if (empty($deliveryAttributes)) {
                $error = $this->t('Enter delivery address or branch');
            } elseif (empty($deliveryType)) {
                $error = $this->t('Enter delivery type');
            }

            $result->addError($this->t('Insufficient data to fill out') . '. ' . $error);
            $result->setState(false);
        }

        foreach ($relatedOrders as $relatedOrder) {
            if (!in_array($relatedOrder->processing_status, $orderAvailableStatusesToUpdate)) {
                $result->addError($this->t("Order No.") . $relatedOrder->id . ' ' . $this->t("disabled for updates"));
                $result->setState(false);
            } elseif ($relatedOrder->isProcessingActive() && $relatedOrder->admin_id != $this->app()->user->id) {
                $result->addError($this->t("Order No.") . $relatedOrder->id . ' ' . $this->t("is being processed by another operator. Try again shortly."));
                $result->setState(false);
            }
        }

        if (!$result->hasErrors()) {
            foreach ($relatedOrders as $relatedOrder) {
                $relatedOrder->delivery_type = $deliveryType;
                $relatedOrder->setDeliveryAttributes($deliveryAttributes);
            }
        }

        $resultSave = UnShardedActiveRecord::saveMany($relatedOrders);
        if (!$resultSave) {
            $errors = [];
            /** @var MoneyControlRecord[] $masControls */
            foreach ($relatedOrders as $relatedOrder) {
                if ($relatedOrder->hasErrors()) {
                    $errors[] = "Ошибки заказ №{$relatedOrder->id}";
                    $errors[] = $relatedOrder->errors;
                }
            }

            $result->addError("Ошибка при обновлении данных. Ошика(и): " . print_r($errors, true));
            $result->setState(false);
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @param $id
     */
    public function actionAjaxGeneratePaymentLink($id)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getEntityManager();
        /** @var Order $order */
        $order = $entityManager
            ->getRepository(Order::class)
            ->find($id);

        $order->setPaymentToken(Order::generatePaymentToken());
        $order->setPaymentTokenExpiresAt(Order::generatePaymentTokenExpiresAt());
        $entityManager->flush();

        $paymentLink = $this->app()->frontendUrlManager->createAbsoluteUrl('pay/index', ['uid' => $id, 'token' => $order->getPaymentToken()]);
        $this->renderJson(['payment_link' => $paymentLink]);
    }

    /**
     * обрабатывает AJAX запрос на продление редактирования заказа
     *
     * @param int $id
     */
    public function actionProcessingActiveUpdate($id)
    {
        $id = Cast::toUInt($id);
        $result = new Reply();
        $admin = $this->app()->user;

        /** @var OrderRecord $model */
        $model = OrderRecord::model()->findByPk($id);

        if ($model === null) {
            $result->addError($this->t('No data to process'));
            $result->setState(false);
        }

        if ($model && $model->bindToAdmin($admin->id)) {
            $this->commitView($model, $this->t('Time extended for the current order'));
            $result->addInfo($this->t('The time for the current order has been extended'));

            //продлить еще необработанные заказы этого пользователя
            /** @var OrderRecord[] $ordersToBind */
            $ordersToBind = OrderRecord::model()
                ->processingStatus(OrderRecord::PROCESSING_UNMODERATED)
                ->processingStatus(OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING, false)
                ->processingStatus(OrderRecord::PROCESSING_CALLCENTER_CALL_LATER, false)
                ->notId($model->id)
                ->userId($model->user_id)
                ->adminId($admin->id)
                ->findAll();

            foreach ($ordersToBind as $orderToBind) {
                if (!$orderToBind->bindToAdmin($admin->id)) {
                    $result->addError($this->t('Failed to extend order editing time') . ' №' . $orderToBind->id);
                } else {
                    $result->addInfo($this->t('The time is extended for the order') . ' №' . $orderToBind->id);
                }
            }
        } else {
            $this->commitView(null, $this->t('Failed to extend order editing time') . '. ' . $this->t('Order') . ' №' . $id);
            $result->addError($this->t('Failed to extend edit time'));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * обрабатывает AJAX запрос на продление редактирования задачи
     *
     * @param $id
     */
    public function actionProcessingTaskActiveUpdate($id)
    {
        $id = Cast::toUInt($id);
        $result = new Reply();
        $admin = $this->app()->user;

        /** @var OrderRecord $model */
        $model = CallcenterTaskRecord::model()->findByPk($id);

        if ($model === null) {
            $result->addError($this->t('No data to process'));
            $result->setState(false);
        }

        if ($model->bindToAdmin($admin->id)) {
            $result->addInfo($this->t('The time for the current order has been extended'));
        } else {
            $result->addError($this->t('Failed to extend edit time'));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * для поиска и добавление товара
     *
     * @param string $name
     * @param integer $page
     */
    public function actionAjaxSearchEventProduct($name, $page)
    {
        $result = new Select2Reply();

        $eventProvider = EventProductRecord::model()->nameLike($name)->idLike($name)->getDataProvider(false, [
            'pagination' => [
                'currentPage' => intval($page) - 1,
                'pageSize' => 10,
            ],
        ]);

        /** @var EventProductRecord[] $events */
        $events = $eventProvider->getData();
        $result->pageCount = $eventProvider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'name', 'category', 'price', 'size'];

        foreach ($events as $event) {
            $result->addItem(ArrayUtils::valuesByKeys($event, $allowedAttributes, true) + [
                    'logoUrl' => $event->product->logo->getThumbnailByWidth(100)->url,
                    'sizeformat' => $event->sizeformatReplacement === false ? '' : $event->sizeformatReplacement,
                ]);
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * для фильтра по пользователям (дубляж для callCenter)
     *
     * @param string $like
     * @param $page для Select2
     */
    public function actionAjaxSearchUser($like = '', $page)
    {
        /** @var UserRecord $section */
        $section = UserRecord::model();
        $result = new Select2Reply();

        if (!empty($like)) {
            $section->nameLike($like)
                ->surnameLike($like)
                ->emailLike($like);
        }

        $usersProvider = $section->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 10,
                'currentPage' => intval($page) - 1,
            ],
        ]);

        /** @var UserRecord[] $users */
        $users = $usersProvider->getData();
        $result->pageCount = $usersProvider->getPagination()->pageCount;

        $result->addItems(ArrayUtils::getColumns($users, [
            'id', 'name', 'surname',
            'email', 'telephone',
        ], 'id'));

        $this->renderJson($result->getResponse());
    }

    /**
     * проверка новых заказов
     */
    public function actionAjaxCheckNewOrders()
    {
        $result = new Reply();

        $model = OrderRecord::model()
            ->orderNew()
            ->processingNotActive()
            ->find();

        if ($model === null) {
            $result->addError($this->t('No new orders'));
        } else {
            $result->addInfo($this->t('New order for processing'));
        }

        if ($model) {
            $this->commitView($model, $this->t('Notify call-center on a new order'));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @param OrderProduct[] $orderProducts
     */
    private function makeStockFromOrder(array $orderProducts)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getEntityManager();

        /** @var WarehouseProductRepository $warehouseRepository */
        $warehouseRepository = $entityManager->getRepository(WarehouseProduct::class);

        /** @var OrderProduct $orderProduct */
        foreach ($orderProducts as $orderProduct) {
            $eventProductId = $orderProduct->getProductId();

            $warehouseProduct = $warehouseRepository->findLatestReservedProduct($eventProductId);
            $warehouseProductStatus = $warehouseProduct ? $warehouseProduct->getStatus() : null;

            if ($warehouseProductStatus !== WarehouseProductRecord::STATUS_WAREHOUSE_STOCK_RESERVED &&
                $warehouseProductStatus !== WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED
            ) {
                return;
            }

            $warehouseProduct->setStatus(WarehouseProductRecord::STATUS_WAREHOUSE_STOCK);

            $entityManager->flush();
        }
    }
}
