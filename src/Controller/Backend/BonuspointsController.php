<?php

namespace MommyCom\Controller\Backend;

use CArrayDataProvider;
use CHttpException;
use MommyCom\Model\Backend\BonuspointsByOrderForm;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use TbForm;

class BonuspointsController extends BackController
{
    const CUSTOM_ADD_BONUS_LIMIT = 100;

    /**
     * @property-description Просмотр бонусов
     */
    public function actionIndex()
    {
        $selector = UserBonuspointsRecord::model();

        $createdAtRangeString = $this->app()->request->getParam('createdAtRange', false);

        if (!empty($createdAtRangeString)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $createdAtRangeString);

            $selector->createdAtGreater(strtotime('today ' . $createdAtGreater));
            $selector->createdAtLower(strtotime('tomorrow ' . $createdAtLower));
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserBonuspointsRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Просмотр баланса пользователя
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionUserDetail($id)
    {
        $user = UserRecord::model()->findByPk($id);

        if ($user === null) {
            throw new CHttpException(404, 'User not found');
        }

        $bonusPoints = (new ShopBonusPoints())->init($user);

        $provider = new CArrayDataProvider($bonusPoints->getBonusPoints(), [
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        $this->render('userDetail', [
            'bonusPoints' => $bonusPoints,
            'provider' => $provider,
            'user' => $user,
        ]);
    }

    /**
     * @property-description Добавление бонуса
     */
    public function actionAdd()
    {
        $countries = $this->app()->countries->getCurrency();
        $model = new UserBonuspointsRecord();

        $form = TbForm::createForm('widgets.form.config.bonuspointsAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        /* @var TbForm $form */

        $form->setModel($model);
        if ($form->submitted('submit') && $form->validate()) {
            $model->is_custom = true;
            $model->admin_id = $this->app()->user->id;

            //LIMIT
            if ($model->points > self::CUSTOM_ADD_BONUS_LIMIT) {
                $model->addError('points', $this->t('Limit for adding bonus without choosing order') . ': ' . self::CUSTOM_ADD_BONUS_LIMIT . $countries->getName('short'));
            }

            if (!$model->hasErrors() && $model->save()) {
                $this->redirect(['bonuspoints/index']);
            }
        }

        $this->commitView(null, $this->t('Add bonus'));
        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @property-description Добавление бонуса по заказу
     */
    public function actionAddByOrder()
    {
        $model = new BonuspointsByOrderForm();

        $form = TbForm::createForm('widgets.form.config.bonuspointsByOrderAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        /* @var TbForm $form */

        $form->setModel($model);
        if ($form->submitted('submit') && $form->validate()) {
            $order = OrderRecord::model()->findByPk($model->orderId);

            //addition control
            $orderWaitRefundPayment = OrderBankRefundRecord::model()->orderId($order->id)->find();
            if ($orderWaitRefundPayment && $model->points > 0) {
                $model->addError('orderId', $this->t("For order No.") . $order->id . ' ' . $this->t("payment transfer No.") . $orderWaitRefundPayment->id . ' ' . $this->t("already made (paid by card)"));
            }

            $orderReturn = OrderReturnRecord::model()
                ->orderId($order->id)
                ->refundType(OrderReturnRecord::REFUND_TYPE_BONUSPOINTS)
                ->priceRefundFrom(1)
                ->find();

            if ($orderReturn && $model->points > 0) {
                $model->addError('orderId', $this->t("For order No.") . $order->id . ' ' . $this->t("refund payment No.") .
                    $orderReturn->id . $this->t("already made"));
            }

            $record = new UserBonuspointsRecord();
            $record->user_id = $order->user_id;
            $record->points = $model->points;
            $record->custom_message = $model->customMessage;
            $record->is_custom = true;
            $record->admin_id = $this->app()->user->id;
            $record->expireAtString = $model->expireAtString;

            if (!$model->hasErrors() && $record->save()) {
                $this->redirect(['bonuspoints/index']);
            }
        }

        $this->commitView(null, $this->t('Add bonus'));
        $this->render('addByOrder', [
            'form' => $form,
            'model' => $model,
        ]);
    }
}
