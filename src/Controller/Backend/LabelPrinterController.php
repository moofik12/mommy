<?php

namespace MommyCom\Controller\Backend;

use CArrayDataProvider;
use CClientScript;
use CHttpException;
use Image_Barcode2;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryDirectory;
use MommyCom\Service\Money\CurrencyFormatter;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;
use MommyCom\YiiComponent\Type\Cast;

class LabelPrinterController extends Controller
{
    use ApplicationTrait;

    /**
     * @var string
     */
    public $layout = '//layouts/printable';

    public function actionWarehouseProducts(array $id)
    {
        $provider = new CArrayDataProvider(
            WarehouseProductRecord::model()
                ->idIn($id, 1000)
                ->with('user', 'product', 'eventProduct', 'event')
                ->findAll(),
            [
                'pagination' => false,
            ]
        );

        $this->render('warehouseProducts', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param string $id
     *
     * @throws CHttpException
     */
    public function actionFulfilment($id)
    {
        /* @var $model OrderRecord */
        $model = OrderRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if (!$availableDeliveries->hasDelivery($model->delivery_type)) {
            throw new CHttpException(401, $this->t('Invalid delivery type'));
        }

        $delivery = $availableDeliveries->getDelivery($model->delivery_type);

        if (!$model->trackcode) {
            $model->trackcode = $delivery->getApi()->getTrackingCode($model);
            $model->save();

            $tracking = $model->tracking ?? new OrderTrackingRecord();
            $tracking->order_id = $model->id;
            $tracking->is_drop_shipping = $model->is_drop_shipping;
            $tracking->supplier_id = $model->supplier_id;
            $tracking->order_delivery_type = $model->delivery_type;
            $tracking->trackcode = $model->trackcode;
            $tracking->delivery_price = $delivery->getApi()->getDeliveryPrice($model);
            $tracking->deadline_for_delivery = $delivery->getApi()->getDeadlineForDelivery($model)->getTimestamp();
            $tracking->status = OrderTrackingRecord::STATUS_UNKNOWN;
            $tracking->save();
        }

        $this->redirect($delivery->getApi()->getLabelUrl($model));
    }

    public function actionAddress($id)
    {
        /* @var $model OrderRecord */
        $model = OrderRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $viewFiles = [
            DeliveryDirectory::DUMMY => 'addressDummy',
        ];

        $this->render($viewFiles[$model->delivery_type], [
            'model' => $model,
        ]);
    }

    public function actionOrderInvoice($id)
    {
        $model = OrderRecord::model()->findByPk($id);

        /* @var $model OrderRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $this->render('orderInvoice', [
            'model' => $model,
        ]);
    }

    public function actionOrderAffiliatePromocodes($id)
    {
        $model = OrderRecord::model()->findByPk($id);

        /* @var $model OrderRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /** @var CClientScript $cs */
        $cs = \Yii::app()->clientScript;
        $cs->reset();
        $cs->registerPackage('coupon');
        $cs->registerPackage('labels');

        $this->render('orderAffiliatePromocodes', [
            'model' => $model,
        ]);
    }

    public function actionBarcode($id, $height = 60, $width = 1, $renderText = true)
    {
        $id = Cast::toUInt($id);
        $height = Cast::toUInt($height, 60, 20, 500);
        $width = Cast::toFloat($width, 1, 0, 10);
        $renderText = Cast::toBool($renderText);

        Image_Barcode2::draw($id, Image_Barcode2::BARCODE_CODE128, Image_Barcode2::IMAGE_PNG, $renderText, $height, $width);
        \Yii::app()->end();
    }

    /**
     * @property-description Печать акта возврата поставщику
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionWarehouseStatementReturn($id)
    {
        /* @var $model WarehouseStatementReturnRecord */
        $model = WarehouseStatementReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == WarehouseStatementReturnRecord::STATUS_UNCONFIRMED) {
            throw new CHttpException(404, $this->t('Act is unavailable'));
        }

        $this->render('warehouseStatementReturn', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @throws \CException
     */
    public function actionSupplierInvoice($id)
    {
        /** @var AdminUserRecord $user */
        $user = $this->app()->user->model;

        /* @var EventRecord $event */
        $event = EventRecord::model()->findByPk($id);
        $supplierId = !is_null($user->supplier) ? $user->supplier->supplier_id : 0;

        if ($supplierId !== $event->supplier_id && !$user->is_root) {
            throw new \CHttpException('Access denied');
        }

        $products = EventProductRecord::model()
            ->eventId($event->id)
            ->orderBy('id', 'asc')
            ->with('product', 'brand', 'event');

        /**
         * @var CurrencyFormatter $currencyFormatter
         */
        $currencyFormatter = $this->container->get(CurrencyFormatter::class);

        $logoUrl = (new FileStorageThumbnail($event->supplier->logo, []))->getUrl();

        $this->render('supplierInvoice', [
            'products' => $products->findAll(),
            'model' => $event,
            'logoUrl' => $logoUrl,
            'currencyFormatter' => $currencyFormatter,
            'translator' => $this->container->get('translator.backend'),
        ]);
    }
}
