<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\UserGlobalLogger\UserGlobalLoggerRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Utf8;

class UserGlobalLoggerController extends BackController
{
    public function actionIndex()
    {
        $selector = UserGlobalLoggerRecord::model();

        //customs filters
        $createdAt = $this->app()->request->getParam('createdAt', '');
        $userId = $this->app()->request->getParam('userId', '');

        if ($createdAt) {
            $createdAt = explode('-', $createdAt);
            $startTime = strtotime('today ' . Utf8::trim($createdAt[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($createdAt[1]));

            $selector->createdAtFrom($startTime)->createdAtTo($endTime);
        }

        if ($userId) {
            $selector->userId($userId);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserGlobalLoggerRecord', []),
                'scenario' => 'searchPrivileged',
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $model = UserGlobalLoggerRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('view', ['model' => $model]);
            $this->app()->end();
        }

        $this->render('view', ['model' => $model]);
    }
}
