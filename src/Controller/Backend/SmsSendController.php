<?php

namespace MommyCom\Controller\Backend;

use MommyCom\Model\Backend\SmsPhoneForm;
use MommyCom\Model\Db\SmsMessageRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Form\Form;

class SmsSendController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Sending SMS';

    public function actionIndex()
    {
        /** @var SmsPhoneForm $model */
        $model = new SmsPhoneForm();
        $userId = $this->app()->user->id;

        /** @var Form $form */
        $form = Form::createForm('widgets.form.config.smsSend', $model, [
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
        ]);

        if ($form->submitted('submit') && $form->validate()) {
            $result = $this->app()->sms->create($userId, $model->phone, $model->text, SmsMessageRecord::TYPE_CUSTOM);
            if ($result) {
                if ($model->saveText) {
                    $model->phone = false;
                } else {
                    $form->setModel(new SmsPhoneForm());
                }

                $this->app()->user->setFlash('info', 'SMS sent');
            } else {
                $this->app()->user->setFlash('error', 'Saving Error');
            }
        }

        $this->render('index', ['form' => $form]);
    }
}
