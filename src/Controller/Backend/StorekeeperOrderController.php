<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CHttpRequest;
use MommyCom\Entity\User;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\StatementOrderRecord;
use MommyCom\Model\Db\StatementRecord;
use MommyCom\Model\Db\TabletPackagingRecord;
use MommyCom\Model\Db\TabletUserRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryApiInterface;
use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\Service\Order\MommyOrder;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;

class StorekeeperOrderController extends BackController
{
    /**
     * Подтвержение на покупку товаров кладовщиком
     */
    public function actionIndex()
    {
        $request = $this->app()->request;
        /* @var $request CHttpRequest */
        // yii has bug in history
        $packagingStatusArray = Cast::toStrArr($request->getParam('packagingStatus'));
        $packagingStatus = end($packagingStatusArray);
        $eventProductIdArray = Cast::toStrArr($request->getParam('eventProductId'));
        $eventProductId = end($eventProductIdArray);
        $eventIdArray = Cast::toStrArr($request->getParam('eventId'));
        $eventId = end($eventIdArray);

        $selector = OrderRecord::model()
            ->processingStatuses([OrderRecord::PROCESSING_CALLCENTER_CONFIRMED, OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED])
            ->isDropShipping(false);
        /* @var $selector OrderRecord */

        if ($eventProductId > 0) {
            $selector->productId($eventProductId);
        }

        if ($eventId > 0) {
            $selector->eventId($eventId);
        }

        if ($packagingStatus == 'ready') {
            $selector->onlyPackagingReady();
            //$selector->orderedAtLower(time() - 86400); // у человека есть сутки на редактирование заказа
        } elseif ($packagingStatus == 'unready') {
            $selector->onlyPackagingUnready();
            $selector->orderedAtLower(time() - 86400); // у человека есть сутки на редактирование заказа
        } elseif ($packagingStatus == 'dangling') {
            $selector->onlyPackagingUnready();
            $selector->orderedAtLower(time() - strtotime('10 days', 0));
        } elseif ($packagingStatus == 'packaged') {
            $selector->onlyPackaged();
            $selector->onlyPackagingReady(); // только те которые по настоящему доступны на складе
            $selector->orderedAtLower(time() - 86400); // у человека есть сутки на редактирование заказа
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id ASC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('OrderRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $providerData = $provider->getData();
        /* @var $providerData OrderRecord */

        $tabletBinds = TabletPackagingRecord::model()->orderIdIn(ArrayUtils::getColumn($providerData, 'id'))->findAll();
        $tabletBinds = ArrayUtils::changeKeyColumn($tabletBinds, 'order_id');

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
            'tabletBinds' => $tabletBinds,

            'packagingStatus' => $packagingStatus,
            'eventProductId' => $eventProductId,
            'eventId' => $eventId,
        ]);
    }

    public function actionMailReady()
    {
        $selector = OrderRecord::model()
            ->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_PACKAGED)
            ->onlyMailReady()
            ->isDropShipping(false);

        //custom filters
        $statementId = $this->app()->request->getParam('filterStatementId', '');

        if ($statementId) {
            $statement = StatementRecord::model()->with('positions')->findByPk($statementId);
            if ($statement) {
                $ordersId = ArrayUtils::getColumn($statement->positions, 'order_id');
                $selector->idIn($ordersId);
            } else {
                $selector->idIn([]);
            }
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'defaultOrder' => 't.id ASC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('OrderRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View a list of ready-to-pack'));
        $this->render('mailReady', [
            'provider' => $provider,
        ]);
    }

    public function actionMailUnReady()
    {
        $selector = OrderRecord::model()
            ->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_PACKAGED)
            ->onlyMailUnReady()
            ->isDropShipping(false);

        $provider = $selector->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'defaultOrder' => 't.id ASC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('OrderRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View a list of products not ready for packing'));
        $this->render('mailUnReady', [
            'provider' => $provider,
        ]);
    }

    public function actionBackToPackaging($id)
    {
        $id = Cast::toUInt($id);
        $model = OrderRecord::model()->isDropShipping(false)->findByPk($id);
        /* @var $model OrderRecord */

        if ($model === null) {
            throw new CHttpException(404, 'Order not found');
        }

        if ($model->processing_status != OrderRecord::PROCESSING_STOREKEEPER_PACKAGED) {
            throw new CHttpException(500, 'Order with status' . ' "' . $model->processingStatusReplacement . '" ' . $this->t('disabled for re-packaging'));
        }

        if (StatementOrderRecord::model()->orderId($model->id)->exists()) {
            throw new CHttpException(500, 'Order re-packaging is disabled as it\'s already recorded in Delivery-Acceptance Act');
        }

        $model->processing_status = OrderRecord::PROCESSING_CALLCENTER_CONFIRMED;
        $model->save(false);
    }

    public function actionSetAsMailed($id)
    {
        $id = Cast::toUInt($id);
        $model = OrderRecord::model()->isDropShipping(false)->findByPk($id);
        /* @var $model OrderRecord */

        if ($model === null) {
            throw new CHttpException(404, 'Order not found');
        }

        if ($model->processing_status != OrderRecord::PROCESSING_STOREKEEPER_PACKAGED) {
            throw new CHttpException(500, 'Order with status' . ' "' . $model->processingStatusReplacement . '"' . 'disabled for dispatching');
        }

        if ($model->statement == null) {
            throw new CHttpException(500, 'Cannot confirm orders without Delivery-Acceptance Act');
        }

        if ($model->statement->status != StatementRecord::STATUS_CONFIRMED) {
            throw new CHttpException(500, 'Cannot confirm the order without receiving Delivery-Acceptance Act');
        }

        /*if ($model->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA && mb_strlen($model->trackcode) == 0) {
            throw new CHttpException(500, 'У кажите трекномер');
        }*/

        $model->processing_status = OrderRecord::PROCESSING_STOREKEEPER_MAILED;
        $model->save();

        $this->redirect(['storekeeperOrder/mailReady']);
    }

    public function actionMailed()
    {
        $provider = OrderRecord::model()
            ->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_MAILED)
            ->isDropShipping(false)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id ASC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View list of dispatched orders'));
        $this->render('mailed', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @throws \CDbException
     * @throws \CException
     */
    public function actionPackaging($id)
    {
        $request = $this->app()->request;
        $adminUser = $this->app()->user;

        /* @var $model OrderRecord */
        $model = OrderRecord::model()->isDropShipping(false)->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        if ($model->processing_status == OrderRecord::PROCESSING_STOREKEEPER_PACKAGED) {
            throw new CHttpException(500, $this->t('Packed'));
        }

        if ($model->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
            && $model->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED
        ) {
            throw new CHttpException(500, $this->t('Pending packaging'));
        }

        $isSameAdmin = $model->admin_id === $adminUser->id;
        $isEditAllowed = (!$model->bindToAdmin($adminUser->id)) ? $isSameAdmin : true;

        if ($model->is_drop_shipping) {
            throw new CHttpException(500, $this->t('Packing unavailable'));
        }

        $positionsProvider = OrderProductRecord::model()
            ->orderId($id)
            ->callcenterStatus(OrderProductRecord::CALLCENTER_STATUS_ACCEPTED)
            ->orderBy('event_id')
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $isFulfilmentDelivery = $this->isFulfilmentDelivery($model->delivery_type);

        $fulfilmentStatus = $isFulfilmentDelivery ? $model->getFulfilmentStatus() : MommyOrder::STATUS_FULFILMENT_INITIAL;

        if ($request->getPost('submit') !== null) {
            switch ($request->getPost('submit')) {
                case 'accept':
                    $model->processing_status = OrderRecord::PROCESSING_STOREKEEPER_PACKAGED;
                    $model->save();
                    break;
                case 'recall':
                    $model->processing_status = OrderRecord::PROCESSING_CALLCENTER_RECALL;
                    $model->save();
                    break;
                case 'send':
                    if (!$isFulfilmentDelivery) {
                        throw new CHttpException(500, $this->t('Action unavailable'));
                    }
                    if (!$model->trackcode) {
                        $model->addError('processing_status', $this->t('You must print a label first'));

                        break;
                    }
                    $model->processing_status = OrderRecord::PROCESSING_STOREKEEPER_PACKAGED;
                    if (!$model->validate()) {
                        break;
                    }
                    $this->createStatement($model);
                    $model->processing_status = OrderRecord::PROCESSING_STOREKEEPER_MAILED;

                    if ($model->save()) {
                        $this->sendTrackOrderEmail($model);
                    }

                    break;
                default:
                    throw new CHttpException(500, $this->t('Action unavailable'));
            }

            if (!$model->hasErrors()) {
                $this->redirect(['storekeeperOrder/index']);
            } else {
                $model->refresh();
            }
        }

        $this->render('packaging', [
            'model' => $model,
            'positionsProvider' => $positionsProvider,
            'isFulfilmentDelivery' => $isFulfilmentDelivery,
            'fulfilmentStatus' => $fulfilmentStatus,
            'isEditAllowed' => $isEditAllowed,
        ]);
    }

    /**
     * @param int $deliveryType
     *
     * @return bool
     */
    private function isFulfilmentDelivery(int $deliveryType): bool
    {
        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if (!$availableDeliveries->hasDelivery($deliveryType)) {
            return false;
        }

        return $availableDeliveries->getDelivery($deliveryType)->isFulfilment();
    }

    /**
     * @param OrderRecord $order
     *
     * @throws CHttpException
     * @throws \CDbException
     */
    private function createStatement(OrderRecord $order)
    {
        $statement = new StatementRecord();
        $statement->delivery_type = $order->delivery_type;

        if (!$statement->save()) {
            $errorMessage = current(current($statement->getErrors()));
            throw new CHttpException(400, 'Delivery-Acceptance Act could not be created' . ': ' . $errorMessage);
        }

        $position = new StatementOrderRecord();
        $position->statement_id = $statement->id;
        $position->order_id = $order->id;

        if (!$position->save()) {
            $statement->delete();

            $errorMessage = current(current($position->getErrors()));
            throw new CHttpException(400, 'Delivery-Acceptance Act could not be created' . ': ' . $errorMessage);
        }
    }

    /**
     * @param OrderRecord $order
     *
     * @throws CHttpException
     */
    private function confirmOrder(OrderRecord $order)
    {
        /** @var AvailableDeliveries $availableDeliveries */
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        $delivery = $availableDeliveries->getDelivery($order->delivery_type);

        try {
            $delivery->getApi()->confirmOrder($order);
        } catch (\Throwable $t) {
            throw new CHttpException(400, 'Cannot confirm order: ' . $t->getMessage());
        }
    }

    public function actionRenewAdminOrderBind($id)
    {
        $adminUser = $this->app()->user;
        $model = OrderRecord::model()->isDropShipping(false)->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        if ($model->processing_status == OrderRecord::PROCESSING_STOREKEEPER_PACKAGED) {
            throw new CHttpException(500, $this->t('Packed'));
        }

        if ($model->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
            && $model->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED
        ) {
            throw new CHttpException(500, $this->t('Pending packaging'));
        }

        if (!$model->bindToAdmin($adminUser->id)) {
            throw new CHttpException(500, $this->t('Locked for editing by') . ': ' . $adminUser->getModel()->fullname);
        }
    }

    public function actionView($id)
    {
        $model = OrderRecord::model()->findByPk($id);
        /* @var $model OrderRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        $positionsProvider = OrderProductRecord::model()
            ->orderId($id)
            ->orderBy('event_id')
            ->callcenterStatus(OrderProductRecord::CALLCENTER_STATUS_ACCEPTED)
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $this->commitView($model,
            $this->t('View order'));
        $this->render('view', [
            'model' => $model,
            'positionsProvider' => $positionsProvider,
        ]);
    }

    public function actionAjaxSwitchProductWarehouse($id, $warehouseId)
    {
        $model = EventProductRecord::model()->findByPk($id);
        $warehouseProduct = WarehouseProductRecord::model()->findByPk($warehouseId);

        if ($model === null) {
            throw new CHttpException(404, $this->t('No record'));
        }

        if ($warehouseProduct === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        $provider = WarehouseProductRecord::model()
            ->productId($model->product_id)
            ->eventProductSizeformat($model->sizeformat)
            ->eventProductSize($model->size)
            ->onlyNotHasOrderConnected()
            ->status(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED)
            ->limit(20)
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $viewData = [
            'model' => $model,
            'warehouseProduct' => $warehouseProduct,
            'provider' => $provider,
        ];

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('ajaxSwitchProductWarehouse', $viewData);
        } else {
            $this->render('ajaxSwitchProductWarehouse', $viewData);
        }
    }

    public function actionSwitchProductWarehouse($id, $switchId)
    {
        $model = WarehouseProductRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        $orderProduct = $model->orderProduct;

        if ($model->order === null || $orderProduct === null) {
            throw new CHttpException(500, $this->t('No match for order'));
        }

        if ($model->order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
            && $model->order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED
        ) {
            throw new CHttpException(500, $this->t('Pending editing'));
        }

        $switchModel = WarehouseProductRecord::model()
            ->idIn([$switchId])
            ->productId($orderProduct->catalog_product_id)
            ->eventProductSizeformat($orderProduct->product_sizeformat)
            ->eventProductSize($orderProduct->product_size)
            ->onlyNotHasOrderConnected()
            ->status(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED)
            ->find();
        /* @var $switchModel WarehouseProductRecord */

        if ($switchModel === null) {
            throw new CHttpException(404, $this->t('Switch unavailable'));
        }

        $product = $model->orderProduct;

        $model->unbindProduct($model->orderProduct);
        $model->save();

        $switchModel->bindProduct($product);
        $switchModel->save();

        $product->storekeeper_status = OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED;
        $product->save();
    }

    public function actionTabletBind($id)
    {
        $model = OrderRecord::model()->findByPk($id);
        /* @var $model OrderRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }
        $task = TabletPackagingRecord::model()->orderId($id)->find();
        /* @var $task TabletPackagingRecord */

        $selector = TabletUserRecord::model()
            ->onlyOnline()
            ->orderBy('last_activity_at', 'desc')
            ->limit(20);

        if ($task !== null) {
            $selector->userIdNot($task->user_id); // убираем из текущей задачи текущего юзера
        }

        $users = $selector->findAll();
        /* @var $users TabletUserRecord */

        if ($users === []) {
            throw new CHttpException(500, $this->t('There are currently no active tablet users'));
        }

        // start --- поиск самого свободного юзера
        $packages = TabletPackagingRecord::model()
            ->orderProcessingStatuses([OrderRecord::PROCESSING_CALLCENTER_CONFIRMED, OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED])
            ->userIdIn(ArrayUtils::getColumn($users, 'user_id'))
            ->limit(200)
            ->findAll();

        $packagesGrouped = ArrayUtils::groupBy($packages, 'user_id');

        $freeUserId = 0;
        $freeItemCount = 0;

        foreach ($users as $user) {
            $packageUserId = $user->user_id;
            $userPackages = isset($packagesGrouped[$user->user_id]) ?
                $packagesGrouped[$user->user_id] : [];

            /* @var $userPackages TabletPackagingRecord[] */
            $itemCount = 0;
            foreach ($userPackages as $package) {
                $order = $package->order;
                if ($order === null) {
                    continue;
                }

                foreach ($order->positionsCallcenterAccepted as $position) {
                    $itemCount += $position->number;
                }
            }

            if ($freeUserId == 0 || $freeItemCount > $itemCount) {
                $freeUserId = $packageUserId;
                $freeItemCount = $itemCount;
            }
        }

        // end --- поиск самого свободного юзера

        if ($freeUserId == 0) {
            throw new CHttpException(500, $this->t('There are currently no active tablet users'));
        }

        if ($task === null) {
            $task = new TabletPackagingRecord();
        }

        $task->user_id = $freeUserId;
        $task->order_id = $model->id;
        $task->save();

        if ($task->hasErrors()) {
            $errors = $task->getErrors();
            $error = reset($errors);
            throw new CHttpException(500, reset($error));
        }
    }

    public function actionSetOrderComment()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', 0);

        $order = OrderRecord::model()->findByPk($pk);
        /* @var $order OrderRecord */

        if ($order === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        if ($order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
            && $order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED
        ) {
            throw new CHttpException(500, $this->t('Unavailable comment changing'));
        }

        $order->storekeeper_comment = $value;
        $order->save();

        if ($order->hasErrors()) {
            $errors = $order->getErrors();
            $error = reset($errors);
            throw new CHttpException(500, reset($error));
        }
    }

    public function actionSetOrderWeight()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', 0);
        $minWeight = OrderRecord::MIN_PACKAGE_WEIGHT;

        $order = OrderRecord::model()->findByPk($pk);
        /* @var $order OrderRecord */

        if ($order === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        if ($order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
            && $order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED
        ) {
            throw new CHttpException(500, $this->t('Unavailable weight changing'));
        }

        if ($value <= $minWeight) {
            throw new CHttpException(500, $this->t("Weight must be at least") . ' ' . $minWeight . 'g');
        }

        $order->weight_custom = $value;
        $order->save();

        if ($order->hasErrors()) {
            $errors = $order->getErrors();
            $error = reset($errors);
            throw new CHttpException(500, reset($error));
        }
    }

    public function actionSwitchProductStatus()
    {
        $request = $this->app()->request;

        $pk = $request->getParam('pk', 0);
        $value = $request->getParam('value', 0);

        $product = OrderProductRecord::model()->findByPk($pk);
        /* @var $product OrderProductRecord */

        if ($product === null) {
            throw new CHttpException(404, $this->t('No record'));
        }

        if ($product->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_ACCEPTED) {
            throw new CHttpException(500, $this->t('Pending call center confirmation'));
        }

        if (($product->order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
                && $product->order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED) ||
            $product->order->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED
        ) {
            throw new CHttpException(500, $this->t('Unavailable status changing'));
        }

        $validStatuses = array_keys(OrderProductRecord::storekeeperStatusReplacements());

        if (!in_array($value, $validStatuses)) {
            throw new CHttpException(500, $this->t('Unsupported status'));
        }

        if ($value == OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED && !$product->isWarehouseFullyConnected) {
            throw new CHttpException(500, $this->t('Confirmation disallowed. No product.'));
        } elseif ($value == OrderProductRecord::STOREKEEPER_STATUS_UNAVAILABLE && $product->isWarehouseFullyConnected) {
            throw new CHttpException(500, $this->t('Status "Not available" disallowed. Order completed'));
        }

        $product->storekeeper_status = $value;
        $product->save();

        if ($product->hasErrors()) {
            $errors = $product->getErrors();
            $error = reset($errors);
            throw new CHttpException(500, reset($error));
        }
    }

    public function actionBindWarehouseConfirm()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', 0);

        $product = OrderProductRecord::model()->findByPk($pk);
        /* @var $product OrderProductRecord */

        if ($product === null) {
            throw new CHttpException(404, $this->t('No record'));
        }

        if ($product->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_ACCEPTED) {
            throw new CHttpException(500, $this->t('Pending call center confirmation'));
        }

        if (($product->order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
                && $product->order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED) ||
            $product->order->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED
        ) {
            throw new CHttpException(500, $this->t('Unavailable status changing'));
        }

        $verifiedNumber = WarehouseProductRecord::model()
            ->orderProductId($product->id)
            ->securityCodeChecked(true)
            ->count();
        $warehouseProducts = $product->warehouseProducts;
        $somethingConfirmed = false;

        if ($verifiedNumber < $product->number) {
            /** @var WarehouseProductRecord $warehouseProduct */
            foreach ($warehouseProducts as $warehouseProduct) {
                if ($warehouseProduct->status != WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED
                    || $warehouseProduct->is_security_code_checked
                    || null !== $warehouseProduct->order
                ) {
                    continue;
                }

                if ($warehouseProduct->compareSecurityCode($value)) {
                    $warehouseProduct->order_id = $product->order_id;
                    $warehouseProduct->order_product_id = $product->id;
                    $warehouseProduct->order_user_id = $product->user_id;
                    $warehouseProduct->is_security_code_checked = true;

                    if ($warehouseProduct->save()) {
                        $somethingConfirmed = true;
                    } else {
                        $errors = $warehouseProduct->getErrors();
                        $error = reset($errors);
                        throw new CHttpException(500, reset($error));
                    }

                    break;
                }
            }
        } else {
            throw new CHttpException(403, $this->t('Already verified'));
        }

        if (!$somethingConfirmed) {
            throw new CHttpException(404, $this->t('Verification Code not found'));
        }
    }

    public function actionUnbindWarehouse($id, $warehouseId)
    {
        $product = OrderProductRecord::model()->findByPk($id);
        $warehouseProduct = WarehouseProductRecord::model()->findByPk($warehouseId);
        /* @var $product OrderProductRecord */
        /* @var $warehouseProduct WarehouseProductRecord */

        if ($product === null) {
            throw new CHttpException(404, $this->t('No record'));
        }

        if ($warehouseProduct === null) {
            throw new CHttpException(404, $this->t('Not found'));
        }

        if (($product->order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
                && $product->order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED) ||
            $product->order->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED
        ) {
            throw new CHttpException(500, $this->t('Unavailable status changing'));
        }

        if ($product->id != $warehouseProduct->order_product_id) {
            throw new CHttpException(500, $this->t('No match for order'));
        }

        $warehouseProduct->unbindProduct($product)->save();
        if ($product->storekeeper_status == OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED) {
            $product->storekeeper_status = OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED;
            $product->save();
        }

        if ($warehouseProduct->hasErrors()) {
            $errors = $warehouseProduct->getErrors();
            $error = reset($errors);
            throw new CHttpException(500, reset($error));
        }
    }

    public function actionSetProductComment()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', '');

        $product = OrderProductRecord::model()->findByPk($pk);
        /* @var $product OrderProductRecord */

        if ($product === null) {
            throw new CHttpException(404, $this->t('No record'));
        }

        if (($product->order->processing_status != OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
                && $product->order->processing_status != OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED) ||
            $product->order->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED
        ) {
            throw new CHttpException(500, $this->t('Unavailable status changing'));
        }

        $product->storekeeper_comment = $value;
        $product->save();

        if ($product->hasErrors()) {
            $errors = $product->getErrors();
            $error = reset($errors);
            throw new CHttpException(500, reset($error));
        }
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionCreateFulfilmentOrder($id)
    {
        /** @var OrderRecord $model */
        $order = OrderRecord::model()->findByPk($id);

        $logger = $this->container->get('logger');
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if (!$availableDeliveries->hasDelivery($order->delivery_type)) {
            throw new CHttpException(401, $this->t('Invalid delivery type'));
        }

        /** @var DeliveryInterface $delivery */
        $delivery = $availableDeliveries->getDelivery($order->delivery_type);
        $trackId = $delivery->getApi()->createOrder($order);
        $this->confirmOrder($order);
        /* Логгируем track id, чтобы fulfilment-заказы можно было без труда восстановить по логам */
        $logger->info('Track ID for order No.' . $order->id . ': ' . $trackId);
        $order->track_id = $trackId;
        $order->save();
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionRefreshFulfilmentData($id)
    {
        /** @var OrderRecord $model*/
        $order = OrderRecord::model()->findByPk($id);
        $availableDeliveries = $this->container->get(AvailableDeliveries::class);

        if (!$availableDeliveries->hasDelivery($order->delivery_type)) {
            throw new CHttpException(401, $this->t('Invalid delivery type'));
        }

        /** @var DeliveryInterface $delivery */
        $delivery = $availableDeliveries->getDelivery($order->delivery_type);
        $deliveryApi = $delivery->getApi();

        if ($deliveryApi instanceof FeedrApi) {
            $details = $deliveryApi->getOrderDetails($order->id);
            $order->track_id = $details['id'];
            $order->trackcode = $details['id'];
            $order->save();
        }
    }

    /**
     * @param OrderRecord $order
     *
     * @throws \CException
     */
    private function sendTrackOrderEmail(OrderRecord $order)
    {
        /** @var DeliveryApiInterface $deliveryApi */
        $deliveryApi = $this->container
            ->get(AvailableDeliveries::class)
            ->getDelivery($order->delivery_type)
            ->getApi();
        $trackLink = $deliveryApi->getTrackUrl($order->getTrackId());

        /** @var User $user */
        $user = $order->user->getEntity();
        $senderEmail = $this->app()->params['layoutsEmails']['welcome2'];
        $senderName = $this->app()->params['distributionEmailName'];
        /** @var EmailUtils $emailUtils */
        $emailUtils = $this->container->get(EmailUtils::class);
        $emailUtils->sendTrackOrderEmail($user, $order->getId(), $trackLink, $senderEmail, $senderName);
    }
}
