<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Entity\OrderReturn;
use MommyCom\Entity\OrderReturnProduct;
use MommyCom\Model\Db\OrderReturnProductRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class OrderReturnController extends BackController
{
    public function actionIndex()
    {
        $provider = OrderReturnRecord::model()
            ->statusIn([
                OrderReturnRecord::STATUS_UNCONFIGURED,
                OrderReturnRecord::STATUS_PARTIAL_CONFIGURED,
                OrderReturnRecord::STATUS_CONFIGURED,
            ])
            ->onlyPayUnready()
            ->with('positions', 'order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderReturnRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    public function actionPayReady()
    {
        $provider = OrderReturnRecord::model()
            ->onlyPayReady()
            ->with('positions', 'order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.status_changed_at ASC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderReturnRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View the list of ready-to-pay'));
        $this->render('payReady', [
            'provider' => $provider,
        ]);
    }

    public function actionHistory()
    {
        $selector = OrderReturnRecord::model();
        $filterSupplierID = $this->app()->request->getParam('filterSupplierID');

        if ($filterSupplierID) {
            $selector->with([
                'positions' => [
                    'joinType' => 'INNER JOIN',
                    'together' => true,
                    'with' => [
                        'eventProduct' => [
                            'joinType' => 'INNER JOIN',
                            'together' => true,
                            'scopes' => [
                                'supplierId' => [$filterSupplierID],
                            ],
                        ],
                    ],
                ],
            ]);
        }

        $provider = $selector
            ->statusNotIn([OrderReturnRecord::STATUS_UNCONFIGURED, OrderReturnRecord::STATUS_PARTIAL_CONFIGURED])
            ->with('positions', 'order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderReturnRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View history'));
        $this->render('history', [
            'provider' => $provider,
        ]);
    }

    public function actionAdd()
    {
        $model = new OrderReturnRecord();

        $form = TbForm::createForm('widgets.form.config.returnAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model->is_custom = true;
            if ($model->save()) {
                $this->redirect(['orderReturn/update', 'id' => $model->id]);
            }
        }

        $this->commitView(null, $this->t('Add a return'));
        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = OrderReturnRecord::model()->findByPk($id);

        $editableStatues = [
            OrderReturnRecord::STATUS_UNCONFIGURED,
            OrderReturnRecord::STATUS_PARTIAL_CONFIGURED,
        ];

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if (!in_array($model->status, $editableStatues)) {
            throw new CHttpException(400, 'Возврат со статусом "' . $model->statusReplacement . '" не подлежит редактированию');
        }

        $form = TbForm::createForm('widgets.form.config.returnUpdate', $this, [
            'type' => 'horizontal',
        ], $model);

        $acceptForm = TbForm::createForm('widgets.form.config.returnAccept', $this, [
            'type' => 'horizontal',
        ], $model);

        /* @var $form TbForm */
        /* @var $acceptForm TbForm */

        if ($form->submitted('submit') && $form->validate()) {
            if ($model->status == OrderReturnRecord::STATUS_UNCONFIGURED) {
                $model->status = OrderReturnRecord::STATUS_PARTIAL_CONFIGURED;
            }
            $model->save();
        }

        if ($acceptForm->submitted('confirm') && $acceptForm->validate()) {
            $model->status = OrderReturnRecord::STATUS_CONFIGURED;

            if ($model->save()) {
                $this->redirect(['orderReturn/index']);
            }
        } elseif ($acceptForm->submitted('cancel')) {
            $model->status = OrderReturnRecord::STATUS_CANCELLED;

            if ($model->save()) {
                $this->redirect(['orderReturn/index']);
            }
        }

        $positionsProvider = OrderReturnProductRecord::model()
            ->returnId($id)
            ->with('return', 'event', 'eventProduct', 'warehouseProduct', 'order', 'orderProduct')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderReturnProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('Return update'));
        $this->render('update', [
            'model' => $model,
            'form' => $form,
            'acceptForm' => $acceptForm,
            'positionsProvider' => $positionsProvider,
        ]);
    }

    public function actionPay($id)
    {
        $model = OrderReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status != OrderReturnRecord::STATUS_CONFIGURED) {
            throw new CHttpException(400, 'Return status' . '"' . $model->statusReplacement . '"' . 'not suitable for refund');
        }

        if (!$model->getIsPayReady()) {
            throw new CHttpException(400, 'Return is not yet ready for refund');
        }

        $confirmForm = TbForm::createForm('widgets.form.config.returnConfirm', $this, [
            'type' => 'horizontal',
        ], $model);

        /* @var $confirmForm TbForm */

        if ($confirmForm->submitted('confirm') && $confirmForm->validate()) {
            $model->status = OrderReturnRecord::STATUS_NEED_PAY;

            if ($model->save()) {
                $this->redirect(['orderReturn/payReady']);
            }
        } elseif ($confirmForm->submitted('cancel')) {
            $model->status = OrderReturnRecord::STATUS_PARTIAL_CONFIGURED;

            if ($model->save()) {
                $this->redirect(['orderReturn/payReady']);
            }
        }

        $positionsProvider = OrderReturnProductRecord::model()
            ->returnId($id)
            ->status(OrderReturnProductRecord::STATUS_OK)
            ->with('return', 'event', 'eventProduct', 'warehouseProduct', 'order', 'orderProduct')
            ->getDataProvider(false, [
                'pagination' => false,
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderReturnProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->render('pay', [
            'model' => $model,
            'positionsProvider' => $positionsProvider,
            'confirmForm' => $confirmForm,
        ]);
    }

    public function actionView($id)
    {
        $model = OrderReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $positionsProvider = OrderReturnProductRecord::model()
            ->returnId($id)
            ->status(OrderReturnProductRecord::STATUS_OK)
            ->with('return', 'event', 'eventProduct', 'warehouseProduct', 'order', 'orderProduct')
            ->getDataProvider(false, [
                'pagination' => false,
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderReturnProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('View return'));
        $this->render('view', [
            'model' => $model,
            'positionsProvider' => $positionsProvider,
        ]);
    }

    public function actionBack($id)
    {
        $model = OrderReturnRecord::model()->findByPk($id);
        /* @var $model OrderReturnRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == OrderReturnRecord::STATUS_PARTIAL_CONFIGURED) {
            return; // уже сконфигурирован
        }

        if ($model->status != OrderReturnRecord::STATUS_CONFIGURED) {
            throw new CHttpException(400, 'Order cannot be returned for edit');
        }

        $model->status = OrderReturnRecord::STATUS_PARTIAL_CONFIGURED;
        $model->save();

        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionCancel($id)
    {
        $model = OrderReturnRecord::model()->findByPk($id);
        /* @var $model OrderReturnRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == OrderReturnRecord::STATUS_CANCELLED) {
            return; // уже отменен
        }

        if ($model->status == OrderReturnRecord::STATUS_NEED_PAY || $model->getIsPayReady()) {
            throw new CHttpException(400, 'Order cannot be canceled');
        }

        $model->status = OrderReturnRecord::STATUS_CANCELLED;
        $model->save();

        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionRestore($id)
    {
        $model = OrderReturnRecord::model()->findByPk($id);
        /* @var $model OrderReturnRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == OrderReturnProductRecord::STATUS_CANCELLED) {
            return; // уже на редактировании
        }

        if ($model->status != OrderReturnRecord::STATUS_CANCELLED) {
            throw new CHttpException(400, 'Позиция не подлежит востановлению');
        }

        $model->status = OrderReturnRecord::STATUS_UNCONFIGURED;
        $model->save();

        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionProductCancel($id)
    {
        $model = OrderReturnProductRecord::model()->findByPk($id);
        /* @var $model OrderReturnProductRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == OrderReturnProductRecord::STATUS_DELETED) {
            return; // уже отменен
        }

        if ($model->status != OrderReturnProductRecord::STATUS_OK) {
            throw new CHttpException(400, 'Order cannot be canceled');
        }

        $model->status = OrderReturnProductRecord::STATUS_DELETED;
        $model->save();

        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionProductRestore($id)
    {
        $model = OrderReturnProductRecord::model()->findByPk($id);
        /* @var $model OrderReturnProductRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == OrderReturnProductRecord::STATUS_OK) {
            return; // уже подтвержден
        }

        if ($model->status != OrderReturnProductRecord::STATUS_DELETED) {
            throw new CHttpException(400, 'Товарная позиция не подлежит востановлению');
        }

        $model->status = OrderReturnProductRecord::STATUS_OK;
        $model->save();

        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionSetParameter()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', 0);

        $entityManager = $this->getEntityManager();
        /** @var OrderReturnProduct $product */
        $product = $entityManager
            ->getRepository(OrderReturnProduct::class)
            ->findOneBy([
                'id' => $pk,
            ]);

        /* @var $product OrderReturnProductRecord */
        $editableStatuses = [
            OrderReturnRecord::STATUS_UNCONFIGURED,
            OrderReturnRecord::STATUS_PARTIAL_CONFIGURED,
        ];

        if ($product === null) {
            throw new CHttpException(404, 'Product not found');
        }

        /** @var OrderReturn $productReturn */
        $productReturn = $this->getEntityManager()
            ->getRepository(OrderReturn::class)
            ->findOneBy([
                'id' => $product->getReturnId(),
            ]);

        if (!in_array($productReturn->getStatus(), $editableStatuses)) {
            throw new CHttpException(400, 'Return is already created. Editing products not allowed');
        }

        $name = $request->getPost('name', 0);
        switch ($name) {
            case 'item_condition':
                $product->setItemCondition($value);
                break;
            case 'is_pay':
                $product->setIsPay($value);
                break;
            case 'client_comment':
                $product->setClientComment($value);
                break;
            case 'comment':
                $product->setComment($value);
                break;
            default:
                throw new CHttpException(400, 'Undefined action');
        }

        $entityManager->flush();
    }
} 
