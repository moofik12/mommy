<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Backend\MoneyControlForm;
use MommyCom\Model\Backend\MoneyControlItemForm;
use MommyCom\Model\Backend\MoneyControlItemsFromFileForm;
use MommyCom\Model\Db\MoneyControlRecord;
use MommyCom\Model\Db\MoneyReceiveStatementOrderRecord;
use MommyCom\Model\Db\MoneyReceiveStatementRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\TrackcodeParser;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use TbActiveForm;
use TbForm;

class MoneyControlController extends BackController
{
    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + validateItem']);
    }

    public function actionIndex()
    {
        $selector = MoneyReceiveStatementRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        $orderID = $this->app()->request->getParam('orderID', false);

        if ($orderID) {
            $selector->with([
                'positions' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'orderId' => $orderID,
                    ],
                ],
            ]);
        }

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('MoneyReceiveStatementRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['provider' => $provider]);
    }

    /**
     * @param int $delivery deliveryType тип доставки
     *
     * @throws CHttpException
     */
    public function actionCreate($delivery)
    {
        $delivery = Cast::toUInt($delivery);
        $modelForm = new MoneyControlForm();
        $modelForm->deliveryType = $delivery;

        $modelItem = new MoneyControlItemForm();
        $modelItem->deliveryType = $delivery;

        $modelFileForm = new MoneyControlItemsFromFileForm();
        $modelFileForm->deliveryType = $delivery;

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.moneyControl', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        $formItem = TbForm::createForm('widgets.form.config.moneyControlItem', $this, [
            'inlineErrors' => true,
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'afterValidate' => "js:function(form, data, hasError) {
                    if (hasError) {
                        return;
                    }

                    if (data) {
                        $(document).trigger('helperForm.add', [data]);
                        if (form[0].reset) {
                            form[0].reset.click();
                        } else {
                            form[0].reset();
                        }

                        return;
                    }
                }",
            ],
        ], $modelItem);

        $formFile = TbForm::createForm('widgets.form.config.moneyControlItemsFromFile', $this, [
            'inlineErrors' => true,
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'beforeValidate' => "js:function(form) {
                        var fd = new FormData();
                        var form = form[0];

                        $.each(form, function(i, el) {
                            if (el.type && el.type == 'file') {
                                fd.append(el.name, el.files[0]);
                            } else {
                                fd.append(el.name, el.value);
                            }
                        });

                        $.ajax({
                                url: form.action,
                                type: 'POST',
                                cache: false,
                                data: fd,
                                dataType: 'json',
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    var hasErrors = false;
                                    var message = {
                                        count: 0,
                                        add: 0,
                                        errors: 0,
                                        foundTrackcodes: 0,
                                        trackErrors: '',
                                    };

                                    if (data.errors) {
                                        hasErrors = true;
                                        helperForm.message(data.errors[0]);
                                    }

                                    if (data.items) {
                                        data.items.forEach(function(el) {
                                            $(document).trigger('helperForm.add', [el]);
                                        });

                                        message.count += data.items.length;
                                        message.add = data.items.length;
                                    }

                                    if (data.foundTrackcodes) {
                                        message.foundTrackcodes = data.foundTrackcodes; 
                                    }


                                    if (data.trackErrors && $.isArray(data.trackErrors)) {
                                        message.count += data.items.length;
                                        message.errors += data.trackErrors.length;
                                        message.trackErrors = data.trackErrors.join(', ');
                                    }

                                    if (!hasErrors) {
                                        helperForm.message('ТТН которые не найдены или ошибка при добавлении (' + message.errors + '): ' + message.trackErrors, false);
                                        helperForm.message('Добавлено заказов Акт: ' + message.add, false, 'info');
                                        helperForm.message('Найдено ТТН: ' + message.foundTrackcodes, false, 'info');

                                        form.reset();
                                    }
                                },
                                error: function(jqXHR) {
                                    helperForm.message('Ошибка: ' + jqXHR.responseText, true, 'error');
                                }
                        });

                        return false;
                }",
            ],
        ], $modelFileForm);

        if (($form->submitted())) {
            if ($form->validate()) {
                $moneyReceiveStatement = new MoneyReceiveStatementRecord();
                $moneyReceiveStatement->delivery_type = $modelForm->deliveryType;
                $moneyReceiveStatement->money_expected = $modelForm->moneyExpected;
                $moneyReceiveStatement->money_real = $modelForm->moneyReal;

                if (!$moneyReceiveStatement->save()) {
                    throw new CHttpException(400, $this->t('Error while creating Act'));
                }

                $moneyReceiveStatement->refresh();

                $orders = $modelForm->getOrders();
                $trackcodes = $modelForm->getItems();
                $statementsOrders = [];

                foreach ($orders as $order) {
                    $statementOrder = new MoneyReceiveStatementOrderRecord();
                    $statementOrder->statement_id = $moneyReceiveStatement->id;
                    $statementOrder->order_id = $order->id;
                    $statementOrder->order_price = $order->getPayPriceHistory();
                    $statementOrder->trackcode = isset($trackcodes[$order->id]) ? $trackcodes[$order->id] : $order->trackcode;

                    $statementsOrders[] = $statementOrder;
                }

                $result = UnShardedActiveRecord::saveMany($statementsOrders);

                if (!$result) {
                    $moneyReceiveStatement->delete();
                    throw new CHttpException(400, $this->t('Error while creating Act'));
                }

                $this->redirect(['index']);
            }
        }

        $this->commitView(null, $this->t('Creating an Act'));
        $this->render('create', ['form' => $form, 'formItem' => $formItem, 'formFile' => $formFile]);
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = MoneyReceiveStatementRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Act for editing not found'));
        } elseif (!$model->isAvailableEditing()) {
            throw new CHttpException(403, $this->t('Disable editing'));
        }

        $modelForm = new MoneyControlForm();
        $modelForm->deliveryType = $model->delivery_type;

        $modelItem = new MoneyControlItemForm();
        $modelItem->deliveryType = $model->delivery_type;

        $modelFileForm = new MoneyControlItemsFromFileForm();
        $modelFileForm->deliveryType = $model->delivery_type;

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.moneyControl', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        $formItem = TbForm::createForm('widgets.form.config.moneyControlItem', $this, [
            'inlineErrors' => true,
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'afterValidate' => "js:function(form, data, hasError) {
                    if (hasError) {
                        return;
                    }

                    if (data) {
                        $(document).trigger('helperForm.add', [data]);
                        if (form[0].reset) {
                            form[0].reset.click();
                        } else {
                            form[0].reset();
                        }

                        return;
                    }

                    console.log('Ошибка при получении результата');
                }",
            ],
        ], $modelItem);

        $formFile = TbForm::createForm('widgets.form.config.moneyControlItemsFromFile', $this, [
            'inlineErrors' => true,
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'beforeValidate' => "js:function(form) {
                        var fd = new FormData();
                        var form = form[0];

                        $.each(form, function(i, el) {
                            if (el.type && el.type == 'file') {
                                fd.append(el.name, el.files[0]);
                            } else {
                                fd.append(el.name, el.value);
                            }
                        });

                        $.ajax({
                                url: form.action,
                                type: 'POST',
                                cache: false,
                                data: fd,
                                dataType: 'json',
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    var hasErrors = false;
                                    var message = {
                                        count: 0,
                                        add: 0,
                                        errors: 0,
                                        foundTrackcodes: 0,
                                        trackErrors: '',
                                    };

                                    if (data.errors) {
                                        hasErrors = true;
                                        helperForm.message(data.errors[0]);
                                    }

                                    if (data.items) {
                                        data.items.forEach(function(el) {
                                            $(document).trigger('helperForm.add', [el]);
                                        });

                                        message.count += data.items.length;
                                        message.add = data.items.length;
                                    }
                                    
                                    if (data.foundTrackcodes) {
                                        message.foundTrackcodes = data.foundTrackcodes; 
                                    }

                                    if (data.trackErrors && $.isArray(data.trackErrors)) {
                                        message.count += data.trackErrors.length;
                                        message.errors += data.trackErrors.length;
                                        message.trackErrors = data.trackErrors.join(', ');
                                    }

                                    if (!hasErrors) {
                                        helperForm.message('ТТН которые не найдены или ошибка при добавлении (' + message.errors + '): ' + message.trackErrors, false);
                                        helperForm.message('Добавлено заказов Акт: ' + message.add, false, 'info');
                                        helperForm.message('Найдено ТТН: ' + message.foundTrackcodes, false, 'info');

                                        form.reset();
                                    }
                                },
                                error: function(jqXHR) {
                                    helperForm.message('Ошибка: ' + jqXHR.responseText, true, 'error');
                                }
                        });

                        return false;
                }",
            ],
        ], $modelFileForm);

        if (($form->submitted())) {
            if ($form->validate()) {
                $model->money_expected = $modelForm->moneyExpected;
                $model->money_real = $modelForm->moneyReal;

                if (!$model->save()) {
                    throw new CHttpException(400, $this->t('Error while creating Act'));
                }

                //очиста старых данных
                foreach ($model->positions as $position) {
                    $position->delete();
                }

                $orders = $modelForm->getOrders();
                $trackcodes = $modelForm->getItems();
                $statementsOrders = [];

                foreach ($orders as $order) {
                    $statementOrder = new MoneyReceiveStatementOrderRecord();
                    $statementOrder->statement_id = $model->id;
                    $statementOrder->order_id = $order->id;
                    $statementOrder->order_price = $order->getPayPriceHistory();
                    $statementOrder->trackcode = isset($trackcodes[$order->id]) ? $trackcodes[$order->id] : $order->trackcode;

                    $statementsOrders[] = $statementOrder;
                }

                $result = UnShardedActiveRecord::saveMany($statementsOrders);

                if (!$result) {
                    throw new CHttpException(400, $this->t('Error saving an Act'));
                }

                $this->redirect(['index']);
            }
        } else {
            $modelForm->setItems($model->positions);
            $modelForm->moneyReal = $model->money_real;
        }

        $this->commitView($model, $this->t('Update act'));
        $this->render('update', ['form' => $form, 'formItem' => $formItem, 'formFile' => $formFile]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $model = MoneyReceiveStatementRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Act for editing not found'));
        } elseif (!$model->isAvailableEditing()) {
            throw new CHttpException(403, $this->t('Disable editing'));
        }

        $model->delete();
        $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $model = MoneyReceiveStatementRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Act for editing not found'));
        }

        $selector = MoneyReceiveStatementOrderRecord::model()->statementId($model->id);
        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('MoneyReceiveStatementOrderRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->commitView($model, $this->t('View Act'));
        $this->render('view', ['model' => $model, 'provider' => $provider]);
    }

    public function actionOrders()
    {
        $selector = MoneyControlRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 20,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('MoneyControlRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('orders', ['provider' => $provider]);
    }

    /**
     * ajax validation MoneyControlItemForm
     */
    public function actionValidateItem()
    {
        $model = new MoneyControlItemForm();

        echo TbActiveForm::validate($model);

        if (!$model->hasErrors()) {
            $this->renderJson([
                'order' => $model->getOrderId(),
                'trackcode' => $model->trackcode,
                'price' => $model->price,
            ]);
        }
    }

    /**
     * ajax validation MoneyControlItemsFormFile
     */
    public function actionValidateFile()
    {
        $result = new Reply();
        $model = new MoneyControlItemsFromFileForm();
        /** @var TbForm $formFile */
        $formFile = TbForm::createForm('widgets.form.config.moneyControlItemsFromFile', $this, [], $model);

        if ($formFile->submitted()) {
            if ($formFile->validate()) {
                $parser = new TrackcodeParser();
                $parser->parse($model->getTextFile());

                $trackcodes = $parser->getTrackcodes();
                $items = [];
                $errorTrackcodes = [];

                foreach ($trackcodes as $trackcode) {
                    $itemForm = new MoneyControlItemForm();
                    $itemForm->trackcode = $trackcode;
                    $itemForm->deliveryType = $model->deliveryType;

                    if ($itemForm->validate()) {
                        $items[$trackcode] = [
                            'order' => $itemForm->getOrderId(),
                            'trackcode' => $itemForm->trackcode,
                            'price' => $itemForm->price,
                        ];
                    } else {
                        $errorsString = array_reduce($itemForm->getErrors(), function ($joinString, $attributeErrors) {
                            return $joinString . implode(' ', $attributeErrors);
                        }, '');

                        $errorTrackcodes[] = '<br>' . $trackcode . ': ' . $errorsString;
                    }
                }

                $result->add('items', array_values($items));
                $result->add('trackErrors', $errorTrackcodes);
                $result->add('foundTrackcodes', count($trackcodes));
            } else {
                $errors = $model->getErrors();
                foreach ($errors as $error) {
                    if (isset($error[0])) {
                        $result->addError($error[0]);
                    }
                }
            }
        }

        $this->renderJson($result->getResponse());
    }
}
