<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\AdminRoleRecord;
use MommyCom\Model\Db\AdminSuppliers;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\Type\Cast;
use TbActiveForm;

class AdminController extends BackController
{
    /**
     * Просмотр списка администраторов
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = AdminUserRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('AdminUserRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * Добавление администратора
     *
     * @throws \CHttpException
     * @throws \CException
     */
    public function actionAdd()
    {
        $model = new AdminUserRecord();

        $rolesIds = $this->app()->request->getPost('rolesIds', []);
        $supplierId = (int)$this->app()->request->getPost('supplierId');

        $this->checkRoleErrors($model, $rolesIds);
        $this->checkSupplierBinding($model, $supplierId);

        if ($this->app()->request->isAjaxRequest && $_POST['ajax'] === 'add-admin-form') {
            echo TbActiveForm::validate($model);
            $this->app()->end();
        }

        if ($this->app()->request->isPostRequest) {
            $model->attributes = $this->app()->request->getPost('AdminUserRecord');
            $roles = AdminRoleRecord::model()->idIn($rolesIds)->findAll();

            $transaction = $model->getDbConnection()->beginTransaction();
            try {
                if ($model->save()) {
                    $model->refresh();
                    $model->assignmentsRoles($roles);
                }

                if ($supplierId > 0) {
                    $supplierUser = AdminSuppliers::model();
                    $supplierUser->supplier_id = $supplierId;
                    $supplierUser->admin_id = $model->id;
                    $supplierUser->save();
                }
            } catch (\Exception $exception) {
                $transaction->rollback();
                throw new \CHttpException('An error occurred. Please, try again.');
            }
            $transaction->commit();

            $this->redirect(['index']);
        }

        $roles = AdminRoleRecord::model()->findAll();
        $rolesData = ArrayUtils::getColumn($roles, 'description', 'id');
        $suppliesRecords = SupplierRecord::model()->findAll(['select' => ['id', 'name']]);
        $suppliersData = ArrayUtils::getColumn($suppliesRecords, 'name', 'id');

        $this->render('add', [
            'rolesData' => $rolesData,
            'model' => $model,
            'suppliersData' => $suppliersData,
        ]);
    }

    /**
     * Удаление администратора
     *
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $id = intval($id);
        if ($id === 0 || is_null($model = AdminUserRecord::model()->findByPk($id))) {
            throw new CHttpException('404');
        }
        $model->status = AdminUserRecord::STATUS_HIDE;
        $model->save();

        if (!$this->app()->getRequest()->isAjaxRequest) {
            $this->redirect($this->app()->request->urlReferrer);
        }
    }

    /**
     * Блокирование администратора
     *
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionBan($id)
    {
        $id = intval($id);
        if ($id === 0 || is_null($model = AdminUserRecord::model()->findByPk($id))) {
            throw new CHttpException('404');
        }
        $model->status = AdminUserRecord::STATUS_BAN;
        $model->save();

        if (!$this->app()->getRequest()->isAjaxRequest) {
            $this->redirect($this->app()->request->urlReferrer);
        }
    }

    /**
     * Редактирование администратора
     *
     * @param $id
     * @throws CHttpException
     * @throws \CDbException
     */
    public function actionUpdate($id)
    {
        $id = intval($id);
        if ($id === 0 || is_null($model = AdminUserRecord::model()->findByPk($id))) {
            throw new \CHttpException('404');
        }

        $wasSupplier = (bool)$model->is_supplier;
        $rolesIds = $this->app()->request->getPost('rolesIds', []);
        $oldSupplierId = $model->supplier ? $model->supplier->supplier_id : 0;
        $supplierId = (int)$this->app()->request->getPost('supplierId');

        $this->checkRoleErrors($model, $rolesIds);
        $this->checkSupplierBinding($model, $supplierId);

        if ($this->app()->request->isPostRequest && !!$data = $this->app()->request->getPost('AdminUserRecord')) {
            $rolesIds = $this->app()->request->getPost('rolesIds', []);
            $roles = AdminRoleRecord::model()->idIn($rolesIds)->findAll();
            $model->setAttributes($data);

            $supplierUser = AdminSuppliers::model()->find('supplier_id = :supplier_id', [
                ':supplier_id' => $oldSupplierId,
            ]);

            if (!$wasSupplier && $model->is_supplier) {
                $model->is_supplier = true;
                $supplierUser = AdminSuppliers::model();
                $supplierUser->supplier_id = $supplierId;
                $supplierUser->admin_id = $model->id;
                $supplierUser->save();
            } elseif ($wasSupplier && $model->is_supplier) {
                $supplierUser->supplier_id = $supplierId;
                $supplierUser->save();
            } elseif ($wasSupplier && !$model->is_supplier) {
                $model->is_supplier = false;
                $supplierUser->delete();
            }

            if ($model->save()) {
                $model->assignmentsRoles($roles);
                $this->redirect(['index']);
            }
        }

        $roles = AdminRoleRecord::model()->findAll();
        $rolesData = ArrayUtils::getColumn($roles, 'description', 'id');
        $rolesHas = ArrayUtils::getColumn($model->roles, 'id');

        $suppliesRecords = SupplierRecord::model()->findAll(['select' => ['id', 'name']]);
        $suppliersData = ArrayUtils::getColumn($suppliesRecords, 'name', 'id');
        $supplierId = $model->supplier ? $model->supplier->supplier_id : 0;

        $this->render('update', [
            'rolesData' => $rolesData,
            'rolesHas' => $rolesHas,
            'model' => $model,
            'suppliersData' => $suppliersData,
            'supplierId' => $supplierId,
        ]);
    }

    /**
     * Поиск бренд-менеджеров
     *
     * @param string $name
     * @param int $page
     */
    public function actionAjaxBrandManager($name, $page)
    {
        $result = new Select2Reply();
        $rolesBrandManager = AdminRoleRecord::model()->name('brandmanager')->findColumnDistinct('id');

        $usersProvider = AdminUserRecord::model()
            ->with([
                'assignments' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'roleIn' => [$rolesBrandManager],
                    ],
                ],
            ])
            ->loginOrFullnameLike($name)
            ->statusIs(AdminUserRecord::STATUS_ACTIVE)
            ->getDataProvider(false, [
                    'pagination' => [
                        'currentPage' => intval($page) - 1,
                    ],
                ]
            );

        /** @var AdminUserRecord[] $users */
        $users = $usersProvider->getData();
        $result->pageCount = $usersProvider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'login', 'fullname', 'email'];

        foreach ($users as $city) {
            $result->addItem(ArrayUtils::valuesByKeys($city, $allowedAttributes, true));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * Поиск бренд-менеджера
     *
     * @param int $id
     */
    public function actionAjaxBrandManagerById($id)
    {
        $result = new Select2Reply();
        $rolesBrandManager = AdminRoleRecord::model()->name('brandmanager')->findColumnDistinct('id');

        $user = AdminUserRecord::model()
            ->with([
                'assignments' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'roleIn' => [$rolesBrandManager],
                    ],
                ],
            ])
            ->idIn([$id])
            ->statusIs(AdminUserRecord::STATUS_ACTIVE)
            ->find();

        if ($user) {
            $allowedAttributes = ['id', 'login', 'fullname', 'email'];
            $result->addItem(ArrayUtils::valuesByKeys($user, $allowedAttributes, true));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * Поиск администраторов
     *
     * @param string $like
     * @param int $page
     */
    public function actionAjaxSearch($like, $page)
    {
        /** @var UserRecord $section */
        $result = new Select2Reply();
        $selector = AdminUserRecord::model()
            ->loginOrFullnameLike($like);

        $usersProvider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 10,
                'currentPage' => intval($page) - 1,
            ],
        ]);

        /** @var UserRecord[] $users */
        $users = $usersProvider->getData();
        $result->pageCount = $usersProvider->getPagination()->pageCount;

        $items = ArrayUtils::getColumns($users, [
            'id', 'login', 'fullname', 'email',
        ], 'id');

        $result->addItems($items);

        $this->renderJson($result->getResponse());
    }

    /**
     * Поиск администратора
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionAjaxGetById($id)
    {
        $id = Cast::toUInt($id);

        $item = UserRecord::model()->findByPk($id);

        if ($item === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $allowedAttributes = ['id', 'login', 'fullname', 'email', 'status', 'is_supplier', 'is_root'];

        $result = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);

        $this->renderJson($result);
    }

    /**
     * @param AdminUserRecord $model
     * @param int $supplierId
     * @throws \CException
     */
    private function checkSupplierBinding(AdminUserRecord $model, int $supplierId)
    {
        $supplier = AdminSuppliers::model()->find('supplier_id = :supplier_id', [
            ':supplier_id' => $supplierId,
        ]);
        $oldSupplierId = $model->supplier !== null ? (int)$model->supplier->supplier_id : 0;

        if ($oldSupplierId !== $supplierId) {
            $model->attachEventHandler('onAfterValidate', function ($event) use ($supplier) {
                if ($event->sender->is_supplier && null !== $supplier) {
                    $event->sender->addError('is_supplier', $this->t('This supplier have been already bound to the user'));
                }
            });
        }
    }

    /**
     * @param AdminUserRecord $model
     * @param array $rolesIds
     */
    private function checkRoleErrors(AdminUserRecord $model, array $rolesIds)
    {
        $model->attachEventHandler('onAfterValidate', function ($event) use ($rolesIds) {
            if (count($rolesIds) === 0 && !$event->sender->is_root) {
                $event->sender->addError('is_root', $this->t('Select role'));
            }
        });
    }
}
