<?php

namespace MommyCom\Controller\Backend;

use CDataProvider;
use CDataProviderIterator;
use CHttpException;
use CMap;
use CSort;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\SupplierRelationPaymentPenaltyRecord;
use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Model\Db\SupplierRelationPaymentTransactionRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use TbForm;

class RelationPaymentController extends BackController
{
    /**
     * @param array $statuses
     *
     * @property-description Просмотр всех записей
     */
    public function actionIndex(array $statuses = [])
    {
        $selector = SupplierRelationPaymentRecord::model();

        if (is_array($statuses) && $statuses) {
            $selector->statusIn($statuses);
        }

        $eventStartAtRangeDataFilter = $this->app()->request->getParam('eventStartAt', '');
        $eventStartAtRangeData = explode('-', $eventStartAtRangeDataFilter);

        if (isset($eventStartAtRangeData[0]) && isset($eventStartAtRangeData[1])) {
            $eventStartTime = strtotime('today ' . Utf8::trim($eventStartAtRangeData[0]));
            $eventEndTime = strtotime('tomorrow ' . Utf8::trim($eventStartAtRangeData[1]));

            $selector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'timeStartsAfter' => $eventStartTime,
                        'timeStartsBefore' => $eventEndTime,
                    ],
                ],
            ]);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierRelationPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $summaryInfo = ['summary' => false, 'additionSummary' => false];

        if (!empty($eventStartAtRangeDataFilter)) {
            $providerSummary = $selector->copy()->getDataProvider(false, [
                'pagination' => false,
                'sort' => [
                    'sortVar' => '_summarySort',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('SupplierRelationPaymentRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

            $summaryInfo = $this->_createSummary($providerSummary);
        }

        $this->commitView(null, $this->t('View dropshiping pending delivery (dropshipping)'));

        $this->render('index', [
            'provider' => $provider,
            'summary' => $summaryInfo['summary'],
            'additionSummary' => $summaryInfo['additionSummary'],
        ]);
    }

    /**
     * @property-description Просмотр списка к оплате
     */
    public function actionToPay()
    {
        $selector = SupplierRelationPaymentRecord::model()
            ->status(SupplierRelationPaymentRecord::STATUS_READY_TO_PAY);

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierRelationPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $providerSummary = $selector->copy()->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'sortVar' => '_summarySort',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierRelationPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $summaryInfo = $this->_createSummary($providerSummary);

        $this->commitView(null, $this->t('View payments due to dropshiping suppliers (dropshipping)'));

        $this->render('toPay', [
            'provider' => $provider,
            'summary' => $summaryInfo['summary'],
            'additionSummary' => $summaryInfo['additionSummary'],
        ]);
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     * @property-description Добавление штрафов
     */
    public function actionAddPenalty($id)
    {
        /* @var $model SupplierRelationPaymentRecord */
        $model = SupplierRelationPaymentRecord::model()
            ->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        } else if (!$model->isPossibleAddPenalty()) {
            throw new CHttpException(404, 'Начисление штрафа для статуса оплаты "' . $model->statusText . '" недоступно');
        }

        $modelPenalty = new SupplierRelationPaymentPenaltyRecord();
        $modelPenalty->event_id = $model->event_id;
        $modelPenalty->supplier_id = $model->supplier_id;

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.supplierRelationPenalty', $this, [
            'type' => 'horizontal',
        ], $modelPenalty);

        if ($form->submitted('submit') && $form->validate()) {
            if ($modelPenalty->save()) {
                $model->is_force_refresh = 1;
                $model->save();

                $this->redirect(['update', 'id' => $model->id]);
            }
        }

        $this->commitView(null, $this->t('Add penalty to supplier'));

        $this->render('addPenalty', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     * @property-description Добавление транзакций
     */
    public function actionPay($id)
    {
        /* @var $model SupplierRelationPaymentRecord */
        $model = SupplierRelationPaymentRecord::model()
            ->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        } elseif (!$model->isPossibleAddPayment()) {
            throw new CHttpException(404, 'Not available for payment');
        }

        $modelTransaction = new SupplierRelationPaymentTransactionRecord();
        $modelTransaction->event_id = $model->event_id;

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.supplierRelationPaymentTransaction', $this, [
            'type' => 'horizontal',
        ], $modelTransaction);

        if ($form->submitted('submit') && $form->validate()) {
            $modelForm = $form->getModel();
            if ($modelForm->save()) {
                $model->is_force_refresh = 1;
                $model->save();

                $this->redirect(['update', 'id' => $model->id]);
            }
        }

        $this->commitView(null, $this->t('Adding a supplier'));

        $this->render('pay', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @property-description Удаление транзакций
     */
    public function actionDeletePay($id)
    {
        /* @var $model SupplierRelationPaymentTransactionRecord */
        $model = SupplierRelationPaymentTransactionRecord::model()
            ->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        } elseif (!$model->isAvailableEdit()) {
            throw new CHttpException(404, 'Editing/deletion disabled');
        }

        if ($model->delete()) {
            $relationPayment = SupplierRelationPaymentRecord::model()->eventId($model->event_id)->find();
            if ($relationPayment) {
                $relationPayment->is_force_refresh = 1;
                $relationPayment->save();
            }
        }

        $this->commitView($model, $this->t('Delete a transaction'));
    }

    /**
     * @throws CHttpException
     * @property-description Изменение статуса штрафа
     */
    public function actionChangePenaltyState()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', 0);

        /* @var $model SupplierRelationPaymentPenaltyRecord */
        $model = SupplierRelationPaymentPenaltyRecord::model()->findByPk($pk);

        if ($model === null) {
            throw new CHttpException(404, 'Penalty not found');
        }

        $model->state = $value;
        if ($model->save()) {
            $relationPayment = SupplierRelationPaymentRecord::model()->eventId($model->event_id)->find();
            if ($relationPayment) {
                $relationPayment->is_force_refresh = 1;
                $relationPayment->save();
            }
        }

        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }

        $this->commitView($model, $this->t('Change penalty status'));
    }

    /**
     * @param integer $id
     *
     * @throws CHttpException
     * @property-description Простмотр взаимоотношения с поставщиком
     */
    public function actionView($id)
    {
        $model = SupplierRelationPaymentRecord::model()->findByPk(intval($id));

        if ($model === null) {
            throw new CHttpException(404, 'Record not found');
        }

        $providerPenalty = SupplierRelationPaymentPenaltyRecord::model()
            ->eventId($model->event_id)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageVar' => '_penaltyPage',
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('SupplierRelationPaymentPenaltyRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        $providerTransactions = SupplierRelationPaymentTransactionRecord::model()
            ->eventId($model->event_id)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageVar' => '_paymentPage',
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('SupplierRelationPaymentTransactionRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        $providerOrders = OrderRecord::model()
            ->eventId($model->event_id)
            ->supplierIdIs($model->supplier_id)
            ->groupBy($model->getTableAlias() . ".id")//для правильной пагинации
            ->getDataProvider(false, [
                'pagination' => [
                    'pageVar' => '_ordersPage',
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        //сброс связей
        if ($providerOrders->getCriteria()->with) {
            $relations = array_keys($providerOrders->getCriteria()->with);
            foreach ($providerOrders->getData() as $order) {
                /* @var OrderRecord $order */
                foreach ($relations as $relation) {
                    if (is_string($relation)) {
                        unset($order->$relation);
                    }
                }
            }
        }

        $this->commitView($model, $this->t('Update Dropshiping Reconciliation'));

        $this->render('view', [
            'model' => $model,
            'providerPenalty' => $providerPenalty,
            'providerTransactions' => $providerTransactions,
            'providerOrders' => $providerOrders,
        ]);
    }

    /**
     * @param integer $id
     *
     * @throws CHttpException
     * @property-description Обновление взаимоотношения с поставщиком
     */
    public function actionUpdate($id)
    {
        $model = SupplierRelationPaymentRecord::model()->findByPk(intval($id));

        if ($model === null) {
            throw new CHttpException(404, $this->t('Record not found'));
        }

        if ($model->status == SupplierRelationPaymentRecord::STATUS_PAID) {
            throw new CHttpException(404, $this->t('Editing unavailable'));
        }

        self::_checkAvailableForUpdate($model);

        $providerPenalty = SupplierRelationPaymentPenaltyRecord::model()
            ->eventId($model->event_id)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageVar' => '_penaltyPage',
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('SupplierRelationPaymentPenaltyRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        $providerTransactions = SupplierRelationPaymentTransactionRecord::model()
            ->eventId($model->event_id)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageVar' => '_penaltyPage',
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('SupplierRelationPaymentTransactionRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        $this->commitView($model, $this->t('Update Dropshiping Reconciliation'));

        $this->render('update', [
            'model' => $model,
            'providerPenalty' => $providerPenalty,
            'providerTransactions' => $providerTransactions,
        ]);
    }

    public function actionAjaxForceUpdateStatistic($id)
    {
        $model = SupplierRelationPaymentRecord::model()->findByPk(intval($id));

        if ($model === null) {
            throw new CHttpException(404, 'Record not found');
        }

        if ($model->status == SupplierRelationPaymentRecord::STATUS_PAID) {
            throw new CHttpException(404, 'Update unavailable');
        }

        $model->is_force_refresh = 1;
        $model->save();
        if ($model->hasErrors()) {
            $errors = $model->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    private static function _checkAvailableForUpdate(SupplierRelationPaymentRecord $model)
    {
        static $closeStatusesForUpdate = [
            SupplierRelationPaymentRecord::STATUS_PAID,
        ];

        if (in_array($model->status, $closeStatusesForUpdate)) {
            throw new CHttpException(404, 'Payment status closed for editing');
        }
    }

    /**
     * @param CDataProvider $provider
     *
     * @return array
     */
    private function _createSummary(CDataProvider $provider)
    {
        $summary = new CMap([
            'orders' => 0,
            'orders_canceled' => 0,
            'orders_mailed' => 0,
            'orders_delivered' => 0,
            'orders_not_delivered' => 0,
            'orders_returned' => 0,
            'orders_amount' => 0.0,
            'orders_not_delivered_amount' => 0.0,
            'orders_prepaid_amount' => 0.0,
            'penalty_amount' => 0.0,
            'commission' => 0.0,
        ]);

        //addition summary
        $additionSummary = new CMap([
            'events' => 0,
            'avr_events_amount' => 0,
            'avr_commission' => 0,
            'orders_delivered_percent' => 0,
        ]);

        $iterator = new CDataProviderIterator($provider, 100);
        foreach ($iterator as $item) {
            /* @var SupplierRelationPaymentRecord $item */
            if ($item->orders == 0) {
                continue;
            }
            $additionSummary['events'] += 1;

            foreach ($summary->toArray() as $attribute => $value) {
                $summary->add($attribute, $value + Cast::toFloat($item->$attribute));
            }
        }

        $additionSummary->add('avr_events_amount', $additionSummary['events'] == 0 ? 0 : round($summary['orders_amount'] / $additionSummary['events'], 1));
        $additionSummary->add('avr_commission', $additionSummary['events'] == 0 ? 0 : round($summary['commission'] / $additionSummary['events'], 1));
        $additionSummary->add('orders_delivered_percent', $summary['orders_mailed'] == 0 ? 0 : round($summary['orders_delivered'] / $summary['orders_mailed'], 3) * 100);

        return ['summary' => $summary, 'additionSummary' => $additionSummary];
    }
}
