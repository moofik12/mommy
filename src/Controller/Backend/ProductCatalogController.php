<?php

namespace MommyCom\Controller\Backend;

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Service\BaseController\BackController;

class ProductCatalogController extends BackController
{
    public function actionIndex()
    {
        $selector = EventProductRecord::model();
        $createdAtRangeString = $this->app()->request->getParam('createdAtRange', false);

        if (!empty($createdAtRangeString)) {
            list($createdAtGreater, $createdAtLower) = explode(' - ', $createdAtRangeString);

            $selector->createdAtGreater(strtotime('today ' . $createdAtGreater));
            $selector->createdAtLower(strtotime('tomorrow ' . $createdAtLower));
        }

        $provider = $selector
            ->orderBy('product_id')
            ->with('product', 'event', 'brand')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('EventProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }
}
