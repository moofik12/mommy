<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Backend\BrandForm;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\FileStorage\File\Image;
use TbForm;

class BrandController extends BackController
{
    public function actionIndex()
    {
        $provider = BrandRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('BrandRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    public function actionAdd()
    {
        $model = new BrandRecord();

        $form = TbForm::createForm('widgets.form.config.brandAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'BrandRecord', 'image/*');

            if ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }
            $model->name = str_replace('/', "\\", $model->name);
            if ($model->save()) {
                $this->redirect(['brand/index']);
            }
        }

        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = BrandRecord::model()->findByPk($id);
        $formModel = new BrandForm();

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $form = TbForm::createForm('widgets.form.config.brandUpdate', $this, [
            'type' => 'horizontal',
        ], $formModel);

        if ($form->submitted('submit') && $form->validate()) {
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'BrandForm', 'image/*');
            $model->setAttributes($formModel->getAttributes());

            if ($formModel->isDeleteLogo) {
                $model->logo = null;
            } elseif ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }
            $model->name = str_replace('/', "\\", $model->name);
            if ($model->save()) {
                $this->redirect(['brand/index']);
            }
        } else {
            $formModel->setAttributes($model->getAttributes());
            $formModel->aliasesStrings = $model->getAliasesStrings();
        }

        $this->render('update', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = BrandRecord::model()->findByPk($id);
        /* @var $model BrandRecord */
        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->productCount > 0) {
            throw new CHttpException(405, $this->t('Disable deleting brand with products'));
        }

        $model->delete();

        $this->redirect(['brand/index']);
    }

    /**
     * @param string $like
     * @param int $page for Select2
     */
    public function actionAjaxSearch($like = '', $page)
    {
        /** @var BrandRecord $section */
        $section = BrandRecord::model();
        $result = new Select2Reply();

        if ($like) {
            $section->nameOrAliasLike($like);
        }

        $usersProvider = $section->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 10,
                'currentPage' => intval($page) - 1,
            ],
        ]);

        /** @var BrandRecord[] $brands */
        $brands = $usersProvider->getData();
        $result->pageCount = $usersProvider->getPagination()->pageCount;

        $result->addItems(ArrayUtils::getColumns($brands, [
            'id', 'name',
        ], 'id'));

        $this->renderJson($result->getResponse());
    }
}
