<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Backend\ProductForm;
use MommyCom\Model\Backend\ProductImageZipForm;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\FileStorage\FileStorageComponent;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;
use ZipArchive;

class ProductController extends BackController
{
    public function actionIndex()
    {
        $provider = ProductRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('ProductRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->saveBackUrl();
        $model = ProductRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /* @var $model ProductRecord */

        $modelForm = new ProductForm();
        $modelForm->logo = $model->logo;
        $modelForm->images = $model->images;
        $modelForm->name = $model->name;
        $modelForm->description = $model->description;
        $modelForm->madeIn = $model->made_in;
        $modelForm->designIn = $model->design_in;
        $modelForm->sectionId = $model->section_id;
        $modelForm->colorCode = $model->color_code;

        $form = TbForm::createForm($modelForm->getConfig(), $this, [
            'type' => 'horizontal',
        ], $modelForm);
        /* @var $form TbForm */

        if ($form->submitted('submit') && $form->validate()) {
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'ProductForm', 'image/*');
            $imageFiles = $modelForm->images;

            // set logo
            if ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }

            // add new images
            if (!empty($imageFiles)) {
                $model->images = $imageFiles;
            }

            $model->description = $modelForm->description;
            $model->name = $modelForm->name;
            $model->made_in = $modelForm->madeIn;
            $model->design_in = $modelForm->designIn;
            $model->section_id = $modelForm->sectionId;
            $model->color_code = $modelForm->colorCode;

            if ($model->save()) {
                $this->redirectBackOr(['product/index']);
            } else {
                $modelForm->addErrors($model->getErrors());
            }
        }

        $this->commitView($model, $this->t('Update product'));
        $this->render('update', [
            'form' => $form,
            'modelForm' => $modelForm,
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = ProductRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $data = [
            'model' => $model,
        ];

        $this->commitView($model, $this->t('View product'));
        $this->render('view', $data);
    }

    /**
     * @see TbFileUpload
     * @property-description загрузка файлов через TbFileUpload в FileStorage
     */
    public function actionPhotoUpload($class = null, $attribute = '')
    {
        $return = ['error' => 'No file to save'];

        $file = $this->app()->filestorage->factoryFromUploadedFile($attribute, $class, 'image/*');

        if ($file instanceof Image) {
            $thumbnails = $this->app()->filestorage->getThumbnails($file->getEncodedId());
            $file = [
                'id' => $file->getEncodedId(),
                'url' => $file->getWebFullPath(),
                'size' => $file->getFileSize(),
                'name' => $file->getFilename(),
                'type' => $file->getType(),
                'thumbnail_url' => $thumbnails->getThumbnail('small150')->url,

                //'delete_url' => $this->app()->createUrl('fileTemp/deleteFile', array('id' => 'id')),
                //'delete_type' => "DELETE",
            ];
            $return = [$file];
        } else {
            throw new CHttpException(500, $this->t("Could not upload file"));
        }

        $this->renderJson($return);
    }

    /**
     * @property-description удаление файлов из FileStorage
     */
    public function actionPhotoDelete()
    {
        $fileId = Cast::toStr($this->app()->request->getParam('fileId'));
        $file = $this->app()->filestorage->get($fileId);
        $result = new Reply();
        $result->setState(false);

        if ($file instanceof Image) {
            $file->remove();

            $result->addInfo('File deleted');
            $result->setState(true);
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Пакетная загрузка изображений для товаров
     */
    public function actionUpdateImages()
    {
        set_time_limit(600);
        setlocale(LC_ALL, 'ru_RU');
        $app = $this->app();
        $request = $app->request;
        $productImageZipModel = new ProductImageZipForm();

        $dataForm = $request->getPost('ProductImageZipForm');
        if (!empty($dataForm)) {
            $productImageZipModel->setAttributes($dataForm);
            if ($productImageZipModel->eventId > 0) {
                $forceUpdate = $productImageZipModel->forceUpdate;
                $tempZips = $this->reArrayFiles($_FILES['ProductImageZipForm'], 'zipes');
                $unzipTempDir = sys_get_temp_dir() . '/event' . $productImageZipModel->eventId . '_' . time();
                $fileStorage = $this->app()->filestorage;
                /** @var  $fileStorage FileStorageComponent */

                $productIds = EventProductRecord::model()->eventId($productImageZipModel->eventId)->findColumnDistinct('product_id');
                $articles = [];
                $photos = [];
                if ($productIds !== false && !empty($productIds)) {
                    $articles = ProductRecord::model()->idIn($productIds)->orderBy('article', 'DESC')->findColumnDistinct('article');
                    if (!empty($articles)) {
                        rsort($articles);
                    }
                    $photos = ProductRecord::model()->idIn($productIds)->findColumnDistinct('photo');
                    if (!empty($photos)) {
                        rsort($photos);
                    }
                }
                if (!empty($articles)) {
                    foreach ($tempZips as $tempZip) {
                        $zip = new ZipArchive();
                        if ($zip->open($tempZip['tmp_name']) === true) {
                            $isExtractTo = $zip->extractTo($unzipTempDir);
                            $zip->close();
                            if ($isExtractTo) {
                                $fileNames = self::getFiles($unzipTempDir);
                                rsort($fileNames);
                                //CVarDumper::dump($fileNames, 100, true);
                                //поиск по полю фото
                                if (!empty($photos)) {
                                    foreach ($photos as $photo) {
                                        $articleFiles = [];
                                        if ($photo != '') {
                                            foreach ($fileNames as $key => $filename) {
                                                if (mb_stripos($filename, $photo) !== false) {
                                                    unset($fileNames[$key]);
                                                    $articleFiles[] = $filename;
                                                }
                                            }
                                            if (!empty($articleFiles)) {
                                                $product = ProductRecord::model()->photoIs($photo)->idIn($productIds)->find();
                                                if ($product !== null) {
                                                    $this->_updateProduct($product, $articleFiles, $forceUpdate, $fileStorage);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!empty($articles)) {
                                    foreach ($articles as $article) {
                                        $articleFiles = [];
                                        if ($article != '') {
                                            foreach ($fileNames as $key => $filename) {
                                                if (mb_stripos($filename, $article) !== false) {
                                                    unset($fileNames[$key]);
                                                    $articleFiles[] = $filename;
                                                }
                                            }
                                            if (!empty($articleFiles)) {
                                                $product = ProductRecord::model()->articleIs($article)->idIn($productIds)->find();
                                                if ($product !== null) {
                                                    $this->_updateProduct($product, $articleFiles, $forceUpdate, $fileStorage);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //очистка папки с файлами архивов
                    self::clearDir($unzipTempDir);
                }
                //очистка загруженных zip-ов
                foreach ($tempZips as $tempZip) {
                    unlink($tempZip['tmp_name']);
                }
                $this->redirect($this->createUrl('event/update', ['id' => $productImageZipModel->eventId]));
            }
        }
        $app->end();
    }

    /**
     * Обновляем товар новыми изображениямми
     *
     * @param ProductRecord $product
     * @param $articleFiles
     * @param $forceUpdate
     * @param $fileStorage
     */
    private function _updateProduct(ProductRecord $product, $articleFiles, $forceUpdate, $fileStorage)
    {
        /** @var  $product ProductRecord */
        $hasImages = !empty($product->images);
        $hasFiles = $product->getIsHasLogo() && $hasImages;
        if (!$hasFiles || $forceUpdate) {
            if (!empty($articleFiles)) {
                $articleFiles = array_reverse($articleFiles);
                //CVarDumper::dump($articleFiles, 100, true);
                $product->logo = null;
                $product->images = []; // удаляем все текущие изображения
                $images = [];

                foreach ($articleFiles as $filename) {
                    $file = File::factoryFromUploadedFile($fileStorage->getStorage(), $filename);
                    if ($file instanceof File) {
                        if ($file->save()) {
                            $images[] = $file;
                        }
                    }
                    if (file_exists($filename)) { //на случай если файл не добавили выше
                        unlink($filename);
                    }
                }
                if (!empty($images)) {
                    $logo = array_shift($images);
                    $product->setLogo($logo);
                    $product->setImages($images);
                    if (!$product->save()) {
                    }
                }
            }
        }
    }

    /**
     * @param $dir
     *
     * @return array
     */
    private static function simpleScanDir($dir)
    {
        $list = scandir($dir);
        unset($list[0], $list[1]);
        return array_values($list);
    }

    /**
     * @property-description Очистка темповой папки
     *
     * @param $dir
     */
    private static function clearDir($dir)
    {
        $list = self::simpleScanDir($dir);
        foreach ($list as $file) {
            if (is_dir($dir . '/' . $file)) {
                //echo '<br>' . $dir.$file . '<br>';
                chmod($dir . '/' . $file, 0777);
                self::clearDir($dir . '/' . $file . '/');
                rmdir($dir . '/' . $file);
            } else {
                //echo ' - '.$dir.'/'. $file . '<br>';
                unlink($dir . '/' . $file);
            }
        }
    }

    /**
     * @property-description Рекурсивное определение списка файлов в папке
     *
     * @param $dir
     *
     * @return array
     */
    private static function getFiles($dir)
    {
        $files = [];
        if ($handle = opendir($dir)) {
            while (false !== ($item = readdir($handle))) {
                if (is_file("$dir/$item")) {
                    $files[] = "$dir/$item";
                } elseif (is_dir("$dir/$item") && ($item != ".") && ($item != "..")) {
                    $files = array_merge($files, self::getFiles("$dir/$item"));
                }
            }
            closedir($handle);
        }
        return $files;
    }

    /**
     * @property-description Сортировка файлов
     *
     * @param $filesPost
     * @param $fileKey
     *
     * @return array
     */
    function reArrayFiles(&$filesPost, $fileKey)
    {
        $files = [];
        $fileCount = count($filesPost['name'][$fileKey]);
        $fileKeys = array_keys($filesPost);

        for ($i = 0; $i < $fileCount; $i++) {
            foreach ($fileKeys as $key) {
                $files[$i][$key] = $filesPost[$key][$fileKey][$i];
            }
        }
        return $files;
    }
}
