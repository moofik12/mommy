<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Backend\EmailUserForm;
use MommyCom\Model\Backend\SmsUserForm;
use MommyCom\Model\Backend\UserPasswordForm;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\Model\Db\SmsMessageRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class UserController extends BackController
{
    /**
     * @property-description Просмотр списка пользователей
     */
    public function actionIndex($pageSize = 50)
    {
        $pageSize = Cast::toUInt($pageSize);

        // grid
        $dataProvider = UserRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => $pageSize < 100 ? $pageSize : 100,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
        ]);

        //new Sms (modal form)
        $smsForm = Form::createForm('widgets.form.config.smsUser', new SmsUserForm(), [
            'type' => 'vertical',
            'id' => 'sms-form',
        ]);

        //new Email (modal form)
        $emailForm = Form::createForm('widgets.form.config.emailUser', new EmailUserForm(), [
            'type' => 'vertical',
            'id' => 'email-form',
        ]);

        $this->commitView(null, $this->t('View list'));
        // render
        $this->render('index', [
            'dataProvider' => $dataProvider,
            'smsForm' => $smsForm,
            'emailForm' => $emailForm,
        ]);
    }

    public function actionAdd()
    {
        //new User
        /** @var UserRecord $model */
        $model = new UserRecord();
        $model->setScenario('register');

        /** @var Form $form */
        $form = Form::createForm('widgets.form.config.user', $model, [
            'type' => 'horizontal',
        ]);
        $submitted = $form->submitted('submit');
        $valid = $form->validate();
        if ($submitted && $valid) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->redirect(['user/index']);
            }
        }

        $this->commitView(null, $this->t('Add user'));
        $this->render('add', ['form' => $form]);
    }

    /**
     * @property-description Редактирование пользователя
     *
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        /** @var $model UserRecord
         * @var $form TbForm
         * @var $formPassword TbForm
         */

        $model = UserRecord::model()->findByPk($id);
        $formNewPassword = new UserPasswordForm();

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $form = TbForm::createForm('widgets.form.config.user', $this, [
            'type' => 'horizontal',
        ], $model);

        $formPassword = TbForm::createForm('widgets.form.config.userPassword', $this, [
            'type' => 'horizontal',
        ], $formNewPassword);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();

            if ($model->save()) {
                $this->redirect(['user/index']);
            }
        } elseif ($formPassword->submitted('submit') && $formPassword->validate()) {
            $model->password = $formNewPassword->password;

            if ($model->save()) {
                $this->redirect(['user/index']);
            }
        }

        $this->commitView(null, $this->t('Editing user'));
        $this->render('update', [
            'model' => $model,
            'form' => $form,
            'formPassword' => $formPassword,
        ]);
    }

    /**
     * @property-description Удаление пользователя
     *
     * @param integer $id
     * * @throws CHttpException
     */
    public function actionBanned($id)
    {
        $model = UserRecord::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $this->commitView(null, $this->t('Blocking user'));
        $model->status = UserRecord::STATUS_BANNED;
        if ($model->save(true, ['status'])) {
            $distributions = UserDistributionRecord::model()->userId($model->id)->findAll();
            if (!empty($distributions)) {
                foreach ($distributions as $distribution) {
                    /** @var UserDistributionRecord $distribution */
                    $distribution->is_update = 1;
                    $distribution->is_subscribe = 0;
                }
                UnShardedActiveRecord::saveMany($distributions, false, ['is_update', 'is_subscribe']);
            }
        }

        if (!$this->app()->getRequest()->isAjaxRequest) {
            $this->redirect($this->app()->request->urlReferrer);
        }
    }

    /**
     * @property-description Удаление пользователя
     *
     * @param integer $id
     * * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $model = UserRecord::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }
        //нет логики удаления связей
        //$model->delete();

        $this->commitView(null, $this->t('Deleting user (status)'));
        if (!$this->app()->getRequest()->isAjaxRequest) {
            $this->redirect($this->app()->request->urlReferrer);
        }
    }

    /**
     * @param integer $id ID пользователя
     *
     * @throws CHttpException
     */
    public function actionMessages($id)
    {
        /** @var UserRecord $model */
        $model = UserRecord::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        // grids
        $smsProvider = SmsMessageRecord::model()->userId($model->id)->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 25,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SmsMessageRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $emailProvider = EmailMessage::model()->sendToLike($model->email)->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 25,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('EmailMessage', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->commitView(null, $this->t('User posts'));
        $this->render('messages'
            , ['smsProvider' => $smsProvider, 'emailProvider' => $emailProvider, 'model' => $model]);
    }

    /**
     * @param string $like
     * @param $page для Select2
     */
    public function actionAjaxSearch($like = '', $page)
    {
        /** @var UserRecord $section */
        $section = UserRecord::model();
        $result = new Select2Reply();

        if (!empty($like)) {
            $section->nameLike($like)
                ->surnameLike($like)
                ->emailLike($like);
        }

        $usersProvider = $section->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 10,
                'currentPage' => intval($page) - 1,
            ],
        ]);

        /** @var UserRecord[] $users */
        $users = $usersProvider->getData();
        $result->pageCount = $usersProvider->getPagination()->pageCount;

        $result->addItems(ArrayUtils::getColumns($users, [
            'id', 'name', 'surname',
            'email', 'telephone',
        ], 'id'));

        $this->renderJson($result->getResponse());
    }

    public function actionAjaxGetById($id)
    {
        $id = Cast::toUInt($id);

        $item = UserRecord::model()->findByPk($id);

        if ($item === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $allowedAttributes = ['id', 'name', 'surname', 'email', 'telephone', 'updated_at'];

        $result = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);

        $this->renderJson($result);
    }
}
