<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class StaticPageController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Managing Static Pages';

    //действия для сортировки (position)
    public function actions()
    {
        return [
            'position' => [
                'class' => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'StaticPageRecord',
            ],
        ];
    }

    /**
     * If creation is successful, the browser will be redirected to the 'index' page.
     */
    public function actionCreate()
    {
        $model = new StaticPageRecord();

        $form = TbForm::createForm('widgets.form.config.staticPage', $this, [
            'type' => 'horizontal',
            'id' => 'form-static-page',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->app()->user->setFlash('success', 'Page added');
                $this->redirect(['index']);
            }
        }

        $this->commitView(null, $this->t('Creating page'));
        $this->render('create', [
            'form' => $form,
        ]);
    }

    /**
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $form = TbForm::createForm('widgets.form.config.staticPage', $this, [
            'type' => 'horizontal',
            'id' => 'form-static-page',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->app()->user->setFlash('success', 'Страница обновлена');
                $this->redirect(['index']);
            }
        }

        $this->commitView($model, $this->t('Page update'));
        $this->render('update', [
            'form' => $form,
        ]);
    }

    /**
     * @param integer $id the ID of the model to be deleted
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if ($this->app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
        } else
            throw new CHttpException(400, $this->t('Invalid request. Please do not repeat this request again.'));
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = StaticPageRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('StaticPageRecord', []),
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|StaticPageRecord
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $id = intval($id);

        $model = StaticPageRecord::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, $this->t('Model does not exist.'));
        return $model;
    }

    /**
     * Support change only StaticPage::is_visible attribute
     *
     * @param integer $id
     */
    public function actionToggle($id)
    {
        $model = $this->loadModel($id);
        $attribute = $this->app()->request->getParam('attribute');

        if ($attribute == 'is_visible' && $this->app()->request->isAjaxRequest) {
            $model->is_visible = !$model->is_visible;
            $model->save(true, ['is_visible']);
        }
    }
}
