<?php

namespace MommyCom\Controller\Backend;

use CArrayDataProvider;
use CCache;
use CDataProviderIterator;
use CDbCriteria;
use CDummyCache;
use CException;
use CHtml;
use CHttpException;
use CJavaScriptExpression;
use CSort;
use CSqlDataProvider;
use DateTime;
use LogicException;
use MommyCom\Model\Db\MoneyReceiveStatementOrderRecord;
use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Model\Db\StatisticFilterUserListRecord;
use MommyCom\Model\Db\StatisticFilterUserRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Model\Statistic\StatisticOrdersBuyoutModel;
use MommyCom\Model\Statistic\StatisticOrdersBuyoutSupplierModel;
use MommyCom\Model\Statistic\StatisticOrdersModel;
use MommyCom\Model\Statistic\StatisticOrdersPaidModel;
use MommyCom\Model\Statistic\StatisticSuppliersPaymentModel;
use MommyCom\Model\Statistic\StatisticTotalOrdersBuyoutModel;
use MommyCom\Model\Statistic\StatisticTotalOrdersModel;
use MommyCom\Model\Statistic\StatisticTotalSuppliersPaymentModel;
use MommyCom\Model\Statistic\StatisticTotalUsersModel;
use MommyCom\Model\Statistic\StatisticUsersModel;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;
use MommyCom\YiiComponent\Statistic\Statistic;
use MommyCom\YiiComponent\Statistic\StatisticFilter;
use MommyCom\YiiComponent\Statistic\StatisticTimeFilter;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use SplObjectStorage;
use TbButton;

class StatisticCustomController extends BackController
{
    /**
     * @property-description Статистика заказов
     *
     * @param int|bool|false $toFilePosition
     */
    public function actionOrders($toFilePosition = false)
    {
        $this->_renderOrders(false, $toFilePosition);
    }

    /**
     * @property-description Статистика заказов (базовая)
     */
    public function actionOrdersBase()
    {
        $this->_renderOrders(true);
    }

    private function _renderOrders($short = false, $toFilePosition = false)
    {
        set_time_limit(500);

        $selector = OrderRecord::model()->with(['user', 'tracking'])->together();
        $selector->getDbCriteria()->distinct = true;

        $statistic = new Statistic();
        $statistic->setSelector($selector);

        $statistic->bufferSize = 2500;

        /** STATISTIC TIME DEFAULT */
        $statisticTime = $statistic->createTimeFilter();
        $statisticTime->setFrom(new DateTime('tomorrow -14 days'));
        $statisticTime->setTo(new DateTime('tomorrow'));
        $statisticTime->column = 'ordered_at';
        $statistic->setTimeFilter($statisticTime);

        $this->_initFilters($statistic);
        $this->_initOrdersFilters($statistic);

        $statistic->setModelClass(StatisticOrdersModel::class);
        $statistic->setTotalModel(new StatisticTotalOrdersModel());
        $dataProvider = $statistic->getDataProvider(['pagination' => ['pageSize' => 50]]);

        if ($toFilePosition !== false) {
            $data = $dataProvider->getData();
            if (isset($data[$toFilePosition])) {
                $this->_sendFileFromOrders($data[$toFilePosition]);
            }
        }

        $this->render('orders', ['provider' => $dataProvider, 'statistic' => $statistic, 'short' => $short]);
    }

    private function _sendFileFromOrders(StatisticOrdersModel $model)
    {
        $ordersData = $model->getOrdersData();
        $df = $this->app()->dateFormatter;

        $userOrders = ArrayUtils::groupBy($ordersData, 'userID');
        $userOrdersQueue = array_chunk($userOrders, 100, true);

        $title = $this->t("Order statistics by user during the period") . ' ' . $model->getDateTimeFrom()->format('d.m.Y') . ' - ' . $model->getDateTimeTo()->format('d.m.Y');
        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];
        $csvDATA[] = [];

        $csvDATA[] = [
            $this->t('User ID'),
            $this->t('Email'),
            $this->t('Name'),
            $this->t('Phone number in profile'),
            $this->t('Phone numbers being used in orders'),
            $this->t('Orders on this day'),
            $this->t('Canceled orders on this day'),
            $this->t('Orders total on this day'),
            $this->t('bonuses used on this day'),
            $this->t('Orders dispached'),
            $this->t('Orders paid'),
            $this->t('Not paid and wouldn\'t be paid'),
            $this->t('Number of returns'),
            $this->t('Date of registration'),
        ];

        foreach ($userOrdersQueue as $heap) {
            foreach ($heap as $userID => $data) {
                $user = UserRecord::model()->with('orders')->together()->findByPk($userID);
                if ($user === null) {
                    throw new CHttpException(404, $this->t('User') . 'id' . $userID . $this->t('not found'));
                }

                $ordersIDHasInPeriod = ArrayUtils::getColumn($data, 'id');
                $ordersAmountInPeriod = array_reduce($user->orders, function ($carry, $item) use ($ordersIDHasInPeriod) {
                    /* @var OrderRecord $item */
                    if (in_array($item->id, $ordersIDHasInPeriod)) {
                        return $carry + $item->price_total;
                    }

                    return $carry;
                }, 0.0);

                $ordersAmountBonusestInPeriod = array_reduce($user->orders, function ($carry, $item) use ($ordersIDHasInPeriod) {
                    /* @var OrderRecord $item */
                    if (in_array($item->id, $ordersIDHasInPeriod)) {
                        return $carry + $item->bonuses;
                    }

                    return $carry;
                }, 0.0);

                $ordersIDCanceledInPeriod = array_filter(array_map(function ($item) use ($ordersIDHasInPeriod) {
                    /* @var OrderRecord $item */
                    if (in_array($item->id, $ordersIDHasInPeriod)
                        && $item->processing_status == OrderRecord::PROCESSING_CANCELLED
                        && !$item->isMerged()
                    ) {
                        return $item->id;
                    }
                    return 0;
                }, $user->orders));

                $telephones = array_map(function ($item) {
                    /* @var OrderRecord $item */
                    return $item->telephone;
                }, $user->orders);

                $ordersMailedCount = OrderRecord::model()->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_MAILED)->userId($user->id)->count();
                $buyoutPayedCount = OrderBuyoutRecord::model()->userId($user->id)->status(OrderBuyoutRecord::STATUS_PAYED)->count();
                $buyoutCancelledCount = OrderBuyoutRecord::model()->userId($user->id)->status(OrderBuyoutRecord::STATUS_CANCELLED)->count();
                $possibleReturnIds = OrderBuyoutRecord::model()->userId($user->id)->status(OrderBuyoutRecord::STATUS_PAYED)->findColumnDistinct('order_id');
                $ordersReturn = OrderReturnRecord::model()->orderIdIn($possibleReturnIds)->statusIn([
                    OrderReturnRecord::STATUS_UNCONFIGURED,
                    OrderReturnRecord::STATUS_PARTIAL_CONFIGURED,
                    OrderReturnRecord::STATUS_CONFIGURED,
                    OrderReturnRecord::STATUS_NEED_PAY,
                    OrderReturnRecord::STATUS_PAYED,
                ])->findColumnDistinctCount('order_id');

                $csvDATA[] = [
                    $user->id,
                    $user->email,
                    "{$user->name} {$user->surname}",
                    $user->telephone,
                    implode(', ', array_unique($telephones)),
                    count($ordersIDHasInPeriod),
                    count($ordersIDCanceledInPeriod),
                    $ordersAmountInPeriod,
                    $ordersAmountBonusestInPeriod,
                    $ordersMailedCount,
                    $buyoutPayedCount,
                    $buyoutCancelledCount,
                    $ordersReturn,
                    $df->formatDateTime($user->created_at),
                ];
            }
        }

        $this->commitView(null, $title);
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = str_ireplace(' ', '_', $title) . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * @property-description Статистика пользователей
     */
    public function actionUsers()
    {
        $request = $this->app()->request;
        set_time_limit(300);

        $selector = UserRecord::model();
        $selector->getDbCriteria()->distinct = true;

        $statistic = new Statistic();
        $statistic->setSelector($selector);

        /** STATISTIC TIME DEFAULT */
        $statisticTime = $statistic->createTimeFilter();
        $statisticTime->setFrom(new DateTime('tomorrow -1 month'));
        $statisticTime->setTo(new DateTime('tomorrow'));
        $statisticTime->column = 'created_at';
        $statistic->setTimeFilter($statisticTime);

        $this->_initFilters($statistic);
        $this->_initUsersFilters($statistic);

        $statistic->setModelClass(StatisticUsersModel::class);
        $statistic->setTotalModel(new StatisticTotalUsersModel());

        if ($request->getPost('submit', false) == 'segment' && $request->isAjaxRequest) {
            $name = $request->getPost('segment', '');
            $result = $this->_createSegment($name, $statistic);

            $this->commitView(null, $this->t('Create a segment by users'));
            $this->renderJson($result->getResponse());
            $this->app()->end();
        }

        $dataProvider = $statistic->getDataProvider(['pagination' => ['pageSize' => 50]]);

        $this->commitView(null, $this->t('View statistics by users'));
        $this->render('users', ['provider' => $dataProvider, 'statistic' => $statistic]);
    }

    /**
     * @property-description Статистика оплаты поставщикам
     */
    public function actionSuppliersPayment()
    {
        set_time_limit(300);

        $selector = SupplierPaymentRecord::model();
        $selector->getDbCriteria()->distinct = true;

        $statistic = new Statistic();
        $statistic->setSelector($selector);

        /** STATISTIC TIME DEFAULT */
        $statisticTime = $statistic->createTimeFilter();
        $statisticTime->setFrom(new DateTime('tomorrow -1 month'));
        $statisticTime->setTo(new DateTime('tomorrow'));
        $statisticTime->column = 'payment_after_at';
        $statistic->setTimeFilter($statisticTime);

        $this->_initFilters($statistic);

        $statistic->setModelClass(StatisticSuppliersPaymentModel::class);
        $statistic->setTotalModel(new StatisticTotalSuppliersPaymentModel());
        $dataProvider = $statistic->getDataProvider(['pagination' => ['pageSize' => 50]]);

        $this->commitView(null, $this->t('View statistics on payments to suppliers'));
        $this->render('suppliersPayment', ['provider' => $dataProvider, 'statistic' => $statistic]);
    }

    /**
     * @property-description Детальная информации об оплатах
     *
     * @param int $from unix timestamp
     * @param int $to unix timestamp
     *
     * @throws CHttpException
     */
    public function actionOrderDetailPaid($from, $to)
    {
        $from = Cast::toUInt($from);
        $to = Cast::toUInt($to);

        if ($from == 0 || $to == 0) {
            throw new CHttpException(500, 'Final and starting dates must be greater than 0');
        }

        if ($from > $to) {
            throw new CHttpException(500, 'The end date is later than starting date');
        }

        //оплачены
        $ordersBuyoutID = OrderBuyoutRecord::model()
            ->together()
            ->with('order', 'order.tracking')
            ->statusChangedAtGreater($from)
            ->statusChangedAtLower($to)
            ->status(OrderBuyoutRecord::STATUS_PAYED)
            ->findColumnDistinct('order_id');

        //подтверждены оплаты
        $moneyReceiveOrdersID = MoneyReceiveStatementOrderRecord::model()
            ->together()
            ->orderIdIn($ordersBuyoutID)
            ->findColumnDistinct('order_id');

        $ordersBuyoutNotConfirmID = array_diff($ordersBuyoutID, $moneyReceiveOrdersID);

        $notConfirmProvider =
            OrderBuyoutRecord::model()
                ->orderIdIn($ordersBuyoutNotConfirmID)
                ->getDataProvider(false, [
                        'pagination' => false,
                        'sort' => [
                            'defaultOrder' => [
                                'price' => CSort::SORT_DESC,
                            ],
                        ],
                    ]
                );

        $this->commitView(null, $this->t('View statistics by orders'));
        $this->render('orderDetailPaid',
            ['notConfirmProvider' => $notConfirmProvider]
        );
    }

    /**
     * @property-description Выкуп заказов
     */
    public function actionOrdersBuyout()
    {
        $citiesToCsv = $this->app()->request->getParam('citiesToCsv');
        $this->_renderOrdersBuyout($citiesToCsv);
    }

    private function _renderOrdersBuyout($citiesToCsv = false)
    {
        set_time_limit(300);

        $selector = OrderRecord::model()->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_MAILED);
        $selector->getDbCriteria()->distinct = true;

        $statistic = new Statistic();
        $statistic->setSelector($selector);

        /** STATISTIC TIME DEFAULT */
        $statisticTime = $statistic->createTimeFilter();
        $statisticTime->setFrom(new DateTime('tomorrow -34 days'));
        $statisticTime->setTo(new DateTime('tomorrow'));
        $statisticTime->column = 'updated_at';
        $statistic->setTimeFilter($statisticTime);

        $this->_initFilters($statistic);
        $this->_initOrdersBuyoutFilters($statistic);

        $statistic->setModelClass(StatisticOrdersBuyoutModel::class);
        $statistic->setTotalModel(new StatisticTotalOrdersBuyoutModel());
        $dataProvider = $statistic->getDataProvider(['pagination' => false]);
        $dataProvider = new CArrayDataProvider($dataProvider->getData(), [
            'pagination' => ['pageSize' => 50],
            'sort' => [
                'attributes' => ['id', 'day', 'orders', 'ordersAmount', 'notBoughtOrders', 'notBoughtOrdersAmount', 'returnedOrders', 'returnedOrdersAmount', 'ordersBought', 'ordersBoughtProducts', 'ordersBoughtAmount'],
            ],
        ]);

        $orderCitiesStorage = new SplObjectStorage();
        /* @var StatisticTotalOrdersBuyoutModel $totalModel */
        $totalModel = $statistic->getTotalModel();

        if ($citiesToCsv) {
            $this->sendCitiesCSV($orderCitiesStorage, $statisticTime, $totalModel);
        }

        $this->commitView(null, $this->t('View statistics by order purchased'));
        $this->render('ordersBuyout', ['provider' => $dataProvider, 'statistic' => $statistic, 'orderCitiesStorage' => $orderCitiesStorage]);
    }

    /**
     * @param SplObjectStorage $orderCitiesStorage
     * @param StatisticTimeFilter $filter
     * @param StatisticTotalOrdersBuyoutModel $total
     *
     * @throws CHttpException
     */
    private function sendCitiesCSV(SplObjectStorage $orderCitiesStorage, StatisticTimeFilter $filter, StatisticTotalOrdersBuyoutModel $total)
    {
        $dataTimeFrom = new DateTime();
        $dataTimeFrom->setTimestamp($filter->getFrom());
        $dataTimeTo = new DateTime();
        $dataTimeTo->setTimestamp($filter->getTo());

        $title = $this->t("Statistics of orders shipping destinations by cities over period") . ' ' . $dataTimeFrom->format('d.m.Y') . ' - ' . $dataTimeTo->format('d.m.Y');
        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];
        $csvDATA[] = [];

        $csvDATA[] = [
            $this->t('City'),
            $this->t('Dispatched'),
            '%',
        ];

        foreach ($orderCitiesStorage as $object) {
            $city = isset($orderCitiesStorage[$object]) ? $orderCitiesStorage[$object] : null;
            $city = $city ? $city->name : $object->id;

            /* @var StatisticOrdersBuyoutSupplierModel $supplier */
            $csvDATA[] = [
                $city,
                $object->countDelivery,
                $object->percentDelivery,
            ];
        }

        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = "statistics_of_orders_shipping_destinations_by cities_over_period_" . $dataTimeFrom->format('d.m.Y') . '_' . $dataTimeTo->format('d.m.Y') . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * @property-description Детальная информация о выкупе заказов
     *
     * @param int $from unix timestamp
     * @param int $to unix timestamp
     * @param $period $to unix timestamp
     * @param int $suppliersToFile
     * @param int $productsToFile
     * @param int $brandManagerToFile
     *
     * @throws CHttpException
     */
    public function actionOrdersSuppliersBuyout($from, $to, $period, $suppliersToFile = 0, $productsToFile = 0, $brandManagerToFile = 0)
    {
        set_time_limit(300);

        $from = Cast::toUInt($from);
        $to = Cast::toUInt($to);
        /* @var CCache $cache */
        $cache = $this->app()->cache instanceof CDummyCache && $this->app()->cacheSecond ? $this->app()->cacheSecond : $this->app()->cache;

        if ($from == 0 || $to == 0) {
            throw new CHttpException(500, 'Final and starting dates must be greater than 0');
        }

        if ($from > $to) {
            throw new CHttpException(500, 'The end date is later than starting date');
        }

        $selector = OrderRecord::model()->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_MAILED);
        $selector->getDbCriteria()->distinct = true;

        $statistic = new Statistic();
        $statistic->setSelector($selector);

        /** STATISTIC TIME DEFAULT */
        $statisticTime = $statistic->createTimeFilter();
        $statisticTime->column = 'updated_at';
        $statisticTime->setFrom($from);
        $statisticTime->setTo($to);
        $statisticTime->setPeriod($period);
        $statistic->setTimeFilter($statisticTime);

        $statistic->setModelClass(StatisticOrdersBuyoutModel::class);
        $dataProvider = $statistic->getDataProvider(['pagination' => false]);

        $cacheKey = "_ordersSuppliersBuyout-$from-$to-$period";
        if ($cacheData = $cache->get($cacheKey)) {
            $data = $cacheData;
        } else {
            $data = $dataProvider->getData();
            $cache->set($cacheKey, $data, 600);
        }

        if (count($data) > 1) {
            throw new LogicException('found many models!');
        }

        /* @var StatisticOrdersBuyoutModel $model */
        $model = reset($data);
        if (!$model instanceof StatisticOrdersBuyoutModel) {
            throw new LogicException('model not instance from StatisticOrdersBuyoutModel!');
        }

        if ($suppliersToFile) {
            $this->_ordersSuppliersBuyoutFile($model);
        } elseif ($productsToFile) {
            $this->_ordersProductsBuyoutFile($model);
        } elseif ($brandManagerToFile) {
            $this->_ordersBrandManagerBuyoutFile($model);
        }

        $dataProvider = new CArrayDataProvider($model->getSuppliers(), [
            'keyField' => 'supplierID',
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'ratioReturned', 'ratioNotBought', 'ratioSold', 'countSold', 'amountSold', 'countReturned', 'amountReturned', 'countNotBought', 'amountNotBought', 'ratioSoldAmount',
                ],
            ],
        ]);

        $this->commitView(null, "View statistics by order purchased over period" . $model->getDateTimeFrom()->format('d.m.Y') . ' - ' . $model->getDateTimeTo()->format('d.m.Y'));
        $this->render('orderSuppliersBuyout',
            ['provider' => $dataProvider, 'model' => $model]
        );
    }

    /**
     * @param StatisticOrdersBuyoutModel $model
     *
     * @throws CHttpException
     */
    private function _ordersSuppliersBuyoutFile(StatisticOrdersBuyoutModel $model)
    {
        $title = "Order statistics by suppliers over period" . $model->getDateTimeFrom()->format('d.m.Y') . ' - ' . $model->getDateTimeTo()->format('d.m.Y');
        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];
        $csvDATA[] = [];

        $csvDATA[] = [
            '№',
            $this->t('Supplier'),
            $this->t('% purchase'),
            $this->t('% purchase (in monetary equivalent)'),
            $this->t('% returns'),
            $this->t('% unfulfilled'),
            $this->t('Products sold'),
            $this->t('Products total'),
            $this->t('Products returned'),
            $this->t('Total refunds'),
            $this->t('Unfulfilled'),
            $this->t('Amount of unfulfilled'),
        ];

        foreach ($model->getSuppliers() as $index => $supplier) {
            /* @var StatisticOrdersBuyoutSupplierModel $supplier */
            $csvDATA[] = [
                $index + 1,
                $supplier->supplierName,
                $supplier->ratioSold,
                $supplier->ratioSoldAmount,
                $supplier->ratioReturned,
                $supplier->ratioNotBought,
                $supplier->countSold,
                $supplier->amountSold,
                $supplier->countReturned,
                $supplier->amountReturned,
                $supplier->countNotBought,
                $supplier->amountNotBought,
            ];
        }

        $this->commitView(null, $title);
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = "order_statistics_by_suppliers_over_period_" . $model->getDateTimeFrom()->format('d.m.Y') . '_' . $model->getDateTimeTo()->format('d.m.Y') . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * @param StatisticOrdersBuyoutModel $model
     *
     * @throws CHttpException
     */
    private function _ordersProductsBuyoutFile(StatisticOrdersBuyoutModel $model)
    {
        $title = $this->t("Order statistics by products over period") . ' ' . $model->getDateTimeFrom()->format('d.m.Y') . ' - ' . $model->getDateTimeTo()->format('d.m.Y');
        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];
        $csvDATA[] = [];

        $csvDATA[] = [
            '№',
            $this->t('Product number'),
            $this->t('Supplier'),
            $this->t('Flash-sale'),
            $this->t('Brand manager'),
            $this->t('Name'),
            $this->t('Price'),
            $this->t('% purchase'),
            $this->t('% returns'),
            $this->t('% unfulfilled'),
            $this->t('Products sold'),
            $this->t('Products total'),
            $this->t('Products returned'),
            $this->t('Total refunds'),
            $this->t('Unfulfilled'),
            $this->t('Unfulfilled orders'),
            $this->t('Amount of unfulfilled'),
            $this->t('Photo'),
        ];

        $counter = 1;
        foreach ($model->getSuppliers() as $index => $supplier) {
            foreach ($supplier->getProducts() as $indexProduct => $product) {
                $fileStorage = new FileStorageThumbnail($product->photoFileID, []);

                /* @var StatisticOrdersBuyoutSupplierModel $supplier */
                $csvDATA[] = [
                    $counter++,
                    $product->productID,
                    $supplier->supplierName,
                    $product->eventID,
                    $product->brandManagerName,
                    $product->name,
                    $product->price,
                    $product->ratioSold,
                    $product->ratioReturned,
                    $product->ratioNotBought,
                    $product->count,
                    $product->amountSold,
                    $product->countReturned,
                    $product->amountReturned,
                    $product->countNotBought,
                    implode('|', $product->notBoughtOrders),
                    $product->amountNotBought,
                    $fileStorage->getThumbnail('mid320')->url,
                ];
            }
        }

        $this->commitView(null, $title);
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = "order_statistics_by_products_over_period_" . $model->getDateTimeFrom()->format('d.m.Y') . '_' . $model->getDateTimeTo()->format('d.m.Y') . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * @param StatisticOrdersBuyoutModel $model
     *
     * @throws CHttpException
     */
    private function _ordersBrandManagerBuyoutFile(StatisticOrdersBuyoutModel $model)
    {
        $title = $this->t("Orders statistics by managers over period") . ' ' . $model->getDateTimeFrom()->format('d.m.Y') . ' - ' . $model->getDateTimeTo()->format('d.m.Y');
        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];
        $csvDATA[] = [];

        $csvDATA[] = [
            '№',
            $this->t('Manager'),
            $this->t('% purchase'),
            $this->t('% purchase (in monetary equivalent)'),
            $this->t('% returns'),
            $this->t('% unfulfilled'),
            $this->t('Products sold'),
            $this->t('Products total'),
            $this->t('Products returned'),
            $this->t('Total refunds'),
            $this->t('Unfulfilled'),
            $this->t('Amount of unfulfilled'),
        ];

        foreach ($model->getBrandManagersProducts() as $index => $brandManager) {
            /* @var StatisticOrdersBuyoutBrandManagerModel $brandManager */
            $csvDATA[] = [
                $index + 1,
                $brandManager->fullname,
                $brandManager->ratioSold,
                $brandManager->ratioSoldAmount,
                $brandManager->ratioReturned,
                $brandManager->ratioNotBought,
                $brandManager->countSold,
                $brandManager->amountSold,
                $brandManager->countReturned,
                $brandManager->amountReturned,
                $brandManager->countNotBought,
                $brandManager->amountNotBought,
            ];
        }

        $this->commitView(null, $title);
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = "orders_statistics_by_managers_over_period_" . $model->getDateTimeFrom()->format('d.m.Y') . '_' . $model->getDateTimeTo()->format('d.m.Y') . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * Инициализация общих фильтров
     *
     * @param Statistic $statistic
     *
     * @throws CException
     */
    protected function _initFilters(Statistic $statistic)
    {
        $timeStatisticFilter = $statistic->getTimeFilter();
        $controller = $this;

        /** PERIOD */
        $periodFilter = $statistic->createFilter();
        $periodFilter->id = 'period';
        $periodFilter->data = $timeStatisticFilter->periodName;
        $periodFilter->value = $this->app()->request->getParam($periodFilter->id, 0);
        $periodFilter->apply = function ($statistic, $filter) {
            /** @var StatisticFilter $filter */
            if (array_key_exists($filter->value, $filter->data)) {
                /** @var Statistic $statistic */
                $statistic->getTimeFilter()->setPeriod($filter->value);
            }
        };
        $periodFilter->renderHtml = function ($statistic, $filter) {
            return CHtml::dropDownList($filter->id, $filter->value, $filter->data, ['style' => 'vertical-align:top;']);
        };

        /** TIME */
        $timeFilter = $statistic->createFilter();
        $timeFilter->id = 'rangeData';
        $timeFilter->value = $this->app()->request->getParam('rangeData', date('d.m.Y', $timeStatisticFilter->getFrom()) . ' - ' . date('d.m.Y', $timeStatisticFilter->getTo()));
        $timeFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $rangeData = explode('-', $filter->value);
            if (isset($rangeData[0]) && isset($rangeData[1])) {
                $statistic->getTimeFilter()->setFrom(strtotime('today ' . Utf8::trim($rangeData[0])));
                $statistic->getTimeFilter()->setTo(strtotime('tomorrow ' . Utf8::trim($rangeData[1])));
            }
        };
        $timeFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = $controller->widget('bootstrap.widgets.TbDateRangePicker', [
                'name' => $filter->id,
                'value' => $filter->value,
                'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period'), 'style' => 'vertical-align:top;'],
                'callback' => new CJavaScriptExpression("function() {
                    $('#{$filter->id}').trigger('change');
                }"),
            ], true);

            return $html;
        };

        /** SET FILTERS */
        $statistic->addFilters([$periodFilter, $timeFilter]);
    }

    /**
     * Инициализация фильтров сецифичные заказам
     *
     * @param Statistic $statistic
     *
     * @throws CException
     */
    protected function _initOrdersFilters(Statistic $statistic)
    {
        $timeStatisticFilter = $statistic->getTimeFilter();
        $controller = $this;

        /** OFFER USERS */
        $userOfferFilter = $statistic->createFilter();
        $userOfferFilter->id = 'usersOfferList';
        $userOfferFilter->joinType = $this->app()->request->getParam($userOfferFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $userOfferFilter->value = $this->app()->request->getParam('usersOfferList');
        $userOfferFilter->data = [
            UserRecord::OFFER_PROVIDER_NONE => $this->t('Organic'),
            UserRecord::OFFER_PROVIDER_ACTIONPAY => 'ACTIONPAY',
            UserRecord::OFFER_PROVIDER_KLUMBA => 'KLUMBA',
            UserRecord::OFFER_PROVIDER_KIDSTUFF => 'KIDSTUFF',
            UserRecord::OFFER_PROVIDER_EXPO => 'EXPO',
            UserRecord::OFFER_PROVIDER_ADMITAD => 'ADMITAD',
        ];
        $userOfferFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = array_filter($filter->value, function ($item) {
                    return is_numeric($item);
                });

                $values = Cast::toUIntArr($values);
                $criteria = new CDbCriteria();
                $criteria->addInCondition('user.offer_provider', $values);
                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $userOfferFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => 'Offers',
                    'multiple' => 'multiple',
                    'data-tags' => 'true',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** OFFER CUSTOM USERS */
        $customOffers = UserRecord::model()->offerProvider(UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN)->findColumnDistinct('offer_id');
        $userCustomOfferFilter = $statistic->createFilter();
        $userCustomOfferFilter->id = 'usersCustomOfferList';
        $userCustomOfferFilter->joinType = $this->app()->request->getParam($userCustomOfferFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $userCustomOfferFilter->value = $this->app()->request->getParam('usersCustomOfferList');
        $userCustomOfferFilter->data = array_combine($customOffers, $customOffers);
        $userCustomOfferFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);

                $criteria = new CDbCriteria();
                $criteria->addInCondition('user.offer_id', $values);
                $criteria->addColumnCondition(['user.offer_provider' => UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN]);

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $userCustomOfferFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => 'Custom offers',
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** OFFER CUSTOM USERS ADD */
        $userCustomOfferAddFilter = $statistic->createFilter();
        $userCustomOfferAddFilter->id = 'usersCustomOfferAddList';
        $userCustomOfferAddFilter->joinType = $this->app()->request->getParam($userCustomOfferAddFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $userCustomOfferAddFilter->value = $this->app()->request->getParam('usersCustomOfferAddList');
        $userCustomOfferAddFilter->data = [
            'yandex' => 'Yandex direct',
            'google' => 'Google adwords',
        ];
        $userCustomOfferAddFilter->apply = function ($statistic, $filter) {
            $likeCondition = [
                'yandex' => 'direct-',
                'google' => 'adw-',
            ];

            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);
                $likeFilters = array_intersect_key($likeCondition, array_flip($values));

                if ($likeFilters) {
                    $criteria = new CDbCriteria();
                    //важен порядок добавления в критерию!!!
                    foreach ($likeFilters as $likeFilter) {
                        $criteria->addSearchCondition('user.offer_id', $likeFilter, true, 'OR');
                    }

                    $criteria->addColumnCondition(['user.offer_provider' => UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN]);

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $userCustomOfferAddFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Traffic sources'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** CUSTOM FILTER */
        $customFilterList = StatisticFilterUserListRecord::model()->cache(20)->findAll();;
        $customFilter = $statistic->createFilter();
        $customFilter->id = 'customFilter';
        $customFilter->joinType = $this->app()->request->getParam($customFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $customFilter->value = $this->app()->request->getParam('customFilter');
        $customFilter->data = ArrayUtils::getColumn($customFilterList, 'name', 'id');
        $customFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toIntArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    $listIn = implode(',', $values);
                    $tableFilters = StatisticFilterUserRecord::model()->tableName();
                    //оптимальный вариант
                    $criteria->join = "LEFT JOIN `users` as tempUser ON tempUser.id=t.user_id INNER JOIN `$tableFilters` tempFilter ON tempFilter.email=tempUser.email AND tempFilter.list_id IN ($listIn)";

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $customFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Users lists'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');
            $html .= CHtml::link('+/-', ['statisticCustomFilterUser/list'], ['class' => 'btn btn-success', 'style' => 'line-height: 19px;']);

            $html .= '</div>';

            return $html;
        };

        /** FILTER PROMOCODES */
        $promocodes = $statistic->createFilter();
        $promocodes->id = 'promocodesFilter';
        $promocodes->joinType = $this->app()->request->getParam($promocodes->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $promocodes->value = $this->app()->request->getParam('promocodesFilter');
        $promocodes->data = [
            'hasPromocode' => $this->t('Orders with promocodes'),
            'hasPromocodeBenefice' => $this->t('Orders with promocodes for payments'),
        ];
        $promocodes->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $alias = $statistic->getSelector()->getTableAlias();
            $conditions = [
                'hasPromocode' => "NOT $alias.promocode=''",
                'hasPromocodeBenefice' => 'promo.benefice_user_id > 0',
            ];

            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);
                $conditionsFilters = array_intersect_key($conditions, array_flip($values));

                if ($conditionsFilters) {
                    $criteria = new CDbCriteria();
                    $criteria->with = 'promo';
                    //важен порядок добавления в критерию!!!
                    foreach ($conditionsFilters as $conditionFilter) {
                        $criteria->addCondition($conditionFilter, 'OR');
                    }

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $promocodes->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Promocodes'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** MOVED SEARCH ENGINE */
        $addOrdersPaidFilter = $statistic->createFilter();
        $addOrdersPaidFilter->id = 'addOrdersPaidFilter';
        $addOrdersPaidFilter->joinType = StatisticFilter::JOIN_AND;
        $addOrdersPaidFilter->value = $this->app()->request->getParam($addOrdersPaidFilter->id);
        $addOrdersPaidFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value && $statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
                $statistic->setModelClass(StatisticOrdersPaidModel::class);
            }
        };
        $addOrdersPaidFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::label($this->t('Collection of data on payments'), $filter->id);

            $html .= CHtml::dropDownList($filter->id, Cast::toUInt($filter->value), ['No', 'Yes']);

            $html .= '</div>';

            return $html;
        };

        /** USER URL REFERER */
        $usersUrlRefererFind = OrderRecord::model()->cache(60)->orderedAtGreater(strtotime('today -1 month'))
            ->orderedAtLower(time())->findColumnDistinct('url_referer');
        $usersUrlRefererFind = array_unique(
            array_filter(
                array_map(function ($url) {
                    return parse_url($url, PHP_URL_HOST);
                }, $usersUrlRefererFind)
            )
        );
        $usersUrlRefererFind = empty($usersUrlRefererFind) ? ['' => $this->t('NO DATA')] : $usersUrlRefererFind;

        $usersUrlRefererFilter = $statistic->createFilter();
        $usersUrlRefererFilter->id = 'usersUrlReferer';
        $usersUrlRefererFilter->joinType = $this->app()->request->getParam($usersUrlRefererFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $usersUrlRefererFilter->value = $this->app()->request->getParam($usersUrlRefererFilter->id);
        $usersUrlRefererFilter->data = array_combine($usersUrlRefererFind, $usersUrlRefererFind);
        $usersUrlRefererFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    //важен порядок добавления в критерию!!!
                    foreach ($values as $value) {
                        $criteria->addSearchCondition('url_referer', $value, true, 'OR');
                    }

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $usersUrlRefererFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);

            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('The user came from...'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** UTM PARAMS UTM_CONTENT */
        $utmParamsFind = OrderRecord::model()->cache(60)->orderedAtGreater(strtotime('today -1 month'))
            ->orderedAtLower(time())->findColumnDistinct('utm_params');

        $utmParamsFind = array_unique(
            array_filter(
                array_map(function ($raw) {
                    $data = json_decode($raw, true);
                    if (isset($data['utm_content'])) {
                        return $data['utm_content'];
                    }

                    return false;
                }, $utmParamsFind)
            )
        );
        //CVarDumper::dump($utmParamsFind, 100, true);
        $utmParamsFind = empty($utmParamsFind) ? ['' => $this->t('NO DATA')] : $utmParamsFind;

        $utmContentFilter = $statistic->createFilter();
        $utmContentFilter->id = 'utmParams';
        $utmContentFilter->joinType = $this->app()->request->getParam($utmContentFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $utmContentFilter->value = $this->app()->request->getParam($utmContentFilter->id);
        $utmContentFilter->data = array_combine($utmParamsFind, $utmParamsFind);
        $utmContentFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    //важен порядок добавления в критерию!!!
                    foreach ($values as $value) {
                        $rowFind = "\"utm_content\":\"$value\"";
                        $criteria->addSearchCondition('utm_params', $rowFind, true, 'OR');
                    }

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $utmContentFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);

            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Date of dispatch/campaign content'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        $orderTypes = [
            StatisticOrdersModel::ORDER_TYPE_ALL => $this->t('Any'),
            StatisticOrdersModel::ORDER_TYPE_OWN => $this->app()->name . ' ' . $this->t('orders'),
            StatisticOrdersModel::ORDER_TYPE_DROP_SHIPPING => $this->t('Dropshipping orders'),
        ];
        $orderTypeFilter = $statistic->createFilter();
        $orderTypeFilter->id = 'orderType';
        $orderTypeFilter->joinType = StatisticFilter::JOIN_AND;
        $orderTypeFilter->value = $this->app()->request->getParam($orderTypeFilter->id, StatisticOrdersModel::ORDER_TYPE_OWN);
        $orderTypeFilter->data = $orderTypes;
        $orderTypeFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $value = Cast::toStr($filter->value);
                $criteria = new CDbCriteria();
                $alias = $statistic->getSelector()->getTableAlias();

                switch ($value) {
                    case StatisticOrdersModel::ORDER_TYPE_OWN:
                        $criteria->addColumnCondition(["$alias.is_drop_shipping" => 0]);
                        $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                        break;

                    case StatisticOrdersModel::ORDER_TYPE_DROP_SHIPPING:
                        $criteria->addColumnCondition(["$alias.is_drop_shipping" => 1]);
                        $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                        break;
                }
            }
        };
        $orderTypeFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            return CHtml::dropDownList($filter->id, $filter->value, $filter->data, ['style' => 'vertical-align:top;']);
        };

        $channelFilter = $statistic->createFilter();
        $channelFilter->id = 'channelType';
        $channelFilter->joinType = StatisticFilter::JOIN_AND;
        $channelFilter->value = $this->app()->request->getParam($channelFilter->id, '');
        $channelFilter->data = '';
        $channelFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $value = Cast::toStr($filter->value);
                $criteria = new CDbCriteria();
                $alias = $statistic->getSelector()->getTableAlias();
                $criteria->params = [
                    ":channel" => '%utm_medium":"' . $value . '"%'
                ];
                $criteria->addCondition("$alias.utm_params LIKE :channel");
                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $channelFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            return CHtml::textField($filter->id, $filter->value, [
                'style' => 'vertical-align:top;',
                'placeholder' => 'Utm-medium (channel)',
            ]);
        };

        /** SET FILTERS */
        $statistic->addFilters([$userOfferFilter, $userCustomOfferFilter, $userCustomOfferAddFilter, $customFilter,
            $promocodes, $addOrdersPaidFilter, $usersUrlRefererFilter, $utmContentFilter, $orderTypeFilter, $channelFilter]);
    }

    /**
     * Инициализация фильтров сецифичные по выкупам заказов
     *
     * @param Statistic $statistic
     *
     * @throws CException
     */
    protected function _initOrdersBuyoutFilters(Statistic $statistic)
    {
        $controller = $this;

        /** CUSTOM FILTER */
        $customFilterList = StatisticFilterUserListRecord::model()->cache(20)->findAll();;
        $customFilter = $statistic->createFilter();
        $customFilter->id = 'customFilter';
        $customFilter->joinType = $this->app()->request->getParam($customFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $customFilter->value = $this->app()->request->getParam('customFilter');
        $customFilter->data = ArrayUtils::getColumn($customFilterList, 'name', 'id');
        $customFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toIntArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    $listIn = implode(',', $values);
                    $tableFilters = StatisticFilterUserRecord::model()->tableName();
                    //оптимальный вариант
                    $criteria->join = "LEFT JOIN `users` as tempUser ON tempUser.id=t.user_id INNER JOIN `$tableFilters` tempFilter ON tempFilter.email=tempUser.email AND tempFilter.list_id IN ($listIn)";

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $customFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Users lists'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');
            $html .= CHtml::link('+/-', ['statisticCustomFilterUser/list'], ['class' => 'btn btn-success', 'style' => 'line-height: 19px;']);

            $html .= '</div>';

            return $html;
        };

        /** USER URL REFERER */
        $usersUrlRefererFind = OrderRecord::model()->cache(60)->orderedAtGreater(strtotime('today -1 month'))
            ->orderedAtLower(time())->findColumnDistinct('url_referer');
        $usersUrlRefererFind = array_unique(
            array_filter(
                array_map(function ($url) {
                    return parse_url($url, PHP_URL_HOST);
                }, $usersUrlRefererFind)
            )
        );
        $usersUrlRefererFind = empty($usersUrlRefererFind) ? ['' => $this->t('NO DATA')] : $usersUrlRefererFind;

        $usersUrlRefererFilter = $statistic->createFilter();
        $usersUrlRefererFilter->id = 'usersUrlReferer';
        $usersUrlRefererFilter->joinType = $this->app()->request->getParam($usersUrlRefererFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $usersUrlRefererFilter->value = $this->app()->request->getParam($usersUrlRefererFilter->id);
        $usersUrlRefererFilter->data = array_combine($usersUrlRefererFind, $usersUrlRefererFind);
        $usersUrlRefererFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    //важен порядок добавления в критерию!!!
                    foreach ($values as $value) {
                        $criteria->addSearchCondition('url_referer', $value, true, 'OR');
                    }

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $usersUrlRefererFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);

            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('The user came from...'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** UTM PARAMS UTM_CONTENT */
        $utmParamsFind = OrderRecord::model()->cache(60)->orderedAtGreater(strtotime('today -1 month'))
            ->orderedAtLower(time())->findColumnDistinct('utm_params');

        $utmParamsFind = array_unique(
            array_filter(
                array_map(function ($raw) {
                    $data = json_decode($raw, true);
                    if (isset($data['utm_content'])) {
                        return $data['utm_content'];
                    }

                    return false;
                }, $utmParamsFind)
            )
        );
        $utmParamsFind = empty($utmParamsFind) ? ['' => $this->t('NO DATA')] : $utmParamsFind;

        $utmContentFilter = $statistic->createFilter();
        $utmContentFilter->id = 'utmParams';
        $utmContentFilter->joinType = $this->app()->request->getParam($utmContentFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $utmContentFilter->value = $this->app()->request->getParam($utmContentFilter->id);
        $utmContentFilter->data = array_combine($utmParamsFind, $utmParamsFind);
        $utmContentFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    //важен порядок добавления в критерию!!!
                    foreach ($values as $value) {
                        $rowFind = "\"utm_content\":\"$value\"";
                        $criteria->addSearchCondition('utm_params', $rowFind, true, 'OR');
                    }

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $utmContentFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);

            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Date of dispatch/campaign content'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        $statistic->addFilters([$customFilter, $usersUrlRefererFilter, $utmContentFilter]);
    }

    /**
     * Инициализация фильтров сецифичные пользователям
     *
     * @param Statistic $statistic
     *
     * @throws CException
     */
    protected function _initUsersFilters(Statistic $statistic)
    {
        $timeStatisticFilter = $statistic->getTimeFilter();
        $controller = $this;

        /** OFFER USERS */
        $userOfferFilter = $statistic->createFilter();
        $userOfferFilter->id = 'usersOfferList';
        $userOfferFilter->joinType = $this->app()->request->getParam($userOfferFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $userOfferFilter->value = $this->app()->request->getParam('usersOfferList');
        $userOfferFilter->data = [
            UserRecord::OFFER_PROVIDER_NONE => 'Organic',
            UserRecord::OFFER_PROVIDER_ACTIONPAY => 'ACTIONPAY',
            UserRecord::OFFER_PROVIDER_KLUMBA => 'KLUMBA',
            UserRecord::OFFER_PROVIDER_KIDSTUFF => 'KIDSTUFF',
            UserRecord::OFFER_PROVIDER_EXPO => 'EXPO',
            UserRecord::OFFER_PROVIDER_ADMITAD => 'ADMITAD',
        ];
        $userOfferFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = array_filter($filter->value, function ($item) {
                    return is_numeric($item);
                });

                $values = Cast::toUIntArr($values);
                $alias = $statistic->getSelector()->getTableAlias();

                $criteria = new CDbCriteria();
                $criteria->addInCondition($alias . '.offer_provider', $values);

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $userOfferFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);

            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => 'Offers',
                    'multiple' => 'multiple',
                    'data-tags' => 'true',
                ],
                'options' => [
                    'tokenSeparators' => [',', ' '],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** OFFER CUSTOM USERS */
        $customOffers = UserRecord::model()->offerProvider(UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN)->findColumnDistinct('offer_id');
        $userCustomOfferFilter = $statistic->createFilter();
        $userCustomOfferFilter->id = 'usersCustomOfferList';
        $userCustomOfferFilter->joinType = $this->app()->request->getParam($userCustomOfferFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $userCustomOfferFilter->value = $this->app()->request->getParam('usersCustomOfferList');
        $userCustomOfferFilter->data = array_combine($customOffers, $customOffers);
        $userCustomOfferFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);
                $alias = $statistic->getSelector()->getTableAlias();

                $criteria = new CDbCriteria();
                $criteria->addInCondition($alias . '.offer_id', $values);
                $criteria->addColumnCondition([$alias . '.offer_provider' => UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN]);

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $userCustomOfferFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => 'Custom offers',
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [',', ' '],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** OFFER CUSTOM USERS ADD */
        $userCustomOfferAddFilter = $statistic->createFilter();
        $userCustomOfferAddFilter->id = 'usersCustomOfferAddList';
        $userCustomOfferAddFilter->joinType = $this->app()->request->getParam($userCustomOfferAddFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $userCustomOfferAddFilter->value = $this->app()->request->getParam('usersCustomOfferAddList');
        $userCustomOfferAddFilter->data = [
            'yandex' => 'Yandex direct',
            'google' => 'Google adwords',
        ];
        $userCustomOfferAddFilter->apply = function ($statistic, $filter) {
            $likeCondition = [
                'yandex' => 'direct-',
                'google' => 'adw-',
            ];

            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);
                $likeFilters = array_intersect_key($likeCondition, array_flip($values));

                if ($likeFilters) {
                    $criteria = new CDbCriteria();
                    $alias = $statistic->getSelector()->getTableAlias();

                    //важен порядок добавления в критерию!!!
                    foreach ($likeFilters as $likeFilter) {
                        $criteria->addSearchCondition($alias . '.offer_id', $likeFilter, true, 'OR');
                    }

                    $criteria->addColumnCondition([$alias . '.offer_provider' => UserRecord::OFFER_PROVIDER_CUSTOM_CAMPAIGN]);

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $userCustomOfferAddFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);

            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Traffic sources'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** FILTER PROMOCODES */
        $promocodes = $statistic->createFilter();
        $promocodes->id = 'promocodesFilter';
        $promocodes->joinType = $this->app()->request->getParam($promocodes->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $promocodes->value = $this->app()->request->getParam('promocodesFilter');
        $promocodes->data = [
            'hasPromocode' => $this->t('Orders with promocode have been placed'),
            'hasPromocodeBenefice' => $this->t('Orders with promocode have been placed for payments'),
        ];
        $promocodes->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $alias = $statistic->getSelector()->getTableAlias();
            $conditions = [
                'hasPromocode' => "NOT orders.promocode=''",
                'hasPromocodeBenefice' => 'promo.benefice_user_id > 0',
            ];

            if ($filter->value) {
                $values = Cast::toStrArr($filter->value);
                $conditionsFilters = array_intersect_key($conditions, array_flip($values));

                if ($conditionsFilters) {
                    $criteria = new CDbCriteria();
                    $criteria->with = [
                        'together' => true,
                        'orders' => [
                            'together' => true,
                            'with' => 'promo',
                        ],
                    ];
                    //важен порядок добавления в критерию!!!
                    foreach ($conditionsFilters as $conditionFilter) {
                        $criteria->addCondition($conditionFilter, 'OR');
                    }

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $promocodes->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Promocodes'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                    'width' => '300px',
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** MOVED SEARCH ENGINE */
        $movedSearchEngines = [1]; //array_filter(UserRecord::model()->cache(60)->findColumnDistinct('moved_search_engine'));
        $movedSearchEngineFilter = $statistic->createFilter();
        $movedSearchEngineFilter->id = 'movedSearchEngine';
        $movedSearchEngineFilter->joinType = $this->app()->request->getParam($movedSearchEngineFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $movedSearchEngineFilter->value = $this->app()->request->getParam($movedSearchEngineFilter->id);
        $movedSearchEngineFilter->data = array_combine($movedSearchEngines, $movedSearchEngines);
        $movedSearchEngineFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = array_filter(Cast::toStrArr($filter->value));
                $alias = $statistic->getSelector()->getTableAlias();

                $criteria = new CDbCriteria();
                $criteria->addInCondition($alias . '.moved_search_engine', $values);

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $movedSearchEngineFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Web search engine'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [',', ' '],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** LANDING NUMBER */
        $land = UserRecord::model()->cache(60)->findColumnDistinct('landing_num');
        $landingsId = count($land) > 0 ? $land : [''];
        $landingFilter = $statistic->createFilter();
        $landingFilter->id = 'landingsId';
        $landingFilter->joinType = $this->app()->request->getParam($landingFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $landingFilter->value = $this->app()->request->getParam($landingFilter->id);
        $landingFilter->data = array_combine($landingsId, $landingsId);
        $landingFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = array_filter(Cast::toStrArr($filter->value));
                $alias = $statistic->getSelector()->getTableAlias();

                $criteria = new CDbCriteria();
                $criteria->addInCondition($alias . '.landing_num', $values);

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $landingFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Landing number'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [',', ' '],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');

            $html .= '</div>';

            return $html;
        };

        /** OFFER NUMBER */
        $offerFilter = $statistic->createFilter();
        $offerFilter->id = 'offersId';
        $offerFilter->joinType = $this->app()->request->getParam($offerFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $offerFilter->value = $this->app()->request->getParam($offerFilter->id);
        $offerFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = array_filter(explode(',', Cast::toStr($filter->value)));
                $values = array_map([Utf8::class, 'trim'], $values);
                $alias = $statistic->getSelector()->getTableAlias();

                $criteria = new CDbCriteria();
                $criteria->addInCondition($alias . '.offer_id', $values);

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $offerFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= CHtml::textArea($filter->id, $filter->value, ['placeholder' => $this->t('Comma separated advertising campaign')]);

            $html .= '</div>';

            return $html;
        };

        /** GOT PROMOCODE */
        $gotPromocodeFilter = $statistic->createFilter();
        $gotPromocodeFilter->id = 'gotPromocode';
        $gotPromocodeFilter->joinType = $this->app()->request->getParam($gotPromocodeFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $gotPromocodeFilter->value = $this->app()->request->getParam($gotPromocodeFilter->id);
        $gotPromocodeFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $alias = $statistic->getSelector()->getTableAlias();

                $criteria = new CDbCriteria();
                if ($filter->value == 'no') {
                    $criteria->addColumnCondition([$alias . '.present_promocode' => '']);
                } else {
                    $criteria->compare($alias . '.present_promocode', "!=''");
                }

                $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
            }
        };
        $gotPromocodeFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */

            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList(
                $filter->getJoinTypeId(),
                $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'],
                ['class' => 'btn filter-type', 'style' => 'float: left;']
            );

            $html .= '<div style="float: left; margin-left: 10px;">';
            $html .= CHtml::label($this->t('Promocode was offered at registration'), $filter->id);

            $html .= CHtml::dropDownList($filter->id, Cast::toStr($filter->value), [
                0 => $this->t('unimportant'),
                'yes' => $this->t('Yes'),
                'no' => $this->t('No'),
            ]);
            $html .= '</div>';

            $html .= '</div>';

            return $html;
        };

        /** CUSTOM FILTER */
        $customFilterList = StatisticFilterUserListRecord::model()->cache(20)->findAll();;
        $customFilter = $statistic->createFilter();
        $customFilter->id = 'customFilter';
        $customFilter->joinType = $this->app()->request->getParam($customFilter->getJoinTypeId(), StatisticFilter::JOIN_OR);
        $customFilter->value = $this->app()->request->getParam('customFilter');
        $customFilter->data = ArrayUtils::getColumn($customFilterList, 'name', 'id');
        $customFilter->apply = function ($statistic, $filter) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            if ($filter->value) {
                $values = Cast::toIntArr($filter->value);

                if ($values) {
                    $criteria = new CDbCriteria();
                    $listIn = implode(',', $values);
                    $mainTableAlias = $statistic->getSelector()->tableAlias;
                    $statistic->getSelector()->getDbCriteria()->select = "$mainTableAlias.*";
                    $tableFilters = StatisticFilterUserRecord::model()->tableName();
                    //оптимальный вариант
                    $criteria->join = " INNER JOIN `$tableFilters` tempFilter ON tempFilter.email=$mainTableAlias.email AND tempFilter.list_id IN ($listIn)";

                    $statistic->getSelector()->dbCriteria->mergeWith($criteria, $filter->joinType);
                }
            }
        };
        $customFilter->renderHtml = function ($statistic, $filter) use ($controller) {
            /**
             * @var StatisticFilter $filter
             * @var Statistic $statistic
             */
            $html = '<div class="input-prepend custom-filter">';

            $html .= CHtml::dropDownList($filter->getJoinTypeId(), $filter->getJoinType(),
                [StatisticFilter::JOIN_OR => 'OR', StatisticFilter::JOIN_AND => 'AND'], ['class' => 'btn filter-type']);
            $html .= $controller->widget('bootstrap.widgets.TbSelect2', [
                'name' => $filter->id,
                'value' => is_array($filter->value) ? ArrayUtils::onlyNonEmpty($filter->value) : $filter->value,
                'data' => $filter->data,
                'asDropDownList' => true,
                'htmlOptions' => [
                    'placeholder' => $this->t('Users lists'),
                    'multiple' => 'multiple',
                ],
                'options' => [
                    'tokenSeparators' => [","],
                ],
            ], true);
            $html .= CHtml::hiddenField($filter->id, '');
            $html .= CHtml::link('+/-', ['statisticCustomFilterUser/list'], ['class' => 'btn btn-success', 'style' => 'line-height: 19px;']);
            $html .= <<<FORM_START
<div style="font-size: 14px;">
    <a href="javascript:void(0)" data-toggle="modal" data-target="#new-segment-modal">Создать сегмент</a>
    <form style="margin:0;" method="POST" onsubmit="this.action=window.location.href">
    <div id="new-segment-modal" class="modal hide fade" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>Название сегмента</h4>
        </div>
        <div class="modal-body">
            <div class="form">
                <input name="segment" require type="text" data-not-filter="true">
FORM_START;
            $html .= $this->widget('bootstrap.widgets.TbButton', [
                'buttonType' => TbButton::BUTTON_AJAXSUBMIT,
                'label' => 'Create',
                'ajaxOptions' => [
                    "beforeSend" => "js:function() {
                            var modalBody = $('#new-segment-modal').find('.modal-body');
                            modalBody.find('.form').addClass('hide');
                            modalBody.find('.loading').removeClass('hide');
                        }",
                    "complete" => "js:function() {
                            var modalBody = $('#new-segment-modal').find('.modal-body');
                            modalBody.find('.form').removeClass('hide');
                            modalBody.find('.loading').addClass('hide');
                        }",
                    "success" => 'js:function(data) {
                            if (data.errors.length) {
                                alert(data.errors[0]);
                                return;
                                
                            } else {
                                var modalBody = $("#new-segment-modal").find(".modal-body");
                                modalBody.find("input[name=segment]").val("");
                            }
                            
                            if (data.info.length) {
                                alert(data.info.join("\n"));
                            }
                            
                            window.location.reload();
                        }',
                    "error" => "js:function() {
                            alert('Processing error');
                        }",
                ],
            ], true);
            $html .= CHtml::hiddenField($this->app()->request->csrfTokenName, $this->app()->request->getCsrfToken(), ['id' => false, 'data-not-filter' => true]);
            $html .= CHtml::hiddenField("submit", "segment", ['data-not-filter' => true]);
            $html .= <<<FORM_END
                </div>
            <div class="loading hide">
                <h4>Обработка...</h4>
            </div>
        </div>
    </div>
    </form>
</div>
FORM_END;
            $html .= '</div>';

            return $html;
        };

        /** SET FILTERS */
        $statistic->addFilters([$userOfferFilter, $userCustomOfferFilter, $userCustomOfferAddFilter,
            $promocodes, $movedSearchEngineFilter, $customFilter, $landingFilter, $offerFilter, $gotPromocodeFilter]);
    }

    /**
     * @param string $name
     * @param Statistic $statistic
     *
     * @return Reply
     */
    protected function _createSegment($name, $statistic)
    {
        $result = new Reply();
        $model = new StatisticFilterUserListRecord();
        $model->name = $name;

        if ($model->save()) {
            $selectorCopy = $statistic->getSelectorCopy();
            $selectorCopy->getDbCriteria()->select = 'email';
            $selectorCopy->getDbCriteria()->distinct = true;
            $equals = [];
            $selectCommand = $selectorCopy->getSqlCommand($equals);
            $equalEmail = $equals['email'];

            $sqlProvider = new CSqlDataProvider($selectCommand, [
                'params' => $selectCommand->params,
                'pagination' => [
                    'pageSize' => 300,
                    'pageVar' => '__internal-page-segment',
                ],
            ]);
            $sqlProvider->setTotalItemCount((int)$selectorCopy->copy()->count());
            $dataIterator = new CDataProviderIterator($sqlProvider);
            $modelsCountNeedCreate = 0;
            $modelsCountCreated = 0;

            $saveStatisticFilterUser = function ($models) use (&$modelsCountCreated, $result) {
                /* @var StatisticFilterUserRecord[] $models */
                $resultSave = UnShardedActiveRecord::saveMany($models);
                if ($resultSave) {
                    $modelsCountCreated += count($models);
                } else {
                    $firstError = '';
                    foreach ($models as $modelFilterUserError) {
                        if ($modelFilterUserError->hasErrors()) {
                            $errors = $modelFilterUserError->getErrors();
                            $firstError = reset($errors)[0];
                        }
                    }
                    if ($firstError) {
                        $result->addError($firstError);
                    }
                }

                return $resultSave;
            };

            /* @var StatisticFilterUserRecord[] $models */
            $models = [];
            foreach ($dataIterator as $key => $item) {
                $modelsCountNeedCreate++;
                $modelFilterUser = new StatisticFilterUserRecord();
                $modelFilterUser->list_id = $model->id;
                $modelFilterUser->email = $item[$equalEmail];

                $models[] = $modelFilterUser;
                if (count($models) >= 50) {
                    $saveStatisticFilterUser($models);
                    $models = [];
                }
            }

            if (count($models) > 0) {
                $saveStatisticFilterUser($models);
                $models = [];
            }

            if ($modelsCountNeedCreate == 0) {
                $result->addError($this->t("No users found to add to the segment"));
                $model->delete();
            } elseif ($modelsCountCreated == 0) {
                $result->addError($this->t("No users were added to the segment"));
                $model->delete();
            } elseif ($modelsCountNeedCreate == $modelsCountCreated) {
                $result->addInfo($this->t("All users have been imported to the segment") . $modelsCountCreated);
                $model->refreshCountItems = true;
                $model->save();
            } else {
                $diff = $modelsCountNeedCreate - $modelsCountCreated;
                $result->addInfo("создано - $modelsCountCreated, нужно - $modelsCountNeedCreate. В сегмент не перенесено " . \Yii::t('application', '{n} пользователь|{n} пользователя|{n} пользователей', $diff));
                $model->refreshCountItems = true;
                $model->save();
            }
        } else {
            $result->setState(false);
            $errors = $model->getErrors();
            $error = reset($errors);
            $result->addError(reset($error));
        }

        return $result;
    }
}
