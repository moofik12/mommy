<?php

namespace MommyCom\Controller\Backend;

use CArrayDataProvider;
use CHttpException;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\StatementRecord;
use MommyCom\Service\BaseController\BackController;

class LabelCourierPrinterController extends BackController
{
    /**
     * @var string
     */
    public $layout = '//layouts/printable';

    public function actionDocuments(array $id)
    {
        $provider = new CArrayDataProvider(
            OrderRecord::model()
                ->idIn($id, 1000)
                ->with('positions')
                ->findAll(),
            [
                'pagination' => false,
            ]
        );

        $this->render('documents', [
            'provider' => $provider,
        ]);
    }

    public function actionStatement($id)
    {
        $model = StatementRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $this->render('statement', [
            'model' => $model,
        ]);
    }
} 
