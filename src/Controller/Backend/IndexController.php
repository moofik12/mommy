<?php

namespace MommyCom\Controller\Backend;

use MommyCom\Model\Db\HelperPageCategoryRecord;
use MommyCom\Service\BaseController\BackController;

class IndexController extends BackController
{
    /**
     * @var string
     */
    public $defaultAction = 'helper';

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionHelper()
    {
        $categories = HelperPageCategoryRecord::model()->findAll();

        $this->render('helper', [
            'categories' => $categories,
            'isSupplier' => $this->app()->user->getModel()->is_supplier,
        ]);
    }
}
