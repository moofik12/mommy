<?php

namespace MommyCom\Controller\Backend;

use CActiveForm;
use CActiveRecord;
use CHttpException;
use CSort;
use MommyCom\Model\Backend\CallcenterCallbackForm;
use MommyCom\Model\Db\CallbackRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class CallcenterCallbackController extends BackController
{
    public function actionIndex()
    {
        $selector = CallbackRecord::model();
        $userLike = $this->app()->request->getParam('user', '');

        if ($userLike) {
            $selector->with([
                'user' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'nameLike' => $userLike,
                        'surnameLike' => $userLike,
                        'emailLike' => $userLike,
                    ],
                ],
            ]);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('CallbackRecord', []),
                'scenario' => 'searchPrivileged',
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('index', ['provider' => $provider]);
    }

    public function actionView($id)
    {
        $id = Cast::toUInt($id);
        /** @var CallbackRecord $model */
        $model = CallbackRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('view', ['model' => $model]);
            $this->app()->end();
        }

        $this->render('view', ['model' => $model]);
    }

    public function actionQueue()
    {
        $model = CallbackRecord::model()
            ->queue()
            ->find();

        if ($model instanceof CActiveRecord) {
            $this->redirect(['processing', 'id' => $model->getPrimaryKey()]);
        }

        $this->redirect(['processing']);
    }

    public function actionProcessing($id = 0)
    {
        $id = Cast::toUInt($id);
        /** @var CallbackRecord $model */
        $model = CallbackRecord::model()->findByPk($id);
        $user = $this->app()->user;

        if ($model === null) {
            $this->render('emptyData');
            $this->app()->end();
        }

        if (!$model->bindToAdmin($user->id)) {
            $this->render('processIsBusy', ['model' => $model]);
            $this->app()->end();
        }

        /** @var $form TbForm */
        $formCallback = new CallcenterCallbackForm();
        $formCallback->callcenterComment = $model->comment_callcenter;
        $form = TbForm::createForm('widgets.form.config.callcenterCallback', $this, [
            'type' => 'vertical',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
        ], $formCallback);
        $form->action = ['update', 'id' => $model->id];

        $providerHistory = CallbackRecord::model()
            ->userId($model->user_id)
            ->notIdIn([$model->id])
            ->getDataProvider(false, [
                    'pagination' => [
                        'pageSize' => 50,
                    ],
                    'sort' => [
                        'defaultOrder' => [
                            'created_at' => CSort::SORT_DESC,
                        ],
                    ],
                ]
            );

        $this->render('processing', [
            'model' => $model,
            'form' => $form,
            'providerHistory' => $providerHistory,
        ]);
    }

    public function actionUpdate($id)
    {
        $id = Cast::toUInt($id);
        $model = CallbackRecord::model()->findByPk($id);
        $status = false;

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /** @var $form TbForm */
        $formCallback = new CallcenterCallbackForm();
        $form = TbForm::createForm('widgets.form.config.callcenterCallback', $this, [
            'type' => 'vertical',
        ], $formCallback);

        //нажатие кнопки
        if ($form->clicked('processed')) {
            $status = CallbackRecord::STATUS_PROCESSED;
        } elseif ($form->clicked('canceled')) {
            $status = CallbackRecord::STATUS_CANCELED;
        } elseif ($form->clicked('postponed')) {
            $status = CallbackRecord::STATUS_POSTPONED;
            $formCallback->setScenario('postponed');
        }

        if ($this->app()->request->isAjaxRequest) {
            echo CActiveForm::validate($formCallback);
            $this->app()->end();
        }

        $form->loadData();
        if ($status !== false && $form->validate()) {
            $model->comment_callcenter = $formCallback->callcenterComment;
            $model->next_callback_at = empty($formCallback->timeString) ? 0 : $formCallback->getNextCallTime();
            $model->status = $status;
            if ($model->save()) {
                $model->unbindFromAdmin();
                $this->redirect(['queue']);
            }
        }

        $this->redirect($this->app()->request->urlReferrer);
    }

    /**
     * обрабатывает AJAX запрос на продление редактирования заказа
     */
    public function actionProcessingActiveUpdate($id)
    {
        $id = Cast::toUInt($id);
        $result = new Reply();

        /** @var OrderRecord $model */
        $model = CallbackRecord::model()->findByPk($id);

        if ($model === null) {
            $result->addError($this->t('No data to process'));
            $result->setState(false);
        }

        if ($model->bindToAdmin($this->app()->user->id)) {
            $result->addInfo($this->t('The time for the current order has been extended'));
        } else {
            $result->addError($this->t('Failed to extend edit time'));
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * проверка новых звонков
     */
    public function actionAjaxCheckQueue()
    {
        $result = new Reply();

        $model = CallbackRecord::model()
            ->queue()
            ->find();

        if ($model === null) {
            $result->addError($this->t('No new orders'));
        } else {
            $result->addInfo($this->t('New order for processing'));
        }

        $this->renderJson($result->getResponse());
    }
}
