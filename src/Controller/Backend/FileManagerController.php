<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CLogRouter;
use CUploadedFile;
use MommyCom\Service\BaseController\BackController;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Yii;

class FileManagerController extends BackController
{
    public $filesRoot;
    public $filesUrl;

    public $redactorFolder;
    public $redactorUrl;

    public function init()
    {
        $this->filesRoot = Yii::getPathOfAlias('webroot') . '/uploads/';
        $this->filesUrl = $this->app()->baseUrl . '/uploads/';

        $this->redactorFolder = $this->filesRoot . 'redactor/';
        $this->redactorUrl = $this->filesUrl . 'redactor/';

        //workaround, remove from json data YiiDebugToolbar
        /** @var CLogRouter $loger */
        $logger = $this->app()->getComponent('log');
        $this->app()->detachEventHandler('onEndRequest', [$logger, 'processLogs']);
    }

    /**
     * @property-description загрузка файлов через Redactor
     *
     * @param string $subdir
     */
    public function actionRedactorImageUpload($subdir = '')
    {
        $result = [];
        $path = $this->filesRoot . $subdir;
        $this->_linkFolder($path);
        $path = realpath($path) . DIRECTORY_SEPARATOR;
        $url = $this->filesUrl . $subdir;

        if (strrpos($url, '/') !== mb_strlen($url) - 1) {
            $url .= '/';
        }

        $file = CUploadedFile::getInstanceByName('file');

        if ($file instanceof CUploadedFile) {
            $file->saveAs($path . $file->getName());
            $result['filelink'] = $url . $file->getName();
        }

        $this->renderJson($result);
    }

    /**
     * @property-description выбор файлов через Redactor
     */
    public function actionRedactorImageGetJson()
    {
        $result = [];

        $this->_linkFolder($this->redactorFolder);

        /** @var RecursiveIteratorIterator $rdir */
        $rdir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->redactorFolder), false);
        /** @var $file SplFileInfo */
        foreach ($rdir as $file) {
            if ($file->isFile()) {
                $dir = str_replace($this->redactorFolder, '', $file->getPath() . DIRECTORY_SEPARATOR);
                $dirName = substr($dir, 0, strrpos($dir, DIRECTORY_SEPARATOR));

                $result[] = [
                    'thumb' => $this->redactorUrl . $dir . $file->getFilename(),
                    'image' => $this->redactorUrl . $dir . $file->getFilename(),
                    'title' => $file->getFilename(),
                    'folder' => empty($dirName) ? '' : $dirName,
                ];
            }
        }

        $this->renderJson($result);
    }

    /**
     * @param $dir
     *
     * @throws CHttpException
     */
    protected function _linkFolder($dir)
    {
        if (is_dir($dir)) {
            return;
        }

        if (!mkdir($dir, 0777, true)) {
            throw new CHttpException(404, 'Невозможно сооздать папку');
        }
    }
}
