<?php

namespace MommyCom\Controller\Backend;

use CActiveForm;
use CHttpException;
use MommyCom\Model\Backend\WarehouseCancellationForm;
use MommyCom\Model\Backend\WarehouseReceptionForm;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class WarehouseController extends BackController
{
    public function actionIndex($warehouseCreatedRange = '', $warehouseUpdatedRange = '')
    {
        $selector = WarehouseProductRecord::model()
            ->groupBy('event_product_id')
            ->orderBy('event_product_id', 'desc');
        /* @var $selector WarehouseProductRecord */

        if (!empty($warehouseCreatedRange)) {
            list($createdAtFrom, $createdAtTo) = array_map('strtotime', explode(' - ', $warehouseCreatedRange)) + [0, 0];
            $selector->createdAtBetween($createdAtFrom, $createdAtTo);
        } else {
            $createdAtFrom = 0;
            $createdAtTo = 0;
        }

        if (!empty($warehouseUpdatedRange)) {
            list($updatedAtFrom, $updatedAtTo) = array_map('strtotime', explode(' - ', $warehouseUpdatedRange)) + [0, 0];
            $selector->updatedAtBetween($updatedAtFrom, $updatedAtTo);
        } else {
            $updatedAtFrom = 0;
            $updatedAtTo = 0;
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View list of products'));
        $this->render('index', [
            'provider' => $provider,
            'warehouseCreatedRange' => $warehouseCreatedRange,
            'warehouseUpdatedRange' => $warehouseUpdatedRange,
            'createdAtFrom' => $createdAtFrom,
            'createdAtTo' => $createdAtTo,
            'updatedAtFrom' => $updatedAtFrom,
            'updatedAtTo' => $updatedAtTo,
        ]);
    }

    public function actionProductReception()
    {
        $warehouseForm = new WarehouseReceptionForm();

        if ($this->app()->request->isAjaxRequest) {
            echo CActiveForm::validate([$warehouseForm]);
            $this->app()->end();
        }

        $warehouseForm->attributes = $this->app()->request->getPost('WarehouseReceptionForm'); // force, sorry for that

        $form = TbForm::createForm($warehouseForm->getFormConfig(), $this, [
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'focus' => [$warehouseForm, 'warehouseId'],
        ], $warehouseForm);
        /* @var $form TbForm */

        if ($form->submitted('addProduct')) {
            $form->validate(); // just validate
        }

        $provider = $warehouseForm->getConnectedWarehousesProvider();

        if ($form->submitted('submit') && $form->validate()) {
            $warehouseForm->save();
        }

        $this->commitView(null, $this->t('Receive products'));
        $this->render('productReception', [
            'form' => $form,
            'provider' => $provider,
        ]);
    }

    public function actionProductCancellation()
    {
        $warehouseForm = new WarehouseCancellationForm();

        if ($this->app()->request->isAjaxRequest) {
            echo CActiveForm::validate([$warehouseForm]);
            $this->app()->end();
        }

        $warehouseForm->attributes = $this->app()->request->getPost('WarehouseCancellationForm'); // force, sorry for that

        $form = TbForm::createForm($warehouseForm->getFormConfig(), $this, [
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
            'focus' => [$warehouseForm, 'warehouseId'],
        ], $warehouseForm);
        /* @var $form TbForm */

        if ($form->submitted('addProduct')) {
            $form->validate(); // just validate
        }

        $provider = $warehouseForm->getConnectedWarehousesProvider();

        if ($form->submitted('submit') && $form->validate()) {
            $warehouseForm->save();
        }

        $this->commitView(null, $this->t('Cancel products'));
        $this->render('productCancellation', [
            'form' => $form,
            'provider' => $provider,
        ]);
    }

    /**
     * Возвращает таблицу заказа по которой был запрос
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionGetRequestFile($id)
    {
        $model = WarehouseProductRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'Warehouse product not found');
        }

        $file = $model->getRequestFile();
        if (!$file->isExists()) {
            throw new CHttpException(404, 'Order table not found');
        }

        $this->app()->request->sendFile("order_table_for_warehouse_product_$id.csv", $file->getFilesystem()->getFileContent($file->getId()), 'text/csv');
    }
}
