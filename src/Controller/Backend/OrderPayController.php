<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use Exception;
use LogicException;
use MommyCom\Model\Backend\BankRefundForm;
use MommyCom\Model\Backend\OrderPaidForm;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PayFromSettlementAccountRecord;
use MommyCom\Model\Db\PayGatewayLogRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Model\PayGateway\PayGatewayReceiveResult;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use TbForm;

class OrderPayController extends BackController
{
    /**
     * @property-description История оплат заказов
     */
    public function actionPaid()
    {
        $selector = PayGatewayRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('PayGatewayRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('History of order payments'));
        $this->render('paid', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Проблемные оплаты
     */
    public function actionPaidErrors()
    {
        $selector = PayGatewayRecord::model()->payStatusIn(PayGatewayRecord::getErrorPayStatuses())->statusIs(PayGatewayReceiveResult::RECEIVE_STATUS_OK);

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('PayGatewayRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'View problematic payments list');
        $this->render('paidErrors', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param int $id
     * @param $status
     *
     * @throws CHttpException
     * @property-description Отмена проблемных оплат
     */
    public function actionPaidCanceled($id, $status)
    {
        $id = Cast::toUInt($id);
        $status = Cast::toUInt($status);
        $availableToStatuses = [
            PayGatewayRecord::PAY_STATUS_CANCELED_ORDER_PAY,
        ];

        $model = PayGatewayRecord::model()->findByPk($id);

        if (!$model) {
            throw new CHttpException(404, $this->t("Model not found"));
        } elseif (!$model->isErrorPayStatus()) {
            throw new CHttpException(403, $this->t("This payment does not contain crediting errors"));
        }

        if (!in_array($status, $availableToStatuses)) {
            throw new CHttpException(403, $this->t("Impossible to change the payment for the current status"));
        }

        $model->pay_status = $status;

        if (!$model->save()) {
            throw new CHttpException(500, $this->t("Updating status error"));
        }
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     * @property-description Создание возврата для проблемных оплат
     */
    public function actionPaidCreateReturn($id)
    {
        $id = Cast::toUInt($id);

        $model = PayGatewayRecord::model()->findByPk($id);

        if (!$model) {
            throw new CHttpException(404, $this->t("Model not found"));
        } elseif (!$model->order) {
            throw new CHttpException(404, $this->t("Model not order"));
        } elseif ($model->order_bank_refund_id > 0) {
            throw new CHttpException(404, $this->t("Refund") . " No.{$model->order_bank_refund_id} " . $this->t("is already recorded in payment"));
        }

        $hasReturn = OrderBankRefundRecord::model()->orderId($model->invoice_id)->find();

        if ($hasReturn) {
            throw new CHttpException(403, $this->t("Found refund") . " No.{$hasReturn->id}" .
                $this->t("for the order") . $hasReturn->order_id . " " . $this->t("for the amount") . $hasReturn->price_refund);
        }

        $result = OrderBankRefundRecord::refundOrder($model->order, $model->amount);

        if (!$result) {
            throw new CHttpException(500, $this->t("Error creating return"));
        } else {
            $orderReturnPay = OrderBankRefundRecord::model()->orderId($model->invoice_id)->orderBy('created_at', 'desc')->find();

            if ($orderReturnPay === null) {
                throw new CHttpException(500, $this->t("No return found for recording"));
            }

            $model->order_bank_refund_id = $orderReturnPay->id;
            $model->pay_status = PayGatewayRecord::PAY_STATUS_CREATED_RETURN;
            if (!$model->save()) {
                throw new CHttpException(500, $this->t("Error while issuing a refund payment"));
            }
        }
    }

    /**
     * Заказы которые оплачены не через ИЭ (через сайт)
     *
     * @property-description Обработка оплат заказов которые оплачены на р/с
     */
    public function actionPaidFromSettlementAccount()
    {
        $selector = PayFromSettlementAccountRecord::model()->statusIs(PayFromSettlementAccountRecord::STATUS_NOT_PROCESSED);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('PayFromSettlementAccountRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'Processing of order payments made to bank account');
        $this->render('paidFromSettlementAccount', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param int $id
     *
     * @property-description Отмена оплаты через р/с
     * @throws CHttpException
     */
    public function actionPaidFromSettlementAccountWrong($id)
    {
        $model = PayFromSettlementAccountRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Record not found'));
        } elseif ($model->status != PayFromSettlementAccountRecord::STATUS_NOT_PROCESSED) {
            throw new CHttpException('500', $this->t('Updating current status is forbidden'));
        }

        $model->status = PayFromSettlementAccountRecord::STATUS_WRONG;

        if (!$model->save()) {
            throw new CHttpException('500', $this->t('Saving Error'));
        }
    }

    /**
     * @param int $id
     *
     * @property-description Подтверждение оплаты через р/с
     * @throws CHttpException
     */
    public function actionPaidFromSettlementAccountConfirm($id)
    {
        /* @var PayFromSettlementAccountRecord $model */
        $model = PayFromSettlementAccountRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Record not found'));
        } elseif ($model->status != PayFromSettlementAccountRecord::STATUS_NOT_PROCESSED) {
            throw new CHttpException('500', $this->t('Updating current status is forbidden'));
        }

        $modelForm = new OrderPaidForm();

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.orderPaid', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        if ($form->submitted('submit')) {
            if ($form->validate()) {
                $transaction = $this->app()->db->beginTransaction();

                try {
                    /** @var PayGatewayRecord $payModel */
                    $payModel = new PayGatewayRecord();

                    $payModel->status = PayGatewayReceiveResult::RECEIVE_STATUS_OK;
                    $payModel->amount = $modelForm->amount;
                    $payModel->invoice_id = $modelForm->order;
                    $payModel->is_custom = true;
                    $payModel->admin_id = $this->app()->user->id;
                    $payModel->message = $modelForm->comment;
                    $payModel->provider = $modelForm->provider;
                    $payModel->unique_payment = $modelForm->unique_payment;

                    if (!$payModel->save()) {
                        throw new LogicException($this->t('Failed to add payment'));
                    }

                    $model->pay_gateway_id = $payModel->id;
                    $model->order_id = $payModel->invoice_id;
                    $model->status = PayFromSettlementAccountRecord::STATUS_CREATED_PAYMENT;

                    if (!$model->save()) {
                        throw new LogicException($this->t('Error adding a payment entry'));
                    }

                    $transaction->commit();
                    $this->redirect(['paidFromSettlementAccount']);
                } catch (Exception $e) {
                    $transaction->rollback();

                    throw new CHttpException('400', $e->getMessage());
                }
            }
        } else {
            $modelForm->amount = $model->amount;
            $modelForm->order = $model->possible_order_id;
            $modelForm->comment = $model->pay_comment;
            $modelForm->provider = $model->provider;
            $modelForm->unique_payment = $model->unique_payment;
        }

        $this->commitView(null, 'Подтверждение оплаты через р/с (создание платежа)');
        $this->render('createPaid', ['form' => $form]);
    }

    /**
     * Заказы которые оплачены не через ИЭ (через сайт)
     *
     * @property-description История оплат заказов через р/с
     */
    public function actionPaidFromSettlementAccountHistory()
    {
        $selector = PayFromSettlementAccountRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('PayFromSettlementAccountRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'Просмотр списка оплат которые оплачены на р/с');
        $this->render('paidFromSettlementAccountHistory', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Возвраты денег оплачееные по б/н
     */
    public function actionBankRefundPayReady()
    {
        $selector = OrderBankRefundRecord::model()->onlyPayReady();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->with('order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderBankRefundRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'View a list of refunds');
        $this->render('bankRefundPayReady', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Подтверждение возврата денег оплаченные по б/н
     */
    public function actionBankRefundConfirmReady()
    {
        $selector = OrderBankRefundRecord::model()
            ->status(OrderBankRefundRecord::STATUS_UNCONFIGURED);

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->with('order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderBankRefundRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'Просмотр списка возврата оплат (способ возврата)');
        $this->render('bankRefundConfirmReady', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description История возвратов денег оплаченные по б/н
     */
    public function actionBankRefund()
    {
        $selector = OrderBankRefundRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->with('order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderBankRefundRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'View a list refund confirmations');
        $this->render('bankRefund', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Ручное создание оплаты б/н за заказ
     */
    public function actionCreatePaid($id = 0)
    {
        /** @var PayGatewayRecord $model */
        $model = new PayGatewayRecord();

        $modelForm = new OrderPaidForm();

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.orderPaid', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        $order = OrderRecord::model()->findByPk($id);
        if ($order instanceof OrderRecord) {
            $modelForm->amount = $order->getPayPrice();
            $modelForm->order = $order->id;
            $modelForm->comment = 'Оплата заказа №' . $order->getIdAligned();
        }

        if ($form->submitted('submit') && $form->validate()) {
            $model->status = PayGatewayReceiveResult::RECEIVE_STATUS_OK;
            $model->amount = $modelForm->amount;
            $model->invoice_id = $modelForm->order;
            $model->is_custom = true;
            $model->admin_id = $this->app()->user->id;
            $model->message = $modelForm->comment;
            $model->provider = $modelForm->provider;
            $model->unique_payment = $modelForm->unique_payment;

            if (!$model->save()) {
                throw new CHttpException('400', $this->t('Error creating payment for order'));
            }

            $this->redirect(['paid']);
        }

        $this->commitView(null, 'Manual non-cash payment creation for order');
        $this->render('createPaid', ['form' => $form]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @property-description Фиксирование выплаты
     */
    public function actionBankRefundConfirm($id)
    {
        /** @var OrderBankRefundRecord $model */
        $model = OrderBankRefundRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Record not found'));
        } elseif (!$model->isAvailableEdit()) {
            throw new CHttpException('500', $this->t('Disable editing'));
        } elseif ($model->order === null) {
            throw new CHttpException('404', $this->t('No order was found'));
        }

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.bankRefundConfirm', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model->status = OrderBankRefundRecord::STATUS_CONFIGURED;

            if ($model->save()) {
                $this->redirect(['bankRefundConfirmReady']);
            }
        }

        $this->commitView($model, $this->t('Recording a refund'));
        $this->render('bankRefundConfirm', ['form' => $form]);
    }

    /**
     * @param int $id
     *
     * @property-description Подтверждение возврата денег
     * @throws CHttpException
     */
    public function actionBankRefundNeedPay($id)
    {
        /** @var OrderBankRefundRecord $model */
        $model = OrderBankRefundRecord::model()->with('order')->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Record not found'));
        } elseif (!$model->isAvailableEdit() && $model->status != OrderBankRefundRecord::STATUS_CONFIGURED) {
            throw new CHttpException('500', $this->t('Disable editing'));
        } elseif ($model->order === null) {
            throw new CHttpException('404', $this->t('No order was found'));
        }

        $modelForm = new BankRefundForm();
        $modelForm->amount = $model->price_refund;
        $modelForm->order = $model->order_id;
        $modelForm->order_card_payed = $model->order->card_payed;
        $modelForm->type_refund = $model->type_refund;

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.bankRefund', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        if ($form->submitted('submit') && $form->validate()) {
            $model->type_refund = $modelForm->type_refund;
            $model->status = OrderBankRefundRecord::STATUS_NEED_PAY;
            $model->transaction = $modelForm->transaction;
            $model->comment = $modelForm->comment;
            $model->admin_id = $this->app()->user->id;

            if ($model->save()) {
                $this->redirect(['bankRefundPayReady']);
            }
        }

        $this->commitView($model, $this->t('Recording return method'));
        $this->render('bankRefundNeedPay', ['form' => $form]);
    }

    /**
     * @param int $id
     *
     * @property-description Отмена возврата денег
     * @throws CHttpException
     */
    public function actionBankRefundDelete($id)
    {
        $model = OrderBankRefundRecord::model()->findByPk($id);
        $statusNotDeleted = [
            OrderBankRefundRecord::STATUS_PAYED,
        ];

        $statusesNotAvailableTimeEdit = [
            OrderBankRefundRecord::STATUS_CONFIGURED,
        ];

        if ($model === null) {
            throw new CHttpException('404', $this->t('Record not found'));
        } elseif (!$model->isAvailableEdit() && !in_array($model->status, $statusesNotAvailableTimeEdit)) {
            throw new CHttpException('500', $this->t('Disable editing'));
        } elseif (in_array($model->status, $statusNotDeleted)) {
            throw new CHttpException('500', $this->t('Updating current status is forbidden'));
        }

        $model->status = OrderBankRefundRecord::STATUS_CANCELLED;

        if (!$model->save()) {
            throw new CHttpException('500', $this->t('Saving Error'));
        }
    }

    /**
     * @param int $id
     * @param int $status
     *
     * @property-description Отмена возврата денег
     * @throws CHttpException
     */
    public function actionBankRefundRefresh($id, $status)
    {
        $availableRefreshStatuses = [
            OrderBankRefundRecord::STATUS_CONFIGURED,
            OrderBankRefundRecord::STATUS_UNCONFIGURED,
        ];

        $model = OrderBankRefundRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Record not found'));
        } elseif (!$model->isAvailableEdit()) {
            throw new CHttpException('500', $this->t('Disable editing'));
        } elseif (!in_array($status, $availableRefreshStatuses)) {
            throw new CHttpException('500', $this->t('Updating current status is forbidden'));
        }

        $model->status = $status;
        $this->commitView($model, $this->t('Return cancelation'));

        if (!$model->save()) {
            throw new CHttpException('500', $this->t('Saving Error'));
        }
    }

    /**
     * @property-description Просмотр списка логирования оплат сделанные через приват24 мерчант
     */
    public function actionLogs()
    {
        $selector = PayGatewayLogRecord::model();

        $rangeData = $this->app()->request->getParam('rangeData', '');
        $rangeData = explode('-', $rangeData);

        if (isset($rangeData[0]) && isset($rangeData[1])) {
            $startTime = strtotime('today ' . Utf8::trim($rangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($rangeData[1]));

            $selector->createdAtGreater($startTime)->createdAtLower($endTime);
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('PayGatewayLogRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, 'Просмотр логов при получении отплаты');
        $this->render('logs', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Просмотр списка заказов которые ожидают оплаты по б/н
     */
    public function actionWait()
    {
        $selector = OrderRecord::model()
            ->processingStatus(OrderRecord::PROCESSING_CALLCENTER_PREPAY, false);
        /* @var $selector OrderRecord */

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id ASC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('OrderRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, 'Просмотр заказов которые ожидают отплаты');
        $this->render('wait', [
            'provider' => $provider,
        ]);
    }
} 
