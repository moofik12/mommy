<?php

namespace MommyCom\Controller\Backend;

use MommyCom\Entity\Order;
use MommyCom\Entity\OrderTracking;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Delivery\AvailableDeliveries;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderTrackingController extends BackController
{
    public function actionIndex()
    {
        $filterEventId = $this->app()->request->getParam('filterEventId', false);

        $selector = OrderTrackingRecord::model();

        $selector->with(['order' => ['together' => true]]);

        if ($filterEventId) {
            $selector->with([
                    'order.positions' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => ['eventId' => $filterEventId]],
                ]
            );
        } else {
            $selector->with('order.positions');
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('OrderTrackingRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Смена номера ТТН
     */
    public function actionChangeTrackcode()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest() ?: new Request();
        $pk = (int)$request->request->get('pk', 0);
        $value = $request->request->get('value', 0);

        $availableDeliveries = $this->container->get(AvailableDeliveries::class);
        $objectManager = $this->getEntityManager();
        $orderTracking = $objectManager->find(OrderTracking::class, $pk);
        $order = $orderTracking->getOrderId() ? $objectManager->find(Order::class, $orderTracking->getOrderId()) : null;

        if (!$orderTracking) {
            throw new NotFoundHttpException($this->t('No document found'));
        }

        if (!$order) {
            throw new NotFoundHttpException($this->t('No order found'));
        }

        if (!$availableDeliveries->hasDelivery($orderTracking->getOrderDeliveryType())) {
            throw new BadRequestHttpException($this->t('Cannot change tracking code for this delivery type.'));
        }

        $delivery = $availableDeliveries->getDelivery($orderTracking->getOrderDeliveryType());
        $deliveryId = $delivery->getApi()->findDeliveryId($value);

        if (!$deliveryId) {
            throw new BadRequestHttpException($this->t('Tracking code is invalid.'));
        }

        $order->setTrackId($deliveryId);
        $order->setTrackcode($value);

        $orderTracking->setTrackcode($value);
        $orderTracking->setStatusPrev($orderTracking->getStatus());
        $orderTracking->setStatus(OrderTracking::STATUS_UNKNOWN);

        $objectManager->flush();

        echo 'save';
    }
}
