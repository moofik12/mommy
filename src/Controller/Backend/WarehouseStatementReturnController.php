<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CMap;
use Exception;
use MommyCom\Model\Backend\WarehouseStatementReturnForm;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use TbForm;

class WarehouseStatementReturnController extends BackController
{
    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + toggleAccountedReturns']);
    }

    /**
     * @property-description Список актов
     */
    public function actionIndex()
    {
        $request = $this->app()->request;
        $selector = WarehouseStatementReturnRecord::model();

        //filer
        $filterEventID = $request->getParam('filterEventID', false);
        $filterEventProductID = $request->getParam('filterEventProductID', false);
        $filterProductID = $request->getParam('filterProductID', false);
        $filterSupplierID = $request->getParam('filterSupplierID', false);

        if ($filterEventID) {
            $selector->with(['positions' => [
                'joinType' => 'INNER JOIN',
                'together' => true,
                'scopes' => [
                    'eventId' => [$filterEventID],
                ],
            ]]);
        }

        if ($filterEventProductID) {
            $selector->with(['positions' => [
                'joinType' => 'INNER JOIN',
                'together' => true,
                'scopes' => [
                    'eventProductId' => [$filterEventProductID],
                ],
            ]]);
        }

        if ($filterProductID) {
            $selector->with(['positions' => [
                'joinType' => 'INNER JOIN',
                'together' => true,
                'scopes' => [
                    'productId' => [$filterProductID],
                ],
            ]]);
        }

        if ($filterSupplierID) {
            $selector->supplier($filterSupplierID);
        }

        if (empty($selector->getDbCriteria()->with)) {
            $selector->with('positions')->together();
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('WarehouseStatementReturnRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Создание акта
     *
     * @param array $products ID WarehouseProductRecords
     *
     * @throws CHttpException
     */
    public function actionCreate(array $products)
    {
        $form = new WarehouseStatementReturnForm();
        $form->productsID = $products;
        $userID = $this->app()->user->id;

        if (!$form->validate()) {
            $errorsRow = array_reduce($form->errors, function ($carry, $errors) {
                return CMap::mergeArray($carry, $errors);
            }, []);

            $error = 'Ошибка при проверке создания Акта. Ошибки: <br>';
            $error .= implode('<br>', $errorsRow);

            throw new CHttpException(404, $error);
        }

        $model = new WarehouseStatementReturnRecord();
        $model->supplier_id = $form->getSupplierID();
        $model->user_id = $userID;

        if (!$model->save()) {
            $errorsRow = array_reduce($model->errors, function ($carry, $errors) {
                return CMap::mergeArray($carry, $errors);
            }, []);

            $error = 'Ошибка при создании Акта. Ошибки: <br>';
            $error .= implode('<br>', $errorsRow);

            throw new CHttpException(404, $error);
        }

        /* @var WarehouseStatementReturnProductRecord[] $statementProducts */
        $statementProducts = [];
        foreach ($form->getProducts() as $product) {
            $statementProduct = new WarehouseStatementReturnProductRecord();
            $statementProduct->statement_id = $model->id;
            $statementProduct->user_id = $userID;
            $statementProduct->product_id = $product->id;
            $statementProduct->event_id = $product->event_id;
            $statementProduct->event_product_id = $product->event_product_id;

            $statementProducts[] = $statementProduct;
        }

        if (!UnShardedActiveRecord::saveMany($statementProducts)) {
            $model->delete();

            $error = '';
            foreach ($statementProducts as $product) {
                if ($product->hasErrors()) {
                    foreach ($product->errors as $errors) {
                        $error = reset($errors);
                        break;
                    }
                }
            }

            throw new CHttpException(404, $this->t('Error when creating flash-sale items. Error: ') . print_r($error, true));
        }

        //обновляем статус товаров на складе
        $model->status = WarehouseStatementReturnRecord::STATUS_READY;
        $model->save();

        $this->commitView(null, $this->t('Create supplier return'));
        $this->redirect(['warehouseStatementReturn/update', 'id' => $model->id]);
    }

    /**
     * @property-description Проверка валидности создания акта
     *
     * @param array $products
     *
     * @throws CHttpException
     */
    public function actionCheckCreate(array $products)
    {
        $response = new Reply();

        $form = new WarehouseStatementReturnForm();
        $form->productsID = $products;

        if (!$form->validate()) {
            $response->setState(false);

            foreach ($form->errors as $errors) {
                $response->addError(reset($errors));
            }
        }

        $this->renderJson($response->getResponse());
    }

    /**
     * @property-description Редактирование акта/подтверждение
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $request = $this->app()->request;
        $model = WarehouseStatementReturnRecord::model()->with('positions')->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status != WarehouseStatementReturnRecord::STATUS_READY) {
            throw new CHttpException(404, $this->t('Act non-editable'));
        }

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.warehouseStatementReturn', $this, [
            'type' => 'vertical',
        ], $model);

        $transaction = $model->getDbConnection()->beginTransaction();
        try {
            if ($form->submitted() && $form->validate()) {
                if ($model->save()) {
                    $transaction->commit();
                }
            } elseif ($request->getPost('submit') !== null) {
                switch ($request->getPost('submit')) {
                    case 'accept':
                        $model->status = WarehouseStatementReturnRecord::STATUS_CONFIRMED;
                        $model->save();
                        break;

                    default:
                        throw new CHttpException(400, 'Unsupported action for the order');
                }

                if (!$model->hasErrors()) {
                    $transaction->commit();

                    $this->redirect(['warehouseStatementReturn/view', 'id' => $model->id]);
                }
            }
        } catch (Exception $e) {
            $transaction->rollback();
            throw new CHttpException(500, $e->getMessage());
        }

        $selectorPositions = WarehouseStatementReturnProductRecord::model()->statementId($model->id);
        $productsProvider = $selectorPositions->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
        ]);

        $this->commitView($model, $this->t('Update supplier return'));
        $this->render('update', [
            'model' => $model,
            'form' => $form,
            'productsProvider' => $productsProvider,
        ]);
    }

    /**
     * @property-description Удаление неподтвержденных актов
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $model = WarehouseStatementReturnRecord::model()->with('positions')->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status != WarehouseStatementReturnRecord::STATUS_UNCONFIRMED
            && $model->status != WarehouseStatementReturnRecord::STATUS_READY
        ) {
            throw new CHttpException(404, 'Act deletion disabled');
        }

        if (!$model->delete()) {
            throw new CHttpException(404, 'Error deleting Act');
        }
    }

    /**
     * @property-description Просмотр акта
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $model = WarehouseStatementReturnRecord::model()->with('positions')->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $selectorPositions = WarehouseStatementReturnProductRecord::model()->statementId($model->id);
        $productsProvider = $selectorPositions->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
        ]);

        $this->commitView($model, $this->t('View return to supplier act '));
        $this->render('view', [
            'model' => $model,
            'productsProvider' => $productsProvider,
        ]);
    }

    /**
     * @property-description Изменение маркера акта при формировании оплаты поставщику
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionToggleAccountedReturns($id)
    {
        /* @var WarehouseStatementReturnRecord $model */
        $model = WarehouseStatementReturnRecord::model()->findByPk($id);
        $result = [
            'success' => false,
            'error' => '',
        ];

        if ($model === null) {
            $result['error'] = $this->t('Data not found');
        } elseif ($model->created_at > strtotime('24.09.2015')) {
            $result['error'] = 'This feature is no longer supported. Adding a refund to payment is done through the menu'
                . '"Оплаты поставщикам" -> "К оплате" ->"Добавить возврат" (в колонке "Сумма возврата")';
        }

        if ($result['error']) {
            $this->renderJson($result);
        }

        if (empty($result['error']) && $model->status != WarehouseStatementReturnRecord::STATUS_CONFIRMED) {
            $statuses = WarehouseStatementReturnRecord::statuses();
            $statusValidText = isset($statuses[WarehouseStatementReturnRecord::STATUS_CONFIRMED])
                ? $statuses[WarehouseStatementReturnRecord::STATUS_CONFIRMED] : WarehouseStatementReturnRecord::STATUS_CONFIRMED;

            $result['error'] = "Изменять данные можно только для статуса '$statusValidText'";
        }

        if (empty($result['error']) && !$model->isAvailableChangeAccountedReturns() && $model->is_accounted_returns) {
            $result['error'] = "Изменение данных недоступно";
        }

        if (empty($result['error']) && $model) {
            $model->is_accounted_returns = !$model->is_accounted_returns;

            if (!$model->save()) {
                $result['error'] = "Ошибка при обновлении данных";
            } else {
                $result['success'] = true;
            }
        }

        $this->renderJson($result);
    }

    /**
     * @property-description Выбор товаров для создания акта
     */
    public function actionReadyToReturn()
    {
        $request = $this->app()->request;

        $notConfirmActs = WarehouseStatementReturnRecord::model()
            ->status(WarehouseStatementReturnRecord::STATUS_UNCONFIRMED)
            ->findColumnDistinct('id');

        /* @var WarehouseProductRecord $selector */
        $selector = WarehouseProductRecord::model()
            ->statusIn([
                WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                WarehouseProductRecord::STATUS_BROKEN,
            ])
            ->orderId(0)
            ->with([
                'supplier' => ['together' => true],
                'eventProduct' => ['together' => true],
            ])
            ->createdAtTo(strtotime('-1 day')); //нужно чтобы возврат был проведен

        if ($notConfirmActs) {
            $notConfirmActsProducts = WarehouseStatementReturnProductRecord::model()
                ->statementIdIn($notConfirmActs)
                ->findColumnDistinct('product_id');

            $selector->idInNot($notConfirmActsProducts);
        }

        //filer
        $filterSupplierID = $request->getParam('filterSupplierID', false);
        $eventProductPriceFrom = $request->getParam('eventProductPriceFrom', false);
        $eventProductPriceTo = $request->getParam('eventProductPriceTo', false);

        if ($filterSupplierID) {
            $selector->supplierId($filterSupplierID);
        }

        if ($eventProductPriceFrom) {
            $selector->eventProductPriceFrom($eventProductPriceFrom);
        }

        if ($eventProductPriceTo) {
            $selector->eventProductPriceTo($eventProductPriceTo);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC, t.event_id ASC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View products for suppliers return'));
        $this->render('readyToReturn', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Удаление товаров из акта (при создании)
     *
     * @param int $id statement ID
     * @param int $position statement position ID
     *
     * @throws CHttpException
     */
    public function actionDeletePosition($id, $position)
    {
        $statement = WarehouseStatementReturnRecord::model()->findByPk($id);

        if ($statement === null) {
            throw new CHttpException(404, $this->t('Act not found'));
        }

        if ($statement->status != WarehouseStatementReturnRecord::STATUS_UNCONFIRMED
            && $statement->status != WarehouseStatementReturnRecord::STATUS_READY
        ) {
            throw new CHttpException(404, $this->t('Act not be changed'));
        }

        $statementPosition = WarehouseStatementReturnProductRecord::model()->findByPk($position);
        if ($statementPosition === null) {
            throw new CHttpException(404, $this->t('Act position not found'));
        }

        $transaction = $statementPosition->getDbConnection()->beginTransaction();
        try {
            $statementPosition->delete();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            throw new CHttpException(500, $e->getMessage());
        }
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionFile($id)
    {
        $request = $this->app()->request;
        $df = $this->app()->dateFormatter;
        $cf = $this->app()->currencyFormatter;
        $cn = $this->app()->countries->getCurrency();
        $model = WarehouseStatementReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Statement not found'));
        }

        if ($model->status == WarehouseStatementReturnRecord::STATUS_UNCONFIRMED) {
            throw new CHttpException(404, $this->t('Act is unavailable'));
        }

        $csvDATA = [];
        $csvDATA[] = ["Возврат поставщику №{$model->id} от " . $df->format('d MMMM y', $model->created_at)];
        $csvDATA[] = [];
        $csvDATA[] = ['Supplier', $this->app()->params['company']];
        $csvDATA[] = ['Сonsignee', $model->supplier->name];
        $csvDATA[] = [];
        $csvDATA[] = [];

        $csvDATA[] = ['№', 'Warehouse No', 'Supplier\'s product number', 'Product', 'Flash-sale', 'Product No in flash-sale ', 'Type of size', 'Size', 'Qty.', 'Price', 'Amount', 'Image'];
        foreach ($model->getProducts() as $index => $product) {
            $csvDATA[] = [
                $index + 1,
                $product->id,
                $product->eventProduct->article,
                $product->eventProduct->name,
                "№{$product->event->id}: {$product->event->name}",
                $product->event_product_id,
                $product->eventProduct->sizeformatReplacement,
                $product->eventProduct->size,
                1,
                $product->eventProduct->price_purchase,
                $product->eventProduct->price_purchase,
                $product->eventProduct->product->logo->getUrl(),
            ];
        }

        $csvDATA[] = [];
        $csvDATA[] = ['Всего', $cf->format($model->getProductsPricePurchase(), $cn->getName('short'))];

        $this->commitView(null, $this->t('Download return to supplier Act'));
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = 'возврат_поставщику_' . $model->id . '_' . date('d-m-Y_H-i') . '.csv';
        $request->sendFile($filename, $csv->toString(), 'text/csv');
    }
}
