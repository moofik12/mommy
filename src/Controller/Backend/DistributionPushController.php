<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Backend\DistributionPushForm;
use MommyCom\Model\Db\ApplicationSubscriberRecord;
use MommyCom\Model\Db\DistributionPushRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class DistributionPushController extends BackController
{
    public function actionIndex()
    {
        $distributionPushTypeCountsSelector = ApplicationSubscriberRecord::model()->groupBy('type');
        $distributionPushTypeCountsSelector->getDbCriteria()->select = 'type, count(type) as amount';
        $distributionPushTypeCounts = $distributionPushTypeCountsSelector->getSqlCommand()->queryAll();
        $statisticTypes = [];
        foreach ($distributionPushTypeCounts as $item) {
            $statisticTypes[$item['type']] = $item['amount'];
        }

        $provider = DistributionPushRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 'id DESC',
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('DistributionPushRecord', []),
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
            'statisticTypes' => $statisticTypes,
        ]);
    }

    public function actionCreate()
    {
        $distributionPushForm = new DistributionPushForm();

        $form = TbForm::createForm('widgets.form.config.distributionPushAdd', $this, [
            'type' => 'vertical',
        ], $distributionPushForm);
        /* @var $form TbForm */

        if ($form->submitted('submit') && $form->validate()) {
            $distributionPush = new DistributionPushRecord();

            $distributionPush->start_at = $distributionPushForm->getStartAt();
            $distributionPush->subject = $distributionPushForm->subject;
            $distributionPush->body = $distributionPushForm->body;
            $distributionPush->url = $distributionPushForm->url;
            $distributionPush->url_desktop = $distributionPushForm->url_desktop;
            $distributionPush->status = DistributionPushRecord::STATUS_PENDING;
            if ($distributionPush->save()) {
                $this->redirect(['distributionPush/index']);
            }
        }

        $this->render('create', ['form' => $form]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $model = DistributionPushRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     */
    public function actionUpdate($id)
    {
        $distributionPushForm = new DistributionPushForm();
        $model = DistributionPushRecord::model()->findByPk($id);
        $distributionPushForm->id = $id;

        $form = TbForm::createForm('widgets.form.config.distributionPushUpdate', $this, [
            'type' => 'vertical',
        ], $distributionPushForm);
        /* @var $form TbForm */

        if (!$form->submitted('submit')) {
            $distributionPushForm->attributes = $model->attributes;
            $distributionPushForm->start_at = date('d.m.Y', $model->start_at);
            $distributionPushForm->start_time = date('H:i', $model->start_at);
        } elseif ($form->validate()) {
            $model->start_at = $distributionPushForm->getStartAt();
            $model->subject = $distributionPushForm->subject;
            $model->body = $distributionPushForm->body;
            $model->url = $distributionPushForm->url;
            if ($model->save()) {
                $this->redirect(['distributionPush/index']);
            }
        }

        $this->render('update', ['form' => $form]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $id = Cast::toUInt($id);

        $model = DistributionPushRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == DistributionPushRecord::STATUS_START) {
            throw new CHttpException(403, $this->t('Mail-out is being processed, try again later.'));
        }

        $model->delete();

        $this->redirect(['distributionPush/index']);
    }
}
