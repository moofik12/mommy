<?php

namespace MommyCom\Controller\Backend;

use CDbException;
use CHttpException;
use MommyCom\Model\Db\StatisticFilterUserListRecord;
use MommyCom\Model\Db\StatisticFilterUserRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class StatisticCustomFilterUserController extends BackController
{
    /**
     * @property-description Управление списком
     */
    public function actionList()
    {
        $model = StatisticFilterUserListRecord::model();
        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('StatisticFilterUserListRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('listIndex', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @property-description Обновление списка
     */
    public function actionListUpdate($id)
    {
        /** @var StatisticFilterUserListRecord $model */
        $model = StatisticFilterUserListRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'model not found');
        }

        $form = TbForm::createForm('widgets.form.config.statisticCustomFilterUserList', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            $model->refreshCountItems = true;
            if ($model->save()) {
                $this->redirect($this->createUrl('list'));
            }
        }

        $dataProvider = StatisticFilterUserRecord::model()->listId($model->id)->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('StatisticFilterUserRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('listUpdate', [
            'form' => $form,
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @property-description Добавление списка
     */
    public function actionListAdd()
    {
        /** @var StatisticFilterUserListRecord $model */
        $model = new StatisticFilterUserListRecord();

        $form = TbForm::createForm('widgets.form.config.statisticCustomFilterUserList', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            if ($model->save()) {
                $this->redirect($this->createUrl('list'));
            }
        }

        $dataProvider = StatisticFilterUserRecord::model()->listId($model->id)->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('StatisticFilterUserRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('listAdd', [
            'form' => $form,
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @property-description Очистка списка
     */
    public function actionListClear($id)
    {
        $model = StatisticFilterUserListRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'model not found');
        }

        $model->refreshCountItems = true;
        $clear = $model->clear();
        $model->save();

        if (!$clear) {
            throw new CHttpException(500, 'Could not clear list');
        }
    }

    /**
     * @param $id
     *
     * @throws CDbException
     * @throws CHttpException
     * @property-description Удаление списка
     */
    public function actionListDelete($id)
    {
        $model = StatisticFilterUserListRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'model not found');
        }

        if (!$model->delete()) {
            throw new CHttpException(500, 'Could not delete list');
        }
    }
}
