<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class ProductSectionController extends BackController
{
    /**
     * @property-description Просмотр списка секций товаров
     */
    public function actionIndex()
    {
        $model = ProductSectionRecord::model();
        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('ProductSectionRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Добавление секции товаров
     */
    public function actionAdd()
    {
        $model = new ProductSectionRecord();
        /* @var $model ProductSectionRecord */

        $form = TbForm::createForm('widgets.form.config.productSectionAdd', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            if ($form->validate()) {
                $model->keywords = !empty($model->keywords) && is_string($model->keywords) ? explode(',', $model->keywords) : [];
                if ($model->save()) {
                    $this->redirect(['productSection/index']);
                }
                $model->keywords = !empty($model->keywords) && is_array($model->keywords) ? implode(',', $model->keywords) : '';
            }
        }

        $this->commitView(null, $this->t('Adding product section'));
        $this->render('add', [
            'form' => $form,
        ]);
    }

    /**
     * @property-description Редактирование секции товаров
     */
    public function actionUpdate($id)
    {
        $id = Cast::toInt($id);

        $model = ProductSectionRecord::model()->findByPk($id);
        /* @var $model ProductSectionRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('No categories found'));
        }
        $model->keywords = !empty($model->keywords) && is_array($model->keywords) ? implode(',', $model->keywords) : '';
        $form = TbForm::createForm('widgets.form.config.productSectionAdd', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            if ($form->validate()) {
                $model->keywords = !empty($model->keywords) && is_string($model->keywords) ? explode(',', $model->keywords) : [];
                if ($model->save()) {
                    $this->redirect(['productSection/index']);
                }
                $model->keywords = !empty($model->keywords) && is_array($model->keywords) ? implode(',', $model->keywords) : '';
            }
        }

        $this->commitView(null, $this->t('Product section editing'));
        $this->render('update', [
            'form' => $form,
        ]);
    }

    /**
     * @property-description Поиск секций
     *
     * @param $name
     * @param $page
     * @param bool $onlyChild
     */
    public function actionAjaxSearch($name, $page, $onlyChild = false)
    {
        $result = ['pageCount' => 0, 'items' => []];

        $selector = ProductSectionRecord::model()
            ->nameLike($name);

        if ($onlyChild) {
            $selector->onlyChild();
        }
        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'currentPage' => $page,
            ],
        ]);

        $data = $provider->getData();
        $result['pageCount'] = $provider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'name'];

        foreach ($data as $item) {
            $result['items'][] = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);
        }

        $this->renderJson($result);
    }

    /**
     * @property-description Поиск секции
     *
     * @param int $id
     */
    public function actionAjaxGetById($id)
    {
        $result = new Select2Reply();
        $section = ProductSectionRecord::model()->findByPk($id);

        if ($section !== null) {
            $allowedAttributes = ['id', 'name', 'parent_id'];
            $result->addItem(ArrayUtils::valuesByKeys($section, $allowedAttributes, true));
        }

        $this->renderJson($result->getResponse());
    }
}
