<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\StatementOrderRecord;
use MommyCom\Model\Db\StatementRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;

class CourierStatementController extends BackController
{
    public function actionIndex()
    {
        $provider = StatementRecord::model()
            ->deliveryType(DeliveryTypeUa::DELIVERY_TYPE_COURIER)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('StatementRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    public function actionView($id)
    {
        $model = StatementRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $ordersProvider = OrderRecord::model()
            ->idIn(ArrayUtils::getColumn($model->positions, 'order_id'))
            ->getDataProvider(false, [
                'pagination' => false,
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('View Act'));
        $this->render('view', [
            'model' => $model,
            'ordersProvider' => $ordersProvider,
        ]);
    }

    public function actionAdd(array $id)
    {
        $id = Cast::toUIntArr($id);
        $orders = OrderRecord::model()
            ->processingStatus(OrderRecord::PROCESSING_STOREKEEPER_PACKAGED)
            ->deliveryType(DeliveryTypeUa::DELIVERY_TYPE_COURIER)
            ->idIn($id)
            ->isDropShipping(false)
            ->findAll();
        /* @var $orders OrderRecord[] */

        if (count($orders) != count($id)) {
            throw new CHttpException(400, 'Некоторые из указанных заказов отправляются не курером');
        }

        if (count($orders) > 100) {
            throw new CHttpException(400, 'Максимальное колличество заказов на один акт приема-передачи равняется 100');
        }

        $statement = new StatementRecord();
        $statement->delivery_type = DeliveryTypeUa::DELIVERY_TYPE_COURIER;
        $statement->status = StatementRecord::STATUS_CONFIRMED;
        if ($statement->save()) {
            $positions = [];
            /* @var $positions StatementOrderRecord[] */

            $isOk = true;
            $errorMessage = '';
            foreach ($orders as $order) {
                $position = new StatementOrderRecord();
                $position->statement_id = $statement->id;
                $position->order_id = $order->id;
                $positions[] = $position;
                if (!$position->validate()) {
                    $isOk = false;
                    $errors = $position->getErrors();
                    $error = reset($errors);
                    $errorMessage = reset($error);
                }
            }

            if ($isOk) {
                foreach ($positions as $position) {
                    $position->save();
                }
            } else {
                $statement->delete();
                throw new CHttpException(400, 'Delivery-Acceptance Act could not be created: ' . $errorMessage);
            }
        } else {
            $errors = $statement->getErrors();
            $error = reset($errors);
            $errorMessage = reset($error);
            throw new CHttpException(400, 'Delivery-Acceptance Act could not be created: ' . $errorMessage);
        }

        $this->commitView(null, $this->t('Creating an Act'));
        $this->redirect(['courierStatement/view', 'id' => $statement->id]);
    }

    public function actionUpdate($id)
    {
        $statement = StatementRecord::model()->findByPk($id);

        if ($statement === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if (count($statement->positionsMailedOnly) > 0) {
            throw new CHttpException(400, 'Some of the specified orders have already been dispatched');
        }

        $statement->status = StatementRecord::STATUS_CONFIRMED;
        $statement->save();

        $this->commitView($statement, 'Обновление акта');
        $this->redirect(['courierStatement/view', 'id' => $id]);
    }
}
