<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Backend\SupplierOrderReturnForm;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\SupplierOrderReturnRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class SupplierOrderReturnController extends BackController
{
    /**
     * @property-description Просмотр списка заявок
     */
    public function actionIndex()
    {
        $selector = SupplierOrderReturnRecord::model();

        $supplier = $this->app()->request->getParam('supplier');
        $createdAtRange = $this->app()->request->getParam('createdAtRange', []);

        if ($supplier) {
            $selector->supplierId($supplier);
        }

        if (!empty($createdAtRange)) {
            list($startAtFrom, $startAtTo) = array_map('strtotime', explode(' - ', $createdAtRange)) + [0, 0];
            $selector->createdAtBetween($startAtFrom, $startAtTo);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierOrderReturnRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View the list of supplier return requests'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @property-description Создание завки на возврат поставщику
     */
    public function actionCreate()
    {
        $model = new SupplierOrderReturnForm();

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.supplierOrderReturnAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        $form->setModel($model);
        if ($form->submitted('submit') && $form->validate()) {
            /* @var $modelForm SupplierOrderReturnForm */
            $modelForm = $form->getModel();
            $order = OrderRecord::model()->findByPk($modelForm->order_id);

            $model = new SupplierOrderReturnRecord();
            $model->order_id = $order->id;
            $model->supplier_id = $order->supplier_id;
            $model->message_callcenter = $modelForm->message_callcenter;
            $model->bankName = $modelForm->bankName;
            $model->bankGiro = $modelForm->bankGiro;

            if ($model->save()) {
                $this->redirect(['index', 'id' => $model->id]);
            }

            $modelForm->addErrors($model->getErrors());
        }

        $this->commitView(null, $this->t('Сreate suppliers return request'));
        $this->render('create', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @property-description Обновление завки на возврат поставщику
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = SupplierOrderReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'Return not found');
        }

        if (!$model->isPossibleCallcenterUpdate()) {
            throw new CHttpException(500, 'Disable editing');
        }

        $modelForm = new SupplierOrderReturnForm();
        $modelForm->setAttributes($model->getAttributes());

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.supplierOrderReturnUpdate', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        if ($form->submitted('submit') && $form->validate()) {
            /* @var $modelForm SupplierOrderReturnForm */
            $model->message_callcenter = $modelForm->message_callcenter;

            if ($model->save()) {
                $this->redirect(['index', 'id' => $model->id]);
            }

            $modelForm->addErrors($model->getErrors());
        }

        $this->commitView(null, $this->t('Updating supplier return request'));
        $this->render('update', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @property-description Просмотр завки на возврат поставщику
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $model = SupplierOrderReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'Return not found');
        }

        $this->commitView(null, $this->t('View resupplier return request'));
        $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     *
     * @property-description Удаление завки на возврат поставщику
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $model = SupplierOrderReturnRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'Record not found');
        }

        if (!$model->isPossibleCallcenterDelete()) {
            throw new CHttpException(500, 'Disable record deleting');
        }

        if (!$model->delete()) {
            throw new CHttpException(500, 'Error deleting record');
        }

        $this->commitView($model, $this->t('Delete supplier return request '));
    }

    public function actionOrderInfo($id)
    {
        throw new CHttpException(404, 'Новопошты больше нету!');
    }
}
