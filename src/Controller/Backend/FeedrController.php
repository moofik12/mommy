<?php

namespace MommyCom\Controller\Backend;

use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\YiiComponent\Backend\Select2Reply;

class FeedrController extends BackController
{
    /**
     * @param string $name
     *
     * @throws \CException
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxProvinces($name = '')
    {
        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $provinces = $feedrApi->getProvinces();
        $provinces = $this->filterByAttribute($provinces, 'name', trim($name));

        $this->renderForSelect($provinces);
    }

    /**
     * @param string $provinceId
     * @param string $name
     *
     * @throws \CException
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxCities($provinceId, $name = '')
    {
        if (!is_numeric($provinceId)) {
            $this->renderForSelect([]);
            return;
        }

        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $cities = $feedrApi->getCities((int)$provinceId);
        $cities = $this->filterByAttribute($cities, 'name', trim($name));

        $this->renderForSelect($cities);
    }

    /**
     * @param string $cityId
     * @param string $name
     *
     * @throws \CException
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxSuburbs($cityId, $name = '')
    {
        if (!is_numeric($cityId)) {
            $this->renderForSelect([]);
            return;
        }

        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $suburbs = $feedrApi->getSuburbs((int)$cityId);
        $suburbs = $this->filterByAttribute($suburbs, 'name', trim($name));

        $this->renderForSelect($suburbs);
    }

    /**
     * @param string $suburbId
     * @param string $name
     *
     * @throws \CException
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxAreas($suburbId, $name = '')
    {
        if (!is_numeric($suburbId)) {
            $this->renderForSelect([]);
            return;
        }

        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $areas = $feedrApi->getAreas((int)$suburbId);
        $areas = $this->filterByAttribute($areas, 'name', trim($name));

        $this->renderForSelect($areas);
    }

    /**
     * @param string $areaId
     * @param string $cashOnDelivery
     * @param string $weight
     * @param string $price
     *
     * @throws \CException
     * @throws \MommyCom\Service\Delivery\Api\ApiException
     */
    public function actionAjaxRates($areaId, $cashOnDelivery, $weight, $price)
    {
        if (!is_numeric($areaId)) {
            $this->renderForSelect([]);
            return;
        }

        /** @var FeedrApi $feedrApi */
        $feedrApi = $this->container->get(FeedrApi::class);

        $weight = (int)$weight / 1000;

        $domesticRates = $feedrApi
            ->getDomesticRates((int)$areaId, $weight, 30, 20, 10, (int)$price, (bool)$cashOnDelivery);

        $regular = $domesticRates['regular'] ?? [];
        $express = $domesticRates['express'] ?? [];

        $rates = array_merge($regular, $express);

        usort($rates, function (array $a, array $b) {
            return $a['finalRate'] - $b['finalRate'];
        });

        array_walk($rates, function (array &$item) {
            $item = [
                'id' => $item['rate_id'],
                'name' => implode(' - ', [$item['name'], $item['rate_name'], $item['finalRate'] . ' IDR']),
            ];
        });

        $this->renderForSelect($rates);
    }

    /**
     * @param array $items
     *
     * @throws \CException
     */
    private function renderForSelect(array $items)
    {
        $result = new Select2Reply();
        $result->setPageCount(1);
        $result->addItems($items);

        $this->renderJson($result->getResponse());
    }

    /**
     * @param array $data
     * @param string $attribute
     * @param string $filter
     *
     * @return array
     */
    private function filterByAttribute(array $data, string $attribute, string $filter): array
    {
        if ('' === $filter) {
            return $data;
        }

        $result = [];
        foreach ($data as $item) {
            if ('' === $filter || false !== strpos(mb_strtolower($item[$attribute]), mb_strtolower($filter))) {
                $result[] = $item;
            }
        }

        return $result;
    }
}
