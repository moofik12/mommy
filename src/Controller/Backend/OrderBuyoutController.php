<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;

class OrderBuyoutController extends BackController
{
    /**
     * @property-description Список заказов
     */
    public function actionIndex()
    {
        $provider = OrderBuyoutRecord::model()
            ->isAutoSynchronizable(false)
            ->onlyStatusChangeable()
            ->with('order')
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('OrderBuyoutRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @property-description Изменение статуса
     */
    public function actionChangeStatus($id)
    {
        $status = $this->app()->request->getParam('status', null);
        $model = OrderBuyoutRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Cannot find entry'));
        }

        if ($status === null) {
            throw new CHttpException(400, $this->t('Please select status'));
        }

        if (!$model->getIsStatusChangingAllowed()) {
            throw new CHttpException(400, $this->t('Status cannot be changed'));
        }

        $model->status = Cast::toUInt($status);
        $model->save();

        if (!$this->app()->request->isAjaxRequest) {
            $this->redirect(['index']);
        }
    }
} 
