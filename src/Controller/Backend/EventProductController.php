<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CUploadedFile;
use LogicException;
use MommyCom\Model\Assortment\AssortmentProduct;
use MommyCom\Model\Assortment\AssortmentTableFactory;
use MommyCom\Model\Assortment\AssortmentUploadForm;
use MommyCom\Model\Assortment\Exception\AssortmentException;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\StockAssortment\StockAssortmentProduct;
use MommyCom\Model\StockAssortment\StockAssortmentUploadForm;
use MommyCom\Model\SupplierRequest\RequestTable;
use MommyCom\Model\SupplierRequest\RequestUploadForm;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Select2Reply;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use TbForm;

class EventProductController extends BackController
{
    public function actionUpdate($id)
    {
        $this->saveBackUrl();
        $model = EventProductRecord::model()->findByPk($id);
        /* @var $model EventProductRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->number_sold > 0 || $model->event->is_stock) {
            throw new CHttpException(400, 'Products already ordered or from warehouse cannot be edited');
        }

        // alter model
        $form = TbForm::createForm('widgets.form.config.eventProductUpdate', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            if ($model->save()) {
                $this->redirectBackOr(['event/index']);
            }
        }

        $this->commitView($model, $this->t('Update product'));
        // render
        $this->render('update', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * Загрузка ассортиментных таблиц
     *
     * @throws \CDbException
     * @throws \MommyCom\Model\Assortment\Exception\InvalidColumnCountException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function actionUploadAssortment()
    {
        set_time_limit(720);

        /** @var AssortmentTableFactory $assortmentTableFactory */
        $assortmentTableFactory = $this->container->get(AssortmentTableFactory::class);
        $model = new AssortmentUploadForm($assortmentTableFactory);

        $form = TbForm::createForm($model->getConfig(), $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($form->validate()) {
                $event = EventRecord::model()->findByPk($model->eventId);
                $products = [];
                $suppliers = [];
                foreach ($model->getAssortmentTable() as $index => $item) { // парсим ассортиментную таблицу
                    if (!$event->supplier->is_approved) {
                        $model->addError(null, 'Unable to add products to flash-sale with unverified supplier');
                        break;
                    }

                    /* @var AssortmentProduct $item */
                    if ($item->validate()) { // валидируем строку
                        try {
                            $product = $item->convertToEventProduct();
                            $products[] = $product;
                            $suppliers[$item->supplier] = $item->supplier;

                            if (!$product->validate()) { // валидируем спаршеный товар
                                $model->addError('lineNum', $this->t("Error in line") . " #$index.");
                                $model->addErrors($product->getErrors());
                                break;
                            }
                        } catch (AssortmentException $e) {
                            $model->addError('lineNum', $this->t("Error in line") . " #$index");
                            $model->addError('article', $e->getMessage());
                        }
                    } else {
                        $model->addErrors($item->getErrors());
                        break;
                    }
                }

                if ($event->is_drop_shipping) {
                    if (count($suppliers) > 1) {
                        $model->addError('file', 'For flash-sales with dropshipping, products from different suppliers are not available');
                    }

                    $eventSupplierName = $event->supplier->name;
                    foreach ($suppliers as $itemSupplierName) {
                        if ($eventSupplierName != $itemSupplierName) {
                            $model->addError('file', "Дропшиппинговая акция создана для поставщика \"$eventSupplierName\", найдены товары в таблице от поставщика \"$itemSupplierName\"");
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    // Удаление предыдущих товаров акции
                    /* @var $event EventRecord */
                    foreach ($event->products as $product) {
                        if (!$event->is_stock
                            && $product->status == EventProductRecord::STATUS_VIRTUAL
                            && $product->getNumberSoldReal() == 0
                            && $product->number_arrived == 0
                        ) {
                            $product->delete();
                        }
                    }

                    foreach ($products as $index => &$product) {
                        /* @var $product EventProductRecord */
                        $product->save(false);
                        unset($product, $products[$index]);
                    }
                    unset($product);
                    $this->app()->user->setReturnUrl($this->app()->request->urlReferrer);
                    $this->redirectBackOr(['event/index']);
                }
            }
        }

        $this->commitView(null, $this->t('Upload preliminary specification'));
        $this->render('uploadAssortment', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    /**
     * Рендерит csv для оформления поставок поставщику товара
     *
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionDownloadAssortment($id)
    {
        $model = EventRecord::model()->findByPk($id);
        /* @var $model EventRecord */

        if ($model === null) {
            throw new CHttpException(404, 'Flash-sale not found');
        }

        if ($model->status == EventRecord::STATUS_EMPTY) {
            throw new CHttpException(400, 'Preliminary specification for empty flash-sale cannot be loaded');
        }

        if ($model->is_stock) {
            throw new CHttpException(400, 'Preliminary specification for flash-sale cannot be loaded');
        }

        /** @var AssortmentTableFactory $assortmentTableFactory */
        $assortmentTableFactory = $this->container->get(AssortmentTableFactory::class);

        $table = $assortmentTableFactory->createFromEvent($model);

        $this->commitView(null, $this->t('Downloading csv for delivery of items to the supplier '));
        $app = $this->app();
        $filename = 'preliminary_specification__' . $model->id . '__' . date('d-m-Y_H-i') . '.csv';
        $app->request->sendFile($filename, $table->toCsvString(), 'text/csv');
    }

    /**
     * @throws CHttpException
     * @throws \CDbException
     */
    public function actionUploadStockAssortment()
    {
        set_time_limit(720);
        $model = new StockAssortmentUploadForm();

        $form = TbForm::createForm($model->getConfig(), $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($form->validate()) {
                $products = [];
                foreach ($model->getAssortmentTable() as $index => $item) { // парсим ассортиментную таблицу
                    /* @var StockAssortmentProduct $item */
                    if ($item->validate()) { // валидируем строку
                        $subproducts = $item->convertToEventProducts();

                        if (!$item->unstockItems()) {
                            throw new CHttpException('Cannot create stock flash-sale');
                        }

                        $products[] = $subproducts;

                        foreach ($subproducts as $product) {
                            if (!$product->validate()) { // валидируем спаршеный товар
                                $model->addError('lineNum', $this->t("Error in line") . " #$index");
                                $model->addErrors($product->getErrors());
                                break 2;
                            }
                        }
                    } else {
                        $model->addErrors($item->getErrors());
                        break;
                    }
                }

                if (!$model->hasErrors()) {
                    // Удаление предыдущих товаров акции
                    $event = EventRecord::model()->findByPk($model->eventId);

                    /* @var $event EventRecord */
                    foreach ($event->products as $product) {
                        if ($product->number_sold == 0) {
                            $product->delete();
                        }
                    }

                    foreach ($products as $index => &$subproducts) {
                        foreach ($subproducts as $subindex => $product) {
                            /* @var $product EventProductRecord */
                            $product->save(false);

                            unset($product, $subproducts[$subindex]);
                        }
                        unset($products[$index]);
                    }
                    unset($subproducts, $products);
                    $this->app()->user->setReturnUrl($this->app()->request->urlReferrer);
                    $this->redirectBackOr(['event/index']);
                }
            }
        }

        $this->commitView(null, $this->t('Loading preliminary specification'));
        $this->render('uploadAssortment', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    /**
     * Загрузка таблиц заказа
     */
    public function actionUploadRequest()
    {
        set_time_limit(720);
        ini_set('memory_limit', '400M');

        $model = new RequestUploadForm();

        $form = TbForm::createForm($model->getConfig(), $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($form->validate()) {
                $products = [];

                $table = $model->getRequestTable();

                $model->addErrors($table->getErrors());
                if ($table->event->is_drop_shipping) {
                    $model->addError('file', 'It is forbidden to load order tables for flash-sales with dropshipping ');
                }

                foreach ($table->toArray() as $index => $item) {
                    if ($item->validate()) { // валидируем строку
                        $subproducts = $item->convertToWarehouseProducts();

                        foreach ($subproducts as $product) {
                            if (!$product->validate()) { // валидируем спаршеный товар
                                $model->addError('lineNum', $this->t("Error in line") . " #$index");
                                $model->addErrors($product->getErrors());
                                break 2; // завершение всех циклов
                            }
                        }

                        $products[] = $subproducts;
                    } else {
                        $model->addError('lineNum', $this->t("Error in line") . " #$index");
                        $model->addErrors($item->getErrors());
                        break;
                    }
                    gc_collect_cycles();
                }

                if (!$model->hasErrors()) {
                    foreach ($products as $index => &$subproducts) {
                        foreach ($subproducts as $product) {
                            /* @var WarehouseProductRecord $product */
                            $product->save(false);
                        }
                        unset($products[$index]);
                        UnShardedActiveRecord::clearModelsCache();
                    }
                    $this->redirectBackOr(['event/index']);
                }
            }
        }

        $this->commitView(null, $this->t('Load order tables'));
        $this->render('uploadRequest', [
            'model' => $model,
            'form' => $form,
        ]);
    }

    /**
     * Рендерит csv для оформления поставок поставщику товара
     *
     * @param integer $id
     * @param integer $supplierId
     * @param bool $addZeroSoldItems
     *
     * @throws CHttpException
     */
    public function actionDownloadProductRequest($id, $supplierId = 0, $addZeroSoldItems = true)
    {
        $model = EventRecord::model()->findByPk($id);
        /* @var $model EventRecord */

        if ($model === null) {
            throw new CHttpException(404, 'Flash-sale not found');
        }

        if ($model->status == EventRecord::STATUS_EMPTY) {
            throw new CHttpException(400, 'Order table for an empty flash-sale cannot be loaded');
        }

        /* @var EventProductRecord[] $positionsBought */
        $positionsBought = EventProductRecord::model()
            ->eventId($model->id)
            ->hasNumberSoldReal()
            ->findAll();

        foreach ($positionsBought as $position) {
            if (!$position->forceRefreshSoldNumber()->save()) {
                throw new LogicException(500, $this->t("Error updating products") . " №{$position->id}");
            }
        }

        $table = RequestTable::fromEvent($model, $supplierId, $addZeroSoldItems);

        $this->commitView(null, $this->t('Downloading csv for delivery of items to the supplier '));
        $app = $this->app();
        $filename = 'order_table__' . $model->id . '__' . date('d-m-Y_H-i') . '.csv';
        $app->request->sendFile($filename, $table->toCsvString(), 'text/csv');
    }

    /**
     * для поиска и товаров
     *
     * @param string $name
     * @param integer $page
     */
    public function actionAjaxSearch($name, $page)
    {
        $result = new Select2Reply();

        $eventProvider = EventProductRecord::model()->nameLike($name)->idLike($name)->getDataProvider(false, [
            'pagination' => [
                'currentPage' => intval($page) - 1,
                'pageSize' => 10,
            ],
        ]);

        /** @var EventProductRecord[] $events */
        $events = $eventProvider->getData();
        $result->pageCount = $eventProvider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'name', 'category', 'price', 'size'];

        foreach ($events as $event) {
            $result->addItem(ArrayUtils::valuesByKeys($event, $allowedAttributes, true) + [
                    'logoUrl' => $event->product->logo->getThumbnailByWidth(100)->url,
                    'sizeformat' => $event->sizeformatReplacement === false ? '' : $event->sizeformatReplacement,
                ]);
        }

        $this->renderJson($result->getResponse());
    }
}
