<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Backend\DistributionForm;
use MommyCom\Model\Db\CallcenterTaskOperationRecord;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class CallcenterTaskController extends BackController
{
    public function actionTask()
    {
        $taskType = $this->app()->request->getParam('taskType', 0);
        $taskType = Cast::toUInt($taskType);
        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()->with('performedAdmin', 'admin', 'operation');

        if ($taskType === 0) {
            $model->adminId($this->app()->user->id);
        }

        if ($taskType === 2) {
            $model->system(true);
        } else {
            $model->system(false);
        }

        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('CallcenterTaskRecord', []),
                'scenario' => 'searchPrivileged',
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => CSort::SORT_DESC,
                ],
            ],
        ]);

        $this->render('task/index', ['provider' => $provider, 'taskType' => $taskType]);
    }

    public function actionInbox()
    {
        $taskType = $this->app()->request->getParam('taskType', 0);
        $taskType = Cast::toUInt($taskType);
        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()->inbox()->with('performedAdmin', 'admin', 'operation');

        if ($taskType === 0) {
            $model->adminId($this->app()->user->id);
        }

        if ($taskType === 2) {
            $model->system(true);
        } else {
            $model->system(false);
        }

        $provider = $model
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('CallcenterTaskRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'is_callcenter_answer_read' => CSort::SORT_ASC,
                        'updated_at' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        $this->render('task/inbox', ['provider' => $provider, 'taskType' => $taskType]);
    }

    public function actionOutbox()
    {
        $taskType = $this->app()->request->getParam('taskType', 0);
        $taskType = Cast::toUInt($taskType);
        $model = CallcenterTaskRecord::model()->outbox()->with('performedAdmin', 'admin', 'operation');

        if ($taskType === 0) {
            $model->adminId($this->app()->user->id);
        }

        if ($taskType === 2) {
            $model->system(true);
        } else {
            $model->system(false);
        }

        $provider = $model
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('CallcenterTaskRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
                'sort' => [
                    'defaultOrder' => [
                        'id' => CSort::SORT_DESC,
                    ],
                ],
            ]);

        $this->render('task/outbox', ['provider' => $provider, 'taskType' => $taskType]);
    }

    public function actionTaskCreate()
    {
        /** @var CallcenterTaskRecord $distributionForm */
        $model = new CallcenterTaskRecord();
        $request = $this->app()->request;

        $oredrIds = $request->getPost('orderIds');
        $oredrIds = is_string($oredrIds) ? Cast::toUIntArr(explode(',', $oredrIds)) : [];

        $eventIds = $request->getPost('eventIds');
        $eventIds = is_string($eventIds) ? Cast::toUIntArr(explode(',', $eventIds)) : [];

        /** @var OrderRecord[] $orders */
        $orders = OrderRecord::model()->idIn($oredrIds)->eventIdIn($eventIds)->findAll();

        /** @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.callcenterTaskAdd', $this, [
            'type' => 'vertical',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            if (empty($orders)) {
                $model->save();
            } else {
                foreach ($orders as $order) {
                    /** @var CallcenterTaskRecord $cloneModel */
                    $cloneModel = $model->copy();
                    $cloneModel->order_id = $order->id;

                    $cloneModel->save();
                }
            }

            $this->redirect(['callcenterTask/task']);
        }

        $this->render('task/create', ['form' => $form]);
    }

    public function actionTaskUpdate($id)
    {
        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        if ($model->admin_id != $this->app()->user->id) {
            throw new CHttpException('The task was created by another operator');
        }

        $model->scenario = 'repeat';

        $oredrId = $this->app()->request->getPost('orderIds', 0);
        $oredrId = Cast::toUInt($oredrId, 0);

        /** @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.callcenterTaskUpdate', $this, [
            'type' => 'vertical',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model->order_id = $oredrId;
            $model->is_callcenter_answer_read = 0;

            if ($model->save()) {
                $this->redirect(['callcenterTask/task']);
            }
        }

        $this->render('task/update', ['form' => $form]);
    }

    public function actionTaskView($id)
    {
        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        if ($model->admin_id == $this->app()->user->id) {
            $model->readCallcenterAnswer();
        }

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('task/view', ['model' => $model]);
            $this->app()->end();
        }

        $this->render('task/view', ['model' => $model]);
    }

    public function actionTaskDelete($id)
    {
        /** @var CallcenterTaskRecord $model */
        $model = CallcenterTaskRecord::model()->findByPk($id);
        $user = $this->app()->user;

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        if ($this->app()->request->isAjaxRequest) {
            if ($model->admin_id == $user->id) {
                $model->delete();
            } else {
                throw new CHttpException(500, 'The task was created by another operator');
            }
        }
    }

    public function actionOperation()
    {
        $provider = CallcenterTaskOperationRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('CallcenterTaskOperationRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => CSort::SORT_ASC,
                ],
            ],
        ]);

        $this->render('operation/index', ['provider' => $provider]);
    }

    public function actionOperationCreate()
    {
        /** @var DistributionForm $distributionForm */
        $operation = new CallcenterTaskOperationRecord();
        /* @var $form TbForm */

        $form = TbForm::createForm('widgets.form.config.callcenterTaskOperation', $this, [
            'type' => 'vertical',
        ], $operation);

        if ($form->submitted('submit') && $form->validate()) {
            if ($operation->save()) {
                $this->redirect(['callcenterTask/operation']);
            }
        }

        $this->render('operation/create', ['form' => $form]);
    }

    public function actionOperationUpdate($id)
    {
        /** @var DistributionForm $distributionForm */
        $operation = CallcenterTaskOperationRecord::model()->findByPk($id);

        if ($operation === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        /* @var $form TbForm */

        $form = TbForm::createForm('widgets.form.config.callcenterTaskOperation', $this, [
            'type' => 'vertical',
        ], $operation);

        if ($form->submitted('submit') && $form->validate()) {
            if ($operation->save()) {
                $this->redirect(['callcenterTask/operation']);
            }
        }

        $this->render('operation/update', ['form' => $form]);
    }

    public function actionOperationDelete($id)
    {
        /** @var DistributionForm $distributionForm */
        $operation = CallcenterTaskOperationRecord::model()->findByPk($id);

        if ($operation === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        if ($this->app()->request->isAjaxRequest) {
            $operation->is_deleted = true;
            $operation->save(true, ['is_deleted']);
            $this->app()->end();
        }

        $this->redirect(['callcenterTask/operation']);
    }

}
