<?php

namespace MommyCom\Controller\Backend;

use CActiveDataProvider;
use CHttpException;
use CHttpRequest;
use CJSON;
use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Entity\Event;
use MommyCom\Entity\EventProduct;
use MommyCom\Entity\WarehouseProduct;
use MommyCom\Model\Assortment\AssortmentTableFactory;
use MommyCom\Model\Assortment\AssortmentUploadForm;
use MommyCom\Model\Backend\ProductImageZipForm;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\StockAssortment\StockAssortmentUploadForm;
use MommyCom\Model\SupplierRequest\RequestUploadForm;
use MommyCom\Repository\WarehouseProductRepository;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\Warehouse\SecurityCodeGenerator;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\BackendTablet\AdmWebUser;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use TbForm;

class EventController extends BackController
{
    public function actionIndex()
    {
        $request = $this->app()->request;
        /* @var $request CHttpRequest */
        // yii has bug in history
        $eventStartAtRangeArray = Cast::toStrArr($request->getParam('eventStartAtRange'));
        $eventStartAtRange = end($eventStartAtRangeArray);
        $eventEndAtRangeArray = Cast::toStrArr($request->getParam('eventEndAtRange'));
        $eventEndAtRange = end($eventEndAtRangeArray);
        $supplierArray = Cast::toStrArr($request->getParam('supplier'));
        $supplier = end($supplierArray);
        $isTimeActiveArray = Cast::toStrArr($request->getParam('isTimeActive'));
        $isTimeActive = end($isTimeActiveArray);
        $isStockArray = Cast::toStrArr($request->getParam('isStock'));
        $isStock = end($isStockArray);
        $isDropShippingArray = Cast::toStrArr($request->getParam('isDropShipping'));
        $isDropShipping = end($isDropShippingArray);

        $selector = EventRecord::model()
            ->isVirtual(false);

        if ($supplier) {
            $selector->with([
                    'supplier' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'nameLike' => $supplier,
                        ],
                    ]]
            );
        } else {
            $selector->with('supplier');
        }
        /* @var $selector EventRecord */

        if (!empty($eventStartAtRange)) {
            list($startAtFrom, $startAtTo) = array_map('strtotime', explode(' - ', $eventStartAtRange)) + [0, 0];
            $selector->timeStartsAfter($startAtFrom);
            $selector->timeStartsBefore($startAtTo);
        }

        if (!empty($eventEndAtRange)) {
            list($endAtFrom, $endAtTo) = array_map('strtotime', explode(' - ', $eventEndAtRange)) + [0, 0];
            $selector->timeEndsAfter($endAtFrom);
            $selector->timeEndsBefore($endAtTo);
        }

        if ($isTimeActive == 'not') {
            $selector->onlyTimeClosed();
        } elseif ($isTimeActive == 'yes') {
            $selector->onlyTimeActive();
        }

        if ($isStock == 'not') {
            $selector->isStock(false);
        } elseif ($isStock == 'yes') {
            $selector->isStock(true);
        }

        if ($isDropShipping == 'not') {
            $selector->isDropShipping(false);
        } elseif ($isDropShipping == 'yes') {
            $selector->isDropShipping(true);
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('EventRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
            'supplier' => $supplier,
            'isTimeActive' => $isTimeActive,
            'isStock' => $isStock,
            'eventStartAtRange' => $eventStartAtRange,
            'eventEndAtRange' => $eventEndAtRange,
            'isDropShipping' => $isDropShipping,
        ]);
    }

    public function actionAdd()
    {
        $model = new EventRecord();

        $form = TbForm::createForm('widgets.form.config.eventAdd', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if (($form->submitted('submit') || $form->submitted('continue'))) {
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'EventRecord', 'image/*');
            $promoFile = $this->app()->filestorage->factoryFromUploadedFile('promo', 'EventRecord', 'image/*');

            $distributionResizeJson = $this->app()->request->getPost('distribution-resize', '{}');
            /** @var array $distributionResize */
            $distributionResize = Cast::toIntArr(CJSON::decode($distributionResizeJson, true));
            $createOptions = empty($distributionResize)
                ? []
                : [
                    [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\Crop',
                        'options' => [
                            'x' => $distributionResize['x'],
                            'y' => $distributionResize['y'],
                            'width' => $distributionResize['w'],
                            'height' => $distributionResize['h'],
                        ],
                    ],
                ];

            $distributionFile = $this->app()->filestorage
                ->factoryFromUploadedFile('distribution', 'EventRecord', 'image/*', $createOptions);

            if ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }

            if ($promoFile instanceof Image) {
                $model->promo = $promoFile;
            }

            if ($distributionFile instanceof Image) {
                $model->distribution = $distributionFile;
            }

            if ($form->validate()) {
                // set logo
                if ($model->save()) {
                    if ($form->submitted('continue')) {
                        $this->redirect(['event/update', 'id' => $model->id, '#' => 'connectProductsTab']);
                    } else {
                        $this->redirect(['event/index']);
                    }
                }
            }
        }

        $this->commitView(null, $this->t('Add flash-sale'));
        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->saveBackUrl();
        $model = EventRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        /**
         * @var AdmWebUser $user
         */
        $user = $this->app()->user;
        $canBeSaved = $model->getIfCanBeSaved() || $user->isRoot();

        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.eventUpdate', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit')) {
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'EventRecord', 'image/*');
            $promoFile = $this->app()->filestorage->factoryFromUploadedFile('promo', 'EventRecord', 'image/*');
            $sizeChartFile = $this->app()->filestorage->factoryFromUploadedFile('sizeChart', 'EventRecord', 'image/*');

            $distributionResizeJson = $this->app()->request->getPost('distribution-resize', '{}');
            /** @var array $distributionResize */
            $distributionResize = Cast::toIntArr(CJSON::decode($distributionResizeJson, true));
            $createOptions = empty($distributionResize)
                ? []
                : [
                    [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\Crop',
                        'options' => [
                            'x' => $distributionResize['x'],
                            'y' => $distributionResize['y'],
                            'width' => $distributionResize['w'],
                            'height' => $distributionResize['h'],
                        ],
                    ],
                ];

            $distributionFile = $this->app()->filestorage->factoryFromUploadedFile('distribution', 'EventRecord', 'image/*', $createOptions);

            // set logo
            if ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }

            if ($promoFile instanceof Image) {
                $model->promo = $promoFile;
            }

            if ($distributionFile instanceof Image) {
                $model->distribution = $distributionFile;
            }

            if ($sizeChartFile instanceof Image) {
                $model->sizeChart = $sizeChartFile;
            }

            if (!$canBeSaved) {
                $model->addError(null, 'Cannot reactivate flash-sale');
            }

            if ($model->validate(null, false) && $form->validate() && $model->save()) {
                $this->redirect(['event/index']);
            }
        }

        // view current event products
        $productsProvider = EventProductRecord::model()
            ->eventId($model->id)
            ->orderBy('id', 'asc')
            ->with('product', 'brand', 'event')
            ->getDataProvider(false, [
                'pagination' => false,
                'filter' => [
                    'attributes' => $this->app()->request->getParam('EventProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);
        /* @var $productsProvider CActiveDataProvider */

        /** @var AssortmentTableFactory $assortmentTableFactory */
        $assortmentTableFactory = $this->container->get(AssortmentTableFactory::class);
        $assortmentUploadModel = new AssortmentUploadForm($assortmentTableFactory);
        $assortmentUploadUrl = 'eventProduct/uploadAssortment';
        $stockAssortmentUploadModel = new StockAssortmentUploadForm();
        $stockAssortmentUploadUrl = 'eventProduct/uploadStockAssortment';

        $assortmentUploadModel->eventId = $model->id;
        $assortmentUploadForm = TbForm::createForm($assortmentUploadModel->getConfig() + [
                'action' => $this->createUrl($assortmentUploadUrl),
            ], $this, [
            'type' => 'inline',
        ], $assortmentUploadModel);

        $stockAssortmentUploadModel->eventId = $model->id;
        $stockAssortmentUploadForm = TbForm::createForm($stockAssortmentUploadModel->getConfig() + [
                'action' => $this->createUrl($stockAssortmentUploadUrl),
            ], $this, [
            'type' => 'inline',
        ], $stockAssortmentUploadModel);


        $requestUploadModel = new RequestUploadForm();
        $requestUploadForm = TbForm::createForm($requestUploadModel->getConfig() + [
                'action' => $this->createUrl('eventProduct/uploadRequest'),
            ], $this, [
            'type' => 'inline',
        ], $requestUploadModel);

        $productImageZipModel = new ProductImageZipForm();
        $productImageZipModel->eventId = $model->id;
        $productImageZipForm = TbForm::createForm($productImageZipModel->getConfig() + [
                'action' => $this->createUrl('product/updateImages'),
            ], $this, [
            'type' => 'inline',
        ], $productImageZipModel);

        $this->commitView(null, $this->t('Update flash-sale'));

        $this->render('update', [
            'form' => $form,
            'model' => $model,
            'productsProvider' => $productsProvider,
            'assortmentUploadForm' => $assortmentUploadForm,
            'stockAssortmentUploadForm' => $stockAssortmentUploadForm,
            'requestUploadForm' => $requestUploadForm,
            'productImageZipForm' => $productImageZipForm,
        ]);
    }

    public function actionDelete($id)
    {
        $model = EventRecord::model()->findByPk($id);

        /* @var $model EventRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $this->commitView(null, $this->t('Attempt to delete a flash-sale'));
        if (!$model->delete() && !$model->is_deleted) {
            throw new CHttpException(500, 'Failed to delete a flash-sale ');
        }
    }

    public function actionSetReservedNumber()
    {
        $request = $this->app()->request;
        $pk = $request->getPost('pk', 0);
        $value = $request->getPost('value', 0);

        $value = Utf8::trim(Cast::toStr($value));

        $product = EventProductRecord::model()->findByPk($pk);
        /* @var $product EventProductRecord */

        if ($product === null) {
            throw new CHttpException(404, 'Product not found');
        }

        if ($product->event->is_stock) {
            throw new CHttpException(400, 'Reservation change for warehouse flash-sales disabled');
        }

        $product->number = $value;

        $product->save();

        if ($product->hasErrors()) {
            $errors = $product->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionAjaxSearch($name, $page)
    {
        $result = ['pageCount' => 0, 'items' => []];

        $selector = EventRecord::model();

        if (intval($name) > 0) {
            $selector->idLike($name);
        } else {
            $selector->nameLike($name);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'currentPage' => $page,
            ],
        ]);

        $data = $provider->getData();
        $result['pageCount'] = $provider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'name', 'created_at', 'updated_at'];

        foreach ($data as $item) {
            $result['items'][] = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);
        }

        $this->renderJson($result);
    }

    public function actionSetSection()
    {
        $request = $this->app()->request;
        $productId = $request->getPost('pk', 0);
        $sectionId = $request->getPost('value', 0);

        $productId = Cast::toInt($productId);
        $sectionId = Cast::toInt($sectionId);

        $product = ProductRecord::model()->findByPk($productId);
        /* @var $product ProductRecord */

        if ($product === null) {
            throw new CHttpException(404, 'Товар не найден');
        }

        $product->section_id = $sectionId;

        $product->save(false, ['section_id']);

        if ($product->hasErrors()) {
            $errors = $product->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionSetColorCode()
    {
        $request = $this->app()->request;
        $productEventId = $request->getPost('pk', 0);
        $colorCode = $request->getPost('value', 0);

        $productId = Cast::toInt($productEventId);
        $colorCode = Cast::toInt($colorCode);

        $product = ProductRecord::model()->findByPk($productId);
        /* @var $product ProductRecord */

        if ($product === null) {
            throw new CHttpException(404, 'Товар не найден');
        }

        $product->color_code = $colorCode;

        $product->save(false, ['color_code']);

        if ($product->hasErrors()) {
            $errors = $product->getErrors();
            $error = reset($errors);
            throw new CHttpException(400, reset($error));
        }
    }

    public function actionCreateFake()
    {
        $model = new EventRecord();

        /** @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.eventCreateFake', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') || $form->submitted('continue')) {
            $form->model->setAttributes([
                'is_virtual' => true,
                'name' => 'Fake flash sale',
                'description' => 'description',
                'description_short' => 'description_short',
                'description_mailing' => 'description_mailing',
                'startAtDateString' => '01.01.2030',
                'startAtTimeString' => '00:00',
                'endAtDateString' => '02.01.2030',
                'endAtTimeString' => '00:00',
                'mailingStartAtDateString' => '03.01.2030',
                'priority' => 0,
                'can_prepay' => 0,
            ]);

            if ($model->save()) {
                if ($form->submitted('continue')) {
                    $this->redirect(['event/update', 'id' => $model->id, '#' => 'connectProductsTab']);
                } else {
                    $this->redirect(['event/index']);
                }
            }
        }

        $this->commitView(null, $this->t('Add fake flash-sale'));
        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @param string $id
     *
     * @throws CHttpException
     * @throws \CException
     */
    public function actionReadyForWarehouse(string $id)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container
            ->get('doctrine')
            ->getManager();

        /** @var Event $event */
        $event = $entityManager
            ->getRepository(Event::class)
            ->findOneBy([
                'id' => $id,
            ]);

        if (null === $event) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if (!$event->isVirtual()) {
            throw new CHttpException(403, $this->t('Access denied'));
        }

        $event->setStartAt(1262304000); //01.01.2010
        $event->setEndAt(1262390400);
        $event->setMailingStartAt(1262476800);

        /** @var EventProduct[] $eventProducts */
        $eventProducts = $entityManager
            ->getRepository(EventProduct::class)
            ->findBy([
                'eventId' => $event->getId(),
            ]);

        /** @var WarehouseProductRepository $warehouseProductRepository */
        $warehouseProductRepository = $entityManager->getRepository(WarehouseProduct::class);
        $securityCodeGenerator = new SecurityCodeGenerator();
        $timestamp = CurrentTime::getUnixTimestamp();

        foreach ($eventProducts as $eventProduct) {
            /** @var EventProduct $eventProduct */
            $securityCode = $securityCodeGenerator->generate($eventProduct);
            $warehouseProduct = $warehouseProductRepository->createFromEventProduct(
                $eventProduct,
                $securityCode,
                WarehouseProductRecord::STATUS_WAREHOUSE_STOCK,
                $timestamp
            );
            $entityManager->persist($warehouseProduct);
        }

        $entityManager->flush();

        $this->redirect(['warehouse/index']);
    }
}
