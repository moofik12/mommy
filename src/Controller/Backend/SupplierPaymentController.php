<?php

namespace MommyCom\Controller\Backend;

use CActiveDataProvider;
use CDataProviderIterator;
use CDbException;
use CException;
use CHttpException;
use CMap;
use CSort;
use DateTime;
use Exception;
use LogicException;
use MommyCom\Model\Backend\SupplierPaymentAdditionForm;
use MommyCom\Model\Db\SupplierContractAssignmentRecord;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierPaymentActRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use TbForm;

class SupplierPaymentController extends BackController
{
    const HISTORY_SAVE_TYPE_SUPPLIER = 'supplier';
    const HISTORY_SAVE_TYPE_SUPPLIER_FULL = 'supplierFull';

    /**
     * @return array
     */
    public function filters()
    {
        return ArrayUtils::merge(parent::filters(), ['ajaxOnly + contractSuppliersDelete, contractSupplierAdd']);
    }

    /**
     * @property-description Просмотр Списока оплат
     *
     * @param bool $createAct
     *
     * @throws CException
     */
    public function actionReadyPay($createAct = false)
    {
        $time = time();

        $selector = SupplierPaymentRecord::model()
            ->statusIn([SupplierPaymentRecord::STATUS_ANALYZED])
            ->paymentAfterAtLower($time)
            ->with('statementReturnsCount')
            ->together();

        $applyFilter = false;

        //customs filters
        $deliveryEndAtRangeData = $this->app()->request->getParam('deliveryEndAt', '');
        $deliveryEndAtRangeData = explode('-', $deliveryEndAtRangeData);

        if (isset($deliveryEndAtRangeData[0]) && isset($deliveryEndAtRangeData[1])) {
            $applyFilter = true;

            $startTime = strtotime('today ' . Utf8::trim($deliveryEndAtRangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($deliveryEndAtRangeData[1]));

            $selector->deliveredEndAtGreater($startTime)->deliveredEndAtLower($endTime);
        }

        $eventEndAtRangeData = $this->app()->request->getParam('eventEndAt', '');
        $eventEndAtRangeData = explode('-', $eventEndAtRangeData);

        if (isset($eventEndAtRangeData[0]) && isset($eventEndAtRangeData[1])) {
            $applyFilter = true;

            $eventStartTime = strtotime('today ' . Utf8::trim($eventEndAtRangeData[0]));
            $eventEndTime = strtotime('tomorrow ' . Utf8::trim($eventEndAtRangeData[1]));

            $selector->eventEndAtGreater($eventStartTime)->eventEndAtLower($eventEndTime);
        }

        $eventStartAtRangeData = $this->app()->request->getParam('eventStartAt', '');
        $eventStartAtRangeData = explode('-', $eventStartAtRangeData);

        if (isset($eventStartAtRangeData[0]) && isset($eventStartAtRangeData[1])) {
            $applyFilter = true;

            $eventStartTime = strtotime('today ' . Utf8::trim($eventStartAtRangeData[0]));
            $eventEndTime = strtotime('tomorrow ' . Utf8::trim($eventStartAtRangeData[1]));

            $selector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'timeStartsAfter' => $eventStartTime,
                        'timeStartsBefore' => $eventEndTime,
                    ],
                ],
            ]);
        }

        $paymentAfterAtRangeData = $this->app()->request->getParam('paymentAfterAt', '');
        $paymentAfterAtRangeData = explode('-', $paymentAfterAtRangeData);

        if (isset($paymentAfterAtRangeData[0]) && isset($paymentAfterAtRangeData[1])) {
            $applyFilter = true;

            $paymentStartTime = strtotime('today ' . Utf8::trim($paymentAfterAtRangeData[0]));
            $paymentEndTime = strtotime('tomorrow ' . Utf8::trim($paymentAfterAtRangeData[1]));

            $selector->paymentAfterAtGreater($paymentStartTime)->paymentAfterAtLower($paymentEndTime);
        }

        $suppliers = $this->app()->request->getParam('suppliers', '');
        if ($suppliers) {
            $applyFilter = true;

            $suppliersArray = explode(',', $suppliers);
            $selector->supplierIdIn($suppliersArray);
        }

        $events = $this->app()->request->getParam('events', '');
        if ($events) {
            $applyFilter = true;

            $eventsArray = explode(',', $events);
            $selector->eventIdIn($eventsArray);
        }

        $payTo = $this->app()->request->getParam('payTo', '');
        if ($payTo == 'cart') {
            $applyFilter = true;

            $selector->with([
                'supplier' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'hasBankCardNum' => [true],
                    ],
                ],
            ]);
        } else if ($payTo == 'ca') {
            $applyFilter = true;

            $selector->with([
                'supplier' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                ],
            ]);
        }

        $hasInAct = $this->app()->request->getParam('hasInAct', 'any');
        if ($hasInAct !== 'any') {
            $applyFilter = true;

            $selector->hasInAct($hasInAct);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'payment_after_at' => CSort::SORT_ASC,
                ],
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $providerSummary = $selector->copy()->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'sortVar' => '_summarySort',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $summary = new CMap([
            'payAmount' => 0.0,
            'request_amount' => 0.0,
            'delivered_amount' => 0.0,
            'returned_amount' => 0.0,
            'penalty_amount' => 0.0,
            'paid_amount' => 0.0,
        ]);

        $paymentsID = [];
        $iterator = new CDataProviderIterator($providerSummary, 100);
        foreach ($iterator as $item) {
            $paymentsID[] = $item->id;
            foreach ($summary->toArray() as $attribute => $value) {
                $summary->add($attribute, $value + Cast::toFloat($item->$attribute));
            }
        }

        if ($createAct) {
            $this->redirect(['createActFromId', 'id' => $paymentsID]);
        }

        $this->commitView(null, 'View the list of ready-to-pay');
        $this->render('readyPay', ['provider' => $provider, 'summary' => $summary, 'applyFilter' => $applyFilter]);
    }

    /**
     * @property-description Просмотр Списока оплат в ожидании
     *
     * @param bool $createAct
     *
     * @throws CException
     */
    public function actionWaitPay($createAct = false)
    {
        $time = time();

        $selector = SupplierPaymentRecord::model()
            ->statusIn([SupplierPaymentRecord::STATUS_ANALYZED])
            ->paymentAfterAtGreater($time)
            ->with('statementReturnsCount')
            ->together();

        $applyFilter = $this->_applyFilter($selector);

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'payment_after_at' => CSort::SORT_ASC,
                ],
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $providerSummary = $selector->copy()->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'sortVar' => '_summarySort',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $summary = new CMap([
            'payAmount' => 0.0,
            'request_amount' => 0.0,
            'delivered_amount' => 0.0,
            'returned_amount' => 0.0,
            'penalty_amount' => 0.0,
            'paid_amount' => 0.0,
        ]);

        $paymentsID[] = [];
        $iterator = new CDataProviderIterator($providerSummary, 100);
        foreach ($iterator as $item) {
            $paymentsID[] = $item->id;

            foreach ($summary->toArray() as $attribute => $value) {
                $summary->add($attribute, $value + Cast::toFloat($item->$attribute));
            }
        }

        if ($createAct) {
            $this->redirect(['createActFromId', 'id' => $paymentsID]);
        }

        $this->commitView(null, 'View a list of pending payments');
        $this->render('waitPay', ['provider' => $provider, 'summary' => $summary, 'applyFilter' => $applyFilter]);
    }

    /**
     * @property-description Просмотр списка поставок поставщиками
     * @throws CException
     */
    public function actionDelivery()
    {
        $time = time();

        $selector = SupplierPaymentRecord::model()
            ->statusIn([
                SupplierPaymentRecord::STATUS_ANALYSIS,
                SupplierPaymentRecord::STATUS_NOT_ANALYZED,
                SupplierPaymentRecord::STATUS_WAIT_DELIVERY,
            ])
            ->eventEndAtLower($time)
            ->with('statementReturnsCount')
            ->together();

        $applyFilter = $this->_applyFilter($selector);

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'payment_after_at' => CSort::SORT_ASC,
                ],
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $providerSummary = $selector->copy()->getDataProvider(false, [
            'pagination' => false,
            'sort' => [
                'sortVar' => '_summarySort',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $summary = new CMap([
            'payAmount' => 0.0,
            'request_amount' => 0.0,
            'delivered_amount' => 0.0,
            'returned_amount' => 0.0,
            'penalty_amount' => 0.0,
            'paid_amount' => 0.0,
        ]);

        $paymentsID[] = [];
        $iterator = new CDataProviderIterator($providerSummary, 100);
        foreach ($iterator as $item) {
            $paymentsID[] = $item->id;

            foreach ($summary->toArray() as $attribute => $value) {
                $summary->add($attribute, $value + Cast::toFloat($item->$attribute));
            }
        }

        $this->commitView(null, 'View a list of payments pending for additional delivery');
        $this->render('delivery', ['provider' => $provider, 'summary' => $summary, 'applyFilter' => $applyFilter]);
    }

    /**
     * @property-description Ручное подтверждение поставки всех товаров поставщиком
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionApplyFullDelivery($id)
    {
        $model = SupplierPaymentRecord::model()->findByPk($id);

        if ($model == null) {
            throw new CHttpException(404, "Payment not found");
        }

        if ($model->contract == null) {
            throw new CHttpException(500, "No contract found for payment");
        } else if (!$model->isCanApplyFullDelivery()) {
            throw new CHttpException(500, "Unable to apply changes");
        }

        $model->status = SupplierPaymentRecord::STATUS_ANALYZED;
        $model->payment_after_at = $model->contract->getPaymentFullDeliveryTimestamp($model->delivery_end_at);

        if (!$model->save()) {
            throw new CHttpException(500, "Error updating payment");
        }
    }

    /**
     * @property-description Просмотр Списока оплат
     *
     * @param bool|false $file look self constants HISTORY_SAVE_TYPE_...
     */
    public function actionHistory($file = false)
    {
        set_time_limit(300);
        $selector = SupplierPaymentRecord::model()->statusNotIn([SupplierPaymentRecord::STATUS_ANALYZED]);
        $filterStartAt = false;
        $filterEndAt = false;

        $this->_applyFilter($selector);

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierPaymentRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        //output file if change
        switch ($file) {
            case self::HISTORY_SAVE_TYPE_SUPPLIER:
                $this->_historySupplierDownloadFile($provider, $filterStartAt, $filterEndAt);
                break;

            case self::HISTORY_SAVE_TYPE_SUPPLIER_FULL:
                $this->_historySupplierDownloadFile($provider, $filterStartAt, $filterEndAt, true);
                break;
        }

        $this->commitView(null, 'View history');
        $this->render('history', ['provider' => $provider]);
    }

    /**
     * Применение кастомных фильтров
     *
     * @param SupplierPaymentRecord $selector
     *
     * @return bool
     */
    protected function _applyFilter(SupplierPaymentRecord $selector)
    {
        $applyFilter = false;

        //customs filters
        $deliveryEndAtRangeData = $this->app()->request->getParam('deliveryEndAt', '');
        $deliveryEndAtRangeData = explode('-', $deliveryEndAtRangeData);

        if (isset($deliveryEndAtRangeData[0]) && isset($deliveryEndAtRangeData[1])) {
            $applyFilter = true;

            $startTime = strtotime('today ' . Utf8::trim($deliveryEndAtRangeData[0]));
            $endTime = strtotime('tomorrow ' . Utf8::trim($deliveryEndAtRangeData[1]));

            $selector->deliveredEndAtGreater($startTime)->deliveredEndAtLower($endTime);
        }

        $eventEndAtRangeData = $this->app()->request->getParam('eventEndAt', '');
        $eventEndAtRangeData = explode('-', $eventEndAtRangeData);

        if (isset($eventEndAtRangeData[0]) && isset($eventEndAtRangeData[1])) {
            $applyFilter = true;

            $eventStartTime = strtotime('today ' . Utf8::trim($eventEndAtRangeData[0]));
            $eventEndTime = strtotime('tomorrow ' . Utf8::trim($eventEndAtRangeData[1]));

            $selector->eventEndAtGreater($eventStartTime)->eventEndAtLower($eventEndTime);
        }

        $eventStartAtRangeData = $this->app()->request->getParam('eventStartAt', '');
        $eventStartAtRangeData = explode('-', $eventStartAtRangeData);

        if (isset($eventStartAtRangeData[0]) && isset($eventStartAtRangeData[1])) {
            $applyFilter = true;

            $eventStartTime = strtotime('today ' . Utf8::trim($eventStartAtRangeData[0]));
            $eventEndTime = strtotime('tomorrow ' . Utf8::trim($eventStartAtRangeData[1]));

            $selector->with([
                'event' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'timeStartsAfter' => $eventStartTime,
                        'timeStartsBefore' => $eventEndTime,
                    ],
                ],
            ]);
        }

        $paymentAfterAtRangeData = $this->app()->request->getParam('paymentAfterAt', '');
        $paymentAfterAtRangeData = explode('-', $paymentAfterAtRangeData);

        if (isset($paymentAfterAtRangeData[0]) && isset($paymentAfterAtRangeData[1])) {
            $applyFilter = true;

            $paymentStartTime = strtotime('today ' . Utf8::trim($paymentAfterAtRangeData[0]));
            $paymentEndTime = strtotime('tomorrow ' . Utf8::trim($paymentAfterAtRangeData[1]));

            $selector->paymentAfterAtGreater($paymentStartTime)->paymentAfterAtLower($paymentEndTime);
        }

        $suppliers = $this->app()->request->getParam('suppliers', '');
        if ($suppliers) {
            $applyFilter = true;

            $suppliersArray = explode(',', $suppliers);
            $selector->supplierIdIn($suppliersArray);
        }

        $events = $this->app()->request->getParam('events', '');
        if ($events) {
            $applyFilter = true;

            $eventsArray = explode(',', $events);
            $selector->eventIdIn($eventsArray);
        }

        $payTo = $this->app()->request->getParam('payTo', '');
        if ($payTo == 'cart') {
            $applyFilter = true;

            $selector->with([
                'supplier' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                ],
            ]);
        } else if ($payTo == 'ca') {
            $applyFilter = true;

            $selector->with([
                'supplier' => [
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'scopes' => [
                        'hasBankGiro' => [true],
                    ],
                ],
            ]);
        }

        $hasInAct = $this->app()->request->getParam('hasInAct', 'any');
        if ($hasInAct !== 'any') {
            $applyFilter = true;

            $selector->hasInAct($hasInAct);
        }

        return $applyFilter;
    }

    /**
     * @param CActiveDataProvider $provider
     * @param int $filterStartAt
     * @param int $filterEndAt
     * @param bool $fullInfo
     *
     * @throws CHttpException
     */
    public function _historySupplierDownloadFile($provider, $filterStartAt, $filterEndAt, $fullInfo = false)
    {
        if (!($provider->model instanceof SupplierPaymentRecord)) {
            throw new LogicException($this->t('Invalid model for data collection'));
        }

        /* @var SupplierPaymentRecord $model */
        $model = clone $provider->model;
        $model->setDbCriteria($provider->model->getDbCriteria());
        $model->getDbCriteria()->mergeWith($provider->getCriteria());
        $criteria = $model->getDbCriteria();
        $model->orderBy(['created_at', 'supplier_id'], CSort::SORT_ASC);

        if (empty($criteria->condition) && empty($criteria->with)) {
            throw new CHttpException(403, $this->t('Too much data. Use filters to specify'));
        }

        if ($filterStartAt === false || $filterEndAt === false) {
            throw new CHttpException(403, 'Time filter not selected. Select data range for search');
        }

        $df = $this->app()->dateFormatter;
        $nf = $this->app()->numberFormatter;
        $tableAlias = $model->tableAlias;
        $model->getDbCriteria()->select = "$tableAlias.id as ID, $tableAlias.supplier_id as supplierID";
        $supplierPaymentsShortInfo = $model->getSqlCommand()->queryAll();
        $supplierPaymentsShortInfoGroup = ArrayUtils::groupBy($supplierPaymentsShortInfo, 'supplierID');
        $headerInfo = [
            'countPayments' => 0,
            'countPaidPayments' => 0,
            'amountPayments' => 0,
            'amountPaidPayments' => 0,
        ];

        $modelPayment = SupplierPaymentRecord::model();
        $headerPayment = [
            $this->t('Payment'). ' №',
            $modelPayment->getAttributeLabel('contract_id'),
            $this->t('Flash-sale') . ' №',
            $modelPayment->getAttributeLabel('event_id'),
            $modelPayment->getAttributeLabel('event_end_at'),
            $modelPayment->getAttributeLabel('delivery_start_at'),
            $modelPayment->getAttributeLabel('delivery_end_at'),
            $modelPayment->getAttributeLabel('payment_after_at'),
            $modelPayment->getAttributeLabel('payment_at'),
            $this->t('Deferral of payment'),
            $this->t('Deferral of payment (working days)'),
            $modelPayment->getAttributeLabel('request_amount'),
            $modelPayment->getAttributeLabel('delivered_amount'),
            $modelPayment->getAttributeLabel('payAmount'),
            $modelPayment->getAttributeLabel('paid_amount'),
            $modelPayment->getAttributeLabel('delivered_amount_wrong_time'),
            $modelPayment->getAttributeLabel('returned_amount'),
            $this->t('Return documents'),
            $this->t('Products in return documents (product numbers)'),
            $modelPayment->getAttributeLabel('penalty_amount'),
            $modelPayment->getAttributeLabel('custom_penalty_amount'),
            $modelPayment->getAttributeLabel('comment'),
        ];
        $modelStatementReturn = WarehouseStatementReturnRecord::model();
        $headerStatementReturn = [
            $this->t('Return No.'),
            $modelStatementReturn->getAttributeLabel('status'),
            $this->t('Amount'),
            $this->t('Products in return documents (product numbers)'),
        ];

        $makeRowFromStatementReturn = function (WarehouseStatementReturnRecord $model) use ($df, $nf) {
            $returnsProducts = ArrayUtils::getColumn($model->positions, 'event_product_id');

            return [
                $model->id,
                $model->statusReplacement,
                $model->positionsPurchaseAmount(),
                implode(',', $returnsProducts),
            ];
        };

        $makeRowFromSupplierPayment = function (SupplierPaymentRecord $model, $counting = true) use ($df, $nf, &$headerInfo) {
            $statementReturns = ArrayUtils::changeKeyColumn($model->statementReturns, 'id');
            $returnsProducts = array_map(function ($item) {
                /* @var WarehouseStatementReturnRecord $item */
                return ArrayUtils::getColumn($item->positions, 'event_product_id');
            }, $statementReturns);
            $returnsProductsText = '';
            foreach ($returnsProducts as $statementReturnID => $masEventsProductsID) {
                if ($returnsProductsText) {
                    $returnsProductsText .= "\n";
                }
                $returnsProductsText .= $this->t("Return document"). "№$statementReturnID: " . implode(',', $masEventsProductsID) . '.';
            }

            if ($counting) {
                $headerInfo['countPayments']++;

                if ($model->status == SupplierPaymentRecord::STATUS_CONFIRMED) {
                    $headerInfo['countPaidPayments']++;
                }

                $headerInfo['amountPayments'] += $model->payAmount;
                $headerInfo['amountPaidPayments'] += $model->paid_amount;
            }

            return [
                $model->id,
                $model->contract ? $model->contract->name : $model->contract_id,
                $model->event_id,
                $model->event ? $model->event->name : $model->event_id,
                $df->formatDateTime($model->event_end_at, 'short', false),
                $df->formatDateTime($model->delivery_start_at, 'short', false),
                $df->formatDateTime($model->delivery_end_at, 'short', false),
                $model->payment_after_at > 0 ? $df->formatDateTime($model->payment_after_at, 'short', false) : $this->t('not assigned'),
                $model->payment_at > 0 ? $df->formatDateTime($model->payment_at, 'short', false) : $this->t('not assigned'),
                $this->t('{n} day |{n} days |{n} days', $model->getStayPayment()->d),
                $this->t( '{n} day |{n} days |{n} days', $model->getStayPaymentWorkDays()),
                $model->request_amount,
                $model->delivered_amount,
                $model->payAmount,
                $model->paid_amount,
                $model->delivered_amount_wrong_time,
                $model->returned_amount,
                implode(',', array_keys($statementReturns)),
                $returnsProductsText,
                $model->penalty_amount,
                $model->custom_penalty_amount,
                $model->comment,
            ];
        };

        //формирование оплаты по поставщику
        $rows = [];
        foreach ($supplierPaymentsShortInfoGroup as $supplierID => $paymentsGroup) {
            $paymentsIDS = ArrayUtils::getColumn($paymentsGroup, 'ID');
            $infoSuppler = [
                'payments' => [],
                'manualReturnsProductsSupplier' => [], //акты возвратов которые были зафиксированы в ручном режиме
                'needReturnsProductsSupplier' => [], //акты возвратов которые ожидают фиксации
            ];

            $supplier = SupplierRecord::model()->findByPk($supplierID);
            $infoSuppler['payments'] = SupplierPaymentRecord::model()->idIn($paymentsIDS)
                ->statusNotIn([SupplierPaymentRecord::STATUS_NOT_ANALYZED, SupplierPaymentRecord::STATUS_ANALYSIS])->with('statementReturns')->together()->findAll();

            if ($fullInfo) {
                $infoSuppler['manualReturnsProductsSupplier'] = WarehouseStatementReturnRecord::model()->supplier($supplierID)->supplierPayment(0)
                    ->isAccountedReturns(true)->createdAtFrom($filterStartAt)->createdAtTo($filterEndAt)->findAll();
                $infoSuppler['needReturnsProductsSupplier'] = WarehouseStatementReturnRecord::model()->supplier($supplierID)->supplierPayment(0)->isAccountedReturns(false)->findAll();
            }

            $infoSuppler['payments'] = ArrayUtils::changeKeyColumn($infoSuppler['payments'], 'id');
            $supplierPaid = array_filter($infoSuppler['payments'], function ($item) {
                /* @var SupplierPaymentRecord $item */
                return $item->status == SupplierPaymentRecord::STATUS_CONFIRMED;
            });
            $supplierPaidNotPaid = array_diff_key($infoSuppler['payments'], $supplierPaid);

            $rows[] = [];
            $rows[] = [];
            $rows[] = [];
            $rows[] = ["Данные по поставщику " . ($supplier ? $supplier->name : $supplierID)];
            if ($supplierPaid) {
                $rows[] = [];
                $rows[] = ["Сделанные оплаты"];
                $rows[] = $headerPayment;

                foreach ($supplierPaid as $supplierPaidItem) {
                    $rows[] = $makeRowFromSupplierPayment($supplierPaidItem);
                }

                $amount = array_reduce($supplierPaid, function ($carry, $item) {
                    /* @var SupplierPaymentRecord $item */
                    return ($carry + $item->paid_amount);
                }, 0.0);
                $rows[] = ["Итого", $nf->formatDecimal($amount)];
            }

            if ($supplierPaidNotPaid) {
                $rows[] = [];
                $rows[] = ["Оплаты в ожидании"];
                $rows[] = $headerPayment;

                foreach ($supplierPaidNotPaid as $supplierPaidNotPaidItem) {
                    $rows[] = $makeRowFromSupplierPayment($supplierPaidNotPaidItem);
                }

                $amount = array_reduce($supplierPaidNotPaid, function ($carry, $item) {
                    /* @var SupplierPaymentRecord $item */
                    return ($carry + $item->payAmount);
                }, 0.0);
                $rows[] = ["Итого", $nf->formatDecimal($amount)];
            }

            if ($fullInfo) {
                if ($infoSuppler['manualReturnsProductsSupplier']) {
                    $rows[] = [];
                    $rows[] = ["Акты возвратов которые были учтены вручную"];
                    $rows[] = $headerStatementReturn;

                    foreach ($infoSuppler['manualReturnsProductsSupplier'] as $manualReturnsProductSupplier) {
                        $rows[] = $makeRowFromStatementReturn($manualReturnsProductSupplier);
                    }

                    $amount = array_reduce($infoSuppler['manualReturnsProductsSupplier'], function ($carry, $item) {
                        /* @var WarehouseStatementReturnRecord $item */
                        return ($carry + $item->positionsPurchaseAmount());
                    }, 0.0);
                    $rows[] = ["Итого", $nf->formatDecimal($amount)];
                }

                if ($infoSuppler['needReturnsProductsSupplier']) {
                    $rows[] = [];
                    $rows[] = ["Акты возвратов которые ожидают включение в оплату"];
                    $rows[] = $headerStatementReturn;

                    foreach ($infoSuppler['needReturnsProductsSupplier'] as $manualReturnsProductSupplier) {
                        $rows[] = $makeRowFromStatementReturn($manualReturnsProductSupplier);
                    }

                    $amount = array_reduce($infoSuppler['needReturnsProductsSupplier'], function ($carry, $item) {
                        /* @var WarehouseStatementReturnRecord $item */
                        return ($carry + $item->positionsPurchaseAmount());
                    }, 0.0);
                    $rows[] = ["Итого", $nf->formatDecimal($amount)];
                }
            }
        }

        $title = "Статистика оплат поставщикам за период " . $df->formatDateTime($filterStartAt, 'short', false) . ' - ' . $df->formatDateTime($filterEndAt, 'short', false);
        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];
        $csvDATA[] = ['Required number of payments', $headerInfo['countPayments']];
        $csvDATA[] = ['Required amount of payments', $headerInfo['amountPayments']];
        $csvDATA[] = ['Number of payments', $headerInfo['countPaidPayments']];
        $csvDATA[] = ['Amount of payments', $headerInfo['amountPaidPayments']];

        $csvDATA[] = [];
        $csvDATA = array_merge($csvDATA, $rows);

        $this->commitView(null, $title);
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = str_ireplace(' ', '_', $title) . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionView($id)
    {
        /* @var SupplierPaymentRecord $model */
        $model = SupplierPaymentRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $providerWarehouseProducts = WarehouseProductRecord::model()->eventId($model->event_id)->supplierId($model->supplier_id)
            ->getDataProvider(false, [
                'sort' => [
                    'defaultOrder' => 't.created_at DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('View payment'));
        $this->render('view', ['model' => $model, 'providerWarehouseProducts' => $providerWarehouseProducts]);
    }

    /**
     * @property-description Изменение дополнительных данных оплаты
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionUpdateAddition($id)
    {
        /* @var SupplierPaymentRecord $model */
        $model = SupplierPaymentRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if ($model->status == SupplierPaymentRecord::STATUS_CONFIRMED) {
            throw new CHttpException(403, 'Оплата подтверждена, редактирование закрыто!');
        }

        $modelForm = new SupplierPaymentAdditionForm($model);

        $form = Form::createForm('widgets.form.config.supplierPaymentAddition', $modelForm, [
            'type' => 'horizontal',
        ]);

        /* @var TbForm $form */
        if ($form->submitted('submit') && $form->validate()) {
            $model->setWarehouseStatementReturns($modelForm->getWarehouseStatementReturns());
            $model->setUnpaidPayments($modelForm->getUnpaidPayments());
            $model->returned_amount = $modelForm->getWarehouseStatementReturnsAmount();
            $model->custom_penalty_amount = $modelForm->custom_penalty_amount;

            if ($model->save()) {
                $this->redirect(['readyPay']);
            } else {
                //bubbles errors
                throw new CHttpException(500, "Ошибка при сохранении оплаты");
            }
        }

        $modelForm->loadDataFromRecord();

        $providerWarehouseProducts = WarehouseProductRecord::model()->eventId($model->event_id)->supplierId($model->supplier_id)
            ->getDataProvider(false, [
                'sort' => [
                    'defaultOrder' => 't.created_at DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('Edit additional payment details'));
        $this->render('updateAddition', ['model' => $model, 'form' => $form, 'providerWarehouseProducts' => $providerWarehouseProducts]);
    }

    /**
     * @property-description Подтверждение оплаты
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionConfirm($id)
    {
        /* @var SupplierPaymentRecord $model */
        $model = SupplierPaymentRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if (!($model->status == SupplierPaymentRecord::STATUS_ANALYZED)) {
            throw new CHttpException(403, 'Подтверждение закрыто!');
        }

        $model->setScenario('confirm');

        $form = Form::createForm('widgets.form.config.supplierPaymentConfirm', $model, [
            'type' => 'horizontal',
        ]);

        /* @var TbForm $form */
        if ($form->submitted('submit') && $form->validate()) {
            $model->status = SupplierPaymentRecord::STATUS_CONFIRMED;

            if ($model->save()) {
                $this->redirect(['readyPay']);
            }
        }

        //fill default
        if (!$form->submitted('submit')) {
            $bankData = '';
            if ($model->supplier) {
                $bankData = $model->supplier->getBankGiro() ? $model->supplier->getBankGiro() : $model->supplier->getBankCardNum();
            }
            $model->paid_amount = $model->getPayAmount();
            $model->setBankGiro($bankData);
        }

        $providerWarehouseProducts = WarehouseProductRecord::model()->eventId($model->event_id)->supplierId($model->supplier_id)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => [
                    'defaultOrder' => 't.created_at DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('Payment confirmation'));
        $this->render('confirm', ['model' => $model, 'form' => $form, 'providerWarehouseProducts' => $providerWarehouseProducts]);
    }

    /**
     * @property-description Отмена оплаты
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionCancel($id)
    {
        /* @var SupplierPaymentRecord $model */
        $model = SupplierPaymentRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if (!(in_array($model->status, [SupplierPaymentRecord::STATUS_ANALYZED, SupplierPaymentRecord::STATUS_ANALYSIS]))) {
            throw new CHttpException(403, 'Отмена оплаты закрыто!');
        }

        $model->setScenario('cancel');

        $form = Form::createForm('widgets.form.config.supplierPaymentCancel', $model, [
            'type' => 'horizontal',
        ]);

        /* @var TbForm $form */
        if ($form->submitted('submit') && $form->validate()) {
            $model->status = SupplierPaymentRecord::STATUS_CANCELED;

            if ($model->save()) {
                $this->redirect(['readyPay']);
            }
        }

        if ($model->unpaid_amount == 0 && $model->penalty_amount > 0) {
            $model->unpaid_amount = $model->penalty_amount;
        }

        $providerWarehouseProducts = WarehouseProductRecord::model()->eventId($model->event_id)->supplierId($model->supplier_id)
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => [
                    'defaultOrder' => 't.created_at DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView($model, $this->t('Payment cancellation'));
        $this->render('confirm', ['model' => $model, 'form' => $form, 'providerWarehouseProducts' => $providerWarehouseProducts]);
    }

    // CONTRACTS ACTIONS

    /**
     * @property-description Просмотр списока контрактов
     */
    public function actionContracts()
    {
        $selector = SupplierContractRecord::model();

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 't.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierContractRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, 'View contracts list');
        $this->render('contracts', ['provider' => $provider]);
    }

    /**
     * @property-description Просмотр содержимого контракта
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionContractView($id)
    {
        $model = SupplierContractRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Data not found'));
        }

        $this->commitView($model, $this->t('View contract'));
        $this->render('contractView', ['model' => $model]);
    }

    /**
     * @property-description Обновление контракта
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionContractUpdate($id)
    {
        $model = SupplierContractRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Data not found'));
        }

        $form = Form::createForm('widgets.form.config.supplierContract', $model, [
            'type' => 'horizontal',
        ]);

        /* @var TbForm $form */
        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect(['contracts']);
            }
        }

        $this->render('contractUpdate', ['form' => $form]);
    }

    /**
     * @property-description Обновление поставщиков контракта
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionContractSuppliersUpdate($id)
    {
        $model = SupplierContractRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', $this->t('Data not found'));
        }

        $provider = SupplierRecord::model()->contractId($model->id)->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => 'created_at DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('contractSuppliersUpdate', ['model' => $model, 'provider' => $provider]);
    }

    /**
     * @property-description Создание контракта
     * @throws CHttpException
     */
    public function actionContractCreate()
    {
        $model = new SupplierContractRecord();

        $form = Form::createForm('widgets.form.config.supplierContract', $model, [
            'type' => 'horizontal',
        ]);

        /* @var TbForm $form */
        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect(['contracts']);
            }
        }

        $this->render('contractCreate', ['form' => $form]);
    }

    /**
     * @property-description Удаление поставщика из котнракта
     *
     * @param int $id supplier contract id
     * @param array $suppliers
     *
     * @throws CHttpException
     */
    public function actionContractSuppliersDelete($id, array $suppliers)
    {
        $model = SupplierContractRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException('404', 'Contract data not found');
        }

        if (empty($suppliers)) {
            throw new CHttpException('500', 'Empty suppliers data');
        }

        $suppliersModel = SupplierRecord::model()->idIn($suppliers)->contractId($model->id)->findAll();

        array_map(function ($supplierModel) {
            /* @var SupplierContractAssignmentRecord $supplierModel */
            $supplierModel->contract_id = '';
            $supplierModel->save();
        }, $suppliersModel);
    }

    /**
     * @property-description Добавление поставщика в котнракт
     *
     * @param int $id supplier contract id
     *
     * @throws CHttpException
     */
    public function actionContractSupplierAdd($id)
    {
        $request = $this->app()->request;
        $force = Cast::toBool($request->getParam('force', 0));
        $supplier = Cast::toUInt($request->getParam('supplier', 0));

        $result = [
            'result' => false,
            'error' => '',
            'hasInAnotherContract' => false,
        ];

        $modelContract = SupplierContractRecord::model()->findByPk($id);
        /* @var SupplierRecord $modelSupplier */
        $modelSupplier = SupplierRecord::model()->with('contract')->together()->findByPk($supplier);

        if ($modelContract === null) {
            $result['error'] = 'Contract not found';
        }

        if ($modelSupplier === null) {
            $result['error'] = 'Supplier not found';
        } else {
            if ($modelSupplier->contract_id == $id) {
                $result['error'] = "Поставщик уже в текущем контракте";
            } elseif (!$force) {
                $result['error'] = "Поставщик найден в контракте '№{$modelSupplier->contractConnected->id} {$modelSupplier->contractConnected->name}'";
                $result['hasInAnotherContract'] = true;
            }
        }

        if (empty($result['error'])) {
            if ($modelSupplier && $force) {
                $modelSupplier->contract_id = $modelContract->id;
            }

            if ($modelSupplier->save()) {
                $result['result'] = true;
            } else {
                $errors = $modelSupplier->getErrors();
                $lastError = end($errors);
                $result['error'] = 'Error adding a supplier';

                if (isset($lastError[0])) {
                    $result['error'] = $lastError[0];
                }
            }
        }

        $this->renderJson($result);
    }


    //ACTS

    /**
     * @property-description Просмотр Акта оплаты поставщикам (печать/удлаение/подтверждение платежа)
     *
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionActView($id)
    {
        $model = SupplierPaymentActRecord::model()->findByPk($id);

        if ($model == null) {
            throw new CHttpException(404, "Акт не найден");
        }

        $provider = SupplierPaymentRecord::model()->actId($model->id)->getDataProvider(false, [
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => [
                'defaultOrder' => 'supplier_id',
            ],
        ]);

        $this->commitView(null, 'View supplier payment Act');
        $this->render('act/view', ['provider' => $provider, 'model' => $model]);
    }

    /**
     * @property-description Скачивание Акта оплаты поставщикам (печать/удлаение/подтверждение платежа)
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionActDownload($id)
    {
        $model = SupplierPaymentActRecord::model()->findByPk($id);
        $modelSupplierPayment = SupplierPaymentRecord::model();
        $modelSupplier = SupplierRecord::model();

        if ($model == null) {
            throw new CHttpException(404, 'Document not found');
        }

        $payments = $model->payments;

        if (empty($payments)) {
            throw new CHttpException(404, 'No payment was found in the Act');
        }

        $paymentsGroup = ArrayUtils::groupBy($payments, 'supplier_id');

        $dataTime = new DateTime();
        $title = "Акт №{$model->id} для оплаты поставщикам";

        $csvDATA = [];
        $csvDATA[] = [$title];
        $csvDATA[] = [];

        $header = [
            $modelSupplierPayment->getAttributeLabel('event_id'),
            '№ оплаты',
            $modelSupplierPayment->getAttributeLabel('supplier_id'),
            $modelSupplier->getAttributeLabel('bankGiro'),
            $modelSupplier->getAttributeLabel('bankCardNum'),
            $modelSupplierPayment->getAttributeLabel('payAmount'),
            $modelSupplierPayment->getAttributeLabel('payAmountWithoutPenalty'),
        ];

        try {
            /* @var SupplierPaymentRecord[] $paymentForSuppliers */
            foreach ($paymentsGroup as $paymentSupplierId => $paymentForSuppliers) {
                $supplier = '';

                $csvDATA[] = $header;

                foreach ($paymentForSuppliers as $paymentForSupplier) {
                    if (empty($supplier)) {
                        $supplier = $paymentForSupplier->supplier->name;
                    }

                    $csvDATA[] = [
                        "№{$paymentForSupplier->event_id} {$paymentForSupplier->event->name}",
                        $paymentForSupplier->id,
                        $supplier,
                        $paymentForSupplier->supplier->getBankGiro(),
                        $paymentForSupplier->supplier->getBankCardNum(),
                        $paymentForSupplier->getPayAmount(),
                        $paymentForSupplier->getPayAmountWithoutPenalty(),
                    ];
                }

                $amount = $model->getAmountPayments(true, $paymentSupplierId);
                $amountWithoutPenalty = $model->getAmountPaymentsWithoutPenalty($paymentSupplierId);

                $csvDATA[] = ["К оплате поставщику $supplier", "", "", "", "", $amount, $amountWithoutPenalty];
                $csvDATA[] = [];
                $csvDATA[] = [];
            }
        } catch (Exception $e) {
            throw new CHttpException(500, "Ошибка при формировании данных для оплаты! \n" . $e->getMessage());
        }

        $csvDATA[] = [];

        $this->commitView(null, 'Download supplier payment Act');
        $csv = CsvFile::loadFromArray($csvDATA);
        $filename = str_ireplace(' ', '_', $title) . '_' . $dataTime->format('d_m_Y_H_i') . '.csv';
        $this->app()->request->sendFile($filename, $csv->toString(), 'text/csv');
    }

    /**
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionActAjaxUpdate($id)
    {
        $model = SupplierPaymentActRecord::model()->findByPk($id);

        if ($model == null) {
            throw new CHttpException(404, 'Document not found');
        }

        $newAttributes = $this->app()->request->getPost('SupplierPaymentActRecord');
        $model->setAttributes($newAttributes);

        if (!$model->save()) {
            throw new CHttpException(500, 'Error updating data');
        }
    }

    /**
     * @property-description Удаление Акта (AJAX)
     *
     * @param int $id
     *
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionActDelete($id)
    {
        $model = SupplierPaymentActRecord::model()->findByPk($id);

        if ($model == null) {
            throw new CHttpException(404, 'Act for deletion not found');
        }

        if (!$model->delete()) {
            throw new CHttpException(500, 'Error deleting a act');
        }

        $this->commitView(null, 'Deleting supplier payment act');
        $this->redirect(['readyPay']);
    }

    /**
     * @property-description Обновление Акта (AJAX)
     *
     * @param int $id
     * @param int $fromActId
     *
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionActDeletePosition($id, $fromActId)
    {
        $modelAct = SupplierPaymentActRecord::model()->findByPk($fromActId);
        $model = SupplierPaymentRecord::model()->findByPk($id);

        if ($modelAct == null) {
            throw new CHttpException(404, 'No Act found to delete item from');
        }

        if ($model == null) {
            throw new CHttpException(404, 'No payment found to be deleted from the Act');
        }

        $model->act_id = '';
        if (!$model->save()) {
            throw new CHttpException(500, 'Error deleting item from the Act');
        }

        $this->commitView(null, 'Deleting payment from supplier payment Act');
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionActAjaxAddPosition($id)
    {
        $paymentId = $this->app()->request->getPost('supplier-payment-add', 0);
        $modelAct = SupplierPaymentActRecord::model()->findByPk($id);
        $model = SupplierPaymentRecord::model()->findByPk($paymentId);

        $result = new Reply();

        if ($modelAct == null) {
            $result->addError($this->t('No Act found to add an item to!'));
        }

        if ($model == null) {
            $result->addError($this->t('No payment found to add to the Act!'));
        }

        if ($model) {
            if ($model->status != SupplierPaymentRecord::STATUS_ANALYZED && $model->status != SupplierPaymentRecord::STATUS_ANALYSIS) {
                $result->addError('Оплата недоступна для добавления в в акт. Статус: ' . $model->getStatusReplacement($model->status));
            }

            if ($model->act_id > 0) {
                $actText = $model->act_id == $modelAct->id ? $this->t('current act') : ($this->t("Act №") . $model->act_id);
                $result->addError($this->t("Payment already documented in") . ' ' . $actText);
            }
        }

        if (!$result->hasErrors()) {
            $model->act_id = $modelAct->id;
            if (!$model->save()) {
                throw new CHttpException(500, 'Error occurred adding payment to the Act');
            }
        }

        $this->renderJson($result->getResponse());
    }

    /**
     * @property-description Создание Акта оплаты поставщикам
     *
     * @param array $id
     *
     * @throws CHttpException
     */
    public function actionCreateActFromId(array $id)
    {
        $errors = [];
        if (empty($id)) {
            throw new CHttpException(500, "Не переданы номера оплат для создания акта");
        }

        $models = SupplierPaymentRecord::model()->idIn($id)->with('supplier')->findAll();

        foreach ($models as $model) {
            $supplier = $model->supplier ? "{$model->supplier->name} ({$model->supplier_id})" : $model->supplier_id;

            if ($model->act_id > 0) {
                $errors[] = "Опалата №{$model->id} поставщику {$supplier} уже зафиксирована в акте {$model->act_id}";
            }

            if ($model->status == SupplierPaymentRecord::STATUS_CONFIRMED
                || $model->status == SupplierPaymentRecord::STATUS_CANCELED) {
                $errorStatusText = $model->getStatusReplacement('Неизветсно');

                $errors[] = "Опалата №{$model->id} поставщику {$supplier} находится в статусе {$errorStatusText} (запрещено для создания акта)";
            }
        }

        if ($errors) {
            $this->render('act/errorsCreateAct', [
                'errors' => $errors,
            ]);

            $this->app()->end();
        }

        $transaction = $this->app()->db->beginTransaction();

        $newAct = new SupplierPaymentActRecord();
        try {
            if (!$newAct->save()) {
                throw new LogicException($this->t("Error creating supplier payment Acts"));
            }

            foreach ($models as $model) {
                $model->act_id = $newAct->id;
                if (!$model->save()) {
                    throw new LogicException($this->t('Error occurred adding payment to the Act') . " №{$model->id} ");
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            throw new CHttpException(500, $e->getMessage());
        }

        $this->commitView(null, 'Create supplier payment Act');
        $this->redirect(['actView', 'id' => $newAct->id]);
    }

    /**
     * @property-description Ajax поиск оплат
     *
     * @param $name
     * @param $page
     */
    public function actionAjaxSearch($name, $page)
    {
        $result = ['pageCount' => 0, 'items' => []];

        $provider = SupplierPaymentRecord::model()->idLike($name)->getDataProvider(false, [
            'pagination' => [
                'currentPage' => $page,
            ],
        ]);

        $data = $provider->getData();
        $result['pageCount'] = $provider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'name', 'created_at', 'updated_at'];

        foreach ($data as $item) {
            $result['items'][] = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);
        }

        $this->renderJson($result);
    }
}
