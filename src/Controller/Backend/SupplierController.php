<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\AdminRoleAssignmentRecord;
use MommyCom\Model\Db\AdminRoleRecord;
use MommyCom\Model\Db\AdminSuppliers;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Security\User\WebUser;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Backend\AdmAuthManager\AdmWebUser;
use MommyCom\YiiComponent\Facade\EmailTranslator;
use MommyCom\YiiComponent\FileStorage\File\Image;
use TbForm;

class SupplierController extends BackController
{
    public function actionIndex()
    {
        $selector = SupplierRecord::model();
        $hasEnableAccess = $this->app()->request->getParam('hasEnableAccess', 'any');

        if ($hasEnableAccess !== 'any') {
            $selector->hasEnableAccess($hasEnableAccess);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
        ]);
    }

    public function actionAdd()
    {
        $model = new SupplierRecord();
        /* @var TbForm $form */
        $form = TbForm::createForm('widgets.form.config.supplierAdd', $this, [
            'type' => 'horizontal',
        ], $model);

        $form->setModel($model);
        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'SupplierRecord', 'image/*');

            if ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }

            if ($model->save()) {
                $this->redirect(['supplier/update', 'id' => $model->id]);
            }
        }

        $this->commitView(null, $this->t('Adding a supplier'));
        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = SupplierRecord::model()->findByPk($id);
        /* @var $model SupplierRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $form = TbForm::createForm('widgets.form.config.supplierUpdate', $this, [
            'type' => 'horizontal',
        ], $model);

        if (null === $model->adminSupplier) {
            $form->setButtons([
                \CHtml::button($this->t('Create account'), [
                    'name' => 'createAccount',
                    'class' => 'btn btn-primary'
                ])
            ]);
        } else {
            $form->setElements([
                \CHtml::label('Supplier has account', '', [
                    'class' => 'text-center'
                ])
            ]);
        }

        $form->setModel($model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            $logoFile = $this->app()->filestorage->factoryFromUploadedFile('logo', 'SupplierRecord', 'image/*');

            if ($logoFile instanceof Image) {
                $model->logo = $logoFile;
            }

            if ($model->save()) {
                $this->redirect(['supplier/index']);
            }
        }

        $this->commitView($model, $this->t('Update supplier data'));
        $this->render('update', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function actionCreateSupplier($id)
    {
        $model = SupplierRecord::model()->findByPk($id);
        $transaction = $model->getDbConnection()->beginTransaction();
        $password = rtrim(base64_encode(random_bytes(10)), '=');

        try {
            $adminUser = AdminUserRecord::model();
            $adminUser->login = $model->email;
            $adminUser->newPassword = $password;
            $adminUser->confirmPassword = $password;
            $adminUser->email = $model->email;
            $adminUser->fullname = $model->name;
            $adminUser->is_supplier = true;
            $adminUser->save();

            $supplierAdminRef = AdminSuppliers::model();
            $supplierAdminRef->admin_id = $adminUser->id;
            $supplierAdminRef->supplier_id = $model->id;
            $supplierAdminRef->save();

            $adminRole = AdminRoleRecord::model()->find('rules = :rule', [
                ':rule' => SupplierRecord::ACCESS_RULE,
            ]);
            $adminRolesRef = AdminRoleAssignmentRecord::model();
            $adminRolesRef->role_id = $adminRole->id;
            $adminRolesRef->admin_id = $adminUser->id;
            $adminRolesRef->save();

            $transaction->commit();
        } catch (\Exception $exception) {
            $transaction->rollback();
        }

        $this->app()->mailer->create(
            [$this->app()->params['layoutsEmails']['supplierRegistration'] => $this->app()->params['distributionEmailName']],
            [$model->email],
            $this->t('Your profile registration data'),
            [
                'view' => 'notification.supplier.registration',
                'data' => [
                    'model' => $model,
                    'password' => $password,
                    'link' => $this->getBackendLink(),
                    'translator' => EmailTranslator::get(),
                ],
            ]
        );
    }

    public function actionView($id)
    {
        $model = SupplierRecord::model()->findByPk($id);
        /* @var $model SupplierRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $this->commitView($model, $this->t('View supplier data'));
        $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSearch($name, $page)
    {
        $result = ['pageCount' => 0, 'items' => []];

        $provider = SupplierRecord::model()->nameLike($name)->getDataProvider(false, [
            'pagination' => [
                'currentPage' => $page,
            ],
        ]);

        $data = $provider->getData();
        $result['pageCount'] = $provider->getPagination()->pageCount;

        $allowedAttributes = ['id', 'name', 'created_at', 'updated_at'];

        foreach ($data as $item) {
            $result['items'][] = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);
        }

        $this->renderJson($result);
    }

    public function actionAjaxGetById($id)
    {
        $item = SupplierRecord::model()->findByPk($id);

        if (empty($item)) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        $allowedAttributes = ['id', 'name', 'created_at', 'updated_at'];

        $result = ArrayUtils::valuesByKeys($item, $allowedAttributes, true);

        $this->renderJson($result);
    }

    public function actionSupplierList()
    {
        $selector = SupplierRecord::model()
            ->approved(true);

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('SupplierRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('supplierList', [
            'provider' => $provider,
        ]);
    }

    /**
     * @return string
     */
    private function getBackendLink()
    {
        $regions = $this->container->get(Regions::class);
        $region = $regions->getServerRegion()->getRegionName();
        $hostname = $regions->getHostname($region);
        $baseUrlWithScheme = $this->app()->getBaseUrl(true);
        $baseUrlWithoutScheme = $regions->getHostname(Region::GLOBAL);
        $baseUrl = (false === strpos($baseUrlWithScheme, $hostname)) ?
            str_replace($baseUrlWithoutScheme, $hostname, $baseUrlWithScheme) : $baseUrlWithScheme;

        return $baseUrl . '/backend.php';
    }
}
