<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CSort;
use MommyCom\Model\Db\AdminAuthlogRecord;
use MommyCom\Service\BaseController\BackController;

class AdminAuthlogController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Manage Login Statistics';

    public $breadcrumbs = [
        'Statistics',
        'List' => ['index'],
    ];

    /**
     * @param integer $id the ID of the model to be deleted
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        /*
        if($this->app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        */
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        // grid
        $dataProvider = AdminAuthlogRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('AdminAuthlogRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        // render
        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('_index', ['dataProvider' => $dataProvider]);
            $this->app()->end();
        }

        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|AdminAuthlogRecord
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $id = intval($id);

        $model = AdminAuthlogRecord::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, $this->t('Model does not exist.'));
        return $model;
    }
}
