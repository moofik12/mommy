<?php

namespace MommyCom\Controller\Backend;

use CActiveForm;
use CHttpException;
use CJSON;
use MommyCom\Model\Db\TaskBoardAnswersRecord;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Reply;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Ajax\Users;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Button\Archive;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Button\Refresh;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Button\To;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Index\All;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Index\Index;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Index\Specialized;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Main\Delete;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Session\AddUserToFilter;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Session\RemoveUserFromFilter;
use MommyCom\YiiComponent\BackendActions\TaskBoard\Session\SelfTasks;
use MommyCom\YiiComponent\Session;
use MommyCom\YiiComponent\TaskBoard\TaskDetermination;
use TbForm;

class TaskBoardController extends BackController
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @var string
     */
    public $pageTitle;

    /**
     * @return array
     * @property-description managers:Доска задач бренд-менеджеров
     */
    public function actions()
    {
        $this->pageTitle = $this->t('Task manager');

        return [
            'managers' => [
                'class' => Index::class,
                'board_type' => TaskBoardRecord::TYPE_BRAND_MANAGER,
                'title' => $this->t('Brand managers task list'),
                'filterAddUrl' => 'taskBoard/addingUserToManagers',
                'filterRemoveUrl' => 'taskBoard/removeUserFromManagers',
                'view' => 'index',
            ],
            'callCenter' => [
                'class' => Index::class,
                'board_type' => TaskBoardRecord::TYPE_CALL_CENTER,
                'title' => $this->t('Call Center task list'),
                'filterAddUrl' => 'taskBoard/addingUserToCallCenter',
                'filterRemoveUrl' => 'taskBoard/removeUserFromCallCenter',
                'view' => 'index',
            ],
            'storage' => [
                'class' => Specialized::class,
                'board_type' => TaskBoardRecord::TYPE_STORAGE,
                'title' => $this->t('Warehouse task list'),
                'key' => 'self-task-storage',
                'view' => 'specialized',
            ],
            'all' => [
                'class' => All::class,
            ],
            'toStorage' => [
                'class' => To::class,
                'board_type' => TaskBoardRecord::TYPE_STORAGE,
            ],
            'toCallCenter' => [
                'class' => To::class,
                'board_type' => TaskBoardRecord::TYPE_CALL_CENTER,
            ],
            'toBrand' => [
                'class' => To::class,
                'board_type' => TaskBoardRecord::TYPE_BRAND_MANAGER,
            ],
            'toCoordination' => [
                'class' => To::class,
                'board_type' => TaskBoardRecord::TYPE_COORDINATION,
            ],
            'coordination' => [
                'class' => Specialized::class,
                'board_type' => TaskBoardRecord::TYPE_COORDINATION,
                'title' => $this->t('Coordinators task list'),
                'key' => 'self-task-coordination',
                'view' => 'specialized',
            ],
            'archive' => [
                'class' => Specialized::class,
                'title' => $this->t('Archive'),
                'archived' => 1,
                'view' => 'archive',
            ],
            'selfTasks' => [
                'class' => SelfTasks::class,
            ],
            'markArchived' => [
                'class' => Archive::class,
            ],
            'refresh' => [
                'class' => Refresh::class,
            ],
            'users' => [
                'class' => Users::class,
            ],
            'delete' => [
                'class' => Delete::class,
            ],
            'addingUserToCallCenter' => [
                'class' => AddUserToFilter::class,
            ],
            'removeUserFromCallCenter' => [
                'class' => RemoveUserFromFilter::class,
            ],
            'addingUserToManagers' => [
                'class' => AddUserToFilter::class,
            ],
            'removeUserFromManagers' => [
                'class' => RemoveUserFromFilter::class,
            ],
        ];
    }

    /**
     * @property-description Добавление задач вручную
     */
    public function actionAddManualTask()
    {
        /* @var $model TaskBoardRecord */
        $model = new TaskBoardRecord('manual');
        $form = TbForm::createForm('widgets.form.config.taskBoard.taskAddManual', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model->owner_id = (int)$this->app()->user->id;
            if ($model->save()) {
                $this->redirect([Session::get(self::REDIRECT_KEY)]);
            }
        }

        $this->commitView(null, $this->t('Creating a task manually'));

        $viewData = [
            'form' => $form,
            'model' => $model,
            'title' => $this->t('Creating a task manually'),
        ];

        $this->render('add', $viewData);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     * @property-description просмотр задачи и добавление ответа
     */
    public function actionView($id)
    {
        Session::set(self::REDIRECT_KEY, $this->app()->controller->action->id . '&id=' . $id);

        /* @var $model TaskBoardRecord */
        $taskModel = TaskBoardRecord::loadModel($id);
        $form = TbForm::createForm('widgets.form.config.taskBoard.taskAddAnswer', $this, [
            'type' => 'horizontal',
        ], $taskModel);

        if ($form->submitted() && $form->validate()) {
            if (strip_tags($taskModel->answer) !== '') {
                $answer = new TaskBoardAnswersRecord();
                $answer->question_id = $taskModel->id;
                $answer->answer = $taskModel->answer;
                $answer->user_id = $this->app()->user->id;
                $answer->save(false);

                ++$taskModel->answer_counter;
            }

            if ($taskModel->save()) {
                $this->redirect([Session::get(self::REDIRECT_KEY)]);
            }
        }

        $viewData = [
            'form' => $form,
            'model' => $taskModel,
            'title' => $this->t('Add reply'),
        ];

        $this->render('view', $viewData);
    }

    /**
     * @property-description Добавление задач вручную из карточки заказов
     */
    public function actionAddManualTaskFromOrderCard()
    {
        if (isset($_GET['order_id'])) {
            $orderId = $_GET['order_id'];
            Session::set('order_id', $orderId);
            if (isset($_GET['products_id'])) {
                $products = $_GET['products_id'];
                Session::set('products_id', $products);
            }
        }

        /* @var $model TaskBoardRecord */
        $model = new TaskBoardRecord('manual');

        if (isset($_POST['TaskBoardRecord'])) {
            $model->attributes = $_POST['TaskBoardRecord'];
            if ($model->validate()) {
                $orderId = Session::get('order_id');
                $products = Session::get('products_id');
                $model->owner_id = (int)$this->app()->user->id;
                $model->order_id = $orderId;
                $model->products = $products === [] ? null : $products;
                if ($model->save()) {
                    Session::destroy('products_id');
                    echo CJSON::encode(['redirect' => $this->createUrl('callcenterOrder/processing', ['id' => Session::get('order_id')])]);
                }
            } else {
                echo CActiveForm::validate($model);
            }
        }

        $this->app()->end();
    }

    /**
     * @property-description Добавление задач из карточки заказа
     */
    public function actionAddTaskFromOrderCard()
    {
        if (isset($_GET['order_id'])) {
            $products = isset($_GET['products_id']) ? explode(',', $_GET['products_id']) : null;
            $orderId = $_GET['order_id'];

            Session::set('order_id', $orderId);
            Session::set('products_id', $products);
        }

        /* @var $model TaskBoardRecord */
        $model = new TaskBoardRecord('productCard');

        if (isset($_POST['TaskBoardRecord'])) {
            $model->attributes = $_POST['TaskBoardRecord'];
            if ($model->validate()) {
                $order = Session::get('order_id');
                $products = Session::get('products_id');

                $owner = (int)$this->app()->user->id;

                $qType = $model->getAttribute('question_type');
                $qDescription = $model->getAttribute('question_description');

                if ($products === [] && (int)$model->question_type === TaskBoardRecord::Q_PRODUCT) {
                    echo CJSON::encode(['error' => $this->t('To start select products')]);
                } else {
                    $determination = new TaskDetermination($owner, $products, $order, $qType, $qDescription);
                    $determination->save();
                    Session::destroy('products_id');
                    echo CJSON::encode(['redirect' => $this->createUrl('callcenterOrder/processing', ['id' => Session::get('order_id')])]);
                }
            } else {
                echo CActiveForm::validate($model);
            }
        }
        $this->app()->end();
    }

    /**
     * @param $id
     *
     * @property-description Редактирование задач
     */
    public function actionUpdate($id)
    {
        /* @var $model TaskBoardRecord */
        $model = TaskBoardRecord::loadModel($id);
        $model->setScenario('update');

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.taskBoard.taskUpdate', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted() && $form->validate()) {
            if (strip_tags($model->answer) !== '') {
                $answer = new TaskBoardAnswersRecord();
                $answer->question_id = $model->id;
                $answer->user_id = $this->app()->user->id;
                $answer->answer = $model->answer;
                $answer->save(false);

                $model->answer_counter += 1;
            }

            if ($model->save()) {
                $this->redirect([Session::get(self::REDIRECT_KEY)]);
            }
        }

        $viewData = [
            'model' => $model,
            'form' => $form,
        ];

        $this->render('update', $viewData);
    }

    /**
     * @property-description Поиск по таскам
     */
    public function actionSearch()
    {
        Session::set(self::REDIRECT_KEY, $this->app()->controller->action->id);

        $model = new TaskBoardRecord('task_search');
        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.taskBoard.taskSearch', $this, [
            'type' => 'horizontal',
        ], $model);

        $provider = [];
        if ($form->submitted() && $form->validate()) {
            $model->getByTaskId($model->id);
            $provider = $model->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('TaskBoardRecord', []),
                    'scenario' => 'search',
                ],
                'sort' => [
                    'defaultOrder' => 't.created_at ASC',
                ],
            ]);
        }

        $viewData = [
            'model' => $model,
            'form' => $form,
            'provider' => $provider,
            'title' => $this->t('Task search'),
        ];

        $this->render('search', $viewData);
    }

    /**
     * @property-description Получение Уведомлений
     */
    public function actionNotification()
    {
        $app = $this->app();
        $id = (int)$app->user->id;
        $reply = new Reply();

        $tasks = TaskBoardRecord::model()->findAll(['condition' => "(user_id =  $id OR owner_id = $id) AND is_archive = 0"]);
        $result = [];

        foreach ($tasks as $key => $value) {
            $type = (int)$value->task_board_type;
            $owner = (int)$value->owner_id;
            $user = (int)$value->user_id;

            if (($owner === $id || $user === $id) && (int)$value->answer_counter !== 0) {
                $result['answer'][$type] = isset($result['answer'][$type]) ? ($result['answer'][$type] + $value->answer_counter) : $value->answer_counter;
            } elseif ((int)$value->answer_counter === 0) {
                if ($user === $id) {
                    $result['question'][$type] = isset($result['question'][$type]) ? ++$result['question'][$type] : 1;
                }
                if ($owner === $id) {
                    $result['waiting'][$type] = isset($result['waiting'][$type]) ? ++$result['waiting'][$type] : 1;
                }
            }
        }

        $question = '';
        $answer = '';
        $waiting = '';

        foreach ($result as $key => $value) {
            foreach ($value as $k => $v) {
                $type = (int)$k;
                $count = (int)$v;
                switch ($key) {
                    case 'question' :
                        $question .= '<p> ' . TaskBoardRecord::getQuestionTypes($type) . ' - ' . $count . '</p>';
                        break;
                    case 'answer':
                        $answer .= '<p> ' . TaskBoardRecord::getQuestionTypes($type) . ' - ' . $count . '</p>';
                        break;
                    case 'waiting':
                        $waiting .= '<p> ' . TaskBoardRecord::getQuestionTypes($type) . ' - ' . $count . '</p>';
                        break;
                }
            }
        }

        $reply->addError($question);
        $reply->addInfo($answer);
        $reply->addWarning($waiting);
        $this->renderJson($reply->getResponse());
    }
}
