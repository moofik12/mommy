<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use MommyCom\Model\Db\UserBlackListRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Backend\Form\Form;

class UserBlackListController extends BackController
{
    /**
     * @property-description Просмотр списка
     */
    public function actionIndex()
    {
        $dataProvider = UserBlackListRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 100,
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('UserBlackListRecord', []),
                'scenario' => 'searchPrivileged',
                'safeOnly' => true, // не обязательный параметр, по умолчанию true
            ],
        ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @property-description Добавление пользователя в список
     */
    public function actionAdd()
    {
        $model = new UserBlackListRecord();

        /** @var Form $form */
        $form = Form::createForm('widgets.form.config.userBlackList', $model, [
            'type' => 'horizontal',
        ]);

        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect(['index']);
            }
        }

        $this->commitView(null, $this->t('Add user'));
        $this->render('add', ['form' => $form]);
    }

    /**
     * @property-description Обновление пользователя в списке
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $model = UserBlackListRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Not found data'));
        }

        /** @var Form $form */
        $form = Form::createForm('widgets.form.config.userBlackList', $model, [
            'type' => 'horizontal',
        ]);

        if ($form->submitted('submit') && $form->validate()) {
            if ($model->save()) {
                $this->redirect(['index']);
            }
        }

        $this->commitView($model, $this->t('User data update'));
        $this->render('update', ['form' => $form]);
    }

    /**
     * @property-description Исключение пользователя из списка
     *
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $model = UserBlackListRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Not found data'));
        }

        $model->status = UserBlackListRecord::STATUS_DELETED;
        $model->save();
    }
}
