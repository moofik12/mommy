<?php

namespace MommyCom\Controller\Backend;

use CActiveForm;
use CHttpException;
use CSort;
use MommyCom\Model\Db\HelperPageCategoryRecord;
use MommyCom\Model\Db\HelperPageRecord;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class HelperPageController extends BackController
{
    //действия для сортировки (position)
    public function actions()
    {
        return [
            'positionPage' => [
                'class' => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'HelperPageRecord',
            ],
        ];
    }

    public function actionCategory()
    {
        $provider = HelperPageCategoryRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('HelperPageCategoryRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_ASC,
                ],
            ],
        ]);

        $this->render('category/index', ['provider' => $provider]);
    }

    public function actionCategoryCreate()
    {
        /** @var HelperPageCategoryRecord $model */
        $model = new HelperPageCategoryRecord();

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.helperPageCategory', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->redirect(['category']);
            }
        }

        $this->render('category/create', ['form' => $form]);
    }

    /**
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionCategoryDelete($id)
    {
        $model = HelperPageCategoryRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        $model->delete();

        $this->redirect(['category']);
    }

    /**
     * @param integer $id
     *
     * @throws CHttpException
     */
    public function actionCategoryUpdate($id)
    {
        /** @var HelperPageCategoryRecord $model */
        $model = HelperPageCategoryRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.helperPageCategory', $this, [
            'type' => 'horizontal',
        ], $model);

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->redirect(['category']);
            }
        }

        $this->render('category/update', ['form' => $form]);
    }

    public function actionPage()
    {
        $provider = HelperPageRecord::model()->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('HelperPageRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_ASC,
                ],
            ],
        ]);

        $categories = HelperPageCategoryRecord::model()->findAll();

        $this->render('page/index', ['provider' => $provider, 'categories' => $categories]);
    }

    public function actionPageCreate()
    {
        /** @var HelperPageRecord $model */
        $model = new HelperPageRecord();

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.helperPage', $this, [
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
        ], $model);

        if ($this->app()->request->isAjaxRequest) {
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->redirect(['page']);
            }
        }

        $this->render('page/create', ['form' => $form]);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionPageDelete($id)
    {
        /** @var HelperPageRecord $model */
        $model = HelperPageRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t($this->t('Data not found')));
        }

        $model->delete();

        $this->redirect(['page']);
    }

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function actionPageUpdate($id)
    {
        /** @var HelperPageRecord $model */
        $model = HelperPageRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException($this->t('Data not found'));
        }

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.helperPage', $this, [
            'type' => 'horizontal',
            'enableAjaxValidation' => true,
        ], $model);

        if ($this->app()->request->isAjaxRequest) {
            echo CActiveForm::validate($model);
            $this->app()->end();
        }

        if ($form->submitted('submit') && $form->validate()) {
            $model = $form->getModel();
            if ($model->save()) {
                $this->redirect(['page']);
            }
        }

        $this->render('category/update', ['form' => $form]);
    }
}
