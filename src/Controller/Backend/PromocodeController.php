<?php

namespace MommyCom\Controller\Backend;

use CHttpException;
use CHttpRequest;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;
use TbForm;

class PromocodeController extends BackController
{
    public function actionIndex()
    {
        $request = $this->app()->request;
        /* @var $request CHttpRequest */
        // yii has bug in history
        $validUntilRangeArray = Cast::toStrArr($request->getParam('validUntilRange'));
        $validUntilRange = end($validUntilRangeArray);
        $isTimeActiveArray = Cast::toStrArr($request->getParam('isTimeActive'));
        $isTimeActive = end($isTimeActiveArray);

        $selector = PromocodeRecord::model();
        /* @var $selector PromocodeRecord */

        if (!empty($validUntilRange)) {
            list($validUntilFrom, $validUntilTo) = explode(' - ', $validUntilRange);

            $selector->timeValidUntilFrom(strtotime('today ' . $validUntilFrom));
            $selector->timeValidUntilTo(strtotime('tomorrow ' . $validUntilTo));
        } else {
            $validUntilFrom = 0;
            $validUntilTo = 0;
        }

        if ($isTimeActive == 'not') {
            $selector->onlyTimeInvalid();
        } elseif ($isTimeActive == 'yes') {
            $selector->onlyTimeValid();
        }

        $provider = $selector
            ->getDataProvider(false, [
                'pagination' => [
                    'pageSize' => 50,
                ],
                'sort' => [
                    'defaultOrder' => 't.id DESC',
                ],
                'filter' => [
                    'attributes' => $this->app()->request->getParam('PromocodeRecord', []),
                    'scenario' => 'searchPrivileged',
                ],
            ]);

        $this->commitView(null, $this->t('View list'));
        $this->render('index', [
            'provider' => $provider,
            'isTimeActive' => $isTimeActive,
            'eventStartAtRange' => $validUntilRange,
        ]);
    }

    public function actionAdd()
    {
        $model = new PromocodeRecord();
        /* @var $model PromocodeRecord */

        $form = TbForm::createForm('widgets.form.config.promocodeAdd', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            if ($model->save()) {
                $this->redirect(['promocode/index']);
            }
        }

        $this->commitView(null, $this->t('Add promocode'));
        $this->render('add', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $this->saveBackUrl();
        $model = PromocodeRecord::model()->findByPk($id);
        /* @var $model PromocodeRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        // alter model
        $form = TbForm::createForm('widgets.form.config.promocodeUpdate', $this, [
            'type' => 'horizontal',
        ], $model);
        /* @var $form TbForm */

        if ($form->submitted('submit')) {
            if ($model->save()) {
                $this->redirect(['promocode/index']);
            }
        }

        $this->commitView($model, $this->t('Update promocode'));
        // render
        $this->render('update', [
            'form' => $form,
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = PromocodeRecord::model()->findByPk($id);

        /* @var $model PromocodeRecord */

        if ($model === null) {
            throw new CHttpException(404, $this->t('Data not found'));
        }

        if (!$model->delete() && !$model->is_deleted) {
            throw new CHttpException(500, $this->t('Failed to delete promocode'));
        }
    }
}
