<?php

namespace MommyCom\Controller\Backend;

use CDataProvider;
use CDataProviderIterator;
use CDbCriteria;
use CHttpException;
use CSort;
use MommyCom\Model\Backend\OrderControlForm;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderControlRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\Model\Misc\CsvFile;
use MommyCom\Service\BaseController\BackController;
use TbForm;

class OrderControlController extends BackController
{
    /**
     * @var string
     */
    public $pageTitle = 'Order monitoring';

    const SAVE_SCV_MODE_NONE = 'none';
    const SAVE_SCV_MODE_FULL = 'full';
    const SAVE_SCV_MODE_FULL_DETAIL = 'fullDetail';

    public function actionIndex($saveCsvMode = self::SAVE_SCV_MODE_NONE)
    {
        $selector = OrderControlRecord::model();

        $filter['mailingEnded'] = $this->app()->request->getParam('mailingEnded', 1);
        $filter['orderDeliveryTypes'] = $this->app()->request->getParam('orderDeliveryTypes', []);
        $filter['statuses'] = $this->app()->request->getParam('statuses', []);
        $filter['orderMailedAt'] = $this->app()->request->getParam('orderMailedAt', false);
        $filter['pageSize'] = $this->app()->request->getParam('pageSize', 50);
        $filter['isDropShipping'] = $this->app()->request->getParam('isDropShipping', 'all');
        $filter['supplierId'] = $this->app()->request->getParam('supplierId', false);

        if ($filter['isDropShipping'] !== 'all') {
            $selector
                ->with([
                    'order' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'isDropShipping' => [$filter['isDropShipping']],
                        ],
                    ],
                ]);
        }

        if ($filter['supplierId']) {
            $selector
                ->with([
                    'order' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'supplierIdIs' => [$filter['supplierId']],
                        ],
                    ],
                ]);
        }

        if ($filter['statuses']) {
            $filter['statuses'] = is_scalar($filter['statuses'])
                ? explode(',', $filter['statuses']) : $filter['statuses'];
            $selector
                ->statusIn($filter['statuses']);
        }

        if ($filter['orderDeliveryTypes']) {
            $filter['orderDeliveryTypes'] = is_scalar($filter['orderDeliveryTypes'])
                ? explode(',', $filter['orderDeliveryTypes']) : $filter['orderDeliveryTypes'];
            $selector
                ->with([
                    'order' => [
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                        'scopes' => [
                            'deliveryTypes' => [$filter['orderDeliveryTypes']],
                        ],
                    ],
                ]);
        }

        if ($filter['mailingEnded']) {
            $selector->mailedOrderEnded();
        }

        if ($filter['orderMailedAt']) {
            list($start, $end) = explode(' - ', $filter['orderMailedAt']);
            $start = strtotime($start);
            $end = strtotime($end);

            if ($start === false || $end === false) {
                throw new CHttpException(500, $this->t('Error applying the time filter'));
            }

            $selector->orderMailedAtGreater($start)->orderMailedAtLower($end);
        }

        // grid
        $dataProvider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => max($filter['pageSize'], 5),
            ],
            'filter' => [
                'scenario' => 'searchPrivileged',
                'attributes' => $this->app()->request->getParam('OrderControlRecord', []),
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
        ]);

        if ($saveCsvMode !== self::SAVE_SCV_MODE_NONE) {
            $this->_sendCsvFile($dataProvider, $saveCsvMode);
            $this->app()->end();
        }

        if ($this->app()->request->isAjaxRequest) {
            $this->renderPartial('index', ['provider' => $dataProvider, 'filter' => $filter]);
            $this->app()->end();
        }

        $this->commitView(null, $this->t('View list'));
        $this->render('index', ['provider' => $dataProvider, 'filter' => $filter]);
    }

    /**'
     * @param int $id
     *
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        /** @var OrderControlRecord $model */
        $model = $this->loadModel($id);

        $modelForm = new OrderControlForm();

        /* @var $form TbForm */
        $form = TbForm::createForm('widgets.form.config.orderControl', $this, [
            'type' => 'horizontal',
        ], $modelForm);

        if (($form->submitted())) {
            if ($form->validate()) {
                $model->operator_status = $modelForm->operatorStatus;
                $model->comment = $modelForm->comment;

                if (!$model->save()) {
                    throw new CHttpException(400, $this->t('Saving error'));
                }

                $this->redirect(['index']);
            }
        } else {
            $modelForm->operatorStatus = $model->operator_status;
            $modelForm->comment = $model->comment;
        }

        $this->commitView($model, $this->t('Updating the order'));
        $this->render('update', ['form' => $form, 'model' => $model]);
    }

    /**
     * @param integer $id the ID of the model to be loaded
     *
     * @return null|StaticPageRecord
     * @throws CHttpException
     */
    protected function loadModel($id)
    {
        $id = intval($id);

        $model = OrderControlRecord::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, $this->t('Model does not exist.'));
        return $model;
    }

    private function _sendCsvFile(CDataProvider $provider, $saveCsvMode)
    {
        set_time_limit(300);

        $dbCriteria = new CDbCriteria();
        $dbCriteria->order = 'order_mailed_at';

        $provider->getSort()->applyOrder($dbCriteria);
        $fileName = 'order_monitoring_' . date('d_m_Y_H_i') . "_$saveCsvMode.csv";

        $legendData = [];
        $headerData = [];
        $dataForTime = [];
        $totalItems = 0;
        $ordersPrice = 0;
        $ordersPayPrice = 0;

        $legendData[] = ['Legend'];
        $legendData[] = []; // empty line
        foreach (OrderControlRecord::statusReplacements() as $key => $status) {
            $descriptionText = '';
            switch ($key) {
                case OrderControlRecord::STATUS_NEW:
                    $descriptionText = $this->t('Data added for monitoring');
                    break;

                case OrderControlRecord::STATUS_NOT_DELIVERED:
                    $descriptionText = $this->t('No information available on order delivery');
                    break;

                case OrderControlRecord::STATUS_DELIVERED:
                    $descriptionText = $this->t('Order delivered');
                    break;

                case OrderControlRecord::STATUS_NOT_PAYED:
                    $descriptionText = $this->t('Payment not recorded in the Acts');
                    break;

                case OrderControlRecord::STATUS_PAYED:
                    $descriptionText = $this->t('Payment recorded in the Act or the order was fully paid by non-cash method');
                    break;

                case OrderControlRecord::STATUS_RETURNED:
                    $descriptionText = $this->t('Order returned to the warehouse');
                    break;

                case OrderControlRecord::STATUS_RETURNED_PART:
                    $descriptionText = $this->t('Order partially returned to the warehouse');
                    break;

                case OrderControlRecord::STATUS_LOST:
                    $descriptionText = $this->t('Order not delivered within 16 days and there is no record of it in payment Acts');
                    break;

                default:
                    break;
            }

            $legendData[] = [$status, $descriptionText];
        }

        $iterator = new CDataProviderIterator($provider, 5);

        foreach ($iterator as $key => $model) {
            /* @var OrderControlRecord $model */
            if (empty($headerData)) {
                $headerData = [
                    '№',
                    $model->getAttributeLabel('order_id'),
                    $model->getAttributeLabel('order_mailed_at'),
                    $model->getAttributeLabel('order.delivery_type'),
                    $model->getAttributeLabel('order.price_total'),
                    $this->t('Amount due'),
                    $model->getAttributeLabel('status'),
                    $model->getAttributeLabel('operator_status'),
                    $this->t('Tracking number'),
                    $this->t('Return tracking number'),
                    $model->getAttributeLabel('comment'),
                    $model->getAttributeLabel('system_comment'),
                ];

                if ($saveCsvMode == self::SAVE_SCV_MODE_FULL_DETAIL) {
                    $headerData[] = 'additional data';
                }
            }

            $dateItem = date('m.Y', $model->order_mailed_at);
            $data = [
                $key + 1,
                $model->order_id,
                $this->app()->dateFormatter->formatDateTime($model->order_mailed_at),
                $model->order->deliveryTypeReplacement,
                $model->order->price_total,
                $model->order->getPayPriceHistory(),
                $model->statusReplacement(),
                $model->operatorStatusReplacement(),
                $model->order->trackcode,
                $model->order->tracking->trackcode_return,
                $model->comment,
                $model->system_comment,
            ];

            if ($saveCsvMode == self::SAVE_SCV_MODE_FULL_DETAIL) {
                $detail = '';

                if ($modelBankRefund = OrderBankRefundRecord::model()->orderId($model->order_id)->find()) {
                    if ($modelBankRefund->is_refunded) {
                        $detail .= "Refund fulfilled" . ' ' . $modelBankRefund->price_refund . ' ' . "on" . ' ' .
                            $modelBankRefund->typeRefundReplacement();
                    }
                }

                if ($orderReturns = OrderReturnRecord::model()->orderId($model->order_id)->findAll()) {
                    /* @var OrderReturnRecord[] $orderReturns */
                    foreach ($orderReturns as $orderReturn) {
                        if ($orderReturn->price_refund) {
                            $detail .= "Order No" . $orderReturn->order_id . ' ' . 'return fulfilled' . ' ' . 'on' .
                                $orderReturn->refundTypeReplacement . ' ' . 'in the amount of' . ' ' . $orderReturn->price_refund;
                        }
                    }
                }

                $data[] = $detail;
            }

            if (!isset($dataForTime[$dateItem])) {
                $dataForTime[$dateItem][] = [];
                $dataForTime[$dateItem][] = [];
                $dataForTime[$dateItem][] = ['Date', $dateItem];
                $dataForTime[$dateItem][] = $headerData;
            }

            $dataForTime[$dateItem][] = $data;
            $totalItems++;
            $ordersPrice += $model->order->price_total;
            $ordersPayPrice += $model->order->getPayPriceHistory();
        }

        $normalizeData = [];

        foreach ($dataForTime as $item) {
            foreach ($item as $row) {
                $normalizeData[] = $row;
            }
        }

        $totalData[] = [];
        $totalData[] = ['Qty', $totalItems];
        $totalData[] = ['Total', $ordersPrice];
        $totalData[] = ['Amount', $ordersPayPrice];
        $totalData[] = [];
        $totalData[] = [];

        $csv = CsvFile::loadFromArray(array_merge($legendData, $totalData, $normalizeData));

        $this->app()->request->sendFile($fileName, $csv->toString(), 'text/csv');
    }

}
