<?php

namespace MommyCom\Controller\BackendTablet;

use MommyCom\Model\BackendTablet\AuthForm;
use MommyCom\Service\BaseController\BackController;

class AuthController extends BackController
{
    /**
     * @var string
     */
    public $layout = '//layouts/auth';

    /**
     * @throws \CException
     */
    public function actionLogin()
    {
        $modelForm = new AuthForm();

        $authForm = $this->app()->getRequest()->getPost('AuthForm');

        if (!empty($authForm)) {
            $modelForm->setAttributes($authForm);

            if ($modelForm->validate() && $modelForm->login()) {
                $this->redirect(['/index']);
            }
        }

        $modelForm->unsetAttributes(['password']);

        $this->render('auth', ['modelForm' => $modelForm]);
    }

    public function actionLogout()
    {
        $this->app()->user->logout();
        $this->redirect(['login']);
    }
}
