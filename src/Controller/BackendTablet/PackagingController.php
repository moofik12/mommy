<?php

namespace MommyCom\Controller\BackendTablet;

use CClientScript;
use CHttpException;
use CHttpRequest;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\TabletPackagingRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Type\Cast;

class PackagingController extends BackController
{
    public function actionIndex()
    {
        $request = $this->app()->request;
        /* @var $request CHttpRequest */
        // yii has bug in history
        $taskOwnerArray = Cast::toStrArr($request->getParam('taskOwner', 'me'));
        $taskOwner = end($taskOwnerArray);

        $selector = TabletPackagingRecord::model()
            ->orderBy('created_at', 'desc')
            ->orderProcessingStatuses([OrderRecord::PROCESSING_CALLCENTER_CONFIRMED, OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED])
            //->onlyUnPackaged()
            ->onlyPackagingReady()
            ->with('order');
        /* @var $selector TabletPackagingRecord */

        if ($taskOwner == 'me') {
            $selector->userId($this->app()->user->id);
        }

        $provider = $selector->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => [
                'defaultOrder' => 't.created_at DESC, t.id DESC',
            ],
            'filter' => [
                'attributes' => $this->app()->request->getParam('TabletPackagingRecord', []),
                'scenario' => 'searchPrivileged',
            ],
        ]);

        $this->render('index', [
            'provider' => $provider,
            'taskOwner' => $taskOwner,
        ]);
    }

    public function actionPositions($id)
    {
        $model = OrderRecord::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'Data not found');
        }

        $positionsProvider = OrderProductRecord::model()
            ->callcenterStatus(OrderProductRecord::CALLCENTER_STATUS_ACCEPTED)
            ->orderId($id)
            ->getDataProvider(false, [
                'pagination' => false,
            ]);

        $data = [
            'model' => $model,
            'positionsProvider' => $positionsProvider,
        ];

        if ($this->app()->request->isAjaxRequest) {
            $clientScript = $this->app()->clientScript;
            /* @var $clientScript CClientScript */
            $output = $this->renderPartial('positions', $data, true);
            $clientScript->renderBodyBegin($output);
            $clientScript->renderBodyEnd($output);
            echo $output;
        } else {
            $this->render('positions', $data);
        }
    }

    public function actionWarehouse($id)
    {
        $model = OrderProductRecord::model()->findByPk($id);
        /* @var $model OrderProductRecord */

        if ($model === null) {
            throw new CHttpException(404, 'Data not found');
        }

        $provider = WarehouseProductRecord::model()
            ->orderProductId($model->id)
            ->getDataProvider();

        $data = [
            'model' => $model,
            'provider' => $provider,
        ];

        if ($this->app()->request->isAjaxRequest) {
            $clientScript = $this->app()->clientScript;
            /* @var $clientScript CClientScript */
            $output = $this->renderPartial('warehouse', $data, true);
            $clientScript->renderBodyBegin($output);
            $clientScript->renderBodyEnd($output);
            echo $output;
        } else {
            $this->render('warehouse', $data);
        }
    }
}
