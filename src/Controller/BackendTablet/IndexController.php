<?php

namespace MommyCom\Controller\BackendTablet;

use MommyCom\Service\BaseController\BackController;

class IndexController extends BackController
{
    public function actionIndex()
    {
        $this->render('index');
    }
} 
