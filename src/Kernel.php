<?php

namespace MommyCom;

use MommyCom\YiiComponent\Facade\AbstractFacade;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    /**
     * @var string
     */
    protected $appName;

    /**
     * Kernel constructor.
     *
     * @param string $appName
     * @param string $environment
     * @param bool $debug
     */
    public function __construct(string $appName, string $environment, bool $debug)
    {
        $this->appName = $appName;

        parent::__construct($environment, $debug);
    }

    public function getCacheDir()
    {
        return $this->getProjectDir() . '/var/cache/' . $this->appName . '/' . $this->environment;
    }

    public function getLogDir()
    {
        return $this->getProjectDir() . '/var/log/' . $this->appName;
    }

    public function getName()
    {
        return 'web';
    }

    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        /** @noinspection PhpIncludeInspection */
        $contents = require $this->getProjectDir() . '/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if (isset($envs['all']) || isset($envs[$this->environment])) {
                yield new $class();
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $container->addResource(new FileResource($this->getProjectDir() . '/config/bundles.php'));
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir() . '/config';

        $loader->load($confDir . '/{packages}/*' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{packages}/' . $this->environment . '/**/*' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{common}' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/{common_' . $this->environment . '}' . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/' . $this->appName . self::CONFIG_EXTS, 'glob');
        $loader->load($confDir . '/' . $this->appName . '_' . $this->environment . self::CONFIG_EXTS, 'glob');
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $confDir = $this->getProjectDir() . '/config';

        $routes->import($confDir . '/{routes}/defaults' . self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir . '/{routes}/' . $this->appName . self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir . '/{routes}/' . $this->environment . '/**/*' . self::CONFIG_EXTS, '/', 'glob');
    }

    public function boot()
    {
        $booted = $this->booted;

        parent::boot();

        if (!$booted) {
            AbstractFacade::setContainer($this->container);
        }
    }

    public function shutdown()
    {
        if ($this->booted) {
            /** @noinspection PhpUnhandledExceptionInspection */
            \Yii::setApplication(null);
            AbstractFacade::clearContainer();
        }

        parent::shutdown();
    }
}
