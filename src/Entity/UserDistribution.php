<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserDistributionRepository")
 * @ORM\Table(name="users_distributions")
 */
class UserDistribution
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @var int
     */
    private $type;

    /**
     * @ORM\Column(name="is_subscribe", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSubscribe;

    /**
     * @ORM\Column(name="is_update", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isUpdate;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isSubscribe()
    {
        return $this->isSubscribe;
    }

    /**
     * @param bool $isSubscribe
     */
    public function setIsSubscribe($isSubscribe)
    {
        $this->isSubscribe = $isSubscribe;
    }

    /**
     * @return bool
     */
    public function isUpdate()
    {
        return $this->isUpdate;
    }

    /**
     * @param bool $isUpdate
     */
    public function setIsUpdate($isUpdate)
    {
        $this->isUpdate = $isUpdate;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
