<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\PromocodeRepository")
 * @ORM\Table(name="promocodes")
 */
class Promocode
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="promocode", type="string", length=21, nullable=false)
     *
     * @var string
     */
    private $promocode;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @var int
     */
    private $type;

    /**
     * @ORM\Column(name="reason", type="integer", nullable=false)
     *
     * @var int
     */
    private $reason;

    /**
     * @ORM\Column(name="reason_description", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $reasonDescription;

    /**
     * @ORM\Column(name="percent", type="integer", nullable=false)
     *
     * @var int
     */
    private $percent;

    /**
     * @ORM\Column(name="cash", type="integer", nullable=false)
     *
     * @var int
     */
    private $cash;

    /**
     * @ORM\Column(name="order_price_after", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderPriceAfter;

    /**
     * @ORM\Column(name="valid_until", type="integer", nullable=false)
     *
     * @var int
     */
    private $validUntil;

    /**
     * @ORM\Column(name="event_ids_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $eventIdsJson;

    /**
     * @ORM\Column(name="event_product_ids_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $eventProductIdsJson;

    /**
     * @ORM\Column(name="products_ids_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $productsIdsJson;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDeleted;

    /**
     * @ORM\Column(name="benefice_user_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $beneficeUserId;

    /**
     * @ORM\Column(name="benefice_percent", type="integer", nullable=false)
     *
     * @var int
     */
    private $beneficePercent;

    /**
     * @ORM\Column(name="benefice_cash", type="integer", nullable=false)
     *
     * @var int
     */
    private $beneficeCash;

    /**
     * @ORM\Column(name="is_benefice_payed", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isBeneficePayed;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $adminId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="partner_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $partnerId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * @param string $promocode
     */
    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param int $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getReasonDescription()
    {
        return $this->reasonDescription;
    }

    /**
     * @param string $reasonDescription
     */
    public function setReasonDescription($reasonDescription)
    {
        $this->reasonDescription = $reasonDescription;
    }

    /**
     * @return int
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param int $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return int
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * @param int $cash
     */
    public function setCash($cash)
    {
        $this->cash = $cash;
    }

    /**
     * @return int
     */
    public function getOrderPriceAfter()
    {
        return $this->orderPriceAfter;
    }

    /**
     * @param int $orderPriceAfter
     */
    public function setOrderPriceAfter($orderPriceAfter)
    {
        $this->orderPriceAfter = $orderPriceAfter;
    }

    /**
     * @return int
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param int $validUntil
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @return string
     */
    public function getEventIdsJson()
    {
        return $this->eventIdsJson;
    }

    /**
     * @param string $eventIdsJson
     */
    public function setEventIdsJson($eventIdsJson)
    {
        $this->eventIdsJson = $eventIdsJson;
    }

    /**
     * @return string
     */
    public function getEventProductIdsJson()
    {
        return $this->eventProductIdsJson;
    }

    /**
     * @param string $eventProductIdsJson
     */
    public function setEventProductIdsJson($eventProductIdsJson)
    {
        $this->eventProductIdsJson = $eventProductIdsJson;
    }

    /**
     * @return string
     */
    public function getProductsIdsJson()
    {
        return $this->productsIdsJson;
    }

    /**
     * @param string $productsIdsJson
     */
    public function setProductsIdsJson($productsIdsJson)
    {
        $this->productsIdsJson = $productsIdsJson;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return int
     */
    public function getBeneficeUserId()
    {
        return $this->beneficeUserId;
    }

    /**
     * @param int $beneficeUserId
     */
    public function setBeneficeUserId($beneficeUserId)
    {
        $this->beneficeUserId = $beneficeUserId;
    }

    /**
     * @return int
     */
    public function getBeneficePercent()
    {
        return $this->beneficePercent;
    }

    /**
     * @param int $beneficePercent
     */
    public function setBeneficePercent($beneficePercent)
    {
        $this->beneficePercent = $beneficePercent;
    }

    /**
     * @return int
     */
    public function getBeneficeCash()
    {
        return $this->beneficeCash;
    }

    /**
     * @param int $beneficeCash
     */
    public function setBeneficeCash($beneficeCash)
    {
        $this->beneficeCash = $beneficeCash;
    }

    /**
     * @return bool
     */
    public function isBeneficePayed()
    {
        return $this->isBeneficePayed;
    }

    /**
     * @param bool $isBeneficePayed
     */
    public function setIsBeneficePayed($isBeneficePayed)
    {
        $this->isBeneficePayed = $isBeneficePayed;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $adminId
     */
    public function setAdminId($adminId)
    {
        $this->adminId = $adminId;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partnerId
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
    }
}
