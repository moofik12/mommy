<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\WarehouseProductRepository")
 * @ORM\Table(name="warehouse_products")
 */
class WarehouseProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="packaging_security_code", type="integer", nullable=false)
     *
     * @var int
     */
    private $packagingSecurityCode;

    /**
     * @ORM\Column(name="event_product_sizeformat", type="string", length=10, nullable=false)
     *
     * @var string
     */
    private $eventProductSizeformat;

    /**
     * @ORM\Column(name="event_product_size", type="string", length=15, nullable=false)
     *
     * @var string
     */
    private $eventProductSize;

    /**
     * @ORM\Column(name="event_product_price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $eventProductPrice;

    /**
     * @ORM\Column(name="order_product_price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $orderProductPrice = 0;

    /**
     * @ORM\Column(name="is_security_code_checked", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSecurityCodeChecked = false;

    /**
     * @ORM\Column(name="is_return", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isReturn = false;

    /**
     * @ORM\Column(name="is_supplier_informed", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSupplierInformed = false;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $statusComment = "";

    /**
     * @ORM\Column(name="status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusPrev = 0;

    /**
     * @ORM\Column(name="status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $statusHistoryJson = "";

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt = 0;

    /**
     * @ORM\Column(name="lost_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $lostStatus = 0;

    /**
     * @ORM\Column(name="lost_status_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $lostStatusComment = '';

    /**
     * @ORM\Column(name="lost_status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $lostStatusPrev = 0;

    /**
     * @ORM\Column(name="lost_status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $lostStatusHistoryJson = '';

    /**
     * @ORM\Column(name="lost_status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $lostStatusChangedAt = 0;

    /**
     * @ORM\Column(name="requestnum", type="integer", nullable=false)
     *
     * @var int
     */
    private $requestnum;

    /**
     * @ORM\Column(name="request_fileid", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $requestFileid = '';

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="event_product_id", type="integer", nullable=false)
     *
     * @var int EventProduct
     */
    private $eventProductId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     *
     * @var int Product
     */
    private $productId;

    /**
     * @ORM\Column(name="order_user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $orderUserId = 0;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId = 0;

    /**
     * @ORM\Column(name="order_product_id", type="integer", nullable=false)
     *
     * @var int OrderProduct
     */
    private $orderProductId = 0;

    /**
     * @ORM\Column(name="return_id", type="integer", nullable=false)
     *
     * @var int OrderReturn
     */
    private $returnId = 0;

    /**
     * @ORM\Column(name="return_product_id", type="integer", nullable=false)
     *
     * @var int OrderReturnProduct
     */
    private $returnProductId = 0;

    /**
     * @ORM\Column(name="prev_warehouse_id", type="integer", nullable=false)
     *
     * @var int WarehouseProduct
     */
    private $prevWarehouseId = 0;

    /**
     * @ORM\Column(name="prev_order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $prevOrderId = 0;

    /**
     * @ORM\Column(name="prev_order_product_id", type="integer", nullable=false)
     *
     * @var int OrderProduct
     */
    private $prevOrderProductId = 0;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPackagingSecurityCode()
    {
        return $this->packagingSecurityCode;
    }

    /**
     * @param int $packagingSecurityCode
     */
    public function setPackagingSecurityCode($packagingSecurityCode)
    {
        $this->packagingSecurityCode = $packagingSecurityCode;
    }

    /**
     * @return string
     */
    public function getEventProductSizeformat()
    {
        return $this->eventProductSizeformat;
    }

    /**
     * @param string $eventProductSizeformat
     */
    public function setEventProductSizeformat($eventProductSizeformat)
    {
        $this->eventProductSizeformat = $eventProductSizeformat;
    }

    /**
     * @return string
     */
    public function getEventProductSize()
    {
        return $this->eventProductSize;
    }

    /**
     * @param string $eventProductSize
     */
    public function setEventProductSize($eventProductSize)
    {
        $this->eventProductSize = $eventProductSize;
    }

    /**
     * @return float
     */
    public function getEventProductPrice()
    {
        return $this->eventProductPrice;
    }

    /**
     * @param float $eventProductPrice
     */
    public function setEventProductPrice($eventProductPrice)
    {
        $this->eventProductPrice = $eventProductPrice;
    }

    /**
     * @return float
     */
    public function getOrderProductPrice()
    {
        return $this->orderProductPrice;
    }

    /**
     * @param float $orderProductPrice
     */
    public function setOrderProductPrice($orderProductPrice)
    {
        $this->orderProductPrice = $orderProductPrice;
    }

    /**
     * @return bool
     */
    public function isSecurityCodeChecked()
    {
        return $this->isSecurityCodeChecked;
    }

    /**
     * @param bool $isSecurityCodeChecked
     */
    public function setIsSecurityCodeChecked($isSecurityCodeChecked)
    {
        $this->isSecurityCodeChecked = $isSecurityCodeChecked;
    }

    /**
     * @return bool
     */
    public function isReturn()
    {
        return $this->isReturn;
    }

    /**
     * @param bool $isReturn
     */
    public function setIsReturn($isReturn)
    {
        $this->isReturn = $isReturn;
    }

    /**
     * @return bool
     */
    public function isSupplierInformed()
    {
        return $this->isSupplierInformed;
    }

    /**
     * @param bool $isSupplierInformed
     */
    public function setIsSupplierInformed($isSupplierInformed)
    {
        $this->isSupplierInformed = $isSupplierInformed;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatusComment()
    {
        return $this->statusComment;
    }

    /**
     * @param string $statusComment
     */
    public function setStatusComment($statusComment)
    {
        $this->statusComment = $statusComment;
    }

    /**
     * @return int
     */
    public function getStatusPrev()
    {
        return $this->statusPrev;
    }

    /**
     * @param int $statusPrev
     */
    public function setStatusPrev($statusPrev)
    {
        $this->statusPrev = $statusPrev;
    }

    /**
     * @return string
     */
    public function getStatusHistoryJson()
    {
        return $this->statusHistoryJson;
    }

    /**
     * @param string $statusHistoryJson
     */
    public function setStatusHistoryJson($statusHistoryJson)
    {
        $this->statusHistoryJson = $statusHistoryJson;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return int
     */
    public function getLostStatus()
    {
        return $this->lostStatus;
    }

    /**
     * @param int $lostStatus
     */
    public function setLostStatus($lostStatus)
    {
        $this->lostStatus = $lostStatus;
    }

    /**
     * @return string
     */
    public function getLostStatusComment()
    {
        return $this->lostStatusComment;
    }

    /**
     * @param string $lostStatusComment
     */
    public function setLostStatusComment($lostStatusComment)
    {
        $this->lostStatusComment = $lostStatusComment;
    }

    /**
     * @return int
     */
    public function getLostStatusPrev()
    {
        return $this->lostStatusPrev;
    }

    /**
     * @param int $lostStatusPrev
     */
    public function setLostStatusPrev($lostStatusPrev)
    {
        $this->lostStatusPrev = $lostStatusPrev;
    }

    /**
     * @return string
     */
    public function getLostStatusHistoryJson()
    {
        return $this->lostStatusHistoryJson;
    }

    /**
     * @param string $lostStatusHistoryJson
     */
    public function setLostStatusHistoryJson($lostStatusHistoryJson)
    {
        $this->lostStatusHistoryJson = $lostStatusHistoryJson;
    }

    /**
     * @return int
     */
    public function getLostStatusChangedAt()
    {
        return $this->lostStatusChangedAt;
    }

    /**
     * @param int $lostStatusChangedAt
     */
    public function setLostStatusChangedAt($lostStatusChangedAt)
    {
        $this->lostStatusChangedAt = $lostStatusChangedAt;
    }

    /**
     * @return int
     */
    public function getRequestnum()
    {
        return $this->requestnum;
    }

    /**
     * @param int $requestnum
     */
    public function setRequestnum($requestnum)
    {
        $this->requestnum = $requestnum;
    }

    /**
     * @return string
     */
    public function getRequestFileid()
    {
        return $this->requestFileid;
    }

    /**
     * @param string $requestFileid
     */
    public function setRequestFileid($requestFileid)
    {
        $this->requestFileid = $requestFileid;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int EventProduct
     */
    public function getEventProductId()
    {
        return $this->eventProductId;
    }

    /**
     * @param int $eventProduct EventProduct
     */
    public function setEventProductId($eventProduct)
    {
        $this->eventProductId = $eventProduct;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int Product
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $product Product
     */
    public function setProductId($product)
    {
        $this->productId = $product;
    }

    /**
     * @return int User
     */
    public function getOrderUserId()
    {
        return $this->orderUserId;
    }

    /**
     * @param int $orderUser User
     */
    public function setOrderUserId($orderUser)
    {
        $this->orderUserId = $orderUser;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int OrderProduct
     */
    public function getOrderProductId()
    {
        return $this->orderProductId;
    }

    /**
     * @param int $orderProduct OrderProduct
     */
    public function setOrderProductId($orderProduct)
    {
        $this->orderProductId = $orderProduct;
    }

    /**
     * @return int OrderReturn
     */
    public function getReturnId()
    {
        return $this->returnId;
    }

    /**
     * @param int $return OrderReturn
     */
    public function setReturnId($return)
    {
        $this->returnId = $return;
    }

    /**
     * @return int OrderReturnProduct
     */
    public function getReturnProductId()
    {
        return $this->returnProductId;
    }

    /**
     * @param int $returnProduct OrderReturnProduct
     */
    public function setReturnProductId($returnProduct)
    {
        $this->returnProductId = $returnProduct;
    }

    /**
     * @return int WarehouseProduct
     */
    public function getPrevWarehouseId()
    {
        return $this->prevWarehouseId;
    }

    /**
     * @param int $prevWarehouse WarehouseProduct
     */
    public function setPrevWarehouseId($prevWarehouse)
    {
        $this->prevWarehouseId = $prevWarehouse;
    }

    /**
     * @return int Order
     */
    public function getPrevOrderId()
    {
        return $this->prevOrderId;
    }

    /**
     * @param int $prevOrder Order
     */
    public function setPrevOrderId($prevOrder)
    {
        $this->prevOrderId = $prevOrder;
    }

    /**
     * @return int OrderProduct
     */
    public function getPrevOrderProductId()
    {
        return $this->prevOrderProductId;
    }

    /**
     * @param int $prevOrderProduct OrderProduct
     */
    public function setPrevOrderProductId($prevOrderProduct)
    {
        $this->prevOrderProductId = $prevOrderProduct;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
