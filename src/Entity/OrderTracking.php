<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;
use MommyCom\Service\CurrentTime;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderTrackingRepository")
 * @ORM\Table(name="orders_tracking")
 */
class OrderTracking
{
    public const STATUS_UNKNOWN = 0; // неизвестно
    public const STATUS_LOST = 1; // потерялось (ну а вдруг) (пока не используется)
    public const STATUS_DELIVERED = 2; // доставлено
    public const STATUS_PROCESSING = 3; // зарезервировано, но не оправлено
    public const STATUS_SHOP_WAREHOUSE = 4; // на почтовом складе для отправики
    public const STATUS_TRANSITION = 5; // в пути
    public const STATUS_CLIENT_WAREHOUSE = 6; // на почтовом складе у клиента
    public const STATUS_COURIER = 7; // у курьера
    public const STATUS_RETURNING = 8; // возвращается
    public const STATUS_RETURNING_WAREHOUSE = 9; // вернулся на склад
    public const STATUS_RETURNED = 10; // вернулось и было получено
    public const STATUS_INIT = 11; // создание без отправки
    public const STATUS_FORWARDING = 12; // переадресация
    public const STATUS_CLIENT_REFUSAL = 13; // отказ от посылки
    public const STATUS_CLIENT_WAREHOUSE_AND_PENALTY = 14; // на складе и начисляется пеня
    public const STATUS_CLIENT_WAREHOUSE_AND_LOST = 15; // на складе и остановлено хранение
    public const STATUS_RETURNING_LOST = 16; // потерялся при возврате

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="is_drop_shipping", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDropShipping;

    /**
     * @ORM\Column(name="order_delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderDeliveryType;

    /**
     * @ORM\Column(name="delivery_price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $deliveryPrice;

    /**
     * @ORM\Column(name="trackcode", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $trackcode;

    /**
     * @ORM\Column(name="trackcode_return", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $trackcodeReturn;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status = self::STATUS_UNKNOWN;

    /**
     * @ORM\Column(name="status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusPrev;

    /**
     * @ORM\Column(name="status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $statusHistoryJson;

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt;

    /**
     * @ORM\Column(name="deadline_for_delivery", type="integer", nullable=false)
     *
     * @var int
     */
    private $deadlineForDelivery;

    /**
     * @ORM\Column(name="delivered_date", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveredDate;

    /**
     * @ORM\Column(name="synchronized_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $synchronizedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDropShipping()
    {
        return $this->isDropShipping;
    }

    /**
     * @param bool $isDropShipping
     */
    public function setIsDropShipping($isDropShipping)
    {
        $this->isDropShipping = $isDropShipping;
    }

    /**
     * @return int
     */
    public function getOrderDeliveryType()
    {
        return $this->orderDeliveryType;
    }

    /**
     * @param int $orderDeliveryType
     */
    public function setOrderDeliveryType($orderDeliveryType)
    {
        $this->orderDeliveryType = $orderDeliveryType;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice()
    {
        return $this->deliveryPrice;
    }

    /**
     * @param float $deliveryPrice
     */
    public function setDeliveryPrice($deliveryPrice)
    {
        $this->deliveryPrice = $deliveryPrice;
    }

    /**
     * @return string
     */
    public function getTrackcode()
    {
        return $this->trackcode;
    }

    /**
     * @param string $trackcode
     */
    public function setTrackcode($trackcode)
    {
        $this->trackcode = $trackcode;
    }

    /**
     * @return string
     */
    public function getTrackcodeReturn()
    {
        return $this->trackcodeReturn;
    }

    /**
     * @param string $trackcodeReturn
     */
    public function setTrackcodeReturn($trackcodeReturn)
    {
        $this->trackcodeReturn = $trackcodeReturn;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusPrev()
    {
        return $this->statusPrev;
    }

    /**
     * @param int $statusPrev
     */
    public function setStatusPrev($statusPrev)
    {
        $this->statusPrev = $statusPrev;
    }

    /**
     * @return string
     */
    public function getStatusHistoryJson()
    {
        return $this->statusHistoryJson;
    }

    /**
     * @param string $statusHistoryJson
     */
    public function setStatusHistoryJson($statusHistoryJson)
    {
        $this->statusHistoryJson = $statusHistoryJson;
    }

    /**
     * @return array
     */
    public function getStatusHistory(): array
    {
        return json_decode((string)$this->getStatusHistoryJson(), true, 3) ?: [];
    }

    /**
     * @param array $statusHistory
     */
    public function setStatusHistory(array $statusHistory): void
    {
        $this->setStatusHistoryJson(json_encode($statusHistory));
    }

    /**
     * @param int $status
     * @param int|null $adminId
     * @param int|null $userId
     *
     * @return array
     */
    public function addStatusToHistory(int $status, ?int $adminId, ?int $userId): array
    {
        $statusHistory = $this->getStatusHistory();

        $statusHistory[CurrentTime::getUnixTimestamp()] = [
            'status' => $status,
            'admin_id' => $adminId,
            'user_id' => $userId,
        ];

        ksort($statusHistory);

        $this->setStatusHistory($statusHistory);

        return $statusHistory;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return int
     */
    public function getDeadlineForDelivery()
    {
        return $this->deadlineForDelivery;
    }

    /**
     * @param int $deadlineForDelivery
     */
    public function setDeadlineForDelivery($deadlineForDelivery)
    {
        $this->deadlineForDelivery = $deadlineForDelivery;
    }

    /**
     * @return int
     */
    public function getDeliveredDate()
    {
        return $this->deliveredDate;
    }

    /**
     * @param int $deliveredDate
     */
    public function setDeliveredDate($deliveredDate)
    {
        $this->deliveredDate = $deliveredDate;
    }

    /**
     * @return int
     */
    public function getSynchronizedAt()
    {
        return $this->synchronizedAt;
    }

    /**
     * @param int $synchronizedAt
     */
    public function setSynchronizedAt($synchronizedAt)
    {
        $this->synchronizedAt = $synchronizedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }
}
