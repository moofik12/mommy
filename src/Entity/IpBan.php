<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\IpBanRepository")
 * @ORM\Table(name="ipbans")
 */
class IpBan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="ip", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $ip;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="section", type="integer", nullable=false)
     *
     * @var int
     */
    private $section;

    /**
     * @ORM\Column(name="banned_to", type="integer", nullable=false)
     *
     * @var int
     */
    private $bannedTo;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param int $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }

    /**
     * @return int
     */
    public function getBannedTo()
    {
        return $this->bannedTo;
    }

    /**
     * @param int $bannedTo
     */
    public function setBannedTo($bannedTo)
    {
        $this->bannedTo = $bannedTo;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
