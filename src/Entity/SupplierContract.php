<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierContractRepository")
 * @ORM\Table(name="suppliers_contracts")
 */
class SupplierContract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDefault;

    /**
     * @ORM\Column(name="not_delivered_penalty_percent", type="integer", nullable=false)
     *
     * @var int
     */
    private $notDeliveredPenaltyPercent;

    /**
     * @ORM\Column(name="delivery_working_days", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryWorkingDays;

    /**
     * @ORM\Column(name="add_delivery_working_days", type="integer", nullable=false)
     *
     * @var int
     */
    private $addDeliveryWorkingDays;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="agreement", type="string", length=255, nullable=true)
     *
     * @var null|string
     */
    private $agreement;

    /**
     * @ORM\Column(name="paid_by", type="string", length=255, nullable=true)
     *
     * @var null|string
     */
    private $paidBy;

    /**
     * @ORM\Column(name="days_to_pay", type="integer", nullable=true)
     *
     * @var null|int
     */
    private $daysToPay;

    /**
     * @ORM\Column(name="discount_from_market", type="integer", nullable=true)
     *
     * @var int
     */
    private $discountFromMarket;

    /**
     * @ORM\Column(name="discount_from_wholesale", type="integer", nullable=true)
     *
     * @var int
     */
    private $discountFromWholesale;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

    /**
     * @return int
     */
    public function getNotDeliveredPenaltyPercent()
    {
        return $this->notDeliveredPenaltyPercent;
    }

    /**
     * @param int $notDeliveredPenaltyPercent
     */
    public function setNotDeliveredPenaltyPercent($notDeliveredPenaltyPercent)
    {
        $this->notDeliveredPenaltyPercent = $notDeliveredPenaltyPercent;
    }

    /**
     * @return int
     */
    public function getDeliveryWorkingDays()
    {
        return $this->deliveryWorkingDays;
    }

    /**
     * @param int $deliveryWorkingDays
     */
    public function setDeliveryWorkingDays($deliveryWorkingDays)
    {
        $this->deliveryWorkingDays = $deliveryWorkingDays;
    }

    /**
     * @return int
     */
    public function getAddDeliveryWorkingDays()
    {
        return $this->addDeliveryWorkingDays;
    }

    /**
     * @param int $addDeliveryWorkingDays
     */
    public function setAddDeliveryWorkingDays($addDeliveryWorkingDays)
    {
        $this->addDeliveryWorkingDays = $addDeliveryWorkingDays;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return null|string
     */
    public function getAgreement(): ?string
    {
        return $this->agreement;
    }

    /**
     * @param null|string $agreement
     */
    public function setAgreement(?string $agreement): void
    {
        $this->agreement = $agreement;
    }

    /**
     * @return null|string
     */
    public function getPaidBy(): ?string
    {
        return $this->paidBy;
    }

    /**
     * @param null|string $paidBy
     */
    public function setPaidBy(?string $paidBy): void
    {
        $this->paidBy = $paidBy;
    }

    /**
     * @return int|null
     */
    public function getDaysToPay(): ?int
    {
        return $this->daysToPay;
    }

    /**
     * @param int|null $daysToPay
     */
    public function setDaysToPay(?int $daysToPay): void
    {
        $this->daysToPay = $daysToPay;
    }

    /**
     * @return null|int
     */
    public function getDiscountFromMarket(): ?int
    {
        return $this->discountFromMarket;
    }

    /**
     * @param null|int $discountFromMarket
     */
    public function setDiscountFromMarket(?int $discountFromMarket): void
    {
        $this->discountFromMarket = $discountFromMarket;
    }

    /**
     * @return null|int
     */
    public function getDiscountFromWholesale(): ?int
    {
        return $this->discountFromWholesale;
    }

    /**
     * @param null|int $discountFromWholesale
     */
    public function setDiscountFromWholesale(?int $discountFromWholesale): void
    {
        $this->discountFromWholesale = $discountFromWholesale;
    }


}
