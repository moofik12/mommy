<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SplitTestTrackingRepository")
 * @ORM\Table(name="split_tests_tracking")
 */
class SplitTestTracking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="target", type="integer", nullable=false)
     *
     * @var int
     */
    private $target;

    /**
     * @ORM\Column(name="test_params", type="string", length=1000, nullable=false)
     *
     * @var string
     */
    private $testParams = '';

    /**
     * @ORM\Column(name="host", type="string", length=127, nullable=false)
     *
     * @var string
     */
    private $host;

    /**
     * @ORM\Column(name="url", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(name="path_from", type="string", length=127, nullable=false)
     *
     * @var string
     */
    private $pathFrom;

    /**
     * @ORM\Column(name="path_to", type="string", length=127, nullable=false)
     *
     * @var string
     */
    private $pathTo;

    /**
     * @ORM\Column(name="session_id", type="string", length=64, nullable=false)
     *
     * @var string
     */
    private $sessionId;

    /**
     * @ORM\Column(name="device_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $deviceType;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param int $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getTestParams()
    {
        return $this->testParams;
    }

    /**
     * @param string $testParams
     */
    public function setTestParams($testParams)
    {
        if (is_array($testParams)) {
            $this->test_params = http_build_query($testParams);
        } elseif (is_scalar($testParams)) {
            $this->test_params = $testParams;
        }
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getPathFrom()
    {
        return $this->pathFrom;
    }

    /**
     * @param string $pathFrom
     */
    public function setPathFrom($pathFrom)
    {
        $this->pathFrom = $pathFrom;
    }

    /**
     * @return string
     */
    public function getPathTo()
    {
        return $this->pathTo;
    }

    /**
     * @param string $pathTo
     */
    public function setPathTo($pathTo)
    {
        $this->pathTo = $pathTo;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return int
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * @param int $deviceType
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }
}
