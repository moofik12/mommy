<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\PayGatewayLogRepository")
 * @ORM\Table(name="pay_gateway_log")
 */
class PayGatewayLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="provider", type="string", length=127, nullable=false)
     *
     * @var string
     */
    private $provider;

    /**
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $invoiceId;

    /**
     * @ORM\Column(name="unique_payment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $uniquePayment;

    /**
     * @ORM\Column(name="is_custom", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCustom;

    /**
     * @ORM\Column(name="amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $amount;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="message", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $message;

    /**
     * @ORM\Column(name="user_ip", type="string", length=40, nullable=false)
     *
     * @var string
     */
    private $userIp;

    /**
     * @ORM\Column(name="user_browser", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $userBrowser;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return string
     */
    public function getUniquePayment()
    {
        return $this->uniquePayment;
    }

    /**
     * @param string $uniquePayment
     */
    public function setUniquePayment($uniquePayment)
    {
        $this->uniquePayment = $uniquePayment;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param bool $isCustom
     */
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * @param string $userIp
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
    }

    /**
     * @return string
     */
    public function getUserBrowser()
    {
        return $this->userBrowser;
    }

    /**
     * @param string $userBrowser
     */
    public function setUserBrowser($userBrowser)
    {
        $this->userBrowser = $userBrowser;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
