<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="is_drop_shipping", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDropShipping;

    /**
     * @ORM\Column(name="drop_shipping_commission", type="float", nullable=false)
     *
     * @var float
     */
    private $dropShippingCommission;

    /**
     * @ORM\Column(name="paid_commission_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $paidCommissionAt;

    /**
     * @ORM\Column(name="is_bonuses_debited", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isBonusesDebited;

    /**
     * @ORM\Column(name="ordered_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderedAt;

    /**
     * @ORM\Column(name="confirmed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $confirmedAt;

    /**
     * @ORM\Column(name="delivery_start_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryStartAt;

    /**
     * @ORM\Column(name="shipped_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $shippedAt;

    /**
     * @ORM\Column(name="mail_after", type="integer", nullable=false)
     *
     * @var int
     */
    private $mailAfter;

    /**
     * @ORM\Column(name="admin_update_last_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $adminUpdateLastAt;

    /**
     * @ORM\Column(name="processing_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $processingStatus;

    /**
     * @ORM\Column(name="processing_status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $processingStatusPrev;

    /**
     * @ORM\Column(name="processing_status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $processingStatusHistoryJson;

    /**
     * @ORM\Column(name="payment_token", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $paymentToken;

    /**
     * @ORM\Column(name="payment_token_expires_at", type="integer", nullable=true)
     *
     * @var int
     */
    private $paymentTokenExpiresAt;

    /**
     * @ORM\Column(name="called_count", type="integer", nullable=false)
     *
     * @var int
     */
    private $calledCount;

    /**
     * @ORM\Column(name="next_called_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $nextCalledAt;

    /**
     * @ORM\Column(name="delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryType;

    /**
     * @ORM\Column(name="client_name", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $clientName;

    /**
     * @ORM\Column(name="client_surname", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $clientSurname;

    /**
     * @ORM\Column(name="price_total", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceTotal;

    /**
     * @ORM\Column(name="to_pay", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $toPay;

    /**
     * @ORM\Column(name="uuid", type="string", length=40, nullable=false)
     *
     * @var string
     */
    private $uuid;

    /**
     * @ORM\Column(name="card_payed", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $cardPayed;

    /**
     * @ORM\Column(name="bonuses", type="integer", nullable=false)
     *
     * @var int
     */
    private $bonuses;

    /**
     * @ORM\Column(name="bonuses_part_used_present", type="integer", nullable=false)
     *
     * @var int
     */
    private $bonusesPartUsedPresent;

    /**
     * @ORM\Column(name="discount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $discount;

    /**
     * @ORM\Column(name="user_discount_benefit", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $userDiscountBenefit;

    /**
     * @ORM\Column(name="user_discount_benefit_paid", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $userDiscountBenefitPaid;

    /**
     * @ORM\Column(name="weight_custom", type="integer", nullable=false)
     *
     * @var int
     */
    private $weightCustom;

    /**
     * @ORM\Column(name="package_count", type="integer", nullable=false)
     *
     * @var int
     */
    private $packageCount;

    /**
     * @ORM\Column(name="address", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(name="zipcode", type="integer", nullable=false)
     *
     * @var int
     */
    private $zipcode;

    /**
     * @ORM\Column(name="telephone", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $telephone;

    /**
     * @ORM\Column(name="novaposta_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $novapostaId;

    /**
     * @ORM\Column(name="trackcode", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $trackcode;

    /**
     * @ORM\Column(name="track_id", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $trackId;

    /**
     * @ORM\Column(name="is_inform_payment", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isInformPayment;

    /**
     * @ORM\Column(name="offer_provider", type="integer", nullable=false)
     *
     * @var int
     */
    private $offerProvider;

    /**
     * @ORM\Column(name="offer_id", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $offerId;

    /**
     * @ORM\Column(name="is_offer_success", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isOfferSuccess;

    /**
     * @ORM\Column(name="order_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $orderComment;

    /**
     * @ORM\Column(name="callcenter_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $callcenterComment;

    /**
     * @ORM\Column(name="storekeeper_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $storekeeperComment;

    /**
     * @ORM\Column(name="supplier_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $supplierComment;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="url_referer", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $urlReferer;

    /**
     * @ORM\Column(name="utm_params", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $utmParams;

    /**
     * @ORM\Column(name="ip_long", type="integer", nullable=false)
     *
     * @var int
     */
    private $ipLong;

    /**
     * @ORM\Column(name="delivery_attributes_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $deliveryAttributesJson;

    /**
     * @ORM\Column(name="affiliate_promocodes_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $affiliatePromocodesJson;

    /**
     * @ORM\Column(name="delivery_price", type="float", precision=8, scale=2, nullable=true)
     *
     * @var float
     */
    private $deliveryPrice;

    /**
     * @ORM\Column(name="promocode", type="string", length=21, nullable=false)
     *
     * @var string
     */
    private $promocode;

    /**
     * @ORM\Column(name="country_code", type="string", length=4, nullable=false)
     *
     * @var string
     */
    private $countryCode;

    /**
     * @ORM\Column(name="line_account", type="string", length=30, nullable=true)
     *
     * @var string|null
     */
    private $lineAccount;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @ORM\Column(name="discount_campaign_id", type="integer", nullable=false)
     *
     * @var int OrderDiscountCampaign
     */
    private $discountCampaignId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDropShipping()
    {
        return $this->isDropShipping;
    }

    /**
     * @param bool $isDropShipping
     */
    public function setIsDropShipping($isDropShipping)
    {
        $this->isDropShipping = $isDropShipping;
    }

    /**
     * @return float
     */
    public function getDropShippingCommission()
    {
        return $this->dropShippingCommission;
    }

    /**
     * @param float $dropShippingCommission
     */
    public function setDropShippingCommission($dropShippingCommission)
    {
        $this->dropShippingCommission = $dropShippingCommission;
    }

    /**
     * @return int
     */
    public function getPaidCommissionAt()
    {
        return $this->paidCommissionAt;
    }

    /**
     * @param int $paidCommissionAt
     */
    public function setPaidCommissionAt($paidCommissionAt)
    {
        $this->paidCommissionAt = $paidCommissionAt;
    }

    /**
     * @return bool
     */
    public function isBonusesDebited()
    {
        return $this->isBonusesDebited;
    }

    /**
     * @param bool $isBonusesDebited
     */
    public function setIsBonusesDebited($isBonusesDebited)
    {
        $this->isBonusesDebited = $isBonusesDebited;
    }

    /**
     * @return int
     */
    public function getOrderedAt()
    {
        return $this->orderedAt;
    }

    /**
     * @param int $orderedAt
     */
    public function setOrderedAt($orderedAt)
    {
        $this->orderedAt = $orderedAt;
    }

    /**
     * @return int
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param int $confirmedAt
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
    }

    /**
     * @return int
     */
    public function getDeliveryStartAt()
    {
        return $this->deliveryStartAt;
    }

    /**
     * @param int $deliveryStartAt
     */
    public function setDeliveryStartAt($deliveryStartAt)
    {
        $this->deliveryStartAt = $deliveryStartAt;
    }

    /**
     * @return int
     */
    public function getShippedAt()
    {
        return $this->shippedAt;
    }

    /**
     * @param int $shippedAt
     */
    public function setShippedAt($shippedAt)
    {
        $this->shippedAt = $shippedAt;
    }

    /**
     * @return int
     */
    public function getMailAfter()
    {
        return $this->mailAfter;
    }

    /**
     * @param int $mailAfter
     */
    public function setMailAfter($mailAfter)
    {
        $this->mailAfter = $mailAfter;
    }

    /**
     * @return int
     */
    public function getAdminUpdateLastAt()
    {
        return $this->adminUpdateLastAt;
    }

    /**
     * @param int $adminUpdateLastAt
     */
    public function setAdminUpdateLastAt($adminUpdateLastAt)
    {
        $this->adminUpdateLastAt = $adminUpdateLastAt;
    }

    /**
     * @return int
     */
    public function getProcessingStatus()
    {
        return $this->processingStatus;
    }

    /**
     * @param int $processingStatus
     */
    public function setProcessingStatus($processingStatus)
    {
        $this->processingStatus = $processingStatus;
    }

    /**
     * @return int
     */
    public function getProcessingStatusPrev()
    {
        return $this->processingStatusPrev;
    }

    /**
     * @param int $processingStatusPrev
     */
    public function setProcessingStatusPrev($processingStatusPrev)
    {
        $this->processingStatusPrev = $processingStatusPrev;
    }

    /**
     * @return string
     */
    public function getProcessingStatusHistoryJson()
    {
        return $this->processingStatusHistoryJson;
    }

    /**
     * @param string $processingStatusHistoryJson
     */
    public function setProcessingStatusHistoryJson($processingStatusHistoryJson)
    {
        $this->processingStatusHistoryJson = $processingStatusHistoryJson;
    }

    /**
     * @return int
     */
    public function getCalledCount()
    {
        return $this->calledCount;
    }

    /**
     * @param int $calledCount
     */
    public function setCalledCount($calledCount)
    {
        $this->calledCount = $calledCount;
    }

    /**
     * @return int
     */
    public function getNextCalledAt()
    {
        return $this->nextCalledAt;
    }

    /**
     * @param int $nextCalledAt
     */
    public function setNextCalledAt($nextCalledAt)
    {
        $this->nextCalledAt = $nextCalledAt;
    }

    /**
     * @return int
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @param int $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
    }

    /**
     * @return string
     */
    public function getClientSurname()
    {
        return $this->clientSurname;
    }

    /**
     * @param string $clientSurname
     */
    public function setClientSurname($clientSurname)
    {
        $this->clientSurname = $clientSurname;
    }

    /**
     * @return float
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @param float $priceTotal
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;
    }

    /**
     * @return float
     */
    public function getToPay()
    {
        return $this->toPay;
    }

    /**
     * @param float $toPay
     */
    public function setToPay($toPay)
    {
        $this->toPay = $toPay;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return float
     */
    public function getCardPayed()
    {
        return $this->cardPayed;
    }

    /**
     * @param float $cardPayed
     */
    public function setCardPayed($cardPayed)
    {
        $this->cardPayed = $cardPayed;
    }

    /**
     * @return int
     */
    public function getBonuses()
    {
        return $this->bonuses;
    }

    /**
     * @param int $bonuses
     */
    public function setBonuses($bonuses)
    {
        $this->bonuses = $bonuses;
    }

    /**
     * @return int
     */
    public function getBonusesPartUsedPresent()
    {
        return $this->bonusesPartUsedPresent;
    }

    /**
     * @param int $bonusesPartUsedPresent
     */
    public function setBonusesPartUsedPresent($bonusesPartUsedPresent)
    {
        $this->bonusesPartUsedPresent = $bonusesPartUsedPresent;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getUserDiscountBenefit()
    {
        return $this->userDiscountBenefit;
    }

    /**
     * @param float $userDiscountBenefit
     */
    public function setUserDiscountBenefit($userDiscountBenefit)
    {
        $this->userDiscountBenefit = $userDiscountBenefit;
    }

    /**
     * @return float
     */
    public function getUserDiscountBenefitPaid()
    {
        return $this->userDiscountBenefitPaid;
    }

    /**
     * @param float $userDiscountBenefitPaid
     */
    public function setUserDiscountBenefitPaid($userDiscountBenefitPaid)
    {
        $this->userDiscountBenefitPaid = $userDiscountBenefitPaid;
    }

    /**
     * @return int
     */
    public function getWeightCustom()
    {
        return $this->weightCustom;
    }

    /**
     * @param int $weightCustom
     */
    public function setWeightCustom($weightCustom)
    {
        $this->weightCustom = $weightCustom;
    }

    /**
     * @return int
     */
    public function getPackageCount()
    {
        return $this->packageCount;
    }

    /**
     * @param int $packageCount
     */
    public function setPackageCount($packageCount)
    {
        $this->packageCount = $packageCount;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param int $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return int
     */
    public function getNovapostaId()
    {
        return $this->novapostaId;
    }

    /**
     * @param int $novapostaId
     */
    public function setNovapostaId($novapostaId)
    {
        $this->novapostaId = $novapostaId;
    }

    /**
     * @return string
     */
    public function getTrackcode()
    {
        return $this->trackcode;
    }

    /**
     * @param string $trackcode
     */
    public function setTrackcode($trackcode)
    {
        $this->trackcode = $trackcode;
    }

    /**
     * @return string
     */
    public function getTrackId()
    {
        return $this->trackId;
    }

    /**
     * @param string $trackId
     */
    public function setTrackId($trackId)
    {
        $this->trackId = $trackId;
    }

    /**
     * @return bool
     */
    public function isInformPayment()
    {
        return $this->isInformPayment;
    }

    /**
     * @param bool $isInformPayment
     */
    public function setIsInformPayment($isInformPayment)
    {
        $this->isInformPayment = $isInformPayment;
    }

    /**
     * @return int
     */
    public function getOfferProvider()
    {
        return $this->offerProvider;
    }

    /**
     * @param int $offerProvider
     */
    public function setOfferProvider($offerProvider)
    {
        $this->offerProvider = $offerProvider;
    }

    /**
     * @return string
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @param string $offerId
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;
    }

    /**
     * @return bool
     */
    public function isOfferSuccess()
    {
        return $this->isOfferSuccess;
    }

    /**
     * @param bool $isOfferSuccess
     */
    public function setIsOfferSuccess($isOfferSuccess)
    {
        $this->isOfferSuccess = $isOfferSuccess;
    }

    /**
     * @return string
     */
    public function getOrderComment()
    {
        return $this->orderComment;
    }

    /**
     * @param string $orderComment
     */
    public function setOrderComment($orderComment)
    {
        $this->orderComment = $orderComment;
    }

    /**
     * @return string
     */
    public function getCallcenterComment()
    {
        return $this->callcenterComment;
    }

    /**
     * @param string $callcenterComment
     */
    public function setCallcenterComment($callcenterComment)
    {
        $this->callcenterComment = $callcenterComment;
    }

    /**
     * @return string
     */
    public function getStorekeeperComment()
    {
        return $this->storekeeperComment;
    }

    /**
     * @param string $storekeeperComment
     */
    public function setStorekeeperComment($storekeeperComment)
    {
        $this->storekeeperComment = $storekeeperComment;
    }

    /**
     * @return string
     */
    public function getSupplierComment()
    {
        return $this->supplierComment;
    }

    /**
     * @param string $supplierComment
     */
    public function setSupplierComment($supplierComment)
    {
        $this->supplierComment = $supplierComment;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getUrlReferer()
    {
        return $this->urlReferer;
    }

    /**
     * @param string $urlReferer
     */
    public function setUrlReferer($urlReferer)
    {
        $this->urlReferer = $urlReferer;
    }

    /**
     * @return string
     */
    public function getUtmParams()
    {
        return $this->utmParams;
    }

    /**
     * @param string $utmParams
     */
    public function setUtmParams($utmParams)
    {
        $this->utmParams = $utmParams;
    }

    /**
     * @return int
     */
    public function getIpLong()
    {
        return $this->ipLong;
    }

    /**
     * @param int $ipLong
     */
    public function setIpLong($ipLong)
    {
        $this->ipLong = $ipLong;
    }

    /**
     * @return string
     */
    public function getDeliveryAttributesJson()
    {
        return $this->deliveryAttributesJson;
    }

    /**
     * @param string $deliveryAttributesJson
     */
    public function setDeliveryAttributesJson($deliveryAttributesJson)
    {
        $this->deliveryAttributesJson = $deliveryAttributesJson;
    }

    /**
     * @return string
     */
    public function getAffiliatePromocodesJson()
    {
        return $this->affiliatePromocodesJson;
    }

    /**
     * @param string $affiliatePromocodesJson
     */
    public function setAffiliatePromocodesJson($affiliatePromocodesJson)
    {
        $this->affiliatePromocodesJson = $affiliatePromocodesJson;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice()
    {
        return $this->deliveryPrice;
    }

    /**
     * @param float $deliveryPrice
     */
    public function setDeliveryPrice($deliveryPrice)
    {
        $this->deliveryPrice = $deliveryPrice;
    }

    /**
     * @return string
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * @param string $promocode
     */
    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return null|string
     */
    public function getLineAccount()
    {
        return $this->lineAccount;
    }

    /**
     * @param null|string $lineAccount
     */
    public function setLineAccount($lineAccount)
    {
        $this->lineAccount = $lineAccount;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return string
     */
    public function getPaymentToken()
    {
        return $this->paymentToken;
    }

    /**
     * @param string $token
     */
    public function setPaymentToken($token)
    {
        $this->paymentToken = $token;
    }

    /**
     * @return int
     */
    public function getPaymentTokenExpiresAt()
    {
        return $this->paymentTokenExpiresAt;
    }

    /**
     * @param int $time
     */
    public function setPaymentTokenExpiresAt($time)
    {
        $this->paymentTokenExpiresAt = $time;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }

    /**
     * @return int OrderDiscountCampaign
     */
    public function getDiscountCampaignId()
    {
        return $this->discountCampaignId;
    }

    /**
     * @param int $discountCampaign OrderDiscountCampaign
     */
    public function setDiscountCampaignId($discountCampaign)
    {
        $this->discountCampaignId = $discountCampaign;
    }

    /**
     * @return string
     */
    public static function generatePaymentToken()
    {
        return sha1(random_bytes(64));
    }

    /**
     * @return false|int
     */
    public static function generatePaymentTokenExpiresAt()
    {
        return strtotime("+1 day");
    }
}
