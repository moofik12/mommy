<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\CallcenterTaskRepository")
 * @ORM\Table(name="callcenter_tasks")
 */
class CallcenterTask
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="comment_task", type="string", length=1024, nullable=true)
     *
     * @var string
     */
    private $commentTask;

    /**
     * @ORM\Column(name="comment_callcenter", type="string", length=1024, nullable=true)
     *
     * @var string
     */
    private $commentCallcenter;

    /**
     * @ORM\Column(name="processing_update_last_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $processingUpdateLastAt;

    /**
     * @ORM\Column(name="next_task_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $nextTaskAt;

    /**
     * @ORM\Column(name="is_system", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSystem;

    /**
     * @ORM\Column(name="is_callcenter_answer", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCallcenterAnswer;

    /**
     * @ORM\Column(name="is_callcenter_answer_read", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCallcenterAnswerRead;

    /**
     * @ORM\Column(name="performed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $performedAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="operation_id", type="integer", nullable=false)
     *
     * @var int CallcenterTaskOperation
     */
    private $operationId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @ORM\Column(name="performed_admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $performedAdminId;

    /**
     * @ORM\Column(name="processing_admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $processingAdminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCommentTask()
    {
        return $this->commentTask;
    }

    /**
     * @param string $commentTask
     */
    public function setCommentTask($commentTask)
    {
        $this->commentTask = $commentTask;
    }

    /**
     * @return string
     */
    public function getCommentCallcenter()
    {
        return $this->commentCallcenter;
    }

    /**
     * @param string $commentCallcenter
     */
    public function setCommentCallcenter($commentCallcenter)
    {
        $this->commentCallcenter = $commentCallcenter;
    }

    /**
     * @return int
     */
    public function getProcessingUpdateLastAt()
    {
        return $this->processingUpdateLastAt;
    }

    /**
     * @param int $processingUpdateLastAt
     */
    public function setProcessingUpdateLastAt($processingUpdateLastAt)
    {
        $this->processingUpdateLastAt = $processingUpdateLastAt;
    }

    /**
     * @return int
     */
    public function getNextTaskAt()
    {
        return $this->nextTaskAt;
    }

    /**
     * @param int $nextTaskAt
     */
    public function setNextTaskAt($nextTaskAt)
    {
        $this->nextTaskAt = $nextTaskAt;
    }

    /**
     * @return bool
     */
    public function isSystem()
    {
        return $this->isSystem;
    }

    /**
     * @param bool $isSystem
     */
    public function setIsSystem($isSystem)
    {
        $this->isSystem = $isSystem;
    }

    /**
     * @return bool
     */
    public function isCallcenterAnswer()
    {
        return $this->isCallcenterAnswer;
    }

    /**
     * @param bool $isCallcenterAnswer
     */
    public function setIsCallcenterAnswer($isCallcenterAnswer)
    {
        $this->isCallcenterAnswer = $isCallcenterAnswer;
    }

    /**
     * @return bool
     */
    public function isCallcenterAnswerRead()
    {
        return $this->isCallcenterAnswerRead;
    }

    /**
     * @param bool $isCallcenterAnswerRead
     */
    public function setIsCallcenterAnswerRead($isCallcenterAnswerRead)
    {
        $this->isCallcenterAnswerRead = $isCallcenterAnswerRead;
    }

    /**
     * @return int
     */
    public function getPerformedAt()
    {
        return $this->performedAt;
    }

    /**
     * @param int $performedAt
     */
    public function setPerformedAt($performedAt)
    {
        $this->performedAt = $performedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int CallcenterTaskOperation
     */
    public function getOperationId()
    {
        return $this->operationId;
    }

    /**
     * @param int $operation CallcenterTaskOperation
     */
    public function setOperationId($operation)
    {
        $this->operationId = $operation;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }

    /**
     * @return int AdminUser
     */
    public function getPerformedAdminId()
    {
        return $this->performedAdminId;
    }

    /**
     * @param int $performedAdmin AdminUser
     */
    public function setPerformedAdminId($performedAdmin)
    {
        $this->performedAdminId = $performedAdmin;
    }

    /**
     * @return int AdminUser
     */
    public function getProcessingAdminId()
    {
        return $this->processingAdminId;
    }

    /**
     * @param int $processingAdmin AdminUser
     */
    public function setProcessingAdminId($processingAdmin)
    {
        $this->processingAdminId = $processingAdmin;
    }
}
