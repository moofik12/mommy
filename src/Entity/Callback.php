<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\CallbackRepository")
 * @ORM\Table(name="callbacks")
 */
class Callback
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="telephone", type="string", length=20, nullable=false)
     *
     * @var string
     */
    private $telephone;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="comment_callcenter", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $commentCallcenter;

    /**
     * @ORM\Column(name="status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $statusHistoryJson;

    /**
     * @ORM\Column(name="processing_update_last_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $processingUpdateLastAt;

    /**
     * @ORM\Column(name="next_callback_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $nextCallbackAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="processing_admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $processingAdminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCommentCallcenter()
    {
        return $this->commentCallcenter;
    }

    /**
     * @param string $commentCallcenter
     */
    public function setCommentCallcenter($commentCallcenter)
    {
        $this->commentCallcenter = $commentCallcenter;
    }

    /**
     * @return string
     */
    public function getStatusHistoryJson()
    {
        return $this->statusHistoryJson;
    }

    /**
     * @param string $statusHistoryJson
     */
    public function setStatusHistoryJson($statusHistoryJson)
    {
        $this->statusHistoryJson = $statusHistoryJson;
    }

    /**
     * @return int
     */
    public function getProcessingUpdateLastAt()
    {
        return $this->processingUpdateLastAt;
    }

    /**
     * @param int $processingUpdateLastAt
     */
    public function setProcessingUpdateLastAt($processingUpdateLastAt)
    {
        $this->processingUpdateLastAt = $processingUpdateLastAt;
    }

    /**
     * @return int
     */
    public function getNextCallbackAt()
    {
        return $this->nextCallbackAt;
    }

    /**
     * @param int $nextCallbackAt
     */
    public function setNextCallbackAt($nextCallbackAt)
    {
        $this->nextCallbackAt = $nextCallbackAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int AdminUser
     */
    public function getProcessingAdminId()
    {
        return $this->processingAdminId;
    }

    /**
     * @param int $processingAdmin AdminUser
     */
    public function setProcessingAdminId($processingAdmin)
    {
        $this->processingAdminId = $processingAdmin;
    }
}
