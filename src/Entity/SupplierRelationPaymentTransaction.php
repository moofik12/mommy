<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierRelationPaymentTransactionRepository")
 * @ORM\Table(name="suppliers_relations_payments_transactions")
 */
class SupplierRelationPaymentTransaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $amount;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="bank_name_enc", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $bankNameEnc;

    /**
     * @ORM\Column(name="bank_giro_enc", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $bankGiroEnc;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getBankNameEnc()
    {
        return $this->bankNameEnc;
    }

    /**
     * @param string $bankNameEnc
     */
    public function setBankNameEnc($bankNameEnc)
    {
        $this->bankNameEnc = $bankNameEnc;
    }

    /**
     * @return string
     */
    public function getBankGiroEnc()
    {
        return $this->bankGiroEnc;
    }

    /**
     * @param string $bankGiroEnc
     */
    public function setBankGiroEnc($bankGiroEnc)
    {
        $this->bankGiroEnc = $bankGiroEnc;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }
}
