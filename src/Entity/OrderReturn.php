<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderReturnRepository")
 * @ORM\Table(name="orders_returns")
 */
class OrderReturn
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="trackcode", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $trackcode;

    /**
     * @ORM\Column(name="trackcode_return", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $trackcodeReturn;

    /**
     * @ORM\Column(name="delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryType;

    /**
     * @ORM\Column(name="refund_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $refundType;

    /**
     * @ORM\Column(name="price_delivery", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceDelivery;

    /**
     * @ORM\Column(name="price_redelivery", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceRedelivery;

    /**
     * @ORM\Column(name="price_products", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceProducts;

    /**
     * @ORM\Column(name="refund_delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $refundDeliveryType;

    /**
     * @ORM\Column(name="refund_redelivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $refundRedeliveryType;

    /**
     * @ORM\Column(name="price_refund", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceRefund;

    /**
     * @ORM\Column(name="payment_after_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $paymentAfterAt;

    /**
     * @ORM\Column(name="exps_client", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $expsClient;

    /**
     * @ORM\Column(name="exps_shop", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $expsShop;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusPrev;

    /**
     * @ORM\Column(name="status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $statusHistoryJson;

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt;

    /**
     * @ORM\Column(name="is_custom", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCustom;

    /**
     * @ORM\Column(name="client_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $clientComment;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="bank_fullname_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankFullnameEnc;

    /**
     * @ORM\Column(name="bank_vatin_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankVatinEnc;

    /**
     * @ORM\Column(name="bank_name_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankNameEnc;

    /**
     * @ORM\Column(name="bank_giro_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankGiroEnc;

    /**
     * @ORM\Column(name="bank_cardnum_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankCardnumEnc;

    /**
     * @ORM\Column(name="bank_correspondent_acc_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankCorrespondentAccEnc;

    /**
     * @ORM\Column(name="bank_idcode_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankIdcodeEnc;

    /**
     * @ORM\Column(name="bank_mfocode_enc", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $bankMfocodeEnc;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="order_user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $orderUserId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTrackcode()
    {
        return $this->trackcode;
    }

    /**
     * @param string $trackcode
     */
    public function setTrackcode($trackcode)
    {
        $this->trackcode = $trackcode;
    }

    /**
     * @return string
     */
    public function getTrackcodeReturn()
    {
        return $this->trackcodeReturn;
    }

    /**
     * @param string $trackcodeReturn
     */
    public function setTrackcodeReturn($trackcodeReturn)
    {
        $this->trackcodeReturn = $trackcodeReturn;
    }

    /**
     * @return int
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @param int $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return int
     */
    public function getRefundType()
    {
        return $this->refundType;
    }

    /**
     * @param int $refundType
     */
    public function setRefundType($refundType)
    {
        $this->refundType = $refundType;
    }

    /**
     * @return float
     */
    public function getPriceDelivery()
    {
        return $this->priceDelivery;
    }

    /**
     * @param float $priceDelivery
     */
    public function setPriceDelivery($priceDelivery)
    {
        $this->priceDelivery = $priceDelivery;
    }

    /**
     * @return float
     */
    public function getPriceRedelivery()
    {
        return $this->priceRedelivery;
    }

    /**
     * @param float $priceRedelivery
     */
    public function setPriceRedelivery($priceRedelivery)
    {
        $this->priceRedelivery = $priceRedelivery;
    }

    /**
     * @return float
     */
    public function getPriceProducts()
    {
        return $this->priceProducts;
    }

    /**
     * @param float $priceProducts
     */
    public function setPriceProducts($priceProducts)
    {
        $this->priceProducts = $priceProducts;
    }

    /**
     * @return int
     */
    public function getRefundDeliveryType()
    {
        return $this->refundDeliveryType;
    }

    /**
     * @param int $refundDeliveryType
     */
    public function setRefundDeliveryType($refundDeliveryType)
    {
        $this->refundDeliveryType = $refundDeliveryType;
    }

    /**
     * @return int
     */
    public function getRefundRedeliveryType()
    {
        return $this->refundRedeliveryType;
    }

    /**
     * @param int $refundRedeliveryType
     */
    public function setRefundRedeliveryType($refundRedeliveryType)
    {
        $this->refundRedeliveryType = $refundRedeliveryType;
    }

    /**
     * @return float
     */
    public function getPriceRefund()
    {
        return $this->priceRefund;
    }

    /**
     * @param float $priceRefund
     */
    public function setPriceRefund($priceRefund)
    {
        $this->priceRefund = $priceRefund;
    }

    /**
     * @return int
     */
    public function getPaymentAfterAt()
    {
        return $this->paymentAfterAt;
    }

    /**
     * @param int $paymentAfterAt
     */
    public function setPaymentAfterAt($paymentAfterAt)
    {
        $this->paymentAfterAt = $paymentAfterAt;
    }

    /**
     * @return float
     */
    public function getExpsClient()
    {
        return $this->expsClient;
    }

    /**
     * @param float $expsClient
     */
    public function setExpsClient($expsClient)
    {
        $this->expsClient = $expsClient;
    }

    /**
     * @return float
     */
    public function getExpsShop()
    {
        return $this->expsShop;
    }

    /**
     * @param float $expsShop
     */
    public function setExpsShop($expsShop)
    {
        $this->expsShop = $expsShop;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusPrev()
    {
        return $this->statusPrev;
    }

    /**
     * @param int $statusPrev
     */
    public function setStatusPrev($statusPrev)
    {
        $this->statusPrev = $statusPrev;
    }

    /**
     * @return string
     */
    public function getStatusHistoryJson()
    {
        return $this->statusHistoryJson;
    }

    /**
     * @param string $statusHistoryJson
     */
    public function setStatusHistoryJson($statusHistoryJson)
    {
        $this->statusHistoryJson = $statusHistoryJson;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param bool $isCustom
     */
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
    }

    /**
     * @return string
     */
    public function getClientComment()
    {
        return $this->clientComment;
    }

    /**
     * @param string $clientComment
     */
    public function setClientComment($clientComment)
    {
        $this->clientComment = $clientComment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getBankFullnameEnc()
    {
        return $this->bankFullnameEnc;
    }

    /**
     * @param string $bankFullnameEnc
     */
    public function setBankFullnameEnc($bankFullnameEnc)
    {
        $this->bankFullnameEnc = $bankFullnameEnc;
    }

    /**
     * @return string
     */
    public function getBankVatinEnc()
    {
        return $this->bankVatinEnc;
    }

    /**
     * @param string $bankVatinEnc
     */
    public function setBankVatinEnc($bankVatinEnc)
    {
        $this->bankVatinEnc = $bankVatinEnc;
    }

    /**
     * @return string
     */
    public function getBankNameEnc()
    {
        return $this->bankNameEnc;
    }

    /**
     * @param string $bankNameEnc
     */
    public function setBankNameEnc($bankNameEnc)
    {
        $this->bankNameEnc = $bankNameEnc;
    }

    /**
     * @return string
     */
    public function getBankGiroEnc()
    {
        return $this->bankGiroEnc;
    }

    /**
     * @param string $bankGiroEnc
     */
    public function setBankGiroEnc($bankGiroEnc)
    {
        $this->bankGiroEnc = $bankGiroEnc;
    }

    /**
     * @return string
     */
    public function getBankCardnumEnc()
    {
        return $this->bankCardnumEnc;
    }

    /**
     * @param string $bankCardnumEnc
     */
    public function setBankCardnumEnc($bankCardnumEnc)
    {
        $this->bankCardnumEnc = $bankCardnumEnc;
    }

    /**
     * @return string
     */
    public function getBankCorrespondentAccEnc()
    {
        return $this->bankCorrespondentAccEnc;
    }

    /**
     * @param string $bankCorrespondentAccEnc
     */
    public function setBankCorrespondentAccEnc($bankCorrespondentAccEnc)
    {
        $this->bankCorrespondentAccEnc = $bankCorrespondentAccEnc;
    }

    /**
     * @return string
     */
    public function getBankIdcodeEnc()
    {
        return $this->bankIdcodeEnc;
    }

    /**
     * @param string $bankIdcodeEnc
     */
    public function setBankIdcodeEnc($bankIdcodeEnc)
    {
        $this->bankIdcodeEnc = $bankIdcodeEnc;
    }

    /**
     * @return string
     */
    public function getBankMfocodeEnc()
    {
        return $this->bankMfocodeEnc;
    }

    /**
     * @param string $bankMfocodeEnc
     */
    public function setBankMfocodeEnc($bankMfocodeEnc)
    {
        $this->bankMfocodeEnc = $bankMfocodeEnc;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int User
     */
    public function getOrderUserId()
    {
        return $this->orderUserId;
    }

    /**
     * @param int $orderUser User
     */
    public function setOrderUserId($orderUser)
    {
        $this->orderUserId = $orderUser;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }
}
