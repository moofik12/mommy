<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserPartnerOutputRepository")
 * @ORM\Table(name="users_partner_output")
 */
class UserPartnerOutput
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="amount", type="integer", nullable=false)
     *
     * @var int
     */
    private $amount;

    /**
     * @ORM\Column(name="type_output", type="integer", nullable=false)
     *
     * @var int
     */
    private $typeOutput;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_reason", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $statusReason;

    /**
     * @ORM\Column(name="number_card", type="string", length=20, nullable=false)
     *
     * @var string
     */
    private $numberCard;

    /**
     * @ORM\Column(name="name_card", type="string", length=30, nullable=false)
     *
     * @var string
     */
    private $nameCard;

    /**
     * @ORM\Column(name="surname_card", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $surnameCard;

    /**
     * @ORM\Column(name="unique_payment", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $uniquePayment;

    /**
     * @ORM\Column(name="admin_update_last_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $adminUpdateLastAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="partner_id", type="integer", nullable=false)
     *
     * @var int UserPartner
     */
    private $partnerId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getTypeOutput()
    {
        return $this->typeOutput;
    }

    /**
     * @param int $typeOutput
     */
    public function setTypeOutput($typeOutput)
    {
        $this->typeOutput = $typeOutput;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatusReason()
    {
        return $this->statusReason;
    }

    /**
     * @param string $statusReason
     */
    public function setStatusReason($statusReason)
    {
        $this->statusReason = $statusReason;
    }

    /**
     * @return string
     */
    public function getNumberCard()
    {
        return $this->numberCard;
    }

    /**
     * @param string $numberCard
     */
    public function setNumberCard($numberCard)
    {
        $this->numberCard = $numberCard;
    }

    /**
     * @return string
     */
    public function getNameCard()
    {
        return $this->nameCard;
    }

    /**
     * @param string $nameCard
     */
    public function setNameCard($nameCard)
    {
        $this->nameCard = $nameCard;
    }

    /**
     * @return string
     */
    public function getSurnameCard()
    {
        return $this->surnameCard;
    }

    /**
     * @param string $surnameCard
     */
    public function setSurnameCard($surnameCard)
    {
        $this->surnameCard = $surnameCard;
    }

    /**
     * @return string
     */
    public function getUniquePayment()
    {
        return $this->uniquePayment;
    }

    /**
     * @param string $uniquePayment
     */
    public function setUniquePayment($uniquePayment)
    {
        $this->uniquePayment = $uniquePayment;
    }

    /**
     * @return int
     */
    public function getAdminUpdateLastAt()
    {
        return $this->adminUpdateLastAt;
    }

    /**
     * @param int $adminUpdateLastAt
     */
    public function setAdminUpdateLastAt($adminUpdateLastAt)
    {
        $this->adminUpdateLastAt = $adminUpdateLastAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int UserPartner
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partner UserPartner
     */
    public function setPartnerId($partner)
    {
        $this->partnerId = $partner;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }
}
