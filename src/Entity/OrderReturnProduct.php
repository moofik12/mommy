<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderReturnProductRepository")
 * @ORM\Table(name="orders_returns_positions")
 */
class OrderReturnProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(name="item_condition", type="integer", nullable=false)
     *
     * @var int
     */
    private $itemCondition;

    /**
     * @ORM\Column(name="is_pay", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isPay;

    /**
     * @ORM\Column(name="client_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $clientComment;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="return_id", type="integer", nullable=false)
     *
     * @var int OrderReturn
     */
    private $returnId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="order_product_id", type="integer", nullable=false)
     *
     * @var int OrderProduct
     */
    private $orderProductId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="event_product_id", type="integer", nullable=false)
     *
     * @var int EventProduct
     */
    private $eventProductId;

    /**
     * @ORM\Column(name="warehouse_id", type="integer", nullable=false)
     *
     * @var int WarehouseProduct
     */
    private $warehouseId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getItemCondition()
    {
        return $this->itemCondition;
    }

    /**
     * @param int $itemCondition
     */
    public function setItemCondition($itemCondition)
    {
        $this->itemCondition = $itemCondition;
    }

    /**
     * @return bool
     */
    public function isPay()
    {
        return $this->isPay;
    }

    /**
     * @param bool $isPay
     */
    public function setIsPay($isPay)
    {
        $this->isPay = $isPay;
    }

    /**
     * @return string
     */
    public function getClientComment()
    {
        return $this->clientComment;
    }

    /**
     * @param string $clientComment
     */
    public function setClientComment($clientComment)
    {
        $this->clientComment = $clientComment;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int OrderReturn
     */
    public function getReturnId()
    {
        return $this->returnId;
    }

    /**
     * @param int $return OrderReturn
     */
    public function setReturnId($return)
    {
        $this->returnId = $return;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int OrderProduct
     */
    public function getOrderProductId()
    {
        return $this->orderProductId;
    }

    /**
     * @param int $orderProduct OrderProduct
     */
    public function setOrderProductId($orderProduct)
    {
        $this->orderProductId = $orderProduct;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int EventProduct
     */
    public function getEventProductId()
    {
        return $this->eventProductId;
    }

    /**
     * @param int $eventProduct EventProduct
     */
    public function setEventProductId($eventProduct)
    {
        $this->eventProductId = $eventProduct;
    }

    /**
     * @return int WarehouseProduct
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * @param int $warehouse WarehouseProduct
     */
    public function setWarehouseId($warehouse)
    {
        $this->warehouseId = $warehouse;
    }
}
