<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\TabletUserRepository")
 * @ORM\Table(name="tablet_users")
 */
class TabletUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="last_activity_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $lastActivityAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId;

    /**
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $createdBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLastActivityAt()
    {
        return $this->lastActivityAt;
    }

    /**
     * @param int $lastActivityAt
     */
    public function setLastActivityAt($lastActivityAt)
    {
        $this->lastActivityAt = $lastActivityAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int AdminUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy AdminUser
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }
}
