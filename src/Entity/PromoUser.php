<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\PromoUserRepository")
 * @ORM\Table(name="promo")
 */
class PromoUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="name", type="string", length=24, nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="adate", type="integer", nullable=false)
     *
     * @var int
     */
    private $adate;

    /**
     * @ORM\Column(name="`from`", type="integer", nullable=false)
     *
     * @var int
     */
    private $from;

    /**
     * @ORM\Column(name="hash", type="string", length=32, nullable=false)
     *
     * @var string
     */
    private $hash;

    /**
     * @ORM\Column(name="actionpay", type="string", length=64, nullable=false)
     *
     * @var string
     */
    private $actionpay;

    /**
     * @ORM\Column(name="download", type="integer", nullable=false)
     *
     * @var int
     */
    private $download;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getAdate()
    {
        return $this->adate;
    }

    /**
     * @param int $adate
     */
    public function setAdate($adate)
    {
        $this->adate = $adate;
    }

    /**
     * @return int
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param int $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getActionpay()
    {
        return $this->actionpay;
    }

    /**
     * @param string $actionpay
     */
    public function setActionpay($actionpay)
    {
        $this->actionpay = $actionpay;
    }

    /**
     * @return int
     */
    public function getDownload()
    {
        return $this->download;
    }

    /**
     * @param int $download
     */
    public function setDownload($download)
    {
        $this->download = $download;
    }
}
