<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\WarehouseProductLostStatusHistoryRepository")
 * @ORM\Table(name="warehouse_products_lost_statushistory")
 */
class WarehouseProductLostStatusHistory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     *
     * @var int EventProduct
     */
    private $productId;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusPrev;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId;

    /**
     * @ORM\Column(name="client_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $clientId;

    /**
     * @ORM\Column(name="time", type="integer", nullable=false)
     *
     * @var int
     */
    private $time;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusPrev()
    {
        return $this->statusPrev;
    }

    /**
     * @param int $statusPrev
     */
    public function setStatusPrev($statusPrev)
    {
        $this->statusPrev = $statusPrev;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return int EventProduct
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $product EventProduct
     */
    public function setProductId($product)
    {
        $this->productId = $product;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int User
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param int $client User
     */
    public function setClientId($client)
    {
        $this->clientId = $client;
    }
}