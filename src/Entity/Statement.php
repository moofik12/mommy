<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\StatementRepository")
 * @ORM\Table(name="statements")
 */
class Statement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryType;

    /**
     * @ORM\Column(name="service_statement_id", type="string", length=24, nullable=false)
     *
     * @var string
     */
    private $serviceStatementId;

    /**
     * @ORM\Column(name="service_statement_ref", type="string", length=36, nullable=false)
     *
     * @var string
     */
    private $serviceStatementRef;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="synchronization_error_message", type="string", length=1024, nullable=true)
     *
     * @var string
     */
    private $synchronizationErrorMessage;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @param int $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return string
     */
    public function getServiceStatementId()
    {
        return $this->serviceStatementId;
    }

    /**
     * @param string $serviceStatementId
     */
    public function setServiceStatementId($serviceStatementId)
    {
        $this->serviceStatementId = $serviceStatementId;
    }

    /**
     * @return string
     */
    public function getServiceStatementRef()
    {
        return $this->serviceStatementRef;
    }

    /**
     * @param string $serviceStatementRef
     */
    public function setServiceStatementRef($serviceStatementRef)
    {
        $this->serviceStatementRef = $serviceStatementRef;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSynchronizationErrorMessage()
    {
        return $this->synchronizationErrorMessage;
    }

    /**
     * @param string $synchronizationErrorMessage
     */
    public function setSynchronizationErrorMessage($synchronizationErrorMessage)
    {
        $this->synchronizationErrorMessage = $synchronizationErrorMessage;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }
}
