<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierPaymentRepository")
 * @ORM\Table(name="suppliers_payments")
 */
class SupplierPayment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="event_end_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $eventEndAt;

    /**
     * @ORM\Column(name="delivery_start_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryStartAt;

    /**
     * @ORM\Column(name="delivery_end_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryEndAt;

    /**
     * @ORM\Column(name="payment_after_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $paymentAfterAt;

    /**
     * @ORM\Column(name="payment_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $paymentAt;

    /**
     * @ORM\Column(name="request_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $requestAmount;

    /**
     * @ORM\Column(name="delivered_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $deliveredAmount;

    /**
     * @ORM\Column(name="count_products_problem_delivery", type="integer", nullable=false)
     *
     * @var int
     */
    private $countProductsProblemDelivery;

    /**
     * @ORM\Column(name="delivered_amount_wrong_time", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $deliveredAmountWrongTime;

    /**
     * @ORM\Column(name="returned_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $returnedAmount;

    /**
     * @ORM\Column(name="penalty_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $penaltyAmount;

    /**
     * @ORM\Column(name="unpaid_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $unpaidAmount;

    /**
     * @ORM\Column(name="unpaid_fixed_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $unpaidFixedAmount;

    /**
     * @ORM\Column(name="paid_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $paidAmount;

    /**
     * @ORM\Column(name="custom_penalty_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $customPenaltyAmount;

    /**
     * @ORM\Column(name="bank_name_enc", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $bankNameEnc;

    /**
     * @ORM\Column(name="bank_giro_enc", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $bankGiroEnc;

    /**
     * @ORM\Column(name="comment", type="string", length=200, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="is_order_positions_worked", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isOrderPositionsWorked;

    /**
     * @ORM\Column(name="order_table_loaded_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderTableLoadedAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="act_id", type="integer", nullable=false)
     *
     * @var int SupplierPaymentAct
     */
    private $actId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="contract_id", type="integer", nullable=false)
     *
     * @var int SupplierContract
     */
    private $contractId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="unpaid_in_payment_id", type="integer", nullable=false)
     *
     * @var int self
     */
    private $unpaidInPaymentId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getEventEndAt()
    {
        return $this->eventEndAt;
    }

    /**
     * @param int $eventEndAt
     */
    public function setEventEndAt($eventEndAt)
    {
        $this->eventEndAt = $eventEndAt;
    }

    /**
     * @return int
     */
    public function getDeliveryStartAt()
    {
        return $this->deliveryStartAt;
    }

    /**
     * @param int $deliveryStartAt
     */
    public function setDeliveryStartAt($deliveryStartAt)
    {
        $this->deliveryStartAt = $deliveryStartAt;
    }

    /**
     * @return int
     */
    public function getDeliveryEndAt()
    {
        return $this->deliveryEndAt;
    }

    /**
     * @param int $deliveryEndAt
     */
    public function setDeliveryEndAt($deliveryEndAt)
    {
        $this->deliveryEndAt = $deliveryEndAt;
    }

    /**
     * @return int
     */
    public function getPaymentAfterAt()
    {
        return $this->paymentAfterAt;
    }

    /**
     * @param int $paymentAfterAt
     */
    public function setPaymentAfterAt($paymentAfterAt)
    {
        $this->paymentAfterAt = $paymentAfterAt;
    }

    /**
     * @return int
     */
    public function getPaymentAt()
    {
        return $this->paymentAt;
    }

    /**
     * @param int $paymentAt
     */
    public function setPaymentAt($paymentAt)
    {
        $this->paymentAt = $paymentAt;
    }

    /**
     * @return float
     */
    public function getRequestAmount()
    {
        return $this->requestAmount;
    }

    /**
     * @param float $requestAmount
     */
    public function setRequestAmount($requestAmount)
    {
        $this->requestAmount = $requestAmount;
    }

    /**
     * @return float
     */
    public function getDeliveredAmount()
    {
        return $this->deliveredAmount;
    }

    /**
     * @param float $deliveredAmount
     */
    public function setDeliveredAmount($deliveredAmount)
    {
        $this->deliveredAmount = $deliveredAmount;
    }

    /**
     * @return int
     */
    public function getCountProductsProblemDelivery()
    {
        return $this->countProductsProblemDelivery;
    }

    /**
     * @param int $countProductsProblemDelivery
     */
    public function setCountProductsProblemDelivery($countProductsProblemDelivery)
    {
        $this->countProductsProblemDelivery = $countProductsProblemDelivery;
    }

    /**
     * @return float
     */
    public function getDeliveredAmountWrongTime()
    {
        return $this->deliveredAmountWrongTime;
    }

    /**
     * @param float $deliveredAmountWrongTime
     */
    public function setDeliveredAmountWrongTime($deliveredAmountWrongTime)
    {
        $this->deliveredAmountWrongTime = $deliveredAmountWrongTime;
    }

    /**
     * @return float
     */
    public function getReturnedAmount()
    {
        return $this->returnedAmount;
    }

    /**
     * @param float $returnedAmount
     */
    public function setReturnedAmount($returnedAmount)
    {
        $this->returnedAmount = $returnedAmount;
    }

    /**
     * @return float
     */
    public function getPenaltyAmount()
    {
        return $this->penaltyAmount;
    }

    /**
     * @param float $penaltyAmount
     */
    public function setPenaltyAmount($penaltyAmount)
    {
        $this->penaltyAmount = $penaltyAmount;
    }

    /**
     * @return float
     */
    public function getUnpaidAmount()
    {
        return $this->unpaidAmount;
    }

    /**
     * @param float $unpaidAmount
     */
    public function setUnpaidAmount($unpaidAmount)
    {
        $this->unpaidAmount = $unpaidAmount;
    }

    /**
     * @return float
     */
    public function getUnpaidFixedAmount()
    {
        return $this->unpaidFixedAmount;
    }

    /**
     * @param float $unpaidFixedAmount
     */
    public function setUnpaidFixedAmount($unpaidFixedAmount)
    {
        $this->unpaidFixedAmount = $unpaidFixedAmount;
    }

    /**
     * @return float
     */
    public function getPaidAmount()
    {
        return $this->paidAmount;
    }

    /**
     * @param float $paidAmount
     */
    public function setPaidAmount($paidAmount)
    {
        $this->paidAmount = $paidAmount;
    }

    /**
     * @return float
     */
    public function getCustomPenaltyAmount()
    {
        return $this->customPenaltyAmount;
    }

    /**
     * @param float $customPenaltyAmount
     */
    public function setCustomPenaltyAmount($customPenaltyAmount)
    {
        $this->customPenaltyAmount = $customPenaltyAmount;
    }

    /**
     * @return string
     */
    public function getBankNameEnc()
    {
        return $this->bankNameEnc;
    }

    /**
     * @param string $bankNameEnc
     */
    public function setBankNameEnc($bankNameEnc)
    {
        $this->bankNameEnc = $bankNameEnc;
    }

    /**
     * @return string
     */
    public function getBankGiroEnc()
    {
        return $this->bankGiroEnc;
    }

    /**
     * @param string $bankGiroEnc
     */
    public function setBankGiroEnc($bankGiroEnc)
    {
        $this->bankGiroEnc = $bankGiroEnc;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isOrderPositionsWorked()
    {
        return $this->isOrderPositionsWorked;
    }

    /**
     * @param bool $isOrderPositionsWorked
     */
    public function setIsOrderPositionsWorked($isOrderPositionsWorked)
    {
        $this->isOrderPositionsWorked = $isOrderPositionsWorked;
    }

    /**
     * @return int
     */
    public function getOrderTableLoadedAt()
    {
        return $this->orderTableLoadedAt;
    }

    /**
     * @param int $orderTableLoadedAt
     */
    public function setOrderTableLoadedAt($orderTableLoadedAt)
    {
        $this->orderTableLoadedAt = $orderTableLoadedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int SupplierPaymentAct
     */
    public function getActId()
    {
        return $this->actId;
    }

    /**
     * @param int $act SupplierPaymentAct
     */
    public function setActId($act)
    {
        $this->actId = $act;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int SupplierContract
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * @param int $contract SupplierContract
     */
    public function setContractId($contract)
    {
        $this->contractId = $contract;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int SupplierPayment
     */
    public function getUnpaidInPaymentId()
    {
        return $this->unpaidInPaymentId;
    }

    /**
     * @param int $unpaidInPayment SupplierPayment
     */
    public function setUnpaidInPaymentId($unpaidInPayment)
    {
        $this->unpaidInPaymentId = $unpaidInPayment;
    }
}
