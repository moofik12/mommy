<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierRelationPaymentPenaltyRepository")
 * @ORM\Table(name="suppliers_relations_payments_penalty")
 */
class SupplierRelationPaymentPenalty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="order_product_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderProductId;

    /**
     * @ORM\Column(name="amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $amount;

    /**
     * @ORM\Column(name="state", type="integer", nullable=false)
     *
     * @var int
     */
    private $state;

    /**
     * @ORM\Column(name="cause_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $causeType;

    /**
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOrderProductId()
    {
        return $this->orderProductId;
    }

    /**
     * @param int $orderProductId
     */
    public function setOrderProductId($orderProductId)
    {
        $this->orderProductId = $orderProductId;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getCauseType()
    {
        return $this->causeType;
    }

    /**
     * @param int $causeType
     */
    public function setCauseType($causeType)
    {
        $this->causeType = $causeType;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }
}
