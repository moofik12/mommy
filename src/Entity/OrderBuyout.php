<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderBuyoutRepository")
 * @ORM\Table(name="orders_buyouts")
 */
class OrderBuyout
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="is_drop_shipping", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDropShipping;

    /**
     * @ORM\Column(name="order_delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderDeliveryType;

    /**
     * @ORM\Column(name="price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(name="delivery_price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $deliveryPrice;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt;

    /**
     * @ORM\Column(name="status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $statusHistoryJson;

    /**
     * @ORM\Column(name="is_auto_synchronizable", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isAutoSynchronizable;

    /**
     * @ORM\Column(name="synchronized_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $synchronizedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDropShipping()
    {
        return $this->isDropShipping;
    }

    /**
     * @param bool $isDropShipping
     */
    public function setIsDropShipping($isDropShipping)
    {
        $this->isDropShipping = $isDropShipping;
    }

    /**
     * @return int
     */
    public function getOrderDeliveryType()
    {
        return $this->orderDeliveryType;
    }

    /**
     * @param int $orderDeliveryType
     */
    public function setOrderDeliveryType($orderDeliveryType)
    {
        $this->orderDeliveryType = $orderDeliveryType;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice()
    {
        return $this->deliveryPrice;
    }

    /**
     * @param float $deliveryPrice
     */
    public function setDeliveryPrice($deliveryPrice)
    {
        $this->deliveryPrice = $deliveryPrice;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return string
     */
    public function getStatusHistoryJson()
    {
        return $this->statusHistoryJson;
    }

    /**
     * @param string $statusHistoryJson
     */
    public function setStatusHistoryJson($statusHistoryJson)
    {
        $this->statusHistoryJson = $statusHistoryJson;
    }

    /**
     * @return bool
     */
    public function isAutoSynchronizable()
    {
        return $this->isAutoSynchronizable;
    }

    /**
     * @param bool $isAutoSynchronizable
     */
    public function setIsAutoSynchronizable($isAutoSynchronizable)
    {
        $this->isAutoSynchronizable = $isAutoSynchronizable;
    }

    /**
     * @return int
     */
    public function getSynchronizedAt()
    {
        return $this->synchronizedAt;
    }

    /**
     * @param int $synchronizedAt
     */
    public function setSynchronizedAt($synchronizedAt)
    {
        $this->synchronizedAt = $synchronizedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }
}
