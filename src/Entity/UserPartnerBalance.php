<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserPartnerBalanceRepository")
 * @ORM\Table(name="users_partner_balance")
 */
class UserPartnerBalance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @var int
     */
    private $type;

    /**
     * @ORM\Column(name="amount", type="integer", nullable=false)
     *
     * @var int
     */
    private $amount;

    /**
     * @ORM\Column(name="description", type="string", length=150, nullable=false)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="partner_id", type="integer", nullable=false)
     *
     * @var int UserPartner
     */
    private $partnerId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int UserPartner
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partner UserPartner
     */
    public function setPartnerId($partner)
    {
        $this->partnerId = $partner;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }
}
