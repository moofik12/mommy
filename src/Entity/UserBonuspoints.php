<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserBonuspointsRepository")
 * @ORM\Table(name="users_bonuspoints")
 */
class UserBonuspoints
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="points", type="integer", nullable=false)
     *
     * @var int
     */
    private $points;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @var int
     */
    private $type;

    /**
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $message;

    /**
     * @ORM\Column(name="is_custom", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCustom;

    /**
     * @ORM\Column(name="custom_message", type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $customMessage;

    /**
     * @ORM\Column(name="expire_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $expireAt;

    /**
     * @ORM\Column(name="is_expiration_processed", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isExpirationProcessed;

    /**
     * @ORM\Column(name="is_user_notified", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isUserNotified;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return null|string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param null|string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param bool $isCustom
     */
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
    }

    /**
     * @return null|string
     */
    public function getCustomMessage()
    {
        return $this->customMessage;
    }

    /**
     * @param null|string $customMessage
     */
    public function setCustomMessage($customMessage)
    {
        $this->customMessage = $customMessage;
    }

    /**
     * @return int
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * @param int $expireAt
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;
    }

    /**
     * @return bool
     */
    public function isExpirationProcessed()
    {
        return $this->isExpirationProcessed;
    }

    /**
     * @param bool $isExpirationProcessed
     */
    public function setIsExpirationProcessed($isExpirationProcessed)
    {
        $this->isExpirationProcessed = $isExpirationProcessed;
    }

    /**
     * @return bool
     */
    public function isUserNotified()
    {
        return $this->isUserNotified;
    }

    /**
     * @param bool $isUserNotified
     */
    public function setIsUserNotified($isUserNotified)
    {
        $this->isUserNotified = $isUserNotified;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }
}
