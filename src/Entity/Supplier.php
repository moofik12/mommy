<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierRepository")
 * @ORM\Table(name="suppliers")
 */
class Supplier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(name="phone", type="string", length=25, nullable=false)
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(name="email", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="full_name", type="string", length=250, nullable=true)
     *
     * @var string|null
     */
    private $fullName;

    /**
     * @ORM\Column(name="login", type="string", length=32, nullable=false)
     *
     * @var string
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="string", length=43, nullable=false)
     *
     * @var string
     */
    private $password;

    /**
     * @ORM\Column(name="password_salt", type="string", length=5, nullable=false)
     *
     * @var string
     */
    private $passwordSalt;

    /**
     * @ORM\Column(name="display_name", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $displayName;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="settings_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $settingsJson;

    /**
     * @ORM\Column(name="is_inform", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isInform;

    /**
     * @ORM\Column(name="ip", type="string", length=40, nullable=false)
     *
     * @var string
     */
    private $ip;

    /**
     * @ORM\Column(name="last_activity_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $lastActivityAt;

    /**
     * @ORM\Column(name="address", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(name="logo", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $logo;

    /**
     * @ORM\Column(name="is_approved", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isApproved;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="contract_id", type="integer", nullable=false)
     *
     * @var int SupplierContract
     */
    private $contractId;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     *
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(name="registration_date", type="integer", nullable=true)
     *
     * @var int
     */
    private $registrationDate;

    /**
     * @ORM\Column(name="chief_name", type="string",  length=255, nullable=true)
     *
     * @var int
     */
    private $chiefName;

    /**
     * @ORM\Column(name="chief_passport", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $chiefPassport;

    /**
     * @ORM\Column(name="license", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $license;

    /**
     * @ORM\Column(name="npwp", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $npwp;

    /**
     * @ORM\Column(name="tdp", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $tdp;

    /**
     * @ORM\Column(name="bank_name", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $bankName;

    /**
     * @ORM\Column(name="bank_address", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $bankAddress;

    /**
     * @ORM\Column(name="bank_account", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $bankAccount;

    /**
     * @ORM\Column(name="messenger", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $messenger;

    /**
     * @ORM\Column(name="certifiable_sections", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $certifiableSections;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param null|string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @param string $passwordSalt
     */
    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = $passwordSalt;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSettingsJson()
    {
        return $this->settingsJson;
    }

    /**
     * @param string $settingsJson
     */
    public function setSettingsJson($settingsJson)
    {
        $this->settingsJson = $settingsJson;
    }

    /**
     * @return string
     */
    public function getBankCardnumEnc()
    {
        return $this->bankCardnumEnc;
    }

    /**
     * @param string $bankCardnumEnc
     */
    public function setBankCardnumEnc($bankCardnumEnc)
    {
        $this->bankCardnumEnc = $bankCardnumEnc;
    }

    /**
     * @return bool
     */
    public function isInform()
    {
        return $this->isInform;
    }

    /**
     * @param bool $isInform
     */
    public function setIsInform($isInform)
    {
        $this->isInform = $isInform;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getLastActivityAt()
    {
        return $this->lastActivityAt;
    }

    /**
     * @param int $lastActivityAt
     */
    public function setLastActivityAt($lastActivityAt)
    {
        $this->lastActivityAt = $lastActivityAt;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        return $this->isApproved;
    }

    /**
     * @param bool $isApproved
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int SupplierContract
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * @param int $contract SupplierContract
     */
    public function setContractId($contract)
    {
        $this->contractId = $contract;
    }

    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return null|int
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @param int $registrationDate
     */
    public function setRegistrationDate(int $registrationDate)
    {
        $this->registrationDate = $registrationDate;
    }

    /**
     * @return null|int
     */
    public function getChiefName()
    {
        return $this->chiefName;
    }

    /**
     * @param int $chiefName
     */
    public function setChiefName(int $chiefName)
    {
        $this->chiefName = $chiefName;
    }

    /**
     * @return null|string
     */
    public function getChiefPassport()
    {
        return $this->chiefPassport;
    }

    /**
     * @param string $chiefPassport
     */
    public function setChiefPassport(string $chiefPassport)
    {
        $this->chiefPassport = $chiefPassport;
    }

    /**
     * @return null|string
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * @param string $license
     */
    public function setLicense(string $license)
    {
        $this->license = $license;
    }

    /**
     * @return null|string
     */
    public function getNpwp()
    {
        return $this->npwp;
    }

    /**
     * @param string $npwp
     */
    public function setNpwp(string $npwp)
    {
        $this->npwp = $npwp;
    }

    /**
     * @return null|string
     */
    public function getTdp()
    {
        return $this->tdp;
    }

    /**
     * @param string $tdp
     */
    public function setTdp(string $tdp)
    {
        $this->tdp = $tdp;
    }

    /**
     * @return null|string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName(string $bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return null|string
     */
    public function getBankAddress()
    {
        return $this->bankAddress;
    }

    /**
     * @param string $bankAddress
     */
    public function setBankAddress(string $bankAddress)
    {
        $this->bankAddress = $bankAddress;
    }

    /**
     * @return null|string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @param string $bankAccount
     */
    public function setBankAccount(string $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    /**
     * @return null|string
     */
    public function getMessenger()
    {
        return $this->messenger;
    }

    /**
     * @param string $messenger
     */
    public function setMessenger(string $messenger)
    {
        $this->messenger = $messenger;
    }

    /**
     * @return null|string
     */
    public function getCertifiableSections()
    {
        return $this->certifiableSections;
    }

    /**
     * @param string $certifiableSections
     */
    public function setCertifiableSections(string $certifiableSections)
    {
        $this->certifiableSections = $certifiableSections;
    }
}
