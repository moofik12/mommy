<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierRelationPaymentRepository")
 * @ORM\Table(name="suppliers_relations_payments")
 */
class SupplierRelationPayment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="orders", type="integer", nullable=false)
     *
     * @var int
     */
    private $orders;

    /**
     * @ORM\Column(name="orders_canceled", type="integer", nullable=false)
     *
     * @var int
     */
    private $ordersCanceled;

    /**
     * @ORM\Column(name="orders_mailed", type="integer", nullable=false)
     *
     * @var int
     */
    private $ordersMailed;

    /**
     * @ORM\Column(name="orders_hold_delivery", type="integer", nullable=false)
     *
     * @var int
     */
    private $ordersHoldDelivery;

    /**
     * @ORM\Column(name="orders_delivered", type="integer", nullable=false)
     *
     * @var int
     */
    private $ordersDelivered;

    /**
     * @ORM\Column(name="orders_not_delivered", type="integer", nullable=false)
     *
     * @var int
     */
    private $ordersNotDelivered;

    /**
     * @ORM\Column(name="orders_returned", type="integer", nullable=false)
     *
     * @var int
     */
    private $ordersReturned;

    /**
     * @ORM\Column(name="orders_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $ordersAmount;

    /**
     * @ORM\Column(name="orders_delivered_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $ordersDeliveredAmount;

    /**
     * @ORM\Column(name="orders_not_delivered_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $ordersNotDeliveredAmount;

    /**
     * @ORM\Column(name="orders_prepaid_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $ordersPrepaidAmount;

    /**
     * @ORM\Column(name="penalty_amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $penaltyAmount;

    /**
     * @ORM\Column(name="commission", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $commission;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="waiting_sending_days", type="integer", nullable=false)
     *
     * @var int
     */
    private $waitingSendingDays;

    /**
     * @ORM\Column(name="to_pay", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $toPay;

    /**
     * @ORM\Column(name="paid", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $paid;

    /**
     * @ORM\Column(name="is_force_refresh", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isForceRefresh;

    /**
     * @ORM\Column(name="synchronized_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $synchronizedAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param int $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return int
     */
    public function getOrdersCanceled()
    {
        return $this->ordersCanceled;
    }

    /**
     * @param int $ordersCanceled
     */
    public function setOrdersCanceled($ordersCanceled)
    {
        $this->ordersCanceled = $ordersCanceled;
    }

    /**
     * @return int
     */
    public function getOrdersMailed()
    {
        return $this->ordersMailed;
    }

    /**
     * @param int $ordersMailed
     */
    public function setOrdersMailed($ordersMailed)
    {
        $this->ordersMailed = $ordersMailed;
    }

    /**
     * @return int
     */
    public function getOrdersHoldDelivery()
    {
        return $this->ordersHoldDelivery;
    }

    /**
     * @param int $ordersHoldDelivery
     */
    public function setOrdersHoldDelivery($ordersHoldDelivery)
    {
        $this->ordersHoldDelivery = $ordersHoldDelivery;
    }

    /**
     * @return int
     */
    public function getOrdersDelivered()
    {
        return $this->ordersDelivered;
    }

    /**
     * @param int $ordersDelivered
     */
    public function setOrdersDelivered($ordersDelivered)
    {
        $this->ordersDelivered = $ordersDelivered;
    }

    /**
     * @return int
     */
    public function getOrdersNotDelivered()
    {
        return $this->ordersNotDelivered;
    }

    /**
     * @param int $ordersNotDelivered
     */
    public function setOrdersNotDelivered($ordersNotDelivered)
    {
        $this->ordersNotDelivered = $ordersNotDelivered;
    }

    /**
     * @return int
     */
    public function getOrdersReturned()
    {
        return $this->ordersReturned;
    }

    /**
     * @param int $ordersReturned
     */
    public function setOrdersReturned($ordersReturned)
    {
        $this->ordersReturned = $ordersReturned;
    }

    /**
     * @return float
     */
    public function getOrdersAmount()
    {
        return $this->ordersAmount;
    }

    /**
     * @param float $ordersAmount
     */
    public function setOrdersAmount($ordersAmount)
    {
        $this->ordersAmount = $ordersAmount;
    }

    /**
     * @return float
     */
    public function getOrdersDeliveredAmount()
    {
        return $this->ordersDeliveredAmount;
    }

    /**
     * @param float $ordersDeliveredAmount
     */
    public function setOrdersDeliveredAmount($ordersDeliveredAmount)
    {
        $this->ordersDeliveredAmount = $ordersDeliveredAmount;
    }

    /**
     * @return float
     */
    public function getOrdersNotDeliveredAmount()
    {
        return $this->ordersNotDeliveredAmount;
    }

    /**
     * @param float $ordersNotDeliveredAmount
     */
    public function setOrdersNotDeliveredAmount($ordersNotDeliveredAmount)
    {
        $this->ordersNotDeliveredAmount = $ordersNotDeliveredAmount;
    }

    /**
     * @return float
     */
    public function getOrdersPrepaidAmount()
    {
        return $this->ordersPrepaidAmount;
    }

    /**
     * @param float $ordersPrepaidAmount
     */
    public function setOrdersPrepaidAmount($ordersPrepaidAmount)
    {
        $this->ordersPrepaidAmount = $ordersPrepaidAmount;
    }

    /**
     * @return float
     */
    public function getPenaltyAmount()
    {
        return $this->penaltyAmount;
    }

    /**
     * @param float $penaltyAmount
     */
    public function setPenaltyAmount($penaltyAmount)
    {
        $this->penaltyAmount = $penaltyAmount;
    }

    /**
     * @return float
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getWaitingSendingDays()
    {
        return $this->waitingSendingDays;
    }

    /**
     * @param int $waitingSendingDays
     */
    public function setWaitingSendingDays($waitingSendingDays)
    {
        $this->waitingSendingDays = $waitingSendingDays;
    }

    /**
     * @return float
     */
    public function getToPay()
    {
        return $this->toPay;
    }

    /**
     * @param float $toPay
     */
    public function setToPay($toPay)
    {
        $this->toPay = $toPay;
    }

    /**
     * @return float
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @param float $paid
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
    }

    /**
     * @return bool
     */
    public function isForceRefresh()
    {
        return $this->isForceRefresh;
    }

    /**
     * @param bool $isForceRefresh
     */
    public function setIsForceRefresh($isForceRefresh)
    {
        $this->isForceRefresh = $isForceRefresh;
    }

    /**
     * @return int
     */
    public function getSynchronizedAt()
    {
        return $this->synchronizedAt;
    }

    /**
     * @param int $synchronizedAt
     */
    public function setSynchronizedAt($synchronizedAt)
    {
        $this->synchronizedAt = $synchronizedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }
}
