<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\WarehouseStatementReturnRepository")
 * @ORM\Table(name="warehouse_statements_return")
 */
class WarehouseStatementReturn
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="is_accounted_returns", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isAccountedReturns;

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt;

    /**
     * @ORM\Column(name="is_need_notify", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isNeedNotify;

    /**
     * @ORM\Column(name="is_notified", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isNotified;

    /**
     * @ORM\Column(name="notify_text", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $notifyText;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=true)
     *
     * @var int|null Supplier|null
     */
    private $supplierId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId;

    /**
     * @ORM\Column(name="supplier_payment_id", type="integer", nullable=false)
     *
     * @var int SupplierPayment
     */
    private $supplierPaymentId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isAccountedReturns()
    {
        return $this->isAccountedReturns;
    }

    /**
     * @param bool $isAccountedReturns
     */
    public function setIsAccountedReturns($isAccountedReturns)
    {
        $this->isAccountedReturns = $isAccountedReturns;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return bool
     */
    public function isNeedNotify()
    {
        return $this->isNeedNotify;
    }

    /**
     * @param bool $isNeedNotify
     */
    public function setIsNeedNotify($isNeedNotify)
    {
        $this->isNeedNotify = $isNeedNotify;
    }

    /**
     * @return bool
     */
    public function isNotified()
    {
        return $this->isNotified;
    }

    /**
     * @param bool $isNotified
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;
    }

    /**
     * @return string
     */
    public function getNotifyText()
    {
        return $this->notifyText;
    }

    /**
     * @param string $notifyText
     */
    public function setNotifyText($notifyText)
    {
        $this->notifyText = $notifyText;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int|null Supplier|null
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int|null $supplier Supplier|null
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int SupplierPayment
     */
    public function getSupplierPaymentId()
    {
        return $this->supplierPaymentId;
    }

    /**
     * @param int $supplierPayment SupplierPayment
     */
    public function setSupplierPaymentId($supplierPayment)
    {
        $this->supplierPaymentId = $supplierPayment;
    }
}
