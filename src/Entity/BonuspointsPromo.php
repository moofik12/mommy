<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\BonuspointsPromoRepository")
 * @ORM\Table(name="bonuspoints_promo")
 */
class BonuspointsPromo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=64,  nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="hash", type="string", length=32,  nullable=false)
     *
     * @var string
     */
    private $hash;

    /**
     * @ORM\Column(name="points", type="integer", nullable=false)
     *
     * @var int
     */
    private $points;

    /**
     * @ORM\Column(name="bonus_available_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $bonusAvailableAt;

    /**
     * @ORM\Column(name="is_stopped", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isStopped;

    /**
     * @ORM\Column(name="log_message", type="string", length=255,  nullable=false)
     *
     * @var string
     */
    private $logMessage;

    /**
     * @ORM\Column(name="user_message", type="string", length=255,  nullable=false)
     *
     * @var string
     */
    private $userMessage;

    /**
     * @ORM\Column(name="email_confirm", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $emailConfirm;

    /**
     * @ORM\Column(name="start_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $startAt;

    /**
     * @ORM\Column(name="end_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $endAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return int
     */
    public function getBonusAvailableAt()
    {
        return $this->bonusAvailableAt;
    }

    /**
     * @param int $bonusAvailableAt
     */
    public function setBonusAvailableAt($bonusAvailableAt)
    {
        $this->bonusAvailableAt = $bonusAvailableAt;
    }

    /**
     * @return bool
     */
    public function isStopped()
    {
        return $this->isStopped;
    }

    /**
     * @param bool $isStopped
     */
    public function setIsStopped($isStopped)
    {
        $this->isStopped = $isStopped;
    }

    /**
     * @return string
     */
    public function getLogMessage()
    {
        return $this->logMessage;
    }

    /**
     * @param string $logMessage
     */
    public function setLogMessage($logMessage)
    {
        $this->logMessage = $logMessage;
    }

    /**
     * @return string
     */
    public function getUserMessage()
    {
        return $this->userMessage;
    }

    /**
     * @param string $userMessage
     */
    public function setUserMessage($userMessage)
    {
        $this->userMessage = $userMessage;
    }

    /**
     * @return bool
     */
    public function isEmailConfirm()
    {
        return $this->emailConfirm;
    }

    /**
     * @param bool $emailConfirm
     */
    public function setEmailConfirm($emailConfirm)
    {
        $this->emailConfirm = $emailConfirm;
    }

    /**
     * @return int
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param int $startAt
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;
    }

    /**
     * @return int
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param int $endAt
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
