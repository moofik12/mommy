<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\EventProductRepository")
 * @ORM\Table(name="events_products")
 */
class EventProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="stock_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $stockId;

    /**
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     *
     * @var int
     */
    private $viewCount;

    /**
     * @ORM\Column(name="max_per_buy", type="integer", nullable=false)
     *
     * @var int
     */
    private $maxPerBuy;

    /**
     * @ORM\Column(name="unique_article", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $uniqueArticle;

    /**
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="category", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $category;

    /**
     * @ORM\Column(name="price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(name="price_market", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceMarket;

    /**
     * @ORM\Column(name="price_purchase", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $pricePurchase;

    /**
     * @ORM\Column(name="number", type="integer", nullable=false)
     *
     * @var int
     */
    private $number;

    /**
     * @ORM\Column(name="number_arrived", type="integer", nullable=false)
     *
     * @var int
     */
    private $numberArrived;

    /**
     * @ORM\Column(name="number_sold", type="integer", nullable=false)
     *
     * @var int
     */
    private $numberSold;

    /**
     * @ORM\Column(name="number_sold_real", type="integer", nullable=false)
     *
     * @var int
     */
    private $numberSoldReal;

    /**
     * @ORM\Column(name="is_sold_out", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSoldOut;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="article", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $article;

    /**
     * @ORM\Column(name="barcode", type="string", length=50, nullable=false)
     *
     * @var string
     */
    private $barcode;

    /**
     * @ORM\Column(name="internal_code", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $internalCode;

    /**
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(name="images", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $images;

    /**
     * @ORM\Column(name="age_to", type="integer", nullable=false)
     *
     * @var int
     */
    private $ageTo;

    /**
     * @ORM\Column(name="age_from", type="integer", nullable=false)
     *
     * @var int
     */
    private $ageFrom;

    /**
     * @ORM\Column(name="sizeformat", type="string", length=10, nullable=false)
     *
     * @var string
     */
    private $sizeformat;

    /**
     * @ORM\Column(name="size", type="string", length=15, nullable=false)
     *
     * @var string
     */
    private $size;

    /**
     * @ORM\Column(name="sizes_json", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $sizesJson;

    /**
     * @ORM\Column(name="size_guide", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $sizeGuide;

    /**
     * @ORM\Column(name="color", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $color;

    /**
     * @ORM\Column(name="color_code", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $colorCode;

    /**
     * @ORM\Column(name="made_in", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $madeIn;

    /**
     * @ORM\Column(name="design_in", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $designIn;

    /**
     * @ORM\Column(name="composition", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $composition;

    /**
     * @ORM\Column(name="shelflife", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $shelflife;

    /**
     * @ORM\Column(name="weight", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $weight;

    /**
     * @ORM\Column(name="dimensions", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $dimensions;

    /**
     * @ORM\Column(name="description", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(name="target", type="string", length=10, nullable=true)
     *
     * @var string|null
     */
    private $target;

    /**
     * @ORM\Column(name="photo", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="number_supplied", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $numberSupplied;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     *
     * @var int Product
     */
    private $productId;

    /**
     * @ORM\Column(name="brand_id", type="integer", nullable=false)
     *
     * @var int Brand
     */
    private $brandId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="certificate", type="string", length=100, nullable=true)
     *
     * @var string
     */
    private $certificate;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * @param int $stockId
     */
    public function setStockId($stockId)
    {
        $this->stockId = $stockId;
    }

    /**
     * @return int
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }

    /**
     * @param int $viewCount
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;
    }

    /**
     * @return int
     */
    public function getMaxPerBuy()
    {
        return $this->maxPerBuy;
    }

    /**
     * @param int $maxPerBuy
     */
    public function setMaxPerBuy($maxPerBuy)
    {
        $this->maxPerBuy = $maxPerBuy;
    }

    /**
     * @return string
     */
    public function getUniqueArticle()
    {
        return $this->uniqueArticle;
    }

    /**
     * @param string $uniqueArticle
     */
    public function setUniqueArticle($uniqueArticle)
    {
        $this->uniqueArticle = $uniqueArticle;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPriceMarket()
    {
        return $this->priceMarket;
    }

    /**
     * @param float $priceMarket
     */
    public function setPriceMarket($priceMarket)
    {
        $this->priceMarket = $priceMarket;
    }

    /**
     * @return float
     */
    public function getPricePurchase()
    {
        return $this->pricePurchase;
    }

    /**
     * @param float $pricePurchase
     */
    public function setPricePurchase($pricePurchase)
    {
        $this->pricePurchase = $pricePurchase;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getNumberArrived()
    {
        return $this->numberArrived;
    }

    /**
     * @param int $numberArrived
     */
    public function setNumberArrived($numberArrived)
    {
        $this->numberArrived = $numberArrived;
    }

    /**
     * @return int
     */
    public function getNumberSold()
    {
        return $this->numberSold;
    }

    /**
     * @param int $numberSold
     */
    public function setNumberSold($numberSold)
    {
        $this->numberSold = $numberSold;
    }

    /**
     * @return int
     */
    public function getNumberSoldReal()
    {
        return $this->numberSoldReal;
    }

    /**
     * @param int $numberSoldReal
     */
    public function setNumberSoldReal($numberSoldReal)
    {
        $this->numberSoldReal = $numberSoldReal;
    }

    /**
     * @return bool
     */
    public function isSoldOut()
    {
        return $this->isSoldOut;
    }

    /**
     * @param bool $isSoldOut
     */
    public function setIsSoldOut($isSoldOut)
    {
        $this->isSoldOut = $isSoldOut;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param string $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getInternalCode()
    {
        return $this->internalCode;
    }

    /**
     * @param string $internalCode
     */
    public function setInternalCode($internalCode)
    {
        $this->internalCode = $internalCode;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param string $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return int
     */
    public function getAgeTo()
    {
        return $this->ageTo;
    }

    /**
     * @param int $ageTo
     */
    public function setAgeTo($ageTo)
    {
        $this->ageTo = $ageTo;
    }

    /**
     * @return int
     */
    public function getAgeFrom()
    {
        return $this->ageFrom;
    }

    /**
     * @param int $ageFrom
     */
    public function setAgeFrom($ageFrom)
    {
        $this->ageFrom = $ageFrom;
    }

    /**
     * @return string
     */
    public function getSizeformat()
    {
        return $this->sizeformat;
    }

    /**
     * @param string $sizeformat
     */
    public function setSizeformat($sizeformat)
    {
        $this->sizeformat = $sizeformat;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getSizesJson()
    {
        return $this->sizesJson;
    }

    /**
     * @param string $sizesJson
     */
    public function setSizesJson($sizesJson)
    {
        $this->sizesJson = $sizesJson;
    }

    /**
     * @return string
     */
    public function getSizeGuide()
    {
        return $this->sizeGuide;
    }

    /**
     * @param string $sizeGuide
     */
    public function setSizeGuide($sizeGuide)
    {
        $this->sizeGuide = $sizeGuide;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * @param string $colorCode
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;
    }

    /**
     * @return string
     */
    public function getMadeIn()
    {
        return $this->madeIn;
    }

    /**
     * @param string $madeIn
     */
    public function setMadeIn($madeIn)
    {
        $this->madeIn = $madeIn;
    }

    /**
     * @return string
     */
    public function getDesignIn()
    {
        return $this->designIn;
    }

    /**
     * @param string $designIn
     */
    public function setDesignIn($designIn)
    {
        $this->designIn = $designIn;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;
    }

    /**
     * @return string
     */
    public function getShelflife()
    {
        return $this->shelflife;
    }

    /**
     * @param string $shelflife
     */
    public function setShelflife($shelflife)
    {
        $this->shelflife = $shelflife;
    }

    /**
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param string $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param string $dimensions
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param null|string $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int|null
     */
    public function getNumberSupplied()
    {
        return $this->numberSupplied;
    }

    /**
     * @param int|null $numberSupplied
     */
    public function setNumberSupplied($numberSupplied)
    {
        $this->numberSupplied = $numberSupplied;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int Product
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $product Product
     */
    public function setProductId($product)
    {
        $this->productId = $product;
    }

    /**
     * @return int Brand
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * @param int $brand Brand
     */
    public function setBrandId($brand)
    {
        $this->brandId = $brand;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return string|null
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @param string $certificate
     */
    public function setCertificate(string $certificate): void
    {
        $this->certificate = $certificate;
    }
}
