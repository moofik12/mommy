<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\PayGatewayRepository")
 * @ORM\Table(name="pay_gateway")
 */
class PayGateway
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="provider", type="string", length=127, nullable=false)
     *
     * @var string
     */
    private $provider;

    /**
     * @ORM\Column(name="unique_payment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $uniquePayment;

    /**
     * @ORM\Column(name="is_custom", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCustom;

    /**
     * @ORM\Column(name="amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $amount;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="pay_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $payStatus;

    /**
     * @ORM\Column(name="order_bank_refund_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderBankRefundId;

    /**
     * @ORM\Column(name="message", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $message;

    /**
     * @ORM\Column(name="user_ip", type="string", length=40, nullable=false)
     *
     * @var string
     */
    private $userIp;

    /**
     * @ORM\Column(name="user_browser", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $userBrowser;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="invoice_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $invoiceId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getUniquePayment()
    {
        return $this->uniquePayment;
    }

    /**
     * @param string $uniquePayment
     */
    public function setUniquePayment($uniquePayment)
    {
        $this->uniquePayment = $uniquePayment;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param bool $isCustom
     */
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getPayStatus()
    {
        return $this->payStatus;
    }

    /**
     * @param int $payStatus
     */
    public function setPayStatus($payStatus)
    {
        $this->payStatus = $payStatus;
    }

    /**
     * @return int
     */
    public function getOrderBankRefundId()
    {
        return $this->orderBankRefundId;
    }

    /**
     * @param int $orderBankRefundId
     */
    public function setOrderBankRefundId($orderBankRefundId)
    {
        $this->orderBankRefundId = $orderBankRefundId;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * @param string $userIp
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
    }

    /**
     * @return string
     */
    public function getUserBrowser()
    {
        return $this->userBrowser;
    }

    /**
     * @param string $userBrowser
     */
    public function setUserBrowser($userBrowser)
    {
        $this->userBrowser = $userBrowser;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Order
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoice Order
     */
    public function setInvoiceId($invoice)
    {
        $this->invoiceId = $invoice;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
