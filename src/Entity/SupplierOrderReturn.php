<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\SupplierOrderReturnRepository")
 * @ORM\Table(name="suppliers_orders_returns")
 */
class SupplierOrderReturn
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="is_created_supplier", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCreatedSupplier;

    /**
     * @ORM\Column(name="message", type="string", length=400, nullable=false)
     *
     * @var string
     */
    private $message;

    /**
     * @ORM\Column(name="message_callcenter", type="string", length=400, nullable=false)
     *
     * @var string
     */
    private $messageCallcenter;

    /**
     * @ORM\Column(name="bank_name_enc", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $bankNameEnc;

    /**
     * @ORM\Column(name="bank_giro_enc", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $bankGiroEnc;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isCreatedSupplier()
    {
        return $this->isCreatedSupplier;
    }

    /**
     * @param bool $isCreatedSupplier
     */
    public function setIsCreatedSupplier($isCreatedSupplier)
    {
        $this->isCreatedSupplier = $isCreatedSupplier;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getMessageCallcenter()
    {
        return $this->messageCallcenter;
    }

    /**
     * @param string $messageCallcenter
     */
    public function setMessageCallcenter($messageCallcenter)
    {
        $this->messageCallcenter = $messageCallcenter;
    }

    /**
     * @return string
     */
    public function getBankNameEnc()
    {
        return $this->bankNameEnc;
    }

    /**
     * @param string $bankNameEnc
     */
    public function setBankNameEnc($bankNameEnc)
    {
        $this->bankNameEnc = $bankNameEnc;
    }

    /**
     * @return string
     */
    public function getBankGiroEnc()
    {
        return $this->bankGiroEnc;
    }

    /**
     * @param string $bankGiroEnc
     */
    public function setBankGiroEnc($bankGiroEnc)
    {
        $this->bankGiroEnc = $bankGiroEnc;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }
}
