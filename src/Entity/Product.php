<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\ProductRepository")
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="unique_article", type="string", length=100, nullable=true)
     *
     * @var string|null
     */
    private $uniqueArticle;

    /**
     * @ORM\Column(name="article", type="string", length=45, nullable=true)
     *
     * @var string|null
     */
    private $article;

    /**
     * @ORM\Column(name="name", type="string", length=250, nullable=true)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(name="category", type="string", length=100, nullable=false)
     *
     * @var string
     */
    private $category;

    /**
     * @ORM\Column(name="logo_fileid", type="string", length=60, nullable=true)
     *
     * @var string|null
     */
    private $logoFileid;

    /**
     * @ORM\Column(name="images_json", type="string", length=1024, nullable=true)
     *
     * @var string|null
     */
    private $imagesJson;

    /**
     * @ORM\Column(name="images_count", type="integer", nullable=false)
     *
     * @var int
     */
    private $imagesCount;

    /**
     * @ORM\Column(name="made_in", type="string", length=45, nullable=true)
     *
     * @var string|null
     */
    private $madeIn;

    /**
     * @ORM\Column(name="design_in", type="string", length=45, nullable=true)
     *
     * @var string|null
     */
    private $designIn;

    /**
     * @ORM\Column(name="description", type="string", length=1024, nullable=true)
     *
     * @var string|null
     */
    private $description;

    /**
     * @ORM\Column(name="color", type="string", length=100, nullable=true)
     *
     * @var string|null
     */
    private $color;

    /**
     * @ORM\Column(name="color_code", type="integer", nullable=false)
     *
     * @var int
     */
    private $colorCode;

    /**
     * @ORM\Column(name="age_from", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $ageFrom;

    /**
     * @ORM\Column(name="age_to", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $ageTo;

    /**
     * @ORM\Column(name="target", type="string", length=10, nullable=true)
     *
     * @var string|null
     */
    private $target;

    /**
     * @ORM\Column(name="photo", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="brand_id", type="integer", nullable=true)
     *
     * @var int|null Brand|null
     */
    private $brandId;

    /**
     * @ORM\Column(name="section_id", type="integer", nullable=false)
     *
     * @var int ProductSection
     */
    private $sectionId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getUniqueArticle()
    {
        return $this->uniqueArticle;
    }

    /**
     * @param null|string $uniqueArticle
     */
    public function setUniqueArticle($uniqueArticle)
    {
        $this->uniqueArticle = $uniqueArticle;
    }

    /**
     * @return null|string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param null|string $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return null|string
     */
    public function getLogoFileid()
    {
        return $this->logoFileid;
    }

    /**
     * @param null|string $logoFileid
     */
    public function setLogoFileid($logoFileid)
    {
        $this->logoFileid = $logoFileid;
    }

    /**
     * @return null|string
     */
    public function getImagesJson()
    {
        return $this->imagesJson;
    }

    /**
     * @param null|string $imagesJson
     */
    public function setImagesJson($imagesJson)
    {
        $this->imagesJson = $imagesJson;
    }

    /**
     * @return int
     */
    public function getImagesCount()
    {
        return $this->imagesCount;
    }

    /**
     * @param int $imagesCount
     */
    public function setImagesCount($imagesCount)
    {
        $this->imagesCount = $imagesCount;
    }

    /**
     * @return null|string
     */
    public function getMadeIn()
    {
        return $this->madeIn;
    }

    /**
     * @param null|string $madeIn
     */
    public function setMadeIn($madeIn)
    {
        $this->madeIn = $madeIn;
    }

    /**
     * @return null|string
     */
    public function getDesignIn()
    {
        return $this->designIn;
    }

    /**
     * @param null|string $designIn
     */
    public function setDesignIn($designIn)
    {
        $this->designIn = $designIn;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param null|string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * @param int $colorCode
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;
    }

    /**
     * @return int|null
     */
    public function getAgeFrom()
    {
        return $this->ageFrom;
    }

    /**
     * @param int|null $ageFrom
     */
    public function setAgeFrom($ageFrom)
    {
        $this->ageFrom = $ageFrom;
    }

    /**
     * @return int|null
     */
    public function getAgeTo()
    {
        return $this->ageTo;
    }

    /**
     * @param int|null $ageTo
     */
    public function setAgeTo($ageTo)
    {
        $this->ageTo = $ageTo;
    }

    /**
     * @return null|string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param null|string $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int|null Brand|null
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * @param int|null $brand Brand|null
     */
    public function setBrandId($brand)
    {
        $this->brandId = $brand;
    }

    /**
     * @return int ProductSection
     */
    public function getSectionId()
    {
        return $this->sectionId;
    }

    /**
     * @param int $section ProductSection
     */
    public function setSectionId($section)
    {
        $this->sectionId = $section;
    }
}
