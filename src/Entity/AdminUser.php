<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\AdminUserRepository")
 * @ORM\Table(name="admins_users")
 */
class AdminUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="login", type="string", length=32, nullable=false)
     *
     * @var string
     */
    private $login;

    /**
     * @ORM\Column(name="password", type="string", length=43, nullable=false)
     *
     * @var string
     */
    private $password;

    /**
     * @ORM\Column(name="password_salt", type="string", length=5, nullable=true)
     *
     * @var string|null
     */
    private $passwordSalt;

    /**
     * @ORM\Column(name="rules", type="string", nullable=true)
     *
     * @var string|null
     */
    private $rules;

    /**
     * @ORM\Column(name="fullname", type="string", length=100, nullable=true)
     *
     * @var string|null
     */
    private $fullname;

    /**
     * @ORM\Column(name="email", type="string", length=128, nullable=true)
     *
     * @var string|null
     */
    private $email;

    /**
     * @ORM\Column(name="is_root", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isRoot;

    /**
     * @ORM\Column(name="is_supplier", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSupplier;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="ip", type="string", length=15, nullable=true)
     *
     * @var string|null
     */
    private $ip;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return null|string
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @param null|string $passwordSalt
     */
    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = $passwordSalt;
    }

    /**
     * @return null|string
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param null|string $rules
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
    }

    /**
     * @return null|string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param null|string $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return null|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->isRoot;
    }

    /**
     * @param bool $isRoot
     */
    public function setIsRoot($isRoot)
    {
        $this->isRoot = $isRoot;
    }

    /**
     * @return bool
     */
    public function isSupplier()
    {
        return $this->isSupplier;
    }

    /**
     * @param bool $isSupplier
     */
    public function setIsSupplier($isSupplier)
    {
        $this->isSupplier = $isSupplier;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return null|string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param null|string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }
}
