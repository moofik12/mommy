<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\CartRepository")
 * @ORM\Table(name="carts")
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $userId;

    /**
     * @ORM\Column(name="anonymous_id", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $anonymousId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     *
     * @var int EventProduct
     */
    private $productId;

    /**
     * @ORM\Column(name="number", type="integer", nullable=false)
     *
     * @var int
     */
    private $number;

    /**
     * @ORM\Column(name="added_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $addedAt;

    /**
     * @ORM\Column(name="reserved_to", type="integer", nullable=false)
     *
     * @var int
     */
    private $reservedTo;

    /**
     * @ORM\Column(name="reservation_bonus_time", type="integer", nullable=false)
     *
     * @var int
     */
    private $reservationBonusTime;

    /**
     * @ORM\Column(name="priority", type="integer", nullable=false)
     *
     * @var int
     */
    private $priority;

    /**
     * @ORM\Column(name="is_sent_notification", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSentNotification;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getAnonymousId()
    {
        return $this->anonymousId;
    }

    /**
     * @param string $anonymousId
     */
    public function setAnonymousId($anonymousId)
    {
        $this->anonymousId = $anonymousId;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int EventProduct
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $product EventProduct
     */
    public function setProductId($product)
    {
        $this->productId = $product;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * @param int $addedAt
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;
    }

    /**
     * @return int
     */
    public function getReservedTo()
    {
        return $this->reservedTo;
    }

    /**
     * @param int $reservedTo
     */
    public function setReservedTo($reservedTo)
    {
        $this->reservedTo = $reservedTo;
    }

    /**
     * @return int
     */
    public function getReservationBonusTime()
    {
        return $this->reservationBonusTime;
    }

    /**
     * @param int $reservationBonusTime
     */
    public function setReservationBonusTime($reservationBonusTime)
    {
        $this->reservationBonusTime = $reservationBonusTime;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return bool
     */
    public function isSentNotification()
    {
        return $this->isSentNotification;
    }

    /**
     * @param bool $isSentNotification
     */
    public function setIsSentNotification($isSentNotification)
    {
        $this->isSentNotification = $isSentNotification;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
