<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements UserInterface, Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="landing_num", type="integer", nullable=false)
     *
     * @var int
     */
    private $landingNum = 0;

    /**
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     *
     * @var string
     */
    private $name = '';

    /**
     * @ORM\Column(name="surname", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $surname = '';

    /**
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="telephone", type="string", length=20, nullable=true)
     *
     * @var string|null
     */
    private $telephone;

    /**
     * @ORM\Column(name="password_hash", type="string", length=128, nullable=false)
     *
     * @var string
     */
    private $passwordHash;

    /**
     * @ORM\Column(name="password_salt", type="string", length=20, nullable=false)
     *
     * @var string
     */
    private $passwordSalt = '';

    /**
     * @ORM\Column(name="address", type="string", length=1024, nullable=true)
     *
     * @var string
     */
    private $address;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status = 0;

    /**
     * @ORM\Column(name="category", type="string", nullable=true)
     *
     * @var string
     */
    private $category;

    /**
     * @ORM\Column(name="psychotype", type="integer", nullable=false)
     *
     * @var int
     */
    private $psychotype = 0;

    /**
     * @ORM\Column(name="discount_percent", type="integer", nullable=false)
     *
     * @var int
     */
    private $discountPercent = 0;

    /**
     * @ORM\Column(name="is_email_verified", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isEmailVerified = 0;

    /**
     * @ORM\Column(name="present_promocode", type="string", length=21, nullable=false)
     *
     * @var string
     */
    private $presentPromocode = '';

    /**
     * @ORM\Column(name="offer_provider", type="integer", nullable=false)
     *
     * @var int
     */
    private $offerProvider = 0;

    /**
     * @ORM\Column(name="offer_id", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $offerId = 0;

    /**
     * @ORM\Column(name="offer_target_url", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $offerTargetUrl = '';

    /**
     * @ORM\Column(name="offer_success", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $offerSuccess = false;

    /**
     * @ORM\Column(name="moved_search_engine", type="string", length=32, nullable=false)
     *
     * @var string
     */
    private $movedSearchEngine = '';

    /**
     * @ORM\Column(name="password_recovery_requested_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $passwordRecoveryRequestedAt = 0;

    /**
     * @ORM\Column(name="email_unsubscribe", type="string", length=128, nullable=true)
     *
     * @var string|null
     */
    private $emailUnsubscribe;

    /**
     * @ORM\Column(name="is_system_subscribe", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSystemSubscribe = true;

    /**
     * @ORM\Column(name="is_subscribe", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSubscribe = true;

    /**
     * @ORM\Column(name="is_subscribed", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSubscribed = false;

    /**
     * @ORM\Column(name="is_service_unsubscribed", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isServiceUnsubscribed = false;

    /**
     * @ORM\Column(name="is_service_possibly_subscribe", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isServicePossiblySubscribe = true;

    /**
     * @ORM\Column(name="ip", type="string", length=40, nullable=true)
     *
     * @var string|null
     */
    private $ip;

    /**
     * @ORM\Column(name="last_order_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $lastOrderAt = 0;

    /**
     * @ORM\Column(name="interests_generated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $interestsGeneratedAt = 0;

    /**
     * @ORM\Column(name="interests_amount", type="integer", nullable=false)
     *
     * @var int
     */
    private $interestsAmount = 0;

    /**
     * @ORM\Column(name="last_interests_mailed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $lastInterestsMailedAt = 0;

    /**
     * @ORM\Column(name="returned_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $returnedAt = 0;

    /**
     * @ORM\Column(name="last_visit_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $lastVisitAt = 0;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="invited_by", type="integer", nullable=true)
     *
     * @var int|null User|null
     */
    private $invitedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLandingNum()
    {
        return $this->landingNum;
    }

    /**
     * @param int $landingNum
     */
    public function setLandingNum($landingNum)
    {
        $this->landingNum = $landingNum;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param null|string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param string $passwordHash
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return string
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @param string $passwordSalt
     */
    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = $passwordSalt;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address ?: '';
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address ?: '';
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return int
     */
    public function getPsychotype()
    {
        return $this->psychotype;
    }

    /**
     * @param int $psychotype
     */
    public function setPsychotype($psychotype)
    {
        $this->psychotype = $psychotype;
    }

    /**
     * @return int
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * @param int $discountPercent
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;
    }

    /**
     * @return bool
     */
    public function isEmailVerified()
    {
        return $this->isEmailVerified;
    }

    /**
     * @param bool $isEmailVerified
     */
    public function setIsEmailVerified($isEmailVerified)
    {
        $this->isEmailVerified = $isEmailVerified;
    }

    /**
     * @return string
     */
    public function getPresentPromocode()
    {
        return $this->presentPromocode;
    }

    /**
     * @param string $presentPromocode
     */
    public function setPresentPromocode($presentPromocode)
    {
        $this->presentPromocode = $presentPromocode;
    }

    /**
     * @return int
     */
    public function getOfferProvider()
    {
        return $this->offerProvider;
    }

    /**
     * @param int $offerProvider
     */
    public function setOfferProvider($offerProvider)
    {
        $this->offerProvider = $offerProvider;
    }

    /**
     * @return string
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * @param string $offerId
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;
    }

    /**
     * @return string
     */
    public function getOfferTargetUrl()
    {
        return $this->offerTargetUrl;
    }

    /**
     * @param string $offerTargetUrl
     */
    public function setOfferTargetUrl($offerTargetUrl)
    {
        $this->offerTargetUrl = $offerTargetUrl;
    }

    /**
     * @return bool
     */
    public function isOfferSuccess()
    {
        return $this->offerSuccess;
    }

    /**
     * @param bool $offerSuccess
     */
    public function setOfferSuccess($offerSuccess)
    {
        $this->offerSuccess = $offerSuccess;
    }

    /**
     * @return string
     */
    public function getMovedSearchEngine()
    {
        return $this->movedSearchEngine;
    }

    /**
     * @param string $movedSearchEngine
     */
    public function setMovedSearchEngine($movedSearchEngine)
    {
        $this->movedSearchEngine = $movedSearchEngine;
    }

    /**
     * @return int
     */
    public function getPasswordRecoveryRequestedAt()
    {
        return $this->passwordRecoveryRequestedAt;
    }

    /**
     * @param int $passwordRecoveryRequestedAt
     */
    public function setPasswordRecoveryRequestedAt($passwordRecoveryRequestedAt)
    {
        $this->passwordRecoveryRequestedAt = $passwordRecoveryRequestedAt;
    }

    /**
     * @return null|string
     */
    public function getEmailUnsubscribe()
    {
        return $this->emailUnsubscribe;
    }

    /**
     * @param null|string $emailUnsubscribe
     */
    public function setEmailUnsubscribe($emailUnsubscribe)
    {
        $this->emailUnsubscribe = $emailUnsubscribe;
    }

    /**
     * @return bool
     */
    public function isSystemSubscribe()
    {
        return $this->isSystemSubscribe;
    }

    /**
     * @param bool $isSystemSubscribe
     */
    public function setIsSystemSubscribe($isSystemSubscribe)
    {
        $this->isSystemSubscribe = $isSystemSubscribe;
    }

    /**
     * @return bool
     */
    public function isSubscribe()
    {
        return $this->isSubscribe;
    }

    /**
     * @param bool $isSubscribe
     */
    public function setIsSubscribe($isSubscribe)
    {
        $this->isSubscribe = $isSubscribe;
    }

    /**
     * @return bool
     */
    public function isSubscribed()
    {
        return $this->isSubscribed;
    }

    /**
     * @param bool $isSubscribed
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;
    }

    /**
     * @return bool
     */
    public function isServiceUnsubscribed()
    {
        return $this->isServiceUnsubscribed;
    }

    /**
     * @param bool $isServiceUnsubscribed
     */
    public function setIsServiceUnsubscribed($isServiceUnsubscribed)
    {
        $this->isServiceUnsubscribed = $isServiceUnsubscribed;
    }

    /**
     * @return bool
     */
    public function isServicePossiblySubscribe()
    {
        return $this->isServicePossiblySubscribe;
    }

    /**
     * @param bool $isServicePossiblySubscribe
     */
    public function setIsServicePossiblySubscribe($isServicePossiblySubscribe)
    {
        $this->isServicePossiblySubscribe = $isServicePossiblySubscribe;
    }

    /**
     * @return null|string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param null|string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getLastOrderAt()
    {
        return $this->lastOrderAt;
    }

    /**
     * @param int $lastOrderAt
     */
    public function setLastOrderAt($lastOrderAt)
    {
        $this->lastOrderAt = $lastOrderAt;
    }

    /**
     * @return int
     */
    public function getInterestsGeneratedAt()
    {
        return $this->interestsGeneratedAt;
    }

    /**
     * @param int $interestsGeneratedAt
     */
    public function setInterestsGeneratedAt($interestsGeneratedAt)
    {
        $this->interestsGeneratedAt = $interestsGeneratedAt;
    }

    /**
     * @return int
     */
    public function getInterestsAmount()
    {
        return $this->interestsAmount;
    }

    /**
     * @param int $interestsAmount
     */
    public function setInterestsAmount($interestsAmount)
    {
        $this->interestsAmount = $interestsAmount;
    }

    /**
     * @return int
     */
    public function getLastInterestsMailedAt()
    {
        return $this->lastInterestsMailedAt;
    }

    /**
     * @param int $lastInterestsMailedAt
     */
    public function setLastInterestsMailedAt($lastInterestsMailedAt)
    {
        $this->lastInterestsMailedAt = $lastInterestsMailedAt;
    }

    /**
     * @return int
     */
    public function getReturnedAt()
    {
        return $this->returnedAt;
    }

    /**
     * @param int $returnedAt
     */
    public function setReturnedAt($returnedAt)
    {
        $this->returnedAt = $returnedAt;
    }

    /**
     * @return int
     */
    public function getLastVisitAt()
    {
        return $this->lastVisitAt;
    }

    /**
     * @param int $lastVisitAt
     */
    public function setLastVisitAt($lastVisitAt)
    {
        $this->lastVisitAt = $lastVisitAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int|null User|null
     */
    public function getInvitedBy()
    {
        return $this->invitedBy;
    }

    /**
     * @param int|null $invitedBy User|null
     */
    public function setInvitedBy($invitedBy)
    {
        $this->invitedBy = $invitedBy;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->passwordHash;
    }

    /**
     * @return null|string
     */
    public function getSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        /* Нужно имплементить только в случае когда храним в токене незащищенные данные (plain password и т.п.) */
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            'id' => $this->id,
            'email' => $this->email,
            'passwordHash' => $this->passwordHash,
            'passwordSalt' => $this->passwordSalt
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $unserializedTokenData = unserialize($serialized);

        $this->id = $unserializedTokenData['id'];
        $this->email = $unserializedTokenData['email'];
        $this->passwordHash = $unserializedTokenData['passwordHash'];
        $this->passwordSalt = $unserializedTokenData['passwordSalt'];
    }
}
