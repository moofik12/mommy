<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserPartnerInviteRepository")
 * @ORM\Table(name="users_partner_invites")
 */
class UserPartnerInvite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="type_incoming", type="integer", nullable=false)
     *
     * @var int
     */
    private $typeIncoming;

    /**
     * @ORM\Column(name="url_referer", type="string", length=500, nullable=false)
     *
     * @var string
     */
    private $urlReferer;

    /**
     * @ORM\Column(name="utm_params", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $utmParams;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="partner_id", type="integer", nullable=false)
     *
     * @var int UserPartner
     */
    private $partnerId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTypeIncoming()
    {
        return $this->typeIncoming;
    }

    /**
     * @param int $typeIncoming
     */
    public function setTypeIncoming($typeIncoming)
    {
        $this->typeIncoming = $typeIncoming;
    }

    /**
     * @return string
     */
    public function getUrlReferer()
    {
        return $this->urlReferer;
    }

    /**
     * @param string $urlReferer
     */
    public function setUrlReferer($urlReferer)
    {
        $this->urlReferer = $urlReferer;
    }

    /**
     * @return string
     */
    public function getUtmParams()
    {
        return $this->utmParams;
    }

    /**
     * @param string $utmParams
     */
    public function setUtmParams($utmParams)
    {
        $this->utmParams = $utmParams;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int UserPartner
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partner UserPartner
     */
    public function setPartnerId($partner)
    {
        $this->partnerId = $partner;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
