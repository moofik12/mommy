<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserPartnerRepository")
 * @ORM\Table(name="users_partner")
 */
class UserPartner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="source", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $source;

    /**
     * @ORM\Column(name="partner_percent", type="integer", nullable=false)
     *
     * @var int
     */
    private $partnerPercent;

    /**
     * @ORM\Column(name="is_sent_unisender", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isSentUnisender;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return int
     */
    public function getPartnerPercent()
    {
        return $this->partnerPercent;
    }

    /**
     * @param int $partnerPercent
     */
    public function setPartnerPercent($partnerPercent)
    {
        $this->partnerPercent = $partnerPercent;
    }

    /**
     * @return bool
     */
    public function isSentUnisender()
    {
        return $this->isSentUnisender;
    }

    /**
     * @param bool $isSentUnisender
     */
    public function setIsSentUnisender($isSentUnisender)
    {
        $this->isSentUnisender = $isSentUnisender;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
