<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserPartnerAdmissionRepository")
 * @ORM\Table(name="users_partner_admissions")
 */
class UserPartnerAdmission
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="count_product_order", type="integer", nullable=false)
     *
     * @var int
     */
    private $countProductOrder;

    /**
     * @ORM\Column(name="sum_order", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $sumOrder;

    /**
     * @ORM\Column(name="amount", type="integer", nullable=false)
     *
     * @var int
     */
    private $amount;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="paid_after_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $paidAfterAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="partner_id", type="integer", nullable=false)
     *
     * @var int UserPartner
     */
    private $partnerId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCountProductOrder()
    {
        return $this->countProductOrder;
    }

    /**
     * @param int $countProductOrder
     */
    public function setCountProductOrder($countProductOrder)
    {
        $this->countProductOrder = $countProductOrder;
    }

    /**
     * @return float
     */
    public function getSumOrder()
    {
        return $this->sumOrder;
    }

    /**
     * @param float $sumOrder
     */
    public function setSumOrder($sumOrder)
    {
        $this->sumOrder = $sumOrder;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getPaidAfterAt()
    {
        return $this->paidAfterAt;
    }

    /**
     * @param int $paidAfterAt
     */
    public function setPaidAfterAt($paidAfterAt)
    {
        $this->paidAfterAt = $paidAfterAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int UserPartner
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partner UserPartner
     */
    public function setPartnerId($partner)
    {
        $this->partnerId = $partner;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
