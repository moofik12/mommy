<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\UserBlackListRepository")
 * @ORM\Table(name="users_black_list")
 */
class UserBlackList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="redemption_orders", type="float", precision=3, scale=2, nullable=false)
     *
     * @var float
     */
    private $redemptionOrders;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $historyJson;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $personId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getRedemptionOrders()
    {
        return $this->redemptionOrders;
    }

    /**
     * @param float $redemptionOrders
     */
    public function setRedemptionOrders($redemptionOrders)
    {
        $this->redemptionOrders = $redemptionOrders;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getHistoryJson()
    {
        return $this->historyJson;
    }

    /**
     * @param string $historyJson
     */
    public function setHistoryJson($historyJson)
    {
        $this->historyJson = $historyJson;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int AdminUser
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int $person AdminUser
     */
    public function setPersonId($person)
    {
        $this->personId = $person;
    }
}
