<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\ProductSectionRepository")
 * @ORM\Table(name="products_sections")
 */
class ProductSection
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="keywords", type="string", length=250, nullable=false)
     *
     * @var string
     */
    private $keywords;

    /**
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     *
     * @var int|null ProductSection|null
     */
    private $parentId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return int|null ProductSection|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parent ProductSection|null
     */
    public function setParentId($parent)
    {
        $this->parentId = $parent;
    }
}
