<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\TaskBoardRepository")
 * @ORM\Table(name="task_board")
 */
class TaskBoard
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="task_board_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $taskBoardType;

    /**
     * @ORM\Column(name="question_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $questionType;

    /**
     * @ORM\Column(name="question_description", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $questionDescription;

    /**
     * @ORM\Column(name="answer_counter", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $answerCounter;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="url", type="string", length=500, nullable=false)
     *
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(name="is_archive", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isArchive;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="products", type="string", length=255, nullable=true)
     *
     * @var string|null
     */
    private $products;

    /**
     * @ORM\Column(name="owner_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $ownerId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $orderId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTaskBoardType()
    {
        return $this->taskBoardType;
    }

    /**
     * @param int $taskBoardType
     */
    public function setTaskBoardType($taskBoardType)
    {
        $this->taskBoardType = $taskBoardType;
    }

    /**
     * @return int
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * @param int $questionType
     */
    public function setQuestionType($questionType)
    {
        $this->questionType = $questionType;
    }

    /**
     * @return string
     */
    public function getQuestionDescription()
    {
        return $this->questionDescription;
    }

    /**
     * @param string $questionDescription
     */
    public function setQuestionDescription($questionDescription)
    {
        $this->questionDescription = $questionDescription;
    }

    /**
     * @return int|null
     */
    public function getAnswerCounter()
    {
        return $this->answerCounter;
    }

    /**
     * @param int|null $answerCounter
     */
    public function setAnswerCounter($answerCounter)
    {
        $this->answerCounter = $answerCounter;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return bool
     */
    public function isArchive()
    {
        return $this->isArchive;
    }

    /**
     * @param bool $isArchive
     */
    public function setIsArchive($isArchive)
    {
        $this->isArchive = $isArchive;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return null|string
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param null|string $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return int AdminUser
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param int $owner AdminUser
     */
    public function setOwnerId($owner)
    {
        $this->ownerId = $owner;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int AdminUser
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order AdminUser
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }
}
