<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderControlRepository")
 * @ORM\Table(name="orders_control")
 */
class OrderControl
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusPrev;

    /**
     * @ORM\Column(name="operator_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $operatorStatus;

    /**
     * @ORM\Column(name="operator_status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $operatorStatusPrev;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="system_comment", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $systemComment;

    /**
     * @ORM\Column(name="order_mailed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderMailedAt;

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt;

    /**
     * @ORM\Column(name="operator_status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $operatorStatusChangedAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusPrev()
    {
        return $this->statusPrev;
    }

    /**
     * @param int $statusPrev
     */
    public function setStatusPrev($statusPrev)
    {
        $this->statusPrev = $statusPrev;
    }

    /**
     * @return int
     */
    public function getOperatorStatus()
    {
        return $this->operatorStatus;
    }

    /**
     * @param int $operatorStatus
     */
    public function setOperatorStatus($operatorStatus)
    {
        $this->operatorStatus = $operatorStatus;
    }

    /**
     * @return int
     */
    public function getOperatorStatusPrev()
    {
        return $this->operatorStatusPrev;
    }

    /**
     * @param int $operatorStatusPrev
     */
    public function setOperatorStatusPrev($operatorStatusPrev)
    {
        $this->operatorStatusPrev = $operatorStatusPrev;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getSystemComment()
    {
        return $this->systemComment;
    }

    /**
     * @param string $systemComment
     */
    public function setSystemComment($systemComment)
    {
        $this->systemComment = $systemComment;
    }

    /**
     * @return int
     */
    public function getOrderMailedAt()
    {
        return $this->orderMailedAt;
    }

    /**
     * @param int $orderMailedAt
     */
    public function setOrderMailedAt($orderMailedAt)
    {
        $this->orderMailedAt = $orderMailedAt;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return int
     */
    public function getOperatorStatusChangedAt()
    {
        return $this->operatorStatusChangedAt;
    }

    /**
     * @param int $operatorStatusChangedAt
     */
    public function setOperatorStatusChangedAt($operatorStatusChangedAt)
    {
        $this->operatorStatusChangedAt = $operatorStatusChangedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }
}
