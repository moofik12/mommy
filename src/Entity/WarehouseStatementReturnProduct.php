<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\WarehouseStatementReturnProductRepository")
 * @ORM\Table(name="warehouse_statements_return_products")
 */
class WarehouseStatementReturnProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="is_change_product_status", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isChangeProductStatus;

    /**
     * @ORM\Column(name="changed_product_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $changedProductStatus;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="statement_id", type="integer", nullable=true)
     *
     * @var int|null WarehouseStatementReturn|null
     */
    private $statementId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     *
     * @var int WarehouseProduct
     */
    private $productId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int Event
     */
    private $eventId;

    /**
     * @ORM\Column(name="event_product_id", type="integer", nullable=false)
     *
     * @var int EventProduct
     */
    private $eventProductId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isChangeProductStatus()
    {
        return $this->isChangeProductStatus;
    }

    /**
     * @param bool $isChangeProductStatus
     */
    public function setIsChangeProductStatus($isChangeProductStatus)
    {
        $this->isChangeProductStatus = $isChangeProductStatus;
    }

    /**
     * @return int
     */
    public function getChangedProductStatus()
    {
        return $this->changedProductStatus;
    }

    /**
     * @param int $changedProductStatus
     */
    public function setChangedProductStatus($changedProductStatus)
    {
        $this->changedProductStatus = $changedProductStatus;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int|null WarehouseStatementReturn
     */
    public function getStatementId()
    {
        return $this->statementId;
    }

    /**
     * @param int|null $statement WarehouseStatementReturn
     */
    public function setStatementId($statement)
    {
        $this->statementId = $statement;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int WarehouseProduct
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $product WarehouseProduct
     */
    public function setProductId($product)
    {
        $this->productId = $product;
    }

    /**
     * @return int Event
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $event Event
     */
    public function setEventId($event)
    {
        $this->eventId = $event;
    }

    /**
     * @return int EventProduct
     */
    public function getEventProductId()
    {
        return $this->eventProductId;
    }

    /**
     * @param int $eventProduct EventProduct
     */
    public function setEventProductId($eventProduct)
    {
        $this->eventProductId = $eventProduct;
    }
}
