<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderBankRefundRepository")
 * @ORM\Table(name="orders_banks_refunds")
 */
class OrderBankRefund
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="price_order", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceOrder;

    /**
     * @ORM\Column(name="discount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $discount;

    /**
     * @ORM\Column(name="price_refund", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $priceRefund;

    /**
     * @ORM\Column(name="payment_after_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $paymentAfterAt;

    /**
     * @ORM\Column(name="order_card_payed", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $orderCardPayed;

    /**
     * @ORM\Column(name="transaction", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $transaction;

    /**
     * @ORM\Column(name="is_refunded", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isRefunded;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusPrev;

    /**
     * @ORM\Column(name="type_refund", type="integer", nullable=false)
     *
     * @var int
     */
    private $typeRefund;

    /**
     * @ORM\Column(name="status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $statusHistoryJson;

    /**
     * @ORM\Column(name="status_changed_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $statusChangedAt;

    /**
     * @ORM\Column(name="is_custom", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCustom;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $comment;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="order_user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $orderUserId;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $adminId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPriceOrder()
    {
        return $this->priceOrder;
    }

    /**
     * @param float $priceOrder
     */
    public function setPriceOrder($priceOrder)
    {
        $this->priceOrder = $priceOrder;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getPriceRefund()
    {
        return $this->priceRefund;
    }

    /**
     * @param float $priceRefund
     */
    public function setPriceRefund($priceRefund)
    {
        $this->priceRefund = $priceRefund;
    }

    /**
     * @return int
     */
    public function getPaymentAfterAt()
    {
        return $this->paymentAfterAt;
    }

    /**
     * @param int $paymentAfterAt
     */
    public function setPaymentAfterAt($paymentAfterAt)
    {
        $this->paymentAfterAt = $paymentAfterAt;
    }

    /**
     * @return float
     */
    public function getOrderCardPayed()
    {
        return $this->orderCardPayed;
    }

    /**
     * @param float $orderCardPayed
     */
    public function setOrderCardPayed($orderCardPayed)
    {
        $this->orderCardPayed = $orderCardPayed;
    }

    /**
     * @return string
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param string $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return bool
     */
    public function isRefunded()
    {
        return $this->isRefunded;
    }

    /**
     * @param bool $isRefunded
     */
    public function setIsRefunded($isRefunded)
    {
        $this->isRefunded = $isRefunded;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusPrev()
    {
        return $this->statusPrev;
    }

    /**
     * @param int $statusPrev
     */
    public function setStatusPrev($statusPrev)
    {
        $this->statusPrev = $statusPrev;
    }

    /**
     * @return int
     */
    public function getTypeRefund()
    {
        return $this->typeRefund;
    }

    /**
     * @param int $typeRefund
     */
    public function setTypeRefund($typeRefund)
    {
        $this->typeRefund = $typeRefund;
    }

    /**
     * @return string
     */
    public function getStatusHistoryJson()
    {
        return $this->statusHistoryJson;
    }

    /**
     * @param string $statusHistoryJson
     */
    public function setStatusHistoryJson($statusHistoryJson)
    {
        $this->statusHistoryJson = $statusHistoryJson;
    }

    /**
     * @return int
     */
    public function getStatusChangedAt()
    {
        return $this->statusChangedAt;
    }

    /**
     * @param int $statusChangedAt
     */
    public function setStatusChangedAt($statusChangedAt)
    {
        $this->statusChangedAt = $statusChangedAt;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->isCustom;
    }

    /**
     * @param bool $isCustom
     */
    public function setIsCustom($isCustom)
    {
        $this->isCustom = $isCustom;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int User
     */
    public function getOrderUserId()
    {
        return $this->orderUserId;
    }

    /**
     * @param int $orderUser User
     */
    public function setOrderUserId($orderUser)
    {
        $this->orderUserId = $orderUser;
    }

    /**
     * @return int AdminUser
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @param int $admin AdminUser
     */
    public function setAdminId($admin)
    {
        $this->adminId = $admin;
    }
}
