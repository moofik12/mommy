<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\ApplicationSubscriberRepository")
 * @ORM\Table(name="application_subscribers")
 */
class ApplicationSubscriber
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     *
     * @var int
     */
    private $type;

    /**
     * @ORM\Column(name="client_id", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $clientId;

    /**
     * @ORM\Column(name="client_gcm_id", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $clientGcmId;

    /**
     * @ORM\Column(name="version_code", type="integer", nullable=false)
     *
     * @var int
     */
    private $versionCode;

    /**
     * @ORM\Column(name="version_name", type="string", length=10, nullable=false)
     *
     * @var string
     */
    private $versionName;

    /**
     * @ORM\Column(name="last_subject", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $lastSubject;

    /**
     * @ORM\Column(name="last_body", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $lastBody;

    /**
     * @ORM\Column(name="last_url", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $lastUrl;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientGcmId()
    {
        return $this->clientGcmId;
    }

    /**
     * @param string $clientGcmId
     */
    public function setClientGcmId($clientGcmId)
    {
        $this->clientGcmId = $clientGcmId;
    }

    /**
     * @return int
     */
    public function getVersionCode()
    {
        return $this->versionCode;
    }

    /**
     * @param int $versionCode
     */
    public function setVersionCode($versionCode)
    {
        $this->versionCode = $versionCode;
    }

    /**
     * @return string
     */
    public function getVersionName()
    {
        return $this->versionName;
    }

    /**
     * @param string $versionName
     */
    public function setVersionName($versionName)
    {
        $this->versionName = $versionName;
    }

    /**
     * @return string
     */
    public function getLastSubject()
    {
        return $this->lastSubject;
    }

    /**
     * @param string $lastSubject
     */
    public function setLastSubject($lastSubject)
    {
        $this->lastSubject = $lastSubject;
    }

    /**
     * @return string
     */
    public function getLastBody()
    {
        return $this->lastBody;
    }

    /**
     * @param string $lastBody
     */
    public function setLastBody($lastBody)
    {
        $this->lastBody = $lastBody;
    }

    /**
     * @return string
     */
    public function getLastUrl()
    {
        return $this->lastUrl;
    }

    /**
     * @param string $lastUrl
     */
    public function setLastUrl($lastUrl)
    {
        $this->lastUrl = $lastUrl;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
