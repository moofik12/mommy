<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\MoneyReceiveStatementOrderRepository")
 * @ORM\Table(name="money_receive_statements_orders")
 */
class MoneyReceiveStatementOrder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderId;

    /**
     * @ORM\Column(name="trackcode", type="string", length=45, nullable=false)
     *
     * @var string
     */
    private $trackcode;

    /**
     * @ORM\Column(name="order_price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $orderPrice;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="statement_id", type="integer", nullable=false)
     *
     * @var int MoneyReceiveStatement
     */
    private $statementId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getTrackcode()
    {
        return $this->trackcode;
    }

    /**
     * @param string $trackcode
     */
    public function setTrackcode($trackcode)
    {
        $this->trackcode = $trackcode;
    }

    /**
     * @return float
     */
    public function getOrderPrice()
    {
        return $this->orderPrice;
    }

    /**
     * @param float $orderPrice
     */
    public function setOrderPrice($orderPrice)
    {
        $this->orderPrice = $orderPrice;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int MoneyReceiveStatement
     */
    public function getStatementId()
    {
        return $this->statementId;
    }

    /**
     * @param int $statement MoneyReceiveStatement
     */
    public function setStatementId($statement)
    {
        $this->statementId = $statement;
    }
}
