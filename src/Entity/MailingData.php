<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\MailingDataRepository")
 * @ORM\Table(name="mailing_data")
 */
class MailingData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="anonymous_id", type="string", length=128, nullable=true)
     *
     * @var string
     */
    private $anonymousId;

    /**
     * @ORM\Column(name="user_id", type="string", length=128, nullable=true)
     *
     * @var string
     */
    private $userId;

    /**
     * @ORM\Column(name="name", type="string", length=30, nullable=true)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="email", type="string", length=128, nullable=true)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $source;

    /**
     * @ORM\Column(name="country", type="string", length=10, nullable=true)
     *
     * @var string
     */
    private $country;

    /**
     * @ORM\Column(name="products_ids", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $productsIds;

    /**
     * @ORM\Column(name="categories_ids", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $categoriesIds;

    /**
     * @ORM\Column(name="is_registered", type="boolean", nullable=true)
     *
     * @var bool
     */
    private $isRegistered;

    /**
     * @ORM\Column(name="is_subscribed", type="boolean", nullable=true)
     *
     * @var bool
     */
    private $isSubscribed;

    /**
     * @ORM\Column(name="ip_address", type="string", length=15, nullable=true)
     *
     * @var string
     */
    private $ipAddress;

    /**
     * @ORM\Column(name="last_login_date", type="integer", nullable=true)
     *
     * @var int
     */
    private $lastLoginDate;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="distribution_event_id", type="integer", nullable=false)
     *
     * @var int MailingEvent
     */
    private $distributionEventId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAnonymousId()
    {
        return $this->anonymousId;
    }

    /**
     * @param string $anonymousId
     */
    public function setAnonymousId($anonymousId)
    {
        $this->anonymousId = $anonymousId;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getProductsIds()
    {
        return $this->productsIds;
    }

    /**
     * @param string $productsIds
     */
    public function setProductsIds($productsIds)
    {
        $this->productsIds = $productsIds;
    }

    /**
     * @return string
     */
    public function getCategoriesIds()
    {
        return $this->categoriesIds;
    }

    /**
     * @param string $categoriesIds
     */
    public function setCategoriesIds($categoriesIds)
    {
        $this->categoriesIds = $categoriesIds;
    }

    /**
     * @return bool
     */
    public function isRegistered()
    {
        return $this->isRegistered;
    }

    /**
     * @param bool $isRegistered
     */
    public function setIsRegistered($isRegistered)
    {
        $this->isRegistered = $isRegistered;
    }

    /**
     * @return bool
     */
    public function isSubscribed()
    {
        return $this->isSubscribed;
    }

    /**
     * @param bool $isSubscribed
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return int
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * @param int $lastLoginDate
     */
    public function setLastLoginDate($lastLoginDate)
    {
        $this->lastLoginDate = $lastLoginDate;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int MailingEvent
     */
    public function getDistributionEventId()
    {
        return $this->distributionEventId;
    }

    /**
     * @param int $distributionEvent MailingEvent
     */
    public function setDistributionEventId($distributionEvent)
    {
        $this->distributionEventId = $distributionEvent;
    }
}
