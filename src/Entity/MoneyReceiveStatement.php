<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\MoneyReceiveStatementRepository")
 * @ORM\Table(name="money_receive_statements")
 */
class MoneyReceiveStatement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="delivery_type", type="integer", nullable=false)
     *
     * @var int
     */
    private $deliveryType;

    /**
     * @ORM\Column(name="money_expected", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $moneyExpected;

    /**
     * @ORM\Column(name="money_real", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $moneyReal;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * @param int $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    }

    /**
     * @return float
     */
    public function getMoneyExpected()
    {
        return $this->moneyExpected;
    }

    /**
     * @param float $moneyExpected
     */
    public function setMoneyExpected($moneyExpected)
    {
        $this->moneyExpected = $moneyExpected;
    }

    /**
     * @return float
     */
    public function getMoneyReal()
    {
        return $this->moneyReal;
    }

    /**
     * @param float $moneyReal
     */
    public function setMoneyReal($moneyReal)
    {
        $this->moneyReal = $moneyReal;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
