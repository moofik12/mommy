<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\PayFromSettlementAccountRepository")
 * @ORM\Table(name="pay_from_settlement_account")
 */
class PayFromSettlementAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     *
     * @var int
     */
    private $status;

    /**
     * @ORM\Column(name="unique_payment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $uniquePayment;

    /**
     * @ORM\Column(name="amount", type="decimal", precision=12, scale=2, nullable=false)
     *
     * @var float
     */
    private $amount;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $orderId;

    /**
     * @ORM\Column(name="provider", type="string", length=127, nullable=false)
     *
     * @var string
     */
    private $provider;

    /**
     * @ORM\Column(name="pay_comment", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $payComment;

    /**
     * @ORM\Column(name="operation_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $operationAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="possible_order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $possibleOrderId;

    /**
     * @ORM\Column(name="pay_gateway_id", type="integer", nullable=false)
     *
     * @var int PayGateway
     */
    private $payGatewayId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getUniquePayment()
    {
        return $this->uniquePayment;
    }

    /**
     * @param string $uniquePayment
     */
    public function setUniquePayment($uniquePayment)
    {
        $this->uniquePayment = $uniquePayment;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getPayComment()
    {
        return $this->payComment;
    }

    /**
     * @param string $payComment
     */
    public function setPayComment($payComment)
    {
        $this->payComment = $payComment;
    }

    /**
     * @return int
     */
    public function getOperationAt()
    {
        return $this->operationAt;
    }

    /**
     * @param int $operationAt
     */
    public function setOperationAt($operationAt)
    {
        $this->operationAt = $operationAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int Order
     */
    public function getPossibleOrderId()
    {
        return $this->possibleOrderId;
    }

    /**
     * @param int $possibleOrder Order
     */
    public function setPossibleOrderId($possibleOrder)
    {
        $this->possibleOrderId = $possibleOrder;
    }

    /**
     * @return int PayGateway
     */
    public function getPayGatewayId()
    {
        return $this->payGatewayId;
    }

    /**
     * @param int $payGateway PayGateway
     */
    public function setPayGatewayId($payGateway)
    {
        $this->payGatewayId = $payGateway;
    }
}
