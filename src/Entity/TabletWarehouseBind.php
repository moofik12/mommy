<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\TabletWarehouseBindRepository")
 * @ORM\Table(name="tablet_warehouse_products_binds")
 */
class TabletWarehouseBind
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="bind_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $bindAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="warehouse_product_id", type="integer", nullable=false)
     *
     * @var int WarehouseProduct
     */
    private $warehouseProductId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $userId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getBindAt()
    {
        return $this->bindAt;
    }

    /**
     * @param int $bindAt
     */
    public function setBindAt($bindAt)
    {
        $this->bindAt = $bindAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int WarehouseProduct
     */
    public function getWarehouseProductId()
    {
        return $this->warehouseProductId;
    }

    /**
     * @param int $warehouseProduct WarehouseProduct
     */
    public function setWarehouseProductId($warehouseProduct)
    {
        $this->warehouseProductId = $warehouseProduct;
    }

    /**
     * @return int AdminUser
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user AdminUser
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }
}
