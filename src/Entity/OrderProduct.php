<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\OrderProductRepository")
 * @ORM\Table(name="orders_products")
 */
class OrderProduct
{
    public const CALLCENTER_STATUS_UNMODERATED = 0;
    public const CALLCENTER_STATUS_CANCELLED = 1;
    public const CALLCENTER_STATUS_ACCEPTED = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="product_sizeformat", type="string", length=10, nullable=false)
     *
     * @var string
     */
    private $productSizeformat;

    /**
     * @ORM\Column(name="product_size", type="string", length=15, nullable=false)
     *
     * @var string
     */
    private $productSize;

    /**
     * @ORM\Column(name="product_stock_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $productStockId;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     *
     * @var int
     */
    private $eventId;

    /**
     * @ORM\Column(name="number", type="integer", nullable=false)
     *
     * @var int
     */
    private $number;

    /**
     * @ORM\Column(name="price", type="float", precision=8, scale=2, nullable=false)
     *
     * @var float
     */
    private $price;

    /**
     * @ORM\Column(name="is_added_by_callcenter", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isAddedByCallcenter;

    /**
     * @ORM\Column(name="callcenter_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $callcenterStatus;

    /**
     * @ORM\Column(name="callcenter_status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $callcenterStatusPrev;

    /**
     * @ORM\Column(name="callcenter_status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $callcenterStatusHistoryJson;

    /**
     * @ORM\Column(name="callcenter_comment", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $callcenterComment;

    /**
     * @ORM\Column(name="storekeeper_status", type="integer", nullable=false)
     *
     * @var int
     */
    private $storekeeperStatus;

    /**
     * @ORM\Column(name="storekeeper_status_prev", type="integer", nullable=false)
     *
     * @var int
     */
    private $storekeeperStatusPrev;

    /**
     * @ORM\Column(name="storekeeper_status_history_json", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $storekeeperStatusHistoryJson;

    /**
     * @ORM\Column(name="storekeeper_comment", type="string", length=1024, nullable=false)
     *
     * @var string
     */
    private $storekeeperComment;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @ORM\Column(name="relation_order_product_id", type="integer", nullable=false)
     *
     * @var int self
     */
    private $relationOrderProductId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     *
     * @var int User
     */
    private $userId;

    /**
     * @ORM\Column(name="catalog_product_id", type="integer", nullable=false)
     *
     * @var int Product
     */
    private $catalogProductId;

    /**
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     *
     * @var int EventProduct
     */
    private $productId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProductSizeformat()
    {
        return $this->productSizeformat;
    }

    /**
     * @param string $productSizeformat
     */
    public function setProductSizeformat($productSizeformat)
    {
        $this->productSizeformat = $productSizeformat;
    }

    /**
     * @return string
     */
    public function getProductSize()
    {
        return $this->productSize;
    }

    /**
     * @param string $productSize
     */
    public function setProductSize($productSize)
    {
        $this->productSize = $productSize;
    }

    /**
     * @return int
     */
    public function getProductStockId()
    {
        return $this->productStockId;
    }

    /**
     * @param int $productStockId
     */
    public function setProductStockId($productStockId)
    {
        $this->productStockId = $productStockId;
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isAddedByCallcenter()
    {
        return $this->isAddedByCallcenter;
    }

    /**
     * @param bool $isAddedByCallcenter
     */
    public function setIsAddedByCallcenter($isAddedByCallcenter)
    {
        $this->isAddedByCallcenter = $isAddedByCallcenter;
    }

    /**
     * @return int
     */
    public function getCallcenterStatus()
    {
        return $this->callcenterStatus;
    }

    /**
     * @param int $callcenterStatus
     */
    public function setCallcenterStatus($callcenterStatus)
    {
        $this->callcenterStatus = $callcenterStatus;
    }

    /**
     * @return int
     */
    public function getCallcenterStatusPrev()
    {
        return $this->callcenterStatusPrev;
    }

    /**
     * @param int $callcenterStatusPrev
     */
    public function setCallcenterStatusPrev($callcenterStatusPrev)
    {
        $this->callcenterStatusPrev = $callcenterStatusPrev;
    }

    /**
     * @return string
     */
    public function getCallcenterStatusHistoryJson()
    {
        return $this->callcenterStatusHistoryJson;
    }

    /**
     * @param string $callcenterStatusHistoryJson
     */
    public function setCallcenterStatusHistoryJson($callcenterStatusHistoryJson)
    {
        $this->callcenterStatusHistoryJson = $callcenterStatusHistoryJson;
    }

    /**
     * @return string
     */
    public function getCallcenterComment()
    {
        return $this->callcenterComment;
    }

    /**
     * @param string $callcenterComment
     */
    public function setCallcenterComment($callcenterComment)
    {
        $this->callcenterComment = $callcenterComment;
    }

    /**
     * @return int
     */
    public function getStorekeeperStatus()
    {
        return $this->storekeeperStatus;
    }

    /**
     * @param int $storekeeperStatus
     */
    public function setStorekeeperStatus($storekeeperStatus)
    {
        $this->storekeeperStatus = $storekeeperStatus;
    }

    /**
     * @return int
     */
    public function getStorekeeperStatusPrev()
    {
        return $this->storekeeperStatusPrev;
    }

    /**
     * @param int $storekeeperStatusPrev
     */
    public function setStorekeeperStatusPrev($storekeeperStatusPrev)
    {
        $this->storekeeperStatusPrev = $storekeeperStatusPrev;
    }

    /**
     * @return string
     */
    public function getStorekeeperStatusHistoryJson()
    {
        return $this->storekeeperStatusHistoryJson;
    }

    /**
     * @param string $storekeeperStatusHistoryJson
     */
    public function setStorekeeperStatusHistoryJson($storekeeperStatusHistoryJson)
    {
        $this->storekeeperStatusHistoryJson = $storekeeperStatusHistoryJson;
    }

    /**
     * @return string
     */
    public function getStorekeeperComment()
    {
        return $this->storekeeperComment;
    }

    /**
     * @param string $storekeeperComment
     */
    public function setStorekeeperComment($storekeeperComment)
    {
        $this->storekeeperComment = $storekeeperComment;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }

    /**
     * @return int OrderProduct
     */
    public function getRelationOrderProductId()
    {
        return $this->relationOrderProductId;
    }

    /**
     * @param int $relationOrderProduct OrderProduct
     */
    public function setRelationOrderProductId($relationOrderProduct)
    {
        $this->relationOrderProductId = $relationOrderProduct;
    }

    /**
     * @return int User
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $user User
     */
    public function setUserId($user)
    {
        $this->userId = $user;
    }

    /**
     * @return int Product
     */
    public function getCatalogProductId()
    {
        return $this->catalogProductId;
    }

    /**
     * @param int $catalogProduct Product
     */
    public function setCatalogProductId($catalogProduct)
    {
        $this->catalogProductId = $catalogProduct;
    }

    /**
     * @return int EventProduct
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $product EventProduct
     */
    public function setProductId($product)
    {
        $this->productId = $product;
    }

    /**
     * @return float
     */
    public function getPriceTotal(): float
    {
        return (float)$this->getPrice() * (int)$this->getNumber();
    }
}
