<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\EventRepository")
 * @ORM\Table(name="events")
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDeleted;

    /**
     * @ORM\Column(name="is_stock", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isStock;

    /**
     * @ORM\Column(name="is_virtual", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isVirtual;

    /**
     * @ORM\Column(name="is_drop_shipping", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isDropShipping;

    /**
     * @ORM\Column(name="is_charity", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isCharity;

    /**
     * @ORM\Column(name="can_prepay", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $canPrepay = true;

    /**
     * @ORM\Column(name="logo_fileid", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $logoFileid;

    /**
     * @ORM\Column(name="promo_fileid", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $promoFileid;

    /**
     * @ORM\Column(name="distribution_fileid", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $distributionFileid;

    /**
     * @ORM\Column(name="size_chart_fileid", type="string", length=60, nullable=false)
     *
     * @var string
     */
    private $sizeChartFileid;

    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $status;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     *
     * @var string|null
     */
    private $description;

    /**
     * @ORM\Column(name="description_short", type="string", nullable=true)
     *
     * @var string|null
     */
    private $descriptionShort;

    /**
     * @ORM\Column(name="description_mailing", type="string", nullable=false)
     *
     * @var string
     */
    private $descriptionMailing;

    /**
     * @ORM\Column(name="start_at", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $startAt;

    /**
     * @ORM\Column(name="end_at", type="integer", nullable=true)
     *
     * @var int|null
     */
    private $endAt;

    /**
     * @ORM\Column(name="mailing_start_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $mailingStartAt;

    /**
     * @ORM\Column(name="is_visible", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $isVisible;

    /**
     * @ORM\Column(name="supplier_confirmation", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $supplierConfirmation;

    /**
     * @ORM\Column(name="manager_confirmation", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $managerConfirmation;

    /**
     * @ORM\Column(name="supplier_notified", type="boolean", nullable=false)
     *
     * @var bool
     */
    private $supplierNotified;

    /**
     * @ORM\Column(name="priority", type="integer", nullable=false)
     *
     * @var int
     */
    private $priority;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     *
     * @var int Supplier
     */
    private $supplierId;

    /**
     * @ORM\Column(name="brand_manager_id", type="integer", nullable=false)
     *
     * @var int AdminUser
     */
    private $brandManagerId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return bool
     */
    public function isStock()
    {
        return $this->isStock;
    }

    /**
     * @param bool $isStock
     */
    public function setIsStock($isStock)
    {
        $this->isStock = $isStock;
    }

    /**
     * @return bool
     */
    public function isVirtual()
    {
        return $this->isVirtual;
    }

    /**
     * @param bool $isVirtual
     */
    public function setIsVirtual($isVirtual)
    {
        $this->isVirtual = $isVirtual;
    }

    /**
     * @return bool
     */
    public function isDropShipping()
    {
        return $this->isDropShipping;
    }

    /**
     * @param bool $isDropShipping
     */
    public function setIsDropShipping($isDropShipping)
    {
        $this->isDropShipping = $isDropShipping;
    }

    /**
     * @return bool
     */
    public function isCharity()
    {
        return $this->isCharity;
    }

    /**
     * @param bool $isCharity
     */
    public function setIsCharity($isCharity)
    {
        $this->isCharity = $isCharity;
    }

    /**
     * @return bool
     */
    public function canPrepay()
    {
        return $this->canPrepay;
    }

    /**
     * @param bool $canPrepay
     */
    public function setCanPrepay($canPrepay)
    {
        $this->canPrepay = $canPrepay;
    }

    /**
     * @return string
     */
    public function getLogoFileid()
    {
        return $this->logoFileid;
    }

    /**
     * @param string $logoFileid
     */
    public function setLogoFileid($logoFileid)
    {
        $this->logoFileid = $logoFileid;
    }

    /**
     * @return string
     */
    public function getPromoFileid()
    {
        return $this->promoFileid;
    }

    /**
     * @param string $promoFileid
     */
    public function setPromoFileid($promoFileid)
    {
        $this->promoFileid = $promoFileid;
    }

    /**
     * @return string
     */
    public function getDistributionFileid()
    {
        return $this->distributionFileid;
    }

    /**
     * @param string $distributionFileid
     */
    public function setDistributionFileid($distributionFileid)
    {
        $this->distributionFileid = $distributionFileid;
    }

    /**
     * @return string
     */
    public function getSizeChartFileid()
    {
        return $this->sizeChartFileid;
    }

    /**
     * @param string $sizeChartFileid
     */
    public function setSizeChartFileid($sizeChartFileid)
    {
        $this->sizeChartFileid = $sizeChartFileid;
    }

    /**
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getDescriptionShort()
    {
        return $this->descriptionShort;
    }

    /**
     * @param null|string $descriptionShort
     */
    public function setDescriptionShort($descriptionShort)
    {
        $this->descriptionShort = $descriptionShort;
    }

    /**
     * @return string
     */
    public function getDescriptionMailing()
    {
        return $this->descriptionMailing;
    }

    /**
     * @param string $descriptionMailing
     */
    public function setDescriptionMailing($descriptionMailing)
    {
        $this->descriptionMailing = $descriptionMailing;
    }

    /**
     * @return int|null
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param int|null $startAt
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;
    }

    /**
     * @return int|null
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param int|null $endAt
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;
    }

    /**
     * @return int
     */
    public function getMailingStartAt()
    {
        return $this->mailingStartAt;
    }

    /**
     * @param int $mailingStartAt
     */
    public function setMailingStartAt($mailingStartAt)
    {
        $this->mailingStartAt = $mailingStartAt;
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * @return bool
     */
    public function isSupplierConfirmation()
    {
        return $this->supplierConfirmation;
    }

    /**
     * @param bool $supplierConfirmation
     */
    public function setSupplierConfirmation($supplierConfirmation)
    {
        $this->supplierConfirmation = $supplierConfirmation;
    }

    /**
     * @return bool
     */
    public function isManagerConfirmation()
    {
        return $this->managerConfirmation;
    }

    /**
     * @param bool $managerConfirmation
     */
    public function setManagerConfirmation($managerConfirmation)
    {
        $this->managerConfirmation = $managerConfirmation;
    }

    /**
     * @return bool
     */
    public function isSupplierNotified()
    {
        return $this->supplierNotified;
    }

    /**
     * @param bool $supplierNotified
     */
    public function setSupplierNotified($supplierNotified)
    {
        $this->supplierNotified = $supplierNotified;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int Supplier
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplier Supplier
     */
    public function setSupplierId($supplier)
    {
        $this->supplierId = $supplier;
    }

    /**
     * @return int AdminUser
     */
    public function getBrandManagerId()
    {
        return $this->brandManagerId;
    }

    /**
     * @param int $brandManager AdminUser
     */
    public function setBrandManagerId($brandManager)
    {
        $this->brandManagerId = $brandManager;
    }
}
