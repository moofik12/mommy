<?php

namespace MommyCom\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MommyCom\Repository\CounterpartyDeliveryOrderRepository")
 * @ORM\Table(name="counterparties_delivery_orders")
 */
class CounterpartyDeliveryOrder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(name="amount", type="float", precision=8, scale=2, nullable=false)
     *
     * @var int
     */
    private $amount;

    /**
     * @ORM\Column(name="toPay", type="float", precision=8, scale=2, nullable=false)
     *
     * @var int
     */
    private $toPay;

    /**
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     *
     * @var int
     */
    private $createdAt;

    /**
     * @ORM\Column(name="counterparty_id", type="integer", nullable=false)
     *
     * @var int CounterpartyDelivery
     */
    private $counterpartyId;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     *
     * @var int Order
     */
    private $orderId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getToPay()
    {
        return $this->toPay;
    }

    /**
     * @param int $toPay
     */
    public function setToPay($toPay)
    {
        $this->toPay = $toPay;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int CounterpartyDelivery
     */
    public function getCounterpartyId()
    {
        return $this->counterpartyId;
    }

    /**
     * @param int $counterparty CounterpartyDelivery
     */
    public function setCounterpartyId($counterparty)
    {
        $this->counterpartyId = $counterparty;
    }

    /**
     * @return int Order
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $order Order
     */
    public function setOrderId($order)
    {
        $this->orderId = $order;
    }
}
