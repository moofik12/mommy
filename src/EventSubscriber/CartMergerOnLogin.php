<?php

namespace MommyCom\EventSubscriber;

use MommyCom\Model\Db\CartRecord;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\SecurityEvents;

class CartMergerOnLogin implements EventSubscriberInterface
{
    use ApplicationTrait;

    public function onSecurityInteractiveLogin(): void
    {
        $webUser = $this->app()->getUser();

        if (!($webUser instanceof ShopWebUser)) {
            return;
        }

        $userCart = $webUser->cart;

        /** @var CartRecord[] $carts */
        $carts = CartRecord::model()
            ->anonymousId($webUser->getAnonymousId())
            ->findAll();

        try {
            foreach ($carts as $cart) {
                $cart->delete();
            }
        } catch (\CDbException $e) {
            return;
        }

        foreach ($carts as $cart) {
            $userCart->put($cart->product, $cart->number);
        }
    }

    public static function getSubscribedEvents()
    {
        if (!isset($_SERVER['APP_NAME']) || 'backend' === $_SERVER['APP_NAME']) {
            return [];
        }

        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
        ];
    }
}
