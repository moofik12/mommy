<?php

namespace MommyCom\EventSubscriber;

use MommyCom\YiiComponent\Application\MommyWebApplication;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class YiiCookiesAdapter implements EventSubscriberInterface
{
    /**
     * @var MommyWebApplication
     */
    private $yiiApp;

    /**
     * YiiControllersBridgeSubscriber constructor.
     *
     * @param MommyWebApplication $yiiApp
     */
    public function __construct(MommyWebApplication $yiiApp)
    {
        $this->yiiApp = $yiiApp;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event): void
    {
        $symfonyRequest = $event->getRequest();
        $yiiRequest = $this->yiiApp->getRequest();

        /** @var \CHttpCookie $cookie */
        foreach ($yiiRequest->getCookies() as $cookie) {
            $symfonyRequest->cookies->set($cookie->name, $cookie->value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 96],
        ];
    }
}
