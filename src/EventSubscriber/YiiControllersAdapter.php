<?php

namespace MommyCom\EventSubscriber;

use MommyCom\Service\BaseController\Controller;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\Application\YiiEndException;
use MommyCom\YiiComponent\BufferedResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class YiiControllersAdapter implements EventSubscriberInterface
{
    /**
     * @var MommyWebApplication
     */
    private $yiiApp;

    /**
     * YiiControllersBridgeSubscriber constructor.
     *
     * @param MommyWebApplication $yiiApp
     */
    public function __construct(MommyWebApplication $yiiApp)
    {
        $this->yiiApp = $yiiApp;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->attributes->has('_controller')) {
            // routing is already done
            return;
        }

        $pathInfo = $request->getPathInfo();
        if (0 === strpos($pathInfo, '/_wdt') || 0 === strpos($pathInfo, '/_profiler')) {
            return;
        }

        try {
            $route = $this->yiiApp->getUrlManager()->parseUrl($this->yiiApp->getRequest());
        } catch (\CException $e) {
            return;
        }

        $ca = $this->yiiApp->createController($route);

        if (!$ca) {
            return;
        }

        /** @var Controller $controller */
        list($controller, $actionID) = $ca;

        if (!$actionID) {
            if (empty($controller->defaultAction)) {
                return;
            }

            $actionID = $controller->defaultAction;
        }

        if (!method_exists($controller, 'action' . $actionID) && !isset($controller->actions()[$actionID])) {
            return;
        }

        $this->yiiApp->setController($controller);

        $controller = function () use ($controller, $actionID) {
            if ($this->yiiApp->hasEventHandler('onBeginRequest')) {
                $this->yiiApp->onBeginRequest(new \CEvent($this->yiiApp));
            }

            try {
                $controller->init();
                $controller->run($actionID);
            } catch (YiiEndException $e) {
                // Do nothing
            }
        };

        $request->attributes->add([
            '_controller' => $controller,
            '_locale' => $matches['_locale'] ?? 'en',
            '_yii' => true,
        ]);

        ob_start();
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        if ($event->getControllerResult() || !$event->getRequest()->attributes->getBoolean('_yii')) {
            return;
        }

        $event->setResponse(new BufferedResponse());
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 64],
            KernelEvents::VIEW => 'onKernelView',
        ];
    }
}
