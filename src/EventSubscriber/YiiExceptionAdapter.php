<?php

namespace MommyCom\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class YiiExceptionAdapter implements EventSubscriberInterface
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof \CHttpException) {
            $event->setException($this->wrapException($exception));
        }
    }

    private function wrapException(\CHttpException $exception): HttpException
    {
        return new HttpException($exception->statusCode, $exception->getMessage(), $exception);
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', -64],
        ];
    }
}
