<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    {% if (!file.error) { %}
            <div class="pull-left current-image" style="padding-right: 5px">
                <a target="_blank" href="{%=file.url%}"><img class="img-polaroid" style="max-width: 150px; max-height: 150px;" src="{%=file.thumbnail_url%}" alt=""></a>
                <input value="{%=file.id%}" name="ProductForm[images][]" type="hidden">
                <label>
                    <input class="btn btn-small btn-danger delete-photo" name="yt0" type="button" value="Delete" data-id="{%=file.id%}">
                </label>
            </div>
    {% } %}
{% } %}
</script>
