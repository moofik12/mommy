<?php

namespace MommyCom\Widget\Backend;

use MommyCom\YiiComponent\Application\ApplicationTrait;

class LanguageSelector extends \CWidget
{
    use ApplicationTrait;

    public function run()
    {
        $currentLang = \Yii::app()->language;
        $languages = \Yii::app()->params->languages;
        $this->render('languageSelector', ['currentLang' => $currentLang, 'languages' => $languages]);
    }
}
