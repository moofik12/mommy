<?php

namespace MommyCom\Widget\Backend;

use CHtml;
use CMenu;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use Yii;

/**
 * Class LeftMenu
 *
 * @property string $countType
 */
class LeftMenu extends CMenu
{
    use ApplicationTrait;

    public $activateParents = true;
    public $cssClass = 'unstyled accordion collapse in';
    public $itemCssClass = 'accordion-group';

    public $submenuCssClass = '';
    public $submenuItemCssClass = '';
    public $submenuActiveCssClass = 'in';
    public $submenuFirstItemCssClass;
    public $submenuLastItemCssClass;

    public $checkAccess = false;

    const COUNT_TYPE_DEFAULT = '';
    const COUNT_TYPE_SUCCESS = 'badge-success';
    const COUNT_TYPE_WARNING = 'badge-warning';
    const COUNT_TYPE_IMPORTANT = 'badge-important';
    const COUNT_TYPE_INFO = 'badge-info';
    const COUNT_TYPE_INVERSE = 'badge-inverse';

    private $_countType = self::COUNT_TYPE_INFO;

    /**
     * @return mixed
     */
    public function getCountType()
    {
        return $this->_countType;
    }

    /**
     * @param mixed $countType
     */
    public function setCountType($countType)
    {
        if (self::_isValidateCountType($countType)) {
            $this->_countType = $countType;
        }
    }

    public function init()
    {
        if (isset($this->htmlOptions['class'])) {
            $this->htmlOptions['class'] .= ' ' . $this->cssClass;
        } else {
            $this->htmlOptions['class'] = $this->cssClass;
        }

        parent::init();
    }

    protected function renderMenuItem($item)
    {
        $label = $this->linkLabelWrapper === null ? $item['label'] : CHtml::tag($this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $item['label']);
        $options = isset($item['linkOptions']) ? $item['linkOptions'] : [];

        $childrenCount = 0;
        if (isset($item['items'])) {
            foreach ($item['items'] as $k) {
                if (!isset($k[0])) {
                    ++$childrenCount;
                }
            }
        }

        //$childrenCount = isset($item['items'])? count($item['items']): 0;
        if ($item['level'] == 0 && $childrenCount > 0) {
            $options += [
                'data-parent' => '#' . $this->getId(),
                'data-toggle' => 'collapse',
                'class' => 'accordion-toggle',
                'data-target' => "#menu-item-children-" . $this->getId() . '-' . $item['index'],
            ];
            $label .= ' ' . '<span class="label label-inverse pull-right">' . $childrenCount . '</span>';
        } elseif (!empty($item['count'])) {
            $count = $item['count'];
            $contType = $this->_countType;
            if (isset($item['countType']) && self::_isValidateCountType($item['countType'])) {
                $contType = $item['countType'];
            }
            $label .= " <span class=\"badge $contType pull-right\">$count</span>";
        }

        /**
         * <a data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed">
         * <i class="icon-pencil icon-large"></i> Forms <span class="label label-inverse pull-right">4</span>
         * </a>
         */

        if (isset($item['url'])) {
            return CHtml::link($label, $item['url'], $options);
        } else {
            return CHtml::link($label, 'javascript:void(0);', $options);
        }
    }

    public function normalizeItems($items, $route, &$active)
    {
        if ($this->checkAccess) {
            foreach ($items as $i => $item) {
                if (isset($item['url'])) {
                    if (is_array($item['url'])) {
                        $user = Yii::app()->user;
                        if (!$user->checkAccess($item['url'][0])) {
                            unset($items[$i]);
                            continue;
                        }
                    }
                } elseif ($item == ['---'] && !isset($items[$i - 1])) {
                    unset($items[$i]);
                }
            }
        }
        return parent::normalizeItems($items, $route, $active);
    }

    /**
     * @param $type
     *
     * @return bool
     */
    protected static function _isValidateCountType($type)
    {
        static $types = [
            self::COUNT_TYPE_DEFAULT,
            self::COUNT_TYPE_SUCCESS,
            self::COUNT_TYPE_WARNING,
            self::COUNT_TYPE_IMPORTANT,
            self::COUNT_TYPE_INFO,
            self::COUNT_TYPE_INVERSE,
        ];

        return in_array($type, $types);
    }

    protected function renderMenuRecursive($items, $level = 0)
    {
        $count = 0;
        $n = count($items);
        foreach ($items as $i => $item) {
            $count++;

            $item['level'] = $level;
            $item['index'] = $count;

            $options = isset($item['itemOptions']) ? $item['itemOptions'] : [];
            $class = [];

            if ($level == 0) {
                if ($item['active'] && $this->activeCssClass != '') {
                    $class[] = $this->activeCssClass;
                }

                if ($count === 1 && $this->firstItemCssClass !== null) {
                    $class[] = $this->firstItemCssClass;
                }

                if ($count === $n && $this->lastItemCssClass !== null) {
                    $class[] = $this->lastItemCssClass;
                }

                if ($this->itemCssClass !== null) {
                    $class[] = $this->itemCssClass;
                }
            } else {
                if ($item['active'] && $this->submenuActiveCssClass != '') {
                    $class[] = $this->submenuActiveCssClass;
                }

                if ($count === 1 && $this->submenuFirstItemCssClass !== null) {
                    $class[] = $this->submenuFirstItemCssClass;
                }

                if ($count === $n && $this->submenuLastItemCssClass !== null) {
                    $class[] = $this->submenuLastItemCssClass;
                }

                if ($this->submenuItemCssClass !== null) {
                    $class[] = $this->submenuItemCssClass;
                }
            }

            if ($class !== []) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            echo CHtml::openTag('li', $options);

            $menu = $this->renderMenuItem($item);
            if (isset($this->itemTemplate) || isset($item['template'])) {
                $template = isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template, ['{menu}' => $menu]);
            } else {
                echo $menu;
            }

            if (isset($item['items']) && count($item['items']) > 0) {
                $submenuOptions = isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions;
                $submenuOptions['id'] = 'menu-item-children-' . $this->getId() . '-' . $item['index'];
                if (isset($submenuOptions['class'])) {
                    $submenuOptions['class'] .= ' ' . $this->submenuCssClass;
                } else {
                    $submenuOptions['class'] = $this->submenuCssClass;
                }
                if (!$item['active']) {
                    $submenuOptions['class'] .= ' ' . 'collapse';
                }

                echo "\n" . CHtml::openTag('ul', $submenuOptions) . "\n";
                $this->renderMenuRecursive($item['items'], $level + 1);
                echo CHtml::closeTag('ul') . "\n";
            }

            echo CHtml::closeTag('li') . "\n";
        }
    }
}
