<?php

use MommyCom\YiiComponent\Facade\RegionalTranslator;

$translator = RegionalTranslator::get();

return [
    'showErrorSummary' => true,
    'elements' => [
        'full_name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Enter new password'),
        ],
        'address' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Name of company'),
        ],
        'registration_date' => [
            'type' => 'date',
        ],
        'chief_name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Director'),
        ],
        'chief_passport' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('KTP'),
        ],
        'license' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('SIUP'),
        ],
        'npwp' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('NPWP'),
        ],
        'tdp' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('TDP'),
        ],
        'bank_name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Bank'),
        ],
        'bank_address' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Bank address'),
        ],
        'email' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('E-mail'),
        ],
        'phone' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Tel.'),
        ],
        'bank_account' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Account #'),
        ],
        'messenger' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Messenger'),
        ],
        'certifiable_sections' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => $translator->t('Category of goods'),
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => $translator->t('Save'),
        ],
    ],
];