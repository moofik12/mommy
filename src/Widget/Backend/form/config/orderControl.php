<?php

use MommyCom\Model\Db\OrderControlRecord;
use MommyCom\YiiComponent\Facade\Translator;

return array(
    'showErrorSummary' => true,
    'elements' => array(
        'operatorStatus' => array(
            'type' => 'dropdownlist',
            'items' => OrderControlRecord::operatorStatusReplacements()
        ),
        'comment' => array(
            'type' => 'textarea',
            'class' => 'span5'
        ),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ),
        'list' => array(
            'type' => 'link',
            'label' => Translator::t('Back to the list'),
            'url' => Yii::app()->controller->createUrl('index'),
        ),
    ),
);
