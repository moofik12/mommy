<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'supplier_id' => [
            'type' => 'text',
        ],
        'user_id' => [
            'type' => 'text',
        ],
        'status' => [
            'type' => 'text',
        ],
        'notify_text' => [
            'type' => 'text',
            'class' => 'span-2',
        ],
        'is_need_notify' => [
            'type' => 'checkbox',
        ],
        'is_accounted_returns' => [
            'type' => 'checkbox',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
