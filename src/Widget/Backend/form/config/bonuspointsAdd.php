<?php

use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'user_id' => [
            'type' => 'select2',
            'asDropDownList' => false,
            'options' => [
                'allowClear' => true,
                'width' => 'resolve',
                'ajax' => [
                    'url' => Yii::app()->createUrl('user/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            like: term,
                            page: page
                        };
                    }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: "<strong>" + value.email + "</strong>"
                                    + " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>",
                            });
                        });

                        return result;
                    }'),
                ],
            ],
        ],
        'type' => [
            'type' => 'dropdownlist',
            'items' => UserBonuspointsRecord::getTypes(),
        ],
        'custom_message' => [
            'type' => 'text',
            'maxlength' => 250,
        ],
        'points' => [
            'type' => 'text',
            'maxlength' => 5,
        ],
        'expireAtString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
            'options' => [
                'startDate' => date('d.m.Y'),
            ],
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
