<?php

use MommyCom\Model\Db\UserBlackListRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

$isNewRecord = $this->model instanceof CActiveRecord && $this->model->isNewRecord;

return [
    'showErrorSummary' => false,
    'elements' => [
        'profile' => [
            'type' => 'form',
            'title' => Translator::t('Profile'),
            'elements' => [
                'status' => [
                    'type' => 'dropdownlist',
                    'items' => UserBlackListRecord::statuses(),
                ],
                'user_id' => [
                    'type' => 'select2',
                    'disabled' => !$isNewRecord,

                    'asDropDownList' => false,
                    'prompt' => Translator::t('Select user'),
                    'options' => [
                        'width' => 'resolve',
                        'initSelection' => new CJavaScriptExpression('function (element, callback) {
                            var value = parseInt(element.val()) || 0;
                            if (value <= 0) {
                                return;
                            }
                            $.ajax({
                                url: "' . Yii::app()->createUrl('user/ajaxGetById') . '",
                                method: "GET",
                                data: {
                                    id: value
                                },
                                success: function(result) {
                                    var data = {
                                        id: result.id,
                                        text: result.name + " " + result.surname + "<strong> (" + result.email + ")</strong>"
                                    };
                                    callback(data);
                                },
                                error: function() {
                                    alert("' . Translator::t('User not found') . '");
                                }
                            });
                        }'),
                        'ajax' => [
                            'url' => Yii::app()->createUrl('user/ajaxSearch'),
                            'type' => 'GET',
                            'dataType' => 'json',
                            'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
                            'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    result.results.push({
                                        id: value.id,
                                        text: value.name + " " + value.surname + "<strong> (" + value.email + ")</strong>"
                                    });
                                });

                                return result;
                            }'),
                        ],
                    ],
                ],
                'comment' => [
                    'type' => 'textarea',
                    'items' => UserBlackListRecord::statuses(),
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
                'reset' => [
                    'type' => 'reset',
                    'label' => Translator::t('Cancel'),
                ],
            ],
        ],
    ],

];
