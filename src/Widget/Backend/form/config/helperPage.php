<?php

use MommyCom\Model\Db\HelperPageCategoryRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'category_id' => [
            'type' => 'dropdownlist',
            'items' => CHtml::listData(HelperPageCategoryRecord::model()->findAll(), 'id', 'name'),
            'prompt' => Translator::t('Select category'),
        ],
        'title' => [
            'type' => 'text',
            'placeholder' => Translator::t('Mail-out'),
            'class' => 'span10',
        ],
        'body' => [
            'type' => 'redactor',
            'options' => [
                'convertDivs' => false,
                'formattingTags' => ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
                'imageUpload' => Yii::app()->controller->createUrl('fileManager/redactorImageUpload'),
                'imageGetJson' => Yii::app()->controller->createUrl('fileManager/redactorImageGetJson'),
                'uploadFields' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                ],
            ],
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
