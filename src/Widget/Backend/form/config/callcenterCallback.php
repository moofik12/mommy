<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'timeString' => [
            'type' => 'time',
            'defaultTime' => 'current',
            'value' => date('H:i'),
        ],
        'callcenterComment' => [
            'type' => 'textarea',
            'maxlength' => 255,
            'rows' => 10,
            'class' => 'row-fluid',
        ],
    ],
    'buttons' => [
        'processed' => [
            'type' => 'submit',
            'layoutType' => 'success',
            'label' => Translator::t('Processed'),
        ],
        'modal' => [
            'type' => 'button',
            'layoutType' => 'info',
            'label' => Translator::t('Repeat in...'),
            'htmlOptions' => [
                'data-toggle' => 'modal',
                'data-target' => '#recall-time-set',
            ],
        ],
        'canceled' => [
            'type' => 'submit',
            'layoutType' => 'danger',
            'label' => Translator::t('Cancel'),
        ],
        'postponed' => [
            'type' => 'submit',
            'layoutType' => 'info',
            'label' => Translator::t('Defer'),
        ],

    ],
];
