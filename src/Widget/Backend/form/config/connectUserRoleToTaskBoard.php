<?php

use MommyCom\Model\Db\AdminRoleRecord;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Facade\Translator;

$list = CHtml::listData(AdminRoleRecord::getRolesList(), 'name', 'name');
return [
    'showErrorSummary' => true,
    'elements' => [
        'task_board_id' => [
            'type' => 'dropdownlist',
            'items' => TaskBoardRecord::getQuestionTypes(),
            'prompt' => Translator::t('Select board'),
            'class' => 'input-xxlarge',
        ],
        'user_role' => [
            'type' => 'dropdownlist',
            'items' => $list,
            'prompt' => Translator::t('Select your role'),
            'class' => 'users-drop-down input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
