<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => Translator::t('Enter brand name'),
        ],
        'aliasesStrings' => [
            'type' => 'select2',
            'asDropDownList' => false,
            'options' => [
                'width' => 'resolve',
                'tags' => '',
                'tokenSeparators' => [","],
            ],
        ],
        'description' => [
            'type' => 'bootstrap.widgets.TbRedactorJs',
            'editorOptions' => [
                'buttons' => ['bold', 'italic', '|', 'unorderedlist', 'orderedlist', '|', 'html'],
            ],
        ],
        'description_short' => [
            'type' => 'textarea',
            'class' => 'span4',
        ],
        'logo' => [
            'type' => 'file',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
