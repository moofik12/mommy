<?php

use MommyCom\YiiComponent\Facade\Translator;

$model = $this->getModel();
return [
    'showErrorSummary' => true,
    'elements' => [
        'question_description' => [
            'type' => 'redactor',
            'options' => [
                'convertDivs' => false,
                'formattingTags' => ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
                'imageUpload' => Yii::app()->controller->createUrl('fileManager/redactorImageUpload'),
                'imageGetJson' => Yii::app()->controller->createUrl('fileManager/redactorImageGetJson'),
                'uploadFields' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                ],
            ],
        ],
        'url' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
