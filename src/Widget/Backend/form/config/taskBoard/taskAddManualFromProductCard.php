<?php

use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'task_board_type' => [
            'type' => 'dropdownlist',
            'items' => TaskBoardRecord::getQuestionTypes(),
            'prompt' => Translator::t('Select board'),
            'class' => 'task-board-drop-down',
            'ajax' => [
                'type' => 'GET',
                'dataType' => 'html',
                'url' => Yii::app()->createUrl('taskBoard/users'),
                'success' => 'function(data) {
                    $(".users-drop-down").find("option").remove();
                    $(".users-drop-down").append(data);
                    $(".users-drop-down").removeAttr("disabled");
                }',
                'error' => 'function(data) {
                    $(".users-drop-down").find("option").remove();
                    $(".users-drop-down").append("<option>' . Translator::t('First, select the board') . '</option>");
                    $(".users-drop-down").attr("disabled","disabled");


                }',
                'data' => ['id' => 'js:this.value'],
            ],
        ],
        'user_id' => [
            'type' => 'dropdownlist',
            'items' => [],
            'prompt' => Translator::t('First, select the board'),
            'disabled' => 'disabled',
            'class' => 'users-drop-down',
        ],
        'question_description' => [
            'type' => 'textarea',
            'class' => 'task-order-card-question-description',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'ajaxSubmit',
            'layoutType' => 'primary',
            'label' => Translator::t('Create task'),
            'url' => Yii::app()->createUrl('taskBoard/addManualTaskFromOrderCard'),
            'ajaxOptions' => [
                'dataType' => 'json',
                'type' => 'POST',
                'success' => 'function(data) {

                    if (typeof(data.TaskBoardRecord_user_id) == "undefined" && typeof(data.TaskBoardRecord_question_description) == "undefined" && typeof(data.TaskBoardRecord_task_board_type) == "undefined") {
                         window.location=data.redirect
                    }
                    if (data.TaskBoardRecord_question_description !== undefined) {
                        $(".task-order-card-question-description").css("border-color","red");
                    } else {
                        $(".task-order-card-question-description").css("border","1px solid #cccccc");
                    }

                    if (data.TaskBoardRecord_user_id !== undefined) {
                         $(".users-drop-down").css("border-color","red");
                    } else {
                        $(".users-drop-down").css("border","1px solid #cccccc");
                    }

                    if (data.TaskBoardRecord_task_board_type !== undefined) {
                         $(".task-board-drop-down").css("border-color","red");
                    } else {
                        $(".task-board-drop-down").css("border","1px solid #cccccc");
                    }
                }',
            ],
        ],
    ],
];
