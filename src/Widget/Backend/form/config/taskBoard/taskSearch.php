<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'id' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Search'),
        ],
    ],
];
