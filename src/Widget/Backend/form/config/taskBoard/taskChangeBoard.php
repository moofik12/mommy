<?php

use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'task_board_type' => [
            'type' => 'dropdownlist',
            'items' => TaskBoardRecord::getQuestionTypes(),
            'prompt' => Translator::t('Select board'),
        ],
        'id' => [
            'type' => 'text',
            'class' => 'hidden',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'ajaxSubmit',
            'layoutType' => 'primary',
            'label' => Translator::t('Link task'),
            'url' => Yii::app()->createUrl('taskBoard/change'),
            'ajaxOptions' => [
                'dataType' => 'json',
                'type' => 'GET',
                'success' => 'function(data) {
                    if (data.error !== undefined) {
                        $("#TaskBoardRecord_task_board_type").css("border-color","red");
                    } else {
                        $("#TaskBoardRecord_task_board_type").css("border","1px solid #cccccc");
                        window.location = data.redirect
                    }
                }',
            ],
        ],
    ],
];
