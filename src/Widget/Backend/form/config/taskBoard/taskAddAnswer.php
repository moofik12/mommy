<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'response' => [
            'type' => 'redactor',
            'options' => [
                'convertDivs' => false,
                'formattingTags' => ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
                'imageUpload' => Yii::app()->controller->createUrl('fileManager/redactorImageUpload'),
                'imageGetJson' => Yii::app()->controller->createUrl('fileManager/redactorImageGetJson'),
                'uploadFields' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                ],
            ],
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Add reply'),
        ],
    ],
];
