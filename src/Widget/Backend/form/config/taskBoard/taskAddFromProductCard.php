<?php

use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'question_type' => [
            'type' => 'dropdownlist',
            'items' => TaskBoardRecord::getQuestionTypes(),
            'prompt' => Translator::t('Question type'),
            'class' => 'task-order-card-question-type',
        ],
        'question_description' => [
            'type' => 'textarea',
            'class' => 'task-order-card-question-description',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'ajaxSubmit',
            'layoutType' => 'primary',
            'label' => Translator::t('Create task'),
            'url' => Yii::app()->createUrl('taskBoard/addTaskFromOrderCard'),
            'ajaxOptions' => [
                'dataType' => 'json',
                'type' => 'POST',
                'success' => 'function(data) {

                    if (data.error !== undefined) {
                        alert(data.error);
                    } else {
                        if (typeof(data.TaskBoardRecord_question_type) == "undefined" && typeof(data.TaskBoardRecord_question_description) == "undefined") {
                         window.location=data.redirect
                    }
                    if (data.TaskBoardRecord_question_description !== undefined) {
                        $(".task-order-card-question-description").css("border-color","red");
                    } else {
                        $(".task-order-card-question-description").css("border","1px solid #cccccc");
                    }

                    if (data.TaskBoardRecord_question_type !== undefined) {
                         $(".task-order-card-question-type").css("border-color","red");
                    } else {
                        $(".task-order-card-question-type").css("border","1px solid #cccccc");
                    }
                    }

                }',
            ],
        ],
    ],
];
