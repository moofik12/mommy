<?php

use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Facade\Translator;

$dropdown = [$_SESSION['manual_task'] => TaskBoardRecord::getQuestionTypes($_SESSION['manual_task'])];
return [
    'showErrorSummary' => true,
    'elements' => [
        'task_board_type' => [
            'type' => 'dropdownlist',
            'items' => $dropdown,
            'prompt' => Translator::t('Select board'),
            'class' => 'input-xxlarge',
            'ajax' => [
                'type' => 'GET',
                'dataType' => 'html',
                'url' => Yii::app()->createUrl('taskBoard/users'),
                'success' => 'function(data) {
                    $(".users-drop-down").find("option").remove();
                    $(".users-drop-down").append(data);
                    $(".users-drop-down").removeAttr("disabled");
                }',
                'error' => 'function(data) {
                    $(".users-drop-down").find("option").remove();
                    $(".users-drop-down").append("<option>' . Translator::t('First, select the board') . '</option>");
                    $(".users-drop-down").attr("disabled","disabled");


                }',
                'data' => ['id' => 'js:this.value'],
            ],
        ],
        'user_id' => [
            'type' => 'dropdownlist',
            'items' => [],
            'prompt' => Translator::t('First, select the board'),
            'disabled' => 'disabled',
            'class' => 'users-drop-down input-xxlarge',
        ],
        'question_description' => [
            'type' => 'redactor',
            'options' => [
                'convertDivs' => false,
                'formattingTags' => ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
                'imageUpload' => Yii::app()->controller->createUrl('fileManager/redactorImageUpload'),
                'imageGetJson' => Yii::app()->controller->createUrl('fileManager/redactorImageGetJson'),
                'uploadFields' => [
                    Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                ],
            ],
        ],
        'url' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
