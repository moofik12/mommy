<?php

use MommyCom\Model\Product\SizeFormats;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'max_per_buy' => [
            'type' => 'text',
        ],
        'price_purchase' => [
            'type' => 'text',
        ],
        'price' => [
            'type' => 'text',
        ],
        'price_market' => [
            'type' => 'text',
        ],
        'number' => [
            'type' => 'text',
        ],
        'sizeformat' => [
            'type' => 'dropdownlist',
            'items' => SizeFormats::instance()->getLabels(),
        ],
        'size' => [
            'type' => 'text',
        ],
        'weight' => [
            'type' => 'text',
        ],
        'dimensions' => [
            'type' => 'text',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
