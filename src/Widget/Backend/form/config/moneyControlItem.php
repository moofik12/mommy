<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'id' => 'moneyControlItem',
    'action' => ['validateItem'],
    'elements' => [
        'deliveryType' => [
            'type' => 'hidden',
        ],
        'trackcode' => [
            'class' => 'input-xxlarge',
            'placeholder' => Translator::t('Tracking number'),
            'autofocus' => 'autofocus',
            'autocomplete' => 'off',
        ],
        'order' => [
            'class' => 'input-xxlarge',
            'placeholder' => Translator::t('Order'),
            'autocomplete' => 'off',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Add'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Clear'),
        ],
    ],
];
