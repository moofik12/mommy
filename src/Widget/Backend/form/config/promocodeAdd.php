<?php

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'reason' => [
            'type' => 'dropdownlist',
            'items' => PromocodeRecord::reasonReplacements(),
        ],
        'reason_description' => [
            'type' => 'textarea',
        ],
        'type' => [
            'type' => 'dropdownlist',
            'items' => PromocodeRecord::typeReplacements(),
        ],
        'user_id' => [
            'type' => 'select2',

            'asDropDownList' => false,
            'prompt' => Translator::t('Select user'),
            'hint' => Translator::t('Only for type "Personal"'),
            'options' => [
                'width' => 'resolve',
                'initSelection' => new CJavaScriptExpression('function (element, callback) {
                    var value = parseInt(element.val()) || 0;
                    if (value <= 0) {
                        return;
                    }
                    $.ajax({
                        url: "' . Yii::app()->createUrl('user/ajaxGetById') . '",
                        method: "GET",
                        data: {
                            id: value
                        },
                        success: function(result) {
                            var data = {
                                id: result.id,
                                text: result.name + " " + result.surname
                            };
                            callback(data);
                        },
                        error: function() {
                            alert("User not found");
                        }
                    });
                }'),
                'ajax' => [
                    'url' => Yii::app()->createUrl('user/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            like: term,
                            page: page
                        };
                    }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.name + " " + value.surname
                            });
                        });

                        return result;
                    }'),
                ],
            ],
        ],

        'cash' => [
            'type' => 'text',
        ],

        'percent' => [
            'type' => 'text',
        ],

        'order_price_after' => [
            'type' => 'text',
        ],
        'partner_id' => [
            'type' => 'select2',

            'asDropDownList' => false,
            'prompt' => Translator::t('Select user'),
            'hint' => Translator::t('Only for a reason "Partner"'),
            'options' => [
                'width' => 'resolve',
                'initSelection' => new CJavaScriptExpression('function (element, callback) {
                    var value = parseInt(element.val()) || 0;
                    if (value <= 0) {
                        return;
                    }
                    $.ajax({
                        url: "' . Yii::app()->createUrl('userPartner/ajaxGetById') . '",
                        method: "GET",
                        data: {
                            id: value
                        },
                        success: function(result) {
                            var data = {
                                id: result.id,
                                text: result.id  + ", " + result.name + " " + result.surname
                            };
                            callback(data);
                        },
                        error: function() {
                            alert("User not found");
                        }
                    });
                }'),
                'ajax' => [
                    'url' => Yii::app()->createUrl('userPartner/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            like: term,
                            page: page
                        };
                    }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.id  + ", " + value.name + " " + value.surname
                            });
                        });

                        return result;
                    }'),
                ],
            ],
        ],

        'validUntilDateString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
            'options' => [
                'startDate' => strftime('%d.%m.%Y', strtotime('tomorrow')),
            ],
        ],

        'eventIdsStrings' => [
            'type' => 'text',
        ],
        'eventProductIdsStrings' => [
            'type' => 'text',
        ],
        'productIdsStrings' => [
            'type' => 'text',
        ],
        'generateShortPromocode' => [
            'type' => 'checkbox',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
