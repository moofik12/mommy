<?php

use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 * @var $model UserRecord
 */

$region = isset($this->getModel()->region) ? $this->getModel()->region->name : "";
$city = isset($this->getModel()->city) ? $this->getModel()->city->name : "";

return [
    'showErrorSummary' => false,
    'elements' => [
        'profile' => [
            'type' => 'form',
            'title' => Translator::t('Profile'),
            'elements' => [
                'status' => [
                    'type' => 'dropdownlist',
                    'items' => $this->getModel()->statusReplacements(),
                ],
                'category' => [
                    'type' => 'dropdownlist',
                    'items' => $this->getModel()->categoryReplacements(),
                    'prompt' => '',
                    'required' => false,
                ],
                'name' => [
                    'placeholder' => Translator::t('Name'),
                ],
                'surname' => [
                    'placeholder' => Translator::t('Last name'),
                ],
                'email' => [
                    'type' => 'email',
                    'placeholder' => 'Email',
                ],
                'telephone' => [
                    'type' => 'text',
                    'placeholder' => Translator::t('Phone number'),
                ],
                'address' => [
                    'type' => 'textarea',
                    'class' => 'input-xxlarge',
                    'placeholder' => Translator::t('Address'),
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
                'reset' => [
                    'type' => 'reset',
                    'label' => Translator::t('Cancel'),
                ],
            ],
        ],
    ],

];
