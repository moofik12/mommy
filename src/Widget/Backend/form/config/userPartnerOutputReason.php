<?php

use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

return [
    'showErrorSummary' => true,
    'elements' => [
        'id' => [
            'type' => 'hidden',
            'id' => 'reason-id',
        ],
        'reason' => [
            'type' => 'textarea',
            'style' => 'padding:0; max-width: 520px; width: 520px;',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'ajaxSubmit',
            'layoutType' => 'primary',
            'label' => Translator::t('Accept payment'),
            'id' => 'reason-submit',
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
