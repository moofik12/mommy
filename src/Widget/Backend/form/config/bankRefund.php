<?php

use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'type_refund' => [
            'type' => 'dropdownlist',
            'items' => OrderBankRefundRecord::typeRefundReplacements(),
        ],
        'order' => [
            'disabled' => 'disabled',
            'class' => 'input-xxlarge',
        ],
        'amount' => [
            'disabled' => 'disabled',
            'class' => 'input-xxlarge',
        ],
        'order_card_payed' => [
            'disabled' => 'disabled',
            'class' => 'input-xxlarge',
        ],
        'transaction' => [
            'class' => 'input-xxlarge',
        ],
        'comment' => [
            'type' => 'textarea',
            'class' => 'input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
        'list' => [
            'type' => 'link',
            'label' => Translator::t('Back to the list'),
            'url' => Yii::app()->controller->createUrl('orderPay/bankRefund'),
        ],
    ],
];
