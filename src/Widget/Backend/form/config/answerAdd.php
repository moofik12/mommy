<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        /*'fromEmail' => array(
            'type' => 'email',
        ),*/
        'answerMessage' => [
            'type' => 'textarea',
            'style' => 'padding:0; max-width: 520px; width: 520px; max-height: 220px; height: 220px;',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Send'),
        ],
    ],
];
