<?php

use MommyCom\Model\Backend\SupplierPaymentAdditionForm;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

/* @var SupplierPaymentAdditionForm $model */
$model = $this->getModel();
$df = Yii::app()->dateFormatter;
$cn = Yii::app()->countries->getCurrency();

$dataRaw = WarehouseStatementReturnRecord::model()
    ->cache(30)
    ->isAccountedReturns(false)
    ->supplier($model->getRecord()->supplier_id)
    ->supplierPaymentIn([0, $model->getRecord()->id])
    ->statusIn([
        WarehouseStatementReturnRecord::STATUS_CONFIRMED,
    ])
    ->getSqlCommand()->queryAll();

$data = [];
foreach ($dataRaw as $recordSR) {
    $data[$recordSR['id']] = '№' . $recordSR['id'] . ", date: " . $df->formatDateTime($recordSR['created_at'], 'short', false);
}
$additionElements = [];
if ($data) {
    $additionElements['warehouseStatementReturnsID'] = [
        'type' => 'select2',
        'multiple' => 'multiple',
        'asDropDownList' => true,
        'data' => $data,
        'value' => $model->getWarehouseStatementReturnsIDAsString(),
    ];
}

$unpaidRecords = SupplierPaymentRecord::model()->getUnpaidPaymentPossibleConditions($model->getRecord()->supplier_id, $model->getRecord()->id)->findAll();
if ($unpaidRecords) {
    $dataUnpaid = [];
    foreach ($unpaidRecords as $record) {
        $dataUnpaid[$record->id] = '№' . $record->id . ", date ("
            . $df->formatDateTime($record->payment_at, 'short', false) . ')  ' . $record->unpaid_amount . ' ' . $cn->getName('short');
    }
    $additionElements['unpaidPaymentsID'] = [
        'type' => 'select2',
        'multiple' => 'multiple',
        'asDropDownList' => true,
        'data' => $dataUnpaid,
        'value' => $model->getUnpaidPaymentsIDAsString(),
    ];
}

return [
    'showErrorSummary' => true,
    'elements' => [
            'custom_penalty_amount' => [
                'type' => 'text',
            ],
        ] + $additionElements,
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
