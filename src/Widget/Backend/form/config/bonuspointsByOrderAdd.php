<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'orderId' => [
            'type' => 'text',
            'maxlength' => 20,
            'name' => Translator::t('Order'),
        ],
        'customMessage' => [
            'type' => 'text',
            'maxlength' => 250,
            'class' => 'span6',
        ],
        'points' => [
            'type' => 'text',
            'maxlength' => 5,
        ],
        'expireAtString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
            'options' => [
                'startDate' => date('d.m.Y'),
            ],
        ],

    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
