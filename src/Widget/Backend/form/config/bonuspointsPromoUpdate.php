<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'points' => [
            'type' => 'text',
            'maxlength' => 5,
        ],
        'user_message' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'input-xxlarge',
        ],
        'is_stopped' => [
            'type' => 'dropdownlist',
            'items' => Yii::app()->format->booleanFormat,
        ],
        'bonusAvailableDays' => [
            'type' => 'text',
            'maxlength' => 3,
        ],
        'startAsString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
            'options' => [
                'startDate' => date('d.m.Y'),
            ],
        ],
        'endAsString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
            'options' => [
                'startDate' => date('d.m.Y'),
            ],
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
