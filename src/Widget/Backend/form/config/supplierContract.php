<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => Translator::t('Contract title'),
            'class' => 'input-xxlarge',
        ],
        'agreement' => [
            'type' => 'dropdownlist',
            'items'=> ['Marketplace' => 'Marketplace', 'Purchase and Sale Agreement' => 'Purchase and Sale Agreement'],
        ],
        'not_delivered_penalty_percent' => [
            'type' => 'text',
            'placeholder' => '%',
        ],
        'delivery_working_days' => [
            'type' => 'number',
            'placeholder' => Translator::t('Number of working days'),
        ],
        'add_delivery_working_days' => [
            'type' => 'number',
            'placeholder' => Translator::t('Number of working days'),
        ],
        'paid_by' => [
            'type' => 'dropdownlist',
            'items'=> ['Mommy', 'Supplier'],
        ],
        'days_to_pay' => [
            'type' => 'number',
        ],
        'is_default' => [
            'type' => 'checkbox',
            'placeholder' => Translator::t('Default'),
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
