<?php

use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

return [
    'showErrorSummary' => false,
    'action' => ['distributionPush/create'],
    'elements' => [
        'subject' => [
            'type' => 'text',
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
        'body' => [
            'type' => 'textarea',
            'rows' => 4,
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
        'start_at' => [
            'type' => 'date',
            'options' => [
                'format' => 'dd.mm.yyyy',
                'startDate' => date('d.m.Y', time()),
            ],
        ],
        'start_time' => [
            'type' => 'time',
            'options' => [
                'defaultTime' => '09:00',
            ],
            'autocomplete' => 'off',
        ],
        'url' => [
            'type' => 'text',
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
        'url_desktop' => [
            'type' => 'text',
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Create'),
        ],
        'list' => [
            'type' => 'link',
            'label' => Translator::t('Back to the list'),
            'url' => Yii::app()->controller->createUrl('distributionPush/index'),
        ],
    ],

];
