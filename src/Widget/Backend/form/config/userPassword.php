<?php

use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 * @var $model \MommyCom\Model\Form\NewPasswordForm
 */

return [
    'showErrorSummary' => false,
    'elements' => [
        'password' => [
            'type' => 'form',
            'title' => Translator::t('Change password'),
            'elements' => [
                'password' => [
                    'type' => 'text',
                ],
            ],
            'buttons' => [
                'submit' => [
                    'type' => 'submit',
                    'layoutType' => 'primary',
                    'label' => Translator::t('Save'),
                ],
                'reset' => [
                    'type' => 'reset',
                    'label' => Translator::t('Cancel'),
                ],
            ],
        ],
    ],
];
