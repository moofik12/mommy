<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [],
    'buttons' => [
        'confirm' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Confirm'),
            'htmlOptions' => [
                'class' => 'btn btn-success btn-large pull-left',
            ],
        ],
        'cancel' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Cancel'),
            'htmlOptions' => [
                'class' => 'btn btn-warning btn-large pull-right',
            ],
        ],
        '<div class="clearfix"></div>',
    ],
];
