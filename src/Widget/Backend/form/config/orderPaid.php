<?php

use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'provider' => [
            'class' => 'input-xxlarge',
            'type' => 'dropdownlist',
            'items' => PayGatewayRecord::getAvailableProvidersToPay(true),
        ],
        'unique_payment' => [
            'class' => 'input-xxlarge',
        ],
        'amount' => [
            'class' => 'input-xxlarge',
        ],
        'order' => [
            'class' => 'input-xxlarge',
        ],
        'comment' => [
            'type' => 'textarea',
            'class' => 'input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
