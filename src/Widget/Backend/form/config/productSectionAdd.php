<?php

use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\YiiComponent\Facade\Translator;

$parentsData = [0 => Translator::t('Not specified')] + CHtml::listData(ProductSectionRecord::model()->onlyParent()->findAll(), 'id', 'name');
return [
    'showErrorSummary' => true,
    'elements' => [
        'name' => [
            'type' => 'text',
            'class' => 'span4',
            'placeholder' => Translator::t('Enter section name'),
        ],
        'parent_id' => [
            'type' => 'select2',
            'asDropDownList' => true,
            'data' => empty($parentsData) ? [''] : $parentsData,
        ],
        'keywords' => [
            'type' => 'textarea',
            'class' => 'span4',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
    ],
];
