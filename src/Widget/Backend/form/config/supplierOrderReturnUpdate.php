<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'message_callcenter' => [
            'type' => 'textarea',
            'maxlength' => 400,
            'class' => 'span4',
            'placeholder' => Translator::t('Enter text'),
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
