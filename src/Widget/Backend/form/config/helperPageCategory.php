<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => Translator::t('Enter category name'),
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
