<?php

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'reason' => [
            'type' => 'dropdownlist',
            'items' => PromocodeRecord::reasonReplacements(),
        ],
        'reason_description' => [
            'type' => 'textarea',
        ],

        'cash' => [
            'type' => 'text',
        ],

        'percent' => [
            'type' => 'text',
        ],

        'order_price_after' => [
            'type' => 'text',
        ],

        'validUntilDateString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
        ],

        'eventIdsStrings' => [
            'type' => 'text',
        ],
        'eventProductIdsStrings' => [
            'type' => 'text',
        ],
        'productIdsStrings' => [
            'type' => 'text',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
