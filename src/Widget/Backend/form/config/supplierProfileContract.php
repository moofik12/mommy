<?php

use MommyCom\YiiComponent\Facade\RegionalTranslator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'agreement' => [
            'type' => 'text',
            'disabled' => 'disabled',
        ],
        'not_delivered_penalty_percent' => [
            'type' => 'text',
            'placeholder' => '%',
            'disabled' => 'disabled',
        ],
        'add_delivery_working_days' => [
            'type' => 'number',
            'disabled' => 'disabled',
        ],
        'delivery_working_days' => [
            'type' => 'number',
            'disabled' => 'disabled',
        ],
        'paid_by' => [
            'type' => 'text',
            'disabled' => 'disabled',
        ],
        'days_to_pay' => [
            'type' => 'number',
            'disabled' => 'disabled',
        ],
        'discount_from_market' => [
            'type' => 'number',
            'disabled' => 'disabled',
        ],
        'discount_from_wholesale' => [
            'type' => 'number',
            'disabled' => 'disabled',
            'value' => '60'
        ],
    ],
];
