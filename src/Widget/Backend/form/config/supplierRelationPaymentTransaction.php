<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'amount' => [
            'type' => 'text',
            'maxlength' => 64,
        ],
        'bankName' => [
            'type' => 'text',
            'maxlength' => 255,
        ],
        'bankGiro' => [
            'type' => 'text',
            'maxlength' => 255,
        ],
        'comment' => [
            'type' => 'textarea',
            'maxlength' => 255,
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
