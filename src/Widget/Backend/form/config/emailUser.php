<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'userIds' => [
            'type' => 'select2',
            'placeholder' => Translator::t('Recipients'),
            'multiple' => 'multiple',
            'asDropDownList' => false,
            'options' => [
                'allowClear' => true,
                'width' => '100%',
                'tags' => [],
                'ajax' => [
                    'url' => Yii::app()->createUrl('user/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    result.results.push({
                                        id: value.id,
                                        text: "<strong>" + value.email + "</strong>"
                                            + " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>",
                                    });
                                });

                                return result;
                            }'),
                ],
            ],
        ],
        'subject' => [
            'type' => 'textarea',
            'style' => 'width: 100%; max-width: 100%; padding: 0;',
        ],
        'body' => [
            'type' => 'redactor',
            'options' => [
                'buttons' => [
                    'html', '|', 'formatting', '|', 'bold', 'italic', 'deleted', '|',
                    'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
                    'table', 'link', '|',
                    'fontcolor', 'backcolor', '|', 'alignment', '|', 'horizontalrule',
                ],
                'convertDivs' => false,
                'formattingTags' => ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
            ],
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'ajaxSubmit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
