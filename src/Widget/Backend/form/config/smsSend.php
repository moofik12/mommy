<?php

use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

return array(
    //'showErrorSummary' => true,
    'elements' => array(
        'phone' => array(),
        'text' => array(
            'type' => 'textarea',
            'rows' => '5',
            'spellcheck' => true,
            'style' => 'padding:0; max-width: 520px; width: 520px;',
        ),
        'saveText' => array(
            'type' => 'checkbox',
        ),
    ),
    'buttons' => array(
        'submit' => array(
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Send'),
        ),
        'reset' => array(
            'type' => 'reset',
            'label' => Translator::t('Cancel')
        ),
    ),
);
