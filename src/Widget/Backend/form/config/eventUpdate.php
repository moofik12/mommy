<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'is_visible' => [
            'type' => 'checkbox',
        ],
        'is_charity' => [
            'type' => 'checkbox',
        ],
        'can_prepay' => [
            'type' => 'checkbox',
        ],
        'brand_manager_id' => [
            'type' => 'select2',

            'asDropDownList' => false,
            'prompt' => Translator::t('Select brand manager'),
            'options' => [
                'width' => 'resolve',
                'initSelection' => new CJavaScriptExpression('function (element, callback) {
                    $.ajax({
                        url: "' . Yii::app()->createUrl('admin/ajaxBrandManagerById') . '",
                        method: "GET",
                        data: {
                            id: parseInt(element.val())
                        },
                        success: function(result) {
                            var brand = result.items[0] || false;
                            if (!brand) {
                                return;
                            }
                            var data = {
                                id: brand.id,
                                text: brand.login + " (" + brand.fullname + ")"
                            };
                            callback(data);
                        },
                        error: function() {
                            alert("Failed to load data on brand managers");
                        }
                    });
                }'),
                'ajax' => [
                    'url' => Yii::app()->createUrl('admin/ajaxBrandManager'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            name: term,
                            page: page
                        };
                    }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.login + " (" + value.fullname + ")"
                            });
                        });

                        return result;
                    }'),
                ],
            ],
        ],
        'name' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'span4',
            'placeholder' => Translator::t('Enter the title of flash-sale'),
        ],
        'description' => [
            'type' => 'bootstrap.widgets.TbRedactorJs',
            'editorOptions' => [
                'buttons' => ['bold', 'italic', '|', 'unorderedlist', 'orderedlist', '|', 'html'],
            ],
        ],
        'description_short' => [
            'type' => 'textarea',
            'class' => 'span4',
        ],
        'description_mailing' => [
            'type' => 'bootstrap.widgets.TbRedactorJs',
            'editorOptions' => [
                'buttons' => ['bold', 'italic', '|', 'unorderedlist', 'orderedlist', '|', 'html'],
            ],
        ],
        'startAtDateString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
        ],
        'startAtTimeString' => [
            'type' => 'time',
            //'append'=>'<i class="icon-time"></i>',
            'class' => 'span1',
        ],
        'endAtDateString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
        ],
        'endAtTimeString' => [
            'type' => 'time',
            //'append'=>'<i class="icon-time"></i>',
            'class' => 'span1',
        ],
        'mailingStartAtDateString' => [
            'type' => 'date',
            'append' => '<i class="icon-calendar"></i>',
            'class' => 'span6',
        ],
        'priority' => [
            'type' => 'text',
            'class' => 'span1',
            'hint' => Translator::t('Sorted first by end time then by this value. no need to change default settings'),
        ],
        'logo' => [
            'type' => 'file',
        ],
        'promo' => [
            'type' => 'file',
        ],
        'distribution' => [
            'type' => 'file',
        ],
        'sizeChart' => [
            'type' => 'file',
        ],
        CHtml::hiddenField('distribution-resize'),
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
