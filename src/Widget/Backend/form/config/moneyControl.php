<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'id' => 'moneyControl',
    'showErrorSummary' => true,
    'elements' => [
        'moneyExpected' => [
            'type' => 'text',
            'disabled' => 'disabled',
        ],
        'moneyReal' => [
            'type' => 'text',
        ],
        'deliveryType' => [
            'type' => 'hidden',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
        'list' => [
            'type' => 'link',
            'label' => Translator::t('Back to the list'),
            'url' => Yii::app()->controller->createUrl('index'),
        ],
    ],
];
