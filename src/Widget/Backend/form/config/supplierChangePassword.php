<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'newPassword' => [
            'type' => 'password',
            'maxlength' => 255,
            'placeholder' => Translator::t('Enter new password'),
        ],
        'confirmPassword' => [
            'type' => 'password',
            'maxlength' => 255,
            'placeholder' => Translator::t('Enter password confirmation'),
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
    ],
];
