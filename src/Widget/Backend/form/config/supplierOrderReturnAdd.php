<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'order_id' => [
            'type' => 'text',
            'maxlength' => 32,
            'class' => 'span4',
            'placeholder' => Translator::t('Please enter order number'),
        ],
        'bankName' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'span4',
        ],
        'bankGiro' => [
            'type' => 'text',
            'maxlength' => 255,
            'class' => 'span4',
        ],
        'message_callcenter' => [
            'type' => 'textarea',
            'maxlength' => 400,
            'class' => 'span11',
            'rows' => 4,
            'placeholder' => Translator::t('Enter text'),
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
