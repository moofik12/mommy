<?php

use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

return [
    'showErrorSummary' => true,
    'elements' => [
        'ip' => [
            'placeholder' => Translator::t('address for blocking'),
        ],
        'section' => [
            'type' => 'dropdownlist',
            'items' => $this->model->getSectionReplacements(),
            'prompt' => Translator::t('Select a type'),
        ],
        'comment' => [
            'type' => 'textarea',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
