<?php

use MommyCom\Model\Db\SupplierRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'elements' => [
        'status' => [
            'type' => 'listbox',
            'items' => SupplierRecord::statuses(),
        ],
        'display_name' => [
            'type' => 'text',
            'maxlength' => 64,
        ],
    ],
    'buttons' => [
        'submitAccess' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
