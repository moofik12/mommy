<?php

use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TbForm
 */

$id = $this->model->id;
return [
    'showErrorSummary' => false,
    'action' => ['distributionPush/update', 'id' => $id],
    'elements' => [
        CHtml::hiddenField('id', $id),
        'subject' => [
            'type' => 'text',
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
        'body' => [
            'type' => 'textarea',
            'rows' => 4,
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
        'start_at' => [
            'type' => 'date',
            'options' => [
                'format' => 'dd.mm.yyyy',
            ],
        ],
        'start_time' => [
            'type' => 'time',
            'autocomplete' => 'off',
        ],
        'url' => [
            'type' => 'text',
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
        'url_desktop' => [
            'type' => 'text',
            'style' => 'max-width:500px; width:500px; max-height: 72px;',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Edit'),
        ],
        'list' => [
            'type' => 'link',
            'label' => Translator::t('Back to the list'),
            'url' => Yii::app()->controller->createUrl('distributionPush/index'),
        ],
    ],

];
