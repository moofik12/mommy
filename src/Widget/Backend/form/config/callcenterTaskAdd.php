<?php

use MommyCom\Model\Db\CallcenterTaskOperationRecord;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;

$operations = ArrayUtils::getColumn(CallcenterTaskOperationRecord::model()->findAll(), 'title', 'id');
$operationId = CallcenterTaskRecord::model()->operationIdDemandedStories();
/** @var CallcenterTaskRecord $model */
$model = $this->model;

return [
    'showErrorSummary' => true,
    'elements' => [
        'operation_id' => [
            'type' => 'select2',
            'asDropDownList' => true,
            'data' => $operations,
            'val' => $operationId,
            'options' => [
                'width' => '540px',
            ],
        ],

        '<label>' . Translator::t('Orders') . '</label>',
        Yii::app()->controller->widget('bootstrap.widgets.TbSelect2', [
            'name' => 'orderIds',
            'asDropDownList' => false,
            'options' => [
                'width' => '540px',
                'tags' => [],
                'placeholder' => Translator::t('A separate task will be created for each order'),
                'maximumSelectionSize' => 20,
                'tokenSeparators' => [',', ' '],
                'formatNoMatches' => new CJavaScriptExpression('function(term) {return "Specify flash-sales"}'),
            ],
        ], true),

        '<label>' . Translator::t('Flash-sale') . '</label>',
        Yii::app()->controller->widget('bootstrap.widgets.TbSelect2', [
            'name' => 'eventIds',
            'asDropDownList' => false,
            'options' => [
                'width' => '540px',
                'tags' => [],
                'placeholder' => Translator::t('Individual tasks will be created for each order during the flash-sale period'),
                'maximumSelectionSize' => 1,
                'tokenSeparators' => [',', ' '],
                'formatNoMatches' => new CJavaScriptExpression('function(term) {return "Specify flash-sale"}'),
            ],
        ], true),

        'comment_task' => [
            'type' => 'textarea',
            'maxlength' => 255,
            'rows' => 10,
            'class' => 'input-xxlarge',
        ],
        'is_callcenter_answer' => [
            'type' => 'checkbox',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => $model->isNewRecord ? Translator::t('Save') : Translator::t('Create recurring task'),
        ],
        'list' => [
            'type' => 'link',
            'label' => Translator::t('Back to the list'),
            'url' => Yii::app()->controller->createUrl('callcenterTask/task'),
        ],
    ],
];
