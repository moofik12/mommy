<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'enctype' => 'multipart/form-data',
    'elements' => [
        'supplier_id' => [
            'type' => 'select2',

            'asDropDownList' => false,
            'prompt' => Translator::t('Select user'),
            'options' => [
                'width' => 'resolve',
                'ajax' => [
                    'url' => Yii::app()->createUrl('supplier/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            name: term,
                            page: page
                        };
                    }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.name,
                            });
                        });

                        return result;
                    }'),
                ],
            ],
        ],

        'brand_manager_id' => [
            'type' => 'select2',

            'asDropDownList' => false,
            'prompt' => Translator::t('Select brand manager'),
            'options' => [
                'width' => 'resolve',
                'initSelection' => new CJavaScriptExpression('function (element, callback) {
                    $.ajax({
                        url: "' . Yii::app()->createUrl('admin/ajaxBrandManagerById') . '",
                        method: "GET",
                        data: {
                            id: parseInt(element.val())
                        },
                        success: function(result) {
                            var brand = result.items[0] || false;
                            if (!brand) {
                                return;
                            }
                            var data = {
                                id: brand.id,
                                text: brand.login + " (" + brand.fullname + ")"
                            };
                            callback(data);
                        },
                        error: function() {
                            alert("Failed to load data on brand managers");
                        }
                    });
                }'),
                'ajax' => [
                    'url' => Yii::app()->createUrl('admin/ajaxBrandManager'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            name: term,
                            page: page
                        };
                    }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.login + " (" + value.fullname + ")"
                            });
                        });

                        return result;
                    }'),
                ],
            ],
        ],

        CHtml::hiddenField('distribution-resize'),
    ],
    'buttons' => [
        'continue' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'icon' => 'icon-forward',
            'label' => Translator::t('Save and continue filling'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
