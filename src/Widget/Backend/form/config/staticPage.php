<?php

use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'category_id' => [
            'type' => 'dropdownlist',
            'items' => CHtml::listData(StaticPageCategoryRecord::model()->findAll(), 'id', 'name'),
            'prompt' => Translator::t('Select category'),
        ],
        'url' => [
            'type' => 'text',
            'placeholder' => Translator::t('with the same url displayed on one page'),
            'class' => 'span6',
        ],
        'title' => [
            'type' => 'text',
            'placeholder' => Translator::t('Mail-out'),
            'class' => 'span10',
        ],
        'body' => [
            'type' => 'redactor',
            'options' => [
                'buttons' => [
                    'html', '|', 'formatting', '|', 'bold', 'italic', 'deleted', '|',
                    'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
                    'table', 'link', '|',
                    'fontcolor', 'backcolor', '|', 'alignment', '|', 'horizontalrule',
                ],
                'convertDivs' => false,
                'formattingTags' => ['p', 'blockquote', 'pre', 'h2', 'h3', 'h4'],
            ],
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
