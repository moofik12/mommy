<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'url' => [
            'type' => 'text',
            'placeholder' => Translator::t('Text in the address bar (for uniqueness)'),
            'class' => 'input-xlarge',
        ],
        'name' => [
            'type' => 'text',
            'placeholder' => Translator::t('Category name'),
            'class' => 'input-xxlarge',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
