<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'userIds' => [
            'type' => 'select2',
            'placeholder' => Translator::t('Recipients'),
            'multiple' => 'multiple',
            'asDropDownList' => false,
            'options' => [
                'width' => '520px',
                'tags' => [],
                'ajax' => [
                    'url' => Yii::app()->createUrl('user/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    result.results.push({
                                        id: value.id,
                                        text: "<strong>" + value.email + "</strong>"
                                            + " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>",
                                    });
                                });

                                return result;
                            }'),
                ],
            ],
        ],
        'text' => [
            'type' => 'textarea',
            'style' => 'padding:0; max-width: 520px; width: 520px;',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'ajaxSubmit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
