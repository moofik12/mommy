<?php

use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'comment' => [
            'type' => 'textarea',
            'class' => 'span5',
        ],
        'client_comment' => [
            'type' => 'textarea',
            'class' => 'span5',
        ],

        'trackcode' => [
            'type' => 'text',
            'class' => 'span5',
        ],
        'trackcode_return' => [
            'type' => 'text',
            'class' => 'span5',
        ],

        'price_delivery' => [
            'type' => 'text',
            'class' => 'span3',
        ],

        'refund_delivery_type' => [
            'type' => 'dropdownlist',
            'items' => OrderReturnRecord::refundDeliveryReplacements(),
            'class' => 'span6',
        ],

        'price_redelivery' => [
            'type' => 'text',
            'class' => 'span3',
        ],

        'refund_redelivery_type' => [
            'type' => 'dropdownlist',
            'items' => OrderReturnRecord::refundDeliveryReplacements(),
            'class' => 'span6',
        ],

        'refund_type' => [
            'type' => 'dropdownlist',
            'class' => 'span4',
            'items' => OrderReturnRecord::refundTypeReplacements(),
            'class' => 'span6',
        ],

        '<script>
            $(function() {
                $("select[name*=refund_type]").on("change", function(e) {
                    var $this = $(this),
                        $bankInfo = $this.closest("form").find("[data-return-bank-info]");
                    if ($this.val() == ' . OrderReturnRecord::REFUND_TYPE_BANK . ') {
                        $bankInfo.show()
                    } else {
                        $bankInfo.hide();
                    }
                }).trigger("change");
            })
        </script>',
        '<div data-return-bank-info="1" style="display: none">',
        'bankFullname' => [
            'type' => 'text',
            'class' => 'span4',
        ],
        'bankVatin' => [
            'type' => 'text',
            'class' => 'span4',
        ],
        'bankName' => [
            'type' => 'text',
            'class' => 'span4',
        ],
        'bankGiro' => [
            'type' => 'text',
            'class' => 'span4',
            'hint' => Translator::t('20 digits'),
        ],
        'bankCardnum' => [
            'type' => 'text',
            'class' => 'span4',
            'hint' => Translator::t('16 digits'),
        ],
        'bankCorrespondentAcc' => [
            'type' => 'text',
            'class' => 'span4',
            'hint' => Translator::t('20 digits'),
        ],
        'bankIdCode' => [
            'type' => 'text',
            'class' => 'span4',
            'hint' => Translator::t('9 digits'),
        ],
        'bankMFOCode' => [
            'type' => 'text',
            'class' => 'span4',
            'hint' => Translator::t('6 digits'),
        ],
        '</div>',
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
