<?php

use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Facade\Translator;

$contracts = SupplierContractRecord::model()->getSqlCommand()->queryAll();
$contractsFilter = ArrayUtils::getColumn($contracts, 'name', 'id');
$defaultContract = $this->model->getContractConnected();
if ($defaultContract) {
    $this->model->contract_id = $defaultContract->id;
}

return [
    'showErrorSummary' => true,
    'elements' => [
        'name' => [
            'type' => 'text',
            'maxlength' => 255,
            'placeholder' => Translator::t('Enter supplier name'),
        ],
        'contract_id' => [
            'type' => 'listbox',
            'items' => $contractsFilter,
        ],
        'full_name' => [
            'type' => 'text',
            'maxlength' => 250,
        ],
        'address' => [
            'type' => 'textarea',
            'maxlength' => 4096,
            'rows' => 4,
        ],
        'phone' => [
            'type' => 'text',
            'maxlength' => 25,
        ],
        'email' => [
            'type' => 'text',
            'maxlength' => 45,
        ],
        'is_inform' => [
            'type' => 'listbox',
            'items' => Yii::app()->format->booleanFormat,
        ],
        'logo' => [
            'type' => 'file',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
    'enctype' => 'multipart/form-data',
];
