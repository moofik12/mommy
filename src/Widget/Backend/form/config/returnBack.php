<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [],
    'buttons' => [
        'back' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Return to edit'),
            'htmlOptions' => [
                'class' => 'btn btn-success btn-large pull-left',
            ],
        ],
        '<div class="clearfix"></div>',
    ],
];
