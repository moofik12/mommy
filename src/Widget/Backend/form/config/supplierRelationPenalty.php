<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'showErrorSummary' => true,
    'elements' => [
        'order_id' => [
            'type' => 'text',
            'maxlength' => 64,
            'hint' => '<span style="color:grey;">' . Translator::t('not required') . '</span>',
        ],
        'amount' => [
            'type' => 'text',
            'maxlength' => 64,
        ],
        'description' => [
            'type' => 'textarea',
            'maxlength' => 255,
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Save'),
        ],
        'reset' => [
            'type' => 'reset',
            'label' => Translator::t('Cancel'),
        ],
    ],
];
