<?php

use MommyCom\YiiComponent\Facade\Translator;

return [
    'id' => 'moneyControlItemsFromFile',
    'enctype' => 'multipart/form-data',
    'action' => ['validateFile'],
    'elements' => [
        'deliveryType' => [
            'type' => 'hidden',
        ],
        'file' => [
            'type' => 'file',
        ],
        'numColumn' => [
            'type' => 'text',
        ],
    ],
    'buttons' => [
        'submit' => [
            'type' => 'submit',
            'layoutType' => 'primary',
            'label' => Translator::t('Load'),
        ],
    ],
];
