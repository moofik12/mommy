<?php

namespace MommyCom\Widget\Backend;

use MommyCom\YiiComponent\Application\ApplicationTrait;

class ButtonColumn extends \TbButtonColumn
{
    use ApplicationTrait;

    public $openNewWindow = true;
    public $template = '{update} {delete}';
    public $htmlOptions = ['class' => 'button-column', 'style' => 'width: 1%; white-space: nowrap;'];
    public $updateButtonIcon = 'edit';
    public $deleteButtonIcon = 'remove';
    public $viewButtonOptions = [
        'class' => 'btn-primary',
    ];
    public $updateButtonOptions = [
        'class' => 'btn-warning',
    ];
    public $deleteButtonOptions = [
        'class' => 'btn-danger',
    ];

    /**
     *### .renderButton()
     * Renders a link button.
     *
     * @param string $id the ID of the button
     * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
     * $button['options']['data'] a PHP expression
     * @param integer $row the row number (zero-based)
     * @param mixed $data the data object associated with the row
     */
    protected function renderButton($id, $button, $row, $data)
    {
        $options = isset($button['options']) ? $button['options'] : [];

        if (!isset($options['target']) && $this->openNewWindow) {
            $options['target'] = '_blank';
        }

        if (!isset($options['class'])) {
            $options['class'] = 'btn ';
        } else {
            $options['class'] = 'btn ' . $options['class'];
        }

        $button['options'] = $options;

        if (isset($button['options']['data'])) {
            foreach ($button['options']['data'] as $name => $text) {
                $button['options']["data-$name"] = $this->evaluateExpression($text, ['data' => $data, 'row' => $row]);
            }

            unset($button['options']['data']);
        }

        parent::renderButton($id, $button, $row, $data);
    }
}
