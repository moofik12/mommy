<?php
/*## TbBox widget class
 *
 * @author Antonio Ramirez <antonio@clevertech.biz>
 * @copyright Copyright &copy; Clevertech 2012-
 * @license [New BSD License](http://www.opensource.org/licenses/bsd-license.php) 
 * @package bootstrap.widgets
 */

namespace MommyCom\Widget\Backend;

use CHtml;
use CWidget;
use MommyCom\YiiComponent\Application\ApplicationTrait;
use Yii;

class ContentBox extends CWidget
{
    use ApplicationTrait;

    private $_id;

    /**
     * @var mixed
     * Box title
     * If set to false, a box with no title is rendered
     */
    public $title = '';

    /**
     * @var string
     * The class icon to display in the header title of the box.
     * @see <http://twitter.github.com/bootstrap/base-css.html#icon>
     */
    public $headerIcon;

    /**
     * @var string
     * Box Content
     * optional, the content of this attribute is echoed as the box content
     */
    public $content = '';

    /**
     * @var string
     * Box Content Url
     */
    public $contentUrl = '';

    /**
     * @var array
     * box HTML additional attributes
     */
    public $htmlOptions = [];

    /**
     * @var array
     * box header HTML additional attributes
     */
    public $htmlHeaderOptions = [];

    /**
     * @var array
     * box content HTML additional attributes
     */
    public $htmlContentOptions = [];

    /**
     * @var array the configuration for additional header buttons. Each array element specifies a single button
     * which has the following format:
     * <pre>
     *     array(
     *        array(
     *          'class' => 'bootstrap.widgets.TbButton',
     *          'label' => '...',
     *          'size' => '...',
     *          ...
     *        ),
     *      array(
     *          'class' => 'bootstrap.widgets.TbButtonGroup',
     *          'buttons' => array( ... ),
     *          'size' => '...',
     *        ),
     *      ...
     * )
     * </pre>
     */
    public $headerButtons = [];

    public $dark = true;

    public $wrapper = '<div class="row-fluid"><div class="span12">{content}</div></div>';

    /**
     *### .init()
     * Widget initialization
     */
    public function init()
    {
        if (isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = 'box  ' . $this->htmlOptions['class'];
        else
            $this->htmlOptions['class'] = 'box ';

        if ($this->dark) {
            $this->htmlOptions['class'] = 'dark ' . $this->htmlOptions['class'];
        }

        if (isset($this->htmlContentOptions['class']))
            $this->htmlContentOptions['class'] = 'body ' . $this->htmlContentOptions['class'];
        else
            $this->htmlContentOptions['class'] = 'body';

        if (!isset($this->htmlContentOptions['id']))
            $this->htmlContentOptions['id'] = $this->getId();

        if (isset($this->htmlHeaderOptions['class']))
            $this->htmlHeaderOptions['class'] = 'box-header ' . $this->htmlHeaderOptions['class'];
        else
            $this->htmlHeaderOptions['class'] = 'box-header';

        ob_start();

        echo CHtml::openTag('div', $this->htmlOptions);

        $this->_initButtons();

        $this->registerClientScript();
        $this->renderHeader();
        $this->renderContentBegin();
    }

    /**
     * Returns the ID of the widget or generates a new one if requested.
     *
     * @param boolean $autoGenerate whether to generate an ID if it is not set previously
     *
     * @return string id of the widget.
     */
    public function getId($autoGenerate = true)
    {
        static $count = 0;

        if ($this->_id !== null) {
            return $this->_id;
        }

        if ($autoGenerate) {
            $count++;
            $id = str_replace([' ', "\n", "\r", "\t"], '_', $this->title);
            return $this->_id = $id . '_w' . $count;
        }
    }

    /**
     * Sets the ID of the widget.
     *
     * @param string $value id of the widget.
     */
    public function setId($value)
    {
        $this->_id = $value;
    }

    protected function _initButtons()
    {
        if (!is_array($this->headerButtons)) {
            return;
        }

        //<a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
        //<i class="icon-chevron-up"></i>  </a>

        $buttons = [
            ['label' => 'Link', 'url' => '#' . $this->getId()],
            [
                'icon' => 'icon-chevron-up',
                'linkOptions' => [
                    'class' => 'accordion-toggle minimize-box',
                    'data-toggle' => 'collapse',
                ],
                'url' => '#' . $this->getId(),
            ],
        ];

        if (!isset($this->headerButtons['items'])) {
            $this->headerButtons['items'] = $buttons;
        } else {
            $this->headerButtons['items'] = array_merge($this->headerButtons['items'], $buttons);
        }
    }

    /**
     *### .run()
     * Widget run - used for closing procedures
     */
    public function run()
    {
        $this->renderContentEnd();
        echo CHtml::closeTag('div') . "\n";

        $html = ob_get_clean();
        if (!empty($this->wrapper)) {
            $html = strtr($this->wrapper, ['{content}' => $html]);
        }
        echo $html;
    }

    /**
     *### .renderHeader()
     * Renders the header of the box with the header control (button to show/hide the box)
     */
    public function renderHeader()
    {
        if ($this->title !== false) {
            echo CHtml::openTag('header', $this->htmlHeaderOptions);
            if ($this->title) {
                $this->title = '<h5>' . $this->title . '</h5>';

                if ($this->headerIcon)
                    $this->title = '<div class="icons"><i class="' . $this->headerIcon . '"></i></div>' . $this->title;

                echo $this->title;
                $this->renderButtons();
            }
            echo CHtml::closeTag('header');
        }
    }

    /**
     *### .renderButtons()
     * Renders a header buttons to display the configured actions
     */
    public function renderButtons()
    {
        if (empty($this->headerButtons))
            return;

        echo '<div class="toolbar">';

        if (!empty($this->headerButtons)) {
            if (is_array($this->headerButtons)) {
                $this->widget('bootstrap.widgets.TbMenu', $this->headerButtons);
            } else {
                echo $this->headerButtons;
            }
        }

        echo '</div>';
    }

    /*
     *### .renderContentBegin()
     *
     * Renders the opening of the content element and the optional content
     */
    public function renderContentBegin()
    {
        echo CHtml::openTag('div', $this->htmlContentOptions);
        if (!empty($this->content))
            echo $this->content;
    }

    /*
     *### .renderContentEnd()
     *
     * Closes the content element
     */
    public function renderContentEnd()
    {
        echo CHtml::closeTag('div');
    }

    /**
     *### .registerClientScript()
     * Registers required script files (CSS in this case)
     */
    public function registerClientScript()
    {
        Yii::app()->bootstrap->registerAssetCss('bootstrap-box.css');
    }
}
