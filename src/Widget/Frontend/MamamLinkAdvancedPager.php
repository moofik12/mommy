<?php

namespace MommyCom\Widget\Frontend;

use CHtml;

class MamamLinkAdvancedPager extends MamamLinkPager
{
    /**
     * @var string
     */
    public $previousPageCssClass = 'pagination-btn pagination-prev';

    /**
     * @var string
     */
    /**
     * @var string
     */
    public $nextPageCssClass = 'pagination-btn pagination-next';

    /**
     * @var string
     */
    public $prevPageLabel;

    /**
     * @var string
     */
    public $firstPageLabel;

    public function init()
    {
        if ($this->nextPageLabel === null)
            $this->nextPageLabel = \Yii::t('common', 'Next');
        if ($this->prevPageLabel === null)
            $this->prevPageLabel = \Yii::t('common', 'Previous');
        if ($this->firstPageLabel === null)
            $this->firstPageLabel = \Yii::t('common', 'The first');
        if ($this->lastPageLabel === null)
            $this->lastPageLabel = \Yii::t('common', 'The last');
        if ($this->header === null)
            $this->header = \Yii::t('common', 'На страницу') . ':';

        if (!isset($this->htmlOptions['id']))
            $this->htmlOptions['id'] = $this->getId();
        if (!isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = 'yiiPager';
    }

    /**
     * Creates the page buttons.
     *
     * @return array a list of page buttons (in HTML code).
     */
    protected function createPageButtons()
    {
        if (($pageCount = $this->getPageCount()) <= 1)
            return [];

        list($beginPage, $endPage) = $this->getPageRange();
        $currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons = [];

        // internal pages
        $isLastPage = false;
        if ($pageCount > $this->maxButtonCount) {
            if ($currentPage + 1 === $pageCount) {
                $beginPage = $beginPage - 1;
                $endPage = $endPage - 1;
                $isLastPage = true;
            } elseif ($currentPage + 1 === $pageCount - 1) {
                $isLastPage = true;
                $beginPage = $beginPage - 1;
                $endPage = $endPage - 1;
            } elseif ($currentPage + 1 === $pageCount - 2) {
                $isLastPage = true;
            }
        }
        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0)
                $page = 0;
            $buttons[] = $this->createPageButtonPrevNext($this->prevPageLabel, $page, $this->previousPageCssClass, $currentPage <= 0, false);
        }

        for ($i = $beginPage; $i <= $endPage; ++$i)
            $buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);

        if ($pageCount > $this->maxButtonCount) {
            //last page
            if (!$isLastPage && ($pageCount > $this->maxButtonCount + 1)) {
                $buttons[] = ' <li class="dots"><span>...</span></li>';
            }
            $buttons[] = $this->createPageButton($pageCount, $pageCount - 1, '', $currentPage >= $pageCount, $currentPage == $pageCount - 1);
        }
        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1)
                $page = $pageCount - 1;
            $buttons[] = $this->createPageButtonPrevNext($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
        }
        return $buttons;
    }

    protected function createPageButtonPrevNext($label, $page, $class, $hidden, $selected)
    {
        $activeClassLi = '';
        if ($hidden || $selected) {
            $activeClassLi = ($selected ? $this->activeClassLi : '');
            $activeClassLi .= ' ' . ($hidden ? 'hide-i' : '');
        }
        return '<li class="' . $activeClassLi . '">' . CHtml::link($label, $this->createPageUrl($page), ['class' => $class]) . '</li>';
    }
}
