<?php

namespace MommyCom\Widget\Frontend;

use CHtml;
use CLinkPager;

class MamamLinkPager extends CLinkPager
{
    /**
     * @var string
     */
    public $activeClassLi = '';

    /**
     * @var string
     */
    public $selectedPageCssClass = 'active-page';

    /**
     * @var bool
     */
    public $cssFile = false;

    /**
     * @var int
     */
    public $maxButtonCount = 4;

    /**
     * @var string
     */
    public $header = '';

    /**
     * Creates the page buttons.
     *
     * @return array a list of page buttons (in HTML code).
     */
    protected function createPageButtons()
    {
        if (($pageCount = $this->getPageCount()) <= 1)
            return [];

        list($beginPage, $endPage) = $this->getPageRange();
        $currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons = [];

        // internal pages
        $isLastPage = false;
        if ($pageCount > $this->maxButtonCount) {
            if ($currentPage + 1 === $pageCount) {
                $beginPage = $beginPage - 1;
                $endPage = $endPage - 1;
                $isLastPage = true;
            } elseif ($currentPage + 1 === $pageCount - 1) {
                $isLastPage = true;
                $beginPage = $beginPage - 1;
                $endPage = $endPage - 1;
            } elseif ($currentPage + 1 === $pageCount - 2) {
                $isLastPage = true;
            }
        }
        for ($i = $beginPage; $i <= $endPage; ++$i)
            $buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);

        if ($pageCount > $this->maxButtonCount) {
            //last page
            if (!$isLastPage && ($pageCount > $this->maxButtonCount + 1)) {
                $buttons[] = ' <li class="dots"><span>...</span></li>';
            }
            $buttons[] = $this->createPageButton($pageCount, $pageCount - 1, '', $currentPage >= $pageCount, $currentPage == $pageCount - 1);
        }
        return $buttons;
    }

    /**
     * Creates a page button.
     * You may override this method to customize the page buttons.
     *
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $hidden whether this page button is visible
     * @param boolean $selected whether this page button is selected
     *
     * @return string the generated button
     */
    protected function createPageButton($label, $page, $class, $hidden, $selected)
    {
        $activeClassLi = '';
        $classPage = '';
        if ($hidden || $selected) {
            $classPage = ($selected ? $this->selectedPageCssClass : '');
            $activeClassLi = ($selected ? $this->activeClassLi : '');
            $activeClassLi .= ' ' . ($hidden ? 'hide-i' : '');
        }
        return '<li class="' . $activeClassLi . '">' . CHtml::link($label, $this->createPageUrl($page), ['class' => $classPage]) . '</li>';
    }
}
