<?php echo CHtml::form(); ?>
    <div id="language-select">
        <?php
        echo CHtml::dropDownList('_lang', $currentLang, $languages,
            [
                'submit' => '',
                'csrf' => true,
            ]
        );
        ?>
    </div>
<?php echo CHtml::endForm(); ?>
