<?php

namespace MommyCom\Widget\Frontend;

use CWidget;
use Yii;

/**
 * Class LanguageSelector
 * Not used?
 */
class LanguageSelector extends CWidget
{
    /**
     * @throws \CException
     */
    public function run()
    {
        $currentLang = Yii::app()->language;
        $languages = Yii::app()->params->languages;
        $this->render('languageSelector', ['currentLang' => $currentLang, 'languages' => $languages]);
    }
}
