<?php

namespace MommyCom\Widget\FrontendMobile;

use CHtml;
use CLinkPager;

class MamamLinkPager extends CLinkPager
{
    public $selectedPageCssClass = 'active';
    public $cssFile = false;
    public $maxButtonCount = 4;
    public $pageClass = 'good-page';
    public $header = '';

    /**
     * Creates the page buttons.
     *
     * @return array a list of page buttons (in HTML code).
     */
    protected function createPageButtons()
    {
        if (($pageCount = $this->getPageCount()) <= 1)
            return [];

        list($beginPage, $endPage) = $this->getPageRange();
        $currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons = [];

        // internal pages
        for ($i = $beginPage; $i <= $endPage; ++$i)
            $buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);

        return $buttons;
    }

    /**
     * Creates a page button.
     * You may override this method to customize the page buttons.
     *
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $hidden whether this page button is visible
     * @param boolean $selected whether this page button is selected
     *
     * @return string the generated button
     */
    protected function createPageButton($label, $page, $class, $hidden, $selected)
    {
        if ($hidden || $selected)
            $class .= ' ' . ($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        return '<li class="' . $this->pageClass . ' ' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
    }
}
