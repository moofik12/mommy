<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180426054916 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE tbl_migration');
    }

    public function down(Schema $schema)
    {
    }
}
