<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180426055127 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE banners');
        $this->addSql('DROP TABLE banners_events');
    }

    public function down(Schema $schema)
    {
    }
}
