<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180619102340 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE orders ADD track_id VARCHAR(45) NOT NULL');
        $this->addSql('UPDATE orders SET track_id = trackcode');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE orders DROP track_id');
    }
}
