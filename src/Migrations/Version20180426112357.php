<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180426112357 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE distributions_statistics');
        $this->addSql('DROP TABLE distributions_statistics_items');
    }

    public function down(Schema $schema)
    {
    }
}
