<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180702121656 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("DROP TABLE user_global_logger_incoming");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql(<<<SQL
CREATE TABLE user_global_logger_incoming
(
  action                 tinyint unsigned not null,
  user_id                int unsigned     not null,
  user_class             varchar(128)     not null,
  class                  varchar(128)     not null,
  class_object_id        int unsigned     not null,
  controller             varchar(128)     not null,
  attributes_before_json varchar(512)     not null,
  attributes_after_json  varchar(512)     not null,
  description            varchar(128)     not null,
  need_move              tinyint unsigned not null,
  updated_at             int unsigned     not null,
  created_at             int unsigned     not null
)
  COMMENT 'новые данные для логирования действий пользователя'
  ENGINE = MEMORY
  CHARSET = utf8
SQL
);

    }
}
