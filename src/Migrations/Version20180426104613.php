<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180426104613 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE orders DROP COLUMN city_id');
        $this->addSql('ALTER TABLE orders DROP COLUMN region_id');
        $this->addSql('ALTER TABLE users DROP COLUMN city_id');
        $this->addSql('ALTER TABLE users DROP COLUMN region_id');
    }

    public function down(Schema $schema)
    {
    }
}
