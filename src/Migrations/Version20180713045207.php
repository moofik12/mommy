<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180713045207 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE orders_products MODIFY price float(12,2) unsigned default '0.00' NOT NULL");
        $this->addSql("ALTER TABLE warehouse_products MODIFY order_product_price float(12,2) unsigned default '0.00' not null comment 'Стоимость товара при продаже'");
        $this->addSql("ALTER TABLE orders_buyouts MODIFY delivery_price float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_banks_refunds MODIFY order_card_payed float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_banks_refunds MODIFY price_order float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_returns MODIFY price_delivery float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_returns MODIFY price_redelivery float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_returns MODIFY price_products float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_returns MODIFY exps_client float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders_returns MODIFY exps_shop float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE money_control MODIFY order_price float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders MODIFY price_total float(12,2) unsigned default '0.00' not null");
        $this->addSql("ALTER TABLE orders MODIFY to_pay float(12,2) unsigned default '0.00' not null");
    }

    public function down(Schema $schema) : void
    {
    }
}
