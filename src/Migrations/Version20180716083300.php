<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180716083300 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE warehouse_products MODIFY packaging_security_code int(4) unsigned not null');
    }

    public function down(Schema $schema) : void
    {
    }
}
