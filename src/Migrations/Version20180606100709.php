<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180606100709 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE suppliers DROP bank_giro_enc');
        $this->addSql('ALTER TABLE suppliers DROP bank_cardnum_enc');
        $this->addSql('ALTER TABLE suppliers ADD type INT(2) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD registration_date INT(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD chief_name VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD chief_passport VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD license VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD npwp VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD tdp VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD bank_name VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD bank_address VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD bank_account VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD messenger VARCHAR (255) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD certifiable_sections VARCHAR (255) DEFAULT NULL');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE suppliers DROP type');
        $this->addSql('ALTER TABLE suppliers DROP registration_date');
        $this->addSql('ALTER TABLE suppliers DROP chief_name');
        $this->addSql('ALTER TABLE suppliers DROP chief_passport');
        $this->addSql('ALTER TABLE suppliers DROP license');
        $this->addSql('ALTER TABLE suppliers DROP npwp');
        $this->addSql('ALTER TABLE suppliers DROP tdp');
        $this->addSql('ALTER TABLE suppliers DROP bank_name');
        $this->addSql('ALTER TABLE suppliers DROP bank_address');
        $this->addSql('ALTER TABLE suppliers DROP bank_account');
        $this->addSql('ALTER TABLE suppliers DROP messenger');
        $this->addSql('ALTER TABLE suppliers DROP certifiable_sections');
    }
}
