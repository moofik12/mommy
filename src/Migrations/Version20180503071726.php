<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180503071726 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE suppliers ADD is_approved BOOLEAN DEFAULT FALSE');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE suppliers DROP is_approved');
    }
}
