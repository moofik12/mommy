<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180619103416 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql(<<<SQL
ALTER TABLE orders_tracking
  ADD deadline_for_delivery INT UNSIGNED NOT NULL,
  ADD delivered_date INT UNSIGNED NOT NULL
SQL
        );
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE orders_tracking DROP deadline_for_delivery, DROP delivered_date");
    }
}
