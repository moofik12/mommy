<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180712182156 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql(<<<SQL
        ALTER TABLE orders
          ADD `payment_token` varchar(255),
          ADD `payment_token_expires_at` INT unsigned
SQL
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE orders drop column `payment_token`');
        $this->addSql('ALTER TABLE orders drop column `payment_token_expires_at`');
    }
}
