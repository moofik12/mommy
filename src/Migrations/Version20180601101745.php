<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180601101745 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE distributions');
        $this->addSql('DROP TABLE distributions_events');
        $this->addSql('DROP TABLE distributions_templates');
    }

    public function down(Schema $schema)
    {
    }
}
