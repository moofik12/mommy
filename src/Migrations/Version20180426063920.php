<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180426063920 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE events ADD supplier_notified BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD address TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers ADD logo TEXT DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE events DROP supplier_notified');
        $this->addSql('ALTER TABLE suppliers DROP address');
        $this->addSql('ALTER TABLE suppliers DROP logo');
    }
}
