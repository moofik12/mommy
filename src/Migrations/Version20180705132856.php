<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180705132856 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql(<<<SQL
        ALTER TABLE carts
            DROP INDEX product,
            ADD UNIQUE KEY product (product_id,event_id,user_id,anonymous_id)
SQL
        );

    }

    public function down(Schema $schema) : void
    {
        $this->addSql(<<<SQL
        ALTER TABLE carts
            DROP INDEX product,
            ADD UNIQUE KEY product (product_id,user_id,event_id)
SQL
        );
    }
}
