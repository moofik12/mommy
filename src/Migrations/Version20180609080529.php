<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180609080529 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE suppliers_contracts DROP not_delivered_penalty_cost');
        $this->addSql('ALTER TABLE suppliers_contracts DROP payment_after_full_delivery_working_days');
        $this->addSql('ALTER TABLE suppliers_contracts DROP payment_after_not_full_delivery_working_days');
        $this->addSql('ALTER TABLE suppliers_contracts ADD agreement VARCHAR (100) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers_contracts ADD paid_by VARCHAR (100) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers_contracts ADD days_to_pay INT(2) DEFAULT NULL');
        $this->addSql('ALTER TABLE suppliers_contracts ADD discount_from_market INT(2) DEFAULT 60');
        $this->addSql('ALTER TABLE suppliers_contracts ADD discount_from_wholesale INT(2) DEFAULT 30');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE suppliers_contracts DROP agreement');
        $this->addSql('ALTER TABLE suppliers_contracts DROP paid_by');
        $this->addSql('ALTER TABLE suppliers_contracts DROP days_to_pay');
        $this->addSql('ALTER TABLE suppliers_contracts DROP discount_from_market');
        $this->addSql('ALTER TABLE suppliers_contracts DROP discount_from_wholesale');
    }
}
