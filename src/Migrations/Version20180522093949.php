<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180522093949 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE IF EXISTS quiz');
        $this->addSql('DROP TABLE IF EXISTS quiz_questions');
        $this->addSql('DROP TABLE IF EXISTS quiz_questions_answers');
        $this->addSql('DROP TABLE IF EXISTS quiz_users_answers');
        $this->addSql('DROP TABLE IF EXISTS quiz_users_progress');
    }

    public function down(Schema $schema)
    {
    }
}
