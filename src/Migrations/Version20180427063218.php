<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180427063218 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE events ADD COLUMN can_prepay TINYINT(1) UNSIGNED NOT NULL DEFAULT 1");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE events DROP COLUMN can_prepay");
    }
}
