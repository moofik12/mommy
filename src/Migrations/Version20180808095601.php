<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180808095601 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE orders_returns_positions CHANGE COLUMN `condition` `item_condition` TINYINT NOT NULL;');
    }

    public function down(Schema $schema) : void
    {
    }
}
