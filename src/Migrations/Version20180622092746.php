<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180622092746 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE users_social_network_accounts');
    }

    public function down(Schema $schema)
    {
    }
}
