<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180426104401 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('DROP TABLE cities');
        $this->addSql('DROP TABLE cities_regions');
        $this->addSql('DROP TABLE couriers_cities');
    }

    public function down(Schema $schema)
    {
    }
}
