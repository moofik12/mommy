<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180626134246 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE `users` ADD `category` varchar(255)');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE `users` DROP COLUMN `category`');
    }
}
