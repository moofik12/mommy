<?php

namespace MommyCom;

use MommyCom\YiiComponent\Application\MommyConsoleApplication;

class ConsoleKernel extends Kernel
{
    public function getName()
    {
        return 'console';
    }

    protected function initializeContainer()
    {
        parent::initializeContainer();

        $this->getContainer()->get(MommyConsoleApplication::class);
    }
}
