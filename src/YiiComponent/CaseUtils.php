<?php

namespace MommyCom\YiiComponent;

final class CaseUtils
{
    public static function isUnderscore($value)
    {
        return strpos($value, '_') != -1;
    }

    public static function isCamel($value)
    {
        return strpos($value, '_') == -1;
    }

    public static function toCamel($value)
    {
        $items = explode('_', $value);
        foreach ($items as &$item) {
            $item = ucfirst(strtolower($item));
        }
        unset($item);
        return lcFirst(implode('', $items));
    }

    public static function toUnderscore($value)
    {
        return strtolower(preg_replace('/([A-Z])/', '_$1', $value));
    }

    /**
     * Конвертирует андэрскор в камел
     * <red>Результат может не полностью соответствовать ожидаемому</red>
     *
     * @param $value
     *
     * @return string
     */
    public static function toCamelUnsafe($value)
    {
        $items = explode('_', $value);
        foreach ($items as &$item) {
            $item = ucfirst($item);
        }
        unset($item);
        return lcFirst(implode('', $items));
    }

    public static function toUnderscoreUnsafe($value)
    {
        return strtolower(preg_replace('/([A-Z])/', '_$1', $value));
    }
}
