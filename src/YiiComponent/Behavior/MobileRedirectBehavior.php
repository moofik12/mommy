<?php

namespace MommyCom\YiiComponent\Behavior;

use MommyCom\YiiComponent\Application\MommyWebApplication;

class MobileRedirectBehavior extends \CBehavior
{
    /**
     * @param \CComponent $owner
     *
     * @throws \CException
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $owner->attachEventHandler('onBeginRequest', [$this, 'handleBeginRequest']);
    }

    public function handleBeginRequest()
    {
        /* @var $owner MommyWebApplication */
        $owner = $this->getOwner();
        $request = $owner->request;

        if (!$owner->userAgent->getIsMobile()) {
            return;
        }

        if ('m' === strstr($_SERVER['HTTP_HOST'], '.', true)) {
            return;
        }

        /* @var $mobileUrlManager \MommyCom\YiiComponent\BuildUrlManager */
        $mobileUrlManager = $owner->getComponent('mobileUrlManager');
        $mobileUrlManager->appendParams = false;

        /** @noinspection PhpUnhandledExceptionInspection */
        $route = $owner->urlManager->parseUrl($request);
        $url = $mobileUrlManager->createAbsoluteUrl($route, $_GET);

        header('Location: ' . $url, true, 302);
        exit;
    }
} 
