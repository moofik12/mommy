<?php

namespace MommyCom\YiiComponent\Behavior;

use MommyCom\YiiComponent\Application\ApplicationTrait;

class UserPreferredLanguageSetter extends \CBehavior
{
    use ApplicationTrait;

    /**
     * @param \CComponent $owner
     *
     * @throws \CException
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $owner->attachEventHandler('onBeginRequest', [$this, 'handleBeginRequest']);
    }

    /**
     * @return void
     */
    public function handleBeginRequest(): void
    {
        $app = $this->app();

        if (isset($_POST['_lang'])) {
            $app->language = $_POST['_lang'];
            $app->user->setState('_lang', $_POST['_lang']);
            $cookie = new \CHttpCookie('_lang', $_POST['_lang']);
            $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
            $app->request->cookies['_lang'] = $cookie;
        } elseif ($app->user->hasState('_lang')) {
            $app->language = $app->user->getState('_lang');
        } elseif (isset($app->request->cookies['_lang'])) {
            $app->language = $app->request->cookies['_lang']->value;
        }
    }
}
