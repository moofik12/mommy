<?php

namespace MommyCom\YiiComponent\Behavior;

use CBehavior;
use CEvent;
use CLogger;
use Exception;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\SplitTestTrackingRecord;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use Yii;

/**
 * Class TestCartPresentThingBehavior
 * Поведения для класса ShopShoppingCart
 * Класс A/B тестирования получения подарка при добавления товара в корзину
 */
class TestCartPresentThingBehavior extends CBehavior
{
    /**
     * Акция с подарками
     *
     * @var int
     */
    public $presentEvent = 10231;

    private $_productPresent;

    public function events()
    {
        return array_merge(parent::events(), [
            'onUpdatePosition' => 'updatePosition',
            'onRemovePosition' => 'removePosition',
        ]);
    }

    public function updatePosition(CEvent $event)
    {
        /** @var  ShopShoppingCart $cart */
        $cart = $this->getOwner();
        if (!($cart instanceof ShopShoppingCart)) {
            return;
        }

        $product = $this->getProductPresent();
        $hasPresentProduct = false;
        $userId = Yii::app()->user ? Yii::app()->user->id : 0;

        if (!$product instanceof EventProductRecord) {
            return;
        }

        //has present
        foreach ($cart->positions as $item) {
            if ($item->event_id == $product->event_id) {
                $hasPresentProduct = true;
                break;
            }
        }

        $hasOrderProduct = OrderProductRecord::model()
            ->callcenterStatuses([OrderProductRecord::CALLCENTER_STATUS_UNMODERATED, OrderProductRecord::CALLCENTER_STATUS_ACCEPTED])
            ->cache(5)
            ->eventId($this->presentEvent)
            ->userId($userId)
            ->exists();

        if ($hasPresentProduct || $hasOrderProduct) {
            if ($hasPresentProduct && !$this->availablePresent()) {
                $cart->remove($product->id);
            }
            return;
        }

        if ($cart->count >= 1 && $this->availablePresent()) {
            $cart->put($product, 1);
            $position = $cart->itemAt($product->id);

            if ($position instanceof CartRecord) {
                $position->priority = 100;
                $position->save(true, ['priority']);
            }
        };

        $this->_splitTestTracking();
    }

    /**
     * Remove present if only in cart
     *
     * @param CEvent $event
     */
    public function removePosition(CEvent $event)
    {
        /** @var  ShopShoppingCart $cart */
        $cart = $this->getOwner();

        if (!($cart instanceof ShopShoppingCart)) {
            return;
        }

        if ($cart->count == 1) {
            $positions = $cart->getPositions();
            /** @var CartRecord $item */
            $item = reset($positions);
            if ($item instanceof CartRecord && $item->event_id == $this->presentEvent) {
                $cart->remove($item->product_id);
            }
        }
    }

    /**
     * @return bool
     */
    protected function isEventsHasProducts()
    {
        return !!$this->getProductPresent();
    }

    /**
     * @return false|EventProductRecord
     */
    protected function getProductPresent()
    {
        if ($this->_productPresent !== null) {
            return $this->_productPresent;
        }

        $event = EventRecord::model()->onlyTimeActive()->with('products')->findByPk($this->presentEvent);
        $product = false;

        if ($event) {
            foreach ($event->products as $item) {
                if ($item->getIsPossibleToBuy()) {
                    $product = $item;
                    break;
                }
            }
        }

        $this->_productPresent = $product;

        return $this->_productPresent;
    }

    /**
     * @return bool
     */
    protected function availablePresent()
    {
        $test = Yii::app()->splitTesting;
        $available = $test->getNum("mamam-present-thing-{$this->presentEvent}", 2);

        return !!$available; //only A/B test
    }

    protected function _splitTestTracking()
    {
        try {
            $userAgent = Yii::app()->userAgent;
            $userId = Yii::app()->user->id;

            $splitTestTracking = new SplitTestTrackingRecord();
            $splitTestTracking->target = SplitTestTrackingRecord::TARGET_PRESENT_THING;
            $splitTestTracking->user_id = $userId;
            $splitTestTracking->order_id = 0;
            $splitTestTracking->testParams = Yii::app()->splitTesting->getAvailable();

            if (isset($_SERVER['REQUEST_URI'])) {
                $splitTestTracking->url = $_SERVER['REQUEST_URI'];
            }

            if (isset($_SERVER['HTTP_HOST'])) {
                $splitTestTracking->host = $_SERVER['HTTP_HOST'];
            }

            if ($userAgent->getIsDesktop()) {
                $splitTestTracking->device_type = SplitTestTrackingRecord::DEVICE_TYPE_DESKTOP;
            } elseif ($userAgent->getIsMobile()) {
                $splitTestTracking->device_type = SplitTestTrackingRecord::DEVICE_TYPE_MOBILE;
            } elseif ($userAgent->getIsMobileTablet()) {
                $splitTestTracking->device_type = SplitTestTrackingRecord::DEVICE_TYPE_TABLET;
            }

            $splitTestTracking->save();
        } catch (Exception $e) {
            Yii::log('Ошибка при создании тест-трекинга для подарка от МАМАМ' . $e->getTraceAsString(), CLogger::LEVEL_ERROR);
        }
    }
}
