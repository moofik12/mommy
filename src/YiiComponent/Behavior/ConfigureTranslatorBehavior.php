<?php

namespace MommyCom\YiiComponent\Behavior;

use MommyCom\YiiComponent\Application\MommyWebApplication;

class ConfigureTranslatorBehavior extends \CBehavior
{
    /**
     * @var string
     */
    public $category = '';

    /**
     * @return array
     */
    public function events()
    {
        return array_merge(parent::events(), [
            'onBeginRequest' => 'configureCommonFallback',
        ]);
    }

    /**
     * @param \CEvent $event
     *
     * @throws \CException
     */
    public function configureCommonFallback(\CEvent $event)
    {
        if (!$this->category) {
            return;
        }

        /** @var MommyWebApplication $app */
        $app = $event->sender;

        $app->messages->attachEventHandler('onMissingTranslation', function (\CMissingTranslationEvent $event) {
            /** @var \CMessageSource $source */
            $source = $event->sender;

            if ($this->category === $event->category) {
                $event->message = $source->translate('common', $event->message);
            }
        });
    }
}
