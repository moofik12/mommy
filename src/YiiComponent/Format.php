<?php

namespace MommyCom\YiiComponent;

use CFormatter;
use CHtml;
use DateInterval;
use DateTime;
use InvalidArgumentException;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class Fromat
 * Расширенный компонент 'format'
 */
class Format extends CFormatter
{
    public $dateFormat = 'd.m.Y';

    public $datetimeFormat = 'd.m.Y, H:i';

    public $purifyDescriptionOptions = [
        'HTML.Doctype' => 'HTML 4.01 Transitional',
        'HTML.Allowed' => '
            h1[style],h2[style],h3[style],br,p[style|align],b[style],span[style],
            strong[style],i[style],em[style],u[style],strike[style],
            a[style|href|title|target],ol[style],ul[style],li[style],
            img[style|alt|title|width|height|src|align],
            caption,tbody,thead,table[style|width|bgcolor|align|cellspacing|cellpadding|border],
            tr[style|align],td[style|align],th[style|align]
        ',
        'CSS.AllowedProperties' => [
            'text-decoration' => true,
            'font-weight' => true,
            //'color' => true,
            'float' => true,
            'text-align' => true,
            /*'margin' => true,
            'margin-left' => true,
            'margin-right' => true,
            'margin-top' => true,
            'margin-bottom' => true*/
            //'padding' => true
        ],
        'AutoFormat.RemoveEmpty.RemoveNbsp' => true,
        'AutoFormat.RemoveEmpty.RemoveNbsp.Exceptions' => [],
        'Core.EscapeInvalidChildren' => true,
        'HTML.TidyLevel' => 'medium',
    ];

    /**
     * \Yii::app()->format->humanTime($time)
     * Форматирует время в читаемый формат
     * до 1 минуты — пишется “только что” ;
     * от 1 минуты до 59 минут - “N минут назад” ;
     * от 60 минут до 5 часов - “N часов назад” ;
     * от 5 часов до 24 часов - “Сегодня в NN:NN ” ;
     * если прошло более 24 часов или начались новые календарные сутки - “Вчера в NN:NN ” ;
     * все что более 48 часов — “дата в NN:NN ”;
     * все что более года — “дата и год” .
     *
     * @author Alhambr
     * @author Bondarev
     * Исправление: отсчет был от даты создания а не от суток если более чем 5-ть часов
     */
    public function formatHumanTime($timestamp)
    {
        $category = isset(\Yii::app()->params['backendFlag']) ? 'backend' : 'common';
        $timestamp = Cast::toUInt($timestamp);
        $dateTimeNow = new DateTime();

        $dateTimeStamp = clone $dateTimeNow;
        $dateTimeStamp->setTimestamp($timestamp);

        $dateTimeStampToday = clone $dateTimeStamp;
        $dateTimeStampToday->modify('today');

        /** @var DateInterval $interval */
        $interval = $dateTimeStamp->diff($dateTimeNow);
        /** @var DateInterval $intervalToday */
        $intervalToday = $dateTimeStampToday->diff($dateTimeNow);

        $timeStr = \Yii::app()->getDateFormatter()->format("d MMM yyyy, HH:mm", $dateTimeStamp->getTimestamp());;

        if ($dateTimeNow->format('Y') !== $dateTimeStamp->format('Y') || $dateTimeNow->format('m') !== $dateTimeStamp->format('m')) {
        } elseif ($intervalToday->d > 1) {//“дата в NN:NN ”
            $timeStr = \Yii::app()->getDateFormatter()->format("d MMM в HH:mm", $dateTimeStamp->getTimestamp());
        } elseif ($intervalToday->d > 0) {//“Вчера в NN:NN ”
            $text = \Yii::t($category, 'Yesterday at');
            $timeStr = $dateTimeStamp->format("$text H:i");
        } elseif ($interval->h > 5) {//“Сегодня в NN:NN”
            $text = \Yii::t($category, 'Today at');
            $timeStr = $dateTimeStamp->format("$text H:i");
        } elseif ($interval->h > 0) {//“N часов назад”
            $timeStr = \Yii::t($category, '{n} hours ago|{n} hours ago|{n} hours ago', $interval->h);
        } elseif ($interval->i > 0) {//“N минут назад”
            $timeStr = \Yii::t($category, '{n} minute ago|{n} minutes ago|{n} minutes ago', $interval->i);
        } elseif ($interval->i === 0) {
            $timeStr = \Yii::t($category, 'now');
        }

        return $timeStr;
    }

    /**
     * Return shorter string, no trunk words by $symbolCount
     *
     * @param string $value some string
     * @param int $symbolCount max length of string
     *
     * @return string the formatted result
     * @author elstar
     */
    public function formatStringCut($value, $symbolCount = 60)
    {
        $value = Cast::toStr($value);
        $words = Utf8::explode(" ", $value);
        if (!empty($words)) {
            $newString = "";
            $nowLength = 0;
            foreach ($words as $key => $w) {
                $nowLength = mb_strlen($newString) + mb_strlen($w);
                if ($nowLength < $symbolCount) {
                    if ($key) {
                        $newString .= " ";
                    }
                    $newString .= $w;
                } else {
                    $value = $newString;
                    break;
                }
            }
        }
        return $value;
    }

    /**
     * @param $value
     * @param int $max
     * @param int $start
     * @param bool $encode
     *
     * @return string
     */
    public function formatTextPreview($value, $max = 60, $start = 0, $encode = true)
    {
        $value = Utf8::trim($value);
        if (mb_strlen($value) > $max) {
            $value = mb_substr($value, $start, $max);
            $value .= '...';
        }

        return $encode ? CHtml::encode($value) : $value;
    }

    /**
     * \Yii::app()->format->purifyDescription($content)
     *
     * @param $content
     *
     * @return string
     */
    public function formatPurifyDescription($content)
    {
        $purifier = $this->getHtmlPurifier();
        $purifier->options = $this->purifyDescriptionOptions;
        return $purifier->purify($content);
    }

    /**
     * @param float|int $value
     *
     * @return string
     * @throw InvalidArgumentException
     */
    public function formatNumberLocale($value)
    {
        if (!is_scalar($value)) {
            throw new InvalidArgumentException('Value must be only scalar');
        }

        return \Yii::app()->getNumberFormatter()->formatDecimal($value);
    }
}
