<?php

namespace MommyCom\YiiComponent\MailerServices;

use CLogger;
use Exception;
use GuzzleHttp\ClientInterface;
use MommyCom\Model\Db\MailingDataRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;
use Yii;

/**
 * Class UniSender
 * обертка для unisender_api
 * fixme пока работа только с customList
 * {{hash}} - ссылка на регистрацию через email
 */
class UniSender extends MailerServiceAbstract
{
    public const BASE_URL = 'https://api.unisender.com/ru/api/';

    /**
     * @var ClientInterface $client
     */
    private $client;

    /**
     * @var string $key
     */
    public $key;

    /**
     * @var string $encoding
     */
    public $encoding = 'UTF8';

    /**
     * @var int $retryCount
     */
    public $retryCount = 4;

    /**
     * @var float $timeout
     */
    public $timeout = 5;

    /**
     * @var bool $compression
     */
    public $compression = false;

    /**
     * ID известного и существующего списока пользователей
     *
     * @var int $listId
     */
    public $listId = false;

    /**
     * UniSender constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client, $APIkey)
    {
        $this->client = $client;
        $this->key = $APIkey;
    }

    /**
     * @param array ...$args
     */
    public function init(...$args)
    {
        $this->listId = $args[0];
    }

    /**
     * подписать адресата на один или несколько списков рассылки
     *
     * @param MailingDataRecord $user
     * @param int $double_optin
     *
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function subscribe($user, $double_optin = 2)
    {
        $key = $this->listId;

        if (empty($key)) {
            return false;
        }

        $params = [
            'list_ids' => $key,
            'double_optin' => $double_optin,
            'fields' => [
                'email' => $user->email,
            ],

        ];

        return $this->callMethod('subscribe', $params);
    }

    /**
     * @param MailerServiceUserModel[] $users
     * @param array $options
     *
     * @return array|bool
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function importContacts(array $users, array $options)
    {
        $data = [];
        $key = $this->listId;

        if (empty($key) || empty($users)) {
            return false;
        }

        if (!isset($users[0]) || !($users[0] instanceof MailerServiceUserModel)) {
            throw new Exception('Пользователь должен быть класса MailerServiceUserModel');
        }

        /** @var $users MailerServiceUserModel[] */
        foreach ($users as $user) {
            $emailStatus = $user->isActive ? 'active' : 'new';
            $name = $user->name;

            $data[] = [
                isset($options['delete']) && $options['delete'] ? 1 : 0,
                $emailStatus,
                $user->email,
                $name,
                $key
            ];
        }

        $params = [
            'double_optin' => 1, //подписчик активный и ему не приходит письмо для подтверждения
            'field_names' => [
                'delete',
                'email_status',
                'email',
                'Name',
                'email_list_ids',
            ],
            'data' => $data,
            'overwrite_lists' => isset($options['overwrite']) && $options['overwrite'] ? 1 : 0
        ];

        return $this->callMethod('importContacts', $params);
    }

    /**
     * исключить адресата из списков рассылки
     * Мы рекомендуем использовать метод exclude в случае, когда управление подпиской/отпиской выполняется по инициативе
     * отправителя (то есть вас, автора рассылки), и метод unsubscribe в случае, когда сам подписчик отказывается от подписки.
     *
     * @param UserRecord $user
     *
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exclude($user)
    {
        $key = $this->listId;

        if (empty($key)) {
            return false;
        }

        $Params = [
            'list_ids' => $key,
            'contact_type' => 'email',
            'contact' => $user->email,
        ];

        return $this->callMethod('exclude', $Params);
    }

    /**
     * исключить адресата из списков рассылки по email
     *
     * @param string $email
     *
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function excludeEmail($email)
    {
        $key = $this->listId;
        $email = trim(strval($email));

        if (empty($key)) {
            return false;
        }

        $Params = [
            'list_ids' => $key,
            'contact_type' => 'email',
            'contact' => $email,
        ];

        return $this->callMethod('exclude', $Params);
    }

    /**
     * не использовать!!!
     * отписать адресата от рассылки (потом нельзя подписать из сайта)
     * Мы рекомендуем использовать метод exclude в случае, когда управление подпиской/отпиской выполняется по инициативе
     * отправителя (то есть вас, автора рассылки), и метод unsubscribe в случае, когда сам подписчик отказывается от подписки.
     * Вернуть статус на «активный» через API нельзя – это может сделать только сам подписчик, перейдя по ссылке
     * активации из письма.
     *
     * @param UserRecord $user
     *
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unsubscribe($user)
    {
        $key = $this->listId;

        if (empty($key)) {
            return false;
        }

        $Params = [
            'list_ids' => $key,
            'contact_type' => 'email',
            'contact' => $user->email,
        ];

        return $this->callMethod('unsubscribe', $Params);
    }

    /**
     * экспорт всех данных контактов
     *
     * @link https://support.unisender.com/index.php?/Knowledgebase/Article/View/61/0/exportcontacts
     *
     * @param int $offset
     * @param int $limit
     * @param string $emailStatus new, invited, active, inactive, unsubscribed, blocked, activation_requested
     *
     * @return MailerServiceUserModel[]|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportContacts(int $offset = 0, int $limit = 500, string $emailStatus = '')
    {
        $offset = Cast::toUInt($offset);
        $limit = Cast::toUInt($limit);
        $users = [];

        $emailStatusIn = [
            'new', //новый.
            'invited', //отправлено приглашение со ссылкой подтверждения подписки, ждём ответа, рассылка по такому адресу пока невозможна.
            'active', //активный адрес, возможна рассылка.
            'inactive', //адрес отключён через веб-интерфейс, никакие рассылки невозможны, но можно снова включить через веб-интерфейс.
            'unsubscribed', //адресат отписался от всех рассылок.
            'blocked', //адрес заблокирован администрацией нашего сервиса (например, по жалобе адресата), рассылка по нему невозможна. Разблокировка возможна только по просьбе самого адресата.
            'activation_requested', //запрошена активация адреса у администрации UniSender, рассылка пока невозможна.'
        ];

        $key = Cast::toStr($this->listId);

        if (empty($key)) {
            return false;
        }

        $params = [
            'list_ids' => $key,
            'offset' => $offset,
            'limit' => $limit,
        ];

        if (!empty($options['emailStatus']) && in_array($options['emailStatus'], $emailStatusIn)) {
            $params += [
                'email_status' => $options['emailStatus'],
            ];
        }

        $response = $this->callMethod('exportContacts', $params);
        if (isset($response['result']) && empty($response['error'])) {
            $users = $this->_convertResult($response['result']);
        }

        return $users;
    }

    /**
     * @param UserRecord $user
     *
     * @return null|MailerServiceUserModel|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUserInfo(UserRecord $user)
    {
        $email = $user->email;
        $userResponse = null;

        $key = Cast::toStr($this->listId);

        if (empty($key)) {
            return false;
        }

        $Params = [
            'email' => $email,
        ];

        $response = $this->callMethod('exportContacts', $Params);

        if (isset($response['result']) && empty($response['error'])) {
            $users = $this->_convertResult($response['result']);
            if (isset($users[0])) {
                $userResponse = $users[0];
            }
        }

        return $userResponse;
    }

    /**
     * @param string $email
     *
     * @return bool|MailerServiceUserModel|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUserInfoByEmail($email)
    {
        $userResponse = null;

        $key = Cast::toStr($this->listId);

        if (empty($key)) {
            return false;
        }

        $Params = [
            'email' => $email,
        ];

        $response = $this->callMethod('exportContacts', $Params);

        if (isset($response['result']) && empty($response['error'])) {
            $users = $this->_convertResult($response['result']);
            if (isset($users[0])) {
                $userResponse = $users[0];
            }
        }

        return $userResponse;
    }

    /**
     * @param string $senderName
     * @param string $senderEmail
     * @param string $subject
     * @param string $body
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createEmailMessage($senderName, $senderEmail, $subject, $body)
    {
        $senderName = Utf8::trim($senderName);
        $senderEmail = Utf8::trim($senderEmail);
        $key = Cast::toStr($this->listId);
        $result = false;

        if (empty($key)) {
            return false;
        }

        $Params = [
            'list_id' => $key,
            'sender_name' => $senderName,
            'sender_email' => $senderEmail,
            'subject' => $subject,
            'body' => $body,
        ];

        $response = $this->callMethod('createEmailMessage', $Params);

        if (isset($response['error'])) {
            Yii::log("Ошибка создания письма на Юнисендере. Ошибки: " . print_r($response, true), CLogger::LEVEL_ERROR);
        }

        if (isset($response['result']) && empty($response['error'])) {
            $result = $response['result']['message_id'];
        }

        return $result;
    }

    /**
     * @param $messageId
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteMessage($messageId)
    {
        $messageId = Cast::toUInt($messageId);
        $result = false;

        $Params = [
            'message_id' => $messageId,
        ];

        $response = $this->callMethod('deleteMessage', $Params);

        if (isset($response['result']) && empty($response['error'])) {
            $result = true;
        }

        return $result;
    }

    /**
     * @param int $messageId
     * @param int $startAt
     *
     * @return MailerServiceCampaignModel|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createCampaign($messageId, $startAt = 0)
    {
        $messageId = Cast::toUInt($messageId);
        $startAt = Cast::toUInt($startAt);
        $startAt = $startAt > time() ? $startAt : 0;
        $result = null;
        $statusReplacements = [
            'scheduled' => MailerServiceCampaignModel::STATUS_SCHEDULED, //'рассылка поставлена очередь и будет отправлена, как только наступит время',
            'waits_censor' => MailerServiceCampaignModel::STATUS_WAITS_CENSOR, //'рассылка ожидает проверки администратором',
            'waits_schedule' => MailerServiceCampaignModel::STATUS_WAITS_SCHEDULE, //'задача на отправку рассылки запомнена системой и будет обработана',
        ];

        $Params = [
            'message_id' => $messageId,
            'track_read' => 1, //отслеживать ли факт прочтения e-mail сообщения
            'track_links' => 1, //отслеживать ли переходы по ссылкам в e-mail сообщениях
            'track_ga' => 0, //включить ли для данной рассылки интеграцию с Google Analytics/Яндекс.Метрика
            'defer' => 1, //Рекомендуется всегда устанавливать это значение в 1
        ];

        if ($startAt > 0) {
            $Params += [
                'start_time' => date('Y-m-d H:i', $startAt),
            ];
        }

        $response = $this->callMethod('createCampaign', $Params);

        if (isset($response['result']) && empty($response['error'])) {
            $result = $response['result'];
            $status = isset($result['status']) ? $result['status'] : MailerServiceCampaignModel::STATUS_UNKNOWN;

            $model = new MailerServiceCampaignModel();
            $model->id = isset($result['campaign_id']) ? $result['campaign_id'] : 0;
            $model->status = isset($statusReplacements[$status]) ? $statusReplacements[$status] : $status;
            $model->countSends = isset($result['count']) ? $result['count'] : 0;

            $result = $model;
        }

        return $result;
    }

    /**
     * @param $campaignId
     *
     * @return MailerServiceCampaignModel|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCampaignStatus($campaignId)
    {
        $campaignId = Cast::toUInt($campaignId);
        $result = null;
        $statusReplacements = [
            'scheduled' => MailerServiceCampaignModel::STATUS_SCHEDULED,
            'waits_censor' => MailerServiceCampaignModel::STATUS_WAITS_CENSOR,
            'censor_hold' => MailerServiceCampaignModel::STATUS_WAITS_CENSOR,
            'waits_schedule' => MailerServiceCampaignModel::STATUS_WAITS_SCHEDULE,
            'declined' => MailerServiceCampaignModel::STATUS_DECLINED,
            'in_progress' => MailerServiceCampaignModel::STATUS_PROGRESS,
            'analysed' => MailerServiceCampaignModel::STATUS_ANALYSED,
            'completed' => MailerServiceCampaignModel::STATUS_COMPLETED,
            'stopped' => MailerServiceCampaignModel::STATUS_PAUSED,
            'canceled' => MailerServiceCampaignModel::STATUS_CANCELED,
        ];

        $Params = [
            'campaign_id' => $campaignId,
        ];

        $response = $this->callMethod('getCampaignStatus', $Params);

        if (isset($response['result']) && empty($response['error'])) {
            $result = $response['result'];
            $status = isset($result['status']) ? $result['status'] : MailerServiceCampaignModel::STATUS_UNKNOWN;
            $campaignStartAt = isset($result['start_time']) ? strtotime($result['start_time']) : 0;
            $campaignCreatedAt = isset($result['creation_time']) ? strtotime($result['creation_time']) : 0;

            $model = new MailerServiceCampaignModel();
            $model->id = isset($result['campaign_id']) ? $result['campaign_id'] : 0;
            $model->status = isset($statusReplacements[$status]) ? $statusReplacements[$status] : $status;
            $model->startAt = $campaignStartAt;
            $model->createdAt = $campaignCreatedAt;

            $result = $model;
        }

        return $result;
    }

    /**
     * @param int $campaignId
     * @param int $updatedAt Unix Timestamp. Возвращать все статусы адресов, изменившиеся начиная с указанного времени включительно
     *
     * @return MailerServiceCampaignItemModel[]|array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCampaignDeliveryStats($campaignId, $updatedAt = 0)
    {
        $campaignId = Cast::toUInt($campaignId);
        $updatedAt = Cast::toUInt($updatedAt);

        $replacementsStatus = [
            'not_sent' => MailerServiceCampaignItemModel::STATUS_NOT_SENT,
            'ok_sent' => MailerServiceCampaignItemModel::STATUS_SENT, //новый которого нет в API
            'ok_delivered' => MailerServiceCampaignItemModel::STATUS_DELIVERED,
            'ok_read' => MailerServiceCampaignItemModel::STATUS_READ,
            'ok_spam_folder' => MailerServiceCampaignItemModel::STATUS_SPAM,
            'ok_link_visited' => MailerServiceCampaignItemModel::STATUS_LINK,
            'ok_unsubscribed' => MailerServiceCampaignItemModel::STATUS_UNSUBSCRIBED,
            'err_user_unknown' => MailerServiceCampaignItemModel::STATUS_BAD_EMAIL,
            'err_user_inactive' => MailerServiceCampaignItemModel::STATUS_BAD_EMAIL,
            'err_mailbox_full' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_spam_rejected' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_spam_folder' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_delivery_failed' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_will_retry' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_resend' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_domain_inactive' => MailerServiceCampaignItemModel::STATUS_BAD_EMAIL,
            'err_skip_letter' => MailerServiceCampaignItemModel::STATUS_BAD_EMAIL,
            'err_spam_skipped' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_spam_retry' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_unsubscribed' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_src_invalid' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_dest_invalid' => MailerServiceCampaignItemModel::STATUS_BAD_EMAIL,
            'err_not_allowed' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_not_available' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_lost' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'err_internal' => MailerServiceCampaignItemModel::STATUS_ERROR,
            'ok_fbl' => MailerServiceCampaignItemModel::STATUS_SPAM_USER, //новый которого нет в API
        ];

        $replacementsText = $this->_emailStatusReplacements();
        $answer = new \CMap();

        $Params = [
            'campaign_id' => $campaignId,
        ];

        if ($updatedAt > 0) {
            $Params['changed_since'] = gmdate('Y-m-d H:i:s', $updatedAt);
        }

        $response = $this->callMethod('getCampaignDeliveryStats', $Params);

        if (isset($response['result']) && empty($response['error'])) {
            $result = $response['result'];

            $fieldNames = array_map(function ($value) {
                if (is_string($value)) {
                    $value = mb_strtolower($value);
                }

                return $value;
            }, $result['fields']);

            $emailKey = array_search('email', $fieldNames);
            $resultKey = array_search('send_result', $fieldNames);

            if (isset($result['data']) && is_array($result['data'])) {
                $datas = $result['data'];

                foreach ($datas as $id => $data) {
                    $statusKey = isset($data[$resultKey]) ? $data[$resultKey] : '';
                    $status = isset($replacementsStatus[$statusKey]) ? $replacementsStatus[$statusKey] : MailerServiceCampaignItemModel::STATUS_UNKNOWN;
                    $text = isset($replacementsText[$statusKey]) ? $replacementsText[$statusKey] : 'Неизвестная ошибка';
                    $email = $emailKey === false ? '' : trim($data[$emailKey]);

                    $model = new MailerServiceCampaignItemModel();
                    $model->id = $id;
                    $model->email = $email;
                    $model->status = $status;
                    $model->status_text = $text;

                    $answer->add($email, $model);
                }
            }
        }

        return $answer->toArray();
    }

    /**
     * @link https://support.unisender.com/index.php?/Knowledgebase/Article/View/65/0/getcampaigns
     *
     * @param int $startAt unix timestamp
     * @param int $endAt unix timestamp
     *
     * @return null|MailerServiceCampaignModel[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCampaigns($startAt = 0, $endAt = 0)
    {
        $startAt = Cast::toUInt($startAt);
        $endAt = Cast::toUInt($endAt);

        $result = null;

        $Params = [];

        if ($startAt > 0) {
            $Params += [
                'from' => date('Y-m-d H:i:s', $startAt),
            ];
        }

        if ($endAt > 0 && $endAt > $startAt) {
            $Params += [
                'to' => date('Y-m-d H:i:s', $endAt),
            ];
        }

        $response = $this->callMethod('getCampaigns', $Params);

        $statusReplacements = [
            'scheduled' => MailerServiceCampaignModel::STATUS_SCHEDULED,
            'waits_censor' => MailerServiceCampaignModel::STATUS_WAITS_CENSOR,
            'censor_hold' => MailerServiceCampaignModel::STATUS_WAITS_CENSOR,
            'waits_schedule' => MailerServiceCampaignModel::STATUS_WAITS_SCHEDULE,
            'declined' => MailerServiceCampaignModel::STATUS_DECLINED,
            'in_progress' => MailerServiceCampaignModel::STATUS_PROGRESS,
            'analysed' => MailerServiceCampaignModel::STATUS_ANALYSED,
            'completed' => MailerServiceCampaignModel::STATUS_COMPLETED,
            'stopped' => MailerServiceCampaignModel::STATUS_PAUSED,
            'canceled' => MailerServiceCampaignModel::STATUS_CANCELED,
        ];

        if (isset($response['result'])) {
            $items = $response['result'];
            foreach ($items as $item) {
                $model = new MailerServiceCampaignModel();
                $model->id = $item['id'];
                $model->status = isset($item['status']) && isset($statusReplacements[$item['status']]) ? $statusReplacements[$item['status']] : MailerServiceCampaignModel::STATUS_UNKNOWN;
                $model->startAt = isset($item['start_time']) ? (int)strtotime($item['start_time']) : 0;

                $result[] = $model;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function _emailStatusReplacements()
    {
        return [
            'not_sent' => 'Сообщение еще не было обработано',
            'ok_sent' => 'Сообщение отправлено',
            'ok_delivered' => 'Сообщение доставлено',
            'ok_read' => 'Сообщение доставлено и зарегистрировано его прочтение',
            'ok_spam_folder' => 'Сообщение доставлено, но почтовый сервер поместил в спам',
            'ok_fbl' => 'Сообщение доставлено, но помещено в папку "спам" получателем',
            'ok_link_visited' => 'Сообщение доставлено, прочитано и выполнен переход по одной из ссылок',
            'ok_unsubscribed' => 'Сообщение доставлено и прочитано, но пользователь отписался по ссылке в письме',
            'err_user_unknown' => 'Адрес не существует, доставка не удалась',
            'err_user_inactive' => 'Адрес когда-то существовал, но сейчас отключен',
            'err_mailbox_full' => 'Почтовый ящик получателя переполнен',
            'err_spam_rejected' => 'Письмо отклонено сервером как спам.',
            'err_spam_folder' => 'Письмо помещено в папку со спамом почтовой службой',
            'err_delivery_failed' => 'Доставка не удалась по иным причинам',
            'err_will_retry' => 'Попытка(и) доставки оказалась(ись) неудачной(имы), но попытки продолжаются',
            'err_resend' => 'Попытка(и) доставки оказалась(ись) неудачной(имы), но попытки продолжаются',
            'err_domain_inactive' => 'Домен не принимает почту или не существует',
            'err_skip_letter' => 'Адресат не является активным - он отключён или заблокирован',
            'err_spam_skipped' => 'Большая часть рассылки попала в cпам и остальные письма отправлять не имеет смысла',
            'err_spam_retry' => 'Был в спаме но теперь его нужно переотправить',
            'err_unsubscribed' => 'Отправка не выполнялась, т.к. адрес, по которому пытались отправить письмо, ранее отписался',
            'err_src_invalid' => 'Неправильный адрес отправителя',
            'err_dest_invalid' => 'Неправильный адрес получателя',
            'err_not_allowed' => 'Возможность отправки писем заблокирована системой',
            'err_not_available' => 'Адрес, по которому пытались отправить письмо, не является доступным',
            'err_lost' => 'Письмо было утеряно из-за сбоя на стороне сервиса',
            'err_internal' => 'Сбой сервиса, переотправка письма отправителем не должна осуществляться',
        ];
    }

    /**
     * @param $result
     *
     * @return MailerServiceUserModel[]
     */
    protected function _convertResult($result)
    {
        $users = new \CMap();

        $fieldNames = array_map(function ($value) {
            if (is_string($value)) {
                $value = mb_strtolower($value);
            }

            return $value;
        }, $result['field_names']);

        $emailKey = array_search('email', $fieldNames);
        $nameKey = array_search('name', $fieldNames);
        $emailStatusKey = array_search('email_status', $fieldNames);
        $emailListIdsKey = array_search('email_list_ids', $fieldNames);

        foreach ($result['data'] as $userData) {
            $emailStatus = is_bool($emailStatusKey) ? '' : $userData[$emailStatusKey];

            $isActive = in_array($emailStatus, ['active']);
            $isSubscribed = in_array($emailStatus, ['new', 'invited', 'active', 'activation_requested']);
            $isPossiblySubscribe = !in_array($emailStatus, ['unsubscribed', 'blocked']);

            $user = new MailerServiceUserModel();
            $user->email = is_bool($emailKey) ? '' : $userData[$emailKey];
            $user->name = is_bool($nameKey) ? '' : $userData[$nameKey];
            $user->isActive = $isActive;
            $user->isSubscribed = $isSubscribed;
            $user->isPossiblySubscribe = $isPossiblySubscribe;

            if ($emailListIdsKey !== false) {
                $user->setLists($userData[$emailListIdsKey]);
            }

            $users->add(null, $user);
        }

        return $users->toArray();
    }

    /**
     * @param string $methodName
     * @param array $params
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function callMethod($methodName, $params = [])
    {
        $url = $methodName . '?format=json';
        $requestParams = [
            'api_key' => $this->key,
        ];

        if (array_key_exists('list_ids', $params)) {
            $requestParams = array_merge($requestParams, ['list_ids' => $params['list_ids']]);
        }

        $requestParams = array_merge($requestParams, $params);
        $response = $this->client->request('POST', $url, [
            'form_params' => $requestParams
        ]);
        $response = json_decode($response->getBody(), true);

        return $response;
    }

    /**
     * @param string $Value
     * @param string $Key
     */
    protected function iconv(&$Value, $Key)
    {
        $Value = iconv($this->encoding, 'UTF8//IGNORE', $Value);
    }

    /**
     * @param string $Value
     * @param string $Key
     */
    protected function mb_convert_encoding(&$Value, $Key)
    {
        $Value = mb_convert_encoding($Value, 'UTF8', $this->encoding);
    }

    /**
     * @return array|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContactCount()
    {
        $key = $this->listId;

        if (empty($key)) {
            return false;
        }

        $Params = [
            'list_id' => $key,
            'params' => [
                'type' => 'address',
            ],
        ];

        return $this->callMethod('getContactCount', $Params);
    }

    /**
     * @param array $result
     * @return boolean
     */
    function isErrorResult(array $result)
    {
        if (empty($result['error'])) {
            return false;
        }

        return true;
    }

    /**
     * @param array ...$args
     * @return int|null
     */
    public function getListId(...$args)
    {
        $country = $args[0];
        $eventName = $args[1];

        return \Yii::app()->params['unisender']['lists'][$country][$eventName];
    }

    /**
     * @return array
     */
    public function getLists()
    {
        return \Yii::app()->params['unisender']['lists'];
    }
}
