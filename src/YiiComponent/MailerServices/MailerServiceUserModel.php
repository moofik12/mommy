<?php

namespace MommyCom\YiiComponent\MailerServices;

/**
 * Class MailerServiceUserModel
 * @package MommyCom\YiiComponent\MailerServices
 */
class MailerServiceUserModel
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var bool
     */
    public $isActive = false;

    /**
     * текущее состояние
     *
     * @var bool
     */
    public $isSubscribed = false;

    /**
     * возможна ли подписка
     *
     * @var bool
     */
    public $isPossiblySubscribe = true;

    /**
     * @var array
     */
    private $_lists = [];

    /**
     * Списки в которых находится пользователь
     *
     * @return array
     */
    public function getLists()
    {
        return $this->_lists;
    }

    /**
     * @param string|array $value
     * @param string $delimiter
     */
    public function setLists($value, $delimiter = ',')
    {
        if (is_scalar($value)) {
            $this->_lists = explode($delimiter, $value);
        } elseif (is_array($value)) {
            $this->_lists = $value;
        }
    }
}
