<?php

namespace MommyCom\YiiComponent\MailerServices;

use CApplicationComponent;
use CException;
use MommyCom\Model\Db\UserRecord;
use Yii;

class MailerServices extends CApplicationComponent
{
    /**
     * список подключенных сервисов
     */
    private $_services;

    /**
     * сонфиг для всех сервисов, где $key - сервис(self::_aliasServices), $value array уникальных параметров
     * <pre>
     * 'config' => array(
     *        'unisender' => array(
     *          'key' => 'hash',
     *          'login' => 'test',
     *      ),
     * ),
     * </pre>
     *
     * @var array
     */
    public $config = [];

    /**
     * @var array $key -> alias, $value -> class name for components/__CLASS__
     */
    public $aliasServices = [
        'unisender' => 'UniSender',
    ];

    /**
     * список сервисов через которые будет отправка или синхронизация
     *
     * @var array
     */
    public $activeService = [];

    /**
     * @var string
     */
    public $pathToComponents = 'common.components.MailerServices';

    public function init()
    {
        foreach ($this->config as $service => $config) {
            if (!isset($this->aliasServices[$service])) {
                throw new CException("service: $service not support");
            } elseif (isset($config['class'])) {
                throw new CException("config['class'] not support");
            }

            $config['class'] = $this->pathToComponents . '.' . $this->aliasServices[$service];

            $objectService = Yii::createComponent($config);
            $objectService->init();

            $this->_services[$service] = $objectService;
        }

        parent::init();
    }

    /**
     * @param $name
     *
     * @return null
     */
    public function getService($name)
    {
        if (isset($this->_services[$name])) {
            return $this->_services[$name];
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->_services;
    }

    /**
     * @return array
     */
    public function getServicesAlias()
    {
        return array_keys($this->aliasServices);
    }

    /**
     * @param UserRecord $user
     *
     * @return mixed
     */
    public function subscribe(UserRecord $user)
    {
        return $this->_runMethod('subscribe', $user);
    }

    /**
     * @param UserRecord $user
     *
     * @return mixed
     */
    public function exclude(UserRecord $user)
    {
        return $this->_runMethod('exclude', $user);
    }

    /**
     * @param string $email
     *
     * @return mixed
     */
    public function excludeEmail($email)
    {
        return $this->_runMethod('excludeEmail', $email);
    }

    /**
     * отписать адресата от рассылки (предпочтительнее exclude)
     * в некоторых сервисах потом нельзя подписать через API
     *
     * @param UserRecord $user
     *
     * @return mixed
     */
    public function unsubscribe(UserRecord $user)
    {
        return $this->_runMethod('unsubscribe', $user);
    }

    /**
     * экспорт всех данных контактов
     *
     * @param string $service
     * @param int $offset
     * @param int $limit
     *
     * @return mixed
     */
    public function exportContacts($service, $offset = 0, $limit = 50)
    {
        if (is_string($service) && !isset($this->_services[$service])) {
            return false;
        }

        if (is_object($service) && !($service instanceof MailerServiceAbstract)) {
            return false;
        }

        $objectService = $this->_services[$service];

        return $objectService->exportContacts($offset, $limit);
    }

    /**
     * @param array $users UserRecords[]
     *
     * @return mixed
     */
    public function importContacts(array $users)
    {
        return $this->_runMethod('importContacts', $users);
    }

    /**
     * @throws CException
     */
    protected function _runMethod()
    {
        $args = func_get_args();
        $result = [];

        if (!isset($args[0]) || empty($args[0])) {
            throw new CException('not method to call for mail service');
        }

        $name = $args[0];
        unset($args[0]);

        foreach ($this->_services as $service => $object) {
            if (in_array($service, $this->activeService)) {
                $result[$service] = call_user_func_array([$object, $name], $args);
            }
        }

        return $result;
    }
}
