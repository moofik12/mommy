<?php

namespace MommyCom\YiiComponent\MailerServices;

use MommyCom\Model\Db\MailingDataRecord;

abstract class MailerServiceAbstract
{
    /**
     * инициализация необходимых действий
     *
     * @param array ...$args
     */
    abstract function init(...$args);

    /**
     * @param MailingDataRecord $user
     *
     * @return array|bool
     */
    abstract function subscribe($user);

    /**
     * @param MailingDataRecord $user
     *
     * @return array|bool
     */
    abstract function exclude($user);

    /**
     * отписать адресата от рассылки (предпочтительнее exclude)
     * в некоторых сервисах потом нельзя подписать через API
     *
     * @param MailingDataRecord $user
     *
     * @return array|bool
     */
    abstract function unsubscribe($user);

    /**
     * экспорт всех данных контактов
     *
     * @param int $offset
     * @param int $limit
     * @param string $emailStatus
     *
     * @return array MailerServiceUserModel[]
     */
    abstract function exportContacts(int $offset = 0, int $limit = 500, string $emailStatus = '');

    /**
     * @param MailerServiceUserModel[] $users
     * @param array $options
     *
     * @return array|bool
     */
    abstract function importContacts(array $users, array $options);

    /**
     * @param string $senderName
     * @param string $senderEmail
     * @param string $subject
     * @param string $body
     *
     * @return bool
     */
    abstract function createEmailMessage($senderName, $senderEmail, $subject, $body);

    /**
     * @param $messageId
     *
     * @return bool
     */
    abstract function deleteMessage($messageId);

    /**
     * создание рассылки
     *
     * @param int $messageId
     * @param int $startAt 0 - начать немедленно
     *
     * @return MailerServiceCampaignModel|null
     */
    abstract function createCampaign($messageId, $startAt = 0);

    /**
     * @param $campaignId
     *
     * @return MailerServiceCampaignModel|null
     */
    abstract function getCampaignStatus($campaignId);

    /**
     * Проверяет результат возвращаенный API на наличие ошибок
     * @param array $result
     *
     * @return boolean
     */
    abstract function isErrorResult(array $result);

    /**
     * Возвращает id списка на который нужно подписать пользователя
     * @param array ...$args
     *
     * @return int|null
     */
    abstract function getListId(...$args);

    /**
     * Возвращает все возможные списки для подписки пользователей
     *
     * @return array
     */
    abstract function getLists();
}
