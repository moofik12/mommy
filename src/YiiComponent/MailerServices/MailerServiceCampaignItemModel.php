<?php

namespace MommyCom\YiiComponent\MailerServices;

/**
 * Class MailerServiceCampaignItemModel
 *
 * @property-read $statusReplacement
 */
class MailerServiceCampaignItemModel
{
    const STATUS_NOT_SENT = 0,
        STATUS_DELIVERED = 1,
        STATUS_READ = 2,
        STATUS_SPAM = 3, //сервер понял как спам
        STATUS_LINK = 4,
        STATUS_UNSUBSCRIBED = 5,
        STATUS_ERROR = 6,
        STATUS_UNKNOWN = 7,
        STATUS_BAD_EMAIL = 8,
        STATUS_SPAM_USER = 9, //пользователь поместил в спам
        STATUS_SENT = 10;

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $status_text;

    /**
     * @var string;
     */
    public $email;

    /**
     * дата создания расслыки
     *
     * @var integer
     */
    public $updatedAt = 0;

    public function statusReplacements()
    {
        return [
            self::STATUS_NOT_SENT => 'не оперелен',
            self::STATUS_DELIVERED => 'доставленно',
            self::STATUS_READ => 'прочитано',
            self::STATUS_SPAM => 'доставлено (сервер поместил в спам)',
            self::STATUS_LINK => 'перешел',
            self::STATUS_ERROR => 'не отправленно',
            self::STATUS_UNSUBSCRIBED => 'отписался',
            self::STATUS_UNKNOWN => 'неизвестная ошибка',
            self::STATUS_BAD_EMAIL => 'email не существует или отключен',
            self::STATUS_SPAM_USER => 'доставлено (пользователь поместил в спам)',
            self::STATUS_SENT => 'отправлено',
        ];
    }

    public function statusReplacement()
    {
        $statuses = $this->statusReplacements();
        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $this->status;
    }
}
