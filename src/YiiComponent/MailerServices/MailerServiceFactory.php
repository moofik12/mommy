<?php

namespace MommyCom\YiiComponent\MailerServices;

use GuzzleHttp\Client;

/**
 * Class MailerServiceFactory
 * @package MommyCom\YiiComponent\MailerServices
 */
class MailerServiceFactory
{
    /**
     * Создает сервис для работы с Unisender
     *
     * @param string $apiKey
     * @return MailerServiceAbstract
     */
    public static function createUnisenderService(string $apiKey): MailerServiceAbstract
    {
        $client = new Client([
            'base_uri' => UniSender::BASE_URL,
            'header' => 'Content-type: application/x-www-form-urlencoded'
        ]);

        return new UniSender($client, $apiKey);
    }
}
