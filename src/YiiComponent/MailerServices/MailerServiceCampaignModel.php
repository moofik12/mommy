<?php

namespace MommyCom\YiiComponent\MailerServices;

class MailerServiceCampaignModel
{
    const STATUS_WAITS_CENSOR = 0, //рассылка ожидает проверки
        STATUS_DECLINED = 1, //рассылка отклонена администратором
        STATUS_WAITS_SCHEDULE = 2, //рассылка ждёт постановки в очередь
        STATUS_SCHEDULED = 3, //рассылка запланирована к запуску
        STATUS_PROGRESS = 4, //рассылка выполняется
        STATUS_ANALYSED = 5, //все сообщения отправлены, идёт анализ результатов
        STATUS_COMPLETED = 6, //все сообщения отправлены и анализ результатов закончен
        STATUS_PAUSED = 7, //рассылка поставлена "на паузу"
        STATUS_CANCELED = 8, //рассылка отменена
        STATUS_UNKNOWN = 9; //не получилось получить статус

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $status = self::STATUS_UNKNOWN;

    /**
     * @var int
     */
    public $countSends = 0;

    /**
     * дата начала рассылки
     *
     * @var integer
     */
    public $startAt = 0;

    /**
     * дата создания расслыки
     *
     * @var integer
     */
    public $createdAt = 0;

    public function statusReplacements()
    {
        return [
            self::STATUS_WAITS_CENSOR => 'рассылка ожидает проверки',
            self::STATUS_DECLINED => 'рассылка отклонена администратором',
            self::STATUS_WAITS_SCHEDULE => 'рассылка ждёт постановки в очередь',
            self::STATUS_SCHEDULED => 'рассылка запланирована к запуску',
            self::STATUS_PROGRESS => 'рассылка выполняется',
            self::STATUS_ANALYSED => 'все сообщения отправлены, идёт анализ результатов',
            self::STATUS_COMPLETED => 'все сообщения отправлены и анализ результатов закончен',
            self::STATUS_PAUSED => 'рассылка поставлена "на паузу"',
            self::STATUS_CANCELED => 'рассылка отменена',
            self::STATUS_UNKNOWN => 'не получилось получить статус',
        ];
    }

    public function getStatusReplacement()
    {
        $statuses = $this->statusReplacements();
        if (isset($statuses[$this->status])) {
            return $statuses[$this->status];
        }

        return $this->status;
    }

    public function isActive()
    {
        $result = false;

        if ($this->status == self::STATUS_WAITS_CENSOR
            || $this->status == self::STATUS_WAITS_SCHEDULE
            || $this->status == self::STATUS_SCHEDULED
            || $this->status == self::STATUS_PROGRESS
            || $this->status == self::STATUS_ANALYSED
            || $this->status == self::STATUS_PAUSED
        ) {
            $result = true;
        }

        return $result;
    }
}
