<?php

namespace MommyCom\YiiComponent\Database;

use CDbConnection;
use Doctrine\DBAL\Connection;
use MommyCom\YiiComponent\Facade\Doctrine;

class DbConnection extends CDbConnection
{
    /**
     * @var Connection|null
     */
    private $dbalConnection = null;

    /**
     * @var DbTransaction|null
     */
    private $currentTransaction = null;

    /**
     * @var string|null
     */
    private $driverName = null;

    /**
     * @return Connection
     */
    private function getDbalConnection(): Connection
    {
        if (null === $this->dbalConnection) {
            $this->dbalConnection = Doctrine::getManager()->getConnection();
        }

        return $this->dbalConnection;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->getDbalConnection()->isConnected();
    }

    /**
     * @param bool $value
     */
    public function setActive($value)
    {
        if ($value) {
            $this->getDbalConnection()->connect();
        } else {
            $this->getDbalConnection()->close();
        }
    }

    /**
     * @return \Doctrine\DBAL\Driver\Connection
     */
    public function getPdoInstance()
    {
        $connection = $this->getDbalConnection();
        $connection->connect();

        return $connection;
    }

    /**
     * @return DbTransaction|null
     */
    public function getCurrentTransaction()
    {
        return ($this->currentTransaction && $this->currentTransaction->getActive()) ? $this->currentTransaction : null;
    }

    /**
     * @return DbTransaction
     */
    public function beginTransaction()
    {
        return $this->currentTransaction = new DbTransaction($this, $this->getDbalConnection());
    }

    /**
     * @param string $sequenceName
     *
     * @return string
     */
    public function getLastInsertID($sequenceName = '')
    {
        return $this->getDbalConnection()->lastInsertId($sequenceName ?: null);
    }

    /**
     * @param mixed $str
     *
     * @return mixed
     */
    public function quoteValue($str)
    {
        if (is_int($str) || is_float($str)) {
            return $str;
        }

        return $this->getDbalConnection()->quote($str);
    }

    /**
     * @param mixed $value
     * @param int $type
     *
     * @return mixed
     */
    public function quoteValueWithType($value, $type)
    {
        return $this->getPdoInstance()->quote($value, $type);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function quoteTableName($name)
    {
        return $this->getDbalConnection()->quoteIdentifier($name);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function quoteColumnName($name)
    {
        return $this->getDbalConnection()->quoteIdentifier($name);
    }

    /**
     * @return string
     */
    public function getDriverName()
    {
        if (null !== $this->driverName) {
            return $this->driverName;
        }

        $name = $this->getDbalConnection()->getDriver()->getName();

        if ('pdo_' === substr($name, 0, 4)) {
            $name = substr($name, 4);
        }

        return $this->driverName = $name;
    }

    /**
     * {@inheritdoc}
     *
     * @deprecated использовалось внутрии Yii
     */
    public function setDriverName($driverName)
    {
        throw new \LogicException(__METHOD__ . ' should not be reached.');
    }

    /**
     * @param int $name
     *
     * @return mixed
     */
    public function getAttribute($name)
    {
        $pdoInstance = $this->getPdoInstance();

        if ($pdoInstance instanceof \PDO) {
            return $pdoInstance->getAttribute($name);
        }

        return null;
    }

    /**
     * @param int $name
     * @param mixed $value
     */
    public function setAttribute($name, $value)
    {
        $pdoInstance = $this->getPdoInstance();

        if ($pdoInstance instanceof \PDO) {
            $pdoInstance->setAttribute($name, $value);
        }
    }
}
