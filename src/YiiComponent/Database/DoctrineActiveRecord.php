<?php

namespace MommyCom\YiiComponent\Database;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Cache\ArrayStatement;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use MommyCom\YiiComponent\Facade\Doctrine;

abstract class DoctrineActiveRecord extends UnShardedActiveRecord
{
    /**
     * @var string
     */
    private $entityClassName;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var ClassMetadata
     */
    private $entityMetadata;

    /**
     * @var ActiveRecordAttributes
     */
    private $modelAttributes;

    /**
     * @var bool|null
     */
    private $isNewOverride = null;

    /**
     * Collection of accessors per ActiveRecord class
     *
     * @var EntityAccessorAttribute[][]
     */
    private static $attributesByClass = [];

    /**
     * {@inheritdoc}
     */
    public static function model($className = __CLASS__)
    {
        if (func_num_args() > 1) {
            $args = array_slice(func_get_args(), 2); // выглядит как косяк, но так было
            /** @noinspection PhpUnhandledExceptionInspection */
            return (new \ReflectionClass($className))->newInstanceArgs($args);
        }

        if (__CLASS__ === $className) {
            $className = get_called_class();
        }

        return new $className();
    }

    /**
     * {@inheritdoc}
     */
    public function __construct($scenario = 'insert')
    {
        parent::__construct($scenario);

        $objectManager = Doctrine::getManager();
        $className = $this->getEntityClassName();

        $this->entityManager = $objectManager;
        $this->entityRepository = $objectManager->getRepository($className);
        $this->entityMetadata = $objectManager->getClassMetadata($className);

        $attributes = $this->getAttributesByClass($className, $this->entityAttributes());
        $this->modelAttributes = new ActiveRecordAttributes(new $className(), $attributes);
    }

    /**
     * @return static
     */
    public function copy(): self
    {
        $copy = new static(null);
        $copy->setDbCriteria(unserialize(serialize($this->getDbCriteria())));

        $copy->setScenario($this->getScenario());
        $copy->setIsNewRecord(true);
        $copy->modelAttributes->setEntity($this->modelAttributes->cloneEntity());

        $copy->attachBehaviors($this->behaviors());

        return $copy;
    }

    /**
     * @return \CActiveRecordMetaData
     */
    public function getMetaData()
    {
        $metaData = parent::getMetaData();

        // чтобы внутренний $_attributes всегда был пуст
        $metaData->attributeDefaults = [];

        return $metaData;
    }

    private function getAttributesByClass(string $class, iterable $attributes): array
    {
        if (!isset(self::$attributesByClass[$class])) {
            $entityAttributes = [];

            foreach ($attributes as $attribute) {
                $attribute = EntityAccessorAttribute::create(...$attribute);
                $entityAttributes[$attribute->getName()] = $attribute;
            }

            self::$attributesByClass[$class] = $entityAttributes;
        }

        return self::$attributesByClass[$class];
    }

    /**
     * @return string
     */
    protected function getEntityClassName(): string
    {
        if (null !== $this->entityClassName) {
            return $this->entityClassName;
        }

        $className = get_class($this);

        if ('Record' !== substr($className, -6)) {
            throw new \LogicException(
                'This model does not follow the naming convention; you must overwrite the getEntityClassName() method.'
            );
        }

        $className = 'MommyCom\\Entity\\' . substr(strrchr($className, '\\'), 1, -6);

        if (!class_exists($className)) {
            throw new \LogicException(
                'Entity for this model does not exist; you must create one or overwrite getEntityClassName() method.'
            );
        }

        return $this->entityClassName = $className;
    }

    /**
     * @return iterable of arrays [name, get, set]
     */
    abstract protected function entityAttributes(): iterable;

    /**
     * @return ObjectManager
     */
    protected function getEntityManager(): ObjectManager
    {
        return $this->entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        if ($this->modelAttributes->offsetExists($name)) {
            return $this->modelAttributes->offsetGet($name);
        }

        return parent::__get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __isset($name)
    {
        if ($this->modelAttributes->offsetExists($name)) {
            return true;
        }

        return parent::__isset($name);
    }

    /**
     * {@inheritdoc}
     */
    public function __unset($name)
    {
        if ($this->modelAttributes->offsetExists($name)) {
            $this->modelAttributes->offsetUnset($name);
        }

        parent::__unset($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getRelated($name, $refresh = false, $params = [])
    {
        $related = parent::getRelated($name, $refresh, $params);

        if (!$refresh || $params) {
            return $related;
        }

        if ($related instanceof self) {
            $related->refresh();

            return $related;
        }

        if (!is_array($related)) {
            return $related;
        }

        foreach ($related as $model) {
            if ($model instanceof self) {
                $model->refresh();
            }
        }

        return $related;
    }

    /**
     * {@inheritdoc}
     */
    public function tableName()
    {
        return $this->entityMetadata->getTableName();
    }

    /**
     * @return \CDbConnection|DbConnection
     * @throws \CDbException
     */
    public function getDbConnection(): DbConnection
    {
        return parent::getDbConnection();
    }

    /**
     * @return object of Doctrine entity
     */
    public function getEntity()
    {
        return $this->modelAttributes->getEntity();
    }

    /**
     * {@inheritdoc}
     */
    public function hasAttribute($name)
    {
        return $this->modelAttributes->offsetExists($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttribute($name)
    {
        if (!$this->modelAttributes->offsetExists($name)) {
            return null;
        }

        return $this->modelAttributes->offsetGet($name);
    }

    /**
     * {@inheritdoc}
     */
    public function setAttribute($name, $value)
    {
        if (!$this->modelAttributes->offsetExists($name)) {
            return false;
        }

        $this->modelAttributes->offsetSet($name, $value);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeNames()
    {
        return $this->modelAttributes->getAttributesNames();
    }

    /**
     * @param array|mixed $names
     *
     * @return array
     */
    public function getAttributes($names = null)
    {
        if (!is_array($names)) {
            $names = $this->attributeNames();
        }

        return $this->modelAttributes->getAttributes($names);
    }

    /**
     * {@inheritdoc}
     */
    public function unsetAttributes($names = null)
    {
        if (null === $names) {
            $className = $this->getEntityClassName();
            $this->modelAttributes->setEntity(new $className());
            $this->isNewOverride = null;

            return;
        }

        parent::unsetAttributes($names);
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    protected function initializeAttributes(array $values, bool $safeOnly): void
    {
        if (!$this->getIsNewRecord()) {
            throw new \LogicException('Cannot initialize attributes on not new record.');
        }

        if ($safeOnly) {
            $values = array_intersect_key($values, array_flip($this->getSafeAttributeNames()));
        }

        $this->modelAttributes->initAttributes($values);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsNewRecord()
    {
        if (null !== $this->isNewOverride) {
            return $this->isNewOverride;
        }

        return !$this->entityManager->contains($this->modelAttributes->getEntity());
    }

    /**
     * @return true
     */
    public function refresh()
    {
        $this->entityManager->refresh($this->modelAttributes->getEntity());

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function findByPk($pk, $condition = '', $params = [])
    {
        if (!$condition && !$params) {
            $found = $this->entityManager->getUnitOfWork()->tryGetById($pk, $this->getEntityClassName());

            if ($found) {
                $record = $this->createRecord($found);
                $record->afterFind();

                return $record;
            }
        }

        return parent::findByPk($pk, $condition, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function populateRecord($attributes, $callAfterFind = true)
    {
        if (false === $attributes) {
            return null;
        }

        $entity = $this->tryToHydrate($attributes) ?: $this->findEntity($attributes);
        $record = $this->createRecord($entity);

        if ($callAfterFind) {
            $record->afterFind();
        }

        return $record;
    }

    /**
     * @param object|null $entity Doctrine entity or null
     *
     * @return static
     */
    private function createRecord($entity): self
    {
        $record = $this->instantiate([]);
        $record->setScenario('update');
        $record->init();

        if ($entity) {
            $record->modelAttributes->setEntity($entity);
        }

        $record->setOldPrimaryKey($record->getPrimaryKey());
        $record->attachBehaviors($record->behaviors());

        return $record;
    }

    /**
     * @param array $attributes
     *
     * @return object|null
     */
    private function tryToHydrate(array $attributes)
    {
        try {
            $rsm = new ResultSetMappingBuilder($this->entityManager, ResultSetMappingBuilder::COLUMN_RENAMING_NONE);
            $rsm->addRootEntityFromClassMetadata($this->getEntityClassName(), 'r');

            $hydrator = $this->entityManager->newHydrator(Query::HYDRATE_SIMPLEOBJECT);

            $arrayStatement = new ArrayStatement([$attributes]);
            $result = $hydrator->hydrateAll($arrayStatement, $rsm);

            return \count($result) ? \reset($result) : null;
        } catch (ORMException $e) {
            \Yii::log(sprintf('Cannot hydate object "%s". Error: %s', $this->getEntityClassName(), $e->getMessage()));
        }

        return null;
    }

    /**
     * @param array $attributes
     *
     * @return object|null
     */
    private function findEntity(array $attributes)
    {
        $identifier = $this->entityMetadata->identifier;
        $identifier = array_intersect_key($attributes, array_flip($identifier));

        if (!$identifier) {
            return null;
        }

        return $this->entityManager->find($this->getEntityClassName(), $identifier);
    }

    /**
     * {@inheritdoc}
     */
    public function insert($attributes = null)
    {
        if (!$this->getIsNewRecord()) {
            throw new \CDbException(\Yii::t(
                'yii',
                'The active record cannot be inserted to database because it is not new.'
            ));
        }

        $this->isNewOverride = true;

        if (!$this->beforeSave()) {
            return false;
        }

        $entity = $this->modelAttributes->getEntity();
        $this->entityManager->persist($entity);

        $this->setDefaults($entity);

        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $this->entityManager->flush($entity);
        $this->entityManager->refresh($entity);

        $this->setOldPrimaryKey($this->getPrimaryKey());
        $this->afterSave();
        $this->setScenario('update');
        $this->isNewOverride = null;

        return true;
    }

    private function setDefaults($entity)
    {
        foreach ($this->entityMetadata->fieldMappings as $name => $mapping) {
            if (!empty($mapping['id']) || !empty($mapping['nullable'])) {
                continue;
            }

            /** @noinspection PhpUnhandledExceptionInspection */
            $property = new \ReflectionProperty($entity, $name);
            $property->setAccessible(true);
            $value = $property->getValue($entity);
            if (null === $value) {
                $property->setValue($entity, '');
            }
            $property->setAccessible(false);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function update($attributes = null)
    {
        if ($this->getIsNewRecord()) {
            throw new \CDbException(\Yii::t('yii', 'The active record cannot be updated because it is new.'));
        }

        if (!$this->beforeSave()) {
            return false;
        }

        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $this->entityManager->flush($this->modelAttributes->getEntity());

        $this->setOldPrimaryKey($this->getPrimaryKey());
        $this->afterSave();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function saveAttributes($attributes)
    {
        $saved = parent::saveAttributes($attributes);

        if ($saved) {
            foreach ($attributes as $name => $value) {
                if (!is_integer($name) && $this->modelAttributes->offsetExists($name)) {
                    $this->modelAttributes->offsetSet($name, $value);
                }
            }
        }

        return $saved;
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function saveCounters($counters)
    {
        throw new \LogicException(sprintf('Method "%s" is deprecated and cannot be used.', __METHOD__));
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        if ($this->getIsNewRecord()) {
            throw new \CDbException(\Yii::t('yii', 'The active record cannot be deleted because it is new.'));
        }

        if (!$this->beforeDelete()) {
            return false;
        }

        $result = parent::deleteByPk($this->getPrimaryKey()) > 0;
        $this->entityManager->getUnitOfWork()->detach($this->modelAttributes->getEntity());

        $this->afterDelete();

        return $result;
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function updateByPk($pk, $attributes, $condition = '', $params = [])
    {
        return parent::updateByPk($pk, $attributes, $condition, $params);
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function updateAll($attributes, $condition = '', $params = [])
    {
        return parent::updateAll($attributes, $condition, $params);
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function updateCounters($counters, $condition = '', $params = [])
    {
        throw new \LogicException(sprintf('Method "%s" is deprecated and cannot be used.', __METHOD__));
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function deleteByPk($pk, $condition = '', $params = [])
    {
        throw new \LogicException(sprintf('Method "%s" is deprecated and cannot be used.', __METHOD__));
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function deleteAll($condition = '', $params = [])
    {
        return parent::deleteAll($condition, $params);
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function deleteAllByAttributes($attributes, $condition = '', $params = [])
    {
        return parent::deleteAllByAttributes($attributes, $condition, $params);
    }
}
