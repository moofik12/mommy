<?php

namespace MommyCom\YiiComponent\Database;

class ActiveRecordAttributes implements \IteratorAggregate, \ArrayAccess
{
    /**
     * @var object Doctrine entity
     */
    private $entity;

    /**
     * @var EntityAccessorAttribute[]
     */
    private $attributes;

    /**
     * ActiveRecordAttributes constructor.
     *
     * @param object $entity
     * @param EntityAccessorAttribute[] $attributes
     */
    public function __construct($entity, array $attributes)
    {
        $this->entity = $entity;
        $this->attributes = $attributes;
    }

    /**
     * @return object
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param object $entity
     */
    public function setEntity($entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return object
     */
    public function cloneEntity()
    {
        $entity = clone $this->entity;

        try {
            $property = new \ReflectionProperty($entity, 'id');
            $property->setAccessible(true);
            $property->setValue($entity, null);
            $property->setAccessible(false);
        } catch (\ReflectionException $e) {
            //nothing to do
        }

        return $entity;
    }

    /**
     * @internal
     *
     * @param string $name
     *
     * @return bool
     */
    public function isReadable(string $name): bool
    {
        if (!isset($this->attributes[$name])) {
            return false;
        }

        return $this->attributes[$name]->isReadable();
    }

    /**
     * @internal
     *
     * @param string $name
     *
     * @return bool
     */
    public function isWritable(string $name): bool
    {
        if (!isset($this->attributes[$name])) {
            return false;
        }

        return $this->attributes[$name]->isWritable();
    }

    /**
     * @internal
     *
     * @param string $name
     *
     * @return bool
     */
    public function isInitializable(string $name): bool
    {
        if (!isset($this->attributes[$name])) {
            return false;
        }

        return $this->attributes[$name]->isInitializable();
    }

    /**
     * @return string[]
     */
    public function getAttributesNames(): array
    {
        return array_keys($this->attributes);
    }

    /**
     * @param array $names
     *
     * @return array
     */
    public function getAttributes(array $names): array
    {
        $attributes = [];

        foreach ($names as $name) {
            if (!isset($this->attributes[$name])) {
                $value = null;
            } elseif (!$this->attributes[$name]->isReadable()) {
                $value = null;
            } else {
                $value = $this->attributes[$name]->callGet($this->entity);
            }

            $attributes[$name] = $value;
        }

        return $attributes;
    }

    /**
     * @internal для CActiveDataProvider
     * @see \MommyCom\YiiComponent\Database\UnShardedActiveRecord::getDataProvider
     *
     * @param array $attributes
     */
    public function initAttributes(array $attributes): void
    {
        foreach ($attributes as $name => $value) {
            if (!isset($this->attributes[$name])) {
                continue;
            }
            if (!$this->attributes[$name]->isInitializable()) {
                continue;
            }

            $this->attributes[$name]->init($this->entity, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return (function () {
            foreach ($this->attributes as $attribute) {
                if (!$attribute->isReadable()) {
                    continue;
                }

                yield $attribute->getName() => $attribute->callGet($this->entity);
            }
        })();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->attributes[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        if (!isset($this->attributes[$offset])) {
            throw new \RuntimeException(sprintf('Attribute "%s" does not exist.', $offset));
        }

        return $this->attributes[$offset]->callGet($this->entity);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if (!isset($this->attributes[$offset])) {
            throw new \RuntimeException(sprintf('Attribute "%s" does not exist.', $offset));
        }

        $this->attributes[$offset]->callSet($this->entity, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        if (isset($this->attributes[$offset])) {
            $this->attributes[$offset]->callSet($this->entity, null);
        }
    }
}
