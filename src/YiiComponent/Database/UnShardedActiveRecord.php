<?php

namespace MommyCom\YiiComponent\Database;

use CActiveDataProvider;
use CActiveRecord;
use CDbColumnSchema;
use CDbCommand;
use CDbCommandBuilder;
use CDbConnection;
use CDbCriteria;
use CDbExpression;
use CException;
use CJoinElement;
use CJoinQuery;
use Exception;
use MommyCom\YiiComponent\CaseUtils;
use MommyCom\YiiComponent\Type\Cast;
use ReflectionClass;
use Yii;

/**
 * Class UnShardedActiveRecord
 *
 * @property-read \CActiveDataProvider $dataProvider провайдер данных на основе текущей критерии
 */
abstract class UnShardedActiveRecord extends CActiveRecord
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function model($className = __CLASS__)
    {
        if (($n = func_num_args()) > 1) { //copy from YiiBase::createComponent
            $args = func_get_args();
            if ($n === 2)
                $model = new $className($args[1]);
            else if ($n === 3)
                $model = new $className($args[1], $args[2]);
            else if ($n === 4)
                $model = new $className($args[1], $args[2], $args[3]);
            else {
                unset($args[0]);
                unset($args[1]);
                $args = array_values($args);
                $class = new ReflectionClass($className);
                // Note: ReflectionClass::newInstanceArgs() is available for PHP 5.1.3+
                $model = $class->newInstanceArgs($args);
            }
        } else {
            $model = new $className;
        }

        return $model;
    }

    /**
     * Копирует модель с сохранением всех данных
     *
     * @return static
     */
    public function copy()
    {
        $model = clone $this;
        $model->setDbCriteria(unserialize(serialize($this->getDbCriteria())));
        $model->setAttributes($this->getAttributes(), false);
        return $model;
    }

    /**
     * Добавляет в текущую критерию сортировку
     *
     * @param string|array $attr имя атрибута для сортировки
     * @param string $order направление сортировки
     * @param string $pos в какую позицию вставлять, end - в конец, start - в начало
     *
     * @throws CException
     * @return static
     */
    public function orderBy($attr, $order = 'asc', $pos = 'end')
    {
        if (is_array($attr)) {
            foreach ($attr as $subAttr) {
                $this->orderBy($subAttr, $order, $pos);
            }
            return $this;
        }

        static $validOrders = ['asc', 'desc'];
        $order = strtolower($order);
        if (!in_array($order, $validOrders)) {
            $order = reset($validOrders);
        }

        $query = '';
        if (strpos($attr, '.') === false && $this->hasAttribute($attr)) {
            $alias = $this->getTableAlias();
            $query = $alias . '.' . $attr . ' ' . $order;
        } else {
            $query = $attr . ' ' . $order;
        }

        $criteria = $this->getDbCriteria();

        if ($criteria->order !== $query) {
            if ($criteria->order === '') {
                $criteria->order = $query;
            } elseif ($criteria->order !== '') {
                if ($pos == 'end') {
                    $criteria->order = $criteria->order . ', ' . $query;
                } elseif ($pos == 'start') {
                    $criteria->order = $query . ', ' . $criteria->order;
                } else {
                    throw new CException('invalid position');
                }
            }
        }

        return $this;
    }

    /**
     * Сортировка случайным образом
     *
     * @return static
     */
    public function orderByRand()
    {
        $this->getDbCriteria()->order = 'RAND()';
        return $this;
    }

    /**
     * @param string $attr
     * @param string $pos
     *
     * @return $this
     * @throws CException
     */
    public function groupBy($attr, $pos = 'end')
    {
        $query = '';
        if (strpos($attr, '.') === false) {
            if ($this->hasAttribute($attr)) {
                $alias = $this->getTableAlias();
                $query = $alias . '.' . $attr;
            }
        } else {
            $query = $attr;
        }

        $criteria = $this->getDbCriteria();

        if ($criteria->group !== $query) {
            if ($criteria->group === '') {
                $criteria->group = $query;
            } elseif ($criteria->group !== '') {
                if ($pos == 'end') {
                    $criteria->group = $criteria->group . ', ' . $query;
                } elseif ($pos == 'start') {
                    $criteria->group = $query . ', ' . $criteria->group;
                } else {
                    throw new CException('invalid position');
                }
            }
        }

        return $this;
    }

    /**
     * Добавляет в текущую критерию лимиты
     *
     * @param integer $count количество элементов на страницу
     * @param integer $page страница
     *
     * @return static
     */
    public function limit($count, $page = 0)
    {
        $count = intval($count);
        $page = intval($page);

        $criteria = $this->getDbCriteria();
        $criteria->limit = $count;
        $criteria->offset = $count * $page;

        return $this;
    }

    /**
     * @param string[] $renamedColumns
     *
     * @return CDbCommand
     */
    public function getSqlCommand(array &$renamedColumns = [])
    {
        $criteria = clone $this->getDbCriteria();
        /* @var $commandBuilder CDbCommandBuilder */

        if (empty($criteria->with)) {
            $cmd = $this->getCommandBuilder()->createFindCommand($this->getTableSchema(), $criteria);
            $columnNames = $this->getTableSchema()->columnNames;
            $renamedColumns = array_combine($columnNames, $columnNames);
        } else {
            $with = $criteria->with;

            $copy = $this->copy();
            $finder = $copy->getActiveFinder($with);

            // find join treee
            $reflectedClass = new ReflectionClass($finder);
            $property = $reflectedClass->getProperty('_joinTree');
            $property->setAccessible(true);

            $joinElement = $property->getValue($finder);
            /* @var $joinElement CJoinElement */

            // get renamed columns
            $reflectedClass = new ReflectionClass($joinElement);
            $property = $reflectedClass->getProperty('_columnAliases');
            $property->setAccessible(true);

            $renamedColumns = array_merge($renamedColumns, $property->getValue($joinElement));
            $resolveChildElements = function ($joinElements) use ($property, &$resolveChildElements) {
                /* @var CJoinElement[] $joinElements */
                $result = [];
                foreach ($joinElements as $joinElement) {
                    $childRenamedColumns = $property->getValue($joinElement);
                    $tableName = $joinElement->tableAlias;
                    foreach ($childRenamedColumns as $columnName => $newColumnName) {
                        $result[$tableName . '.' . $columnName] = $newColumnName;
                    }
                    $result = array_merge($result, $resolveChildElements($joinElement->children));
                }
                return $result;
            };
            $renamedColumns = array_merge($renamedColumns, $resolveChildElements($joinElement->children));

            // create find
            $join = new CJoinQuery($joinElement, $criteria);
            $joinElement->buildQuery($join);
            $cmd = $join->createCommand($copy->getCommandBuilder());
            $criteria->params += $join->params;
        }

        $result = $this->getCommandBuilder()->createSqlCommand($cmd->text);
        $result->params = $criteria->params;
        return $result;
    }

    /**
     * Создает провайдер
     *
     * @param boolean $resetScopes не использовать поции из модели
     * @param array $providerOptions опции провайдера
     *
     * @return CActiveDataProvider провайдер
     */
    public function getDataProvider($resetScopes = false, $providerOptions = [])
    {
        $filter = false;
        if (isset($providerOptions['filter'])) {
            $filter = $providerOptions['filter'] + [
                    'attributes' => [],
                    'scenario' => 'search',
                    'safeOnly' => true,
                ];
            unset($providerOptions['filter']);
        }

        $provider = new CActiveDataProvider(get_class($this), [
                'criteria' => $resetScopes ? new CDbCriteria : clone $this->getDbCriteria(),
            ] + $providerOptions);

        if ($filter !== false && is_array($filter['attributes'])) {
            /** @var static $model */
            $model = $provider->model;
            $model->scenario = $filter['scenario'];
            $model->initializeAttributes($filter['attributes'], $filter['safeOnly']);
        }

        $sort = $provider->getSort();
        if ($sort !== false) {
            $attributes = $sort->attributes;

            if (empty($attributes)) {
                $alias = $provider->getCriteria()->alias;
                if (empty($alias)) {
                    $alias = 't';
                }

                foreach ($this->tableSchema->columnNames as $attr) {
                    $query = $alias . '.' . $attr;
                    $attributes[$attr] = [
                        'asc' => $query . ' ASC',
                        'desc' => $query . ' DESC',
                    ];

                    // добавляем сортировки для заменяемых атрибутов
                    $replacementAttr = CaseUtils::toCamel($attr) . 'Replacement';
                    if ($this->hasAttribute($replacementAttr)) {
                        $attributes[$replacementAttr] = $attributes[$attr];
                    }
                }

                $sort->attributes = $attributes;
            }
        }

        return $provider;
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    protected function initializeAttributes(array $values, bool $safeOnly): void
    {
        $this->setAttributes($values, $safeOnly);
    }

    /**
     * @param string $function
     * @param string $column
     * @param integer $limit
     *
     * @return false|CDbCommand
     */
    protected function _createAggregateFindCommand($function, $column, $limit = null)
    {
        $relations = [];
        if (strpos($column, '.') !== false) {
            $relations = explode('.', $column);
            $column = array_pop($relations);
        }

        if (empty($relations) && !$this->hasAttribute($column)) {
            return false;
        }

        $criteria = clone $this->getDbCriteria();
        /* @var $commandBuilder CDbCommandBuilder */

        // в случае если выборка идет по всем полям и взаимоисключающих лимитов
        // а так же когда нет влияющих пост-группировочных функций: group, having
        // тогда можно не создавать подзапросы
        if ($criteria->select === '*' && ($limit === null || $criteria->limit === -1) &&
            empty($criteria->with) && empty($criteria->having) && empty($criteria->group)) {
            $function = str_replace('{column}', $column, $function);
            $criteria->select = new CDbExpression($function);

            if ($limit !== null) {
                $criteria->limit = Cast::toUInt($limit);
            }

            return $this->getCommandBuilder()->createFindCommand($this->getTableSchema(), $criteria);
        }

        if (empty($criteria->with)) {
            $cmd = $this->getCommandBuilder()->createFindCommand($this->getTableSchema(), $criteria);
        } else {
            $with = $criteria->with;

            $copy = $this->copy();
            $finder = $copy->getActiveFinder($with);

            $reflectedClass = new ReflectionClass($finder);
            $property = $reflectedClass->getProperty('_joinTree');
            $property->setAccessible(true);

            $joinElement = $property->getValue($finder);

            /* @var $joinElement CJoinElement */

            foreach ($relations as $relation) {
                $joinElement = $joinElement->children[$relation];
            }

            $columnRenameExpression = $joinElement->getColumnSelect($column);
            preg_match('/^(.*?)\s+AS\s+(.*?)($|,)/im', $columnRenameExpression, $matches);
            $column = $matches[2];

            $join = new CJoinQuery($joinElement, $criteria);
            $joinElement->buildQuery($join);
            $cmd = $join->createCommand($copy->getCommandBuilder());
            $criteria->params = $join->params;
        }

        $function = str_replace('{column}', $column, $function);

        $query = "SELECT $function FROM ({$cmd->text}) AS temp";

        if ($limit !== null) {
            $query .= ' LIMIT ' . Cast::toStr(Cast::toUInt($limit));
        }

        $cmd = $this->getCommandBuilder()->createSqlCommand($query, $criteria->params);

        return $cmd;
    }

    /**
     * @param string $column
     * @param integer|null $limit
     *
     * @return mixed|false false on error
     *
     * @deprecated выпилить нахер
     */
    public function findColumnMin($column, $limit = null)
    {
        if (!$result = $this->_createAggregateFindCommand('MIN({column})', $column, $limit)) {
            return false;
        }
        return $result->queryScalar();
    }

    /**
     * @param string $column
     * @param integer|null $limit
     *
     * @return mixed|false false on error
     *
     * @deprecated выпилить нахер
     */
    public function findColumnMax($column, $limit = null)
    {
        if (!$result = $this->_createAggregateFindCommand('MAX({column})', $column, $limit)) {
            return false;
        }
        return $result->queryScalar();
    }

    /**
     * @param string $column
     * @param integer|null $limit
     *
     * @return integer|false false on error
     *
     * @deprecated выпилить нахер
     */
    public function findColumnSum($column, $limit = null)
    {
        if (!$result = $this->_createAggregateFindCommand('SUM({column})', $column, $limit)) {
            return false;
        }
        return $result->queryScalar();
    }

    /**
     * @param string $column
     * @param integer|null $limit
     *
     * @return integer|false false on error
     *
     * @deprecated выпилить нахер
     */
    public function findColumnAvg($column, $limit = null)
    {
        if (!$result = $this->_createAggregateFindCommand('AVG({column})', $column, $limit)) {
            return false;
        }
        return $result->queryScalar();
    }

    /**
     * @param string $column
     * @param integer|null $limit
     *
     * @return array|false false on error
     */
    public function findColumnDistinct($column, $limit = null)
    {
        if (!$result = $this->_createAggregateFindCommand('DISTINCT {column}', $column, $limit)) {
            return false;
        }
        return $result->queryColumn();
    }

    /**
     * @param string $column
     * @param integer|null $limit
     *
     * @return integer|false false on error
     *
     * @deprecated выпилить нахер
     */
    public function findColumnDistinctCount($column, $limit = null)
    {
        if (!$result = $this->_createAggregateFindCommand('COUNT(DISTINCT {column})', $column, $limit)) {
            return false;
        }
        return $result->queryScalar();
    }

    /**
     * Фильтрация обьектов по их id
     *
     * @param array $ids
     * @param integer $max не больше такого количества
     *
     * @return static
     */
    public function idIn($ids, $max = -1)
    {
        if ($max > 0) {
            $ids = array_slice($ids, 0, $max);
        }
        $ids = Cast::toUIntArr($ids);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addInCondition($alias . '.id', $ids);
        return $this;
    }

    /**
     * Фильтрация обьектов по их id
     *
     * @param array $ids
     * @param integer $max не больше такого количества
     *
     * @return static
     */
    public function idInNot($ids, $max = -1)
    {
        if ($max > 0) {
            $ids = array_slice($ids, 0, $max);
        }
        $ids = Cast::toUIntArr($ids);
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->addNotInCondition($alias . '.id', $ids);
        return $this;
    }

    /**
     * Сохранение большого количества моделей за раз
     * Если хотябы одна модель не сохранится не сохранятся все
     * !!WARNING!! - ДЛЯ НОВЫХ ЗАПИСЕЙ НЕ ВЫЗЫВАЕТСЯ AFTERSAVE
     *
     * @param UnShardedActiveRecord[] $models
     * @param bool $runValidation
     * @param array|null $attributes
     *
     * @throws CException
     * @return boolean
     *
     * @deprecated выпилить нахер
     */
    public static function saveMany(array $models, $runValidation = true, $attributes = null)
    {
        $valuesByClass = [];

        foreach ($models as $index => $model) { // validate and group
            if (!$model instanceof UnShardedActiveRecord) {
                throw new CException('Model must be instance of UnShardedActiveRecord');
            }

            $class = get_class($model);
            if (!isset($valuesByClass[$class])) {
                $valuesByClass[$class] = [];
            }

            if ($runValidation && !$model->validate($attributes)) {
                return false;
            }

            $valuesByClass[$class][] = $model;
        }

        $db = self::$db instanceof CDbConnection ? self::$db : Yii::app()->db; // resolve database

        // save all
        $transaction = $db->beginTransaction();
        try {
            foreach ($valuesByClass as $class => $values) {
                /* @var $values UnShardedActiveRecord[] */
                $classModel = $class::model();
                /* @var $classModel UnShardedActiveRecord */

                $columns = $classModel->tableSchema->columns;
                /* @var $columns CDbColumnSchema[] */
                $nullUnAllowedColumns = [];
                foreach ($columns as $name => $value) {
                    if (!$value->allowNull) {
                        $nullUnAllowedColumns[$name] = $value->defaultValue;
                    }
                }

                $insertsData = [];

                foreach ($values as $model) {
                    if (!$model->beforeSave()) {
                        continue;
                    }

                    $attrValues = $model->getAttributes($attributes);

                    if ($model->isNewRecord) {
                        foreach ($attrValues as $name => $value) {
                            if (isset($nullUnAllowedColumns[$name]) && $value === null) {
                                $attrValues[$name] = $nullUnAllowedColumns[$name];
                            }
                        }

                        $insertsData[] = $attrValues;
                        // внимание! для новых записей не вызывается after save
                    } else {
                        $classModel->updateByPk($model->getPrimaryKey(), $attrValues);
                        $model->afterSave();
                    }
                }

                if (!empty($insertsData)) {
                    // внимание не выполняется after save
                    $commandBuilder = $classModel->commandBuilder;
                    $insertCommand = $commandBuilder->createMultipleInsertCommand($classModel->tableSchema, $insertsData);
                    $insertCommand->execute();
                }
            }

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'application.database.saveMany');
            $transaction->rollback();
            return false;
        }
    }

    /**
     * @deprecated
     */
    public static function clearModelsCache()
    {
        // Загрушка для поиска старого кода
    }
}
