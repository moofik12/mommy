<?php

namespace MommyCom\YiiComponent\Database;

use Doctrine\DBAL\Connection as DbalConnection;

class DbTransaction extends \CDbTransaction
{
    /**
     * @var DbalConnection
     */
    private $dbalConnection;

    /**
     * DbTransaction constructor.
     *
     * @param DbConnection $connection
     * @param DbalConnection $dbalConnection
     */
    public function __construct(DbConnection $connection, DbalConnection $dbalConnection)
    {
        $dbalConnection->beginTransaction();

        $this->dbalConnection = $dbalConnection;

        parent::__construct($connection);
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function commit()
    {
        $this->dbalConnection->commit();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function rollback()
    {
        $this->dbalConnection->rollBack();
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return ($this->dbalConnection->getTransactionNestingLevel() > 0);
    }
}
