<?php

namespace MommyCom\YiiComponent\Database;

use MommyCom\Service\CurrentTime;

class EntityAccessorAttribute
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var callable|null
     */
    private $get;

    /**
     * @var callable|null
     */
    private $set;

    /**
     * @var string|null
     */
    private $init;

    /**
     * @param string $name
     * @param callable|null $get
     * @param callable|null $set
     * @param string|null $init
     */
    public function __construct(string $name, ?callable $get, ?callable $set, ?string $init)
    {
        $this->name = $name;
        $this->get = $get;
        $this->set = $set;
        $this->init = $init;
    }

    /**
     * @param string $name
     * @param string|callable|false $get
     * @param string|callable|false $set
     * @param string|false $init
     *
     * @return self
     */
    public static function create(string $name, $get, $set = false, $init = false): self
    {
        $get = $get ? static::makeCallable($get, true) : null;
        $set = $set ? static::makeCallable($set, false) : null;
        $init = $init ? (string)$init : null;

        return new static($name, $get, $set, $init);
    }

    private static function makeCallable($method, bool $isGetter): callable
    {
        if (is_string($method) && strlen($method)) {
            return function ($entity, ...$args) use ($method) {
                if (!method_exists($entity, $method)) {
                    throw new \LogicException(sprintf(
                        'Entity "%s" has no method "%s"().',
                        get_class($entity),
                        $method
                    ));
                }

                return call_user_func_array([$entity, $method], $args);
            };
        }

        if (is_callable($method)) {
            return $method;
        }

        throw new \LogicException(sprintf(
            'Cannot create %s because invalid type "%s".',
            $isGetter ? 'getter' : 'setter',
            is_object($method) ? get_class($method) : gettype($method)
        ));
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isReadable(): bool
    {
        return (null !== $this->get);
    }

    /**
     * @return bool
     */
    public function isWritable(): bool
    {
        return (null !== $this->set);
    }

    /**
     * @return bool
     */
    public function isInitializable(): bool
    {
        return (null !== $this->init);
    }

    /**
     * @param object $entity
     *
     * @return mixed
     */
    public function callGet($entity)
    {
        if (null === $this->get) {
            throw new \RuntimeException(sprintf('Property "%s" is not readable.', $this->name));
        }

        return call_user_func($this->get, $entity);
    }

    /**
     * @param object $entity
     * @param mixed $value
     */
    public function callSet($entity, $value)
    {
        if (null === $this->set) {
            throw new \RuntimeException(sprintf('Property "%s" is not writable.', $this->name));
        }

        if ($value instanceof \CDbExpression) {
            if ('UNIX_TIMESTAMP()' !== (string)$value) {
                throw new \LogicException(sprintf('CDbExpression not supported.'));
            }

            $value = CurrentTime::getUnixTimestamp();
        }

        call_user_func($this->set, $entity, $value);
    }

    /**
     * @param object $entity
     * @param mixed $value
     */
    public function init($entity, $value): void
    {
        if (null === $this->init) {
            throw new \LogicException(sprintf('Property "%s" is not initializable.', $this->name));
        }

        try {
            $property = new \ReflectionProperty(get_class($entity), $this->init);
            $property->setAccessible(true);
            $property->setValue($entity, $value);
            $property->setAccessible(false);
        } catch (\ReflectionException $e) {
            throw new \LogicException(sprintf(
                'Property "%s" cannot be initialized because reflection error: %s',
                $this->init,
                $e->getMessage()
            ));
        }
    }
}
