<?php

namespace MommyCom\YiiComponent;

use CAssetManager;
use CException;
use CFileHelper;
use CLogger;
use Yii;

/**
 * Class AssetManager
 * Модифицированный компонент assetManager.
 * Добавлена возможность автообновления публикуемых директорий, если в директории появились обновленные файлы.
 * При условии что при вызове publish() параметр hashByName = false (исп. по-умолчанию).
 *
 * @param bool autorefresh включает, выключает эту возможность. (не влияет на алгоритм полученя имени директорий)
 */
class AssetManager extends CAssetManager
{
    /**
     * Перепубликация папки, если есть обновившееся файлы в ней
     *
     * @var bool
     */
    public $autorefresh = true;

    /**
     * /!\ Не уверен что работает именно так, но этот параметр используется в CFileHelper::validatePath()
     * Имена файлов для которых будет проверяться обновление, если указаны.
     *
     * @var array list of valid file name suffixes (without dot).
     */
    public $includeFiles = [];

    private $_published = [];

    /**
     * Модифицированный CAssetManager::publish()
     *
     * @param string $path
     * @param bool $hashByName
     * @param int $level
     * @param null $forceCopy
     *
     * @return string
     * @throws CException
     */
    public function publish($path, $hashByName = false, $level = -1, $forceCopy = null)
    {
        if ($forceCopy === null)
            $forceCopy = $this->forceCopy;
        if ($forceCopy && $this->linkAssets)
            throw new CException(\Yii::t('yii', 'The "forceCopy" and "linkAssets" cannot be both true.'));
        if (isset($this->_published[$path]))
            return $this->_published[$path];
        elseif (($src = realpath($path)) !== false) {
            $dir = $this->generatePath($src, $hashByName);
            $dstDir = $this->getBasePath() . DIRECTORY_SEPARATOR . $dir;

            if (is_file($src)) {
                $fileName = basename($src);
                $dstFile = $dstDir . DIRECTORY_SEPARATOR . $fileName;

                if (!is_dir($dstDir)) {
                    mkdir($dstDir, $this->newDirMode, true);
                    chmod($dstDir, $this->newDirMode);
                }

                if ($this->linkAssets && !is_file($dstFile)) symlink($src, $dstFile);
                elseif (@filemtime($dstFile) < @filemtime($src)) {
                    copy($src, $dstFile);
                    chmod($dstFile, $this->newFileMode);
                }

                return $this->_published[$path] = $this->getBaseUrl() . "/$dir/$fileName";
            } elseif (is_dir($src)) {
                /*********************************
                 * Автообновление директорий
                 * Когда $hashByName == false, то имя целевой директории составляется из времени ее изменения.
                 * Для таких случаев время последнего изменения вычисляется по файлам в этой директории, затем полученное время если оно отличается
                 * от времени изм самой дирекории, обновляется у самой диретории.
                 */
                if ($hashByName == false && $this->autorefresh) {
                    $filemtime = $this->getDirectoryMaxFilemtime($src, '', $level);
                    //новое имя папки с учетом обновившихся файлов в ней и путь
                    $dir = $this->hash($src . $filemtime);
                    $dstDir = $this->getBasePath() . DIRECTORY_SEPARATOR . $dir;
                    // обновляю время изменения этой директории только в таких случаях (когда директория обновилась либо еще не была создана)
                    if (!is_dir($dstDir) && $filemtime > filemtime($src)) {
                        Yii::log(\Yii::t('common', 'Обновились файлы в ') . $src, CLogger::LEVEL_INFO, 'application.assetmanager');
                        // уст. новое время доступа директории, равное времени последнего измененного файла в ней.
                        @touch($src, $filemtime);
                    }
                }
                /************************************/

                if ($this->linkAssets && !is_dir($dstDir)) {
                    symlink($src, $dstDir);
                } elseif (!is_dir($dstDir) || $forceCopy) {
                    CFileHelper::copyDirectory($src, $dstDir, [
                        'exclude' => $this->excludeFiles,
                        'level' => $level,
                        'newDirMode' => $this->newDirMode,
                        'newFileMode' => $this->newFileMode,
                    ]);
                }

                return $this->_published[$path] = $this->getBaseUrl() . '/' . $dir;
            }
        }
        throw new CException(\Yii::t('yii', 'The asset "{asset}" to be published does not exist.',
            ['{asset}' => $path]));
    }

    /**
     * Рекурсивно проверяет файлы в директории и возвращает максимальное время последней модификации файла.
     *
     * @param $src
     * @param $base
     * @param $level
     *
     * @return int timestamp
     */
    protected function getDirectoryMaxFilemtime($src, $base, $level)
    {
        $times = [filemtime($src)];
        $folder = opendir($src);
        while (($file = readdir($folder)) !== false) {
            if ($file === '.' || $file === '..')
                continue;
            $path = $src . DIRECTORY_SEPARATOR . $file;
            $isFile = is_file($path);
            if ($this->validatePath($base, $file, $isFile, $this->includeFiles, $this->excludeFiles)) {
                if ($isFile) {
                    $times[] = filemtime($path);
                } else if ($level)
                    $times[] = $this->getDirectoryMaxFilemtime($path, $base . '/' . $file, $level - 1);
            }
        }
        closedir($folder);
        return max($times);
    }

    /**
     * CFileHelper::validatePath()
     *
     * @return bool
     */
    protected function validatePath($base, $file, $isFile, $fileTypes, $exclude)
    {
        foreach ($exclude as $e) {
            if ($file === $e || strpos($base . '/' . $file, $e) === 0)
                return false;
        }
        if (!$isFile || empty($fileTypes))
            return true;
        if (($type = pathinfo($file, PATHINFO_EXTENSION)) !== '')
            return in_array($type, $fileTypes);
        else
            return false;
    }
}
