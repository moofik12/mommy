<?php

namespace MommyCom\YiiComponent\AuthManager;

use CHttpCookie;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\IpBanRecord;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Db\UserPartnerInviteRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Security\User\WebUser;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\UserReferer;
use Yii;

/**
 * @property-read UserRecord $model
 * @property integer $offerProvider
 * @property string $offerId
 * @property string $offerTargetUrl
 * @property integer $lastOfferProvider
 * @property string $lastOfferId
 * @property string $lastSearchEngine
 * @property-read ShopShoppingCart $cart
 * @property-read ShopBonusPoints $bonuspoints
 */
class ShopWebUser extends WebUser
{
    const OFFER_LIFETIME = 2592000;
    const ANONYMOUS_LIFETIME = 2592000;
    const PARTNER_LIFETIME = 2592000;

    const MARK_OFFER_PSYCHOTYPE = 'offerPs';
    const MARK_OFFER_BANNER_NUMBER = 'offerBn';

    private $_model = null;

    /**
     * ключи (section) с доступом которые запрещены по данному IP
     *
     * @var array
     */
    private $_bannedKeys = false;

    /**
     * @var ShopShoppingCart
     */
    private $_cart;

    /**
     * @var ShopBonusPoints
     */
    private $_bonuspoints;

    private $_utmAllowedParams = ['utm_source', 'utm_medium', 'utm_term', 'utm_content', 'utm_campaign'];

    public function init()
    {
        if ($this->getModel() instanceof UserRecord) {
            $this->getModel()->updateLastVisit(true);
            $this->setShowComebacker();
        }

        $referer = Yii::app()->userReferer;
        $refererHost = isset($_SERVER['SERVER_NAME'])
            ? implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2)) //учитываем переход из моб. версии
            : '';

        if ($refererHost && !preg_match("/^[^?]*$refererHost/ui", $referer->url)) {
            $this->setUrlReferer($referer->url);
        }

        $utmAllowedParams = $this->_utmAllowedParams;
        $findUtmParams = [];
        foreach ($_GET as $key => $value) {
            if (in_array($key, $utmAllowedParams)) {
                $findUtmParams[$key] = $value;
            }
        }

        if ($findUtmParams) {
            $this->setUtmParams($findUtmParams);
        }
        $this->getAnonymousId();
    }

    public function setShowComebacker()
    {
        $cookies = Yii::app()->request->cookies;
        $cookies->add('isShowComebacker', new CHttpCookie('isShowComebacker', 1, ['expire' => strtotime('tomorrow')]));
    }

    public function getShowComebacker()
    {
        $cookies = Yii::app()->request->cookies;
        $key = 'isShowComebacker';
        $value = $cookies->contains($key) ? $cookies->itemAt($key)->value : 0;
        return $value;
    }

    public function isExistShowComebacker()
    {
        $cookies = Yii::app()->request->cookies;
        $key = 'isShowComebacker';
        return $cookies->contains($key);
    }

    /**
     * @return integer
     */
    public function getOfferProvider()
    {
        if (!$this->getIsGuest()) {
            return $this->model->offer_provider;
        }

        $key = 'offerProvider';
        $cookies = Yii::app()->request->cookies;
        $default = $cookies->contains($key) ? $cookies->itemAt($key)->value : '';

        return Cast::toStr($this->getState($key, $default));
    }

    public function getOfferId()
    {
        if (!$this->getIsGuest()) {
            return $this->model->offer_id;
        }

        $key = 'offerId';
        $cookies = Yii::app()->request->cookies;
        $default = $cookies->contains($key) ? $cookies->itemAt($key)->value : 0;

        return Cast::toStr($this->getState($key, $default));
    }

    public function getOfferTargetUrl()
    {
        if (!$this->getIsGuest()) {
            return $this->model->offer_target_url;
        }

        $key = 'offerTargetUrl';
        $cookies = Yii::app()->request->cookies;
        $default = $cookies->contains($key) ? $cookies->itemAt($key)->value : '';

        return Cast::toStr($this->getState($key, $default));
    }

    public function getLandingNum()
    {
        if (!$this->getIsGuest()) {
            return $this->model->landing_num;
        }
        return $this->getState('landingNum', 0);
    }

    public function setLandingNum($value)
    {
        $this->setState('landingNum', $value);
    }

    public function setOfferProvider($value)
    {
        $lifeTime = time() + self::OFFER_LIFETIME;
        $this->setState('offerProvider', $value);
        Yii::app()->request->cookies['offerProvider'] = new CHttpCookie('offerProvider', $value, [
            'expire' => $lifeTime,
        ]);
    }

    public function setOfferId($value)
    {
        $lifeTime = time() + self::OFFER_LIFETIME;
        $this->setState('offerId', $value);
        Yii::app()->request->cookies['offerId'] = new CHttpCookie('offerId', $value, [
            'expire' => $lifeTime,
        ]);

        if ($this->getOfferProvider() == UserRecord::OFFER_PROVIDER_ACTIONPAY) {
            // Yii will protect cookies so I use stl function
            setCookie('actionpay', $value, $lifeTime, '/');
        } elseif ($this->getOfferProvider() == UserRecord::OFFER_PROVIDER_ADMITAD) {
            // Yii will protect cookies so I use stl function
            setCookie('admitad', $value, $lifeTime, '/');
        } elseif ($this->getOfferProvider() == UserRecord::OFFER_PROVIDER_SSP) {
            // Yii will protect cookies so I use stl function
            setCookie('ssp', $value, $lifeTime, '/');
        }
    }

    public function setOfferTargetUrl($value)
    {
        $lifeTime = time() + self::OFFER_LIFETIME;
        $this->setState('offerTargetUrl', $value);
        Yii::app()->request->cookies['offerTargetUrl'] = new CHttpCookie('offerTargetUrl', $value, [
            'expire' => $lifeTime,
        ]);
    }

    public function getLastOfferProvider()
    {
        $key = 'offerProvider';
        $cookies = Yii::app()->request->cookies;

        $default = '';
        if ($cookies->contains($key)) {
            $default = $cookies->itemAt($key)->value;
        } elseif (!$this->getIsGuest() && $this->model->created_at + self::OFFER_LIFETIME >= time()) {
            $default = $this->model->offer_provider;
        }

        return Cast::toStr($this->getState($key, $default));
    }

    public function getLastOfferId()
    {
        $key = 'offerId';
        $cookies = Yii::app()->request->cookies;

        $default = '';
        if ($cookies->contains($key)) {
            $default = $cookies->itemAt($key)->value;
        } elseif (!$this->getIsGuest() && $this->model->created_at + self::OFFER_LIFETIME >= time()) {
            $default = $this->model->offer_provider;
        }

        return Cast::toStr($this->getState($key, $default));
    }

    /**
     * @param bool $value
     * @param bool $force
     */
    public function setEnableOfferBenefice($value, $force = false)
    {
        $value = (int)Cast::toBool($value);
        $lifeTime = time() + self::OFFER_LIFETIME;
        $cookies = Yii::app()->request->cookies;
        $hasOfferBenefice = $this->hasState('offerBenefice') || $cookies->contains('offerBenefice');

        if (!$hasOfferBenefice || $force) {
            $this->setState('offerBenefice', $value);
            Yii::app()->request->cookies['offerBenefice'] = new CHttpCookie('offerBenefice', $value, [
                'expire' => $lifeTime,
            ]);
        }
    }

    /**
     * @param bool $value , false или null для удаления
     * @param bool $force
     */
    public function setPsychotype($value, $force = false)
    {
        $value = (int)Cast::toUInt($value);
        $lifeTime = $value == false || $value == null ? 0 : time() + self::OFFER_LIFETIME;
        $cookies = Yii::app()->request->cookies;
        $hasOfferBenefice = $this->hasState(self::MARK_OFFER_PSYCHOTYPE) || $cookies->contains(self::MARK_OFFER_PSYCHOTYPE);

        if (!$hasOfferBenefice || $force) {
            $this->setState(self::MARK_OFFER_PSYCHOTYPE, $value);
            Yii::app()->request->cookies[self::MARK_OFFER_PSYCHOTYPE] = new CHttpCookie(self::MARK_OFFER_PSYCHOTYPE, $value, [
                'expire' => $lifeTime,
            ]);
        }
    }

    /**
     * @param bool|mixed $default
     *
     * @return int|mixed
     */
    public function getPsychotype($default = false)
    {
        $value = null;
        $cookies = Yii::app()->request->cookies;
        if ($cookies->contains(self::MARK_OFFER_PSYCHOTYPE)) {
            $value = $cookies->itemAt(self::MARK_OFFER_PSYCHOTYPE)->value;
        }

        if ($this->hasState(self::MARK_OFFER_PSYCHOTYPE)) {
            $value = $this->getState(self::MARK_OFFER_PSYCHOTYPE, false);
        }

        return is_numeric($value) && in_array((int)$value, UserRecord::psychotypes(false)) ? (int)$value : $default;
    }

    /**
     * @param bool $value , false или null для удаления
     * @param bool $force
     */
    public function setOfferBannerNumber($value, $force = false)
    {
        $value = (int)Cast::toUInt($value);
        $lifeTime = $value == false || $value == null ? 0 : time() + self::OFFER_LIFETIME;
        $cookies = Yii::app()->request->cookies;
        $hasOfferBenefice = $this->hasState(self::MARK_OFFER_BANNER_NUMBER) || $cookies->contains(self::MARK_OFFER_BANNER_NUMBER);

        if (!$hasOfferBenefice || $force) {
            $this->setState(self::MARK_OFFER_BANNER_NUMBER, $value);
            Yii::app()->request->cookies[self::MARK_OFFER_BANNER_NUMBER] = new CHttpCookie(self::MARK_OFFER_BANNER_NUMBER, $value, [
                'expire' => $lifeTime,
            ]);
        }
    }

    /**
     * @param bool|mixed $default
     *
     * @return int|mixed
     */
    public function getOfferBannerNumber($default = false)
    {
        $value = null;
        $cookies = Yii::app()->request->cookies;
        if ($cookies->contains(self::MARK_OFFER_BANNER_NUMBER)) {
            $value = $cookies->itemAt(self::MARK_OFFER_BANNER_NUMBER)->value;
        }

        if ($this->hasState(self::MARK_OFFER_BANNER_NUMBER)) {
            $value = $this->getState(self::MARK_OFFER_BANNER_NUMBER, false);
        }

        return is_numeric($value) ? (int)$value : $default;
    }

    /**
     * @param string $value
     */
    public function setUrlReferer($value)
    {
        if (!is_string($value)) {
            return;
        }

        $this->setState('userUrlReferer', $value);
    }

    /**
     * @return string
     */
    public function getUrlReferer()
    {
        return $this->getState('userUrlReferer', '');
    }

    /**
     * @param array $value
     */
    public function setUtmParams(array $value)
    {
        $this->setState('userUtmParams', $value);
    }

    /**
     * @return string
     */
    public function getUtmParams()
    {
        return $this->getState('userUtmParams', []);
    }

    /**
     * @return bool
     */
    public function isEnableOfferBenefice()
    {
        $cookies = Yii::app()->request->cookies;
        $enable = false;

        if ($this->hasState('offerBenefice')) {
            $enable = !!$this->getState('offerBenefice');
        } elseif ($cookies->contains('offerBenefice')) {
            $enable = !!$cookies->itemAt('offerBenefice')->value;
        }

        return $enable;
    }

    /**
     * Специальные предложения для пользователей которые перешли из поисковиков
     *
     * @return bool
     */
    public function isEnableOfferSearchBenefice()
    {
        $referer = Yii::app()->userReferer;
        $enable = false;

        if ($referer instanceof UserReferer && $referer->isSearchEngine() && $this->getIsGuest()) {
            $availableSearchEngineThanBenefice = [
                UserReferer::SEARCH_ENGINE_GOOGLE,
                UserReferer::SEARCH_ENGINE_YANDEX,
            ];

            return in_array($referer->searchEngine, $availableSearchEngineThanBenefice);
        }

        return $enable;
    }

    /**
     * @param bool $default
     *
     * @return mixed
     */
    public function getLastSearchEngine($default = false)
    {
        return $this->getState('lastSearchEngine', $default);
    }

    /**
     * @param string $searchEngine
     */
    public function setLastSearchEngine($searchEngine)
    {
        if ($searchEngine) {
            $this->setState('lastSearchEngine', $searchEngine);
        }
    }

    /**
     * Код подтверждения (выдается только один раз)
     *
     * @return string|false
     */
    public function getOfferSuccessCode()
    {
        $model = $this->model;
        /* @var $model UserRecord */

        if ($this->getIsGuest()) {
            return false;
        }

        if ($model->is_email_verified == false || $model->offer_provider == UserRecord::OFFER_PROVIDER_NONE || $model->offer_success) {
            return false;
        }

        $result = '';
        $id = $model->id;
        $offerId = $model->offer_id;

        $model->offer_success = 1;
        $model->save(true, ['offer_success']);

        return $result;
    }

    /**
     * @return ShopShoppingCart
     */
    public function getCart()
    {
        if ($this->_cart === null || $this->_cart->isGuest != $this->getIsGuest()) {
            $this->_cart = new ShopShoppingCart();
            $this->_cart->init($this->getModel());
        }

        return $this->_cart;
    }

    /**
     * @return ShopBonusPoints
     */
    public function getBonuspoints()
    {
        if ($this->_bonuspoints === null || $this->_bonuspoints->isGuest != $this->getIsGuest()) {
            $this->_bonuspoints = new ShopBonusPoints();
            $this->_bonuspoints->init($this->getModel());
        }
        return $this->_bonuspoints;
    }

    /**
     * проверяет запрещено ли данное действие блокировкой по ip
     *
     * @param string $section ('all' | 'admin' | 'site' | 'cart')
     *
     * @return bool
     */
    public function isBanned($section = 'all')
    {
        $sections = [
            IpBanRecord::SECTION_ALL => 'all',
            IpBanRecord::SECTION_AUTH_ADMIN => 'admin',
            IpBanRecord::SECTION_AUTH_SITE => 'site',
            IpBanRecord::SECTION_CART => 'cart',

        ];

        $key = array_search($section, $sections);

        if ($this->_bannedKeys === false) {
            $ip = Yii::app()->request->getUserHostAddress();
            $result = IpBanRecord::model()->getBannedKeys($ip);
            $this->_bannedKeys = is_array($result) ? $result : [];
        }

        return in_array(IpBanRecord::SECTION_ALL, $this->_bannedKeys)
            ? true : in_array($key, $this->_bannedKeys);
    }

    /**
     * Модель пользователя
     *
     * @return UserRecord
     */
    public function getModel()
    {
        if (!$this->getIsGuest()) {
            $this->_model = UserRecord::model()->find('email = :email', [
                ':email' => $this->getToken()->getUsername(),
            ]);
        }

        return $this->_model;
    }

    /**
     * @param bool $fromCookie
     */
    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);

        if ($this->isBanned('site')) {
            $this->logout();
        }
        $this->updateCart();
        $this->setEnableOfferBenefice(false, true);
        $this->setPsychotype(false, true);
        $this->setOfferBannerNumber(false, true);
    }

    /**
     * Изменение режима типа рассылки
     *
     * @param int $type
     *
     * @return bool
     */
    public function _saveDistribution($type = UserDistributionRecord::TYPE_EVERY_WEEK)
    {
        $user = $this->getModel();
        if ($user !== null) {
            /** @var $user UserRecord */
            if ($type == UserDistributionRecord::TYPE_EVERY_DAY) {
                $distributionEveryDay = UserDistributionRecord::model()
                    ->type(UserDistributionRecord::TYPE_EVERY_DAY)
                    ->userId($user->id)
                    ->find();
                if ($distributionEveryDay === null) {
                    $distributionEveryDay = new UserDistributionRecord();
                    $distributionEveryDay->user_id = $user->id;
                    $distributionEveryDay->type = UserDistributionRecord::TYPE_EVERY_DAY;
                }
                $distributionEveryDay->is_subscribe = 1;
                $distributionEveryDay->is_update = 1;
                $distributionEveryDay->save();
                $distributionEveryWeek = UserDistributionRecord::model()
                    ->type(UserDistributionRecord::TYPE_EVERY_WEEK)
                    ->userId($user->id)
                    ->find();
                if ($distributionEveryWeek !== null) {
                    $distributionEveryWeek->is_subscribe = 0;
                    $distributionEveryWeek->is_update = 1;
                    $distributionEveryWeek->save();
                }
            } else {
                $distributionEveryWeek = UserDistributionRecord::model()
                    ->type(UserDistributionRecord::TYPE_EVERY_WEEK)
                    ->userId($user->id)
                    ->find();
                if ($distributionEveryWeek === null) {
                    $distributionEveryWeek = new UserDistributionRecord();
                    $distributionEveryWeek->user_id = $user->id;
                    $distributionEveryWeek->type = UserDistributionRecord::TYPE_EVERY_WEEK;
                }
                $distributionEveryWeek->is_subscribe = 1;
                $distributionEveryWeek->is_update = 1;
                $distributionEveryWeek->save();
                $distributionEveryDay = UserDistributionRecord::model()
                    ->type(UserDistributionRecord::TYPE_EVERY_DAY)
                    ->userId($user->id)
                    ->find();
                if ($distributionEveryDay !== null) {
                    $distributionEveryDay->is_subscribe = 0;
                    $distributionEveryDay->is_update = 1;
                    $distributionEveryDay->save();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getAnonymousId()
    {
        $key = 'anonymousId';
        $cookies = Yii::app()->request->cookies;
        $default = $cookies->contains($key) ? $cookies->itemAt($key)->value : $this->setAnonymousId();

        return Cast::toStr($this->getState($key, $default));
    }

    public function setAnonymousId()
    {
        $key = 'anonymousId';
        $lifeTime = time() + self::ANONYMOUS_LIFETIME;
        $anonymousId = CartRecord::generateAnonymousId();
        $this->setState($key, $anonymousId);
        if (!headers_sent()) {
            Yii::app()->request->cookies[$key] = new CHttpCookie($key, $anonymousId, [
                'expire' => $lifeTime,
            ]);
        }
    }

    public function updateCart()
    {
        $anonymousId = $this->getAnonymousId();
        if (!$this->getIsGuest()) {
            $cartItems = CartRecord::model()->anonymousId($anonymousId)->findAll();
            if (!empty($cartItems)) {
                foreach ($cartItems as $cartItem) {
                    /** @var $cartItem  CartRecord */
                    $cartItem->user_id = $this->getModel()->id;
                    $cartItem->anonymous_id = '';
                    $cartItem->save(false, ['user_id', 'anonymous_id']);
                }
            }
        }
    }

    /**
     * @property-description Получение ид партнера
     * @return int
     */
    public function getPartnerId()
    {
        if (!$this->getIsGuest()) {
            $inviteUser = UserPartnerInviteRecord::model()->userId($this->id)->find();
            if ($inviteUser !== null) {
                /** @var $inviteUser UserPartnerInviteRecord */
                return $inviteUser->partner_id;
            }
        }

        $key = 'partnerId';
        $cookies = Yii::app()->request->cookies;
        $default = $cookies->contains($key) ? $cookies->itemAt($key)->value : 0;

        return Cast::toInt($this->getState($key, $default));
    }

    /**
     * @property-description Закрепление за пользователем ИД партнера
     *
     * @param int $value
     */
    public function setPartnerId($value)
    {
        $lifeTime = time() + self::PARTNER_LIFETIME;
        $key = 'partnerId';
        $this->setState($key, $value);
        Yii::app()->request->cookies[$key] = new CHttpCookie($key, $value, [
            'expire' => $lifeTime,
        ]);
    }

    public function setPartnerRefererUrl($value)
    {
        $lifeTime = time() + self::PARTNER_LIFETIME;
        $this->setState('partnerRefererUrl', $value);
        Yii::app()->request->cookies['partnerRefererUrl'] = new CHttpCookie('partnerRefererUrl', $value, [
            'expire' => $lifeTime,
        ]);
    }

    public function getPartnerRefererUrl()
    {
        if (!$this->getIsGuest()) {
            $inviteUser = UserPartnerInviteRecord::model()->userId($this->id)->find();
            if ($inviteUser !== null) {
                /** @var $inviteUser UserPartnerInviteRecord */
                return $inviteUser->url_referer;
            }
        }

        $key = 'partnerRefererUrl';
        $cookies = Yii::app()->request->cookies;
        $default = $cookies->contains($key) ? $cookies->itemAt($key)->value : '';

        return Cast::toStr($this->getState($key, $default));
    }

    /**
     * @param array $value
     */
    public function setPartnerUtmParams(array $value)
    {
        $this->setState('partnerUtmParams', $value);
    }

    /**
     * @return array
     */
    public function getPartnerUtmParams()
    {
        return $this->getState('partnerUtmParams', []);
    }
}
