<?php

namespace MommyCom\YiiComponent\AuthManager;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

trait AuthenticationTrait
{
    /**
     * @param TokenStorageInterface $tokenStorage
     * @param UserInterface $user
     * @param string $providerKey
     *
     * @return bool
     */
    protected function authenticateUser(TokenStorageInterface $tokenStorage, UserInterface $user, string $providerKey)
    {
        try {
            $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
            $tokenStorage->setToken($token);

            /** @var ShopWebUser $webUser */
            $webUser = $this->app()->user;
            $webUser->setToken($token);
            $webUser->updateCart();
        } catch (\InvalidArgumentException $exception) {
            return false;
        }

        return true;
    }
}
