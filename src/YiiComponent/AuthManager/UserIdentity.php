<?php

namespace MommyCom\YiiComponent\AuthManager;

use CUserIdentity;
use MommyCom\Model\Db\IpBanRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\Type\Cast;

class UserIdentity extends CUserIdentity
{
    /**
     * @var UserRecord|null
     */
    private $userRecord = null;

    public function authenticate()
    {
        /* @var $object UserRecord */
        $object = UserRecord::model()->findByAttributes(['email' => $this->username]);
        if (is_null($object)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->errorMessage = \Yii::t('common', 'Wrong e-mail or password');

            return !$this->errorCode;
        }

        if ($object->password_hash !== UserRecord::encodePassword($this->password, $object->password_salt)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            $this->errorMessage = \Yii::t('common', 'Wrong e-mail or password');

            return !$this->errorCode;
        }

        /**
         * фильтрация блокированных пользователей по IP или статусу пользователя
         *
         * @var $bannedModel IpBanRecord
         */
        $ip = \Yii::app()->request->getUserHostAddress();
        $bannedModel = IpBanRecord::model()->cache(300)->bannedIp($ip)->section(IpBanRecord::SECTION_AUTH_SITE)->find();
        $status = Cast::toUInt($object->status);
        if ($bannedModel || $status !== UserRecord::STATUS_ACTIVE) {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            $this->errorMessage = \Yii::t('common', 'Access is blocked');

            return !$this->errorCode;
        }

        $object->ip = $ip;
        $object->save(true, ['ip']);

        $this->errorCode = self::ERROR_NONE;
        $this->userRecord = $object;

        return !$this->errorCode;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->userRecord->id;
    }
}
