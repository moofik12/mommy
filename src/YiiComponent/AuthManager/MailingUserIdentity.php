<?php

namespace MommyCom\YiiComponent\AuthManager;

use CBaseUserIdentity;
use MommyCom\Model\Db\UserRecord;

/**
 * Class MailingUserIdentity
 */
class MailingUserIdentity extends CBaseUserIdentity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $loginId;

    /**
     * @var string
     */
    public $hash;

    /**
     * MailingUserIdentity constructor.
     *
     * @param int $loginId
     * @param string $hash
     */
    public function __construct($loginId, $hash)
    {
        $this->loginId = $loginId;
        $this->hash = $hash;
    }

    /**
     * Authenticates the user.
     * The information needed to authenticate the user
     * are usually provided in the constructor.
     *
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        if (!is_numeric($this->loginId)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            return false;
        }

        $user = UserRecord::model()->findByPk($this->loginId);
        /* @var $user UserRecord */

        if ($user === null) {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            return false;
        }

        if ($user->getLoginHash() != $this->hash) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            return false;
        }

        $this->id = $user->id;

        return true;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->id;
    }
}
