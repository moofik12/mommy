<?php

namespace MommyCom\YiiComponent\Cache;

use MommyCom\YiiComponent\Facade\Cache;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class Psr6CacheBridge extends \CCache
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->cache = Cache::cache();
    }

    /**
     * {@inheritdoc}
     */
    protected function generateUniqueKey($key)
    {
        return base64_encode(hash('sha256', $key, true));
    }

    /**
     * {@inheritdoc}
     */
    protected function getValue($key)
    {
        try {
            return $this->cache->get($key, false);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getValues($keys)
    {
        try {
            return $this->cache->getMultiple($keys, false);
        } catch (InvalidArgumentException $e) {
            return array_fill(0, count($keys), false);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function setValue($key, $value, $expire)
    {
        try {
            return $this->cache->set($key, $value, $expire ?: null);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function addValue($key, $value, $expire)
    {
        try {
            if ($this->cache->has($key)) {
                return false;
            }

            return $this->cache->set($key, $value, $expire ?: null);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteValue($key)
    {
        try {
            return $this->cache->delete($key);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function flushValues()
    {
        return $this->cache->clear();
    }
}
