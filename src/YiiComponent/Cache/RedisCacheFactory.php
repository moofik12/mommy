<?php

namespace MommyCom\YiiComponent\Cache;

use Symfony\Component\Cache\Simple\RedisCache;

class RedisCacheFactory
{
    /**
     * @param string $host
     * @param string $db
     *
     * @return RedisCache
     */
    public static function getCache(string $host, string $db): RedisCache
    {
        $dsn = 'redis://' . $host . '/' . $db;
        $prefix = 'mommy' . $db;

        return new RedisCache(RedisCache::createConnection($dsn), $prefix);
    }
}
