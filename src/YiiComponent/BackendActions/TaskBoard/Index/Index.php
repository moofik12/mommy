<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Index;

use CAction;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;
use Yii;

/**
 * Class Index
 */
class Index extends CAction
{
    const MANUAL_TASK = 'manual_task';
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @var string
     */
    public $board_type = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $view = '';

    /**
     * @var int
     */
    public $archived = 0;

    /**
     * @var string
     */
    public $filterAddUrl = '';

    /**
     * @var string
     */
    public $filterRemoveUrl = '';

    /**
     * @var array
     */
    public $zeroActions = ['storage', 'coordination'];

    /**
     * @return void
     */
    public function run()
    {
        $app = Yii::app();
        $id = $app->user->id;
        $controller = $this->getController();

        $key = TaskBoardRecord::$sessionConstants[$this->board_type];
        Session::set(self::REDIRECT_KEY, $app->controller->action->id);
        Session::set(self::MANUAL_TASK, $this->board_type);

        /* @var $model TaskBoardRecord */
        $model = TaskBoardRecord::model();
        $attributes = in_array($app->controller->action->id, $this->zeroActions) ? [0 => 0] : Session::get($key);
        $model->getTasks($id, $attributes); // в данном случаее вернет список пользователей , добавленых для чтения
        $model->archived($this->archived);
        $model->getByBoardType($this->board_type);

        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => Yii::app()->request->getParam('TaskBoardRecord', []),
                'scenario' => 'search',
            ],
            'sort' => [
                'defaultOrder' => 't.created_at ASC'
                //'defaultOrder' => '(t.answer <> " ") , t.created_at ASC'
            ],
        ]);

        $users = TaskBoardRecord::model()->getUsersByBoardId($this->board_type);

        $viewData = [
            'provider' => $provider,
            'users' => $users,
            'sessionKey' => $key,
            'title' => $this->title,
            'filterAdd' => $this->filterAddUrl,
            'filterRemove' => $this->filterRemoveUrl,
        ];

        $controller->render($this->view, $viewData);
    }
}
