<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Index;

use CAction;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;
use Yii;

/**
 * Class Specialized
 */
class Specialized extends CAction
{
    const MANUAL_TASK = 'manual_task';
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @var string
     */
    public $board_type = '';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $view = '';

    /**
     * @var int
     */
    public $archived = 0;

    /**
     * @var string
     */
    public $key = '';

    /**
     * @return void
     */
    public function run()
    {
        $app = Yii::app();
        $userId = $app->user->id;
        $controller = $this->getController();
        Session::set(self::REDIRECT_KEY, $app->controller->action->id);
        Session::set(self::MANUAL_TASK, $this->board_type);

        /* @var $model TaskBoardRecord */
        $model = TaskBoardRecord::model();

        if ($this->board_type !== '') {
            $model->getByBoardType($this->board_type);
        }

        if (isset($_SESSION[$this->key][$userId])) {
            $model->userTasks($userId);
        }

        $model->archived($this->archived);

        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => Yii::app()->request->getParam('TaskBoardRecord', []),
                'scenario' => 'search',
            ],
            'sort' => [
                'defaultOrder' => 't.created_at ASC'
                //'defaultOrder' => '(t.answer <> " ") , t.created_at ASC'
            ],
        ]);

        $viewData = [
            'provider' => $provider,
            'title' => $this->title,
            'key' => $this->key,
        ];

        $controller->render($this->view, $viewData);
    }
}
