<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Index;

use CAction;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;
use Yii;

/**
 * Class All
 */
class All extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @var string
     */
    public $view = 'all';

    /**
     * @var string
     */
    public $title = 'Просмотр всех задач';

    /**
     * @return void
     */
    public function run()
    {
        $app = Yii::app();
        $userId = $app->user->id;
        $controller = $this->getController();

        /* @var $model TaskBoardRecord */
        $model = TaskBoardRecord::model();
        $model->userId($userId);
        $model->archived(0);

        Session::set(self::REDIRECT_KEY, $app->controller->action->id);

        $provider = $model->getDataProvider(false, [
            'pagination' => [
                'pageSize' => 50,
            ],
            'filter' => [
                'attributes' => Yii::app()->request->getParam('TaskBoardRecord', []),
                'scenario' => 'search',
            ],
            'sort' => [
                'defaultOrder' => 't.created_at ASC',
            ],
        ]);

        $viewData = [
            'provider' => $provider,
            'title' => $this->title,
        ];

        $controller->render($this->view, $viewData);
    }
}
