<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Session;

use CAction;
use MommyCom\YiiComponent\Session;

/**
 * Class AddUserToFilter
 */
class AddUserToFilter extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @return void
     */
    public function run()
    {
        $controller = $this->getController();
        if (isset($_POST['adding-user'], $_GET[0]['key'])) {
            $key = $_GET[0]['key'];
            $user = $_POST['adding-user'];
            $_SESSION[$key][$user] = $user;
        }

        $controller->redirect([Session::get(self::REDIRECT_KEY)]);
    }
}
