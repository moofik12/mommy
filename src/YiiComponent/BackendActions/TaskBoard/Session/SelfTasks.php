<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Session;

use CAction;
use Yii;

/**
 * Class SelfTasks
 */
class SelfTasks extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @return void
     */
    public function run()
    {
        $app = Yii::app();
        $userId = $app->user->id;
        $controller = $this->getController();

        if (isset($_POST['key'])) {
            $key = $_POST['key'];
            if (isset($_SESSION[$key][$userId])) {
                unset($_SESSION[$key][$userId]);
            } else {
                $_SESSION[$key][$userId] = $userId;
            }
        }

        $controller->redirect([\MommyCom\YiiComponent\Session::get(self::REDIRECT_KEY)]);
    }
}
