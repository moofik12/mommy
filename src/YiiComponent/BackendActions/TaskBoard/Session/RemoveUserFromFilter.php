<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Session;

use CAction;
use CHttpException;
use Yii;

/**
 * Class RemoveUserFromFilter
 */
class RemoveUserFromFilter extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @throws CHttpException
     */
    public function run()
    {
        $controller = $this->getController();
        if (isset($_POST['adding-user'], $_GET[0]['id'], $_GET[0]['key'])) {
            $user = $_POST['adding-user'];
            $id = $_GET[0]['id'];
            $key = $_GET[0]['key'];

            if ($id !== Yii::app()->user->id) {
                throw new CHttpException(500, 'Вы не можете сдеать это действие.');
            }

            if (isset($_SESSION[$key][$user])) {
                unset($_SESSION[$key][$user]);
            }
        }
        $controller->redirect([\MommyCom\YiiComponent\Session::get(self::REDIRECT_KEY)]);
    }
}
