<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Main;

use CAction;
use CDbException;
use CHttpException;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;
use Yii;

/**
 * Class Delete
 */
class Delete extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @param $id
     *
     * @throws CDbException
     * @throws CHttpException
     */
    public function run($id)
    {
        $controller = $this->getController();
        if (Yii::app()->request->isPostRequest) {
            /* @var $model TaskBoardRecord */
            $model = TaskBoardRecord::loadModel($id);
            $model->delete();
            $controller->redirect([Session::get(self::REDIRECT_KEY)]);
        } else {
            throw new CHttpException(404, 'Проблема при удалении задачи.');
        }
    }
}
