<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Ajax;

use CAction;
use CHttpException;
use MommyCom\Model\Db\TaskBoardRecord;

/**
 * Class Users
 */
class Users extends CAction
{
    /**
     * @param int $id
     *
     * @throws CHttpException
     */
    public function run($id)
    {
        if (!array_key_exists($id, TaskBoardRecord::getQuestionTypes())) {
            throw new CHttpException(500, 'Проблема с выбором доски.');
        }

        $managers = TaskBoardRecord::model()->getUsersByBoardId($id);
        $select = '';
        foreach ($managers as $key => $value) {
            $select .= '<option value=' . $key . '>' . $value . '</option>';
        }

        echo $select;
    }
}
