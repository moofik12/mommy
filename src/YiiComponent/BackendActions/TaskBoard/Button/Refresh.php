<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Button;

use CAction;
use CHttpException;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;

/**
 * Class Refresh
 */
class Refresh extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @param int $id
     *
     * @throws CHttpException
     */
    public function run($id)
    {
        $controller = $this->getController();
        $model = TaskBoardRecord::loadModel($id);
        $model->is_archive = 0;
        $model->created_at = time();
        $model->save(false);
        $controller->redirect([Session::get(self::REDIRECT_KEY)]);
    }
}
