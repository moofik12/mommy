<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Button;

use CAction;
use CHttpException;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;

/**
 * Class To
 */
class To extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @var string
     */
    public $board_type = '';

    /**
     * @param $id
     *
     * @throws CHttpException
     */
    public function run($id)
    {
        $controller = $this->getController();
        $model = TaskBoardRecord::loadModel($id);
        $model->task_board_type = $this->board_type;
        $model->user_id = 0;
        $model->created_at = time();
        $model->save(false);
        $controller->redirect([Session::get(self::REDIRECT_KEY)]);
    }
}
