<?php

namespace MommyCom\YiiComponent\BackendActions\TaskBoard\Button;

use CAction;
use CHttpException;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Session;

/**
 * Class Archive
 */
class Archive extends CAction
{
    const REDIRECT_KEY = 'redirect_url';

    /**
     * @param int $id
     *
     * @throws CHttpException
     */
    public function run($id)
    {
        $controller = $this->getController();
        /* @var $model TaskBoardRecord */
        $model = TaskBoardRecord::loadModel($id);
        $model->is_archive = 1;
        $model->save(false);
        $controller->redirect([Session::get(self::REDIRECT_KEY)]);
    }
}
