<?php

namespace MommyCom\YiiComponent\Backend;

use CBehavior;
use CComponent;
use CException;
use CHttpCookie;
use Yii;

class BeginRequestBehavior extends CBehavior
{
    /**
     * Attaches event handler
     * The attachEventHandler() mathod attaches an event handler to an event.
     * So: onBeginRequest, the handleBeginRequest() method will be called.
     *
     * @param CComponent $owner
     *
     * @throws CException
     */
    public function attach($owner)
    {
        $owner->attachEventHandler('onBeginRequest', [$this, 'handleBeginRequest']);
    }

    /**
     * @return void
     */
    public function handleBeginRequest()
    {
        $app = Yii::app();

        if (isset($_POST['_lang'])) {
            $app->language = $_POST['_lang'];
            $app->user->setState('_lang', $_POST['_lang']);
            $cookie = new CHttpCookie('_lang', $_POST['_lang']);
            $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
            $app->request->cookies['_lang'] = $cookie;
        } else if ($app->user->hasState('_lang'))
            $app->language = $app->user->getState('_lang');
        else if (isset($app->request->cookies['_lang']))
            $app->language = $app->request->cookies['_lang']->value;
    }
}
