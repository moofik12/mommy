<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

/**
 * Class StatisticOrdersPerDay
 *
 * @property-read int $count
 * @property-read int $countEmailVerified
 * @property-read int $countCreatedOrganic
 * @property-read int $countCreatedOffer
 * @property-read int $countCreatedOrganicEmailVerified
 * @property-read int $countCreatedOfferEmailVerified
 * @property-read array $statisticCreatedOffers
 * @property-read array $statisticCreatedOffersEmailVerified
 */
class StatisticUsersPerDays extends \CMap
{
    private $_count = 0;
    private $_countEmailVerified = 0;
    private $_countCreatedOrganic = 0;
    private $_countCreatedOffer = 0;
    private $_countCreatedOrganicEmailVerified = 0;
    private $_countCreatedOfferEmailVerified = 0;

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffers = [];

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffersEmailVerified = [];

    /**
     * @param $timestamp
     * @param array $users
     */
    public function addUsers($timestamp, array $users)
    {
        $listUsers = new StatisticUsersPerDay($timestamp, $users);
        $this->add($timestamp, $listUsers);
        $this->_counting($listUsers);
    }

    /**
     * @return StatisticUsersPerDay[]
     */
    public function getData()
    {
        return $this->toArray();
    }

    /**
     * @param StatisticUsersPerDay $statistic
     */
    protected function _counting(StatisticUsersPerDay $statistic)
    {
        $this->_countCreatedOffer += $statistic->countCreatedOffer;
        $this->_countCreatedOrganic += $statistic->countCreatedOrganic;

        $this->_countCreatedOrganicEmailVerified += $statistic->countCreatedOrganicEmailVerified;
        $this->_countCreatedOfferEmailVerified += $statistic->countCreatedOfferEmailVerified;
        $this->_count += $statistic->count;
        $this->_countEmailVerified += $statistic->countEmailVerified;

        $this->_statisticCreatedOffers = $this->_sumArray($this->_statisticCreatedOffers, $statistic->statisticCreatedOffers);
        $this->_statisticCreatedOffersEmailVerified =
            $this->_sumArray($this->_statisticCreatedOffersEmailVerified, $statistic->statisticCreatedOffersEmailVerified);
    }

    private function _sumArray(array $a, array $b)
    {
        foreach ($b as $key => $value) {
            if (isset($a[$key])) {
                $a[$key] += $value;
            } else {
                $a[$key] = $value;
            }
        }

        return $a;
    }

    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        return parent::__get($name);
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOffer($offerProvider)
    {
        return isset($this->_statisticCreatedOffers[$offerProvider]) ? $this->_statisticCreatedOffers[$offerProvider] : 0;
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOfferEmailVerified($offerProvider)
    {
        return isset($this->_statisticCreatedOffersEmailVerified[$offerProvider])
            ? $this->_statisticCreatedOffersEmailVerified[$offerProvider] : 0;
    }

}
