<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

use CHttpException;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Facade\Translator;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class StatisticOrdersPerDay
 * список заказаов за день
 *
 * @property-read int $day
 * @property-read int $countConfirmed
 * @property-read int $countCanceled
 * @property-read int $countTotal
 * @property-read int $countMerged
 * @property-read int $productsTotalConfirmed
 * @property-read float $usedBonuses
 * @property-read float $usedDiscounts
 * @property-read int $count
 * @property-read float $profit //маржа
 * @property-read float $netProfit //прибыль
 */
class StatisticOrdersPerDay extends \CMap
{
    /**
     * to grid
     *
     * @var
     */
    public $id;

    /**
     * @var integer timestamp начало дня
     */
    public $day;

    private $_countConfirmed = 0;
    private $_countCanceled = 0;
    private $_countNotConfirmed = 0;
    private $_priceTotal = 0;
    private $_countMerged = 0;

    private $_productsTotalConfirmed = 0;
    private $_usedBonuses = 0.0;
    private $_usedDiscounts = 0.0;
    private $_count = 0;
    private $_profit = 0.0;
    private $_netProfit = 0.0;

    /**
     * @param $timestampDay
     * @param $orders
     */
    public function __construct($timestampDay, $orders)
    {
        $this->id = $timestampDay;
        $this->day = $timestampDay;

        $this->_counting($orders);
    }

    /**
     * общая сумма в заказе
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->_priceTotal;
    }

    /**
     * @return integer
     */
    public function getCountConfirmed()
    {
        return $this->_countConfirmed;
    }

    /**
     * @return integer
     */
    public function getCountCanceled()
    {
        $countMerged = $this->getCountMerged();
        return $this->_countCanceled - $countMerged;
    }

    public function getCountMerged()
    {
        return $this->_countMerged;
    }

    /**
     * @return integer
     */
    public function getCountNotConfirmed()
    {
        return $this->_countNotConfirmed;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->_count;
    }

    /**
     * @return float
     */
    public function getUsedBonuses()
    {
        return $this->_usedBonuses;
    }

    /**
     * @return float
     */
    public function getUsedDiscounts()
    {
        return $this->_usedDiscounts;
    }

    /**
     * @return float
     */
    public function getProfit()
    {
        return $this->_profit;
    }

    /**
     * @return float
     */
    public function getNetProfit()
    {
        return $this->_netProfit;
    }

    /**
     * @return float
     */
    public function getPercentUsedBonuses()
    {
        $usedBonuses = $this->_usedBonuses;
        $priceTotal = $this->_priceTotal > 0 ? $this->_priceTotal : 1;

        return round(($usedBonuses * 100) / $priceTotal, 2);
    }

    /**
     * @return float
     */
    public function getPercentUsedDiscounts()
    {
        $usedDiscounts = $this->_usedDiscounts;
        $priceTotal = $this->_priceTotal > 0 ? $this->_priceTotal : 1;

        return round(($usedDiscounts * 100) / $priceTotal, 2);
    }

    /**
     * @return float
     */
    public function getAverageCheck()
    {
        $countConfirmed = $this->getCountConfirmed();

        if ($countConfirmed <= 0) {
            $countConfirmed = 1;
        }

        return round($this->_priceTotal / $countConfirmed, 2);
    }

    /**
     * @return float
     */
    public function getAverageProducts()
    {
        $countProductsConfirmed = $this->getProductsTotalConfirmed();
        $countConfirmed = $this->getCountConfirmed();

        if ($countConfirmed <= 0) {
            $countConfirmed = 1;
        }

        return round($countProductsConfirmed / $countConfirmed, 1);
    }

    /**
     * @return int
     */
    public function getProductsTotalConfirmed()
    {
        return $this->_productsTotalConfirmed;
    }

    /**
     * вычисления
     */
    protected function _counting(array $orders)
    {
        $countConfirmed = 0;
        $countCanceled = 0;
        $countNotConfirmed = 0;
        $countProductsConfirmed = 0;
        $countMerged = 0;
        $priceTotal = 0.0;
        $usedBonuses = 0.0;
        $usedDiscounts = 0.0;
        $count = count($orders);
        $profit = 0.0;

        $ordersIds = array_map(function ($order) {
            return $order->id;
        }, $orders);

        /** @var $orders OrderRecord[] object */
        foreach ($orders as $order) {
            if ($order->isCanceled()) {
                $countCanceled++;
            } elseif ($order->isNotConfirmed()) {
                $countNotConfirmed++;
            } else {
                $countConfirmed++;

                // результат только для подтвержденных
                $priceTotal += $order->price_total;
                $usedBonuses += $order->bonuses;
                $usedDiscounts += $order->discount;
            }

            if ($order->processing_status == OrderRecord::PROCESSING_CANCELLED
                && strpos($order->callcenter_comment, 'Отменено. По причине объединения с заказом №') !== false
            ) {
                $countMerged++;
            }
        }

        /** MYSQL FUNCTION "IN" SUPPORT ONLY 1000 ITEMS */
        $queue = array_chunk($ordersIds, 1000);
        foreach ($queue as $ids) {
            /** @var  OrderProductRecord $productsSelect */
            $productsSelect = OrderProductRecord::model()
                ->with('product')
                ->orderIds($ids)
                ->callcenterStatuses([OrderProductRecord::CALLCENTER_STATUS_ACCEPTED]);

            $productsSelect->getDbCriteria()->select = 'SUM((t.price - product.price_purchase)*t.number) as sum, COUNT(t.id) as counts';
            $row = $productsSelect->getSqlCommand()->query()->read();

            if (!$row) {
                throw new CHttpException(400, Translator::t('Error in margin calculations, average number of units in order!!!'));
            }

            $profit += Cast::toFloat($row['sum']);
            $countProductsConfirmed += Cast::toUInt($row['counts']);
        }

        $this->_countMerged = $countMerged;
        $this->_usedBonuses = $usedBonuses;
        $this->_productsTotalConfirmed = $countProductsConfirmed;
        $this->_priceTotal = $priceTotal;
        $this->_countCanceled = $countCanceled;
        $this->_countConfirmed = $countConfirmed;
        $this->_countNotConfirmed = $countNotConfirmed;
        $this->_count = $count;
        $this->_profit = $profit;
        $this->_usedDiscounts = $usedDiscounts;
        $this->_netProfit = $profit - $usedBonuses - $usedDiscounts;
    }

}
