<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class StatisticOrdersForDayLanding
 *
 * @property-read $countCanceledLanding;
 * @property-read $countNotConfirmedLanding;
 * @property-read $countConfirmedLanding;
 * @property-read $totalPriceLanding;
 * @property-read $totalOrdersLanding;
 */
class StatisticOrdersPerDayLanding extends StatisticOrdersPerDay
{
    private $_statisticLandings = false;
    /**
     * @var StatisticOrdersPerDaysLanding
     */
    private $_sender;

    private $_countCanceledLanding = false;
    private $_countNotConfirmedLanding = false;
    private $_countConfirmedLanding = false;
    private $_totalPriceLanding = false;
    private $_totalOrdersLanding = false;

    public function __construct($timestampDay, array $data, StatisticOrdersPerDaysLanding $sender)
    {
        $this->_sender = $sender;

        parent::__construct($timestampDay, $data);
    }

    protected function _counting(array $orders)
    {
        parent::_counting($orders);

        $statisticLandings = [];

        /** @var OrderRecord[] $orders */
        foreach ($orders as $order) {
            /** @var $orders OrderRecord[] */
            $landingNum = Cast::toUInt($order->user->landing_num);

            $statistic = isset($statisticLandings[$landingNum])
                ? $statisticLandings[$landingNum] : $this->_sender->getLandingStatisticDefault();

            if ($order->isCanceled()) {
                $statistic['countCanceled']++;
            } elseif ($order->isNotConfirmed()) {
                $statistic['countNotConfirmed']++;
            } else {
                $statistic['countConfirmed']++;
            }

            $statistic['totalOrders']++;
            $statistic['totalPrice'] += $order->price_total;

            $statisticLandings[$landingNum] = $statistic;
        }

        $this->_statisticLandings = $statisticLandings;

        $this->_countingLanding();
    }

    protected function _countingLanding()
    {
        $countCanceled = 0;
        $countNotConfirmed = 0;
        $countConfirmed = 0;
        $totalPrice = 0;

        $landings = $this->getStatisticLandings();

        foreach ($landings as $num => $landing) {
            if ($num > 0) {
                $countCanceled += $landing['countCanceled'];
                $countNotConfirmed += $landing['countNotConfirmed'];
                $countConfirmed += $landing['countConfirmed'];
                $totalPrice += $landing['totalPrice'];
            }
        }

        $this->_countCanceledLanding = $countCanceled;
        $this->_countNotConfirmedLanding = $countNotConfirmed;
        $this->_countConfirmedLanding = $countConfirmed;
        $this->_totalPriceLanding = $totalPrice;
        $this->_totalOrdersLanding = $countCanceled + $countNotConfirmed + $countConfirmed;
    }

    /**
     * @return array
     */
    public function getStatisticLandings()
    {
        return $this->_statisticLandings;
    }

    /**
     * @param integer $id
     *
     * @return array
     */
    public function getStatisticLanding($id)
    {
        return isset($this->_statisticLandings[$id]) ? $this->_statisticLandings[$id] : $this->_sender->getLandingStatisticDefault();
    }

    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            if ($this->$property === false) {
                $this->_countingLanding();
            }

            return $this->$property;
        }

        return parent::__get($name);
    }
}
