<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

/**
 * список дней с заказами
 * Class StatisticOrdersPerDays
 *
 * @property-read int $totalAverageProducts;
 * @property-read int $totalAverageCheck;
 * @property-read int $totalConfirmed;
 * @property-read int $totalNotConfirmed;
 * @property-read int $totalCanceled;
 * @property-read int $totalMerged;
 * @property-read int $totalPrice;
 * @property-read int $totalOrders;
 * @property-read float $percentConfirmed;
 * @property-read float $percentNotConfirmed;
 * @property-read float $percentCanceled;
 * @property-read float $usedBonuses
 * @property-read float $usedDiscounts
 * @property-read float $profit //маржа
 * @property-read float $netProfit //чистая прибыль
 * @property-read float $percentUsedBonuses
 * @property-read float $percentUsedDiscount
 */
class StatisticOrdersPerDays extends \CMap
{

    protected $_totalAverageProducts = false;
    protected $_totalAverageCheck = false;
    protected $_totalConfirmed = false;
    protected $_totalNotConfirmed = false;
    protected $_totalMerged = false;
    protected $_totalCanceled = false;
    protected $_totalPrice = false;
    protected $_totalOrders = false;
    protected $_usedBonuses = false;
    protected $_usedDiscounts = false;

    protected $_percentConfirmed = false;
    protected $_percentNotConfirmed = false;
    protected $_percentCanceled = false;
    protected $_percentUsedBonuses = false;
    protected $_percentUsedDiscount = false;
    protected $_profit = false;
    protected $_netProfit = false;

    /**
     * @param $timestamp
     * @param array $orders
     */
    public function addOrders($timestamp, array $orders)
    {
        $listOrders = new StatisticOrdersPerDay($timestamp, $orders);
        $this->add($timestamp, $listOrders);
    }

    /**
     * @return StatisticOrdersPerDay[]
     */
    public function getData()
    {
        return $this->toArray();
    }

    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            if ($this->$property === false) {
                $this->_couting();
            }

            return $this->$property;
        }

        return parent::__get($name);
    }

    protected function _couting()
    {
        $totalAverageProducts = 0;
        $totalAverageCheck = 0;
        $totalConfirmed = 0;
        $totalCanceled = 0;
        $totalNotConfirmed = 0;
        $totalMerged = 0;
        $totalOrders = 0;
        $totalPrice = 0.0;
        $countDays = $this->count() > 0 ? $this->count() : 1;
        $usedBonuses = 0;
        $usedDiscounts = 0;
        $profit = 0.0;

        /** @var $day StatisticOrdersPerDay */
        foreach ($this->getData() as $day) {
            $totalPrice += $day->getPrice();
            $totalCanceled += $day->getCountCanceled();
            $totalConfirmed += $day->getCountConfirmed();
            $totalNotConfirmed += $day->getCountNotConfirmed();
            $totalMerged += $day->getCountMerged();
            $totalAverageCheck += $day->getAverageCheck();
            $totalAverageProducts += $day->getAverageProducts();
            $totalOrders += $day->count();
            $usedBonuses += $day->getUsedBonuses();
            $profit += $day->getProfit();
            $usedDiscounts += $day->getUsedDiscounts();
        }
        $totalOrdersForPercent = $totalOrders > 0 ? $totalOrders : 1;

        $percentConfirmed = ($totalConfirmed / $totalOrdersForPercent) * 100;
        $percentCanceled = ($totalCanceled / $totalOrdersForPercent) * 100;
        $percentNotConfirmed = ($totalNotConfirmed / $totalOrdersForPercent) * 100;
        $countValidOrdersToPrice = $totalConfirmed + $totalNotConfirmed;

        if ($countValidOrdersToPrice <= 0) {
            $countValidOrdersToPrice = 1;
        }
        //проверка на 0
        $calculationsTotalPrice = $totalPrice > 0 ? $totalPrice : 1;

        $this->_totalMerged = $totalMerged;
        $this->_percentUsedBonuses = round(($usedBonuses * 100 / $calculationsTotalPrice), 2);
        $this->_percentUsedDiscount = round(($usedDiscounts * 100 / $calculationsTotalPrice), 2);
        $this->_percentConfirmed = round($percentConfirmed, 1);
        $this->_percentNotConfirmed = round($percentNotConfirmed, 1);
        $this->_percentCanceled = round($percentCanceled, 1);
        $this->_usedBonuses = $usedBonuses;
        $this->_totalPrice = $totalPrice;
        $this->_totalOrders = $totalOrders;
        $this->_totalCanceled = $totalCanceled;
        $this->_totalConfirmed = $totalConfirmed;
        $this->_totalNotConfirmed = $totalNotConfirmed;
        $this->_totalAverageProducts = round($totalAverageProducts / $countDays, 1);
        $this->_totalAverageCheck = round($totalPrice / $countValidOrdersToPrice, 2);
        $this->_profit = $profit;
        $this->_usedDiscounts = $usedDiscounts;
        $this->_netProfit = $profit - $usedBonuses - $usedDiscounts;
    }
}
