<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

use MommyCom\YiiComponent\Type\Cast;

/**
 * список дней с заказами
 * Class StatisticOrdersPerDaysLanding
 */
class StatisticOrdersPerDaysLanding extends StatisticOrdersPerDays
{
    /**
     * @var bool|array
     */
    protected $_landingsStatisticTotal = false;
    protected $_landingsStatistic = [];
    protected $_defaultStatisticLanding = [
        'countCanceled' => 0,
        'countNotConfirmed' => 0,
        'countConfirmed' => 0,
        'totalPrice' => 0,
        'totalOrders' => 0,
    ];

    public function addOrders($timestamp, array $orders)
    {
        $listOrders = new StatisticOrdersPerDayLanding($timestamp, $orders, $this);

        $this->add($timestamp, $listOrders);
    }

    public function setLandingsNum($values)
    {
        $values = Cast::toUIntArr($values);

        foreach ($values as $value) {
            $this->_landingsStatistic[$value] = $this->_defaultStatisticLanding;
        }
    }

    public function getLandingStatisticDefault()
    {
        return $this->_defaultStatisticLanding;
    }

    public function getLandingsStatistic()
    {
        if ($this->_landingsStatisticTotal === false) {
            $this->_counting();
            ksort($this->_landingsStatistic);
        }

        return $this->_landingsStatistic;
    }

    public function getLandingStatisticTotal()
    {
        if ($this->_landingsStatisticTotal === false) {
            $this->_countingTotal();
        }

        return $this->_landingsStatisticTotal;
    }

    protected function _counting()
    {
        /** @var StatisticOrdersPerDayLanding[] $landingsDays */
        $landingsDays = $this->toArray();
        $landingsStatistic = [];

        foreach ($landingsDays as $landingsDay) {
            $landings = $landingsDay->getStatisticLandings();

            foreach ($landings as $num => $landing) {
                if ($num > 0) {
                    $landingTotal = isset($landingsStatistic[$num])
                        ? $landingsStatistic[$num] : $this->_defaultStatisticLanding;

                    $landingTotal['countCanceled'] += $landing['countCanceled'];
                    $landingTotal['countNotConfirmed'] += $landing['countNotConfirmed'];
                    $landingTotal['countConfirmed'] += $landing['countConfirmed'];
                    $landingTotal['totalPrice'] += $landing['totalPrice'];
                    $landingTotal['totalOrders'] += $landing['totalOrders'];

                    $landingsStatistic[$num] = $landingTotal;
                }
            }
        }

        $this->_landingsStatistic = $landingsStatistic;
    }

    protected function _countingTotal()
    {
        $landings = $this->getLandingsStatistic();
        $landingsTotal = $this->_defaultStatisticLanding;

        foreach ($landings as $num => $statistic) {
            if ($num > 0) {
                $landingsTotal['countCanceled'] += $statistic['countCanceled'];
                $landingsTotal['countNotConfirmed'] += $statistic['countNotConfirmed'];
                $landingsTotal['countConfirmed'] += $statistic['countConfirmed'];
                $landingsTotal['totalPrice'] += $statistic['totalPrice'];
                $landingsTotal['totalOrders'] += $statistic['totalOrders'];
            }
        }

        $this->_landingsStatisticTotal = $landingsTotal;
    }
}
