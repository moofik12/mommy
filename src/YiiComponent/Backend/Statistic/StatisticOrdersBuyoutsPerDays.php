<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

use CException;

class StatisticOrdersBuyoutsPerDays extends \CMap
{
    /** @var  float */
    public $totalPaid = 0;

    /** @var float */
    public $totalCancelled = 0;

    /** @var float */
    public $totalUnpaid = 0;

    /** @var array summary info StatisticOrdersBuyoutsPerDay::getStatisticsDelivered() */
    protected $_statisticsDelivered = [];

    /**
     * @param array $statistic
     *
     * @throws CException
     */
    public function setStatistic(array $statistic)
    {
        reset($statistic);

        if ($statistic && !current($statistic) instanceof StatisticOrdersBuyoutsPerDay) {
            throw new CException('order must be instance of StatisticOrdersBuyoutsPerDay');
        }

        $this->_calculation($statistic, true);
    }

    /**
     * @param array $statistic
     *
     * @throws CException
     */
    public function addStatistic(array $statistic)
    {
        reset($statistic);

        if ($statistic && !current($statistic) instanceof StatisticOrdersBuyoutsPerDay) {
            throw new CException('order must be instance of StatisticOrdersBuyoutsPerDay');
        }

        $this->_calculation($statistic);
    }

    /**
     * @param array $statistic
     * @param bool $reset
     */
    protected function _calculation(array $statistic, $reset = false)
    {
        $totalPaid = $reset ? 0 : $this->totalPaid;
        $totalUnpaid = $reset ? 0 : $this->totalUnpaid;
        $totalCancelled = $reset ? 0 : $this->totalCancelled;
        $statisticsDelivered = $reset ? [] : $this->_statisticsDelivered;

        /** @var StatisticOrdersBuyoutsPerDay[] $statistic */
        foreach ($statistic as $statisticDay) {
            $totalPaid += $statisticDay->totalPaid;
            $totalUnpaid += $statisticDay->totalUnpaid;
            $totalCancelled += $statisticDay->totalCancelled;

            foreach ($statisticDay->statisticsDelivered as $type => $statisticDelivered) {
                $workStatistic = isset($statisticsDelivered[$type]) ? $statisticsDelivered[$type] : [];

                if ($workStatistic) {
                    foreach ($statisticDelivered as $key => $value) {
                        $workStatistic[$key] += $value;
                    }
                } else {
                    $workStatistic = $statisticDelivered;
                }

                $statisticsDelivered[$type] = $workStatistic;
            }
        }

        $this->totalPaid = $totalPaid;
        $this->totalUnpaid = $totalUnpaid;
        $this->totalCancelled = $totalCancelled;
        $this->_statisticsDelivered = $statisticsDelivered;
    }

    /**
     * @return array
     */
    public function getStatisticsDelivered()
    {
        return $this->_statisticsDelivered;
    }
}
