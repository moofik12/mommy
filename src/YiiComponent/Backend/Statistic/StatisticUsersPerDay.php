<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

use MommyCom\Model\Db\UserRecord;

/**
 * Class StatisticOrdersPerDay
 * список заказаов за день
 *
 * @property-read int $count
 * @property-read int $countCreatedOrganic
 * @property-read int $countCreatedOffer
 * @property-read int $countEmailVerified
 * @property-read int $countCreatedOrganicEmailVerified
 * @property-read int $countCreatedOfferEmailVerified
 * @property-read array $statisticCreatedOffers
 * @property-read array $statisticCreatedOffersEmailVerified
 */
class StatisticUsersPerDay extends \CMap
{
    /**
     * to grid
     *
     * @var
     */
    public $id;

    /**
     * @var integer timestamp начало дня
     */
    public $day;

    private $_count = 0;
    private $_countCreatedOrganic = 0;
    private $_countCreatedOffer = 0;
    private $_countEmailVerified = 0;
    private $_countCreatedOrganicEmailVerified = 0;
    private $_countCreatedOfferEmailVerified = 0;

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffers = [];

    /**
     * @var array array(offerProvider => count);
     */
    private $_statisticCreatedOffersEmailVerified = [];

    /**
     * @param $timestampDay
     * @param array $users
     */
    public function __construct($timestampDay, $users)
    {
        $this->id = $timestampDay;
        $this->day = $timestampDay;

        $this->_counting($users);
    }

    /**
     * вычисления
     *
     * @param array $users
     */
    protected function _counting(array $users)
    {
        $countCreatedOrganic = 0;
        $countCreatedOffer = 0;
        $count = count($users);
        $countEmailVerified = 0;
        $countCreatedOrganicEmailVerified = 0;
        $countCreatedOfferEmailVerified = 0;

        /** @var $users UserRecord[] object */
        foreach ($users as $user) {
            if ($user->is_email_verified) {
                $countEmailVerified++;

                if ($user->offer_provider == UserRecord::OFFER_PROVIDER_NONE) {
                    $countCreatedOrganicEmailVerified++;
                } else {
                    $countCreatedOfferEmailVerified++;
                    $this->_statisticOfferEmailVerifedCounters($user->offer_provider, 1);
                }
            }

            if ($user->offer_provider == UserRecord::OFFER_PROVIDER_NONE) {
                $countCreatedOrganic++;
            } else {
                $countCreatedOffer++;
                $this->_statisticOfferCounters($user->offer_provider, 1);
            }
        }

        $this->_countCreatedOffer = $countCreatedOffer;
        $this->_countCreatedOrganic = $countCreatedOrganic;
        $this->_count = $count;
        $this->_countCreatedOfferEmailVerified = $countCreatedOfferEmailVerified;
        $this->_countCreatedOrganicEmailVerified = $countCreatedOrganicEmailVerified;
        $this->_countEmailVerified = $countEmailVerified;
    }

    private function _statisticOfferCounters($offerProvider, $count)
    {
        if (!isset($this->_statisticCreatedOffers[$offerProvider])) {
            $this->_statisticCreatedOffers[$offerProvider] = 0;
        }

        $this->_statisticCreatedOffers[$offerProvider] += $count;
    }

    private function _statisticOfferEmailVerifedCounters($offerProvider, $count)
    {
        if (!isset($this->_statisticCreatedOffersEmailVerified[$offerProvider])) {
            $this->_statisticCreatedOffersEmailVerified[$offerProvider] = 0;
        }

        $this->_statisticCreatedOffersEmailVerified[$offerProvider] += $count;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        $property = "_$name";
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        return parent::__get($name);
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOffer($offerProvider)
    {
        return isset($this->_statisticCreatedOffers[$offerProvider]) ? $this->_statisticCreatedOffers[$offerProvider] : 0;
    }

    /**
     * @param int $offerProvider
     *
     * @return int
     */
    public function getStatisticCreatedOfferEmailVerified($offerProvider)
    {
        return isset($this->_statisticCreatedOffersEmailVerified[$offerProvider])
            ? $this->_statisticCreatedOffersEmailVerified[$offerProvider] : 0;
    }
}
