<?php

namespace MommyCom\YiiComponent\Backend\Statistic;

use CException;
use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * Class StatisticOrdersBuyoutsPerDay
 *
 * @property-read array $statisticsDelivered
 */
class StatisticOrdersBuyoutsPerDay extends \CMap
{
    /** @var  int timestamp day */
    public $id = 0;

    /** @var  float */
    public $totalPaid = 0;

    /** @var float */
    public $totalCancelled = 0;

    /** @var float */
    public $totalUnpaid = 0;

    /** @var array summary info about delivered
     *  array(
     *      typeDelivered => $this->_statisticDeliveredTemplate,
     * );
     */
    protected $_statisticsDelivered = [];

    /** @var array */
    protected $_statisticDeliveredTemplate = [
        'freeDelivery' => 0,
        'paidDelivery' => 0,
        'totalDelivery' => 0,
    ];

    /**
     * @param array $orders
     * @param $timeStampDay
     *
     * @throws CException
     */
    public function setOrders(array $orders, $timeStampDay)
    {
        $this->id = $timeStampDay;
        reset($orders);

        if ($orders && !current($orders) instanceof OrderBuyoutRecord) {
            throw new CException('order must be instance of OrderBuyoutRecord');
        }

        $this->_calculation($orders);
    }

    /**
     * @param array $orders
     */
    protected function _calculation(array $orders)
    {
        $statisticDelivered = &$this->_statisticsDelivered;
        /** @var $orders OrderBuyoutRecord[] */
        foreach ($orders as $order) {
            $price = $order->price;

            if ($order->status == OrderBuyoutRecord::STATUS_CANCELLED) {
                $this->totalCancelled += $price;
                continue;
            } elseif ($order->status == OrderBuyoutRecord::STATUS_UNPAYED) {
                $this->totalUnpaid += $price;
                continue;
            }

            $deliveryType = $order->order_delivery_type;

            $item = isset($statisticDelivered[$deliveryType])
                ? $statisticDelivered[$deliveryType] : $this->_statisticDeliveredTemplate;

            $deliveryPrice = $order->delivery_price;
            if ($deliveryPrice > 0) {
                $item['paidDelivery'] += $price;
            } else {
                $item['freeDelivery'] += $price;
            }
            $item['totalDelivery'] += $price;

            $statisticDelivered[$deliveryType] = $item;
            $this->totalPaid += $order->price;
        }

        ksort($statisticDelivered);
        unset($statisticDelivered);
    }

    /**
     * @return array
     */
    public function getStatisticsDelivered()
    {
        return $this->_statisticsDelivered;
    }

    /**
     * @return array
     */
    public static function statisticDeliveryReplacements()
    {
        return [
            'freeDelivery' => Translator::t('Total amount of free delivery'),
            'paidDelivery' => Translator::t('Total amount of chargeable delivery'),
            'totalDelivery' => Translator::t('Total amount of delivery'),
        ];
    }
}
