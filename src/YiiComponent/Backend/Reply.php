<?php

namespace MommyCom\YiiComponent\Backend;

use CMap;
use InvalidArgumentException;

class Reply extends CMap
{
    /**
     * @var array
     */
    private $_errors = [];

    /**
     * @var array
     */
    private $_info = [];

    /**
     * @var array
     */
    private $_warnings = [];

    /**
     * @var array
     */
    private $_data = [];

    /**
     * @var int
     */
    public $maxErrors = 10;

    /**
     * Reply constructor.
     *
     * @param null $data
     * @param bool $readOnly
     *
     * @throws \CException
     */
    public function __construct($data = null, $readOnly = false)
    {
        parent::__construct($data = null, $readOnly = false);
        $this->add('errors', false);
        $this->add('success', true);
    }

    /**
     * @param $message
     *
     * @return bool
     */
    public function addError($message)
    {
        $result = true;

        if ($this->countErrors() < $this->maxErrors) {
            $this->_errors[] = $message;
        } elseif ($this->countErrors() === $this->maxErrors) {
            $this->_errors[] = '...';
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->_errors) > 0;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * @return int
     */
    public function countErrors()
    {
        return count($this->_errors);
    }

    /**
     * @param $message
     */
    public function addInfo($message)
    {
        $this->_info[] = $message;
    }

    /**
     * @return bool
     */
    public function hasInfo()
    {
        return count($this->_info) > 0;
    }

    /**
     * @return array
     */
    public function getWarnings()
    {
        return $this->_warnings;
    }

    /**
     * @param string $warning
     */
    public function addWarning($warning)
    {
        $this->_warnings[] = $warning;
    }

    /**
     * @return bool
     */
    public function hasWarnings()
    {
        return count($this->_warnings) > 0;
    }

    /**
     * @param $state boolean
     *
     * @throws \CException
     */
    public function setState($state)
    {
        $this->add('success', $state);
    }

    /**
     * @return array
     * @throws \CException
     */
    public function getResponse()
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param $value
     */
    public function setData(array $value)
    {
        $this->_data = $value;
    }

    /**
     * @param $key
     * @param $value
     */
    public function dataItem($key, $value)
    {
        if (empty($key)) {
            throw new InvalidArgumentException('Key must be not empty');
        }

        $this->_data[$key] = $value;
    }

    /**
     * @return array
     * @throws \CException
     */
    public function toArray()
    {
        $this->add('errors', false);
        $this->add('info', false);
        $this->add('data', $this->_data);

        if (count($this->_errors) > 0) {
            $this->add('errors', $this->_errors);
        }

        if (count($this->_info) > 0) {
            $this->add('info', $this->_info);
        }

        if (count($this->_warnings) > 0) {
            $this->add('warnings', $this->_warnings);
        }

        return parent::toArray();
    }
}
