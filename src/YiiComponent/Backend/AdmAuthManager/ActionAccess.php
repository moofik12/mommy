<?php

namespace MommyCom\YiiComponent\Backend\AdmAuthManager;

class ActionAccess
{
    public static function getControllerActions()
    {
        return [

            [
                'classDoc' => ' Управление Статистикой входа',
                'className' => 'AdminAuthlogController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Администраторами',
                'className' => 'AdminController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionBan',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxBrandManager',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxBrandManagerById',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxGetById',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Ролями',
                'className' => 'AdminRoleController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка ролей',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Добавление роли',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => ' Удаление роли',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => ' Редактирование роли',
                            'methodName' => 'actionUpdate',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Бонусами',
                'className' => 'BonuspointsController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр бонусов',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Просмотр баланса пользователя',
                            'methodName' => 'actionUserDetail',
                        ],

                        [
                            'methodDoc' => ' Добавление бонуса',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => ' Добавление бонуса по заказу',
                            'methodName' => 'actionAddByOrder',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Бонусами для рассылок',
                'className' => 'BonuspointsPromoController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка бонусов',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Просмотр бонуса для рассылки',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Создание бонуса для рассылки',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => ' Редактирование бонуса для рассылки',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Удаление бонуса для рассылки',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Брендами',
                'className' => 'BrandController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление обратными звонками (коллцентр)',
                'className' => 'CallcenterCallbackController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionQueue',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProcessing',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProcessingActiveUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxCheckQueue',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Call центром',
                'className' => 'CallcenterOrderController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSendRequisitesSmsMessage',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionNew',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProcessing',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionRepeat',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCancelled',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionHistory',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionNotСonfirmed',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderSeparate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderMerge',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderProductUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderProductAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionRefreshProductPrice',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionHistoryDetails',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTask',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionWaitingTask',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProcessingTask',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProductEditable',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderProductReplacementSize',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxCopyDelivery',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProcessingActiveUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProcessingTaskActiveUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearchEventProduct',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearchUser',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxCheckNewOrders',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление задачами для Call центра',
                'className' => 'CallcenterTaskController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTask',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionInbox',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOutbox',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTaskCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTaskUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTaskView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTaskDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOperation',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOperationCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOperationUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOperationDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Актами приема-передачи курьеров',
                'className' => 'CourierStatementController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление PUSH рассылкой',
                'className' => 'DistributionPushController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление E-mail сообщениями',
                'className' => 'EmailController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка E-mail сообщений',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Просмотр E-mail сообщения',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Просмотр только текста E-mail сообщения',
                            'methodName' => 'actionViewBody',
                        ],

                        [
                            'methodDoc' => ' Удаление E-mail сообщения',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => ' Создание E-mail по ID пользователя (пользователей) и постановка в очередь',
                            'methodName' => 'actionCreateFromUsers',
                        ],

                        [
                            'methodDoc' => ' Рассылка сообщений группе пользователей',
                            'methodName' => 'actionCreateDistribution',
                        ],

                        [
                            'methodDoc' => ' Обновление статуса E-mail',
                            'methodName' => 'actionUpdateStatus',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Акциями',
                'className' => 'EventController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetReservedNumber',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetSection',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetColorCode',
                        ],

                        [
                            'methodDoc' => 'Создание фэйковой акции для загрузки на склад',
                            'methodName' => 'actionCreateFake',
                        ],

                        [
                            'methodDoc' => 'Указание фэйковой акции, что она завершена',
                            'methodName' => 'actionReadyForWarehouse',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Товарами акции',
                'className' => 'EventProductController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUploadAssortment',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDownloadAssortment',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUploadStockAssortment',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUploadRequest',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDownloadProductRequest',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление обратной связью пользователей',
                'className' => 'FeedbackController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр новых обращений',
                            'methodName' => 'actionNew',
                        ],

                        [
                            'methodDoc' => ' Просмотр обработанных обращений',
                            'methodName' => 'actionHistory',
                        ],

                        [
                            'methodDoc' => ' Отмена обращения',
                            'methodName' => 'actionCancel',
                        ],

                        [
                            'methodDoc' => ' Ответ на обращение',
                            'methodName' => 'actionAnswer',
                        ],
                    ],
            ],

            [
                'classDoc' => '',
                'className' => 'FeedrController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxProvinces',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxCities',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSuburbs',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxAreas',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxRates',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление файлами',
                'className' => 'FileManagerController',
                'methods' =>
                    [
                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' загрузка файлов через Redactor',
                            'methodName' => 'actionRedactorImageUpload',
                        ],

                        [
                            'methodDoc' => ' выбор файлов через Redactor',
                            'methodName' => 'actionRedactorImageGetJson',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление справкой',
                'className' => 'HelperPageController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionpositionPage',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCategory',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCategoryCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCategoryDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCategoryUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPage',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPageCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPageDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPageUpdate',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Блокировкой по IP',
                'className' => 'IpBanController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Печать лейблов для курьеров',
                'className' => 'LabelCourierPrinterController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDocuments',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionStatement',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Печать лейблов',
                'className' => 'LabelPrinterController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionWarehouseProducts',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionFulfilment',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAddress',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderInvoice',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderAffiliatePromocodes',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionBarcode',
                        ],

                        [
                            'methodDoc' => ' Печать акта возврата поставщику',
                            'methodName' => 'actionWarehouseStatementReturn',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Актами возврата денег',
                'className' => 'MoneyControlController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrders',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionValidateItem',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionValidateFile',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Отслеживание выкупа заказов',
                'className' => 'OrderBuyoutController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Список заказов',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Изменение статуса',
                            'methodName' => 'actionChangeStatus',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Отслеживание оплат',
                'className' => 'OrderBuyoutTrackingController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Контроль заказов',
                'className' => 'OrderControlController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление оплатами заказов',
                'className' => 'OrderPayController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' История оплат заказов',
                            'methodName' => 'actionPaid',
                        ],

                        [
                            'methodDoc' => ' Проблемные оплаты',
                            'methodName' => 'actionPaidErrors',
                        ],

                        [
                            'methodDoc' => ' Отмена проблемных оплат',
                            'methodName' => 'actionPaidCanceled',
                        ],

                        [
                            'methodDoc' => ' Создание возврата для проблемных оплат',
                            'methodName' => 'actionPaidCreateReturn',
                        ],

                        [
                            'methodDoc' => ' Обработка оплат заказов которые оплачены на р/с',
                            'methodName' => 'actionPaidFromSettlementAccount',
                        ],

                        [
                            'methodDoc' => ' Отмена оплаты через р/с',
                            'methodName' => 'actionPaidFromSettlementAccountWrong',
                        ],

                        [
                            'methodDoc' => ' Подтверждение оплаты через р/с',
                            'methodName' => 'actionPaidFromSettlementAccountConfirm',
                        ],

                        [
                            'methodDoc' => ' История оплат заказов через р/с',
                            'methodName' => 'actionPaidFromSettlementAccountHistory',
                        ],

                        [
                            'methodDoc' => ' Возвраты денег оплачееные по б/н',
                            'methodName' => 'actionBankRefundPayReady',
                        ],

                        [
                            'methodDoc' => ' Подтверждение возврата денег оплаченные по б/н',
                            'methodName' => 'actionBankRefundConfirmReady',
                        ],

                        [
                            'methodDoc' => ' История возвратов денег оплаченные по б/н',
                            'methodName' => 'actionBankRefund',
                        ],

                        [
                            'methodDoc' => ' Ручное создание оплаты б/н за заказ',
                            'methodName' => 'actionCreatePaid',
                        ],

                        [
                            'methodDoc' => ' Фиксирование выплаты',
                            'methodName' => 'actionBankRefundConfirm',
                        ],

                        [
                            'methodDoc' => ' Подтверждение возврата денег',
                            'methodName' => 'actionBankRefundNeedPay',
                        ],

                        [
                            'methodDoc' => ' Отмена возврата денег',
                            'methodName' => 'actionBankRefundDelete',
                        ],

                        [
                            'methodDoc' => ' Отмена возврата денег',
                            'methodName' => 'actionBankRefundRefresh',
                        ],

                        [
                            'methodDoc' => ' Просмотр списка логирования оплат сделанные через приват24 мерчант',
                            'methodName' => 'actionLogs',
                        ],

                        [
                            'methodDoc' => ' Просмотр списка заказов которые ожидают оплаты по б/н',
                            'methodName' => 'actionWait',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление возвратами',
                'className' => 'OrderReturnController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPayReady',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionHistory',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPay',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionBack',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCancel',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionRestore',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProductCancel',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProductRestore',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetProductCondition',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetProductPay',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetProductClientComment',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetProductComment',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Отслеживание посылок',
                'className' => 'OrderTrackingController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Смена номера ТТН',
                            'methodName' => 'actionChangeTrackcode',
                        ],
                    ],
            ],

            [
                'classDoc' => '',
                'className' => 'Privat24MerchantHistoryController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Каталог товаров',
                'className' => 'ProductCatalogController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Товаром',
                'className' => 'ProductController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' загрузка файлов через TbFileUpload в FileStorage',
                            'methodName' => 'actionPhotoUpload',
                        ],

                        [
                            'methodDoc' => ' удаление файлов из FileStorage',
                            'methodName' => 'actionPhotoDelete',
                        ],

                        [
                            'methodDoc' => ' Пакетная загрузка изображений для товаров',
                            'methodName' => 'actionUpdateImages',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление секциями товаров',
                'className' => 'ProductSectionController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка секций товаров',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Добавление секции товаров',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => ' Редактирование секции товаров',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Поиск секций',
                            'methodName' => 'actionAjaxSearch',
                        ],

                        [
                            'methodDoc' => ' Поиск секции',
                            'methodName' => 'actionAjaxGetById',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Промокодами',
                'className' => 'PromocodeController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление взаимоотношениями с поставщиками (дропшиппинг)',
                'className' => 'RelationPaymentController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр всех записей',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Просмотр списка к оплате',
                            'methodName' => 'actionToPay',
                        ],

                        [
                            'methodDoc' => ' Добавление штрафов',
                            'methodName' => 'actionAddPenalty',
                        ],

                        [
                            'methodDoc' => ' Добавление транзакций',
                            'methodName' => 'actionPay',
                        ],

                        [
                            'methodDoc' => ' Удаление транзакций',
                            'methodName' => 'actionDeletePay',
                        ],

                        [
                            'methodDoc' => ' Изменение статуса штрафа',
                            'methodName' => 'actionChangePenaltyState',
                        ],

                        [
                            'methodDoc' => ' Простмотр взаимоотношения с поставщиком',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Обновление взаимоотношения с поставщиком',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxForceUpdateStatistic',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление отчетами (заказы, обработка заказов)',
                'className' => 'ReportingController',
            ],

            [
                'classDoc' => ' Управление SMS сообщениями',
                'className' => 'SmsController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCreateFromUsers',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCreateDistribution',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxValid',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Отправка SMS сообщений',
                'className' => 'SmsSendController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Категориями статических страниц',
                'className' => 'StaticPageCategoryController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Статическими страницами',
                'className' => 'StaticPageController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionposition',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionToggle',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Просмотр Статистики',
                'className' => 'StatisticController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrdersPerDaysBase',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrdersPerDays',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrdersPerMonths',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrdersPerDaysLanding',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrdersBuyout',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUsers',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxOrdersPerMonth',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Просмотр новой статистики с пользовательскими фильтрами',
                'className' => 'StatisticCustomController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Статистика заказов',
                            'methodName' => 'actionOrders',
                        ],

                        [
                            'methodDoc' => ' Статистика заказов (базовая)',
                            'methodName' => 'actionOrdersBase',
                        ],

                        [
                            'methodDoc' => ' Статистика пользователей',
                            'methodName' => 'actionUsers',
                        ],

                        [
                            'methodDoc' => ' Статистика оплаты поставщикам',
                            'methodName' => 'actionSuppliersPayment',
                        ],

                        [
                            'methodDoc' => ' Детальная информации об оплатах',
                            'methodName' => 'actionOrderDetailPaid',
                        ],

                        [
                            'methodDoc' => ' Выкуп заказов',
                            'methodName' => 'actionOrdersBuyout',
                        ],

                        [
                            'methodDoc' => ' Детальная информация о выкупе заказов',
                            'methodName' => 'actionOrdersSuppliersBuyout',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление пользовательским фильтром',
                'className' => 'StatisticCustomFilterUserController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Управление списком',
                            'methodName' => 'actionList',
                        ],

                        [
                            'methodDoc' => ' Обновление списка',
                            'methodName' => 'actionListUpdate',
                        ],

                        [
                            'methodDoc' => ' Добавление списка',
                            'methodName' => 'actionListAdd',
                        ],

                        [
                            'methodDoc' => ' Очистка списка',
                            'methodName' => 'actionListClear',
                        ],

                        [
                            'methodDoc' => ' Удаление списка',
                            'methodName' => 'actionListDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Заказами на упаковку',
                'className' => 'StorekeeperOrderController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionMailReady',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionMailUnReady',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionBackToPackaging',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetAsMailed',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionMailed',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionPackaging',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionRenewAdminOrderBind',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSwitchProductWarehouse',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSwitchProductWarehouse',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionTabletBind',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetOrderComment',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetOrderWeight',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSwitchProductStatus',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionBindWarehouseConfirm',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUnbindWarehouse',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionSetProductComment',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Поставщиками',
                'className' => 'SupplierController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxGetById',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Просмотр Акций для поставщиков',
                'className' => 'SupplierEventController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление заявками на возврат поставщикам (дропшиппинг)',
                'className' => 'SupplierOrderReturnController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка заявок',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Создание завки на возврат поставщику',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => ' Обновление завки на возврат поставщику',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Просмотр завки на возврат поставщику',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Удаление завки на возврат поставщику',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionOrderInfo',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Оплатами Поставщикам',
                'className' => 'SupplierPaymentController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр Списока оплат',
                            'methodName' => 'actionReadyPay',
                        ],

                        [
                            'methodDoc' => ' Просмотр Списока оплат в ожидании',
                            'methodName' => 'actionWaitPay',
                        ],

                        [
                            'methodDoc' => ' Просмотр списка поставок поставщиками',
                            'methodName' => 'actionDelivery',
                        ],

                        [
                            'methodDoc' => ' Ручное подтверждение поставки всех товаров поставщиком',
                            'methodName' => 'actionApplyFullDelivery',
                        ],

                        [
                            'methodDoc' => ' Просмотр Списока оплат',
                            'methodName' => 'actionHistory',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Изменение дополнительных данных оплаты',
                            'methodName' => 'actionUpdateAddition',
                        ],

                        [
                            'methodDoc' => ' Подтверждение оплаты',
                            'methodName' => 'actionConfirm',
                        ],

                        [
                            'methodDoc' => ' Отмена оплаты',
                            'methodName' => 'actionCancel',
                        ],

                        [
                            'methodDoc' => ' Просмотр списока контрактов',
                            'methodName' => 'actionContracts',
                        ],

                        [
                            'methodDoc' => ' Просмотр содержимого контракта',
                            'methodName' => 'actionContractView',
                        ],

                        [
                            'methodDoc' => ' Обновление контракта',
                            'methodName' => 'actionContractUpdate',
                        ],

                        [
                            'methodDoc' => ' Обновление поставщиков контракта',
                            'methodName' => 'actionContractSuppliersUpdate',
                        ],

                        [
                            'methodDoc' => ' Создание контракта',
                            'methodName' => 'actionContractCreate',
                        ],

                        [
                            'methodDoc' => ' Удаление поставщика из котнракта',
                            'methodName' => 'actionContractSuppliersDelete',
                        ],

                        [
                            'methodDoc' => ' Добавление поставщика в котнракт',
                            'methodName' => 'actionContractSupplierAdd',
                        ],

                        [
                            'methodDoc' => ' Просмотр Акта оплаты поставщикам (печать/удлаение/подтверждение платежа)',
                            'methodName' => 'actionActView',
                        ],

                        [
                            'methodDoc' => ' Скачивание Акта оплаты поставщикам (печать/удлаение/подтверждение платежа)',
                            'methodName' => 'actionActDownload',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionActAjaxUpdate',
                        ],

                        [
                            'methodDoc' => ' Удаление Акта (AJAX)',
                            'methodName' => 'actionActDelete',
                        ],

                        [
                            'methodDoc' => ' Обновление Акта (AJAX)',
                            'methodName' => 'actionActDeletePosition',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionActAjaxAddPosition',
                        ],

                        [
                            'methodDoc' => ' Создание Акта оплаты поставщикам',
                            'methodName' => 'actionCreateActFromId',
                        ],

                        [
                            'methodDoc' => ' Ajax поиск оплат',
                            'methodName' => 'actionAjaxSearch',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Пользователями планшетов',
                'className' => 'TabletUserController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Диспетчер задач',
                'className' => 'TaskBoardController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Добавление задач вручную',
                            'methodName' => 'actionAddManualTask',
                        ],

                        [
                            'methodDoc' => ' просмотр задачи и добавление ответа',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Добавление задач вручную из карточки заказов',
                            'methodName' => 'actionAddManualTaskFromOrderCard',
                        ],

                        [
                            'methodDoc' => ' Добавление задач из карточки заказа',
                            'methodName' => 'actionAddTaskFromOrderCard',
                        ],

                        [
                            'methodDoc' => ' Редактирование задач',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Поиск по таскам',
                            'methodName' => 'actionSearch',
                        ],

                        [
                            'methodDoc' => ' Получение Уведомлений',
                            'methodName' => 'actionNotification',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Добавление ролей к диспетчеру задач',
                'className' => 'TaskBoardUserRoleConnectionController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр привязанный ролей к доскам',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Добавление ролей к доскам',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => ' Удаление ролей',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление "черным списком" пользователей',
                'className' => 'UserBlackListController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Добавление пользователя в список',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => ' Обновление пользователя в списке',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Исключение пользователя из списка',
                            'methodName' => 'actionDelete',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Пользователями',
                'className' => 'UserController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка пользователей',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAdd',
                        ],

                        [
                            'methodDoc' => ' Редактирование пользователя',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Удаление пользователя',
                            'methodName' => 'actionBanned',
                        ],

                        [
                            'methodDoc' => ' Удаление пользователя',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionMessages',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxGetById',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Просмотр Логирования',
                'className' => 'UserGlobalLoggerController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление пользователями партнерами',
                'className' => 'UserPartnerController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка пользователей-партнеров по ресурсу прихода и периоду',
                            'methodName' => 'actionResource',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionAjaxSearch',
                        ],

                        [
                            'methodDoc' => ' Поиск по ид парнера или емейлу',
                            'methodName' => 'actionAjaxSearchByIdOrEmail',
                        ],

                        [
                            'methodDoc' => ' Поиск партнера по id',
                            'methodName' => 'actionAjaxGetById',
                        ],

                        [
                            'methodDoc' => ' Просмотр списка заявок на вывод средств партнерам',
                            'methodName' => 'actionFinance',
                        ],

                        [
                            'methodDoc' => ' Проводка выплаты с заявки партнера на Приват',
                            'methodName' => 'actionMakeOutputPrivat',
                        ],

                        [
                            'methodDoc' => ' Проводка выплаты с заявки партнера на Mamam',
                            'methodName' => 'actionMakeOutputMamam',
                        ],

                        [
                            'methodDoc' => ' Отмена выплаты с заявки партнера и возврат средств на баланс партнера',
                            'methodName' => 'actionCanceledOutput',
                        ],

                        [
                            'methodDoc' => ' Просмотр истории заявок на вывод средств партнерам',
                            'methodName' => 'actionHistoryFinance',
                        ],

                        [
                            'methodDoc' => ' Просмотр списка партнеров',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Подтвержденный баланс и детальный просмотр информации о партнере',
                            'methodName' => 'actionBalance',
                        ],

                        [
                            'methodDoc' => ' Статистика партнерской программы',
                            'methodName' => 'actionStatistic',
                        ],

                        [
                            'methodDoc' => ' Приглашенные партнерами пользователи',
                            'methodName' => 'actionInvited',
                        ],

                        [
                            'methodDoc' => ' Заказы приглашенных партнерами пользователей',
                            'methodName' => 'actionOrderUsers',
                        ],

                        [
                            'methodDoc' => ' Финансы: возвраты',
                            'methodName' => 'actionReturn',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Просмотр неоформленных корзин пользователей',
                'className' => 'UserUnformedCartController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Просмотр списка неоформленных корзин',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionView',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление Складом',
                'className' => 'WarehouseController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProductReception',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionProductCancellation',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionGetRequestFile',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Скачивание отчета по складу',
                'className' => 'WarehouseDownloadController',
                'methods' =>
                    [

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionGrouped',
                        ],
                    ],
            ],

            [
                'classDoc' => ' Управление возвратами поставщикам',
                'className' => 'WarehouseStatementReturnController',
                'methods' =>
                    [

                        [
                            'methodDoc' => ' Список актов',
                            'methodName' => 'actionIndex',
                        ],

                        [
                            'methodDoc' => ' Создание акта',
                            'methodName' => 'actionCreate',
                        ],

                        [
                            'methodDoc' => ' Проверка валидности создания акта',
                            'methodName' => 'actionCheckCreate',
                        ],

                        [
                            'methodDoc' => ' Редактирование акта/подтверждение',
                            'methodName' => 'actionUpdate',
                        ],

                        [
                            'methodDoc' => ' Удаление неподтвержденных актов',
                            'methodName' => 'actionDelete',
                        ],

                        [
                            'methodDoc' => ' Просмотр акта',
                            'methodName' => 'actionView',
                        ],

                        [
                            'methodDoc' => ' Изменение маркера акта при формировании оплаты поставщику',
                            'methodName' => 'actionToggleAccountedReturns',
                        ],

                        [
                            'methodDoc' => ' Выбор товаров для создания акта',
                            'methodName' => 'actionReadyToReturn',
                        ],

                        [
                            'methodDoc' => ' Удаление товаров из акта (при создании)',
                            'methodName' => 'actionDeletePosition',
                        ],

                        [
                            'methodDoc' => '',
                            'methodName' => 'actionFile',
                        ],
                    ],
            ],
        ];
    }
}
