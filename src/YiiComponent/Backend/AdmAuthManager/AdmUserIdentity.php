<?php

namespace MommyCom\YiiComponent\Backend\AdmAuthManager;

use CLogger;
use CUserIdentity;
use MommyCom\Model\Db\AdminAuthlogRecord;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\IpBanRecord;
use Yii;

class AdmUserIdentity extends CUserIdentity
{
    /**
     * @var AdminUserRecord|null
     */
    private $userRecord = null;

    /**
     * @return bool
     */
    public function authenticate()
    {
        /* @var $object AdminUserRecord */
        $object = AdminUserRecord::model()->findByAttributes(['login' => $this->username]);
        if (is_null($object)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->errorMessage = "Неверный логин";

            $this->_logging($this->username, false, $this->errorMessage);

            return !$this->errorCode;
        }

        if ($object->password !== AdminUserRecord::encodePassword($this->password, $object->password_salt)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            $this->errorMessage = "Неверный пароль";

            $this->_logging($object->login, false, $this->errorMessage);

            return !$this->errorCode;
        }

        //фильтрация удаленных и блокированных пользователей
        if ($object->status == AdminUserRecord::STATUS_HIDE || $object->status == AdminUserRecord::STATUS_BAN) {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            $this->errorMessage = "Доступ заблокирован";

            $this->_logging($object->login, false, $this->errorMessage);

            return !$this->errorCode;
        }

        /**
         * фильтрация блокированных пользователей по IP
         *
         * @var $bannedModel IpBanRecord
         */
        $ip = Yii::app()->request->getUserHostAddress();
        $bannedModel = IpBanRecord::model()->bannedIp($ip)->section(IpBanRecord::SECTION_AUTH_ADMIN)->find();
        if ($bannedModel) {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            $this->errorMessage = "Доступ заблокирован";

            $this->_logging($object->login, false, $this->errorMessage . ' по IP (конец блокировки - '
                . date('d/m/Y H:i', $bannedModel->banned_to) . ')');

            return !$this->errorCode;
        }

        $this->userRecord = $object;
        $this->errorCode = self::ERROR_NONE;
        $model = $object;
        $model->setScenario('login');
        $model->save();

        /** fixme Нужна логика для вытягивания user_id если админ зарегистрировался на сайте */
        $this->_logging($object->login, true);

        return !$this->errorCode;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->userRecord->id;
    }

    /**
     * @param $login
     * @param $success
     * @param null|integer $user_id
     */
    private function _logging($login, $success, $comment = null, $user_id = null)
    {
        //логирование
        $ip = Yii::app()->request->getUserHostAddress();
        $logger = new AdminAuthlogRecord();
        $logger->ip = $ip;
        $logger->user_agent = Yii::app()->request->userAgent;
        $logger->login = $login;
        $logger->is_success = $success;
        $logger->comment = $comment;
        $logger->user_id = $user_id;

        if (!$logger->save()) {
            Yii::log('Ошибка записи ' . get_class($logger), CLogger::LEVEL_ERROR);
        }
    }
}
