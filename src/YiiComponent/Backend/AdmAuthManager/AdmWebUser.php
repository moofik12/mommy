<?php

namespace MommyCom\YiiComponent\Backend\AdmAuthManager;

use CMap;
use CWebUser;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\IpBanRecord;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Utf8;

class AdmWebUser extends CWebUser
{
    /**
     * название контроллера
     *
     * @var array
     */
    public $allowController = [];

    private $_model = null;
    private $_rules = null;

    /**
     * ключи (section) с доступом которые запрещены
     *
     * @var array
     */
    private $_bannedKeys = false;

    /**
     * Модель пользователя
     *
     * @return AdminUserRecord
     */
    public function getModel()
    {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = AdminUserRecord::model()->findByPk($this->id);
        }
        return $this->_model;
    }

    /**
     * @return array|null
     */
    public function getRules()
    {
        if ($this->isGuest) {
            return null;
        }

        if ($this->_rules === null) {
            $rules = [];
            $roles = $this->getModel()->roles;
            if ($roles) {
                foreach ($roles as $role) {
                    $rules = CMap::mergeArray($rules, $role->rulesArray);
                }
            }

            $this->_rules = $rules;
        }

        return $this->_rules;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        if (!$this->isGuest) {
            return Cast::toBool($this->getModel()->is_root);
        }
        return false;
    }

    /**
     * проверяет запрещено ли данное действие блокировкой по ip
     *
     * @param int|bool $sectionKey self::$ipBanRecord
     *
     * @return bool
     */
    public function isBanned($sectionKey = false)
    {
        if ($this->_bannedKeys === false) {
            $this->_bannedKeys = IpBanRecord::model()->getBannedKeys();
        }

        return empty($this->_bannedKeys) ? false : in_array($sectionKey, $this->_bannedKeys);
    }

    public function checkAccess($urlAccess = null, $default = null, $allowCaching = true)
    {
        if ($this->isRoot()) {
            return true;
        }

        $separator = '.';
        $allAction = '*';

        $access = $this->getRules();
        $controller = \Yii::app()->controller->id;
        $controllerAccess = mb_strtoupper($controller . 'controller');

        $action = \Yii::app()->controller->action->id;
        $actionAccess = mb_strtoupper('action' . $action);

        if (!is_null($urlAccess)) {
            $urlAccess = Utf8::explode('/', $urlAccess);

            if (empty($urlAccess[0]) || empty($urlAccess[1])) {
                return false;
            }

            $controller = $urlAccess[0];
            $controllerAccess = mb_strtoupper($urlAccess[0] . 'controller');
            $actionAccess = mb_strtoupper('action' . $urlAccess[1]);
        }

        if (in_array($controller, $this->allowController)) {
            return true;
        }

        $validAccess = [$controllerAccess . $separator . $allAction
            , $controllerAccess . $separator . $actionAccess];

        if (!empty($access)) {
            $accessCheck = false;
            if (in_array($validAccess[0], $access) || in_array($validAccess[1], $access)) {
                $accessCheck = true;
            }

            return $accessCheck;
        }

        return false;
    }

    /**
     * @param array $allowController
     */
    public function setAllowController(array $allowController): void
    {
        $this->allowController = $allowController;
    }

    /**
     * @param array $loginUrl
     */
    public function setLoginUrl(array $loginUrl): void
    {
        $this->loginUrl = $loginUrl;
    }
}
