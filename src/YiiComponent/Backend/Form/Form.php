<?php

namespace MommyCom\YiiComponent\Backend\Form;

use CActiveForm;
use TbForm;
use Yii;

class Form extends TbForm
{
    /**
     * Create the TbForm and assign the TbActiveForm with options as activeForm
     *
     * @param array $config
     * @param $parent
     * @param array $options
     * @param null $model
     *
     * @return mixed
     */
    public static function createForm($config, $parent, $options = [], $model = null)
    {
        $options['class'] = 'TbActiveForm';

        $form = new static($config, $parent);
        $form->activeForm = $options;

        return $form;
    }

    /**
     * Выполняет ajax валидацию в слачае ajax запроса и если она включена в настройках виджета,
     * затем вызывает родительский метод CForm::submitted()
     *
     * @param string $buttonName
     * @param bool $loadData
     *
     * @return bool|true
     */
    public function submitted($buttonName = 'submit', $loadData = true)
    {
        $ret = $this->performAjax();
        if ($ret) {
            $ret = parent::submitted($buttonName, $loadData);
        }
        return $ret;
    }

    /**
     * Performs check for ajax request and generate responce of validation status
     *
     * @return true to allow use in if condition
     */
    protected function performAjax()
    {
        $ajaxVar = strcasecmp($this->method, 'get') ? Yii::app()->request->getPost('ajax') : Yii::app()->request->getQuery('ajax');
        // $this->getActiveFormWidget() - в данном случае вернет null т.к. форма не отрендерена т.е. виджета еще нет, есть только его конфиг $this->activeForm
        // если есть id то проверяем его, если нет то только isset($ajaxVar) ?
        $ajax = isset($ajaxVar) ? (isset($this->activeForm['id']) ? $this->activeForm['id'] === $ajaxVar : true) : false;
        //у формы созданной конструктором CForm обязательно есть скрытое поле с уникальным ID (CForm::getUniqueId())
        if ($this->getAjaxValidation() && $ajax && $this->clicked($this->getUniqueId())) {
            $this->loadData();
            // because of renderPartial method needs to clean output buffer
            ob_get_clean();
            echo CActiveForm::validate($this->getModels(), null, false);
            Yii::app()->end();
        }
        return true;
    }

    /**
     * @return bool status of using ajax validation
     */
    public function getAjaxValidation()
    {
        if (isset($this->activeForm['enableAjaxValidation']))
            return $this->activeForm['enableAjaxValidation'];
        return false;
    }
}
