<?php

namespace MommyCom\YiiComponent\Backend\Form;

use CHtml;

class TbModalForm extends Form
{
    /**
     * Renders the body content of this form.
     * This method mainly renders {@link elements} and {@link buttons}.
     * If {@link title} or {@link description} is specified, they will be rendered as well.
     * And if the associated model contains error, the error summary may also be displayed.
     * The form tag will not be rendered. Please call {@link renderBegin} and {@link renderEnd}
     * to render the open and close tags of the form.
     * You may override this method to customize the rendering of the form.
     *
     * @return string the rendering result
     */
    public function renderBody()
    {
        $output = '';
        if ($this->title !== null) {
            if ($this->getParent() instanceof self) {
                $fieldset = true;
                $attributes = $this->attributes;
                unset($attributes['name'], $attributes['type']);
                $output = CHtml::openTag('fieldset', $attributes) . "<legend>" . $this->title . "</legend>\n";
            } else
                $output = "<div class=\"modal-header\"><a class=\"close\" data-dismiss=\"modal\">×</a><h4>" . $this->title . "</h4></div>\n";
        }
        $output .= '<div class="modal-body">';

        if ($this->description !== null)
            $output .= "<div class=\"description\">\n" . $this->description . "</div>\n";

        if ($this->showErrorSummary && ($model = $this->getModel(false)) !== null)
            $output .= $this->getActiveFormWidget()->errorSummary($model) . "\n";

        //после элементов формы закрывается "modal-body", перед кнопками
        $output .= $this->renderElements();
        $output .= "</div>\n";
        $output .= $this->renderButtons() . "\n";

        if (isset($fieldset))
            $output .= "</fieldset>\n";

        return $output;
    }

    public function renderButtons()
    {
        $output = '';
        foreach ($this->getButtons() as $button)
            $output .= $this->renderElement($button) . '&nbsp;';

        //form-actions div wrapper only if not is inline form
        if ($output !== '' && $this->getActiveFormWidget()->type !== 'inline')
            $output = "<div class=\"modal-footer\">\n" . $output . "</div>\n";

        return $output;
    }
}
