<?php

namespace MommyCom\YiiComponent\Backend;

use CMap;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class Select2Response
 *
 * @property $pageCount
 */
class Select2Reply extends CMap
{
    /**
     * @var array
     */
    private $_items = [];

    /**
     * Select2Reply constructor.
     *
     * @param null $data
     * @param bool $readOnly
     *
     * @throws \CException
     */
    public function __construct($data = null, $readOnly = false)
    {
        parent::__construct($data = null, $readOnly = false);
        $this->add('pageCount', 0);
        $this->add('items', $this->_items);
    }

    /**
     * @param $item array
     */
    public function addItem(array $item)
    {
        $this->_items[] = $item;
    }

    /**
     * @param array $items
     */
    public function addItems(array $items)
    {
        $this->_items = CMap::mergeArray($this->_items, $items);
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        return count($this->_items) > 0;
    }

    public function getPageCount()
    {
        return $this->itemAt('pageCount');
    }

    /**
     * @param integer $count
     *
     * @throws \CException
     */
    public function setPageCount($count)
    {
        $this->add('pageCount', Cast::toUInt($count));
    }

    /**
     * @return array
     * @throws \CException
     */
    public function getResponse()
    {
        return $this->toArray();
    }

    /**
     * @return array
     * @throws \CException
     */
    public function toArray()
    {
        $this->add('items', $this->_items);

        return parent::toArray();
    }
}
