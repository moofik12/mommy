<?php

namespace MommyCom\YiiComponent\Backend;

class SupplierOrderReturnControllerTrackingInfo
{
    /** @var string любая инофмация о результате получения данных */
    public $fetchStatusText = '';

    /** @var  int  номер ТТН */
    public $number;
    /** @var  float сумма обратной доставки */
    public $redeliverySum;
    /** @var string */
    public $statusText = '';
    /** @var string дата создания ТТН */
    public $dateCreated = '';
    /** @var string  ФИО получателя */
    public $recipientFullName = '';
    /** @var string город получателя */
    public $recipientCity = '';
    /** @var string адрес получателя */
    public $recipientAddress = '';
    /** @var string телефон получателя */
    public $recipientPhone = '';
    /** @var string  ФИО отправителя */
    public $senderFullName = '';
    /** @var string город отправителя */
    public $senderCity = '';
    /** @var string адрес отправителя */
    public $senderAddress = '';
    /** @var string телефон отправителя */
    public $senderPhone = '';
}
