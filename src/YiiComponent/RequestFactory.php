<?php

namespace MommyCom\YiiComponent;

class RequestFactory
{
    /**
     * @return \CHttpRequest
     */
    public static function getRequest(): \CHttpRequest
    {
        return \Yii::app()->getRequest();
    }
}
