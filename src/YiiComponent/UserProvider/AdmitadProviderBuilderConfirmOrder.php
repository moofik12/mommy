<?php

namespace MommyCom\YiiComponent\UserProvider;

use CJSON;
use MommyCom\Model\Db\OrderRecord;
use Yii;

class AdmitadProviderBuilderConfirmOrder extends AdmitadProviderBuilderAbstract
{
    /**
     * @var AdmitadProviderBuilderModel[]
     */
    private $_models = [];

    function addOrder(OrderRecord $value)
    {
        $orders = array_merge([$value], $value->getRelationOrders(true));

        foreach ($orders as $order) {
            $positionCounts = count($order->positions);

            foreach ($order->positions as $positionNumber => $position) {
                $model = new AdmitadProviderBuilderModel();

                $model->uid = $order->offer_id;
                $model->order_id = $order->id;
                $model->position_id = $positionNumber + 1;
                $model->tariff_code = $this->getTariffCode($order);
                $model->currency_code = Yii::app()->countries->getCurrency()->getCode();
                $model->position_count = $positionCounts;
                $model->quantity = $position->number;
                $model->product_id = $position->product_id;
                $model->price = $position->price;
                $model->payment_type = 'sale';
                $model->tracking = '';
                $model->coupon = !empty($order->promocode);
                //$model->old_customer = $order->user ? $order->user->is_email_verified : 0;

                $this->_models[] = $model;
            }
        }
    }

    /**
     * Рапределение заказов по разным тарифам
     *
     * @param OrderRecord $order
     *
     * @return int
     */
    protected function getTariffCode(OrderRecord $order)
    {
        $price = $order->getPrice();

        if ($price >= 500 && $price < 1000) {
            return 2;
        } elseif ($price > 1000) {
            return 3;
        }

        return 1; // До 500
    }

    function addProduct()
    {
        //not Support
        return;
    }

    function toHtml()
    {
        $html = '';
        $modelsData = array_map(function ($item) {
            /** @var AdmitadProviderBuilderModel $item */
            return $item->getFilledAttributes();
        }, $this->_models);

        $provider = $this->getProvider();

        foreach ($modelsData as $modelData) {
            $html .= '<img src="//ad.admitad.com/r?campaign_code=' . $provider->campaign_code
                . '&action_code=' . $provider->action_code . '&response_type=' . $provider->response_type
                . '&' . http_build_query($modelData) . '" width="1" height="1" alt="">';
        }

        return $html;
    }

    function toJs()
    {
        $provider = $this->getProvider();
        $modelsData = array_map(function ($item) {
            /** @var AdmitadProviderBuilderModel $item */
            return $item->getFilledAttributes();
        }, $this->_models);

        $js = <<<EOD
(function (d, w) {
    w._admitadPixel = {
        response_type: '{$provider->response_type}',
        action_code: '{$provider->action_code}',
        campaign_code: '{$provider->campaign_code}'
    };
    w._admitadPositions = w._admitadPositions || [];
EOD;

        //fucking
        foreach ($modelsData as $model) {
            $js .= "w._admitadPositions.push(" . CJSON::encode($model) . ");";
        }

        $js .= <<<EOD
    var id = '_admitad-pixel';
    if (d.getElementById(id)) { return; }
    var s = d.createElement('script');
    s.id = id;
    var r = (new Date).getTime();
    var protocol = (d.location.protocol === 'https:' ? 'https:' : 'http:');
    s.src = protocol + '//cdn.asbmit.com/static/js/ad/pixel.min.js?r=' + r;
    d.head.appendChild(s);
})(document, window)
EOD;

        return $js;
    }
}
