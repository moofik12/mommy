<?php

namespace MommyCom\YiiComponent\UserProvider;

use MommyCom\Model\Db\OrderRecord;

abstract class AdmitadProviderBuilderAbstract
{
    protected $_provider;

    abstract function toJs();

    abstract function toHtml();

    abstract function addOrder(OrderRecord $order);

    abstract function addProduct();

    public function __construct(AdmitadProvider $provider)
    {
        $this->_provider = $provider;
    }

    /**
     * @return AdmitadProvider
     */
    public function getProvider()
    {
        return $this->_provider;
    }
} 
