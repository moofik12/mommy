<?php

namespace MommyCom\YiiComponent\UserProvider;

use LogicException;

class AdmitadProvider
{
    const TYPE_CONFIRM_ORDER = 'confirm_order';

    private $type;

    public $campaign_code = '6bae56227d';
    public $action_code = '2';
    public $response_type = 'img';

    public function __construct($type)
    {
        if (!in_array($type, [
            self::TYPE_CONFIRM_ORDER,
        ])) {
            throw new LogicException("Type '$type' not support");
        }

        $this->type = $type;
    }

    /**
     * @return AdmitadProviderBuilderAbstract
     */
    public function getBuilder()
    {
        $model = null;

        switch ($this->type) {
            case self::TYPE_CONFIRM_ORDER:
                $model = new AdmitadProviderBuilderConfirmOrder($this);
                break;

            default:
                break;
        }

        return $model;
    }
} 
