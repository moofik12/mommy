<?php

namespace MommyCom\YiiComponent\UserProvider;

use CModel;
use ReflectionClass;

class AdmitadProviderBuilderModel extends CModel
{
    /**
     * Уникальный    идентификатор    Пользователя
     *
     * @var string
     */
    public $uid;

    /** @var  int */
    public $order_id;

    /**
     * Порядковый номер товара в заказе
     *
     * @var int
     */
    public $position_id;

    /**
     * @var string|int
     */
    public $client_id;

    /**
     * @var int
     */
    public $tariff_code;

    /**
     * Код вылюты
     *
     * @var string
     */
    public $currency_code;

    /**
     * Код страны
     *
     * @var string
     */
    public $country_code;

    /**
     * кол-во товаров в заказе
     *
     * @var int
     */
    public $position_count = 1; //default

    /**
     * Цена товара
     *
     * @var float
     */
    public $price;

    /**
     * Кол-во товара
     *
     * @var int
     */
    public $quantity;

    /**
     *  Артикул
     *
     * @var string
     */
    public $product_id;

    /**
     * Разрешение экрана пользователя
     *
     * @var string
     */
    public $screen;

    public $tracking;

    /**
     * @var int (0|1)
     */
    public $old_customer;
    /**
     * @var int (0|1)
     */
    public $coupon;
    /**
     * @var string sale|lead
     */
    public $payment_type;

    private static $_names = [];

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        $className = get_class($this);
        if (!isset(self::$_names[$className])) {
            $class = new ReflectionClass(get_class($this));
            $names = [];
            foreach ($class->getProperties() as $property) {
                $name = $property->getName();
                if ($property->isPublic() && !$property->isStatic())
                    $names[] = $name;
            }
            return self::$_names[$className] = $names;
        } else
            return self::$_names[$className];
    }

    /**
     * @param null $names
     *
     * @return array
     */
    public function getFilledAttributes($names = null)
    {
        return array_filter($this->getAttributes($names), function ($item) {
            return !is_null($item);
        });
    }
}
