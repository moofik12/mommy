<?php

namespace MommyCom\YiiComponent\TaskBoard;

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\Model\Db\WarehouseProductRecord;

class TaskDetermination
{
    /**
     * @param $owner
     * @param array $products
     * @param $order
     * @param $type
     * @param $description
     */
    public function __construct($owner, array $products, $order, $type, $description)
    {
        $this->owner = $owner;
        $this->order = $order;
        $this->products = $products;
        $this->type = $type;
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function save()
    {
        switch ($this->type) {
            case TaskBoardRecord::Q_PRODUCT:
                $products = OrderProductRecord::getProducts($this->products, $this->order); // массив продуктов
                $data = $this->getDataForProductQuestions($products);
                $this->createTasks($data);
                break;
            case TaskBoardRecord::Q_SHIPPING:
                $this->createTask(TaskBoardRecord::TYPE_COORDINATION);
                break;
            case TaskBoardRecord::Q_COURIER:
            case TaskBoardRecord::Q_RENEWALS_FORWARDING:
                $this->createTask(TaskBoardRecord::TYPE_STORAGE);
                break;
        }

        return true;
    }

    /**
     * @param $board
     */
    public function createTask($board)
    {
        /* @var $task TaskBoardRecord */
        $task = new TaskBoardRecord();
        $task->user_id = 0;
        $task->owner_id = $this->owner;
        $task->task_board_type = $board;
        $task->order_id = $this->order;
        $task->products = $this->products !== null ? (string)implode(',', $this->products) : null;
        $task->question_description = $this->description;
        $task->question_type = $this->type;
        $task->save(false);
    }

    /**
     * @param array $data
     */
    public function createTasks(array $data)
    {
        foreach ($data as $eId => $manager) {
            $task = new TaskBoardRecord();
            $task->owner_id = $this->owner;
            $task->order_id = $this->order;
            $task->question_description = $this->description;
            $task->question_type = $this->type;
            foreach ($manager as $managerId => $boardData) {
                $task->user_id = $managerId;
                foreach ($boardData as $board => $products) {
                    $task->task_board_type = $board;
                    $prod = [];
                    foreach ($products as $k => $v) {
                        array_push($prod, $v);
                    }
                    $task->products = implode(',', $prod);
                }
            }
            $task->save(false);
        }
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function getDataForProductQuestions(array $data)
    {
        $result = [];
        foreach ($data as $key => $value) {
            if (!($value instanceof OrderProductRecord)) {
                continue;
            }

            /* @var $value OrderProductRecord */
            $product = $value->product;
            $event = $product->event;
            $brandManager = $event->brandManager->id;
            $status = $event->brandManager->status;

            $wereHouse = WarehouseProductRecord::model()->find($product->id);

            if (in_array($wereHouse->status, [
                WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                WarehouseProductRecord::STATUS_MAILED_TO_CLIENT,
                WarehouseProductRecord::STATUS_READY_SHIPPING_TO_SUPPLIER,
                WarehouseProductRecord::STATUS_MAILED_TO_SUPPLIER,
                WarehouseProductRecord::STATUS_BROKEN], true)) {
                $result[$event->id][0][TaskBoardRecord::TYPE_STORAGE][] = $value->product->id;
            } elseif ((int)$status === (int)AdminUserRecord::STATUS_ACTIVE) {
                $result[$event->id][$brandManager][TaskBoardRecord::TYPE_BRAND_MANAGER][] = $value->product->id;
            }
        }
        return $result;
    }

}
