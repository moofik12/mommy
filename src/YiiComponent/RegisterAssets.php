<?php

namespace MommyCom\YiiComponent;

use CApplicationComponent;
use CClientScript;
use Yii;

final class RegisterAssets extends CApplicationComponent
{
    public $packages = [];
    public $css = [];
    public $scripts = [];

    public function init()
    {
        parent::init();
        $clientScript = Yii::app()->getComponent('clientScript');
        /* @var $clientScript CClientScript */

        foreach ($this->packages as $name => $package) {
            $clientScript->packages[$name] = $package;
            $render = isset($package['render']) ? (bool)$package['render'] : true;
            if ($render) {
                $clientScript->registerPackage($name);

                if (isset($package['jsActions'])) {
                    $packageUrl = $clientScript->getPackageBaseUrl($name);
                    $packageDir = Yii::getPathOfAlias($package['basePath']);

                    list($controller, $action) = explode('/', Yii::app()->urlManager->parseUrl(Yii::app()->request)) + [Yii::app()->defaultController, 'index'];
                    $controller = $controller ? $controller : Yii::app()->defaultController;

                    foreach ($package['jsActions'] as $filename) {
                        $filename = str_replace('<controller>', $controller, $filename);
                        $filename = str_replace('<action>', $action, $filename);

                        if (is_file($packageDir . '/' . $filename)) {
                            $modifiedAt = @filemtime($packageDir . '/' . $filename);
                            $clientScript->registerScriptFile($packageUrl . '/' . $filename . '?' . $modifiedAt, CClientScript::POS_END);
                        }
                    }

                    unset($package['jsActions']);
                }
            }
        }

        foreach ($this->css as $id => $css) {
            $media = 'all';
            if (is_array($css)) {
                list($css, $media) = $css; //unpack css array 
            }
            $clientScript->registerCss($id, $css, $media);
        }

        foreach ($this->scripts as $id => $script) {
            $position = CClientScript::POS_END;
            if (is_array($script)) {
                list($script, $position) = $script; //unpack script array
            }
            $clientScript->registerScript($id, $script, $position);
        }
    }
}
