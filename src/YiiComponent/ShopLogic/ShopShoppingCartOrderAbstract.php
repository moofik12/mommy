<?php

namespace MommyCom\YiiComponent\ShopLogic;

use CMap;
use InvalidArgumentException;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\OrderDiscountCampaignRecord;
use MommyCom\YiiComponent\Type\Cast;

/**
 * @property-read boolean $isGuest
 * @property-read boolean $isFull
 * @property-read boolean $isEmpty
 * @property-read boolean $isOwn
 * @property-read int $supplierId
 */
abstract class ShopShoppingCartOrderAbstract extends CMap
{
    const MAX_CART_SIZE = 20;

    /**
     * Собственный заказ
     *
     * @var bool
     */
    protected $_isOwn;

    /** @var ShopShoppingCart */
    protected $_cart;

    /** @var  string */
    protected $_displayName;

    /** @var  int */
    protected $_supplierId = 0;

    /**
     * Мин сумма для оформления заказа
     *
     * @return float
     */
    abstract function getMinOrderAmount();

    /**
     * @return integer
     */
    static function getMaxCartSize()
    {
        return static::MAX_CART_SIZE;
    }

    /**
     * @param bool|true $onlyReserved
     *
     * @return null|OrderDiscountCampaignRecord
     */
    abstract function getOrderDiscountCampaign($onlyReserved = true);

    /**
     * @return float|int
     */
    abstract function getUserDiscountAsBonus();

    /**
     * @param bool $onlyReserved
     *
     * @return int
     */
    abstract function getBonusesCost($onlyReserved = true);

    /**
     * @param string $promocode
     * @param bool $onlyReserved
     *
     * @return float
     */
    abstract function getPromocodeSavings($promocode, $onlyReserved = true);

    /**
     * @param bool $onlyReserved
     *
     * @return float
     */
    abstract function getUserDiscountSavings($onlyReserved = true);

    /**
     * @param bool|true $onlyReserved
     * @param bool $possible возможно будет успользовать скидку
     *
     * @return bool
     */
    abstract function isEnableOrderDiscountCampaign($onlyReserved = true, $possible = false);

    /**
     * ShopShoppingCartOrderAbstract constructor.
     *
     * @param bool $isOwn
     * @param ShopShoppingCart $cart
     * @param int $supplierId
     * @param null $data
     * @param bool $readOnly
     */
    public function __construct($isOwn, ShopShoppingCart $cart, $supplierId = 0, $data = null, $readOnly = false)
    {
        $this->_isOwn = !!$isOwn;
        $this->_cart = $cart;
        $this->_supplierId = (int)$supplierId;
        parent::__construct($data, $readOnly);
    }

    /**
     * @return ShopShoppingCart
     */
    public function getCart()
    {
        return $this->_cart;
    }

    /**
     * @return bool
     */
    public function getIsOwn()
    {
        return $this->_isOwn;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->_supplierId;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->_displayName;
    }

    /**
     * @param string $supplierName
     */
    public function setDisplayName($supplierName)
    {
        $this->_displayName = $supplierName;
    }

    /**
     * Add $value items to position with $key specified
     *
     * @return void
     *
     * @param mixed $key - not used
     * @param CartRecord $value
     *
     * @throws InvalidArgumentException
     */
    public function add($key, $value)
    {
        if (!$value instanceof CartRecord) {
            throw new InvalidArgumentException('$value not instanceof EventProductRecord');
        }

        parent::add($key, $value);
    }

    /**
     * Returns count of items in shopping cart
     *
     * @return int
     */
    public function getItemsCount()
    {
        $count = 0;
        foreach ($this->getPositions() as $position) {
            $count += $position->number;
        }
        return $count;
    }

    /**
     * @return int
     */
    public function getReservedCount()
    {
        return count($this->getReservedPositions());
    }

    /**
     * Returns count of items in shopping cart
     *
     * @return int
     */
    public function getReservedItemsCount()
    {
        $count = 0;
        foreach ($this->getReservedPositions() as $position) {
            $count += $position->number;
        }
        return $count;
    }

    /**
     * Возможность оформить заказ
     *
     * @return bool
     */
    public function isAvailableToBuy()
    {
        return $this->getReservedItemsCount() > 0 && $this->isEnoughToBuy();
    }

    /**
     * @param bool $onlyReserved
     *
     * @return bool
     */
    public function isEnoughToBuy($onlyReserved = true)
    {
        if ($this->_getCost($onlyReserved) > $this->getMinOrderAmount()) {
            return true;
        }

        return false;
    }

    /**
     * @param $onlyReserved
     *
     * @return float
     */
    protected function _getCost($onlyReserved)
    {
        $price = 0.0;

        $list = $onlyReserved ? $this->getReservedPositions() : $this->getPositions();
        foreach ($list as $position) {
            $price += $position->totalPrice;
        }

        return $price;
    }

    /**
     * Возвращает суммарную стоимость всех тововаров в корзине
     *
     * @param bool $withBonuses
     * @param string $promocode промокод
     * @param bool $onlyReserved
     *
     * @return float
     */
    public function getCost($withBonuses = true, $promocode = '', $onlyReserved = true)
    {
        $price = $this->_getCost($onlyReserved);

        if ($promocode) {
            $price -= $this->getPromocodeSavings($promocode, $onlyReserved);
        }

        if ($withBonuses) {
            $price -= $this->getBonusesCost($onlyReserved);
        }

        return $price;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return int
     */
    public function getWeight($onlyReserved = true)
    {
        $weight = 0;

        $list = $onlyReserved ? $this->getReservedPositions() : $this->getPositions();
        foreach ($list as $position) {
            $productWeight = Cast::toUInt($position->product->weight);
            $weight += $productWeight * $position->number;
        }

        return $weight;
    }

    /**
     * Экономия
     *
     * @param bool $onlyReserved
     *
     * @return float
     */
    public function getSavings($onlyReserved = true)
    {
        $price = 0.0;

        $list = $onlyReserved ? $this->getReservedPositions() : $this->getPositions();
        foreach ($list as $position) {
            $price += $position->getTotalSavings();
        }

        return $price;
    }

    /**
     * @return int|float
     */
    public function getUserTotalSaving()
    {
        return max($this->getOrderDiscountCampaignSavings(), $this->getUserDiscountSavings(), 0);
    }

    /**
     * @param bool $onlyReserved
     *
     * @return float
     */
    public function getOrderDiscountCampaignSavings($onlyReserved = true)
    {
        $amount = 0.0;
        if ($this->getCart()->getIsGuest()) {
            return $amount;
        }

        $campaign = $this->getOrderDiscountCampaign($onlyReserved);
        if ($campaign) {
            $amount = $campaign->getDiscount($this->_getCost($onlyReserved));
        }

        return $amount;
    }

    /**
     * Returns array all positions
     *
     * @return CartRecord[]
     */
    public function getPositions()
    {
        return $this->toArray();
    }

    /**
     * @return bool
     */
    public function getIsFull()
    {
        return $this->getCount() >= self::getMaxCartSize();
    }

    /**
     * @return CartRecord[]
     */
    public function getReservedPositions()
    {
        $positions = [];

        foreach ($this->getPositions() as $position) {
            if ($position->getIsReserved()) {
                $positions[] = $position;
            }
        }

        return $positions;
    }

}
