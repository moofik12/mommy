<?php

namespace MommyCom\YiiComponent\ShopLogic;

use CMap;

/**
 * Class ShopBonusPointsTypes
 * @method ShopBonusPointsType[] toArray()
 * @method ShopBonusPointsType|null itemAt() itemAt(string $key)
 */
class ShopBonusPointsTypes extends CMap
{
    public function __construct($data = null)
    {
        parent::__construct($data, true);
    }

    public function addItem(ShopBonusPointsType $item)
    {
        $this->setReadOnly(false);
        parent::add($item->type, $item);
        $this->setReadOnly(true);
    }

    public function removeItem(ShopBonusPointsType $item)
    {
        $this->setReadOnly(false);
        parent::remove($item->type);
        $this->setReadOnly(true);
    }
}

