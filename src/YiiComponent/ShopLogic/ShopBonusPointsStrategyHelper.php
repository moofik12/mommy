<?php

namespace MommyCom\YiiComponent\ShopLogic;

use LogicException;

/**
 * Class ShopBonusPointsStrategy
 * Глобальный класс описывающий логику списывания разных бонусов
 */
abstract class ShopBonusPointsStrategyHelper
{
    /** @var  ShopBonusPoints */
    protected $_shopBonusPoints;

    protected function __construct(ShopBonusPoints $shopBonusPoints)
    {
        $this->_shopBonusPoints = $shopBonusPoints;
    }

    /**
     * @param ShopBonusPoints $shopBonusPoints
     * @param string $strategy look ShopBonusPoints strategies constants
     *
     * @return ShopBonusPointsStrategyHelper
     */
    public static function init(ShopBonusPoints $shopBonusPoints, $strategy)
    {
        $model = null;
        switch ($strategy) {
            case ShopBonusPoints::STRATEGY_SUBTRACT_FIRST_PRESENT:
                $model = new ShopBonusPointsStrategyHelperPresent($shopBonusPoints);
                break;

            case ShopBonusPoints::STRATEGY_SUBTRACT_FIRST_INNER:
                $model = new ShopBonusPointsStrategyHelperInner($shopBonusPoints);
                break;

            case ShopBonusPoints::STRATEGY_SUBTRACT_ORDER:
                $model = new ShopBonusPointsStrategyHelperOrder($shopBonusPoints);
                break;
        }

        if ($model === null) {
            throw new LogicException('Unknown strategy model!' . __FUNCTION__);
        }

        return $model;
    }

    /**
     * @param int|float $points
     * @param bool $subtractLocked
     *
     * @return ShopBonusPointsTypes
     */
    abstract public function getToSubtractTypes($points, $subtractLocked = true);
}
