<?php

namespace MommyCom\YiiComponent\ShopLogic;

use CComponent;

/**
 * Class ShopBonusPointsType
 *
 * @property-read int $type
 * @property-read int|float $value
 */
class ShopBonusPointsType extends CComponent
{
    /** @var  int */
    private $_type;
    /** @var  int|float */
    private $_value;

    /**
     * @param int $type
     * @param int|float $value
     */
    public function __construct($type, $value)
    {
        $this->_type = $type;
        $this->_value = $value;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @return int|float
     */
    public function getValue()
    {
        return $this->_value;
    }
}
