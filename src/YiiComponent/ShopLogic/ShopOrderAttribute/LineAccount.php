<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Service\RegionalValidator\SanitizerInterface;
use MommyCom\Service\RegionalValidator\ValidatorInterface;
use MommyCom\Service\Translator\TranslatorInterface;

class LineAccount implements Attribute
{
    public const NAME = 'lineAccount';

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var SanitizerInterface
     */
    private $sanitizer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var mixed
     */
    private $value = null;

    /**
     * @param TranslatorInterface $translator
     * @param SanitizerInterface $sanitizer
     * @param ValidatorInterface $validator
     */
    public function __construct(
        TranslatorInterface $translator,
        SanitizerInterface $sanitizer,
        ValidatorInterface $validator
    )
    {
        $this->translator = $translator;
        $this->sanitizer = $sanitizer;
        $this->validator = $validator;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->translator->t('LINE account');
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            [self::NAME, AttributeSanitizer::class, 'sanitizer' => $this->sanitizer],
            [self::NAME, AttributeValidator::class, 'validator' => $this->validator],
        ];
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
