<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Service\RegionalValidator\SanitizerInterface;

class AttributeSanitizer extends \CValidator
{
    /**
     * @var SanitizerInterface
     */
    public $sanitizer = null;

    /**
     * @param \CModel $object
     * @param string $attribute
     */
    protected function validateAttribute($object, $attribute)
    {
        if (!($this->sanitizer instanceof SanitizerInterface)) {
            throw new \LogicException(sprintf(
                'Property "sanitizer" must be instance of "%s", "%s" given.',
                SanitizerInterface::class,
                is_object($this->sanitizer) ? get_class($this->sanitizer) : gettype($this->sanitizer)
            ));
        }

        $value = $object->{$attribute};
        $value = $this->sanitizer->sanitize($value);
        $object->{$attribute} = $value;
    }
}
