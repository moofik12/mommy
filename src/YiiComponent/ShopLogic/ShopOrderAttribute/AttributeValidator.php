<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Service\RegionalValidator\ValidatorInterface;
use MommyCom\Service\RegionalValidator\ValidatorRunner;
use MommyCom\YiiComponent\Facade\Translator;

class AttributeValidator extends \CValidator
{
    /**
     * @var bool
     */
    public $allowEmpty = true;

    /**
     * @var ValidatorInterface
     */
    public $validator = null;

    /**
     * @param \CModel $object
     * @param string $attribute
     */
    protected function validateAttribute($object, $attribute)
    {
        if (!($this->validator instanceof ValidatorInterface)) {
            throw new \LogicException(sprintf(
                'Property "validator" must be instance of "%s", "%s" given.',
                ValidatorRunner::class,
                is_object($this->validator) ? get_class($this->validator) : gettype($this->validator)
            ));
        }

        $value = $object->{$attribute};

        if (null === $value && $this->allowEmpty) {
            return;
        }

        $violations = $this->validator->validate($value);

        foreach ($violations as $violation) {
            $object->addError($attribute, Translator::t($violation->getMessage()));

            break;
        }
    }
}
