<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Service\Delivery\AvailableDeliveries;

class DeliveryTypeValidator extends \CValidator
{
    /**
     * @var AvailableDeliveries|null
     */
    public $availableDeliveries = null;

    /**
     * @param \CModel $object
     * @param string $attribute
     */
    protected function validateAttribute($object, $attribute)
    {
        if (!$this->availableDeliveries instanceof AvailableDeliveries) {
            throw new \RuntimeException(
                sprintf('AvailableDeliveries must be instance of "%s".', AvailableDeliveries::class)
            );
        }

        $value = $object->$attribute;

        if ($this->availableDeliveries->hasDelivery((int)$value)) {
            return;
        }

        $object->addError($attribute, 'Invalid delivery type.');
    }
}
