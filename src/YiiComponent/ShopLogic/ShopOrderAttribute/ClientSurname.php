<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Service\RegionalValidator\ValidatorInterface;
use MommyCom\Service\Translator\TranslatorInterface;

class ClientSurname implements Attribute
{
    public const NAME = 'clientSurname';

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var mixed
     */
    private $value = null;

    /**
     * PromoCode constructor.
     *
     * @param TranslatorInterface $translator
     * @param ValidatorInterface $validator
     */
    public function __construct(TranslatorInterface $translator, ValidatorInterface $validator)
    {
        $this->translator = $translator;
        $this->validator = $validator;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->translator->t('Surname');
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            [self::NAME, 'type', 'type' => 'string'],
            [self::NAME, 'filter', 'filter' => 'trim'],
            [self::NAME, 'safe'],
            [self::NAME, AttributeValidator::class, 'validator' => $this->validator],
        ];
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
