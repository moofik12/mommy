<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Regional;

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Attribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractAttributesFactory
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AbstractAttributesFactory constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param ContainerInterface $container
     *
     * @return self
     */
    public static function getFactory(ContainerInterface $container): self
    {
        /** @var Regions $regions */
        $regions = $container->get(Regions::class);
        $region = $regions->getServerRegion()->getRegionName();

        switch ($region) {
            case Region::INDONESIA:
                return new IndonesiaAttributesFactory($container);
            case Region::VIETNAM:
                return new VietnamAttributesFactory($container);
            case Region::ROMANIA:
            case Region::COLOMBIA:
                return new ExampleAttributesFactory($container);
            default:
                return new DefaultAttributesFactory($container);
        }
    }

    /**
     * @return Attribute[]
     */
    abstract public function getAttributes(): array;
}
