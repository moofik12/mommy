<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Regional;

class DefaultAttributesFactory extends AbstractAttributesFactory
{
    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return [];
    }
}
