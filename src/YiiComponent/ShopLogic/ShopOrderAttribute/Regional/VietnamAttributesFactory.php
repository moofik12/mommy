<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Regional;

use MommyCom\Service\RegionalValidator\Filter\CharFilter;
use MommyCom\Service\RegionalValidator\Filter\SpecialCharFilter;
use MommyCom\Service\RegionalValidator\FilterRunner;
use MommyCom\Service\RegionalValidator\ValidatorRunner;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\ClientName;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\ClientSurname;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PhoneNumber;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class VietnamAttributesFactory extends AbstractAttributesFactory
{
    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->container->get(TranslatorInterface::class);

        $validator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();

        return [
            $this->createPhoneNumberAttribute($translator, $validator),
            $this->createClientNameAttribute($translator, $validator),
            $this->createClientSurnameAttribute($translator, $validator),
        ];
    }

    private function createPhoneNumberAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): PhoneNumber
    {
        $filterRunner = new FilterRunner([
            new CharFilter(['-', ' ']),
            new SpecialCharFilter(SpecialCharFilter::REMOVE_FIRST_CHARACTER),
        ]);

        $validatorRunner = new ValidatorRunner([
            new NotBlank(),
            new Length([
                'min' => 3,
                'max' => 12,
                'minMessage' => $translator('Phone number has not enough characters.'),
                'maxMessage' => $translator('Phone number has too much characters.'),
            ]),
            new Regex([
                'pattern' => '/^\d+$/',
                'message' => $translator('Phone number contains illegal characters.'),
            ]),
        ], $validator);

        return new PhoneNumber($translator, $filterRunner, $validatorRunner);
    }

    private function createClientNameAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): ClientName
    {
        $validatorRunner = new ValidatorRunner([
            new NotBlank(),
        ], $validator);

        return new ClientName($translator, $validatorRunner);
    }

    private function createClientSurnameAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): ClientSurname
    {
        $validatorRunner = new ValidatorRunner([
            new NotBlank(),
        ], $validator);

        return new ClientSurname($translator, $validatorRunner);
    }
}
