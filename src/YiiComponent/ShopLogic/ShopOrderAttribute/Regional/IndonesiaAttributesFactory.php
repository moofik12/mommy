<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Regional;

use MommyCom\Service\RegionalValidator\Filter\CharFilter;
use MommyCom\Service\RegionalValidator\Filter\SpecialCharFilter;
use MommyCom\Service\RegionalValidator\Filter\StartWithFilter;
use MommyCom\Service\RegionalValidator\FilterRunner;
use MommyCom\Service\RegionalValidator\ValidatorRunner;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\ClientName;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\ClientSurname;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PhoneNumber;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class IndonesiaAttributesFactory extends AbstractAttributesFactory
{
    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->container->get(TranslatorInterface::class);

        $validator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();

        $phoneNumberAttribute = $this->createPhoneNumberAttribute($translator, $validator);
        $clientNameAttribute = $this->createClientNameAttribute($translator, $validator);
        $clientSurnameAttribute = $this->createClientSurnameAttribute($translator, $validator);

        return [
            $phoneNumberAttribute,
            $clientNameAttribute,
            $clientSurnameAttribute,
        ];
    }

    private function createPhoneNumberAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): PhoneNumber
    {
        $filterRunner = new FilterRunner([
            new CharFilter(['-', ' ']),
            new SpecialCharFilter(SpecialCharFilter::REMOVE_FIRST_CHARACTER),
            new StartWithFilter(['0', '62']),
        ]);

        $callbackValidator = function ($value, ExecutionContextInterface $context) use ($translator) {
            if ($value) {
                return;
            }

            $context
                ->buildViolation($translator('You must specify phone number.'))
                ->atPath(PhoneNumber::NAME)
                ->addViolation();
        };

        $validatorRunner = new ValidatorRunner([
            new Callback($callbackValidator),
            new Length([
                'min' => 8,
                'max' => 12,
                'minMessage' => $translator('Phone number has not enough characters.'),
                'maxMessage' => $translator('Phone number has too much characters.'),
            ]),
            new Regex([
                'pattern' => '/^8\d+$/',
                'message' => $translator('Phone number contains illegal characters.'),
            ]),
        ], $validator);

        return new PhoneNumber($translator, $filterRunner, $validatorRunner);
    }

    private function createClientNameAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): ClientName
    {
        $validatorRunner = new ValidatorRunner([
        ], $validator);

        return new ClientName($translator, $validatorRunner);
    }

    private function createClientSurnameAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): ClientSurname
    {
        $validatorRunner = new ValidatorRunner([
        ], $validator);

        return new ClientSurname($translator, $validatorRunner);
    }
}
