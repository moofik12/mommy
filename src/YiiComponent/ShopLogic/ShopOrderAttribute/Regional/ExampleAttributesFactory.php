<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Regional;

use MommyCom\Service\RegionalValidator\Filter\CharFilter;
use MommyCom\Service\RegionalValidator\Filter\SpecialCharFilter;
use MommyCom\Service\RegionalValidator\FilterRunner;
use MommyCom\Service\RegionalValidator\ValidatorRunner;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PhoneNumber;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PromoCode;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ExampleAttributesFactory extends AbstractAttributesFactory
{
    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->container->get(TranslatorInterface::class);

        $validator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();

        return [
            $this->createPromoCodeAttribute($translator),
            $this->createPhoneNumberAttribute($translator, $validator),
        ];
    }

    private function createPromoCodeAttribute(TranslatorInterface $translator): PromoCode
    {
        /** @var MommyWebApplication $app */
        $app = \Yii::app();
        /** @var ShopWebUser $user */
        $user = $app->user;

        return new PromoCode($user, $translator);
    }

    private function createPhoneNumberAttribute(
        TranslatorInterface $translator,
        ValidatorInterface $validator
    ): PhoneNumber
    {
        $filterRunner = new FilterRunner([
            new CharFilter(['-', ' ']),
            new SpecialCharFilter(SpecialCharFilter::REMOVE_FIRST_CHARACTER),
        ]);

        $validatorRunner = new ValidatorRunner([
            new NotBlank(),
            new Length([
                'min' => 3,
                'max' => 12,
                'minMessage' => $translator('Phone number has not enough characters.'),
                'maxMessage' => $translator('Phone number has too much characters.'),
            ]),
            new Regex([
                'pattern' => '/^\d+$/',
                'message' => $translator('Phone number contains illegal characters.'),
            ]),
        ], $validator);

        return new PhoneNumber($translator, $filterRunner, $validatorRunner);
    }
}
