<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

class PromoCodeValidator extends \CValidator
{
    /**
     * @var ShopWebUser|null
     */
    public $user = null;

    /**
     * @var TranslatorInterface|null
     */
    public $translator = null;

    /**
     * @param \CModel $object
     * @param string $attribute
     */
    protected function validateAttribute($object, $attribute)
    {
        $value = $object->$attribute;

        if (!$value) {
            return;
        }

        if (!$this->user instanceof ShopWebUser) {
            throw new \RuntimeException(sprintf('User must be instance of "%s".', ShopWebUser::class));
        }

        if (!$this->translator instanceof TranslatorInterface) {
            throw new \RuntimeException(sprintf('Translator must be instance of "%s".', TranslatorInterface::class));
        }

        $cart = $this->user->getCart();
        $tr = $this->translator;

        if (!$cart->hasPossibleOwnOrder()) {
            $object->addError($attribute, $tr('In Fast delivery orders, the promotional code is not active'));

            return;
        }

        if (!PromocodeRecord::checkPromocode($value)) {
            $object->addError($attribute, $tr('Invalid promotional code'));

            return;
        }

        $promocode = PromocodeRecord::model()->promocode($value)->find();

        if ($promocode instanceof PromocodeRecord && !$promocode->getIsUsable($this->user->id)) {
            $object->addError($attribute, $tr('You can not use this promotional code'));

            return;
        }
    }
}
