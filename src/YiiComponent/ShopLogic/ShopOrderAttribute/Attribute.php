<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

interface Attribute
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @return array of rules
     */
    public function getRules(): array;

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param mixed $value
     */
    public function setValue($value);
}
