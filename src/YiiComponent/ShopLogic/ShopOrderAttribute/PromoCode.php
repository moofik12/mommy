<?php

namespace MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute;

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

class PromoCode implements Attribute
{
    public const NAME = 'promocode';

    /**
     * @var ShopWebUser
     */
    private $user;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var mixed
     */
    private $value = null;

    /**
     * PromoCode constructor.
     *
     * @param ShopWebUser $user
     * @param TranslatorInterface $translator
     */
    public function __construct(ShopWebUser $user, TranslatorInterface $translator)
    {
        $this->user = $user;
        $this->translator = $translator;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->translator->t('Promotional code');
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return [
            [self::NAME, 'filter', 'filter' => 'trim'],
            [self::NAME, 'exist', 'className' => PromocodeRecord::class, 'attributeName' => self::NAME,
                'message' => $this->translator->t('Invalid promotional code')],
            [self::NAME, PromoCodeValidator::class, 'user' => $this->user, 'translator' => $this->translator],
        ];
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
