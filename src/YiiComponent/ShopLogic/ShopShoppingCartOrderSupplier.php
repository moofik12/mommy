<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Model\Db\OrderDiscountCampaignRecord;

class ShopShoppingCartOrderSupplier extends ShopShoppingCartOrderAbstract
{
    /**
     * Мин сумма для оформления заказа
     *
     * @return float
     */
    function getMinOrderAmount()
    {
        return 0;
    }

    /**
     * @param bool|true $onlyReserved
     *
     * @return null|OrderDiscountCampaignRecord
     */
    function getOrderDiscountCampaign($onlyReserved = true)
    {
        return null;
    }

    /**
     * @return float|int
     */
    function getUserDiscountAsBonus()
    {
        return 0;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return int
     */
    function getBonusesCost($onlyReserved = true)
    {
        return 0;
    }

    /**
     * @param string $promocode
     * @param bool $onlyReserved
     *
     * @return float
     */
    function getPromocodeSavings($promocode, $onlyReserved = true)
    {
        return 0;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return float
     */
    function getUserDiscountSavings($onlyReserved = true)
    {
        return 0;
    }

    /**
     * @param bool|true $onlyReserved
     * @param bool $possible возможно будет успользовать скидку
     *
     * @return bool
     */
    function isEnableOrderDiscountCampaign($onlyReserved = true, $possible = false)
    {
        return false;
    }

}
