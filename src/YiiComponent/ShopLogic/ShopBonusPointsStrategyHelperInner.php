<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Model\Db\UserBonuspointsRecord;

/**
 * Class ShopBonusPointsStrategyHelperInner
 */
class ShopBonusPointsStrategyHelperInner extends ShopBonusPointsStrategyHelper
{
    public function getToSubtractTypes($points, $subtractLocked = true)
    {
        $results = new ShopBonusPointsTypes();

        $presentPoints = $this->_shopBonusPoints->getAvailableToSpendOnlyInner($subtractLocked);
        $balance = $points - $presentPoints;
        if ($balance > 0) {
            $results->addItem(new ShopBonusPointsType(UserBonuspointsRecord::TYPE_INNER, $presentPoints));
            $results->addItem(new ShopBonusPointsType(UserBonuspointsRecord::TYPE_PRESENT, $balance));
        } else {
            $results->addItem(new ShopBonusPointsType(UserBonuspointsRecord::TYPE_INNER, $points));
        }

        return $results;
    }
}
