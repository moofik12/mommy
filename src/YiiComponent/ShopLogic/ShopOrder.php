<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Attribute;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\DeliveryTypeValidator;

class ShopOrder extends \CFormModel
{
    /**
     * @var string
     */
    public $comment;

    /**
     * @var int
     */
    public $deliveryType;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var AvailableDeliveries
     */
    private $availableDeliveries;

    /**
     * @var Attribute[]
     */
    private $attributes = [];

    /**
     * ShopOrder constructor.
     *
     * @param TranslatorInterface $translator
     * @param AvailableDeliveries $availableDeliveries
     * @param array $attributes
     * @param string $scenario
     */
    public function __construct(
        TranslatorInterface $translator,
        AvailableDeliveries $availableDeliveries,
        array $attributes,
        string $scenario = ''
    )
    {
        $this->availableDeliveries = $availableDeliveries;
        $this->translator = $translator;

        foreach ($attributes as $attribute) {
            $this->addAttribute($attribute);
        }

        parent::__construct($scenario);
    }

    /**
     * @param Attribute $attribute
     */
    private function addAttribute(Attribute $attribute): void
    {
        $this->attributes[$attribute->getName()] = $attribute;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    private function hasAttribute(string $name): bool
    {
        return isset($this->attributes[$name]);
    }

    /**
     * @param string $name
     *
     * @return Attribute
     */
    private function getAttribute(string $name): Attribute
    {
        if (!isset($this->attributes[$name])) {
            throw new \RuntimeException(sprintf('Attribute %s not exist.', $name));
        }

        return $this->attributes[$name];
    }

    /**
     * @inheritdoc
     */
    public function attributeNames()
    {
        $attributeNames = parent::attributeNames();

        foreach ($this->attributes as $attribute) {
            $attributeNames[] = $attribute->getName();
        }

        return $attributeNames;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [
            'comment' => $this->translator->t('Comment to the order'),
        ];

        foreach ($this->attributes as $attribute) {
            $attributeLabels[$attribute->getName()] = $attribute->getLabel();
        }

        return $attributeLabels;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $tr = $this->translator;

        $rules = [
            ['deliveryType, countryCode', 'safe'],

            ['comment', 'length', 'max' => 255, 'encoding' => false, 'tooLong' => $tr('{attribute} is too long')],

            ['deliveryType', DeliveryTypeValidator::class, 'availableDeliveries' => $this->availableDeliveries],
        ];

        foreach ($this->attributes as $attribute) {
            $rules = array_merge($rules, $attribute->getRules());
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if ($this->hasAttribute($name)) {
            return $this->getAttribute($name)->getValue();
        }

        return parent::__get($name);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if ($this->hasAttribute($name)) {
            return $this->getAttribute($name)->setValue($value);
        }

        return parent::__set($name, $value);
    }

    /**
     * @inheritdoc
     */
    public function __isset($name)
    {
        if ($this->hasAttribute($name)) {
            return true;
        }

        return parent::__isset($name);
    }

    /**
     * @inheritdoc
     */
    public function __unset($name)
    {
        if ($this->hasAttribute($name)) {
            $this->getAttribute($name)->setValue(null);

            return null;
        }

        return parent::__unset($name);
    }

    /**
     * @param ShopWebUser $user
     * @param DeliveryModel $deliveryModel
     *
     * @return OrderRecord|null
     */
    public function makeOrder(ShopWebUser $user, DeliveryModel $deliveryModel): ?OrderRecord
    {
        $uuid = OrderRecord::generateUid($user->id, CurrentTime::getUnixTimestamp());

        /** @var OrderRecord[] $savedOrders */
        $savedOrders = [];

        foreach ($user->cart->getPositionsAsOrders() as $cartOrder) {
            if (!$cartOrder->isAvailableToBuy()) {
                continue;
            }

            $newOrder = OrderRecordFactory::makeOrder($user, $this, $cartOrder, $uuid);

            $newOrder->setDeliveryModel($deliveryModel);

            if (!$newOrder->save()) {
                $this->addErrors($newOrder->getErrors());
                break;
            } else {
                $savedOrders[] = $newOrder;
            }

            if (!count($newOrder->positions)) {
                $this->addError('', $this->translator->t('Cannot create order'));
                break;
            }
        }

        if ($this->hasErrors()) {
            foreach ($savedOrders as $order) {
                try {
                    $order->delete();
                } catch (\CDbException $e) {
                    // nothing to do
                }
            }

            return null;
        }

        if (!$savedOrders) {
            $this->addError('', $this->translator->t('Has no available positions'));

            return null;
        }

        return reset($savedOrders);
    }
}
