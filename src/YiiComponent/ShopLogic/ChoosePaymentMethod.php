<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\YiiComponent\Facade\Translator;

class ChoosePaymentMethod extends \CFormModel
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var bool
     */
    public $cashOnDelivery;

    public function rules()
    {
        return [
            ['id, cashOnDelivery', 'safe'],
            ['id', 'validateAttributes'],
        ];
    }

    public function validateAttributes()
    {
        $this->id = trim($this->id);
        $this->cashOnDelivery = trim($this->cashOnDelivery);

        if ($this->cashOnDelivery) {
            return;
        }

        if (!strlen($this->id)) {
            $this->addError('id', Translator::t('You should choose payment type'));
        }
    }
}
