<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Regional\AbstractAttributesFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShopOrderFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $scenario
     *
     * @return ShopOrder
     */
    public static function createShopOrder(ContainerInterface $container, string $scenario = ''): ShopOrder
    {
        /** @var AvailableDeliveries $deliveries */
        $deliveries = $container->get(AvailableDeliveries::class);

        /** @var TranslatorInterface $translator */
        $translator = $container->get(TranslatorInterface::class);

        $attributes = AbstractAttributesFactory::getFactory($container)->getAttributes();

        return new ShopOrder($translator, $deliveries, $attributes, $scenario);
    }
}
