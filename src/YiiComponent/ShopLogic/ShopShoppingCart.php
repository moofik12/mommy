<?php

namespace MommyCom\YiiComponent\ShopLogic;

use CEvent;
use CMap;
use DateTime;
use InvalidArgumentException;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\OrderDiscountCampaignRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\Behavior\TestCartPresentThingBehavior;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

/**
 * @property-read UserRecord $user
 * @property-read boolean $isGuest
 * @property-read boolean $isFull
 * @property-read boolean $isEmpty
 * @property-read CartRecord[] $positions
 * @property-read CartRecord[] $expiredPositions
 */
class ShopShoppingCart extends CMap
{
    /**
     * @var UserRecord
     */
    protected $_user;

    protected $_isGuest;

    /** @var  OrderDiscountCampaignRecord */
    protected $_orderDiscountCampaign = false;

    public function getUser()
    {
        return $this->_user;
    }

    public function init(UserRecord $user = null)
    {
        $this->_user = $user;
        $this->_isGuest = $user === null || $user->isNewRecord;
        $this->restoreFromDb();
        $this->initBehaviors();
    }

    /**
     * attach Behaviors
     */
    protected function initBehaviors()
    {
        $this->attachBehaviors([
            'onUpdatePosition' => TestCartPresentThingBehavior::class,
            'onRemovePosition' => TestCartPresentThingBehavior::class,
        ]);
    }

    public function getIsGuest()
    {
        return $this->_isGuest;
    }

    public function updateReservations()
    {
        foreach ($this->getPositions() as $position) {
            $position->updateReservation();
        }
    }

    /**
     * Restores the shopping cart from the db
     */
    public function restoreFromDb()
    {
        $cart = CartRecord::model();
        if ($this->isGuest) {
            $shopWebUser = Yii::app()->user;
            /** @var $shopWebUser ShopWebUser */
            $cart->anonymousId($shopWebUser->getAnonymousId());
        } else {
            $cart->userId($this->user->id);
        }

        $items = $cart
            ->with('product', 'event')
            ->priority()
            ->findAll();
        /* @var $items CartRecord[] */

        foreach ($items as $item) {
            parent::add($item->product_id, $item);
        }
    }

    /**
     * Add item to the shopping cart
     * If the position was previously added to the cart,
     * then information about it is updated, and count increases by $quantity
     *
     * @param EventProductRecord $item
     * @param int $quantity of elements positions
     *
     * @return bool
     */
    public function put(EventProductRecord $item, $quantity = 1)
    {
        $key = $item->id;
        $position = $this->itemAt($key);

        // code here
        if ($position instanceof CartRecord) {
            $quantity += $position->number;
        }

        return $this->update($item, $quantity);
    }

    /**
     * Add $value items to position with $key specified
     *
     * @return void
     *
     * @param mixed $key - not used
     * @param EventProductRecord $value
     *
     * @throws InvalidArgumentException
     */
    public function add($key, $value)
    {
        if (!$value instanceof EventProductRecord) {
            throw new InvalidArgumentException();
        }
        $this->put($value, 1);
    }

    /**
     * @param EventProductRecord $item
     *
     * @return CartRecord|null
     */
    public function getFromEventProduct(EventProductRecord $item)
    {
        return $this->itemAt($item->id);
    }

    /**
     * @param int $supplierId
     *
     * @return null|ShopShoppingCartOrderAbstract
     */
    public function getPossibleOrderFromSupplierId($supplierId)
    {
        $cartOrders = $this->getPositionsAsOrders();

        foreach ($cartOrders as $cartOrder) {
            if ($cartOrder->supplierId == $supplierId) {
                return $cartOrder;
            }
        }

        return null;
    }

    /**
     * @return null|ShopShoppingCartOrderAbstract
     */
    public function getPossibleOwnOrder()
    {
        $cartOrders = $this->getPositionsAsOrders();

        foreach ($cartOrders as $cartOrder) {
            if ($cartOrder->isOwn) {
                return $cartOrder;
            }
        }

        return null;
    }

    /**
     * Removes position from the shopping cart of key
     *
     * @param mixed $key
     *
     * @return mixed
     */
    public function remove($key)
    {
        $position = $this->itemAt($key);
        if ($position instanceof CartRecord) {
            $position->delete();
        }

        $result = parent::remove($key);

        $this->onRemovePosition(new CEvent($this));
        return $result;
    }

    /**
     * Updates the position in the shopping cart
     * If position was previously added, then it will be updated in shopping cart,
     * if position was not previously in the cart, it will be added there.
     * Если количество меньше 1, тогда позиция будет удалена.
     *
     * @param EventProductRecord $item
     * @param int $quantity
     *
     * @return boolean
     */
    public function update(EventProductRecord $item, $quantity)
    {
        $shopWebUser = Yii::app()->user;
        /** @var $shopWebUser ShopWebUser */

        $key = $item->id;
        $position = $this->itemAt($key);

        if ($position instanceof CartRecord && $position->getIsExpired()) {
            $quantity -= $position->number;
            $this->remove($key);
            $position = null;
        }

        if ($quantity < 1) {
            $this->remove($key);
        } else {
            $additionalQuantity = min($quantity, $item->max_per_buy);
            if ($position instanceof CartRecord) {
                $additionalQuantity -= $position->number;
            }

            if ($additionalQuantity > 0 && !$item->getIsPossibleToBuy($additionalQuantity)) {
                return false;
            }

            if (!$position instanceof CartRecord) {
                $position = new CartRecord();
                $position->user_id = ($this->_isGuest) ? 0 : $this->user->id;
                $position->anonymous_id = ($this->_isGuest) ? $shopWebUser->getAnonymousId() : '';
                $position->event_id = $item->event_id;
                $position->product_id = $item->id;
            }

            $position->number = $quantity;

            parent::add($key, $position);
        }

        //$this->applyDiscounts();
        $this->onUpdatePosition(new CEvent($this));
        $this->saveState();

        return true;
    }

    /**
     * Saves the state of the object in the session.
     *
     * @return void
     */
    protected function saveState()
    {
        foreach ($this->getPositions() as $position) {
            //send notification again if left position
            if ($position->is_sent_notification) {
                $position->is_sent_notification = 0;
            }

            $position->save();
        }
    }

    /**
     * Returns count of items in shopping cart
     *
     * @return int
     */
    public function getItemsCount()
    {
        $count = 0;
        foreach ($this->getPositions() as $position) {
            $count += $position->number;
        }
        return $count;
    }

    /**
     * Returns count of items in shopping cart
     *
     * @return int
     */
    public function getReservedItemsCount()
    {
        $count = 0;
        foreach ($this->getReservedPositions() as $position) {
            $count += $position->number;
        }
        return $count;
    }

    /**
     * Returns count of items in shopping cart
     *
     * @return int
     */
    public function getOrdersReservedItemsCount()
    {
        $count = 0;

        $cartOrders = $this->getPositionsAsOrders();

        foreach ($cartOrders as $cartOrder) {
            if (!$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $count += $cartOrder->getReservedItemsCount();
        }
        return $count;
    }

    /**
     * Возможность оформить заказы
     *
     * @return bool
     */
    public function isAvailableToBuy()
    {
        return $this->getOrdersReservedItemsCount() > 0 && $this->isEnoughToBuy();
    }

    /**
     * @return bool
     */
    public function canPrepay(): bool
    {
        foreach ($this->getPositions() as $position) {
            if (!$position->event->can_prepay) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param bool $allPositions
     *
     * @return bool
     */
    public function isEnoughToBuy($allPositions = false)
    {
        $cartOrders = $this->getPositionsAsOrders();
        $enough = $allPositions ? true : false;

        foreach ($cartOrders as $cartOrder) {
            $enoughToBuyOrder = $cartOrder->isEnoughToBuy();

            if ($enoughToBuyOrder && !$allPositions) {
                return true;
            }

            $enough = $enough && $enoughToBuyOrder;
        }

        return $enough;
    }

    /**
     * @param bool $enoughToBuy
     * @param bool $onlyReserved
     *
     * @return bool
     */
    public function hasPossibleOwnOrder($enoughToBuy = true, $onlyReserved = true)
    {
        foreach ($this->getPositionsAsOrders() as $cartOrder) {
            if ($cartOrder->isOwn) {
                if (!$enoughToBuy || $cartOrder->isEnoughToBuy($onlyReserved)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return bool
     */
    public function hasPossibleOwnOrderNotEnoughToBuy($onlyReserved = true)
    {
        foreach ($this->getPositionsAsOrders() as $cartOrder) {
            if ($cartOrder->isOwn) {
                if (!$cartOrder->isEnoughToBuy($onlyReserved)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Возвращает суммарную стоимость всех тововаров в корзине
     *
     * @param bool $withBonuses
     * @param string $promocode промокод
     * @param bool $onlyReserved
     * @param bool $onlyEnoughToBuy
     *
     * @return float
     */
    public function getCost($withBonuses = true, $promocode = '', $onlyReserved = true, $onlyEnoughToBuy = true)
    {
        $cost = 0.0;

        $cartOrders = $this->getPositionsAsOrders();

        foreach ($cartOrders as $cartOrder) {
            if ($onlyEnoughToBuy && !$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $cost += $cartOrder->getCost($withBonuses, $promocode, $onlyReserved);
        }

        return $cost;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return int
     */
    public function getWeight($onlyReserved = true)
    {
        $weight = 0;

        $list = $onlyReserved ? $this->getReservedPositions() : $this->getPositions();
        foreach ($list as $position) {
            $productWeight = Cast::toUInt($position->product->weight);
            $weight += $productWeight * $position->number;
        }

        return $weight;
    }

    /**
     * Экономия
     *
     * @param bool $onlyReserved
     *
     * @return float
     */
    public function getSavings($onlyReserved = true)
    {
        $price = 0.0;

        $list = $onlyReserved ? $this->getReservedPositions() : $this->getPositions();
        foreach ($list as $position) {
            $price += $position->getTotalSavings();
        }

        return $price;
    }

    /**
     * Экономия
     *
     * @param bool $onlyReserved
     * @param bool $onlyEnoughToBuy
     *
     * @return float
     */
    public function getOrdersSavings($onlyReserved = true, $onlyEnoughToBuy = true)
    {
        $price = 0.0;
        $cartOrders = $this->getPositionsAsOrders();

        foreach ($cartOrders as $cartOrder) {
            if ($onlyEnoughToBuy && !$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $list = $onlyReserved ? $cartOrder->getReservedPositions() : $cartOrder->getPositions();
            foreach ($list as $position) {
                $price += $position->getTotalSavings();
            }
        }

        return $price;
    }

    /**
     * @param bool $onlyReserved
     * @param bool $onlyEnoughToBuy
     *
     * @return int
     */
    public function getBonusesCost($onlyReserved = true, $onlyEnoughToBuy = true)
    {
        /*if ($this->getIsGuest()) {
            return 0;
        }*/

        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if ($onlyEnoughToBuy && !$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getBonusesCost($onlyReserved);
        }

        return $amount;
    }

    /**
     * @param string $promocode
     * @param bool $onlyReserved
     * @param bool $onlyEnoughToBuy
     *
     * @return float
     */
    public function getPromocodeSavings($promocode, $onlyReserved = true, $onlyEnoughToBuy = true)
    {
        /*if ($this->getIsGuest()) {
            return 0.0;
        }*/

        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if ($onlyEnoughToBuy && !$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getPromocodeSavings($promocode, $onlyReserved);
        }

        return $amount;
    }

    /**
     * @param bool $onlyReserved
     * @param bool $onlyEnoughToBuy
     *
     * @return float
     */
    public function getUserDiscountSavings($onlyReserved = true, $onlyEnoughToBuy = true)
    {
        /*if ($this->getIsGuest()) {
            return 0.0;
        }*/

        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if ($onlyEnoughToBuy && !$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getUserDiscountSavings($onlyReserved);
        }

        return $amount;
    }

    /**
     * @return int|float
     */
    public function getUserTotalSaving()
    {
        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if (!$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getUserTotalSaving();
        }

        return $amount;
    }

    /**
     * @param bool|true $onlyReserved
     *
     * @return null|OrderDiscountCampaignRecord
     */
    public function getOrderDiscountCampaign($onlyReserved = true)
    {
        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if (!$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getOrderDiscountCampaign($onlyReserved);
        }

        return $amount;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return float
     */
    public function getOrderDiscountCampaignSavings($onlyReserved = true)
    {
        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if (!$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getOrderDiscountCampaignSavings($onlyReserved);
        }

        return $amount;
    }

    /**
     * @param bool|true $onlyReserved
     * @param bool $possible возможно будет успользовать скидку
     *
     * @return bool
     */
    public function isEnableOrderDiscountCampaign($onlyReserved = true, $possible = false)
    {
        $isEnable = false;

        if ($this->getIsGuest() || !$this->getOrderDiscountCampaign()) {
            return false;
        }

        $cartOrders = $this->getPositionsAsOrders();

        foreach ($cartOrders as $cartOrder) {
            if (!$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $isEnable = $isEnable || $cartOrder->isEnableOrderDiscountCampaign($onlyReserved, $possible);
        }

        return $isEnable;
    }

    /**
     * Копипаст с OrderRecord::getUserDiscountAsBonus, увы
     *
     * @return float|int
     */
    public function getUserDiscountAsBonus()
    {
        $cartOrders = $this->getPositionsAsOrders();
        $amount = 0;

        foreach ($cartOrders as $cartOrder) {
            if (!$cartOrder->isEnoughToBuy()) {
                continue;
            }

            $amount += $cartOrder->getUserDiscountAsBonus();
        }

        return $amount;
    }

    /**
     * onRemovePosition event
     *
     * @param  $event
     *
     * @return void
     */
    public function onRemovePosition($event)
    {
        $this->raiseEvent('onRemovePosition', $event);
    }

    /**
     * onUpdatePosition event
     *
     * @param  $event
     *
     * @return void
     */
    public function onUpdatePosition($event)
    {
        $this->raiseEvent('onUpdatePosition', $event);
    }

    /**
     * Returns array all positions
     *
     * @return CartRecord[]
     */
    public function getPositions()
    {
        $positions = $this->toArray();
        /* @var $positions CartRecord[] */

        // удаляем все позиции которые разрезервированы более 3-х дней назад
        $time = new DateTime();
        $time->modify('today -3 days');
        $megaExpiredTime = $time->getTimestamp();
        foreach ($positions as $index => $position) {
            $needDelete = !$position instanceof CartRecord;
            $needDelete = $needDelete || $position->event === null || $position->product === null;
            $needDelete = $needDelete || !$position->event->isTimeActive;
            $needDelete = $needDelete || (!$position->getIsNewRecord() && $position->reservedTo < $megaExpiredTime);

            if ($needDelete) {
                $this->remove($index);
                unset($positions[$index]);
            }
        }

        return $positions;
    }

    /**
     * @return CartRecord[]
     */
    public function getReservedPositions()
    {
        $positions = [];

        foreach ($this->getPositions() as $position) {
            if ($position->getIsReserved()) {
                $positions[] = $position;
            }
        }

        return $positions;
    }

    /**
     * @return CartRecord[]
     */
    public function getExpiredPositions()
    {
        $positions = [];

        foreach ($this->getPositions() as $position) {
            if ($position->getIsExpired()) {
                $positions[] = $position;
            }
        }

        return $positions;
    }

    /**
     * @return int
     */
    public function getReservedCount()
    {
        return count($this->getReservedPositions());
    }

    /**
     * Returns if cart is empty
     *
     * @return bool
     */
    public function getIsEmpty()
    {
        return !(bool)$this->getReservedCount();
    }

    /**
     * @return bool
     */
    public function getIsFull()
    {
        if (empty($this->positions)) {
            return false;
        }

        $cartOrders = $this->getPositionsAsOrders();
        $isFull = true;
        foreach ($cartOrders as $cartOrder) {
            $isFull = $isFull && $cartOrder->getIsFull();
        }

        return $isFull;
    }

    /**
     * Количество времени до истечения хотя бы одного товара в корзине
     *
     * @return integer
     */
    public function getTotalReservedTo()
    {
        $columns = ArrayUtils::getColumn($this->getReservedPositions(), 'reservedTo');
        return count($columns) > 0 ? min($columns) : 0;
    }

    /**
     * Выдает бонусное время
     */
    public function giveBonusTime()
    {
        foreach ($this->getReservedPositions() as $position) {
            $position->giveBonusTime();
        }
        $this->saveState();
    }

    /**
     * @return ShopShoppingCartOrderAbstract[]
     */
    public function getPositionsAsOrders()
    {
        $itemsGroupByDropShipping = ArrayUtils::groupBy($this->getPositions(), 'event.is_drop_shipping');
        $orders = [];

        //Собстенные заказы
        if (isset($itemsGroupByDropShipping[0])) {
            $ownOrder = new ShopShoppingCartOrderOwn(true, $this);
            $ownOrder->setDisplayName(mb_strtoupper(Yii::app()->name));

            foreach ($itemsGroupByDropShipping[0] as $item) {
                /* @var CartRecord $item */
                $ownOrder->add($item->product_id, $item);
            }

            $orders[] = $ownOrder;
        }

        //Дропшиппинговые заказы
        if (isset($itemsGroupByDropShipping[1])) {
            $itemsGroupBySupplier = ArrayUtils::groupBy($itemsGroupByDropShipping[1], 'event.supplier_id');

            foreach ($itemsGroupBySupplier as $supplierId => $items) {
                $supplierOrder = new ShopShoppingCartOrderSupplier(false, $this, $supplierId);
                foreach ($items as $item) {
                    /* @var CartRecord $item */
                    if (empty($supplierOrder->getDisplayName() && $item->event && $item->event->supplier)) {
                        $supplierOrder->setDisplayName($item->event->supplier->getDisplayName());
                    }

                    $supplierOrder->add($item->product_id, $item);
                }

                $orders[] = $supplierOrder;
            }
        }

        return $orders;
    }

    /**
     * @param bool $isOwn
     *
     * @return int
     */
    public static function getMaxCartSize($isOwn)
    {
        return $isOwn ? ShopShoppingCartOrderOwn::getMaxCartSize() : ShopShoppingCartOrderSupplier::getMaxCartSize();
    }
}
