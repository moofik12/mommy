<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Model\Db\OrderDiscountCampaignRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\YiiComponent\Type\Cast;

class ShopShoppingCartOrderOwn extends ShopShoppingCartOrderAbstract
{

    /** сумма минимального заказа */
    const MIN_ORDER_AMOUNT = 9;

    /** @var  OrderDiscountCampaignRecord */
    protected $_orderDiscountCampaign = false;

    /**
     * Мин сумма для оформления заказа
     *
     * @return float
     */
    function getMinOrderAmount()
    {
        return self::MIN_ORDER_AMOUNT;
    }

    /**
     * @param bool|true $onlyReserved
     *
     * @return null|OrderDiscountCampaignRecord
     */
    function getOrderDiscountCampaign($onlyReserved = true)
    {
        if ($this->_orderDiscountCampaign === false) {
            $this->_orderDiscountCampaign = OrderDiscountCampaignRecord::findBestCampaign($this->_getCost($onlyReserved));
        }

        return $this->_orderDiscountCampaign;
    }

    /**
     * @return float|int
     */
    function getUserDiscountAsBonus()
    {
        $discount = 0;
        $hasDiscount = $this->getUserTotalSaving();

        if (!OrderRecord::isEnableUserDiscountAsBonus(time()) || $hasDiscount > 0) {
            return $discount;
        }

        $percentDiscount = Cast::toUInt($this->getCart()->user->discount_percent);
        $discount = $this->getCost(true) * $percentDiscount / 100;

        return ceil($discount);
    }

    /**
     * @param bool $onlyReserved
     *
     * @return int
     */
    function getBonusesCost($onlyReserved = true)
    {
        if ($this->getCart()->getIsGuest()) {
            return 0;
        }

        $bonuses = new ShopBonusPoints();
        $bonuses->init($this->getCart()->user);

        return $bonuses->getBonusesCost($this->_getCost($onlyReserved));
    }

    /**
     * @param string $promocode
     * @param bool $onlyReserved
     *
     * @return float
     */
    function getPromocodeSavings($promocode, $onlyReserved = true)
    {
        if ($this->getCart()->getIsGuest()) {
            return 0.0;
        }

        $positions = $onlyReserved ? $this->getReservedPositions() : $this->getPositions();

        $promo = PromocodeRecord::model()->promocode($promocode)->cache(5)->find();
        /* @var $promo PromocodeRecord */

        return $promo !== null && !$promo->getIsUsed($this->getCart()->user->id) ? $promo->getDiscount($positions) : 0.0;
    }

    /**
     * @param bool $onlyReserved
     *
     * @return float
     */
    function getUserDiscountSavings($onlyReserved = true)
    {
        if ($this->getCart()->getIsGuest()) {
            return 0.0;
        }

        if (OrderRecord::isEnableUserDiscountAsBonus(time())) {
            return 0.0;
        }

        $cost = $this->getCart()->user->getDiscountCost($this->_getCost($onlyReserved));
        return $cost;
    }

    /**
     * @param bool|true $onlyReserved
     * @param bool $possible возможно будет успользовать скидку
     *
     * @return bool
     */
    function isEnableOrderDiscountCampaign($onlyReserved = true, $possible = false)
    {
        $isEnable = false;

        if ($this->getCart()->getIsGuest() || !$this->getOrderDiscountCampaign()) {
            return false;
        }

        $coast = $this->_getCost($onlyReserved);

        if ($possible && $coast < $this->getOrderDiscountCampaign()->min_amount) {
            $coast = $this->getOrderDiscountCampaign()->min_amount;
        }

        $orderDiscountCampaignSavings = $this->getOrderDiscountCampaign()->getDiscount($coast);
        $userUserDiscountSavings = $this->getCart()->user->getDiscountCost($coast);

        if ($orderDiscountCampaignSavings == 0) {
            return $isEnable;
        }

        if ($this->getPromocodeSavings($onlyReserved) < $orderDiscountCampaignSavings
            && $userUserDiscountSavings < $orderDiscountCampaignSavings
        ) {
            $isEnable = true;
        }

        return $isEnable;
    }

}
