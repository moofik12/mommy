<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Model\Db\UserBonuspointsRecord;

/**
 * Class ShopBonusPointsStrategyHelperPresent
 */
class ShopBonusPointsStrategyHelperPresent extends ShopBonusPointsStrategyHelper
{
    public function getToSubtractTypes($points, $subtractLocked = true)
    {
        $results = new ShopBonusPointsTypes();

        $presentPoints = $this->_shopBonusPoints->getAvailableToSpendOnlyPresent($subtractLocked);
        $balance = $points - $presentPoints;
        if ($balance > 0) {
            $results->addItem(new ShopBonusPointsType(UserBonuspointsRecord::TYPE_PRESENT, $presentPoints));
            $results->addItem(new ShopBonusPointsType(UserBonuspointsRecord::TYPE_INNER, $balance));
        } else {
            $results->addItem(new ShopBonusPointsType(UserBonuspointsRecord::TYPE_PRESENT, $points));
        }

        return $results;
    }
}
