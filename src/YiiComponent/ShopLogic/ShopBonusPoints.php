<?php

namespace MommyCom\YiiComponent\ShopLogic;

use CException;
use CLogger;
use CMap;
use InvalidArgumentException;
use LogicException;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserRecord;
use Yii;

/**
 * Class ShopBonusPoints
 *
 * @property-read boolean $isGuest
 * @property-read UserRecord $user
 * @property-read UserBonuspointsRecord $bonusPoints
 * @property-read integer $availableToSpend
 * @property-read ShopBonusPointsLock $locks
 */
class ShopBonusPoints extends CMap
{
    //стратегии списывания бонусов
    const STRATEGY_SUBTRACT_FIRST_PRESENT = 'present'; //сначала списываются подарочные бонусы
    const STRATEGY_SUBTRACT_FIRST_INNER = 'inner'; //сначала списываются внутренние бонусы
    const STRATEGY_SUBTRACT_ORDER = 'order'; //бонусы списываются по очереди

    protected $_isGuest;

    /**
     * @var UserRecord
     */
    protected $_user;

    protected $_locks;

    /** @var  string */
    protected $_strategy = self::STRATEGY_SUBTRACT_FIRST_PRESENT;

    /**
     * @return string
     */
    public function getStrategy()
    {
        return $this->_strategy;
    }

    /**
     * @param string $strategy
     */
    public function setStrategy($strategy)
    {
        if (in_array($strategy, self::availableStrategy())) {
            throw new InvalidArgumentException("strategy '$strategy' not available in " . __CLASS__);
        }

        $this->_strategy = $strategy;
    }

    /**
     * @return array
     */
    public static function availableStrategy()
    {
        static $strategy = [
            self::STRATEGY_SUBTRACT_FIRST_PRESENT,
            self::STRATEGY_SUBTRACT_FIRST_INNER,
            self::STRATEGY_SUBTRACT_ORDER,
        ];

        return $strategy;
    }

    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @param UserRecord|null $user
     *
     * @return static
     */
    public function init(UserRecord $user = null)
    {
        $this->_user = $user;
        $this->_isGuest = $user === null || $user->isNewRecord;
        $this->restoreFromDb();

        $this->_locks = new ShopBonusPointsLock();
        $this->_locks->init($user);

        return $this;
    }

    public function getIsGuest()
    {
        return $this->_isGuest;
    }

    /**
     * @return ShopBonusPointsLock
     */
    public function getLocks()
    {
        return $this->_locks;
    }

    /**
     * Restores the shopping cart from the db
     */
    public function restoreFromDb()
    {
        if ($this->isGuest) {
            return;
        }

        $items = UserBonuspointsRecord::model()->userId($this->user->id)->findAll();
        /* @var $items UserBonuspointsRecord[] */
        foreach ($items as $item) {
            parent::add($item->id, $item);
        }
    }

    /**
     * @param integer $points положительное число которое будет добавлено к бонусам
     * @param string $message
     */
    public function sendInner($points, $message)
    {
        if ($this->isGuest) {
            return;
        }

        $points = (int)floor($points);

        if ($points == 0) {
            return;
        }

        $bonus = new UserBonuspointsRecord();
        $bonus->user_id = $this->getUser()->id;
        $bonus->points = $points;
        $bonus->message = $message;
        $bonus->type = UserBonuspointsRecord::TYPE_INNER;

        parent::add(null, $bonus);
        $this->saveState();
    }

    /**
     * @param integer $points положительное число которое будет вычтено из бонусов
     * @param string $message
     *
     * @return ShopBonusPointsTypes
     * @throws CException
     */
    public function subtract($points, $message)
    {
        if ($this->isGuest) {
            return;
        }

        $points = (int)ceil($points);

        if ($points == 0) {
            return;
        }

        $types = $this->_getPointsTypesHelper()->getToSubtractTypes($points, false);

        foreach ($types->toArray() as $item) {
            if ($item->value == 0) {
                continue;
            }

            $this->subtractType($item->type, $item->value, $message);
        }

        return $types;
    }

    public function subtractType($type, $points, $message)
    {
        if ($this->isGuest) {
            return;
        }

        $points = (int)ceil($points);

        if ($points == 0) {
            return;
        }

        $this->_validateType($type);

        $bonus = new UserBonuspointsRecord();
        $bonus->user_id = $this->getUser()->id;
        $bonus->points = -$points;
        $bonus->message = $message;
        $bonus->type = $type;

        parent::add(null, $bonus);

        $this->saveState();
    }

    /**
     * @param $operationID
     * @param $points
     *
     * @return ShopBonusPointsTypes
     */
    public function lock($operationID, $points)
    {
        $types = $this->_getPointsTypesHelper()->getToSubtractTypes($points);
        foreach ($types->toArray() as $item) {
            $this->getLocks()->lock($operationID, $item->value, $item->type);
        }

        return $types;
    }

    public function unlock($operationID)
    {
        $this->getLocks()->unlockAll($operationID);
    }

    /**
     * Saves the state of the object in the session.
     *
     * @return void
     */
    protected function saveState()
    {
        if ($this->isGuest) {
            return;
        }

        $result = true;
        $transaction = \Yii::app()->db->beginTransaction();
        foreach ($this->getBonusPoints() as $bonus) {
            if ($bonus->isNewRecord) {
                $result = $result && $bonus->save();
                if (!$saveItem = $bonus->save()) {
                    Yii::log(\Yii::t('common', 'Ошибка при сохранении бонуса в ') . __FUNCTION__ . '. Errors: ' . print_r($bonus->getErrors(), true), CLogger::LEVEL_ERROR);
                }
                $result = $result && $saveItem;
            }
        }

        if (!$result) {
            $transaction->rollback();
            throw new LogicException('Some bonus not save!');
        } else {
            $transaction->commit();
        }

        $this->optimizePoints();
    }

    /**
     * Особым смешивает баллы если полей слишком много
     */
    public function optimizePoints()
    {
        // write code here
    }

    /**
     * @param bool $subtractLocked
     *
     * @return integer
     */
    public function getAvailableToSpend($subtractLocked = true)
    {
        $result = 0;
        $result += $this->getAvailableToSpendOnlyInner($subtractLocked);
        $result += $this->getAvailableToSpendOnlyPresent($subtractLocked);

        return $result;
    }

    public function getAvailableToSpendOnlyInner($subtractLocked = true)
    {
        $result = array_reduce($this->getBonusPoints(UserBonuspointsRecord::TYPE_INNER), function ($carry, $item) {
            /* @var UserBonuspointsRecord $item */

            return $carry + $item->points;
        }, 0);

        if ($subtractLocked) {
            $result -= $this->locks->getLockedPoints(UserBonuspointsRecord::TYPE_INNER);
        }

        return $result;
    }

    public function getAvailableToSpendOnlyPresent($subtractLocked = true)
    {
        $result = array_reduce($this->getBonusPoints(UserBonuspointsRecord::TYPE_PRESENT), function ($carry, $item) {
            /* @var UserBonuspointsRecord $item */

            return $carry + $item->points;
        }, 0);

        if ($subtractLocked) {
            $result -= $this->locks->getLockedPoints(UserBonuspointsRecord::TYPE_PRESENT);
        }

        return $result;
    }

    /**
     * Возвращает количество бонусов которые можно потратить для указанной суммы
     *
     * @param float $cost
     * @param bool $subtractLocked
     *
     * @return integer
     */
    public function getBonusesCost($cost, $subtractLocked = true)
    {
        $bonuses = (int)min(
            $this->getAvailableToSpend($subtractLocked),
            ceil($cost * UserBonuspointsRecord::BONUSPOINTS_BUY_PERCENTAGE)
        );
        $bonuses = max(0, $bonuses); // не допускаем отрицательных величин
        $bonuses = min($bonuses, $cost); // не допускаем выхода за предел стоимости заказа
        $bonuses = min($bonuses, UserBonuspointsRecord::MAX_BONUSPOINTS_PER_TRANSFER);
        return $bonuses;
    }

    /**
     * @return UserBonuspointsRecord[]
     *
     * @param $type UserBonuspointsRecord types
     */
    public function getBonusPoints($type = null)
    {
        if ($type === null) {
            return $this->toArray();
        }

        $this->_validateType($type);

        $bonusPoints = array_filter($this->getBonusPoints(), function ($item) use ($type) {
            /* @var UserBonuspointsRecord $item */
            return $item->type == $type;
        });

        return $bonusPoints;
    }

    /**
     * @param $type
     */
    private function _validateType($type)
    {
        if (!in_array($type, [UserBonuspointsRecord::TYPE_INNER, UserBonuspointsRecord::TYPE_PRESENT])) {
            throw new InvalidArgumentException('Type not support in ' . __FUNCTION__ . ' must be compatible only UserBonuspointsRecord');
        }
    }

    private function _getPointsTypesHelper()
    {
        $points = ShopBonusPointsStrategyHelper::init($this, $this->getStrategy());

        return $points;
    }
}
