<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Entity\Order;
use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\CurrentTime;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

class OrderRecordFactory
{
    public static function makeOrder(
        ShopWebUser $user,
        ShopOrder $shopOrder,
        ShopShoppingCartOrderAbstract $cartOrder,
        string $uuid
    ): OrderRecord
    {
        $positions = $cartOrder->getReservedPositions();

        $time = CurrentTime::getUnixTimestamp();

        $order = new OrderRecord();
        $order->user_id = $user->getId();
        $order->delivery_type = $shopOrder->deliveryType;
        $order->country_code = $shopOrder->countryCode ?? '';
        $order->telephone = $shopOrder->telephone ?? '';
        $order->line_account = $shopOrder->lineAccount ?? '';
        $order->client_name = $shopOrder->clientName ?? '';
        $order->client_surname = $shopOrder->clientSurname ?? '';
        $order->order_comment = $shopOrder->comment ?? '';
        $order->ordered_at = $time;
        $order->uuid = $uuid;
        $order->payment_token = Order::generatePaymentToken();
        $order->payment_token_expires_at = Order::generatePaymentTokenExpiresAt();

        if ($cartOrder->isOwn) {
            if ($cartOrder->getOrderDiscountCampaign()) {
                $order->discount_campaign_id = $cartOrder->getOrderDiscountCampaign()->id;
            }
        } else {
            /* @var CartRecord $firstPosition */
            $firstPosition = reset($positions);

            $order->is_drop_shipping = 1;
            $order->supplier_id = $firstPosition->event ? $firstPosition->event->supplier_id : 0;
        }

        $order->offer_provider = $user->getLastOfferProvider();
        $order->offer_id = $user->getLastOfferId();

        $order->bonuses = $user->bonuspoints->getBonusesCost($order->getPrice());

        $order->setCartPositions($positions);

        return $order;
    }
}
