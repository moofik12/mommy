<?php

namespace MommyCom\YiiComponent\ShopLogic;

use MommyCom\Model\Db\UserBonuspointsRecord;

/**
 * Class ShopBonusPointsStrategyHelperOrder
 */
class ShopBonusPointsStrategyHelperOrder extends ShopBonusPointsStrategyHelper
{
    public function getToSubtractTypes($points, $subtractLocked = true)
    {
        $lastTimeSubtractInner = 0;
        $lastTimeSubtractPresent = 0;
        foreach ($this->_shopBonusPoints->getBonusPoints(UserBonuspointsRecord::TYPE_INNER) as $item) {
            if ($item->points < 0) {
                $lastTimeSubtractInner = max($lastTimeSubtractInner, $item->created_at);
            }
        }
        foreach ($this->_shopBonusPoints->getBonusPoints(UserBonuspointsRecord::TYPE_PRESENT) as $item) {
            if ($item->points < 0) {
                $lastTimeSubtractPresent = max($lastTimeSubtractPresent, $item->created_at);
            }
        }

        if ($lastTimeSubtractInner < $lastTimeSubtractPresent) {
            $instance = self::init($this->_shopBonusPoints, ShopBonusPoints::STRATEGY_SUBTRACT_FIRST_INNER);
            return $instance->getToSubtractTypes($points, $subtractLocked = true);
        } else {
            $instance = self::init($this->_shopBonusPoints, ShopBonusPoints::STRATEGY_SUBTRACT_FIRST_PRESENT);
            return $instance->getToSubtractTypes($points, $subtractLocked = true);
        }
    }
}

