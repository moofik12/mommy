<?php

namespace MommyCom\YiiComponent\ShopLogic;

use CMap;
use InvalidArgumentException;
use MommyCom\Model\Db\UserBonuspointsLockRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * Class ShopBonusPointsLock
 *
 * @property-read boolean $isGuest
 * @property-read UserRecord $user
 * @property-read integer $lockedPoints количество замороженных бонусов
 * @method UserBonuspointsLockRecord[] toArray
 */
class ShopBonusPointsLock extends CMap
{
    protected $_isGuest;

    /**
     * @var UserRecord
     */
    protected $_user;

    public function getUser()
    {
        return $this->_user;
    }

    public function init(UserRecord $user = null)
    {
        $this->_user = $user;
        $this->_isGuest = $user === null || $user->isNewRecord;
        $this->restoreFromDb();
    }

    public function getIsGuest()
    {
        return $this->_isGuest;
    }

    /**
     * @param UserBonuspointsLockRecord $item
     *
     * @return string
     */
    private function getModelKey(UserBonuspointsLockRecord $item)
    {
        return "{$item->operation_id}_{$item->type}";
    }

    /**
     * @param $operationId
     * @param $type
     *
     * @return string
     */
    private function getKey($operationId, $type)
    {
        return "{$operationId}_{$type}";
    }

    /**
     * Restores the shopping cart from the db
     */
    public function restoreFromDb()
    {
        if ($this->isGuest) {
            return;
        }

        $items = UserBonuspointsLockRecord::model()->userId($this->user->id)->findAll();
        /* @var $items UserBonuspointsLockRecord[] */

        foreach ($items as $item) {
            parent::add($this->getModelKey($item), $item);
        }
    }

    /**
     * @param integer $operationId
     * @param integer $points
     * @param int $type look UserBonuspointsRecord types
     * @param bool $override
     *
     * @return false|integer количество замороженых баллов
     */
    public function lock($operationId, $points, $type, $override = true)
    {
        if ($this->isGuest) {
            return false;
        }

        $points = (int)floor($points);

        if ($points == 0) {
            return 0;
        }

        $item = parent::itemAt($this->getKey($operationId, $type));
        /* @var $item UserBonuspointsLockRecord */

        if ($item !== null) {
            if ($item->points == $points) {
                return $item->points;
            } elseif (!$override) {
                return false;
            } else {
                $this->unlock($operationId, $type);
            }
        }

        $lock = new UserBonuspointsLockRecord();
        $lock->user_id = $this->user->id;
        $lock->points = $points;
        $lock->type = $type;
        $lock->operation_id = $operationId;

        $this->add($this->getKey($operationId, $type), $lock);
        $this->saveState();

        return $lock->points;
    }

    /**
     * Разблокирует баллы
     *
     * @param $operationId
     * @param int $type look UserBonuspointsRecord types
     *
     * @return bool
     */
    public function unlock($operationId, $type)
    {
        if ($this->isGuest) {
            return;
        }

        $this->remove($this->getKey($operationId, $type));

        return true;
    }

    public function unlockAll($operationId)
    {
        if ($this->isGuest) {
            return;
        }

        foreach ($this->toArray() as $item) {
            if ($item->operation_id == $operationId) {
                $this->unlock($operationId, $item->type);
            }
        }
    }

    /**
     * Возвращает количество баллов заблокированных по указанной операции
     *
     * @param integer $operationId
     *
     * @return integer
     */
    public function getLockAll($operationId)
    {
        if ($this->isGuest) {
            return 0;
        }

        $points = 0;
        foreach ($this->toArray() as $item) {
            if ($item->operation_id == $operationId) {
                $points += $item->points;
            }
        }

        return $points;
    }

    /**
     * Возвращает количество баллов заблокированных по указанной операции и определенному типу
     *
     * @param integer $operationId
     * @param int $type look UserBonuspointsRecord types
     *
     * @return integer
     */
    public function getLock($operationId, $type)
    {
        $item = $this->itemAt($this->getKey($operationId, $type));
        /* @var $item UserBonuspointsLockRecord */
        return $item !== null ? $item->points : 0;
    }

    public function add($key, $value)
    {
        if (!$value instanceof UserBonuspointsLockRecord) {
            throw new InvalidArgumentException;
        }

        parent::add($key, $value);
    }

    public function remove($key)
    {
        $item = parent::remove($key);
        /* @var $item UserBonuspointsLockRecord */
        if ($item !== null && !$item->isNewRecord) {
            $item->delete();
        }
    }

    public function saveState()
    {
        foreach ($this->toArray() as $item) {
            if ($item->isNewRecord) {
                $item->save();
            }
        }
    }

    /**
     * Суммарное колличество замороженых бонусов
     *
     * @param int|bool|false $type look UserBonuspointsRecord types
     *
     * @return int
     */
    public function getLockedPoints($type = false)
    {
        $result = 0;
        foreach ($this->toArray() as $item) {
            /* @var $item UserBonuspointsLockRecord */
            if ($type === false || $type == $item->type) {
                $result += $item->points;
            }
        }
        return $result;
    }
}
