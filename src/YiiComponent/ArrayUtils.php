<?php

namespace MommyCom\YiiComponent;

/**
 * Class ArrayUtils
 * Вспомогательные функции для работы с массивами и объектами на них "похожими"
 * Крайне низкая производительность. Не рекомендуется к использованию.
 */
class ArrayUtils
{
    protected static function _assetArray($value, $throw = true)
    {
        if (!is_array($value) && !(is_object($value) && $value instanceof \ArrayAccess && $value instanceof \IteratorAggregate)) {
            if ($throw) {
                throw new \InvalidArgumentException('Value must be of array type');
            }
            return false;
        }
        return true;
    }

    /**
     * Обьединяет два и более массива в один
     * Гибрид между array_merge_recursive и array_replace_recursive.
     * Работакт как array_merge_recursive, но не сливает строковые ключи в массив, а заменяет их.
     *
     * @param array|\ArrayAccess $a
     * @param array|\ArrayAccess $b
     *
     * @return array
     */
    public static function merge($a, $b)
    {
        self::_assetArray($a);
        self::_assetArray($b);

        $arguments = func_get_args();

        while (($count = count($arguments)) > 2) {
            $arguments[$count - 2] = self::merge($arguments[$count - 2], $arguments[$count - 1]);
            unset($arguments[$count - 1]);
        }

        list($a, $b) = $arguments;

        if (is_object($a)) {
            $a = clone $a;
        }

        foreach ($b as $k => $v) {
            if (is_integer($k)) {
                isset($a[$k]) ? $a[] = $v : $a[$k] = $v;
            } else if (self::_assetArray($v, false) && isset($a[$k]) && self::_assetArray($a[$k])) {
                $a[$k] = self::merge($a[$k], $v);
            } else {
                $a[$k] = $v;
            }
        }
        return $a;
    }

    /**
     * Разница между двумя и более масивами
     * Умеет работать с ArrayAccess объектами.
     *
     * @deprecated use regular array_diff
     *
     * @param array|\ArrayAccess $a
     * @param array|\ArrayAccess $b
     *
     * @return array
     */
    public static function diff($a, $b)
    {
        self::_assetArray($a);
        self::_assetArray($b);

        $arguments = func_get_args();

        if (($count = count($arguments)) > 2) {
            do {
                $arguments[$count - 2] = self::diff($arguments[$count - 2], $arguments[$count - 1]);
                unset($arguments[$count - 1]);
            } while (($count = count($arguments)) > 2);

            list($a, $b) = $arguments;
        }

        if (is_object($a)) {
            $a = clone $a;
        }

        foreach ($b as $k => $v) {
            if (isset($a[$k]) || array_key_exists($k, $a)) {
                if (self::_assetArray($v, false) && self::_assetArray($a[$k], false)) {
                    $diff = self::diff($a[$k], $v);
                    if (!empty($diff)) {
                        $a[$k] = $diff;
                    } else {
                        unset($a[$k]);
                    }
                } elseif (is_object($v) && is_object($a[$k]) && serialize($v) === serialize($a[$k])) {
                    unset($a[$k]);
                } elseif ($v == $a[$k]) {
                    unset($a[$k]);
                }
            } else {
                $a[$k] = $v;
            }
        }
        return $a;
    }

    /**
     * Возвращает только не нулевые значения из массива
     *
     * @deprecated use array_filter()
     *
     * @param array|\ArrayAccess $array
     * @param boolean $preserveKeys сохранить ключи
     *
     * @return array
     */
    public static function onlyNonZero($array, $preserveKeys = false)
    {
        self::_assetArray($array);

        $items = [];
        $keys = [];
        foreach ($array as $key => &$item) {
            if ($item !== 0) {
                $items[] = $item;
                $keys[] = $key;
            }
        }
        unset($item);

        return $preserveKeys && !empty($items) ? array_combine($keys, $items) : $items;
    }

    /**
     * Возвращает только не нулевые значения из массива
     *
     * @deprecated use array_filter()
     *
     * @param array|\ArrayAccess $array
     * @param boolean $preserveKeys сохранить ключи
     *
     * @return array
     */
    public static function onlyNonEmpty($array, $preserveKeys = false)
    {
        self::_assetArray($array);

        $items = [];
        $keys = [];
        foreach ($array as $key => &$item) {
            if (!empty($item)) {
                $items[] = $item;
                $keys[] = $key;
            }
        }
        unset($item);

        return $preserveKeys && !empty($items) ? array_combine($keys, $items) : $items;
    }

    /**
     * Возвращает индекс первого найденого элемента найденому по указанному значению
     * <br><b>В отличии от стандартной она возвращает Индес а не Ключ элемента</b>
     * И нафига это надо?!
     *
     * @deprecated use array_filter()
     *
     * @param array|\ArrayAccess $array массив
     * @param mixed $value значение
     * @param boolean $strict с учетом типа
     *
     * @return integer если найден то <b>индекс</b>, если не найден то <b>-1</b>
     */
    public static function indexOf($array, $value, $strict = false)
    {
        self::_assetArray($array);
        $index = 0;
        if ($strict) {
            foreach ($array as $item) {
                if ($item === $value) {
                    return $index;
                }
                $index++;
            }
        } else {
            foreach ($array as $item) {
                if ($item == $value) {
                    return $index;
                }
                $index++;
            }
        }

        return -1;
    }

    /**
     * Возвращает колонку из массива
     *
     * @deprecated use array_column()
     *
     * @param array|\ArrayAccess $array
     * @param string $column колонка массива которую нужно вернуть
     * @param null|string $key колонка которая будет ключем массива. Если null то не менять, если строка то другая колонка если колонка не существует то будет по порятдку
     *
     * @return array
     */
    public static function getColumn($array, $column, $key = null)
    {
        self::_assetArray($array);

        $items = [];

        $firstElement = reset($array);
        if (!empty($array) && isset($firstElement[$column])) {
            if ($key === null) {
                foreach ($array as $i => &$item) {
                    $items[$i] = $item[$column];
                }
            } else {
                if ($key !== false && isset($firstElement[$key])) {
                    foreach ($array as &$item) {
                        $items[$item[$key]] = $item[$column];
                    }
                } else {
                    foreach ($array as &$item) {
                        $items[] = $item[$column];
                    }
                }
            }
            unset($item);
        }

        return $items;
    }

    /**
     * Возвращает несколько колонок из массива
     *
     * @deprecated use array_walk() or some else
     *
     * @param array|\ArrayAccess $array
     * @param array $columns
     * @param null|string $key колонка которая будет ключем массива. Если null то не менять, если строка то другая колонка если колонка не существует то будет по порятдку
     *
     * @return array
     */
    public static function getColumns($array, array $columns, $key = null)
    {
        self::_assetArray($array);

        $items = [];

        $firstElement = reset($array);

        if ($key === null) {
            foreach ($array as $i => &$item) {
                $row = [];
                foreach ($columns as $column) {
                    $row[$column] = $item[$column];
                }
                $items[$i] = $row;
            }
        } else {
            if ($key !== false && isset($firstElement[$key])) {
                foreach ($array as &$item) {
                    $row = [];
                    foreach ($columns as $column) {
                        $row[$column] = $item[$column];
                    }
                    $items[$item[$key]] = $row;
                }
            } else {
                foreach ($array as &$item) {
                    $row = [];
                    foreach ($columns as $column) {
                        $row[$column] = $item[$column];
                    }
                    $items[] = $row;
                }
            }
        }
        unset($item);

        return $items;
    }

    /**
     * Смена ключей массива на значения из его колонки, например для получения id => array('id' => 15, 'name' => 'vasily')
     *
     * @deprecated use array_column() and array_combine()
     *
     * @param array|\ArrayAccess $array
     * @param string $column
     * @param string|null $oldKeyColumn колонка в которую будет сохранен старый ключ, если null то он будет выброшен
     *
     * @return array
     */
    public static function changeKeyColumn($array, $column, $oldKeyColumn = null)
    {
        //self::_assetArray($array);

        $keys = self::getColumn($array, $column);

        if ($oldKeyColumn !== null) {
            $oldKeys = array_keys($array);
            $array = self::combine($keys, $array);
            self::addColumn($array, $oldKeyColumn, $oldKeys);
            return $array;
        } else {
            return self::combine($keys, $array);
        }
    }

    /**
     * Добавление колонки в массив
     *
     * @internal
     *
     * @param array|\ArrayAccess $array
     * @param string $column
     * @param array $values
     *
     * @return array
     */
    private static function addColumn($array, $column, $values)
    {
        self::_assetArray($array);
        self::_assetArray($values);

        $result = $array;

        $value = reset($values);
        foreach ($result as &$item) {
            $item[$column] = $value;
            $value = next($values);
        }

        return $result;
    }

    /**
     * Тоже что и array_combine но будет работать в случае если размеры ключей и значений не совпадают
     *
     * @deprecated use array_combine()
     *
     * @param array|\ArrayAccess $keys
     * @param array $values
     *
     * @return array
     */
    public static function combine($keys, $values)
    {
        self::_assetArray($keys);
        self::_assetArray($values);

        $keysCount = count($keys);
        $valuesCount = count($values);

        if ($keysCount === $valuesCount) {
            return array_combine($keys, $values);
        } else {
            $result = [];
            $value = reset($values);
            foreach ($keys as $key) {
                $result[$key] = $value;
                $value = next($values);
            }

            return $result;
        }
    }

    /**
     * Возвращает массив значений массива по его ключам
     *
     * @deprecated use array_column() or array_walk()
     *
     * @param array|\ArrayAccess $array
     * @param array $keys
     * @param boolean $preserveKeys сохранить ключи
     *
     * @return array
     */
    public static function valuesByKeys($array, $keys, $preserveKeys = false)
    {
        self::_assetArray($array);
        self::_assetArray($keys);

        $items = [];
        $existKeys = [];

        foreach ($keys as $key) {
            if (isset($array[$key]) || array_key_exists($key, $array)) {
                $items[] = $array[$key];
                $existKeys[] = $key;
            }
        }

        return $preserveKeys && !empty($items) ? array_combine($existKeys, $items) : $items;
    }

    /**
     * Получение значение из массива по указанному пути
     * (напр. $array = array('vasya' => array(1 => 'batkovich')), $path = 'vasya.1')
     *
     * @deprecated бесполезная дичь
     *
     * @param array|\ArrayAccess $array
     * @param string $path
     * @param string $delimiter (optional, default = '.')
     *
     * @return mixed
     */
    public static function getValueByPath($array, $path, $delimiter = '.')
    {
        if (!empty($path)) {
            self::_assetArray($array);

            $keys = explode($delimiter, $path);

            foreach ($keys as $index => $key) {
                if (substr($key, -2, strlen($key)) == '[]') {
                    $fixedKey = substr($key, 0, -2);
                    if ((array_key_exists($fixedKey, $array) || isset($array[$fixedKey]))
                        && self::_assetArray($array[$fixedKey], false)) {
                        $context = $array[$fixedKey];
                        $array = [];
                        $nextPath = implode($delimiter, array_slice($keys, $index + 1));
                        foreach ($context as $value) {
                            $array[] = self::getValueByPath(
                                $value, $nextPath, $delimiter
                            );
                        }
                        break;
                    }
                } else {
                    $array = $array[$key];
                }
            }
        }

        return $array;
    }

    /**
     * Проверка существания значения в массиве по указанному пути
     *
     * @deprecated бесполезная дичь
     *
     * @param array|\ArrayAccess $array
     * @param string $path
     * @param string $delimiter (optional, default = '.')
     *
     * @return boolean
     */
    public static function isValidPath($array, $path, $delimiter = '.')
    {
        if (empty($path)) {
            return true;
        }

        self::_assetArray($array);

        $keys = explode($delimiter, $path);
        foreach ($keys as $index => $key) {
            if (substr($key, -2, strlen($key)) == '[]') {
                $fixedKey = substr($key, 0, -2);
                if ((array_key_exists($fixedKey, $array) || isset($array[$fixedKey]))
                    && self::_assetArray($array[$fixedKey], false)) {
                    $context = $array[$fixedKey];
                    $nextPath = implode($delimiter, array_slice($keys, $index + 1));
                    foreach ($context as $value) {
                        self::isValidPath(
                            $value, $nextPath, $delimiter
                        );
                    }
                    break;
                } else {
                    return false;
                }
            } else {
                if (array_key_exists($key, $array) || isset($array[$key])) {
                    $array = $array[$key];
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Поиск пути к элементу по его значению
     *
     * @deprecated бесполезная дичь
     *
     * @param array|\ArrayAccess $array
     * @param mixed $value
     * @param string $delimiter (optional, default = '.')
     *
     * @return string
     */
    public static function findPathByValue($array, $value, $delimiter = '.')
    {
        self::_assetArray($array);

        //делаем одномерный массив со значениями {value, key, parentNode}, для удобного обратного обхода (от значения к корню)
        $arrayToTree = function ($array, array &$result, $parentNode = null) use ($value, &$arrayToTree) {
            foreach ($array as $key => $value) {
                $node = new \stdClass();
                $node->value = $value;
                $node->key = $key;
                $node->parentNode = $parentNode;
                $result[] = $node;
                if (is_array($value)) {
                    $arrayToTree($value, $result, $node);
                }
            }
        };

        $tree = [];
        $arrayToTree($array, $tree);

        //нахождение ключа значения
        $valueKey = '';
        foreach ($tree as $treeKey => $treeValue) {
            if ($value == $treeValue->value) {
                $valueKey = $treeKey;
            }
        }

        //собственно сборка пути
        $node = $tree[$valueKey];
        $path = [];

        while (!is_null($node)) {
            $path[] = $node->key;
            $node = $node->parentNode;
        }

        return implode($delimiter, array_reverse($path));
    }

    /**
     * Групировка элементов
     *
     * @deprecated use array_walk() or array_map() etc
     *
     * @param array|\ArrayAccess $array
     * @param string|array $groupBy прим.: 'name', array('name', 'category_id'), array('user.name', 'category.id')
     * @param string $delimiter
     *
     * @return array
     */
    public static function groupBy($array, $groupBy, $delimiter = '.')
    {
        self::_assetArray($array);
        is_string($groupBy) || self::_assetArray($groupBy);

        $result = [];
        if (is_string($groupBy)) {
            foreach ($array as &$item) {
                $groupKey = self::getValueByPath($item, $groupBy, $delimiter);
                if (!isset($result[$groupKey])) {
                    $result[$groupKey] = [];
                }
                $result[$groupKey][] = $item;
            }
            unset($item);
        } else {
            foreach ($array as &$item) {
                $groupKey = [];
                foreach ($groupBy as $groupItem) {
                    $groupKey[] = self::getValueByPath($item, $groupItem, $delimiter);
                }
                $groupKey = serialize($groupKey);

                if (!isset($result[$groupKey])) {
                    $result[$groupKey] = [];
                }
                $result[$groupKey][] = $item;
            }
            unset($item);
        }

        return $result;
    }
}
