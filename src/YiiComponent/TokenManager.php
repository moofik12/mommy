<?php

namespace MommyCom\YiiComponent;

use CHttpException;
use MommyCom\Service\CurrentTime;

class TokenManager
{
    const TOKEN_VALID_PERIOD = 3600;

    /**
     * @var string
     */
    private $_privateKey = 'sz8@fj73!2z@';

    protected function _generateToken($name, $time)
    {
        $params = [];
        $params[] = $time - $time % self::TOKEN_VALID_PERIOD;
        $params[] = serialize((string)$name);
        $params[] = $this->_privateKey;
        return hash('crc32b', implode($params, '@'));
    }

    /**
     * @param string|mixed $name
     *
     * @return string
     */
    public function getToken($name)
    {
        return $this->_generateToken($name, CurrentTime::getUnixTimestamp());
    }

    /**
     * @param string $name
     * @param string $token
     *
     * @return bool
     */
    public function isValid($name, $token)
    {
        $time = CurrentTime::getUnixTimestamp();

        return $this->_generateToken($name, $time) == $token ||
            $this->_generateToken($name, $time - self::TOKEN_VALID_PERIOD) == $token;
    }

    /**
     * @param $name
     * @param $token
     *
     * @throws CHttpException
     */
    public function validateToken($name, $token)
    {
        if (!is_string($token) || !$this->isValid($name, $token)) {
            throw new CHttpException(400, \Yii::t('yii', 'The CSRF token could not be verified.'));
        }
    }
}
