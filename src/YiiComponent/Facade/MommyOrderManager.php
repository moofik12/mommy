<?php

namespace MommyCom\YiiComponent\Facade;

use MommyCom\Service\Order\MommyOrder;
use MommyCom\Service\Order\MommyOrderManager as MommyOrderManagerService;

class MommyOrderManager extends AbstractFacade
{
    /**
     * @param int $id
     *
     * @return MommyOrder|null
     */
    public static function find(int $id): ?MommyOrder
    {
        return static::getContainer()->get(MommyOrderManagerService::class)->find($id);
    }
}
