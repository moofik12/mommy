<?php

namespace MommyCom\YiiComponent\Facade;

use Psr\SimpleCache\CacheInterface;

class Cache extends AbstractFacade
{
    /**
     * @return CacheInterface
     */
    public static function cache(): CacheInterface
    {
        return static::getContainer()->get(CacheInterface::class);
    }
}
