<?php

namespace MommyCom\YiiComponent\Facade;

use MommyCom\Service\Translator\RegionalTranslator as TranslatorService;

class EmailTranslator extends AbstractFacade
{
    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function t(string $sourceText, $params = []): string
    {
        return static::get()->t($sourceText, $params);
    }

    /**
     * @return TranslatorService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function get(): TranslatorService
    {
        return static::getContainer()->get('translator.email');
    }
}
