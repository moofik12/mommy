<?php

namespace MommyCom\YiiComponent\Facade;

use MommyCom\Service\Region\Regions as RegionsService;
use MommyCom\Service\Region\Region;

class Regions extends AbstractFacade
{
    /**
     * @return string[]
     */
    public static function getRegions(): array
    {
        return self::getContainer()->get(RegionsService::class)->getRegions();
    }

    /**
     * @param string $region
     *
     * @return string
     */
    public static function getHostname(string $region): string
    {
        return self::getContainer()->get(RegionsService::class)->getHostname($region);
    }

    /**
     * @return Region
     */
    public static function getUserRegion(): Region
    {
        return self::getContainer()->get(RegionsService::class)->getUserRegion();
    }

    /**
     * @return Region
     */
    public static function getServerRegion(): Region
    {
        return self::getContainer()->get(RegionsService::class)->getServerRegion();
    }
}
