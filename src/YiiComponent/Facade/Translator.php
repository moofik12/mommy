<?php

namespace MommyCom\YiiComponent\Facade;

use MommyCom\Service\Translator\TranslatorInterface;

/**
 * Class Translator
 * Фасад для сервиса переводов, рекоммендуется как замена Yii::t() в моделях
 */
class Translator extends AbstractFacade
{
    /**
     * @param string $sourceText
     * @param mixed $params
     *
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function t(string $sourceText, $params = []): string
    {
        return static::get()->t($sourceText, $params);
    }

    /**
     * @return TranslatorInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function get(): TranslatorInterface
    {
        return static::getContainer()->get(TranslatorInterface::class);
    }
}
