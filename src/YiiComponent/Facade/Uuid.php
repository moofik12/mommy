<?php

namespace MommyCom\YiiComponent\Facade;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidInterface;

class Uuid extends AbstractFacade
{
    /**
     * @return UuidFactory
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function factory(): UuidFactory
    {
        return static::getContainer()->get(UuidFactory::class);
    }

    /**
     * @return UuidInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function uuid4(): UuidInterface
    {
        return static::factory()->uuid4();
    }

    /**
     * @param string $uuid
     *
     * @return bool
     */
    public static function isValid(string $uuid): bool
    {
        return RamseyUuid::isValid($uuid);
    }
}
