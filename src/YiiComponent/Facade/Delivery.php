<?php

namespace MommyCom\YiiComponent\Facade;

use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryDirectory;
use MommyCom\Service\Delivery\DeliveryInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class Delivery
 *
 * @package MommyCom\YiiComponent\Facade
 */
class Delivery extends AbstractFacade
{
    /**
     * @param int $id
     *
     * @return DeliveryInterface
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function get(int $id): DeliveryInterface
    {
        if (!isset(DeliveryDirectory::CLASSES[$id])) {
            throw new \RuntimeException(sprintf('Delivery with id %d not found.', $id));
        }

        $class = DeliveryDirectory::CLASSES[$id];

        return static::getContainer()->get($class);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public static function has(int $id): bool
    {
        if (!isset(DeliveryDirectory::CLASSES[$id])) {
            return false;
        }

        $class = DeliveryDirectory::CLASSES[$id];

        return static::getContainer()->has($class);
    }

    /**
     * @return AvailableDeliveries
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function available(): AvailableDeliveries
    {
        return static::getContainer()->get(AvailableDeliveries::class);
    }
}
