<?php

namespace MommyCom\YiiComponent\LocalizationTool\Actions;

/**
 * Class NewLocaleAction
 * @package MommyCom\YiiComponent\LocalizationTool\Actions
 */
class NewLocaleAction extends AbstractAction
{
    use GenericActionsTrait;

    /**
     * @param string $path
     * @param string $filename
     * @param string $localename
     */
    protected function createDummyTranslationsAndCsv
    (
        string $path,
        string $filename,
        string $localename
    ) {
        $file = include_once($path . DIRECTORY_SEPARATOR . self::ETHALON .
            DIRECTORY_SEPARATOR . $filename);
        $newFile = [];

        foreach ($file as $key => $value) {
            $newFile[$key] = '';
        }

        $newLocalePath = $path . DIRECTORY_SEPARATOR .
            $localename . DIRECTORY_SEPARATOR;

        $this->generateCsv(
            $newLocalePath . str_replace('php', 'csv', $filename),
            $newFile
        );

        file_put_contents(
            $newLocalePath .
            $filename, $this->makePhpFileReturningArray($newFile)
        );
    }

    public function runAction()
    {
        $this->info('Enter the name of new locale: ');
        $locale = trim(fgets($this->getInput()));
        $this->info(PHP_EOL . "Generating new locale... Please wait." . PHP_EOL);

        $newLocalePath = $this->getMessagesPath() . DIRECTORY_SEPARATOR . $locale;
        if (!is_dir($newLocalePath)) {
            mkdir($newLocalePath);
        }

        foreach ($this->translationFiles as $file) {
            $this->createDummyTranslationsAndCsv($this->getMessagesPath(), $file, $locale);
        }

        $this->info('PHP and CSV files were successfully generated into new locale directory.' . PHP_EOL);
    }
}
