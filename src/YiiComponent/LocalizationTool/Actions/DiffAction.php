<?php

namespace MommyCom\YiiComponent\LocalizationTool\Actions;

/**
 * Class DiffAction
 * @package MommyCom\YiiComponent\LocalizationTool\Actions
 */
class DiffAction extends AbstractAction
{
    /**
     * @var string
     */
    const PREFIX = 'diff';

    use GenericActionsTrait;

    public function runAction()
    {
        $this->info('Please enter locale to generate diff for:');
        $locale = trim(fgets($this->getInput()));
        $diffLocale = $this->getMessagesPath() . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR;
        $ethalonLocale = $this->getMessagesPath() . DIRECTORY_SEPARATOR . self::ETHALON . DIRECTORY_SEPARATOR;

        if (is_dir($diffLocale)) {
            $this->info(
                "Generating translation diff files... Please wait." .
                PHP_EOL . PHP_EOL
            );

            $diffCommon = include $diffLocale . $this->translationFiles[0];
            $diffEmail = include $diffLocale . $this->translationFiles[1];
            $ethalonCommon = include $ethalonLocale . $this->translationFiles[0];
            $ethalonEmail = include $ethalonLocale . $this->translationFiles[1];

            $resultDiffCommon = array_diff_key($ethalonCommon, $diffCommon);
            foreach ($ethalonCommon as $key => $value) {
                if (!empty($diffCommon[$key]) &&
                    (
                        $diffCommon[$key] === $key ||
                        preg_match('/.*?[а-яёЁ]+.*/ius',  $diffCommon[$key]) ||
                        $diffCommon[$key] === ''
                    )
                ) {
                    $resultDiffCommon[$key] = $key;
                }
            }
            $fileName = self::PREFIX . str_replace('php', 'csv', $this->translationFiles[0]);
            $this->generateCsv($diffLocale . $fileName, $resultDiffCommon);

            $resultDiffEmail = array_diff_key($ethalonEmail, $diffEmail);
            foreach ($ethalonEmail as $key => $value) {
                if (!empty($diffEmail[$key]) &&
                    (
                        $diffEmail[$key] === $key ||
                        preg_match('/.*?[а-яёЁ]+.*/ius',  $diffEmail[$key]) ||
                        $diffEmail[$key] === ''
                    )
                ) {
                    $resultDiffEmail[$key] = $key;
                }
            }
            $fileName = self::PREFIX . str_replace('php', 'csv', $this->translationFiles[1]);
            $this->generateCsv($diffLocale . $fileName , $resultDiffEmail);
        } else {
            $this->error('Something went wrong.' . PHP_EOL);
        }
    }
}
