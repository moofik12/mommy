<?php

namespace MommyCom\YiiComponent\LocalizationTool\Actions;

/**
 * Interface ActionInterface
 * @package MommyCom\YiiComponent\LocalizationTool
 */
interface ActionInterface
{
    public function runAction();
}
