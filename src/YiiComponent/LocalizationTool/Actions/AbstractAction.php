<?php

namespace MommyCom\YiiComponent\LocalizationTool\Actions;

use MommyCom\YiiComponent\LocalizationTool\LocalizationTool;

/**
 * Class AbstractAction
 * @package MommyCom\YiiComponent\LocalizationTool\Actions
 */
abstract class AbstractAction implements ActionInterface
{
    const ETHALON = 'in';

    /**
     * @var array
     */
    protected $translationFiles = [
        'common.php',
        'email.php',
        'backend.php'
    ];

    /**
     * @var LocalizationTool $tool
     */
    private $tool;

    /**
     * AbstractAction constructor.
     * @param LocalizationTool $localizationTool
     */
    public function __construct(LocalizationTool $localizationTool)
    {
        $this->tool = $localizationTool;
    }

    /**
     * @return resource
     */
    protected function getInput()
    {
        return $this->tool->getInput();
    }

    /**
     * @return string
     */
    protected function getMessagesPath(): string
    {
        return $this->tool->getMessagesPath();
    }
}
