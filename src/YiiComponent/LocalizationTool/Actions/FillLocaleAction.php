<?php

namespace MommyCom\YiiComponent\LocalizationTool\Actions;
use MommyCom\YiiComponent\LocalizationTool\PathException;

/**
 * Class FillLocaleAction
 * @package MommyCom\YiiComponent\LocalizationTool\Actions
 */
class FillLocaleAction extends AbstractAction
{
    use GenericActionsTrait;

    /**
     * @var string - exception message
     */
    const LOCALE_PATH_ERROR = 'Incorrect locale path';

    /**
     * @var string - exception message
     */
    const CSV_PATH_ERROR = 'Incorrect CSV file path';

    /**
     * Execute fill locale action
     */
    public function runAction()
    {
        $this->info('Please enter locale to fill:');
        $locale = trim(fgets($this->getInput()));

        foreach ($this->translationFiles as $file) {
            $this->info('Enter CSV name (or 0 for skip) for ' . $file . ':');
            $CSVName = trim(fgets($this->getInput()));

            if (strlen($CSVName) === 0) {
                continue;
            }

            try {
                $this->fillFromCSV($locale, $file, $CSVName);
            } catch (PathException $exception) {
                $this->info($exception->getMessage() . PHP_EOL);
                $this->info('Please, try again.' . PHP_EOL . PHP_EOL);
                return;
            }
        }
    }

    /**
     * @param $locale
     * @param $file
     * @param $CSVName
     * @throws PathException
     */
    private function fillFromCSV($locale, $file, $CSVName)
    {
        $filePath = $this->getMessagesPath() . DIRECTORY_SEPARATOR . $locale .
            DIRECTORY_SEPARATOR . $file;
        $CSVPath = $this->getMessagesPath() . DIRECTORY_SEPARATOR . $locale .
            DIRECTORY_SEPARATOR . $CSVName;

        if (!is_file($filePath)) {
            throw new PathException(self::LOCALE_PATH_ERROR);
        } elseif (!is_file($CSVPath)) {
            throw new PathException(self::CSV_PATH_ERROR);
        }

        $content = require_once $filePath;

        $CSVFile = fopen($CSVPath, 'r');
        while (($data = fgetcsv($CSVFile, 0, "~")) !== false) {
            $content[$data[0]] = $data[1];
        }
        fclose($CSVFile);

        $fileContent = $this->makePhpFileReturningArray($content);
        file_put_contents($filePath, $fileContent);
    }
}
