<?php

namespace MommyCom\YiiComponent\LocalizationTool\Actions;

/**
 * Trait GenericActionsTrait
 * @package MommyCom\YiiComponent\LocalizationTool
 */
trait GenericActionsTrait
{
    /**
     * @param array $arr
     * @return string
     */
    protected function makePhpFileReturningArray(array $arr)
    {
        ob_start();
        var_export($arr);
        $contents = ob_get_clean();
        $contents = "<?php\nreturn " . $contents . ';';
        return $contents;
    }

    /**
     * @param $filename
     * @param $data
     * @param string $delimiter
     * @param string $enclosure
     */
    protected function generateCsv($filename, $data, $delimiter = '~', $enclosure = '"')
    {
        file_put_contents($filename, '');
        $handle = fopen($filename, 'w+');
        foreach ($data as $key => $line) {
            $key = str_replace('"', '', $key);
            fputcsv($handle, [$key], $delimiter, $enclosure);
        }
        fclose($handle);

        $forClean = file_get_contents($filename);
        $forClean = str_replace('"', '', $forClean);
        file_put_contents($filename, $forClean);
    }

    /**
     * @param $msg - info message
     */
    protected function info($msg)
    {
        echo "\033[1;33m" . $msg . "\033[0m";
    }

    /**
     * @param $msg - error message
     */
    protected function error($msg)
    {
        echo "\033[1;31m" . 'Error: ' . $msg . "\033[0m";
    }
}
