<?php

namespace MommyCom\YiiComponent\LocalizationTool;

/**
 * Class PathException
 * @package MommyCom\YiiComponent\LocalizationTool
 */
class PathException extends \Exception
{
}
