<?php

namespace MommyCom\YiiComponent\LocalizationTool;

use MommyCom\YiiComponent\LocalizationTool\Actions\AbstractAction;
use MommyCom\YiiComponent\LocalizationTool\Actions\ActionInterface;
use MommyCom\YiiComponent\LocalizationTool\Actions\DiffAction;
use MommyCom\YiiComponent\LocalizationTool\Actions\FillLocaleAction;
use MommyCom\YiiComponent\LocalizationTool\Actions\GenericActionsTrait;
use MommyCom\YiiComponent\LocalizationTool\Actions\NewLocaleAction;

/**
 * Class LocalizationTool
 * @package MommyCom\YiiComponent\LocalizationTool
 */
class LocalizationTool
{
    use GenericActionsTrait;

    /**
     * @var string - exception message
     */
    const MESSAGES_PATH_ERROR = 'Please specify REAL messages directory path.';

    /**
     * @var bool|resource
     */
    private $input;

    /**
     * @var null|string - messages directory relative path
     */
    private $messageDirectoryAbsolutePath;

    /**
     * LocalizationTool constructor.
     * @param $messageDirectoryAbsolutePath
     * @throws PathException
     */
    public function __construct($messageDirectoryAbsolutePath)
    {
        $this->input = fopen('php://stdin', 'r');

        if (
            !empty($messageDirectoryAbsolutePath) &&
            is_dir($messageDirectoryAbsolutePath) &&
            is_dir($messageDirectoryAbsolutePath . DIRECTORY_SEPARATOR .
                AbstractAction::ETHALON . DIRECTORY_SEPARATOR)
        ) {
            $this->messageDirectoryAbsolutePath = $messageDirectoryAbsolutePath;
        } else {
            $this->error(self::MESSAGES_PATH_ERROR . PHP_EOL);
            throw new PathException(self::MESSAGES_PATH_ERROR);
        }
    }

    /**
     * @param ActionInterface $actionRunner
     */
    protected function dispatchAction(ActionInterface $actionRunner)
    {
        $actionRunner->runAction();
    }

    /**
     * @return resource
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @return string
     */
    public function getMessagesPath(): string
    {
        return $this->messageDirectoryAbsolutePath;
    }

    /**
     * Run localization tool
     */
    public function run()
    {
        $this->info("Messages directory is located in: " .
            $this->messageDirectoryAbsolutePath . PHP_EOL . PHP_EOL);

        while (true) {
            $this->info('Choose the menu point:' . PHP_EOL);
            $this->info('1. Generate new locale' . PHP_EOL);
            $this->info('2. Generate translation diff files' . PHP_EOL);
            $this->info('3. Fill locale from CSV files' . PHP_EOL);
            $this->info('4. Quit' . PHP_EOL);

            $action = (int)fgets($this->input);

            switch ($action) {
                case 1:
                    $this->dispatchAction(new NewLocaleAction($this));
                    break;
                case 2:
                    $this->dispatchAction(new DiffAction($this));
                    break;
                case 3:
                    $this->dispatchAction(new FillLocaleAction($this));
                    break;
                case 4:
                    return;
                default:
                    $this->error("Wrong action." . PHP_EOL);
                    break;
            }
        }
    }
}
