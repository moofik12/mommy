<?php

namespace MommyCom\YiiComponent\Statistic;

use CComponent;
use Closure;

/**
 * Class StatisticFilter
 *
 * @property string $joinType
 * @property-read string $availableJoinType
 */
class StatisticFilter extends CComponent
{
    const JOIN_OR = 'OR';
    const JOIN_AND = 'AND';

    /**
     * unique string
     *
     * @var string
     */
    public $id;

    /**
     * @var string|array
     */
    public $value;

    /**
     * @var Statistic
     */
    public $statistic;

    /**
     * @var Closure|array
     */
    public $apply;

    /**
     * вывод HTML
     *
     * @var Closure|array
     */
    public $renderHtml;

    /**
     * @var bool
     */
    public $isVisible = true;

    /**
     * internal storage
     *
     * @var array
     */
    public $data;

    /**
     * Тип фильтрации с другими фильтрами
     *
     * @var string
     */
    protected $_joinType = self::JOIN_OR;

    /**
     * @return string
     */
    public function getJoinType()
    {
        return $this->_joinType;
    }

    /**
     * @param string $joinType
     */
    public function setJoinType($joinType)
    {
        $validValue = $this->getAvailableJoinType();

        if (in_array($joinType, $validValue)) {
            $this->_joinType = $joinType;
        }
    }

    /**
     * @param Statistic $statistic
     */
    public function __construct(Statistic $statistic)
    {
        $this->statistic = $statistic;
    }

    public function output()
    {
        $output = '';

        if (is_callable($this->renderHtml)) {
            $output .= call_user_func_array($this->renderHtml, [$this->statistic, $this]);
        } elseif (is_string($this->renderHtml)) {
            $output .= $this->renderHtml;
        }

        return $output;
    }

    public function apply()
    {
        if (is_callable($this->apply)) {
            call_user_func_array($this->apply, [$this->statistic, $this]);
        }
    }

    public function connect()
    {
        $this->statistic->addFilter($this->id, $this);
    }

    public function disconnect()
    {
        $this->statistic->removeFilter($this);
    }

    /**
     * возвращает идентификатор типа соединения фильтра для рендеринга в HTML
     *
     * @return string
     */
    public function getJoinTypeId()
    {
        static $private = '_joinType';

        return $this->id . $private;
    }

    /**
     * @return array
     */
    public function getAvailableJoinType()
    {
        return [
            self::JOIN_OR,
            self::JOIN_AND,
        ];
    }
}
