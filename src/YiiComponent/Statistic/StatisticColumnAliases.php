<?php

namespace MommyCom\YiiComponent\Statistic;

class StatisticColumnAliases
{
    /**
     * @var array
     */
    protected $_aliases;

    /**
     * @var array
     */
    protected $_columns;

    /**
     * @param array $aliases
     */
    public function __construct(array $aliases)
    {
        $this->_aliases = $aliases;
        $this->_columns = array_flip($aliases);
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function getAlias($name)
    {
        return isset($this->_aliases[$name]) ? $this->_aliases[$name] : $name;
    }

    /**
     * @param $alias
     *
     * @return mixed
     */
    public function getColumn($alias)
    {
        return isset($this->_columns[$alias]) ? $this->_columns[$alias] : $alias;
    }
}
