<?php

namespace MommyCom\YiiComponent\Statistic;

use ArrayAccess;
use CException;
use DateTime;
use ReflectionClass;

/**
 * Class StatisticModelAbstract
 */
abstract class StatisticModelAbstract implements StatisticModelInterface, ArrayAccess
{
    /**
     * @var DateTime
     */
    protected $dateTimeFrom;

    /**
     * @var DateTime
     */
    protected $dateTimeTo;

    /**
     * @var Statistic
     */
    protected $statistic;

    /**
     * @var string[]
     */
    private static $names = [];

    /**
     * StatisticModelAbstract constructor.
     *
     * @param Statistic $statistic
     * @param DateTime $dateTimeFrom
     * @param DateTime $dateTimeTo
     */
    public function __construct(Statistic $statistic, DateTime $dateTimeFrom, DateTime $dateTimeTo)
    {
        $this->statistic = $statistic;
        $this->dateTimeFrom = $dateTimeFrom;
        $this->dateTimeTo = $dateTimeTo;

        $this->afterCreate();
    }

    /**
     * Инициализация, иные вычисления на основе дат
     */
    abstract protected function afterCreate();

    /**
     * @return DateTime
     */
    public function getDateTimeFrom()
    {
        return $this->dateTimeFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTimeTo()
    {
        return $this->dateTimeTo;
    }

    /**
     * @return Statistic
     */
    public function getStatistic()
    {
        return $this->statistic;
    }

    /**
     * Whether a offset exists
     *
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     *
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return in_array($offset, $this->attributeNames());
    }

    /**
     * Offset to retrieve
     *
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     *
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        if ($this->offsetExists($offset)) {
            return $this->$offset;
        }

        return null;
    }

    /**
     * Offset to set
     *
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     *
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        if ($this->offsetExists($offset)) {
            $this->$offset = $value;
        }
    }

    /**
     * Offset to unset
     *
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     *
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            $this->$offset = null;
        }
    }

    /**
     * Returns the list of attribute names.
     * By default, this method returns all public properties of the class.
     * You may override this method to change the default.
     *
     * @return array list of attribute names. Defaults to all public properties of the class.
     */
    public function attributeNames()
    {
        $className = get_class($this);
        if (!isset(self::$names[$className])) {
            $class = new ReflectionClass(get_class($this));
            $names = [];
            foreach ($class->getProperties() as $property) {
                $name = $property->getName();
                if ($property->isPublic() && !$property->isStatic())
                    $names[] = $name;
            }
            return self::$names[$className] = $names;
        } else
            return self::$names[$className];
    }

    /**
     * @param $name
     *
     * @return mixed
     * @throws CException
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        throw new CException(\Yii::t('yii', 'Property "{class}.{property}" is not defined.',
            ['{class}' => get_class($this), '{property}' => $name]));
    }

    /**
     * @param $name
     * @param $value
     *
     * @return mixed
     * @throws CException
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            return $this->$setter($value);
        }

        throw new CException(\Yii::t('yii', 'Property "{class}.{property}" is not defined.',
            ['{class}' => get_class($this), '{property}' => $name]));
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        }

        return false;
    }

    /**
     * @param $name
     *
     * @throws CException
     */
    public function __unset($name)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter(null);
        } elseif (method_exists($this, 'get' . $name)) {
            throw new CException(\Yii::t('yii', 'Property "{class}.{property}" is read only.',
                ['{class}' => get_class($this), '{property}' => $name]));
        }
    }

}
