<?php

namespace MommyCom\YiiComponent\Statistic;

use CComponent;
use CDataProviderIterator;
use CDbCommand;
use CEvent;
use CException;
use CMap;
use CSqlDataProvider;
use DateTime;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;

class Statistic extends CComponent
{
    /**
     * Буфер выбора с SQL, массивы данных
     *
     * @var int
     */
    public $bufferSize = 5000;

    /**
     * @var UnShardedActiveRecord
     */
    protected $_selector;

    /**
     * @var CMap
     */
    protected $_filters;

    /**
     * @var StatisticTimeFilter
     */
    protected $_timeFilter;

    /**
     * @var string
     */
    protected $_modelClass;

    /**
     * @var StatisticTotalModelInterface
     */
    protected $_totalModel;

    /**
     * @var StatisticColumnAliases
     */
    protected $_columnAliases;

    /**
     * @var bool
     */
    private $_appliedFilters = false;

    public function __construct()
    {
        $this->_filters = new CMap();
    }

    /**
     * @return StatisticTimeFilter
     * @throws CException
     */
    public function getTimeFilter()
    {
        if ($this->_timeFilter === null) {
            throw new CException ('Not found timeFilter in statistic');
        }

        return $this->_timeFilter;
    }

    /**
     * @param StatisticTimeFilter $timerFilter
     */
    public function setTimeFilter(StatisticTimeFilter $timerFilter)
    {
        $this->_timeFilter = $timerFilter;
    }

    /**
     * @return UnShardedActiveRecord
     * @throws CException
     */
    public function getSelector()
    {
        if ($this->_selector === null) {
            throw new CException ('Not found selector in statistic');
        }

        return $this->_selector;
    }

    /**
     * @param UnShardedActiveRecord $selector
     */
    public function setSelector(UnShardedActiveRecord $selector)
    {
        $this->_selector = $selector;
    }

    /**
     * @param bool $filterTime
     *
     * @return UnShardedActiveRecord
     */
    public function getSelectorCopy($filterTime = true)
    {
        $this->_applyFilters();
        $selector = $this->getSelector()->copy();
        if ($filterTime) {
            $selector->dbCriteria->addCondition($this->getTimeFilter()->getSql());
        }

        return $selector;
    }

    /**
     * @return StatisticModelAbstract
     * @throws CException
     */
    public function getModelClass()
    {
        if (empty($this->_modelClass)) {
            throw new CException ('Empty model in statistic');
        }

        if (!class_exists($this->_modelClass)) {
            throw new CException ("Model {$this->_modelClass} not found for statistic");
        }

        return $this->_modelClass;
    }

    /**
     * @param string $modelClass
     */
    public function setModelClass($modelClass)
    {
        $this->_modelClass = $modelClass;
    }

    /**
     * @return StatisticTotalModelInterface
     */
    public function getTotalModel()
    {
        return $this->_totalModel;
    }

    /**
     * @param StatisticTotalModelInterface $totalModel
     */
    public function setTotalModel(StatisticTotalModelInterface $totalModel)
    {
        $this->_totalModel = $totalModel;
    }

    /**
     * @return StatisticColumnAliases
     */
    public function getColumnAliases()
    {
        if ($this->_columnAliases === null) {
            $aliases = [];
            $this->getSelector()->getSqlCommand($aliases);
            $this->_columnAliases = new StatisticColumnAliases($aliases);
        }

        return $this->_columnAliases;
    }

    public function getDataProvider(array $config)
    {
        return new StatisticDataProvider($this, $config);
    }

    public function run()
    {
        $this->renderFilters();
    }

    /**
     * @throws CException
     */
    protected function onBeforeRun()
    {
        if ($this->hasEventHandler(__FUNCTION__)) {
            $this->raiseEvent(__FUNCTION__, new CEvent($this));
        }
    }

    /**
     * @throws CException
     */
    protected function onAfterRun()
    {
        if ($this->hasEventHandler(__FUNCTION__)) {
            $this->raiseEvent(__FUNCTION__, new CEvent($this));
        }
    }

    /**
     * @param bool $return
     *
     * @return string|null
     */
    public function renderFilters($return = false)
    {
        ob_start();
        foreach ($this->getFilters() as $filter) {
            echo $filter->output();
        }
        $filters = ob_get_clean();

        if (!$return) {
            echo $filters;
            return '';
        }

        return $filters;
    }

    /**
     * @param $key
     * @param bool $return
     *
     * @return string
     */
    public function renderFilter($key, $return = false)
    {
        $output = '';
        if ($this->_filters->contains($key)) {
            $output .= $this->_filters->itemAt($key)->output();
        }

        if (!$return) {
            echo $output;
            return '';
        }

        return $output;
    }

    /**
     * @return StatisticFilter[]
     */
    public function getFilters()
    {
        return $this->_filters->toArray();
    }

    /**
     * @param array $filters
     *
     * @throws CException
     */
    public function setFilters(array $filters)
    {
        if (empty($filters)) {
            $this->_filters->clear();
        } else {
            foreach ($filters as $filter) {
                if (!$filter instanceof StatisticFilter) {
                    throw new CException('filter must be instance of StatisticFilter');
                }

                $this->_filters->add($filter->id, $filter);
            }
        }
    }

    /**
     * @param StatisticFilter $filter
     *
     * @return bool|mixed
     * @throws CException
     */
    public function removeFilter(StatisticFilter $filter)
    {
        if ($this->_filters->contains($filter->id)) {
            return $this->_filters->remove($filter->id);
        }

        return false;
    }

    /**
     * @param StatisticFilter $filter
     *
     * @throws CException
     */
    public function addFilter(StatisticFilter $filter)
    {
        $this->_filters->add($filter->id, $filter);
    }

    /**
     * @param $name
     *
     * @return StatisticFilter|null
     */
    public function getFilter($name)
    {
        if ($this->_filters->contains($name)) {
            return $this->_filters->itemAt($name);
        }

        return null;
    }

    /**
     * @param array $filters
     */
    public function addFilters(array $filters)
    {
        foreach ($filters as $filter) {
            $this->addFilter($filter);
        }
    }

    /**
     * @return StatisticFilter
     */
    public function createFilter()
    {
        return new StatisticFilter($this);
    }

    /**
     * @return StatisticTimeFilter
     */
    public function createTimeFilter()
    {
        return new StatisticTimeFilter($this);
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return $this->getSqlCommand()->getText();
    }

    /**
     * @return mixed
     * @throws CException
     */
    public function isUseRenamedColumns()
    {
        return empty($this->getSelector()->dbCriteria->with);
    }

    /**
     * @return CDbCommand
     */
    public function getSqlCommand()
    {
        $this->_applyFilters();
        return clone $this->getSelector()->getSqlCommand();
    }

    protected function _applyFilters()
    {
        if ($this->_appliedFilters === false) {
            foreach ($this->getFilters() as $filter) {
                $filter->apply();
            }

            $this->_appliedFilters = true;
        }
    }

    /**
     * @param integer $page
     * @param integer $pageSize
     *
     * @return StatisticModelInterface[]
     */
    public function getData($page = null, $pageSize = null)
    {
        $selector = $this->_copySelector();

        $result = [];

        $selectCommand = $selector->getSqlCommand();
        $sqlProvider = new CSqlDataProvider($selectCommand, [
            'params' => $selectCommand->params,
            'pagination' => [
                'pageSize' => $this->bufferSize,
                'pageVar' => '__internal-page',
            ],
        ]);
        $sqlProvider->setTotalItemCount((int)$selector->count());

        $dataIterator = new CDataProviderIterator($sqlProvider);
        $totalModel = $this->getTotalModel();
        $currentDate = '';
        $currentDateTime = null;
        $modelClass = $this->getModelClass();
        $aliases = $this->getColumnAliases();
        $timeFilter = $this->getTimeFilter();
        $buffer = [];
        $bufferSize = 0;

        $start = false;
        $end = false;
        $current = 0;

        if ($page !== null && $pageSize !== null) {
            $start = $page * $pageSize;
            $end = $start + $pageSize;
        }

        $bufferWatch = function ($bufferFlush = false) use (&$result, &$buffer, &$bufferSize, &$timeFilter, &$currentDate, &$currentDateTime, &$current, &$start, &$end, &$totalModel, $modelClass) {
            /* @var DateTime|null $currentDateTime */
            if ($currentDateTime === null && empty($buffer)) {
                return;
            }

            /* @var StatisticModelAbstract $modelInstance */
            $dateTimePeriodStart = $timeFilter->getStartPeriodDateTime($currentDateTime->getTimestamp());
            $dateTimePeriodEnd = $timeFilter->getEndPeriodDateTime($dateTimePeriodStart->getTimestamp());

            $modelInstance = isset($result[$currentDate]) ? $result[$currentDate] : new $modelClass($this, $dateTimePeriodStart, $dateTimePeriodEnd);
            $modelInstance->convertData($buffer);

            if ($totalModel && !$totalModel->isUseOlyPageItem() && !$bufferFlush) {
                $totalModel->countingModel($modelInstance);
            }

            if ($current >= $start && $current < $end || ($start === false || $end === false)) {
                $result[$currentDate] = $modelInstance;
                if ($totalModel && $totalModel->isUseOlyPageItem() && !$bufferFlush) {
                    $totalModel->countingModel($modelInstance);
                }
            }

            $buffer = [];
            $bufferSize = 0;
        };

        $convertData = function ($data) use ($bufferWatch, &$buffer, &$result, &$bufferSize, &$currentDate, &$currentDateTime, &$timeFilter, $modelClass, &$totalModel, &$start, &$end, &$current) {
            $bufferFlush = $bufferSize > $this->bufferSize;

            $bufferDateTime = new DateTime();
            $bufferDateTime->setTimestamp($data[$timeFilter->column]);
            $bufferDate = $bufferDateTime->format($timeFilter->getGroupPhpDate());

            if (empty($currentDate) || empty($currentDateTime)) {
                $currentDate = $bufferDate;
                $currentDateTime = $bufferDateTime;
            }

            if ($currentDate != $bufferDate || $bufferFlush) {
                $bufferWatch($bufferFlush);

                if (!$bufferFlush) {
                    $currentDate = $bufferDate;
                    $currentDateTime = $bufferDateTime;
                }

                $current++;
            }

            $buffer[] = $data;
            $bufferSize++;
        };

        foreach ($dataIterator as $key => $item) {
            $data = [];
            foreach ($item as $itemKey => $itemValue) {
                $data[$aliases->getColumn($itemKey)] = $itemValue;
            }

            $convertData($data);
        }

        $bufferWatch();
        unset($buffer);

        return array_values($result);
    }

    /**
     * @return string
     * @throws CException
     */
    public function getDataCount()
    {
        $selector = $this->_copySelector();
        $selector->dbCriteria->group = $this->getTimeFilter()->getGroupSql();
        return (int)$selector->count();
    }

    /**
     * @param bool $filterTime
     *
     * @return UnShardedActiveRecord
     * @throws CException
     */
    protected function _copySelector($filterTime = true)
    {
        $this->_applyFilters();
        $selector = $this->getSelector()->copy();

        if ($filterTime) {
            $selector->dbCriteria->addCondition($this->getTimeFilter()->getSql());
        }

        $selector->dbCriteria->order = $this->getTimeFilter()->getSqlColumn() . ' DESC';

        return $selector;
    }
}
