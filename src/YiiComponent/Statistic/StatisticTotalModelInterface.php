<?php

namespace MommyCom\YiiComponent\Statistic;

interface StatisticTotalModelInterface
{
    /**
     * give item only visible on page
     *
     * @return bool
     */
    public function isUseOlyPageItem();

    /**
     * Сan be used many times
     *
     * @param StatisticModelInterface $model
     */
    public function countingModel(StatisticModelInterface $model);
}
