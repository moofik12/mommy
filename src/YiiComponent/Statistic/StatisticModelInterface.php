<?php

namespace MommyCom\YiiComponent\Statistic;

interface StatisticModelInterface
{
    /**
     * Сan be used many times
     *
     * @param array $data
     *
     * @return mixed
     */
    public function convertData(array $data);
}
