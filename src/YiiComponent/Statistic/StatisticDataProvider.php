<?php

namespace MommyCom\YiiComponent\Statistic;

use CDataProvider;

class StatisticDataProvider extends CDataProvider
{
    /**
     * @var Statistic
     */
    public $statistic;

    /**
     * @param Statistic $statistic
     * @param array $config
     */
    public function __construct(Statistic $statistic, $config = [])
    {
        $this->statistic = $statistic;

        foreach ($config as $key => $value) {
            $this->$key = $value;
        }

        if (($pagination = $this->getPagination()) !== false) {
            $pagination->setItemCount($this->getTotalItemCount());
        }
    }

    /**
     * Calculates the total number of data items.
     *
     * @return integer the total number of data items.
     */
    protected function calculateTotalItemCount()
    {
        return $this->statistic->getDataCount();
    }

    /**
     * Fetches the data from the persistent data storage.
     *
     * @return array list of data items
     */
    protected function fetchData()
    {
        if (($pagination = $this->getPagination()) !== false) {
            $data = $this->statistic->getData($pagination->currentPage, $pagination->pageSize);
        } else {
            $data = $this->statistic->getData(null, null);
        }

        return $data;
    }

    /**
     * Fetches the data item keys from the persistent data storage.
     *
     * @return array list of data item keys.
     */
    protected function fetchKeys()
    {
        return array_keys($this->getData());
    }
}
