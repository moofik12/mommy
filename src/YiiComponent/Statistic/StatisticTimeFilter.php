<?php

namespace MommyCom\YiiComponent\Statistic;

use CException;
use DateTime;
use InvalidArgumentException;
use MommyCom\YiiComponent\Type\Cast;

class StatisticTimeFilter
{
    const PERIOD_DAY = 'day';
    const PERIOD_WEEK = 'week';
    const PERIOD_MONTH = 'month';

    protected $_groupedSql = [
        self::PERIOD_DAY => '%d.%m.%Y',
        self::PERIOD_WEEK => '%x.%v',
        self::PERIOD_MONTH => '%m.%Y',
    ];

    protected $_groupedPhp = [
        self::PERIOD_DAY => 'd.m.Y',
        self::PERIOD_WEEK => 'W.o',
        self::PERIOD_MONTH => 'm.Y',
    ];

    protected $_startPeriod = [
        self::PERIOD_DAY => 'today',
        self::PERIOD_WEEK => 'first day of this week',
        self::PERIOD_MONTH => 'first day of this month',
    ];

    protected $_endPeriod = [
        self::PERIOD_DAY => 'tomorrow',
        self::PERIOD_WEEK => 'next monday',
        self::PERIOD_MONTH => 'first day of next month',
    ];

    public $periodName = [
        self::PERIOD_DAY => 'Day',
        self::PERIOD_WEEK => 'Week',
        self::PERIOD_MONTH => 'Month',
    ];

    /**
     * @var string
     */
    protected $_period = self::PERIOD_DAY;

    /**
     * @var int
     */
    protected $_from;

    /**
     * @var int
     */
    protected $_to;

    /**
     * @var string
     */
    public $column;

    /**
     * @param Statistic $statistic
     */
    public function __construct(Statistic $statistic)
    {
        $this->statistic = $statistic;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return $this->_period;
    }

    /**
     * @param $period
     *
     * @throws InvalidArgumentException
     */
    public function setPeriod($period)
    {
        if (!in_array($period, [
            self::PERIOD_DAY,
            self::PERIOD_WEEK,
            self::PERIOD_MONTH,
        ])) {
            throw new InvalidArgumentException("Period '$period' not support");
        }

        $this->_period = $period;
    }

    /**
     * @return int
     */
    public function getFrom()
    {
        return $this->_from;
    }

    /**
     * @param DateTime|int $timeFrom
     */
    public function setFrom($timeFrom)
    {
        if ($timeFrom instanceof DateTime) {
            $this->_from = $timeFrom->getTimestamp();
        } else {
            $this->_from = Cast::toInt($timeFrom);
        }
    }

    /**
     * @return int
     */
    public function getTo()
    {
        return $this->_to;
    }

    /**
     * @param DateTime|int $timeTo
     */
    public function setTo($timeTo)
    {
        if ($timeTo instanceof DateTime) {
            $this->_to = $timeTo->getTimestamp();
        } else {
            $this->_to = Cast::toInt($timeTo);
        }
    }

    /**
     * @return string
     */
    public function getSql()
    {
        $column = $this->getSqlColumn();

        $from = Cast::toStr($this->_from);
        $to = Cast::toStr($this->_to);

        return $column . ' > ' . $from . ' AND ' . $column . ' <= ' . $to;
    }

    /**
     * @return mixed
     */
    public function getGroupSql()
    {
        $period = $this->_groupedSql[$this->getPeriod()];
        $column = $this->getSqlColumn();

        return "FROM_UNIXTIME($column, '$period')";
    }

    /**
     * @return string
     */
    public function getGroupPhpDate()
    {
        return $this->_groupedPhp[$this->getPeriod()];
    }

    /**
     * @param string $default
     *
     * @return string
     */
    public function getPeriodName($default = '')
    {
        return is_array($this->periodName) && isset($this->periodName[$this->getPeriod()]) ? \Yii::t('common', $this->periodName[$this->getPeriod()]) : $default;
    }

    /**
     * @param  int $timestamp
     *
     * @return DateTime
     */
    public function getStartPeriodDateTime($timestamp)
    {
        $dateTime = new DateTime();
        $dateTime->setTimestamp((int)$timestamp);

        if (isset($this->_startPeriod[$this->getPeriod()])) {
            if ($this->getPeriod() == self::PERIOD_WEEK) {
                $dateTime->modify('today');

                if ($dateTime->format('D') != 'Mon') {
                    $dateTime->modify('last Monday');
                }

                return $dateTime;
            }

            $dateTime->modify($this->_startPeriod[$this->getPeriod()]);
        }

        if ($dateTime->getTimestamp() < $this->getFrom()) {
            //fucking behavior
            $dateTime->modify('now');
            $dateTime->setTimestamp($this->getFrom());
        }

        return $dateTime;
    }

    /**
     * @param int $timestamp
     *
     * @return string
     */
    public function getEndPeriodDateTime($timestamp)
    {
        $dateTime = new DateTime();
        $dateTime->setTimestamp((int)$timestamp);

        if (isset($this->_endPeriod[$this->getPeriod()])) {
            $dateTime->modify($this->_endPeriod[$this->getPeriod()]);
        }

        if ($dateTime->getTimestamp() > $this->getTo()) {
            //fucking behavior
            $dateTime->modify('now');
            $dateTime->setTimestamp($this->getTo());
        }

        return $dateTime;
    }

    /**
     * @return mixed|string
     * @throws CException
     */
    public function getSqlColumn()
    {
        $aliases = $this->statistic->getColumnAliases();
        $column = $aliases->getAlias($this->column);

        if (strpos($column, '.') === false) {
            $tableAlias = $this->statistic->getSelector()->getTableAlias();
            $column = $tableAlias . '.' . $this->column;
        }

        return $column;
    }
}
