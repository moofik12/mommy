<?php

namespace MommyCom\YiiComponent;

use CApplicationComponent;
use CDbExpression;
use CException;
use CLogger;
use CMap;
use Exception;
use MommyCom\Model\Db\EmailMessage;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_Transport;
use Yii;

/**
 * Application component for creating and sending emails.
 */
class Emailer extends CApplicationComponent
{
    // Mime content types.
    const CONTENT_PLAIN = 'text/plain';
    const CONTENT_HTML = 'text/html';

    /**
     * @var array the email template configuration (name=>config).
     */
    public $templates = [];
    /**
     * @var string the mail options for the mailer.
     * @see http://swiftmailer.org/docs/sending.html
     */
    public $mailOptions;
    /**
     * @var array the smtp options for the mailer.
     * @see http://swiftmailer.org/docs/sending.html
     * <code>
     * array(
     *     'host'=>'сервер',
     *     'port'=>'порт',
     *     'username'=>'логин',
     *     'password'=>'пароль'
     *     )
     * </code>
     */
    public $smtpOptions = [];
    /**
     * @var string the path alias for where the email views are located.
     */
    private $viewPath = 'application.views.email';
    /**
     * @var string the path to the default layout file. Setting this to false means that no layout will be used.
     */
    public $defaultLayout = false;
    /**
     * @var array global data that is passed to all email templates.
     */
    public $data = [];
    /**
     * @var string the default character set.
     */
    public $charset = 'utf8';
    /**
     * @var string the controller class to use when sending emails from the c
     */
    public $controller = 'CController';
    /**
     * @var string the logging category.
     */
    public $logCategory = 'emailer';
    /**
     * @var boolean whether the enable logging.
     */
    public $logging = true;
    /**
     * @var array the email filters that specify which email addresses are allowed to receive emails.
     */
    public $emailFilters = [];
    /**
     * @var string email address that will receive all emails sent through the emailer.
     */
    public $catchAllEmail;
    /**
     * @var boolean whether to prevent the actual sending of emails.
     */
    public $dryRun = false;

    protected $_mailer;
    protected $_failedRecipients = [];

    /**
     * Creates an email message from a template.
     *
     * @param string $name the template name.
     * @param array $config the email configuration.
     *
     * @return EmailMessage the model.
     * @throws CException if required configuration parameters are missing.
     */
    public function createFromTemplate($name, $config = [])
    {
        if (!isset($this->templates[$name])) {
            throw new CException('Email template `' . $name . '` not found.');
        }

        $config = CMap::mergeArray($this->templates[$name], $config);

        if (!isset($config['from'])) {
            throw new CException('Configuration must contain a `from` property.');
        }
        if (!isset($config['to'])) {
            throw new CException('Configuration must contain a `to` property.');
        }
        if (!isset($config['subject'])) {
            throw new CException('Configuration must contain a `subject` property.');
        }

        return $this->create($config['from'], $config['to'], $config['subject'], $config);
    }

    /**
     * Creates an email message.
     *
     * @param mixed $from the sender email address(es).
     * @param mixed $to the recipient email address(es).
     * @param string $subject the subject text.
     * @param array $config the email configuration.
     *
     * @return EmailMessage|false the model.
     * @throws CException if required configuration parameters are missing.
     */
    public function create($from, $to, $subject, $config = [], $type = EmailMessage::TYPE_SYSTEM)
    {
        $data = array_merge(isset($config['data']) ? $config['data'] : [], $this->data);
        $replacePairs = [];
        foreach ($data as $froms => $tos) {
            if (is_scalar($tos)) {
                $replacePairs['{' . $froms . '}'] = $tos;
            }
        }

        try {
            $controller = Yii::app()->getController();
            $layout = isset($config['layout']) ? $config['layout'] : $this->defaultLayout;

            $subject = strtr($subject, $replacePairs);
            if (isset($config['body'])) {
                if ($layout !== false) {
                    $originalPageTitle = $controller->pageTitle;
                    $layoutFile = $controller->getLayoutFile($layout);

                    $controller->pageTitle = $subject;
                    $content = $config['body'];
                    $body = $controller->renderFile($layoutFile, ['content' => $content], true);

                    $controller->pageTitle = $originalPageTitle;
                } else {
                    $body = $config['body'];
                }
            } else if (isset($config['view'])) {
                $view = $config['view'];
                if ($controller === null) {
                    // todo: include a console controller in this extension.
                    $controller = new $this->controller('email')/* for console */
                    ;
                }
                if (is_string($view) && $view[0] !== '.') {
                    $view = $this->viewPath . '.' . $view;
                }
                if ($layout !== false) {
                    $originalPageTitle = $controller->pageTitle;
                    $layoutFile = $controller->getLayoutFile($layout);

                    $controller->pageTitle = $subject;
                    $content = $controller->renderPartial($view, ['data' => $data], true);
                    $body = $controller->renderFile($layoutFile, ['content' => $content], true);

                    $controller->pageTitle = $originalPageTitle;
                } else {
                    $body = $controller->renderPartial($view, ['data' => $data], true);
                }
            } else {
                throw new CException('Configuration must contain either a `body` or a `view` property.');
            }
            unset($config['body'], $config['view']);

            return $this->createMessage($from, $to, $subject, $body, $config, $type);
        } catch (Exception $e) {
            if (is_array($from)) {
                $from = implode(",", $from);
            }

            if (is_array($to)) {
                $to = implode(",", $to);
            }

            Yii::log("Error create E-mail: from: $from to: $to \n" . $e, CLogger::LEVEL_ERROR, 'mailer.create');
            return false;
        }
    }

    /**
     * Sends a single email.
     *
     * @param EmailMessage $model the model instance.
     *
     * @return integer the number of recipients.
     */
    public function send(EmailMessage $model)
    {
        if ($this->logging) {
            $this->log(__CLASS__ . '.' . __FUNCTION__ . ':' . $model->to);
        }
        if (!$this->allowEmail($model->to)) {
            return -1; // not allowed to send
        }
        if ($this->dryRun) {
            return $model->getRecipientCount();
        }
        if (isset($this->catchAllEmail)) {
            $model->to = $this->catchAllEmail;
            $model->cc = null;
            $model->bcc = null;
        }

        try {
            $recipientCount = $this->getMailer()->send($model->createMessage(), $this->_failedRecipients);
            if ($recipientCount > 0) {
                $model->status = EmailMessage::STATUS_SENT;
            } else {
                $model->status = EmailMessage::STATUS_ERROR;
            }

            $model->sent_at = new CDbExpression('UNIX_TIMESTAMP()');
            $model->save(false, ['sent_at', 'status']);
            return $recipientCount;
        } catch (Exception $e) {
            $model->status = EmailMessage::STATUS_ERROR;
            $model->save(false);
            Yii::log("Error send E-mail: from: {$model->from} to: {$model->to}", CLogger::LEVEL_ERROR, 'mailer.sent');
            //throw new CException("Error send E-mail: from: {$model->from} to: {$model->to}; " . $e->getMessage());
            return false;
        }
    }

    /**
     * запуск отправки писем
     *
     * @return array
     */
    public function sendWaiting()
    {
        $errors = 0;
        $success = 0;

        $mails = EmailMessage::model()->status(EmailMessage::STATUS_WAITING)->limit(50)->findAll();
        foreach ($mails as $mail) {
            if ($this->send($mail)) {
                $success++;
            } else {
                $errors++;
            }
        }

        $count = count($mails);
        return ['count' => $count, 'success' => $success, 'errors' => $errors];
    }

    /**
     * Creates a url to view an email message in the browser.
     * Override this method to implement your own logic for rendering emails.
     *
     * @param EmailMessage $model the message model.
     *
     * @return string the url.
     */
    public function createViewUrl($model)
    {
        return Yii::app()->createUrl('/email/view', ['id' => $model->id]);
    }

    /**
     * Checks to see if the email address is allowed.
     *
     * @param string $email the email address.
     *
     * @return boolean whether the email is allowed.
     */
    protected function allowEmail($email)
    {
        if (empty($this->emailFilters)) {
            return true;
        }
        foreach ($this->emailFilters as $filter) {
            if ($filter === '*' || $filter === $email || (($pos = strpos($filter, '*')) !== false && strpos($email, substr($filter, $pos + 1)) !== false)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Logs the given email using Yii::log().
     *
     * @param string $message the message to log.
     * @param string $level the log level.
     */
    protected function log($message, $level = CLogger::LEVEL_INFO)
    {
        Yii::log($message, $level, $this->logCategory);
    }

    /**
     * Creates an email message.
     *
     * @param mixed $from the sender email address(es).
     * @param mixed $to the recipient email address(es).
     * @param string $subject the subject text.
     * @param string $body the body text.
     * @param array $config the email configuration.
     *
     * @return EmailMessage the model.
     */
    protected function createMessage($from, $to, $subject, $body, $config = [], $type = EmailMessage::TYPE_SYSTEM)
    {
        $message = new Swift_Message();

        // Determine content type and character set.
        $contentType = isset($config['contentType']) ? $config['contentType'] : self::CONTENT_HTML;
        $charset = isset($config['charset']) ? $config['charset'] : $this->charset;

        $message->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($body, $contentType, $charset);

        // Set cc and bcc if applicable.
        if (isset($config['cc'])) {
            $message->setCc($config['cc']);
        }
        if (isset($config['bcc'])) {
            $message->setBcc($config['bcc']);
        }

        return $this->createModel($message, $contentType, $charset, $type);
    }

    /**
     * Creates a model from the given Swift_Message instance and saves it.
     *
     * @param Swift_Message $message
     * @param string $contentType
     * @param string $charset
     *
     * @return EmailMessage the model.
     */
    protected function createModel(Swift_Message $message, $contentType, $charset, $type = EmailMessage::TYPE_SYSTEM)
    {
        $model = new EmailMessage;
        $model->from = $message->getFrom();
        $model->to = $message->getTo();
        $cc = $message->getCc();
        if (is_array($cc)) {
            $model->cc = implode(', ', $cc);
        }
        $bcc = $message->getBcc();
        if (is_array($bcc)) {
            $model->bcc = implode(', ', $bcc);
        }
        $model->subject = $message->getSubject();
        $model->body = $message->getBody();
        $model->headers = implode("\n", $message->getHeaders()->getAll());
        $model->contentType = $contentType;
        $model->charset = $charset;
        $model->status = EmailMessage::STATUS_WAITING;
        $model->type = $type;
        if ($model->save()) {
            // need to save the model to get its id.
            $model->body = str_replace('{viewUrl}', $this->createViewUrl($model), $model->body);
            $model->save(false, ['body']);
            $model->refresh();
        } else {
            Yii::log("Model errors create E-mail: " . print_r($model->errors, true), CLogger::LEVEL_ERROR, 'mailer.create');
            print_r($model->body);
        }

        return $model->isNewRecord ? null : $model;
    }

    /**
     * Creates the transport instance.
     *
     * @return Swift_Transport the instance.
     */
    protected function createTransport()
    {
        $transport = new Swift_SmtpTransport();

        foreach ($this->smtpOptions as $option => $value) {
            $setter = 'set' . ucfirst($option);
            $transport->{$setter}($value); // sets option with the setter method
        }

        return $transport;
    }

    /**
     * Returns the mailer instance.
     *
     * @return Swift_Mailer the instance.
     */
    private function getMailer()
    {
        if (isset($this->_mailer)) {
            return $this->_mailer;
        } else {
            $transport = $this->createTransport();
            return $this->_mailer = new Swift_Mailer($transport);
        }
    }

    /**
     * Returns a list of the failed recipients for the most recent mail.
     *
     * @return array the recipients.
     */
    public function getFailedRecipients()
    {
        return $this->_failedRecipients;
    }

    /**
     * @return string
     */
    public function getViewPath(): string
    {
        return $this->viewPath;
    }

    /**
     * @param string $viewPath
     */
    public function setViewPath(string $viewPath): void
    {
        $this->viewPath = $viewPath;
    }
}
