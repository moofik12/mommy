<?php

namespace MommyCom\YiiComponent;

use CAssetManager;
use CClientScript;
use JSMin\JSMin;
use Minify_CSS;
use Yii;

/**
 * Class ClientScript
 * Добавление возможности использовать less
 */
final class ClientScript extends CClientScript
{
    /**
     * Флаг, если истина то файлы с расширением .less будут переименованы в .css
     *
     * @var boolean
     */
    public $changeLessExtension = true;
    protected $_lessCompiler = null;

    public $minify = false;

    /**
     * @param string $cssFile
     *
     * @return string created filename
     */
    protected function _minifyCss($cssFile)
    {
        $minifiedName = md5(basename($cssFile)) . '.css';
        $basePath = dirname($cssFile);

        $minifiedFilename = $basePath . DIRECTORY_SEPARATOR . $minifiedName;

        $modifiedAt = @filemtime($cssFile);
        $compiledAt = @filemtime($minifiedFilename);

        if ($modifiedAt > $compiledAt) {
            $compiled = Minify_CSS::minify(file_get_contents($cssFile));
            file_put_contents($minifiedFilename, $compiled);
        }

        return $minifiedName;
    }

    /**
     * @param array $javascriptFiles
     * @param string $outputDir
     *
     * @return string created filename
     */
    protected function _minifyJavascript(array $javascriptFiles, $outputDir)
    {
        $javascriptContents = '';

        $minifiedName = md5(implode($javascriptFiles)) . '.js';
        $minifiedFilename = $outputDir . DIRECTORY_SEPARATOR . $minifiedName;

        $modifiedAt = max(@array_map('filemtime', $javascriptFiles));
        $compiledAt = @filemtime($minifiedFilename);

        if ($modifiedAt > $compiledAt) {
            foreach ($javascriptFiles as $javascript) {
                $javascriptContents .= file_get_contents($javascript) . "\n";
            }

            $compiled = JSMin::minify($javascriptContents);
            file_put_contents($minifiedFilename, $compiled);
        }

        return $minifiedName . '?v' . $modifiedAt;
    }

    protected function _compileCoreLess()
    {
        foreach ($this->coreScripts as $name => &$package) {
            if (!empty($package['less'])) {
                $this->getPackageBaseUrl($name); //hack to publish package
                $basePath = $this->getPackageBasePath($name);

                if ($basePath === false) {
                    continue;
                }

                $package += ['css' => []];

                foreach ($package['less'] as $key => $less) {
                    $css = explode('.', $less);
                    array_pop($css);
                    array_push($css, 'css'); // change extension
                    $css = implode('.', $css);

                    $lessFilePath = $basePath . DIRECTORY_SEPARATOR . $less;
                    $compiledFilePath = $basePath . DIRECTORY_SEPARATOR . $css;

                    if (!file_exists($compiledFilePath) || filemtime($lessFilePath) > filemtime($compiledFilePath)) {
                        $this->getLessCompiler()->compileLess($lessFilePath, $compiledFilePath);
                    }
                    $package['css'][$key] = $css;
                }

                unset($package['less']);
            }
        }
    }

    protected function _minifyCoreScripts()
    {
        if (!$this->minify) {
            return;
        }

        foreach ($this->coreScripts as $name => &$package) {
            $this->getPackageBaseUrl($name); //hack to publish package
            $basePath = $this->getPackageBasePath($name);

            if ($basePath === false) {
                continue;
            }

            if (!empty($package['js'])) { // minify javascript
                $javascripts = $package['js'];

                foreach ($javascripts as &$javascript) {
                    $javascript = $basePath . DIRECTORY_SEPARATOR . $javascript;
                }
                unset($javascript);

                $minifiedName = $this->_minifyJavascript($javascripts, $basePath);

                // override
                $package['js'] = [
                    $minifiedName,
                ];
            }

            if (!empty($package['css'])) { // minify css
                $minifiedCss = [];
                foreach ($package['css'] as $css) {
                    $originalFilename = $basePath . DIRECTORY_SEPARATOR . $css;
                    $directory = dirname($css);
                    $minifiedName = $this->_minifyCss($originalFilename);

                    $minifiedCss[] = $directory . DIRECTORY_SEPARATOR . $minifiedName;
                }

                // override
                $package['css'] = $minifiedCss;
            }
        }
    }

    public function renderCoreScripts()
    {
        if ($this->coreScripts === null) {
            return;
        }

        $this->_compileCoreLess(); //Добавляем поддержку лесс файлов

        $this->_minifyCoreScripts(); // Минифицируем скрипты

        $cssFiles = [];
        $jsFiles = [];

        foreach ($this->coreScripts as $name => $package) {
            $baseUrl = $this->getPackageBaseUrl($name);
            $basePath = $this->getPackageBasePath($name);

            if (!empty($package['js'])) {
                foreach ($package['js'] as $js) {
                    $modifiedAt = @filemtime($basePath . '/' . $js);
                    $jsFiles[$baseUrl . '/' . $js] = $baseUrl . '/' . $js . '?' . $modifiedAt;
                }
            }
            if (!empty($package['css'])) {
                foreach ($package['css'] as $css) {
                    $modifiedAt = @filemtime($basePath . '/' . $css);
                    $cssFiles[$baseUrl . '/' . $css . '?' . $modifiedAt] = '';
                }
            }
        }
        // merge in place
        if ($cssFiles !== []) {
            foreach ($this->cssFiles as $cssFile => $media)
                $cssFiles[$cssFile] = $media;
            $this->cssFiles = $cssFiles;
        }
        if ($jsFiles !== []) {
            if (isset($this->scriptFiles[$this->coreScriptPosition])) {
                foreach ($this->scriptFiles[$this->coreScriptPosition] as $url => $value) {
                    $jsFiles[$url] = $value;
                }
            }
            $this->scriptFiles[$this->coreScriptPosition] = $jsFiles;
        }
    }

    /**
     * @return LessCompiler
     */
    public function getLessCompiler()
    {
        if (is_null($this->_lessCompiler)) {
            $this->_lessCompiler = Yii::createComponent([
                'class' => LessCompiler::class,
            ]);
        }
        return $this->_lessCompiler;
    }

    /**
     * Возвращает базовый путь к опубликованному пакету
     *
     * @param string $name
     *
     * @return string|false false if unknown
     */
    public function getPackageBasePath($name)
    {
        if (!isset($this->coreScripts[$name])) {
            return false;
        }
        $package = $this->coreScripts[$name];

        $assetManager = Yii::app()->assetManager;
        /* @var $assetManager CAssetManager */

        return isset($package['basePath']) ? $assetManager->getPublishedPath(Yii::getPathOfAlias($package['basePath'])) : false;
    }

    //copy from CClientScript
    public function getPackageBaseUrl($name)
    {
        if (!isset($this->coreScripts[$name]))
            return false;
        $package = $this->coreScripts[$name];
        if (isset($package['baseUrl'])) {
            $baseUrl = $package['baseUrl'];
            if (($baseUrl === '' || $baseUrl[0] !== '/') && strpos($baseUrl, '://') === false)
                $baseUrl = Yii::app()->getRequest()->getBaseUrl() . '/' . $baseUrl;
            $baseUrl = rtrim($baseUrl, '/');
        } else if (isset($package['basePath'])) {
            $assetManager = Yii::app()->assetManager;
            /* @var $assetManager CAssetManager */

            if (isset($package['forceOverride']) && $package['forceOverride']) {
                $baseUrl = $assetManager->publish(Yii::getPathOfAlias($package['basePath']), false, -1, true);
            } else {
                $baseUrl = $assetManager->publish(Yii::getPathOfAlias($package['basePath']));
            }
        } else
            $baseUrl = $this->getCoreScriptUrl();

        return $this->coreScripts[$name]['baseUrl'] = $baseUrl;
    }
}
