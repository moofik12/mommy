<?php

namespace MommyCom\YiiComponent\Purifier;

use CHtmlPurifier;
use COutputProcessor;
use MommyCom\YiiComponent\ArrayUtils;

class StaticPagePurifier extends COutputProcessor
{
    public $options = [
        'AutoFormat.RemoveEmpty.RemoveNbsp' => true,
        'AutoFormat.RemoveEmpty.RemoveNbsp.Exceptions' => [],
        'Core.EscapeInvalidChildren' => true,
        'HTML.TidyLevel' => 'medium',
    ];

    public function __construct(array $options = [])
    {
        $this->options = ArrayUtils::merge($this->options, $options);
    }

    public function purify($content)
    {
        $purifier = new CHtmlPurifier();
        $purifier->options = $this->options;
        return $purifier->purify($content);
    }
}
