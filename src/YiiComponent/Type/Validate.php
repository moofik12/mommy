<?php

namespace MommyCom\YiiComponent\Type;

final class Validate
{
    protected static function _sanitizeRange($min, $max)
    {
        if ($max > PHP_INT_MAX || $min > $max) {
            $max = PHP_INT_MAX;
        }
        if ($min == PHP_INT_MAX) {
            $min = PHP_INT_MIN;
        }
        return [$max, $min];
    }

    public static function isInt($value, $min = PHP_INT_MIN, $max = PHP_INT_MAX)
    {
        list($min, $max) = self::_sanitizeRange($min, $max);
        return is_int($value) && $value >= $min && $value <= $max;
    }

    public static function isUInt($value, $min = 0, $max = PHP_INT_MAX)
    {
        $min = max(0, $min); //запрещение чисел меньше нуля
        return self::isInt($value, $min, $max);
    }

    public static function isStr($value, $notEmpty = false, $regexp = null)
    {
//        if ($regexp === null) {
//            return is_string($value);
//        } else {
//            return is_string($value) && preg_match_all($regexp, $value, $matchesArray) != 0;
//        }
//        return false;
        return (!$notEmpty || !empty($value)) && is_string($value) &&
            ($regexp === null || preg_match_all($regexp, $value, $matchesArray) != 0);
    }

    public static function isFloat($value, $min = PHP_INT_MIN, $max = PHP_INT_MAX)
    {
        list($min, $max) = self::_sanitizeRange($min, $max); //для флоата это не совсем подходит но лучше чем ничего
        return is_float($value) && $value >= $min && $value <= $max;
    }
}
