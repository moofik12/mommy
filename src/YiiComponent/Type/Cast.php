<?php

namespace MommyCom\YiiComponent\Type;

use MommyCom\YiiComponent\Utf8;

final class Cast
{
    /**
     * @param mixed $value
     * @param mixed $default (default: 0)
     * @param integer $min (default: PHP_INT_MIN)
     * @param integer $max (default: PHP_INT_MAX)
     *
     * @return integer
     */
    public static function toInt($value, $default = 0, $min = PHP_INT_MIN, $max = PHP_INT_MAX)
    {
        if (isset($value)) {
            $result = (int)$value;
        }

        if (!isset($result) || $result < $min || $result > $max) {
            $result = $default;
        }
        return $result;
    }

    /**
     * @param mixed $value
     * @param mixed $default (default: 0)
     * @param integer $min (default: 0)
     * @param integer $max (default: PHP_INT_MAX)
     *
     * @return integer
     */
    public static function toUInt($value, $default = 0, $min = 0, $max = PHP_INT_MAX)
    {
        return self::toInt($value, $default, $min, $max);
    }

    /**
     * @param mixed $value
     * @param mixed $default (default: 0)
     * @param float|int $min (default: PHP_INT_MIN)
     * @param float|int $max (default: PHP_INT_MAX)
     *
     * @return float
     */
    public static function toFloat($value, $default = 0, $min = PHP_INT_MIN, $max = PHP_INT_MAX)
    {
        if (isset($value)) {
            $result = (float)$value;
        }

        if (!isset($result) || $result < $min || $result > $max) {
            $result = $default;
        }
        return $result;
    }

    /**
     * @param mixed $value
     * @param mixed $default (default: 0)
     * @param float|int $min (default: 0)
     * @param float|int $max (default: PHP_INT_MAX)
     *
     * @return float
     */
    public static function toUFloat($value, $default = 0, $min = 0, $max = PHP_INT_MAX)
    {
        return self::toFloat($value, $default, $min, $max);
    }

    /**
     * @param mixed $value
     * @param mixed $default (default: '')
     *
     * @return string
     */
    public static function toStr($value, $default = '')
    {
        switch (true) {
            case is_int($value):
            case is_bool($value):
            case is_float($value):
                $result = $value == 0 ? '0' : strval($value);
                break;

            case is_scalar($value):
                $result = strval($value);
                break;

            case is_object($value) && method_exists($value, '__toString'):
                $result = strval($value->__toString());
                break;

            default:
                $result = $default;
        }
        return $result;
    }

    /**
     * @param mixed $value
     * @param mixed $default
     * @param boolean $wrapScalarTypes
     * @param mixed $defaultVal
     * @param integer $minVal
     * @param integer $maxVal
     *
     * @return integer[]|array
     */
    public static function toIntArr($value, $default = [], $wrapScalarTypes = true,
                                    $defaultVal = 0, $minVal = PHP_INT_MIN, $maxVal = PHP_INT_MAX)
    {
        $value = self::toArr($value, $default, $wrapScalarTypes);
        foreach ($value as &$item) {
            $item = self::toInt($item, $defaultVal, $minVal, $maxVal);
        }
        unset($item);
        return $value;
    }

    /**
     * @param $value
     * @param array $default
     * @param bool $wrapScalarTypes
     * @param int $defaultVal
     * @param int $minVal
     * @param int $maxVal
     *
     * @return integer[]|array
     */
    public static function toUIntArr($value, $default = [], $wrapScalarTypes = true,
                                     $defaultVal = 0, $minVal = 0, $maxVal = PHP_INT_MAX)
    {
        return self::toIntArr($value, $default, $wrapScalarTypes, $defaultVal, $minVal, $maxVal);
    }

    /**
     * @param mixed $value
     * @param mixed $default
     * @param boolean $wrapScalarTypes
     * @param mixed $defaultVal
     *
     * @return array array of string
     */
    public static function toStrArr($value, $default = [], $wrapScalarTypes = true,
                                    $defaultVal = '')
    {
        $value = self::toArr($value, $default, $wrapScalarTypes);
        foreach ($value as &$item) {
            $item = self::toStr($item, $defaultVal);
        }
        unset($item);
        return $value;
    }

    /**
     * @param $value
     * @param array $default
     * @param bool $wrapScalarTypes
     *
     * @return array
     */
    public static function toArr($value, $default = [], $wrapScalarTypes = true)
    {
        switch (true) {
            case is_array($value):
                return $value;

            case is_scalar($value) && $wrapScalarTypes:
                return [$value];

            default:
                return (array)$default;
        }
    }

    /**
     * @param mixed $value
     * @param string $default
     *
     * @return null|object
     */
    public static function toObj($value, $default = 'stdClass')
    {
        switch (true) {
            case is_object($value):
                return $value;

            case is_array($value):
                return (object)$value;

            default:
                return is_string($default) && class_exists($default, false) ? new $default : $default;
        }
    }

    /**
     * @param mixed $value
     * @param null $className
     * @param mixed $default
     * @param boolean $wrapScalarTypes
     * @param mixed $defaultVal
     *
     * @return array array of string
     */
    public static function toObjArr($value, $className = null,
                                    $default = [], $wrapScalarTypes = true, $defaultVal = '')
    {
        $value = self::toArr($value, $default, $wrapScalarTypes);
        if ($className === null) {
            foreach ($value as &$item) {
                $item = self::toObj($item, $defaultVal);
            }
        } else {
            foreach ($value as $key => &$item) {
                $item = self::toObj($item, $defaultVal);
                if (!$item instanceof $className) {
                    unset($value[$key]);
                }
            }
        }
        unset($item);
        return $value;
    }

    /**
     * @param $value
     * @param bool $default
     *
     * @return bool
     */
    public static function toBool($value, $default = false)
    {
        if (isset($value)) {
            return (bool)$value;
        }
        return $default;
    }

    /**
     * @param mixed $value
     * @param int|mixed $default
     * @param int|mixed $min
     * @param int|mixed $max
     *
     * @return float
     */
    public static function smartStringToFloat($value, $default = 0, $min = PHP_INT_MIN, $max = PHP_INT_MAX)
    {
        $value = (string)$value;
        $value = Utf8::trim($value);
        $value = str_replace(',', '.', $value);
        $value = preg_replace("/\s*/u", "", $value);
        return Cast::toFloat($value, $default, $min, $max);
    }

    /**
     * @param mixed $value
     * @param int|mixed $default
     * @param int|mixed $min
     * @param int|mixed $max
     *
     * @return float
     */
    public static function smartStringToUFloat($value, $default = 0, $min = PHP_INT_MIN, $max = PHP_INT_MAX)
    {
        $value = (string)$value;
        $value = Utf8::trim($value);
        $value = str_replace(',', '.', $value);
        $value = preg_replace("/\s*/u", "", $value);
        return Cast::toUFloat($value, $default, $min, $max);
    }
}
