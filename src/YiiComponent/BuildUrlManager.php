<?php

namespace MommyCom\YiiComponent;

use CUrlManager;
use Yii;
use MommyCom\YiiComponent\Facade\Regions;

class BuildUrlManager extends CUrlManager
{
    /**
     * @var array
     */
    public $configApp = [];

    /**
     * @var string
     */
    public $scriptName = 'index.php';

    /**
     * @var string
     */
    public $configUrlManagerName = 'urlManager';

    /**
     * @var string
     */
    public $host;

    private $_baseUrl;

    public function init()
    {
        if (is_array($this->configApp)) {
            $configSet = $this->configApp;

            if (isset($configSet['components'][$this->configUrlManagerName])) {
                $config = $configSet['components'][$this->configUrlManagerName];
                unset($config['class']);
                foreach ($config as $key => $value)
                    $this->$key = $value;
            }
        }

        if (! $this->host) {
            $this->host = Regions::getHostname(Regions::getServerRegion()->getRegionName());
        }

        parent::init();
    }

    public function getBaseUrl()
    {
        if ($this->_baseUrl === null) {
            $this->_baseUrl = Yii::app()->getRequest()->getBaseUrl();

            if ($this->host) {
                $this->_baseUrl = '';
            }

            if ($this->showScriptName) {
                $this->_baseUrl .= "/" . $this->scriptName;
            }
        }

        return $this->_baseUrl;
    }

    public function createAbsoluteUrl($route, $params = [], $schema = '', $ampersand = '&')
    {
        $url = $this->createUrl($route, $params, $ampersand);

        if (strpos($url, 'http') !== 0) {
            if ($this->host) {
                $url = $this->host . $url;

                if (strpos($url, 'http') !== 0) {
                    $url = 'http://' . $url;
                }
            } else {
                $url = Yii::app()->getRequest()->getHostInfo($schema) . $url;
            }
        }

        return $url;
    }
}
