<?php

namespace MommyCom\YiiComponent\Reporting;

use CModel;
use DateTime;

/**
 * Class ActiveReportModel
 */
class ActiveReportModel extends CModel
{
    /**
     * @var ActiveReportColumn[]
     */
    protected $_columns = [];

    /**
     * @var array
     */
    protected $_attributeValues = [];

    /**
     * @param ActiveReportColumn[] $columns
     */
    public function __construct(array $columns)
    {
        foreach ($columns as $column) {
            $this->_columns[$column->name] = $column;
        }
    }

    public function attributeNames()
    {
        return array_keys($this->_columns);
    }

    public function __set($key, $value)
    {
        if (array_key_exists($key, $this->_columns)) {
            $this->_attributeValues[$key] = $value;
            return;
        }
        parent::__set($key, $value);
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->_columns)) {
            return isset($this->_attributeValues[$key]) ? $this->_attributeValues[$key] : null;
        }
        return parent::__get($key);
    }

    public function __isset($key)
    {
        if (array_key_exists($key, $this->_columns)) {
            return true;
        }
        return parent::__isset($key);
    }

    /**
     * @param string $name attribute name
     * @param bool $forceString
     *
     * @return DateTime|float|int|string
     */
    public function getFormatted($name, $forceString = false)
    {
        return $this->_columns[$name]->format($this->$name, $forceString);
    }
}
