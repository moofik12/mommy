<?php

namespace MommyCom\YiiComponent\Reporting;

use CComponent;
use MommyCom\YiiComponent\Type\Cast;

class ActiveReportTimeFilter extends CComponent
{
    public $column;
    public $from;
    public $to;

    /**
     * @var ActiveReport
     */
    public $report;

    /**
     * @param $useRenamedColumns
     *
     * @return string
     */
    public function getSql($useRenamedColumns)
    {
        if ($this->column === null || $this->from === null || $this->to === null) {
            return '';
        }

        $aliases = $this->report->getRawColumnAliases();
        $column = $this->column;
        if ($useRenamedColumns) {
            $column = $aliases->getAlias($this->column);
        } elseif (strpos($column, '.') === false) {
            $tableAlias = $this->report->selector->getTableAlias();
            $column = $tableAlias . '.' . $this->column;
        }

        $from = !is_numeric($this->from) ? strtotime(Cast::toStr($this->from)) : $this->from;
        $to = !is_numeric($this->to) ? strtotime(Cast::toStr($this->to)) : $this->to;

        $from = Cast::toStr($from);
        $to = Cast::toStr($to);

        return $column . ' > ' . $from . ' AND ' . $column . ' <= ' . $to;
    }
} 
