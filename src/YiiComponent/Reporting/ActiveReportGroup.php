<?php

namespace MommyCom\YiiComponent\Reporting;

use CComponent;

/**
 * Class ActiveReportGroup
 */
class ActiveReportGroup extends CComponent
{
    /**
     * Заголовок группировки
     *
     * @var string
     */
    public $label;

    /**
     * Название группировки
     *
     * @var string
     */
    public $name;

    /**
     * Sql Выражение для группировки
     *
     * @var string
     */
    public $expression;

    /**
     * Sql выражение для получения группы по условию группы (например группы за определенную дату)
     * используется для получения дочерних элементов
     *
     * @var string
     */
    public $filterExpression;

    /**
     * Название переменной в которую будет идти подстановка значения для фильтрации
     *
     * @var string
     */
    public $filterExpressionQueryNamespace = 'filter';

    /**
     * @var ActiveReport
     */
    public $report;

    /**
     * @param string $expression
     *
     * @return string[] поля которые учавствуют в expression
     */
    protected function getColumns($expression)
    {
        preg_match_all('/{(.*?)}/im', $expression, $matches);

        if (is_array($matches) && is_array($matches[1])) {
            return array_unique($matches[1]);
        }
        return [];
    }

    /**
     * @param boolean $useRenamedColumns
     *
     * @return string
     */
    public function getGroupSql($useRenamedColumns)
    {
        $expression = $this->expression;
        $columns = $this->getColumns($expression);

        $aliases = $this->report->getRawColumnAliases();
        foreach ($columns as $column) {
            if ($useRenamedColumns) {
                $alias = $aliases->getAlias($column);
            } else {
                $alias = $column;
            }
            $expression = str_replace('{' . $column . '}', $alias, $expression);
        }

        return $expression;
    }

    /**
     * @param boolean $useRenamedColumns
     * @param array $row
     *
     * @return string
     */
    public function getFilterSql($useRenamedColumns, $row)
    {
        $expression = $this->filterExpression;

        $queryNamespace = $this->filterExpressionQueryNamespace . '.';
        $queryNamespaceLength = strlen($queryNamespace);
        $columns = $this->getColumns($expression);
        $filterColumns = [];

        foreach ($columns as $index => $value) {
            if (strpos($value, $queryNamespace) === 0) {
                $filterColumns[] = substr($value, $queryNamespaceLength);
                unset($columns[$index]);
            }
        }

        $aliases = $this->report->getRawColumnAliases();

        foreach ($columns as $column) {
            if ($useRenamedColumns) {
                $alias = $aliases->getAlias($column);
            } else {
                $alias = $column;
                if (strpos($alias, '.') === false) {
                    $alias = 't.' . $alias;
                }
            }
            $expression = str_replace('{' . $column . '}', $alias, $expression);
        }

        foreach ($filterColumns as $column) {
            $alias = $aliases->getAlias($column);
            $expression = str_replace('{' . $queryNamespace . $column . '}', $row[$alias], $expression);
        }

        return $expression;
    }
} 
