<?php

namespace MommyCom\YiiComponent\Reporting;

use CDataProvider;
use DateTime;
use Yii;

class ActiveReportDataProvider extends CDataProvider
{
    /**
     * @var ActiveReport
     */
    public $report;

    /**
     * @var bool
     */
    public $caseSensitiveSort = true;

    /**
     * @param ActiveReport $activeReport
     * @param array $config
     */
    public function __construct(ActiveReport $activeReport, $config = [])
    {
        $this->report = $activeReport;

        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Fetches the data from the persistent data storage.
     *
     * @return array list of data items
     */
    protected function fetchData()
    {
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->setItemCount($this->getTotalItemCount());
            $data = $this->report->getData($pagination->currentPage, $pagination->pageSize);
        } else {
            $data = $this->report->getData(null, null);
        }

        if (($sort = $this->getSort()) !== false && ($order = $sort->getOrderBy()) != '') {
            $this->_sortData($data, $this->_getSortDirections($order));
        }
        return $data;
    }

    public function getSort($className = 'CSort')
    {
        $sort = parent::getSort($className);
        if ($sort !== null && $sort->attributes === []) {
            foreach ($this->getColumns() as $column) {
                $sort->attributes[] = $column->name;
            }
        }

        return $sort;
    }

    /**
     * Fetches the data item keys from the persistent data storage.
     *
     * @return array list of data item keys.
     */
    protected function fetchKeys()
    {
        return array_keys($this->getData());
    }

    /**
     * Calculates the total number of data items.
     *
     * @return integer the total number of data items.
     */
    protected function calculateTotalItemCount()
    {
        return $this->report->getDataCount();
    }

    /**
     * @return ActiveReportColumn[]
     */
    public function getColumns()
    {
        $columns = $this->report->getReportColumns();
        $result = [];
        foreach ($columns as $column) {
            $result[$column->name] = $column;
        }
        return $result;
    }

    /**
     * Sorts the raw data according to the specified sorting instructions.
     * After calling this method, {@link rawData} will be modified.
     *
     * @param array $rawData
     * @param array $directions the sorting directions (field name => whether it is descending sort)
     */
    protected function _sortData(array &$rawData, $directions)
    {
        if (empty($directions)) {
            return;
        }

        $args = [];
        $dummy = [];
        foreach ($directions as $name => $descending) {
            $column = [];

            $fields_array = preg_split('/\.+/', $name, -1, PREG_SPLIT_NO_EMPTY);
            foreach ($rawData as $index => $data) {
                $column[$index] = $this->_getSortingFieldValue($data, $fields_array);
            }

            $args[] = &$column;
            $dummy[] = &$column;
            unset($column);

            $direction = $descending ? SORT_DESC : SORT_ASC;
            $args[] = &$direction;
            $dummy[] = &$direction;
            unset($direction);
        }

        $args[] = &$rawData;

        $args[] = SORT_NATURAL;

        call_user_func_array('array_multisort', $args);
    }

    /**
     * Get field for sorting, using dot like delimiter in query.
     *
     * @param mixed $data array or object
     * @param array $fields sorting fields in $data
     *
     * @return mixed $data sorting field value
     */
    protected function _getSortingFieldValue($data, $fields)
    {
        if (is_object($data)) {
            if ($data instanceof ActiveReportModel) {
                foreach ($fields as $field) {
                    $data = isset($data->$field) ? $data->getFormatted($field) : null;
                    if ($data instanceof DateTime) {
                        $data = $data->getTimestamp();
                    }
                }
            } else {
                foreach ($fields as $field) {
                    $data = isset($data->$field) ? $data->$field : null;
                }
            }
        } else {
            foreach ($fields as $field) {
                $data = isset($data[$field]) ? $data[$field] : null;
            }
        }
        return $this->caseSensitiveSort ? $data : mb_strtolower($data, Yii::app()->charset);
    }

    /**
     * Converts the "ORDER BY" clause into an array representing the sorting directions.
     *
     * @param string $order the "ORDER BY" clause.
     *
     * @return array the sorting directions (field name => whether it is descending sort)
     */
    protected function _getSortDirections($order)
    {
        $segs = explode(',', $order);
        $directions = [];
        foreach ($segs as $seg) {
            if (preg_match('/(.*?)(\s+(desc|asc))?$/i', trim($seg), $matches)) {
                $directions[$matches[1]] = isset($matches[3]) && !strcasecmp($matches[3], 'desc');
            } else {
                $directions[trim($seg)] = false;
            }
        }
        return $directions;
    }
}
