<?php

namespace MommyCom\YiiComponent\Reporting;

use CComponent;

class ActiveRawColumnAliases extends CComponent
{
    /**
     * @var string[]
     */
    public $aliases;

    /**
     * @var ActiveReport
     */
    public $report;

    /**
     * @param string $name
     *
     * @return string
     */
    public function getAlias($name)
    {
        return isset($this->aliases[$name]) ? $this->aliases[$name] : $name;
    }
} 
