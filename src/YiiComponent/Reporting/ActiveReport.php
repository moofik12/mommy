<?php

namespace MommyCom\YiiComponent\Reporting;

use CComponent;
use CSqlDataProvider;
use MommyCom\YiiComponent\Database\UnShardedActiveRecord;
use Yii;

class ActiveReport extends CComponent
{
    /**
     * @var UnShardedActiveRecord
     */
    public $selector;

    public $cache = 0;

    public $with = [];

    public $timeFilter = [];

    public $groups = [];

    public $customGroups = [];

    public $columns = [];

    public $selectedGroup = 'days';

    public $selectedCustomGroups = ['admins'];

    /**
     * @var ActiveRawColumnAliases
     */
    protected $_rawColumnAliases;

    /**
     * @param bool $onlyVisible
     *
     * @return ActiveReportColumn[]
     */
    public function getReportColumns($onlyVisible = true)
    {
        $result = [];

        foreach ($this->columns as $column) {
            $config = [
                'class' => ActiveReportColumn::class,
                'report' => $this,
            ] + $column;

            $column = Yii::createComponent($config);
            /* @var $column ActiveReportColumn */

            if (!$onlyVisible || $column->getIsVisible()) {
                $result[] = $column;
            }
        }
        return $result;
    }

    /**
     * @return ActiveReportGroup[]
     */
    private function getReportGroups()
    {
        $result = [];
        foreach ($this->groups as $group) {
            $config = [
                'class' => ActiveReportGroup::class,
                'report' => $this,
            ] + $group;

            $result[] = Yii::createComponent($config);
        }
        return $result;
    }

    /**
     * @return ActiveReportGroup|false
     */
    public function getSelectedReportGroup()
    {
        $groups = $this->getReportGroups();
        foreach ($groups as $group) {
            if ($group->name == $this->selectedGroup) {
                return $group;
            }
        }
        return reset($groups);
    }

    /**
     * @return ActiveReportGroup[]
     */
    public function getReportCustomGroups()
    {
        $result = [];
        foreach ($this->customGroups as $group) {
            $config = [
                'class' => ActiveReportGroup::class,
                'report' => $this,
            ] + $group;

            $result[] = Yii::createComponent($config);
        }
        return $result;
    }

    /**
     * @return ActiveReportGroup[]
     */
    private function getSelectedReportCustomGroups()
    {
        $selected = [];
        $groups = $this->getReportCustomGroups();
        foreach ($groups as $group) {
            if (in_array($group->name, $this->selectedCustomGroups)) {
                $selected[] = $group;
            }
        }
        return $selected;
    }

    /**
     * @return ActiveRawColumnAliases
     */
    public function getRawColumnAliases()
    {
        if ($this->_rawColumnAliases === null) {
            $selector = $this->selector->copy();
            $selector->with($this->with);
            $renamedColumns = [];
            $selector->getSqlCommand($renamedColumns);

            $config = [
                'class' => ActiveRawColumnAliases::class,
                'aliases' => $renamedColumns,
                'report' => $this,
            ];
            $this->_rawColumnAliases = Yii::createComponent($config);
        }
        return $this->_rawColumnAliases;
    }

    /**
     * @return ActiveReportTimeFilter
     */
    private function getTimeFilter()
    {
        $config = [
            'class' => ActiveReportTimeFilter::class,
            'report' => $this,
        ] + $this->timeFilter;

        return Yii::createComponent($config);
    }

    /**
     * @param integer $page
     * @param integer $pageSize
     *
     * @return ActiveReportModel[]
     */
    public function getData($page = null, $pageSize = null)
    {
        $selector = $this->selector->copy();
        $selector->with($this->with);

        $db = $selector->dbConnection;

        $timeFilter = $this->getTimeFilter(); // create timefilter

        // here apply groups

        $providerConfig = [
            'pagination' => false,
        ];

        $timeFilterAdded = false;
        if (stripos($selector->dbCriteria->condition, ' OR ') === false) {
            $timeFilterAdded = true;
            $selector->dbCriteria->addCondition($timeFilter->getSql(false));
        }

        $selectCommand = $selector->getSqlCommand();

        if ($page !== null && $pageSize !== null && $selectCommand->getLimit() == 0 && $selectCommand->getOffset() == 0) {
            $selectCommand->limit($pageSize, $page * $pageSize);
        }

        $command = $db->createCommand(); // wrap
        $command->from = '(' . $selectCommand->text . ')' . ' AS reportTemp';
        $command->params = $selectCommand->params;

        if ($page !== null && $pageSize !== null) {
            $command->limit($pageSize, $page * $pageSize);
        }

        $sqlProvider = new CSqlDataProvider($command, $providerConfig);

        // add raw columns
        $columnAliases = $this->getRawColumnAliases();
        $command->select = implode(', ', $columnAliases->aliases);

        // create report selector
        $reportSelector = $selector->copy();

        // filter by time
        if (!$timeFilterAdded) {
            $sqlProvider->sql->andWhere($timeFilter->getSql(!empty($selector->dbCriteria->with)));

            $reportSelector->dbCriteria->mergeWith([
                'condition' => $timeFilter->getSql(false),
            ]);
        }

        // apply groups
        $reportGroup = $this->getSelectedReportGroup();
        $sqlProvider->sql->group($reportGroup->getGroupSql(!empty($selector->dbCriteria->with)));

        $reportSelector->dbCriteria->mergeWith([
            'group' => $reportGroup->getGroupSql(false),
        ]);

        //apply custom groups
        $reportCustomGroups = $this->getSelectedReportCustomGroups();
        foreach ($reportCustomGroups as $reportCustomGroup) {
            $groupSql = $reportCustomGroup->getGroupSql(!empty($selector->dbCriteria->with));
            if (empty($sqlProvider->sql->group)) {
                $sqlProvider->sql->group($groupSql);
            } else {
                $sqlProvider->sql->group($sqlProvider->sql->group . ', ' . $groupSql);
            }

            $reportSelector->dbCriteria->mergeWith([
                'group' => $reportCustomGroup->getGroupSql(false),
            ]);
        }

        // apply columns
        $reportColumns = $this->getReportColumns();
        foreach ($reportColumns as $column) {
            $column->applyCurrentSql($sqlProvider, $reportSelector);
        }

        $sqlProvider->setTotalItemCount($this->getDataCount());
        $data = $sqlProvider->getData();

        foreach ($data as &$row) {
            // apply expression
            foreach ($reportColumns as $column) {
                $rawName = $column->getRawName();
                if (!isset($row[$rawName])) {
                    $row[$rawName] = null;
                }

                $column->applyCurrentExpression($row[$rawName], $row, $reportSelector);
            }

            // apply children sql and expression
            // для каждого получения дочерних элементов нужно чутка подправить фильтр
            $childrenCommand = $db->createCommand(); // wrap
            $childrenCommand->from = '(' . $selectCommand->text . ')' . ' AS reportTemp';
            $childrenCommand->params = $selectCommand->params;

            $childrenProvider = new CSqlDataProvider($childrenCommand, [
                'pagination' => false,
            ]);

            $childrenSelector = $selector->copy();

            $childrenCommand->andWhere($reportGroup->getFilterSql(!empty($reportSelector->dbCriteria->with), $row));

            $childrenSelector->dbCriteria->mergeWith([
                'condition' => $reportGroup->getFilterSql(false, $row),
            ]);

            foreach ($reportCustomGroups as $reportCustomGroup) {
                $childrenCommand->andWhere($reportCustomGroup->getFilterSql(!empty($reportSelector->dbCriteria->with), $row));
                $childrenSelector->dbCriteria->mergeWith([
                    'condition' => $reportCustomGroup->getFilterSql(false, $row),
                ]);
            }

            foreach ($reportColumns as $column) {
                $rawName = $column->getRawName();
                $column->applyChildrenSql($row[$rawName], $row, $selector, $childrenProvider, $childrenSelector);
                $column->applyChildrenExpression($row[$rawName], $row, $selector, $childrenSelector->getDataProvider(), $childrenSelector);
            }
        }
        unset($row);

        // modify data
        $reportColumns = $this->getReportColumns();
        $columnNames = [];
        foreach ($reportColumns as $column) {
            $columnNames[$column->getRawName()] = $column->name;
        }

        $result = [];
        foreach ($data as $row) {
            $model = new ActiveReportModel($reportColumns);
            foreach ($columnNames as $rawName => $name) {
                $model->$name = $row[$rawName];
            }
            $result[] = $model;
        }

        return $result;
    }

    /**
     * @return integer
     */
    public function getDataCount()
    {
        $selector = $this->selector->copy();
        $selector->with($this->with);

        $timeFilter = $this->getTimeFilter();

        $db = $selector->dbConnection;

        // here apply groups

        $timeFilterAdded = false;
        if (stripos($selector->dbCriteria->condition, ' OR ') === false) {
            $timeFilterAdded = true;
            $selector->dbCriteria->addCondition($timeFilter->getSql(false));
        }

        $selectCommand = $selector->getSqlCommand();

        $command = $db->createCommand(); // wrap
        $command->from = '(' . $selectCommand->text . ')' . ' AS reportTemp';
        $command->params = $selectCommand->params;

        $sqlProvider = new CSqlDataProvider($command);

        // filter by time
        if (!$timeFilterAdded) {
            $sqlProvider->sql->andWhere($timeFilter->getSql(!empty($selector->dbCriteria->with)));
        }

        // apply groups
        $reportGroup = $this->getSelectedReportGroup();
        $sqlProvider->sql->group($reportGroup->getGroupSql(!empty($selector->dbCriteria->with)));

        //apply custom groups
        $reportCustomGroups = $this->getSelectedReportCustomGroups();
        foreach ($reportCustomGroups as $reportCustomGroup) {
            $groupSql = $reportCustomGroup->getGroupSql(!empty($selector->dbCriteria->with));
            if (empty($sqlProvider->sql->group)) {
                $sqlProvider->sql->group($groupSql);
            } else {
                $sqlProvider->sql->group($sqlProvider->sql->group . ', ' . $groupSql);
            }
        }

        // sql provider does not support totalCount
        $countCommand = $db->createCommand(); // wrap
        $countCommand->select = 'COUNT(*)';
        $countCommand->from = '(' . $sqlProvider->sql->text . ')' . ' AS reportCount';
        $countCommand->params = $sqlProvider->sql->params;
        return (int)$countCommand->queryScalar();
    }

    /**
     * @param array $config
     *
     * @return  ActiveReportDataProvider
     */
    public function getDataProvider($config = [])
    {
        return new ActiveReportDataProvider($this, $config);
    }
}
