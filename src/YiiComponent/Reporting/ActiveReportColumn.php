<?php

namespace MommyCom\YiiComponent\Reporting;

use CActiveDataProvider;
use CComponent;
use CDbCommand;
use CException;
use CSqlDataProvider;
use DateTime;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class ActiveReportColumn
 */
class ActiveReportColumn extends CComponent
{
    const VALUE_LIST_TYPE_CURRENT = 'current';
    const VALUE_LIST_TYPE_CHILDREN = 'children';

    const VALUE_TYPE_SQL = 'sql';
    const VALUE_TYPE_EXPRESSION = 'expression';

    /**
     * Заголовок
     *
     * @var string
     */
    public $label;

    /**
     * Внутренее название
     *
     * @var string
     */
    public $name;

    /**
     * Тип источника данных (из текущего списка либо из дочерних элементов)
     *
     * @var string
     */
    public $valueListType = self::VALUE_LIST_TYPE_CURRENT;

    /**
     * Тип формирования значения (sql запрос либо пхп)
     *
     * @var string
     */
    public $valueType = self::VALUE_TYPE_SQL;

    /**
     * Выражение для получения значения
     *
     * @var string|callback
     */
    public $value;

    /**
     * @var ActiveReport
     */
    public $report;

    /**
     * При какой активной группировке эта колонка попадет в провайдер
     *
     * @var string
     */
    public $visibleOn;

    /**
     * @var bool
     */
    public $visible = true;

    /**
     * supported date, datetime, integer, float, currency, string
     *
     * @var string
     */
    public $format = '';

    public function init()
    {
        if ($this->label === null) {
            $this->label = $this->name;
        }
        if ($this->value === null && $this->valueType == self::VALUE_TYPE_SQL && $this->valueListType == self::VALUE_LIST_TYPE_CURRENT) {
            $this->value = $this->name;
        } else {
            throw new CException('please specify column value');
        }
    }

    /**
     * @return bool
     */
    public function getIsVisible()
    {
        if ($this->visibleOn === null) {
            return true;
        }

        $selectedGroup = $this->report->getSelectedReportGroup();
        if ($selectedGroup !== false && $this->visibleOn == $this->report->getSelectedReportGroup()->name) {
            return true;
        }

        if (in_array($this->visibleOn, $this->report->selectedCustomGroups)) {
            return true;
        }

        return false;
    }

    public function getRawName()
    {
        return 'activeReport_' . $this->name;
    }

    /**
     * Запуск: Выполняется до выборки
     *
     * @param CSqlDataProvider $provider
     * @param \MommyCom\YiiComponent\Database\UnShardedActiveRecord $currentSelector
     */
    public function applyCurrentSql($provider, $currentSelector)
    {
        if ($this->valueType != self::VALUE_TYPE_SQL) {
            return;
        }
        if ($this->valueListType != self::VALUE_LIST_TYPE_CURRENT) {
            return;
        }

        $command = $provider->sql;
        /* @var $command CDbCommand */

        $selectExpression = $this->value;

        preg_match_all('/{(.*?)}/im', $selectExpression, $matches);

        if (is_array($matches) && is_array($matches[1])) {
            $aliases = $this->report->getRawColumnAliases();
            foreach (array_unique($matches[1]) as $selectName) {
                $alias = $aliases->getAlias($selectName);
                $selectExpression = str_replace('{' . $selectName . '}', $alias, $selectExpression);
            }
        }

        $columnSelector = '(' . $selectExpression . ')' . ' AS ' . $this->getRawName();

        $command->setSelect($command->select . ', ' . $columnSelector);
    }

    /**
     * Запуск: После выборки
     *
     * @param mixed $item
     * @param array $row
     * @param \MommyCom\YiiComponent\Database\UnShardedActiveRecord $currentSelector
     */
    public function applyCurrentExpression(&$item, $row, $currentSelector)
    {
        if ($this->valueType != self::VALUE_TYPE_EXPRESSION) {
            return;
        }
        if ($this->valueListType != self::VALUE_LIST_TYPE_CURRENT) {
            return;
        }

        $item = $this->evaluateExpression($this->value, [
            'item' => $item,
            'row' => $row,
            'currentSelector' => $currentSelector->copy(),
        ]);
    }

    /**
     * Запуск: После выборки
     *
     * @param mixed $item
     * @param array $row
     * @param \MommyCom\YiiComponent\Database\UnShardedActiveRecord $currentSelector
     * @param CSqlDataProvider $childrenProvider
     * @param \MommyCom\YiiComponent\Database\UnShardedActiveRecord $childrenSelector
     */
    public function applyChildrenSql(&$item, $row, $currentSelector, $childrenProvider, $childrenSelector)
    {
        if ($this->valueType != self::VALUE_TYPE_SQL) {
            return;
        }
        if ($this->valueListType != self::VALUE_LIST_TYPE_CHILDREN) {
            return;
        }

        $db = $childrenSelector->dbConnection;
        $command = $db->createCommand(); // wrap
        $command->from = '(' . $childrenProvider->sql->text . ')' . ' AS reportChildrenTemp';
        $command->params = $childrenProvider->sql->params;

        $selectExpression = $this->value;

        preg_match_all('/{(.*?)}/im', $selectExpression, $matches);

        if (is_array($matches) && is_array($matches[1])) {
            $aliases = $this->report->getRawColumnAliases();
            foreach (array_unique($matches[1]) as $selectName) {
                $alias = $aliases->getAlias($selectName);
                $selectExpression = str_replace('{' . $selectName . '}', $alias, $selectExpression);
            }
        }

        $columnSelector = '(' . $selectExpression . ')' . ' AS ' . $this->getRawName();

        $command->setSelect($columnSelector);

        $item = $command->queryScalar();
    }

    /**
     * Запуск: После выборки
     *
     * @param mixed $item
     * @param array $row
     * @param \MommyCom\YiiComponent\Database\UnShardedActiveRecord $currentSelector
     * @param CActiveDataProvider $childrenProvider
     * @param \MommyCom\YiiComponent\Database\UnShardedActiveRecord $childrenSelector
     */
    public function applyChildrenExpression(&$item, $row, $currentSelector, $childrenProvider, $childrenSelector)
    {
        if ($this->valueType != self::VALUE_TYPE_EXPRESSION) {
            return;
        }
        if ($this->valueListType != self::VALUE_LIST_TYPE_CHILDREN) {
            return;
        }

        $item = $this->evaluateExpression($this->value, [
            'item' => $item,
            'row' => $row,
            'currentSelector' => $currentSelector->copy(),
            'childrenProvider' => $childrenProvider,
            'childrenSelector' => $childrenSelector->copy(),
        ]);
    }

    /**
     * @param $item
     * @param boolean $forceString
     *
     * @return \DateTime|float|int|string
     */
    public function format($item, $forceString = false)
    {
        if ($this->format == '') {
            return $item;
        }

        switch ($this->format) {
            case 'datetime':
                if (is_numeric($item)) {
                    $item = '@' . $item;
                }
                $item = new DateTime($item);
                break;
            case 'date':
                if (is_numeric($item)) {
                    $item = '@' . $item;
                }
                $item = new DateTime($item);
                $item = $item->format('d.m.Y 00:00');
                $item = new DateTime($item);
                break;
            case 'integer':
                $item = Cast::toInt($item);
                break;
            case 'float':
                $item = Cast::smartStringToFloat($item);
                break;
            case 'currency':
                $item = Cast::smartStringToFloat($item);
                break;
            case 'string':
                $item = Cast::toStr($item);
                break;
        }

        if ($forceString) {
            switch ($this->format) {
                case 'datetime':
                    $item = $item->format('d.m.Y H:i');
                    break;
                case 'date':
                    $item = $item->format('d.m.Y');
                    break;
                case 'float':
                case 'integer':
                case 'currency':
                    $item = Cast::toStr($item);
                    break;
            }
        }

        return $item;
    }
} 
