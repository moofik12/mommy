<?php

namespace MommyCom\YiiComponent\FileStorage;

interface PreModifierInterface
{
    /**
     * @param string $file
     * @param array options
     */
    public function __construct($file, array $options);

    /**
     * @return boolean
     */
    public function apply();
}
