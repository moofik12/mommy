<?php

namespace MommyCom\YiiComponent\FileStorage\File\Image;

use MommyCom\YiiComponent\FileStorage\File\Image;

class Bmp extends Image
{
    public function validateContent()
    {
        if (parent::validateContent()) {
            $correct = false;
            $this->workWithFile(function ($filename) use (&$correct) {
                list($width, $height, $type) = getimagesize($filename);
                $correct = $width > 0 && $height > 0 && $type == IMAGETYPE_BMP;
                return false;
            });
            return $correct;
        }
        return false;
    }
}
