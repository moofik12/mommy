<?php

namespace MommyCom\YiiComponent\FileStorage\File\Image;

use MommyCom\YiiComponent\FileStorage\File\Image;

class Gif extends Image
{
    public function validateContent()
    {
        if (parent::validateContent()) {
            $correct = false;
            $this->workWithFile(function ($filename) use (&$correct) {
                list($width, $height, $type) = getimagesize($filename);
                $correct = $width > 0 && $height > 0 && $type == IMAGETYPE_GIF;
                return false;
            });
            return $correct;
        }
        return false;
    }
}
