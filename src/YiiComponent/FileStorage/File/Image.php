<?php

namespace MommyCom\YiiComponent\FileStorage\File;

use Exception;
use Imagick;
use ImagickException;
use MommyCom\YiiComponent\FileStorage\Exception\NotImplementedException;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\Modifiers\Image\MakeThumbs;

abstract class Image extends File
{
    private $_imageInformation;

    /**
     * Возвращает путь к тумбе по ее имени, <b>внимание: проверки на существование тумбы НЕТ</b>
     *
     * @param string $name Имя тумбы
     *
     * @return Image тумба
     */
    public function getThumb($name)
    {
        $this->_moveToStorage();

        $thumbId = MakeThumbs::getThumbId($this->getStorage(), $this->getId(), $name);
        return File::factory($this->_storage, $thumbId);
    }

    /**
     * Поворачивает изображение на указанный угол
     *
     * @param integer $angle
     */
    public function rotateImage($angle)
    {
        throw new NotImplementedException;

        $angle = intval($angle);
        $angle = $angle % 360 * $angle > 0 ? 1 : -1;
        if ($angle != 0) {
        }
    }

    /**
     * array(properties => *getImageProperties*, geometry => *getImageGeometry*)
     *
     * @return array
     */
    protected function _getImageInformation()
    {
        if (empty($this->_imageInformation)) {
            $imgInfo = null;
            $imgGeometry = ['width' => null, 'height' => null];
            $imgCount = 0;
            $this->workWithFile(function ($filename) use (&$imgInfo, &$imgGeometry, &$imgCount) {
                $imagick = new Imagick($filename);
                try {
                    $imgInfo = $imagick->getImageProperties('*');
                    $imgGeometry = $imagick->getImageGeometry();
                    $imgCount = $imagick->getNumberImages();
                } catch (ImagickException $e) {
                } // подавляем возможную ошибку
                unset($imagick);
                return false;
            });

            $this->_imageInformation = [
                'props' => $imgInfo,
                'geometry' => $imgGeometry,
                'numberImages' => $imgCount,
            ];
        }
        return $this->_imageInformation;
    }

    /**
     * Содержит ли изображение анимацию
     *
     * @return boolean
     */
    public function hasImageAnimation()
    {
        return $this->getImageFrameCount() > 1;
    }

    /**
     * Возвращает количество фреймов в изображении (если не анимированное то вернет 1)
     *
     * @return integer
     */
    public function getImageFrameCount()
    {
        $information = $this->_getImageInformation();
        return $information['numberImages'];
    }

    /**
     * Возвращает размер изображения
     *
     * @return array array('width' => *integer*, 'height' => *integer*)
     */
    public function getImageGeometry()
    {
        $information = $this->_getImageInformation();
        return $information['geometry'];
    }

    /**
     * Возвращает exif информацию о изображении
     *
     * @return array
     */
    public function getImageExif()
    {
        $information = $this->_getImageInformation();
        return $information['props']['exif'];
    }

    /**
     * Возвращает информацию предоставляемую Imagick::getImageProperties
     *
     * @return array
     */
    public function getImageProperties()
    {
        $information = $this->_getImageInformation();
        return $information['props'];
    }

    public function validateContent()
    {
        $correct = true;
        $this->workWithFile(function ($filename) use (&$correct) {
            try {
                $image = new Imagick($filename);

                //у некоротых изображений есть проблемы с внутренней структорой
                //в итоге они поддаются просмотру но не поддаются редактированию
                if (!$image->cropImage(1, 1, 0, 0)) {
                    $correct = false;
                } elseif (strlen($image->getImageBlob()) == 0) {
                    $correct = false;
                }
            } catch (Exception $e) {
                $correct = false;
            }
            return false;
        });
        return $correct;
    }
}
