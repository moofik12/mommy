<?php

namespace MommyCom\YiiComponent\FileStorage\File;

use MommyCom\YiiComponent\FileStorage\File;

class Unknown extends File
{
    public function validateContent()
    {
        return true;
    }
}
