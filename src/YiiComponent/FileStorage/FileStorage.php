<?php

namespace MommyCom\YiiComponent\FileStorage;

use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\SimpleEventListener\Event;
use MommyCom\YiiComponent\SimpleEventListener\SimpleEventListener;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Class FileStorage
 *
 * @property-read Event $eventAddFile callback(string $eventName, string $filename)
 * @property-read Event $eventBeforeAddFile callback(string $eventName, string $filename)
 * @property-read Event $eventRemoveFile callback(string $eventName, string $id)
 * @property-read Event $eventBeforeRemoveFile callback(string $eventName, string $id)
 */
class FileStorage
{
    protected $_loadedFiles;
    protected $_config;
    protected $_eventListener;

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (!defined('FILESTORAGE_ROOT_DIR')) {
            require __DIR__ . '/resources/compat.php';
        }

        $this->_config = ArrayUtils::merge((array)(include FILESTORAGE_ROOT_DIR . '/resources/config/config.php'), $config);

        $this->_loadedFiles = [];

        $this->_eventListener = new SimpleEventListener();
        $this->_eventListener->createEvent('AddFile');
        $this->_eventListener->createEvent('BeforeAddFile');
        $this->_eventListener->createEvent('RemoveFile');
        $this->_eventListener->createEvent('BeforeRemoveFile');
    }

    public function __destruct()
    {
        foreach ($this->_loadedFiles as &$loadedFile) {
            unset($loadedFile);
        }
    }

    /**
     * Хук для встраивания ивентов в обьект
     *
     * @param string $name
     *
     * @return \MommyCom\YiiComponent\SimpleEventListener\Event
     */
    public function __get($name)
    {
        if (substr($name, 0, 5) == 'event' && $this->_eventListener->isExists($eventName = substr($name, 5, -1))) {
            return $this->_eventListener->get($eventName);
        }
        return $this->$name;
    }

    /**
     * @param string $path (optional, default = '')
     *
     * @return array
     */
    public function getConfig($path = '')
    {
        return ArrayUtils::getValueByPath($this->_config, $path);
    }

    /**
     * Возвращает сервера с заданными параметрами
     *
     * @param null|boolean $acceptNewFiles
     * @param integer|null $fsVersion файловая система используемая сервером (если null то без проверки)
     *
     * @internal param bool|null $allowNewFiles принимает ли север новые файлы (если null то без проверки)
     * @return array
     */
    public function getServers($acceptNewFiles = null, $fsVersion = null)
    {
        $servers = $this->getConfig('servers');
        if (!is_null($acceptNewFiles) || !is_null($fsVersion)) { //проверка что-бы не запускать лишний цикл
            foreach ($servers as $serverId => $server) {
                if ((!is_null($acceptNewFiles) && $server['acceptNewFiles'] != (bool)$acceptNewFiles) ||
                    (!is_null($fsVersion) && $server['fsVersion'] == Cast::toUInt($fsVersion))) {
                    unset($servers[$serverId]);
                }
            }
        }
        return $servers;
    }

    /**
     * Создает файл по его id
     *
     * @param string $id
     *
     * @return File
     */
    public function getFile($id)
    {
        if (!isset($this->_loadedFiles[$id])) {
            $this->_loadedFiles[$id] = File::factory($this, $id);
        }
        return $this->_loadedFiles[$id];
    }

    /**
     * Удаляет файл
     *
     * @param string $id
     *
     * @return boolean
     */
    public function removeFile($id)
    {
        return $this->getFile($id)->remove();
    }

    /**
     * Проверяет файл на существование
     *
     * @param string $id
     *
     * @return boolean
     */
    public function isExists($id)
    {
        return $this->getFile($id)->isExists();
    }

    /**
     * Проверяет валидность id
     *
     * @param string $id
     *
     * @return boolean
     */
    public function isValidId($id)
    {
        return IdProtocolAbstract::isValidProtocol($this, $id);
    }

    /**
     * Проверяет валидность id
     *
     * @param string $id
     *
     * @return boolean
     */
    public function isValidEncodedId($id)
    {
        return IdProtocolAbstract::isValidEncodedProtocol($this, $id);
    }

    /**
     * @param string $id
     *
     * @return string
     */
    public static function encodeId($id)
    {
        return IdProtocolAbstract::encodeId($id);
    }

    /**
     * @param string $id
     *
     * @return string
     */
    public static function decodeId($id)
    {
        return IdProtocolAbstract::decodeId($id);
    }
}
