<?php

namespace MommyCom\YiiComponent\FileStorage;

interface ModifierInterface
{
    /**
     * @param File $file
     * @param array $options
     * @param array $createOptions
     */
    public function __construct(File $file, array $options, array $createOptions = []);

    /**
     * @return boolean
     */
    public function apply();

    /**
     * вызывается когда удаляется файл
     *
     * @return void
     */
    public function remove();
}
