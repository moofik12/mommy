<?php

namespace MommyCom\YiiComponent\FileStorage\IdProtocol;

use MommyCom\YiiComponent\FileStorage\FileStorage;
use MommyCom\YiiComponent\FileStorage\IdProtocolAbstract;
use MommyCom\YiiComponent\FileStorage\Type;
use MommyCom\YiiComponent\Type\Cast;

class V0 extends IdProtocolAbstract
{
    const ID_RAND_PART_SIZE = 7;

    /**
     * @var integer
     */
    protected $_idVersion;

    /**
     * @var integer
     */
    protected $_serverId;

    /**
     * @var string
     */
    protected $_rand;

    /**
     * @var integer
     */
    protected $_type;

    /**
     * @var integer
     */
    protected $_tag;

    /**
     * @var integer
     */
    protected $_creationTime;

    /**
     * @var boolean
     */
    protected $_idParsed;

    /**
     * @var boolean
     */
    protected $_idChanged;

    /**
     * @param FileStorage $storage
     * @param mixed $param
     * @param int $paramType
     */
    protected function __construct(FileStorage $storage, $param, $paramType = self::CONSTRUCT_FROM_ID)
    {
        parent::__construct($storage, $param, $paramType);
        $this->_idParsed = false;
        $this->_idChanged = false;
    }

    /**
     * @return string
     */
    protected function _getSecretKey()
    {
        return 'TD_*1&x*Q63A:xCkuaqEni%%63@qq:cR';
    }

    protected function _encryptId($id)
    {
//        $ivSize = mcrypt_get_iv_size(MCRYPT_GOST, MCRYPT_MODE_CBC);
//        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
//        return mcrypt_encrypt(MCRYPT_GOST, $this->_getSecretKey(), $id, MCRYPT_MODE_CBC, $iv);
        return $id;
    }

    protected function _decryptId($id)
    {
//        $ivSize = mcrypt_get_iv_size(MCRYPT_GOST, MCRYPT_MODE_CBC);
//        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
//        return mcrypt_decrypt(MCRYPT_GOST, $this->_getSecretKey(), $id, MCRYPT_MODE_CBC, $iv);
        return $id;
    }

    protected function _parseId()
    {
        if (!$this->_idParsed) {
            $this->_idParsed = true;
            list(
                $this->_idVersion,
                $this->_serverId,
                $this->_rand,
                $this->_type,
                $this->_creationTime,
                $this->_tag
                ) = array_values(unpack('CidVersion/SserverId/a' . self::ID_RAND_PART_SIZE . 'random/Stype/Ltime/Ltag', $this->_decryptId($this->_id)));
        }
    }

    /**
     * @param string $filename
     *
     * @return string id
     */
    public function _generateId($filename)
    {
        $availableServers = $this->_storage->getServers(true);
        shuffle($availableServers); //нужно получить случайный сервер (небольшая балансировка нагрузки)
        $firstAvailableServer = reset($availableServers);

        $idVersion = 0;
        $serverId = $firstAvailableServer['id'];
        $rand = $this->_generateRand(self::ID_RAND_PART_SIZE);
        $type = Type::factoryFromLocalFile($this->_storage, $filename)->getTypeCode();
        $time = time();
        $tag = 0;

        return $this->_compileId([
            $idVersion,
            $serverId,
            $rand,
            $type,
            $time,
            $tag,
        ]);
    }

    /**
     * @param array $params
     *
     * @return type
     */
    protected function _compileId(array $params)
    {
        array_unshift($params, 'CSA' . self::ID_RAND_PART_SIZE . 'SII');
        return $this->_encryptId(call_user_func_array('pack', $params));
    }

    /**
     * @param integer $size
     *
     * @return string
     */
    protected function _generateRand($size)
    {
        $size = Cast::toUInt($size, 2, 2, 64);

        if (substr(php_uname('s'), 0, 3) == 'Win') {
            $random = '';
            for ($i = 0; $i < $size; $i++) {
                $random .= chr(rand(0, 255));
            }
            return $random;
        }

        $randomSize = intval(file_get_contents('/proc/sys/kernel/random/entropy_avail'));
        if ($randomSize > $size * 10) { //x10 на случай если кто-то уже читает рандом и у нас был шанс обойтись без блока
            $random = fread(fopen('/dev/random', 'r'), $size);
        } else {
            $random = fread(fopen('/dev/urandom', 'r'), $size);
        }
        return $random;
    }

    public function getId()
    {
        if ($this->_idChanged) {
            $this->_idChanged = false;

            $this->_id = $this->_compileId([
                $this->_idVersion,
                $this->_serverId,
                $this->_rand,
                $this->_type,
                $this->_creationTime,
                $this->_tag,
            ]);
        }
        return $this->_id;
    }

    public function getVersion()
    {
        $this->_parseId();
        return $this->_idVersion;
    }

    public function getServerId()
    {
        $this->_parseId();
        return $this->_serverId;
    }

    public function getRand()
    {
        $this->_parseId();
        return $this->_rand;
    }

    public function getCreationTime()
    {
        $this->_parseId();
        return $this->_creationTime;
    }

    public function setCreationTime($time)
    {
        $this->_parseId();
        $this->_idChanged = true;
        return $this->_creationTime = $time;
    }

    public function getType()
    {
        $this->_parseId();
        return $this->_type;
    }

    public function setType($typeCode)
    {
        $this->_parseId();
        $this->_idChanged = true;
        return $this->_type = $typeCode;
    }

    public function getTag()
    {
        $this->_parseId();
        return $this->_tag;
    }

    public function setTag($tag)
    {
        $this->_parseId();
        $this->_idChanged = true;
        return $this->_tag = $tag;
    }
}
