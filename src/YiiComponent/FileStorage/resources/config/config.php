<?php
return [
    'versions' => [
        'idProtocol' => 0,
        'metadata' => 0,
    ],
    'base' => [
        'maxUploadedFileSize' => 7 * 8 * 1024 * 1024, //7 megabytes
    ],
    'servers' => [
//        1 => array(
//            'id' => 1,
//            'fsVersion' => 0, //версия файловой системы
//            'acceptNewFiles' => true, // принимает новые файлы
//            'liveChecker' => function() { //ф-ция проверки того что сервер "жив"
//                return true;
//            },
//            'web' => array(
//                'scheme' => 'http',
//                'port' => 80,
//                'host' => 'localhost',
//                'path' => '/netbeansproj/enoso/trunk/web/filestorage/fs1',
//            ),
//            'fsSpecified' => array( // настройки специфичные для каждой файловой системы, в данном случае local storage или nfs-like
//                'path' => '/opt/lampp/htdocs/netbeansproj/enoso/trunk/web/filestorage/fs1', //путь к хранилищу
//                'secretKey' => 'F5n!5Xkd>>un6{xq+vg)PWpUH?5$a:wU:j<J=yFHxLPnh=T3ODi_q>OXgMJc5b>C'
//            ),
//        ),
    ],
    'mime' => (array)(include __DIR__ . '/typeSpecified.php'),
];
