<?php
return [
    'supportedMimes' => [
        //тип файла согласно стандартам => тип файла согласно движку
        'unknown/unknown' => ['unknown'],
        'image/jpeg' => ['image', 'jpeg'],
        'image/png' => ['image', 'png'],
        'image/bmp' => ['image', 'bmp'],
        'image/gif' => ['image', 'gif'],
        //'image/svg+xml' => array('image', 'svg'),
        //'image/ico' => array('image', 'ico'),
        //'image/tiff' => array('image', 'tiff'),
        //'image/psd' => array('image', 'psd'),

        //'application/vnd.msword' => array('document', 'word'),
        //'application/opendocument' => array('document', 'odt'),
    ],
    'engineMimes' => [
        //соответствует именам классов например FileStorage\Type\Image, FileStorage\Type\Image\Jpeg
        'unknown' => 0,
        'image' => [
            'jpeg' => 1,
            'png' => 2,
            'bmp' => 3,
            'gif' => 4,
            //'psd' => 5,
            //'svg' => 7,
        ],
    ],
    'engineMimesExtensions' => [
        'unknown' => '',
        'image' => [
            'jpeg' => 'jpg',
            'png' => 'png',
            'bmp' => 'bmp',
            'gif' => 'gif',
            //'psd' => 'psd',
            //'svg' => 'svg',
        ],
    ],
    'modifiers' => [
//        'image' => array(array(
//                'name' => 'FileStorage\Modifiers\Image_MakeThumbs',
//                'options' => array(
//                    'resolution' => array(
//                        //format: array(name, width, height, quality, method, methodParams), blur < 1 = sharpen, blur > 1 blurry, blur == 1 nothing
//                        array('small50crop', 50, 50, 90, 'crop', array('openCV' => 'face', 'blur' => 0.5)), //50х50 с лицом и шарпеном
//                        array('small60crop', 60, 60, 90, 'crop', array('openCV' => 'face', 'blur' => 0.6)), //60х60 с лицом и шарпеном
//                        array('mid100crop', 100, 100, 90, 'crop', array('openCV' => 'human', 'blur' => 0.7)), //100х100 с человеком и шарпеном
//                        array('mid200crop', 200, 200, 90, 'crop', array('openCV' => 'human', 'blur' => 0.8)), //200х200 с человеком
//                        array('mid200prop', 200, 200, 90, 'propResize', array('openCV' => 'human', 'blur' => 0.8)),
//                        array('mid640prop', 640, 480, 90, 'propResize'),
//                        array('mid800prop', 800, 600, 90, 'propResize'),
//                        array('hd1024prop', 1024, 768, 90, 'propResize'),
//                    )
//                )
//            ), array(
//                'name' => 'FileStorage\Modifiers\Image\Sanitize',
//                'options' => array(
//                    'emptyExif' => false, //очистить exif
//                    'deleteFormatSpecifiedUnnecessaryData' => true, //удалить возможные дополнительные данные у изображения (например у png удалить скрытые chunks)
//                    'deleteAnimation' => true //удалить анимацию
//                )
//            ), array(
//                'name' => 'FileStorage\Modifiers\Image\Compress',
//                'options' => array(
//                    'maxImageSize' => 3 * 1024 * 1024,
//                    'maxImageResolution' => array(1920, 1080)
//                )
//            ), array(
//                'name' => 'FileStorage\Modifiers\Image\EffectSharpen',
//                'options' => array(
//                    'maxImageResolution' => array(640, 480)
//                )
//            )
//        )
    ],
]; 
