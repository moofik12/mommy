<?php

namespace MommyCom\YiiComponent\FileStorage;

use MommyCom\YiiComponent\FileStorage\Exception\IdProtocol\EmptyIdException;
use MommyCom\YiiComponent\FileStorage\Exception\IdProtocol\InvalidVersionException;
use MommyCom\YiiComponent\FileStorage\Exception\IdProtocolException;
use MommyCom\YiiComponent\FileStorage\IdProtocol\V0;

abstract class IdProtocolAbstract
{
    /**
     * @var \MommyCom\YiiComponent\FileStorage\FileStorage
     */
    protected $_storage;

    /**
     * @var string
     */
    protected $_id;

    /**
     * @var boolean
     */
    protected $_systemDataLocked;

    const CONSTRUCT_FROM_ID = 0;
    const CONSTRUCT_FROM_FILENAME = 1;

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param mixed $param
     * @param int $paramType
     *
     * @throws IdProtocolException
     */
    protected function __construct(FileStorage $storage, $param, $paramType = self::CONSTRUCT_FROM_ID)
    {
        $this->_storage = $storage;

        switch ($paramType) {
            case self::CONSTRUCT_FROM_ID:
                $this->_id = $param;
                break;
            case self::CONSTRUCT_FROM_FILENAME:
                $this->_id = $this->_generateId($param);
                break;
            default:
                throw new IdProtocolException();
        }

        $this->_systemDataLocked = false;
        $this->_isChanged = false;
    }

    /**
     * @return boolean
     */
    public function isSystemDataLocked()
    {
        return $this->_systemDataLocked;
    }

    /**
     * @param string $filename ;
     *
     * @return string
     */
    abstract protected function _generateId($filename);

    /**
     * @return integer
     */
    abstract public function getVersion();

    /**
     * @return string
     */
    abstract public function getId();

    /**
     * @return integer
     */
    abstract public function getServerId();

    /**
     * @return integer unixtime
     */
    abstract public function getCreationTime();

    /**
     * @param integer
     */
    abstract public function setCreationTime($time);

    /**
     * @return integer
     */
    abstract public function getType();

    /**
     * @param integer
     */
    abstract public function setType($typeCode);

    /**
     * Тег - специальный расдел с ключе
     *
     * @return integer
     */
    abstract public function getTag();

    /**
     * @param $tag
     *
     * @return integer
     */
    abstract public function setTag($tag);

    /**
     * Вытягивает из id его версию
     *
     * @param string $id
     *
     * @return integer|null
     */
    public static function extractIdVersion($id)
    {
        if (empty($id) || !is_string($id)) {
            throw new EmptyIdException();
        }
        list($versionId) = array_values(unpack('Cversion', $id)); //unsigned char
        return intval($versionId);
    }

    /**
     * Проверяет является ли id правильным
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $id
     *
     * @return boolean
     */
    public static function isValidProtocol(FileStorage $storage, $id)
    {
        try {
            $storageProtocolParser = self::factory($storage, $id); //Если произойдет ошибка значит с протоколом что-то не так

            $availableServers = array_keys($storage->getServers());
            if (!in_array($storageProtocolParser->getServerId(), $availableServers)) {
                return false;
            }

            unset($storageProtocolParser);
            return true;
        } catch (FileStorageException $exception) {
            unset($exception);
            return false;
        }
    }

    /**
     * Проверяет является ли id правильным
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param $encodedId
     *
     * @internal param string $id
     * @return boolean
     */
    public static function isValidEncodedProtocol(FileStorage $storage, $encodedId)
    {
        return self::isValidProtocol($storage, self::decodeId($encodedId));
    }

    /**
     * Проверяет является ли id правильным
     *
     * @return boolean
     */
    public function getEncodedId()
    {
        return $this->encodeId($this->getId());
    }

    /**
     * Кодирует Id
     *
     * @param string $id
     *
     * @return string
     */
    public static function encodeId($id)
    {
        return strtr(
            rtrim(base64_encode($id), '='),
            '+/', '*-'
        );
    }

    /**
     * Декодирует Id
     *
     * @param string $encodedId
     *
     * @return string
     */
    public static function decodeId($encodedId)
    {
        return base64_decode(
            strtr($encodedId, '*-', '+/') .
            '=='
        );
    }

    /**
     * Создает экземпляр класса по управлению протоколом id
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string|IdProtocolAbstract $id
     *
     * @return IdProtocolAbstract
     */
    public static function factory(FileStorage $storage, $id)
    {
        static $ids = [];
        $class = __CLASS__;
        if (!$id instanceof $class) {
            if (empty($ids[$id])) {
                if (0 !== (int)self::extractIdVersion($id)) {
                    throw new InvalidVersionException;
                }

                $idProtocol = new V0($storage, $id);
                $ids[$id] = clone $idProtocol;
            } else {
                // ну не будем-же всем выдавать один и тот-же экземпляр?, 
                // на случай если кто-то подправить id захочет и не распостранять изменения 
                // на остальные обьекты использующие данный обьект
                $idProtocol = clone $ids[$id];
            }
        } else {
            $idProtocol = $id;
        }
        return $idProtocol;
    }

    /**
     * Создает экземпляр класса по управлению протоколом id (для закодированной id)
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $encodedId
     *
     * @return IdProtocolAbstract
     */
    public static function factoryFromEncodedId(FileStorage $storage, $encodedId)
    {
        return self::factory($storage, self::decodeId($encodedId));
    }

    /**
     * Создает экземпляр класса по управлению протоколом id для нового файла
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $filename
     *
     * @return IdProtocolAbstract
     */
    public static function factoryFromLocalFile(FileStorage $storage, $filename)
    {
        return new V0($storage, $filename, IdProtocolAbstract::CONSTRUCT_FROM_FILENAME);
    }

    public function __toString()
    {
        return $this->getEncodedId();
    }
}
