<?php

namespace MommyCom\YiiComponent\FileStorage;

use CApplicationComponent;
use CLogger;
use Exception;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

/**
 * FileStorageComponent
 *
 * @property-read FileStorage $storage
 */
class FileStorageComponent extends CApplicationComponent
{
    const FILESTORAGE_DEFAULT_GLOBAL = 'filestorage';

    public static $filestorageGlobal = 'filestorage';

    /**
     * @var FileStorage
     */
    protected $_filestorage;

    protected $_cache = [];

    public $config = [];

    public $thumbnailsConfig = [];

    public function init()
    {
        $this->_filestorage = new FileStorage($this->config);
        parent::init();
    }

    /**
     * @return FileStorage
     */
    public function getStorage()
    {
        return $this->_filestorage;
    }

    /**
     * @param string $name
     * @param string|null $namespace
     * @param string $type тип файлов который может быть загружен, напр (array('image/*', 'video/*') или 'image/*' или 'image/jpeg')
     * @param array $createOptions
     *
     * @return File|integer При неудаче возвращает код ошибки, коды смотреть в FileStorage\File::ERROR_*
     */
    public function factoryFromUploadedFile($name, $namespace = null, $type = '', $createOptions = [])
    {
        $context = is_null($namespace) ? $_FILES : ArrayUtils::getValueByPath($_FILES, $namespace);
        if ($namespace ? isset($context['name'][$name]) : isset($context[$name])) {
            //remap
            if (!is_null($namespace)) {
                $fileInfo = [
                    'name' => $context['name'][$name],
                    'type' => $context['type'][$name],
                    'tmp_name' => $context['tmp_name'][$name],
                    'error' => $context['error'][$name],
                    'size' => $context['size'][$name],
                ];
            } else {
                $fileInfo = $context[$name];
            }

            $file = File::factoryFromUploadedFile($this->getStorage(), $fileInfo, $type, $createOptions);
            if ($file instanceof File) {
                $this->_cache[$file->getEncodedId()] = $file;
            }
            return $file;
        }
        return File::ERROR_IS_NOT_FILE;
    }

    /**
     * @param string $name
     * @param string|null $namespace
     * @param string $type тип файлов который может быть загружен, напр (array('image/*', 'video/*') или 'image/*' или 'image/jpeg')
     * @param array $createOptions
     *
     * @return File[] При неудаче возвращает пустой массив
     */
    public function factoryFromUploadedFiles($name, $namespace = null, $type = '', $createOptions = [])
    {
        $files = [];

        $context = is_null($namespace) ? $_FILES : @ArrayUtils::getValueByPath($_FILES, $namespace);

        if (isset($context['name']) && isset($context['name'][$name]) && is_array($context['name'][$name])) {
            foreach (array_keys($context['name'][$name]) as $index) {
                $fileInfo = [
                    'name' => $context['name'][$name][$index],
                    'type' => $context['type'][$name][$index],
                    'tmp_name' => $context['tmp_name'][$name][$index],
                    'error' => $context['error'][$name][$index],
                    'size' => $context['size'][$name][$index],
                ];

                $file = File::factoryFromUploadedFile($this->getStorage(), $fileInfo, $type, $createOptions);

                if ($file instanceof File) {
                    $this->_cache[$file->getEncodedId()] = $file;
                    $files[$file->getEncodedId()] = $file;
                }
            }
        }

        return $files;
    }

    /**
     * @param $filename
     * @param string $type
     * @param string $type тип файлов который может быть загружен, напр (array('image/*', 'video/*') или 'image/*' или 'image/jpeg')
     * @param array $createOptions
     *
     * @return \MommyCom\YiiComponent\FileStorage\File|integer
     */
    public function factoryFromLocalFile($filename, $type = '', $createOptions = [])
    {
        $file = File::factoryFromLocalFile($this->getStorage(), $filename, $type, $createOptions);

        if ($file instanceof File) {
            $this->_cache[$file->getEncodedId()] = $file;
            $files[$file->getEncodedId()] = $file;
        }

        return $file;
    }

    /**
     * Создает файл из закодированного id
     *
     * @param string $id
     *
     * @return File|null
     */
    public function factoryFromEncodedId($id)
    {
        if (!IdProtocolAbstract::isValidEncodedProtocol($this->storage, $id)) {
            return null;
        }
        if (empty($this->_cache[$id])) {
            try {
                $this->_cache[$id] = File::factoryFromEncodedId($this->_filestorage, $id);
            } catch (Exception $e) {
                Yii::log('Invalid file, value = ' . Cast::toStr($id), CLogger::LEVEL_WARNING, 'application.filestorage');
            }
        }
        return $this->_cache[$id];
    }

    /**
     * Создает файл из закодированного id
     * <b>Alias of factoryFromEncodedId</b>
     *
     * @param string $id
     *
     * @return File
     */
    public function get($id)
    {
        return $this->factoryFromEncodedId($id);
    }

    /**
     * @param string $id
     * @param array $defaults
     *
     * @return \MommyCom\YiiComponent\FileStorage\FileStorageThumbnail
     */
    public function getThumbnails($id, $defaults = [])
    {
        return new FileStorageThumbnail($this->get($id), $defaults, $this);
    }

    /**
     * Проверяет является ли id правильным
     *
     * @param string $id
     *
     * @return boolean
     */
    public function isValidEncodedId($id)
    {
        return $this->_filestorage->isValidEncodedId($id);
    }

    /**
     * Создает файл из удаленного файла
     *
     * @param string $remoteFilename
     * @param integer $maxSize
     *
     * @return File
     */
    public function factoryFromRemoteFile($remoteFilename, $maxSize = 15000000)
    {
        return File::factoryFromRemoteFile($this->_filestorage, $remoteFilename, $maxSize);
    }

    /**
     * Создает файл из base64 строки
     *
     * @param string $base64String
     *
     * @return File
     */
    public function factoryFromBase64String($base64String)
    {
        return File::factoryFromBase64String($this->_filestorage, $base64String);
    }

    /**
     * Создает файл из бинарной строки
     *
     * @param string $binaryString
     *
     * @return File
     */
    public function factoryFromBinaryString($binaryString)
    {
        return File::factoryFromBinaryString($this->_filestorage, $binaryString);
    }
}
