<?php

namespace MommyCom\YiiComponent\FileStorage\Exception\Cache;

use MommyCom\YiiComponent\FileStorage\Exception\CacheException;

class InvalidProviderException extends CacheException
{
}
