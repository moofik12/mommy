<?php

namespace MommyCom\YiiComponent\FileStorage\Exception;

use MommyCom\YiiComponent\FileStorage\FileStorageException;

class NoFinfoExtensionLoadedException extends FileStorageException
{
}
