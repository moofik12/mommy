<?php

namespace MommyCom\YiiComponent\FileStorage\Exception\IdProtocol;

use MommyCom\YiiComponent\FileStorage\Exception\IdProtocolException;

class EmptyIdException extends IdProtocolException
{
}
