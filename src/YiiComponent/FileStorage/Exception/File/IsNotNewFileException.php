<?php

namespace MommyCom\YiiComponent\FileStorage\Exception\File;

use MommyCom\YiiComponent\FileStorage\Exception\FileException;

class IsNotNewFileException extends FileException
{
}
