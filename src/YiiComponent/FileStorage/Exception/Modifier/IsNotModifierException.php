<?php

namespace MommyCom\YiiComponent\FileStorage\Exception\Modifier;

use MommyCom\YiiComponent\FileStorage\FileStorageException;

class IsNotModifierException extends FileStorageException
{
}
