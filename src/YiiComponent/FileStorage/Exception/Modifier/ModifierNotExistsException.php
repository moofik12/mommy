<?php

namespace MommyCom\YiiComponent\FileStorage\Exception\Modifier;

use MommyCom\YiiComponent\FileStorage\FileStorageException;

class ModifierNotExistsException extends FileStorageException
{
}
