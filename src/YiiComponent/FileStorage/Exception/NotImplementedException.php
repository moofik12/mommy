<?php

namespace MommyCom\YiiComponent\FileStorage\Exception;

use MommyCom\YiiComponent\FileStorage\FileStorageException;

class NotImplementedException extends FileStorageException
{
    public function __toString()
    {
        return "Если вы видете это сообщение значит что-то что вы запустили еще не реализовано, реализуйте это плз.\n\n\n" . parent::__toString();
    }
}

