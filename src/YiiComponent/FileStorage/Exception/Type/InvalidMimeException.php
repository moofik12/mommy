<?php

namespace MommyCom\YiiComponent\FileStorage\Exception\Type;

use MommyCom\YiiComponent\FileStorage\Exception\TypeException;

class InvalidMimeException extends TypeException
{
}
