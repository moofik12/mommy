<?php

namespace MommyCom\YiiComponent\FileStorage\Exception;

use MommyCom\YiiComponent\FileStorage\FileStorageException;

class IdProtocolException extends FileStorageException
{
}
