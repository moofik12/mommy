<?php

namespace MommyCom\YiiComponent\FileStorage;

use CComponent;
use Exception;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\FileStorage\Modifiers\Image\MakeThumbs;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

/**
 * FileStorageThumbnail
 *
 * @property-read Image|null $file
 * @property-read boolean $isEmpty
 * @property-read boolean $isExists
 * @property-read string $encodedId
 * @property-read string $url
 * @property-read
 */
class FileStorageThumbnail extends CComponent
{
    /**
     * @var array
     */
    protected $_defaults = [];

    /**
     * Файл в хранилище
     *
     * @var Image
     */
    protected $_file = null;

    /**
     * Id файла
     *
     * @var string
     */
    protected $_fileId = '';

    /**
     * Врапер к файлстораджу
     *
     * @var FileStorageComponent
     */
    protected $_filestorage = null;

    /**
     * @param string|File $file
     * @param array $defaults
     * @param FileStorageComponent|null $filestorage
     *
     * @return FileStorageThumbnail
     */
    public function __construct($file, $defaults, $filestorage = null)
    {
        $this->_filestorage = is_null($filestorage) ? Yii::app()->{FileStorageComponent::FILESTORAGE_DEFAULT_GLOBAL} : $filestorage;
        $defaults = $defaults + $this->_filestorage->thumbnailsConfig;
        if (!$file instanceof Image) {
            try {
                if (!empty($file)) {
                    $file = $this->_filestorage->factoryFromEncodedId((string)$file);
                }
                if (!$file instanceof Image) { // only images are allowed
                    $file = null;
                }
            } catch (Exception $e) {
                $file = null;
            }
        }
        $this->_defaults = $defaults;
        $this->_file = $file;

        if (!is_null($file)) {
            $this->_fileId = $file->getEncodedId();
        }
    }

    /**
     * Обьект-враппер к хранилищу файлов
     *
     * @return FileStorageComponent
     */
    public function getFileStorageComponent()
    {
        return $this->_filestorage;
    }

    /**
     * Возвращает тумбу
     *
     * @param string $name имя тумбы
     *
     * @return ThumbLink
     */
    public function getThumbnail($name)
    {
        static $cache = [];

        $name = (string)$name;

        $cacheKey = $name . '_' . $this->_fileId . '_' . spl_object_hash($this->getFileStorageComponent());

        if (isset($cache[$cacheKey])) { // уже есть в кэше
            return $cache[$cacheKey];
        }

        $availableThumbnails = $this->getStorageAvailableThumbnails();

        $result = new ThumbLink();

        $result->realName = $result->name = $name;
        $result->isPseudo = true;
        $result->file = null;
        $result->url = null;

        $defaults = $this->_defaults;

        // resolve links
        if (isset($defaults[$name]) && isset($defaults[$name][0]) && $defaults[$name][0] == ':') {
            $name = mb_substr($defaults[$name], 1);
            $resolvedResult = $this->getThumbnail($name);
            $resolvedResult->name = $result->name;
            return $resolvedResult;
        }

        switch (true) {
            // такого рода тумбы существуют и файл тоже
            case isset($availableThumbnails[$name]) && ($file = $this->getFile()) !== null:
                $thumb = $file->getThumb($name);
                $result->url = $thumb->getWebFullPath();
                $result->isPseudo = false;
                $result->file = $thumb;
                break;
            // такого рода тумбы существуют, файла нет, но есть дефолтовый урл
            case isset($defaults[$name]):
                $result->url = $defaults[$name];
                break;
            // такого рода тумбы не сущетсвуют, файла нет, но есть дефолтовый урл
            case isset($defaults['default']):
                $result->url = $defaults['default'];
                break;
        }

        return $cache[$cacheKey] = $result;
    }

    /**
     * Возвращает наиболее подходящую по ширине тумбу
     *
     * @param integer $idealWidth
     * @param string $type 'all' - все типы, 'prop' - пропорциональные, 'crop' - обрезанные
     *
     * @return ThumbLink
     */
    public function getThumbnailByWidth($idealWidth, $type = 'all')
    {
        $idealWidth = Cast::toUInt($idealWidth);

        $availableThumbnails = $this->getStorageAvailableThumbnails();

        $widthDiffs = [];
        foreach ($availableThumbnails as $name => $thumb) {
            if ($type == 'all' || ($type == 'crop' && $thumb[4] == 'crop') || ($type == 'prop' && $thumb[4] == 'propResize')) {
                $widthDiffs[$name] = $thumb[1] - $idealWidth;
            }
        }

        $buildCompareFunction = function ($max) {
            return function ($a, $b) use ($max) {
                if ($a < 0) {
                    $a = $max - $a;
                }
                if ($b < 0) {
                    $b = $max - $b;
                }

                if ($a == $b) {
                    return 0;
                }
                return $a > $b ? 1 : -1;
            };
        };

        uasort($widthDiffs, $buildCompareFunction(max($widthDiffs)));

        reset($widthDiffs);

        return $this->getThumbnail(key($widthDiffs));
    }

    /**
     * Файл изображения
     *
     * @return Image|null
     */
    public function getFile()
    {
        return $this->_file;
    }

    /**
     * Возвращает настройки возможно существующих тумб
     *
     * @return array
     */
    public function getStorageAvailableThumbnails()
    {
        static $cache = [];
        $cacheKey = spl_object_hash($this->getFileStorageComponent());

        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }

        $thumbnails = [];
        $modifiers = $this->getFileStorageComponent()->getStorage()->getConfig('mime.modifiers.image');
        foreach ($modifiers as $modifier) {
            if ($modifier['name'] == MakeThumbs::class) {
                $settings = $modifier['options']['resolution'];
                foreach ($settings as $option) {
                    $thumbnails[$option[0]] = $option; // zero index element is name of created thumb
                }
            }
        }

        return $cache[$cacheKey] = $thumbnails;
    }

    /**
     * Все доступные тумбы
     *
     * @return ThumbLink[]
     */
    public function getAvailableThumbnails()
    {
        static $cache = [];

        if (isset($cache[$this->_fileId])) {
            return $cache[$this->_fileId];
        }

        $result = [];

        $names = array_keys($this->_defaults !== [] ? $this->_defaults : $this->getStorageAvailableThumbnails());

        foreach ($names as $thumbnailName) {
            if ($thumbnailName !== 'default') {
                $result[$thumbnailName] = $this->getThumbnail($thumbnailName);
            }
        }

        return $cache[$this->_fileId] = $result;
    }

    /**
     * Урлы ко всем доступным тумбам
     *
     * @return array
     */
    public function getAvailableThumbnailUrls()
    {
        static $cache = [];

        if (isset($cache[$this->_fileId])) {
            return $cache[$this->_fileId];
        }

        $result = [];

        foreach ($this->getAvailableThumbnails() as $thumbnailName => $thumbLink) {
            /* @var $thumbLink ThumbLink */
            $result[$thumbnailName] = $thumbLink->url;
        }

        return $cache[$this->_fileId] = $result;
    }

    /**
     * @return bool
     */
    public function getIsEmpty()
    {
        return empty($this->_file);
    }

    /**
     * @return bool
     */
    public function getExists()
    {
        return !$this->getIsEmpty() && $this->getFile()->isExists();
    }

    /**
     * @return string
     */
    public function getEncodedId()
    {
        return !$this->getIsEmpty() ? $this->getFile()->getEncodedId() : '';
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return !$this->getIsEmpty() ? $this->getFile()->getWebFullPath() : '';
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (!method_exists($this, $getter) && !$this->__isset($name)) {
            return $this->getThumbnail($name);
        }
        return parent::__get($name);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUrl();
    }
}
