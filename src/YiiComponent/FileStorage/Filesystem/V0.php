<?php

namespace MommyCom\YiiComponent\FileStorage\Filesystem;

use MommyCom\YiiComponent\FileStorage\FileStorage;
use MommyCom\YiiComponent\FileStorage\FilesystemAbstract;
use MommyCom\YiiComponent\FileStorage\IdProtocolAbstract;
use MommyCom\YiiComponent\FileStorage\Type;
use MommyCom\YiiComponent\Type\Cast;

/**
 * Адаптер к стандартной файловой системе
 */
class V0 extends FilesystemAbstract
{
    const METADATA_FILEEXT = 'met';
    const METADATA_FILEPOSTFIX = 'mt';

    /**
     * @var array
     */
    protected $_serverDir;

    /**
     * @var string
     */
    protected $_secretKey;

    /**
     * @param FileStorage $storage
     * @param integer $serverId
     */
    protected function __construct(FileStorage $storage, $serverId)
    {
        parent::__construct($storage, $serverId);
        $this->_serverDir = $this->_serverInfo['fsSpecified']['path'];
        $this->_secretKey = $this->_serverInfo['fsSpecified']['secretKey'];
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    protected function _getLocalFilePath($id)
    {
        static $filepaths = [];
        static $idProtocolInstances = [];
        static $typeInstances = [];

        $fileKey = '';
        if (is_array($id) && isset($fileKey)) {
            list($id, $fileKey) = $id;
            $fileKey = dechex(crc32($fileKey));
        }

        $cacheKey = $id . '_' . $fileKey;

        //echo base64_encode($cacheKey); exit;

        if (isset($filepaths[$cacheKey])) {
            return $filepaths[$cacheKey];
        }

        if (empty($idProtocolInstances[$id])) {
            $idProtocolInstances[$id] = IdProtocolAbstract::factory($this->_storage, $id);
        }

        $idProtocol = $idProtocolInstances[$id];
        /* @var $idProtocol IdProtocolAbstract */

        $typeCode = $idProtocol->getType();

        if (empty($typeInstances[$typeCode])) {
            $typeInstances[$typeCode] = Type::factory($this->_storage, $typeCode);
        }
        $type = $typeInstances[$typeCode];

        $directoryTree = sha1($idProtocol->getId() . $this->_secretKey);
        $directoryTree = str_split($directoryTree, intval(strlen($directoryTree) / 4)); //разбиваем на четыре уровня вложености папок

        foreach ($directoryTree as &$directory) {
            $directory = dechex(fmod(hexdec($directory), 0x7AAA)); //делаем так чтобы количество папок на одном уровне было < 0x7AAA (~31k)
        }
        unset($directory);

        $path = implode(DIR_SEP, $directoryTree);
        $filenameHash = base64_encode(hash('sha256', $idProtocol->getId() . $this->_secretKey, true));
        $filenameHash = strtr(
            rtrim(base64_encode($filenameHash), '='),
            '+/', '*-'
        );

        $filename = $filenameHash . $fileKey . $type->getExtension(true);
        return $filepaths[$cacheKey] = $path . DIR_SEP . $filename;
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    protected function _getMetadataFilePath($id)
    {
        $localPath = $this->_getLocalFilePath($id);
        list($localPathWithoutExt) = explode('.', $localPath);
        $metadataFilename = $localPathWithoutExt . self::METADATA_FILEPOSTFIX . '.' . self::METADATA_FILEEXT;
        return $metadataFilename;
    }

    /**
     * Если файловая система защищена от удаления то всегда возвращается false
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return boolean
     */
    public function removeFile($id)
    {
        if (!$this->isAllowDeleteFiles()) {
            return false;
        }
        return @unlink($this->_serverDir . DIR_SEP . $this->_getLocalFilePath($id));
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    public function getFileContent($id)
    {
        return file_get_contents($this->_serverDir . DIR_SEP . $this->_getLocalFilePath($id));
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $data
     *
     * @return boolean
     */
    public function setFileContent($id, $data)
    {
        return file_put_contents($this->_serverDir . DIR_SEP . $this->_getLocalFilePath($id), $data) !== false;
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return integer
     */
    public function getFileSize($id)
    {
        return filesize($this->_serverDir . DIR_SEP . $this->_getLocalFilePath($id));
    }

    /**
     * @param array|string $id
     *
     * @return boolean
     */
    public function isFileExists($id)
    {
        return file_exists($this->_serverDir . DIR_SEP . $this->_getLocalFilePath($id));
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    public function getWebPath($id)
    {
        $webCfg = $this->_serverInfo['web'];
        $urlParts = [
            'scheme' => $webCfg['scheme'],
            'host' => $webCfg['host'],
            'path' => $webCfg['path'] . DIR_SEP . $this->_getLocalFilePath($id),
        ];

        if (!in_array($webCfg['port'], [80, 433])) {
            $urlParts['port'] = $webCfg['port'];
        }

        return http_build_url('', $urlParts);
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    public function getFilename($id)
    {
        return basename($this->_getLocalFilePath($id));
    }

    /**
     * Рекурсивно создает директорию, в отличии от стандартной пхп функции она правильно раставляет флаги доступа
     *
     * @param string $path
     * @param string $basePath
     * @param integer $mode
     *
     * @return bool
     */
    protected function _makeDirectory($path, $basePath = '', $mode = 0777)
    {
        $pathNodes = explode(DIR_SEP, $path);
        $directory = rtrim($basePath, DIR_SEP);
        foreach ($pathNodes as $dir) {
            $directory .= DIR_SEP . $dir;
            if (!is_dir($directory) && !mkdir($directory, $mode)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param string $filename
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return boolean
     */
    public function copyFileToStorage($filename, $id)
    {
        if (is_string($filename) && is_file($filename)) {
            $localPath = $this->_getLocalFilePath($id);
            $pathname = explode(DIR_SEP, $localPath);
            array_pop($pathname);
            $pathname = implode(DIR_SEP, $pathname);

            if ($this->_makeDirectory($pathname, $this->_serverDir)) {
                //throw new Exception();
                $storageFilename = $this->_serverDir . DIR_SEP . $localPath;
                if (is_uploaded_file($filename)) {
                    return move_uploaded_file($filename, $storageFilename);
                } else {
                    return copy($filename, $storageFilename);
                }
            }
        }
        return false;
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $filename
     *
     * @return bool
     */
    public function copyFromStorage($id, $filename)
    {
        $pathname = explode(DIR_SEP, $filename);
        array_pop($pathname);
        @mkdir(implode(DIR_SEP, $pathname), 0777, true);
        return copy($this->_serverDir . DIR_SEP . $this->_getLocalFilePath($id), $filename);
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $key
     * @param mixed $value
     *
     * @return boolean
     */
    public function setData($id, $key, $value)
    {
        $key = crc32(Cast::toStr($key)); //crc32 для того что бы ключи весили не так много
        $metadataFilename = $this->_getMetadataFilePath($id);
        $rawMetadata = file_get_contents($metadataFilename);

        $metadata = $rawMetadata === false ? unserialize($rawMetadata) : [];
        $metadata[$key] = $value;

        $rawMetadata = serialize($metadata);

        return file_put_contents($metadataFilename, $rawMetadata);
    }

    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $key
     *
     * @return mixed|false on fail
     */
    public function getData($id, $key)
    {
        $key = crc32(Cast::toStr($key)); //crc32 для того что бы ключи весили не так много
        $metadataFilename = $this->_getMetadataFilePath($id);
        $rawMetadata = file_get_contents($metadataFilename);

        if ($rawMetadata !== false) {
            $metadata = unserialize($rawMetadata);
            return $metadata[$key];
        }

        return false;
    }
}
