<?php

namespace MommyCom\YiiComponent\FileStorage\Filesystem;

use MommyCom\YiiComponent\FileStorage\IdProtocolAbstract;
use MommyCom\YiiComponent\FileStorage\Type;

/**
 * Адаптер к стандартной файловой системе
 * Отличие от нулевой версии в том, что создается не 4 уровня а только 2
 * Отличие от первой версии в том, что имена файлов намного короче
 * Максимальное количество файлов в системе 256 ^ 2 * 32000
 */
class V2 extends V0
{
    /**
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    protected function _getLocalFilePath($id)
    {
        static $filepaths = [];
        static $idProtocolInstances = [];
        static $typeInstances = [];

        $fileKey = '';
        if (is_array($id) && isset($fileKey)) {
            list($id, $fileKey) = $id;
            $fileKey = dechex(crc32($fileKey));
        }

        $cacheKey = $id . '_' . $fileKey;

        //echo base64_encode($cacheKey); exit;

        if (isset($filepaths[$cacheKey])) {
            return $filepaths[$cacheKey];
        }

        if (empty($idProtocolInstances[$id])) {
            $idProtocolInstances[$id] = IdProtocolAbstract::factory($this->_storage, $id);
        }

        $idProtocol = $idProtocolInstances[$id];
        /* @var $idProtocol IdProtocolAbstract */

        $typeCode = $idProtocol->getType();

        if (empty($typeInstances[$typeCode])) {
            $typeInstances[$typeCode] = Type::factory($this->_storage, $typeCode);
        }
        $type = $typeInstances[$typeCode];

        $directoryTree = sha1($idProtocol->getId() . $this->_secretKey);
        $directoryTree = str_split($directoryTree, intval(strlen($directoryTree) / 2)); //разбиваем на два уровня вложености папок

        foreach ($directoryTree as &$directory) {
            $directory = dechex(fmod(hexdec($directory), 0xFF)); //делаем так чтобы количество папок на одном уровне было < 0xFFF (4095)
        }
        unset($directory);

        $path = implode(DIR_SEP, $directoryTree);
        $filename = $idProtocol->getEncodedId() . $fileKey . $type->getExtension(true);

        return $filepaths[$cacheKey] = $path . DIR_SEP . $filename;
    }
}
