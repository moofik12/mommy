<?php

namespace MommyCom\YiiComponent\FileStorage;

use MommyCom\YiiComponent\FileStorage\Filesystem\V0;
use MommyCom\YiiComponent\FileStorage\Filesystem\V1;
use MommyCom\YiiComponent\FileStorage\Filesystem\V2;

abstract class FilesystemAbstract
{
    const LOCK_WRITE = 1;
    const LOCK_READWRITE = 2;

    /**
     * @var FileStorage
     */
    protected $_storage;

    /**
     * @var integer
     */
    protected $_serverId;

    /**
     * @var array
     */
    protected $_serverInfo;

    /**
     * @param FileStorage $storage
     * @param integer $serverId
     */
    protected function __construct(FileStorage $storage, $serverId)
    {
        $this->_storage = $storage;
        $this->_serverId = $serverId;

        $config = $storage->getConfig();
        $this->_serverInfo = $config['servers'][$serverId];
    }

    /**
     * @return boolean
     */
    public function isAcceptNewFiles()
    {
        return isset($this->_serverInfo['acceptNewFiles']) ? $this->_serverInfo['acceptNewFiles'] : true;
    }

    /**
     * @return boolean
     */
    public function isAllowDeleteFiles()
    {
        return isset($this->_serverInfo['allowDeleteFiles']) ? $this->_serverInfo['allowDeleteFiles'] : true;
    }

    /**
     * Путь к файлу по http
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    abstract public function getWebPath($id);

    /**
     * Копирование файл в хранилище
     *
     * @param string $filename
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     */
    abstract public function copyFileToStorage($filename, $id);

    /**
     * Копирование файла из хранилища
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $filename
     */
    abstract public function copyFromStorage($id, $filename);

    /**
     * Удаление файла из хранилища
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return boolean
     */
    abstract public function removeFile($id);

    /**
     * Возвращает имя файла
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    abstract public function getFilename($id);

    /**
     * Добавление\изменение дополнительной информации связанной с файлом
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $key
     * @param mixed $value
     */
    abstract public function setData($id, $key, $value);

    /**
     * Получение дополнительной информации связанной с файлом
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $key false при ошибке
     */
    abstract public function getData($id, $key);

    /**
     * Получение содержимого файла
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return string
     */
    abstract public function getFileContent($id);

    /**
     * Установка содержимого файла
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     * @param string $data
     *
     * @return boolean
     */
    abstract public function setFileContent($id, $data);

    /**
     * Получение размера файла
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return int
     */
    abstract public function getFileSize($id);

    /**
     * Проверка на существование файла
     *
     * @param string|array $id (id файла как строка или связка array(id, fileKey))
     *
     * @return boolean
     */
    abstract public function isFileExists($id);

    /**
     * Фабрика для создания адаптера хранилища
     *
     * @param FileStorage $storage
     * @param integer $serverId
     *
     * @return FilesystemAbstract
     */
    public static function factory(FileStorage $storage, $serverId)
    {
        $servers = $storage->getConfig('servers');
        $serverInfo = $servers[$serverId];
        $fsVersions = [
            0 => V0::class,
            1 => V1::class,
            2 => V2::class,
        ];

        $filesystemClass = $fsVersions[(int)$serverInfo['fsVersion']];
        return new $filesystemClass($storage, $serverId);
    }
}
