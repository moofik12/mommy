<?php

namespace MommyCom\YiiComponent\FileStorage;

use Exception;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\Exception\Modifier\IsNotModifierException;
use MommyCom\YiiComponent\FileStorage\Exception\Modifier\ModifierNotExistsException;
use MommyCom\YiiComponent\FileStorage\Exception\Type\TypeMismatchException;
use MommyCom\YiiComponent\FileStorage\File\Unknown;
use MommyCom\YiiComponent\Type\Cast;
use MommyCom\YiiComponent\Type\Validate;

abstract class File
{

    /**
     * @var FileStorage
     */
    protected $_storage;

    /**
     * @var IdProtocolAbstract
     */
    protected $_idProtocol;

    /**
     * @var FilesystemAbstract
     */
    protected $_filesystem;

    /**
     * @var string
     */
    protected $_filename;

    /**
     * @var string
     */
    protected $_filedir;

    /**
     * @var Type
     */
    protected $_type;

    /**
     * @var array
     */
    protected $_newFileInfo;

    /**
     * @var boolean
     */
    protected $_isNewFile;

    const ERROR_UNKNOWN = 1;
    const ERROR_IS_NOT_FILE = 2;
    const ERROR_TOO_LARGE = 3;
    const ERROR_TYPE_UNKNOWN = 4;
    const ERROR_TYPE_MISSMATCH = 5;
    const ERROR_FILE_MOVED_PERMANENTLY = 301;
    const ERROR_FILE_NOT_FOUND = 404;
    const ERROR_INTERNAL_SERVER_ERROR = 500;
    const ERROR_BAD_GETEWAY = 502;

    /**
     * @param FileStorage $storage
     * @param IdProtocolAbstract $idProtocol
     * @param Type $type
     */
    protected function __construct(FileStorage $storage, IdProtocolAbstract $idProtocol, Type $type)
    {
        $this->_storage = $storage;
        $this->_idProtocol = $idProtocol;
        $this->_type = $type;
        $this->_isNewFile = false;
    }

    public function __destruct()
    {
        $this->_moveToStorage();
    }

    /**
     * Форсированное сохранение файла
     *
     * @return boolean
     */
    public function save()
    {
        return $this->_moveToStorage();
    }

    /**
     * @return ModifierInterface[]
     * @throws IsNotModifierException
     * @throws ModifierNotExistsException
     */
    public function _getModifiers()
    {
        $modifiers = [];
        $modifiersSettings = $this->_storage->getConfig('mime.modifiers');
        $engineType = explode('/', $this->_type->getEngineMimeType());
        $typeHierarhy = [];

        $createOptions = isset($this->_newFileInfo['createOptions']) ? $this->_newFileInfo['createOptions'] : [];

        foreach ($engineType as $type) {
            $typeHierarhy[] = $type;
            $path = implode('.', $typeHierarhy);
            if (ArrayUtils::isValidPath($modifiersSettings, $path)) {
                $typeModifiersSettings = ArrayUtils::getValueByPath($modifiersSettings, $path);

                // merge with addition createOptions
                foreach ($typeModifiersSettings as &$typeModifierSettings) {
                    foreach ($createOptions as $key => $extraSettings) {
                        if ($typeModifierSettings['name'] == $extraSettings['name']) {
                            $typeModifierSettings['createOptions'] = $extraSettings['options'];
                            unset($createOptions[$key]);
                        }
                    }
                }
                unset($typeModifierSettings);
                if (count($createOptions) > 0) {
                    foreach ($createOptions as $extraSettings) {
                        $typeModifiersSettings[] = $extraSettings;
                    }
                }
                // end

                foreach ($typeModifiersSettings as $typeModifierSettings) {
                    $modifierClassName = $typeModifierSettings['name'];

                    if (is_string($modifierClassName) && class_exists($modifierClassName, true)) {
                        $modifierCreateOptions = isset($typeModifierSettings['createOptions']) ? $typeModifierSettings['createOptions'] : [];

                        $modifier = new $modifierClassName($this, $typeModifierSettings['options'], $modifierCreateOptions);
                        if (!$modifier instanceof ModifierInterface) {
                            throw new IsNotModifierException();
                        }
                        $modifiers[] = $modifier;
                    } else {
                        throw new ModifierNotExistsException();
                    }
                }
            }
        }

        return $modifiers;
    }

    protected function _applyModifiers()
    {
        $modifiers = $this->_getModifiers();
        foreach ($modifiers as $modifier) {
            $modifier->apply();
        }
    }

    protected function _removeModifiers()
    {
        $modifiers = $this->_getModifiers();
        foreach ($modifiers as $modifier) {
            $modifier->remove();
        }
    }

    /**
     * @param array $fileInfo
     * @param array $createOptions
     */
    protected function _setNewFileInfo(array $fileInfo, array $createOptions = [])
    {
        $this->_isNewFile = true;
        $this->_newFileInfo = $fileInfo;
        $this->_newFileInfo['createOptions'] = $createOptions;
    }

    /**
     * @return boolean
     */
    protected function _moveToStorage()
    {
        if ($this->_isNewFile) {
            $this->_isNewFile = false;
            $sendedToStorage = $this->getFilesystem()->copyFileToStorage($this->_newFileInfo['tmp_name'], $this->_idProtocol->getId());
            if ($sendedToStorage) {
                $this->_applyModifiers();
                @unlink($this->_newFileInfo['tmp_name']);
            }
            return $sendedToStorage;
        }
        return false;
    }

    /**
     * Возвращает локальный путь к файлу (внимание если файл не является новым то он будет скопирован в временную локальную папку)
     *
     * @return string|false filename if ok, false on failure
     */
    protected function _getLocalFilename()
    {
        if ($this->isNewFile()) {
            return $this->_newFileInfo['tmp_name'];
        } else {
            static $filenames = [];
            $id = $this->getEncodedId();
            if (!isset($filenames[$id]) || !is_file($filenames[$id])) {
                $totalFiles = count($filenames) + 1;
                $dir = sys_get_temp_dir();
                $name = $totalFiles . '_' . $this->getFilename(); // не допускаем перезаписи файлов с таким же именем
                $localFilename = $dir . DIR_SEP . $name;
                if ($this->getFilesystem()->copyFromStorage($this->getId(), $localFilename)) {
                    $filenames[$id] = $localFilename;
                } else {
                    $filenames[$id] = false;
                }
            }
            return $filenames[$id];
        }
        return false;
    }

    /**
     * Проверяет является ли файл корректным
     *
     * @return boolean
     */
    abstract public function validateContent();

    /**
     * @return \MommyCom\YiiComponent\FileStorage\FileStorage
     */
    public function getStorage()
    {
        return $this->_storage;
    }

    /**
     * Возвращает экземпляр класса хранящего\парсящего айди файла
     *
     * @return IdProtocolAbstract
     */
    public function getIdProtocol()
    {
        return $this->_idProtocol;
    }

    /**
     * Возращает экземпляр класса управляюшего типами файлов
     *
     * @return Type
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @return FilesystemAbstract
     */
    public function getFilesystem()
    {
        if (empty($this->_filesystem)) {
            $this->_filesystem = FilesystemAbstract::factory($this->_storage, $this->_idProtocol->getServerId());
            $this->_moveToStorage();
        }
        return $this->_filesystem;
    }

    /**
     * @return boolean
     */
    public function isNewFile()
    {
        return $this->_isNewFile;
    }

    /**
     * @param bool $removeModifiers удалять так же возможные дополнительные файлы созданные модификаторами (например превью изображний)
     *
     * @return boolean
     */
    public function remove($removeModifiers = true)
    {
        if ($this->isNewFile()) {
            $this->_isNewFile = false;
            @unlink($this->_newFileInfo['tmp_name']);
        } else {
            $result = $this->getFilesystem()->removeFile($this->getId());
            if ($removeModifiers && $result) {
                $this->_removeModifiers();
            }
            return $result;
        }
    }

    /**
     * @return boolean существование файла
     */
    public function isExists()
    {
        if ($this->isNewFile()) {
            return is_file($this->_newFileInfo['tmp_name']);
        } else {
            return $this->getFilesystem()->isFileExists($this->getId());
        }
    }

    /**
     * @return int размер файла в байтах
     */
    public function getFileSize()
    {
        if ($this->isNewFile()) {
            return filesize($this->_newFileInfo['tmp_name']);
        } else {
            return $this->getFilesystem()->getFileSize($this->getId());
        }
    }

    /**
     * Возращает айди файла
     *
     * @return string
     */
    public function getId()
    {
        return $this->_idProtocol->getId();
    }

    /**
     * Возвращает айди файла (пригодное для вставки в текст документа и\или в URL)
     *
     * @return string
     */
    public function getEncodedId()
    {
        return $this->_idProtocol->encodeId($this->getId());
    }

    /**
     * Возращает имя файла
     *
     * @return string
     */
    public function getFilename()
    {
        $this->_moveToStorage();
        return $this->getFilesystem()->getFilename($this->getId());
    }

    /**
     * возвращает имя файла без расширения
     *
     * @return string
     */
    public function getFilenameWithoutExt()
    {
        $filename = $this->getFilename();
        $explodedFilename = explode('.', $filename);
        if (count($explodedFilename) > 1) {
            array_pop($explodedFilename);
        }
        return implode('.', $explodedFilename);
    }

    /**
     * Возвращает расширение файла
     *
     * @return boolean
     */
    public function getFilenameExt()
    {
        return $this->getType()->getExtension(true);
    }

    /**
     * Полный путь к файлу по http
     *
     * @return string путь к файлу
     */
    public function getWebFullPath()
    {
        return $this->getFilesystem()->getWebPath($this->getId());
    }

    /**
     * Копирование файла из хранилища
     *
     * @param string $filename
     *
     * @return boolean
     */
    public function copyTo($filename)
    {
        return $this->getFilesystem()->copyFromStorage($this->getId(), $filename);
    }

    /**
     * Создает дубликат текущего файла, с <b>новым id</b>, <b>новым адрессом</b> и скорее всего <b>другим сервером</b> (как кости лягут)
     *
     * @param array $createOptions настройки специфичные для этого файла (передается в качестве $createOptions в модификаторы)
     *
     * @return File экземпляр класса для работы с файлами, при ошибке ее код
     */
    public function duplicate(array $createOptions = [])
    {
        $filename = $this->_getLocalFilename();
        $file = static::factoryFromUploadedFile($this->getStorage(), $filename, '', $createOptions);
        $file->save();
        @unlink($filename);
        return $file;
    }

    /**
     * Замена файла другим
     *
     * @param string $filename
     *
     * @return boolean
     */
    public function overrideWith($filename)
    {
        return $this->getFilesystem()->copyFileToStorage($filename, $this->getId());
    }

    /**
     * @param callable $callback колбек, в него передается имя локального файла с которым можно работать а затем измененым файлом перезаливается старый
     *
     * @throws TypeMismatchException
     * @return boolean true если в итоге файл был обновлен
     */
    public function workWithFile($callback)
    {
        if (!is_callable($callback)) {
            throw new TypeMismatchException('$callback must be of type Callable');
        }
        $localFilename = $this->_getLocalFilename();

        $result = false;
        if (call_user_func($callback, $localFilename) !== false) {
            $result = $this->overrideWith($localFilename);
        }

        if (!$this->isNewFile()) {
            try {
                unlink($localFilename); //лишний раз не мусорим
            } catch (Exception $e) {
            }
        }
        return $result;
    }

    /**
     * Фабрика для содания экземпляра файла
     *
     * @param FileStorage $storage
     * @param string $id
     *
     * @return File
     */
    public static function factory(FileStorage $storage, $id)
    {
        $idProtocol = IdProtocolAbstract::factory($storage, $id);
        $type = Type::factory($storage, $idProtocol->getType());

        $engineMime = $type->getEngineMimeType();
        $engineMimeHierarhy = explode('/', $engineMime);
        $fileClassName = __NAMESPACE__ . '\\File\\' . implode('\\', array_map('ucfirst', $engineMimeHierarhy));

        if (!class_exists($fileClassName, true)) {
            $fileClassName = Unknown::class;
        }

        return new $fileClassName($storage, $idProtocol, $type);
    }

    /**
     * Фабрика для содания экземпляра файла
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $id
     *
     * @return File
     */
    public static function factoryFromEncodedId(FileStorage $storage, $id)
    {
        return self::factory($storage, IdProtocolAbstract::factoryFromEncodedId($storage, $id));
    }

    /**
     * @param FileStorage $storage
     * @param string $filename
     * @param string|array $validType внутренний mime файла которые принимаются, напр. image/* or image/jpeg
     * @param array $createOptions настройки специфичные для этого файла (передается в качестве $createOptions в модификаторы)
     *
     * @return File|integer
     */
    public static function factoryFromLocalFile(FileStorage $storage, $filename, $validType = '', array $createOptions = [])
    {
        if (Validate::isStr($filename) && is_file($filename)) {
            return self::factoryFromUploadedFile($storage, $filename, $validType, $createOptions);
        }
        return self::ERROR_IS_NOT_FILE;
    }

    /**
     * @return string
     */
    public static function getTempFileName()
    {
        return tempnam(sys_get_temp_dir(), 'fileslist');
    }

    /**
     * @param $fileName
     *
     * @return mixed|string
     */
    public static function getFileExtension($fileName)
    {
        $explodedFileName = explode('.', $fileName);
        $extension = '';
        if (count($fileName) > 1) {
            $extension = end($explodedFileName);
        }
        return $extension;
    }

    /**
     * Возвращает статус удаленого файла
     *
     * @param $remoteFileUrl
     *
     * @return int|mixed
     */
    public static function remoteFileStatus($remoteFileUrl)
    {
        $statusCode = 0;
        $curl = curl_init($remoteFileUrl);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        $result = curl_exec($curl);
        if ($result !== false) {
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        }
        curl_close($curl);
        return $statusCode;
    }

    /**
     * Загрузка файла с удаленного сервера
     *
     * @param FileStorage $storage
     * @param string $remoteFilename
     * @param integer $maxSize
     * @param string $validType
     * @param array $createOptions
     *
     * @throws FileStorageException
     * @return File
     */
    public static function factoryFromRemoteFile(FileStorage $storage, $remoteFilename, $maxSize, $validType = '', array $createOptions = [])
    {
        $maxSize = Cast::toUInt($maxSize);
        if (Validate::isStr($remoteFilename)) {
            $statusCode = self::remoteFileStatus($remoteFilename);
            switch ($statusCode) {
                case 301:
                    return self::ERROR_FILE_MOVED_PERMANENTLY;
                    break;
                case 404:
                    return self::ERROR_FILE_NOT_FOUND;
                    break;
                case 500:
                    return self::ERROR_INTERNAL_SERVER_ERROR;
                    break;
                case 502:
                    return self::ERROR_BAD_GETEWAY;
                    break;
                default:
                    $localTempFileName = self::getTempFileName(self::getFileExtension($remoteFilename));
                    try {
                        $remoteFileHandle = fopen($remoteFilename, 'r');
                    } catch (Exception $exeption) {
                        throw new Exception("");
                    }

                    $localFileHandle = fopen($localTempFileName, 'w');
                    $bytesTransfered = 0;
                    $packetSize = 32000;
                    $error = false;
                    while (!feof($remoteFileHandle)) {
                        $packet = fread($remoteFileHandle, $packetSize);
                        $bytesTransfered += $packetSize;
                        fwrite($localFileHandle, $packet);
                        if ($bytesTransfered > $maxSize) {
                            $error = true;
                            break;
                        }
                    }
                    fclose($remoteFileHandle);
                    fclose($localFileHandle);
                    if ($error) {
                        @unlink($localTempFileName);
                        return self::ERROR_TOO_LARGE;
                    } else {
                        return self::factoryFromLocalFile($storage, $localTempFileName, $validType, $createOptions);
                    }
                    break;
            }
        }
        return self::ERROR_IS_NOT_FILE;
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string|array $file ключ в массиве $_FILES или сам массив с информацией о файле
     * @param string|array $validType внутренний mime файла которые принимаются, напр. image/* or image/jpeg
     * @param array $createOptions настройки специфичные для этого файла (передается в качестве $createOptions в модификаторы)
     *
     * @return File|integer экземпляр класса для работы с файлами, при ошибке ее код
     */
    public static function factoryFromUploadedFile(FileStorage $storage, $file, $validType = '', array $createOptions = [])
    {
        if (Validate::isStr($file)) {
            if (is_file($file)) {
                $file = [ //создание массива идентичного тому который выдает $_FILES['somefile']
                    'type' => finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file, FILEINFO_MIME),
                    'size' => filesize($file),
                    'error' => UPLOAD_ERR_OK,
                    'name' => basename($file),
                    'tmp_name' => $file,
                ];
            } else {
                return self::ERROR_IS_NOT_FILE;
            }
        }
        if ($file['error'] == UPLOAD_ERR_OK) {
            $idProtocol = IdProtocolAbstract::factoryFromLocalFile(
                $storage, $file['tmp_name']
            );

            $storedFile = self::factory($storage, $idProtocol);

            $storedFile->_setNewFileInfo($file, $createOptions);

            if (!$storedFile->validateContent()) { //валидация внутренней структуры файла
                $storedFile = self::ERROR_TYPE_MISSMATCH;
            }

            if (!empty($validType) && $storedFile instanceof File) { //валидация миме файла
                if (!$storedFile->getType()->isMatch($validType)) {
                    $storedFile = self::ERROR_TYPE_MISSMATCH;
                }
            }

            return $storedFile;
        }
        return self::ERROR_UNKNOWN;
    }

    public static function factoryFromBinaryString(FileStorage $storage, $string, $validType = '', array $createOptions = [])
    {
        if (Validate::isStr($string)) {
            $filename = tempnam(sys_get_temp_dir(), 'binfile');
            file_put_contents($filename, $string);
            return self::factoryFromLocalFile($storage, $filename, $validType, $createOptions);
        }
        return self::ERROR_IS_NOT_FILE;
    }

    public static function factoryFromBase64String(FileStorage $storage, $string, $validType = '', array $createOptions = [])
    {
        if (Validate::isStr($string)) {
            return self::factoryFromBinaryString($storage, base64_decode($string), $validType, $createOptions);
        }
        return self::ERROR_IS_NOT_FILE;
    }

    public function __toString()
    {
        return $this->getIdProtocol()->__toString();
    }
}

