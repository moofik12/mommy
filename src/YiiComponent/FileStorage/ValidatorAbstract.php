<?php

namespace MommyCom\YiiComponent\FileStorage;

use MommyCom\YiiComponent\FileStorage\Validator\FileSize;

abstract class ValidatorAbstract
{
    public static $buildinValidators = [
        'fileSize' => FileSize::class,
    ];

    abstract public function validate(File $file, array $params);
}
