<?php

namespace MommyCom\YiiComponent\FileStorage\Cache;

use MommyCom\YiiComponent\FileStorage\CacheAbstract;

/**
 * @package FileStorage
 * @author scriptbunny
 */
class Redis extends CacheAbstract
{

    /**
     * Добавляет значение в кеш
     *
     * @param string $key
     * @param mixed $value
     * @param integer|null $ttl
     */
    protected function _addToCache($key, $value, $ttl = null)
    {
        // TODO: Implement _addToCache() method.
    }

    /**
     * Закешировано ли значение
     *
     * @param string $key
     *
     * @return boolean
     */
    protected function _getIsCached($key)
    {
        // TODO: Implement _getIsCached() method.
    }

    /**
     * @param string
     *
     * @return mixed
     */
    protected function _getFromCache($key)
    {
        // TODO: Implement _getFromCache() method.
    }

    /**
     * Удаляет значение из кеша
     *
     * @param string $key
     *
     * @return void
     */
    protected function _removeFromCache($key)
    {
        // TODO: Implement _removeFromCache() method.
    }

    /**
     * Чистит кеш
     *
     * @return void
     */
    protected function _flushCache()
    {
        // TODO: Implement _flushCache() method.
    }
}
