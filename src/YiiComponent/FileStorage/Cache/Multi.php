<?php

namespace MommyCom\YiiComponent\FileStorage\Cache;

use MommyCom\YiiComponent\FileStorage\CacheAbstract;

/**
 * @package FileStorage
 * @author scriptbunny
 */
class Multi extends CacheAbstract
{
    /**
     * @var array
     */
    protected $_cacheProviders;

    public function __construct($storage)
    {
        $this->_cacheProviders = [
            'Apc' => CacheAbstract::factory($storage, Apc::class),
            'Redis' => CacheAbstract::factory($storage, Redis::class),
        ];
    }

    protected function _addToCache($key, $value, $ttl = null)
    {
    }

    protected function _getIsCached($key)
    {
    }

    protected function _removeFromCache($key)
    {
    }

    protected function _getFromCache($key)
    {
    }

    protected function _flushCache()
    {
    }
}
