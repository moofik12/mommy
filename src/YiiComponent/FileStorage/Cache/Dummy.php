<?php

namespace MommyCom\YiiComponent\FileStorage\Cache;

use MommyCom\YiiComponent\FileStorage\CacheAbstract;

/**
 * @package FileStorage
 * @author scriptbunny
 */
class Dummy extends CacheAbstract
{
    protected function _addToCache($key, $value, $ttl = null)
    {
    }

    protected function _getIsCached($key)
    {
        return false;
    }

    protected function _removeFromCache($key)
    {
    }

    protected function _getFromCache($key)
    {
    }

    protected function _flushCache()
    {
    }
}

