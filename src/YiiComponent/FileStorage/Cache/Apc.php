<?php

namespace MommyCom\YiiComponent\FileStorage\Cache;

use MommyCom\YiiComponent\FileStorage\CacheAbstract;

/**
 * @package FileStorage
 * @author scriptbunny
 */
class Apc extends CacheAbstract
{
    //const MAX_ELEMENTS_IN_CACHE = 10000;

    protected function _addToCache($key, $value, $ttl = null)
    {
        return apc_add($key, $value, $ttl);
    }

    protected function _getIsCached($key)
    {
        return apc_exists($key);
    }

    protected function _removeFromCache($key)
    {
        return apc_delete($key);
    }

    protected function _getFromCache($key)
    {
        return apc_fetch($key);
    }

    protected function _flushCache()
    {
        return apc_clear_cache('user');
    }

}
