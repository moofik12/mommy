<?php

namespace MommyCom\YiiComponent\FileStorage;

use finfo;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;

class Type
{
    protected $_storage;
    protected $_typeCode;

    protected function __construct(FileStorage $storage, $typeCode)
    {
        $this->_storage = $storage;
        $this->_typeCode = $typeCode;
    }

    /**
     * @return integer
     */
    public function getTypeCode()
    {
        return $this->_typeCode;
    }

    /**
     * @param boolean $withDot добавить точку (может быть добавлена только к не пустому типу файла)
     *
     * @return string предпочтительное расширение файла для данного типа
     */
    public function getExtension($withDot = false)
    {
        $extension = self::getMimeTypeExtension($this->_storage, $this->getMimeType());
        if ($withDot && !empty($extension)) {
            $extension = '.' . $extension;
        }
        return $extension;
    }

    /**
     * Возвращает тип файла
     *
     * @return string
     */
    public function getMimeType()
    {
        return self::_convertTypeCodeToMimeType($this->_storage, $this->_typeCode);
    }

    /**
     * Взвращает системный тип файла
     *
     * @return string
     */
    public function getEngineMimeType()
    {
        return self::_convertTypeCodeToEngineMimeType($this->_storage, $this->_typeCode);
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param integer $typeCode
     *
     * @return string
     */
    protected static function _convertTypeCodeToMimeType(FileStorage $storage, $typeCode)
    {
        $supportedMimeTypes = $storage->getConfig('mime.supportedMimes');
        $engineMimeType = self::_convertTypeCodeToEngineMimeType($storage, $typeCode);
        $engineMimeTypeValue = explode('/', $engineMimeType);
        return ArrayUtils::findPathByValue($supportedMimeTypes, $engineMimeTypeValue, '/');
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $mimeType
     *
     * @return integer
     */
    protected static function _convertMimeTypeToTypeCode(FileStorage $storage, $mimeType)
    {
        $engineMime = self::_convertMimeTypeToEngineMimeType($storage, $mimeType);
        return $storage->getConfig('mime.engineMimes.' . str_replace('/', '.', $engineMime));
    }

    /**
     * @param FileStorage $storage
     * @param integer $typeCode
     *
     * @return string
     */
    protected static function _convertTypeCodeToEngineMimeType(FileStorage $storage, $typeCode)
    {
        $engineMimeTypes = $storage->getConfig('mime.engineMimes');
        return ArrayUtils::findPathByValue($engineMimeTypes, $typeCode, '/');
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $mimeType
     *
     * @return integer
     */
    protected static function _convertMimeTypeToEngineMimeType(FileStorage $storage, $mimeType)
    {
        $mimeType = self::_sanitizeMimeType($storage, $mimeType);
        return implode('/', $storage->getConfig("mime.supportedMimes.{$mimeType}"));
    }

    /**
     * Получение расширения файла по его миме типу
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $mimeType
     *
     * @return string
     */
    protected static function getMimeTypeExtension(FileStorage $storage, $mimeType)
    {
        return self::_getEngineMimeTypeExtension($storage,
            self::_convertMimeTypeToEngineMimeType($storage, $mimeType)
        );
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $mimeType
     *
     * @return string
     */
    protected static function _getEngineMimeTypeExtension(FileStorage $storage, $mimeType)
    {
        $mimeType = str_replace('/', '.', $mimeType);
        return $storage->getConfig("mime.engineMimesExtensions.{$mimeType}");
    }

    /**
     * Чистит миме тип файла
     *
     * @param FileStorage $storage
     * @param string $mimeType
     *
     * @return string
     */
    protected static function _sanitizeMimeType(FileStorage $storage, $mimeType)
    {
        $isMimeSupported = ArrayUtils::isValidPath($storage->getConfig(), "mime.supportedMimes.{$mimeType}");
        if (!$isMimeSupported) {
            $mimeType = 'unknown/unknown';
        }
        return $mimeType;
    }

    /**
     * Проверка на соответствие миме файла определенным типам
     *
     * @param array|string $pattern тип(ы) файлов, напр (array('image/*', 'video/*') или 'image/*' или 'image/jpeg')
     * @param string $delimiter
     *
     * @return boolean
     */
    public function isMatch($pattern, $delimiter = '/')
    {
        $types = array_map(function ($value) use ($delimiter) {
            return explode($delimiter, $value);
        }, Cast::toArr($pattern));
        $mime = explode('/', $this->getEngineMimeType());
        $mimeSize = count($mime);
        $isTypeMatched = false;
        foreach ($types as $type) {
            foreach ($mime as $index => $mimeNode) {
                $isLastNode = $mimeSize == $index + 1;
                $typeNode = isset($type[$index]) ? $type[$index] : null;

                if ($typeNode === '*' || ($isLastNode && $typeNode == $mimeNode)) { //тип соответствует
                    $isTypeMatched = true;
                    break 2;
                } else if (is_null($typeNode) || $typeNode !== $mimeNode) { //тип не соответствует
                    continue 2;
                }
            }
        }
        return $isTypeMatched;
    }

    /**
     * @param FileStorage $storage
     * @param integer $typeCode
     *
     * @return self
     */
    public static function factory(FileStorage $storage, $typeCode)
    {
        return new self($storage, $typeCode);
    }

    /**
     * @param FileStorage $storage
     * @param string $filename
     *
     * @return self
     */
    public static function factoryFromLocalFile(FileStorage $storage, $filename)
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($filename);
        unset($finfo);
        if (!$mime) {
            $mime = 'unknown/unknown';
        }
        return self::factoryFromMimeType($storage, $mime);
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $mime
     *
     * @return Type
     */
    public static function factoryFromMimeType(FileStorage $storage, $mime)
    {
        return self::factory($storage, self::_convertMimeTypeToTypeCode($storage, $mime));
    }

    /**
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param File $file
     *
     * @return self
     */
    public static function factoryFromFile(FileStorage $storage, File $file)
    {
        return self::factory($storage, $file->getIdProtocol()->getType());
    }
}
