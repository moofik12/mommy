<?php

namespace MommyCom\YiiComponent\FileStorage;

use MommyCom\YiiComponent\FileStorage\Exception\Cache\InvalidProviderException;

abstract class CacheAbstract
{
    /**
     * @var FileStorage
     */
    protected $_storage;

    /**
     * @param FileStorage $storage
     */
    protected function __construct(FileStorage $storage)
    {
        $this->_storage = $storage;
    }

    /**
     * Добавляет значение в кеш
     *
     * @param string $key
     * @param mixed $value
     * @param integer|null $ttl
     */
    abstract protected function _addToCache($key, $value, $ttl = null);

    /**
     * Закешировано ли значение
     *
     * @param string $key
     *
     * @return boolean
     */
    abstract protected function _getIsCached($key);

    /**
     * @param string
     *
     * @return mixed
     */
    abstract protected function _getFromCache($key);

    /**
     * Удаляет значение из кеша
     *
     * @param string $key
     *
     * @return void
     */
    abstract protected function _removeFromCache($key);

    /**
     * Чистит кеш
     *
     * @return void
     */
    abstract protected function _flushCache();

    /**
     * @param string $id
     * @param callback $notFoundCallback
     *
     * @return File
     */
    public function getFileFromCache($id, $notFoundCallback)
    {
        if ($this->_getIsCached($id)) {
            return $this->_getFromCache($id);
        } else {
            $file = call_user_func($notFoundCallback, $id);
            $this->_addToCache($id, $file);
            return $file;
        }
    }

    public function removeFileFromCache($id)
    {
    }

    /**
     * Создает экземпляр класса работы с кешем
     *
     * @param \MommyCom\YiiComponent\FileStorage\FileStorage $storage
     * @param string $cacheProviderClass
     *
     * @return CacheAbstract
     * @throws InvalidProviderException
     */
    public static function factory(FileStorage $storage, $cacheProviderClass)
    {
        if (!empty($cacheProviderClass) && is_string($cacheProviderClass)) {
            if (class_exists($cacheProviderClass, true)) {
                return new $cacheProviderClass($storage);
            } else {
                throw new InvalidProviderException;
            }
        } else {
            throw new InvalidProviderException;
        }
    }
}
