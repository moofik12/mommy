<?php

namespace MommyCom\YiiComponent\FileStorage;

use MommyCom\YiiComponent\FileStorage\File\Image;

class ThumbLink
{
    /**
     * Имя (может быть пвсевдонимом, например в настроках 'mini' => ':small32crop', здесь будет 'mini' но на самом деле ссылка будет на 'small32crop')
     *
     * @var string
     */
    public $name;

    /**
     * Настоящее имя
     *
     * @var string
     */
    public $realName;

    /**
     * Url
     *
     * @var string
     */
    public $url;

    /**
     * Тумба взята из дефолтовых значений (на самом деле файл не существует)
     *
     * @var boolean
     */
    public $isPseudo;

    /**
     * @var Image|null
     */
    public $file;
}
