<?php

namespace MommyCom\YiiComponent\FileStorage\Modifiers\Image;

use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\Exception\NotImplementedException;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\ModifierInterface;

class AutoRotate implements ModifierInterface
{
    /**
     * @var File
     */
    protected $_file;

    /**
     * @var array
     */
    protected $_options;

    /**
     * @param File $file
     * @param array $options
     * @param array $createOptions
     */
    public function __construct(File $file, array $options, array $createOptions = [])
    {
        $this->_file = $file;
        $this->_options = ArrayUtils::merge($options, $createOptions);
    }

    public function apply()
    {
        throw new NotImplementedException;
    }

    /**
     * вызывается когда удаляется файл
     *
     * @return void
     */
    public function remove()
    {
    }
}
