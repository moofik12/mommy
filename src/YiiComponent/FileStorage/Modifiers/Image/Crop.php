<?php

namespace MommyCom\YiiComponent\FileStorage\Modifiers\Image;

use Imagick;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\ModifierInterface;
use MommyCom\YiiComponent\Type\Cast;

class Crop implements ModifierInterface
{
    /**
     * @var File
     */
    protected $_file;

    /**
     * @var array
     */
    protected $_options;

    const DEFAULT_QUALITY_RATE = 90; //качество картинки по умолчанию

    /**
     * @param File $file
     * @param array $options
     * @param array $createOptions
     */
    public function __construct(File $file, array $options, array $createOptions = [])
    {
        $this->_file = $file;
        $this->_options = ArrayUtils::merge($options, $createOptions);
    }

    /**
     * @access protected
     *
     * @param string $filename
     *
     * @return bool
     */
    public function _crop($filename)
    {
        $options = ArrayUtils::merge([
            'x' => 0,
            'y' => 0,
            'width' => 0,
            'height' => 0,
            'quality' => 90,
            'aspectRatio' => false,
            'maxWidth' => false,
            'maxHeight' => false,
            'minWidth' => false,
            'minHeight' => false,
            'resizeWidth' => 0,
            'resizeHeight' => 0,
        ], $this->_options);

        list(
            $x, $y,
            $width, $height,
            $quality, $aspectRatio,
            $maxWidth, $maxHeight,
            $minWidth, $minHeight,
            $resizeWidth, $resizeHeight
            ) = array_values($options);

        $maxWidth = $maxWidth == 0 ? $width : $maxWidth;
        $maxHeight = $maxHeight == 0 ? $height : $maxHeight;

        $minWidth = $minWidth == 0 ? $width : $minWidth;
        $minHeight = $minHeight == 0 ? $height : $minHeight;

        $width = Cast::toInt($width, 1, $minWidth, $maxWidth);
        $height = Cast::toInt($height, 1, $minHeight, $maxHeight);

        if (isset($aspectRatio) && $aspectRatio > 0) {
            $height = (int)round($width * $aspectRatio);
        }

        $image = new Imagick($filename);
        $image = $image->current(); // only first frame
        $image->setImagePage(0, 0, 0, 0);

        $imageGeometry = $image->getImageGeometry();

        if ($x == 0 && $y == 0 &&
            $width == $imageGeometry['width'] && $height == $imageGeometry['height']) {
            // для обрезки было выбрано все изображение, а значит она не требуется, но на всякий случай пересохраняем т.к. у нас запрещена анимация
            $image->writeimage();
            return true;
        }

        $image->cropImage($width, $height, $x, $y);

        if ($resizeWidth > 0 || $resizeHeight > 0) {
            if ($resizeWidth == 0) {
                $resizeWidth = $width;
            }
            if ($resizeHeight == 0) {
                $resizeHeight = $height;
            }
            $image->resizeimage($resizeWidth, $resizeHeight, Imagick::FILTER_LANCZOS, 1, false);
        }

        $image->setImageCompressionQuality(Cast::toInt(
            $quality, self::DEFAULT_QUALITY_RATE, 1, 100
        ));

        $image->writeimage();

        unset($image);

        return true;
    }

    public function apply()
    {
        if (!empty($this->_options)) {
            $this->_file->workWithFile([$this, '_crop']);
        }
    }

    /**
     * вызывается когда удаляется файл
     *
     * @return void
     */
    public function remove()
    {
        // do nothing
    }
}
