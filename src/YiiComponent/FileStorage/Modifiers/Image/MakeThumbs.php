<?php

namespace MommyCom\YiiComponent\FileStorage\Modifiers\Image;

use Exception;
use Imagick;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\File\Image;
use MommyCom\YiiComponent\FileStorage\FileStorage;
use MommyCom\YiiComponent\FileStorage\IdProtocolAbstract;
use MommyCom\YiiComponent\FileStorage\ModifierInterface;
use MommyCom\YiiComponent\FileStorage\Type;
use MommyCom\YiiComponent\Type\Cast;

class MakeThumbs implements ModifierInterface
{
    /**
     * @var Image
     */
    protected $_file;

    /**
     * @var array
     */
    protected $_options;

    /**
     * @var array
     */
    protected static $_validResizeMethods = ['propResize', 'crop'];

    const DEFAULT_QUALITY_RATE = 90; //качество картинки по умолчанию
    const DEFAULT_RESIZE_METHOD = 'propResize'; //метод ресайза по умолчанию

    /**
     * @param File $file
     * @param array $options
     * @param array $createOptions
     */
    public function __construct(File $file, array $options, array $createOptions = [])
    {
        $this->_file = $file;
        $this->_options = ArrayUtils::merge($options, $createOptions);
    }

    /**
     * @param FileStorage $storage
     * @param string $id
     * @param $thumbName
     *
     * @return string|false $id false on fail
     */
    public static function getThumbId(FileStorage $storage, $id, $thumbName)
    {
        $idProtocol = IdProtocolAbstract::factory($storage, $id);
        if ((is_string($id) || is_array($id)) && is_string($thumbName)) {
            $key = crc32('tmb_' . $thumbName);
            $idProtocol->setType(
                Type::factoryFromMimeType($storage, 'image/jpeg')->getTypeCode()
            );
            $idProtocol->setTag($key);
            return $idProtocol->getId();
        }
        return false;
    }

    /**
     * @access protected
     *
     * @param string $filename
     *
     * @return bool
     */
    public function _generateThumbs($filename)
    {
        $options = $this->_options;

        if (empty($options['resolution']) || !is_array($options['resolution'])) {
            return false;
        }

        $originalImage = new Imagick($filename);
        $originalImage = $originalImage->current(); // only first frame

        $originalImage->setImagePage(0, 0, 0, 0);
        $imageGeometry = $originalImage->getImageGeometry();

        foreach ($options['resolution'] as $thumbResolution) {
            $thumbImage = clone $originalImage;

            if (is_array($thumbResolution)) {
                list($name, $width, $height, $quality, $method, $addOpts) = $thumbResolution + [
                    '', 0, 0, 0, '', [],
                ];

                $addOpts += [
                    'blur' => 1,
                    'openCV' => '',
                ];

                if (!empty($name) && $width > 0 && $height > 0) {
                    $method = in_array($method, self::$_validResizeMethods) ? $method : self::DEFAULT_RESIZE_METHOD;

                    if (!empty($addOpts) && is_array($addOpts)) {
                        //here code for specified additional options
                    }

                    switch ($method) {
                        case 'propResize':
                            $ratio = $imageGeometry['width'] / $imageGeometry['height'];
                            if ($imageGeometry['width'] > $imageGeometry['height']) {
                                $thumbImage->resizeImage($width, round($width / $ratio), Imagick::FILTER_CATROM, $addOpts['blur'], false);
                            } else {
                                $ratio = $imageGeometry['width'] / $imageGeometry['height'];
                                $thumbImage->resizeImage(round($height * $ratio), $height, Imagick::FILTER_CATROM, $addOpts['blur'], false);
                            }
                            break;
                        default:
                        case 'crop':
                            $ratio = max($width / $imageGeometry['width'], $height / $imageGeometry['height']);
                            $resizeWidth = ceil($imageGeometry['width'] * $ratio);
                            $resizeHeight = ceil($imageGeometry['height'] * $ratio);
                            $offsetX = round(($resizeWidth - $width) / 2);
                            $offsetY = round(($resizeHeight - $height) / 2);

                            $thumbImage->resizeImage($resizeWidth, $resizeHeight, Imagick::FILTER_CATROM, $addOpts['blur'], false);
                            $thumbImage->cropImage($width, $height, $offsetX, $offsetY);
                            break;
                    }

                    $thumbImage->setImageFormat('jpeg');
                    $thumbImage->setImageCompression(Imagick::COMPRESSION_JPEG);
                    $thumbImage->setImageCompressionQuality(Cast::toInt(
                        $quality, self::DEFAULT_QUALITY_RATE, 1, 100
                    ));

                    $thumbImage->stripImage();

                    $thumbFilename = tempnam(sys_get_temp_dir(), 'thumb');
                    $thumbImage->writeImage($thumbFilename);

                    $this->_file->getFilesystem()->copyFileToStorage(
                        $thumbFilename,
                        $this->getThumbId($this->_file->getStorage(), $this->_file->getId(), $name)
                    );

                    $thumbImage->destroy();
                    unset($thumbImage);

                    try {
                        unlink($thumbFilename); //лишний раз не мусорим
                    } catch (Exception $e) {
                    }
                }
            }
        }
        $originalImage->destroy();
        unset($originalImage);

        return false; // do not touch original file
    }

    public function apply()
    {
        if (!empty($this->_options)) {
            $this->_file->workWithFile([$this, '_generateThumbs']);
        }
    }

    /**
     * вызывается когда удаляется файл
     *
     * @return void
     */
    public function remove()
    {
        $options = $this->_options;

        if (empty($options['resolution']) || !is_array($options['resolution'])) {
            return;
        }

        foreach ($options['resolution'] as $thumbResolution) {
            $name = reset($thumbResolution);
            if (empty($name)) {
                continue;
            }

            $id = $this->getThumbId($this->_file->getStorage(), $this->_file->getId(), $name);
            $file = $this->_file->getStorage()->getFile($id);
            if ($file instanceof File) {
                $file->remove(false);
            }
        }
    }
}
