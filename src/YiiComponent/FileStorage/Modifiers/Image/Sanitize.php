<?php

namespace MommyCom\YiiComponent\FileStorage\Modifiers\Image;

use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FileStorage\File;
use MommyCom\YiiComponent\FileStorage\ModifierInterface;

class Sanitize implements ModifierInterface
{
    /**
     * @var File
     */
    protected $_file;

    /**
     * @var array
     */
    protected $_options;

    /**
     * @param File $file
     * @param array $options
     * @param array $createOptions
     */
    public function __construct(File $file, array $options, array $createOptions = [])
    {
        $this->_file = $file;
        $this->_options = ArrayUtils::merge($options, $createOptions);
    }

    public function _processImage($filename)
    {
        $options = $this->_options + [
                'emptyExif' => false, //очистить exif
                'deleteFormatSpecifiedUnnecessaryData' => true, //удалить возможные дополнительные данные у изображения (например у png удалить скрытые chunks)
                'deleteAnimation' => true //удалить анимацию
            ];
    }

    public function apply()
    {
        //$this->_file->workWithFile(array($this, '_processImage'));
    }

    /**
     * вызывается когда удаляется файл
     *
     * @return void
     */
    public function remove()
    {
        // do nothing
    }
}
