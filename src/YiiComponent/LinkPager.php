<?php

namespace MommyCom\YiiComponent;

use CHtml;
use CLinkPager;

class LinkPager extends CLinkPager
{
    /**
     * Default disable
     *
     * @var bool
     */
    public $cssFile = false;

    /**
     * @var bool|string Text for links
     */
    public $header = false;

    /**
     * @var bool show first number
     */
    public $isShowFirstPage;

    /**
     * @var string show text label
     */
    public $prevPageLabel;

    /**
     * @var string show text label
     */
    public $nextPageLabel;

    /**
     * @var bool show last number
     */
    public $isShowLastPage;

    /**
     * Creates the page buttons.
     *
     * @return array a list of page buttons (in HTML code).
     */
    protected function createPageButtons()
    {
        if (($pageCount = $this->getPageCount()) <= 1)
            return [];

        list($beginPage, $endPage) = $this->getPageRange();
        $currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons = [];

        // first page
        if ($this->isShowFirstPage) {
            $buttons[] = $this->createPageButton(1, 0, $this->firstPageCssClass, $beginPage <= 0, false);
        }

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $beginPage - 1) < 0) {
                $page = 0;
            }

            $buttons[] = $this->createPageButton($this->prevPageLabel, $page, $this->previousPageCssClass, $page <= 0, false);
        }

        // internal pages
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $endPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }

            $buttons[] = $this->createPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $endPage >= $pageCount - 2, false);
        }

        // last page
        if ($this->isShowLastPage !== false) {
            $buttons[] = $this->createPageButton($pageCount, $pageCount - 1, $this->lastPageCssClass, $endPage >= $pageCount - 1, false);
        }

        return $buttons;
    }

    protected function createPageButton($label, $page, $class, $hidden, $selected)
    {
        if ($hidden || $selected) {
            $class .= ' ' . ($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        }

        return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page), ['data-page' => $page + 1]) . '</li>';
    }

}
