<?php

namespace MommyCom\YiiComponent\FrontendMobile;

/**
 * Class ReplyAuth
 * стандартизировать ответ с сервера
 */
class ReplyResult extends \CMap
{
    /**
     * @var int
     */
    private $maxErrors = 10;

    /**
     * ReplyResult constructor.
     *
     * @param null $data
     * @param bool $readOnly
     *
     * @throws \CException
     */
    public function __construct($data = null, $readOnly = false)
    {
        parent::__construct($data = null, $readOnly = false);

        $this->add('errors', []);
        $this->add('success', false);
        $this->add('info', false);
    }

    /**
     * @param string $message
     *
     * @return bool
     * @throws \CException
     */
    public function addError($message)
    {
        $result = true;
        $newError = false;

        if ($this->countErrors() < $this->maxErrors) {
            $newError = $message;
        } elseif ($this->countErrors() === $this->maxErrors) {
            $newError = '...';
        } else {
            $result = false;
        }

        if ($newError) {
            $errors = $this->itemAt('errors');
            $errors[] = $newError;
            $this->add('errors', $errors);
        }

        return $result;
    }

    /**
     * @return int
     */
    private function countErrors()
    {
        return count($this->itemAt('errors'));
    }

    /**
     * @param boolean $value
     *
     * @throws \CException
     */
    public function setSuccess($value)
    {
        $this->add('success', !!$value);
    }

    /**
     * @param string boolean
     *
     * @throws \CException
     */
    public function setInfo($value)
    {
        $this->add('info', $value);
    }
}
