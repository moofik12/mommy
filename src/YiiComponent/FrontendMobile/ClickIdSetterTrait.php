<?php

namespace MommyCom\YiiComponent\FrontendMobile;

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use Symfony\Component\DependencyInjection\ContainerInterface;

trait ClickIdSetterTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    private function setClickId()
    {
        $cookies = \Yii::app()->request->cookies;

        if ($cookies[ClickIdConstantsInterface::CLICK_ID_NAME]) {
            return;
        }

        $clickId = \Yii::app()->request->getParam(ClickIdConstantsInterface::CLICK_ID_NAME);
        if (!$clickId) {
            return;
        }

        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);

        $cookies['clickid'] = new \CHttpCookie('clickid', $clickId, [
            'domain' => '.' . $regions->getHostname(Region::DEFAULT),
            'expire' => time() + ClickIdConstantsInterface::CLICK_ID_LIFETIME,
        ]);
    }
}
