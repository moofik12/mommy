<?php

namespace MommyCom\YiiComponent\FrontendMobile;

use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;

class FrontController extends Controller implements ClickIdConstantsInterface
{
    use ClickIdSetterTrait;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $productHeader = '';

    /**
     * @var bool
     */
    public $visibleOnlyLogo = false;

    /**
     * @var string
     */
    public $filterHeaderHtml = '';

    /**
     * Отображение попапа для подтверждения региона
     *
     * @var bool
     */
    public $showRegionConfirmPopup = false;

    /**
     * Отображение модалки для выбора региона
     *
     * @var bool
     */
    public $showRegionChooseModal = false;

    /**
     * @var string название страниц
     */
    private $_pageTitle;

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'users' => ['@'],
            ],
            [
                'deny',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $this->checkRegion();
        $this->setClickId();

        return true;
    }

    private function checkRegion()
    {
        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);

        $region = $regions->getUserRegion();
        $serverRegion = $regions->getServerRegion();

        if ($region->isDetectBot()) {
            return;
        }

        $canRedirect = true;

        switch ($region->getDetectLevel()) {
            case Region::DETECT_LEVEL_CONFIRMED:
                $canRedirect = true;

                break;
            case Region::DETECT_LEVEL_UNCONFIRMED:
                $canRedirect = true;
                $this->showRegionConfirmPopup = true;

                break;
            case Region::DETECT_LEVEL_UNKNOWN:
                $canRedirect = false;
                $this->showRegionChooseModal = true;

                break;
        }

        if ($canRedirect && $serverRegion->getRegionName() !== $region->getRegionName()) {
            $this->redirect('//' . $regions->getHostname($region->getRegionName()));
        }
    }

    /**
     * @param string $value
     */
    public function setPageTitle($value)
    {
        if ($this->id == 'index') {
            $this->_pageTitle = htmlspecialchars_decode($value, ENT_QUOTES);
        } else {
            $this->_pageTitle = htmlspecialchars_decode($value, ENT_QUOTES) . ' - ' . \Yii::t('common', 'MOMMY.COM: Shopping club for moms and children');
        }
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        if ($this->_pageTitle !== null) {
            return $this->_pageTitle;
        } else {
            return parent::getPageTitle();
        }
    }
}
