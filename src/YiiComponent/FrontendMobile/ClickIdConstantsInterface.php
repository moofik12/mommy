<?php

namespace MommyCom\YiiComponent\FrontendMobile;

interface ClickIdConstantsInterface
{
    const CLICK_ID_NAME = 'clickid';
    const CLICK_ID_LIFETIME = 5 * 24 * 3600; // 5 days
}
