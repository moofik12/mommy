<?php

namespace MommyCom\YiiComponent;

/**
 * Class Utf8
 *
 * @deprecated бесполезная дичь
 */
class Utf8
{
    /**
     * Обычный implode работает не хуже
     *
     * @param string $delimiter
     * @param array $pieces
     *
     * @return string
     **/
    public static function implode($delimiter, $pieces)
    {
        $result = '';
        if (is_array($pieces)) {
            if (!empty($pieces)) {
                reset($pieces);
                list($key, $value) = each($pieces);
                $result = strval($value);
                while (list($key, $value) = each($pieces)) {
                    $result .= $delimiter . strval($value);
                }
            }
        } else {
            $result = $pieces;
        }
        return $result;
    }

    /**
     * Эта версия теоретически может работать лучше, если нужен многобайтовый разделитель
     * Но в таком случае нужно использовать подходящие mb_* функции
     *
     * @param string $delimiter
     * @param string $string
     * @param integer $limit (optional)
     *
     * @return array
     **/
    public static function explode($delimiter, $string, $limit = -1)
    {
        if ($delimiter == "") {
            return false;
        } else {
            $delimiter = preg_quote($delimiter);
            return mb_split($delimiter, $string, $limit);
        }
    }

    /**
     * Обычный nl2br работает не хуже
     *
     * @param string $string
     *
     * @return string
     **/
    public static function nl2br($string)
    {
        return self::replace("\n", "<br>", $string);
    }

    /**
     * Если в replacement не запихивать не UTF8, то обычный str_replace работает как надо
     *
     * @param string $needle
     * @param string $replacement
     * @param string $haystack
     *
     * @return string
     **/
    public static function replace($needle, $replacement, $haystack)
    {
        $needleLen = mb_strlen($needle);
        $replacementLen = mb_strlen($replacement);
        $pos = mb_strpos($haystack, $needle);
        while ($pos !== false) {
            $haystack = mb_substr($haystack, 0, $pos) . $replacement
                . mb_substr($haystack, $pos + $needleLen);
            $pos = mb_strpos($haystack, $needle, $pos + $replacementLen);
        }
        return $haystack;
    }

    /**
     * Обрезает пробелы по бокам у строки ровно так же как обычный trim
     *
     * @param string $string
     * @param string $replacement
     *
     * @return string
     **/
    public static function trim($string, $replacement = '')
    {
        $replacement = $replacement === '' ? '\s' : preg_quote($replacement, '/');
        return preg_replace("/(^$replacement+)|($replacement+$)/us", '', $string);
    }

    /**
     * Обрезает пробелы у всех элементов массива
     * Можно использовать array_map, array_walk и даже array_filter
     *
     * @param array $strings
     * @param string $replacement
     *
     * @return array
     */
    public static function trimArr($strings, $replacement = '')
    {
        foreach ($strings as &$string) {
            $string = is_string($string) ? self::trim($string, $replacement) : '';
        }
        unset($string);
        return $strings;
    }

    /**
     * Обрезает пробелы слева у строки как обычный ltrim
     *
     * @param string $string
     * @param string $replacement
     *
     * @return string
     **/
    public static function ltrim($string, $replacement = '')
    {
        $replacement = $replacement === '' ? '\s' : preg_quote($replacement, '/');
        return preg_replace("/(^$replacement+)/us", '', $string);
    }

    /**
     * Обрезает пробелы справа у строки как обычный rtrim
     *
     * @param string $string
     * @param string $replacement
     *
     * @return string
     **/
    public static function rtrim($string, $replacement = '')
    {
        $replacement = $replacement === '' ? '\s' : preg_quote($replacement, '/');
        return preg_replace("/($replacement+$)/us", '', $string);
    }

    /**
     * Устанавливает первый символ строки в верхний регистр
     * Хотя для этого есть mb_convert_case($string, MB_CASE_TITLE, "UTF-8")
     *
     * @param $string
     *
     * @return string
     */
    public static function ucfirst($string)
    {
        if (mb_strlen($string) === 0) {
            return '';
        }

        $firstChar = mb_substr($string, 0, 1);
        $restOfString = mb_substr($string, 1);

        return mb_strtoupper($firstChar) . $restOfString;
    }

    /**
     * Устанавливает первый символ строки в нижний регистр
     * А нафига бы это было надо?
     *
     * @param $string
     *
     * @return string
     */
    public static function lcfirst($string)
    {
        if (mb_strlen($string) === 0) {
            return '';
        }

        $firstChar = mb_substr($string, 0, 1);
        $restOfString = mb_substr($string, 1);

        return mb_strtolower($firstChar) . $restOfString;
    }

    /**
     * Какая-то неведомая хрень
     *
     * @param string $text
     * @param integer $chunklen of the substring
     * @param string $end
     *
     * @return string
     **/
    public static function chunkSplit($text, $chunklen = 76, $end = "\r\n")
    {
        if (empty($text)) false;
        $chunklen = intval($chunklen);
        $textLength = mb_strlen($text);
        $result = '';
        $index = 0;

        $result = mb_substr($text, 0, $chunklen);
        $index++;
        while (($pos = $index * $chunklen) <= $textLength) {
            $result .= $end . mb_substr($text, $pos, $chunklen);
            $index++;
        }
        return $result;
    }

    /**
     * Возвращает количество слов в строке
     * Очень "полезно"
     *
     * @param string
     *
     * @return integer
     **/
    public static function wordCount($string)
    {
        return count(Utf8::words($string));
    }

    /**
     * Возвращает слова из строки разделенные любыми пробельными символами
     * Очень "полезно"
     *
     * @param string
     *
     * @return array
     **/
    public static function words($string)
    {
        $string = self::trim(preg_replace('/\s+/u', ' ', $string));
        return self::explode(" ", $string);
    }

    /**
     * Обычный strtr работает не хуже
     *
     * @param $str
     * @param $from
     * @param $to
     *
     * @return mixed
     */
    public static function strtr($str, $from, $to)
    {
        return str_replace(self::split($from), self::split($to), $str);
    }

    /**
     * Используется в функции выше
     *
     * @param string
     *
     * @return string
     **/
    public static function split($str)
    {
        preg_match_all('/.{1}|[^\x00]{1}$/us', $str, $ar);
        return $ar[0];
    }

    /**
     * Обрезает строку и добавлят троеточия в конец (по умолчанию)
     *
     * @param string $string
     * @param integer $length
     * @param string $end
     *
     * @return string
     **/
    public static function truncate($string, $length = 80, $end = '...')
    {
        $length = intval($length);
        $endLength = mb_strlen($end);
        $stringLength = mb_strlen($string);
        if ($length <= $stringLength - $endLength) {
            return mb_substr($string, 0, $length) . $end;
        }
        return $string;
    }

    /**
     * Выпиливает из текста символы разметки и форматирования
     *
     * @param string $string
     *
     * @return string
     */
    public static function stripUnnessesaryChars($string)
    {
        $urlbrackets = '\[\]\(\)';
        $urlspacebefore = ':;\'_\*%@&?!' . $urlbrackets;
        $urlspaceafter = '\.,:;\'\-_\*@&\/\\\\\?!#' . $urlbrackets;
        $urlall = '\.,:;\'\-_\*%@&\/\\\\\?!#' . $urlbrackets;

        $specialquotes = '\'"\*<>';

        $fullstop = '\x{002E}\x{FE52}\x{FF0E}';
        $comma = '\x{002C}\x{FE50}\x{FF0C}';
        $arabsep = '\x{066B}\x{066C}';
        $numseparators = $fullstop . $comma . $arabsep;

        $numbersign = '\x{0023}\x{FE5F}\x{FF03}';
        $percent = '\x{066A}\x{0025}\x{066A}\x{FE6A}\x{FF05}\x{2030}\x{2031}';
        $prime = '\x{2032}\x{2033}\x{2034}\x{2057}';
        $nummodifiers = $numbersign . $percent . $prime;

        return preg_replace([
            // Remove separator, control, formatting, surrogate,
            // open/close quotes.
            '/[\p{Z}\p{Cc}\p{Cf}\p{Cs}\p{Pi}\p{Pf}]/u',
            // Remove other punctuation except special cases
            '/\p{Po}(?<![' . $specialquotes . $numseparators . $urlall . $nummodifiers . '])/u',
            // Remove non-URL open/close brackets, except URL brackets.
            '/[\p{Ps}\p{Pe}](?<![' . $urlbrackets . '])/u',
            // Remove special quotes, dashes, connectors, number
            // separators, and URL characters followed by a space
            '/[' . $specialquotes . $numseparators . $urlspaceafter . '\p{Pd}\p{Pc}]+((?= )|$)/u',
            // Remove special quotes, connectors, and URL characters
            // preceded by a space
            '/((?<= )|^)[' . $specialquotes . $urlspacebefore . '\p{Pc}]+/u',
            // Remove dashes preceded by a space, but not followed by a number
            '/((?<= )|^)\p{Pd}+(?![\p{N}\p{Sc}])/u',
            // Remove consecutive spaces
            '/ +/',
            // remove minus
            '/-/',
        ], ' ', $string);
    }
}
