<?php

namespace MommyCom\YiiComponent\BackendTablet;

use CWebUser;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\IpBanRecord;
use MommyCom\Model\Db\TabletUserRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;
use Yii;

class AdmWebUser extends CWebUser
{
    /**
     * @var AdminUserRecord|null
     */
    private $_model = null;

    /**
     * @var array ключи (section) с доступом которые запрещены
     */
    private $_bannedKeys = [];

    /**
     * @var bool был ли выполнен запрос
     */
    private $_bannedQuery = false;

    public function init()
    {
        parent::init();

        // обновление tablet::last_activity_at
        if ($this->id > 0) {
            $tabletUser = TabletUserRecord::model()->userId($this->id)->find();
            if ($tabletUser !== null) {
                TabletUserRecord::model()->updateByPk($tabletUser->id, [
                    'last_activity_at' => time(),
                ]);
            }
        }
    }

    /**
     * Модель пользователя
     *
     * @return AdminUserRecord
     */
    public function getModel()
    {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = AdminUserRecord::model()->findByPk($this->id);
        }
        return $this->_model;
    }

    public function getRules()
    {
        if (!$this->isGuest) {
            return AdminUserRecord::unserializeRules($this->getModel()->rules);
        }
        return null;
    }

    public function isRoot()
    {
        if (!$this->isGuest) {
            return Cast::toBool($this->getModel()->is_root);
        }
        return false;
    }

    /**
     * проверяет запрещено ли данное действие блокировкой по ip
     *
     * @param string $section ('all' | 'admin' | 'site' | 'cart')
     *
     * @return bool
     */
    public function isBanned($section = 'all')
    {
        $sections = [
            IpBanRecord::SECTION_ALL => 'all',
            IpBanRecord::SECTION_AUTH_ADMIN => 'admin',
            IpBanRecord::SECTION_AUTH_SITE => 'site',
            IpBanRecord::SECTION_CART => 'cart',

        ];

        $key = array_search($section, $sections);

        if (!$this->_bannedQuery) {
            $this->_bannedQuery = true;
            $ip = Yii::app()->request->getUserHostAddress();
            $model = IpBanRecord::model()->bannedIp($ip)->distinctSection()->findAll();
            $this->_bannedKeys = ArrayUtils::getColumn($model, 'section');
        }

        return in_array(IpBanRecord::SECTION_ALL, $this->_bannedKeys)
            ? true : in_array($key, $this->_bannedKeys);
    }

    public function checkAccess($urlAccess = null, $default = null, $allowCaching = true)
    {
        return true;
    }
}
