<?php

namespace MommyCom\YiiComponent\BackendTablet;

use CUrlManager;
use Yii;

class FrontendUrlManager extends CUrlManager
{
    /**
     * @var string
     */
    private $_baseUrl;

    /**
     * FrontendUrlManager constructor.
     */
    public function __construct()
    {
        $frontend = include ROOT_PATH . '/config/yii/frontend/main.php';

        $config = $frontend['components']['urlManager'];
        unset($config['class']);
        foreach ($config as $key => $value)
            $this->$key = $value;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        if ($this->_baseUrl !== null)
            return $this->_baseUrl;
        else {
            if ($this->showScriptName)
                $this->_baseUrl = Yii::app()->getRequest()->getBaseUrl() . '/frontend.php';
            else
                $this->_baseUrl = Yii::app()->getRequest()->getBaseUrl();
            return $this->_baseUrl;
        }
    }

    /**
     * @param string $route
     * @param array $params
     * @param string $schema
     * @param string $ampersand
     *
     * @return string
     */
    public function createAbsoluteUrl($route, $params = [], $schema = '', $ampersand = '&')
    {
        $url = $this->createUrl($route, $params, $ampersand);

        if (strpos($url, 'http') === 0)
            return $url;
        else
            return Yii::app()->getRequest()->getHostInfo($schema) . $url;
    }
}
