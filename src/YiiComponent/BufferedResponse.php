<?php

namespace MommyCom\YiiComponent;

use Symfony\Component\HttpFoundation\Response;

class BufferedResponse extends Response
{
    /**
     * @var bool
     */
    private $headersSent = false;

    /**
     * @var bool
     */
    private $contentSent = false;

    /**
     * @var bool
     */
    private $bufferCleared = false;

    /**
     * BufferedResponse constructor.
     *
     * @param int $status
     * @param array $headers
     */
    public function __construct(int $status = 200, array $headers = [])
    {
        parent::__construct(null, $status, $headers);
    }

    /**
     * @param null $content
     * @param int $status
     * @param array $headers
     *
     * @return BufferedResponse
     */
    public static function create($content = null, $status = 200, $headers = [])
    {
        return new static($status, $headers);
    }

    /**
     * {@inheritdoc}
     */
    public function sendHeaders()
    {
        if ($this->headersSent) {
            return $this;
        }

        $this->headersSent = true;

        foreach (headers_list() as $header) {
            $header = strstr($header, ':', true);
            if ($this->headers->has($header)) {
                $this->headers->remove($header);
            }
        }

        return parent::sendHeaders();
    }

    /**
     * {@inheritdoc}
     */
    public function sendContent()
    {
        if ($this->contentSent) {
            return $this;
        }

        if ($this->bufferCleared) {
            return parent::sendContent();
        }

        while (ob_get_level()) {
            ob_end_flush();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        if ($this->contentSent) {
            throw new \LogicException('The content have already been sent.');
        }

        return parent::setContent($content);
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        if (!$this->bufferCleared) {
            while (ob_get_level() > 1) {
                ob_end_flush();
            }

            $this->content = ob_get_clean();
            $this->bufferCleared = true;
        }

        return parent::getContent();
    }
}
