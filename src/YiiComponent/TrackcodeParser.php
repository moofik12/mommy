<?php

namespace MommyCom\YiiComponent;

class TrackcodeParser
{
    const TYPE_NOVAPOSHTA = 0;
    const TYPE_UKRPOSHTA = 1;

    public $type = self::TYPE_NOVAPOSHTA;

    /**
     * @var string
     */
    protected $_pattern = '';

    /**
     * @var string[]
     */
    protected $_patterns = [
        self::TYPE_NOVAPOSHTA => '/[0-9]{14}/u',
        self::TYPE_UKRPOSHTA => '',
    ];

    /**
     * @var string[]
     */
    protected $_trackcodes = [];

    /**
     * @param string|array $body
     *
     * @return bool
     */
    public function parse($body)
    {
        if (is_array($body)) {
            $body = implode(' ', $body);
        }

        if (!is_string($body)) {
            return false;
        }

        $pattern = $this->_getPattern();
        preg_match_all($pattern, $body, $result);
        $this->_trackcodes = isset($result[0]) ? $result[0] : [];

        return true;
    }

    /**
     * @return array
     */
    public function getTrackcodes()
    {
        return $this->_trackcodes;
    }

    /**
     * @return string
     */
    protected function _getPattern()
    {
        $pattern = '';

        if (isset($this->_patterns[$this->type])) {
            $pattern = $this->_patterns[$this->type];
        }

        return $pattern;
    }
} 
