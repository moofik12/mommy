<?php

namespace MommyCom\YiiComponent;

use CComponent;
use CHttpException;
use Yii;

class MailerMessage extends CComponent
{
    /**
     * Название шаблона сообщения
     *
     * @var string $template
     */
    public $template;

    /**
     * Ид пользователя
     *
     * @var integer $userId
     */
    public $userId = 0;

    /**
     * email на который отправляется письмо
     *
     * @var string $email
     */
    public $email = null;

    /**
     * Обратный email
     *
     * @var string $mailFrom
     */
    public $mailFrom = null;

    /**
     * Тема сообщения
     *
     * @var string $subject
     */
    public $subject = null;

    /**
     * Текст сообщения
     *
     * @var array $body
     */
    public $body = [];

    /**
     * Срочная отправка сообщения
     *
     * @var bool $isSend
     */
    public $isSend = false;

    /**
     * Установка типа отправляемого сообщения
     *
     * @var string Type of email.  Options include "text/html" and "text/plain"
     */
    public $type = 'text/html';

    /**
     * Установка текущего языка
     *
     * @var string language to encode the message in (eg "Japanese", "ja", "English", "en" and "uni" (UTF-8))
     */
    public $language = 'uni';

    /**
     * Кодировка текста сообщения
     *
     * @var string the content-type of the email
     */
    public $contentType = 'utf-8';

    /**
     * Длина максимальная длина строки в письме
     *
     * @var integer $lineLength
     */
    public $lineLength = 70;

    /**
     * Ожидается ответ на сообщение или нет
     *
     * @var boolean $reply
     */
    public $reply = false;

    /**
     * ХТМЛка которая будет отправляться пользователю
     *
     * @var string
     */
    public $message;

    public function getBody($template, $body)
    {
        $this->message = self::_renderLetter($template, $body);
        return $this->message;
    }

    public function getContainer($template, $body)
    {
        $this->message = self::_MessageContainer($template, $body);
        return $this->message;
    }

    protected static function _renderLetter($template, $body)
    {
        $path = Yii::getPathOfAlias(Yii::app()->mail->viewPath) . DIRECTORY_SEPARATOR . $template . '.php';
        if (!file_exists($path))
            throw new CHttpException(404, 'Template ' . $path . ' does not exist.');
        return Yii::app()->controller->renderFile($path, ['body' => $body], true);
    }

    protected static function _MessageContainer($template, $body)
    {
        return self::_renderLetter('_' . $template, $body);
    }

}
