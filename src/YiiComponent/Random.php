<?php

namespace MommyCom\YiiComponent;

final class Random
{
    const ALPHABET_FULL = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?';
    const ALPHABET_SIMPLIFIED = 'abcdefghkmnopqrstuvz123456789';
    const DIGIT = '0123456789';

    public static function alphabet($size, $characters = self::ALPHABET_FULL)
    {
        $charsCount = strlen($characters);

        $i = 0;
        $salt = "";
        while ($i < $size) {
            $idx = rand(0, $charsCount - 1);
            $salt .= $characters[$idx];
            $i++;
        }
        return $salt;
    }
}
