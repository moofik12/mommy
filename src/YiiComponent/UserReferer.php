<?php

namespace MommyCom\YiiComponent;

use CApplicationComponent;

/**
 * Class UserReferer
 *
 * @property string $url
 */
class UserReferer extends CApplicationComponent
{
    const SEARCH_ENGINE_UNKNOWN = 'unknown';
    const SEARCH_ENGINE_GOOGLE = 'google';
    const SEARCH_ENGINE_YANDEX = 'yandex';
    const SEARCH_ENGINE_MAIL_RU = 'go.mail';
    const SEARCH_ENGINE_YAHOO = 'yahoo';
    const SEARCH_ENGINE_BING = 'bing';
    const SEARCH_ENGINE_RAMBLER = 'rambler';
    const SEARCH_ENGINE_MSN = 'msn';
    const SEARCH_ENGINE_BIGMIR = 'bigmir';
    const SEARCH_ENGINE_ASK = 'ask';
    const SEARCH_ENGINE_HOTBOT = 'hotbot';
    const SEARCH_ENGINE_TEOMA = 'teoma';

    /**
     * @var string e.g. http
     */
    public $scheme;

    /** @var  string */
    public $host;

    /** @var  int */
    public $port;

    /** @var  string */
    public $user;

    /** @var  string */
    public $pass;

    /** @var  string */
    public $path;

    /**
     * @var  string after the question mark ?
     */
    public $query;

    /**
     * @var string  after the hashmark #
     */
    public $fragment;

    /** @var string */
    public $searchEngine = self::SEARCH_ENGINE_UNKNOWN;

    private $_url;

    /**
     * @param null $url
     */
    public function __construct($url = null)
    {
        if (!$url && isset($_SERVER['HTTP_REFERER'])) {
            $url = $_SERVER['HTTP_REFERER'];
        }

        if ($url) {
            $this->setUrl($url);
        }
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        if (!$url) {
            return;
        }

        if (mb_stripos($url, 'http') === false) {
            $url = 'http://' . $url;
        }

        $this->_url = $url;
        $parse = @parse_url($url);
        foreach ($parse as $param => $value) {
            if (property_exists($this, $param)) {
                $this->$param = $value;
            }
        }

        //structured host
        if (isset($parse['host'])) {
            $this->host = str_ireplace('www.', '', $this->host);
        }

        if ($this->host) {
            $this->searchEngine = self::SEARCH_ENGINE_UNKNOWN;
            foreach ($this->searchEngines() as $engine) {
                if (mb_stripos($this->host, $engine) === 0) {
                    $this->searchEngine = $engine;
                    break;
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function isSearchEngine()
    {
        return $this->searchEngine != self::SEARCH_ENGINE_UNKNOWN;
    }

    /**
     * @return array
     */
    public function searchEngines()
    {
        return [
            self::SEARCH_ENGINE_GOOGLE,
            self::SEARCH_ENGINE_YANDEX,
            self::SEARCH_ENGINE_MAIL_RU,
            self::SEARCH_ENGINE_YAHOO,
            self::SEARCH_ENGINE_BING,
            self::SEARCH_ENGINE_RAMBLER,
            self::SEARCH_ENGINE_MSN,
            self::SEARCH_ENGINE_BIGMIR,
            self::SEARCH_ENGINE_ASK,
            self::SEARCH_ENGINE_HOTBOT,
            self::SEARCH_ENGINE_TEOMA,
        ];
    }
}
