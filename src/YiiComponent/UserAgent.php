<?php

namespace MommyCom\YiiComponent;

use CComponent;

/**
 * Class UserAgent
 * Класс для работы юзерагентом пользователя.
 * Имеет смысл добавить еще методов
 */
class UserAgent extends CComponent
{
    public function init()
    {
    }

    protected function _check($queries)
    {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            return false;
        }

        foreach ($queries as $name) {
            if (preg_match($name, $_SERVER['HTTP_USER_AGENT'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function getIsMobile()
    {
        $mobile = [
            "/iPhone/", "/iPad/", "/iPod/",
            "/Android/", "/Opera M/", "/webOS/",
            "/BlackBerry/", "/Mobile/",
            "/WindowsPhone/", "/Windows Phone/", "/WP7/", "/WP8/",
            "/Fennec/", "/Maemo/",
            "/Symbian/", "/HTC_/",
            "/GoBrowser/",
        ];

        return $this->_check($mobile);
    }

    /**
     * @return bool
     */
    public function getIsMobileAndroid()
    {
        $mobile = [
            "/Android/",
        ];

        return $this->_check($mobile);
    }

    /**
     * @return bool
     */
    public function getIsMobileIOs()
    {
        $mobile = [
            "/iPhone/", "/iPad/", "/iPod/",
        ];

        return $this->_check($mobile);
    }

    /**
     * @return bool
     */
    public function getIsMobileTablet()
    {
        $mobile = [
            '/iPad/',
            '/Sch-I800/', '/Xoom/', '/Playbook/',
            '/Kindle/', '/Android 3.0/',
            '/Tablet/',
        ];

        return $this->_check($mobile);
    }

    /**
     * @return bool
     */
    public function getIsMobilePhone()
    {
        return $this->getIsMobile() && !$this->getIsMobileTablet();
    }

    /**
     * @return bool
     */
    public function getIsDesktop()
    {
        return !$this->getIsMobile();
    }
}
