<?php

namespace MommyCom\YiiComponent\Frontend;

class TimerFormatter extends \CComponent
{
    protected $_messageWeek = 'more than a week';
    protected $_messageFinished = 'ended';
    protected $_messageDays = '%s, %s:%s:%s';
    protected $_messageHours = '%s %s:%s';
    protected $_messageMinutes = '%s %s';
    protected $_messageSeconds = '%s';

    public function init()
    {
    }

    /**
     * @param integer $timestamp
     *
     * @return string
     */
    public function format($timestamp)
    {
        $nowTimestamp = time();

        $now = new \DateTime('@' . $nowTimestamp);
        $end = new \DateTime('@' . $timestamp);

        $intervalTimestamp = $timestamp - $nowTimestamp;
        $interval = $now->diff($end);

        if ($intervalTimestamp > 604800) {
            return \Yii::t('common', 'more than a week');
        } elseif ($intervalTimestamp <= 0) {
            return \Yii::t('common', 'ended');
        } elseif ($intervalTimestamp > 86400) {
            return sprintf(
                $this->_messageDays,
                \Yii::t('common', '{n} day|{n} days|{n} days', $interval->days),
                str_pad($interval->h, 2, '0', STR_PAD_LEFT),
                str_pad($interval->i, 2, '0', STR_PAD_LEFT),
                str_pad($interval->s, 2, '0', STR_PAD_LEFT)
            );
        } elseif ($intervalTimestamp > 60 * 60) {
            return sprintf(
                $this->_messageHours,
                \Yii::t('common', '{n} hour|{n} hours|{n} hours', $interval->h),
                str_pad($interval->i, 2, '0', STR_PAD_LEFT),
                str_pad($interval->s, 2, '0', STR_PAD_LEFT)
            );
        } elseif ($intervalTimestamp > 60) {
            return sprintf(
                $this->_messageMinutes,
                \Yii::t('common', '{n} minute|{n} minutes|{n} minutes', $interval->i),
                \Yii::t('common', '{n} second|{n} seconds|{n} seconds', $interval->s)
            );
        } elseif ($intervalTimestamp > 0) {
            return sprintf(
                $this->_messageSeconds,
                \Yii::t('common', '{n} second|{n} seconds|{n} seconds', $interval->s)
            );
        }

        return '';
    }

    /**
     * @param integer $timestamp
     *
     * @return string
     */
    public function formatMachine($timestamp)
    {
        $time = new \DateTime('@' . $timestamp);
        return $time->format(\DateTime::W3C);
    }
}
