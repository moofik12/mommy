<?php

namespace MommyCom\YiiComponent\Frontend;

class UserRefererSearchEngineBenefice extends \CBehavior
{
    public function events()
    {
        return array_merge(parent::events(), [
            'onBeginRequest' => 'beginRequest',
        ]);
    }

    public function beginRequest()
    {
        $this->_initBenefice();
    }

    protected function _initBenefice()
    {
        /** @var \CApplication $app */
        $app = $this->getOwner();

        if ($app->user->isEnableOfferSearchBenefice()) {
            if ($app->userReferer->isSearchEngine()) {
                $app->user->setLastSearchEngine($app->userReferer->searchEngine);
            }

            $app->user->setEnableOfferBenefice(true);
        }
    }
}
