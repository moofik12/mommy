<?php

namespace MommyCom\YiiComponent;

use CHttpRequest;
use Exception;
use Yii;

final class SmartHttpRequest extends CHttpRequest
{
    private $_baseUrlParsed = null;
    private $_pathInfo;

    /** @var array */
    public $noCsrfValidationRoutes = [];

    public function setPathInfo($text)
    {
        if (is_string($text)) {
            $this->_pathInfo = $text;
        }
    }

    public function getPathInfo()
    {
        if ($this->_pathInfo !== null) {
            return $this->_pathInfo;
        }

        return parent::getPathInfo();
    }

    protected function normalizeRequest()
    {
        //remove the event handler CSRF if this is a route we want skipped
        if ($this->enableCsrfValidation && is_array($this->noCsrfValidationRoutes)) {
            $url = Yii::app()->getUrlManager()->parseUrl($this);
            foreach ($this->noCsrfValidationRoutes as $route) {
                if (strpos($url, $route) === 0) {
                    $this->enableCsrfValidation = false;
                    break;
                }
            }
        }

        //attach event handlers for CSRFin the parent
        parent::normalizeRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getUrlReferrer($internalOnly = true)
    {
        $referer = parent::getUrlReferrer();
        if ($internalOnly) {
            if (is_null($this->_baseUrlParsed)) {
                $this->_baseUrlParsed = parse_url(Yii::app()->baseUrl, PHP_URL_HOST);
            }
            $refererParsed = parse_url($referer, PHP_URL_HOST);
            if ($refererParsed != $this->_baseUrlParsed) {
                $refererParsed = Yii::app()->baseUrl;
            }
        }
        return $referer;
    }

    public function redirectInternal($url, $terminate = true, $statusCode = 302)
    {
        if (strpos($url, '/') === 0 && strpos($url, '//') !== 0) {
            $url = $this->getHostInfo() . $url;
        }

        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];

        $urlParsed = parse_url($url, PHP_URL_HOST);

        if ($urlParsed != $host) {
            $url = Yii::app()->getBaseUrl(true);
        }

        header('Location: ' . $url, true, $statusCode);
        if ($terminate) {
            Yii::app()->end();
        }
    }

    public function getRouteFromReferrer($url)
    {
        if (!is_string($url)) {
            return false;
        }

        $parseUrl = parse_url($url);
        if ($parseUrl === false) {
            return false;
        }

        try {
            $scheme = isset($parseUrl['scheme']) ? $parseUrl['scheme'] : '';
            $host = isset($parseUrl['host']) ? $parseUrl['host'] : '';
            $query = isset($parseUrl['query']) ? $parseUrl['query'] : '';
            $path = isset($parseUrl['path']) ? trim($parseUrl['path'], '/') : '';

            $urlManager = Yii::app()->urlManager;
            $request = Yii::app()->request;
            $referrerHost = $scheme . '://' . $host;
            if ($referrerHost !== $request->getHostInfo()) {
                return false;
            }
            parse_str($query, $params);
            if (isset($params[$urlManager->routeVar])) {
                return $params[$urlManager->routeVar];
            }

            $copyRequest = clone $this;
            $copyRequest->setPathInfo($path);

            $result = $urlManager->parseUrl($copyRequest);
            return empty($result) ? false : $result;
        } catch (Exception $e) {
            return false;
        }
    }
}
