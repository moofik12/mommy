<?php

namespace MommyCom\YiiComponent;

use CViewRenderer;

class ViewRenderer extends CViewRenderer
{
    /**
     * @var bool
     */
    public $minify = true;

    /**
     * Parses the source view file and saves the results as another file.
     *
     * @param string $sourceFile the source view file path
     * @param string $viewFile the resulting view file path
     */
    protected function generateViewFile($sourceFile, $viewFile)
    {
        $code = file_get_contents($sourceFile);

        if ($this->minify) {
            $code = Utf8::replace("\t", ' ', $code);
            $code = Utf8::trim(preg_replace('/((?<!\?>)\n)[\s]+/u', '\1', $code));
            /*$search = array(
            '/\>[^\S ]+/su',  // strip whitespaces after tags, except space
            '/[^\S ]+\</su',  // strip whitespaces before tags, except space
            '/(\s)+/su'       // shorten multiple whitespace sequences
            );

            $replace = array(
            '>',
            '<',
            '\\1'
            );

            $code = preg_replace($search, $replace, $code);
            */
        }

        file_put_contents($viewFile, $code);
    }
}
