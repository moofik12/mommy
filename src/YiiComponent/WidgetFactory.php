<?php

namespace MommyCom\YiiComponent;

use CApplicationComponent;
use CBaseController;
use CException;
use CWidget;
use IWidgetFactory;

/**
 * Class CWidgetFactory
 */
class WidgetFactory extends CApplicationComponent implements IWidgetFactory
{
    public $widgets = [];

    /**
     * Initializes the application component.
     * This method overrides the parent implementation by resolving the skin path.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Creates a new widget based on the given class name and initial properties.
     *
     * @param CBaseController $owner the owner of the new widget
     * @param string $className the class name of the widget. This can also be a path alias (e.g. system.web.widgets.COutputCache)
     * @param array $properties the initial property values (name=>value) of the widget.
     *
     * @return CWidget the newly created widget whose properties have been initialized with the given values.
     * @throws CException
     */
    public function createWidget($owner, $className, $properties = [])
    {
        if (false !== strpos($className, '.')) {
            $className = \Yii::import($className, true);
        }
        $widget = new $className($owner);

        if (isset($this->widgets[$className])) {
            if (empty($properties)) {
                $properties = $this->widgets[$className];
            } else {
                $properties = ArrayUtils::merge($this->widgets[$className], $properties);
            }
        }

        foreach ($properties as $name => $value) {
            $widget->$name = $value;
        }

        return $widget;
    }
}
