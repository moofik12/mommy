<?php

namespace MommyCom\YiiComponent\Sms;

class SmsProviderAlphaSms extends SmsProviderAbstract
{
    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    private $_url = 'http://alphasms.com.ua/api/http.php';

    /**
     * @param $command
     * @param array $params
     *
     * @return string
     */
    private function _buildUrl($command, array $params)
    {
        $serviceParams = [
            'login' => $this->login,
            'password' => $this->password,
            'command' => $command,
            'version' => 'http',
        ];

        $params = array_merge($params, $serviceParams);
        $url = $this->_url;

        if (mb_stripos($this->_url, '?') === false) {
            $url .= '?';
        }

        $url .= http_build_query($params);

        return $url;
    }

    public function balance()
    {
        $link = $this->_buildUrl('balance', []);
        $result = false;
        $answer = $this->_smsSender->execute($link);

        $posBalance = strpos($answer, 'balance');

        if ($posBalance !== false) {
            $balance = substr($answer, $posBalance + 8);
            $result = round(floatval($balance), 2);
        }

        return $result;
    }

    /**
     * @param string $phone
     * @param string $message
     * @param string $from
     *
     * @return bool
     */
    public function send($phone, $message, $from)
    {
        $result = false;

        $link = $this->_buildUrl('send', [
            'from' => $from,
            'to' => $this->normalizePhone($phone),
            'message' => $message,
        ]);

        $answer = $this->_smsSender->execute($link);

        if ($answer) {
            if (strpos($answer, 'errors') === false) {
                $result = true;
            } else {
                $this->lastError = $answer;
            }
        }

        return $result;
    }

    /**
     * @param $phone
     *
     * @return bool
     */
    public function isValidNumber($phone)
    {
        $phone = preg_replace("/[^0-9]/", "", $phone);
        $phoneNumber = mb_substr($phone, -9, 9);

        $operatorKey = mb_substr($phoneNumber, 0, 2);
        $operatorKeys = ['97', '96', '98', '67', '68', '50', '66', '95', '99', '63', '93', '92', '91'];

        return mb_strlen($phoneNumber) === 9 && in_array($operatorKey, $operatorKeys);
    }

    /**
     * @param string $phone
     *
     * @return mixed
     */
    function normalizePhone($phone)
    {
        $phone = preg_replace("/[^0-9]/", "", $phone);
        $phoneNumber = mb_substr($phone, -9);

        $phone = '380' . $phoneNumber;

        return $phone;
    }
}
