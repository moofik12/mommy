<?php

namespace MommyCom\YiiComponent\Sms;

use CApplicationComponent;
use CException;
use LogicException;
use MommyCom\Model\Db\SmsMessageRecord;
use Yii;

class SmsSender extends CApplicationComponent
{
    /**
     * @see self::PROVIDER_...
     * @var string
     */
    public $provider = '';

    /**
     * @var string
     */
    public $from = "";

    /**
     * @var string Log level
     */
    public $level = "";

    /**
     * @var string Log category
     */
    public $category = "";

    /**
     * @var string|false false - для отмены
     */
    public $errorLevel = 'error';

    /**
     * @var SmsProviderAbstract
     */
    private $_provider;

    /**
     * @throws CException
     */
    public function init()
    {
        parent::init();

        if (empty($this->provider)) {
            throw new LogicException("Empty provider config");
        }

        $provider = Yii::createComponent($this->provider, $this);

        if (!$provider instanceof SmsProviderAbstract) {
            throw new LogicException("Provider " . get_class($provider) . ' not instance of SmsProviderAbstract');
        }

        $this->_provider = $provider;
    }

    /**
     * @param int $userId
     * @param string $telephone
     * @param string $text
     * @param int $type
     * @param bool $returnModel
     *
     * @return bool|SmsMessageRecord|null
     */
    public function create($userId, $telephone, $text, $type = SmsMessageRecord::TYPE_SYSTEM, $returnModel = false)
    {
        if (!$this->isValidNumber($telephone)) {
            return false;
        }

        $model = new SmsMessageRecord();
        $model->user_id = $userId;
        $model->telephone = self::digitalPhone($telephone);
        $model->text = $text;
        $model->type = $type;

        $result = $model->save();

        if ($returnModel) {
            $result = $model;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getBalance()
    {
        return $this->_provider->balance();
    }

    /**
     * @param SmsMessageRecord $message
     *
     * @return bool
     */
    public function send(SmsMessageRecord $message)
    {
        $message->setScenario('send');
        $level = $this->level;
        $category = $this->category;
        $text = $message->textToSend;
        $phone = $message->telephone;

        $result = $this->_provider->send($phone, $text, $this->from);

        if ($result) {
            $message->status = SmsMessageRecord::STATUS_SENT;
        } else {
            $message->status = SmsMessageRecord::STATUS_ERROR;
            $level = empty($this->errorLevel) ? $level : $this->errorLevel;

            Yii::log("Error sending sms to $phone. Error: " . $this->_provider->lastError . " TEXT: " . $text, $level, $category);
        }

        $message->sent_at = time();
        $message->save(false, ['status', 'sent_at']);

        return $result;
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    public function sendWaiting($limit = 50)
    {
        $errors = 0;
        $success = 0;

        /** @var  \MommyCom\Model\Db\SmsMessageRecord[] $messages */
        $messages = SmsMessageRecord::model()->status(SmsMessageRecord::STATUS_WAITING)->limit($limit)->findAll();
        foreach ($messages as $message) {
            if ($this->send($message)) {
                $success++;
            } else {
                $errors++;
            }
        }

        $count = count($messages);
        return ['count' => $count, 'success' => $success, 'errors' => $errors];
    }

    public static function digitalPhone($phone)
    {
        return preg_replace("/[^0-9]/", "", $phone);
    }

    /**
     * @param $phone
     *
     * @return bool
     */
    public function isValidNumber($phone)
    {
        return $this->_provider->isValidNumber($phone);
    }

    /**
     * @param string $link url to connect
     *
     * @return mixed
     */
    public function execute($link)
    {
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $answer = @curl_exec($ch);
        curl_close($ch);

        return $answer;
    }
}
