<?php

namespace MommyCom\YiiComponent\Sms;

abstract class SmsProviderAbstract
{
    /**
     * @var bool
     */
    public $normalizedPhone = true;

    /**
     * @var string
     */
    public $lastError = '';

    /**
     * @var SmsSender
     */
    protected $_smsSender;

    /**
     * SmsProviderAbstract constructor.
     *
     * @param SmsSender $sender
     */
    public function __construct(SmsSender $sender)
    {
        $this->_smsSender = $sender;
    }

    /**
     * @param string $phone
     * @param string $message
     * @param string $from
     *
     * @return bool
     */
    abstract function send($phone, $message, $from);

    /**
     * @param $phone
     *
     * @return bool
     */
    abstract function isValidNumber($phone);

    /**
     * @return float|false
     */
    abstract function balance();

    /**
     * @param string $phone
     *
     * @return mixed
     */
    abstract function normalizePhone($phone);
}
