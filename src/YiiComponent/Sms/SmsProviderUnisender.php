<?php

namespace MommyCom\YiiComponent\Sms;

class SmsProviderUnisender extends SmsProviderAbstract
{
    /**
     * @var string
     */
    public $apiKey;

    /**
     * @var string
     */
    private $_url = 'https://api.unisender.com/ru/api';

    /**
     * @param string $command
     * @param array $params
     *
     * @return string
     */
    private function _buildUrl($command, array $params)
    {
        $serviceParams = [
            'format' => 'json',
            'api_key' => $this->apiKey,
        ];

        $params = array_merge($params, $serviceParams);
        $url = $this->_url;

        if (!preg_match('/\/$/', $url)) {
            $url .= '/';
        }

        $url .= $command;

        if (mb_stripos($this->_url, '?') === false) {
            $url .= '?';
        }

        $url .= http_build_query($params);

        return $url;
    }

    public function balance()
    {
        return false;
    }

    /**
     * @param string $phone
     * @param string $message
     * @param string $from
     *
     * @return bool
     */
    public function send($phone, $message, $from)
    {
        $result = false;

        $link = $this->_buildUrl('sendSms', [
            'sender' => $from,
            'phone' => $this->normalizePhone($phone),
            'text' => $message,
        ]);

        $answer = $this->_smsSender->execute($link);
        $answerJson = json_decode($answer, true, 3);

        if ($answerJson) {
            if (!empty($answerJson['error'])) {
                $this->lastError = $answerJson['error'];
            } else {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param $phone
     *
     * @return bool
     */
    public function isValidNumber($phone)
    {
        return true;
    }

    /**
     * @param string $phone
     *
     * @return mixed
     */
    function normalizePhone($phone)
    {
        $phone = preg_replace("/[^0-9]/", "", $phone);
        return $phone;
    }

}
