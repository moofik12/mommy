<?php

namespace MommyCom\YiiComponent\SupplierOrder;


class SupplierOrderNotification extends AbstractEventNotification
{
    /**
     * @return string
     */
    protected function getMailContent(): string
    {
        $eventStartTime = '';
        $eventEndTime = '';
        $link = '';

        $body = '<p>' . $this->translator->t('Dear Sir/Madam') . '<p>';
        $body .= '<p>' . $this->translator->t('We would like to inform you about the orders made during the flash-sale');
        $body .= " ({$eventStartTime} - {$eventEndTime})</p>";
        $body .= $this->translator->t('You need to confirm the order list') . ':';
        $body .= '<li>' . $this->translator->t('Follow the link') . ": {$link}</li>";
        $body .= '<li>' . $this->translator->t('Confirm the order by putting a tick into the field "Confirmed"') .'</li>';
        $body .= '<li>' . $this->translator->t(
            'If some items are not available now, please fill in the number of products available for shipping. In case product is unavailable put "0"'
            ) . '.</li></ul>';
        $body .= '<p>' . $this->translator->t('You can also print invoice or download it') . '.</p>';
        $body .= '<p>' . $this->translator->t('If you have any questions, feel free to contact us') . '.</p>';

        return $body;
    }
}
