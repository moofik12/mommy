<?php

namespace MommyCom\YiiComponent\SupplierOrder;

class ManagerNotificationData extends AbstractEventNotificationData
{
    /**
     * @return string
     */
    public function getMailContent(): string
    {
        $body = "<p>Supplier {$this->getSupplierName()} has confirmed the order./<p>";
        if ($this->hasSupplyDifference()) {
            $body .= 'There are differences between order and supplier\'s confirmation.<br/>';
        } else {
            $body .= 'There is match between order and suppliers confirmation.<br/>';
        }
        $body .= 'Please see the report: ' . $this->getNotificationLink();

        return $body;
    }

    /**
     * @return bool
     */
    private function hasSupplyDifference(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    private function getSupplierName(): string
    {
        return 'toast';
    }

    /**
     * @return string
     */
    protected function getNotificationLink(): string
    {
        return $this->backendLink;
    }
}
