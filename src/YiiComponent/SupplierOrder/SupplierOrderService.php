<?php

namespace MommyCom\YiiComponent\SupplierOrder;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use MommyCom\Entity\Event;
use MommyCom\Entity\EventProduct;
use MommyCom\Entity\Order;
use MommyCom\Entity\WarehouseProduct;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Repository\OrderRepository;
use MommyCom\Repository\WarehouseProductRepository;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\Warehouse\SecurityCodeGenerator;

class SupplierOrderService
{
    private const DELIVERED_QUANTITY = 'deliveredQty';
    private const IS_CONFIRMED = 'isConfirmed';
    private const ORDER_COMMENT = 'comment';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SupplierOrderService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Сохраняет информацию подтвержденную поставщиками ( о количестве товаров готовых для поставки )
     *
     * @param array $products
     *
     * @throws ORMException
     */
    public function confirmProducts(array $products)
    {
        $eventProducts = $this->entityManager
            ->getRepository(EventProduct::class)
            ->findBy([
                'id' => array_keys($products),
            ]);

        /** @var EventProduct $eventProduct */
        foreach ($eventProducts as $eventProduct) {
            $id = $eventProduct->getId();
            $deliveredQuantity = (int)$products[$id][self::DELIVERED_QUANTITY];
            $eventProduct->setNumberSupplied($deliveredQuantity);
        }

        /** @var Event $event */
        $event = $this->entityManager
            ->getRepository(Event::class)
            ->findOneBy([
                'id' => $eventProduct->getEventId(),
            ]);
        $event->setSupplierConfirmation(true);

        $this->entityManager->flush();
    }

    /**
     * Сохраняет подтвержденные менеджером товары на склад
     *
     * @param SecurityCodeGenerator $securityCodeGenerator
     * @param array $products
     *
     * @throws ORMException
     */
    public function confirmWarehouse(SecurityCodeGenerator $securityCodeGenerator, array $products)
    {
        $eventProducts = $this->entityManager
            ->getRepository(EventProduct::class)
            ->findBy([
                'id' => array_keys($products),
            ]);

        /** @var WarehouseProductRepository $warehouseProductRepository */
        $warehouseProductRepository = $this->entityManager->getRepository(WarehouseProduct::class);

        $onceConfirmed = false;
        $timestamp = CurrentTime::getUnixTimestamp();
        /** @var EventProduct $eventProduct */
        foreach ($eventProducts as $eventProduct) {
            $id = $eventProduct->getId();
            $delivered = (int)$products[$id][self::DELIVERED_QUANTITY];
            $confirmed = ('true' === $products[$id][self::IS_CONFIRMED]);

            if ($delivered && $confirmed) {
                for ($i = 0; $i < $delivered; $i++) {
                    $securityCode = $securityCodeGenerator->generate($eventProduct);

                    $warehouseProduct = $warehouseProductRepository->createFromEventProduct(
                        $eventProduct,
                        $securityCode,
                        WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED,
                        $timestamp
                    );

                    $this->entityManager->persist($warehouseProduct);
                }

                $onceConfirmed = true;
            }
        }

        if ($onceConfirmed) {
            /** @var Event $event */
            $event = $this->entityManager
                ->getRepository(Event::class)
                ->findOneBy([
                    'id' => $eventProduct->getEventId(),
                ]);

            $event->setManagerConfirmation(true);
        }

        $this->entityManager->flush();
    }

    /**
     * Отправляет бренд-менеджерам нотификации о подтверждении поставщиком заказа на поставку
     *
     * @param NotificationData $notificationData
     * @param AbstractEventNotification $notification
     * @param array $products
     *
     * @throws \CException
     */
    public function sendManagerNotification(
        NotificationData $notificationData,
        AbstractEventNotification $notification,
        array $products
    )
    {
        $notificationSent = false;

        foreach ($products as $id => $product) {
            if ('true' === $product[self::IS_CONFIRMED] && !$notificationSent) {
                $notification->send($notificationData);
                $notificationSent = true;
            }
        }
    }

    /**
     * Отправляет на перезвон самые дешевые пользовательские заказы
     * товар в которых не может быть поставлен поставщиком в требуемом для акции количестве
     *
     * @param array $products
     */
    public function makePartialDeliveryRecalls(array $products)
    {
        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->entityManager->getRepository(Order::class);

        $eventProducts = $this->entityManager
            ->getRepository(EventProduct::class)
            ->findBy([
                'id' => array_keys($products),
            ]);

        /** @var EventProduct $eventProduct */
        foreach ($eventProducts as $eventProduct) {
            $id = $eventProduct->getId();
            if ((int)$products[$id][self::DELIVERED_QUANTITY] < $eventProduct->getNumberSold()
                && 0 !== $eventProduct->getProductId()
            ) {
                $order = $orderRepository->getCheapestOrderWithProduct($id);
                $order->setProcessingStatus(OrderRecord::PROCESSING_CALLCENTER_RECALL);
                $order->setCallcenterComment($products[$id][self::ORDER_COMMENT]);
            }
        }

        $this->entityManager->flush();
    }
}