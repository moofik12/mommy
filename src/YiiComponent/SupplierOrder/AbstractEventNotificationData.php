<?php

namespace MommyCom\YiiComponent\SupplierOrder;

use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Service\Translator\TranslatorInterface;

abstract class AbstractEventNotificationData
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var TranslatorInterface $translator
     */
    protected $translator;

    /**
     * @var int
     */
    protected $eventId;

    /**
     * @var string
     */
    protected $backendLink;

    /**
     * AbstractEventNotification constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     * @param int $eventId
     * @param string $backendLink
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        int $eventId,
        string $backendLink
    )
    {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->eventId = $eventId;
        $this->backendLink = $backendLink;
    }

    /**
     * @return string
     */
    abstract public function getMailContent(): string;

    /**
     * @return string
     */
    abstract protected function getNotificationLink(): string;
}
