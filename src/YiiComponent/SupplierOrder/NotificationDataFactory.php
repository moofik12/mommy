<?php

namespace MommyCom\YiiComponent\SupplierOrder;

use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\Application\ApplicationTrait;

class NotificationDataFactory
{
    public const TYPE_SUPPLIER_CONFIRMATION = 0;
    public const TYPE_BRAND_MANAGER_CONFIRMATION = 1;
    public const TYPE_DAILY_REPORT = 2;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Regions
     */
    private $regions;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var bool
     */
    private $test;

    /**
     * NotificationFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     * @param Regions $regions
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Regions $regions,
        bool $test
    )
    {
        $this->entityManager = $entityManager;
        $this->regions = $regions;
        $this->translator = $translator;
        $this->test = $test;
    }

    /**
     * @param int $type
     * @param int $eventId
     *
     * @return AbstractEventNotificationData
     */
    public function create(
        int $type,
        int $eventId
    )
    {
        switch ($type) {
            case self::TYPE_BRAND_MANAGER_CONFIRMATION:
                return new ManagerNotificationData(
                    $this->entityManager,
                    $this->translator,
                    $eventId,
                    $this->getBackendLink($type)
                );
            case self::TYPE_DAILY_REPORT:
                break;
            default:
                break;
        }
    }

    /**
     * @param $action
     *
     * @return string
     */
    private function getBackendLink($action): string
    {
        $region = $this->regions->getServerRegion()->getRegionName();
        $hostname = $this->regions->getHostname($region);
        $schema = $this->test ? 'http://' : 'https://';
        $link = $schema . $hostname . '/backend.php?r=' . $action;

        return $link;
    }
}