<?php

namespace MommyCom\YiiComponent\SupplierOrder;

class DailyReportNotification extends AbstractEventNotification
{
    /**
     * @return string
     */
    public function getMailContent(): string
    {
        return 'this is daily notification';
    }
}