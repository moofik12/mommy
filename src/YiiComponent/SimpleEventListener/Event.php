<?php

namespace MommyCom\YiiComponent\SimpleEventListener;

use MommyCom\YiiComponent\SimpleEventListener\Exception\InvalidCallback;
use MommyCom\YiiComponent\SimpleEventListener\Exception\InvalidEventName;

class Event
{
    /**
     * @var string
     */
    protected $_eventName;

    /**
     * @var array
     */
    protected $_binds;

    /**
     * @param string $eventName
     */
    protected function _throwIfInvalidEventName($eventName)
    {
        if (!is_string($eventName)) {
            throw new InvalidEventName();
        }
    }

    /**
     * @param callable $callback
     *
     * @throws \MommyCom\YiiComponent\SimpleEventListener\Exception\InvalidCallback
     */
    protected function _throwIfInvalidCallback($callback)
    {
        if (!is_callable($callback)) {
            throw new InvalidCallback();
        }
    }

    /**
     * @param string $eventName
     */
    public function __construct($eventName)
    {
        $this->_throwIfInvalidEventName($eventName);
        $this->_eventName = $eventName;
        $this->_binds = [];
    }

    /**
     * @param callable $callback
     */
    public function bind($callback)
    {
        $this->_throwIfInvalidCallback($callback);
        $this->_binds[] = $callback;
    }

    /**
     * @param array $params
     */
    public function trigger(array $params = [])
    {
        array_unshift($params, $this->_eventName);
        foreach ($this->_binds as $callback) {
            !is_null($callback) && call_user_func_array($callback, $params);
        }
    }

    /**
     * @param callable|null $callback
     */
    public function unbind($callback = null)
    {
        if (is_null($callback)) {
            $this->_binds = [];
        } else {
            $this->_throwIfInvalidCallback($callback);
            foreach ($this->_binds as &$callback) {
                if ($callback == $callback) {
                    $callback = null;
                }
            }
        }
    }

    /**
     * @param callable $callback
     */
    public function one($callback)
    {
        $this->_throwIfInvalidCallback($callback);
        $this->unbind();
        $this->bind($callback);
    }

    /**
     * @return array
     */
    public function getAssociatedCallbacks()
    {
        return $this->_associatedCallbacks;
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->_eventName;
    }
}
