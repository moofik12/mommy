<?php

namespace MommyCom\YiiComponent\SimpleEventListener;

class SimpleEventListener
{
    protected $_events;

    public function __construct()
    {
        $this->_events = new \stdClass();
    }

    /**
     * @param string $eventName
     *
     * @return Event
     */
    public function createEvent($eventName)
    {
        if (!$this->isExists($eventName)) {
            $this->_events->$eventName = new Event($eventName);
        }
        return $this->_events->$eventName;
    }

    /**
     * @param string $eventName
     *
     * @return boolean
     */
    public function isExists($eventName)
    {
        return isset($this->_events->$eventName) && $this->_events->$eventName instanceof Event;
    }

    /**
     * @param $eventName
     *
     * @return Event
     */
    public function get($eventName)
    {
        return $this->_events->$eventName;
    }
}
