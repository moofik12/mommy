<?php

namespace MommyCom\YiiComponent;

use CComponent;
use lessc;

final class LessCompiler extends CComponent
{
    /**
     * @param string $sourceFilename
     * @param string $compiledFilename
     *
     * @return bool
     */
    public function compileLess($sourceFilename, $compiledFilename)
    {
        return lessc::ccompile($sourceFilename, $compiledFilename);
    }
}
