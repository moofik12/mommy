<?php

namespace MommyCom\YiiComponent\Application;

use MommyCom\Service\Translator\TranslatorAwareInterface;
use MommyCom\Service\Translator\TranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MommyWebApplication extends \CWebApplication
{
    use MagicGetFromContainerTrait;

    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * MommyWebApplication constructor.
     *
     * @param string $config
     * @param ContainerInterface $container
     */
    public function __construct(string $config, ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->container->get('user');
    }

    /**
     * {@inheritdoc}
     */
    public function getComponent($id, $createIfNull = true)
    {
        if ('user' === $id) {
            return $this->getUser();
        }

        return parent::getComponent($id, $createIfNull);
    }

    /**
     * @inheritdoc
     */
    public function createController($route, $owner = null)
    {
        $result = parent::createController($route, $owner);

        if (null === $result) {
            return null;
        }

        $controller = reset($result);

        if ($controller instanceof ContainerAwareInterface) {
            $controller->setContainer($this->getContainer());
        }

        if ($controller instanceof TranslatorAwareInterface) {
            $controller->setTranslator($this->getContainer()->get(TranslatorInterface::class));
        }

        return $result;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * {@inheritdoc}
     *
     * Бросает исключение вместо полной остановки выполнения
     */
    public function end($status = 0, $exit = true)
    {
        if ($this->hasEventHandler('onEndRequest')) {
            $this->onEndRequest(new \CEvent($this));
        }

        if ($exit) {
            throw new YiiEndException($status);
        }
    }
}
