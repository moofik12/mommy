<?php

namespace MommyCom\YiiComponent\Application;

use Symfony\Component\DependencyInjection\ContainerInterface;

trait MagicGetFromContainerTrait
{
    protected static $restrictedNames = [
        'mailer' => true,
        'session' => true,
    ];

    /**
     * @param string $name
     *
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        $container = $this->getContainer();

        if (!isset(self::$restrictedNames[$name]) && $container->has($name)) {
            return $container->get($name);
        }

        /** @noinspection PhpUndefinedClassInspection */
        return parent::__get($name);
    }

    /**
     * @return ContainerInterface
     */
    abstract protected function getContainer(): ContainerInterface;
}
