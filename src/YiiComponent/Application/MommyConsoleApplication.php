<?php

namespace MommyCom\YiiComponent\Application;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MommyConsoleApplication extends \CConsoleApplication
{
    use MagicGetFromContainerTrait;

    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * MommyConsoleApplication constructor.
     *
     * @param string $config
     * @param ContainerInterface $container
     */
    public function __construct(string $config, ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct($config);
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}
