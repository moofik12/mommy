<?php

namespace MommyCom\YiiComponent\Application;

use Throwable;

/**
 * Class YiiEndException
 *
 * Используется как индикатор завершения Yii приложения, вместо вызова exit()
 *
 * @see CApplication::end()
 */
class YiiEndException extends \DomainException
{
    public function __construct(int $code = 0, Throwable $previous = null)
    {
        parent::__construct('', $code, $previous);
    }
}
