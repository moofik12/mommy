<?php

namespace MommyCom\YiiComponent\Application;

trait ApplicationTrait
{
    /**
     * @return MommyWebApplication
     */
    public function app()
    {
        /** @var MommyWebApplication $app */
        $app = \Yii::app();

        return $app;
    }
}
