<?php
// @fixme Нуждается в правке
$SERVER_HOST = 'mommy.com';

// for filestorage
$_SERVER['HTTP_HOST'] = $SERVER_HOST;
// for mailer
$_SERVER['SERVER_NAME'] = $SERVER_HOST;

Yii::setPathOfAlias('common', ROOT_PATH . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('frontend', ROOT_PATH . DIRECTORY_SEPARATOR . 'frontend');
Yii::setPathOfAlias('web', ROOT_PATH . DIRECTORY_SEPARATOR . 'public'); //webroot недоступен
Yii::setPathOfAlias('extensions', ROOT_PATH . DIRECTORY_SEPARATOR . 'extensions');

return [
    'basePath' => ROOT_PATH,
    'runtimePath' => ROOT_PATH . '/var',
    'name' => 'MOMMY',
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'import' => [],
    'preload' => [
        'log', 'clientScript', 'registerAssets',
    ],
    'aliases' => [
        'components' => 'application.components',
        'ext' => 'extensions',
    ],
    'params' => include __DIR__ . '/params.php',
    'commandMap' => [
        'migrate' => [
            'class' => 'system.cli.commands.MigrateCommand',
            'migrationPath' => 'application.migrations',
            'migrationTable' => 'tbl_migration',
            'connectionID' => 'db',
        ],
    ],
    'components' => [
        'filestorage' => include __DIR__ . '/filestorage_' . getenv('APP_ENV') . '.php',
        'db' => include __DIR__ . '/dbConnection.php',
        'cache' => ['class' => \MommyCom\YiiComponent\Cache\Psr6CacheBridge::class],
        'urlManager' => [
            'urlFormat' => 'path',
            'rules' => [
                //static page
                'static/<category>/<page>' => 'static/index',

                'product/<eventId>/<id>' => 'product/index',

                '<controller:\w+>/<id:\d+>' => '<controller>/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<name:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
            'showScriptName' => false,   //Убирает index.php в console необходим
        ],

        'frontendUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'configApp' => include_once __DIR__ . '/frontend-mobile/main.php', //include_once обязательно
            'showScriptName' => false,
        ],

        //FIXME ХАК для консольных приложений, при изменении адреса сайта необходимо изменить hostInfo
        'request' => [
            'hostInfo' => 'https://' . $SERVER_HOST,
            'baseUrl' => '',
            'scriptUrl' => '',
        ],
        'user' => [
            'loginUrl' => ['auth/login'],
            'class' => \MommyCom\YiiComponent\AuthManager\ShopWebUser::class,
            'allowAutoLogin' => true,
//            'identityCookie' => isset($_SERVER['SERVER_NAME']) ? array('domain' => '.'.$_SERVER['SERVER_NAME']) : '',
        ],
        'clientScript' => [
            'class' => \MommyCom\YiiComponent\ClientScript::class,
        ],
        'format' => [
            'class' => \MommyCom\YiiComponent\Format::class,
            'booleanFormat' => ['no', 'Yes'],
        ],
        'timerFormatter' => [
            'class' => \MommyCom\YiiComponent\Frontend\TimerFormatter::class,
        ],
        'mailer' => [
            'class' => \MommyCom\YiiComponent\Emailer::class,
            'logCategory' => 'emailer',
            'logging' => false,
            'viewPath' => 'application.views.common.email-second',
            'smtpOptions' => [
                'host' => 'smtp.mailgun.org',
                'port' => '587',
                'username' => 'postmaster@mail.mommy.com',
                'password' => '0f48bd20f4a37d5e12b6fa2bda08b31e',
            ],
        ],
        'sms' => [
            //рассылка через /frontend/commands/MessageCommand
            'class' => \MommyCom\YiiComponent\Sms\SmsSender::class,
            'from' => 'MAMAM',
            'level' => 'info',
            'category' => 'sms',
            'provider' => [
                'class' => \MommyCom\YiiComponent\Sms\SmsProviderAlphaSms::class,
                'login' => '380671181163',
                'password' => 'fJdij43eE',
            ],
        ],
        'mailerServices' => [
            'class' => \MommyCom\YiiComponent\MailerServices\MailerServices::class,
            'config' => [
                'unisender' => [
                    'key' => '56wrq6tsrea4ndd5n3tuydq35bo4c3s5c6wk8wge',
                    'listId' => 5365450, //сайт -> 2376744
                ],
            ],
            'activeService' => ['unisender'],
        ],
        'log' => [
            'class' => 'CLogRouter',
            'enabled' => true,
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'except' => 'debug',
                    'logFile' => 'console-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'debug.*',
                    'logFile' => 'debug.log',
                ],
            ],
        ],
    ],
];
