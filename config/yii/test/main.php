<?php
$SERVER_HOST = 'mommy.com';

// for filestorage
$_SERVER['HTTP_HOST'] = $SERVER_HOST;
// for mailer
$_SERVER['SERVER_NAME'] = $SERVER_HOST;

Yii::setPathOfAlias('common', ROOT_PATH . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('frontend', ROOT_PATH . DIRECTORY_SEPARATOR . 'frontend');
Yii::getPathOfAlias('assets') or Yii::setPathOfAlias('assets', ROOT_PATH . '/assets/frontend');

$config = [
    'basePath' => ROOT_PATH,
    'runtimePath' => ROOT_PATH . '/var',
    'viewPath' => ROOT_PATH . '/views/frontend',
    'controllerPath' => ROOT_PATH . '/src/Controller/Frontend',
    'name' => 'MOMMY',
    'id' => 'MOMMY', //уникальный ID для всего приложения
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'defaultController' => 'index',
    'import' => [],
    'preload' => [],
    'params' => ['sendEmails' => []] + include __DIR__ . '/../params.php',
    'components' => [
        'filestorage' => include __DIR__ . '/../filestorage_dev.php',
        'fixture' => [
            'class' => 'system.test.CDbFixtureManager',
        ],
        'db' => include __DIR__ . '/../dbConnection.php', //поменять, если для тестов нужно использовать другую БД
        'cache' => ['class' => \MommyCom\YiiComponent\Cache\Psr6CacheBridge::class],
        'mailer' => [
            'class' => \MommyCom\YiiComponent\Emailer::class,
            'logCategory' => 'emailer',
            'logging' => true,
            'viewPath' => 'application.views.common.email',
            'defaultLayout' => 'application.views.common.layouts.email',
        ],
        'frontendUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'configApp' => 'main.php',
            'showScriptName' => false,
            'host' => 'https://mommy.com',
        ],

        // ХАК для консольных приложений, при изменении адреса сайта необходимо изменить hostInfo
        'request' => [
            'hostInfo' => 'https://' . $SERVER_HOST,
            'baseUrl' => '',
            'scriptUrl' => '',
        ],
        'user' => [
            'loginUrl' => ['auth/login'],
            'class' => \MommyCom\YiiComponent\AuthManager\ShopWebUser::class,
            'allowAutoLogin' => true,
            'identityCookie' => ['domain' => '.' . implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2))],
        ],
        'userReferer' => [
            'class' => \MommyCom\YiiComponent\UserReferer::class,
        ],
    ],
];

//fixed for PHPUnit_Util_GlobalState, problem found only in filestorage
array_walk_recursive($config, function (&$item) {
    if ($item instanceof Closure) {
        $item = call_user_func_array($item, []);
    }
});

return $config;
