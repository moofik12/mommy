<?php

Yii::setPathOfAlias('common', ROOT_PATH . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('extensions', ROOT_PATH . DIRECTORY_SEPARATOR . 'extensions');
Yii::getPathOfAlias('assets') or Yii::setPathOfAlias('assets', ROOT_PATH . '/assets/frontend');

//кука ставится на домен 2-го уровня
$userCookie = isset($_SERVER['SERVER_NAME']) ?
    ['domain' => '.' . implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2))] : [];

return [
    'basePath' => ROOT_PATH,
    'runtimePath' => ROOT_PATH . '/var',
    'viewPath' => ROOT_PATH . '/views/frontend',
    'controllerPath' => ROOT_PATH . '/src/Controller/Frontend',
    'name' => 'MOMMY',
    'id' => 'MOMMY', //уникальный ID для всего приложения
    'sourceLanguage' => 'en',
    'language' => getenv('LANGUAGE'),
    'defaultController' => 'index',
    'controllerNamespace' => 'MommyCom\Controller\Frontend',
    'import' => [],
    'preload' => [
        'log', 'clientScript', 'registerAssets', 'extSelect',
        'splitTesting',
    ],
    'aliases' => [
        'components' => 'application.components',
        'ext' => 'extensions',
    ],
    'behaviors' => [
        \MommyCom\YiiComponent\Behavior\MobileRedirectBehavior::class,
        \MommyCom\YiiComponent\Behavior\UserPreferredLanguageSetter::class,
    ],
    'params' => include __DIR__ . '/params.php',

    'components' => [
        'filestorage' => include __DIR__ . '/../filestorage_' . getenv('APP_ENV') . '.php',
        'db' => include __DIR__ . '/../dbConnection.php',
        'cache' => ['class' => \MommyCom\YiiComponent\Cache\Psr6CacheBridge::class],
        'securityManager' => [
            'validationKey' => 'JSeq3nal5XxfiiT8Oi52J15W9tb6qsRy',
        ],
        'messages' => [
            'basePath' => ROOT_PATH . DIRECTORY_SEPARATOR . 'messages',
        ],
        'urlManager' => [
            'urlFormat' => 'path',
            'rules' => [
                '' => 'index/index', // default is index file
                //static page
                'static/<category>/<page>' => 'static/index',

                'category/<category>' => 'event/category',
                'age/<age>/<target>' => 'event/age',
                'brand/<name>' => 'event/brand',
                'usersProvider/customCampaign/<name>' => 'usersProvider/customCampaign',
                'pay/index/<uid>' => 'pay/index',

                'product/<eventId>/<id>' => 'product/index',
                'instruction' => 'instruction/index',
                'expo' => 'usersProvider/expo',
                'past' => 'past/index',
                'catalog' => 'catalog/index',

                '<controller:\w+>/<id:\d+>' => '<controller>/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<name:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                '125' => 'lottery/index',
            ],
            'showScriptName' => false,   //Убирает index.php
        ],

        'mobileUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'scriptName' => 'frontend-mobile.php',
            'host' => 'https://m.' . getenv('PROJECTNAME'),
            'configApp' => include_once __DIR__ . '/../frontend-mobile/main.php', //include_once обязательно
        ],

        'session' => [
            'cookieParams' => [
                    'httpOnly' => true,
                    'httponly' => true, // need test
                ] + $userCookie, //кука ставится на домен 2-го уровня
        ],
        'user' => [
            'loginUrl' => ['auth/auth'],
            'class' => \MommyCom\YiiComponent\AuthManager\ShopWebUser::class,
            'allowAutoLogin' => true,
            'identityCookie' => $userCookie, //авторизационная кука считывается с домена 2-го уровня
        ],
        'errorHandler' => [
            // use 'site/error' action to display errors
            'errorAction' => 'error/index',
        ],

        'assetManager' => [
//            'class' => \MommyCom\YiiComponent\AssetManager::class,
//            'basePath' => ROOT_PATH . '/assets/frontend',
            'forceCopy' => false,
        ],

        'userAgent' => [
            'class' => \MommyCom\YiiComponent\UserAgent::class,
        ],

        'userReferer' => [
            'class' => \MommyCom\YiiComponent\UserReferer::class,
        ],

        'clientScript' => [
            'class' => \MommyCom\YiiComponent\ClientScript::class,
            'minify' => true,
        ],

        'viewRenderer' => [
            'class' => \MommyCom\YiiComponent\ViewRenderer::class,
            'minify' => true,
        ],

        'extSelect' => [
            'class' => 'ext.extSelect.components.ExtendedSelect',
            'coreCss' => false // disable css
        ],

        'registerAssets' => [
            'class' => \MommyCom\YiiComponent\RegisterAssets::class,

            'packages' => [
                'jquery' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        // jquery base
                        'jquery/jquery.min.js',
                        'jquery/jquery.mousewheel.js',
                        'jquery/jquery.serialize-object.js',

                        'jqueryCookie/jquery.cookie.js',

                        // pjax
                        'jqueryPjax/jquery.pjax.js',

                        // jquery tools
                        'jqueryTools/jquery-browser-detection.js',
                        'jqueryTools/jquery-tools.min.js',

                        // jquery lazy load
                        'jqueryLazyload/jquery.scrollstop.min.js',
                        'jqueryLazyload/jquery.lazyload.min.js',

                        // elevate zoom
                        'elevateZoom/jquery.elevatezoom.js',
                    ],
                    'render' => true,
                ],

                'underscore' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'underscore/underscore.min.js',
                    ],
                    'render' => true,
                ],

                'countdown' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'countdown/countdown.min.js',
                        'countdown/date.iso8601.js',
                    ],
                    'render' => true,
                ],

                'markup' => [
                    'basePath' => 'assets.markup',
                    'js' => [
                        'js/html5.js',
                        'js/bootstrap-dropdown.js',
                        'js/bootstrap-modal.js',
                        'js/bootstrap-collapse.js',
                    ],
                    'css' => [
                        'css/reset.css',
                        'css/style.css',

                        'select/css/select.css' // override extSelect css
                    ],
                    'render' => true,
                    'forceOverride' => YII_DEBUG,
                ],

                'front' => [
                    'basePath' => 'assets.front',
                    'js' => [
                        'js/i18n.js',
                        'js/event.tracker.js',
                        'js/registration.tracker.js',
                        'js/mamam.js',
                        'js/mamam.countdown.js',
                        'js/mamam.cart.js',
                        'js/mamam.imagesLazyload.js',
                        'js/mamam.eventsThrottle.js',
                        'js/mamam.actionpay.js',
                        'js/mamam.admitad.js',
                        'js/mamam.mailru.js',
                        'js/mamam.rtbhouse.js',
                        'js/mamam.carousel.js',
                        'js/mamam.event.js',
                        'js/mamam.push.js',
                        'js/mamam.ecommerce.js',
                        'js/mamam.gaEvent.js',
                        'js/mamam.visibility.js',

                        'js/jquery.filterableGoods.js',
                        'js/jquery.numericalSpinner.js',
                        'js/jquery.productPhotogallery.js',
                        'js/jquery.yiierror.fix.js',
                        'js/jquery.loadingMoreGoods.js',
                    ],
                    'jsActions' => [
                        'actions/setup.js',
                        'actions/split-test.js',
                        'actions/<controller>-<action>.js',
                        'actions/<controller>.js',
                    ],
                    'depends' => ['yiiactiveform'],
                    'render' => true,
                    'forceOverride' => YII_DEBUG,
                ],

                'lottery' => [
                    'basePath' => 'assets.markup',
                    'css' => [
                        'css/lottery.css',
                    ],
                    'depends' => ['markup'],
                    'render' => false,
                    'forceOverride' => YII_DEBUG,
                ],
            ],
        ],
        'request' => [
            'class' => \MommyCom\YiiComponent\SmartHttpRequest::class,
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'csrfTokenName' => '__token',
            'csrfCookie' => [
                'httpOnly' => true,
            ],
            'noCsrfValidationRoutes' => ['pay/receive', 'pay/result', 'tracker/record'],
        ],
        'format' => [
            'class' => \MommyCom\YiiComponent\Format::class,
            'booleanFormat' => ['нет', 'да'],
        ],
        'timerFormatter' => [
            'class' => \MommyCom\YiiComponent\Frontend\TimerFormatter::class,
        ],
        'mailer' => [
            'class' => \MommyCom\YiiComponent\Emailer::class,
            'logCategory' => 'emailer',
            'logging' => true,
            'viewPath' => 'application.views.common.email',
            'defaultLayout' => 'application.views.common.layouts.email',
        ],
        'sms' => [
            //рассылка через /frontend/commands/MessageCommand
            'class' => \MommyCom\YiiComponent\Sms\SmsSender::class,
            'from' => 'MAMAM',
            'level' => 'info',
            'category' => 'sms',
            'provider' => [
                'class' => \MommyCom\YiiComponent\Sms\SmsProviderAlphaSms::class,
                'login' => '380671181163',
                'password' => 'fJdij43eE',
            ],
        ],
        'log' => [
            'class' => 'CLogRouter',
            'enabled' => LOGS,
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'exception.*',
                    'logFile' => 'exceptions-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'system.*',
                    'logFile' => 'system-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'except' => 'exception.*, system.*',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                    'categories' => 'paygateway.*, paygateway',
                    'logFile' => 'paygateway.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'logFile' => 'notification.log',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'application.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'console.*',
                ],
            ],
        ],
    ],
];
