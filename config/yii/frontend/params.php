<?php
return [
        'androidAppId' => 'ua.com.mammam',

        'googleTagManagerId' => getenv('GOOGLE_TAG_MANAGER_ID'),

        'salt' => '14523%#^yh8g&TBT5r21323',
        'remember' => 31556926, // one year
        'supportEmail' => 'support@mail.mommy.com',
        'distributionEmail' => 'mommy@mail.mommy.com',
        'distributionEmailName' => 'MOMMY.COM',
        'phone' => getenv('PHONE'),

        'unisenderActiveUserList' => 3147794, //список активных пользователей + новых от 24.04.2014

        //социальные группы
        'groupVk' => 'https://vk.com/mamamclub', //'http://vk.com/club56056518'
        'groupOd' => 'https://odnoklassniki.ru/group/51898779631829',
        'groupFb' => 'https://www.facebook.com/Mommycom-221697445038793/',

        //sync offers
        'syncAdmitadLogin' => 'admitad',
        'syncAdmitadPassword' => 'bkWBCIHa<*=C|^',
        'syncAdvertsspLogin' => 'assp',
        'syncAdvertsspPassword' => 'b018e4cb298746c3a63a',

        'requisites' => [
            'recipient' => 'ТОВ "Mom"',
            'companyId' => '388 044 72', // ОКПО
            'bank' => 'Privat Bank',
            'sortCode' => '300 711', // МФО
            'checkingAccount' => '2600 7052 7266 42', // р/с
        ],

        'privat24Merchant' => [
            'id' => 102174,
            'pass' => 'fTt3pH1ygY1e7f6lnXTot79cEG09frj5',
        ],

        'languages' => [
            'ru' => 'Русский',
            'en' => 'English',
            'in' => 'Indonesian',
            'ro' => 'Romanian',
            'vi' => 'Vietnamese',
            'co' => 'Colombian',
        ],
        'newsletters' => false,
    ] + include __DIR__ . '/../params.php';
