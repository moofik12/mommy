<?php
return [
    'class' => \MommyCom\YiiComponent\FileStorage\FileStorageComponent::class,
    'config' => [
        'versions' => [
            'idProtocol' => 0, //текущая версия протокола
            'metadata' => 0, //текущая версия метаданных
        ],
        'base' => [
            'maxUploadedFileSize' => 7 * 8 * 1024 * 1024, //7 megabytes
        ],
        'servers' => [
            1 => [
                'id' => 1, //id сервера (тоже самое что и ключ)
                'fsVersion' => 0, //версия файловой системы
                'acceptNewFiles' => false, // принимает новые файлы
                'allowDeleteFiles' => false, // разрешено удаление файлов
                'liveChecker' => function () { //ф-ция проверки того что сервер "жив"
                    return true;
                },
                'web' => [
                    'scheme' => 'https',
                    'port' => 443,
                    'host' => 'fs.mamam.ua',
                    'path' => '/fs1',
                ],
                'fsSpecified' => [ // настройки специфичные для каждой файловой системы, в данном случае local storage или nfs-like
                    'path' => ROOT_PATH . '/filestorage/fs1', //путь к хранилищу
                    'secretKey' => 'Fasdf2342szsL34pUH?5$a:wU:j<J=yFHxLPnh=T3ODi_q>OXgMJ2c5b>C',
                ],
            ],
            2 => [
                'id' => 2, //id сервера (тоже самое что и ключ)
                'fsVersion' => 1, //версия файловой системы
                'acceptNewFiles' => false, // принимает новые файлы
                'allowDeleteFiles' => false, // разрешено удаление файлов
                'liveChecker' => function () { //ф-ция проверки того что сервер "жив"
                    return true;
                },
                'web' => [
                    'scheme' => 'https',
                    'port' => 443,
                    'host' => 'fs.mamam.ua',
                    'path' => '/fs2',
                ],
                'fsSpecified' => [ // настройки специфичные для каждой файловой системы, в данном случае local storage или nfs-like
                    'path' => ROOT_PATH . '/filestorage/fs2', //путь к хранилищу
                    'secretKey' => '74>1M2c30c0be3@2f936a98!e5827d344652C',
                ],
            ],
            3 => [
                'id' => 3, //id сервера (тоже самое что и ключ)
                'fsVersion' => 1, //версия файловой системы
                'acceptNewFiles' => false, // принимает новые файлы
                'allowDeleteFiles' => false, // разрешено удаление файлов
                'liveChecker' => function () { //ф-ция проверки того что сервер "жив"
                    return true;
                },
                'web' => [
                    'scheme' => 'https',
                    'port' => 443,
                    'host' => 'fs.mamam.ua',
                    'path' => '/fs3',
                ],
                'fsSpecified' => [ // настройки специфичные для каждой файловой системы, в данном случае local storage или nfs-like
                    'path' => ROOT_PATH . '/filestorage/fs3', //путь к хранилищу
                    'secretKey' => '0be3@2f936a9874>1M2c30c!e5827d344652C',
                ],
            ],
            4 => [
                'id' => 4, //id сервера (тоже самое что и ключ)
                'fsVersion' => 1, //версия файловой системы
                'acceptNewFiles' => false, // принимает новые файлы
                'allowDeleteFiles' => false, // разрешено удаление файлов
                'liveChecker' => function () { //ф-ция проверки того что сервер "жив"
                    return true;
                },
                'web' => [
                    'scheme' => 'https',
                    'port' => 443,
                    'host' => 'fs.mamam.ua',
                    'path' => '/fs4',
                ],
                'fsSpecified' => [ // настройки специфичные для каждой файловой системы, в данном случае local storage или nfs-like
                    'path' => ROOT_PATH . '/filestorage/fs4', //путь к хранилищу
                    'secretKey' => 'qeui0Qd2#O9TJ=EFW?#20fl]dQmg[FguHF1!6',
                ],
            ],
            5 => [
                'id' => 5, //id сервера (тоже самое что и ключ)
                'fsVersion' => 2, //версия файловой системы
                'acceptNewFiles' => false, // принимает новые файлы
                'allowDeleteFiles' => false, // разрешено удаление файлов
                'liveChecker' => function () { //ф-ция проверки того что сервер "жив"
                    return true;
                },
                'web' => [
                    'scheme' => 'https',
                    'port' => 443,
                    'host' => 'fs.mamam.ua',
                    'path' => '/fs5',
                ],
                'fsSpecified' => [ // настройки специфичные для каждой файловой системы, в данном случае local storage или nfs-like
                    'path' => ROOT_PATH . '/filestorage/fs5', //путь к хранилищу
                    'secretKey' => 'RY2YUARxfUoWW2cqHXKuaUUZXqHTr9oeuRUSc',
                ],
            ],
            // активное хранилище
            10 => array(
                'id' => 10,
                'fsVersion' => 2,
                'acceptNewFiles' => true,
                'allowDeleteFiles' => true,
                'liveChecker' => function () {
                    return true;
                },
                'web' => array(
                    'scheme' => 'https',
                    'port' => 443,
                    'host' => getenv('PROJECTNAME'),
                    'path' => '/filestorage/fs10',
                ),
                'fsSpecified' => array(
                    'path' => ROOT_PATH . '/filestorage/fs10',
                    'secretKey' => '8NKp1WIqI42ZkSR8CzIINoIWXDNwALbGWa2'
                ),
            ),
        ],
        'mime' => [
            'modifiers' => [
                'image' => [
                    [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\Sanitize',
                        'options' => [
                            'emptyExif' => true, //очистить exif
                            'deleteFormatSpecifiedUnnecessaryData' => true, //удалить возможные дополнительные данные у изображения (например у png удалить скрытые chunks)
                            'deleteAnimation' => true //удалить анимацию
                        ],
                    ], [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\Compress',
                        'options' => [
                            'maxImageSize' => 5 * 1024 * 1024,
                            'maxImageResolution' => [1920, 1200],
                        ],
                    ], [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\Crop',
                        'options' => [ // пусто, потому не вырезать ничего

                        ],
                    ], [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\MakeThumbs',
                        'options' => [
                            'resolution' => [
                                //format: array(name, width, height, quality, method, methodParams), blur < 1 = sharpen, blur > 1 blurry, blur == 1 nothing
                                ['small60', 60, 70, 95, 'crop', ['blur' => 0.8]],
                                ['small70', 72, 86, 95, 'crop', ['blur' => 0.8]],
                                ['small150', 147, 183, 95, 'crop', ['blur' => 0.8]],
                                ['small150brand', 153, 93, 95, 'crop', ['blur' => 0.8]], //бренды
                                ['mid320promo', 322, 98, 95, 'crop', ['blur' => 0.9]], // разрешение для промо у акций
                                ['mid320', 322, 236, 95, 'crop', ['blur' => 0.9]], // акция в списке акция
                                ['mid320_prodlist', 322, 392, 95, 'crop', ['blur' => 0.9]], // товар в списке товаров
                                ['mid380', 380, 462, 95, 'crop', ['blur' => 0.9]],
                                ['big620mailing', 620, 331, 95, 'crop', ['blur' => 0.9]],

                                //рассылка
                                ['mid360promo', 357, 215, 95, 'propResize', ['blur' => 0.9]],
                                ['mid260promo', 261, 150, 95, 'crop', ['blur' => 0.9]],
                                ['mid210promo', 208, 99, 95, 'crop', ['blur' => 0.9]],
                                ['mid210prodlist', 205, 263, 95, 'crop', ['blur' => 0.9]],
                            ],
                        ],
                    ], [
                        'name' => 'MommyCom\YiiComponent\FileStorage\Modifiers\Image\EffectSharpen',
                        'options' => [
                            'maxImageResolution' => [640, 480],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
