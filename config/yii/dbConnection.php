<?php

return [
    'class' => \MommyCom\YiiComponent\Database\DbConnection::class,
    'connectionString' => 'mysql:host=' . getenv('DBHOST') . ';dbname=' . getenv('DBNAME'),
    'emulatePrepare' => true,
    'username' => getenv('DBUSER'),
    'password' => getenv('DBPASS'),
    'charset' => 'utf8',
    'schemaCachingDuration' => YII_DEBUG ? 10 : 3600,
    'enableProfiling' => YII_DEBUG,
    'enableParamLogging' => YII_DEBUG,
    'persistent' => true,
];
