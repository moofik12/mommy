<?php
return [
        'supportEmail' => 'support@mail.mommy.com',
        'distributionEmail' => 'mamam@mail.mommy.com',
        'salt' => '14523%#^yh8g&TBT5r21323',
        'remember' => 604800,

        //социальные группы
        'groupVk' => 'http://vk.com/mamamclub', //'http://vk.com/club56056518'
        'groupOd' => 'http://odnoklassniki.ru/group/51898779631829',
        'groupFb' => 'https://www.facebook.com/Mommycom-221697445038793/',
    ] + include __DIR__ . '/../params.php';
