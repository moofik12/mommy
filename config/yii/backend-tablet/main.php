<?php
Yii::setPathOfAlias('common', ROOT_PATH . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('frontend', ROOT_PATH . DIRECTORY_SEPARATOR . 'frontend');
Yii::setPathOfAlias('extensions', ROOT_PATH . DIRECTORY_SEPARATOR . 'extensions');

return [
    'basePath' => ROOT_PATH,
    'runtimePath' => ROOT_PATH . '/var',
    'viewPath' => ROOT_PATH . '/views/backend-tablet',
    'controllerPath' => ROOT_PATH . '/src/Controller/BackendTablet',
    'name' => 'MOMMY',
    'sourceLanguage' => 'en',
    'language' => getenv('LANGUAGE'),
    'charset' => 'utf-8',
    'defaultController' => 'index',
    'controllerNamespace' => 'MommyCom\Controller\BackendTablet',
    'import' => [
    ],
    'preload' => [
        'log', 'clientScript', 'registerAssets', 'bootstrap',
    ],
    'aliases' => [
        'components' => 'application.components',
        'ext' => 'extensions',
    ],
    'params' => include __DIR__ . '/params.php',
    'components' => [
        'db' => include __DIR__ . '/../dbConnection.php',
        'filestorage' => include __DIR__ . '/../filestorage_' . getenv('APP_ENV') . '.php',

        'urlManager' => [
            //'urlFormat' => 'path',
            'rules' => [
                '<action:(login|logout)>' => 'auth/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
            //'showScriptName'=>false,   //Убирает index.php
        ],
        'frontendUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'configApp' => include_once __DIR__ . '/../frontend/main.php', //include_once обязательно
            'showScriptName' => false,
        ],
        'user' => [
            'loginUrl' => ['auth/login'],
            'class' => \MommyCom\YiiComponent\BackendTablet\AdmWebUser::class,
            'allowAutoLogin' => true,
        ],
        'bootstrap' => [
            'class' => 'ext.yiiBooster.components.Bootstrap', // assuming you extracted bootstrap under extensions
            'coreCss' => true, // whether to register the Bootstrap core CSS (bootstrap.min.css), defaults to true
            'responsiveCss' => true, // whether to register the Bootstrap responsive CSS (bootstrap-responsive.min.css), default to false
            'enableBootboxJS' => false,
            'fontAwesomeCss' => true,
            'plugins' => [
                // Optionally you can configure the "global" plugins (button, popover, tooltip and transition)
                // To prevent a plugin from being loaded set it to false as demonstrated below
                'transition' => true,
                'tooltip' => [
                    'selector' => 'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
                    'options' => [
                        'placement' => 'bottom', // place the tooltips below instead
                    ],
                ],
                'typeahead' => true,
                'alert' => true,
                'dropdown' => true,
                'tab' => true,
                'yiiCss' => true,
            ],
        ],
        'assetManager' => [
            'class' => \MommyCom\YiiComponent\AssetManager::class,
            'autorefresh' => true
        ],
        'registerAssets' => [
            'class' => \MommyCom\YiiComponent\RegisterAssets::class,
            'packages' => []
        ],
        'format' => [
            'class' => \MommyCom\YiiComponent\Format::class,
            'booleanFormat' => ['no', 'yes'],
        ],
        'currencyFormatter' => [
            'class' => \MommyCom\Service\Deprecated\CurrencyFormatter::class,
        ],

        'request' => [
            'class' => \MommyCom\YiiComponent\SmartHttpRequest::class,
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'csrfTokenName' => '__backend_tablet_token',
            'csrfCookie' => [
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'cookieParams' => [
                'httpOnly' => true,
                'httponly' => true, // need test
            ],
            'sessionName' => 'PHPBSESSID',
        ],

        'log' => [
            'class' => 'CLogRouter',
            'enabled' => LOGS,
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'exception.*',
                    'logFile' => 'exceptions-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'system.*',
                    'logFile' => 'system-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'except' => 'exception.*, system.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'application.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'console.*',
                ],
            ],
        ],
        'widgetFactory' => include __DIR__ . '/widgetFactory.php',
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
    ],
];
