<?php
return [
    'class' => \MommyCom\YiiComponent\WidgetFactory::class,
    'widgets' => [
        'TbGridView' => [
            'enableHistory' => true,
            'type' => 'table bordered condensed hover striped',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbExtendedGridView' => [
            'enableHistory' => true,
            'fixedHeader' => true,
            'headerOffset' => 42,
            'type' => 'table bordered condensed hover striped',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbGroupGridView' => [
            'enableHistory' => true,
            'type' => 'table bordered condensed hover striped',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbDetailView' => [
            'type' => ['striped', 'bordered', 'condensed'],
        ],
        'GroupGridViewRelation' => [
            'enableHistory' => true,
            'type' => 'striped bordered',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbActiveForm' => [
            //'enableAjaxValidation'=>true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
            ],
        ],
        'TbDateRangePicker' => [
            'options' => [
                'format' => 'dd.MM.yyyy',
                'locale' => [
                    'applyLabel' => 'Сохр.',
                    'clearLabel' => 'Очистить',
                    'fromLabel' => 'От',
                    'toLabel' => 'До',
                    'firstDay' => 1,
                    'daysOfWeek' => ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                    'monthNames' => [
                        "Январь", "Февраль", "Март", "Апрель", "Май",
                        "Июнь", "Июль", "Август", "Сентябрь",
                        "Октябрь", "Ноябрь", "Декабрь",
                    ],
                ],
            ],
        ],
        'TbDatePicker' => [
            'options' => [
                'format' => 'dd.mm.yyyy',
                'languages' => 'ru',
            ],
        ],
        'TbTimePicker' => [
            'options' => [
                'minuteStep' => 15,
                'showMeridian' => false,
            ],
        ],
        'TbSelect2' => [
            'options' => [
                'formatSearching' => new CJavaScriptExpression('function() {return "Поиск..."; }'),
                'formatNoMatches' => new CJavaScriptExpression('function(term) {return "<span class=\"text-error\">Не найдено</span>"}'),
                'formatInputTooShort' => new CJavaScriptExpression('function (input, min)
                            { var count = min - input.length;
                            return "Введите " + count + (count == 1 ? " символ" : " символа") +" для начала поиска"; }'),
                'formatLoadMore' => new CJavaScriptExpression('function (pageNumber) { return "Загрузка..."; }'),
                'formatSelectionTooBig' => new CJavaScriptExpression('function (limit)
                            { return "Вы можете выбрать только " + limit + " item" + (limit == 1 ? "" : "s"); }'),
            ],
        ],
    ],
];
