<?php

Yii::setPathOfAlias('common', ROOT_PATH . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('frontend', ROOT_PATH . DIRECTORY_SEPARATOR . 'frontend');
Yii::setPathOfAlias('extensions', ROOT_PATH . DIRECTORY_SEPARATOR . 'extensions');
Yii::getPathOfAlias('assets') or Yii::setPathOfAlias('assets', ROOT_PATH . '/assets/frontend-mobile');

//кука ставится на домен 2-го уровня
$userCookie = isset($_SERVER['SERVER_NAME']) ?
    ['domain' => '.' . implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2))] : [];

return [
    'basePath' => ROOT_PATH,
    'runtimePath' => ROOT_PATH . '/var',
    'viewPath' => ROOT_PATH . '/views/frontend-mobile',
    'controllerPath' => ROOT_PATH . '/src/Controller/FrontendMobile',
    'name' => 'MOMMY',
    'id' => 'MOMMY', //уникальный ID для всего приложения
    'sourceLanguage' => 'en',
    'language' => getenv('LANGUAGE'),
    'defaultController' => 'index',
    'controllerNamespace' => 'MommyCom\Controller\FrontendMobile',
    'import' => [],
    'preload' => [
        'log', 'clientScript', 'registerAssets', 'extSelect',
    ],
    'aliases' => [
        'components' => 'application.components',
        'ext' => 'extensions',
    ],
    'behaviors' => [
        \MommyCom\YiiComponent\Behavior\MobileRedirectBehavior::class,
        \MommyCom\YiiComponent\Behavior\UserPreferredLanguageSetter::class,
    ],
    'params' => include __DIR__ . '/params.php',
    'catchAllRequest' => @file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . 'maintenance') ? ['maintenance'] : null,

    'components' => [
        'filestorage' => include __DIR__ . '/../filestorage_' . getenv('APP_ENV') . '.php',
        'db' => include __DIR__ . '/../dbConnection.php',

        //for debug
        'assetManager' => [
            'forceCopy' => false,
        ],
        'messages' => [
            'basePath' => ROOT_PATH . DIRECTORY_SEPARATOR . 'messages',
        ],
        'frontendUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'scriptName' => 'index.php',
            'host' => 'https://' . getenv('PROJECTNAME'),
            'configApp' => include_once __DIR__ . '/../frontend/main.php', //include_once обязательно
        ],
        'securityManager' => [
            'validationKey' => 'JSeq3nal5XxfiiT8Oi52J15W9tb6qsRy',
        ],
        'cache' => ['class' => \MommyCom\YiiComponent\Cache\Psr6CacheBridge::class],
        'urlManager' => [
            'urlFormat' => 'path',
            'rules' => [
                '' => 'index/index', // default is index file
                //static page
                'instruction' => 'instruction/index',
                'static/<category>/<page>' => 'static/index',

                'category/<category>' => 'event/category',
                'age/<age>/<target>' => 'event/age',
                'brand/<name>' => 'event/brand',
                'usersProvider/customCampaign/<name>' => 'usersProvider/customCampaign',
                'pay/index/<uid>' => 'pay/index',

                'product/<eventId>/<id>' => 'product/index',
                'login' => 'auth/auth/name/login',
                'registration' => 'auth/auth/name/registration',
                'past' => 'past/index',

                '<controller:\w+>/<id:\d+>' => '<controller>/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<name:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                '125' => 'lottery/index',
            ],
            'showScriptName' => false,   //Убирает index1.php
        ],

        'mobileUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'scriptName' => 'frontend-mobile.php',
            'host' => 'http://m.' . getenv('PROJECTNAME'),
            'configApp' => include_once __DIR__ . '/../frontend-mobile/main.php', //include_once обязательно
        ],

        'session' => [
            'cookieParams' => [
                    'httpOnly' => true,
                    'httponly' => true, // need test
                ] + $userCookie,
        ],
        'user' => [
            'loginUrl' => ['auth/auth'],
            'class' => MommyCom\YiiComponent\AuthManager\ShopWebUser::class,
            'allowAutoLogin' => true,
            'identityCookie' => $userCookie,
        ],
        //одинаковый стейт между приложениями, авторизация по куке
        'statePersister' => [
            'stateFile' => Yii::getPathOfAlias('frontend') . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'state.bin',
        ],
        'errorHandler' => [
            // use 'site/error' action to display errors
            'errorAction' => 'error/index',
        ],

        'userAgent' => [
            'class' => \MommyCom\YiiComponent\UserAgent::class,
        ],

        'userReferer' => [
            'class' => \MommyCom\YiiComponent\UserReferer::class,
        ],

        'clientScript' => [
            'class' => \MommyCom\YiiComponent\ClientScript::class,
            'minify' => true,
        ],

        'viewRenderer' => [
            'class' => \MommyCom\YiiComponent\ViewRenderer::class,
            'minify' => true,
        ],

        'extSelect' => [
            'class' => 'ext.extSelect.components.ExtendedSelectMobile',
            'coreCss' => false // disable css
        ],

        'registerAssets' => [
            'class' => \MommyCom\YiiComponent\RegisterAssets::class,

            'packages' => [
                'jquery' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'jquery/jquery.min.js',
                        'jquery/jquery.serialize-object.js',
                        'jqueryCookie/jquery.cookie.js',
                        // jquery lazy load
                        'jqueryLazyload/jquery.scrollstop.min.js',
                        'jqueryLazyload/jquery.lazyload.js',
                    ],
                    'render' => true,
                ],

                'jqueryTools' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'jqueryTools/jquery-browser-detection.js',
                        'jqueryTools/jquery-tools.min.js',
                    ],
                    'render' => true,
                ],

                'jqueryLazyload' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'jqueryLazyload/jquery.scrollstop.min.js',
                    ],
                    'render' => true,
                ],

                'underscore' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'underscore/underscore.min.js',
                    ],
                    'render' => true,
                ],

                'countdown' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'countdown/countdown.min.js',
                        'countdown/date.iso8601.js',
                    ],
                    'render' => true,
                ],

                'markup' => [
                    'basePath' => 'assets.markup',
                    'js' => [
                        'js/html5.js',
                        'js/bootstrap-dropdown.js',
                        'js/bootstrap-modal.js',
                        'js/bootstrap-collapse.js',
                    ],
                    'css' => [
                        'css/style.css',
                        'css/select.css',
                    ],
                    'render' => true,
                ],

                'front' => [
                    'basePath' => 'assets.front',
                    'js' => [
                        'js/i18n.js',
                        'js/event.tracker.js',
                        'js/registration.tracker.js',
                        'js/mamam.js',
                        'js/mamam.countdown.js',
                        'js/mamam.cart.js',
                        'js/mamam.imagesLazyload.js',
                        'js/mamam.actionpay.js',
                        'js/mamam.admitad.js',
                        'js/mamam.rtbhouse.js',
                        'js/mamam.mailru.js',
                        'js/mamam.app.js',
                        'js/mamam.push.js',
                        'js/mamam.ecommerce.js',
                        'js/mamam.gaEvent.js',
                        'js/mamam.visibility.js',

                        'js/jquery.filterableGoods.js',
                        'js/jquery.numericalSpinner.js',
                        'js/jquery.simpleExtSelect.js',
                        'js/jquery.productCarousel.js',
                        'js/jquery.yiierror.fix.js',
                        'js/jquery.showOnce.js',
                    ],
                    'jsActions' => [
                        'actions/setup.js',
                        'actions/footer.js',
                        'actions/<controller>-<action>.js',
                        'actions/<controller>.js',
                    ],
                    'depends' => ['yiiactiveform'],
                    'render' => true,
                ],

                'lottery' => [
                    'basePath' => 'assets.markup',
                    'css' => [
                        'css/lottery.css',
                    ],
                    'depends' => ['markup'],
                    'render' => false,
                    'forceOverride' => YII_DEBUG,
                ],
            ],
        ],
        'request' => [
            'class' => \MommyCom\YiiComponent\SmartHttpRequest::class,
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'csrfTokenName' => '__token',
            'csrfCookie' => [
                'httpOnly' => true,
            ],
            'noCsrfValidationRoutes' => ['pay/receive', 'pay/result', 'tracker/record'],
        ],
        'format' => [
            'class' => \MommyCom\YiiComponent\Format::class,
            'booleanFormat' => ['нет', 'да'],
        ],
        'timerFormatter' => [
            'class' => \MommyCom\YiiComponent\FrontendMobile\TimerFormatter::class,
        ],
        'mailer' => [
            'class' => \MommyCom\YiiComponent\Emailer::class,
            'logCategory' => 'emailer',
            'logging' => true,
            'viewPath' => 'application.views.common.email',
            'defaultLayout' => 'application.views.common.layouts.email',
        ],
        'sms' => [
            //рассылка через /frontend/commands/MessageCommand
            'class' => \MommyCom\YiiComponent\Sms\SmsSender::class,
            'from' => 'MAMAM',
            'level' => 'info',
            'category' => 'sms',
            'provider' => [
                'class' => \MommyCom\YiiComponent\Sms\SmsProviderAlphaSms::class,
                'login' => '380671181163',
                'password' => 'fJdij43eE',
            ],
        ],
        'log' => [
            'class' => 'CLogRouter',
            'enabled' => LOGS,
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'exception.*',
                    'logFile' => 'exceptions-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'system.*',
                    'logFile' => 'system-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'except' => 'exception.*, system.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'application.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'console.*',
                ],
            ],
        ],
    ],
];
