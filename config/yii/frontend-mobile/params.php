<?php
return [
        'fullVersion' => 'https://mommy.com',

        'googleTagManagerId' => getenv('GOOGLE_TAG_MANAGER_ID'),

        'salt' => '14523%#^yh8g&TBT5r21323',
        'remember' => 31556926, // one year

        'supportEmail' => 'support@mail.mommy.com',
        'distributionEmail' => 'mamam@mail.mommy.com',
        'distributionEmailName' => 'MOMMY.COM',
        'phone' => getenv('PHONE'),

        'unisenderActiveUserList' => 3147794, //список активных пользователей + новых от 24.04.2014

        //социальные группы
        'groupVk' => 'https://vk.com/mamamclub', //'http://vk.com/club56056518'
        'groupOd' => 'https://odnoklassniki.ru/group/51898779631829',
        'groupFb' => 'https://www.facebook.com/Mommycom-221697445038793/',

        'languages' => [
            'ru' => 'Русский',
            'en' => 'English',
            'in' => 'Indonesian',
            'ro' => 'Romanian',
            'vi' => 'Vietnamese',
            'co' => 'Colombian',
        ],
        'newsletters' => false,
    ] + include __DIR__ . '/../params.php';
