<?php
return [
    'class' => \MommyCom\YiiComponent\WidgetFactory::class,
    'widgets' => [
        'TbGridView' => [
            'enableHistory' => true,
            'type' => 'table bordered condensed hover striped',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbExtendedGridView' => [
            'enableHistory' => true,
            'fixedHeader' => true,
            'headerOffset' => 42,
            'type' => 'table bordered condensed hover striped',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbGroupGridView' => [
            'enableHistory' => true,
            'type' => 'table bordered condensed hover striped',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbDetailView' => [
            'type' => ['striped', 'bordered', 'condensed'],
        ],
        'GroupGridViewRelation' => [
            'enableHistory' => true,
            'type' => 'striped bordered',
            'pagerCssClass' => 'pagination pagination-right',
            'pager' => [
                'class' => 'bootstrap.widgets.TbPager',
                'displayFirstAndLast' => true,
                'maxButtonCount' => 15,
            ],
            'template' => "{summary}\n{pager}\n{items}\n{pager}",
        ],
        'TbActiveForm' => [
            //'enableAjaxValidation'=>true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
            ],
        ],
        'TbDateRangePicker' => [
            'options' => [
                'format' => 'dd.MM.yyyy',
                'locale' => [
                    'applyLabel' => \Yii::t('backend', 'Save'),
                    'clearLabel' => \Yii::t('backend', 'Clear'),
                    'fromLabel' => \Yii::t('backend', 'From'),
                    'toLabel' => \Yii::t('backend', 'To'),
                    'firstDay' => 1,
                    'daysOfWeek' => [
                        \Yii::t('backend', "Sun"),
                        \Yii::t('backend', "Mon"),
                        \Yii::t('backend', "Tue"),
                        \Yii::t('backend', "Wed"),
                        \Yii::t('backend', "Thu"),
                        \Yii::t('backend', "Fri"),
                        \Yii::t('backend', "Sat"),
                    ],
                    'monthNames' => [
                        \Yii::t('backend', "January"),
                        \Yii::t('backend', "February"),
                        \Yii::t('backend', "March"),
                        \Yii::t('backend', "April"),
                        \Yii::t('backend', "May"),
                        \Yii::t('backend', "June"),
                        \Yii::t('backend', "July"),
                        \Yii::t('backend', "August"),
                        \Yii::t('backend', "September"),
                        \Yii::t('backend', "October"),
                        \Yii::t('backend', "November"),
                        \Yii::t('backend', "December"),
                    ],
                ],
            ],
        ],
        'TbDatePicker' => [
            'options' => [
                'format' => 'dd.mm.yyyy',
                'languages' => 'ru',
            ],
        ],
        'TbTimePicker' => [
            'options' => [
                'minuteStep' => 15,
                'showMeridian' => false,
            ],
        ],
        'TbSelect2' => [
            'options' => [
                'formatSearching' => new CJavaScriptExpression('function() {return "Поиск..."; }'),
                'formatNoMatches' => new CJavaScriptExpression('function(term) {return "<span class=\"text-error\">Not found</span>"}'),
                'formatInputTooShort' => new CJavaScriptExpression('function (input, min)
                            { var count = min - input.length;
                            return "Введите " + count + (count == 1 ? " символ" : " символа") +" для начала поиска"; }'),
                'formatLoadMore' => new CJavaScriptExpression('function (pageNumber) { return "Загрузка..."; }'),
                'formatSelectionTooBig' => new CJavaScriptExpression('function (limit)
                            { return "Вы можете выбрать только " + limit + " item" + (limit == 1 ? "" : "s"); }'),
            ],
        ],
    ],
];
