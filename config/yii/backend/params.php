<?php
return [
        'supportEmail' => 'support@mail.mommy.com',
        'distributionEmail' => 'mamam@mail.mommy.com',
        'distributionEmailName' => 'MOMMY.COM',
        'salt' => '14523%#^yh8g&TBT5r21323',
        'remember' => 604800,
        'phone' => '(044) 374 50 40',
        'company' => 'ООО МАМАМ',

        // социальные группы
        'groupVk' => 'http://vk.com/mamamclub', //'http://vk.com/club56056518'
        'groupOd' => 'http://odnoklassniki.ru/group/51898779631829',
        'groupFb' => 'https://www.facebook.com/Mommycom-221697445038793/',

        // adsender token
        'adsender' => [
            'token' => 'dc453b9fc42601a202bc077a75067ca5',
            'platform' => 1,
        ],

        'requisites' => [
            'recipient' => 'ТОВ "Мамам"',
            'companyId' => '388 044 72', // ОКПО
            'bank' => 'Приватбанк',
            'sortCode' => '300 711', // МФО
            'checkingAccount' => '2600 7052 7266 42', // р/с

            'requisitesSMS' => "ТОВ 'МАМАМ'\nОКПО 38804472\nМФО 300711\nр/с 2600 7052 7266 42",
        ],

        'languages' => ['ru' => 'Русский', 'en' => 'English'],

        'backendFlag' => true,
    ] + include __DIR__ . '/../params.php';
