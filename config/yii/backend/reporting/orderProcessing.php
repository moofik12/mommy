<?php

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderProcessingStatusHistoryRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'timeColumn' => 'time',
    'report' => [
        'groups' => [
            [
                'label' => Translator::t('By hours'),
                'name' => 'hours',
                'expression' => '
                    YEAR(FROM_UNIXTIME({time})),
                    MONTH(FROM_UNIXTIME({time})),
                    DAY(FROM_UNIXTIME({time})),
                    HOUR(FROM_UNIXTIME({time}))
                ',
                'filterExpression' => '
                    YEAR(FROM_UNIXTIME({time})) = YEAR(FROM_UNIXTIME({filter.time})) AND
                    MONTH(FROM_UNIXTIME({time})) = MONTH(FROM_UNIXTIME({filter.time})) AND
                    DAY(FROM_UNIXTIME({time})) = DAY(FROM_UNIXTIME({filter.time})) AND
                    HOUR(FROM_UNIXTIME({time})) = HOUR(FROM_UNIXTIME({filter.time}))
                ',
            ],
            [
                'label' => Translator::t('By days'),
                'name' => 'days',
                'expression' => '
                    YEAR(FROM_UNIXTIME({time})),
                    MONTH(FROM_UNIXTIME({time})),
                    DAY(FROM_UNIXTIME({time}))
                ',
                'filterExpression' => '
                    YEAR(FROM_UNIXTIME({time})) = YEAR(FROM_UNIXTIME({filter.time})) AND
                    MONTH(FROM_UNIXTIME({time})) = MONTH(FROM_UNIXTIME({filter.time})) AND
                    DAY(FROM_UNIXTIME({time})) = DAY(FROM_UNIXTIME({filter.time}))
                ',
            ],
            [
                'label' => Translator::t('By months'),
                'name' => 'months',
                'expression' => '
                    YEAR(FROM_UNIXTIME({time})),
                    MONTH(FROM_UNIXTIME({time}))
                ',
                'filterExpression' => '
                    YEAR(FROM_UNIXTIME({time})) = YEAR(FROM_UNIXTIME({filter.time})) AND
                    MONTH(FROM_UNIXTIME({time})) = MONTH(FROM_UNIXTIME({filter.time}))
                ',
            ],
        ],
        'customGroups' => [
            [
                'label' => Translator::t('By users'),
                'name' => 'admins',
                'expression' => '{user_id}',
                'filterExpression' => '{user_id} = {filter.user_id}',
            ],
        ],
        'columns' => [
            [
                'label' => Translator::t('Date'), // Заголовок
                'format' => 'datetime',
                'name' => 'time', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => 'FROM_UNIXTIME({time}, "%d.%m.%Y %H:00")',
                'visibleOn' => 'hours',
            ],
            [
                'label' => Translator::t('Date'), // Заголовок
                'format' => 'date',
                'name' => 'time', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => 'FROM_UNIXTIME({time}, "%d.%m.%Y")',
                'visibleOn' => 'days',
            ],
            [
                'label' => Translator::t('Date'), // Заголовок
                'format' => 'date',
                'name' => 'time', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => 'FROM_UNIXTIME({time}, "%d.%m.%Y")',
                'visibleOn' => 'months',
            ],
            [
                'label' => Translator::t('User'),
                'format' => 'string',
                'name' => 'admin',
                'valueListType' => 'children',
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    /* @var $childrenProvider CActiveDataProvider */
                    /* @var $childrenSelector OrderProcessingStatusHistoryRecord */
                    $userIdArray = $childrenSelector->findColumnDistinct('user_id');
                    $userId = reset($userIdArray);
                    if ($userId > 0 && ($user = AdminUserRecord::model()->findByPk($userId))) {
                        return $user->fullname;
                    }
                    return 'Клиенты';
                },
                'visibleOn' => 'admins',
            ],
            [
                'label' => Translator::t('New'),
                'format' => 'integer',
                'name' => 'orderNewCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    /* @var $childrenSelector OrderProcessingStatusHistoryRecord */
                    return $childrenSelector->statusIn([
                        OrderRecord::PROCESSING_UNMODERATED,
                    ])->groupBy('order_id')->count();
                },
            ],
            [
                'label' => Translator::t('Confirmed'),
                'format' => 'integer',
                'name' => 'orderConfirmedCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    /* @var $childrenSelector OrderProcessingStatusHistoryRecord */
                    return $childrenSelector->statusIn([
                        OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
                        OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
                    ])->groupBy('order_id')->count();
                },
            ],
            [
                'label' => Translator::t('Canceled'),
                'format' => 'integer',
                'name' => 'orderCancelledCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    /* @var $childrenSelector OrderProcessingStatusHistoryRecord */
                    return $childrenSelector->statusIn([
                        OrderRecord::PROCESSING_CANCELLED,
                        OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER,
                    ])->groupBy('order_id')->count();
                },
            ],
            [
                'label' => Translator::t('Packed'),
                'format' => 'integer',
                'name' => 'orderPackagedCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    /* @var $childrenSelector OrderProcessingStatusHistoryRecord */
                    return $childrenSelector->statusIn([
                        OrderRecord::PROCESSING_STOREKEEPER_PACKAGED,
                    ])->groupBy('order_id')->count();
                },
            ],
            [
                'label' => Translator::t('Dispatched'),
                'format' => 'integer',
                'name' => 'orderMailedCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    /* @var $childrenSelector OrderProcessingStatusHistoryRecord */
                    return $childrenSelector->statusIn([
                        OrderRecord::PROCESSING_STOREKEEPER_MAILED,
                    ])->groupBy('order_id')->count();
                },
            ],
        ],
    ],
];
