<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Facade\Translator;

return [
    'timeColumn' => 'created_at',
    'report' => [
        'with' => [
            'positionsCallcenterAccepted' => ['together' => true],
            'user' => ['together' => true],
        ],
        'groups' => [
            [
                'label' => Translator::t('By hours'),
                'name' => 'hours',
                'expression' => '
                    YEAR(FROM_UNIXTIME({created_at})),
                    MONTH(FROM_UNIXTIME({created_at})),
                    DAY(FROM_UNIXTIME({created_at})),
                    HOUR(FROM_UNIXTIME({created_at}))
                ',
                'filterExpression' => '
                    YEAR(FROM_UNIXTIME({created_at})) = YEAR(FROM_UNIXTIME({filter.created_at})) AND
                    MONTH(FROM_UNIXTIME({created_at})) = MONTH(FROM_UNIXTIME({filter.created_at})) AND
                    DAY(FROM_UNIXTIME({created_at})) = DAY(FROM_UNIXTIME({filter.created_at})) AND
                    HOUR(FROM_UNIXTIME({created_at})) = HOUR(FROM_UNIXTIME({filter.created_at}))
                ',
            ],
            [
                'label' => Translator::t('By days'),
                'name' => 'days',
                'expression' => '
                    YEAR(FROM_UNIXTIME({created_at})),
                    MONTH(FROM_UNIXTIME({created_at})),
                    DAY(FROM_UNIXTIME({created_at}))
                ',
                'filterExpression' => '
                    YEAR(FROM_UNIXTIME({created_at})) = YEAR(FROM_UNIXTIME({filter.created_at})) AND
                    MONTH(FROM_UNIXTIME({created_at})) = MONTH(FROM_UNIXTIME({filter.created_at})) AND
                    DAY(FROM_UNIXTIME({created_at})) = DAY(FROM_UNIXTIME({filter.created_at}))
                ',
            ],
            [
                'label' => Translator::t('By months'),
                'name' => 'months',
                'expression' => '
                    YEAR(FROM_UNIXTIME({created_at})),
                    MONTH(FROM_UNIXTIME({created_at}))
                ',
                'filterExpression' => '
                    YEAR(FROM_UNIXTIME({created_at})) = YEAR(FROM_UNIXTIME({filter.created_at})) AND
                    MONTH(FROM_UNIXTIME({created_at})) = MONTH(FROM_UNIXTIME({filter.created_at}))
                ',
            ],
        ],
        'customGroups' => [
            [
                'label' => Translator::t('By landing'),
                'name' => 'landingNum',
                'expression' => '{user.landing_num}',
                'filterExpression' => '{user.landing_num} = {filter.user.landing_num}',
            ],
        ],
        'columns' => [
            [
                'label' => Translator::t('Date'), // Заголовок
                'format' => 'datetime',
                'name' => 'time', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => 'FROM_UNIXTIME({created_at}, "%d.%m.%Y %H:00")',
                'visibleOn' => 'hours',
            ],
            [
                'label' => Translator::t('Date'), // Заголовок
                'format' => 'date',
                'name' => 'time', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => 'FROM_UNIXTIME({created_at}, "%d.%m.%Y")',
                'visibleOn' => 'days',
            ],
            [
                'label' => Translator::t('Date'), // Заголовок
                'format' => 'date',
                'name' => 'time', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => 'FROM_UNIXTIME({created_at}, "%d.%m.%Y")',
                'visibleOn' => 'months',
            ],
            [
                'label' => Translator::t('Landing'), // Заголовок
                'format' => 'integer',
                'name' => 'landingNum', // Название присваемой колонки
                'valueListType' => 'current', // Тип списка из которого будут изьяты данные, current - текущий
                'valueType' => 'sql',
                'value' => '{user.landing_num}',
                'visibleOn' => 'landingNum',
            ],
            [
                'label' => Translator::t('Orders'),
                'format' => 'integer',
                'name' => 'orderCount',
                'valueListType' => 'current', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'sql',
                'value' => 'COUNT(DISTINCT {id})',
            ],
            [
                'label' => Translator::t('Confirmed'),
                'format' => 'integer',
                'name' => 'orderConfirmedCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    return $childrenSelector->processingStatuses([
                        OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
                        OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
                        OrderRecord::PROCESSING_STOREKEEPER_PACKAGED,
                        OrderRecord::PROCESSING_STOREKEEPER_MAILED,
                    ])->findColumnDistinctCount('id');
                },
            ],
            [
                'label' => Translator::t('Canceled'),
                'format' => 'integer',
                'name' => 'orderCancelledCount',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    return $childrenSelector->processingStatuses([
                        OrderRecord::PROCESSING_CANCELLED,
                        OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER,
                    ])->findColumnDistinctCount('id');
                },
            ],
            [
                'label' => Translator::t('Bonuses used'),
                'format' => 'currency',
                'name' => 'orderBonuspoints',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    $selector = $childrenSelector->processingStatuses([
                        OrderRecord::PROCESSING_UNMODERATED,
                        OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
                        OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
                        OrderRecord::PROCESSING_STOREKEEPER_PACKAGED,
                        OrderRecord::PROCESSING_STOREKEEPER_MAILED,
                    ]);
                    return $selector->groupBy('id')->findColumnSum("bonuses");
                },
            ],
            [
                'label' => Translator::t('Total'),
                'format' => 'currency',
                'name' => 'orderPrice',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'expression',
                'value' => function ($item, $row, $currentSelector, $childrenProvider, $childrenSelector) {
                    $selector = $childrenSelector->processingStatuses([
                        OrderRecord::PROCESSING_UNMODERATED,
                        OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
                        OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED,
                        OrderRecord::PROCESSING_STOREKEEPER_PACKAGED,
                        OrderRecord::PROCESSING_STOREKEEPER_MAILED,
                    ]);
                    return $selector->groupBy('id')->findColumnSum("price_total");
                },
            ],
            [
                'label' => Translator::t('Avg. units'),
                'format' => 'float',
                'name' => 'avgPositions',
                'valueListType' => 'children', // children - не сгруппированный но отфильтрованный по данным условиям список
                'valueType' => 'sql',
                'value' => 'ROUND(SUM({positionsCallcenterAccepted.number}) / COUNT(DISTINCT {positionsCallcenterAccepted.order_id}), 1)',
            ],
        ],
    ],
];
