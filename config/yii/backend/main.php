<?php
Yii::setPathOfAlias('common', ROOT_PATH . DIRECTORY_SEPARATOR . 'common');
Yii::setPathOfAlias('frontend', ROOT_PATH . DIRECTORY_SEPARATOR . 'frontend');
Yii::setPathOfAlias('extensions', ROOT_PATH . DIRECTORY_SEPARATOR . 'extensions');
Yii::getPathOfAlias('assets') or Yii::setPathOfAlias('assets', ROOT_PATH . '/assets/backend');
Yii::getPathOfAlias('widgets') or Yii::setPathOfAlias('widgets', ROOT_PATH . '/src/Widget/Backend');

return [
    'basePath' => ROOT_PATH,
    'runtimePath' => ROOT_PATH . '/var',
    'viewPath' => ROOT_PATH . '/views/backend',
    'controllerPath' => ROOT_PATH . '/src/Controller/Backend',
    'name' => 'MOMMY',
    'sourceLanguage' => 'en',
    'language' => 'en',
    'charset' => 'utf-8',
    'defaultController' => 'index',
    'controllerNamespace' => 'MommyCom\Controller\Backend',
    'import' => [
        'ext.yiiBooster.widgets.*',
        'ext.yiiBooster.widgets.input',
    ],
    'preload' => [
        'log', 'clientScript', 'registerAssets', 'bootstrap',
    ],
    'aliases' => [
        'components' => 'application.components',
        'ext' => 'extensions',
    ],
    'behaviors' => [
        ['class' => \MommyCom\YiiComponent\Behavior\ConfigureTranslatorBehavior::class, 'category' => 'backend'],
    ],
    'params' => include __DIR__ . '/params.php',
    'components' => [
        'db' => include __DIR__ . '/../dbConnection.php',
        'filestorage' => include __DIR__ . '/../filestorage_' . getenv('APP_ENV') . '.php',

        'cache' => ['class' => \CDummyCache::class],
        'cacheSecond' => ['class' => \MommyCom\YiiComponent\Cache\Psr6CacheBridge::class],
        'securityManager' => [
            'validationKey' => 'JSeq3nal5XxfiiT8Oi52J15W9tb6qsRy',
        ],
        'messages' => [
            'basePath' => ROOT_PATH . DIRECTORY_SEPARATOR . 'messages',
        ],
        'urlManager' => [
            //'urlFormat' => 'path',
            'rules' => [
                '<action:(login|logout)>' => 'auth/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
            //'showScriptName'=>false,   //Убирает index.php
        ],
        'frontendUrlManager' => [
            'class' => \MommyCom\YiiComponent\BuildUrlManager::class,
            'configApp' => include_once __DIR__ . '/../frontend/main.php', //include_once обязательно
        ],
        'user' => [
            'loginUrl' => ['auth/login'],
            'class' => \MommyCom\YiiComponent\Backend\AdmAuthManager\AdmWebUser::class,
            'allowAutoLogin' => true,
            'allowController' => ['auth', 'index', 'error'],
        ],
        'bootstrap' => [
            'class' => 'ext.yiiBooster.components.Bootstrap', // assuming you extracted bootstrap under extensions
            'coreCss' => true, // whether to register the Bootstrap core CSS (bootstrap.min.css), defaults to true
            'responsiveCss' => true, // whether to register the Bootstrap responsive CSS (bootstrap-responsive.min.css), default to false
            'enableBootboxJS' => false,
            'fontAwesomeCss' => true,
            'plugins' => [
                // Optionally you can configure the "global" plugins (button, popover, tooltip and transition)
                // To prevent a plugin from being loaded set it to false as demonstrated below
                'transition' => true,
                'tooltip' => [
                    'selector' => 'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
                    'options' => [
                        'placement' => 'bottom', // place the tooltips below instead
                    ],
                ],
                'typeahead' => true,
                'alert' => true,
                'dropdown' => true,
                'tab' => true,
                'yiiCss' => true,
            ],
        ],
        'assetManager' => [
            'class' => \MommyCom\YiiComponent\AssetManager::class,
            'autorefresh' => true,
        ],
        'registerAssets' => [
            'class' => \MommyCom\YiiComponent\RegisterAssets::class,
            'packages' => [
                'jquery' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        // jquery base
                        'jquery/jquery.min.js',
                    ],
                    'render' => true,
                ],
                'jquery.ui' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'jquery/jui-1.9.2/js/jquery-ui.min.js',
                        'jquery/jui-1.9.2/js/jquery-ui-i18n.min.js',
                    ],
                    'css' => [
                        'jquery/jui-1.9.2/css/base/jquery-ui.css',
                    ],
                    'depends' => ['jquery'],
                ],
                'growl' => [
                    'basePath' => 'assets.growl',
                    'css' => [
                        'stylesheets/jquery.growl.css',
                    ],
                    'js' => [
                        'javascripts/jquery.growl.js',
                    ],
                    'depends' => ['jquery'],
                ],
                'bootstrapAdmin' => [
                    'basePath' => 'assets.markup',
                    'css' => [
                        'css/style.css',
                        'css/login.css',
                        'css/error.css',
                        'css/theme.css',
                    ],
                    'js' => [
                        'js/main.js',
                    ],
                ],
                'labels' => [
                    'basePath' => 'assets.labels',
                    'css' => [
                        'css/labels.css',
                    ],
                ],
                'coupon' => [
                    'basePath' => 'assets.coupon',
                    'css' => [
                        'coupon.css',
                    ],
                    'render' => false,
                ],

                'front' => [
                    'basePath' => 'assets.actions',
                    'jsActions' => [
                        'setup.js',
                        '<controller>-<action>.js',
                        '<controller>.js',
                    ],
                    'render' => true,
                ],

                'jcrop' => [
                    'basePath' => 'assets.libs',
                    'js' => [
                        'jcrop/js/jquery.Jcrop.min.js',
                    ],
                    'css' => [
                        'jcrop/css/jquery.Jcrop.min.css',
                    ],
                    'depends' => ['jquery'],
                    'render' => false,
                ],
            ],
        ],

        'format' => [
            'class' => \MommyCom\YiiComponent\Format::class,
            'booleanFormat' => ['no', 'yes'],
        ],
        'request' => [
            'class' => \MommyCom\YiiComponent\SmartHttpRequest::class,
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'csrfTokenName' => '__backend_token',
            'csrfCookie' => [
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'cookieParams' => [
                'httpOnly' => true,
                'httponly' => true, // need test
            ],
            'sessionName' => 'PHPBSESSID',
        ],
        'mailer' => [
            'class' => \MommyCom\YiiComponent\Emailer::class,
            'logCategory' => 'emailer',
            'logging' => true,
            'viewPath' => 'application.views.common.email',
            'defaultLayout' => 'application.views.common.layouts.email',
        ],
        'sms' => [
            //рассылка через /frontend/commands/MessageCommand
            'class' => \MommyCom\YiiComponent\Sms\SmsSender::class,
            'from' => 'MAMAM',
            'level' => 'info',
            'category' => 'sms',
            'provider' => [
                'class' => \MommyCom\YiiComponent\Sms\SmsProviderAlphaSms::class,
                'login' => '380671181163',
                'password' => 'fJdij43eE',
            ],
        ],
        'mailerServices' => [
            'class' => \MommyCom\YiiComponent\MailerServices\MailerServices::class,
            'config' => [
                'unisender' => [
                    'key' => '56wrq6tsrea4ndd5n3tuydq35bo4c3s5c6wk8wge',
                    'listId' => 5365450, //на backend работа с другим списком (список активных пользователей)
                ],
            ],
            'activeService' => ['unisender'],
        ],

        'log' => [
            'class' => 'CLogRouter',
            'enabled' => LOGS,
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'exception.*',
                    'logFile' => 'exceptions-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'categories' => 'system.*',
                    'logFile' => 'system-log.log',
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'except' => 'exception.*, system.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'application.*',
                ],
                [
                    'class' => 'CWebLogRoute',
                    'levels' => 'trace, info, profiling',
                    'showInFireBug' => true,
                    'categories' => 'console.*',
                ],
            ],
        ],
        'widgetFactory' => include __DIR__ . '/widgetFactory.php',
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
    ],
];
