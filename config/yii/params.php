<?php

use MommyCom\Service\Region\Region;

defined('CHINA_PRODUCTS') || define('CHINA_PRODUCTS', 0);
defined('LOCAL_PRODUCTS') || define('LOCAL_PRODUCTS', 1);
defined('CHINA_LOCAL_PRODUCTS') || define('CHINA_LOCAL_PRODUCTS', 2);
defined('EMAIL_FIELD') || define('EMAIL_FIELD', 0);
defined('EMAIL_PHONE_NAME_FIELD') || define('EMAIL_PHONE_NAME_FIELD', 1);
defined('NATIVE_DB') || define('NATIVE_DB', 0);
defined('NATIVE_DB_BACKUP') || define('NATIVE_DB_BACKUP', 1);

return [
    'telegram' => [
        'dream_team_telegram_chat_id' => '-1001219241863',
        'events' => [
            'min_critical_events_count' => 5,
            'cache_key_telegram_bot_few_events' => 'telegram_bot_few_events',
            'cache_key_telegram_bot_few_events_ttl' => 3600,
        ],
    ],
    'madeInCountries' => [
        Region::VIETNAM => [
            CHINA_PRODUCTS => [
                'China',
                'Trung Quốc',
            ],
            LOCAL_PRODUCTS => [
                'Vietnam',
            ],
            CHINA_LOCAL_PRODUCTS => [
                'China',
                'Trung Quốc',
                'Vietnam',
            ],
        ],
        Region::INDONESIA => [
            CHINA_PRODUCTS => [
                'China',
            ],
            LOCAL_PRODUCTS => [
                'Indonesia',
            ],
            CHINA_LOCAL_PRODUCTS => [
                'China',
                'Indonesia',
            ],
        ],
        Region::ROMANIA => [
            CHINA_PRODUCTS => [
                'China',
            ],
            LOCAL_PRODUCTS => [
                'Romania',
            ],
            CHINA_LOCAL_PRODUCTS => [
                'China',
                'Romania',
            ],
        ],
        Region::COLOMBIA => [
            CHINA_PRODUCTS => [
                'China',
            ],
            LOCAL_PRODUCTS => [
                'Colombia',
            ],
            CHINA_LOCAL_PRODUCTS => [
                'China',
                'Colombia',
            ],
        ],
        Region::GLOBAL => [
            CHINA_PRODUCTS => [
                'China',
            ],
            LOCAL_PRODUCTS => [
                'US',
            ],
            CHINA_LOCAL_PRODUCTS => [
                'China',
                'US',
            ],
        ],
    ],
    'registrationFieldsVariants' => [
        EMAIL_FIELD,
        EMAIL_PHONE_NAME_FIELD,
    ],
    'dbVariants' => [
        NATIVE_DB,
        NATIVE_DB_BACKUP,
    ],
    'validatorRegex' => [
        'phone' => [
            'vi' => [
                'placeholder' => '012 3456789',
                'pattern' => '\\d{6,10}',
                'flags' => '',
            ],
            'in' => [
                'placeholder' => '123 4567890',
                'pattern' => '\\d{6,10}',
                'flags' => '',
            ],
            'en' => [
                'placeholder' => '',
                'pattern' => '\\d{6,10}',
                'flags' => '',
            ],
        ],
        'email' => [
            'default' => [
                'placeholder' => 'example@gmail.com',
                'pattern' => '^([a-z0-9_\\.\\-])+(\\+[a-z0-9_\\.\\-]+)?@[a-z0-9\\-]+\.([a-z]{2,4}\\.)?[a-z]{2,4}$',
                'flags' => 'i',
            ],
        ],
    ],
    'layoutsEmails' => [
        'registrationAutoPassword' => 'auth@mail.mommy.com',
        'recoveryPassword' => 'auth@mail.mommy.com',
        'welcome2' => 'auth@mail.mommy.com',
        'welcomeOfferWithBonuses' => 'auth@mail.mommy.com',
        'welcomeOfferWithPromocode2' => 'auth@mail.mommy.com',
        'supplierRegistration' => 'auth@mail.mommy.com',

        'confirmed' => 'order@mail.mommy.com',
        'create' => 'order@mail.mommy.com',
        'mailed' => 'order@mail.mommy.com',
        'prepay' => 'order@mail.mommy.com',
        'automaticConfirmed' => 'order@mail.mommy.com',

        'tryConsole2' => 'weekly@mail.mommy.com',
        'tryPromocodeTenPercentConsole2' => 'weekly@mail.mommy.com',

        'answer' => 'reply@mail.mommy.com',

        'supplierDeliveredInfoConsole' => 'warehouse@mail.mommy.com',
        'warehouseStatementReturn' => 'warehouse@mail.mommy.com',

        'inviteByEmail' => 'partner@mail.mommy.com',
        'newPartner' => 'partner@mail.mommy.com',

        'weWorry' => 'letters@mail.mommy.com',
        'default' => 'letters@mail.mommy.com',
    ],
    'facebookLinks' => [
        'Global' => '',
        'Indonesia' => 'https://www.facebook.com/mommy.com.Indonesia',
//        'Vietnam' => 'https://www.facebook.com/mommy.viet/',
        'Vietnam' => '',
        'Romania' => '',
        'Colombia' => '',
    ],
    'unisender' => [
        'lists' => [
            'Indonesia' => [
                'unfinished_registration_filling_email' => 13090369,
                'finished_registration' => 13090377,
                'lost_cart' => 13090381,
                'view_section' => 13090393,
            ],
            'Vietnam' => [
                'unfinished_registration_filling_email' => 13019749,
                'finished_registration' => 13019765,
                'lost_cart' => 13019789,
                'view_section' => 13019805,
            ],
        ],
    ],
];
