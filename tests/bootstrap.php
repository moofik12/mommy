<?php

use Symfony\Component\Dotenv\Dotenv;

define('ROOT_PATH', dirname(__DIR__));
define('YII_ENABLE_EXCEPTION_HANDLER', false);
define('YII_ENABLE_ERROR_HANDLER', false);
define('LOGS', false);

require_once ROOT_PATH . '/vendor/autoload.php';

Yii::import('system.test.CTestCase');
Yii::import('system.test.CDbTestCase');
Yii::import('system.test.CWebTestCase');

spl_autoload_unregister(['YiiBase', 'autoload']);

(new Dotenv())->load(ROOT_PATH . '/.env');

umask(0000);
