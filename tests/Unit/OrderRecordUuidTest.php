<?php

namespace MommyCom\Test\Unit;

use MommyCom\Model\Db\OrderRecord;
use PHPUnit\Framework\TestCase;

class OrderRecordUuidTest extends TestCase
{
    public function testGenerate()
    {
        $uid = OrderRecord::generateUid(123456, 1500000000);

        $this->assertSame(20, strlen($uid));
        $this->assertSame('QOIBAAAvaF', substr($uid, 0, 10));
    }

    public function testCheckTrue()
    {
        $isUuid = OrderRecord::isUid('lIgBAMciuVp5XOnCLRy1');

        $this->assertTrue($isUuid);
    }

    public function testCheckFalse()
    {
        $isUuid = OrderRecord::isUid('100500');

        $this->assertFalse($isUuid);
    }

    public function testCheckOnWrongString()
    {
        $isUuid = OrderRecord::isUid('hngP(&BVC*bui76[p*VB');

        $this->assertFalse($isUuid);
    }

    public function testCheckOnOldType()
    {
        $isUuid = OrderRecord::isUid('550010-1514541922');

        $this->assertTrue($isUuid);
    }

    public function testCheckOnOldInvalid()
    {
        $isUuid = OrderRecord::isUid('550010-1514541abc');

        $this->assertFalse($isUuid);
    }
}
