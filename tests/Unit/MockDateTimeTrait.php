<?php

namespace MommyCom\Test\Unit;

use MommyCom\Service\CurrentTime;

trait MockDateTimeTrait
{
    /**
     * @param string $time Y-m-d H:i:s
     *
     * @throws \ReflectionException
     */
    protected function mockDateTime(string $time): void
    {
        $time = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $time);

        $class = new \ReflectionClass(CurrentTime::class);
        $property = $class->getProperty('dateTime');
        $property->setAccessible(true);
        $property->setValue($time);
    }
}
