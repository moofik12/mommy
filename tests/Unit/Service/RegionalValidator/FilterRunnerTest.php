<?php

namespace MommyCom\Test\Unit\Service\RegionalValidator;

use MommyCom\Service\RegionalValidator\Filter\FilterInterface;
use MommyCom\Service\RegionalValidator\FilterRunner;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FilterRunnerTest extends TestCase
{
    public function testValidate()
    {
        $param = '~qt e-st';

        /** @var FilterInterface|MockObject $filter */
        $filter = $this
            ->getMockBuilder(FilterInterface::class)
            ->getMockForAbstractClass();

        $filter
            ->expects($this->once())
            ->method('filter')
            ->with($param)
            ->willReturn('test');

        $runner = new FilterRunner([$filter]);
        $result = $runner->sanitize($param);

        $this->assertEquals('test', $result);
    }
}
