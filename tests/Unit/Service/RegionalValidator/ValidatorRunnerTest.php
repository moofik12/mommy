<?php

namespace MommyCom\Test\Unit\Service\RegionalValidator;

use MommyCom\Service\RegionalValidator\ValidatorRunner;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Validation;

class ValidatorRunnerTest extends TestCase
{
    public function providerValidate()
    {
        yield 'No violation' => ['test', 0];
        yield 'Too long' => ['too long', 1];
        yield 'Wrong first char' => ['wrong', 1];
        yield 'Too long with wrong first char' => ['It is too long', 1];
    }

    /**
     * @dataProvider providerValidate
     *
     * @param string $param
     * @param int $violationsCount
     */
    public function testValidate(string $param, int $violationsCount)
    {
        $constraintsValidator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();

        $constraints = [
            new Length(['min' => 3, 'max' => 5]),
            new Regex('/^t/'),
        ];
        $validator = new ValidatorRunner($constraints, $constraintsValidator);
        $violationList = $validator->validate($param);

        $this->assertCount($violationsCount, $violationList);
    }
}
