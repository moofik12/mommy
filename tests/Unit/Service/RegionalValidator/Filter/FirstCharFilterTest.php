<?php

namespace MommyCom\Test\Unit\Service\RegionalValidator\Filter;

use MommyCom\Service\RegionalValidator\Filter\StartWithFilter;
use PHPUnit\Framework\TestCase;

class StartWithFilterTest extends TestCase
{
    /**
     * @var StartWithFilter $filter
     */
    private $filter;

    public function setUp(): void
    {
        $this->filter = new StartWithFilter(['a', 'b', 'c']);

        parent::setUp();
    }

    /**
     * @dataProvider dirtyStringProvider
     *
     * @param string $param
     */
    public function testFilterDirtyString(string $param)
    {
        $result = $this->filter->filter($param);
        $this->assertEquals($result, 'test');
    }

    /**
     * @dataProvider cleanStringProvider
     *
     * @param string $param
     */
    public function testFilterCleanString(string $param)
    {
        $result = $this->filter->filter($param);
        $this->assertEquals($result, 'test');
    }

    public function dirtyStringProvider()
    {
        $strings = [
            'atest' => 'test',
            'btest' => 'test',
            'ctest' => 'test',
        ];

        foreach ($strings as $from => $to) {
            yield $from => [$from, $to];
        }
    }

    public function cleanStringProvider()
    {
        yield ['test'];
    }
}
