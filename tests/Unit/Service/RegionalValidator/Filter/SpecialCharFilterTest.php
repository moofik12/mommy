<?php

namespace MommyCom\Test\Unit\Service\RegionalValidator\Filter;

use MommyCom\Service\RegionalValidator\Filter\SpecialCharFilter;
use PHPUnit\Framework\TestCase;

class SpecialCharFilterTest extends TestCase
{
    /**
     * @dataProvider firstSpecialCharStringProvider
     *
     * @param string $param
     * @param string $expected
     */
    public function testModeRemoveFirstCharacterFilter(string $param, string $expected)
    {
        $filter = new SpecialCharFilter(SpecialCharFilter::REMOVE_FIRST_CHARACTER);

        $actual = $filter->filter($param);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider randomSpecialCharStringProvider
     *
     * @param string $param
     * @param string $expected
     */
    public function testModeRemoveAllCharactersFilter(string $param, string $expected)
    {
        $filter = new SpecialCharFilter(SpecialCharFilter::REMOVE_ALL_CHARACTERS);

        $actual = $filter->filter($param);

        $this->assertEquals($expected, $actual);
    }

    public function firstSpecialCharStringProvider()
    {
        $strings = [
            'test$' => 'test$',
            '+test' => 'test',
            '?test$' => 'test$',
        ];

        foreach ($strings as $from => $to) {
            yield $from => [$from, $to];
        }
    }

    public function randomSpecialCharStringProvider()
    {
        $strings = [
            'test' => 'test',
            '+test+' => 'test',
            '?+tes!@#$%^&*()_+|\t+?' => 'test',
        ];

        foreach ($strings as $from => $to) {
            yield $from => [$from, $to];
        }
    }
}
