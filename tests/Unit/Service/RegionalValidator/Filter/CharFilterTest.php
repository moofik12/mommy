<?php

namespace MommyCom\Test\Unit\Service\RegionalValidator\Filter;

use MommyCom\Service\RegionalValidator\Filter\CharFilter;
use PHPUnit\Framework\TestCase;

class CharFilterTest extends TestCase
{
    /**
     * @var CharFilter
     */
    private $filter;

    public function setUp(): void
    {
        $this->filter = new CharFilter(['a', 'b', 'c']);

        parent::setUp();
    }

    /**
     * @dataProvider stringProvider
     *
     * @param string $param
     */
    public function testFilterValid(string $param)
    {
        $result = $this->filter->filter($param);
        $this->assertEquals($result, 'test');
    }

    public function stringProvider()
    {
        $strings = [
            'atesta' => 'test',
            'abtestab' => 'test',
            'abctest' => 'test',
        ];

        foreach ($strings as $from => $to) {
            yield $from => [$from, $to];
        }
    }
}
