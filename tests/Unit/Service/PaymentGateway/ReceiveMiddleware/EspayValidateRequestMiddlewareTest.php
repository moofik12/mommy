<?php

namespace MommyCom\Test\Unit\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Service\PaymentGateway\Model\AbstractEspayModel;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\EspayValidateRequestMiddleware;
use MommyCom\Test\Unit\ProcessMiddlewareTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class EspayValidateRequestMiddlewareTest extends TestCase
{
    use ProcessMiddlewareTrait;

    /**
     * @var EspayValidateRequestMiddleware
     */
    private $middleware;

    /**
     * @var bool
     */
    private $modelIsValid = false;

    protected function setUp()
    {
        $signatureKey = 'someStringKey';

        /** @var MockObject|AbstractEspayModel $espayModel */
        $espayModel = $this
            ->getMockBuilder(AbstractEspayModel::class)
            ->setConstructorArgs([$signatureKey])
            ->setMethods(['validate'])
            ->getMockForAbstractClass();

        $espayModel
            ->method('validate')
            ->willReturnCallback(function () {
                return $this->modelIsValid;
            });

        $this->middleware = new EspayValidateRequestMiddleware($espayModel, '1;Error message;');
    }

    public function testProcessMiddlewareValid()
    {
        $this->modelIsValid = true;

        /** @var MockObject|ServerRequestInterface $request */
        $request = $this
            ->getMockBuilder(ServerRequestInterface::class)
            ->getMockForAbstractClass();

        $request
            ->method('withAttribute')
            ->with(
                $this->identicalTo('espayModel'),
                $this->isInstanceOf(AbstractEspayModel::class)
            )
            ->willReturnSelf();

        $expectedResponse = $this
            ->getMockBuilder(ResponseInterface::class)
            ->getMockForAbstractClass();

        $handler = $this
            ->getMockBuilder(RequestHandlerInterface::class)
            ->getMockForAbstractClass();

        $handler->method('handle')
            ->with()
            ->willReturn($expectedResponse);

        $response = $this->processMiddleware($this->middleware, $request, $handler);

        $this->assertSame($expectedResponse, $response);
    }

    public function testProcessMiddlewareInvalid()
    {
        $this->modelIsValid = false;

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('1;Error message;', $response->getBody()->getContents());
    }
}
