<?php

namespace MommyCom\Test\Unit\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PayGatewayLogRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Service\PaymentGateway\Model\EspayPaymentModel;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\EspaySetPaymentMiddleware;
use MommyCom\Test\Unit\MockDateTimeTrait;
use MommyCom\Test\Unit\ProcessMiddlewareTrait;
use Money\Currency;
use PHPUnit\Framework\Constraint\ArraySubset;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\UuidFactory;

class EspaySetPaymentMiddlewareTest extends TestCase
{
    use MockDateTimeTrait;
    use ProcessMiddlewareTrait {
        processMiddleware as private baseProcessMiddleware;
    }

    /**
     * @var float
     */
    private $amount = 0.0;

    /**
     * @var string
     */
    private $currencyCode = 'USD';

    /**
     * @var OrderRecord[]
     */
    private $orderRecordsWithUid = [];

    /**
     * @var MockObject|PayGatewayLogRecord
     */
    private $payGatewayLogRecord;

    /**
     * @var MockObject|PayGatewayRecord
     */
    private $payGatewayRecord;

    /**
     * @var EspaySetPaymentMiddleware
     */
    private $middleware;

    protected function setUp(): void
    {
        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('findByUid')
            ->willReturnCallback(function () {
                return $this->orderRecordsWithUid;
            });

        /** @var MockObject|PayGatewayLogRecord $payGatewayLogRecord */
        $payGatewayLogRecord = $this
            ->getMockBuilder(PayGatewayLogRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|PayGatewayRecord $payGatewayRecord */
        $payGatewayRecord = $this
            ->getMockBuilder(PayGatewayRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UuidFactory $uuidFactory */
        $uuidFactory = $this
            ->getMockBuilder(UuidFactory::class)
            ->getMock();

        $uuidFactory
            ->method('uuid4')
            ->willReturn(
                '0404a0a3-62b6-4541-aa60-3163df291f4e',
                '5434e0cd-c7b2-4857-92b6-a5d3d2bbdb11',
                '5838ffdf-eab2-4e3c-9f60-6cb1cb8d69d9'
            );

        $this->payGatewayLogRecord = $payGatewayLogRecord;
        $this->payGatewayRecord = $payGatewayRecord;

        $this->middleware = new EspaySetPaymentMiddleware(
            new Currency('USD'),
            $orderRecord,
            $payGatewayLogRecord,
            $payGatewayRecord,
            $uuidFactory
        );
    }

    /**
     * @param MiddlewareInterface $middleware
     * @param ServerRequestInterface|null $request
     * @param RequestHandlerInterface|null $handler
     *
     * @return ResponseInterface
     */
    protected function processMiddleware(
        MiddlewareInterface $middleware,
        ServerRequestInterface $request = null,
        RequestHandlerInterface $handler = null
    ): ResponseInterface {
        if (null === $request) {
            /** @var MockObject|EspayPaymentModel $espayModel */
            $espayModel = $this
                ->getMockBuilder(EspayPaymentModel::class)
                ->setConstructorArgs(['someStringKey'])
                ->getMock();

            $espayModel->amount = $this->amount;
            $espayModel->ccy = $this->currencyCode;
            $espayModel->order_id = 'TxOrderID';

            /** @var MockObject|ServerRequestInterface $request */
            $request = $this
                ->getMockBuilder(ServerRequestInterface::class)
                ->getMockForAbstractClass();

            $request
                ->method('getAttribute')
                ->with('espayModel')
                ->willReturn($espayModel);
        }

        return $this->baseProcessMiddleware($middleware, $request, $handler);
    }

    public function testProcessNoRecords()
    {
        $this->orderRecordsWithUid = [];

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('1,Invalid Order Id,,,', $response->getBody()->getContents());
    }

    public function testProcessInvalidAmount()
    {
        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('getPayPrice')
            ->willReturn(1000.5);

        $orderRecord
            ->method('getDeliveryPrice')
            ->willReturn(400.0);

        $this->orderRecordsWithUid = [$orderRecord];
        $this->amount = 1400.6;

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('2,Invalid amount,,,', $response->getBody()->getContents());
    }

    public function testProcessInvalidCurrency()
    {
        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('getPayPrice')
            ->willReturn(1000.5);

        $orderRecord
            ->method('getDeliveryPrice')
            ->willReturn(400.0);

        $this->orderRecordsWithUid = [$orderRecord];
        $this->amount = 1400.5;
        $this->currencyCode = 'RUR';

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('3,Invalid currency,,,', $response->getBody()->getContents());
    }

    public function testProcessOneRecord()
    {
        $expectedCode = 200;
        $expectedBody = '0,Success,2966712229,TxOrderID,2018-03-27 17:00:00';

        $this->mockDateTime('2018-03-27 17:00:00');

        $this
            ->payGatewayLogRecord
            ->expects($this->once())
            ->method('setAttributes')
            ->with(new ArraySubset(['amount' => 1400.5], true));
        $this
            ->payGatewayLogRecord
            ->expects($this->once())
            ->method('validate')
            ->willReturn(true);
        $this
            ->payGatewayLogRecord
            ->expects($this->once())
            ->method('save')
            ->willReturn(true);
        $this
            ->payGatewayRecord
            ->expects($this->once())
            ->method('setAttributes')
            ->with(new ArraySubset(['amount' => 1400.5], true));
        $this
            ->payGatewayRecord
            ->expects($this->once())
            ->method('validate')
            ->willReturn(true);
        $this
            ->payGatewayRecord
            ->expects($this->once())
            ->method('save')
            ->willReturn(true);

        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('getPayPrice')
            ->willReturn(1000.5);

        $orderRecord
            ->method('getDeliveryPrice')
            ->willReturn(400.0);

        $this->orderRecordsWithUid = [$orderRecord];
        $this->amount = 1400.5;

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame($expectedCode, $response->getStatusCode());
        $this->assertSame($expectedBody, $response->getBody()->getContents());
    }

    public function testProcessOneRecordWithFailedSaving()
    {
        $expectedCode = 200;
        $expectedBody = '4,Unknown error,,,';

        $this
            ->payGatewayLogRecord
            ->expects($this->once())
            ->method('validate')
            ->willReturn(true);
        $this
            ->payGatewayLogRecord
            ->expects($this->once())
            ->method('save')
            ->willReturn(true);
        $this
            ->payGatewayRecord
            ->expects($this->once())
            ->method('validate')
            ->willReturn(true);
        $this
            ->payGatewayRecord
            ->expects($this->once())
            ->method('save')
            ->willReturn(false);

        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('getPayPrice')
            ->willReturn(1000.5);

        $orderRecord
            ->method('getDeliveryPrice')
            ->willReturn(400.0);

        $this->orderRecordsWithUid = [$orderRecord];
        $this->amount = 1400.5;

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame($expectedCode, $response->getStatusCode());
        $this->assertSame($expectedBody, $response->getBody()->getContents());
    }
}
