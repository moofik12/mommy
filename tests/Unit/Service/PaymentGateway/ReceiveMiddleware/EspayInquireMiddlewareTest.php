<?php

namespace MommyCom\Test\Unit\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\PaymentGateway\Model\EspayInquireModel;
use MommyCom\Service\PaymentGateway\ReceiveMiddleware\EspayInquireMiddleware;
use MommyCom\Test\Unit\MockDateTimeTrait;
use MommyCom\Test\Unit\ProcessMiddlewareTrait;
use Money\Currency;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class EspayInquireMiddlewareTest extends TestCase
{
    use MockDateTimeTrait;
    use ProcessMiddlewareTrait {
        processMiddleware as private baseProcessMiddleware;
    }

    /**
     * @var EspayInquireMiddleware
     */
    private $middleware;

    /**
     * @var OrderRecord[]
     */
    private $orderRecordsWithUid = [];

    protected function setUp()
    {
        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('findByUid')
            ->willReturnCallback(function () {
                return $this->orderRecordsWithUid;
            });

        $this->middleware = new EspayInquireMiddleware(new Currency('USD'), $orderRecord);
    }

    /**
     * @param MiddlewareInterface $middleware
     * @param ServerRequestInterface|null $request
     * @param RequestHandlerInterface|null $handler
     *
     * @return ResponseInterface
     */
    protected function processMiddleware(
        MiddlewareInterface $middleware,
        ServerRequestInterface $request = null,
        RequestHandlerInterface $handler = null
    ): ResponseInterface {
        if (null === $request) {
            /** @var MockObject|EspayInquireModel $espayModel */
            $espayModel = $this
                ->getMockBuilder(EspayInquireModel::class)
                ->setConstructorArgs(['someStringKey'])
                ->getMock();

            $espayModel->order_id = 'TxOrderID';

            /** @var MockObject|ServerRequestInterface $request */
            $request = $this
                ->getMockBuilder(ServerRequestInterface::class)
                ->getMockForAbstractClass();

            $request
                ->method('getAttribute')
                ->with('espayModel')
                ->willReturn($espayModel);
        }

        return $this->baseProcessMiddleware($middleware, $request, $handler);
    }

    public function testProcessNoRecords()
    {
        $this->orderRecordsWithUid = [];

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('1;Invalid Order Id;;;;;', $response->getBody()->getContents());
    }

    public function testProcessRecord()
    {
        $expectedCode = 200;
        $expectedBody = '0;Success;TxOrderID;1400.50;USD;Pay for 1 order(s);27/03/2018 17:00:00';

        $this->mockDateTime('2018-03-27 17:00:00');

        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('getPayPrice')
            ->willReturn(1000.5);

        $orderRecord
            ->method('getDeliveryPrice')
            ->willReturn(400.0);

        $this->orderRecordsWithUid = [$orderRecord];

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame($expectedCode, $response->getStatusCode());
        $this->assertSame($expectedBody, $response->getBody()->getContents());
    }

    public function testProcessTwoRecords()
    {
        $expectedCode = 200;
        $expectedBody = '0;Success;TxOrderID;2800.66;USD;Pay for 2 order(s);27/03/2018 08:00:00';

        $this->mockDateTime('2018-03-27 08:00:00');

        /** @var MockObject|OrderRecord $orderRecord */
        $orderRecord = $this
            ->getMockBuilder(OrderRecord::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRecord
            ->method('getPayPrice')
            ->willReturn(1000.33);

        $orderRecord
            ->method('getDeliveryPrice')
            ->willReturn(400.0);

        $this->orderRecordsWithUid = [$orderRecord, $orderRecord];

        $response = $this->processMiddleware($this->middleware);

        $this->assertSame($expectedCode, $response->getStatusCode());
        $this->assertSame($expectedBody, $response->getBody()->getContents());
    }
}
