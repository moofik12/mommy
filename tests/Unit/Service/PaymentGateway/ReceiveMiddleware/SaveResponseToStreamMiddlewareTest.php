<?php

namespace MommyCom\Test\Unit\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Service\PaymentGateway\ReceiveMiddleware\SaveResponseToStreamMiddleware;
use MommyCom\Service\StreamSerializer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SaveResponseToStreamMiddlewareTest extends TestCase
{
    public function testProcessAndHandleNext()
    {
        $request = $this->makeRequest();
        $response = $this->makeResponse();
        $handler = $this->makeHandler();
        $streamSerializer = $this->makeStreamSerializer();

        $handler
            ->expects($this->once())
            ->method('handle')
            ->with($request)
            ->willReturn($response);

        $middleware = new SaveResponseToStreamMiddleware($streamSerializer);

        // test mock
        $middleware->process($request, $handler);
    }

    public function testProcessAndReturnResponse()
    {
        $request = $this->makeRequest();
        $response = $this->makeResponse();
        $handler = $this->makeHandler();

        $handler
            ->method('handle')
            ->willReturn($response);

        $middleware = new SaveResponseToStreamMiddleware($this->makeStreamSerializer());

        $result = $middleware->process($request, $handler);

        $this->assertSame($response, $result);
    }

    public function testProcessAndSave()
    {
        $request = $this->makeRequest();
        $response = $this->makeResponse();
        $handler = $this->makeHandler();
        $streamSerializer = $this->makeStreamSerializer();

        $handler
            ->method('handle')
            ->willReturn($response);

        $streamSerializer
            ->expects($this->once())
            ->method('append')
            ->with($response);

        $middleware = new SaveResponseToStreamMiddleware($streamSerializer);

        // test mock
        $middleware->process($request, $handler);
    }

    public function testProcessAndSaveTwice()
    {
        $request = $this->makeRequest();
        $response = $this->makeResponse();
        $handler = $this->makeHandler();
        $streamSerializer = $this->makeStreamSerializer();

        $handler
            ->method('handle')
            ->willReturn($response);

        $streamSerializer
            ->expects($this->exactly(2))
            ->method('append')
            ->with($response);

        $middleware = new SaveResponseToStreamMiddleware($streamSerializer);

        // test mock
        $middleware->process($request, $handler);
        $middleware->process($request, $handler);
    }

    /**
     * @return MockObject|ServerRequestInterface
     */
    private function makeRequest(): ServerRequestInterface
    {
        return $this
            ->getMockBuilder(ServerRequestInterface::class)
            ->getMockForAbstractClass();
    }

    /**
     * @return MockObject|ResponseInterface
     */
    private function makeResponse(): ResponseInterface
    {
        return $this
            ->getMockBuilder(ResponseInterface::class)
            ->getMockForAbstractClass();
    }

    /**
     * @return MockObject|RequestHandlerInterface
     */
    private function makeHandler(): RequestHandlerInterface
    {
        return $this
            ->getMockBuilder(RequestHandlerInterface::class)
            ->getMockForAbstractClass();
    }

    /**
     * @return MockObject|StreamSerializer
     */
    private function makeStreamSerializer(): StreamSerializer
    {
        return $this
            ->getMockBuilder(StreamSerializer::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
