<?php

namespace MommyCom\Test\Unit\Service\PaymentGateway\ReceiveMiddleware;

use MommyCom\Service\PaymentGateway\ReceiveMiddleware\MiddlewarePipe;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;

class MiddlewarePipeTest extends TestCase
{
    public function testHandle()
    {
        $order = [];

        /** @var MockObject|ServerRequestInterface $request */
        $request = $this
            ->getMockBuilder(ServerRequestInterface::class)
            ->getMockForAbstractClass();

        $pipe = (new MiddlewarePipe())
            ->pipe($this->makeMiddleware($request, 0, $order))
            ->pipe($this->makeMiddleware($request, 1, $order));

        // test mock
        $pipe->handle($request);
        $pipe->handle($request);

        $this->assertSame([0, 1], $order);
    }

    private function makeMiddleware(ServerRequestInterface $request, int $callId, array &$order): MiddlewareInterface
    {
        /** @var MockObject|ResponseInterface $response */
        $response = $this
            ->getMockBuilder(ResponseInterface::class)
            ->getMockForAbstractClass();

        /** @var MockObject|MiddlewareInterface $middleware */
        $middleware = $this
            ->getMockBuilder(MiddlewareInterface::class)
            ->getMockForAbstractClass();

        $middleware
            ->expects($this->once())
            ->method('process')
            ->with($this->identicalTo($request), $this->isInstanceOf(MiddlewarePipe::class))
            ->willReturnCallback(function () use ($response, $callId, &$order) {
                $order[] = $callId;

                return $response;
            });

        return $middleware;
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Empty middleware pipe
     */
    public function testHandleOnEmptyPipe()
    {
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this
            ->getMockBuilder(ServerRequestInterface::class)
            ->getMockForAbstractClass();

        $pipe = new MiddlewarePipe();

        // throws exception
        $pipe->handle($request);
    }
}
