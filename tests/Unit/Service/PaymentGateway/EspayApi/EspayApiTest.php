<?php

namespace MommyCom\Test\Unit\Service\PaymentGateway\EspayApi;

use GuzzleHttp\ClientInterface;
use MommyCom\Service\PaymentGateway\EspayApi\EspayApi;
use MommyCom\Service\PaymentGateway\EspayApi\Provider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\SimpleCache\CacheInterface;

class EspayApiTest extends TestCase
{
    /**
     * @var MockObject|ClientInterface
     */
    private $client;

    /**
     * @var MockObject|CacheInterface
     */
    private $cache;

    /**
     * @var EspayApi
     */
    private $api;

    protected function setUp(): void
    {
        /** @var MockObject|ClientInterface $client */
        $client = $this
            ->getMockBuilder(ClientInterface::class)
            ->getMockForAbstractClass();

        /** @var MockObject|CacheInterface $cache */
        $cache = $this
            ->getMockBuilder(CacheInterface::class)
            ->getMockForAbstractClass();

        $this->client = $client;
        $this->cache = $cache;
        $this->api = new EspayApi($cache, $client, '//example.com/', 'someApiKey');
    }

    public function testGetProviders(): void
    {
        $responseJson = <<<'JSON'
{
    "error_code": "0000",
    "error_message": "",
    "data": [
        {
            "bankCode": "008",
            "productCode": "MANDIRIIB",
            "productName": "MANDIRI IB"
        },
        {
            "bankCode": "013",
            "productCode": "PERMATAATM",
            "productName": "PERMATA ATM"
        }
    ]
}
JSON;

        $response = $this
            ->getMockBuilder(ResponseInterface::class)
            ->getMockForAbstractClass();

        $response
            ->method('getBody')
            ->willReturn($responseJson);

        $this
            ->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'merchant/merchantinfo?key=someApiKey')
            ->willReturn($response);

        $this
            ->cache
            ->expects($this->once())
            ->method('get')
            ->willReturnCallback(function (string $key) use (&$cacheKeyGet) {
                $cacheKeyGet = $key;
                return false;
            });

        $this
            ->cache
            ->expects($this->once())
            ->method('set')
            ->willReturnCallback(function (string $key, array $data) use (&$cacheKeySet) {
                $cacheKeySet = $key;
                $this->assertCount(2, $data);
            });

        $providers = $this->api->getProviders();

        $this->assertContainsOnlyInstancesOf(Provider::class, $providers);
        $this->assertCount(2, $providers);
        $this->assertSame($cacheKeySet, $cacheKeyGet);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Invalid response
     */
    public function testGetProvidersError(): void
    {
        $responseJson = <<<'JSON'
{
    "error_code": "1234",
    "error_message": "Some error",
    "data": []
}
JSON;

        $response = $this
            ->getMockBuilder(ResponseInterface::class)
            ->getMockForAbstractClass();

        $response
            ->method('getBody')
            ->willReturn($responseJson);

        $this
            ->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'merchant/merchantinfo?key=someApiKey')
            ->willReturn($response);

        $this
            ->cache
            ->expects($this->once())
            ->method('get')
            ->willReturn(false);
        $this
            ->cache
            ->expects($this->never())
            ->method('set');

        $this->api->getProviders();
    }

    public function testGetProvidersFromCache()
    {
        $expected = [
            [
                "bankCode" => "008",
                "productCode" => "MANDIRIIB",
                "productName" => "MANDIRI IB",
            ],
            [
                "bankCode" => "013",
                "productCode" => "PERMATAATM",
                "productName" => "PERMATA ATM",
            ],
        ];

        $this
            ->cache
            ->expects($this->once())
            ->method('get')
            ->willReturn($expected);

        $this
            ->client
            ->expects($this->never())
            ->method('request');

        $providers = $this->api->getProviders();

        $this->assertContainsOnlyInstancesOf(Provider::class, $providers);
        $this->assertCount(2, $providers);
    }

    public function testGetProvidersTwice()
    {
        $expected = [
            [
                "bankCode" => "008",
                "productCode" => "MANDIRIIB",
                "productName" => "MANDIRI IB",
            ],
            [
                "bankCode" => "013",
                "productCode" => "PERMATAATM",
                "productName" => "PERMATA ATM",
            ],
        ];

        $this
            ->cache
            ->expects($this->once())
            ->method('get')
            ->willReturn($expected);

        $this
            ->client
            ->expects($this->never())
            ->method('request');

        $providersFirst = $this->api->getProviders();
        $providersSecond = $this->api->getProviders();

        $this->assertContainsOnlyInstancesOf(Provider::class, $providersFirst);
        $this->assertCount(2, $providersFirst);
        $this->assertSame($providersSecond, $providersFirst);
    }

    public function testGetJsUrl()
    {
        $jsUrl = $this->api->getJsUrl();

        $this->assertSame('//example.com/public/signature/js', $jsUrl);
    }

    public function testGetImageUrl()
    {
        $imageUrl = $this->api->getImageUrl('someName');

        $this->assertSame('//example.com/images/products/someName.png', $imageUrl);
    }
}
