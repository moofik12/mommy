<?php

namespace MommyCom\Test\Unit\Service;

use MommyCom\Service\Deprecated\Currency2Words;
use PHPUnit\Framework\TestCase;

class Currency2WordsTest extends TestCase
{
    public function providerConvert()
    {
        return [
            ['uk_UA', 'девʼятсот вісімдесят сім тисяч шістсот пʼятдесят чотири'],
            ['en_US', 'nine hundred eighty-seven thousand six hundred fifty-four'],
            ['id_ID', 'sembilan ratus delapan puluh tujuh ribu enam ratus lima puluh empat'],
            ['vi_VN', 'chín trăm tám mươi bảy nghìn sáu trăm năm mươi bốn'],
        ];
    }

    /**
     * @dataProvider providerConvert
     *
     * @param $locale
     * @param $expected
     */
    public function testConvert($locale, $expected)
    {
        $currency2Words = Currency2Words::instance($locale);

        $this->assertSame($expected, $currency2Words->convert(987654));
    }
}
