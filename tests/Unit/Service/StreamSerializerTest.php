<?php

namespace MommyCom\Test\Unit\Service;

use MommyCom\Service\StreamSerializer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\StreamInterface;

class StreamSerializerTest extends TestCase
{
    /**
     * @var StreamSerializer
     */
    private $streamSerializer;

    /**
     * @var StreamInterface|MockObject
     */
    private $stream;

    protected function setUp(): void
    {
        /** @var StreamInterface|MockObject $stream */
        $stream = $this
            ->getMockBuilder(StreamInterface::class)
            ->getMockForAbstractClass();

        $this->streamSerializer = new StreamSerializer($stream);
        $this->stream = $stream;
    }

    public function testAppend()
    {
        $object = new \stdClass();
        $object->some = 'data';
        $serialized = 'O:8:"stdClass":1:{s:4:"some";s:4:"data";}';
        $prefix = pack('V', 41);
        $length = 45;

        $this
            ->stream
            ->expects($this->once())
            ->method('write')
            ->with($prefix . $serialized)
            ->willReturn($length);

        $wrote = $this->streamSerializer->append($object);

        $this->assertSame($length, $wrote);
    }

    public function testReadNext()
    {
        $object = new \stdClass();
        $object->some = 'data';
        $serialized = 'O:8:"stdClass":1:{s:4:"some";s:4:"data";}';
        $prefix = pack('V', 41);

        $this
            ->stream
            ->expects($this->exactly(5))
            ->method('read')
            ->withConsecutive([4], [41], [4], [41])
            ->willReturn($prefix, $serialized, $prefix, $serialized);

        $first = $this->streamSerializer->readNext();
        $second = $this->streamSerializer->readNext();
        $third = $this->streamSerializer->readNext();

        $this->assertEquals($object, $first);
        $this->assertEquals($object, $second);
        $this->assertEquals(null, $third);
    }

    public function testIterate()
    {
        $object = new \stdClass();
        $object->some = 'data';
        $serialized = 'O:8:"stdClass":1:{s:4:"some";s:4:"data";}';
        $prefix = pack('V', 41);

        $this
            ->stream
            ->expects($this->exactly(5))
            ->method('read')
            ->withConsecutive([4], [41], [4], [41])
            ->willReturn($prefix, $serialized, $prefix, $serialized);

        $objects = iterator_to_array($this->streamSerializer);

        $this->assertEquals([$object, $object], $objects);
    }

    public function testRewind()
    {
        $this
            ->stream
            ->expects($this->once())
            ->method('rewind');

        $this->streamSerializer->rewind();
    }
}
