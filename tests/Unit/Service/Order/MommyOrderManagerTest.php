<?php

namespace MommyCom\Test\Unit\Service\Order;

use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Entity\EventProduct;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderProduct;
use MommyCom\Repository\EventProductRepository;
use MommyCom\Repository\OrderProductRepository;
use MommyCom\Repository\OrderRepository;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Money\MoneyFactory;
use MommyCom\Service\Order\MommyOrder;
use MommyCom\Service\Order\MommyOrderManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MommyOrderManagerTest extends TestCase
{
    /**
     * @var MommyOrderManager
     */
    private $mommyOrderManager;

    /**
     * @var OrderRepository|MockObject
     */
    private $orderRepository;

    protected function setUp(): void
    {
        /** @var OrderRepository|MockObject $orderRepository */
        $orderRepository = $this
            ->getMockBuilder(OrderRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityManager = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->getMockForAbstractClass();

        $entityManager
            ->method('getRepository')
            ->willReturnMap([
                [Order::class, $orderRepository],
                [OrderProduct::class, $this->createMock(OrderProductRepository::class)],
                [EventProduct::class, $this->createMock(EventProductRepository::class)],
            ]);

        /** @noinspection PhpParamsInspection */
        $this->mommyOrderManager = new MommyOrderManager(
            $entityManager,
            $this->createMock(MoneyFactory::class),
            $this->createMock(AvailableDeliveries::class)
        );

        $this->orderRepository = $orderRepository;
    }

    public function testFindOne()
    {
        $orderId = 31137;

        $order = $this
            ->getMockBuilder(Order::class)
            ->getMock();

        $order
            ->method('getId')
            ->willReturn($orderId);

        $this
            ->orderRepository
            ->expects($this->once())
            ->method('find')
            ->with($orderId)
            ->willReturn($order);

        $mommyOrder = $this->mommyOrderManager->find($orderId);

        $this->assertInstanceOf(MommyOrder::class, $mommyOrder);
        $this->assertSame($orderId, $mommyOrder->getId());
    }

    public function testFindTwice()
    {
        $orderId = 31137;

        $order = $this
            ->getMockBuilder(Order::class)
            ->getMock();

        $order
            ->method('getId')
            ->willReturn($orderId);

        $this
            ->orderRepository
            ->expects($this->once())
            ->method('find')
            ->with($orderId)
            ->willReturn($order);

        $mommyOrderFirst = $this->mommyOrderManager->find($orderId);
        $mommyOrderSecond = $this->mommyOrderManager->find($orderId);

        $this->assertSame($mommyOrderFirst, $mommyOrderSecond);
    }

    public function testFindNotFound()
    {
        $orderId = 31137;

        $this
            ->orderRepository
            ->expects($this->once())
            ->method('find')
            ->with($orderId)
            ->willReturn(null);

        $mommyOrder = $this->mommyOrderManager->find($orderId);

        $this->assertInternalType('null', $mommyOrder);
    }
}
