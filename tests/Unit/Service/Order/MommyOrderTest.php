<?php

namespace MommyCom\Test\Unit\Service\Order;

use MommyCom\Entity\EventProduct;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderProduct;
use MommyCom\Repository\EventProductRepository;
use MommyCom\Repository\OrderProductRepository;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use MommyCom\Service\Delivery\FormModel\Dummy;
use MommyCom\Service\Money\MoneyFactory;
use MommyCom\Service\Order\MommyOrder;
use MommyCom\Service\Translator\TranslatorInterface;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MommyOrderTest extends TestCase
{
    private const ORDER_ID = 31137;

    /**
     * @var MommyOrder
     */
    private $mommyOrder;

    /**
     * @var Order|MockObject
     */
    private $order;

    /**
     * @var OrderProductRepository|MockObject
     */
    private $orderProductRepository;

    /**
     * @var EventProductRepository|MockObject
     */
    private $eventProductRepository;

    /**
     * @var MoneyFactory|MockObject
     */
    private $moneyFactory;

    /**
     * @var AvailableDeliveries|MockObject
     */
    private $availableDeliveries;

    protected function setUp(): void
    {
        $order = $this->makeOrder();
        $orderProductRepository = $this->makeOrderProductRepository();
        $eventProductRepository = $this->makeEventProductRepository();
        $moneyFactory = $this->makeMoneyFactory();
        $availableDeliveries = $this->makeAvailableDeliveries();

        $this->mommyOrder = new MommyOrder(
            $order,
            $orderProductRepository,
            $eventProductRepository,
            $moneyFactory,
            $availableDeliveries
        );

        $this->order = $order;
        $this->orderProductRepository = $orderProductRepository;
        $this->eventProductRepository = $eventProductRepository;
        $this->moneyFactory = $moneyFactory;
        $this->availableDeliveries = $availableDeliveries;
    }

    public function testGetId()
    {
        $id = $this->mommyOrder->getId();

        $this->assertSame(self::ORDER_ID, $id);
    }

    public function testGetTrackId()
    {
        $this
            ->order
            ->expects($this->once())
            ->method('getTrackId')
            ->willReturn('megaTrack');

        $trackId = $this->mommyOrder->getTrackId();

        $this->assertSame('megaTrack', $trackId);
    }

    public function testGetCost()
    {
        $this
            ->order
            ->expects($this->once())
            ->method('getBonuses')
            ->willReturn(0.5);

        $this
            ->order
            ->expects($this->once())
            ->method('getDiscount')
            ->willReturn(1.0);

        $this
            ->order
            ->expects($this->never())
            ->method('getCardPayed');

        $this
            ->orderProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['orderId' => self::ORDER_ID])
            ->willReturn([
                $this->makeOrderProduct(1, 1.0, 0),
                $this->makeOrderProduct(2, 2.0, 1), // отмененная позиция
                $this->makeOrderProduct(3, 3.0, 2),
            ]);

        $expected = new Money(2500, new Currency('USD'));
        $this
            ->moneyFactory
            ->method('makeMoney')
            ->with(2.5)
            ->willReturn($expected);

        // test mock
        $cost = $this->mommyOrder->getCost();

        $this->assertEquals($expected, $cost);
    }

    public function testGetCostWithBigDiscount()
    {
        $this
            ->order
            ->expects($this->once())
            ->method('getBonuses')
            ->willReturn(0.5);

        $this
            ->order
            ->expects($this->once())
            ->method('getDiscount')
            ->willReturn(10.0); // скидка больше стоимости заказа

        $this
            ->orderProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['orderId' => self::ORDER_ID])
            ->willReturn([
                $this->makeOrderProduct(1, 1.0, 0),
            ]);

        $expected = new Money(0, new Currency('USD'));
        $this
            ->moneyFactory
            ->method('makeMoney')
            ->with(0.0)
            ->willReturn($expected);

        // test mock
        $cost = $this->mommyOrder->getCost();

        $this->assertEquals($expected, $cost);
    }

    public function testGetInvoice()
    {
        $this
            ->orderProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['orderId' => self::ORDER_ID])
            ->willReturn([
                $this->makeOrderProduct(1, 1.0, 0),
            ]);

        $expected = new Money(100, new Currency('USD'));
        $this
            ->moneyFactory
            ->method('makeMoney')
            ->with(1)
            ->willReturn($expected);

        // test mock
        $cost = $this->mommyOrder->getInvoice();

        $this->assertEquals($expected, $cost);
    }

    public function testGetInvoiceWhenPaid()
    {
        $this
            ->order
            ->expects($this->once())
            ->method('getCardPayed')
            ->willReturn(1.0); // заказ оплачен

        $this
            ->orderProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['orderId' => self::ORDER_ID])
            ->willReturn([
                $this->makeOrderProduct(1, 1.0, 0),
            ]);

        $expected = new Money(0, new Currency('USD'));
        $this
            ->moneyFactory
            ->method('makeMoney')
            ->with(0.0)
            ->willReturn($expected);

        // test mock
        $cost = $this->mommyOrder->getInvoice();

        $this->assertEquals($expected, $cost);
    }

    public function testGetWeightWithCustom()
    {
        $this
            ->order
            ->method('getWeightCustom')
            ->willReturn(800);

        $weight = $this->mommyOrder->getWeight();

        $this->assertSame(800, $weight);
    }

    public function testGetWeightCalculate()
    {
        $this
            ->orderProductRepository
            ->method('findBy')
            ->with(['orderId' => self::ORDER_ID])
            ->willReturn([
                $this->makeOrderProduct(1, 1.0, 0, 1),
                $this->makeOrderProduct(2, 2.0, 0, 2),
                $this->makeOrderProduct(3, 3.0, 0, 1),
            ]);

        $this
            ->eventProductRepository
            ->method('find')
            ->willReturnCallback(function ($id) {
                return $this->makeEventProduct($id, $id * 100);
            });

        $weight = $this->mommyOrder->getWeight();

        $this->assertSame(800, $weight);
    }

    public function testGetDeliveryModel()
    {
        /** @var TranslatorInterface|MockObject $translator */
        $translator = $this
            ->getMockBuilder(TranslatorInterface::class)
            ->getMockForAbstractClass();

        $delivery = $this
            ->getMockBuilder(DeliveryInterface::class)
            ->getMockForAbstractClass();

        $delivery
            ->expects($this->once())
            ->method('createFormModel')
            ->willReturn(new Dummy($translator));

        $this
            ->order
            ->method('getDeliveryType')
            ->willReturn(7);

        $this
            ->order
            ->method('getDeliveryAttributesJson')
            ->willReturn(json_encode(['address' => 'Some test address']));

        $this
            ->availableDeliveries
            ->method('getDelivery')
            ->with(7)
            ->willReturn($delivery);

        $deliveryModel = $this->mommyOrder->getDeliveryModel();

        $this->assertInstanceOf(DeliveryModel::class, $deliveryModel);
        $this->assertAttributeSame('Some test address', 'address', $deliveryModel);
    }

    public function testGetCustomerName()
    {
        $this
            ->order
            ->method('getClientName')
            ->willReturn('Name');

        $this
            ->order
            ->method('getClientSurname')
            ->willReturn('Surname');

        $customerName = $this->mommyOrder->getCustomerName();

        $this->assertSame('Name Surname', $customerName);
    }

    public function testGetPhoneNumber()
    {
        $this
            ->order
            ->method('getTelephone')
            ->willReturn('+79112223344');

        $phoneNumber = $this->mommyOrder->getPhoneNumber();

        $this->assertSame('+79112223344', $phoneNumber);
    }

    private function makeOrder(): Order
    {
        /** @var Order|MockObject $order */
        $order = $this
            ->getMockBuilder(Order::class)
            ->getMock();

        $order
            ->method('getId')
            ->willReturn(self::ORDER_ID);

        return $order;
    }

    private function makeOrderProductRepository(): OrderProductRepository
    {
        /** @var OrderProductRepository|MockObject $orderProductRepository */
        $orderProductRepository = $this
            ->getMockBuilder(OrderProductRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $orderProductRepository;
    }

    private function makeEventProductRepository(): EventProductRepository
    {
        /** @var EventProductRepository|MockObject $eventProductRepository */
        $eventProductRepository = $this
            ->getMockBuilder(EventProductRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $eventProductRepository;
    }

    private function makeMoneyFactory(): MoneyFactory
    {
        /** @var MoneyFactory|MockObject $moneyFactory */
        $moneyFactory = $this
            ->getMockBuilder(MoneyFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $moneyFactory;
    }

    private function makeOrderProduct(int $productId, float $price, int $status, int $quantity = 1): OrderProduct
    {
        /** @var OrderProduct|MockObject $orderProduct */
        $orderProduct = $this
            ->getMockBuilder(OrderProduct::class)
            ->getMock();

        $orderProduct
            ->method('getProductId')
            ->willReturn($productId);

        $orderProduct
            ->method('getPriceTotal')
            ->willReturn($price);

        $orderProduct
            ->method('getCallcenterStatus')
            ->willReturn($status);

        $orderProduct
            ->method('getNumber')
            ->willReturn($quantity);

        return $orderProduct;
    }

    private function makeEventProduct(int $id, int $weight)
    {
        /** @var EventProduct|MockObject $eventProduct */
        $eventProduct = $this
            ->getMockBuilder(EventProduct::class)
            ->getMock();

        $eventProduct
            ->method('getId')
            ->willReturn($id);

        $eventProduct
            ->method('getWeight')
            ->willReturn($weight);

        return $eventProduct;
    }

    private function makeAvailableDeliveries(): AvailableDeliveries
    {
        /** @var AvailableDeliveries|MockObject $availableDeliveries */
        $availableDeliveries = $this
            ->getMockBuilder(AvailableDeliveries::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $availableDeliveries;
    }
}
