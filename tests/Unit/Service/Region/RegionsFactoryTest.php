<?php

namespace MommyCom\Test\Unit\Service\Region;

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionsFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RegionsFactoryTest extends TestCase
{
    /**
     * @param string $host
     *
     * @return RequestStack
     */
    private function makeRequestStack(string $host): RequestStack
    {
        $request = new Request();
        $request->headers->set('Host', $host);

        /** @var RequestStack|MockObject $requestStack */
        $requestStack = $this
            ->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack
            ->method('getCurrentRequest')
            ->willReturn($request);

        return $requestStack;
    }

    /**
     * @return RequestStack
     */
    private function makeEmptyRequestStack(): RequestStack
    {
        /** @var RequestStack|MockObject $requestStack */
        $requestStack = $this
            ->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $requestStack;
    }

    public function testCreateRegionsDesktop()
    {
        $regionsSettings = ['desktop' => ['Indonesia' => 'id.mommy.com']];
        $requestStack = $this->makeRequestStack('id.mommy.com');

        $regions = RegionsFactory::createRegions($requestStack, $regionsSettings, 'Indonesia');

        $allowedRegions = $regions->getRegions();
        $hostname = $regions->getHostname('Indonesia');
        $userRegion = $regions->getUserRegion();
        $serverRegion = $regions->getServerRegion();

        $this->assertSame($regionsSettings['desktop'], $allowedRegions);
        $this->assertSame('id.mommy.com', $hostname);
        $this->assertInstanceOf(Region::class, $userRegion);
        $this->assertInstanceOf(Region::class, $serverRegion);
        $this->assertSame('Global', $userRegion->getRegionName());
        $this->assertSame('Indonesia', $serverRegion->getRegionName());
        $this->assertSame(0, $userRegion->getDetectLevel());
        $this->assertSame(3, $serverRegion->getDetectLevel());
    }

    public function testCreateRegionsMobile()
    {
        $regionsSettings = ['mobile' => ['Indonesia' => 'm.id.mommy.com']];
        $requestStack = $this->makeRequestStack('m.id.mommy.com');

        $regions = RegionsFactory::createRegions($requestStack, $regionsSettings, 'Indonesia');

        $allowedRegions = $regions->getRegions();
        $hostname = $regions->getHostname('Indonesia');
        $userRegion = $regions->getUserRegion();
        $serverRegion = $regions->getServerRegion();

        $this->assertSame($regionsSettings['mobile'], $allowedRegions);
        $this->assertSame('m.id.mommy.com', $hostname);
        $this->assertInstanceOf(Region::class, $userRegion);
        $this->assertInstanceOf(Region::class, $serverRegion);
        $this->assertSame('Global', $userRegion->getRegionName());
        $this->assertSame('Indonesia', $serverRegion->getRegionName());
        $this->assertSame(0, $userRegion->getDetectLevel());
        $this->assertSame(3, $serverRegion->getDetectLevel());
    }

    /**
     * @expectedException \LogicException
     */
    public function testCreateRegionsWithWrongRegionsConfig()
    {
        $regionsSettings = ['wrong' => ['array']];
        $requestStack = $this->makeRequestStack('id.mommy.com');

        // test exception
        RegionsFactory::createRegions($requestStack, $regionsSettings, 'Indonesia');
    }

    /**
     * @expectedException \MommyCom\Service\Region\Exception\UnknownRegionException
     */
    public function testCreateRegionsWithWrongServerRegion()
    {
        $regionsSettings = ['desktop' => ['Indonesia' => 'id.mommy.com']];
        $requestStack = $this->makeRequestStack('id.mommy.com');

        // test exception
        RegionsFactory::createRegions($requestStack, $regionsSettings, 'Wrong');
    }

    public function testCreateDevRegions()
    {
        $regionsSettings = ['desktop' => ['Indonesia' => 'id.mommy.com']];
        $requestStack = $this->makeRequestStack('id.mommy.com');

        $regions = RegionsFactory::createDevRegions($requestStack, $regionsSettings);

        $allowedRegions = $regions->getRegions();
        $hostname = $regions->getHostname('Indonesia');
        $userRegion = $regions->getUserRegion();
        $serverRegion = $regions->getServerRegion();

        $this->assertSame($regionsSettings['desktop'], $allowedRegions);
        $this->assertSame('id.mommy.com', $hostname);
        $this->assertInstanceOf(Region::class, $userRegion);
        $this->assertInstanceOf(Region::class, $serverRegion);
        $this->assertSame('Global', $userRegion->getRegionName());
        $this->assertSame('Indonesia', $serverRegion->getRegionName());
        $this->assertSame(0, $userRegion->getDetectLevel());
        $this->assertSame(3, $serverRegion->getDetectLevel());
    }

    public function testCreateRegionsWithoutRequest()
    {
        $regionsSettings = ['desktop' => ['Indonesia' => 'id.mommy.com']];
        $requestStack = $this->makeEmptyRequestStack();

        $regions = RegionsFactory::createRegions($requestStack, $regionsSettings, 'Indonesia');

        $allowedRegions = $regions->getRegions();
        $hostname = $regions->getHostname('Indonesia');
        $userRegion = $regions->getUserRegion();
        $serverRegion = $regions->getServerRegion();

        $this->assertSame($regionsSettings['desktop'], $allowedRegions);
        $this->assertSame('id.mommy.com', $hostname);
        $this->assertInstanceOf(Region::class, $userRegion);
        $this->assertInstanceOf(Region::class, $serverRegion);
        $this->assertSame('Global', $userRegion->getRegionName());
        $this->assertSame('Indonesia', $serverRegion->getRegionName());
        $this->assertSame(0, $userRegion->getDetectLevel());
        $this->assertSame(3, $serverRegion->getDetectLevel());
    }

    public function testCreateDevRegionsWithoutRequest()
    {
        $regionsSettings = ['desktop' => ['Indonesia' => 'id.mommy.com']];
        $requestStack = $this->makeEmptyRequestStack();

        $regions = RegionsFactory::createDevRegions($requestStack, $regionsSettings);

        $allowedRegions = $regions->getRegions();
        $hostname = $regions->getHostname('Indonesia');
        $userRegion = $regions->getUserRegion();
        $serverRegion = $regions->getServerRegion();

        $this->assertSame($regionsSettings['desktop'], $allowedRegions);
        $this->assertSame('id.mommy.com', $hostname);
        $this->assertInstanceOf(Region::class, $userRegion);
        $this->assertInstanceOf(Region::class, $serverRegion);
        $this->assertSame('Global', $userRegion->getRegionName());
        $this->assertSame('Global', $serverRegion->getRegionName());
        $this->assertSame(0, $userRegion->getDetectLevel());
        $this->assertSame(3, $serverRegion->getDetectLevel());
    }
}
