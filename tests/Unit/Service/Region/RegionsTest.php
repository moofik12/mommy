<?php

namespace MommyCom\Test\Unit\Service\Region;

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use PHPUnit\Framework\TestCase;

class RegionsTest extends TestCase
{
    public function testConstruct()
    {
        $regionsSettings = ['Indonesia' => 'id.mommy.com'];

        $serverRegion = new Region('Indonesia', Region::DETECT_LEVEL_SERVER, false);
        $userRegion = new Region('Global', Region::DETECT_LEVEL_UNKNOWN, false);
        $regions = new Regions($regionsSettings, $serverRegion, $userRegion);

        $allowedRegions = $regions->getRegions();
        $hostname = $regions->getHostname('Indonesia');
        $actualUserRegion = $regions->getUserRegion();
        $actualServerRegion = $regions->getServerRegion();

        $this->assertSame($regionsSettings, $allowedRegions);
        $this->assertSame('id.mommy.com', $hostname);
        $this->assertSame($userRegion, $actualUserRegion);
        $this->assertSame($serverRegion, $actualServerRegion);
    }

    /**
     * @expectedException \LogicException
     */
    public function testConstructWithWrongRegionsConfig()
    {
        $regionsSettings = ['Indonesia' => ['array']];
        $serverRegion = new Region('Indonesia', Region::DETECT_LEVEL_SERVER, false);
        $userRegion = new Region('Global', Region::DETECT_LEVEL_UNKNOWN, false);

        // test exception
        new Regions($regionsSettings, $serverRegion, $userRegion);
    }

    /**
     * @expectedException \MommyCom\Service\Region\Exception\UnknownRegionException
     */
    public function testConstructWithWrongRegions()
    {
        $regionsSettings = ['Wrong' => 'mommy.com'];
        $serverRegion = new Region('Indonesia', Region::DETECT_LEVEL_SERVER, false);
        $userRegion = new Region('Global', Region::DETECT_LEVEL_UNKNOWN, false);

        // test exception
        new Regions($regionsSettings, $serverRegion, $userRegion);
    }
}
