<?php

namespace MommyCom\Test\Unit\Service\Region;

use MommyCom\Service\Region\Region;
use PHPUnit\Framework\TestCase;

class RegionTest extends TestCase
{

    public function testGetCurrencyCode()
    {
        $region = new Region(Region::GLOBAL, Region::DETECT_LEVEL_SERVER, false);

        $currencyCode = $region->getCurrencyCode();

        $this->assertSame('USD', $currencyCode);
    }

    public function testRegionIsValid()
    {
        $this->assertTrue(Region::regionIsValid('Indonesia'));
        $this->assertFalse(Region::regionIsValid('indonesia'));
    }

    public function testFindRegion()
    {
        $this->assertSame('Indonesia', Region::findRegion('id'));
        $this->assertSame('Indonesia', Region::findRegion('ind'));
        $this->assertSame('Romania', Region::findRegion('rum'));
        $this->assertSame('', Region::findRegion('unk'));
    }

    /**
     * @expectedException \MommyCom\Service\Region\Exception\UnknownRegionException
     */
    public function testConstructWithInvalidRegion()
    {
        new Region('wrong', 0, false);
    }

    /**
     * @expectedException \MommyCom\Service\Region\Exception\UnknownDetectLevelException
     */
    public function testConstructWithInvalidLevel()
    {
        new Region('Global', 5, false);
    }
}
