<?php

namespace MommyCom\Test\Unit\Service\Region;

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionDetector;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class RegionDetectorTest extends TestCase
{
    public function providerDetectRegion()
    {
        return [
            [$this->makeRequest('', 'Mad browser', []), 'Global', 0, false],
            [$this->makeRequest('', 'Crazy bot', []), 'Global', 0, true],
            [$this->makeRequest('', '', ['id']), 'Indonesia', 1, false],
            [$this->makeRequest('Indonesia', '', []), 'Indonesia', 2, false],
        ];
    }

    /**
     * @param string $region
     * @param string $ua
     * @param array $languages
     *
     * @return Request
     */
    private function makeRequest(string $region, string $ua, array $languages): Request
    {
        /** @var Request|MockObject $request */
        $request = $this
            ->getMockBuilder(Request::class)
            ->setMethods(['getLanguages'])
            ->getMock();

        $request
            ->method('getLanguages')
            ->willReturn($languages);

        $request->headers->set('User-Agent', $ua);

        if ($region) {
            $request->cookies->set('region', $region);
        }

        return $request;
    }

    /**
     * @dataProvider providerDetectRegion
     *
     * @param Request $request
     * @param string $name
     * @param int $level
     * @param bool $bot
     */
    public function testDetectRegion(Request $request, string $name, int $level, bool $bot)
    {
        $region = RegionDetector::detectRegion($request);

        $this->assertInstanceOf(Region::class, $region);
        $this->assertSame($name, $region->getRegionName());
        $this->assertSame($level, $region->getDetectLevel());
        $this->assertSame($bot, $region->isDetectBot());
    }
}
