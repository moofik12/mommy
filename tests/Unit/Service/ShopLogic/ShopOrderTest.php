<?php

namespace MommyCom\Test\Unit\Service\ShopLogic;

use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Translator\TranslatorInterface;
use MommyCom\YiiComponent\ShopLogic\ShopOrder;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\Attribute;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ShopOrderTest extends TestCase
{
    /**
     * @var ShopOrder
     */
    private $shopOrder;

    /**
     * @var Attribute|MockObject
     */
    private $attribute;

    protected function setUp()
    {
        /** @var TranslatorInterface|MockObject $translator */
        $translator = $this
            ->getMockBuilder(TranslatorInterface::class)
            ->getMockForAbstractClass();

        $translator
            ->method('t')
            ->willReturnArgument(0);

        $translator
            ->method('__invoke')
            ->willReturnArgument(0);

        /** @var AvailableDeliveries|MockObject $availableDeliveries */
        $availableDeliveries = $this
            ->getMockBuilder(AvailableDeliveries::class)
            ->setConstructorArgs([[]])
            ->getMock();

        $availableDeliveries
            ->method('hasDelivery')
            ->willReturn(true);

        $attribute = $this
            ->getMockBuilder(Attribute::class)
            ->getMockForAbstractClass();

        $attribute
            ->method('getName')
            ->willReturn('attrId');

        $attribute
            ->method('getLabel')
            ->willReturn('Attribute label');

        $attribute
            ->method('getRules')
            ->willReturn([['attrId', 'filter', 'filter' => 'trim']]);

        $attribute
            ->method('getValue')
            ->willReturn('attr value');

        $this->shopOrder = new ShopOrder(
            $translator,
            $availableDeliveries,
            [$attribute]
        );

        $this->attribute = $attribute;

        parent::setUp();
    }

    public function testAttributeNames()
    {
        $expected = [
            'comment',
            'deliveryType',
            'countryCode',
            'attrId',
        ];

        $attributeNames = $this->shopOrder->attributeNames();

        $this->assertSame($expected, $attributeNames);
    }

    public function testAttributeLabels()
    {
        $expected = [
            'comment' => 'Comment to the order',
            'attrId' => 'Attribute label',
        ];

        $attributeNames = $this->shopOrder->attributeLabels();

        $this->assertSame($expected, $attributeNames);
    }

    public function testRules()
    {
        $rules = $this->attribute->getRules();

        $orderRules = $this->shopOrder->rules();

        $this->assertCount(4, $orderRules);
        $this->assertSame(end($rules), end($orderRules));
    }

    public function testGetValue()
    {
        $value = $this->shopOrder->attrId;

        $this->assertSame('attr value', $value);
    }

    public function testSetValue()
    {
        $this->attribute
            ->expects($this->once())
            ->method('setValue')
            ->with('new value');

        $this->shopOrder->attrId = 'new value';
    }

    public function testIsSetValue()
    {
        $true = isset($this->shopOrder->attrId);
        $false = isset($this->shopOrder->wrongId);

        $this->assertTrue($true);
        $this->assertFalse($false);
    }

    public function testUnsetValue()
    {
        $this->attribute
            ->expects($this->once())
            ->method('setValue')
            ->with(null);

        unset($this->shopOrder->attrId);
    }

    public function testValidate()
    {
        $validated = $this->shopOrder->validate();
        $errors = $this->shopOrder->getErrors();

        $this->assertTrue($validated);
        $this->assertCount(0, $errors);
    }

    public function testMakeOrder()
    {
        $this->markTestSkipped('Not implemented');
    }
}
