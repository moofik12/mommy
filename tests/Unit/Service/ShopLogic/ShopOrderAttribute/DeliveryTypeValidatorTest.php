<?php

namespace MommyCom\Test\Unit\Service\ShopLogic\ShopOrderAttribute;

use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\DeliveryTypeValidator;
use PHPUnit\Framework\TestCase;

class DeliveryTypeValidatorTest extends TestCase
{
    /**
     * @var \CModel
     */
    private $model;

    /**
     * @var DeliveryTypeValidator
     */
    private $validator;

    protected function setUp()
    {
        $model = $this
            ->getMockBuilder(\CModel::class)
            ->setMethods(['attributeNames', '__get'])
            ->getMock();

        $model
            ->expects($this->any())
            ->method('attributeNames')
            ->willReturn(['deliveryType']);

        $model
            ->expects($this->any())
            ->method('__get')
            ->willReturn('555');

        $this->model = $model;
        $this->validator = DeliveryTypeValidator::createValidator(
            DeliveryTypeValidator::class,
            $this->model,
            ['deliveryType']
        );

        parent::setUp();
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage AvailableDeliveries
     */
    public function testValidateWithoutInit()
    {
        $this->validator->validate($this->model);
    }

    public function testValidateCorrect()
    {
        $availableDeliveries = $this
            ->getMockBuilder(AvailableDeliveries::class)
            ->disableOriginalConstructor()
            ->getMock();

        $availableDeliveries
            ->expects($this->any())
            ->method('hasDelivery')
            ->with(555)
            ->willReturn(true);

        $this->validator->availableDeliveries = $availableDeliveries;

        $this->validator->validate($this->model);

        $errors = $this->model->getErrors();

        $this->assertEmpty($errors);
    }

    public function testValidateIncorrect()
    {
        $availableDeliveries = $this
            ->getMockBuilder(AvailableDeliveries::class)
            ->disableOriginalConstructor()
            ->getMock();

        $availableDeliveries
            ->expects($this->any())
            ->method('hasDelivery')
            ->with(555)
            ->willReturn(false);

        $this->validator->availableDeliveries = $availableDeliveries;

        $this->validator->validate($this->model);

        $errors = $this->model->getErrors();
        $expected = ['deliveryType' => ['Invalid delivery type.']];

        $this->assertSame($expected, $errors);
    }
}
