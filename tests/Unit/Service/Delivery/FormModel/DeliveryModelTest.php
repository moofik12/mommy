<?php

namespace MommyCom\Test\Unit\Service\Delivery\FormModel;

use MommyCom\Service\Delivery\FormModel\DeliveryModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DeliveryModelTest extends TestCase
{
    /**
     * @var DeliveryModel|MockObject
     */
    private $deliveryModel;

    protected function setUp()
    {
        /** @var DeliveryModel|MockObject $deliveryModel */
        $deliveryModel = $this
            ->getMockBuilder(DeliveryModel::class)
            ->disableOriginalConstructor()
            ->setMethods(['setAttributes'])
            ->getMockForAbstractClass();

        $this->deliveryModel = $deliveryModel;

        parent::setUp();
    }

    public function testGetModelName()
    {
        $modelName = $this->deliveryModel->getModelName();

        $this->assertStringStartsWith('Mock_DeliveryModel_', $modelName);
    }

    public function testSetAttributesFromRequest()
    {
        /** @var \CHttpRequest|MockObject $request */
        $request = $this
            ->getMockBuilder(\CHttpRequest::class)
            ->getMock();

        $request->expects($this->once())
            ->method('getPost')
            ->willReturnCallback(function ($modelName) {
                $this->assertStringStartsWith('Mock_DeliveryModel_', $modelName);

                return ['test' => 'attribute'];
            });

        $this->deliveryModel
            ->expects($this->once())
            ->method('setAttributes')
            ->with(['test' => 'attribute']);

        $this->deliveryModel->setAttributesFromRequest($request);
    }
}
