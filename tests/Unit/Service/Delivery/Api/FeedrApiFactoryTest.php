<?php

namespace MommyCom\Test\Unit\Service\Delivery\Api;

use GuzzleHttp\Client;
use MommyCom\Service\Delivery\Api\FeedrApiFactory;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Symfony\Component\Cache\Simple\NullCache;

class FeedrApiFactoryTest extends TestCase
{
    public function testCreateFeedrApi()
    {
        $feedrApi = FeedrApiFactory::createFeedrApi(new NullCache(), new NullLogger(), 'someKey', true);

        /** @var Client $client */
        $client = $this->readAttribute($feedrApi, 'client');

        $this->assertAttributeSame('someKey', 'apiKey', $feedrApi);
        $this->assertAttributeSame('https://shipper.id/label-dev/sticker.php?oid={oid}&uid={uid}', 'labelUrl', $feedrApi);
        $this->assertInstanceOf(Client::class, $client);
        $this->assertSame('https://api.shipper.id/sandbox/', (string)$client->getConfig('base_uri'));
        $this->assertStringStartsWith('Mozilla/5.0 (Linux x86_64) GuzzleHttp/', $client->getConfig('headers')['User-Agent']);
    }

    public function testCreateFeedrApiProd()
    {
        $feedrApi = FeedrApiFactory::createFeedrApi(new NullCache(), new NullLogger(), 'someKey', false);

        /** @var Client $client */
        $client = $this->readAttribute($feedrApi, 'client');

        $this->assertAttributeSame('someKey', 'apiKey', $feedrApi);
        $this->assertAttributeSame('https://shipper.id/label/sticker.php?oid={oid}&uid={uid}', 'labelUrl', $feedrApi);
        $this->assertInstanceOf(Client::class, $client);
        $this->assertSame('https://api.shipper.id/prod/', (string)$client->getConfig('base_uri'));
        $this->assertStringStartsWith('Mozilla/5.0 (Linux x86_64) GuzzleHttp/', $client->getConfig('headers')['User-Agent']);
    }
}
