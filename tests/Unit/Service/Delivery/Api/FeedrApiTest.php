<?php

namespace MommyCom\Test\Unit\Service\Delivery\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\Service\Order\OrderInterface;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;

class FeedrApiTest extends TestCase
{
    /**
     * @param array $responses
     *
     * @return FeedrApi
     */
    private function makeFeedrApi(array $responses): FeedrApi
    {
        $handler = MockHandler::createWithMiddleware($responses);
        $client = new Client([
            'handler' => $handler,
            'base_url' => 'http://api.example.com',
        ]);
        $labelUrl = 'https://shipper.id/label-dev/sticker.php?oid={oid}&uid={uid}';
        /** @var LoggerInterface|MockObject $logger */
        $logger = $this->getMockBuilder(LoggerInterface::class)->getMockForAbstractClass();

        return new FeedrApi($client, $logger, 'someApiKey', $labelUrl, 1, 'address', 'http://example.com/track/');
    }

    /**
     * @param string &$requestUri
     * @param array $body
     * @param int $code
     * @param array $headers
     *
     * @return \Closure
     */
    private function makeResponseHandler(&$requestUri, array $body, int $code = 200, array $headers = [])
    {
        return function (RequestInterface $request) use (&$requestUri, $code, $headers, $body) {
            $requestUri = (string)$request->getUri();

            return new Response($code, $headers, json_encode($body));
        };
    }

    public function testGetProvinces()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'rows' => [
                    [
                        'id' => 1,
                        'name' => 'Bali',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Bangka Belitung',
                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $provinces = $feedrApi->getProvinces();

        $this->assertSame('public/v1/provinces?apiKey=someApiKey', $requestUri);
        $this->assertEquals($body['data']['rows'], $provinces);
    }

    public function testGetCities()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'rows' => [
                    [
                        'id' => 8,
                        'name' => 'Badung',
                    ],
                    [
                        'id' => 6,
                        'name' => 'Bangli',
                    ],

                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $cities = $feedrApi->getCities(1);

        $this->assertSame('public/v1/cities?apiKey=someApiKey&province=1', $requestUri);
        $this->assertEquals($body['data']['rows'], $cities);
    }

    public function testGetCitiesAll()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'rows' => [
                    [
                        'id' => 8,
                        'name' => 'Badung',
                        'province_id' => 1,
                        'province_name' => 'Bali',
                    ],
                    [
                        'id' => 6,
                        'name' => 'Bangli',
                        'province_id' => 1,
                        'province_name' => 'Bali',
                    ],

                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $cities = $feedrApi->getCities();

        $this->assertSame('public/v1/cities?apiKey=someApiKey&origin=all', $requestUri);
        $this->assertEquals($body['data']['rows'], $cities);
    }

    public function testGetSuburbs()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'rows' => [
                    [
                        'id' => 1,
                        'name' => 'Pekutatan',
                        'alias' => '',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Mendoyo',
                        'alias' => '',
                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $suburbs = $feedrApi->getSuburbs(1);

        $this->assertSame('public/v1/suburbs?apiKey=someApiKey&city=1', $requestUri);
        $this->assertEquals($body['data']['rows'], $suburbs);
    }

    public function testGetAreas()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'rows' => [
                    [
                        'id' => 1,
                        'name' => 'Pulukan',
                        'alias' => '',
                        'postcode' => '82262',
                    ],
                    [
                        'id' => 2,
                        'name' => 'Pekutatan',
                        'alias' => '',
                        'postcode' => '82262',
                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $areas = $feedrApi->getAreas(1);

        $this->assertSame('public/v1/areas?apiKey=someApiKey&suburb=1', $requestUri);
        $this->assertEquals($body['data']['rows'], $areas);
    }

    /**
     * @expectedException \MommyCom\Service\Delivery\Api\ApiException
     */
    public function testGetServerError()
    {
        $body = [
            'status' => 'failed',
            'data' => [
                'statusCode' => 500,
            ],
        ];

        $feedrApi = $this->makeFeedrApi([new Response(500, [], json_encode($body))]);

        // throws exception
        $feedrApi->getCities(1);
    }

    /**
     * @expectedException \MommyCom\Service\Delivery\Api\ApiException
     * @expectedExceptionMessage Invalid response data
     */
    public function testGetEmptyResponse()
    {
        $body = [];

        $feedrApi = $this->makeFeedrApi([new Response(200, [], json_encode($body))]);

        // throws exception
        $feedrApi->getCities(1);
    }

    /**
     * @expectedException \MommyCom\Service\Delivery\Api\ApiException
     * @expectedExceptionMessage Cities data is invalid
     */
    public function testGetWrongResponse()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'statusCode' => 200,
            ],
        ];

        $feedrApi = $this->makeFeedrApi([new Response(200, [], json_encode($body))]);

        // throws exception
        $feedrApi->getCities(1);
    }

    public function testGetDomesticRates()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'content' => 'Successfully retrieving rates',
                'weight' => 1.5,
                'volumeWeight' => 0.17,
                'rule' => 'Text about Shipping Restriction is here',
                'originArea' => 'Pulukan, Pekutatan, Jembrana',
                'destinationArea' => 'Pekutatan, Pekutatan, Jembrana',
                'rates' => [
                    'logistic' => [
                        'regular' => [
                            [
                                'rate_id' => 49,
                                'show_id' => 1,
                                'name' => 'POS Indonesia',
                                'logo_url' => 'http://cdn.shipper.cloud/logistic/small/POS.png',
                                'rate_name' => 'Paket Kilat Khusus',
                                'min_day' => 1,
                                'max_day' => 2,
                                'liability' => 120000,
                                'stop_origin' => 34407,
                                'stop_destination' => 34406,
                                'item_price' => 120000,
                                'finalWeight' => 2,
                                'finalRate' => 17000,
                                'insuranceRate' => 550,
                                'compulsory_insurance' => 0,
                                'discount' => 0,
                            ],
                            [
                                'rate_id' => 57,
                                'show_id' => 1,
                                'name' => 'J&T',
                                'logo_url' => 'http://cdn.shipper.cloud/logistic/small/JNT.png',
                                'rate_name' => 'Express',
                                'min_day' => 1,
                                'max_day' => 3,
                                'liability' => 120000,
                                'stop_origin' => 259,
                                'stop_destination' => 3846,
                                'item_price' => 120000,
                                'finalWeight' => 2,
                                'finalRate' => 22000,
                                'insuranceRate' => 5300,
                                'compulsory_insurance' => 0,
                                'discount' => 0,
                            ],
                        ],
                        'express' => [
                            [
                                'rate_id' => 56,
                                'show_id' => 2,
                                'name' => 'POS Indonesia',
                                'logo_url' => 'http://cdn.shipper.cloud/logistic/small/POS.png',
                                'rate_name' => 'Express Next Day',
                                'min_day' => 1,
                                'max_day' => 1,
                                'liability' => 120000,
                                'stop_origin' => 34407,
                                'stop_destination' => 34406,
                                'item_price' => 120000,
                                'finalWeight' => 2,
                                'finalRate' => 42000,
                                'insuranceRate' => 550,
                                'compulsory_insurance' => 0,
                                'discount' => 0,
                            ],
                        ],
                    ],
                ],
                'statusCode' => 200,
            ],
        ];
        $uri = 'public/v1/domesticRates?apiKey=someApiKey&o=1&d=2&wt=1.5&l=10&w=10&h=10&v=120000&cod=0&order=1';

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $rates = $feedrApi->getDomesticRates(2, 1.5, 10, 10, 10, 120000, false);

        $this->assertSame($uri, $requestUri);
        $this->assertEquals($body['data']['rates']['logistic'], $rates);
    }

    public function testGetDomesticRatesWhenNotFound()
    {
        $body = [
            'status' => 'fail',
            'data' => ['statusCode' => 404],
        ];
        $uri = 'public/v1/domesticRates?apiKey=someApiKey&o=1&d=2&wt=1.5&l=10&w=10&h=10&v=120000&cod=0&order=1';

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $rates = $feedrApi->getDomesticRates(2, 1.5, 10, 10, 10, 120000, false);

        $this->assertSame($uri, $requestUri);
        $this->assertSame([], $rates);
    }

    /**
     * @expectedException \MommyCom\Service\Delivery\Api\ApiException
     * @expectedExceptionMessage Domestic rates data is invalid
     */
    public function testGetDomesticRatesWithWrongResponse()
    {
        $body = [
            'status' => 'success',
            'data' => [],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        // throws exception
        $feedrApi->getDomesticRates(2, 1.5, 10, 10, 10, 120000, false);
    }

    public function testCreateDomesticOrder()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'id' => '5aabcee6e1029e78bb58d712',
            ],
        ];
        $uri = 'public/v1/orders/domestics?apiKey=someApiKey';
        $params = implode('&', [
            'o=1',
            'originAddress=address',
            'd=2',
            'destinationAddress=some+address',
            'wt=0.3',
            'l=30',
            'w=20',
            'h=10',
            'v=200000',
            'cod=0',
            'rateID=3',
            'logistic=1',
            'packageType=2',
            'itemName=Package',
            'consigneeName=Me',
            'consigneePhoneNumber=%2B62123456789',
            'externalID=100500',
            'contents=some+description',
        ]);

        $response = function (RequestInterface $request) use (&$requestUri, &$requestParams, $body) {
            $requestParams = (string)$request->getBody();
            $requestUri = (string)$request->getUri();

            return new Response(200, [], json_encode($body));
        };
        $feedrApi = $this->makeFeedrApi([$response]);

        $id = $feedrApi->createDomesticOrder(
            $destinationAreaId = 2,
            'some address',
            $weight = 0.3,
            $length = 30,
            $width = 20,
            $height = 10,
            $value = 200000,
            $cashOnDelivery = false,
            $rateId = 3,
            $isExpress = false,
            $itemName = 'Package',
            $consigneeName = 'Me',
            $consigneePhoneNumber = '+62123456789',
            $orderId = 100500,
            $orderDescription = 'some description'
        );

        $this->assertSame($uri, $requestUri);
        $this->assertSame($params, $requestParams);
        $this->assertSame('5aabcee6e1029e78bb58d712', $id);
    }

    public function testCreateOrder()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'id' => '5aabcee6e1029e78bb58d712',
            ],
        ];
        $uri = 'public/v1/orders/domestics?apiKey=someApiKey';
        $params = implode('&', [
            'o=1',
            'originAddress=address',
            'd=2',
            'destinationAddress=some+address',
            'wt=0.3',
            'l=30',
            'w=20',
            'h=10',
            'v=200000',
            'cod=0',
            'rateID=3',
            'logistic=1',
            'packageType=2',
            'itemName=Package',
            'consigneeName=Me',
            'consigneePhoneNumber=%2B62123456789',
            'externalID=100500',
            'contents=some+description',
        ]);

        $response = function (RequestInterface $request) use (&$requestUri, &$requestParams, $body) {
            $requestParams = (string)$request->getBody();
            $requestUri = (string)$request->getUri();

            return new Response(200, [], json_encode($body));
        };
        $feedrApi = $this->makeFeedrApi([$response]);

        /** @var MockObject|OrderInterface $order */
        $order = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $order
            ->method('getId')
            ->willReturn(100500);

        $order
            ->method('getTrackId')
            ->willReturn('5aabcee6e1029e78bb58d712');

        $order
            ->method('getCost')
            ->willReturn(new Money(200000, new Currency('USD')));

        $order
            ->method('getInvoice')
            ->willReturn(new Money(0, new Currency('USD')));

        $order
            ->method('getWeight')
            ->willReturn(300);

        /** @var Feedr|MockObject $deliveryModel */
        $deliveryModel = $this
            ->getMockBuilder(Feedr::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $deliveryModel->address = 'some address';
        $deliveryModel->areaId = 2;
        $deliveryModel->rateId = 3;

        $order
            ->method('getDeliveryModel')
            ->willReturn($deliveryModel);

        $order
            ->method('getCustomerName')
            ->willReturn('Me');

        $order
            ->method('getPhoneNumber')
            ->willReturn('+62123456789');

        $order
            ->method('getOrderDescription')
            ->willReturn('some description');

        $id = $feedrApi->createOrder($order);

        $this->assertSame($uri, $requestUri);
        $this->assertSame($params, $requestParams);
        $this->assertSame('5aabcee6e1029e78bb58d712', $id);
    }

    public function testCreateOrderWithCod()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'id' => '5aabcee6e1029e78bb58d712',
            ],
        ];
        $uri = 'public/v1/orders/domestics?apiKey=someApiKey';
        $params = implode('&', [
            'o=1',
            'originAddress=address',
            'd=2',
            'destinationAddress=some+address',
            'wt=0.3',
            'l=30',
            'w=20',
            'h=10',
            'v=192308',
            'cod=1',
            'rateID=3',
            'logistic=1',
            'packageType=2',
            'itemName=Package',
            'consigneeName=Me',
            'consigneePhoneNumber=%2B62123456789',
            'externalID=100500',
            'contents=some+description',
        ]);

        $response = function (RequestInterface $request) use (&$requestUri, &$requestParams, $body) {
            $requestParams = (string)$request->getBody();
            $requestUri = (string)$request->getUri();

            return new Response(200, [], json_encode($body));
        };
        $feedrApi = $this->makeFeedrApi([$response]);

        /** @var MockObject|OrderInterface $order */
        $order = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $order
            ->method('getId')
            ->willReturn(100500);

        $order
            ->method('getTrackId')
            ->willReturn('5aabcee6e1029e78bb58d712');

        $order
            ->method('getCost')
            ->willReturn(new Money(200000, new Currency('USD')));

        $order
            ->method('getInvoice')
            ->willReturn(new Money(200000, new Currency('USD')));

        $order
            ->method('getWeight')
            ->willReturn(300);

        /** @var Feedr|MockObject $deliveryModel */
        $deliveryModel = $this
            ->getMockBuilder(Feedr::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $deliveryModel->address = 'some address';
        $deliveryModel->areaId = 2;
        $deliveryModel->rateId = 3;

        $order
            ->method('getDeliveryModel')
            ->willReturn($deliveryModel);

        $order
            ->method('getCustomerName')
            ->willReturn('Me');

        $order
            ->method('getPhoneNumber')
            ->willReturn('+62123456789');

        $order
            ->method('getOrderDescription')
            ->willReturn('some description');

        $id = $feedrApi->createOrder($order);

        $this->assertSame($uri, $requestUri);
        $this->assertSame($params, $requestParams);
        $this->assertSame('5aabcee6e1029e78bb58d712', $id);
    }

    public function testGetOrderDetail()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'content' => 'Successfully retrieving history order detail',
                'order' => [
                    'detail' => [
                        '_id' => '5aabcee6e1029e78bb58d712',
                        'id' => '1A02682',
                        'groupID' => 153,
                        'specialID' => 'A2682',
                        'externalID' => '100502',
                        'labelChecksum' => '93d3eb9be44a44a2e5a0bf792b552579',
                        'consigner' => [
                            'id' => '5aa675303d25d0fd388b4567',
                            'name' => 'Andrew Smith',
                            'phoneNumber' => '+6291122334455',
                        ],
                        'consignee' => [
                            'id' => '5aabcc75e1029e78bb58d70b',
                            'name' => 'Sergey',
                            'phoneNumber' => '+62123456789',
                        ],
                        'batchID' => 0,
                        'awbNumber' => '',
                        'stickerNumber' => '',
                        'shipmentStatus' => [
                            'statusCode' => 1,
                            'name' => 'Order Diterima',
                            'description' => 'Order sudah diterima',
                            'updatedBy' => 'Andrew Smith',
                            'updateDate' => '2018-06-26T14:28:40+00:00',
                        ],
                    ],
                ],
                'statusCode' => 200,
            ],
        ];
        $uri = 'public/v1/orders/5aabcee6e1029e78bb58d712?apiKey=someApiKey';

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $detail = $feedrApi->getOrderDetails('5aabcee6e1029e78bb58d712');

        $this->assertSame($uri, $requestUri);
        $this->assertEquals($body['data']['order']['detail'], $detail);
    }

    public function testFindDeliveryId()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'content' => 'Successfully retrieving history order detail',
                'order' => [
                    'detail' => [
                        '_id' => '5aabcee6e1029e78bb58d712',
                        'id' => '1A02682',
                        'groupID' => 153,
                        'specialID' => 'A2682',
                        'externalID' => '100502',
                        'labelChecksum' => '93d3eb9be44a44a2e5a0bf792b552579',
                        'consigner' => [
                            'id' => '5aa675303d25d0fd388b4567',
                            'name' => 'Andrew Smith',
                            'phoneNumber' => '+6291122334455',
                        ],
                        'consignee' => [
                            'id' => '5aabcc75e1029e78bb58d70b',
                            'name' => 'Sergey',
                            'phoneNumber' => '+62123456789',
                        ],
                        'batchID' => 0,
                        'awbNumber' => '',
                        'stickerNumber' => '',
                        'shipmentStatus' => [
                            'statusCode' => 1,
                            'name' => 'Order Diterima',
                            'description' => 'Order sudah diterima',
                            'updatedBy' => 'Andrew Smith',
                            'updateDate' => '2018-06-26T14:28:40+00:00',
                        ],
                        'creationDate' => '2018-06-26T14:28:40+00:00',
                    ],
                ],
                'statusCode' => 200,
            ],
        ];
        $uri = 'public/v1/orders/1A02682?apiKey=someApiKey';

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        $deliveryId = $feedrApi->findDeliveryId('1A02682');

        $this->assertSame($uri, $requestUri);
        $this->assertSame('5aabcee6e1029e78bb58d712', $deliveryId);
    }

    public function testGetLabelUrl()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'order' => [
                    'detail' => [
                        'id' => '1A02682',
                        'labelChecksum' => '93d3eb9be44a44a2e5a0bf792b552579',

                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $expectedUrl = 'https://shipper.id/label-dev/sticker.php?oid=1A02682&uid=93d3eb9be44a44a2e5a0bf792b552579';

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        /** @var MockObject|OrderInterface $order */
        $order = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $order
            ->method('getTrackId')
            ->willReturn('5aabcee6e1029e78bb58d712');

        $labelUrl = $feedrApi->getLabelUrl($order);

        $this->assertEquals($expectedUrl, $labelUrl);
    }

    public function testGetDeliveryPrice()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'content' => 'Successfully retrieving history order detail',
                'order' => [
                    'detail' => [
                        '_id' => '5aabcee6e1029e78bb58d712',
                        'id' => '1A02682',
                        'groupID' => 153,
                        'specialID' => 'A2682',
                        'externalID' => '100502',
                        'courier' => [
                            'rate' => [
                                'value' => 11000,
                                'UoM' => 'IDR',
                            ],
                        ],
                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        /** @var MockObject|OrderInterface $order */
        $order = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $order
            ->method('getTrackId')
            ->willReturn('5aabcee6e1029e78bb58d712');

        $deliveryPrice = $feedrApi->getDeliveryPrice($order);

        $this->assertSame(11000, $deliveryPrice);
    }

    public function testGetDeadlineForDelivery()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'content' => 'Successfully retrieving history order detail',
                'order' => [
                    'detail' => [
                        '_id' => '5aabcee6e1029e78bb58d712',
                        'id' => '1A02682',
                        'groupID' => 153,
                        'specialID' => 'A2682',
                        'externalID' => '100502',
                        'courier' => [
                            'min_day' => 4,
                            'max_day' => 7,
                            'rate' => [
                                'value' => 11000,
                                'UoM' => 'IDR',
                            ],
                        ],
                        'creationDate' => '2018-06-19T11:00:31+00:00',
                        'activeDate' => '',
                        'lastUpdatedDate' => '2018-06-19T11:00:31+00:00',
                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        /** @var MockObject|OrderInterface $order */
        $order = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $order
            ->method('getTrackId')
            ->willReturn('5aabcee6e1029e78bb58d712');

        $deadlineForDelivery = $feedrApi->getDeadlineForDelivery($order);
        $expectedDateTime = new \DateTime('2018-06-26T11:00:31+00:00');

        $this->assertSame($expectedDateTime->getTimestamp(), $deadlineForDelivery->getTimestamp());
    }

    public function testGetDeliveryStatus()
    {
        $body = [
            'status' => 'success',
            'data' => [
                'title' => 'OK',
                'content' => 'Successfully retrieving history order detail',
                'order' => [
                    'detail' => [
                        '_id' => '5aabcee6e1029e78bb58d712',
                        'id' => '1A02682',
                        'groupID' => 153,
                        'specialID' => 'A2682',
                        'externalID' => '100502',
                        'labelChecksum' => '93d3eb9be44a44a2e5a0bf792b552579',
                        'consigner' => [
                            'id' => '5aa675303d25d0fd388b4567',
                            'name' => 'Andrew Smith',
                            'phoneNumber' => '+6291122334455',
                        ],
                        'consignee' => [
                            'id' => '5aabcc75e1029e78bb58d70b',
                            'name' => 'Sergey',
                            'phoneNumber' => '+62123456789',
                        ],
                        'batchID' => 0,
                        'awbNumber' => '',
                        'stickerNumber' => '',
                        'shipmentStatus' => [
                            'statusCode' => 1,
                            'name' => 'Order Diterima',
                            'description' => 'Order sudah diterima',
                            'updatedBy' => 'Andrew Smith',
                            'updateDate' => '2018-06-26T14:28:40+00:00',
                        ],
                    ],
                ],
                'statusCode' => 200,
            ],
        ];

        $response = $this->makeResponseHandler($requestUri, $body);
        $feedrApi = $this->makeFeedrApi([$response]);

        /** @var MockObject|OrderInterface $order */
        $order = $this
            ->getMockBuilder(OrderInterface::class)
            ->getMockForAbstractClass();

        $order
            ->method('getTrackId')
            ->willReturn('5aabcee6e1029e78bb58d712');

        $deliveryStatus = $feedrApi->getDeliveryStatus($order);

        $this->assertSame(11, $deliveryStatus);
    }

    public function testGetTrackUrl()
    {
        $response = $this->makeResponseHandler($requestUrl, []);
        $feedrApi = $this->makeFeedrApi([$response]);

        $trackUrl = $feedrApi->getTrackUrl('test');

        $this->assertEquals('http://example.com/track/test', $trackUrl);
    }
}
