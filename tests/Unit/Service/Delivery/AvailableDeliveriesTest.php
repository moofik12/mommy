<?php

namespace MommyCom\Test\Unit\Service\Delivery;

use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryInterface;
use PHPUnit\Framework\TestCase;

class AvailableDeliveriesTest extends TestCase
{
    /**
     * @var DeliveryInterface
     */
    private $delivery;

    /**
     * @var AvailableDeliveries
     */
    private $availableDeliveries;

    protected function setUp()
    {
        $delivery = $this
            ->getMockBuilder(DeliveryInterface::class)
            ->getMockForAbstractClass();

        $delivery
            ->expects($this->any())
            ->method('getId')
            ->willReturn(100500);

        $this->delivery = $delivery;
        $this->availableDeliveries = new AvailableDeliveries([$delivery]);

        parent::setUp();
    }

    public function testGetDeliveries()
    {
        $deliveries = $this->availableDeliveries->getDeliveries();

        $this->assertSame([100500 => $this->delivery], $deliveries);
    }

    public function testGetFirstDelivery()
    {
        $delivery = $this->availableDeliveries->getFirstDelivery();

        $this->assertSame($this->delivery, $delivery);
    }

    public function testGetDelivery()
    {
        $delivery = $this->availableDeliveries->getDelivery(100500);

        $this->assertSame($this->delivery, $delivery);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Delivery with id 333 is unavailable
     */
    public function testGetDeliveryByIncorrectId()
    {
        $this->availableDeliveries->getDelivery(333);
    }

    public function testHasDelivery()
    {
        $hasDelivery = $this->availableDeliveries->hasDelivery(100500);
        $hasNotDelivery = $this->availableDeliveries->hasDelivery(333);

        $this->assertTrue($hasDelivery);
        $this->assertFalse($hasNotDelivery);
    }

    public function testGetIterator()
    {
        $deliveries = iterator_to_array($this->availableDeliveries);

        $this->assertSame([100500 => $this->delivery], $deliveries);
    }
}
