<?php

namespace MommyCom\Test\Unit\Service;

use CHtml;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\SplitTesting;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;

class SplitTestingTest extends TestCase
{
    /**
     * @runInSeparateProcess
     */
    public function providerRandom()
    {
        /** @var MockObject|Regions $regions */
        $regions = $this
            ->getMockBuilder(Regions::class)
            ->disableOriginalConstructor()
            ->getMock();

        $regions->expects($this->any())
            ->method('getServerRegion')
            ->willReturn(new Region(Region::GLOBAL, Region::DETECT_LEVEL_SERVER, false));

        /** @var MockObject|CacheInterface $cache */
        $cache = $this
            ->getMockBuilder(CacheInterface::class)
            ->getMockForAbstractClass();

        $splitTest = new SplitTesting($regions, $cache, true);

        $testName = 'test';
        $cookieName = 'splittest-' . CHtml::getIdByName($testName);
        $results = [];

        $countTest = 10001;
        $variations = 3;
        $fault = 330; //погрешность
        $goldenMean = round($countTest / $variations);

        for ($i = 0; $i < $countTest; $i++) {
            //clear params
            if (isset($_COOKIE[$cookieName])) {
                unset($_COOKIE[$cookieName]);
            }

            $results[] = $splitTest->getNum($testName, $variations);
        }

        $this->assertNotEmpty($results);

        $results = array_count_values($results);

        $checkResults = array_map(function ($item) use ($goldenMean, $fault) {
            return [$item, $goldenMean - $fault, $goldenMean + $fault];
        }, $results);

        return $checkResults;
    }

    /**
     * @param $check
     * @param $minValid
     * @param $maxValid
     *
     * @dataProvider providerRandom
     */
    public function testAdd($check, $minValid, $maxValid)
    {
        $this->assertTrue($check >= $minValid && $check <= $maxValid);
    }
}
