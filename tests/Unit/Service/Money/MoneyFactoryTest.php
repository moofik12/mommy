<?php

namespace MommyCom\Test\Unit\Service\Money;

use MommyCom\Service\Money\MoneyFactory;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class MoneyFactoryTest extends TestCase
{
    public function providerMakeMoney()
    {
        return [
            '0 $' => ['USD', 0, new Money(0, new Currency('USD'))],
            '1 cent' => ['USD', 0.01, new Money(1, new Currency('USD'))],
            '1 $' => ['USD', 1, new Money(100, new Currency('USD'))],
            '~1.5 $' => ['USD', 1.495, new Money(150, new Currency('USD'))],
            '1 VND' => ['VND', 1.0, new Money(1, new Currency('VND'))],
        ];
    }

    /**
     * @dataProvider providerMakeMoney
     *
     * @param $code
     * @param $value
     * @param $expected
     */
    public function testMakeMoney($code, $value, $expected)
    {
        $currencies = new ISOCurrencies();
        $moneyFactory = new MoneyFactory($currencies, new Currency($code));

        $actual = $moneyFactory->makeMoney($value);

        $this->assertEquals($expected, $actual);
    }
}
