<?php

namespace MommyCom\Test\Unit\Service\Money;

use MommyCom\Service\Money\Formatter;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class FormatterTest extends TestCase
{
    public function providerFormat()
    {
        return [
            ['USD', 100, '1 $'],
            ['USD', 250250250, '2 502 502.5 $'],
            ['IDR', 100, 'idr 1'],
            ['IDR', 250250250, 'idr 2.502.503'],
            ['VND', 1, '1 ₫'],
            ['VND', 2502502, '2 502 502 ₫'],
        ];
    }

    /**
     * @dataProvider providerFormat
     *
     * @param string $code
     * @param int $amount
     * @param string $expected
     */
    public function testFormat($code, $amount, $expected)
    {
        $money = new Money($amount, new Currency($code));
        $formatter = new Formatter(new ISOCurrencies());

        $actual = $formatter->format($money);

        $this->assertSame($expected, $actual);
    }
}
