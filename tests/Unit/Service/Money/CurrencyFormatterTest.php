<?php

namespace MommyCom\Test\Unit\Service\Money;

use MommyCom\Service\Money\CurrencyFormatter;
use MommyCom\Service\Money\Formatter;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class CurrencyFormatterTest extends TestCase
{
    public function providerFormat()
    {
        return [
            ['USD', null, ''],
            ['USD', 0, '0 $'],
            ['USD', '0', '0 $'], // non-numeric bug
            ['USD', '000500.50', '500.5 $'],
            ['USD', 1, '1 $'],
            ['USD', 25002500, '25 002 500 $'],
            ['VND', 1, '1 ₫'],
            ['VND', 25002500, '25 002 500 ₫'],
            ['IDR', 1234.5, 'idr 1.235'], // round
            ['USD', new Money(100, new Currency('USD')), '1 $'],
            ['USD', new Money(100, new Currency('VND')), '100 ₫'],
        ];
    }

    /**
     * @dataProvider providerFormat
     *
     * @param $code
     * @param $value
     * @param $expected
     */
    public function testFormat($code, $value, $expected)
    {
        $currencies = new ISOCurrencies();
        $currencyFormatter = new CurrencyFormatter($currencies, new Currency($code), new Formatter($currencies));

        $actual = $currencyFormatter->format($value);

        $this->assertSame($expected, $actual);
    }
}
