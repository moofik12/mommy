<?php

namespace MommyCom\Test\Unit\Service\SupplierOrder;

use Doctrine\ORM\EntityManager;
use MommyCom\Entity\Event;
use MommyCom\Entity\Supplier;
use MommyCom\Repository\EventRepository;
use MommyCom\Repository\SupplierRepository;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\Service\Mail\EmailUtils;
use MommyCom\YiiComponent\SupplierOrder\NotificationDataFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use MommyCom\Service\Translator\TranslatorInterface;

class NotificationDataFactoryTest extends TestCase
{
    /**
     * @var EntityManager|MockObject
     */
    private $entityManager;

    /**
     * @var EventRepository|MockObject
     */
    private $eventRepository;

    /**
     * @var SupplierRepository|MockObject
     */
    private $supplierRepository;

    /**
     * @var Regions
     */
    private $regions;

    /**
     * @var NotificationDataFactory
     */
    private $notificationDataFactory;

    public function setUp()
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->eventRepository = $this->createMock(EventRepository::class);
        $this->supplierRepository = $this->createMock(SupplierRepository::class);
        $this->entityManager
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnMap([
                [Event::class, $this->eventRepository],
                [Supplier::class, $this->supplierRepository],
            ]);

        /** @var TranslatorInterface|MockObject $translator */
        $translator = $this->getMockForAbstractClass(TranslatorInterface::class);
        $this->notificationDataFactory = new NotificationDataFactory(
            $this->entityManager,
            $translator,
            $this->createRegions(),
            false
        );
    }

    public function testCreateManagerNotification()
    {

        $notification = $this->notificationDataFactory->create(
            NotificationDataFactory::TYPE_BRAND_MANAGER_CONFIRMATION,
            1
        );

        $content = $notification->getMailContent();

        $this->assertContains('https://id.mommy.com/backend.php', $content);
    }

    private function createRegions(): Regions
    {
        $regionsSettings = ['Indonesia' => 'id.mommy.com'];

        $serverRegion = new Region('Indonesia', Region::DETECT_LEVEL_SERVER, false);
        $userRegion = new Region('Global', Region::DETECT_LEVEL_UNKNOWN, false);
        $regions = new Regions($regionsSettings, $serverRegion, $userRegion);

        return $regions;
    }
}