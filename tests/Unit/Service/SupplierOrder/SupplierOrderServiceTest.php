<?php

namespace MommyCom\Test\Unit\Service\SupplierOrder;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Entity\Event;
use MommyCom\Entity\EventProduct;
use MommyCom\Entity\Order;
use MommyCom\Entity\WarehouseProduct;
use MommyCom\Repository\EventProductRepository;
use MommyCom\Repository\EventRepository;
use MommyCom\Repository\OrderRepository;
use MommyCom\Repository\WarehouseProductRepository;
use MommyCom\Service\Warehouse\SecurityCodeGenerator;
use MommyCom\YiiComponent\SupplierOrder\ManagerNotification;
use MommyCom\YiiComponent\SupplierOrder\NotificationData;
use MommyCom\YiiComponent\SupplierOrder\SupplierOrderService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SupplierOrderServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private $entityManager;

    /**
     * @var SupplierOrderService|MockObject
     */
    private $supplierOrderService;

    /**
     * @var EventProduct|MockObject
     */
    private $eventProduct;

    /**
     * @var EventRepository|MockObject
     */
    private $eventRepository;

    /**
     * @var EventProductRepository|MockObject
     */
    private $eventProductRepository;

    /**
     * @var OrderRepository|MockObject
     */
    private $orderRepository;

    /**
     * @var WarehouseProductRepository|MockObject
     */
    private $warehouseProductRepository;

    /**
     * @var array $products
     */
    private $products;

    protected function setUp(): void
    {
        $this->eventProduct = $this->createEventProduct();
        $this->eventRepository = $this->createMock(EventRepository::class);
        $this->eventProductRepository = $this->createMock(EventProductRepository::class);
        $this->orderRepository = $this->createMock(OrderRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->warehouseProductRepository = $this->createMock(WarehouseProductRepository::class);

        $this->entityManager
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnMap([
                [EventProduct::class, $this->eventProductRepository],
                [Event::class, $this->eventRepository],
                [Order::class, $this->orderRepository],
                [WarehouseProduct::class, $this->warehouseProductRepository],
            ]);

        $this->supplierOrderService = new SupplierOrderService($this->entityManager);

        $this->products = [
            1 => [
                'isConfirmed' => 'true',
                'deliveredQty' => 1,
                'comment' => 'test',
            ],
            2 => [
                'isConfirmed' => 'true',
                'deliveredQty' => 2,
                'comment' => 'test',
            ],
        ];
    }

    public function testConfirmProducts()
    {
        $this->eventRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn(new Event());

        $this->eventProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->willReturn([$this->eventProduct, $this->eventProduct]);

        $this->supplierOrderService->confirmProducts($this->products);
    }

    public function testSendManagerNotification()
    {
        $notificationData = $this
            ->getMockBuilder(NotificationData::class)
            ->disableOriginalConstructor()
            ->getMock();

        $notification = $this
            ->getMockBuilder(ManagerNotification::class)
            ->disableOriginalConstructor()
            ->getMock();

        $notification
            ->expects($this->once())
            ->method('send');

        $this->supplierOrderService->sendManagerNotification($notificationData, $notification, $this->products);
    }

    public function testMakePartialDeliveryRecalls()
    {
        $this->eventProduct
            ->expects($this->exactly(2))
            ->method('getNumberSold')
            ->willReturn(20);

        $this->orderRepository
            ->expects($this->exactly(2))
            ->method('getCheapestOrderWithProduct')
            ->willReturn(new Order());

        $this->eventProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->willReturn([$this->eventProduct, $this->eventProduct]);

        $supplierOrderService = new SupplierOrderService($this->entityManager);

        $supplierOrderService->makePartialDeliveryRecalls($this->products);
    }

    public function testConfirmWarehouse()
    {
        $this->eventRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn(new Event());

        $this->eventProductRepository
            ->expects($this->once())
            ->method('findBy')
            ->willReturn([$this->eventProduct, $this->eventProduct]);

        $this->entityManager
            ->expects($this->exactly(2))
            ->method('persist');

        $this->warehouseProductRepository
            ->expects($this->exactly(2))
            ->method('createFromEventProduct')
            ->willReturn(new WarehouseProduct());

        $this->supplierOrderService->confirmWarehouse(new SecurityCodeGenerator(), $this->products);
    }

    /**
     * @return EventProduct|MockObject
     */
    private function createEventProduct(): EventProduct
    {
        $eventProductParameters = [
            'getId' => 1,
            'getSupplierId' => 1,
            'getEventId' => 1,
            'getSizeformat' => 1,
            'getSize' => 1,
            'getPrice' => 1,
            'getProductId' => 1,
        ];

        $eventProduct = $this->createMock(EventProduct::class);

        foreach ($eventProductParameters as $method => $returnValue) {
            $eventProduct
                ->expects($this->any())
                ->method($method)
                ->willReturn($returnValue);
        }

        return $eventProduct;
    }
}
