<?php

namespace MommyCom\Test\Unit\Service\Warehouse;

use MommyCom\Entity\EventProduct;
use MommyCom\Service\Warehouse\SecurityCodeGenerator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SecurityCodeGeneratorTest extends TestCase
{
    private function makeEventProduct(int $productId, string $color, int $size): EventProduct
    {
        $eventProductParameters = [
            'getProductId' => $productId,
            'getColor' => $color,
            'getSize' => $size,
        ];

        /** @var EventProduct|MockObject $eventProduct */
        $eventProduct = $this->createMock(EventProduct::class);

        foreach ($eventProductParameters as $method => $returnValue) {
            $eventProduct
                ->expects($this->any())
                ->method($method)
                ->willReturn($returnValue);
        }

        return $eventProduct;
    }

    public function providerGenerate()
    {
        return [
            [1, 'red', 1, 5588],
            [42, 'blue', 555, 6706],
        ];
    }

    /**
     * @dataProvider providerGenerate
     *
     * @param int $productId
     * @param string $color
     * @param int $size
     * @param int $expected
     */
    public function testGenerate(int $productId, string $color, int $size, int $expected)
    {
        $generator = new SecurityCodeGenerator();
        $code = $generator->generate($this->makeEventProduct($productId, $color, $size));

        $this->assertLessThan(10000, $code);
        $this->assertGreaterThan(0, $code);
        $this->assertSame($expected, $code);
    }
}
