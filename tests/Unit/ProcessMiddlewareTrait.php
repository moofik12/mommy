<?php

namespace MommyCom\Test\Unit;

use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

trait ProcessMiddlewareTrait
{
    /**
     * @param MiddlewareInterface $middleware
     * @param ServerRequestInterface|null $request
     * @param RequestHandlerInterface|null $handler
     *
     * @return ResponseInterface
     */
    protected function processMiddleware(
        MiddlewareInterface $middleware,
        ServerRequestInterface $request = null,
        RequestHandlerInterface $handler = null
    ): ResponseInterface {
        if (null === $request) {
            /** @var MockObject|ServerRequestInterface $request */
            $request = $this
                ->getMockBuilder(ServerRequestInterface::class)
                ->getMockForAbstractClass();

            foreach (get_class_methods($request) as $method) {
                if ('with' === substr($method, 0, 4)) {
                    $request
                        ->method($method)
                        ->willReturnSelf();
                }
            }
        }

        if (null === $handler) {
            $response = $this
                ->getMockBuilder(ResponseInterface::class)
                ->getMockForAbstractClass();

            /** @var MockObject|RequestHandlerInterface $handler */
            $handler = $this
                ->getMockBuilder(RequestHandlerInterface::class)
                ->getMockForAbstractClass();

            $handler
                ->method('handle')
                ->willReturn($response);
        }

        return $middleware->process($request, $handler);
    }

    abstract public function getMockBuilder($className): MockBuilder;
}
