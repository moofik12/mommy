<?php

namespace MommyCom\Test\Integration\Command;

use Doctrine\ORM\EntityManagerInterface;
use MommyCom\Command\AppSyncDeliveryStatusCommand;
use MommyCom\ConsoleApplication;
use MommyCom\Entity\Order;
use MommyCom\Entity\OrderTracking;
use MommyCom\Repository\OrderRepository;
use MommyCom\Repository\OrderTrackingRepository;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryApiInterface;
use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Order\MommyOrder;
use MommyCom\Service\Order\MommyOrderManager;
use MommyCom\Test\Integration\WebTestCase;
use MommyCom\Test\Unit\MockDateTimeTrait;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class AppSyncDeliveryStatusCommandTest extends WebTestCase
{
    use MockDateTimeTrait;

    /**
     * @var Command
     */
    private $command;

    /**
     * @var AvailableDeliveries|MockObject
     */
    private $availableDeliveries;

    /**
     * @var MommyOrderManager|MockObject
     */
    private $mommyOrderManager;

    /**
     * @var OrderTrackingRepository|MockObject
     */
    private $orderTrackingRepository;

    /**
     * @var OrderRepository|MockObject
     */
    private $orderRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new ConsoleApplication($kernel);

        $application->add($this->createCommand());

        $this->command = $application->find('app:sync-delivery-status');
    }

    private function runCommand(): string
    {
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([
            'command' => $this->command->getName(),
        ]);

        return $commandTester->getDisplay();
    }

    public function testJustExecute()
    {
        $output = $this->runCommand();

        $this->assertNotContains('ERROR', $output);
        $this->assertContains('Done.', $output);
    }

    public function testExecuteWhenStatusNotChanged()
    {
        $mockedTime = '2018-08-01 22:03:38';
        $this->mockDateTime($mockedTime);

        $orderId = 100500;
        $deliveryType = 42;
        $deliveryStatus = 0;

        $orderTracking = $this->makeOrderTracking($orderId, $deliveryType, $deliveryStatus, strtotime($mockedTime));

        $orderTracking
            ->expects($this->never())
            ->method('setDeliveredDate');

        $orderTracking
            ->expects($this->never())
            ->method('setStatus');

        $this
            ->orderTrackingRepository
            ->method('findNotSynchronized')
            ->willReturn([$orderTracking]);

        $this
            ->availableDeliveries
            ->expects($this->once())
            ->method('hasDelivery')
            ->with($deliveryType)
            ->willReturn(true);

        $this
            ->availableDeliveries
            ->expects($this->once())
            ->method('getDelivery')
            ->with($deliveryType)
            ->willReturn($this->makeDelivery($deliveryStatus));

        $this
            ->mommyOrderManager
            ->expects($this->once())
            ->method('find')
            ->with($orderId)
            ->willReturn($this->makeOrder());

        // test mocks
        $this->runCommand();
    }

    public function testExecuteWhenStatusChanged()
    {
        $mockedTime = '2018-08-01 22:03:38';
        $this->mockDateTime($mockedTime);

        $orderId = 100500;
        $deliveryType = 42;
        $currentStatus = 0;
        $newStatus = 7;

        $orderTracking = $this->makeOrderTracking($orderId, $deliveryType, $currentStatus, strtotime($mockedTime));

        $orderTracking
            ->expects($this->never())
            ->method('setDeliveredDate');

        $orderTracking
            ->expects($this->once())
            ->method('setStatus')
            ->with($newStatus);

        $this
            ->orderTrackingRepository
            ->method('findNotSynchronized')
            ->willReturn([$orderTracking]);

        $this
            ->availableDeliveries
            ->expects($this->once())
            ->method('hasDelivery')
            ->with($deliveryType)
            ->willReturn(true);

        $this
            ->availableDeliveries
            ->expects($this->once())
            ->method('getDelivery')
            ->with($deliveryType)
            ->willReturn($this->makeDelivery($newStatus));

        $this
            ->mommyOrderManager
            ->expects($this->once())
            ->method('find')
            ->with($orderId)
            ->willReturn($this->makeOrder());

        // test mocks
        $this->runCommand();
    }

    public function testExecuteWhenStatusChangedToDelivered()
    {
        $mockedTime = '2018-08-01 22:03:38';
        $this->mockDateTime($mockedTime);

        $orderId = 100500;
        $deliveryType = 42;
        $currentStatus = 0;
        $newStatus = 2;

        $orderTracking = $this->makeOrderTracking($orderId, $deliveryType, $currentStatus, strtotime($mockedTime));

        $orderTracking
            ->expects($this->once())
            ->method('setDeliveredDate')
            ->with(strtotime($mockedTime));

        $orderTracking
            ->expects($this->once())
            ->method('setStatus')
            ->with($newStatus);

        $this
            ->orderTrackingRepository
            ->method('findNotSynchronized')
            ->willReturn([$orderTracking]);

        $this
            ->availableDeliveries
            ->expects($this->once())
            ->method('hasDelivery')
            ->with($deliveryType)
            ->willReturn(true);

        $this
            ->availableDeliveries
            ->expects($this->once())
            ->method('getDelivery')
            ->with($deliveryType)
            ->willReturn($this->makeDelivery($newStatus));

        $this
            ->mommyOrderManager
            ->expects($this->once())
            ->method('find')
            ->with($orderId)
            ->willReturn($this->makeOrder());

        // test mocks
        $this->runCommand();
    }

    private function createCommand(): AppSyncDeliveryStatusCommand
    {
        /** @var AvailableDeliveries|MockObject $availableDeliveries */
        $availableDeliveries = $this
            ->getMockBuilder(AvailableDeliveries::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MommyOrderManager|MockObject $mommyOrderManager */
        $mommyOrderManager = $this
            ->getMockBuilder(MommyOrderManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var EntityManagerInterface|MockObject $entityManager */
        $entityManager = $this
            ->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderRepository = $this
            ->getMockBuilder(OrderRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderTrackingRepository = $this
            ->getMockBuilder(OrderTrackingRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityManager
            ->method('getRepository')
            ->willReturnMap([
                [Order::class, $orderRepository],
                [OrderTracking::class, $orderTrackingRepository],
            ]);

        $this->availableDeliveries = $availableDeliveries;
        $this->mommyOrderManager = $mommyOrderManager;
        $this->orderRepository = $orderRepository;
        $this->orderTrackingRepository = $orderTrackingRepository;

        return new AppSyncDeliveryStatusCommand($availableDeliveries, $mommyOrderManager, $entityManager);
    }

    /**
     * @param int $orderId
     * @param int $deliveryType
     * @param int $deliveryStatus
     * @param int $time
     *
     * @return OrderTracking|MockObject
     */
    private function makeOrderTracking(int $orderId, int $deliveryType, int $deliveryStatus, int $time): OrderTracking
    {
        /** @var OrderTracking|MockObject $orderTracking */
        $orderTracking = $this
            ->getMockBuilder(OrderTracking::class)
            ->disableOriginalConstructor()
            ->getMock();

        $orderTracking
            ->method('getOrderId')
            ->willReturn($orderId);

        $orderTracking
            ->method('getOrderDeliveryType')
            ->willReturn($deliveryType);

        $orderTracking
            ->method('getStatus')
            ->willReturn($deliveryStatus);

        $orderTracking
            ->expects($this->once())
            ->method('setSynchronizedAt')
            ->with($time);

        return $orderTracking;
    }

    /**
     * @param int $deliveryStatus
     *
     * @return DeliveryInterface|MockObject
     */
    private function makeDelivery(int $deliveryStatus): DeliveryInterface
    {
        /** @var DeliveryApiInterface|MockObject $deliveryApi */
        $deliveryApi = $this
            ->getMockBuilder(DeliveryApiInterface::class)
            ->getMockForAbstractClass();

        $deliveryApi
            ->method('getDeliveryStatus')
            ->willReturn($deliveryStatus);

        /** @var DeliveryInterface|MockObject $delivery */
        $delivery = $this
            ->getMockBuilder(DeliveryInterface::class)
            ->getMockForAbstractClass();

        $delivery
            ->method('getApi')
            ->willReturn($deliveryApi);

        return $delivery;
    }

    /**
     * @return MommyOrder|MockObject
     */
    private function makeOrder(): MommyOrder
    {
        /** @var MommyOrder|MockObject $order */
        $order = $this
            ->getMockBuilder(MommyOrder::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $order;
    }
}
