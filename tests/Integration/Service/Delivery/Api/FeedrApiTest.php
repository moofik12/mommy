<?php

namespace MommyCom\Test\Integration\Service\Delivery\Api;

use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\Api\FeedrApiFactory;
use MommyCom\Test\Integration\WebTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class FeedrApiTest extends WebTestCase
{
    protected function setUp(): void
    {
        static::bootKernel();
    }

    /**
     * @return FeedrApi
     */
    private function makeFeedrApi(): FeedrApi
    {
        /** @var CacheInterface|MockObject $cache */
        $cache = $this
            ->getMockBuilder(CacheInterface::class)
            ->getMockForAbstractClass();

        $cache
            ->method('get')
            ->willReturnArgument(1);

        /** @var LoggerInterface|MockObject $logger */
        $logger = $this
            ->getMockBuilder(LoggerInterface::class)
            ->getMockForAbstractClass();

        return FeedrApiFactory::createFeedrApi($cache, $logger, 'af3d6a42cf86cacd46e6e5c63d0b4bea', true);
    }

    public function testGetProvinces()
    {
        $feedrApi = $this->makeFeedrApi();

        $provinces = $feedrApi->getProvinces();

        $this->assertCount(34, $provinces);
    }

    public function testGetCities()
    {
        $feedrApi = $this->makeFeedrApi();

        $cities = $feedrApi->getCities(1);

        $this->assertCount(9, $cities);
    }

    public function testGetSuburbs()
    {
        $feedrApi = $this->makeFeedrApi();

        $suburbs = $feedrApi->getSuburbs(1);

        $this->assertCount(5, $suburbs);
    }

    public function testGetAreas()
    {
        $feedrApi = $this->makeFeedrApi();

        $areas = $feedrApi->getAreas(1);

        $this->assertCount(8, $areas);
    }

    public function testGetDomesticRates()
    {
        $feedrApi = $this->makeFeedrApi();

        $rates = $feedrApi->getDomesticRates(2, 0.3, 30, 20, 10, 200000, false);

        $this->assertArrayHasKey('regular', $rates);
        $this->assertArrayHasKey('express', $rates);
    }

    public function testCreateDomesticOrder()
    {
        $feedrApi = $this->makeFeedrApi();

        $id = $feedrApi->createDomesticOrder(
            $destinationAreaId = 2,
            $destinationAddress = 'some address',
            $weight = 0.3,
            $length = 30,
            $width = 20,
            $height = 10,
            $value = 200000,
            $cashOnDelivery = false,
            $rateId = 1,
            $isExpress = false,
            $itemName = 'Package',
            $consigneeName = 'Me',
            $consigneePhoneNumber = '+62123456789',
            $orderId = time(),
            $orderDescription = 'random'
        );

        $this->assertInternalType('string', $id);
        $this->assertSame(24, strlen($id));

        return $id;
    }

    /**
     * @depends testCreateDomesticOrder
     *
     * @param string $id
     *
     * @return string
     */
    public function testGetOrderDetails(string $id)
    {
        $feedrApi = $this->makeFeedrApi();

        $details = $feedrApi->getOrderDetails($id);

        $this->assertArrayHasKey('_id', $details);
        $this->assertArrayHasKey('id', $details);
        $this->assertSame($details['_id'], $id);
        $this->assertInternalType('string', $details['id']);

        return $details['id'];
    }

    /**
     * @depends testGetOrderDetails
     *
     * @param string $id
     */
    public function testFindDeliveryId(string $id)
    {
        $feedrApi = $this->makeFeedrApi();

        $details = $feedrApi->getOrderDetails($id);

        $this->assertArrayHasKey('_id', $details);
        $this->assertArrayHasKey('id', $details);
        $this->assertSame($details['id'], $id);
    }
}
