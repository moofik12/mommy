<?php

namespace MommyCom\Test\Integration\Service\PaymentGateway;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\ServerRequest;
use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\PaymentGateway\EspayApi\EspayApi;
use MommyCom\Service\PaymentGateway\EspayApi\Provider;
use MommyCom\Service\PaymentGateway\EspayPaymentGateway;
use MommyCom\Service\PaymentGateway\PaymentMethodInterface;
use MommyCom\Test\Integration\WebTestCase;
use MommyCom\Test\Unit\MockDateTimeTrait;
use MommyCom\YiiComponent\TokenManager;
use Money\Currency;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ServerRequestInterface;
use Ramsey\Uuid\UuidFactory;
use Symfony\Component\Cache\Simple\NullCache;

class EspayPaymentGatewayTest extends WebTestCase
{
    use MockDateTimeTrait;

    /**
     * @var EspayPaymentGateway
     */
    private $espayPaymentGateway;

    protected function setUp(): void
    {
        static::bootKernel();

        $this->mockDateTime('2018-03-27 17:00:00');

        $client = $this
            ->getMockBuilder(ClientInterface::class)
            ->getMockForAbstractClass();

        $provider = new Provider([
            'bankCode' => 15,
            'productCode' => 'CODE',
            'productName' => 'NAME',
        ]);

        /** @var MockObject|EspayApi $espayApi */
        $espayApi = $this
            ->getMockBuilder(EspayApi::class)
            ->setConstructorArgs([
                new NullCache(),
                $client,
                'https://sandbox-kit.espay.id/',
                'fa3ca5b1724e9dde4699f79561dc0c86',
            ])
            ->setMethods(['getProviders'])
            ->getMock();

        $espayApi
            ->method('getProviders')
            ->willReturn([$provider]);

        $this->espayPaymentGateway = new EspayPaymentGateway(
            new Currency('USD'),
            new UuidFactory(),
            new TokenManager(),
            $espayApi,
            'key',
            sys_get_temp_dir()
        );
    }

    public function testGetId()
    {
        $id = $this->espayPaymentGateway->getId();
        $this->assertSame('espay', $id);
    }

    public function testGetPaymentMethods()
    {
        $methods = $this->espayPaymentGateway->getPaymentMethods();

        $this->assertContainsOnlyInstancesOf(PaymentMethodInterface::class, $methods);
        $this->assertCount(1, $methods);
    }

    public function testRender()
    {
        /** @var MockObject|Controller $controller */
        $controller = $this
            ->getMockBuilder(Controller::class)
            ->setConstructorArgs(['order'])
            ->getMockForAbstractClass();

        $html = $this->espayPaymentGateway->render($controller, 100500);

        $this->assertRegExp('#https://sandbox-kit.espay.id/#', $html);
        $this->assertRegExp('#pay/result&uid=100500&token=9ab5300b#', $html);
    }

    public function testHandleInquiry()
    {
        $request = (new ServerRequest('GET', '/'))
            ->withQueryParams(['type' => 'Inquiry'])
            ->withParsedBody([
                'rq_uuid' => 'rq_uuid',
                'rq_datetime' => 'rq_datetime',
                'comm_code' => 'comm_code',
                'order_id' => '1',
                'signature' => '61a0e8ee9f1b2e9cb96d082929c87cf2be3c7831408d9487373972253727359d',
            ]);

        $response = $this->espayPaymentGateway->handle($request);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertStringStartsWith('0;Success;', $response->getBody()->getContents());
    }

    public function testHandlePayment()
    {
        $request = (new ServerRequest('GET', '/'))
            ->withQueryParams(['type' => 'Payment'])
            ->withParsedBody([
                'rq_uuid' => 'rq_uuid',
                'rq_datetime' => 'rq_datetime',
                'comm_code' => 'comm_code',
                'order_id' => '1',
                'ccy' => 'IDR',
                'amount' => '1',
                'payment_datetime' => 'payment_datetime',
                'payment_ref' => 'payment_ref',
                'debit_from_bank' => 'debit_from_bank',
                'credit_to_bank' => 'credit_to_bank',
                'signature' => 'd9bcd4e6669be85223e94187907a843fdf01b0253aaefc10a0fa3f3065bb3aa1',
            ]);

        $response = $this->espayPaymentGateway->handle($request);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame('2,Invalid amount,,,', $response->getBody()->getContents());
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Unknown handler type
     */
    public function testHandleUnknownType()
    {
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this
            ->getMockBuilder(ServerRequestInterface::class)
            ->getMockForAbstractClass();

        $this->espayPaymentGateway->handle($request);
    }
}
