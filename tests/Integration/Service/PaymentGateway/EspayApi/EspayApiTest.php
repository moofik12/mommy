<?php

namespace MommyCom\Test\Integration\Service\PaymentGateway\EspayApi;

use GuzzleHttp\Client;
use MommyCom\Service\PaymentGateway\EspayApi\EspayApi;
use MommyCom\Service\PaymentGateway\EspayApi\Provider;
use MommyCom\Test\Integration\WebTestCase;
use Symfony\Component\Cache\Simple\NullCache;

class EspayApiTest extends WebTestCase
{
    /**
     * @var EspayApi
     */
    private $api;

    protected function setUp(): void
    {
        static::bootKernel();

        $client = new Client(['base_uri' => 'https://sandbox-api.espay.id/rest/']);
        $this->api = new EspayApi(
            new NullCache(),
            $client,
            'https://sandbox-kit.espay.id/',
            '65ebeb3286bd3f0f860fcbe5adca9be4'
        );
    }

    public function testGetProviders(): void
    {
        $providers = $this->api->getProviders();

        $this->assertContainsOnlyInstancesOf(Provider::class, $providers);
        $this->assertGreaterThan(0, count($providers));
    }
}
