<?php

namespace MommyCom\Test\Integration;

use MommyCom\YiiComponent\Application\MommyWebApplication;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

abstract class WebTestCase extends BaseWebTestCase
{
    /**
     * {@inheritdoc}
     */
    protected static function createKernel(array $options = [])
    {
        if (null === static::$class) {
            static::$class = static::getKernelClass();
        }

        if (isset($options['appName'])) {
            $app = $options['appName'];
        } elseif (isset($_ENV['APP_NAME'])) {
            $app = $_ENV['APP_NAME'];
        } elseif (isset($_SERVER['APP_NAME'])) {
            $app = $_SERVER['APP_NAME'];
        } else {
            $app = 'test';
        }

        if (isset($options['environment'])) {
            $env = $options['environment'];
        } elseif (isset($_ENV['APP_ENV'])) {
            $env = $_ENV['APP_ENV'];
        } elseif (isset($_SERVER['APP_ENV'])) {
            $env = $_SERVER['APP_ENV'];
        } else {
            $env = 'test';
        }

        if (isset($options['debug'])) {
            $debug = $options['debug'];
        } elseif (isset($_ENV['APP_DEBUG'])) {
            $debug = $_ENV['APP_DEBUG'];
        } elseif (isset($_SERVER['APP_DEBUG'])) {
            $debug = $_SERVER['APP_DEBUG'];
        } else {
            $debug = true;
        }

        return new static::$class($app, $env, $debug);
    }

    /**
     * {@inheritdoc}
     */
    protected static function bootKernel(array $options = [])
    {
        $kernel = parent::bootKernel($options);

        $kernel->getContainer()->get(MommyWebApplication::class);

        return $kernel;
    }
}
