<?php

use MommyCom\Model\Db\UserRecord;

/**
 * @var UserRecord $model
 * @var $this CController
 */

$model = isset($data['model']) ? $data['model'] : new UserRecord();
$password = isset($data['password']) ? $data['password'] : '';
$this->pageTitle = $model->fullname . ', ' . Yii::t('email', 'Thank you for registering');
$baseUrl = Yii::app()->getBaseUrl(true) . '/';
$frontendUrl = Yii::app()->getComponent('frontendUrlManager')
    ? Yii::app()->frontendUrlManager->createAbsoluteUrl('index/index') : Yii::app()->createAbsoluteUrl('index/index');
?>


<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:100%;">
    <tr>
        <td>
            <table align="center" cellpadding="0" cellspacing="0"
                   style="border-collapse:collapse;border:0;font-family:arial;width:679px;">
                <tr>
                    <td>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url('<?= $baseUrl . '/static/mailing/notification' ?>/header.jpg');width:100%;">
                            <tr>
                                <td valign="middle" style="padding-left:15px;">
                                    <a href="<?= $frontendUrl ?>"
                                       style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                        <img src="<?= $baseUrl . '/static/mailing/notification-second' ?>/logo-mommy.png"
                                             width="278"
                                             alt="<?= Yii::t('common', 'MOMMY.COM - shopping club') ?>"
                                             title="<?= Yii::t('common', 'Shopping club for children and moms mommy.com') ?>">
                                    </a>
                                </td>
                                <td align="right" valign="middle" style="padding-right:15px;">
                                    <a href="#"
                                       style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;"><?= Yii::t('common', 'AVAILABLE CARE') ?> <?= Yii::t('common', 'FOR YOUR FAMILY') ?></a>
                                </td>
                            </tr>
                        </table>
                        <table align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;border-left:1px solid #e9e9e9;border-right:1px solid #e9e9e9;width:100%;">
                            <tr>
                                <td valign="top" style="padding-top:15px;padding-bottom:20px;">
                                    <table align="center" cellpadding="0" cellspacing="0"
                                           style="border-collapse:collapse;border:0;width:667px;">
                                        <tr>
                                            <td>

                                                <!-- content -->

                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= Yii::t('email', 'Thank you for registering on') ?> <?= CHtml::link(Yii::app()->name, $frontendUrl, ['target' => '_blank']) ?>
                                                </p>

                                                <br>

                                                <p style="font-size:16px;color:#3e3e3e"><?= Yii::t('email', 'Your login') ?> <?= $model->email ?></p>

                                                <p style="font-size:16px;color:#3e3e3e;font-weight:bold"><?= Yii::t('email', 'To complete registration, click on the link') ?>
                                                    : </p>

                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= CHtml::link(Yii::app()->createAbsoluteUrl('auth/finishRegistration'
                                                        , [
                                                            'h' => $model->generateHashEmailToVerify(),
                                                            'uid' => $model->id,
                                                        ]
                                                    ), Yii::app()->createAbsoluteUrl('auth/finishRegistration'
                                                        , [
                                                            'h' => $model->generateHashEmailToVerify(),
                                                            'uid' => $model->id]
                                                    ), ['target' => '_blank']) ?>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= Yii::t('email', 'Confirmation is required to prevent unauthorized use of your e-mail address. For confirmation, just click on the link, you do not need to send additional emails.') ?>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= Yii::t('email', 'If the above link does not open, copy it to the clipboard, paste it into the address bar of the browser and press enter.') ?>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= Yii::t('email', 'Sincerely, shopping club') ?> <?= CHtml::link(Yii::app()->name, $frontendUrl, ['target' => '_blank']) ?>
                                                </p>

                                                <!-- content -->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url(<?= $baseUrl . '/static/mailing/notification' ?>/footer.jpg);width:100%;">
                            <tr>
                                <td width="170" align="left" valign="middle" style="padding-left:10px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
