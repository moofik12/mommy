<?php

$translator = $data['translator'];
$supplierLink = $data['supplierLink'];
$baseUrl = $data['baseUrl'];
$frontendUrl = $data['frontendUrl'];

?>

<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:100%;">
    <tr>
        <td>
            <table align="center" cellpadding="0" cellspacing="0"
                   style="border-collapse:collapse;border:0;font-family:arial;width:679px;">
                <tr>
                    <td>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url('<?= $baseUrl . '/static/mailing/notification' ?>/header.jpg');width:100%;">
                            <tr>
                                <td valign="middle" style="padding-left:15px;">
                                    <a href="<?= $frontendUrl ?>"
                                       style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                        <img src="<?= $baseUrl . '/static/mailing/notification-second' ?>/logo-mommy.png"
                                             width="278"
                                             alt="<?= $translator->t('MOMMY.COM - shopping club') ?>"
                                             title="<?= $translator->t('Shopping club for children and moms mommy.com') ?>">
                                    </a>
                                </td>
                                <td align="right" valign="middle" style="padding-right:15px;">
                                    <a href="#"
                                       style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;"><?= $translator->t('AVAILABLE CARE') ?> <?= $translator->t('FOR YOUR FAMILY') ?></a>
                                </td>
                            </tr>
                        </table>
                        <table align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;border-left:1px solid #e9e9e9;border-right:1px solid #e9e9e9;width:100%;">
                            <tr>
                                <td valign="top" style="padding-top:15px;padding-bottom:20px;">
                                    <table align="center" cellpadding="0" cellspacing="0"
                                           style="border-collapse:collapse;border:0;width:667px;">
                                        <tr>
                                            <td>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator->t('Please confirm the order: ') ?>,
                                                    <span style="font-weight:bold"><?= $supplierLink ?></span></p>
                                                <br>
                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Sincerely') ?>
                                                    ,</p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Administration of the shopping club "MOMMY"') ?></p>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url(<?= $baseUrl . '/static/mailing/notification' ?>/footer.jpg);width:100%;">
                            <tr>
                                <td width="170" align="left" valign="middle" style="padding-left:10px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
