<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Translator\RegionalTranslator;
use MommyCom\YiiComponent\BuildUrlManager;

/* @var $order OrderRecord */
/* @var $frontendUrlManager BuildUrlManager */
/* @var RegionalTranslator $translator */
/* @var $data array */

$translator = $data['translator'];
$order = $data['order'];
$frontendUrlManager = $data['frontendUrlManager'];
$user = $order->user;
$baseUrl = Yii::app()->getBaseUrl(true);
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$uid = count($order->getRelationOrders()) ? $order->uuid : $order->id;
$token = $order->payment_token;

$frontendUrl = Yii::app()->getComponent('frontendUrlManager')
    ? Yii::app()->frontendUrlManager->createAbsoluteUrl('index/index')
    : Yii::app()->createAbsoluteUrl('index/index');

$linkToPay = Yii::app()->getComponent('frontendUrlManager')
    ? Yii::app()->frontendUrlManager->createAbsoluteUrl('pay/index', ['uid' => $uid, 'token'=>$token])
    : Yii::app()->createAbsoluteUrl('pay/index', ['uid' => $uid, 'token'=>$token]);


?>

<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:100%;">
    <tr>
        <td>
            <table align="center" cellpadding="0" cellspacing="0"
                   style="border-collapse:collapse;border:0;font-family:arial;width:679px;">
                <tr>
                    <td>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url('<?= $baseUrl . '/static/mailing/notification' ?>/header.jpg');width:100%;">
                            <tr>
                                <td valign="middle" style="padding-left:15px;">
                                    <a href="<?= $frontendUrl ?>"
                                       style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                        <img src="<?= $baseUrl . '/static/mailing/notification-second' ?>/logo-mommy.png"
                                             width="278"
                                             alt="<?= $translator->t('MOMMY.COM - shopping club') ?>"
                                             title="<?= $translator->t('Shopping club for children and moms mommy.com') ?>">
                                    </a>
                                </td>
                                <td align="right" valign="middle" style="padding-right:15px;">
                                    <a href="#"
                                       style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;"><?= $translator->t('AVAILABLE CARE') ?> <?= $translator->t('FOR YOUR FAMILY') ?></a>
                                </td>
                            </tr>
                        </table>
                        <table align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;border-left:1px solid #e9e9e9;border-right:1px solid #e9e9e9;width:100%;">
                            <tr>
                                <td valign="top" style="padding-top:15px;padding-bottom:20px;">
                                    <table align="center" cellpadding="0" cellspacing="0"
                                           style="border-collapse:collapse;border:0;width:667px;">
                                        <tr>
                                            <td>

                                                <!-- content -->

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Hello') ?>,
                                                    <span style="font-weight:bold"><?= $user->fullname ?></span></p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Our manager will contact you via phone shortly to confirm the order') ?> </p>
                                                <br>
                                                <p style="font-size:16px;color:#3e3e3e;font-weight:bold"><?= $translator->t('Please, check the details of your order') ?>:</p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Order code') ?>: <span style="font-weight:bold">№<?= $order->id ?></span></p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('The cost of goods (without the shipping сost)') ?>:
                                                    <span style="font-weight:bold"><?= $cf->format($order->getPayPrice(), $cn->getName('short')) ?></span>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Shipping сost') ?>
                                                    :
                                                    <span style="font-weight:bold"><?= $cf->format($order->getDeliveryPrice(), $cn->getName('short')) ?></span>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Total:') ?>
                                                    <span style="font-weight:bold"><?= $cf->format($order->getPayPrice() + $order->getDeliveryPrice(), $cn->getName('short')) ?></span>
                                                </p>
                                                <br>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('You can proceed to payment using this link: ') ?><a href="<?= $linkToPay ?>"><?= $linkToPay ?></a></p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Sincerely') ?>
                                                    ,</p>

                                                <p style="font-size:16px;color:#3e3e3e"><?= $translator->t('Administration of the shopping club "MOMMY"') ?></p>

                                                <!-- content -->

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url(<?= $baseUrl . '/static/mailing/notification' ?>/footer.jpg);width:100%;">
                            <tr>
                                <td width="170" align="left" valign="middle" style="padding-left:10px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
