<?php

use MommyCom\Service\Translator\RegionalTranslator;
use MommyCom\Entity\User;

/* @var User $user
/* @var RegionalTranslator $translator */
/* @var $data array */

$translator = $data['translator'];
$user = $data['user'];
$link = $data['link'];
$baseUrl = $data['baseUrl'];
$frontendUrl = $data['frontendUrl'];
?>
<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:100%;">
    <tr>
        <td>
            <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;font-family:arial;width:679px;">
                <tr>
                    <td>
                        <table height="99" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url('<?= $baseUrl . '/static/mailing/notification' ?>/header.jpg');width:100%;">
                            <tr>
                                <td valign="middle" style="padding-left:15px;">
                                    <a href="<?= $frontendUrl ?>" style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                        <img src="<?= $baseUrl . '/static/mailing/notification-second' ?>/logo-mommy.png" width="278" alt="<?= $translator( 'MOMMY.COM - shopping club') ?>" title="<?= $translator('Shopping club for children and moms mommy.com') ?>">
                                    </a>
                                </td>
                                <td align="right" valign="middle" style="padding-right:15px;">
                                    <a href="#" style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;"><?= $translator('AVAILABLE CARE') ?> <?= $translator('FOR YOUR FAMILY') ?></a>
                                </td>
                            </tr>
                        </table>
                        <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;border-left:1px solid #e9e9e9;border-right:1px solid #e9e9e9;width:100%;">
                            <tr>
                                <td valign="top" style="padding-top:15px;padding-bottom:20px;">
                                    <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:667px;">
                                        <tr>
                                            <td>
                                                <h1 style="text-align: center"><?= $translator('SHIPPED') ?></h1>
                                                <h2 style="text-align: center"><?= $translator('YOUR PACKAGE IS ON ITS WAY') ?></h2>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('Hello') ?>, <?= $user->getName() ?>
                                                </p>
                                                <br>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('We are happy to tell you that your order is on its way and will soon arrive to your place.  If you want to track it, click the link below') ?><br/><br/>
                                                    <a style="border: 1px solid black; padding: 8px 12px; background: #4479BA; color: #FFF" href="<?= $link ?>">
                                                        <?= $translator('Track your order') ?>
                                                    </a>
                                                </p>
                                                <br/>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('Or click here and check out novelties on mommy.com while they are still in stock!') ?>
                                                </p>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('If you have any questions feel free to ask;)') ?>
                                                </p>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('Our  WA') ?>
                                                </p>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('Our phone') ?>
                                                </p>
                                                <p style="font-size:16px;color:#3e3e3e">
                                                    <?= $translator('Delivery policy') ?><br/>
                                                    <?= $translator('Please, click here to learn about our delivery policy') ?>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table height="99" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url(<?= $baseUrl . '/static/mailing/notification' ?>/footer.jpg);width:100%;">
                            <tr>
                                <td width="170" align="left" valign="middle" style="padding-left:10px;">
                                    <?= $translator('Yours Mommy.com customers service') ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
