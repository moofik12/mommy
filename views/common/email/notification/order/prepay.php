<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Translator\RegionalTranslator;
use MommyCom\YiiComponent\BuildUrlManager;

/* @var $order OrderRecord */
/* @var $frontendUrlManager BuildUrlManager */
/* @var RegionalTranslator $translator */
/* @var $data array */

$translator = $data['translator'];
$order = $data['order'];
$frontendUrlManager = $data['frontendUrlManager'];
$user = $order->user;
$baseUrl = Yii::app()->getBaseUrl(true);
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
?>

<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:100%;">
    <tr>
        <td>
            <table align="center" cellpadding="0" cellspacing="0"
                   style="border-collapse:collapse;border:0;font-family:arial;width:679px;">
                <tr>
                    <td>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url('<?= $baseUrl . '/static/mailing/notification' ?>/header.jpg');width:100%;">
                            <tr>
                                <td valign="middle" style="padding-left:15px;">
                                    <a href="<?= $frontendUrlManager->createAbsoluteUrl('index/index') ?>"
                                       style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                        <img src="<?= $baseUrl . '/static/mailing/notification' ?>/logo.png" width="278"
                                             height="54" alt="Шоппинг–клуб MOMMY.COM"
                                             title="Шоппинг–клуб для детей и мам MOMMY.COM">
                                    </a>
                                </td>
                                <td align="right" valign="middle" style="padding-right:15px;">
                                    <a href="#"
                                       style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;">ПЕРВЫЙ
                                        В УКРАИНЕ ШОППИНГ-КЛУБ<br>ДЛЯ МАМ И ДЕТЕЙ</a>
                                </td>
                            </tr>
                        </table>
                        <table align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;border-left:1px solid #e9e9e9;border-right:1px solid #e9e9e9;width:100%;">
                            <tr>
                                <td valign="top" style="padding-top:15px;padding-bottom:20px;">
                                    <table align="center" cellpadding="0" cellspacing="0"
                                           style="border-collapse:collapse;border:0;width:667px;">
                                        <tr>
                                            <td>

                                                <!-- content -->

                                                <p style="font-size:16px;color:#3e3e3e">Здравствуйте, <span
                                                            style="font-weight:bold"><?= $user->fullname ?></span></p>

                                                <p style="font-size:16px;color:#3e3e3e">Некоторое время назад Вы
                                                    разместили заказ <span
                                                            style="font-weight:bold">№<?= $order->id ?></span> в
                                                    шоппинг-клубе "MOMMY",<br>
                                                    <a style="color:#ff203b;"
                                                       href="<?= $frontendUrlManager->createAbsoluteUrl('index/index') ?>"><?= $frontendUrlManager->createAbsoluteUrl('index/index') ?></a>
                                                </p>
                                                <br>
                                                <p style="font-size:16px;color:#3e3e3e">Заказ в настоящее время ожидает
                                                    оплаты.<br>
                                                    <span style="font-weight:bold">Для того, чтобы перейти к оплате немедленно, пожалуйста, нажмите на ссылку ниже:</span>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e"><a style="color:#ff203b;"
                                                                                           href="<?= $frontendUrlManager->createAbsoluteUrl('pay/index', ['id' => $order->id]) ?>"><?= $frontendUrlManager->createAbsoluteUrl('pay/index', ['id' => $order->id]) ?></a>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e">На этой странице Вы найдете все
                                                    необходимые инструкции для того, чтобы оплатить заказ удобным для
                                                    Вас способом.</p>

                                                <p style="font-size:16px;color:#3e3e3e;font-weight:bold">Пожалуйста,
                                                    проверьте данные Вашего заказа:</p>

                                                <p style="font-size:16px;color:#3e3e3e">Код заказа: <span
                                                            style="font-weight:bold">№<?= $order->id ?></span></p>

                                                <p style="font-size:16px;color:#3e3e3e">Стоимость товаров (без стоимости
                                                    доставки): <span
                                                            style="font-weight:bold"><?= $cf->format($order->getPayPrice(), $cn->getName('short')) ?></span>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e">Стоимость доставки: <span
                                                            style="font-weight:bold"><?= $cf->format($order->deliveryPrice, $cn->getName('short')) ?></span>
                                                </p>

                                                <p style="font-size:16px;color:#3e3e3e">ИТОГО К ОПЛАТЕ: <span
                                                            style="font-weight:bold"><?= $cf->format($order->getPayPrice() + $order->getDeliveryPrice(), $cn->getName('short')) ?></span>
                                                </p>
                                                <br>
                                                <p style="font-size:16px;color:#3e3e3e">Благодарим Вас за размещение
                                                    заказа в шоппинг-клубе "MOMMY".</p>

                                                <p style="font-size:16px;color:#3e3e3e">С уважением,</p>

                                                <p style="font-size:16px;color:#3e3e3e">Администрация шоппинг-клуба
                                                    "MOMMY"</p>

                                                <!-- content -->

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table height="99" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse;border:0;background-color:#53a28f;background-image:url(<?= $baseUrl . '/static/mailing/notification' ?>/footer.jpg);width:100%;">
                            <tr>
                                <td width="170" align="left" valign="middle" style="padding-left:10px;">
                                    <a href="<?= $frontendUrlManager->createAbsoluteUrl('index/index') ?>"
                                       style="color:#fff;font-size:12px;font-weight:normal;text-align:left;display:block;">
                                        <img src="<?= $baseUrl . '/static/mailing/notification' ?>/footer-img.png"
                                             width="167" height="89" alt="Шоппинг–клуб MOMMY.COM"
                                             title="Шоппинг–клуб для детей и мам MOMMY.COM">
                                    </a>
                                </td>
                                <td align="left" valign="middle" style="padding-left:60px;">
                                    <span style="color:#fff;font-size:36px;font-weight:bold;text-transform:uppercase;display:block;"><?= Yii::app()->params['phone'] ?></span>
                                </td>
                                <td width="39" align="right" valign="middle" style="padding-right:15px;">
                                    <table height="99" align="center" cellpadding="0" cellspacing="0"
                                           style="border-collapse:collapse;border:0;width:100%;">
                                        <tr>
                                            <td height="38" valign="middle">
                                                <a href="<?= Yii::app()->params['groupOd'] ?>"
                                                   style="color:#fff;font-size:8px;font-weight:normal;text-align:left;display:block;">
                                                    <img src="<?= $baseUrl . '/static/mailing/notification' ?>/footer-od.png"
                                                         width="38" height="38" alt="ОД" title="Одноклассики"
                                                         style="display:block;">
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="38" valign="middle">
                                                <a href="<?= Yii::app()->params['groupVk'] ?>"
                                                   style="color:#fff;font-size:8px;font-weight:normal;text-align:left;display:block;">
                                                    <img src="<?= $baseUrl . '/static/mailing/notification' ?>/footer-vk.png"
                                                         width="39" height="38" alt="ВК" title="Вконтакте"
                                                         style="display:block;">
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
