<?php

use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var ShopShoppingCart $model
 * @var CController $this
 * @var string $title
 * @var PromocodeRecord|null $promo
 * @var string $promocode
 * @var float $promocodeSavings
 * @var float $cost
 * @var float $costWithPromocode
 */

if (!$model instanceof ShopShoppingCart) {
    throw new CException('Ошибка передачи модели, ожидается ShopShoppingCart');
}

$user = $model->getUser();
$baseUrl = Yii::app()->getBaseUrl(true) . '/';
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$frontendUrl = Yii::app()->getComponent('frontendUrlManager')
    ? Yii::app()->frontendUrlManager->createAbsoluteUrl('index/index') : Yii::app()->createAbsoluteUrl('index/index');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?= CHtml::encode($title); ?></title>
</head>
<body style="margin: 0;">

<table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border: 0px; width: 100%;">
    <tbody>
    <tr>
        <td style="padding: 0 15px;">
            <table align="center" cellpadding="0" cellspacing="0"
                   style="border-collapse: collapse; text-align: left; font-family: Arial, Helvetica, sans-serif; font-weight: normal; font-size: 12px; line-height: 15pt; color: #ffffff; width: 628px;">
                <tbody>
                <!-- header -->
                <tr>
                    <td align="center" colspan="2" bgcolor="#ff798c"
                        style="font-family: arial; font-size: 11px; line-height: 16pt; color: #ffffff;">
        <span style="font-size: 11px; font-family: arial; color: #ffffff;">
            <a href="mailto:
            <?= Yii::app()->params['distributionEmail'] ?>" style="text-decoration: none; color: #ffffff;"
               target="_blank"> <?= Yii::t('email', 'In order not to miss any action from MOMMY, add {mail} to the address book', [
                    '{mail}' => '<strong>' . Yii::app()->params['distributionEmail'] . '</strong>',
                ]) ?> </a>
        </span>
                    </td>
                </tr>
                <!-- header -->
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style=" width: 100%;">
                            <tbody>
                            <tr>
                                <td colspan="3" width="100%" height="15" valign="top"
                                    style="border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="100%"
                                             height="15" style="display:block;" border="0">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="15" height="15" valign="top"
                                    style="border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="15"
                                             height="15" style="display:block;" border="0">
                                    </div>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" style="width: 596px;">
                                        <!-- banner -->
                                        <tr>
                                            <td width="596" height="80" valign="top" style="background:#ff7591;"
                                                bgcolor="#ff7591">
                                                <table height="99" align="center" cellpadding="0" cellspacing="0"
                                                       style="border-collapse:collapse;border:0;background-color:#ff7591;background-image:url('<?= $baseUrl . '/static/mailing/notification-second' ?>/head-bg.jpg');width:100%;">
                                                    <tr>
                                                        <td valign="middle" style="padding-left:15px;">
                                                            <a href="<?= $frontendUrl ?>"
                                                               style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                                                <img src="<?= $baseUrl . '/static/mailing/notification-second' ?>/logo-mommy.png"
                                                                     width="278"
                                                                     alt="<?= Yii::t('common', 'MOMMY.COM - shopping club') ?>"
                                                                     title="<?= Yii::t('common', 'Shopping club for children and moms mommy.com') ?>">
                                                            </a>
                                                        </td>
                                                        <td align="right" valign="middle" style="padding-right:15px;">
                                                            <a href="#"
                                                               style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;"><?= Yii::t('common', 'AVAILABLE CARE') ?> <?= Yii::t('common', 'FOR YOUR FAMILY') ?></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- banner -->

                                        <!-- content -->
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="width: 596px;">
                                                    <tr>
                                                        <td align="center" style="padding:10px 5px 0;">
                                                            <span style="font-weight:normal;font-size:14px;line-height:25px;color:#707070;"><?= Yii::t('email', 'Hello') ?>
                                                                , <?= $user->getFullname() ?>!</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:0 5px 15px;">
                                                            <span style="font-weight:bold;font-size:14px;line-height:25px;color:#707070;"><?= Yii::t('email', 'The product you have selected is still in stock, hurry up to order it!') ?></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="0" cellspacing="0" style="width: 596px;">
                                                    <tr>
                                                        <td style="padding:5px;border-top:1px solid #f3f3f3;border-bottom:1px solid #f3f3f3;font-size:12px;font-weight:bold;color:#707070;"><?= Yii::t('common', 'Product') ?></td>
                                                        <td width="328"
                                                            style="padding:5px;border-top:1px solid #f3f3f3;border-bottom:1px solid #f3f3f3;font-size:12px;font-weight:bold;color:#707070;"><?= Yii::t('common', 'Quantity') ?></td>
                                                        <td width="164"
                                                            style="padding:5px;border-top:1px solid #f3f3f3;border-bottom:1px solid #f3f3f3;font-size:12px;font-weight:bold;color:#707070;"><?= Yii::t('common', 'Quantity and cost') ?></td>
                                                    </tr>
                                                    <?php
                                                    foreach ($model->getPositions() as $position):
                                                        /** @var $cart CartRecord */
                                                        ?>
                                                        <tr>
                                                            <td width="72" height="86"
                                                                style="padding:5px 0 5px;border-bottom:1px solid #f3f3f3;">
                                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'newsletter', 'utm_medium' => 'cart', 'utm_campaign' => 'cart'])) ?>"
                                                                   style="display: block;border:1px solid #f5f5f5;"
                                                                   target="_blank">
                                                                    <img src="<?= $position->product->product->getLogo()->getThumbnail('small70')->url ?>"
                                                                         width="72" height="86"
                                                                         alt="<?= $position->product->product->name ?>"
                                                                         style="display:block;" border="0"></a>
                                                            </td>
                                                            <td style="padding:5px;border-bottom:1px solid #f3f3f3;">
                                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'newsletter', 'utm_medium' => 'cart', 'utm_campaign' => 'cart'])) ?>"
                                                                   style="display: block;text-decoration:none;"
                                                                   target="_blank">
                                                                    <span style="font-size:15px;font-weight:bold;text-decoration:none;color:#ff7b8e;"><?= $position->product->product->name ?></span><br>
                                                                    <span style="font-size:14px;text-decoration:none;color:#3e3e3e;"><?= $position->event->name ?></span>
                                                                </a>
                                                            </td>
                                                            <td style="padding:5px;border-bottom:1px solid #f3f3f3;">
                                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'newsletter', 'utm_medium' => 'cart', 'utm_campaign' => 'cart'])) ?>"
                                                                   style="display: block;text-decoration:none;"
                                                                   target="_blank">
                                <span style="font-size:14px;color:#ff6178;">
                                    <span style="font-weight:bold;"><?= $cf->format($position->price, $cn->getName('short')) ?></span>
                                    <span style="color:#a5a5a5;">x</span>
                                    <span style="font-weight:bold;color:#3e3e3e;"><?= $position->number ?></span>
                                    <span style="color:#a5a5a5;">=</span>
                                    <span style="font-weight:bold;"><?= $cf->format($position->totalPrice, $cn->getName('short')) ?></span>
                                </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td rowspan="2" valign="middle" width="202"
                                                            style="padding:10px 0px 5px;font-weight:bold;font-size:14px;line-height:25px;border-bottom:1px solid #f3f3f3;color:#3e3e3e;"><?= Yii::t('email', ' {n} good for purchase | {n} goods for purchase |{n} goods for purchase ', $model->getCount()) ?></td>
                                                        <?php if ($promocodeSavings > 0): ?>
                                                            <td align="right" valign="middle"
                                                                style="padding:5px 5px 0px;font-weight:bold;line-height:25px;font-size:16px;color:#3e3e3e;"><?= Yii::t('common', 'A discount') ?>
                                                                :
                                                                <span style="font-size:24px;color:#ff6178;"><?= $cf->format($promocodeSavings, $cn->getName('short')) ?></span>
                                                            </td>
                                                        <?php endif ?>
                                                    </tr>
                                                    <tr>
                                                        <?php if ($promocodeSavings > 0): ?>
                                                            <td align="right" valign="middle"
                                                                style="padding:0px 5px 5px;font-weight:bold;line-height:25px;font-size:16px;border-bottom:1px solid #f3f3f3;color:#3e3e3e;"><?= Yii::t('common', 'Total, including discounts:') ?>
                                                                <span style="font-size:24px;color:#ff6178;"><?= $cf->format($costWithPromocode, $cn->getName('short')) ?></span>
                                                            </td>
                                                        <?php else: ?>
                                                            <td align="right" valign="middle"
                                                                style="padding:0px 5px 5px;font-weight:bold;line-height:25px;font-size:16px;border-bottom:1px solid #f3f3f3;color:#3e3e3e;"><?= Yii::t('common', 'Total:') ?>
                                                                <span style="font-size:24px;color:#ff6178;"><?= $cf->format($cost, $cn->getName('short')) ?></span>
                                                            </td>
                                                        <?php endif ?>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- content -->
                                    </table>
                                </td>
                                <td width="15" height="15" valign="top"
                                    style="border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="15"
                                             height="15" style="display:block;" border="0">
                                    </div>
                                </td>
                            </tr>
                            <?php if ($promocode != ''): ?>
                                <tr>
                                    <td colspan="3" width="100%" height="70" valign="top"
                                        style="padding:0;border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff;">
                                        <table style="border-collapse:collapse" height="25" border="0" cellpadding="0"
                                               cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="background: #ffffff;" height="35" align="left"
                                                    bgcolor="#ffffff" valign="middle" width="25%">
                                                    <img style="display:block" alt="">
                                                </td>
                                                <td height="25"
                                                    style="width:50%;vertical-align:bottom;font-family:Arial,Helvetica,sans-serif;font-size:19px;color:#010101;text-align:center">
                                                    Ваш промокод для скидки
                                                </td>
                                                <td style="background: #ffffff;" align="right" bgcolor="#ffffff"
                                                    valign="middle" width="25%">
                                                    <img style="display:block" alt="">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table style="border-collapse:collapse" height="55" border="0" cellpadding="0"
                                               cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td style="background: #ffffff;" height="55" align="left"
                                                    bgcolor="#ffffff" valign="middle" width="33%">
                                                    <img style="display:block" alt="">
                                                </td>
                                                <td height="70"
                                                    style="font-family:Arial;font-size:25px;color:#f00;text-align:center">
                                                    <div style="padding:10px;border:#f00 2px dashed;border-radius:7px;width:320px;margin:0 auto">
                                                        <strong><?= $promocode ?></strong>
                                                    </div>
                                                </td>
                                                <td style="background: #ffffff;" align="right" bgcolor="#ffffff"
                                                    valign="middle" width="33%">
                                                    <img style="display:block" alt="">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td colspan="3" width="100%" height="70" valign="top"
                                    style="padding:15px 0;border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff;">
                                    <table style="border-collapse:collapse" height="55" border="0" cellpadding="0"
                                           cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="background: #ffffff;" height="55" align="left" bgcolor="#ffffff"
                                                valign="middle" width="33%">
                                                <img style="display:block" alt="">
                                            </td>
                                            <td style="background:#53a28f" align="center" height="55" bgcolor="#53a28f"
                                                valign="middle" width="">
                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'newsletter', 'utm_medium' => 'cart', 'utm_campaign' => 'cart'])) ?>"
                                                   style="display:block;text-decoration:none;color:#ffffff!important;line-height: 55px;"
                                                   target="_blank">
                                                    <span style="font-family:sans-serif;font-size:16px;color:#ffffff;text-decoration:none"><b><?= Yii::t('common', 'Checkout') ?></b></span>
                                                </a>
                                            </td>
                                            <td style="background: #ffffff;" align="right" bgcolor="#ffffff"
                                                valign="middle" width="33%">
                                                <img style="display:block" alt="">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" width="100%" height="10" valign="top"
                                    style="border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="100%"
                                             height="10" style="display:block;" border="0">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="15" height="15" valign="top"
                                    style="border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="15"
                                             height="15" style="display:block;" border="0">
                                    </div>
                                </td>
                                <td style="padding-top: 15px; padding-bottom: 15px; padding-left: 15px; padding-right: 15px; background-color: #f3f3f3;">
                                </td>
                                <td width="15" height="15" valign="top"
                                    style="border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="15"
                                             height="15" style="display:block;" border="0">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" width="100%" height="15" valign="top"
                                    style="border-right-color: #ff798c; border-right-style: solid; border-right-width: 1px; border-left-color: #ff798c; border-left-style: solid; border-left-width: 1px; background: #ffffff; padding: 0;">
                                    <div style="line-height: 0;">
                                        <img src="<?= $baseUrl . 'static/mailing/cart/blank.gif' ?>" width="100%"
                                             height="15" style="display:block;" border="0">
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <!-- footer -->
                <tr>
                    <td align="center" colspan="2" bgcolor="#ff798c"
                        style="font-family: arial; font-size: 12px; line-height: 16pt; padding: 10px 0; color: #ffffff;">
       <span style="font-size: 12px; font-family: arial; color: #ffffff;">
            <?= Yii::t('email', 'Copyright') ?>
           &copy; MOMMY.COM<br/> <?= Yii::t('email', 'You received this newsletter on') ?>&nbsp; <a
                   href="mailto:<?= $user->email ?>"
                   target="_blank"><?= $user->email ?></a>, <?= Yii::t('email', 'because you are a member of the Mommy Club.') ?>
       </span>
                    </td>
                </tr>
                <!-- footer -->
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
