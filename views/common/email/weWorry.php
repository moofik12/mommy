<?php

use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\Utf8;

/**
 * @var $this CController
 * @var $user UserRecord
 */

$baseUrl = Yii::app()->getBaseUrl(true) . '/';
$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/we_worry/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$marker = 'we_worry' . date('d.m.Y');
$utmParams = ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-info', 'utm_content' => $marker];
$mainUrl = $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams))]);
$frontendUrl = Yii::app()->getComponent('frontendUrlManager')
    ? Yii::app()->frontendUrlManager->createAbsoluteUrl('index/index') : Yii::app()->createAbsoluteUrl('index/index');
?>
<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl ?>bg.png');">
    <tbody>
    <tr>
        <td align="center">
            <div style="width:753px;padding-bottom:20px;">
                <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td width="596" height="80" valign="top" style="background:#ff7591;" bgcolor="#ff7591">
                            <table height="99" align="center" cellpadding="0" cellspacing="0"
                                   style="border-collapse:collapse;border:0;background-color:#ff7591;background-image:url('<?= $baseUrl . '/static/mailing/notification-second' ?>/head-bg.jpg');width:100%;">
                                <tr>
                                    <td valign="middle" style="padding-left:15px;">
                                        <a href="<?= $frontendUrl ?>"
                                           style="color:#fff;font-size:16px;font-weight:normal;text-align:left;display:block;">
                                            <img src="<?= $baseUrl . '/static/mailing/notification-second' ?>/logo-mommy.png"
                                                 width="278"
                                                 alt="<?= Yii::t('common', 'MOMMY.COM - shopping club') ?>"
                                                 title="<?= Yii::t('common', 'Shopping club for children and moms mommy.com') ?>">
                                        </a>
                                    </td>
                                    <td align="right" valign="middle" style="padding-right:15px;">
                                        <a href="#"
                                           style="color:#fff;font-size:18px;text-shadow: 0 1px #bbdcd3;text-transform:uppercase;text-decoration:none;display:block;"><?= Yii::t('common', 'AVAILABLE CARE') ?> <?= Yii::t('common', 'FOR YOUR FAMILY') ?></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- header -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl ?>top-menu-bg.png');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-male.png"
                                                 width="21" height="24" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-female.png"
                                                 width="23" height="24" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-books.png"
                                                 width="20" height="22" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-parents.png"
                                                 width="45" height="23" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-household.png"
                                                 width="21" height="21" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div> <!-- menu --> <!-- content wrapper -->
                <div style="color: #505050; font-family: sans-serif;padding: 10px 10px 0 10px; border-right-color: rgb(225, 103, 131); border-right-style: solid; border-right-width: 1px; font-size: 16px; border-left-color: rgb(225, 103, 131); border-left-style: solid; border-left-width: 1px; background-color: rgb(255, 255, 255);">
                    <h1 style="font-weight: normal; text-align: center; font-size: 24px; line-height: 1.3; padding:5px 0 5px; margin: 0;"><?= Yii::t('email', 'Dear user!') ?></h1>
                    <div style="margin-top: 5px; line-height: 1.3; text-align:left; font-size: 16px; padding-top: 5px;"><?= Yii::t('email', 'You have not visited us so long, we missed you and decided to please you with stunning actions.') ?></div>
                    <div style="line-height: 1.3; text-align:left; font-size: 16px; padding-top: 8px;"><?= Yii::t('email', 'In the shopping club Mommy.com you will find a wide selection of practical and stylish clothes, shoes, books and accessories for the home', [
                            '{href}' => $mainUrl,
                        ]) ?>.
                    </div>
                    <div style="line-height: 1.3; text-align:left; font-size: 16px; padding-top: 8px;">
                        <a href="<?= $mainUrl ?>"
                           style="color:#ff839b;font-weight:bold;"><?= Yii::t('email', 'Watch for promotions and enjoy shopping with Mommy.com!') ?></a> </a>
                    </div>
                    <a href="<?= $mainUrl ?>"
                       style="display:block;margin:25px 0 0;text-align:center;font-size:18px;color:#ffffff;text-decoration:none;width:261px;height:59px;line-height:59px;background:#ff758e url('button.jpg') no-repeat top left;"><?= Yii::t('email', 'View sales') ?></a>
                    <div style="line-height: 1.3; text-align:center; font-size: 16px; font-weight:bold; padding: 35px 0 25px; color:#c6576a;"><?= Yii::t('email', 'Have a good mood and pleasant shopping for Mommy!') ?></div>
                    <table cellspacing="0" cellpadding="0" style="width:100%;">
                        <tr>
                            <td valign="top">
                                <div style="line-height: 1.3; text-align:left; font-size: 16px; padding-top:30px;"><?= Yii::t('email', 'Sincerely') ?>
                                    , <br/>
                                    <a href="<?= $mainUrl ?>" style="color:#ff839b;font-weight:bold;">Mommy.com</a>
                                </div>
                            </td>
                            <td valign="bottom" style="width:280px">
                                <img style="width:100%;" src="<?= $baseImgUrl ?>flowers.jpg" alt=""/>
                            </td>
                        </tr>
                    </table>
                </div> <!-- content wrapper -->
                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;">
                                    <span style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                    <?= Yii::t('email', 'Copyright') ?>
                                        &copy; MOMMY.COM<br/> <?= Yii::t('email', 'You received this newsletter on') ?>
                                        &nbsp; <a href="mailto:<?= $user->email ?>"
                                                  target="_blank"><?= $user->email ?></a>, <?= Yii::t('email', 'because you are a member of the Mommy Club.') ?>
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
