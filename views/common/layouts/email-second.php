<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body style="margin: 0;">
<?= $content; ?>
</body>
</html>
