<?php

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\Utf8;

/**
 * @var UserRecord $model
 * @var PromocodeRecord $promocode
 * @var $this CController
 */

$this->pageTitle = 'Важная информация + Скидка для нового участника клуба';
$user = isset($data['user']) ? $data['user'] : new UserRecord();
$password = isset($data['password']) ? $data['password'] : '';
$promocode = isset($data['promocode']) ? $data['promocode'] : new PromocodeRecord();

$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/welcome/2/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$marker = 'reg_with_promocode' . date('d.m.Y');
$promocodeAvailableDays = $promocode->getAvailableDays();
$utmParams = ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $marker];

$homeUrl = $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams))]);
?>
<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl ?>bg.jpg');">
    <tbody>
    <tr>
        <td style="padding-top:30px;padding-bottom:30px;">
            <div style="height:0;display:none;line-height:0;background-color:transparent;color:transparent;">
                Ознакомьтесь для получения максимальной ВЫГОДЫ. Доступная забота о себе и близких
            </div>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody>
                <tr>
                    <td style="border-top:1px solid #e16783;border-left:1px solid #e16783;border-right:1px solid #e16783;border-bottom:1px solid #e16783;background-color:#fff;">

                        <!--menu-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle" width="15%" height="46"
                                    style="text-align:center;vertical-align:middle;">
                                    <a style="color:#929292;font-size:11px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;line-height:normal;"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS] + $utmParams))]) ?>">
                                        <img src="<?= $baseImgUrl ?>top-menu-male.png" width="21" height="24" alt=""
                                             style="vertical-align:middle;">
                                        <span style="margin-left:5px;vertical-align:middle;"><?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?></span>
                                    </a>
                                </td>
                                <td align="center" valign="middle" width="15%" height="46"
                                    style="text-align:center;vertical-align:middle;">
                                    <a style="color:#929292;font-size:11px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;line-height:normal;"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS] + $utmParams))]) ?>">
                                        <img src="<?= $baseImgUrl ?>top-menu-female.png" width="23" height="24" alt=""
                                             style="vertical-align:middle;">
                                        <span style="margin-left:5px;vertical-align:middle;"><?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?></span>
                                    </a>
                                </td>
                                <td align="center" valign="middle" width="18%" height="46"
                                    style="text-align:center;vertical-align:middle;">
                                    <a style="color:#929292;font-size:11px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;line-height:normal;"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS] + $utmParams))]) ?>">
                                        <img src="<?= $baseImgUrl ?>top-menu-books.png" width="20" height="22" alt=""
                                             style="vertical-align:middle;">
                                        <span style="margin-left:5px;vertical-align:middle;"><?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?></span>
                                    </a>
                                </td>
                                <td align="center" valign="middle" width="18%" height="46"
                                    style="text-align:center;vertical-align:middle;">
                                    <a style="color:#929292;font-size:11px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;line-height:normal;"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME] + $utmParams))]) ?>">
                                        <img src="<?= $baseImgUrl ?>top-menu-household.png" width="21" height="21"
                                             alt="" style="vertical-align:middle;">
                                        <span style="margin-left:5px;vertical-align:middle;"><?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?></span>
                                    </a>
                                </td>
                                <td align="center" valign="middle" width="22%" height="46"
                                    style="text-align:center;vertical-align:middle;">
                                    <a style="color:#929292;font-size:11px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;line-height:normal;"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS] + $utmParams))]) ?>">
                                        <img src="<?= $baseImgUrl ?>top-menu-parents.png" width="45" height="23" alt=""
                                             style="vertical-align:middle;">
                                        <span style="margin-left:5px;vertical-align:middle;"><?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?></span>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--menu-->

                        <!--header-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle"
                                    style="height:95px;text-align:left;background-color:#FF859C;background-image:url('head-bg.jpg');">
                                    <div style="padding-top:5px;padding-left:17px;overflow:hidden;">
                                        <a href="<?= $homeUrl ?>"
                                           style="font-family:arial;font-size:50px;width:278px;float:left;display:block;text-align:center;color:#ffffff;">
                                            <img src="<?= $baseImgUrl ?>logo.png" width="278" height="54"
                                                 alt="MOMMY.COM" style="display:block;" border="0">
                                        </a>
                                        <div style="width:280px;float:right;text-align:center;">
                                            <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                               href="<?= $homeUrl ?>">
                                                <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                    СЕБЕ И БЛИЗКИХ</b>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--header-->
                        <!--content-->
                        <table align="center" border="0" cellpadding="0" cellspacing="3" width="580px">
                            <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="<?= $baseImgUrl ?>img1.jpg" style="width:100%;" alt=""/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="<?= $baseImgUrl ?>img2.jpg" style="width:100%;" alt=""/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="<?= $baseImgUrl ?>img3.jpg" style="width:100%;" alt=""/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="<?= $baseImgUrl ?>img4.jpg" style="width:100%;" alt=""/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <img src="<?= $baseImgUrl ?>img5.jpg" style="width:100%;" alt=""/>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <p style="margin:10px 0 0;padding:0 0 20px;text-align:center;font-size:22px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.3;">
                                        Нужные и красивые вещи по низким<br>ценам - это в <a href="<?= $homeUrl ?>"
                                                                                             style="color:#dc2f49;text-decoration:none;">MOMMY.COM</a>
                                    </p>
                                    <p style="margin:10px 0 10px;padding:0;font-size:15px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.4;">
                                        Ваш логин: <span
                                                style="font-weight:normal;font-size:16px;"><?= $user->email ?></span>
                                    </p>
                                    <p style="margin:10px 0 10px;padding:0;font-size:15px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.4;">
                                        Ваш пароль: <span
                                                style="font-weight:normal;font-size:16px;"><?= $password ?></span>
                                    </p>
                                    <p style="margin:10px 0 10px;padding:0;font-size:15px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.4;">
                                        Ваша регистрация на <a href="<?= $homeUrl ?>"
                                                               style="color:#dc2f49;text-decoration:none;">МАМАМ.UA</a>
                                        прошла успешно.
                                        Для перехода на сайт нажмите на эту <a href="<?= $homeUrl ?>"
                                                                               style="color:#dc2f49;text-decoration:none;">ссылку</a>.
                                    </p>
                                    <p style="margin:10px 0 10px;padding:0;text-align:center;font-size:15px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.4;">
                                        Следите за <a href="<?= $homeUrl ?>"
                                                      style="color:#dc2f49;text-decoration:none;">нашими распродажами
                                        </a>
                                        и вам не придется ходить по магазинам и тратить свое время и нервы.
                                    </p>
                                    <p style="margin:10px 0 0px;padding:0;text-align:center;font-size:15px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:1.4;">
                                        Радуйте себя и своих близких не выходя из дома!</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if ($promocode->cash) : ?>
                                        <p style="text-align:left;line-height: 25px;font-family: sans-serif;font-size: 18px;">
                                            Как и обещали, вы можете получить скидку <?= $promocode->cash ?> гривен на
                                            заказ. Чтобы воспользоваться скидкой введите промокод
                                        </p>
                                    <?php else : ?>
                                        <p style="text-align:left;line-height: 25px;font-family: sans-serif;font-size: 18px;">
                                            Как и обещали, вы можете получить скидку <?= $promocode->percent ?> %, чтобы
                                            воспользоваться скидкой введите промокод
                                        </p>
                                    <?php endif ?>
                                    <div style="font-family:Arial;font-size:25px;color:#f00;height:auto;text-align:center;padding:10px;border:#f00 2px dashed;border-radius:7px;width:320px;margin:0 auto">
                                        <strong><?= $promocode->promocode ?></strong>
                                    </div>
                                    <?php if ($promocodeAvailableDays > 0) : ?>
                                        <p style="text-align:left;line-height: 25px;font-family: sans-serif;font-size: 18px;text-align: center;">
                                            действителен
                                            всего <?= Yii::t('message', '{n} день!|{n} дня!|{n} дней!', $promocodeAvailableDays) ?>
                                            !
                                        </p>
                                    <?php endif ?>

                                    <?php if ($promocode->order_price_after) : ?>
                                        <p style="text-align:left;line-height: 25px;font-family: sans-serif;"><i>Обратите
                                                внимание! Бонус будет начислен только при покупке на сумму
                                                от <?= Yii::t('message', '{n} гривны|{n} гривен|{n} гривен', $promocode->order_price_after) ?></i>
                                        </p>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams))]) ?>"
                                       target="_blank"
                                       style="display:block;width:240px;height:54px;line-height:54px;margin:0 auto 20px;text-align:center;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:20px;text-decoration:none;color:#ffffff;background:url('<?= $baseImgUrl ?>button.jpg') no-repeat top center;">
                                        Начать покупки
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="3" width="580px"
                               style="padding:10px 0 25px;">
                            <tbody>
                            <tr>
                                <td align="center"><img src="<?= $baseImgUrl ?>icon1.jpg" alt=""/></td>
                                <td align="center"><img src="<?= $baseImgUrl ?>icon2.jpg" alt=""/></td>
                                <td align="center"><img src="<?= $baseImgUrl ?>icon3.jpg" alt=""/></td>
                                <td align="center"><img src="<?= $baseImgUrl ?>icon4.jpg" alt=""/></td>
                            </tr>
                            </tbody>
                        </table>
                        <!--content-->

                        <!-------->
                        <!--main-->

                        <!--contacts-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle" width="50%" height="70"
                                    style="background-color:#f3f3f3;">
                                    <span style="color:#6a6a6a;font-size:14px;font-family:Arial,Helvetica,sans-serif;font-weight:bold;line-height:normal;vertical-align:middle;margin-right:30px;">МЫ В СОЦСЕТЯХ:</span>
                                    <a href="<?= Yii::app()->params['groupVk'] ?>" style="margin-right:5px;"><img
                                                src="<?= $baseImgUrl ?>vk.png" width="38" height="38" alt=""
                                                style="vertical-align:middle;"></a>
                                    <a href="<?= Yii::app()->params['groupOd'] ?>"><img src="<?= $baseImgUrl ?>od.png"
                                                                                        width="38" height="38" alt=""
                                                                                        style="vertical-align:middle;"></a>
                                </td>
                                <td align="center" valign="middle" width="50%" height="70"
                                    style="background-color:#f3f3f3;">
                                    <a style="color:#ff839b;font-size:20px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:normal;display:block;vertical-align:middle;"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams))]) ?>">MOMMY.COM</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--contacts-->

                        <!--footer-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle" height="70"
                                    style="background-color:#ff798c; padding:10px 0 10px;">
                                        <span style="color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:13px;">
                                            Aвторские права © <?= date('Y') ?> MOMMY<br>
                                            Письмо содержит данные для доступа в личный кабинет, не передавайте<br>
                                            его третьим лицам<br>
                                            Вы получили это письмо поскольку являетесь участником клуба MOMMY
                                        </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--footer-->
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
