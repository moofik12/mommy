<?php

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\GroupedProduct;

/**
 * @var CController $this
 * @var string $title
 * @var UserRecord $user
 * @var array $products товары в виде массива [['category' => *internal-category-name*, 'label' => *category-label*, 'items' => GroupedProduct[]]]
 * @var PromocodeRecord|null $promo
 * @var string $promocode
 * @var integer $promocodeValidUntil
 */

$app = Yii::app();
$baseUrl = $app->getBaseUrl(true) . '/';
$assetsUrl = $baseUrl . 'static/mailing/interesting-products/';
$frontendUrlManager = $app->hasComponent('frontendUrlManager') ? $app->getComponent('frontendUrlManager') : $app;
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$date = date('d.m.Y');

$userUnsubscribeUrl = $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('account/unsubscribe', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]));
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?= CHtml::encode($title); ?></title>
</head>
<body style="margin: 0;">

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border:0;width:100%;">
    <tbody>
    <tr>
        <td style="padding-top:20px;padding-bottom:20px;">
            <div style="height:0;display:none;line-height:0;background-color:transparent;color:transparent;">Самые
                низкие цены на МАМАМ.UA
            </div>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="580">
                <tbody>
                <tr>
                    <td valign="top">

                        <!--menu-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="middle" width="203" height="56"
                                    style="text-align:center;vertical-align:middle;">
                                    <a href="<?= $baseUrl ?>"
                                       style="color:#989898;font-size:11px;font-family:Arial,Helvetica,sans-serif;text-align:center;text-decoration:none;line-height:normal;">
                                        <img src="<?= $assetsUrl . 'logo.png' ?>" width="203" height="36" alt=""
                                             style="display:block;vertical-align:middle;">
                                        <span style="font-size:11px;color:#989898;">Доступная забота о себе и близких</span>
                                    </a>
                                </td>
                                <td align="right" valign="middle" height="56"
                                    style="text-align:right;vertical-align:middle;">
                                    <a href="<?= $baseUrl ?>"
                                       style="color:#989898;font-size:13px;font-family:Arial,Helvetica,sans-serif;text-align:center;text-decoration:none;line-height:normal;">
                                        Открыть в браузере
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--menu-->

                        <?php if ($promo !== null): ?>
                            <!--promo-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="center" valign="top"
                                        style="padding-top:12px;padding-bottom:22px;text-align:center;vertical-align:middle;">
                                        <div style="width:340px;margin:0 auto;padding-top:8px;padding-left:40px;padding-right:40px;padding-bottom:8px;border-radius:8px;text-align:center;background-color:#f7f7f7;">
                                            <span style="display:block;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#6c6c6c;text-align:center;text-transform:uppercase;">Ваш промокод для дополнительной скидки</span>
                                            <div style="margin-top:6px;margin-bottom:6px;padding-top:14px;padding-bottom:14px;text-align:center;border: 1px dashed #ff364d;border-radius:8px;background-color:#ffffff;">
                                                <span style="font-family:Arial,Helvetica,sans-serif;font-size:25px;line-height:28px;color:#ff364d;"><?= $promocode ?></span>
                                            </div>
                                            <span style="display:block;font-weight:bold;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#6c6c6c;text-align:center;">*код действителен в течение <?= (int)round($promocodeValidUntil / 60 / 60) ?>
                                                часов</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--promo-->
                        <?php endif ?>

                        <!--mail header-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="center" valign="top" style="text-align:center;vertical-align:middle;">
                                    <div style="height:44px;border-radius:28px 28px 0 0;text-align:center;background-color:#f7f7f7;">
                                        <span style="line-height:44px;display:block;font-weight:bold;font-family:Arial,Helvetica,sans-serif;font-size:19px;color:#ff6e7d;text-align:center;text-transform:uppercase;">Отобрано персонально для Вас</span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!--mail header-->

                        <?php foreach ($products as $category): ?>
                            <?php $bigProducts = array_slice($category['items'], 0, 3); /* @var $bigProducts GroupedProduct[] */ ?>
                            <?php $smallProducts = array_slice($category['items'], 3, 5); /* @var $smallProducts GroupedProduct[] */ ?>

                            <!--content section-->
                            <div style="margin-top:16px;">
                                <div style="padding-bottom:4px;border-bottom: 1px dashed #dedede;">
                                    <span style="line-height:20px;display:block;font-weight:normal;font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#3e3e3e;text-align:left;text-transform:uppercase;"><?= $category['label'] ?></span>
                                </div>
                                <!-- big sized products -->
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <?php foreach ($bigProducts as $product): ?>
                                            <?php $url = $frontendUrlManager->createAbsoluteUrl(
                                                'usersProvider/mailingRegistered', [
                                                    'id' => $user->id,
                                                    'hash' => $user->getLoginHash(),
                                                    'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl(
                                                        'product/index', [
                                                        'eventId' => $product->event->id,
                                                        'id' => $product->product->id,
                                                        'utm_source' => 'email',
                                                        'utm_medium' => 'email',
                                                        'utm_campaign' => 'interesting-products',
                                                        'utm_content' => $date,
                                                    ]))]
                                            ) ?>
                                            <td align="left" valign="top" width="193">
                                                <div style="width:185px;margin-top:14px;">
                                                    <a href="<?= $url ?>" style="text-decoration:none;"><img
                                                                src="<?= $product->product->logo->getThumbnail('mid320_prodlist')->url ?>"
                                                                width="185" alt="" style="display:block;border:0;"></a>
                                                    <a href="<?= $url ?>"
                                                       style="display:block;padding-top:3px;padding-bottom:3px;color:#333333;font-size:12px;font-weight:bold;font-family:Arial,Helvetica,sans-serif;text-align:center;text-decoration:none;text-transform:uppercase;"><?= CHtml::encode($product->product->name) ?></a>
                                                </div>
                                            </td>
                                        <?php endforeach ?>
                                    </tr>
                                    </tbody>
                                </table>
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                       style="margin-top:2px;">
                                    <tbody>
                                    <tr>
                                        <?php foreach ($bigProducts as $product): ?>
                                            <td align="left" valign="top" width="193">
                                                <div style="text-align: center; width: 193px;">
                                                    <span style="color:#9d9d9d;display:inline-block;font-size:13px;font-weight:normal;font-family:Arial,Helvetica,sans-serif;line-height:30px;text-decoration:line-through;"><?= $cf->format($product->priceOld, $cn->getName('short')) ?></span>
                                                    <span style="margin-left:12px;padding-left:12px;padding-right:12px;color:#ee4f62;display:inline-block;font-size:15px;font-weight:bold;font-family:Arial,Helvetica,sans-serif;background-color:#fff1f3;line-height:30px;"><?= $cf->format($product->price, $cn->getName('short')) ?></span>
                                                </div>
                                            </td>
                                        <?php endforeach ?>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- big sized products -->
                            </div>

                            <?php if ($smallProducts !== []): ?>
                                <!-- small sized products -->
                                <div style="margin-top:16px;padding-top:16px;border-top:1px solid #f2f2f2;">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <?php foreach ($smallProducts as $product): ?>
                                                <?php $url = $frontendUrlManager->createAbsoluteUrl(
                                                    'usersProvider/mailingRegistered', [
                                                        'id' => $user->id,
                                                        'hash' => $user->getLoginHash(),
                                                        'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl(
                                                            'product/index', [
                                                            'eventId' => $product->event->id,
                                                            'id' => $product->product->id,
                                                            'utm_source' => 'email',
                                                            'utm_medium' => 'email',
                                                            'utm_campaign' => 'interesting-products',
                                                            'utm_content' => $date,
                                                        ]))]
                                                ) ?>

                                                <td align="center" valign="top" width="20%">
                                                    <div style="width:auto;">
                                                        <a href="<?= $url ?>" style="text-decoration:none;"><img
                                                                    src="<?= $product->product->logo->getThumbnail('small150')->url ?>"
                                                                    width="100" alt="" style="display:block;border:0;"></a>
                                                        <span style="color:#9d9d9d;display:block;font-size:12px;font-weight:normal;font-family:Arial,Helvetica,sans-serif;line-height:30px;text-decoration:line-through;"><?= $cf->format($product->priceOld, $cn->getName('short')) ?></span>
                                                        <span style="padding-left:12px;padding-right:12px;color:#ee4f62;display:inline-block;font-size:13px;font-weight:bold;font-family:Arial,Helvetica,sans-serif;background-color:#fff1f3;line-height:30px;"><?= $cf->format($product->price, $cn->getName('short')) ?></span>
                                                    </div>
                                                </td>
                                            <?php endforeach ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- small sized products -->
                            <?php endif ?>

                            <!--content section-->
                        <?php endforeach ?>

                        <!--footer-->
                        <div style="margin-top:20px;border-radius: 0 0 28px 28px;background-color:#f7f7f7;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="middle" height="70"
                                        style="padding:12px;padding-left:18px;text-align:left;vertical-align:middle;">
                                        <a href="<?= $baseUrl ?>"
                                           style="color:#333333;font-size:14px;font-family:Arial,Helvetica,sans-serif;text-align:center;text-decoration:none;line-height:normal;text-transform:uppercase;">Шоппинг
                                            клуб «MOMMY»</a>
                                    </td>
                                    <td align="center" valign="middle" width="140"
                                        style="padding:12px;padding-right:14px;">
                                        <span style="padding-bottom:6px;display:block;color:#333333;font-size:14px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:normal;text-align:center;vertical-align:middle;">МЫ В СОЦСЕТЯХ</span>
                                        <a href="<?= Yii::app()->params['groupVk'] ?>" style="margin-right:5px;"><img
                                                    src="<?= $assetsUrl . 'vk.png' ?>" width="36" height="36" alt=""
                                                    style="vertical-align:middle;"></a>
                                        <a href="<?= Yii::app()->params['groupOd'] ?>" style="margin-right:5px;"><img
                                                    src="<?= $assetsUrl . 'od.png' ?>" width="36" height="36" alt=""
                                                    style="vertical-align:middle;"></a>
                                        <a href="<?= Yii::app()->params['groupFb'] ?>" style="margin-right:5px;"><img
                                                    src="<?= $assetsUrl . 'fb.png' ?>" width="36" height="36" alt=""
                                                    style="vertical-align:middle;"></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--footer-->

                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<table bgcolor="white" align="left" width="100%">
    <tbody>
    <tr>
        <td>
            <span style="font-family:arial,helvetica,sans-serif;color:black;font-size:12px">
                Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам
            </span>
        </td>
    </tr>
    <tr>
        <td>
            <span style="font-family:arial,helvetica,sans-serif;color:black;font-size:12px">Чтобы отписаться от этой рассылки, перейдите по
                <a
                        href="<?= $userUnsubscribeUrl ?>"
                        target="_blank">
                    ссылке
                </a>
            </span>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
