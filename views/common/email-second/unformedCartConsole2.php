<?php

use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var ShopShoppingCart $model
 * @var CController $this
 * @var string $title
 * @var PromocodeRecord|null $promo
 * @var string $promocode
 * @var float $promocodeSavings
 * @var float $cost
 * @var float $costWithPromocode
 * @var array $cartPositions
 */

if (!$model instanceof ShopShoppingCart) {
    throw new CException('Ошибка передачи модели, ожидается ShopShoppingCart');
}
/** @var UserRecord $user */
$user = $model->getUser();
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/cart-second/2/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$date = date('d.m.Y');
$appName = 'MAMAMA.UA';
$indexUrl = $frontendUrlManager->createAbsoluteUrl(
    'usersProvider/mailingRegistered',
    [
        'id' => $user->id,
        'hash' => $user->getLoginHash(),
        'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date])),
    ]
);
$cartUrl = $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]));
$userUnsubscribeUrl = $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('account/unsubscribe', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]));
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($title); ?></title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
</head>
<body style="margin:0; padding:0; background-color:#ececec;background-image:url(<?= $baseImgUrl . 'main-bg.jpg' ?>);">
<table cellspacing="0" cellpadding="0" width="100%"
       style="background-color:#ececec; background-image:url(<?= $baseImgUrl . 'main-bg.jpg' ?>); overflow:hidden;">
    <tr>
        <td valign="top">
            <table cellspacing="0" cellpadding="0" width="100%"
                   style="background:url(<?= $baseImgUrl . 'top-bg.jpg' ?>) repeat-x top left;">
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" width="757px" align="center"
                               style="background:url(<?= $baseImgUrl . 'cart-img.png' ?>) no-repeat bottom right;">
                            <tr>
                                <td valign="top">
                                    <div style="height:0;display:none;line-height:0;background-color:transparent;color:transparent">
                                        Истек срок резерва на понравившиеся Вам товары
                                    </div>
                                    <div style="width:257px; height:112px; padding:30px 10px 0; text-align:center;">
                                        <a href="<?= $indexUrl ?>">
                                            <img src="<?= $baseImgUrl . 'logo.png' ?>"
                                                 style="vertical-align:bottom; border:none; outline:none;"
                                                 alt="<?= $appName ?>"/>
                                        </a>
                                        <p style="color:#ffffff; font-size:16px; line-height:1.25; font-family:'PT Sans', sans-serif;">
                                            ДОСТУПНАЯ ЗАБОТА О СЕБЕ И БЛИЗКИХ</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <p style="color:#ffffff; padding-top:3px; padding-left:15px; font-size:31px; line-height:1.2; font-family:'PT Sans', sans-serif; font-weight:700; text-shadow:2px 3px 0 #cb949d;">
                                        Выбранные Вами товары<br/>еще есть в наличии,<br/>успейте заказать!<br/>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table cellspacing="0" cellpadding="0" width="757px" align="center" bgcolor="#ffffff"
                               style="border-radius:10px; font-size:16px; font-family:'PT Sans', sans-serif; font-weight:700; color:#707070; box-shadow:2px 2px 8px #cdcdcd;">
                            <tr>
                                <td style="border-bottom:1px solid #ececec; padding:16px 18px 10px; line-height:1.3;">
                                    Товар
                                </td>
                                <td style="border-bottom:1px solid #ececec; padding:16px 18px 10px; line-height:1.3;">
                                    Описание
                                </td>
                                <td style="border-bottom:1px solid #ececec; padding:16px 18px 10px; line-height:1.3;">
                                    Колличество
                                </td>
                                <td align="center"
                                    style="border-bottom:1px solid #ececec; padding:16px 18px 10px; line-height:1.3;">
                                    Стоимость
                                </td>
                            </tr>
                            <?php
                            foreach ($cartPositions as $position):
                                /** @var $position CartRecord */
                                ?>
                                <tr>
                                    <td style="padding:16px 3px 3px 18px;">
                                        <a href="<?= $cartUrl ?>" target="_blank" style="text-decoration:none;">
                                            <img src="<?= $position->product->product->getLogo()->getThumbnail('small70')->url ?>"
                                                 width="72" height="86"
                                                 style="vertical-align:middle; outline:none; border:1px solid #c2c3c5;"
                                                 alt="фото товара"/>
                                        </a>
                                    </td>
                                    <td style="padding:16px 18px 3px;">
                                        <a style="text-decoration:none; display:block; line-height:1; color:#ff7c91; padding-bottom:2px;"
                                           href="<?= $cartUrl ?>" target="_blank">
                                            <?= $position->product->product->name ?>
                                        </a>
                                        <p style="line-height:1.2; padding:0; margin:0; font-weight:400;">
                                            <a style="text-decoration:none;color:#707070;" href="<?= $cartUrl ?>"
                                               target="_blank">
                                                <?= $position->event->name ?>
                                            </a>
                                        </p>
                                    </td>
                                    <td style="padding:16px 18px 3px; font-weight:400;">1 шт.</td>
                                    <td align="right" width="145px" style="padding:16px 18px 3px;">
                                        <span style="font-size:20px; font-weight:400; text-decoration:line-through;"><?= $cf->format($position->product->price_market) ?>
                                            <b style="font-size:16px; font-weight:400;"></b></span>
                                        <span style="color:#ff7c91; font-size:20px; padding-left:5px;"><?= $cf->format($position->price) ?>
                                            <b style="font-size:16px;"></b></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td valign="top" colspan="4" style="padding-top:16px">
                                    <table cellspacing="0" cellpadding="0" width="757px" align="center"
                                           style="border-top:1px solid #ececec;">
                                        <tr>
                                            <td align="left"
                                                style="padding:7px 18px 0; font-weight:400;"><?= Yii::t('app', ' {n} good for purchase | {n} товара для покупки |{n} goods for purchase ', $model->getCount()) ?></td>
                                            <td align="right" style="padding:7px 18px 0; font-size:18px;">Итого: <span
                                                        style="font-size:22px; color:#ff7c91;"><?= $cf->format($cost) ?>
                                                    <b style="font-weight:400; font-size:18px;"> <?= $cn->getName('short') ?></b></span>
                                            </td>
                                        </tr>
                                        <?php if ($promocode != ''): ?>
                                            <tr>
                                                <td colspan="2" style="padding-top:37px;">
                                                    <table cellspacing="0" cellpadding="0" width="757px" align="center">
                                                        <tr>
                                                            <td width="124px"></td>
                                                            <td width="509px">
                                                                <div style="background:#f1fcfa; border-radius:7px; border:2px dashed #63bda7; text-align:center; padding:13px 0 9px;">
                                                                    <p style="margin:0; font-size:20px; font-weight:400; color:#000000; line-height:1.55;">
                                                                        Ваш промокод для дополнительной скидки</p>
                                                                    <strong style="display:block; font-size:26px; font-weight:700; color:#ff0000; line-height:1.55;"><?= $promocode ?></strong>
                                                                </div>
                                                                <p style="line-height:1.2; font-size:20px; text-align:center; margin:0; padding-top:20px;">
                                                                    действителен только 24 часа</p>
                                                            </td>
                                                            <td width="124px"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <td align="center" colspan="2" style="padding-top:22px;">
                                                <a href="<?= $cartUrl ?>"
                                                   style="width:253px; height:52px; line-height:52px; font-size:18px; color:#ffffff; display:block; text-decoration:none; text-align:center; background:#63bda7;">Оформить
                                                    заказ</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-top:30px;">
                                                <table cellspacing="0" cellpadding="0" width="757px" height="72px;"
                                                       align="center" bgcolor="#f3f3f3"
                                                       style="border-radius:0 0 10px 10px;">
                                                    <tr>
                                                        <td align="left" style="padding-left:35px;">
                                                            <span style="padding-right:34px;">МЫ В СОЦСЕТЯХ:</span>
                                                            <a href="<?= Yii::app()->params['groupVk'] ?>"
                                                               style="margin-right:4px;"><img width="34" height="34"
                                                                                              style="vertical-align:middle; border:none; outline:none;"
                                                                                              src="<?= $baseImgUrl . 'vk.png' ?>"
                                                                                              alt="Вконтакте"/></a>
                                                            <a href="<?= Yii::app()->params['groupOd'] ?>"
                                                               style="margin-right:4px;"><img width="34" height="34"
                                                                                              style="vertical-align:middle; border:none; outline:none;"
                                                                                              src="<?= $baseImgUrl . 'od.png' ?>"
                                                                                              alt="Однокласники"/></a>
                                                        </td>
                                                        <td align="right" style="padding-right:35px;">
                                                            <a href="<?= $indexUrl ?>"
                                                               style="font-size:18px; color:#ff839b;">MOMMY.COM</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <p style="text-align:center; font-size:16px; color:#707070; font-family:'PT Sans', sans-serif; margin:0; line-height:1.2; padding:15px 0;">
                            Aвторские права © <?= date('Y') ?> MOMMY<br/>
                            Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам<br>
                            Вы получили это письмо поскольку являетесь участником клуба MOMMY
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table bgcolor="white" align="left" width="100%">
    <tbody>
    <tr>
        <td><span style="font-family:arial,helvetica,sans-serif;color:black;font-size:12px">Чтобы отписаться от этой рассылки, перейдите по
                <a
                        href="<?= $userUnsubscribeUrl ?>"
                        target="_blank">
                    ссылке
                </a>
            </span>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
