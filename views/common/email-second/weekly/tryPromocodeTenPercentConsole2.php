<?php

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\Utf8;

/**
 * @var UserRecord $model
 * @var $this CController
 * @var PromocodeRecord $promocode
 */

$user = isset($user) ? $user : new UserRecord();
$promocode = isset($promocode) ? $promocode : new PromocodeRecord();

$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/notification-second/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();

$validDays = false;
if ($promocode->valid_until) {
    $dateTimePromocode = new DateTime('today');
    $dateTimePromocode->setTimestamp($promocode->valid_until);

    $interval = $dateTimePromocode->diff(new DateTime('today'));

    if ($interval) {
        $validDays = $interval->days;
    }
}
$date = date('d.m.Y');
$app = Yii::app();
$utmParams = ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date];
$indexUrl = $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams))]);
$userUnsubscribeUrl = $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($frontendUrlManager->createAbsoluteUrl('account/unsubscribe', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]));
?>
<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl ?>bg.png');">
    <tbody>
    <tr>
        <td align="center">
            <div style="height:0;display:none;line-height:0;background-color:transparent;color:transparent;">Сколько
                дней и ночей пришлось провести мне, чтобы сделать это
            </div>
            <div style="width:753px;padding-bottom:20px;"> <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="bottom" style="height:24px;">
                            <div style="padding-top:15px;padding-left:15px;"><img
                                        src="<?= $baseImgUrl ?>head-logo-top.png" width="308" height="9" alt=""
                                        style="display:block;" border="0"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="height:132px;text-align:left;background:#e0728c;">
                            <div style="width:734px;height:132px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;padding-left:17px;background:url('<?= $baseImgUrl ?>head-bg.jpg');">
                                <a href="<?= $indexUrl ?>"
                                   style="font-family:arial;font-size:50px;width:305px;float:left;display:block;text-align:center;color:#ffffff;">
                                    <img src="<?= $baseImgUrl ?>head-logo-bottom.png" width="305" height="131"
                                         alt="MOMMY.COM" style="display:block;" border="0">
                                </a>
                                <div>
                                    <div style="width:420px;float:right;padding-top:46px;text-align:center;">
                                        <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                           href="<?= $indexUrl ?>">
                                            <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                СЕБЕ И БЛИЗКИХ</b></a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table> <!-- header --> <!-- menu -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl ?>top-menu-bg.png');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-male.png"
                                                 width="21" height="24" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-female.png"
                                                 width="23" height="24" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-books.png"
                                                 width="20" height="22" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-parents.png"
                                                 width="45" height="23" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-household.png"
                                                 width="21" height="21" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div> <!-- menu --> <!-- content wrapper -->
                <div style="color: #505050; text-align: center; font-family: sans-serif;padding: 10px 10px 0 10px; border-right-color: rgb(225, 103, 131); border-right-style: solid; border-right-width: 1px; font-size: 16px; border-left-color: rgb(225, 103, 131); border-left-style: solid; border-left-width: 1px; background-color: rgb(255, 255, 255);">
                    <h1 style="font-weight: normal; font-size: 24px; line-height: 1.3;">Хотим вас немножко порадовать
                        <img src="<?= $baseImgUrl ?>smile2.jpg" style="vertical-align:bottom;" alt=":)"/></h1>
                    <div style="margin-top: 5px; line-height: 1.3; font-size: 18px;"><b>Теперь еще больше скидок</b>
                    </div>
                    <div style="margin-top: 5px; line-height: 1.3; font-size: 18px; padding-top: 5px;"><?= $validDays ? Yii::t('mail', 'В течении {n} дня вы | В течении {n} дней вы', $validDays) : 'Вы' ?>
                        можете использовать <b>дополнительную скидку 10%</b><br/>к вашим покупкам!
                    </div>
                    <div style="margin-top: 5px; line-height: 1.3; font-size: 18px; padding-top: 15px;"><b>Обновляйте
                            гардероб выгодно с <a href="<?= $indexUrl ?>" style="color: #ff758d;">МАМАМ.UA!</a></b>
                    </div>
                    <table align="center" cellpadding="0" cellspacing="0" border="0"
                           style="margin-top:20px;width:516px;border-collapse:collapse;">
                        <tr>
                            <td valign="center"
                                style="height:133px;background:#2da17c url('promocode-bg.jpg') no-repeat top center;">
                                <table align="center" cellpadding="0" cellspacing="0" border="0"
                                       style="width:100%;border-collapse:collapse;">
                                    <tr>
                                        <td valign="top"><p
                                                    style="margin:0;padding:0;font-size:18px;color:#ffffff;text-align:center;font-weight:bold;line-height:1;font-family: sans-serif;">
                                                Ваш промокод на дополнительную скидку <span
                                                        style="line-height:1;font-size:40px;font-style:italic;color:#f8ff8b;vertical-align:middle;"><?= $promocode->percent ?>
                                                    %</span></p></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <div style="font-family: sans-serif; font-size:29px;text-align:center;color:#333333;border-radius:5px;display:block;background:#ffffff;margin:0 auto;padding:15px;width:440px;"><?= $promocode->promocode ?></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding:10px 0 10px;">
                                <table align="center" cellpadding="0" cellspacing="0" border="0"
                                       style="width:100%;border-collapse:collapse;">
                                    <tr>
                                        <td valign="top" style="text-align:right;"><img
                                                    src="<?= $baseImgUrl ?>left-arrow.jpg" alt=""/></td>
                                        <td valign="top" style="width:265px;"><a href="<?= $indexUrl ?>"
                                                                                 style="color:#ffffff;text-align:center;text-decoration:none;font-size:20px;display:block;width:261px;height:59px;line-height:59px;background:#d5637c url(button.jpg) no-repeat top center; font-family: sans-serif; margin:8px auto;">Начать
                                                покупки</a></td>
                                        <td valign="top" style="text-align:left;"><img
                                                    src="<?= $baseImgUrl ?>right-arrow.jpg" alt=""/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!-- content wrapper -->
                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="padding-left:7px;padding-right:7px;padding-top:15px;padding-bottom:10px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                                <div style="height:70px;background-color:#f3f3f3;">
                                    <table align="left" cellpadding="0" cellspacing="0"
                                           style="height:70px;border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <span style="font-family:arial;font-size:14px;font-weight:bold;padding-left:30px;padding-right:40px;color:#6a6a6a;">МЫ В СОЦСЕТЯХ:</span>
                                            </td>
                                            <td><a style="font-size:18px;color:#ff839b;"
                                                   href="<?= Yii::app()->params['groupVk'] ?>"><img
                                                            src="<?= $baseImgUrl ?>vk.png" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupOd'] ?>"><img
                                                            src="<?= $baseImgUrl ?>od.png" width="38" height="38"
                                                            alt="ОД"></a></td>
                                            <td style="padding-left:160px;">
                                                <a style="color:#ff839b;font-size:20px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:normal;display:block;vertical-align:middle;text-decoration:none;"
                                                   href="<?= $indexUrl ?>">Mommy.com</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;">
                                    <span style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                        Aвторские права © <?= date('Y') ?> MOMMY<br>
                                        Вы получили это письмо поскольку являетесь участником клуба MOMMY
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
