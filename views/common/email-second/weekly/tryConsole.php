<?php

use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\Utf8;

/**
 * @var UserRecord $model
 * @var $this CController
 */

$user = isset($user) ? $user : new UserRecord();

$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/notification-second/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$date = date('d.m.Y');
$app = Yii::app();
$userUnsubscribeUrl = $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($frontendUrlManager->createAbsoluteUrl('account/unsubscribe', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]));
?>


<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl . 'bg.png' ?>');">
    <tbody>
    <tr>
        <td align="center">
            <div style="width:753px;padding-bottom:20px;">

                <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="bottom" style="height:24px;">
                            <div style="padding-top:15px;padding-left:15px;">
                                <img src="<?= $baseImgUrl . 'head-logo-top.png' ?>" width="308" height="9" alt=""
                                     style="display:block;" border="0">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="height:132px;text-align:left;background:#e0728c;">
                            <div style="width:734px;height:132px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;padding-left:17px;background:url('<?= $baseImgUrl . 'head-bg.jpg' ?>');">
                                <a href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>"
                                   style="font-family:arial;font-size:50px;width:305px;float:left;display:block;text-align:center;color:#ffffff;">
                                    <img src="<?= $baseImgUrl . 'head-logo-bottom.png' ?>" width="305" height="131"
                                         alt="MOMMY.COM" style="display:block;" border="0">
                                </a>
                                <div>
                                    <div style="width:420px;float:right;padding-top:46px;text-align:center;">
                                        <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                           href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>">
                                            <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                СЕБЕ И БЛИЗКИХ</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- header -->

                <!-- menu -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl . 'top-menu-bg.png' ?>');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-male.png' ?>" width="21"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-female.png' ?>" width="23"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-books.png' ?>" width="20"
                                                    height="22" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-parents.png' ?>" width="45"
                                                    height="23" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-household.png' ?>" width="21"
                                                    height="21" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- menu -->

                <!-- content wrapper -->
                <div style="font-size: 17px; text-align: justify;font-family: arial;  padding: 0 10px; border-right-color: rgb(225, 103, 131); border-right-style: solid; border-right-width: 1px; border-left-color: rgb(225, 103, 131); border-left-style: solid; border-left-width: 1px; background-color: rgb(255, 255, 255);">
                    <div style="text-align: center;padding-top: 18px;padding-bottom: 20px;"><b>Вам стоит это
                            попробовать!</b></div>
                    <div style="color: #333333;">
                        Экономить с нами - <b>ЛЕГКО</b>! Распродажи с большими скидками и широким ассортиментом товаров
                        для всей семьи! Самые низкие цены, гарантия возврата, лучший клиентский сервис, более <b>10 000
                            новых </b>товаров каждый день!
                    </div>
                    <div><br></div>
                    <div style="color: #333333;">
                        Ежедневно мы присылаем вам письма с нашими предложениями,&nbsp;команда &nbsp;<a
                                href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>"
                                style="line-height: 25px; text-align: left; font-weight: bold; text-decoration: none; color: rgb(227, 90, 116);">МАМАМ.UA</a>
                        подбирает для вас лучшие брендовые товары по самым низким ценам! Больше не нужно ходить по
                        магазинам и ждать скидок.

                    </div>
                    <div><br></div>
                    <div style="color: #333333;">
                        Радуйте себя и своих близких, экономьте время и деньги вместе с нами!
                    </div>
                    <div><br></div>
                    <div>
                        <a href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-weekly', 'utm_content' => $date]))]) ?>"
                           style="font-family: cursive; font-size: 20px; display: inline !important;">
                            <img alt="try-it" height="426" src="<?= $baseImgUrl . 'ecommerce.jpg' ?>" width="710"
                                 style="width: 729px; height: 450px;">
                        </a>
                    </div>
                </div>
                <!-- content wrapper -->

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="padding-left:7px;padding-right:7px;padding-top:15px;padding-bottom:10px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                                <div style="height:70px;background-color:#f3f3f3;">
                                    <table align="left" cellpadding="0" cellspacing="0"
                                           style="height:70px;border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <span style="font-family:arial;font-size:14px;font-weight:bold;padding-left:30px;padding-right:40px;color:#6a6a6a;">МЫ В СОЦСЕТЯХ:</span>
                                            </td>
                                            <td><a style="font-size:18px;color:#ff839b;"
                                                   href="<?= Yii::app()->params['groupVk'] ?>"><img
                                                            src="<?= $baseImgUrl . 'vk.png' ?>" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupOd'] ?>"><img
                                                            src="<?= $baseImgUrl . 'od.png' ?>" width="38" height="38"
                                                            alt="ОД"></a></td>
                                            <td style="padding-left:170px;">
                                                <a style="font-family:arial;font-size:20px;font-weight:bold;color:#ff839b;"
                                                   href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">MOMMY.COM</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;">
                                    <span style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                        Aвторские права © <?= date('Y') ?> MOMMY<br>
                                        Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам<br>
                                        Вы получили это письмо поскольку являетесь участником клуба MOMMY
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<table bgcolor="white" align="left" width="100%">
    <tbody>
    <tr>
        <td><span style="font-family:arial,helvetica,sans-serif;color:black;font-size:12px">Чтобы отписаться от этой рассылки, перейдите по
                <a
                        href="<?= $userUnsubscribeUrl ?>"
                        target="_blank">
                    ссылке
                </a>
            </span>
        </td>
    </tr>
    </tbody>
</table>
