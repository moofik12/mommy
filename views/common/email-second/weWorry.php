<?php

use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\Utf8;

/**
 * @var $this CController
 * @var $user UserRecord
 */

$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/we_worry/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$marker = 'we_worry' . date('d.m.Y');
$utmParams = ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-info', 'utm_content' => $marker];
$mainUrl = $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams))]);
?>
<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl ?>bg.png');">
    <tbody>
    <tr>
        <td align="center">
            <div style="height:0;display:none;line-height:0;background-color:transparent;color:transparent;">Почему вы
                так давно к нам не заходили?
            </div>
            <div style="width:753px;padding-bottom:20px;"> <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="bottom" style="height:24px;">
                            <div style="padding-top:15px;padding-left:15px;"><img
                                        src="<?= $baseImgUrl ?>head-logo-top.png" width="308" height="9" alt=""
                                        style="display:block;" border="0"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="height:132px;text-align:left;background:#e0728c;">
                            <div style="width:734px;height:132px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;padding-left:17px;background:url('head-bg.jpg');">
                                <a href="<?= $mainUrl ?>"
                                   style="font-family:arial;font-size:50px;width:305px;float:left;display:block;text-align:center;color:#ffffff;">
                                    <img src="<?= $baseImgUrl ?>head-logo-bottom.png" width="305" height="131"
                                         alt="MOMMY.COM" style="display:block;" border="0">
                                </a>
                                <div>
                                    <div style="width:420px;float:right;padding-top:46px;text-align:center;">
                                        <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                           href="<?= $mainUrl ?>">
                                            <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                СЕБЕ И БЛИЗКИХ</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table> <!-- header --> <!-- menu -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl ?>top-menu-bg.png');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-male.png"
                                                 width="21" height="24" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-female.png"
                                                 width="23" height="24" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-books.png"
                                                 width="20" height="22" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-parents.png"
                                                 width="45" height="23" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('top-menu-sep.png') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;">
                                            <img style="display:block;" src="<?= $baseImgUrl ?>top-menu-household.png"
                                                 width="21" height="21" alt="">
                                        </td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME] + $utmParams))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div> <!-- menu --> <!-- content wrapper -->
                <div style="color: #505050; font-family: sans-serif;padding: 10px 10px 0 10px; border-right-color: rgb(225, 103, 131); border-right-style: solid; border-right-width: 1px; font-size: 16px; border-left-color: rgb(225, 103, 131); border-left-style: solid; border-left-width: 1px; background-color: rgb(255, 255, 255);">
                    <h1 style="font-weight: normal; text-align: center; font-size: 24px; line-height: 1.3; padding:5px 0 5px; margin: 0;">
                        Уважаемый пользователь!</h1>
                    <div style="margin-top: 5px; line-height: 1.3; text-align:left; font-size: 16px; padding-top: 5px;">
                        Вы так давно к нам не заходили, мы успели соскучиться и решили порадовать вас сногсшибательными
                        акциями.
                    </div>
                    <div style="line-height: 1.3; text-align:left; font-size: 16px; padding-top: 8px;">В шоппинг-клубе
                        <a href="<?= $mainUrl ?>" style="color:#ff839b;font-weight:bold;">Mommy.com</a> вы найдете
                        большой выбор практичной и стильной одежды, обувь, книги и принадлежности для дома.
                    </div>
                    <div style="line-height: 1.3; text-align:left; font-size: 16px; padding-top: 8px;">
                        <a href="<?= $mainUrl ?>" style="color:#ff839b;font-weight:bold;">Следите за акциями</a> и
                        наслаждайтесь шопингом вместе с <a href="<?= $mainUrl ?>"
                                                           style="color:#ff839b;font-weight:bold;">Mommy.com!</a>
                    </div>
                    <a href="<?= $mainUrl ?>"
                       style="display:block;margin:25px 0 0;text-align:center;font-size:18px;color:#ffffff;text-decoration:none;width:261px;height:59px;line-height:59px;background:#ff758e url('button.jpg') no-repeat top left;">Посмотреть
                        распродажи</a>
                    <div style="line-height: 1.3; text-align:center; font-size: 16px; font-weight:bold; padding: 35px 0 25px; color:#c6576a;">
                        Хорошего настроения и приятных покупок на Mамам!
                    </div>
                    <table cellspacing="0" cellpadding="0" style="width:100%;">
                        <tr>
                            <td valign="top">
                                <div style="line-height: 1.3; text-align:left; font-size: 16px; padding-top:30px;">С
                                    уважением,<br/> Клуб распродаж товаров для мам, детей<br/> и малышей
                                    <a href="<?= $mainUrl ?>" style="color:#ff839b;font-weight:bold;">Mommy.com</a>
                                </div>
                            </td>
                            <td valign="bottom" style="width:280px">
                                <img style="width:100%;" src="<?= $baseImgUrl ?>flowers.jpg" alt=""/>
                            </td>
                        </tr>
                    </table>
                </div> <!-- content wrapper -->
                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="padding-left:7px;padding-right:7px;padding-top:15px;padding-bottom:10px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                                <div style="height:70px;background-color:#f3f3f3;">
                                    <table align="left" cellpadding="0" cellspacing="0"
                                           style="height:70px;border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <span style="font-family:arial;font-size:14px;font-weight:bold;padding-left:30px;padding-right:40px;color:#6a6a6a;">МЫ В СОЦСЕТЯХ:</span>
                                            </td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupFb'] ?>"><img
                                                            src="<?= $baseImgUrl ?>fb.png" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupVk'] ?>"><img
                                                            src="<?= $baseImgUrl ?>vk.png" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupOd'] ?>"><img
                                                            src="<?= $baseImgUrl ?>od.png" width="38" height="38"
                                                            alt="ОД"></a></td>
                                            <td style="padding-left:160px;">
                                                <a style="color:#ff839b;font-size:20px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;line-height:normal;display:block;vertical-align:middle;font-weight:bold;"
                                                   href="<?= $mainUrl ?>">MOMMY.COM</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;"><span
                                            style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                        Aвторские права © <?= date('Y') ?> MOMMY<br>Вы получили это письмо поскольку являетесь участником клуба MOMMY</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
