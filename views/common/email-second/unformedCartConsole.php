<?php

use MommyCom\Model\Db\CartRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\Utf8;

/**
 * @var ShopShoppingCart $model
 * @var CController $this
 * @var string title
 * @var PromocodeRecord|null $promo
 * @var string $promocode
 * @var float $promocodeSavings
 * @var float $cost
 * @var float $costWithPromocode
 * @var array $cartPositions
 */

if (!$model instanceof ShopShoppingCart) {
    throw new CException('Ошибка передачи модели, ожидается ShopShoppingCart');
}
/** @var UserRecord $user */
$user = $model->getUser();
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/cart-second/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$date = date('d.m.Y');

$userUnsubscribeUrl = $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('account/unsubscribe', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]));
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($title); ?></title>
</head>
<body style="margin: 0;">

<table cellpadding="0" cellspacing="0"
       style="font-family: arial; border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl . 'bg.png' ?>');">
    <tbody>
    <tr>
        <td align="center">
            <div style="width:753px;padding-bottom:20px;">

                <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="bottom" style="height:24px;">
                            <div style="padding-top:15px;padding-left:15px;">
                                <img src="<?= $baseImgUrl . 'head-logo-top.png' ?>" width="308" height="9" alt=""
                                     style="display:block;" border="0">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="height:132px;text-align:left;background:#e0728c;">
                            <div style="width:734px;height:132px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;padding-left:17px;background:url('<?= $baseImgUrl . 'head-bg.jpg' ?>');">
                                <a href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>"
                                   style="font-family:arial;font-size:50px;width:305px;float:left;display:block;text-align:center;color:#ffffff;">
                                    <img src="<?= $baseImgUrl . 'head-logo-bottom.png' ?>" width="305" height="131"
                                         alt="MOMMY.COM" style="display:block;" border="0">
                                </a>
                                <div>
                                    <div style="width:420px;float:right;padding-top:46px;text-align:center;">
                                        <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                           href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>">
                                            <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                СЕБЕ И БЛИЗКИХ</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- header -->

                <!-- menu -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl . 'top-menu-bg.png' ?>');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-male.png' ?>" width="21"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-female.png' ?>" width="23"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-books.png' ?>" width="20"
                                                    height="22" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-parents.png' ?>" width="45"
                                                    height="23" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-household.png' ?>" width="21"
                                                    height="21" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- menu -->

                <!-- content wrapper -->
                <div style="padding:10px 11px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                    <!-- content -->
                    <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                        <tbody>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td style="padding:0 5px 15px;">
                                            <span style="font-weight:bold;font-size:14px;line-height:25px;color:#707070;">Выбранный Вами товар еще есть в наличии, успейте заказать!</span>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td style="padding:5px;border-top:1px solid #f3f3f3;border-bottom:1px solid #f3f3f3;font-size:12px;font-weight:bold;color:#707070;">
                                            Товар
                                        </td>
                                        <td width="328"
                                            style="padding:5px;border-top:1px solid #f3f3f3;border-bottom:1px solid #f3f3f3;font-size:12px;font-weight:bold;color:#707070;">
                                            Описание
                                        </td>
                                        <td width="164"
                                            style="padding:5px;border-top:1px solid #f3f3f3;border-bottom:1px solid #f3f3f3;font-size:12px;font-weight:bold;color:#707070;">
                                            Количество и стоимость
                                        </td>
                                    </tr>
                                    <?php
                                    foreach ($cartPositions as $position):
                                        /** @var $cart CartRecord */
                                        ?>
                                        <tr>
                                            <td width="72" height="86"
                                                style="padding:5px 0 5px;border-bottom:1px solid #f3f3f3;">
                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date])) ?>"
                                                   style="display: block;border:1px solid #f5f5f5;" target="_blank">
                                                    <img src="<?= $position->product->product->getLogo()->getThumbnail('small70')->url ?>"
                                                         width="72" height="86"
                                                         alt="<?= $position->product->product->name ?>"
                                                         style="display:block;" border="0"></a>
                                            </td>
                                            <td style="padding:5px;border-bottom:1px solid #f3f3f3;">
                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date])) ?>"
                                                   style="display: block;text-decoration:none;" target="_blank">
                                                    <span style="font-size:15px;font-weight:bold;text-decoration:none;color:#ff7b8e;"><?= $position->product->product->name ?></span><br>
                                                    <span style="font-size:14px;text-decoration:none;color:#3e3e3e;"><?= $position->event->name ?></span>
                                                </a>
                                            </td>
                                            <td style="padding:5px;border-bottom:1px solid #f3f3f3;">
                                                <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date])) ?>"
                                                   style="display: block;text-decoration:none;" target="_blank">
                                                        <span style="font-size:14px;color:#ff6178;">
                                                            <span style="font-weight:bold;"><?= $cf->format($position->price, $cn->getName('short')) ?></span>
                                                            <span style="color:#a5a5a5;">x</span>
                                                            <span style="font-weight:bold;color:#3e3e3e;"><?= $position->number ?></span>
                                                            <span style="color:#a5a5a5;">=</span>
                                                            <span style="font-weight:bold;"><?= $cf->format($position->totalPrice, $cn->getName('short')) ?></span>
                                                        </span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td rowspan="2" valign="middle" width="202"
                                            style="padding:10px 0px 5px;font-weight:bold;font-size:14px;line-height:25px;border-bottom:1px solid #f3f3f3;color:#3e3e3e;"><?= Yii::t('app', ' {n} good for purchase | {n} goods for purchase |{n} goods for purchase ', $model->getCount()) ?></td>
                                        <?php if ($promocodeSavings > 0): ?>
                                            <td align="right" valign="middle"
                                                style="padding:5px 5px 0px;font-weight:bold;line-height:25px;font-size:16px;color:#3e3e3e;">
                                                Дополнительная СКИДКА: <span
                                                        style="font-size:24px;color:#ff6178;"><?= $cf->format($promocodeSavings, $cn->declination($promocodeSavings)) ?></span>
                                            </td>
                                        <?php endif ?>
                                    </tr>
                                    <tr>
                                        <?php if ($promocodeSavings > 0): ?>
                                            <td align="right" valign="middle"
                                                style="padding:0px 5px 5px;font-weight:bold;line-height:25px;font-size:16px;border-bottom:1px solid #f3f3f3;color:#3e3e3e;">
                                                Итого, с учетом скидки: <span
                                                        style="font-size:24px;color:#ff6178;"><?= $cf->format($costWithPromocode, $cn->getName('short')) ?></span>
                                            </td>
                                        <?php else: ?>
                                            <td align="right" valign="middle"
                                                style="padding:0px 5px 5px;font-weight:bold;line-height:25px;font-size:16px;border-bottom:1px solid #f3f3f3;color:#3e3e3e;">
                                                Итого: <span
                                                        style="font-size:24px;color:#ff6178;"><?= $cf->format($cost, $cn->getName('short')) ?></span>
                                            </td>
                                        <?php endif ?>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;border-collapse:collapse;margin-top:15px;">
                        <tbody>
                        <?php if ($promocode != ''): ?>
                            <tr>
                                <td colspan="3" width="100%" height="70" valign="top" style="padding:0;">
                                    <table style="border-collapse:collapse" height="25" border="0" cellpadding="0"
                                           cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td colspan="3" height="25"
                                                style="width:50%;vertical-align:bottom;font-family:Arial,Helvetica,sans-serif;font-size:19px;color:#010101;text-align:center">
                                                Ваш промокод для ДОПОЛНИТЕЛЬНОЙ СКИДКИ
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table style="border-collapse:collapse" height="55" border="0" cellpadding="0"
                                           cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="background: #ffffff;" height="55" align="left" valign="middle"
                                                width="33%">
                                                <img style="display:block" alt="">
                                            </td>
                                            <td height="70"
                                                style="font-family:Arial;font-size:25px;color:#f00;text-align:center">
                                                <div style="padding:10px;border:#f00 2px dashed;border-radius:7px;width:320px;margin:0 auto">
                                                    <strong><?= $promocode ?></strong>
                                                </div>
                                            </td>
                                            <td style="background: #ffffff;" align="right" valign="middle" width="33%">
                                                <img style="display:block" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background: #ffffff;" height="25" align="left" valign="middle"
                                                width="33%">
                                                <img style="display:block" alt="">
                                            </td>
                                            <td valign="middle" align="center">
                                                <span style="font-family:arial;font-size:18px;font-weight:bold;color:#6a6a6a;">действителен только 24 часа</span>
                                            </td>
                                            <td style="background: #ffffff;" align="right" valign="middle" width="33%">
                                                <img style="display:block" alt="">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td colspan="3" width="100%" height="70" valign="top" style="padding:15px 0;">
                                <table style="border-collapse:collapse" height="55" border="0" cellpadding="0"
                                       cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td style="background: #ffffff;" height="55" align="left" valign="middle"
                                            width="33%"></td>
                                        <td style="background:#53a28f" align="center" height="55" bgcolor="#53a28f"
                                            valign="middle" width="">
                                            <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($app->frontendUrlManager->createAbsoluteUrl('order/index', ['promocode' => $promocode, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart'])) ?>"
                                               style="display:block;text-decoration:none;color:#ffffff!important;line-height: 55px;"
                                               target="_blank">
                                                <span style="font-family:sans-serif;font-size:16px;color:#ffffff;text-decoration:none"><b>ОФОРМИТЬ ЗАКАЗ</b></span>
                                            </a>
                                        </td>
                                        <td style="background: #ffffff;" align="right" valign="middle" width="33%"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- content -->
                </div>
                <!-- content wrapper -->

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="padding-left:7px;padding-right:7px;padding-top:15px;padding-bottom:10px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                                <div style="height:70px;background-color:#f3f3f3;">
                                    <table align="left" cellpadding="0" cellspacing="0"
                                           style="height:70px;border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <span style="font-family:arial;font-size:14px;font-weight:bold;padding-left:30px;padding-right:40px;color:#6a6a6a;">МЫ В СОЦСЕТЯХ:</span>
                                            </td>
                                            <td><a style="font-size:18px;color:#ff839b;"
                                                   href="<?= Yii::app()->params['groupVk'] ?>"><img
                                                            src="<?= $baseImgUrl . 'vk.png' ?>" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupOd'] ?>"><img
                                                            src="<?= $baseImgUrl . 'od.png' ?>" width="38" height="38"
                                                            alt="ОД"></a></td>
                                            <td style="padding-left:170px;">
                                                <a style="font-family:arial;font-size:20px;font-weight:bold;color:#ff839b;"
                                                   href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">MOMMY.COM</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;">
                                    <span style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                        Aвторские права © <?= date('Y') ?> MOMMY<br>
                                        Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам<br>
                                        Вы получили это письмо поскольку являетесь участником клуба MOMMY
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<table bgcolor="white" align="left" width="100%">
    <tbody>
    <tr>
        <td><span style="font-family:arial,helvetica,sans-serif;color:black;font-size:12px">Чтобы отписаться от этой рассылки, перейдите по
                <a
                        href="<?= $userUnsubscribeUrl ?>"
                        target="_blank">
                    ссылке
                </a>
            </span>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
