<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\YiiComponent\BuildUrlManager;
use MommyCom\YiiComponent\Utf8;

/* @var $order OrderRecord */
/* @var $frontendUrlManager BuildUrlManager */
/* @var $data array */

$order = $data['order'];
/** @var UserRecord $user */
$user = $order->user;
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/notification-third/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$orderAmount = $order->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_COURIER
    ? $order->getPayPrice() + $order->getDeliveryPrice() : $order->getPayPrice();
$date = date('d.m.Y');
$utmParams = ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'cart', 'utm_content' => $date];

$indexUrl = $frontendUrlManager->createAbsoluteUrl(
    'usersProvider/mailingRegistered',
    [
        'id' => $user->id,
        'hash' => $user->getLoginHash(),
        'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', $utmParams)),
    ]
);
$payUrl = $frontendUrlManager->createAbsoluteUrl(
    'usersProvider/mailingRegistered',
    ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => $frontendUrlManager->createUrl('pay/index', ['uid' => $order->id] + $utmParams)]
);
?>
<table style="width:100%; background:url(<?= $baseImgUrl . 'bg-main.png' ?>)" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table style="width:100%; min-width:800px; background:url(<?= $baseImgUrl . 'bg.png' ?>) repeat-x top center;"
                   cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table style="width:750px; background:#fd869c url(<?= $baseImgUrl . 'bg.png' ?>) repeat-x top center;"
                               align="center" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td style="height:67px;">
                                    <a target="_blank" href="<?= $indexUrl ?>" style="margin-left:20px;"><img
                                                src="<?= $baseImgUrl . 'logo.png' ?>" alt="МАМАМ.UA"/></a>
                                </td>
                                <td style="width:446px; height:67px;">
                                    <p style="color:#ffffff; font-size:24px; font-family:'Open Sans',sans-serif; font-weight:600;">
                                        Доступная забота о себе и близких</p>
                                </td>
                            </tr>
                        </table>
                        <table style="background:#fefefe; width:750px; box-shadow:0 0 8px #cdcdcd;" align="center"
                               cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="height:42px; text-align:center; border-bottom:1px solid #ffe7eb;">
                                    <a target="_blank"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS] + $utmParams))]) ?>"
                                       style="text-decoration:none; color:#fd869c; font-size:14px; font-family:Tahoma;">
                                        <img style="border:none; outline:none; padding-right:5px; vertical-align:middle;"
                                             src="<?= $baseImgUrl . 'category-boys.png' ?>"
                                             alt="<?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>"/>
                                        <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                    </a>
                                </td>
                                <td style="height:42px; text-align:center; border-bottom:1px solid #ffe7eb;">
                                    <a target="_blank"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS] + $utmParams))]) ?>"
                                       style="text-decoration:none; color:#fd869c; font-size:14px; font-family:Tahoma;">
                                        <img style="border:none; outline:none; padding-right:5px; vertical-align:middle;"
                                             src="<?= $baseImgUrl . 'category-girls.png' ?>"
                                             alt="<?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>"/>
                                        <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                    </a>
                                </td>
                                <td style="height:42px; text-align:center; border-bottom:1px solid #ffe7eb;">
                                    <a target="_blank"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS] + $utmParams))]) ?>"
                                       style="text-decoration:none; color:#fd869c; font-size:14px; font-family:Tahoma;">
                                        <img style="border:none; outline:none; padding-right:5px; vertical-align:middle;"
                                             src="<?= $baseImgUrl . 'category-toys.png' ?>"
                                             alt="<?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>"/>
                                        <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                    </a>
                                </td>
                                <td style="height:42px; text-align:center; border-bottom:1px solid #ffe7eb;">
                                    <a target="_blank"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME] + $utmParams))]) ?>"
                                       style="text-decoration:none; color:#fd869c; font-size:14px; font-family:Tahoma;">
                                        <img style="border:none; outline:none; padding-right:5px; vertical-align:middle;"
                                             src="<?= $baseImgUrl . 'category-home.png' ?>"
                                             alt="<?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>"/>
                                        <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                    </a>
                                </td>
                                <td style="height:42px; text-align:center; border-bottom:1px solid #ffe7eb;">
                                    <a target="_blank"
                                       href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS] + $utmParams))]) ?>"
                                       style="text-decoration:none; color:#fd869c; font-size:14px; font-family:Tahoma;">
                                        <img style="border:none; outline:none; padding-right:5px; vertical-align:middle;"
                                             src="<?= $baseImgUrl . 'category-parents.png' ?>"
                                             alt="<?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>"/>
                                        <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <table style="width:100%; padding:0 20px 0" align="center" cellspacing="7px"
                                           cellpadding="0" border="0">
                                        <tr>
                                            <td style="height:42px; text-align:center;" colspan="2" valign="middle">
                                                <span style="color:#444444; font-size:27px; font-family:'Open Sans',sans-serif; font-weight:600;">Спасибо за ваш заказ!</span>
                                                <p style="margin:0; padding:0 0 10px 0; font-size:16px; font-family:'Open Sans',sans-serif; font-weight:400; line-height:1.3;">
                                                    Заказ успешно подтвержден менеджером нашего магазина.</p>
                                            </td>
                                        </tr>
                                        <?php if ($order->is_drop_shipping): ?>
                                            <tr>
                                                <td style="text-align:center;" colspan="2" valign="middle">
                                                    <table style="width:100%; background-color:#eef7f3;" cellspacing="0"
                                                           cellpadding="0">
                                                        <tr>
                                                            <td style="width:127px;" valign="middle">
                                                                <img style="vertical-align:bottom;"
                                                                     src="<?= $baseImgUrl . 'car.png' ?>"
                                                                     alt="Быстрая доставка"/>
                                                            </td>
                                                            <td style="padding:10px 0 15px;">
                                                                <p style="margin:0; padding:0; line-height:1.3; color:#939393; text-align:left; font-size:16px; font-family:'Open Sans',sans-serif; font-weight:400;">
                                                                    Заказ был сделан из акции <b
                                                                            style="color:#58b087; font-family:'Open Sans',sans-serif; font-weight:700;">“Быстрая
                                                                        доставка”</b>. Это означает, что<br/> данный
                                                                    заказ отправляется
                                                                    непосредственно нашими партнерами<br/> Вам <span
                                                                            style="font-family:'Open Sans',sans-serif; font-weight:700;">в течение 3 дней после подтверждения заказа.</span>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <td style="text-align:center;" colspan="2" valign="middle">
                                                <table style="width:100%;" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td style="padding-top:10px;">
                                                            <p style="margin:0; padding:0; text-align:left; font-size:14px; line-height:1.3; font-family:'Open Sans',sans-serif; font-weight:600;">
                                                                Напоминаем, что компания Новая Почта взымает комиссию за
                                                                заказы отправленные
                                                                <span style="font-family:'Open Sans',sans-serif; font-weight:700;">наложенным платежом.</span>
                                                                Чтобы избежать этого, рекомендуем вам произвести
                                                                предоплату заказа.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="text-align:center; width:50%; padding:10px 0 10px;">
                                                <table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="text-align:left;">
                                                            <img style="border:none; outline:none;"
                                                                 src="<?= $baseImgUrl . 'pay-default.png' ?>"
                                                                 alt="оплата обычным способом"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="text-align:center; width:50%; padding:10px 0 10px;">
                                                <table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="text-align:right;">
                                                            <img style="border:none; outline:none;"
                                                                 src="<?= $baseImgUrl . 'pay-safely.png' ?>"
                                                                 alt="предоплата"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p style="margin:0; padding:0; font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400;">
                                                    Вы можете оплатить заказ через <a target="_blank"
                                                                                      href="<?= $payUrl ?>"
                                                                                      style="text-decoration:none; color:#fd869c; font-weight:600;">перейдя
                                                        по ссылке ниже</a>, а также в разделе <a target="_blank"
                                                                                                 href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('account/orders', $utmParams))]) ?>"
                                                                                                 style="text-decoration:none; color:#fd869c; font-weight:600;">"Мои
                                                        заказы"</a>!
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="padding-bottom:15px;">
                                                <table style="width:100%; padding-top:15px;" cellspacing="0"
                                                       cellpadding="0">
                                                    <tr>
                                                        <td></td>
                                                        <td valign="bottom"
                                                            style="width:508px; height:155px; text-align:center; background:#9fd36f url(<?= $baseImgUrl . 'green_bg2.png' ?>) no-repeat top center;">
                                                            <p style="margin:0; padding:0; line-height:1.2; color:#ffffff; font-size:20px; text-transform:uppercase; font-family:'Open Sans',sans-serif; font-weight:600;">
                                                                оплатить через</p>
                                                            <p style="margin:0; padding:0;"><img
                                                                        style="vertical-align:bottom;"
                                                                        src="<?= $baseImgUrl . 'arrow.png' ?>"/></p>
                                                            <div style="padding-bottom: 10px;">
                                                                <a style="vertical-align: top; display: inline-block; font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:0; padding:0; text-decoration: none; color: #ffffff;"
                                                                   target="_blank" href="<?= $payUrl ?>">
                                                                    <img width="240" height="76"
                                                                         style="border:none; outline:none;"
                                                                         src="<?= $baseImgUrl ?>privat_btn2.png"
                                                                         alt="Оплатить"/></a>
                                                                <a style="vertical-align: top; display: inline-block;font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:0; padding:0; text-decoration: none; color: #ffffff;"
                                                                   target="_blank" href="<?= $payUrl ?>">
                                                                    <img width="240" height="76"
                                                                         style="border:none; outline:none;"
                                                                         src="<?= $baseImgUrl ?>banks-btn.png"
                                                                         alt="Оплатить"/>
                                                                    <span style="display: block;">Карты других банков</span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                                <p style="font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:20px 0 8px; padding:0;">
                                                    Пожалуйста, проверьте данные вашего заказа:</p>
                                                <p style="font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:8px 0 8px; padding:0;">
                                                    Номер заказа: <span
                                                            style="font-weight:700;">№<?= $order->id ?></span></p>
                                                <p style="font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:8px 0 8px; padding:0;">
                                                    Стоимость товаров (без стоимости
                                                    доставки): <?= $cf->format($order->getPayPrice(), $cn->getName('short')) ?></p>
                                                <?php if ($order->card_payed > 0): ?>
                                                    <p style="font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:8px 0 8px; padding:0;">
                                                        Уже
                                                        оплачено: <?= $cf->format($order->card_payed, $cn->getName('short')) ?></p>
                                                <?php endif ?>
                                                <p style="font-size:14px; font-family:'Open Sans',sans-serif; font-weight:400; margin:8px 0 8px; padding:0; font-size:16px; font-weight:700;">
                                                    ИТОГО К
                                                    ОПЛАТЕ: <?= $cf->format($orderAmount, $cn->getName('short')) ?></p>
                                                <p style="font-size:14px; font-family:'Open Sans',sans-serif; font-weight:600; font-style:italic; margin:8px 0 8px; line-height:1.3; padding:0 font-size:16px;">
                                                    С любовью,<br/>МАМАМ</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100%; height:65px; border-top:1px solid #efefef;"
                                                colspan="2">
                                                <table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td style="width:50%;">
                                                            <a target="_blank" href="<?= $indexUrl ?>"
                                                               style="text-decoration:none; font-size:20px; color:#fd869c; font-family:'Open Sans',sans-serif; font-weight:700;">MOMMY.COM</a>
                                                        </td>
                                                        <td style="width:50%; text-align:right;">
                                                            <span style="vertical-align:middle; font-size:13px; color:#b5b5b5; font-family:'Open Sans',sans-serif; font-weight:600;">В соцсетях:</span>
                                                            <a target="_blank"
                                                               href="<?= Yii::app()->params['groupFb'] ?>"
                                                               style="margin-left:4px;"><img
                                                                        style="vertical-align:middle; border:none; outline:none;"
                                                                        src="<?= $baseImgUrl . 'fb.png' ?>"
                                                                        alt="FB"/></a>
                                                            <a target="_blank"
                                                               href="<?= Yii::app()->params['groupVk'] ?>"
                                                               style="margin-left:4px;"><img
                                                                        style="vertical-align:middle; border:none; outline:none;"
                                                                        src="<?= $baseImgUrl . 'vk.png' ?>"
                                                                        alt="VK"/></a>
                                                            <a target="_blank"
                                                               href="<?= Yii::app()->params['groupOd'] ?>"
                                                               style="margin-left:4px;"><img
                                                                        style="vertical-align:middle; border:none; outline:none;"
                                                                        src="<?= $baseImgUrl . 'ok.png' ?>"
                                                                        alt="OD"/></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table style="width:750px; text-align:center;" align="center" cellspacing="0" cellpadding="0"
                               border="0">
                            <tr>
                                <td style="height:100px;">
                                    <p style="font-size:14px; color:#999999; font-family:'Open Sans',sans-serif; text-shadow:0 1px 0 #ffffff;">
                                        Aвторские права &copy; <?= date('Y') ?> MOMMY <br>
                                        Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим
                                        лицам <br>
                                        Вы получили эту рассылку на адрес <?= $order->user->email ?>, поскольку
                                        являетесь <br>
                                        участником клуба MOMMY
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
