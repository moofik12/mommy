<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\YiiComponent\BuildUrlManager;
use MommyCom\YiiComponent\Utf8;

/* @var $order OrderRecord */
/* @var $frontendUrlManager BuildUrlManager */
/* @var $data array */

$order = $data['order'];
/** @var UserRecord $user */
$user = $order->user;
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/notification-second/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$date = date('d.m.Y');
$orderAmount = $order->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_COURIER
    ? $order->getPayPrice() + $order->getDeliveryPrice() : $order->getPayPrice();

?>

<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl . 'bg.png' ?>');">
    <tbody>
    <tr>
        <td align="center">
            <div style="width:753px;padding-bottom:20px;">

                <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="bottom" style="height:24px;">
                            <div style="padding-top:15px;padding-left:15px;">
                                <img src="<?= $baseImgUrl . 'head-logo-top.png' ?>" width="308" height="9" alt=""
                                     style="display:block;" border="0">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="height:132px;text-align:left;background:#e0728c;">
                            <div style="width:734px;height:132px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;padding-left:17px;background:url('<?= $baseImgUrl . 'head-bg.jpg' ?>');">
                                <a href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>"
                                   style="font-family:arial;font-size:50px;width:305px;float:left;display:block;text-align:center;color:#ffffff;">
                                    <img src="<?= $baseImgUrl . 'head-logo-bottom.png' ?>" width="305" height="131"
                                         alt="MOMMY.COM" style="display:block;" border="0">
                                </a>
                                <div>
                                    <div style="width:420px;float:right;padding-top:46px;text-align:center;">
                                        <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                           href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                            <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                СЕБЕ И БЛИЗКИХ</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- header -->

                <!-- menu -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl . 'top-menu-bg.png' ?>');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-male.png' ?>" width="21"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-female.png' ?>" width="23"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-books.png' ?>" width="20"
                                                    height="22" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-parents.png' ?>" width="45"
                                                    height="23" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-household.png' ?>" width="21"
                                                    height="21" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- menu -->

                <!-- content wrapper -->
                <div style="text-align:left;padding:10px 11px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">

                    <!-- content -->

                    <p style="font-size:16px;color:#3e3e3e">Некоторое время назад вы разместили заказ <span
                                style="font-weight:bold">№<?= $order->id ?></span> в
                        шоппинг-клубе <?= CHtml::link(Yii::app()->name, $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]), ['target' => '_blank']) ?>
                        .</p>
                    <br>
                    <p style="font-size:16px;color:#3e3e3e">Заказ в настоящее время ожидает оплаты.<br>
                        <span style="font-weight:bold">Для того, чтобы перейти к оплате немедленно, пожалуйста, нажмите на ссылку ниже:</span>
                    </p>

                    <p style="font-size:16px;color:#3e3e3e"><a style="color:#ff203b;"
                                                               href="<?= $frontendUrlManager->createAbsoluteUrl('pay/index', ['uid' => $order->id]) ?>"><?= $frontendUrlManager->createAbsoluteUrl('pay/index', ['uid' => $order->id, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]) ?></a>
                    </p>

                    <p style="font-size:16px;color:#3e3e3e">На этой странице вы найдете все необходимые инструкции для
                        того, чтобы оплатить заказ удобным для вас способом.</p>

                    <p style="font-size:16px;color:#3e3e3e;font-weight:bold">Пожалуйста, проверьте данные вашего
                        заказа:</p>

                    <p style="font-size:16px;color:#3e3e3e">Код заказа: <span
                                style="font-weight:bold">№<?= $order->id ?></span></p>

                    <p style="font-size:16px;color:#3e3e3e">Стоимость товаров (без стоимости доставки): <span
                                style="font-weight:bold"><?= $cf->format($order->getPayPrice(), $cn->getName('short')) ?></span>
                    </p>

                    <?php if ($order->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_COURIER) : ?>
                        <p style="font-size:16px;color:#3e3e3e">Стоимость доставки: <span
                                    style="font-weight:bold"><?= $cf->format($order->deliveryPrice, $cn->getName('short')) ?></span>
                        </p>
                    <?php endif ?>

                    <p style="font-size:16px;color:#3e3e3e">ИТОГО К ОПЛАТЕ: <span
                                style="font-weight:bold"><?= $cf->format($orderAmount, $cn->getName('short')) ?></span>
                    </p>
                    <br>
                    <p style="font-size:16px;color:#3e3e3e">Благодарим вас за размещение заказа в шоппинг-клубе
                        "MOMMY".</p>

                    <p style="font-size:16px;color:#3e3e3e">
                        С любовью,<br>
                        <?= CHtml::link(Yii::app()->name, $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]), ['target' => '_blank']) ?>
                    </p>

                    <!-- content -->

                </div>
                <!-- content wrapper -->

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="padding-left:7px;padding-right:7px;padding-top:15px;padding-bottom:10px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                                <div style="height:70px;background-color:#f3f3f3;">
                                    <table align="left" cellpadding="0" cellspacing="0"
                                           style="height:70px;border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <span style="font-family:arial;font-size:14px;font-weight:bold;padding-left:30px;padding-right:40px;color:#6a6a6a;">МЫ В СОЦСЕТЯХ:</span>
                                            </td>
                                            <td><a style="font-size:18px;color:#ff839b;"
                                                   href="<?= Yii::app()->params['groupVk'] ?>"><img
                                                            src="<?= $baseImgUrl . 'vk.png' ?>" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupOd'] ?>"><img
                                                            src="<?= $baseImgUrl . 'od.png' ?>" width="38" height="38"
                                                            alt="ОД"></a></td>
                                            <td style="padding-left:170px;">
                                                <a style="font-family:arial;font-size:20px;font-weight:bold;color:#ff839b;"
                                                   href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">MOMMY.COM</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;">
                                    <span style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                        Aвторские права © <?= date('Y') ?> MOMMY<br>
                                        Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам<br>
                                        Вы получили это письмо поскольку являетесь участником клуба MOMMY
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
