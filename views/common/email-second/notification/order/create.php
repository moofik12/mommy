<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\YiiComponent\BuildUrlManager;
use MommyCom\YiiComponent\Utf8;

/* @var $order OrderRecord */
/* @var $frontendUrlManager BuildUrlManager */
/* @var $data array */

$order = $data['order'];
/** @var UserRecord $user */
$user = $order->user;
$app = Yii::app();
$cf = $app->currencyFormatter;
$cn = Yii::app()->countries->getCurrency();
$df = Yii::app()->getDateFormatter();
$baseImgUrl = Yii::app()->getBaseUrl(true) . '/' . 'static/mailing/notification-second/';
$frontendUrlManager = Yii::app()->hasComponent('frontendUrlManager')
    ? Yii::app()->getComponent('frontendUrlManager') : Yii::app();
$categoryGroups = CategoryGroups::instance();
$deliveryMethod = 'наш менеджер свяжется с вами для уточнения данных';
$date = date('d.m.Y');

if ($order->isDeliveryAddressRight()) {
    $deliveryMethod = $order->getDeliveryAddress(true);
}
?>


<table cellpadding="0" cellspacing="0"
       style="border-collapse:collapse;border:0;width:100%;background:url('<?= $baseImgUrl . 'bg.png' ?>');">
    <tbody>
    <tr>
        <td align="center">
            <div style="width:753px;padding-bottom:20px;">

                <!-- header -->
                <table align="center" cellpadding="0" cellspacing="0"
                       style="width:100%;height:156px;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="bottom" style="height:24px;">
                            <div style="padding-top:15px;padding-left:15px;">
                                <img src="<?= $baseImgUrl . 'head-logo-top.png' ?>" width="308" height="9" alt=""
                                     style="display:block;" border="0">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="height:132px;text-align:left;background:#e0728c;">
                            <div style="width:734px;height:132px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;padding-left:17px;background:url('<?= $baseImgUrl . 'head-bg.jpg' ?>');">
                                <a href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>"
                                   style="font-family:arial;font-size:50px;width:305px;float:left;display:block;text-align:center;color:#ffffff;">
                                    <img src="<?= $baseImgUrl . 'head-logo-bottom.png' ?>" width="305" height="131"
                                         alt="MOMMY.COM" style="display:block;" border="0">
                                </a>
                                <div>
                                    <div style="width:420px;float:right;padding-top:46px;text-align:center;">
                                        <a style="font-family:arial;font-size:20px;line-height:24px;text-decoration:none;color:#ffffff;"
                                           href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                            <span style="font-weight:bold;text-transform:uppercase;">ДОСТУПНАЯ ЗАБОТА</span><br><b>О
                                                СЕБЕ И БЛИЗКИХ</b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- header -->

                <!-- menu -->
                <div style="height:34px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ececec;">
                    <table align="center" cellpadding="0" cellspacing="0"
                           style="width:100%;height:34px;border-collapse:collapse;background:url('<?= $baseImgUrl . 'top-menu-bg.png' ?>');">
                        <tbody>
                        <tr>
                            <td align="center" valign="top" style="width:146px;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-male.png' ?>" width="21"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_BOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_BOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:146px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-female.png' ?>" width="23"
                                                    height="24" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_GIRLS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_GIRLS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-books.png' ?>" width="20"
                                                    height="22" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_TOYS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_TOYS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:160px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-parents.png' ?>" width="45"
                                                    height="23" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_MOMS_AND_DADS, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_MOMS_AND_DADS)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top"
                                style="width:150px;height:34px;background:url('<?= $baseImgUrl . 'top-menu-sep.png' ?>') top left no-repeat;">
                                <table align="center" cellpadding="0" cellspacing="0"
                                       style="width:auto;height:34px;border-collapse:collapse;">
                                    <tr>
                                        <td valign="middle" style="width:21px;padding-right:4px;"><img
                                                    style="display:block;"
                                                    src="<?= $baseImgUrl . 'top-menu-household.png' ?>" width="21"
                                                    height="21" alt=""></td>
                                        <td>
                                            <a style="font-family:arial;font-size:13px;line-height:34px;display:block;text-decoration:none;color:#ff869a;"
                                               href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('event/category', ['category' => CategoryGroups::CATEGORY_HOME, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">
                                                <?= Utf8::ucfirst($categoryGroups->getLabel(CategoryGroups::CATEGORY_HOME)) ?>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- menu -->

                <!-- content wrapper -->
                <div style="padding:10px 8px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                    <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">

                        <!-- content -->
                        <tr>
                            <td align="left" valign="top">
                                <table cellpadding="0" cellspacing="0" align="center" style="width:681px;">
                                    <tr>
                                        <td valign="top">
                                            <p style="font-family:arial;font-size:24px;margin-top:10px;margin-bottom:5px;color:#333333;">
                                                Спасибо, что сделали заказ на сайте МАМАМ!</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:10px;margin-bottom:5px;color:#333333;">
                                                Мы рады сообщить Вам, что Ваш заказ был принят и в ближайшее время с
                                                Вами свяжется наш менеджер.</p>
                                            <?php if ($order->is_drop_shipping): ?>
                                                <p style="font-family:arial;font-size:14px;margin-top:10px;margin-bottom:5px;color:#333333;">
                                                    <b>Внимание!</b><br>
                                                    Напоминаем, что заказ был сделан из акции <a style="color:#ff6178;"
                                                                                                 href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date, '#' => 'definition']))]) ?>">“Быстрая
                                                        доставка”</a>.
                                                    Это означает, что данный заказ отправляется непосредственно нашими
                                                    партнерами Вам в течение 3 дней после подтверждения заказа.
                                                </p>
                                            <?php endif; ?>
                                            <div style="font-family:arial;font-size:14px;margin-top:10px;margin-bottom:5px;padding:10px;border-width:2px;border-style:dashed;border-color:#e16783;text-align:center;color:#333333;">
                                                При обращениях используйте <b style="font-size:18px;">ваш номер
                                                    заказа: <?= $order->id ?></b></div>

                                            <p style="font-family:arial;font-size:14px;color:#333333;">Компания Новая
                                                Почта взимает комиссию за заказы отправленные наложенным платежом. Чтобы
                                                избежать этого, рекомендуем Вам произвести предоплату заказа.</p>
                                            <p style="font-family:arial;font-size:14px;color:#333333;">Вы указали
                                                следующую информацию в заказе.</p>
                                            <p style="font-family:arial;font-size:24px;margin-top:10px;margin-bottom:5px;color:#333333;">
                                                Информация о доставке</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                <b>Получатель:</b> <?= "{$order->client_name} {$order->client_surname}" ?>
                                            </p>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                <b>Способ доставки:</b> <?= $deliveryMethod ?> </p>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                <b>Телефон получателя:</b> <?= $order->telephone ?></p>
                                            <p style="font-family:arial;font-size:14px;margin-top:25px;margin-bottom:5px;color:#333333;">
                                                <b>Ожидаемый период доставки
                                                    товара: <?= $df->format('d.MM', $order->getStartMailingAt()) ?>
                                                    – <?= $df->format('d.MM', $order->getMailAfterEndAt()) ?></b></p>
                                            <p style="font-family:arial;font-size:14px;margin-top:15px;margin-bottom:5px;color:#333333;">
                                                Общая сумма вашего заказа:
                                                <b><?= $cf->format($order->getPrice(), $cn->getName('short')) ?></b>.
                                            </p>
                                            <p style="font-family:arial;font-size:24px;margin-top:25px;margin-bottom:5px;color:#333333;">
                                                Информация о заказе</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:15px;margin-bottom:5px;color:#333333;">
                                                Вот содержимое вашего заказа</p>
                                            <table cellpadding="0" cellspacing="0" align="left"
                                                   style="width:681px;margin-top:5px;margin-bottom:15px;color:#333333;">
                                                <?php foreach ($order->positions as $position) : ?>
                                                    <tr>
                                                        <td valign="top"
                                                            style="width:100px;padding-bottom:10px;padding-right:20px;">
                                                            <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($frontendUrlManager->createAbsoluteUrl('product/index', ['eventId' => $position->event_id, 'id' => $position->product->product_id, 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date, '#' => 'content'])) ?>"
                                                               style="display:block;">
                                                                <img src="<?= $position->product->product->getLogo()->getThumbnail('small70')->url ?>"
                                                                     width="72" height="86"
                                                                     alt="<?= $position->product->product->name ?>"
                                                                     style="display:block;">
                                                            </a>
                                                        </td>
                                                        <td valign="middle" style="padding-bottom:10px;">
                                                            <p style="font-family:arial;font-size:14px;margin-top:0px;margin-bottom:0px;color:#333333;"><?= $position->product->product->name ?>
                                                                , артикул
                                                                товара: <?= $position->product->id ?><?= $position->product->size ? ", Размер: {$position->product->size}" : '' ?></p>
                                                            <p style="font-family:arial;font-size:14px;margin-top:0px;margin-bottom:0px;color:#333333;">
                                                                <b>Цена: <?= ($position->number > 1 ? "{$position->number}х" : '') ?> <?= $cf->format($position->price, $cn->getName('short')) ?></b>.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </table>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                <b>Итого вы
                                                    заказали <?= Yii::t('app', ' {n} товар | {n} товара |{n} товаров ', $order->positionNumber) ?>
                                                    общей
                                                    стоимостью <?= $cf->format($order->getPrice(), $cn->getName('short')) ?>
                                                    .</b></p>

                                            <p style="font-family:arial;font-size:24px;margin-top:25px;margin-bottom:5px;color:#333333;">
                                                Как Вы можете изменить свой заказ?</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:15px;margin-bottom:5px;color:#333333;">
                                                Если вы решили изменить ваш заказ, свяжитесь с нашей службой поддержки
                                                <a style="color:#ff6178;"
                                                   href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">на
                                                    сайте</a>.</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                Обязательно сообщите номер вашего заказа <b><?= $order->id ?></b>.</p>
                                            <?php if ($order->is_drop_shipping): ?>
                                                <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                    Если Вам понадобится вернуть заказ, Вы сможете это сделать в течение
                                                    14 дней с момента его получения.
                                                    <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($frontendUrlManager->createAbsoluteUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date, '#' => 'return'])) ?>"
                                                       style="color:#ff6178;">Узнать о правилах возврата</a>.
                                                </p>
                                            <?php else: ?>
                                                <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                    Если Вам понадобится вернуть заказ, Вы сможете это сделать в течение
                                                    14 дней с момента его получения.
                                                    <a href="<?= $user->buildLoginUrl('usersProvider/mailingRegistered') . urlencode($frontendUrlManager->createAbsoluteUrl('static/index', ['category' => 'service', 'page' => 'recurrence', 'utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date])) ?>"
                                                       style="color:#ff6178;">Узнать о правилах возврата</a>.
                                                </p>
                                            <?php endif; ?>
                                            <p style="font-family:arial;font-size:24px;margin-top:25px;margin-bottom:5px;color:#333333;">
                                                Остались вопросы?</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:15px;margin-bottom:5px;color:#333333;">
                                                Тогда просто позвоните нам на горячую
                                                линию
                                            </p>
                                            <p style="font-family:arial;font-size:14px;margin-top:10px;margin-bottom:5px;color:#333333;">
                                                Статус вашего заказа вы можете проверить в любой момент в личном
                                                кабинете.</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:10px;margin-bottom:5px;color:#333333;">
                                                Всего хорошего,</p>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                Служба поддержки клиентов шопинг-клуба <b>MOMMY</b></p>
                                            <p style="font-family:arial;font-size:14px;margin-top:5px;margin-bottom:5px;color:#333333;">
                                                <a href="mailto:support@mamam.com.ua" style="color:#ff6178;">support@mamam.com.ua</a>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- content -->

                    </table>
                </div>
                <!-- content wrapper -->

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="padding-left:7px;padding-right:7px;padding-top:15px;padding-bottom:10px;border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ffffff;">
                                <div style="height:70px;background-color:#f3f3f3;">
                                    <table align="left" cellpadding="0" cellspacing="0"
                                           style="height:70px;border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <span style="font-family:arial;font-size:14px;font-weight:bold;padding-left:30px;padding-right:40px;color:#6a6a6a;">МЫ В СОЦСЕТЯХ:</span>
                                            </td>
                                            <td><a style="font-size:18px;color:#ff839b;"
                                                   href="<?= Yii::app()->params['groupVk'] ?>"><img
                                                            src="<?= $baseImgUrl . 'vk.png' ?>" width="38" height="38"
                                                            alt="ВК"></a></td>
                                            <td style="padding-left:8px;"><a style="font-size:18px;color:#ff839b;"
                                                                             href="<?= Yii::app()->params['groupOd'] ?>"><img
                                                            src="<?= $baseImgUrl . 'od.png' ?>" width="38" height="38"
                                                            alt="ОД"></a></td>
                                            <td style="padding-left:170px;">
                                                <a style="font-family:arial;font-size:20px;font-weight:bold;color:#ff839b;"
                                                   href="<?= $frontendUrlManager->createAbsoluteUrl('usersProvider/mailingRegistered', ['id' => $user->id, 'hash' => $user->getLoginHash(), 'redirectTo' => urlencode($frontendUrlManager->createAbsoluteUrl('index/index', ['utm_source' => 'email', 'utm_medium' => 'email', 'utm_campaign' => 'notification-order', 'utm_content' => $date]))]) ?>">MOMMY.COM</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <table align="center" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td align="left" valign="top">
                            <div style="border-right-color:#e16783;border-right-style:solid;border-right-width:1px;border-left-color:#e16783;border-left-style:solid;border-left-width:1px;background-color:#ff798c;">
                                <div style="padding:10px 0;text-align:center;background-color:#ff798c;">
                                    <span style="font-family:arial;font-size:13px;display:block;line-height:17px;color:#ffe7ec;">
                                        Aвторские права © <?= date('Y') ?> MOMMY<br>
                                        Письмо содержит данные для доступа в личный кабинет, не передавайте его третьим лицам<br>
                                        Вы получили это письмо поскольку являетесь участником клуба MOMMY
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
