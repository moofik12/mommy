<?php

use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;

/**
 * @var $image FileStorageThumbnail
 */

echo CHtml::image($image->getThumbnail('mid380')->url, '', [
    'style' => 'max-width: 260px; max-height: 400px;',
]);
