<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\TabletPackagingRecord;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */
/* @var $taskOwner string */

$this->pageTitle = 'Для упаковки';
?>


<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<label>
    <div>Заказы:</div>
    <?= CHtml::dropDownList('taskOwner', $taskOwner, [
        'me' => 'Мои',
        '' => 'Не важно',
    ]) ?>
</label>
<?php $this->endWidget() ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    //'enableHistory'=> false,
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'id' => 'grid-available-orders',
    'summaryText' => 'Orders {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filterForm :input',
    'columns' => [
        ['name' => 'createdBy.fullname'],
        ['name' => 'order_id', 'htmlOptions' => ['class' => 'span2']],
        ['name' => 'order.delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data TabletPackagingRecord */
            return $data->order->deliveryTypeReplacement;
        }],
        ['name' => 'order.positionsCallcenterAcceptedCount', 'filter' => false],
        ['name' => 'order.weight', 'filter' => false, 'value' => function ($data) {
            /* @var $data TabletPackagingRecord */
            return ProductWeight::instance()->convertToString($data->order->getWeight());
        }],
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'rowIdColumn' => 'order_id',
            'url' => $this->createUrl('packaging/positions'),
            'value' => function ($data) {
                return CHtml::tag(
                    'span',
                    [
                        'class' => 'btn btn-success',
                    ],
                    'Показать'
                );
            },
        ],
    ],
]); ?>

<script>
    // hack, cause yii doest support nested grids
    $('#grid-available-orders').on('click', '.pagination a[href]', function (e) {
        var $this = $(this),
            $grid = $this.closest('.grid-view');
        var url = $this.attr('href');

        e.preventDefault();
        e.stopPropagation();
        $.fn.yiiGridView.update($grid.attr('id'), {
            url: url
        });
    });
</script>

<script>
    /*var $document = $(document);

    var positionsShowBtnSelector = '.tbrelational-column a[href*=positions]';
    $($document).on('click', positionsShowBtnSelector, function() {

        $(positionsShowBtnSelector).not(this).not('[data-opened="false"]').click();
    })*/
</script>
