<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\TabletWarehouseBindRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model OrderProductRecord */
/* @var $provider CActiveDataProvider */
/* @var $binds TabletWarehouseBindRecord[] */

$this->pageTitle = 'Товар на складе';
$controller = $this;
$request = Yii::app()->request;
/* @var $request CHttpRequest */

$csrfValue = $request->csrfToken;
$csrfName = $request->csrfTokenName;
$csrf = $request->enableCsrfValidation ? [$csrfName => $csrfValue] : [];

$gridId = 'grid-product-warehouse' . $model->id;
?>

<?php foreach ($model->product->product->getPhotogalleryImages() as $image): ?>
    <?= CHtml::link(
        CHtml::image(
            $image->getThumbnail('small70')->url,
            '',
            [
                'class' => 'img-polaroid',
            ]
        ),
        'javascript:void(0)', [
        'data-toggle' => 'popover',
        'data-placement' => 'right',
        'data-trigger' => 'hover',
        'data-html' => 'true',
        'data-content' => $this->renderPartial('_popoverImage', ['image' => $image], true),
    ]); ?>
<?php endforeach ?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableSorting' => false,
    'dataProvider' => $provider,
    'enableHistory' => false,
    'id' => $gridId,
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'event_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'event_product_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'product_id', 'headerHtmlOptions' => ['class' => 'span2']],
    ],
]) ?>
