<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model OrderRecord */
/* @var $positionsProvider CActiveDataProvider */
$this->pageTitle = 'Сборка';
$controller = $this;
$request = Yii::app()->request;

$gridId = 'grid-order-products-' . $model->id;
?>

<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'id' => $gridId,
    'enableSorting' => false,
    'enableHistory' => false,
    'dataProvider' => $positionsProvider,
    'summaryText' => '',

    'extraRowColumns' => ['event.name'],

    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span1'], 'value' => function ($data, $row) {
            /* @var OrderProductRecord $data */
            return $row + 1;
        }],
        ['name' => 'product_id', 'type' => 'raw', 'value' => function ($data) {
            /* @var OrderProductRecord $data */
            $product = $data->product;

            $result = CHtml::link($product->id, 'javascript:void(0)', [
                'data-toggle' => 'popover',
                'data-placement' => 'right',
                'data-trigger' => 'hover',
                'data-html' => 'true',
                'data-content' => $this->renderPartial('_popoverProduct', ['product' => $product], true),
            ]);

            return $result;
        }],
        [
            'name' => 'number',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var OrderProductRecord $data */
                return $data->number . ' ' . '(доступно ' . $data->warehouseNumberAvailable . ')';
            },
            'cssClassExpression' => function ($row, $data) {
                /* @var OrderProductRecord $data */
                return $data->number > $data->warehouseNumberAvailable ? 'error' : 'success';
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'callcenter_comment',
            'type' => 'ntext',
            'htmlOptions' => ['class' => 'span3'],
        ],
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'cacheData' => false,
            'url' => $this->createUrl('packaging/warehouse'),
            'rowIdColumn' => 'id',
            'cssClass' => 'tbrelational-column-warehouse',
            'value' => function ($data) {
                return CHtml::tag(
                    'span',
                    [
                        'class' => 'btn btn-warning',
                    ],
                    'Склад'
                );
            },
        ],
        [
            'name' => 'event.name',
            'type' => 'raw',
            'headerHtmlOptions' => ['class' => 'hide'],
            'htmlOptions' => ['class' => 'hide'],
            'value' => function ($data) {
                /* @var OrderProductRecord $data */
                return CHtml::link($data->event->id . ', ' . $data->event->name, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
                ]);
            },
        ],
    ],
]); ?>
