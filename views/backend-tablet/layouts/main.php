<?php
$user = Yii::app()->user;
?>
<!doctype html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div id="wrap">
    <!-- BEGIN TOP BAR -->
    <div id="top">
        <!-- .navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <?= CHtml::link(Yii::app()->name, ['/index'], ['class' => 'brand']) ?>
                    <div class="nav-collapse collapse">
                        <!-- .nav -->
                        <?php $this->widget('bootstrap.widgets.TbMenu', [
                            'items' => [
                                [
                                    'label' => 'Главная',
                                    'url' => ['/index'],
                                ],
                                [
                                    'label' => 'Упаковка',
                                    'url' => ['packaging/index'],
                                ],
                                [
                                    'label' => 'Выход',
                                    'url' => ['auth/logout'],
                                    'htmlOptions' => [
                                        'class' => 'pull-right',
                                    ],
                                ],
                            ],
                        ]) ?>
                        <!-- /.nav -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.navbar -->
    </div>
    <!-- END TOP BAR -->


    <div class="clearfix"></div>

    <!-- BEGIN HEADER.head -->
    <header class="head">
        <!-- ."main-bar -->
        <div class="main-bar">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <h3><i class="icon-home"></i>
                            <?= $this->pageTitle ?>
                        </h3>
                    </div>
                </div>
                <!-- /.row-fluid -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.main-bar -->
    </header>
    <!-- END HEADER.head -->

    <div id="content" class="">
        <!-- .outer -->
        <div class="container-fluid outer">
            <?php $this->widget('bootstrap.widgets.TbAlert', [
                'alerts' => [ // configurations per alert type
                    'error' => ['block' => true, 'fade' => true, 'closeText' => '×'], // success, info, warning, error or danger
                ],
            ]); ?>
            <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', [
                'links' => $this->breadcrumbs,
                'htmlOptions' => ['class' => 'body'],
            ]); ?>
            <div class="row-fluid">
                <div class="span12 inner">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- #push do not remove -->
    <div id="push"></div>
    <!-- /#push -->
    <div class="clearfix"></div>
</div>

<!-- BEGIN FOOTER -->
<div id="footer" style="padding-top: 40px;">
    <div class="container-fluid" style="position: fixed; bottom: 0; background: white; width: 100%;">
        <p><?= date('Y') ?> &copy; <?= Yii::app()->name ?></p>
    </div>
</div>
</body>
</html>
