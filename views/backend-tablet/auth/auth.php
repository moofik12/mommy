<?php

use MommyCom\Controller\BackendTablet\AuthController;
use MommyCom\Model\BackendTablet\AuthForm;

/**
 * @var $this AuthController
 * @var $modelForm AuthForm
 * @var $form CActiveForm
 */
?>

<div class="container">
    <div class="row">
        <div class="span4 offset4">
            <div class="signin">
                <div class="tab-content">
                    <div id="login" class="tab-pane active">

                        <?php $form = $this->beginWidget('CActiveForm', [
                            'id' => 'auth-form',
                            'focus' => [$modelForm, 'login'],
                        ]); ?>

                        <p class="muted tac">Введите логин и пароль</p>

                        <div class="control-group">
                            <?php echo $form->error($modelForm, 'password', ['class' => 'text-error']); ?>

                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-user"></i></span>
                                    <?= $form->textField($modelForm, 'login', ['placeholder' => 'Login']); ?>
                                </div>
                            </div>

                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-lock"></i></span>
                                    <?= $form->passwordField($modelForm, 'password', ['placeholder' => 'Password']); ?>
                                </div>
                            </div>
                        </div>

                        <?php $this->widget('bootstrap.widgets.TbButton', [
                            'label' => 'Вход',
                            'type' => 'success',
                            'buttonType' => 'submit',
                        ]); ?>

                        <?php $this->endWidget(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
