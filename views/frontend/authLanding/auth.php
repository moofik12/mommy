<?php

use MommyCom\Controller\Frontend\AuthLandingController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Product\GroupedProduct;

/**
 * @var $this AuthLandingController
 * @var $authForm AuthForm
 * @var $registrationForm UserRecord
 * @var $form CActiveForm
 * @var $tab string
 * @var $productsProvider CArrayDataProvider
 */

$this->pageTitle = $this->t('Login Registration');
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
?>

<div class="container landing-page">
    <?= CHtml::link($app->name, ['index/index', '#' => 'events'], ['class' => 'logo']) ?>
    <div class="landing-conteiner">
        <div class="subscribe">
            <!--<div class="subscribe-sale">-5%</div>-->
            <div class="subscribe-text">
                <p class="dark-pink text-center"><?= $this->t('The first shopping club for children and moms in the USA') ?></p>
                <p class="text-justify"><?= $this->t('Fashionable and high-quality goods for moms and toddlers with discounts of up to 70%') ?></p>
                <p class="dark-pink text-center size18"><?= $this->t('Welcome!') ?></p>
            </div>
            <div class="tabs">
                <div class="tab <?= $tab === 'login' ? 'active' : '' ?>"
                     data-tab="enter"><?= $this->t('Enter the site') ?></div>
                <div class="tab <?= $tab === 'registration' ? 'active' : '' ?>"
                     data-tab="registration"><?= $this->t('check in') ?></div>
            </div>
            <div class="landing-wrapper">
                <?php $this->renderPartial(
                    '_login',
                    [
                        'authForm' => $authForm,
                        'visible' => $tab === 'login',
                    ]
                ) ?>
            </div>
            <div class="landing-wrapper">
                <?php $this->renderPartial(
                    '_registration',
                    [
                        'registrationForm' => $registrationForm,
                        'visible' => $tab === 'registration',
                    ]
                ) ?>
            </div>
        </div>
    </div>
</div>
<div class="landing-goods">
    <div class="landing-list">
        <div class="landing-title"><?= $this->t('Today\'s sales, hurry up and buy!') ?></div>
        <ul>
            <?php foreach (array_slice($productsProvider->getData(), 0, 8) as $item): /* @var $item GroupedProduct */ ?>
                <li>
                        <span class="box">
                            <span class="image-wrapper">
                                <?= CHtml::image($item->product->logo->getThumbnail('small150')->url, '', [
                                    'width' => 97,
                                    'height' => 121,
                                ]) ?>
                            </span>
                            <span class="box-foot">
                                <span class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></span>
                                <span class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></span>
                            </span>
                        </span>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>
<div class="modals">
    <div class="modal hide" id="policy">
        <div class="modal-wrapper">
            <span class="close" data-dismiss="modal">x</span>
            <div class="modal-body">
                <p class="pc-title"><?= $this->t('privacy policy') ?></p>
                <p><?= $this->t('The Company operates on the principle of recognition of the information transmitted by the Customer, including personal data, confidential, and therefore does not allow the use of such information for purposes other than fulfillment of the contract of retail sale or other purposes determined by this privacy policy.') ?></p>
                <p><?= $this->t('When completing the Application on the website, the Client provides the following personal data:') ?></p>
                <ul>
                    <li><?= $this->t('Surname, First name;') ?></li>
                    <li><?= $this->t('E-mail address;') ?></li>
                    <li><?= $this->t('Contact phone number;') ?></li>
                    <li><?= $this->t('Delivery address of the products,') ?></li>
                </ul>
                <p><?= $this->t('expressly, explicitly and unconditionally give their consent to the processing of such data by the Company without a time limit, with a view to executing a contract of retail sale, informing the Client about the conditions of cooperation and promotion of goods on the market.') ?></p>
                <p><?= $this->t('The Company ensures the confidentiality of the client\'s personal data in accordance with the USA law regarding personal data, takes the necessary legal, organizational and technical measures to protect personal data from unauthorized or accidental access, destruction, modification, blocking, copying, distribution, as well as other illegal actions.') ?></p>
                <p><?= $this->t('The Customer agrees to provide the Company with their data for the purposes of executing the concluded contracts and the Privacy Policy and the Company\'s obligations under them to sell and deliver the Goods to the Customer. The Customer\'s consent is expressed in the indication of the information in the appropriate boxes when completing the Application. Consent to the processing of personal data may be withdrawn by sending a written notice to the Company specified in the Contacts.') ?></p>
                <p><?= $this->t('Within 7 days from the date of receipt of the notification, the available personal data will be destroyed and the processing terminated. The Customer agrees to the transfer by the Company of the information provided to the agents and third parties acting on the basis of the contract with the Company, in order to fulfill their obligations to the Customer.') ?></p>
                <p><?= $this->t('The Company is not responsible for the information provided by the Customer on the Site being used publicly.') ?></p>
                <p><?= $this->t('Being a user of the shopping club Mommy.COM, the user can receive periodic mailings (information, holidays and others) to the registered email address, as well as automatic confirmation of orders. Each user voluntarily chooses whether to receive it or not. We guarantee that your email address will not be used for spamming.') ?></p>
            </div>
        </div>
    </div>
</div>


<?php
//пока нет дизайна вывода сообщений через alert()
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;

if ($app->user->hasFlash('message')) {
    $cs->registerScript('alertMessage', '
        alert("' . $app->user->getFlash('message') . '");
    ', CClientScript::POS_READY);
}
?>
