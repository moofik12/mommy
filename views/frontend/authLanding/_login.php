<div class="enter <?= $visible ? '' : 'hide' ?>" id="enter">
    <?php $form = $this->beginWidget('CActiveForm', [
        'enableAjaxValidation' => false,
        'action' => $this->app()->createUrl('authLanding/index', ['name' => 'login']),
        'enableClientValidation' => true,
        'clientOptions' => [
            'submitting' => true,
            'validateOnSubmit' => true,
        ],
        //'focus' => array($authForm, 'email'),
    ]) ?>
    <?= $form->labelEx($authForm, 'email') ?>
    <div class="field <?= $authForm->hasErrors('email') ? 'error' : '' ?>">
        <?= $form->textField($authForm, 'email', ['placeholder' => $this->t('Your E-mail'), 'autocomplete' => 'off']) ?>
        <?= $form->error($authForm, 'email', ['class' => 'message']) ?>
    </div>
    <?= $form->labelEx($authForm, 'password') ?>
    <div class="field <?= $authForm->hasErrors('password') ? 'error' : '' ?>">
        <?= $form->passwordField($authForm, 'password', ['placeholder' => $this->t('your password')]) ?>
        <?= $form->error($authForm, 'password', ['class' => 'message']) ?>
    </div>
    <?= CHtml::submitButton($this->t('Login to the site')) ?>
    <div class="login-checkbox">
        <?= $form->checkBox($authForm, 'remember') ?>
        <?= $form->labelEx($authForm, 'remember') ?>
        <?php /* <span class="reminder" data-tab="forgot">Напомнить пароль</span>--> */ ?>
    </div>
    <?php $this->endWidget() ?>
    <?php /*    <!--
    <div class="login-line"><span>или</span></div>
    <div class="login-social">
        <a class="social-enter left active" href="#">Войти через Вконтакт</a>
        <a class="social-enter right" href="#">Войти через Facebook</a>
    </div>
    --> */ ?>
</div>
