<?php

use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var \MommyCom\YiiComponent\Frontend\FrontController $this
 * @var CActiveForm $form
 * @var Feedr $model
 * @var string $tabId
 * @var ShopShoppingCart $cart
 */

/** @var FeedrApi $feedrApi */
$feedrApi = $this->container->get(FeedrApi::class);

?>
<script>
    var feedrDomesticRates = (function () {
        var rates = {},
            lastId = 0,
            getRates = function (areaId) {
                if ('undefined' === typeof rates[areaId]) {
                    var dfd = $.Deferred();

                    $.ajax({
                        url: '<?= $this->createUrl('order/ajaxFeedrRates') ?>',
                        data: {areaId: areaId}
                    }).done(function (data) {
                        dfd.resolve(data);
                    });

                    rates[areaId] = dfd.promise();
                }

                return rates[areaId];
            },
            getData = function (areaId, type) {
                areaId = areaId || lastId;
                var dfd = $.Deferred();

                if (areaId) {
                    getRates(areaId).then(function (data) {
                        var results = {};
                        $.each(data[type], function (index, value) {
                            results[index] = {
                                val: value.id,
                                text: value.name
                            };
                        });

                        dfd.resolve(results);
                    });
                } else {
                    dfd.resolve({});
                }

                lastId = areaId;

                return dfd.promise();
            };

        return {
            clearData: function () {
                rates = {};
            },
            getRegularRatesWithCod: function (areaId) {
                return getData(areaId, 'regularWithCod');
            },
            getRegularRatesWithoutCod: function (areaId) {
                return getData(areaId, 'regularWithoutCod');
            }
        };
    })();

    $(function () {
        $("#<?= CHtml::activeId($model, 'cashOnDelivery') ?> input").on('change', function () {

            var select = $("#<?= CHtml::activeId($model, 'rateId') ?>").data("extselect");
            select.getDropdown().filterText("");
            select.val("");
            select.text("");
            select.enable();
            select.getDataProvider().setData({}, true);

            var handler = ($(this).val())
                ? feedrDomesticRates.getRegularRatesWithCod
                : feedrDomesticRates.getRegularRatesWithoutCod;

            handler().done(function (rates) {
                select.getDataProvider().setData(rates, true);
            });
        });
    });
</script>
<div class="hide" data-tab-for="<?= $tabId ?>">
    <div class="standard-block">
        <div class="standard-wrapper">
            <div class="standard-title">
                <label class="left-part"><?= $this->t('Where to deliver?') ?></label>
            </div>
            <div class="left-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'provinceId',
                        'data' => [
                            $feedrApi->getProvinces(), 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a province'),
                            'clearSelection' => false,
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var select = $("#' . CHtml::activeId($model, 'cityId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                    select.getDataProvider().reload();

                                    select = $("#' . CHtml::activeId($model, 'suburbId') . '").data("extselect");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);

                                    select = $("#' . CHtml::activeId($model, 'areaId') . '").data("extselect");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'provinceId', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'cityId',
                        'data' => [
                            $model->provinceId ? $feedrApi->getCities($model->provinceId) : [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a city'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'dataProvider' => [
                                'model' => 'ajax',
                                'ajax' => [
                                    'url' => $this->createUrl('order/ajaxFeedrCities'),
                                    'cache' => true,
                                    'data' => new CJavaScriptExpression('function(filter) {
                                        var select = $("#' . CHtml::activeId($model, 'provinceId') . '").data("extselect");
                                        return {
                                            filter: filter || "",
                                            provinceId: select.val()
                                        };
                                    }'),
                                    'results' => new CJavaScriptExpression('function(response) {
                                        var results = {};
                                        $.each(response, function(index, value) {
                                            results[index] = {
                                                val: value.id,
                                                text: value.name,
                                            };
                                        });
                                        return results;
                                    }'),
                                ],
                            ],
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var s, select = s = $("#' . CHtml::activeId($model, 'suburbId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                    select.getDataProvider().reload();

                                    select = $("#' . CHtml::activeId($model, 'areaId') . '").data("extselect");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'cityId', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'suburbId',
                        'data' => [
                            $model->cityId ? $feedrApi->getSuburbs($model->cityId) : [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a suburb'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'dataProvider' => [
                                'model' => 'ajax',
                                'ajax' => [
                                    'url' => $this->createUrl('order/ajaxFeedrSuburbs'),
                                    'cache' => true,
                                    'data' => new CJavaScriptExpression('function(filter) {
                                        var select = $("#' . CHtml::activeId($model, 'cityId') . '").data("extselect");
                                        return {
                                            filter: filter || "",
                                            cityId: select.val()
                                        };
                                    }'),
                                    'results' => new CJavaScriptExpression('function(response) {
                                        var results = {};
                                        $.each(response, function(index, value) {
                                            results[index] = {
                                                val: value.id,
                                                text: value.name,
                                            };
                                        });
                                        return results;
                                    }'),
                                ],
                            ],
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var s, select = s = $("#' . CHtml::activeId($model, 'areaId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                    select.getDataProvider().reload();
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'suburbId', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'areaId',
                        'data' => [
                            $model->suburbId ? $feedrApi->getAreas($model->suburbId) : [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select an area'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'dataProvider' => [
                                'model' => 'ajax',
                                'ajax' => [
                                    'url' => $this->createUrl('order/ajaxFeedrAreas'),
                                    'cache' => true,
                                    'data' => new CJavaScriptExpression('function(filter) {
                                        var select = $("#' . CHtml::activeId($model, 'suburbId') . '").data("extselect");
                                        return {
                                            filter: filter || "",
                                            suburbId: select.val()
                                        };
                                    }'),
                                    'results' => new CJavaScriptExpression('function(response) {
                                        var results = {};
                                        $.each(response, function(index, value) {
                                            results[index] = {
                                                val: value.id,
                                                text: value.name,
                                            };
                                        });
                                        return results;
                                    }'),
                                ],
                            ],
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var select = $("#' . CHtml::activeId($model, 'rateId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, true);
                                    if (!item) {
                                        return;
                                    }

                                    var
                                        promiseWithoutCod = feedrDomesticRates.getRegularRatesWithoutCod(item.val),
                                        promiseWithCod = feedrDomesticRates.getRegularRatesWithCod(item.val);

                                    if (!' . (int)$cart->canPrepay() . ') {
                                        promiseWithCod.done(function(rates) {
                                            $("#' . CHtml::activeId($model, 'cashOnDelivery') . ' input[value=1]")
                                                .prop("checked", true)
                                                .change()
                                                .attr("disabled", $.isEmptyObject(rates));
                                            $("#cashOnDeliveryInfo").text($.isEmptyObject(rates) ? "' . $this->t("Delivery to your region is temporary unavailable.") . '" : "");
                                        });

                                        return;
                                    }

                                    promiseWithoutCod.done(function(rates) {
                                        $("#' . CHtml::activeId($model, 'cashOnDelivery') . ' input[value=]")
                                            .prop("checked", true)
                                            .change()
                                            .attr("disabled", $.isEmptyObject(rates));
                                    });

                                    promiseWithCod.done(function(rates) {
                                        $("#' . CHtml::activeId($model, 'cashOnDelivery') . ' input[value=1]")
                                            .attr("disabled", $.isEmptyObject(rates));
                                    });

                                    $.when(promiseWithoutCod, promiseWithCod).done(function(ratesWithoutCod, ratesWithCod) {
                                        if ($.isEmptyObject(ratesWithoutCod)) {
                                            $("#cashOnDeliveryInfo").text("");
                                            return;
                                        }
                                        if ($.isEmptyObject(ratesWithCod)) {
                                            $("#cashOnDeliveryInfo").text("' . $this->t("Pay on delivery unavailable in your region.") . '");
                                            return;
                                        }
                                        var value = ratesWithoutCod[0].text.split(" - ")[2];
                                        $("#cashOnDeliveryInfo").text("' . $this->t("If you choose Prepay the delivery price would be ") . '" + value);
                                    });
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'areaId', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <?= $form->label($model, 'address', ['class' => 'left-part']) ?>
        </div>
        <div class="standard-wrapper">
            <div class="field textarea <?= $model->hasErrors('address') ? 'error' : '' ?> bottom">
                <?= $form->textArea($model, 'address', []) ?>
                <?= $form->error($model, 'address', ['class' => 'message']) ?>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Choose a payment type') ?></label>
        </div>
        <div class="standard-wrapper radio-list">
            <?= $form->radioButtonList($model, 'cashOnDelivery', [
                (string)false => $this->t('Prepay'),
                (string)true => $this->t('Pay on delivery'),
            ], [
                'template' => '<div class="radio-item">{input} {label}</div>',
                'separator' => "\n",
                'disabled' => 'disabled',
            ]) ?>
            <span id="cashOnDeliveryInfo"></span>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <?= $form->label($model, 'rateId', ['class' => 'left-part']) ?>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field <?= $model->hasErrors('rateId') ? 'error' : '' ?> bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'rateId',
                        'data' => [
                            [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a delivery service'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    if (!item) return;
                                    var price = parseInt(item.getSelectedText().split(\' - \')[2])
                                    var $container = $("li[data-delivery-price=true]");
                                    
                                    $container
                                        .find(".price")
                                        .removeClass("hide-i")
                                        .find("span")
                                        .data("value", price)
                                        .text(price);
                                    $container.find(".sublabel").text(item.getSelectedText().split(\' - \')[0]);
                                    $(".container .check").trigger("recalculate");
                                    
                                    $("#' . CHtml::activeId($model, 'rateVisualName') . '").val(item.getSelectedText());
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'rateId', ['class' => 'message']) ?>
                    <?= $form->hiddenField($model, 'rateVisualName') ?>
                </div>
            </div>
        </div>
    </div>
</div>
