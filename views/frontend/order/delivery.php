<?php

use MommyCom\Service\Delivery\DeliveryDirectory;
use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Deprecated\CurrencyName;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopOrder;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\LineAccount;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PhoneNumber;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var FrontController $this
 * @var ShopShoppingCart $cart
 * @var ShopBonusPoints $bonuspoints
 * @var string $promocode
 * @var ShopOrder $order
 * @var int $deliveryType
 * @var DeliveryInterface[] $availableDeliveries
 * @var string $countryCode
 */

$this->pageTitle = $this->t('Delivery method - Ordering');

/** @var MommyWebApplication $app */
$app = $this->app();
$splitTesting = $app->splitTesting;
/* @var $splitTesting SplitTesting */

$baseImgUrl = $app->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $currencyFormatter = $app->currencyFormatter;
$tm = $tokenManager = $app->tokenManager;

/** @var CurrencyName $currency */
$currency = $app->countries->getCurrency();

$userDiscountSaving = $cart->getUserTotalSaving();
$userDiscountAsBonus = $cart->getUserDiscountAsBonus();

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseAssetsUrl = $this->app()->getAssetManager()->publish(\Yii::getPathOfAlias('assets.front'));
$cs->registerCssFile($baseAssetsUrl . "/css/intlTelInput.css");
$cs->registerScriptFile($baseAssetsUrl . "/js/intlTelInput.js");

$orderPositions = [];
foreach ($cart->getPositions() as $position) {
    $orderPositions[] = $position->event_id . '/' . $position->product->product_id;
}

$hasChoose = (count($availableDeliveries) > 1);

?>
<script type="application/javascript">
    Mamam.currency = '<?= $cf->getCurrency() ?>';
    Mamam.ecommerce.checkout(<?= json_encode($orderPositions) ?>, 1);
</script>
<div class="container">
    <?php
    /* @var $form CActiveForm */
    $form = $this->beginWidget('CActiveForm', [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'afterValidate' => 'js:function($form, data, hasError) {
                if (!hasError) {
                    return true;
                }    
            
                if ($(".error").hasClass("intl-tel-input")) {
                    var trueParent = $(".error.intl-tel-input").parent();
                    trueParent.addClass("error");
                }
                
                $(document).trigger("hideErrors");
                
                if (typeof $(".error").first().offset() !== "undefined") {
                    $("html, body").animate({
                        scrollTop: $(".error").first().offset().top
                    }, 1000);
                }
                
                return false;
            }',
            'beforeValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                var flagElement = $(".selected-flag")[0];
                
                if (flagElement === document.activeElement) {
                    return false;
                }
                
                return true;
            }',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                $(document).trigger({
                    type: "mommy.hideErrors",
                    delay: 0
                });
                
                if (hasError) {
                    var trueParent = $(".intl-tel-input").parent();
                    if ($(".error").hasClass("intl-tel-input")) {
                        trueParent.addClass("error");
                    }
                    
                    $(document).trigger("hideErrors");
                }
            }',
        ],
        'focus' => [$order, PhoneNumber::NAME],
        'htmlOptions' => ['name' => 'delivery-form'],
    ]);
    ?>
    <div class="cart-form">
        <div class="ordering">
            <div class="styled-title">
                <?= $this->t('Delivery') ?>
                <div class="promotional-time">
                    <span class="style"><?= CHtml::tag(
                            'time',
                            [
                                'datetime' => $tf->formatMachine($cart->getTotalReservedTo()),
                                'data-countdown' => true,
                            ],
                            $tf->format($cart->getTotalReservedTo())
                        ) ?></span>
                </div>
            </div>
            <div class="white-place">
                <div class="standard-block <?= $hasChoose ? 'first' : 'hide' ?>">
                    <div class="standard-title">
                        <label class="left-part"><?= $this->t('Choose a shipping method') ?></label>
                    </div>
                    <ul class="delivery-service">
                        <?php
                        foreach ($availableDeliveries as $delivery) {
                            echo CHtml::tag(
                                'li',
                                [],
                                $form->radioButton($order, 'deliveryType', [
                                    'checked' => $delivery->getId() == $deliveryType,
                                    'value' => $delivery->getId(),
                                    'id' => 'deliveryType_' . $delivery->getId(),
                                ]) .
                                CHtml::label($this->t($delivery->getName()), 'deliveryType_' . $delivery->getId())
                            );
                        }
                        ?>
                    </ul>
                </div>
                <div class="standard-block <?= $hasChoose ? '' : 'first' ?>">
                    <?php if (isset($order->{PhoneNumber::NAME})) : ?>
                        <div id="phoneNumberBlock" class="standard-wrapper">
                            <div class="standard-title">
                                <label class="left-part"><?= $this->t('You phone number') . (isset($order->{LineAccount::NAME}) ? ' <a href="#" class="level">' . $this->t('or LINE account') . '</a>' : '') ?></label>
                                <label class="right-part"><?= $this->t('You can fill the delivery information') ?></label>
                            </div>
                            <div class="left-part">
                                <div class="field bottom <?= $order->hasErrors(PhoneNumber::NAME) ? 'error' : '' ?>">
                                    <?= $form->textField(
                                        $order,
                                        PhoneNumber::NAME
                                    ) ?>
                                    <?= $form->error($order, PhoneNumber::NAME, ['class' => 'message']) ?>
                                </div>
                            </div>
                            <div class="right-part">
                                <div class="quick-checkout-block">
                                    <span><?= $this->t('or') ?></span>
                                    <button type="submit"
                                            class="btn green btn-quick-checkout"><?= $this->t('Checkout now') ?></button>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($order->{LineAccount::NAME})) : ?>
                        <div id="lineAccountBlock"
                             class="standard-wrapper <?= isset($order->{PhoneNumber::NAME}) ? 'hide' : '' ?>">
                            <div class="standard-title">
                                <label class="left-part"><?= $this->t('You LINE account') . (isset($order->{PhoneNumber::NAME}) ? ' <a href="#" class="level">' . $this->t('or phone number') . '</a>' : '') ?></label>
                                <label class="right-part"><?= $this->t('You can fill the delivery information') ?></label>
                            </div>
                            <div class="left-part">
                                <div class="field bottom <?= $order->hasErrors(LineAccount::NAME) ? 'error' : '' ?>">
                                    <div class="intl-tel-input">
                                        <div class="flag-container">
                                            <div class="selected-flag">
                                                <div class="iti-flag line-logo"></div>
                                            </div>
                                        </div>
                                        <?= $form->textField($order, LineAccount::NAME, ['placeholder' => '@']) ?>
                                    </div>
                                    <?= $form->error($order, LineAccount::NAME, ['class' => 'message']) ?>
                                </div>
                            </div>
                            <div class="right-part">
                                <div class="quick-checkout-block">
                                    <span><?= $this->t('or') ?></span>
                                    <button type="submit"
                                            class="btn green btn-quick-checkout"><?= $this->t('Checkout now') ?></button>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <script>
                        $('#phoneNumberBlock').find('a.level').on('click', function (e) {
                            e.preventDefault();
                            $('#phoneNumberBlock')
                                .addClass('hide')
                                .find('input').val('');
                            $('#lineAccountBlock')
                                .removeClass('hide')
                                .find('input').focus();
                        });
                        $('#lineAccountBlock').find('a.level').on('click', function (e) {
                            e.preventDefault();
                            $('#lineAccountBlock')
                                .addClass('hide')
                                .find('input').val('');
                            $('#phoneNumberBlock')
                                .removeClass('hide')
                                .find('input').focus();
                        });
                        $('#lineAccountBlock').find('input').on('change, keyup', function (e) {
                            var val = $(this).val();
                            if (val && val.charAt(0) !== '@') {
                                $(this).val('@' + val);
                            }
                        })
                    </script>
                </div>
                <?php if (DeliveryDirectory::DUMMY !== $deliveryType): ?>
                    <div class="standard-block">
                        <div class="standard-title">
                            <label title="">
                                <?= $this->t('Anyway you can complete your order by contacting our manager via <a target="_blank" class="link-out" href="https://m.me/mommy.com.Indonesia">Facebook Messenger</a> or <a target="_blank" class="link-out" href="https://api.whatsapp.com/send?phone=6282147486726">Whatsapp</a>') ?>
                            </label>
                        </div>
                    </div>
                <?php endif; ?>
                    <div class="standard-block" data-block="info">
                        <div class="standard-title">
                            <?= $form->label($order, 'clientName', ['class' => 'left-part']) ?>
                            <?= $form->label($order, 'clientSurname', ['class' => 'right-part']) ?>
                        </div>
                        <div class="standard-wrapper">
                            <div class="left-part">
                                <div class="field bottom <?= $order->hasErrors('clientName') ? 'error' : '' ?>">
                                    <?= $form->textField($order, 'clientName') ?>
                                    <?= $form->error($order, 'clientName', ['class' => 'message']) ?>
                                </div>
                            </div>
                            <div class="right-part">
                                <div class="field bottom <?= $order->hasErrors('clientSurname') ? 'error' : '' ?>">
                                    <?= $form->textField($order, 'clientSurname') ?>
                                    <?= $form->error($order, 'clientSurname', ['class' => 'message']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php if (DeliveryDirectory::DUMMY !== $deliveryType): ?>
                    <div class="" data-block="info">
                        <?php
                        foreach ($availableDeliveries as $delivery) {
                            $deliveryModel = $delivery->createFormModel();
                            $this->renderPartial('_deliveryInfo' . $deliveryModel->getModelName(), [
                                'form' => $form,
                                'model' => $deliveryModel,
                                'tabId' => 'deliveryType_' . $delivery->getId(),
                                'cart' => $cart,
                            ]);
                        }
                        ?>
                        <div class="standard-block">
                            <div class="standard-title">
                                <?= $form->label($order, 'comment', ['class' => 'left-part']) ?>
                            </div>
                            <div class="standard-wrapper">
                                <div class="field textarea <?= $order->hasErrors('comment') ? 'error' : '' ?> bottom">
                                    <?= $form->textArea($order, 'comment', []) ?>
                                    <?= $form->error($order, 'comment', ['class' => 'message']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="show-standard-block hide-i" id="delivery-write" data-open="0">
                        <span class="style"><?= $this->t('Fill in the delivery information') ?></span>
                        <span class="small">(<?= $this->t('not necessarily, we can clarify by phone') ?>)</span>
                    </div>
                <?php else: ?>
                    <div class="" data-block="info">
                        <?php
                        foreach ($availableDeliveries as $delivery) {
                            $deliveryModel = $delivery->createFormModel();
                            $this->renderPartial('_deliveryInfo' . $deliveryModel->getModelName(), [
                                'form' => $form,
                                'model' => $deliveryModel,
                                'tabId' => 'deliveryType_' . $delivery->getId(),
                                'cart' => $cart,
                            ]);
                        }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="check">
            <div class="check-inner">
                <div class="check-title"><?= $this->t('Your check') ?></div>
                <ul>
                    <li data-products-price="true">
                        <div class="label">
                            <?= $this->t('Order price') ?>
                            <div class="sublabel">(<span
                                        class="items"><?= $cart->getOrdersReservedItemsCount() ?></span> <?= $this->t('thing', $cart->getOrdersReservedItemsCount()) ?>
                                )
                            </div>
                        </div>
                        <div class="price" style="color: #3e3e3e;"><span
                                    data-value="<?= $cart->getCost(false) ?>"><?= $cf->format($cart->getCost(false)) ?></span>
                        </div>
                    </li>

                    <li data-delivery-price="true">
                        <div class="label"><?= $this->t('Cost of delivery') ?>
                            <div class="sublabel"><?= $this->t('not selected') ?></div>
                        </div>
                        <div class="price hide-i">&#8776; <span data-value="0"></span> <?= $currency->getName() ?></div>
                    </li>

                    <li data-delivery-full="true" style="display: none;">
                        <div class="label"><?= $this->t('Amount') ?></div>
                        <div class="price"><span data-value="0">0</span> <?= $currency->getName() ?></div>
                    </li>

                    <li class="blank" data-promocode="true" data-token="<?= $tm->getToken('promocode') ?>">
                        <div class="label blank-title collapsed" data-target="#promocode-blank" data-toggle="collapse">
                            <span class="line"><?= $this->t('I have a promotional code') ?></span>
                        </div>
                        <div class="blank-info collapse width" id="promocode-blank">
                            <div class="label blank-info">
                                <div class="sublabel">
                                    <div class="field bottom <?= $order->hasErrors('promocode') ? 'error' : '' ?>">
                                        <?= $form->textField($order, 'promocode', ['data-promocode-input' => 1]) ?>
                                        <?= $form->error($order, 'promocode', ['class' => 'message']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="blank-info price bonus"><span data-value="0">0</span> <?= $cf->getCurrency() ?>
                            </div>
                        </div>
                    </li>

                    <li class="help-hover" data-bonuspoints="true"
                        data-bonuspoints-total="<?= $cart->getBonusesCost() ?>">
                        <div class="label">
                            <?= $this->t('Bonuses') ?>
                            <?php if ($bonuspoints->availableToSpend > 0 && $cart->getBonusesCost() == 0): ?>
                                <div class="sublabel">
                                    <a class="why"
                                       href="javascript:void(0)"><?= $this->t('Why are your bonuses not being used?') ?></a>
                                    <div class="help-message">
                                        <?php /*<!--<div class="title">Бонусы могут быть использованы для:</div>--> */ ?>
                                        <div class="text"><?= $this->t('The bonus system does not apply to promotions marked as Fast Delivery - the prices for them are already reduced to the maximum') ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="price bonus"><span
                                    data-value="-<?= $cart->getBonusesCost() ?>"><?= $cf->format($cart->getBonusesCost()) ?></span>
                        </div>
                    </li>

                    <li data-user-discount="true" class="<?= $userDiscountSaving > 0 ? '' : 'hide' ?>">
                        <div class="label"><?= $this->t('A discount') ?></div>
                        <div class="price bonus"><span
                                    data-value="-<?= $userDiscountSaving ?>"><?= $userDiscountSaving ?></span> <?= $currency->getName() ?>
                        </div>
                    </li>

                    <li data-user-discount-as-bonus="true" class="<?= $userDiscountAsBonus > 0 ? '' : 'hide' ?>">
                        <div class="label"><?= $this->t('Will be credited for bonuses') ?></div>
                        <div class="price bonus"><span
                                    data-value="0"><?= $userDiscountAsBonus ?></span> <?= $currency->getName() ?>
                        </div>
                    </li>

                    <li data-delivery-total="true">
                        <div class="label"><?= $this->t('Total') ?></div>
                        <div class="price end-price" style="color: #3e3e3e;"><span data-value="0">0</span></div>
                    </li>
                </ul>
            </div>
            <div class="check-footer"></div>
            <div class="check-buttons">
                <button style="width: 100%" type="submit"
                        class="btn green btn-checkout"><?= $this->t('Checkout') ?></button>
            </div>
        </div>
    </div>
    <div class="cart-form-footer shortfoot">
        <a class="back"
           href="<?= $app->createUrl('order/index', ['promocode' => $promocode] + $splitTesting->getAvailable()) ?>"><?= $this->t('Back to the list') ?></a>
    </div>
    <?php $this->endWidget() ?>
    <div class="shortfoot">
        <div class="achieve middle">
            <ul>
                <li>
                    <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-back-guarantee.png' ?>"
                         alt="<?= $this->t('Refund Guarantee') ?>">
                    <div class="achieve-text-wrapper">
                        <div class="achieve-text">
                            <?= $this->t('Refund Guarantee') ?>
                        </div>
                    </div>
                </li>
                <li>
                    <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-daily-updates.png' ?>"
                         alt="<?= $this->t('Excellent quality of goods') ?>">
                    <div class="achieve-text-wrapper">
                        <div class="achieve-text">
                            <?= $this->t('Excellent quality of goods') ?>
                        </div>
                    </div>
                </li>
                <li>
                    <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-best-price.png' ?>"
                         alt="<?= $this->t('Guaranteed low prices') ?>">
                    <div class="achieve-text-wrapper">
                        <div class="achieve-text">
                            <?= $this->t('Low prices guaranteed ') ?>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#delivery-write').on('click', function () {
            if (parseInt($(this).attr('data-open')) === 1) {
                $(this).attr('data-open', 0);
                $(this).html('<span class="style"><?= $this->t('Hide delivery information') ?></span>');
                $("[data-block='info']").removeClass('hide');
            } else {
                $(this).attr('data-open', 1);
                $(this).html('<span class="style"><?= $this->t('Fill in the delivery information') ?></span><span class="small">(<?= $this->t('not necessarily, we can clarify by phone') ?>)</span>');
                $("[data-block='info']").addClass('hide');
            }
        });

        $('[name="delivery-form"]').on('submit', function (e) {
            var countryCode = $("#ShopOrder_telephone").intlTelInput("getSelectedCountryData").dialCode;
            $(this).append('<input type="hidden" name="ShopOrder[countryCode]" value="' + countryCode + '">');

            return true;
        });

        $('#ShopOrder_telephone').intlTelInput({
            autoPlaceholder: 'off',
            preferredCountries: [],
            initialCountry: '<?= $countryCode ?>',
            utilsScript: '<?= $baseAssetsUrl . "/js/intlTelInput.utils.js"?>'
        });

        var timer = null;

        function clearErrors() {
            $('.error .message').css('display', 'none');
            $('.error').removeClass('error');
        }

        $(document).on('hideErrors', function () {
            if (null !== timer) {
                clearTimeout(timer);
            }

            timer = setTimeout(clearErrors, 3500);
        });
    });
</script>
