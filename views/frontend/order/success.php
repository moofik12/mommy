<?php

use MommyCom\Controller\Frontend\OrderController;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\PaymentGateway\PaymentGatewayInterface;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

/**
 * @var OrderController $this
 * @var OrderRecord $order
 * @var string $uid
 * @var bool $canPrepay
 * @var PaymentGatewayInterface[] $availableGateways
 */

$this->pageTitle = $this->t('Success - Order');

/** @var \MommyCom\YiiComponent\Application\MommyWebApplication $app */
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$countRelationOrders = count($order->getRelationOrders());
$baseImgUrl = $app->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$linkToPay = $app->createUrl('pay/index', ['uid' => $order->id]);
$linkToPayOrders = $app->createUrl('pay/index', ['uid' => $order->uuid]);
$splitTesting = $app->splitTesting;

/** @var $user ShopWebUser */
$user = $app->user;

$orderPositions = [];
foreach ($order->positions as $position) {
    $orderPositions[$position->event_id . '/' . $position->product->product_id] = [
        'id' => $orderPositions[] = $position->event_id . ' / ' . $position->product->product_id,
        'name' => $position->product->product->name,
        'brand' => $position->product->product->brand->name,
        'category' => $position->product->product->category,
        'variant' => $position->product->size,
        'price' => $position->price,
        'quantity' => $position->number,
    ];
}
?>
<script>
    Mamam.ecommerce.products.setCache(<?= json_encode($orderPositions, JSON_UNESCAPED_UNICODE) ?>);
    Mamam.ecommerce.purchase(<?= json_encode(array_keys($orderPositions)) ?>, <?= $order->id ?>);
    $(document).trigger($.Event('mamam.purchase.success', {
        totalPrice: <?= $order->getPrice() ?>,
        purchases: <?= json_encode($orderPositions) ?>
    }));
</script>

<div class="container">
    <div class="standard-message">
        <div class="message-wrapper">
            <div class="message-inner">
                <div class="order-success-img"></div>
                <div class="order-message message-title">
                    <?php if ($countRelationOrders == 0) : ?>
                        <?= $this->t('Your order was successfully processed') ?>
                    <?php else: ?>
                        <?= $this->t('Your orders are successfully processed') ?>
                    <?php endif; ?>
                </div>
                <div class="order-success-text">
                    <?php if ($countRelationOrders == 0) : ?>
                        <?= $this->t('Your order will be accepted for fulfilment after confirmation on the specified phone.') ?>
                    <?php else: ?>
                        <?= $this->t('Your orders will be accepted for execution after confirmation on the specified phone.') ?>
                    <?php endif; ?>
                    <?= $canPrepay ? $this->t('Also, we remind you that <strong> to save your funds you can make a prepayment with</strong>') : '' ?>
                </div>
                <div class="paid-block no-select">
                    <?php
                    foreach ($canPrepay ? $availableGateways : [] as $availableGateway) {
                        echo $availableGateway->render($this, $uid);
                    }
                    ?>
                </div>
                <div class="message-footer order-success-footer no-select">
                    <a class="btn gray btn-center"
                       href="<?= $app->createUrl('account/orders') ?>"><?= $this->t('My orders') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
