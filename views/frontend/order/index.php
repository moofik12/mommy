<?php

use MommyCom\Model\Db\OrderDiscountCampaignRecord;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCartOrderOwn;

/* @var $this FrontController */
/* @var $cart ShopShoppingCart */
/* @var $bonuspoints ShopBonusPoints */
/* @var $returnUrl string */
/* @var $deliveryToken string */
/* @var $othersProvider CArrayDataProvider */
/* @var $bonusAmount float */
/* @var $discountAmount float */
/* @var $promocode string */
/* @var $cost string */
/* @var $promocodeSavings string */
/* @var $costWithPromocode string */
/* @var $remainToFreeDelivery integer|false */
/* @var $costTotal float */

$this->pageTitle = $this->t('List of products - Order');
$app = $this->app();
$splitTesting = $app->splitTesting;
/* @var $splitTesting \MommyCom\Service\SplitTesting */

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$countPositionsAsOrders = count($cart->getPositionsAsOrders());
?>
<div class="container">
    <form>
        <?php foreach ($cart->getPositionsAsOrders() as $key => $cartOrder): ?>
            <?php
            $orderId = $key + 1;
            $orderCost = $cartOrder->getCost(false);
            $orderPromocodeSavings = $cartOrder->getPromocodeSavings($promocode);
            $orderCostWithPromocode = $orderCost - $orderPromocodeSavings;
            $orderDiscountCampaign = $cartOrder->getOrderDiscountCampaign();
            $discountCampaignText = '';

            if ($orderDiscountCampaign
                && $cartOrder->getReservedCount() > 0
                && $cartOrder->isEnableOrderDiscountCampaign(true, true)
                && $orderDiscountCampaign->getAmountNotEnoughUseCampaign($orderCost) > 0) {
                $type = $orderDiscountCampaign->getMayBeUsedType($orderCost);
                $amountNotEnoughUseCampaign = $orderDiscountCampaign->getAmountNotEnoughUseCampaign($orderCost);

                if ($type == OrderDiscountCampaignRecord::USED_PERCENT) {
                    $discountCampaignText = $this->t(
                        '<div class="more-discount">for a discount <span class="green">{percent}%</span> <span class="red">{amount} {currency}</span> left</div> ',
                        ['{percent}' => $orderDiscountCampaign->percent, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
                } elseif ($type == OrderDiscountCampaignRecord::USED_CASH) {
                    $discountCampaignText = $this->t(
                        '<div class="more-discount">for a discount <span class="green">{cash} {currency}</span> <span class="red">{amount} {currency}</span> left</div>',
                        ['{cash}' => $orderDiscountCampaign->cash, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
                }
            }

            ?>
            <div class="cart-title <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>">
                <?= $this->t('Order №') ?><?= $orderId ?>
                (<?= CHtml::encode($this->t($cartOrder->getDisplayName())) ?>)
                <?php if (!$cartOrder->isOwn): ?>
                    <a class="icon-fast-delivery" target="_blank"
                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                       title="<?= $this->t("What is 'Fast Delievery'?") ?>"></a>
                <?php endif; ?>
                <?php if ((!$cartOrder->isOwn || $countPositionsAsOrders > 1) && !$cartOrder->isEnoughToBuy() && $cartOrder->getReservedItemsCount() > 0): ?>
                    <div class="cart-form-not-enough">
                        <span>
                            <?= $this->t('Minimum order amount') ?> <?= $cf->format($cartOrder->getMinOrderAmount()) ?>
                        </span>
                    </div>
                <?php endif; ?>
            </div>
            <table class="cart-goods">
                <thead>
                <tr>
                    <th class="image"><?= $this->t('Product') ?></th>
                    <th class="about"><?= $this->t('Description') ?></th>
                    <th class="reserve"><?= $this->t('To complete the booking') ?></th>
                    <th class="price"><?= $this->t('Number and cost') ?></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <td class="image <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>"
                        colspan="2"><span
                                class="number"><?= $cartOrder->getItemsCount() ?></span> <?= $this->t('goods | goods |', $cartOrder->getItemsCount()) ?> <?= $this->t('for purchase') ?>
                    </td>
                    <td class="price <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>"
                        colspan="2">
                        <?php if ($cartOrder->getBonusesCost() > 0): ?>
                            <div class="price-sale">
                                <?= $this->t('Offers') ?>: <?= $cf->format($cartOrder->getBonusesCost()) ?>
                            </div>
                        <?php endif ?>
                        <?php if ($promocodeSavings > 0): ?>
                            <div class="price-sale">
                                <?= $this->t('A discount') ?>: <?= $cf->format($orderPromocodeSavings) ?>
                            </div>
                        <?php endif ?>
                        <div class="special-price">
                            <?php if ($remainToFreeDelivery !== false && $remainToFreeDelivery <= 0 && $orderCostWithPromocode > 0): ?>
                                <?php if ($promocodeSavings > 0): ?>
                                    <span class="price-label"><?= $this->t('Итого, с учетом скидки') ?>:</span>
                                <?php else: ?>
                                    <span class="price-label"><?= $this->t('Total') ?>:</span>
                                <?php endif; ?>
                            <?php else: ?>
                                <span class="price-label"><?= $this->t('Total') ?>:</span>
                            <?php endif ?>
                            <span class="price-value">
                                <span class="in-total"><span
                                            class="price-total"><?= $cf->format($cartOrder->getCost(true, $promocode)) ?></span></span>
                            </span>
                        </div>
                        <?php if ($remainToFreeDelivery !== false && $orderCostWithPromocode > 0): ?>
                            <?php if ($remainToFreeDelivery > 0): ?>
                                <div class="special-price">
                                    <span class="price-label"><?= $this->t('До бесплатной доставки осталось') ?>
                                        :</span>
                                    <span class="price-value">
                                        <span class="in-total">
                                            <span class="price-total"><?= $cf->format($remainToFreeDelivery) ?></span>
                                        </span>
                                    </span>
                                </div>
                            <?php else: ?>
                                <div class="special-price">
                                    <span class="price-value">
                                        <span class="in-total">
                                            <span class="price-total"><?= $this->t('Free shipping') ?></span>
                                        </span>
                                    </span>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <?php if ($discountCampaignText) : ?>
                            <?= $discountCampaignText ?>
                        <?php endif ?>

                    </td>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($cartOrder->getPositions() as $position): ?>
                    <tr class="separator">
                        <td colspan="4"></td>
                    </tr>
                    <tr data-position-price="<?= $position->product->price ?>"
                        data-position-number="<?= $position->number ?>"
                        data-position-event="<?= $position->event_id ?>"
                        data-position-product="<?= $position->product->product_id ?>"
                        data-position-size="<?= $position->product->size ?>"
                        data-position-token="<?= $this->app()->tokenManager->getToken($position->product->product_id) ?>"
                    >
                        <td class="image">
                            <?= CHtml::link(
                                CHtml::image($position->product->product->logo->getThumbnail('small70')->url),
                                ['product/index', 'id' => $position->product->product_id, 'eventId' => $position->event_id],
                                ['class' => 'title', 'data-toggle' => 'modal', 'data-target' => "#cart-img-{$position->product->product_id}"]
                            ) ?>
                        </td>
                        <td class="about">
                            <div class="about-inner">
                                <a class="item-link"
                                   href="<?= $app->createUrl('product/index', ['id' => $position->product->product_id, 'eventId' => $position->event_id, '#' => 'content']) ?>">
                                    <?= CHtml::encode($position->product->product->name) ?>
                                </a>
                                <a class="item-cat-link"
                                   href="<?= $app->createUrl('event/index', ['id' => $position->event_id, '#' => 'content']) ?>">
                                    <?= CHtml::encode($position->event->name) ?>
                                </a>
                                <?php if (!empty($position->product->color) && !empty($position->product->size)): ?>
                                    <ul>
                                        <?php if (!empty($position->product->color)): ?>
                                            <li><?= $this->t('Colour') ?>: <span
                                                        class="color"><?= $position->product->color ?></span></li>
                                        <?php endif ?>

                                        <?php if (!empty($position->product->size)): ?>
                                            <li><?= $this->t('Size') ?>: <span
                                                        class="size"><?= $position->product->size ?></span></li>
                                        <?php endif ?>
                                    </ul>
                                <?php endif ?>
                            </div>
                        </td>
                        <td class="reserve <?= $position->isExpired ? 'cross-out' : '' ?>">
                            <div class="extend">
                                <span class="time-out"><?= $this->t('time is over') ?></span>
                                <?= CHtml::tag(
                                    'a',
                                    [
                                        'id' => 'update1' . $position->id,
                                        'href' => '#',
                                        'class' => 'extend-btn',
                                        'data-is-update-btn' => 'true',
                                    ],
                                    $this->t('prolong the reservation')
                                ) ?>
                            </div>
                            <?= CHtml::tag(
                                'time',
                                ['datetime' => $tf->formatMachine($position->reservedTo), 'data-countdown' => true],
                                $tf->format($position->reservedTo)
                            ) ?>
                        </td>
                        <td class="price">
                            <div class="price-wrapper">
                                <div class="price-inner-wrapper <?= $position->isExpired ? 'cross-out' : '' ?>">
                                    <div class="price-inner">
                                        <span class="our-price">
                                            <span class="item-price"><span
                                                        class="number"><?= $cf->format($position->price) ?></span></span>
                                            <span class="el">x</span>
                                            <span class="number"><?= $position->number ?></span>
                                            <span class="el">=</span>
                                            <span class="result"><span
                                                        class="number"><?= $cf->format($position->totalPrice) ?></span></span>
                                        </span>
                                    </div>
                                    <span class="extend">
                                        <?= CHtml::tag(
                                            'a',
                                            [
                                                'id' => 'update2' . $position->id,
                                                'href' => '#',
                                                'data-is-update-btn' => 'true',
                                            ],
                                            $this->t('prolong the reservation')
                                        ) ?>
                                    </span>
                                </div>
                                <?= CHtml::ajaxLink(
                                    '',
                                    [
                                        'cart/numberDown',
                                        'eventId' => $position->event_id,
                                        'productId' => $position->product->product_id,
                                        'size' => $position->product->size,
                                    ],
                                    [
                                        'update' => '.supreme-inner',
                                        'complete' => 'function() {
                                            Mamam.ecommerce.remove("' . $position->event_id . '/' . $position->product->product_id . '", 1);
                                            Mamam.cart.reloadQuickCart();
                                        }',
                                    ],
                                    [
                                        'class' => 'minus',
                                        'id' => 'numdown' . $position->id,
                                    ]
                                ) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        <?php endforeach; ?>
        <?php if ($countPositionsAsOrders > 1): ?>
            <div class="cart-footer">
                <div class="cart-number">
                    <span class="number"><?= $cart->getOrdersReservedItemsCount() ?></span> <?= $this->t('goods | goods |', $cart->getOrdersReservedItemsCount()) ?> <?= $this->t('for purchase') ?>
                </div>
                <div class="price">
                    <?php if ($bonusAmount): ?>
                        <div class="price-bonus"><?= $this->t('Offers') ?>
                            : <?= $bonusAmount ?> <?= $cn->getName('short') ?></div>
                    <?php endif ?>
                    <?php if ($promocodeSavings > 0): ?>
                        <div class="price-sale"><?= $this->t('A discount') ?>
                            : <?= $cf->format($promocodeSavings) ?></div>
                    <?php endif ?>
                    <div class="special-price">
                        <?php if ($remainToFreeDelivery !== false && $remainToFreeDelivery <= 0 && $costWithPromocode > 0): ?>
                            <?php if ($promocodeSavings > 0): ?>
                                <span class="price-label"><?= $this->t('Итого, с учетом скидки') ?>:</span>
                            <?php else: ?>
                                <span class="price-label"><?= $this->t('Total') ?></span>
                            <?php endif; ?>
                        <?php else: ?>
                            <span class="price-label"><?= $this->t('Total') ?></span>
                        <?php endif ?>
                        <span class="price-value">
                        <span class="in-total"><span class="price-total"><?= $cf->format($costTotal) ?></span></span>
                    </span>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="cart-form-footer">
            <?php if ($countPositionsAsOrders == 1 && $cart->hasPossibleOwnOrderNotEnoughToBuy() && $cart->getReservedItemsCount() > 0): ?>
                <div class="cart-form-not-enough">
                    <span>
                        <?= $this->t('Minimum order amount') ?> <?= $cf->format(ShopShoppingCartOrderOwn::MIN_ORDER_AMOUNT) ?>
                    </span>
                </div>
            <?php endif; ?>
            <a class="back <?= !$cart->isEnoughToBuy() ? 'green' : '' ?>"
               href="<?= $returnUrl ?>"><?= $this->t('Continue shopping') ?></a>
            <?php if ($app->user->isGuest): ?>
                <?php if ($cart->isAvailableToBuy()): ?>
                    <a class="btn green btn-checkout right" href="<?= $app->createUrl('order/auth') ?>"><?= $this->t('Checkout') ?></a>
                <?php elseif (!$cart->isEnoughToBuy() && $cart->getReservedItemsCount() > 0): ?>
                    <a class="btn block right" href="javascript:void(0);"><?= $this->t('Checkout') ?></a>
                <?php endif; ?>
            <?php else: ?>
                <?php if ($cart->isAvailableToBuy()): ?>
                    <a class="btn green btn-checkout right"
                       href="<?= $app->createUrl('order/delivery', ['promocode' => $promocode, '#' => 'content'] + $splitTesting->getAvailable()) ?>">
                        <?= $this->t('Checkout') ?></a>
                <?php elseif (!$cart->isEnoughToBuy() && $cart->getReservedItemsCount() > 0): ?>
                    <a class="btn block right" href="javascript:void(0);"><?= $this->t('Checkout') ?></a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="achieve">
            <ul>
                <li>
                    <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-back-guarantee.png' ?>"
                         alt="<?= $this->t('Refund Guarantee') ?>">
                    <div class="achieve-text-wrapper">
                        <div class="achieve-text">
                            <?= $this->t('Refund Guarantee') ?>
                        </div>
                    </div>
                </li>
                <li>
                    <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-daily-updates.png' ?>"
                         alt="<?= $this->t('Excellent quality of goods') ?>">
                    <div class="achieve-text-wrapper">
                        <div class="achieve-text">
                            <?= $this->t('Excellent quality of goods') ?>
                        </div>
                    </div>
                </li>
                <li>
                    <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-best-price.png' ?>"
                         alt="<?= $this->t('Guaranteed low prices') ?>">
                    <div class="achieve-text-wrapper">
                        <div class="achieve-text">
                            <?= $this->t('Low prices guaranteed ') ?>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </form>

    <?php if ($this->beginCache('cart-product-others', ['duration' => 10, 'varyByLanguage' => true, 'varyBySession' => true])) { ?>
        <?php if ($othersProvider->totalItemCount >= 6): ?>
            <div class="lined-header cart-line">
                <span class="title"><?= $this->t('People who bought this product also bought') ?></span>
                <div class="solid"></div>
            </div>
            <div class="other-goods-box">
                <ul class="other-goods cart-other-goods">
                    <?php foreach (array_slice($othersProvider->getData(), 0, 6) as $item): /* @var $item GroupedProduct */ ?>
                        <li>
                            <a href="<?= $app->createUrl('product/index', ['id' => $item->product->id, 'eventId' => $item->event->id]) ?>">
                                <span class="box">
                                    <span class="image-wrapper">
                                        <?= CHtml::image($item->product->logo->getThumbnail('small150')->url) ?>
                                    </span>
                                    <span class="text-wrapper">
                                        <span class="title"><?= CHtml::encode($item->product->name) ?></span>
                                        <span class="price">
                                            <span class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></span>
                                            <span class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        <?php endif ?>
        <?php $this->endCache();
    } ?>
</div>
<div class="modals">
    <?php foreach ($cart->getPositions() as $position): ?>
        <div class="modal hide cart-img" id="cart-img-<?= $position->product->product_id ?>">
            <div class="modal-wrapper">
                <div class="big-image-container">
                    <?= CHtml::image($position->product->product->logo->getThumbnail('mid380')->url) ?>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
