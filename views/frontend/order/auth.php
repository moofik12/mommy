<?php

use MommyCom\Controller\Frontend\OrderController;
use MommyCom\Model\Form\RegistrationSimpleForm;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var $this OrderController
 * @var $registrationForm RegistrationSimpleForm
 * @var $percent int
 */

$this->pageTitle = $this->t('Choose the amount of your order');
$app = $this->app();
$splitTesting = $app->splitTesting;
/* @var $splitTesting \MommyCom\Service\SplitTesting */

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
/* @var ShopShoppingCart $cart */
$cart = $app->user->cart;
$sumOrder = $cart->getCost(false, null, true, false);
$sumOrderWithPercent = $sumOrder - $sumOrder * $percent / 100;
?>
<div class="pre-registration-block">
    <?php $form = $this->beginWidget('CActiveForm', [
        'action' => $this->app()->createUrl('order/auth'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'afterValidate' => 'js:function($form, data, hasError) {
                if (hasError) {
                    if (!$form.find("input[type=radio]:checked").length) {
//                        $("#pre-modal-info").modal("show");
                    }
                    return;
                }

                $(document).trigger("mamam.registration.success");

                return true;
            }',
        ],
        'focus' => [$registrationForm, 'email'],
        'id' => 'pre-form-reg',
    ]) ?>
    <div class="pre-registration-inner pre-big">
        <div class="pre-registration-image"></div>
        <h1 class="pre-registration-title"
            style="display: none;"><?= $this->t('Choose the amount of your order') ?></h1>
        <div class="pre-registration-content">
            <div class="pre-registration-item1 no-select" style="display: none;">
                <div class="pre-radio-item">
                    <input id="pre-radio1" value="" type="radio" name="pre-radio">
                    <label for="pre-radio1">
                        <span class="radio-space">
                            <span class="title"><?= $cf->format($sumOrder, $cn->getName('short')) ?></span>
                         </span>
                    </label>
                </div>
            </div>
            <div class="pre-registration-item2 active no-select" style="display: none;">
                <div class="pre-radio-item">
                    <input id="pre-radio2" value="" type="radio" name="pre-radio">
                    <label for="pre-radio2">
                        <span class="pre-registration-sale">
                            <span class="text1"><?= $this->t('a discount') ?></span>
                            <span class="text2">-<?= $percent ?>%</span>
                        </span>
                        <span class="radio-space">
                            <span class="title"><?= $cf->format($sumOrderWithPercent, $cn->getName('short')) ?></span>
                        </span>
                    </label>
                </div>
            </div>
            <div class="pre-registration-reg" id="pre-block-reg">
                <p class="text1"><?= $this->t('This discount is available to new members of the MOMMY.COM Club') ?></p>
                <p class="text2"><?= $this->t('Join now! Enter your e-mail address:') ?></p>
                <div class="field bottom">
                    <?= $form->textField($registrationForm, 'email', ['placeholder' => $this->app()
                        ->params['validatorRegex']['email']['default']['placeholder']]) ?>
                    <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="pre-registration-footer">
        <button class="pre-btn no-select" type="submit" id="pre-registration"><?= $this->t('Next') ?></button>
        <a class="pre-link" href="javascript:void(0);"
           id="pre-auth"><?= $this->t('I already have an account') ?></a>
    </div>
    <?php $this->endWidget() ?>
</div>
<aside class="modals">
    <div class="pre-modal modal hide" id="pre-modal">
        <div class="modal-wrapper">
            <div class="modal-inner">
                <p class="text-modal"><?= $this->t('Sorry, but these items are available for order <br>only <a href="javascript:void(0);" id="pre-btn-modal1">to members of the Mommy club</a>') ?></p>
                <button class="pre-btn-modal" id="pre-btn-modal2">OK</button>
            </div>
        </div>
    </div>
    <div class="pre-modal modal hide" id="pre-modal-info">
        <div class="modal-wrapper">
            <div class="modal-inner">
                <p class="text-modal"><?= $this->t('Choose the amount of your order') ?></p>
                <button class="pre-btn-modal" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</aside>
