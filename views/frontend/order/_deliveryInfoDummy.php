<?php

use MommyCom\Service\Delivery\FormModel\Dummy;

/**
 * @var CActiveForm $form
 * @var Dummy $model
 * @var string $tabId
 */

?>
<div class="hide" data-tab-for="<?= $tabId ?>">
    <div class="standard-block">
        <div class="standard-title">
            <?= $form->label($model, 'address', ['class' => 'left-part']) ?>
        </div>
        <div class="standard-wrapper">
            <div class="field textarea <?= $model->hasErrors('address') ? 'error' : '' ?> bottom">
                <?= $form->textArea($model, 'address', []) ?>
                <?= $form->error($model, 'address', ['class' => 'message']) ?>
            </div>
        </div>
    </div>
</div>
