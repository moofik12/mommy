<?php

use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/* @var $this FrontController */
/* @var $cart ShopShoppingCart */
/* @var $bonuspoints ShopBonusPoints */
/* @var $returnUrl string */
/* @var $deliveryToken string */
/* @var $othersProvider CArrayDataProvider */
/* @var $bonusAmount float */
/* @var $discountAmount float */
/* @var $promocode string */
/* @var $cost string */
/* @var $promocodeSavings string */
/* @var $costWithPromocode string */
/* @var $remainToFreeDelivery integer|false */
/* @var $costTotal float */

$this->pageTitle = $this->t('List of products - Order');
$app = $this->app();
/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
?>
<div class="container">
    <div class="cart-clear">
        <div class="cart-clear-img">
            <img width="222" height="115" src="<?= $baseImgUrl . 'cart-clear.svg' ?>"
                 alt="<?= $this->t('Empty shopping cart') ?>"/>
        </div>
        <p><?= $this->t('There\'s nothing here yet') ?></p>
        <p><?= $this->t('But it can be fixed!') ?></p>
        <a class="back green" href="<?= $returnUrl ?>"><?= $this->t('Continue shopping') ?></a>
    </div>
    <div class="achieve">
        <ul>
            <li>
                <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-back-guarantee.png' ?>"
                     alt="<?= $this->t('Refund Guarantee') ?>">
                <div class="achieve-text-wrapper">
                    <div class="achieve-text"><?= $this->t('Refund Guarantee') ?></div>
                </div>
            </li>
            <li>
                <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-daily-updates.png' ?>"
                     alt="<?= $this->t('Excellent quality of goods') ?>">
                <div class="achieve-text-wrapper">
                    <div class="achieve-text"><?= $this->t('Excellent quality of goods') ?></div>
                </div>
            </li>
            <li>
                <img class="achieve-icon" src="<?= $baseImgUrl . 'advantage-best-price.png' ?>"
                     alt="<?= $this->t('Guaranteed low prices') ?>">
                <div class="achieve-text-wrapper">
                    <div class="achieve-text"><?= $this->t('Low prices guaranteed ') ?></div>
                </div>
            </li>
        </ul>
    </div>
</div>
