<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Widget\Frontend\MamamLinkPager;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var $this FrontController
 * @var $provider CDataProvider
 * @method EventRecord[] $provider->getData()
 * @var array $events
 */

$this->pageTitle = $this->t('Past promotions');
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
?>
<div class="main">
    <div class="container">
        <div class="catalog-pagination catalog-pagination-top catalog-pagination-fix">
            <ul>
                <?php $this->widget(MamamLinkPager::class, [
                    'pages' => $provider->pagination,
                    'maxButtonCount' => 9,
                ]) ?>
            </ul>
        </div>
        <div class="pagination-top-clear"></div>
        <div class="promo-now">
            <?php foreach ($events as $index => $item): /* @var $item EventRecord */ ?>
                <section>
                    <a href="<?= $app->createUrl('event/index', ['id' => $item->id]) ?>">
                        <div class="box">
                            <header>
                                <div class="image-wrapper">
                                    <?= CHtml::image($item->logo->getThumbnail('mid320')->url) ?>
                                    <label class="sale-event sale-spring">
                                        <span class="text">-<?= $item->promoDiscountPercent ?><span>%</span></span>
                                    </label>
                                    <?php if ($item->is_drop_shipping): ?>
                                        <label class="fast-delivery"></label>
                                    <?php endif; ?>
                                </div>
                                <div class="title-wrapper"><h1 class="title"><?= CHtml::encode($item->name) ?></h1>
                                </div>
                            </header>
                            <div class="text-wrapper">
                                <p class="about text"><?= CHtml::encode($item->description_short) ?></p>
                                <p class="time text"><?= CHtml::tag(
                                        'time',
                                        ['datetime' => $tf->formatMachine($item->end_at), 'data-countdown' => true],
                                        $tf->format($item->end_at)
                                    ) ?></p>
                                <p class="price text"><?= $this->t('from') ?> <span
                                            class="lowest-price"><?= $cf->format($item->getMinSellingPrice()) ?></span>
                                </p>
                            </div>
                        </div>
                    </a>
                </section>
            <?php endforeach ?>
        </div>
        <div class="catalog-pagination catalog-pagination-bottom catalog-pagination-fix">
            <?php $this->widget(MamamLinkPager::class, [
                'pages' => $provider->pagination,
                'maxButtonCount' => 9,
            ]) ?>
        </div>
    </div>
</div>
