<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerOutputRecord;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 * @var $reportCounts array
 */

$this->pageTitle = $this->t('Summary of cases');

/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $this->app()->currencyFormatter;
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place">
            <div class="styled-title"><?= $this->t('Summary of cases') ?></div>
            <div class="white-place office-place">
                <div class="summary-top-block">
                    <div class="summary-top-text"><?= $this->t('Your percentage') ?>
                        <span class="text">
                            <?= $partner->partner_percent ?>%
                            <span class="office-help right">
                                <span class="office-help-block">
                                    <?= $this->t('Your income is calculated as the purchase amount of the user minus bonuses and discounts, the remaining amount is multiplied by your percentage and is transferred to your Potential balance') ?>
                                </span>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="summary-block summary-balance">
                        <div class="summary-block-inner">
                            <div class="title">
                                <span class="title-text"><?= $this->t('Potential balance') ?>
                                    <span class="office-help right">
                                        <span class="office-help-block">
                                            <?= $this->t('This balance is charged for the user after the purchase.<br><br>After the user has redeemed the order and 14 days passed, the funds are transferred to the Confirmed Balance Sheet') ?>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="summ"><?= $partner->getPotentialBalance() ?> <?= $cf->getCurrency() ?></div>
                        </div>
                        <div class="summary-block-inner">
                            <div class="title">
                                <span class="title-text"><?= $this->t('Confirmed balance sheet') ?>
                                    <span class="office-help right">
                                        <span class="office-help-block">
                                            <?= $this->t('With this balance, you can withdraw funds to your bonus account in MAMAM (with a 10% premium) or on a card in Privatbank.<br><br>The minimum amount to be withdrawn to Privatbank is {min} $.<br><br>There is no minimum withdrawal amount in MOMMY, enrollment is automatic.', ['{min}' => UserPartnerOutputRecord::MIN_AMOUNT_OUTPUT_PRIVAT]) ?>
                                        </span>
                                    </span>
                                </span>
                            </div>
                            <div class="summ"><?= $partner->getBalance() ?> <?= $cf->getCurrency() ?></div>
                        </div>
                    </div>
                    <div class="summary-block">
                        <div class="summary-header icon-summary-profit">
                            <div class="summary-title"><?= $this->t('Earned') ?></div>
                        </div>
                        <ul class="summary-list">
                            <li>
                                <span class="summary-text"><?= $this->t('for today') ?></span>
                                <span class="summary-summ"><?= $reportCounts['amountOrders']['today'] ?> <?= $cf->getCurrency() ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('for yesterday') ?></span>
                                <span class="summary-summ"><?= $reportCounts['amountOrders']['yesterday'] ?> <?= $cf->getCurrency() ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('in the last 7 days') ?></span>
                                <span class="summary-summ"><?= $reportCounts['amountOrders']['seven'] ?> <?= $cf->getCurrency() ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('in the last 30 days') ?></span>
                                <span class="summary-summ"><?= $reportCounts['amountOrders']['thirty'] ?> <?= $cf->getCurrency() ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="summary-block">
                        <div class="summary-header icon-summary-user">
                            <div class="summary-title"><?= $this->t('New users') ?></div>
                        </div>
                        <ul class="summary-list">
                            <li>
                                <span class="summary-text"><?= $this->t('for today') ?></span>
                                <span class="summary-summ"><?= $reportCounts['invited']['today'] ?> <?= $this->t('people') ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('for yesterday') ?></span>
                                <span class="summary-summ"><?= $reportCounts['invited']['yesterday'] ?> <?= $this->t('people') ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('in the last 7 days') ?></span>
                                <span class="summary-summ"><?= $reportCounts['invited']['seven'] ?> <?= $this->t('people') ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('in the last 30 days') ?></span>
                                <span class="summary-summ"><?= $reportCounts['invited']['thirty'] ?> <?= $this->t('people') ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="summary-block">
                        <div class="summary-header icon-summary-orders">
                            <div class="summary-title"><?= $this->t('Orders') ?></div>
                        </div>
                        <ul class="summary-list">
                            <li>
                                <span class="summary-text"><?= $this->t('for today') ?></span>
                                <span class="summary-summ"><?= $reportCounts['orders']['today'] ?> <?= $this->t('PC') ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('for yesterday') ?></span>
                                <span class="summary-summ"><?= $reportCounts['orders']['yesterday'] ?> <?= $this->t('PC') ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('in the last 7 days') ?></span>
                                <span class="summary-summ"><?= $reportCounts['orders']['seven'] ?> <?= $this->t('PC') ?></span>
                            </li>
                            <li>
                                <span class="summary-text"><?= $this->t('in the last 30 days') ?></span>
                                <span class="summary-summ"><?= $reportCounts['orders']['thirty'] ?> <?= $this->t('PC') ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
