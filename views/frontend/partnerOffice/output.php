<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerOutputRecord;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 * @var $dataProvider CActiveDataProvider
 * @var $fromDate int
 * @var $toDate int
 */

$this->pageTitle = $this->t('Finance - Purchase history');
$data = $dataProvider->getData();
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.libs.jqueryUI'));
$cs->registerScriptFile($baseUrl . "/jquery-ui.js");
$cs->registerCssFile($baseUrl . "/jquery-ui.css");
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place finance-place">
            <div class="styled-title"><?= $this->t('Finance') ?></div>
            <div class="white-place office-place">
                <?= $this->renderPartial('_financeTop', ['partner' => $partner]) ?>
                <div class="finance-content-place">
                    <?= $this->renderPartial('_financeTabs') ?>
                    <div class="finance-period">
                        <?php
                        /* @var CActiveForm $form */
                        $form = $this->beginWidget('CActiveForm', [
                            'action' => $this->app()->createUrl('partnerOffice/output'),
                            'id' => 'period-form',
                        ]) ?>
                        <div class="clearfix">
                            <span class="period-title"><?= $this->t('from') ?></span>
                            <?= CHtml::textField('fromDate', date('d.m.Y', $fromDate), ['class' => 'input-period']); ?>
                            <span class="period-title"><?= $this->t('before') ?></span>
                            <?= CHtml::textField('toDate', date('d.m.Y', $toDate), ['class' => 'input-period']); ?>
                        </div>
                        <?php $this->endWidget() ?>
                    </div>
                    <div class="finance-explanation">
                        <p><?= $this->t('This table displays your withdrawals from your balance') ?></p>
                    </div>
                    <?php if (empty($data)) : ?>
                        <div class="finance-period-no"><?= $this->t('Nothing found during the specified period') ?></div>
                    <?php else: ?>
                        <table class="finance-table">
                            <thead>
                            <tr>
                                <th class="finance-date"><?= $this->t('date and time') ?></th>
                                <th><?= $this->t('Amount (USD)') ?></th>
                                <th><?= $this->t('Direction') ?></th>
                                <th>
                                    <span class="finance-status">
                                        <?= $this->t('Status') ?>
                                        <span class="office-help" style="text-align: left; font-weight: normal;">
                                            <span class="office-help-block">
                                                <?= $this->t('Возможные статусы') ?>
                                                : <?= implode(', ', UserPartnerOutputRecord::statusReplacements()) ?>
                                            </span>
                                        </span>
                                    </span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data as $item): /** @var $item UserPartnerOutputRecord */ ?>
                                <?php
                                $classFinance = '';
                                switch ($item->status) {
                                    case UserPartnerOutputRecord::STATUS_WAIT:
                                        $classFinance = '';
                                        break;
                                    case UserPartnerOutputRecord::STATUS_DONE:
                                        $classFinance = 'finance-awaiting';
                                        break;
                                    case UserPartnerOutputRecord::STATUS_CANCEL:
                                        $classFinance = 'finance-disapproved';
                                        break;
                                }
                                ?>
                                <tr>
                                    <td class="finance-date"><?= date('d.m.Y H:i', $item->created_at) ?></td>
                                    <td class="finance-bold"><?= $item->amount ?></td>
                                    <td class="finance-<?= ($item->type_output == UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM) ? 'mamam' : 'privat' ?>"><?= $item->typeOutputReplacement ?></td>
                                    <td class="finance-bold <?= $classFinance ?>">
                                        <span>
                                            <?php if ($item->status == UserPartnerOutputRecord::STATUS_CANCEL) : ?>
                                                <span class="finance-disapproved-mess"
                                                      style="height: auto; word-break: break-all; line-height: 21px;"><?= $item->status_reason ?></span>
                                            <?php endif; ?>
                                            <?= $item->statusReplacement ?>
                                        </span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<aside class="modals-office">
    <?= $this->renderPartial('_financeModals', ['partner' => $partner]) ?>
</aside>
<?php
$days = [
    0 => $this->t('All'),
    1 => $this->t('Mon'),
    2 => $this->t('W'),
    3 => $this->t('Wed'),
    4 => $this->t('Thu'),
    5 => $this->t('Fri'),
    6 => $this->t('Sat'),
];
$months = [
    1 => $this->t('January'),
    2 => $this->t('February'),
    3 => $this->t('March'),
    4 => $this->t('April'),
    5 => $this->t('May'),
    6 => $this->t('June'),
    7 => $this->t('July'),
    8 => $this->t('August'),
    9 => $this->t('September'),
    10 => $this->t('October'),
    11 => $this->t('November'),
    12 => $this->t('December'),
];
?>
<script>
    $(function () {
        $("#fromDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            },
        });
        $("#toDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            }
        });
    });
</script>
