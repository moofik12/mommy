<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;

/**
 * @var $this PartnerOfficeController
 */
?>
<div class="content-menu partner-office">
    <?php $this->widget('zii.widgets.CMenu', [
        'items' => $this->menu,
    ]); ?>
</div>
