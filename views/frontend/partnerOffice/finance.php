<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerAdmissionRecord;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Statistic\UserPartnerOrderModel;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 * @var $dataProviderReport CArrayDataProvider
 * @var $fromDate int
 * @var $toDate int
 */

$this->pageTitle = $this->t('Finance - Enrollment history');

$data = $dataProviderReport->getData();

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.libs.jqueryUI'));
$cs->registerScriptFile($baseUrl . "/jquery-ui.js");
$cs->registerCssFile($baseUrl . "/jquery-ui.css");
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place finance-place">
            <div class="styled-title"><?= $this->t('Finance') ?></div>
            <div class="white-place office-place">
                <?= $this->renderPartial('_financeTop', ['partner' => $partner]) ?>
                <div class="finance-content-place">
                    <?= $this->renderPartial('_financeTabs') ?>
                    <div class="finance-period">
                        <?php
                        /* @var CActiveForm $form */
                        $form = $this->beginWidget('CActiveForm', [
                            'action' => $this->app()->createUrl('partnerOffice/finance'),
                            'id' => 'period-form',
                        ]) ?>
                        <div class="clearfix">
                            <span class="period-title"><?= $this->t('from') ?></span>
                            <?= CHtml::textField('fromDate', date('d.m.Y', $fromDate), ['class' => 'input-period']); ?>
                            <span class="period-title"><?= $this->t('before') ?></span>
                            <?= CHtml::textField('toDate', date('d.m.Y', $toDate), ['class' => 'input-period']); ?>
                        </div>
                        <?php $this->endWidget() ?>
                    </div>
                    <div class="finance-explanation">
                        <p><?= $this->t('This table shows the number of purchases made by you and the amount to be credited. Clicking the Details link will display the list of purchases for a specific day.') ?></p>
                    </div>
                    <?php if (empty($data)) : ?>
                        <div class="finance-period-no"><?= $this->t('Nothing found during the specified period') ?></div>
                    <?php else: ?>
                        <table class="finance-table">
                            <thead>
                            <tr>
                                <th class="finance-date"><?= $this->t('date') ?></th>
                                <th><?= $this->t('Quantity of purchases') ?></th>
                                <th><?= $this->t('Amount to be credited (USD)') ?></th>
                                <th><?= $this->t('Details') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data as $item) : /** @var $item UserPartnerOrderModel */ ?>
                                <tr>
                                    <td class="finance-date"><?= date('d.m.Y', $item->startTime) ?></td>
                                    <td><?= $item->countOrder ?></td>
                                    <td class="finance-bold <?= $item->sumOutput == 0 ? 'finance-none' : '' ?>"><?= $item->sumOutput ?></td>
                                    <td>
                                        <div class="finance-more-wrapper">
                                            <a class="finance-more <?= $item->sumOutput == 0 ? 'disabled' : '' ?>"
                                               href="javascript:void(0);" <?= $item->sumOutput > 0 ? 'data-action="show-detal-finance" data-modal="' . $item->startTime . '"' : '' ?>><?= $this->t('Learn More') ?></a>

                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<aside class="modals-office">
    <?php foreach ($dataProviderReport->getData() as $item) : /** @var $item UserPartnerOrderModel */ ?>
        <?php if ($item->sumOutput > 0 && !empty($item->admissions)) : ?>
            <div class="modal finance-modal finance-withdraw-more hide" data-id="modal<?= $item->startTime ?>">
                <div class="finance-modal-header">
                    <h2 class="title"><?= $this->t('DETAILS FOR') ?> <?= date('d.m.Y', $item->startTime) ?></h2>
                    <div class="close" data-dismiss="modal"></div>
                </div>
                <div class="finance-modal-table">
                    <div class="finance-table-head">
                        <div class="clearfix">
                            <div class="finance-table-th"><?= $this->t('Order number') ?></div>
                            <div class="finance-table-th"><?= $this->t('Amount') ?></div>
                        </div>
                    </div>
                    <div class="finance-table-body">
                        <div class="finance-table-scroll">
                            <?php foreach ($item->admissions as $admission): /** @var $admission UserPartnerAdmissionRecord */ ?>
                                <div class="clearfix">
                                    <div class="finance-table-td"><?= str_pad($admission->order_id, 10, '0', STR_PAD_LEFT) ?></div>
                                    <div class="finance-table-td"><?= $admission->amount ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
    <?= $this->renderPartial('_financeModals', ['partner' => $partner]) ?>
</aside>
<?php
$days = [
    0 => $this->t('All'),
    1 => $this->t('Mon'),
    2 => $this->t('W'),
    3 => $this->t('Wed'),
    4 => $this->t('Thu'),
    5 => $this->t('Fri'),
    6 => $this->t('Sat'),
];
$months = [
    1 => $this->t('January'),
    2 => $this->t('February'),
    3 => $this->t('March'),
    4 => $this->t('April'),
    5 => $this->t('May'),
    6 => $this->t('June'),
    7 => $this->t('July'),
    8 => $this->t('August'),
    9 => $this->t('September'),
    10 => $this->t('October'),
    11 => $this->t('November'),
    12 => $this->t('December'),
];
?>
<script>
    $(function () {
        $(document).on("click", "[data-action='show-detal-finance']", function () {
            var idModal = $(this).attr('data-modal');
            $("[data-id='modal" + idModal + "']").modal('show');
        });
        $("#fromDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            },
        });
        $("#toDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            }
        });
    });
</script>
