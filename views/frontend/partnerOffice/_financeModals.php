<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Form\PrivatOutputForm;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 */

$privatOutputForm = new PrivatOutputForm();

$textError0 = $this->t('The amount must be greater than 0');
$textErrorConnect = $this->t('Error. Check your internet connection');

?>
<div class="modal finance-modal hide" id="withdraw">
    <div class="finance-modal-header">
        <h2 class="title"><?= $this->t('Withdrawal of funds') ?></h2>
        <div class="close" data-dismiss="modal"></div>
    </div>
    <div class="finance-modal-summ">
        <div class="field top">
            <input type="text" value="<?= $partner->getBalance() ?>" id="output-amount">
            <div class="message"></div>
        </div>
    </div>
    <div class="finance-modal-btn">
        <div class="finance-btn-inner">
            <?= CHtml::ajaxLink(
                '<span>' . $this->t('Mommy Balance') . '</span>',
                $this->app()->createUrl('partnerOffice/outputMamam'),
                [
                    'beforeSend' => 'function(jqXHR, settings) { 
                        var amount = parseInt($("#output-amount").val());
                        if (amount <= 0) {
                            $("#output-amount").parent().addClass("error");
                            $("#output-amount").parent().find(".message").text("' . $textError0 . '");
                            return false;
                        }                    
                    }',
                    'data' => 'js: {amount: $("#output-amount").val()}',
                    'success' => 'function(data) {
                        if (data.success) {
                            $("#withdraw").modal("hide");
                            window.location.replace(Mamam.createUrl("partnerOffice/output"));
                            return;
                        }                                    
                        var error = data.errors.length > 0 ? data.errors[0] : false;
                        if (error) {
                            $("#output-amount").parent().addClass("error");
                            $("#output-amount").parent().find(".message").text(error);
                        }
                    }',
                    'error' => 'function() {
                        alert("' . $textErrorConnect . '");
                    }',
                ],
                ['class' => 'finance-btn finance-btn-mamam']
            ) ?>
        </div>
        <div class="finance-btn-inner">
            <?= CHtml::ajaxLink(
                '<span>' . $this->t('Privat Bank') . '</span>',
                $this->app()->createUrl('partnerOffice/validOutputPrivat'),
                [
                    'beforeSend' => 'function(jqXHR, settings) { 
                        var amount = parseInt($("#output-amount").val());
                        if (amount <= 0) {
                            $("#output-amount").parent().addClass("error");
                            $("#output-amount").parent().find(".message").text("' . $textError0 . '");
                            return false;
                        }                    
                    }',
                    'data' => 'js: {amount: $("#output-amount").val()}',
                    'success' => 'function(data) {
                        if (data.success) {
                            $("#withdraw").modal("hide");
                            $("#withdraw-privat").modal("show");
                            return;
                        }                                    
                        var error = data.errors.length > 0 ? data.errors[0] : false;
                        if (error) {
                            $("#output-amount").parent().addClass("error");
                            $("#output-amount").parent().find(".message").text(error);
                        }
                    }',
                    'error' => 'function() {
                        alert("' . $textErrorConnect . '");
                    }',
                ],
                ['class' => 'finance-btn finance-btn-privat']
            ) ?>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#output-amount").on('input', function () {
            $("#output-amount").parent().removeClass("error");
            $("#output-amount").parent().find(".message").text("");
        });
    });
</script>
<div class="modal finance-withdraw-privat finance-modal hide" id="withdraw-privat">
    <div class="finance-modal-header">
        <h2 class="title"><?= $this->t('Withdrawal via Privatbank') ?></h2>
        <div class="close" data-dismiss="modal"></div>
    </div>
    <div class="finance-standard-block standard-block">
        <?php
        /* @var CActiveForm $form */
        $form = $this->beginWidget('CActiveForm', [
            'action' => $this->app()->createUrl('partnerOffice/outputPrivat'),
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'afterValidate' => 'js:function($form, data, hasError) {
                    if (hasError) {
                        return;
                    }    
                    $("#full-error").hide();
                    $("#full-error").text();
                    var amount = parseInt($("#output-amount").val());
                    if (amount <= 0) {
                        $("#full-error").show();
                        $("#full-error").find(".message").text("' . $textError0 . '");
                        return false;
                    } else {
                        $("#amount-privat").val(amount);
                    }
                    $.ajax({
                        url: $form.attr("action"),
                        type: $form.attr("method"),
                        data: $form.serialize(),
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            if (data !== null && typeof data === "object") {
                                if (data.success) {
                                    $("#withdraw-privat").modal("hide");
                                    window.location.replace(Mamam.createUrl("partnerOffice/output"));
                                } else if (data.errors && data.errors.length > 0) {
                                    $("#full-error").show();
                                    $("#full-error").text(data.errors[0]);
                                }
                            }
                        },
                        error: function () {
                            alert("' . $textErrorConnect . '");
                        }
                    });
            
                    return false;
                }',
            ],
        ]) ?>
        <?= $form->hiddenField($privatOutputForm, 'amount', ['id' => 'amount-privat']) ?>
        <div class="finance-standard-wrapper standard-wrapper">
            <div class="standard-title">
                <label class="left-part"><?= $this->t('Card number') ?></label>
            </div>
            <div class="left-part left-part-full">
                <div class="field <?= $privatOutputForm->hasErrors('cardNumber') ? 'error' : '' ?> top">
                    <?= $form->textField($privatOutputForm, 'cardNumber', ['autocomplete' => 'off']) ?>
                    <?= $form->error($privatOutputForm, 'cardNumber', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
        <div class="standard-wrapper">
            <div class="standard-title">
                <label class="left-part"><?= $this->t('Your name') ?></label>
                <label class="right-part"><?= $this->t('Your last name') ?></label>
            </div>
            <div class="left-part">
                <div class="field <?= $privatOutputForm->hasErrors('nameCard') ? 'error' : '' ?> top">
                    <?= $form->textField($privatOutputForm, 'nameCard', ['autocomplete' => 'off']) ?>
                    <?= $form->error($privatOutputForm, 'nameCard', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field <?= $privatOutputForm->hasErrors('surnameCard') ? 'error' : '' ?> top">
                    <?= $form->textField($privatOutputForm, 'surnameCard', ['autocomplete' => 'off']) ?>
                    <?= $form->error($privatOutputForm, 'surnameCard', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
        <div class="standard-wrapper" style="color: #ff6178;" id="full-error"></div>
        <div class="finance-standard-wrapper standard-wrapper">
            <input class="btn red" type="submit" value="<?= $this->t('Send') ?>">
            <input class="btn reset" type="submit" value="<?= $this->t('Cancel Output') ?>" data-toggle="modal"
                   data-dismiss="modal"
                   data-target="#withdraw">
        </div>
        <?php $this->endWidget() ?>
    </div>
</div>
