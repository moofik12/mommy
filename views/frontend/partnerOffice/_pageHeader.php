<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;

/**
 * @var $this PartnerOfficeController
 */
?>
<div class="page-header">
    <div class="container page-title">
        <?= $this->t('Affiliate cabinet') ?>
    </div>
</div>
