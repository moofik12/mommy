<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;

/**
 * @var $this PartnerOfficeController
 */
?>
<div class="finance-tabs">
    <ul class="clearfix">
        <li class="<?= $this->action->id == 'finance' ? 'active' : '' ?>"><a
                    href="<?= $this->app()->createUrl('partnerOffice/finance') ?>"><?= $this->t('Enrollment') ?></a>
        </li>
        <li class="<?= $this->action->id == 'return' ? 'active' : '' ?>"><a
                    href="<?= $this->app()->createUrl('partnerOffice/return') ?>"><?= $this->t('Returns') ?></a>
        </li>
        <li class="<?= $this->action->id == 'output' ? 'active' : '' ?>"><a
                    href="<?= $this->app()->createUrl('partnerOffice/output') ?>"><?= $this->t('Withdrawal of funds history') ?></a>
        </li>
    </ul>
</div>
