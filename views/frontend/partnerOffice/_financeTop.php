<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 */

/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $this->app()->currencyFormatter;
?>
<div class="finance-top">
    <div class="finance-top-place clearfix">
        <div class="finance-top-inner">
            <div class="finance-top-title"><?= $this->t('Potential balance') ?></div>
            <div class="finance-top-summ"><?= $partner->getPotentialBalance() ?> <?= $cf->getCurrency() ?></div>
        </div>
        <div class="finance-top-inner">
            <div class="finance-top-title"><?= $this->t('Confirmed balance sheet') ?></div>
            <div class="finance-top-summ"><?= $partner->getBalance() ?> <?= $cf->getCurrency() ?></div>
        </div>
    </div>
    <div class="finance-withdraw">
        <div class="finance-withdraw-btn"
             onclick="$('#withdraw').modal('show');"><?= $this->t('Output') ?></div>
    </div>
</div>
