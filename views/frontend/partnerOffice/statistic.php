<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Statistic\UserPartnerOrderModel;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 * @var $dataProviderReport CArrayDataProvider
 * @var $fromDate int
 * @var $toDate int
 */

$this->pageTitle = $this->t('Statistics');
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.libs.jqueryUI'));
$cs->registerScriptFile($baseUrl . "/jquery-ui.js");
$cs->registerCssFile($baseUrl . "/jquery-ui.css");
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place statistics-place">
            <div class="styled-title"><?= $this->t('Statistics') ?></div>
            <div class="statistics-period">
                <?php
                /* @var CActiveForm $form */
                $form = $this->beginWidget('CActiveForm', [
                    'action' => $this->app()->createUrl('partnerOffice/statistic'),
                    'id' => 'period-form',
                ]) ?>
                <div class="clearfix">
                    <span class="period-title"><?= $this->t('from') ?></span>
                    <?= CHtml::textField('fromDate', date('d.m.Y', $fromDate), ['class' => 'input-period']); ?>
                    <span class="period-title"><?= $this->t('before') ?></span>
                    <?= CHtml::textField('toDate', date('d.m.Y', $toDate), ['class' => 'input-period']); ?>
                </div>
                <?php $this->endWidget() ?>
            </div>
            <div class="white-place office-place">
                <table class="statistics-table">
                    <thead>
                    <tr>
                        <th class="statistics-date"><?= $this->t('date') ?></th>
                        <th><?= $this->t('Registrations') ?></th>
                        <th><?= $this->t('Promotional codes used') ?></th>
                        <th><?= $this->t('Quantity of purchases') ?></th>
                        <th><?= $this->t('Potential amount') ?></th>
                        <th><?= $this->t('Confirmed amount') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($dataProviderReport->getData() as $item): /** @var $item UserPartnerOrderModel */ ?>
                        <tr>
                            <td class="statistics-date <?= $item->countOrder == 0 && $item->countRegPartnerInvitedUser == 0 && $item->countOrder == 0 ? 'statistics-none' : '' ?>"><?= date('d.m.Y', $item->startTime) ?></td>
                            <td class="<?= $item->countRegPartnerInvitedUser == 0 ? 'statistics-none' : '' ?>"><?= $item->countRegPartnerInvitedUser ?></td>
                            <td class="<?= $item->countUsedPromocode == 0 ? 'statistics-none' : '' ?>"><?= $item->countUsedPromocode ?></td>
                            <td class="<?= $item->countOrder == 0 ? 'statistics-none' : '' ?>"><?= $item->countOrder ?></td>
                            <td class="statistics-bold <?= $item->sumOutput == 0 ? 'statistics-none' : '' ?>"><?= $item->sumOutput ?></td>
                            <td class="statistics-bold <?= $item->sumConfirmedOutput == 0 ? 'statistics-none' : '' ?>"><?= $item->sumConfirmedOutput ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
$days = [
    0 => $this->t('All'),
    1 => $this->t('Mon'),
    2 => $this->t('W'),
    3 => $this->t('Wed'),
    4 => $this->t('Thu'),
    5 => $this->t('Fri'),
    6 => $this->t('Sat'),
];
$months = [
    1 => $this->t('January'),
    2 => $this->t('February'),
    3 => $this->t('March'),
    4 => $this->t('April'),
    5 => $this->t('May'),
    6 => $this->t('June'),
    7 => $this->t('July'),
    8 => $this->t('August'),
    9 => $this->t('September'),
    10 => $this->t('October'),
    11 => $this->t('November'),
    12 => $this->t('December'),
];
?>
<script>
    $(function () {
        $("#fromDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            },
        });
        $("#toDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            }
        });
    });
</script>
