<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerBalanceRecord;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 * @var $dataProvider CActiveDataProvider
 * @var $fromDate int
 * @var $toDate int
 */

$this->pageTitle = $this->t('Finance - Refunds');
$data = $dataProvider->getData();
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.libs.jqueryUI'));
$cs->registerScriptFile($baseUrl . "/jquery-ui.js");
$cs->registerCssFile($baseUrl . "/jquery-ui.css");
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place finance-place">
            <div class="styled-title"><?= $this->t('Finance') ?></div>
            <div class="white-place office-place">
                <?= $this->renderPartial('_financeTop', ['partner' => $partner]) ?>
                <div class="finance-content-place">
                    <?= $this->renderPartial('_financeTabs') ?>
                    <div class="finance-period">
                        <?php
                        /* @var CActiveForm $form */
                        $form = $this->beginWidget('CActiveForm', [
                            'action' => $this->app()->createUrl('partnerOffice/return'),
                            'id' => 'period-form',
                        ]) ?>
                        <div class="clearfix">
                            <span class="period-title"><?= $this->t('from') ?></span>
                            <?= CHtml::textField('fromDate', date('d.m.Y', $fromDate), ['class' => 'input-period']); ?>
                            <span class="period-title"><?= $this->t('before') ?></span>
                            <?= CHtml::textField('toDate', date('d.m.Y', $toDate), ['class' => 'input-period']); ?>
                        </div>
                        <?php $this->endWidget() ?>
                    </div>
                    <div class="finance-explanation">
                        <p>
                            <?= $this->t('This table specifies refunds of your users. Since we compensate users with money for their refunds - we accordingly take away the commission from your balance in case of a refund.') ?>
                        </p>
                    </div>
                    <?php if (empty($data)) : ?>
                        <div class="finance-period-no"><?= $this->t('Nothing found during the specified period') ?></div>
                    <?php else: ?>
                        <table class="finance-table">
                            <thead>
                            <tr>
                                <th class="finance-date"><?= $this->t('date and time') ?></th>
                                <th><?= $this->t('Order number') ?></th>
                                <th><?= $this->t('Amount (USD)') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($data as $item): /** @var $item UserPartnerBalanceRecord */ ?>
                                <tr>
                                    <td class="finance-date"><?= date('d.m.Y H:i', $item->created_at) ?></td>
                                    <td><?= str_pad($item->order_id, 10, '0', STR_PAD_LEFT) ?></td>
                                    <td class="finance-bold"><?= $item->amount ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<aside class="modals-office">
    <?= $this->renderPartial('_financeModals', ['partner' => $partner]) ?>
</aside>
<?php
$days = [
    0 => $this->t('All'),
    1 => $this->t('Mon'),
    2 => $this->t('W'),
    3 => $this->t('Wed'),
    4 => $this->t('Thu'),
    5 => $this->t('Fri'),
    6 => $this->t('Sat'),
];
$months = [
    1 => $this->t('January'),
    2 => $this->t('February'),
    3 => $this->t('March'),
    4 => $this->t('April'),
    5 => $this->t('May'),
    6 => $this->t('June'),
    7 => $this->t('July'),
    8 => $this->t('August'),
    9 => $this->t('September'),
    10 => $this->t('October'),
    11 => $this->t('November'),
    12 => $this->t('December'),
];
?>
<script>
    $(function () {
        $("#fromDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            },
        });
        $("#toDate").datepicker({
            dateFormat: "dd.mm.yy",
            dayNamesMin: [ <?php echo '"' . implode('","', $days) . '"' ?> ],
            monthNames: [ <?php echo '"' . implode('","', $months) . '"' ?> ],
            minDate: new Date(2017, 6, 1),
            onSelect: function (dateText) {
                $('#period-form').submit();
            }
        });
    });
</script>
