<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Form\GeneratorUrlForm;

/**
 * @var $this PartnerOfficeController
 * @var $partner UserPartnerRecord
 * @var $reportCounts array
 * @var $partnerPromoCodes PromocodeRecord[]
 */

$this->pageTitle = $this->t('Promotional materials');
$generatorForm = new GeneratorUrlForm();
$baseImgUrl = $this->app()->getBaseUrl(true) . '/' . 'static/partner/promo/';

$textError = $this->t('Authorization Error. Check your internet connection');
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place statistics-place">
            <div class="styled-title"><?= $this->t('Promotional materials') ?></div>
            <div class="white-place office-place">
                <div class="advertising-block">
                    <label><?= $this->t('Your home page link') ?>
                        <span class="office-help right">
                            <span class="office-help-block"><?= $this->t('You can copy the link and send it to the interested person. And we will know that this person is invited by you') ?></span>
                        </span>
                    </label>
                    <div class="advertising-wrapper advertising-high clearfix">
                        <div class="input-advertising">
                            <div class="field red">
                                <div class="input"
                                     id="main-partner-link"><?= $this->app()->createAbsoluteUrl('partner/invite', ['id' => $partner->id]) ?></div>
                            </div>
                        </div>
                        <div class="advertising-label"><?= $this->t('Copy the link for registration and send it to your friend') ?></div>
                        <div class="advertising-label">
                            <br>
                            <a class="advertising-copy" id="copyLink1"
                               href="javascript:void(0);"><?= $this->t('Copy to clipboard') ?></a>
                        </div>
                    </div>
                </div>
                <div class="advertising-block">
                    <label><?= $this->t('Link generator to any page of the site') ?>
                        <span class="office-help right">
                            <span class="office-help-block"><?= $this->t('You can generate an invite link and send it to the interested person. And we will know that this person is invited by you') ?></span>
                        </span>
                    </label>
                    <div class="advertising-wrapper clearfix">
                        <div class="input-advertising button advertising-input-style">
                            <?php
                            /* @var CActiveForm $form */
                            $form = $this->beginWidget('CActiveForm', [
                                'action' => $this->app()->createUrl('partnerOffice/generateUrl', ['partnerId' => $partner->id]),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => [
                                    'validateOnSubmit' => true,
                                    'afterValidate' => 'js:function($form, data, hasError) {
                                        if (hasError) {
                                            return;
                                        }                                        
                                        $.ajax({
                                            url: $form.attr("action"),
                                            type: $form.attr("method"),
                                            data: $form.serialize(),
                                            dataType: "json",
                                            success: function (data) {
                                                if (data !== null && typeof data === "object") {
                                                    if (data.success) {
                                                        $("#generated-link").text(data.generatedLink);
                                                        $(".advertising-copy").show();
                                                    } else if (data.errors && data.errors.length > 0) {
                                                        alert(data.errors[0]);
                                                    }
                                                }
                                            },
                                            error: function () {
                                                alert("' . $textError . '");
                                            }
                                        });
        
                                        return false;
                                    }',
                                ],
                            ]) ?>
                            <div class="field <?= $generatorForm->hasErrors('url') ? 'error' : '' ?> top">
                                <?= $form->textField($generatorForm, 'url', ['placeholder' => $this->t('Paste the copied site link'), 'autocomplete' => 'off']) ?>
                                <?= $form->error($generatorForm, 'url', ['class' => 'message']) ?>
                            </div>
                            <input type="submit" value="<?= $this->t('Next') ?>">
                            <?php $this->endWidget() ?>
                        </div>
                        <div class="advertising-label"><?= $this->t('Generate a link to any page of the site to share it') ?></div>
                    </div>
                    <div class="advertising-wrapper advertising-high clearfix">
                        <div class="input-advertising advertising-input-style advertising-input-disabled">
                            <div class="field">
                                <div class="input"
                                     id="generated-link"><?= $this->t('Your personal link will be generated here') ?></div>
                            </div>
                        </div>
                        <div class="advertising-label"><a class="advertising-copy" id="copyLink2" style="display: none;"
                                                          href="javascript:void(0);"><?= $this->t('Copy to clipboard') ?></a>
                        </div>
                    </div>
                </div>
                <script>
                    function copy(str) {
                        var tmp = document.createElement('INPUT'),
                            focus = document.activeElement;

                        tmp.value = str;

                        document.body.appendChild(tmp);
                        tmp.select();
                        document.execCommand('copy');
                        document.body.removeChild(tmp);
                        focus.focus();
                    }

                    $(function () {
                        var copyMainLinkSelector = document.querySelector('#copyLink1');
                        var copyLinkSelector = document.querySelector('#copyLink2');
                        copyLinkSelector.addEventListener('click', function (e) {
                            e.preventDefault();
                            copy($('#generated-link').text());
                        });
                        copyMainLinkSelector.addEventListener('click', function (e) {
                            e.preventDefault();
                            copy($('#main-partner-link').text());
                        });
                    });
                </script>
                <div class="advertising-code-block">
                    <label><?= $this->t('Your promo codes') ?>
                        <span class="office-help right">
                            <span class="office-help-block">
                                <?= $this->t('Share with your friends and acquaintances these promotional codes. If you use a promotional code, the user will be assigned to you and you will receive a commission from his purchases. A new user can use the promotional code ONLY ONCE.') ?>
                            </span>
                        </span>
                    </label>
                    <?php foreach ($partnerPromoCodes as $partnerPromoCode): /**@var $partnerPromoCode PromocodeRecord */ ?>
                        <div class="advertising-code-wrapper clearfix">
                            <div class="input-code-advertising">
                                <div class="field red">
                                    <div class="input"><?= $partnerPromoCode->promocode ?></div>
                                </div>
                            </div>
                            <div class="advertising-code-label">
                            <span>
                                <?= $this->t('<strong>Promotional code for {cash}$, </strong> minimum order - {min}$', ['{cash}' => $partnerPromoCode->cash, '{min}' => $partnerPromoCode->order_price_after]) ?>
                            </span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="advertising-block-share">
                    <p class="text-top"><?= $this->t('You can use this pictures to publish on your<br> Social Network pages to attract new customers') ?></p>
                    <p class="text-top-notify"><?= $this->t('We remind you that SPAM in any of its manifestations is strictly PROHIBITED') ?></p>
                    <div class="advertising-share-inner clearfix">
                        <?php for ($i = 1; $i <= 16; $i++) : ?>
                            <?php $promoShareUrl = $this->app()->createAbsoluteUrl('partner/invite', ['id' => $partner->id, 'promo' => $i]); ?>
                            <div class="advertising-share-item">
                                <div class="advertising-share-image">
                                    <img src="<?= $baseImgUrl ?>promo<?= $i ?>.jpg" data-action="share-partner-promo"
                                         data-img-url="<?= $baseImgUrl ?>promo<?= $i ?>.jpg"
                                         data-url="<?= $promoShareUrl ?>"
                                         alt="<?= $this->t('Image for advertising in social networks') ?>"
                                         title="<?= $this->t('Image for advertising in social networks') ?>">
                                </div>
                                <div class="advertising-share-link">
                                    <a href="<?= $baseImgUrl ?>promo<?= $i ?>.jpg" target="_blank"
                                       download><?= $this->t('Download') ?></a>
                                    <a href="javascript:void(0);" data-action="share-partner-promo"
                                       data-img-url="<?= $baseImgUrl ?>promo<?= $i ?>.jpg"
                                       data-url="<?= $promoShareUrl ?>"><?= $this->t('To place') ?></a>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<aside class="modals">
    <div class="finance-modal modal finance-modal-share hide" id="share-promo-modal">
        <div class="finance-modal-header">
            <h2 class="title"><?= $this->t('PLACES') ?></h2>
            <div class="close" data-dismiss="modal"></div>
            <p class="text"><?= $this->t('Select your favorite social network and click on its button - a window will open in which you can see what your post will look like and make changes to it.') ?></p>
        </div>
        <div class="finance-share-line">
            <a class="fb icon-finance-share-fb" href="javascript:void(0);" data-share-type="fb">Facebook</a>
            <a class="gp icon-finance-share-gp" href="javascript:void(0);" data-share-type="gl">Google+</a>
        </div>
        <div class="finance-share-line">
            <a class="vk icon-finance-share-vk" href="javascript:void(0);"
               data-share-type="vk"><?= $this->t('In contact with') ?></a>
            <a class="od icon-finance-share-od" href="javascript:void(0);"
               data-share-type="ok"><?= $this->t('Classmates') ?></a>
        </div>
    </div>
</aside>
<?
$shareTitle = $this->t('MOMMY.COM');
$shareDescription = $this->t('MOMMY.COM - shopping club');
?>
<script>
    $(function () {
        var shareImgUrl = '';
        var shareTitle = '<?= $shareTitle ?>';
        var shareDescription = '<?= $shareDescription  ?>';
        var partnerLink = '#';
        $(document).on('click', "[data-action='share-partner-promo']", function (e) {
            e.preventDefault();
            shareImgUrl = $(this).attr('data-img-url');
            partnerLink = $(this).attr('data-url');
            $("#share-promo-modal").modal("show");
        });
        $(document).on('click', "a[data-share-type]", function (e) {
            e.preventDefault();

            var shareType = $(this).attr('data-share-type');
            var shareUrl = '#';
            console.log(partnerLink);
            if (shareType == 'vk') {
                shareUrl = 'https://vk.com/share.php?' +
                    'url=' + encodeURIComponent(partnerLink) + '&' +
                    'title=' + encodeURIComponent(shareTitle) + '&' +
                    'image=' + encodeURIComponent(shareImgUrl) + '&' +
                    'description=' + encodeURIComponent(shareDescription) + '&' + 'noparse=true';
            } else if (shareType == 'fb') {
                shareUrl = 'http://www.facebook.com/sharer.php?s=100';
                shareUrl += '&t=' + encodeURIComponent(shareTitle);
                shareUrl += '&summary=' + encodeURIComponent(shareDescription);
                shareUrl += '&u=' + encodeURIComponent(partnerLink);
                shareUrl += '&picture=' + encodeURIComponent(shareImgUrl);
            } else if (shareType == 'tw') {
                shareUrl = 'http://twitter.com/share?';
                shareUrl += 'text=' + encodeURIComponent(shareTitle);
                shareUrl += '&url=' + encodeURIComponent(partnerLink);
                shareUrl += '&image=' + encodeURIComponent(shareImgUrl);
            } else if (shareType == 'ok') {
                shareUrl = 'https://connect.ok.ru/offer';
                shareUrl += '?url=' + encodeURIComponent(partnerLink);
                shareUrl += '&title=' + encodeURIComponent(shareTitle);
                shareUrl += '&description=' + encodeURIComponent(shareDescription);
                shareUrl += '&imageUrl=' + encodeURIComponent(shareImgUrl);

            } else if (shareType == 'gl') {
                shareUrl = 'https://plus.google.com/share?';
                shareUrl += 'url=' + encodeURIComponent(partnerLink);
                shareUrl += '&hl=' + encodeURIComponent(shareTitle);
            } else if (shareType == 'mr') {
                shareUrl = 'https://connect.mail.ru/share';
                shareUrl += '?url=' + encodeURIComponent(partnerLink);
                shareUrl += '&title=' + encodeURIComponent(shareTitle);
                shareUrl += '&description=' + encodeURIComponent(shareDescription);
                shareUrl += '&imageUrl=' + encodeURIComponent(shareImgUrl);
            }

            window.open(shareUrl, '', 'toolbar=0,status=0,width=626,height=436');
            $("#share-promo-modal").modal("hide");
        });
    });
</script>
