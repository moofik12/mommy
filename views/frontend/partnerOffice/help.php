<?php

use MommyCom\Controller\Frontend\PartnerOfficeController;
use MommyCom\Model\Db\UserPartnerOutputRecord;
use MommyCom\Model\Form\FeedbackForm;

/**
 * @var $this PartnerOfficeController
 * @var $feedbackForm FeedbackForm
 */

$this->pageTitle = $this->t('Help');
$app = $this->app();
if ($app->user->hasFlash('message')) {
    $app->clientScript->registerScript('alertMessage', '
        alert("' . $app->user->getFlash('message') . '");
    ', CClientScript::POS_READY);
}
?>
<?= $this->renderPartial('_pageHeader') ?>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>
        <div class="content-place statistics-place">
            <div class="styled-title"><?= $this->t('Help') ?></div>
            <div class="white-place office-place">
                <div class="advertising-block-share">
                    <h2><?= $this->t('Personal cabinet of the partner "Partner Cabinet"') ?></h2>
                    <p><?= $this->t('This is a section where each member of the affiliate program can monitor information about income, inviteed users, as well as the number of orders issued by them for the last 30 days.') ?></p>
                    <p><?= $this->t('Your income is calculated as the sum of purchases of inviteed users, without taking into account bonuses and discounts used when placing an order, the remaining amount is multiplied by your percentage and is transferred to your potential balance.') ?></p>
                    <h2><?= $this->t('Potential balance') ?></h2>
                    <p><?= $this->t('This balance is paid after the user makes a purchase. After 14 days from the receipt of the order, the funds are transferred to the confirmed balance.') ?></p>
                    <h2><?= $this->t('Confirmed balance sheet') ?></h2>
                    <p><?= $this->t('From this balance, the funds are withdrawn to the bonus account of MOMMY (with a charge of {percent}%) or to the Privatbank card.', ['{percent}' => UserPartnerOutputRecord::PERCENT_OUTPUT_MAMAM]) ?></p>
                    <p><?= $this->t('The withdrawal of funds is made in the section "Finance".') ?></p>
                    <p class="text-top-notify"><?= $this->t('Note! In the event of a refund, we take the commission from your balance, since we compensate the users for the refund amount.') ?></p>
                    <h2><?= $this->t('Promotional materials') ?></h2>
                    <p><?= $this->t('n this section there is your personal link to invite new users, as well as personal promotional codes with additional discounts for new users.') ?></p>
                    <p><?= $this->t('Send a personal link to your friends and acquaintances for registration and they will be assigned to you.') ?></p>
                    <p><?= $this->t('Share promotional codes and also receive rewards from purchases of your friends and acquaintances. A promotional code can be used by a new user only once.') ?></p>
                    <p><?= $this->t('Also, to attract new users, use images to place them in Social Networks. To do this, click on the image you like and select your social network in the window that opens. Next, the social network window opens, in which you will complete the placement of your advertisement message.') ?></p>
                    <h2><?= $this->t('Terms & Conditions') ?></h2>
                    <p><?= $this->t('With the terms and conditions of participation in the Partner Program of MOMMY you can find on <a style="color: #dd6b6b;" href="{url}" target="_blank">this page</a>.', ['url' => $this->app()->createUrl('static/index', ['category' => 'company', 'page' => 'partner - rules'])]) ?></p>
                    <h2><?= $this->t('Feedback') ?></h2>
                    <p><?= $this->t('If you have any questions, please fill out the form below. We\'ll send the answers to the email you provided us with when you signed up.') ?></p>
                    <?php $form = $this->beginWidget('CActiveForm', [
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                        ],
                    ]) ?>
                    <div class="standard-block">
                        <div class="standard-title">
                            <label class="left-part"><?= $this->t('Ask your question') ?></label>
                        </div>
                        <div class="standard-wrapper">
                            <div class="field textarea top <?= $feedbackForm->hasErrors('message') ? 'error' : '' ?>">
                                <?= $form->textArea($feedbackForm, 'message') ?>
                                <?= $form->error($feedbackForm, 'message', ['class' => 'message']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="standard-block">
                        <div class="standard-wrapper">
                            <?= CHtml::submitButton($this->t('Send'), ['class' => 'btn red']) ?>
                        </div>
                    </div>
                    <?php $this->endWidget() ?>
                </div>
            </div>
        </div>
    </div>
</div>
