<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Model\Product\SizeGuideGroups;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\SmartHttpRequest;

/* @var $this FrontController */
/* @var $event EventRecord */
/* @var $productGroup GroupedProduct */
/* @var $othersProvider CArrayDataProvider */
/* @var $othersIsSimilar boolean */
/* @var $size string */
/* @var $returnUrl string */

$app = $this->app();
/** @var SmartHttpRequest $req */
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
$splitTesting = $app->splitTesting;
/* @var $splitTesting SplitTesting */

$this->pageTitle = $productGroup->product->name . ' — ' . $productGroup->event->name;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
/** @var $cs CClientScript */
$cs = $app->clientScript;
$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/sizeguide.css'));

$currentUrl = $this->createAbsoluteUrl($req->url);
$eventMailingStartAt = $event->is_drop_shipping ? $event->end_at : $event->mailing_start_at;
$isPastEvent = ($event->end_at <= time());
$events = [];
if ($isPastEvent) {
    $events = EventRecord::model()
        ->isVirtual(false)
        ->onlyVisible()
        ->onlyPublished()
        ->onlyTimeActive()
        ->isDeleted(false)
        ->orderBy('end_at', 'desc')
        ->orderBy('priority', 'asc')
        ->cache(300)
        ->limit(3)
        ->findAll();
}

$targets = $productGroup->getTargets();
$categoryForPastUrl = '';
if (isset($targets[0])) {
    $categoriesByTarget = CategoryGroups::instance()->getCategoriesByTarget($targets[0]);
    if (isset($categoriesByTarget[0])) {
        $categoryForPastUrl = $categoriesByTarget[0];
    }
}
?>
<section>
    <header class="content-header container">
        <div class="promotional">
            <div class="title-wrapper">
                <?php if ($req->urlReferrer !== null && strpos($req->urlReferrer, 'catalog')) : ?>
                    <a class="back-to-catalog"
                       href="<?= $req->urlReferrer ?>"><?= $this->t('Back to catalog') ?></a>
                <?php endif; ?>
                <?php if (!$event->is_virtual): ?>
                    <a href="<?= $returnUrl ?>" itemprop="breadcrumb"><h2
                                class="title"><?= CHtml::encode($event->name) ?></h2></a>
                <?php else: ?>
                    <a href="<?= $app->createUrl('event/fast') ?>" itemprop="breadcrumb"><h2
                                class="title"><?= $this->t('Instant sale') ?></h2></a>
                <?php endif ?>
                <?php if ($event->is_drop_shipping): ?>
                    <a class="icon-fast-delivery what-is-it" target="_blank"
                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                       title="<?= $this->t("What is 'Fast Delievery'?") ?>"><span><?= $this->t('what is it?') ?></span></a>
                <?php endif; ?>
            </div>
            <div class="promotional-time">
                <span class="style"><?= CHtml::tag(
                        'time',
                        ['datetime' => $tf->formatMachine($event->end_at), 'data-countdown' => true],
                        $tf->format($event->end_at)
                    ) ?></span>
            </div>
        </div>
        <div class="promotional-info">
            <div class="info-text <?php if ($event->is_charity) { ?>charity-event<?php } ?>">
                <p><?= ''/*$event->description*/ ?></p>
            </div>
            <!--            --><?php //if($event->is_charity): ?>
            <!--                <a class="charity-link" href="-->
            <? //= $app->urlManager->createUrl('static/index', array('category' => 'company', 'page' => 'charity'))?><!--"></a>-->
            <!--            --><?php //endif ?>
        </div>
    </header>
    <div class="dotted"></div>
    <div class="container">
        <article itemscope itemtype="http://schema.org/Product">
            <meta itemprop="image" content="<?= $productGroup->product->getLogo()->url ?>">
            <meta itemprop="name" content="<?= $productGroup->product->name ?>">
            <meta itemprop="manufacturer" content="<?= $productGroup->product->brand->name ?>">
            <meta itemprop="color" content="<?= $productGroup->product->color ?>">
            <header class="content-header card-header">
                <h1>
                    <label class="gender">
                        <?php if ($productGroup->isForTarget(ProductTargets::TARGET_BOY) || $productGroup->isForTarget(ProductTargets::TARGET_MAN)): ?>
                            <i class="header-icon male"></i>
                        <?php endif ?>
                        <?php if ($productGroup->isForTarget(ProductTargets::TARGET_GIRL) || $productGroup->isForTarget(ProductTargets::TARGET_WOMAN)): ?>
                            <i class="header-icon female"></i>
                        <?php endif ?>
                    </label>
                    <span class="title"><?= $productGroup->product->name ?></span>
                </h1>
                <div class="title-price testing-variant">
                    <div class="title-new-price"><?= $cf->format($productGroup->price, $cn->getName('short')) ?></div>
                    <div class="title-old-price"><?= $cf->format($productGroup->priceOld, $cn->getName('short')) ?></div>
                    <div class="title-perc">-<?= $productGroup->discountPercent ?>%</div>
                </div>
                <?php /*
        <!--        <dl class="likes-bar">
                    <dt>Поделиться с друзьями:</dt>
                    <dd><?= CHtml::link(
                        'Vkontakte',
                        'https://vk.com/share.php?' .
                            'url=' . urlencode($currentUrl) . '&' .
                            'title=' . urlencode($this->pageTitle) . '&' .
                            'image=' . urlencode($productGroup->product->logo->getThumbnail('mid380')->url) . '&' .
                            'description=' . urlencode(Utf8::truncate($productGroup->product->description)) . '&' .
                            'noparse=true',
                        array('class' => 'vk', 'target' => '_blank'))
                    ?></dd>
                    <dd><?= CHtml::link(
                        'Facebook',
                        'https://www.facebook.com/sharer/sharer.php?' .
                            'u=' . urlencode($currentUrl),
                        array('class' => 'fb', 'target' => '_blank'))
                    ?></dd>
                    <dd><?= CHtml::link(
                        'Twitter',
                        'https://twitter.com/share?' .
                            'url=' . urlencode($currentUrl) . '&' .
                            'text=' . urlencode($this->pageTitle),
                        array('class' => 'tw', 'target' => '_blank'))
                    ?></dd>
                    <dd><?= CHtml::link(
                        'Google+',
                        'https://plus.google.com/share?' .
                            'url=' . urlencode($currentUrl) . '&' .
                            'hl=' . $app->locale->id,
                        array('class' => 'gp', 'target' => '_blank'))
                    ?></dd>
                </dl>
        --> */ ?></header>
            <div class="content-body card-body">
                <div class="image-bar">
                    <div class="box">
                        <div class="image-wrapper"></div>
                        <ul class="image-list">
                            <?php $productImages = $productGroup->product->getPhotogalleryImages(); ?>
                            <?php foreach ($productImages as $image): ?>
                                <li data-big-url="<?= $image->getThumbnail('mid380')->url ?>"
                                    data-source-url="<?= $image->file->getWebFullPath() ?>">
                                    <div class="hover-style"></div>
                                    <?= CHtml::image($image->getThumbnail('small70')->url) ?>
                                </li>
                            <?php endforeach ?>
                            <?php if (count($productImages) < 5 && $event->size_chart_fileid) : ?>
                                <li data-big-url="<?= $event->sizeChart->getThumbnail('mid380')->url ?>"
                                    data-source-url="<?= $event->sizeChart->file->getWebFullPath() ?>">
                                    <div class="hover-style"></div>
                                    <?= CHtml::image($event->sizeChart->getThumbnail('small70')->url) ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <script> $('.image-bar').productPhotogallery(); </script>
                </div>

                <div class="card">
                    <div class="about-item">
                        <label class="title"><?= $this->t('Description') ?></label>
                        <p class="text show" itemprop="description">
                            <?= $productGroup->product->description . ' '?>
                            <?php if ($productGroup->isSoldOut): ?>
                                <span class="unavailable"><?= $this->t('Product is sold out')?></span>
                            <?php endif; ?>
                        </p>
                    </div>
                    <div class="card-inner">
                        <?= $this->renderPartial('//layouts/_productJson', ['productGroup' => $productGroup]) ?>
                        <script>
                            Mamam.ecommerce.detail('<?= $productGroup->event->id . '/' . $productGroup->product->id ?>');
                        </script>
                        <form class="card-form" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

                            <meta itemprop="availabilityStarts"
                                  content="<?= $tf->formatMachine($productGroup->event->start_at) ?>">
                            <meta itemprop="availabilityEnds"
                                  content="<?= $tf->formatMachine($productGroup->event->end_at) ?>">
                            <meta itemprop="validFrom"
                                  content="<?= $tf->formatMachine($productGroup->event->start_at) ?>">
                            <meta itemprop="validThrough"
                                  content="<?= $tf->formatMachine($productGroup->event->end_at) ?>">
                            <meta itemprop="sku" content="<?= $productGroup->product->id ?>">

                            <?php if ($productGroup->event->isTimeActive && !$productGroup->isSoldOut): ?>
                                <link itemprop="availability" href="http://schema.org/InStock"/>
                            <?php else: ?>
                                <link itemprop="availability" href="http://schema.org/OutOfStock"/>
                            <?php endif ?>

                            <input type="hidden" name="token"
                                   value="<?= $tokenManager->getToken($productGroup->product->id) ?>">
                            <input type="hidden" name="eventId" value="<?= $event->id ?>">
                            <input type="hidden" name="productId" value="<?= $productGroup->product->id ?>">
                            <ul>
                                <li class="hide">
                                    <meta itemprop="priceCurrency" content="UAH">
                                    <label class="label"><?= $this->t('Price') ?></label>
                                    <div class="form-price old-price-larger indent-price">
                                        <?php if ($productGroup->priceOld != $productGroup->price): ?>
                                            <div class="old-price"><?= $cf->format($productGroup->priceOld, $cn->getName('short')) ?></div>
                                            <div class="new-price"><span class="number"
                                                                         itemprop="price"><?= $cf->format($productGroup->price) ?></span>
                                            </div>
                                            <div class="sale sale-bigger">-<?= $productGroup->discountPercent ?>%</div>
                                        <?php else: ?>
                                            <div class="new-price"><span class="number"
                                                                         itemprop="price"><?= $cf->format($productGroup->price) ?></span>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </li>
                                <li class="size <?php if ($productGroup->sizeCount == 0) { ?> hide <?php } ?>">
                                    <div class="size-select">
                                        <label class="label"><?= $this->t('Size') ?></label>
                                        <div class="form-size">
                                            <?php $this->widget('extSelect.widgets.ExtSelect', [
                                                'name' => 'size',
                                                'standardData' => true,
                                                'data' => $productGroup->eventProducts,
                                                'transformData' => function ($row, $item) use ($size, $cf, $cn) {
                                                    $product = $item['text'];
                                                    /* @var $product EventProductRecord */
                                                    $numberAvailable = $product->numberAvailable;
                                                    $text = $product->size;
                                                    if ($numberAvailable == 0) {
                                                        $text = CHtml::tag('s', [], $product->size);
                                                        $text .= CHtml::tag('span', ['class' => 'red'], ' (' . $this->t('ended') . ')');
                                                    }
                                                    return [
                                                        'val' => $product->size,
                                                        'text' => $text,
                                                        'selected' => $size ? $size == $product->size : $row == 0,
                                                        'storage' => [
                                                            'numberAvailable' => $numberAvailable,
                                                            'maxPerBuy' => $product->max_per_buy,
                                                            'price' => $cf->format($product->price, $cn->getName('short')),
                                                            'priceOld' => $cf->format($product->price_market, $cn->getName('short')),
                                                            'discount' => round((1 - $product->price / $product->price_market) * 100),
                                                            'sizeGuide' => $product->size_guide,
                                                        ],
                                                    ];
                                                },
                                                'settings' => [
                                                    'placeholder' => $this->t('Choose a size'),
                                                    'clearSelection' => false,
                                                    'dropdown' => [
                                                        'search' => false,
                                                    ],

                                                    'events' => [
                                                        'select initialize' => new CJavaScriptExpression('function() {
                                                            var storage = this.selectedItem().storage;
                                                            var max = Math.min(storage.numberAvailable, storage.maxPerBuy);
        
                                                            // force max
                                                            $(function() {
                                                                var $number = $(".form-number");
                                                                $number.attr("data-max", max);
                                                                $number.find(":input:visible").trigger("change");
                                                            });
                                                        }'),
                                                    ],
                                                ],
                                            ]) ?>
                                        </div>
                                    </div>
                                    <?php $sizeGuide = $productGroup->getSizeGuide() === '' ? false : SizeGuideGroups::instance()->getGroupTargetForAssociation($productGroup->getSizeGuide()) ?>
                                    <div class="size-description <?= $sizeGuide ? 'new-size-description' : 'hide-i' ?>">
                                        <?php if ($sizeGuide) : ?>
                                            <a data-toggle="modal" href="#sizeGuideModal"
                                               data-src="<?= $app->createUrl('sizeGuide/group', ['name' => $sizeGuide]) ?>"><?= $this->t('which size to choose') ?>
                                                <i class="icon-size-description"></i></a>
                                        <?php endif; ?>
                                    </div>
                                </li>
                                <li>
                                    <label class="label"><?= $this->t('Amount') ?></label>
                                    <div class="form-number" data-min="1" data-max="<?= $productGroup->maxPerBuy ?>">
                                        <div class="minus">&minus;</div>
                                        <input type="hidden" name="number" value="1">
                                        <input type="text" value="1">
                                        <div class="plus">&#43;</div>
                                    </div>
                                    <script> $('.form-number').numericalSpinner(); </script>
                                </li>
                                <?php if ($isPastEvent) : ?>
                                    <li class="rel">
                                        <div class="action-end">
                                            <div class="action-end-header">
                                                <p>
                                                    <?= $this->t('The stock event with this product has already passed<br />...but we can offer you <a href="{url}">hundreds of similar products</a>with<br/></span> <span class="big-text"> the lowest prices!</span>', ['{url}' => $app->createUrl('event/category', ['category' => $categoryForPastUrl])]) ?>
                                                </p>
                                                <div class="close"></div>
                                            </div>
                                            <div class="action-end-content">
                                                <?php foreach ($events as $item): /* @var $item EventRecord */ ?>
                                                    <div class="action-end-elem">
                                                        <a href="<?= $app->createUrl('event/category', ['category' => $categoryForPastUrl]) ?>">
                                                            <div class="img">
                                                                <div class="discount">
                                                                    -<?= $item->promoDiscountPercent ?>%
                                                                </div>
                                                                <?= CHtml::image($item->logo->getThumbnail('mid320')->url) ?>
                                                            </div>
                                                            <div class="desc">
                                                                <?= CHtml::encode($item->name) ?>
                                                            </div>
                                                            <div class="price"><?= $this->t('from') ?> <?= $cf->format($item->getMinSellingPrice()) ?></div>
                                                        </a>
                                                    </div>
                                                <?php endforeach ?>
                                                <a href="<?= $app->createUrl('event/category', ['category' => $categoryForPastUrl]) ?>"
                                                   class="action-end-next"></a>
                                            </div>
                                            <script type="text/javascript">
                                                $(function () {
                                                    $(".action-end .close").click(function () {
                                                        $(".action-end").hide();
                                                    });
                                                });
                                            </script>
                                        </div>
                                        <a href="<?= $app->createUrl('event/category', ['category' => $categoryForPastUrl]) ?>"
                                           type="button" class="opened btn green-dark"
                                           style="width: 300px;"><?= $this->t('OPEN') ?><i
                                                    class="icon-opened"></i></a>
                                    </li>
                                <?php else: ?>
                                    <li>
                                        <button class="in-cart btn green <?= $isPastEvent ? 'disabled' : '' ?>" <?= $isPastEvent ? 'disabled="disabled"' : '' ?>>
                                            <i class="icon-in-cart"></i><?= $this->t('Add to cart') ?></button>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </form>
                        <ul class="product-info">
                            <?php if ($event->is_drop_shipping && $event->supplier): ?>
                                <li>
                                    <label class="label"><?= $this->t('Provider') ?></label>
                                    <span class="info"><?= CHtml::encode($event->supplier->getDisplayName()) ?></span>
                                </li>
                            <?php endif; ?>
                            <?php foreach ($productGroup->productBaseAttributes as $attribute => $data): ?>
                                <li>
                                    <label class="label"><?= CHtml::encode($data['label']) ?></label>
                                    <span class="info"><?= CHtml::encode($data['value']) ?></span>
                                </li>
                            <?php endforeach ?>
                            <li>
                                <label class="label"><?= $this->t('Delivery') ?></label>
                                <span class="info"><?= $this->t('from') ?> <?= $df->format('d MMMM yyyy', $eventMailingStartAt) ?></span>
                            </li>
                            <?php if (!empty($productGroup->getCertificate())): ?>
                                <li>
                                    <label class="label"><?= $this->t('Certificate') ?></label>
                                    <span class="info"><?= $productGroup->getCertificate() ?></span>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php if (!empty($productGroup->getCertificate())): ?>
                        <aside class="payment-info certificate">
                            <p><?= $this->t('You can check certificate on the next website') ?></p>
                        </aside>
                    <?php endif; ?>
                    <aside class="payment-info">
                        <h4 class="title"><a target="_blank"
                                             href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'recurrence']) ?>"><?= $this->t('Return') ?></a>
                        </h4>
                        <p><?= $this->t('Within 14 days from the receipt of the order you can fully or partially return the goods purchased from us') ?></p>
                    </aside>
                </div>
            </div>
        </article>
        <?php if (!$othersIsSimilar || $othersProvider->totalItemCount >= 6): ?>
            <aside class="other-goods-aside clearfix">
                <div class="lined-header">
                    <?php if ($othersIsSimilar): ?>
                        <h3 class="title">
                            <span><?= $this->t('People who viewed this item also viewed') ?></span></h3>
                    <?php else: ?>
                        <h3 class="title"><span><?= $this->t('You recently searched for') ?></span></h3>
                    <?php endif ?>
                </div>
                <div class="other-goods">
                    <?php foreach (array_slice($othersProvider->getData(), 0, 6) as $item): /* @var $item GroupedProduct */ ?>
                        <div class="other-item" itemscope itemtype="http://schema.org/Product">
                            <a href="<?= $app->createUrl('product/index', ['id' => $item->product->id, 'eventId' => $item->event->id]) ?>">
                                <div class="box">
                                    <div class="image-wrapper">
                                        <?= CHtml::image($item->product->logo->getThumbnail('small150')->url, '', ['itemprop' => "image"]) ?>
                                    </div>
                                    <div class="text-wrapper">
                                        <h4 class="title"
                                            itemprop="name"><?= CHtml::encode($item->product->name) ?></h4>
                                        <div class="price">
                                            <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                            <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>
            </aside>
        <?php endif ?>
        <!-- Size guide modal box -->
        <div class="modal fade" id="sizeGuideModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body size-guide-modal-body">
                    </div>
                </div>

            </div>
        </div>
        <!-- Size modal block end -->
    </div>
</section>
<script>
    /*Modal box info request*/
    $(document).ready(function () {
        $('#sizeGuideModal').hide();
        var url = $('a[href="#sizeGuideModal"]').data('src');
        if (null !== url && undefined !== url) {
            $.ajax({
                url: url,
                dataType: 'html',
                success: function (data) {
                    var actualData = $(data).find('.supreme-inner').html();
                    $('.size-guide-modal-body').html(actualData);
                }
            });
        }
    });
</script>
