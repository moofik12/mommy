<?php
/**
 * @author: Bondarev
 */
?>
<section>
    <header>
        <div class="page-title promotional">
            <h1 class="title"><?= $this->t('Footwear') ?></h1>
            <h2 class="size-table-title"><?= $this->t('Children\'s shoes') ?></h2>
        </div>
    </header>
    <div class="container">
        <div class="content-body">
            <div class="size-content">
                <table id="size-shoes-first" class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Foot<br>size') ?> (cm)</td>
                        <td class="shaded color">9.5</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">10.5</td>
                        <td class="shaded color">11</td>
                        <td class="shaded color">11.5</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">12.5</td>
                        <td class="shaded color">13</td>
                        <td class="shaded color">13.5</td>
                        <td class="shaded color">14</td>
                        <td class="shaded color">14.5</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded color">16</td>
                        <td class="shaded color">16.5</td>
                        <td class="shaded color">17</td>
                        <td class="shaded color">18</td>
                        <td class="shaded color">19</td>
                        <td class="shaded color">19.5</td>
                        <td class="shaded color">20</td>
                        <td class="shaded color">21</td>
                        <td class="shaded color">22</td>
                        <td class="shaded color">22.5</td>
                        <td class="shaded color">23</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded color">1</td>
                        <td class="shaded color">1.2</td>
                        <td class="shaded color">2</td>
                        <td class="shaded color">2.5</td>
                        <td class="shaded color">3</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">5</td>
                        <td class="shaded color">5.5</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">6.5</td>
                        <td class="shaded color">7</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded color">0</td>
                        <td class="shaded color">0-1</td>
                        <td class="shaded color">1</td>
                        <td class="shaded color">1.5</td>
                        <td class="shaded color">2.5</td>
                        <td class="shaded color">3</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">4.5</td>
                        <td class="shaded color">5</td>
                        <td class="shaded color">5.5</td>
                        <td class="shaded color">6-6.5</td>
                    </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h2 class="size-table-title"><?= $this->t('Women\'s shoes') ?></h2>
                <table id="size-shoes-second" class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Foot<br>size') ?> (cm)</td>
                        <td class="shaded color">21</td>
                        <td class="shaded color">21.5</td>
                        <td class="shaded color">22.5</td>
                        <td class="shaded color">23</td>
                        <td class="shaded color">23.5</td>
                        <td class="shaded color">24.5</td>
                        <td class="shaded color">25</td>
                        <td class="shaded color">25.5</td>
                        <td class="shaded color">26.5</td>
                        <td class="shaded color">27</td>
                        <td class="shaded color">27.5</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded color">34</td>
                        <td class="shaded color">35</td>
                        <td class="shaded color">36</td>
                        <td class="shaded color">37</td>
                        <td class="shaded color">38</td>
                        <td class="shaded color">39</td>
                        <td class="shaded color">40</td>
                        <td class="shaded color">41</td>
                        <td class="shaded color">42</td>
                        <td class="shaded color">43</td>
                        <td class="shaded color">44</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">4.5</td>
                        <td class="shaded color">5.5</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">7</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">8.5</td>
                        <td class="shaded color">9.5</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">11</td>
                        <td class="shaded color">11.5</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded color">2</td>
                        <td class="shaded color">2.5</td>
                        <td class="shaded color">3.5</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">5</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">6.5</td>
                        <td class="shaded color">7.5</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">9</td>
                        <td class="shaded color">9.5</td>
                    </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h2 class="size-table-title"><?= $this->t('Men\'s shoes') ?></h2>
                <table class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Foot<br>size') ?> (cm)</td>
                        <td class="shaded color">24</td>
                        <td class="shaded color">25</td>
                        <td class="shaded color">25.5</td>
                        <td class="shaded color">26.5</td>
                        <td class="shaded color">27</td>
                        <td class="shaded color">27.5</td>
                        <td class="shaded color">28.5</td>
                        <td class="shaded color">29</td>
                        <td class="shaded color">29.5</td>
                        <td class="shaded color">30.5</td>
                        <td class="shaded color">31</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded color">39</td>
                        <td class="shaded color">40</td>
                        <td class="shaded color">41</td>
                        <td class="shaded color">42</td>
                        <td class="shaded color">43</td>
                        <td class="shaded color">44</td>
                        <td class="shaded color">45</td>
                        <td class="shaded color">46</td>
                        <td class="shaded color">47</td>
                        <td class="shaded color">48</td>
                        <td class="shaded color">49</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded color">6.5</td>
                        <td class="shaded color">7</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">8.5</td>
                        <td class="shaded color">9.5</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">11</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">12.5</td>
                        <td class="shaded color">13.5</td>
                        <td class="shaded color">14</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">6.5</td>
                        <td class="shaded color">7.5</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">9</td>
                        <td class="shaded color">9.5</td>
                        <td class="shaded color">10.5</td>
                        <td class="shaded color">11.5</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">13</td>
                        <td class="shaded color">13.5</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
