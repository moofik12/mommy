<?php

use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();
$user = $this->app()->user;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <?php if (strpos($this->bodyClass, 'land') !== false): ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
    <?php endif; ?>
    <?php $this->renderPartial('//layouts/_headScripts') ?>
</head>
<body class="<?= $this->bodyClass ?>">
<?php $this->renderPartial('//layouts/_globalScripts') ?>
<?= $content; ?>
</body>
</html>
