<?php

use MommyCom\Model\Form\RegistrationOfferForm;

/**
 * @var CController $this
 */

$cookies = $this->app()->request->cookies;
$formOfferRegistration = new RegistrationOfferForm();
$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
$user = $this->app()->user;
$splitTesting = $this->app()->splitTesting;

$notAvailableController = ['cart', 'order'];
$userTestTargetActive = !$user->isGuest && $user->cart->count >= 2;

$testName = 'unformed-cart-alien';
$bannerNumber = $splitTesting->getNum($testName, 1);
$bannerName = 'alien';
$enableBanner = $userTestTargetActive && !in_array($this->id, $notAvailableController);
if ($cookies->itemAt($testName) !== null) {
    $enableBanner = false;
}
?>

<!-- basket -->
<div class="modal hide cool-modal-1" id="cool-modal-1">
	<?php if ($userTestTargetActive) : ?>
		<!-- preload -->
		<img height="0" src="<?= $baseImgUrl . 'cool-modal-1-bg.jpg' ?>" width="0">
	<?php endif ?>
	<div class="modal-wrapper">
		<div class="modal-body">
			<span class="close" data-dismiss="modal"></span>
			<a class="modal-an-order" href="<?= $this->createUrl('splitTesting/unformedCartActivate', array('redirectUrl' => urlencode($this->createUrl('order/delivery')))) ?>"></a>
			<a class="modal-view-cart" href="<?= $this->createUrl('splitTesting/unformedCartActivate', array('redirectUrl' => urlencode($this->createUrl('cart/index')))) ?>"></a>
		</div>
	</div>
</div>
<!-- basket -->

<?php
/** @var $cs CClientScript */
$cs = $this->app()->clientScript;
$urlActive = $this->createUrl('splitTesting/unformedCart'); //регистрация показа баннера
$enableBanner = CJSON::encode($enableBanner);

$textOpen = $this->t('Opening the window final products') . ' (' . $bannerName . ')';
$textClose = $this->t('Close the final products window') . ' (' . $bannerName . ')';

$cs->registerScript('modalUnformedCart', "
    var modalObject = $('[id^=cool-modal]:first');

    modalObject
    .on('show', function() {
        Mamam.trackEvent('modals', 'unformed-cart-alien', '$textOpen');
    })
    .on('hide', function() {
        Mamam.trackEvent('modals', 'unformed-cart-alien', '$textClose');
    });

    Mamam.activateEvents.exitIntent({active:$enableBanner}, null, function() {
        if (modalObject.length > 0) {
            modalObject.modal('show');

            $.ajax({
                url: \"$urlActive\"
            });
        }
    });
");
?>
