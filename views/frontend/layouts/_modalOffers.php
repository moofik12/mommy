<?php

use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

$cookies = $this->app()->request->cookies;
$formOfferRegistration = new RegistrationOfferForm();
$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
$user = $this->app()->user;
/** @var  $user ShopWebUser */
$splitTesting = $this->app()->splitTesting;
/** @var  $splitTesting \MommyCom\Service\SplitTesting */

$enableOffer = $user->isGuest && $user->isEnableOfferBenefice();

$textReg = $this->t('Registration is completed via modal window №');
$textErrorBonus = $this->t('An error occurred while calculating bonuses. Contact Support');
$textError = $this->t('Error. Check your internet connection');
$textOpen = $this->t('Opening the registration window №');
$textClose = $this->t('Close the modal registration № window');
$textOpen1 = $this->t('The first open modal registration window №');

?>
<script>
    var isShowInfoAfterMailingType = false;
    var comebackerModalIsOpen = false;
</script>
<!-- offers -->
<?php if ($enableOffer): ?>
    <?php
    $modalPresentStartUp = $cookies->contains('isOfferPresent') === false;
    $specialPsychotypeBanner = $user->getOfferBannerNumber(false);
    $bannerName = $specialPsychotypeBanner == false ? "PS:default" : "PS:$specialPsychotypeBanner";

    if ($modalPresentStartUp) {
        $cookies->add('isOfferPresent', new CHttpCookie('isOfferPresent', 1, ['expire' => strtotime('tomorrow')]));
    }
    ?>

    <!-- Offer benefice 100 for 1kr and yandex direct -->
    <?php
    $formOfferRegistration->benefice = RegistrationOfferForm::BENEFICE_100;
    $formOfferRegistration->strategy = RegistrationOfferForm::STRATEGY_PROMOCODE;
    $formOfferRegistration->strategyPayments = RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY;
    $formOfferRegistration->bannerName = $bannerName;
    ?>

    <?php if ($specialPsychotypeBanner == 4): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>

        <div class="modal hide present-modal5" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button" type="submit"></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($specialPsychotypeBanner == 2): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>

        <div class="modal hide present-modal7" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();    
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button" type="submit"></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($specialPsychotypeBanner == 1): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>

        <div class="modal hide present-modal8" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="present-modal8-img"></div>
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button" type="submit"></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($specialPsychotypeBanner == 3): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>

        <div class="modal hide present-modal6" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="present-modal6-img"></div>
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button" type="submit"></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($specialPsychotypeBanner == 5): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>
        <div class="modal hide present-modal9" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button" type="submit"><?= $this->t('Get a gift') ?></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($specialPsychotypeBanner == 6): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>
        <div class="modal hide present-modal10" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button"
                                    type="submit"><?= $this->t('Get a discount on the purchase') ?></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>
                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>

    <?php elseif ($specialPsychotypeBanner == 7): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>
        <div class="modal hide present-modal11" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="present-modal11-img"></div>
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button"
                                    type="submit"><?= $this->t('Start Shopping') ?></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>

    <?php elseif ($specialPsychotypeBanner == 8): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>
        <div class="modal hide present-modal12" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="present-modal12-img"></div>
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <?php
                        /** @var CActiveForm $formOffer */
                        $formOffer = $this->beginWidget('CActiveForm', [
                            'action' => ['auth/offerRegistration'],
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'clientOptions' => [
                                'validateOnSubmit' => true,
                                'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    document.getElementById('present').remove();    
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                            ],
                        ]) ?>
                        <div class="standard-block">
                            <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                <label><?= $this->t('Enter your email address') ?></label>
                                <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            </div>
                            <button class="modal-button"
                                    type="submit"><?= $this->t('Get a discount') ?></button>
                        </div>
                        <div class="checkbox-field">
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <label>
                                <?= $this->t('By sending your e-mail you agree with the <a target="_blank" href="{url}">Terms of Use</a>', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                            </label>
                        </div>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>

                        <?php $this->endWidget() ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($specialPsychotypeBanner == 10): ?>
        <?php $formOfferRegistration->isConfirmPrivacy = 1; ?>
        <div class="modal hide present-modal14" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <div class="modal14-content">
                            <div class="modal14-top">
                                <div class="modal14-logo">
                                    <img src="<?= $baseImgUrl ?>logo-mommy.svg" alt="" width="192" height="34"/>
                                </div>
                                <div class="modal14-discount">
                                    <p><?= $this->t('Discounts on quality branded clothes<br /> and accessories up to -70%') ?></p>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <h2><?= $this->t('SUPER discounts from MOMMY!') ?></h2>
                            <p><?= $this->t('A unique opportunity to take part in the draw and get a discount on your first order!') ?></p>
                            <div class="modal14-bonus">
                                <div><?= $this->t('You can <b>be guaranteed</b> to get <br /> from <span>1</span>to<span>3</span> $ bonuses <br /> for your first purchase!') ?></div>
                            </div>
                            <?php
                            /** @var CActiveForm $formOffer */
                            $formOffer = $this->beginWidget('CActiveForm', [
                                'action' => ['auth/offerRegistration'],
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => [
                                    'validateOnSubmit' => true,
                                    'afterValidate' => "js:function(form, data, hasError){
                                        if (hasError) {
                                            return;
                                        }                
                                        $.ajax({
                                            url: form.attr('action'),
                                            type: form.attr('method'),
                                            data: form.serialize(),
                                            dataType: 'json',
                                            success: function (data) {
                                                if (data !== null && typeof data === 'object') {
                                                    if (data.success) {
                                                        form.closest('.modal').modal('hide');                                                        
                                                        form[0].reset();
                                                        document.getElementById('present').remove();
                                                        $(document).trigger('mamam.registration.success');                                                        
                                                        $(document).trigger('mamam.auth.success');                                                        
                                                        Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                        isShowInfoAfterMailingType = true;
                                                        $('.get-gift-wrapper').hide();
                                                        $('#mailing-type').modal('show');
                                                    } else  {
                                                        alert('$textErrorBonus');
                                                    }
                                                }
                                            },
                                            error: function () {
                                                alert('$textError');
                                            }
                                        });
                                        return false;
                                    }",
                                ],
                            ]) ?>
                            <div class="modal14-form">
                                <div class="standard-block">
                                    <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                        <label><?= $this->t('Enter your email address') ?></label>
                                        <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                        <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                                    </div>
                                    <button type="submit"><?= $this->t('Participate in the drawing') ?></button>
                                </div>
                                <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                                <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                                <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                                <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>
                                <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            </div>
                            <?php $this->endWidget() ?>
                            <h3><?= $this->t('Everybody, without exception, WINS in our raffle!') ?></h3>
                            <div class="modal14-details">
                                <a href="<?= $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode']) ?>"
                                   target="_blank"><?= $this->t('Terms of use') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal hide present-modal15" id="present11">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <div class="modal15-logo">
                            <img src="<?= $baseImgUrl ?>logo-mommy.svg" alt="" width="192" height="34"/>
                        </div>
                        <div class="modal15-text">
                            <div>
                                <p><?= $this->t('Congratulations!<br />You won!') ?></p>
                                <strong><?= $this->t('100 IDR') ?></strong>
                                <div class="present-info"><?= $this->t('You have the promotional code in your mail') ?></div>
                            </div>
                        </div>
                        <a href="#" class="modal15-btn"
                           data-dismiss="modal"><?= $this->t('Start shopping') ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <?php
        //баннер с эмблемами
        $bannerNumber = 0;
        $bannerName = ' box-100-emblems';
        $formOfferRegistration->bannerName = $bannerName;
        $formOfferRegistration->isConfirmPrivacy = true;
        ?>
        <div class="modal hide present-modal20" id="present">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="modal-body">
                        <h2><?= $this->t('We are trusted by thousands of buyers!') ?></h2>
                        <div class="present-modal20-elements">
                            <div class="present-modal20-item present-modal20-icon1">
                                <p><?= $this->t('Одобрено<br /> мамами') ?></p>
                            </div>
                            <div class="present-modal20-item present-modal20-icon2">
                                <p><?= $this->t('Guarantee <br /> of refunds') ?></p>
                            </div>
                            <div class="present-modal20-item present-modal20-icon3">
                                <p><?= $this->t('10 000 happy<br /> clients') ?></p>
                            </div>
                        </div>
                        <div class="present-modal20-gift">
                            <span><?= $this->t('We gift </b> <b class="size2">3</b><b class="curr"> $</b><b class="size1"> to obtain <br /> new dresses</b>') ?></span>
                        </div>
                        <div class="present-modal20-form">
                            <?php
                            /** @var CActiveForm $formOffer */
                            $formOffer = $this->beginWidget('CActiveForm', [
                                'action' => ['auth/offerRegistration'],
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => [
                                    'validateOnSubmit' => true,
                                    'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    form.closest('.modal').remove();    
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('.get-gift-wrapper').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                                ],
                            ]) ?>
                            <div class="standard-block">
                                <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                                    <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                                    <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                                </div>
                                <button class="btn" type="submit"><?= $this->t('Take money') ?></button>
                                <div class="checkbox-field">
                                    <div>
                                        <?= $this->t('I agree with the <a target="_blank" href="{url}">rules</a> of using the site', ['{url}' => $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?>
                                    </div>
                                </div>
                            </div>
                            <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                            <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                            <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                            <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>
                            <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                            <?php $this->endWidget() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <!-- Offer benefice 100 for 1kr and yandex direct -->
    <!--begin  success-info-landing -->
    <div class="modal hide present-modal15" id="success-info-landing">
        <div class="modal-wrapper">
            <div class="modal-inner">
                <div class="close" data-dismiss="modal"></div>
                <div class="modal-body">
                    <div class="modal15-logo">
                        <img src="<?= $baseImgUrl ?>logo-mommy.svg" alt="" width="192" height="34"/>
                    </div>
                    <div class="modal15-text">
                        <div>
                            <p><?= $this->t('Thank you for registering! <br/> You get') ?></p>
                            <strong<?= $this->t('100 IDR') ?></strong>
                            <div class="present-info"><?= $this->t('You have the promotional code in your mail') ?></div>
                        </div>
                    </div>
                    <a href="#" class="modal15-btn" data-dismiss="modal"><?= $this->t('Start shopping') ?></a>
                </div>
            </div>
        </div>
    </div>
    <!-- end success-info-landing -->
    <?php
    $checkBoxId = CHtml::activeId($formOfferRegistration, 'isConfirmPrivacy');
    $jsStartUp = CJavaScript::encode(!!$modalPresentStartUp);

    /** @var $cs CClientScript */
    $cs = $this->app()->clientScript;
    $cs->registerScript('modalOffers', "
    $('#{$formOffer->id}').on('click', '#$checkBoxId', function() {
        var self = $(this);
        var submitButton = self.closest('form').find('[type=submit]');

        if(self.prop('checked')) {
            submitButton.attr('disabled', false);
        } else {
            submitButton.attr('disabled', 'disabled');
        }
    });

    var modal = $('#present');

    modal
    .on('show', function() {
        Mamam.trackEvent('modals', 'present-registration', '$textOpen.$bannerName');
    })
    .on('hide', function() {
        Mamam.trackEvent('modals', 'present-registration', '$textClose.$bannerName');
    });

    if($jsStartUp) {
        Mamam.shownModalOnStart = true;
        Mamam.trackEvent('modals', 'present-registration', '$textOpen1.$bannerName');
        $('#present').modal('show');
    }

    Mamam.activateEvents.exitIntent({active:true}, null, function() {
        if (modal.length > 0 && !isShowInfoAfterMailingType) {
            modal.modal('show');
        }
    });
");
    ?>
<?php elseif ($user->isGuest) : ?>
    <?php //$this->renderPartial('//layouts/modals/_comebacker') ?>
<?php endif ?>
<!-- offers -->
<!-- distribution begin -->
<div class="modal hide mailing-type" id="mailing-type" data-backdrop="static">
    <div class="modal-wrapper">
        <div class="modal-inner">
            <div class="modal-body">
                <div class="mailing-type-header">
                    <div class="header-image"></div>
                    <div class="header-title">
                        <h2><?= $this->t('We often launch new shares with the best<br /> products at the lowest prices for you') ?></h2>
                    </div>
                </div>
                <div class="mailing-type-content">
                    <h3><?= $this->t('Choose which e-mail notifications you want to receive from us') ?></h3>
                    <div class="mailing-type-items">
                        <div class="mailing-type-item">
                            <?= CHtml::ajaxLink(
                                '<div class="img mailing-icon3"></div>
                                <p>' . Yii::t("common", "Newsletters with individual offers") . '</p>
                                <div class="select"><span>' . Yii::t("common", "Choose") . '</span></div>',
                                $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_WEEK]),
                                [
                                    'success' => 'function(data) {
                                            if (data.success) {
                                                $("#mailing-type").modal("hide");
                                                if (isShowInfoAfterMailingType) {
                                                    if ($("#success-info-landing").length > 0) {
                                                        $("#success-info-landing").modal("show");
                                                    }
                                                }
                                                return;
                                            }                                    
                                            var error = data.errors.length > 0 ? data.errors[0] : false;
                                            if (error) {
                                                alert(error);
                                            }
                                        }',
                                    'error' => 'function() {
                                            alert("' . $textError . '");
                                        }',
                                ],
                                ['class' => 'block-link']
                            ) ?>
                        </div>
                        <div class="mailing-type-item center-item">
                            <?= CHtml::ajaxLink(
                                '<div class="img mailing-icon2"></div>
                                <p>' . Yii::t("common", "Letters of new promotions, individual offers, <span>dollar's bonus</span> for <br /> purchases and <span>gifts</span>") . '</p>                                
                                <div class="select"><span>' . Yii::t("common", "Choose") . '</span></div>',
                                $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_DAY]),
                                [
                                    'success' => 'function(data) {
                                            if (data.success) {
                                                $("#mailing-type").modal("hide");
                                                if (isShowInfoAfterMailingType) {
                                                    if ($("#success-info-landing").length > 0) {
                                                        $("#success-info-landing").modal("show");
                                                    }
                                                }
                                                return;
                                            }
                                            
                                            var error = data.errors.length > 0 ? data.errors[0] : false;
                                            if (error) {
                                                alert(error);
                                            }
                                        }',
                                    'error' => 'function() {
                                            alert("' . $textError . '");
                                        }',
                                ],
                                ['class' => 'block-link']
                            ) ?>
                        </div>
                        <div class="mailing-type-item">
                            <?= CHtml::ajaxLink(
                                '<div class="img mailing-icon1"></div>
                                <p>' . Yii::t("common", "Letters<br />about new stocks") . '</p>
                                <div class="select"><span>' . Yii::t("common", "Choose") . '</span></div>',
                                $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_DAY]),
                                [
                                    'success' => 'function(data) {
                                            if (data.success) {
                                                $("#mailing-type").modal("hide");
                                                if (isShowInfoAfterMailingType) {
                                                    if ($("#success-info-landing").length > 0) {
                                                        $("#success-info-landing").modal("show");
                                                    }
                                                }
                                                return;
                                            }
                                            
                                            var error = data.errors.length > 0 ? data.errors[0] : false;
                                            if (error) {
                                                alert(error);
                                            }
                                        }',
                                    'error' => 'function() {
                                            alert("' . $textError . '");
                                        }',
                                ],
                                ['class' => 'block-link']
                            ) ?>
                        </div>
                    </div>
                    <h3><?= $this->t('Please, enable notifications') ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- distribution end -->
<?php if (isset($_GET['success-reg-landing'])): ?>
    <?php if ($_GET['success-reg-landing'] == 'success'): // если пришло с лендинга ?>
        <div class="modal hide present-modal15" id="success-info-land">
            <div class="modal-wrapper">
                <div class="modal-inner">
                    <div class="close" data-dismiss="modal"></div>
                    <div class="modal-body">
                        <div class="modal15-logo">
                            <img src="<?= $baseImgUrl ?>logo-mommy.svg" alt="" width="192" height="34"/>
                        </div>
                        <div class="modal15-text">
                            <div>
                                <p><?= $this->t('Thank you for registering! <br/> You get') ?></p>
                                <strong><?= $this->t('100 IDR') ?></strong>
                                <div class="present-info"><?= $this->t('You have the promotional code in your mail') ?></div>
                            </div>
                        </div>
                        <a href="#" class="modal15-btn"
                           data-dismiss="modal"><?= $this->t('Start shopping') ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php /** @var $cs CClientScript */
        $cs = $this->app()->clientScript;
        $cs->registerScript('showSuccessInfoLanding', '
            $("#success-info-land").modal("show");
        ', CClientScript::POS_READY); ?>
    <?php endif; ?>
<?php endif; ?>
<?php if ($user->hasFlash('message')): ?>
    <?php
    /** @var $cs CClientScript */
    $cs = $this->app()->clientScript;
    $cs->registerScript('alertMessage', '
        $("#alert-window-message").modal("show");
    ', CClientScript::POS_READY);
    ?>
    <div class="modal hide login login-social-end" id="alert-window-message">
        <div class="modal-wrapper">
            <div class="modal-body">
                <div class="forgot">
                    <div class="form-title"
                         style="text-align: center;"><?= $this->t('Message from MOMMY') ?></div>
                    <div class="form-title" style="text-align: center; font-weight: normal;"
                         id="inform-message-welcome"><?= $this->app()->user->getFlash('message') ?></div>
                    <div class="login-vk-group">
                        <?= CHtml::submitButton('OK', ['style' => 'margin:0 auto; width: 100%;', 'onclick' => '$(this).closest(".modal").modal("hide");']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
