<?php

use MommyCom\YiiComponent\ClientScript;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Utf8;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();
/* @var ClientScript $clientScript */
$clientScript = $app->clientScript;
$clientScript->registerPackage('lottery');
?>
<!doctype html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta name="theme-color" content="#ff7c91">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <link rel="alternate"
          href="android-app://<?= $app->params['androidAppId'] ?>/<?= Utf8::replace('://', '/', $app->createAbsoluteUrl($app->request->url)) ?>">
    <meta name="description"
          content="<?= $this->t('The first shopping-club for children and moms in the USA mommy.com. Discounts of up to 70%.') ?>">
    <!--[if IE 8]>
    <script
            src="//cdnjs.cloudflare.com/ajax/libs/ie8/0.4.1/ie8.js"
    ></script><![endif]-->
</head>
<body class="lottery">
<?php $this->renderPartial('//layouts/_globalScripts') ?>

<?= $content ?>

</body>
</html>
