<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductBrands;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\BuildUrlManager;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Utf8;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();

$phone = $app->params['phone'];

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$sp = StaticPageRecord::model();

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
$regionName = $regions->getUserRegion()->getRegionName();
$facebookLink = $this->app()->params['facebookLinks'][$regionName];

$user = $this->app()->user;
/** @var  $user ShopWebUser */
$cart = $this->app()->user->cart;
$bonuspoints = $this->app()->user->bonuspoints;
$splitTesting = $this->app()->splitTesting;

$eventsIds = EventRecord::model()->display(300)->findColumnDistinct('id');
$productBrandsId = $this->app()->db
    ->cache(300)
    ->createCommand()
    ->selectDistinct('brand_id')
    ->from(EventProductRecord::model()->tableName())
    ->where(['IN', 'event_id', $eventsIds])
    ->queryColumn();

$productBrands = ProductBrands::fromBrandsId($productBrandsId);

$urlManager = $this->app()->getComponent('urlManager');
/* @var $urlManager BuildUrlManager */

$mobileUrlManager = $this->app()->getComponent('mobileUrlManager');
/* @var $mobileUrlManager BuildUrlManager */

$bodyClassCss = in_array($this->id, ['event']) ? ' responsive' : '';
$bodyClassCss .= ($this->id === 'order' && $this->action->id === 'auth') ? ' pre-registration' : '';

$isShowInfoMailBlock = false;
/*if (!$user->isGuest) {
    $userModel = $user->getModel();
    $emailParts = explode('@', $userModel->email);
    $rusEmails = array('mail.ru', 'yandex.ru', 'yandex.ua', 'ya.ru', 'list.ru', 'inbox.ru', 'bk.ru', 'vk.ru', 'ok.ru', 'mail.ua', 'rambler.ru', 'yandex.kz', 'yandex.com', 'yandex.by');
    if (in_array($emailParts[1], $rusEmails)) {
        $isShowInfoMailBlock = true;
    }
}*/

/** @var $cs CClientScript */
$cs = $this->app()->clientScript;
$cs->registerScriptFile($app->createUrl('language/dictionary'));

$staticPages = $this->app()->params['staticPages'];
?>
<!doctype html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta name="theme-color" content="#ff7c91">
    <link rel=manifest href="<?= $app->baseUrl ?>/manifest.json">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css"
          integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css"
          integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <link rel="alternate"
          href="android-app://<?= $app->params['androidAppId'] ?>/<?= Utf8::replace('://', '/', $app->createAbsoluteUrl($app->request->url)) ?>">
    <meta name="description"
          content="<?= $this->description ? $this->description : $this->t('The first shopping-club for children and moms in the USA mommy.com. Discounts of up to 70%.') ?>">
    <meta name="google-site-verification" content="fYkNAfPbgoASVXBWjWwMdOEwv1S762L0DgbAOoisnKk"/>
    <!--[if IE 8]>
    <script
            src="//cdnjs.cloudflare.com/ajax/libs/ie8/0.4.1/ie8.js"
    ></script><![endif]-->
    <?php $this->renderPartial('//layouts/_headScripts') ?>
</head>
<body class="<?= $bodyClassCss ?>" <?php if (property_exists($this, 'showPromoLogin') && $this->showPromoLogin) { ?> data-show-promo-login="true" <?php } ?>
      itemscope itemtype="http://schema.org/WebPage">
<?php if (!empty($facebookLink)): ?>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php endif; ?>
<?php if ($isShowInfoMailBlock) : ?>
    <aside class="prompt-top">
        <div class="container">
            <a class="prompt-link" style="font-size: 18px;" href="<?= $this->createUrl('account/about'); ?>"><span
                        style="color: red;"><?= $this->t('Important!') ?></span> <?= $this->t('You need to change the e-mail in connection with the decree about the blocking. Click here to change now') ?>
            </a>
            <div class="close" onclick="$('.prompt-top').remove();"><?= $this->t('Close') ?></div>
        </div>
    </aside>
<?php endif; ?>
<?= $app->user->getOfferSuccessCode() ?>

<?php $this->renderPartial('//layouts/_globalScripts') ?>
<header class="main-header" role="banner">
    <?php $this->renderPartial('//layouts/_mainHeader') ?>
    <nav class="navigation">
        <div class="container">
            <?php if ($this->beginCache('navigation-menu-icon', ['varyByRoute' => true, 'varyByLanguage' => true, 'duration' => 5])) { ?>
                <ul class="menu">
                    <li class="dropdown  <?= $this->getRoute() == 'catalog/index' ? 'accent' : ''; ?>">
                        <?= CHtml::link($this->t('Catalog of products'), ['catalog/index'], ['class' => 'menu-item icon-menu-catalog']) ?>
                    </li>
                    <li class="dropdown menu-category <?= $this->getRoute() == 'event/category' ? 'accent' : ''; ?>">
                        <span class="menu-item" role="button"
                              data-toggle="dropdown"><?= $this->t('By Category') ?> <b></b></span>
                        <div class="dropdown-menu menu-wrapper">
                            <div class="menu-inner">
                                <ul class="menu-list">
                                    <?php foreach (CategoryGroups::instance()->getLabels() as $name => $label): ?>
                                        <?php
                                        switch ($name) {
                                            case CategoryGroups::CATEGORY_BOYS:
                                                $categoryIconName = 'boys';
                                                break;
                                            case CategoryGroups::CATEGORY_GIRLS:
                                                $categoryIconName = 'girls';
                                                break;
                                            case CategoryGroups::CATEGORY_MOMS_AND_DADS:
                                                $categoryIconName = 'mom-and-dad';
                                                break;
                                            case CategoryGroups::CATEGORY_TOYS:
                                                $categoryIconName = 'toys';
                                                break;
                                            case CategoryGroups::CATEGORY_BEAUTY:
                                                $categoryIconName = 'health';
                                                break;
                                            default:
                                                $categoryIconName = '';
                                        }
                                        ?>
                                        <?php if ($categoryIconName) : ?>
                                            <li>
                                                <a class="item-link icon-menu-category-<?= $categoryIconName ?>"
                                                   href="<?= $app->createUrl('event/category', ['category' => $name]) ?>"><?= $label ?></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <div class="menu-inner">
                                <ul class="menu-list">
                                    <?php foreach (CategoryGroups::instance()->getLabels() as $name => $label): ?>
                                        <?php
                                        switch ($name) {
                                            case CategoryGroups::CATEGORY_ACCESSORIES:
                                                $categoryIconName = 'accessory';
                                                break;
                                            case CategoryGroups::CATEGORY_HOME:
                                                $categoryIconName = 'home';
                                                break;
                                            case CategoryGroups::CATEGORY_MOMS:
                                                $categoryIconName = 'mother';
                                                break;
                                            case CategoryGroups::CATEGORY_DADS:
                                                $categoryIconName = 'father';
                                                break;
                                            default:
                                                $categoryIconName = '';
                                        }
                                        ?>
                                        <?php if ($categoryIconName) : ?>
                                            <li>
                                                <a class="item-link icon-menu-category-<?= $categoryIconName ?>"
                                                   href="<?= $app->createUrl('event/category', ['category' => $name]) ?>"><?= $label ?></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown  <?= $this->getRoute() == 'event/age' ? 'accent' : ''; ?>">
                        <span class="menu-item" role="button" data-toggle="dropdown"><?= $this->t('By age') ?>
                            <b></b></span>
                        <div class="dropdown-menu menu-wrapper">
                            <?php foreach (ProductTargets::instance()->getSupportsAges() as $target): ?>
                                <div class="menu-inner">
                                    <span class="menu-head <?= ProductTargets::TARGET_BOY === $target ? 'icon-for-boys' : 'icon-for-girls' ?>"><?= ProductTargets::instance()->getLabel($target) ?></span>
                                    <ul class="menu-list">
                                        <?php foreach (AgeGroups::instance()->getLabels() as $name => $label): /* @var $item EventRecord */ ?>
                                            <li>
                                                <a class="item-link"
                                                   href="<?= $app->createUrl('event/age', ['age' => $name, 'target' => $target]) ?>"><?= $label ?></a>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </li>
                </ul>
                <?php $this->endCache();
            } ?>
            <?php if (EventRecord::model()->onlyTimeActive()->isVirtual(true)->exists()): ?>
                <a class="instant-sale" href="<?= $this->createUrl('event/fast') ?>">
                    <span class="instant-sale-one"><?= $this->t('instant sale') ?></span>
                    <span class="instant-sale-two"><?= $this->t('megascripts!') ?></span>
                </a>
            <?php endif ?>
        </div>
    </nav>
</header>
<main class="supreme-container">
    <a name="events"></a>

    <div class="supreme-inner <?= $this->id == 'catalog' ? 'wide' : '' ?>">
        <a name="content"></a>

        <?php
        $domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);
        $holidaysMessageCookie = sprintf('holidaysMessage=1;domain=%s;expires=Thu, 21 Jun 2018 00:00:00 GMT;path=/', $domain);

        /** @var Regions $regions */
        $regions = $this->container->get(Regions::class);
        $isHolidays = (Region::INDONESIA === $regions->getServerRegion()->getRegionName() && strtotime('2018-06-21') > time());
        ?>
        <?php if ($isHolidays): ?>
            <div class="main hide" id="holidaysMessage">
                <div class="container" style="border-bottom: 1px solid #ff7c91; position: relative">
                    <div style="font-size: 1.5em; padding: 20px 40px 20px 20px">
                        <p>Selamat Hari Raya Idul Fitri!</p>
                        <p>Sehubungan dengan perayaan Idul Fitri Mommy.com tidak melakukan pengiriman dari tgl&nbsp;13/06/18 -
                            20/06/18</p>
                        <p>Semua pesanan akan dikirim pada tgl&nbsp;21/06/18</p>
                        <p>
                            Layanan pelanggan kami aktif selama liburan, Anda bisa hubungi kami di
                            <a target="_blank" style="color: #ff7c91; text-decoration: underline"
                               href="https://api.whatsapp.com/send?phone=6282147486726">WhatsApp</a>, dan
                            <a target="_blank" style="color: #ff7c91; text-decoration: underline"
                               href="https://m.me/mommy.com.Indonesia">Facebook</a>
                        </p>
                    </div>
                    <div style="position: absolute; top:0;right: 0; width: 24px; height: 24px; padding: 8px; cursor: pointer"
                         id="holidaysMessageClose">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" width="24" height="24"
                             viewBox="0 0 24 24"
                             enable-background="new 0 0 24 24" xml:space="preserve">
<path fill="#ff7c91"
      d="M12,24C5.4,24,0,18.6,0,12S5.4,0,12,0s12,5.4,12,12S18.6,24,12,24z M17.4,8.5c0.2-0.2,0.2-0.6,0-0.8 l-1.1-1.3c-0.3-0.2-0.6-0.2-0.8,0L12,9.9L8.5,6.4C8.3,6.2,8,6.2,7.7,6.4L6.4,7.7c-0.2,0.2-0.2,0.6,0,0.8L9.9,12l-3.5,3.5 c-0.2,0.2-0.2,0.5,0,0.8l1.3,1.3c0.2,0.2,0.6,0.2,0.8,0l3.5-3.5l3.5,3.5c0.2,0.2,0.6,0.2,0.8,0l1.3-1.3c0.2-0.2,0.2-0.6,0-0.8 L14.1,12L17.4,8.5z"/>
</svg>
                    </div>
                </div>
            </div>
            <script>
                $(function () {
                    if ($.cookie('holidaysMessage') !== '1') {
                        $('#holidaysMessage').removeClass('hide');
                    }

                    $('#holidaysMessageClose').click(function () {
                        $('#holidaysMessage').addClass('hide');
                        document.cookie = '<?= $holidaysMessageCookie ?>';
                    });
                });
            </script>
        <?php endif; ?>


        <?= $content; ?>
    </div>
    <div class="scroll-to-top-wrapper">
        <div class="scroll-to-top hide">
            <span class="scroll-text"><?= $this->t('Top') ?></span>
        </div>
    </div>
    <?php $isShowGift = false; ?>
    <?php if ($user->isGuest && $user->isEnableOfferBenefice()) : ?>
        <?php $isShowGift = true; ?>
        <!--<div class="get-gift-wrapper" data-toggle="modal" data-target="#present">
            <div class="get-gift-label"></div>
            <div class="get-gift-text">
                <? //= $this->t('Receive gifts') ?>
            </div>-->
        </div>
    <?php endif ?>
    <?php if ($user->isGuest && !$isShowGift) : ?>
        <?php if (!$user->isExistShowComebacker()) : ?>
            <?php
            $cookies = $this->app()->request->cookies;
            $modalPresentStartUp = $cookies->contains('isOfferPresentComebacker') === false;
            ?>
            <!--<div class="get-gift-wrapper" data-toggle="modal"
                 data-target="#comebacker" <? //= $modalPresentStartUp ? 'style="display: none;"' : '' ?>>
                <div class="get-gift-label"></div>
                <div class="get-gift-text">
                    <? //= $this->t('Receive gifts') ?>
                </div>
            </div>-->
        <?php endif ?>
    <?php endif ?>
</main>
<footer class="main-footer">
    <div class="container">
        <?php if ($this->beginCache('footer-menu', ['varyByLanguage' => true, 'duration' => 0])) { ?>
        <nav class="footer-nav">
            <div>
                <div class="logo">
                    <a href="<?= $app->createUrl('index/index') ?>">mommy</a>
                </div>
                <div class="footer-group contacts">
                    <h3><?= $this->t('We are in social networks') ?>: </h3>
                    <?php if (!empty($facebookLink)): ?>
                        <div class="fb-page"
                             data-href="<?= $this->app()->params['facebookLinks'][$regionName] ?>" data-tabs="timeline"
                             data-width="240" data-height="70" data-small-header="false"
                             data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
                            <blockquote cite="<?= $this->app()->params['facebookLinks'][$regionName] ?>"
                                        class="fb-xfbml-parse-ignore">
                                <a href="<?= $this->app()->params['facebookLinks'][$regionName] ?>">
                                    Mommy.com - <?= $regionName ?>
                                </a>
                            </blockquote>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <ul>
                <li>
                    <a href="<?= $sp->getFullPageLink('about') ?>">
                        <?= $this->t('About us') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('contacts') ?>">
                        <?= $this->t('Contacts') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('work-in-company') ?>">
                        <?= $this->t('Work in company') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('for-partners') ?>">
                        <?= $this->t('For partners') ?>
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="<?= $sp->getFullPageLink('confidentiality') ?>">
                        <?= $this->t('Confidentiality') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('offer') ?>">
                        <?= $this->t('Offer') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('ordering') ?>">
                        <?= $this->t('Ordering') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('faq') ?>">
                        <?= $this->t('FAQ') ?>
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="<?= $sp->getFullPageLink('delivery') ?>">
                        <?= $this->t('Delivery') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('payment') ?>">
                        <?= $this->t('Payment') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('return') ?>">
                        <?= $this->t('Return') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('coupons-and-shares') ?>">
                        <?= $this->t('Coupons and shares action') ?>
                    </a>
                </li>
            </ul>
            <ul style="text-align: right; width: 200px">
                <li>
                    <a class="detached-pink-link" href="<?= $urlManager->createUrl('instruction') ?>">
                        <svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"
                             data-id="8c248f5a3e3ff78a6f53693b32d7d296"
                             style="opacity: 1; fill: rgb(223, 90, 109); margin-top: 6px; width: 20px; height: 20px;">
                            <g>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"
                                        style="fill: rgb(223, 90, 109);"></path>
                            </g>
                        </svg>
                        <?= $this->t('How to determine a size') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $sp->getFullPageLink('fabrics-and-care') ?>">
                        <?= $this->t('Fabrics and care') ?>
                    </a>
                </li>
                <li>
                    <a class="mob detached-pink-link" href="<?= $mobileUrlManager->createAbsoluteUrl('index/index') ?>">
                        <svg version="1.2" preserveAspectRatio="none" viewBox="0 0 24 24"
                             data-id="708c806850ace596069c5a5f8d9522f1"
                             style="opacity: 1; fill: rgb(223, 90, 109); margin-top: 6px; width: 20px; height: 20px;">
                            <g>
                                <path
                                        d="M16 1H8C6.34 1 5 2.34 5 4v16c0 1.66 1.34 3 3 3h8c1.66 0 3-1.34 3-3V4c0-1.66-1.34-3-3-3zm-2 20h-4v-1h4v1zm3.25-3H6.75V4h10.5v14z"
                                        style="fill: rgb(223, 90, 109);"></path>
                            </g>
                        </svg>
                        <?= $this->t('Mobile version') ?>
                    </a>
                </li>
                <li>
                    <?php if ($phone): ?>
                        <i class="fas fa-phone-square phone-icon"></i>
                        <?= $phone ?>
                    <?php endif; ?>
                </li>
            </ul>
            <?php $this->endCache();
            } ?>
        </nav>
    </div>
</footer>
<aside class="modals">
    <?php if ($user->isGuest): ?>
        <?php $this->renderPartial('//layouts/_modalsAuth') ?>
    <?php endif; ?>
    <!--    not support    -->
    <?php //$this->renderPartial('//layouts/_modalRecall') ?>
    <!--    not support    -->
    <?php $this->renderPartial('//layouts/_modalOffers') ?>
    <?php //$this->renderPartial('//layouts/_modalUnformedCart') ?>
    <?php $this->renderPartial('//layouts/_modalRegionSwitcher') ?>
</aside>
<?php $session = new \Symfony\Component\HttpFoundation\Session\Session(); ?>
<?php if ($session->get('justRegistered')): ?>
    <script>dataLayer.push({'event': 'mamam.registration.success'})</script>
    <?php $session->set('justRegistered', null) ?>
<?php endif; ?>
</body>
</html>
