<?php

use MommyCom\YiiComponent\BuildUrlManager;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Utf8;

/* @var $this FrontController */

$app = $this->app();
/* @var $mobileUrlManager BuildUrlManager */
?>
<!doctype html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <link rel="alternate"
          href="android-app://<?= $app->params['androidAppId'] ?>/<?= Utf8::replace('://', '/', $app->createAbsoluteUrl($app->request->url)) ?>">
    <meta name="description"
          content="<?= $this->t('The first shopping-club for children and moms in the USA mommy.com. Discounts of up to 70%.') ?>">
</head>
<body>
<div class="header">
    <header class="container" role="banner">
        <?= CHtml::link($app->name, ['index/index'], ['class' => 'logo flag', 'itemprop' => 'breadcrumb']) ?>
        <div class="slogan">
            <span class="slogan-style-one"><?= $this->t('AVAILABLE CARE') ?> </span>
            <span class="slogan-style-two"><?= $this->t('FOR YOUR FAMILY') ?></span>
        </div>
    </header>
</div>

<div class="supreme-container maintenance-inner">
    <div class="supreme-inner">
        <div class="container">
            <div class="maintenance page">
                <div class="maintenance-page">
                    <div class="maintenance-title"><?= $this->t('Technical work is carried out') ?></div>
                    <div class="maintenance-subtitle"><?= $this->t('We apologize for the inconvenience') ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <footer class="container">
        <div class="logo">
            <a href="<?= $app->createUrl('index/index') ?>">mommy</a>
        </div>
        <div class="contacts">
            <dl class="social">
                <dt><?= $this->t('We are in social networks') ?></dt>
                <!--                    <dd><a class="vk" href="-->
                <? //= $this->app()->params['groupVk'] ?><!--" target="_blank">Vkontakte</a></dd>-->
                <!--                    <dd><a class="od" href="-->
                <? //= $this->app()->params['groupOd'] ?><!--" target="_blank">Odnoklassniki</a></dd>-->
                <dd><a class="fb" href="<?= $this->app()->params['groupFb'] ?>" target="_blank">Facebook</a></dd>
            </dl>
        </div>
    </footer>
</div>
</body>
</html>
