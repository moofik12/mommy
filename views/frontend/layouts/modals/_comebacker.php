<?php

use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

$cookies = $this->app()->request->cookies;
$formOfferRegistration = new RegistrationOfferForm();
$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
$user = $this->app()->user;
/** @var  $user ShopWebUser */
$modalPresentStartUp = $cookies->contains('isOfferPresentComebacker') === false;
$specialPsychotypeBanner = $user->getOfferBannerNumber(false);
$bannerName = "PS:comebacker";
$isFirst = false;
if ($modalPresentStartUp) {
    $isFirst = true;
    $cookies->add('isOfferPresentComebacker', new CHttpCookie('isOfferPresentComebacker', 1, ['expire' => strtotime('tomorrow')]));
}
$formOfferRegistration->benefice = RegistrationOfferForm::BENEFICE_100;
$formOfferRegistration->strategy = RegistrationOfferForm::STRATEGY_PROMOCODE;
$formOfferRegistration->strategyPayments = RegistrationOfferForm::STRATEGY_PAYMENTS_MONEY;
$formOfferRegistration->bannerName = $bannerName;
$formOfferRegistration->isConfirmPrivacy = 1;

$textReg = $this->t('Registration is completed via modal window №');
$textErrorBonus = $this->t('An error occurred while calculating bonuses. Contact Support');
$textError = $this->t('Error. Check your internet connection');
$textOpen = $this->t('Opening the registration window №');
$textClose = $this->t('Close the modal registration № window');

?>
<?php if (!$user->isExistShowComebacker()): ?>
    <div class="modal hide present-modal17" id="comebacker">
        <div class="modal-wrapper">
            <div class="modal-inner">
                <div class="modal-body">
                    <strong class="present-modal17-top-title">
                        <img src="<?= $baseImgUrl . 'gift.png' ?>" />
                    </strong>
                    <span class="close-modal17" data-dismiss="modal"></span>
                    <?php
                    /** @var CActiveForm $formOffer */
                    $formOffer = $this->beginWidget('CActiveForm', [
                        'action' => ['auth/offerRegistration'],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $.ajax({
                                        url: form.attr('action'),
                                        type: form.attr('method'),
                                        data: form.serialize(),
                                        dataType: 'json',
                                        success: function (data) {
                                            if (data !== null && typeof data === 'object') {
                                                if (data.success) {
                                                    form.closest('.modal').modal('hide');                                                        
                                                    form[0].reset();
                                                    form.closest('.modal').remove();    
                                                    $(document).trigger('mamam.registration.success');                                                        
                                                    $(document).trigger('mamam.auth.success');                                                        
                                                    Mamam.trackEvent('modals', 'present-registration-finish', '$textReg.$bannerName');
                                                    isShowInfoAfterMailingType = true;
                                                    $('[data-target=\"#comebacker\"]').hide();
                                                    $('#mailing-type').modal('show');
                                                } else  {
                                                    alert('$textErrorBonus');
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert('$textError');
                                        }
                                    });
                                    return false;
                                }",
                        ],
                    ]) ?>
                    <div class="standard-block">
                        <div class="text-modal17">
                            <?= $this->t('We gift you $ 100 to buy goods') ?>.
                        </div>
                        <div class="field bottom <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                            <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => $this->t('E-mail address'), 'autocomplete' => 'off']) ?>
                            <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                            <button class="btn" type="submit"><?= $this->t('Take money') ?></button>
                        </div>
                    </div>
                    <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                    <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                    <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                    <?= $formOffer->hiddenField($formOfferRegistration, 'bannerName') ?>
                    <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                    <?php $this->endWidget() ?>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                comebackerModalIsOpen = parseInt(<?=$isFirst ? 0 : 1?>);
                $(document).mouseleave(function (e) {
                    if (Mamam.isGuest) {
                        if (e.pageY <= 2 && comebackerModalIsOpen == false) {
                            $('#comebacker').modal("show");
                            comebackerModalIsOpen = true;
                        }
                    }
                });
            });
        </script>
    </div>
    <div class="modal hide present-modal15" id="success-info-landing">
        <div class="modal-wrapper">
            <div class="modal-inner">
                <div class="close" data-dismiss="modal"></div>
                <div class="modal-body">
                    <div class="modal15-logo">
                        <img src="<?= $baseImgUrl ?>logo-mommy.svg" alt="" width="192" height="34"/>
                    </div>
                    <div class="modal15-text">
                        <div>
                            <p><?= $this->t('Thank you for registering! <br/> You get') ?></p>
                            <strong><?= $this->t('100 IDR') ?></strong>
                            <div class="present-info"><?= $this->t('You have the promotional code in your mail') ?></div>
                        </div>
                    </div>
                    <a href="#" class="modal15-btn" data-dismiss="modal"><?= $this->t('Start shopping') ?></a>
                </div>
            </div>
        </div>
    </div>
    <?php
    /** @var $cs CClientScript */
    $cs = $this->app()->clientScript;
    $cs->registerScript('modalOffersComebacker', "
    var modalComebacker = $('#comebacker');

    modalComebacker
    .on('show', function() {
        $('[data-target=\"#comebacker\"]').show();
        Mamam.trackEvent('modals', 'present-registration', '$textOpen.$bannerName');
    })
    .on('hide', function() {
        Mamam.trackEvent('modals', 'present-registration', '$textClose.$bannerName');
    });
");
    ?>
<?php endif; ?>
