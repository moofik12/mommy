<?php

use MommyCom\Model\Form\CallbackForm;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */

$app = $this->app();
$formCallback = new CallbackForm();
$user = $this->app()->user;
?>

<!-- callback -->
<div class="modal hide login" id="recall">
    <div class="modal-wrapper">
        <div class="login-conteiner">
            <div class="login-wrapper">
                <div class="forgot">
                    <?php
                    /** @var CActiveForm $form */
                    $form = $this->beginWidget('CActiveForm', [
                        'action' => ['index/callback'],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function(form, data, hasError){
                                        if (!hasError) {
                                            $.ajax({
                                                type: "POST",
                                                data: form.serialize(),
                                                url: form.prop("action")
                                            }).success(function(data) {
                                                if (data["success"]) {
                                                    form.closest(".login-wrapper").addClass("hide");
                                                    $("#recall").find(".recall-success").removeClass("hide");
                                                }
                                            });
                                        }

                                        return false;
                                    }',
                        ],
                    ]) ?>
                    <div class="form-title"><?= $this->t('Back call') ?></div>
                    <?= $form->labelEx($formCallback, 'telephone') ?>
                    <div class="field top <?= $formCallback->hasErrors('telephone') ? 'error' : '' ?>">
                        <?= $form->textField($formCallback, 'telephone', ['placeholder' => $this->t('Your phone number')]) ?>
                        <?= $form->error($formCallback, 'telephone', ['class' => 'message']) ?>
                    </div>
                    <div class="login-recall">
                        <input class="recall-enter left" type="submit" value="<?= $this->t('call me back') ?>">
                        <a class="submit recall-enter right" href="#"
                           data-dismiss="modal"><?= $this->t('Cancel') ?></a>
                    </div>
                    <?php $this->endWidget() ?>
                    <div class="help"><?= $this->t('Our manager will contact you soon') ?></div>
                </div>
            </div>
            <div class="login-wrapper hide recall-success">
                <div class="forgot">
                    <form>
                        <div class="form-title recall-alert"><?= $this->t('You will be called soon') ?></div>
                        <a class="submit" href="#" data-dismiss="modal"><?= $this->t('Continue shopping') ?></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- callback -->
