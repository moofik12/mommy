<?php

use MommyCom\YiiComponent\Type\Cast;

$baseImgUrl = $this->app()->getBaseUrl(true) . '/';
$basePromoPartnerImgUrl = $baseImgUrl . 'static/partner/promo/';
$promoPartner = Cast::toInt($_GET['promo-partner'] ?? 0);
?>
<?php if ($promoPartner > 0): ?>
    <link rel="image_src" href="<?= $basePromoPartnerImgUrl . 'promo' . $promoPartner . '.jpg' ?>">
    <meta property="og:image" content="<?= $basePromoPartnerImgUrl . 'promo' . $promoPartner . '.jpg' ?>">
    <meta property="og:title" content="<?= CHtml::encode($this->pageTitle); ?>">
<?php endif; ?>
