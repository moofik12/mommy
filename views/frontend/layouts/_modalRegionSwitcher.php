<?php

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionDetector;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\ClientScript;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var FrontController $this
 */

if (!$this->showRegionConfirmPopup && !$this->showRegionChooseModal) {
    return;
}

$app = $this->app();
$user = $app->user;
/** @var ClientScript $cs */
$cs = $app->clientScript;

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
/** @var $cs CClientScript */
$cs = $app->clientScript;

$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/region.css'));

$baseImgUrl = $app->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$createRegionCookie = function ($value) use ($regions, $user) {
    $value = $this->app()->getSecurityManager()->hashData(serialize($value));
    $value = rawurlencode($value);

    $name = RegionDetector::getCookieName();
    $domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);

    return sprintf('%s=%s;domain=%s;expires=Tue, 19 Jan 2038 03:14:07 GMT;path=/', $name, $value, $domain);
};

$popupHide = $this->showRegionConfirmPopup ? '' : 'hide';
$modalHide = $this->showRegionChooseModal ? '' : 'hide';

?>
<div class="regionSwitcher">
    <div class="container">
        <div class="regionPopup <?= $popupHide ?>">
            <p class="regionPopup__question">Your country?</p>
            <p class="regionPopup__city"><?= $regions->getServerRegion()->getRegionName() ?></p>
            <div class="regionPopup__ctrl">
                <button class="regionPopup__ctrlBtn _confirm">Yes, thank you</button>
                <button class="regionPopup__ctrlBtn _decline">No, choose</button>
            </div>
        </div>
    </div>
    <div class="regionModal <?= $modalHide ?>">
        <?= CHtml::image($baseImgUrl . 'geo.svg', '', ['class' => 'regionModal__geo']) ?>
        <p class="regionModal__title">Choose your country</p>
        <form action="" class="regionModal__choice">
            <div class="regionModal__choiceItem">
                <input type="radio" name="country" id="regionSwitcherRomania" class="regionModal__choiceItemInp"
                       data-href="//<?= $regions->getHostname(Region::ROMANIA) ?>"
                       data-cookie="<?= $createRegionCookie(Region::ROMANIA) ?>">
                <label for="regionSwitcherRomania" class="regionModal__choiceItemLabel">Romania</label>
            </div>
            <div class="regionModal__choiceItem">
                <input type="radio" name="country" id="regionSwitcherIndonesia" class="regionModal__choiceItemInp"
                       data-href="//<?= $regions->getHostname(Region::INDONESIA) ?>"
                       data-cookie="<?= $createRegionCookie(Region::INDONESIA) ?>">
                <label for="regionSwitcherIndonesia" class="regionModal__choiceItemLabel">Indonesia</label>
            </div>
            <div class="regionModal__choiceItem">
                <input type="radio" name="country" id="regionSwitcherColombia" class="regionModal__choiceItemInp"
                       data-href="//<?= $regions->getHostname(Region::COLOMBIA) ?>"
                       data-cookie="<?= $createRegionCookie(Region::COLOMBIA) ?>">
                <label for="regionSwitcherColombia" class="regionModal__choiceItemLabel">Colombia</label>
            </div>
            <div class="regionModal__choiceItem">
                <input type="radio" name="country" id="regionSwitcherVietnam" class="regionModal__choiceItemInp"
                       data-href="//<?= $regions->getHostname(Region::VIETNAM) ?>"
                       data-cookie="<?= $createRegionCookie(Region::VIETNAM) ?>">
                <label for="regionSwitcherVietnam" class="regionModal__choiceItemLabel">Vietnam</label>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('.regionPopup__ctrlBtn._confirm').click(function (e) {
        e.preventDefault();
        $(this).closest('.regionPopup').hide();

        document.cookie = '<?= $createRegionCookie($regions->getServerRegion()->getRegionName()) ?>';
    });

    $('.regionPopup__ctrlBtn._decline').click(function (e) {
        e.preventDefault();
        $(this).closest('.regionPopup').hide();
        $('.regionModal').show();
        $('body').append('<div class="modal-backdrop in"></div>');
    });

    $('.regionModal__choiceItem input').click(function () {
        var $checked = $(this);

        document.cookie = $checked.data('cookie');
        window.location.replace($checked.data('href'));
    });

    <?php if ($this->showRegionChooseModal): ?>
    $('body').append('<div class="modal-backdrop in"></div>');
    <?php endif; ?>
</script>
