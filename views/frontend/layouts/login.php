<?php

use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();
$user = $this->app()->user;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;

$bodyCssClass = $this->id === 'authLanding' ? 'landing' : 'login';
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta name="theme-color" content="#ff7c91">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
</head>
<body class="<?= $bodyCssClass ?>">
<?php $this->renderPartial('//layouts/_globalScripts') ?>
<?= $content; ?>
</body>
</html>
