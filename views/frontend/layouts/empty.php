<?php
/* @var $content string */
?>
<!doctype html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<?php $this->renderPartial('//layouts/_globalScripts') ?>
<?= $content; ?>
</body>
</html>
