<?php

use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\Frontend\ClickIdConstantsInterface;
use MommyCom\YiiComponent\Type\Cast;

/**
 * @var $this Controller
 */

/** @var MommyWebApplication $app */
$app = $this->app();
$user = $app->user;
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $currencyFormatter = $app->currencyFormatter;
/** @var Regions $regions */
$regions = $this->container->get(Regions::class);

$clickId = (string)$app->request->cookies[ClickIdConstantsInterface::CLICK_ID_NAME];
$googleTagManagerId = (string)$app->params['googleTagManagerId'];
?>
<script>
    window.Mamam = window.Mamam || {};
    window.dataLayer = window.dataLayer || [];

    Mamam.baseUrl = '<?= $app->baseUrl . $_SERVER['SCRIPT_NAME'] ?>';
    Mamam.urlFormat = '<?= $app->urlManager->urlFormat ?>';
    Mamam.isGuest = <?= $app->user->isGuest ? 'true' : 'false' ?>;
    Mamam.userId = <?= Cast::toStr(intval($app->user->id)) ?>;
    Mamam.timeServer = new Date(<?= time() * 1000 ?>);
    Mamam.timeClient = new Date();
    Mamam.currency = '<?= $cf->getCurrency() ?>';
    Mamam.currencyCode = '<?= $regions->getServerRegion()->getCurrencyCode() ?>';
    Mamam.clickId = '<?= $clickId ?>';
</script>

<?php if ($googleTagManagerId): ?>
    <!-- Google Tag Manager -->
    <script>
        dataLayer.push({
            'event': 'gtm.js',
            'gtm.start': new Date().getTime()
        });
    </script>
    <script async src="https://www.googletagmanager.com/gtm.js?id=<?= $googleTagManagerId ?>"></script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=<?= $googleTagManagerId ?>" height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->    <!-- End Google Tag Manager (noscript) -->
<?php endif; ?>
