<?php

use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\RecoveryPasswordForm;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */

$app = $this->app();

$authForm = new AuthForm();
$authForm->remember = true;

$recoveryForm = new RecoveryPasswordForm();
$registrationForm = new RegistrationForm('simple');

$textReg = $this->t('Registration is completed via modal window №');
$textError = $this->t('Error. Check your internet connection');
?>
<script>
    var isRedirectToDelivery = false;
</script>
<div class="modal hide auth" id="enter">
    <div class="modal-wrapper">
        <div class="auth-conteiner">
            <div class="tabs">
                <div class="tab active" data-tab="signup"><?= $this->t('Sign Up') ?></div>
                <div class="tab" data-tab="signin"><?= $this->t('Log in') ?></div>
            </div>
            <div id="signin" data-tab-view class="auth-wrapper hide">
                <div class="enter">
                    <?php
                    /* @var CActiveForm $form */
                    $form = $this->beginWidget('CActiveForm', [
                        'action' => $this->app()->createUrl('/auth/authenticate'),
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'beforeValidate' => 'js:function($form) {
                                var $field = $("#AuthForm_email");
                                if ($field && $field.val()) {
                                    $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                                }
                                
                                var $field = $("#RegistrationForm_email");
                                if ($field && $field.val()) {
                                    $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                                }
                                
                                return true;
                            }',
                            'afterValidate' => 'js:function($form, data, hasError) {
                                if (hasError) {
                                    return;
                                }
                                
                                $.ajax({
                                    url: $form.attr("action"),
                                    type: $form.attr("method"),
                                    data: $form.serialize(),
                                    dataType: "json",
                                    success: function (data) {
                                        if (data !== null && typeof data === "object") {
                                            if (data.success) {
                                                $form.closest(".modal").modal("hide");
                                                $(document).trigger("mamam.auth.success");
                                                $form[0].reset();
                                                if (isRedirectToDelivery) {
                                                    window.location.replace(Mamam.createUrl("order/delivery"));
                                                }
                                                comebackerModalIsOpen = true;
                                                if ($(\'[data-target="#comebacker"]\').length > 0) {
                                                    $(\'[data-target="#comebacker"]\').hide();
                                                }
                                                
                                                window.location.reload();
                                            } else if (data.errors && data.errors.length > 0) {
                                                
                                            }
                                        }
                                    },
                                    error: function () {
                                        alert("' . $textError . '");
                                    }
                                });

                                return false;
                            }',
                        ],
                        'htmlOptions' => [
                            'id' => 'signin-form',
                        ],
                        'focus' => [$authForm, 'email'],
                    ]) ?>
                    <label class="visible-title"><?= $this->t('Your E-mail') ?>:</label>
                    <div class="field <?= $authForm->hasErrors('email') ? 'error' : '' ?>">
                        <?= $form->textField($authForm, 'email', [
                            'placeholder' => $this->app()->params['validatorRegex']['email']['default']['placeholder'],
                            'autocomplete' => 'off'])
                        ?>
                        <?= $form->error($authForm, 'email', ['class' => 'message']) ?>
                    </div>
                    <label class="visible-title"><?= $this->t('Your password') ?>:</label>
                    <div class="field <?= $authForm->hasErrors('password') ? 'error' : '' ?>">
                        <?= $form->passwordField($authForm, 'password', ['placeholder' => $this->t('your password')]) ?>
                        <?= $form->error($authForm, 'password', ['class' => 'message']) ?>
                    </div>
                    <?= $form->hiddenField($authForm, 'remember') ?>
                    <?= CHtml::submitButton($this->t('Start shopping')) ?>
                    <span class="reminder" data-tab="forgot"><?= $this->t('Password reminder') ?></span>
                    <?php $this->endWidget() ?>
                </div>
            </div>
            <script type="application/javascript">
                registrationTracker.validatorRegex = <?= json_encode($this->app()
                    ->params['validatorRegex']) ?>;
            </script>
            <div id="signup" data-tab-view class="auth-wrapper">
                <div class="registration">
                    <?php $form = $this->beginWidget('CActiveForm', [
                        'action' => $this->app()->createUrl('auth/registration'),
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'beforeValidate' => 'js:function($form) {
                                var $field = $("#RegistrationForm_email");
                                if ($field && $field.val()) {
                                    $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                                }
                                
                                return true;
                            }',
                            'afterValidate' => 'js:function($form, data, hasError) {
                                if (
                                    !registrationTracker.validateEmail("#RegistrationForm_email")
                                ) { 
                                    return false;
                                }
                                
                                $.ajax({
                                    url: $form.attr("action"),
                                    type: $form.attr("method"),
                                    data: $form.serialize(),
                                    dataType: "json",
                                    success: function (data) {
                                        if (data !== null && typeof data === "object") {
                                            if (data.success) {
                                                $form.closest(".modal").modal("hide");
                                                $(document).trigger("mamam.registration.success");
                                                $(document).trigger("mamam.auth.success");
                                                $form[0].reset();
                                                $("#mailing-type").modal("show");
                                                comebackerModalIsOpen = true;
                                                if ($(\'[data-target="#comebacker"]\').length > 0) {
                                                    $(\'[data-target="#comebacker"]\').hide();
                                                }
                                                if (Mamam.push) {
                                                    Mamam.push.init();
                                                }
                                            }
                                        }
                                    },
                                    error: function () {
                                        alert("' . $textError . '");
                                    }
                                });

                                return false;
                            }',
                        ],
                    ]) ?>
                    <?= $form->labelEx($registrationForm, 'email') ?>
                    <div class="field">
                        <label class="visible-title"><?= $this->t('Your E-mail') ?>:</label>
                        <?= $form->textField($registrationForm, 'email', ['placeholder' => $this->app()->params['validatorRegex']['email']['default']['placeholder']]) ?>
                        <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
                    </div>

                    <div class="policy" data-toggle="modal"
                         data-target="#policy"><?= $this->t('privacy policy') ?></div>
                    <?= CHtml::submitButton($this->t('Start shopping')) ?>
                    <?php $this->endWidget() ?>
                </div>


            </div>
            <div id="forgot" data-tab-view class="auth-wrapper hide">
                <div class="forgot">
                    <?php $form = $this->beginWidget('CActiveForm', [
                        'action' => $this->app()->createUrl('auth/recovery'),
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'beforeValidate' => 'js:function($form) {
                                var $field = $("#RecoveryPasswordForm_email");
                                if ($field && $field.val()) {
                                    $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                                }
                                
                                return true;
                            }',
                            'afterValidate' => 'js:function($form, data, hasError) {
                                if (hasError) {
                                    return;
                                }
                                
                                $.ajax({
                                    url: $form.attr("action"),
                                    type: $form.attr("method"),
                                    data: $form.serialize(),
                                    dataType: "json",
                                    success: function (data) {
                                        if (data !== null && typeof data === "object") {
                                            if (data.success) {
                                                $form.closest(".modal").modal("hide");
                                                $form[0].reset();

                                            

                                            } else if (data.errors && data.errors.length > 0) {
                                                
                                            }
                                        }
                                    },
                                    error: function () {
                                        alert("' . $textError . '");
                                    }
                                });

                                return false;
                            }',
                        ],
                    ]) ?>
                    <div class="form-title"><?= $this->t('Password recovery') ?></div>
                    <?= $form->labelEx($recoveryForm, 'email') ?>
                    <div class="field">
                        <?= $form->textField($recoveryForm, 'email', ['placeholder' => $this->t('Your E-mail')]) ?>
                        <?= $form->error($recoveryForm, 'email', ['class' => 'message']) ?>
                    </div>
                    <?= CHtml::submitButton($this->t('Send the letter')) ?>
                    <?php $this->endWidget() ?>
                    <div class="help"><?= $this->t('In the letter there will be a link to reset the password') ?></div>
                </div>
            </div>
            <div class="auth-social">
                <div class="social-links">
                    <p><?= $this->t('or through') ?></p>
                    <ul>
                        <li><a class="fb" href="/connect/facebook">Facebook</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
            <div class="auth-modal-bottom-wrapp">
                <!--<div class="auth-modal-more-discounts-img"></div>-->
                <div class="auth-modal-more-discounts-text">
                    <p class="str1"><?= $this->t('We open the opportunity to register<br />at the MOMMY Shopping Club again!') ?></p>
                    <p class="str2"><?= $this->t('Hurry up to get access to the lowest prices for <span>{count} popular products</span>',
                            ['{count}' => 2729]) ?></p>
                    <p class="str3"><?= $this->t('The number of places in our club is limited!') ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal hide policy-box" id="policy">
    <div class="modal-wrapper">
        <span class="close" data-dismiss="modal">x</span>
        <div class="modal-body">
            <p class="pc-title"><?= $this->t('privacy policy') ?></p>
            <p><?= $this->t('The Company operates on the principle of recognition of the information transmitted by the Customer, including personal data, confidential, and therefore does not allow the use of such information for purposes other than fulfillment of the contract of retail sale or other purposes determined by this privacy policy.') ?></p>
            <p><?= $this->t('When completing the Application on the website, the Client provides the following personal data:') ?></p>
            <ul>
                <li><?= $this->t('Surname, First name;') ?></li>
                <li><?= $this->t('E-mail address;') ?></li>
                <li><?= $this->t('Contact phone number;') ?></li>
                <li><?= $this->t('Delivery address of the products,') ?></li>
            </ul>
            <p><?= $this->t('expressly, explicitly and unconditionally give their consent to the processing of such data by the Company without a time limit, with a view to executing a contract of retail sale, informing the Client about the conditions of cooperation and promotion of goods on the market.') ?></p>
            <p><?= $this->t('The Company ensures the confidentiality of the client\'s personal data in accordance with the USA law regarding personal data, takes the necessary legal, organizational and technical measures to protect personal data from unauthorized or accidental access, destruction, modification, blocking, copying, distribution, as well as other illegal actions.') ?></p>
            <p><?= $this->t('The Customer agrees to provide the Company with their data for the purposes of executing the concluded contracts and the Privacy Policy and the Company\'s obligations under them to sell and deliver the Goods to the Customer. The Customer\'s consent is expressed in the indication of the information in the appropriate boxes when completing the Application. Consent to the processing of personal data may be withdrawn by sending a written notice to the Company specified in the Contacts.') ?></p>
            <p><?= $this->t('Within 7 days from the date of receipt of the notification, the available personal data will be destroyed and the processing terminated. The Customer agrees to the transfer by the Company of the information provided to the agents and third parties acting on the basis of the contract with the Company, in order to fulfill their obligations to the Customer.') ?></p>
            <p><?= $this->t('The Company is not responsible for the information provided by the Customer on the Site being used publicly.') ?></p>
            <p><?= $this->t('Being a user of the shopping club Mommy.COM, the user can receive periodic mailings (information, holidays and others) to the registered email address, as well as automatic confirmation of orders. Each user voluntarily chooses whether to receive it or not. We guarantee that your email address will not be used for spamming.') ?></p>
        </div>
    </div>
</div>
