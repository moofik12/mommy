<?php

use MommyCom\Model\Db\OrderDiscountCampaignRecord;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/* @var $this FrontController */

$app = $this->app();

$phone = $app->params['phone'];

/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $app->countries->getCurrency();
/* @var ShopShoppingCart $cart */
$cart = $app->user->cart;

$cartCost = $cart->getCost(false);
$orderDiscountCampaign = $cart->getOrderDiscountCampaign();
$discountCampaignText = '';

if ($orderDiscountCampaign
    && $cart->isEnableOrderDiscountCampaign(true, true)
    && $orderDiscountCampaign->getAmountNotEnoughUseCampaign($cartCost) > 0) {
    $type = $orderDiscountCampaign->getMayBeUsedType($cartCost);
    $amountNotEnoughUseCampaign = $orderDiscountCampaign->getAmountNotEnoughUseCampaign($cartCost);

    if ($type == OrderDiscountCampaignRecord::USED_PERCENT) {
        $discountCampaignText = $this->t(
            '<span class="quick-cart-discount">for a discount <span class="green">{percent}%</span><br><span class="red">{amount} {currency}</span>осталось</span>',
            ['{percent}' => $orderDiscountCampaign->percent, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
    } elseif ($type == OrderDiscountCampaignRecord::USED_CASH) {
        $discountCampaignText = $this->t(
            '<span class="quick-cart-discount">for a discount <span class="green">{cash} {currency}</span><br><span class="red">{amount} {currency}</span>left</span>',
            ['{cash}' => $orderDiscountCampaign->cash, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
    }
}
?>
<script>
    (function () {
        <?php
        $orderPositions = [];
        foreach ($cart->getPositions() as $position) {
            $orderPositions[$position->event_id . '/' . $position->product->product_id] = [
                'id' => $orderPositions[] = $position->event_id . ' / ' . $position->product->product_id,
                'name' => $position->product->product->name,
                'brand' => $position->product->product->brand->name,
                'category' => $position->product->product->category,
                'variant' => $position->product->size,
                'price' => $position->price,
                'quantity' => $position->number,
            ];
        }
        ?>
        Mamam.ecommerce.products.setCache(<?= json_encode($orderPositions, JSON_UNESCAPED_UNICODE) ?>);
    })();
</script>
<div class="header-container">
    <div class="container">
        <?= CHtml::link($app->name, ['index/index', '#' => 'events'], ['class' => 'logo flag', 'itemprop' => 'breadcrumb']) ?>
        <div class="slogan">
            <span class="slogan-style-one"><?= $this->t('AVAILABLE CARE') ?> </span>
            <span class="slogan-style-two"><?= $this->t('FOR YOUR FAMILY') ?></span>
        </div>
        <ul class="user-bar">
            <?php if ($cart->getReservedCount() > 0): ?>
                <li class="timer-place" data-quickcart-timer="true" style="display: none">
                        <span class="timer"><?= CHtml::tag(
                                'time',
                                ['datetime' => $tf->formatMachine($cart->totalReservedTo), 'data-countdown' => true],
                                $tf->format($cart->totalReservedTo)
                            ) ?></span>
                </li>
            <?php else: ?>
                <li class="timer-place" data-quickcart-timer="true" style="display: none">
                        <span class="timer" style="display: none"><?= CHtml::tag(
                                'time',
                                ['datetime' => $tf->formatMachine(time())],
                                '0'
                            ) ?></span>
                </li>
            <?php endif ?>
            <li class="dropdown" data-quickcart-container="true">
                <span class="dropdown-toggle cart" role="button"
                      data-toggle="dropdown"><?= $this->t('Basket:') ?> <span
                            class="goods"><?= $cart->getCount() ?></span></span>
                <div class="dropdown-menu quick-cart" <?php if ($cart->getItemsCount() == 0) { ?> style="display: none" <?php } ?>>
                    <ul class="quick-cart-goods">
                        <?php foreach ($cart->getPositions() as $position): ?>
                            <li>
                                <div class="item"
                                     data-is-expired="<?= $position->isExpired ? 'true' : 'false' ?>"
                                     data-position-price="<?= $position->product->price ?>"
                                     data-position-number="<?= $position->number ?>"
                                     data-position-event="<?= $position->event_id ?>"
                                     data-position-product="<?= $position->product->product_id ?>"
                                     data-position-size="<?= $position->product->size ?>"
                                     data-position-token="<?= $app->tokenManager->getToken($position->product->product_id) ?>"
                                >
                                    <div class="title-line">
                                        <?= CHtml::link(
                                            CHtml::image($position->product->product->logo->getThumbnail('small60')->url) . CHtml::encode($position->product->product->name),
                                            ['product/index', 'id' => $position->product->product_id, 'eventId' => $position->event_id],
                                            ['class' => 'title']
                                        ) ?>
                                        <span class="number-line">x<span class="number"><?= $position->number ?></span></span>
                                        <span class="time"><?= CHtml::tag(
                                                'time',
                                                ['datetime' => $tf->formatMachine($position->reservedTo), 'data-countdown' => true],
                                                $tf->format($position->reservedTo)
                                            ) ?></span>
                                    </div>
                                    <span class="size-line">
                                            <?php if ($position->product->size): ?>
                                                <?= $this->t('Size') ?>: <span
                                                        class="size"><?= $position->product->size ?></span>
                                            <?php endif; ?>
                                        &nbsp;
                                        </span>
                                    <span class="foot-line">
                                            <span class="price"><?= $cf->format($position->product->price, $cn->getName('short')) ?></span>
                                            <span class="delete"><?= $this->t('Remove') ?></span>
                                        </span>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="quick-cart-footer">
                        <div class="quick-cart-price">
                            <span class="price"><?= $cf->format($cart->getCost(false, null, true, false), $cn->getName('short')) ?></span>
                            <?php if ($discountCampaignText) : ?>
                                <?= $discountCampaignText ?>
                            <?php endif ?>
                        </div>
                        <?php if ($cart->isEnoughToBuy(true) && !$app->user->isGuest): ?>
                            <a class="buy btn green"
                               href="<?= $app->createUrl('order/delivery', ['#' => 'content'] + $splitTesting->getAvailable()) ?>"><?= $this->t('Checkout') ?></a>
                        <?php else: ?>
                            <a class="buy btn green"
                               href="<?= $app->createUrl('order/index', ['#' => 'content'] + $splitTesting->getAvailable()) ?>"><?= $this->t('Checkout') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </li>
            <?php if ($app->user->isGuest): ?>
                <li><?= \CHtml::link($this->t('Sign Up'), '#', ['data-modal-auth' => 'signup']) ?></li>
                <li><?= \CHtml::link($this->t('Log in'), '#', ['data-modal-auth' => 'signin']) ?></li>
            <?php else: ?>
                <li><?= \CHtml::link($this->t('Logout'), ['auth/logout']) ?></li>
                <li>|</li>
                <li><?= \CHtml::link($this->t('My account'), ['/account']) ?></li>
            <?php endif ?>
        </ul>
    </div>
</div>

