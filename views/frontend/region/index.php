<?php

use MommyCom\Controller\Frontend\RegionController;
use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionDetector;
use MommyCom\Service\Region\Regions;

/**
 * @var $this RegionController
 */

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
$user = $this->app()->user;

$languages = [
    'en' => 'English',
    'in' => 'Indonesian',
    'vi' => 'Vietnamese',
    'ro' => 'Romanian',
    'ru' => 'Russian',
    'es_co' => 'Colombian',
];
$langCookieName = '_lang';

/** @var CSecurityManager $sm */
$sm = $this->app()->securityManager;

$createCookie = function ($name, $value) use ($sm, $regions, $user) {
    $value = $sm->hashData(serialize($value));
    $value = rawurlencode($value);

    $domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);

    return sprintf('%s=%s;domain=%s;expires=Tue, 19 Jan 2038 03:14:07 GMT;path=/', $name, $value, $domain);
};

$clearCookie = function ($name) use ($regions, $user) {
    $value = '0';
    $domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);

    return sprintf('%s=%s;domain=%s;expires=Thu, 01 Jan 1970 01:00:00 GMT;path=/', $name, $value, $domain);
};

$getButtonColor = function (string $cookieName, string $value) use ($regions) {
    $cookie = (string)$this->app()->request->getCookies()->itemAt($cookieName);

    return ($value === $cookie) ? 'btn-color active' : 'btn-color';
}

?>

<style>
    .btn-color {
        background-color: #d5d5d5;
    }

    .btn-color:hover {
        background-color: #edeeee;
        color: #6d7477;
    }

    .btn-color.active {
        background-color: #7aa78b;
        color: #fff;
    }
</style>

<div class="main">
    <div class="container" style="display: flex; margin-bottom: 30px">
        <div style="flex: 1 1 auto">
            <h2 style="text-align: center">Region</h2>
            <ul style="margin: auto; width: 250px">
                <?php foreach ($regions->getRegions() as $regionName => $regionDomain): ?>
                    <li>
                        <button class="set-action btn <?= $getButtonColor(RegionDetector::getCookieName(), $regionName) ?>"
                                data-cookie="<?= $createCookie(RegionDetector::getCookieName(), $regionName) ?>">
                            <?= $regionName ?>
                        </button>
                    </li>
                <?php endforeach; ?>

                <li>
                    <button class="set-action btn red"
                            data-cookie="<?= $clearCookie(RegionDetector::getCookieName()) ?>">Reset
                    </button>
                </li>
            </ul>
        </div>
        <div style="flex: 1 1 auto">
            <h2 style="text-align: center">Language</h2>
            <ul style="margin: auto; width: 250px">
                <?php foreach ($languages as $code => $language): ?>
                    <li>
                        <button class="set-action btn <?= $getButtonColor($langCookieName, $code) ?>"
                                data-cookie="<?= $createCookie($langCookieName, $code) ?>">
                            <?= $language ?>
                        </button>
                    </li>
                <?php endforeach; ?>

                <li>
                    <button class="set-action btn red"
                            data-cookie="<?= $clearCookie($langCookieName) ?>">Reset
                    </button>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.set-action').click(function (e) {
        e.preventDefault();

        document.cookie = $(this).data('cookie');

        $(this).closest('ul').find('.set-action').removeClass('active');
        $(this).addClass('active');

        window.location.reload();
    });
</script>
