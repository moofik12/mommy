<?php

use MommyCom\Controller\Frontend\MessageController;

/**
 * @var MessageController $this
 * @var string $title
 * @var string $message
 * @var string $description
 */

$this->pageTitle = $title;
$app = $this->app();
?>

<div class="container">
    <div class="standard-message">
        <div class="message-wrapper">
            <div class="message-inner">
                <div class="message-title"><?= $message ?></div>
                <?php if ($description) : ?>
                    <div class="line"></div>
                    <div class="message-text"><?= $description ?></div>
                <?php endif ?>
                <div class="message-footer">
                    <a class="btn green center"
                       href="<?= $app->createUrl('index/index') ?>"><?= $this->t('Home') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($_GET['present-registration-box-registered'])) : ?>
    <?php if (strpos($_GET['present-registration-box-registered'], 'success') !== false) : ?>
        <script>
            $(function () {
                $('#mailing-type').modal('show');
            });
        </script>
    <?php endif; ?>
<?php endif; ?>
