<?php

$app = $this->app();

/** @var $cs CClientScript */
$cs = $app->clientScript;

$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/sizeguide.css'));
?>
<div class="sizes-wrapper">
    <div class="container sizes">
        <h1><?= $this->t('Dimension mesh') ?></h1>
        <ul>
            <li>
                <a href="#size-woman-first" class="size-header">
                    <img class="size-icon" src="/static/sizing/dress.png"/>
                    <div class="size-desc">
                        <?= $this->t('Women\'s clothing') ?><br/>
                        <span class="size-instruction"><?= $this->t('Blouses, tunics, jackets, dresses') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
            <li>
                <a href="#size-woman-second" class="size-header">
                    <img class="size-icon" src="/static/sizing/pegged-pants.png"/>
                    <div class="size-desc">
                        <?= $this->t('Women\'s clothing') ?><br/>
                        <span class="size-instruction"><?= $this->t('Pants, skirts, shorts, jeans') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
        </ul>
        <ul>
            <li>
                <a href="#size-men-first" class="size-header">
                    <img class="size-icon" src="/static/sizing/chino-shorts.png"/>
                    <div class="size-desc">
                        <?= $this->t('Men\'s Clothing') ?><br/>
                        <span class="size-instruction"><?= $this->t('Pants, shorts, jeans') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
            <li>
                <a href="#size-men-second" class="size-header">
                    <img class="size-icon" src="/static/sizing/t-shirt.png"/>
                    <div class="size-desc">
                        <?= $this->t('Men\'s Clothing') ?><br/>
                        <span class="size-instruction"><?= $this->t('Jackets, sweaters, vests, bathrobes, shirts') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
        </ul>
        <ul>
            <li>
                <a href="#size-shoes-first" class="size-header">
                    <img class="size-icon" src="/static/sizing/baby-shoes.png"/>
                    <div class="size-desc">
                        <?= $this->t('Footwear') ?><br/>
                        <span class="size-instruction"><?= $this->t('Children\'s shoes') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
            <li>
                <a href="#size-shoes-second" class="size-header">
                    <img class="size-icon" src="/static/sizing/high-heels-2.png"/>
                    <div class="size-desc">
                        <?= $this->t('Footwear') ?><br/>
                        <span class="size-instruction"><?= $this->t('Women\'s and men\'s shoes') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
        </ul>
        <ul>
            <li>
                <a href="#size-baby-first" class="size-header">
                    <img class="size-icon" src="/static/sizing/baby.png"/>
                    <div class="size-desc">
                        <?= $this->t('Baby clothes') ?><br/>
                        <span class="size-instruction"><?= $this->t('Boys') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
            <li>
                <a href="#size-baby-second" class="size-header">
                    <img class="size-icon" src="/static/sizing/baby-girl.png"/>
                    <div class="size-desc">
                        <?= $this->t('Baby clothes') ?><br/>
                        <span class="size-instruction"><?= $this->t('Girls') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
            <li>
                <a href="#size-baby-third" class="size-header">
                    <img class="size-icon" src="/static/sizing/pijama.png"/>
                    <div class="size-desc">
                        <?= $this->t('Baby clothes') ?><br/>
                        <span class="size-instruction"><?= $this->t('Babies (up to 3 years)') ?></span>
                    </div>
                </a>
            </li>
            <div class="clearfix"></div>
        </ul>
    </div>

    <?php $this->renderPartial('//sizeGuide/woman') ?>
    <?php $this->renderPartial('//sizeGuide/man') ?>
    <?php $this->renderPartial('//sizeGuide/shoes') ?>
    <?php $this->renderPartial('//sizeGuide/children') ?>
</div>
