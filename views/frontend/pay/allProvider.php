<?php

use MommyCom\Controller\Frontend\PayController;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Money\CurrencyFormatter;
use MommyCom\Service\PaymentGateway\PaymentGatewayInterface;

/**
 * @var PayController $this
 * @var string $uid
 * @var OrderRecord[] $orders
 * @var float $amount
 * @var float $deliveryPrice
 * @var float $bonusesAmount
 * @var float $discountAmount
 * @var int $countProductsNumber
 * @var PaymentGatewayInterface[] $availableGateways
 */

/** @var CurrencyFormatter $cf */
$cf = $this->container->get(CurrencyFormatter::class);

$this->pageTitle = $this->t('Order payment');

?>
<div class="container">
    <?php foreach ($orders as $order): ?>
        <div class="cart-title"><?= $this->t('Payment for order №') ?> <?= $order->id ?></div>
        <?php $this->renderPartial('_orderProducts', ['order' => $order]) ?>
    <?php endforeach; ?>
    <div class="cart-footer pay">
        <div class="cart-number">
            <span class="number"><?= $countProductsNumber ?></span> <?= $this->t('goods | goods |', $countProductsNumber) ?> <?= $this->t('for purchase') ?>
        </div>
        <div class="price">
            <?php if ($bonusesAmount > 0): ?>
                <div class="price-bonus"><?= $this->t('Offers') ?> : <?= $cf->format($bonusesAmount) ?></div>
            <?php endif ?>
            <?php if ($discountAmount > 0): ?>
                <div class="price-sale"><?= $this->t('A discount') ?>: <?= $cf->format($discountAmount) ?></div>
            <?php endif ?>
            <?php if ($deliveryPrice > 0): ?>
                <div class="price-delivery"><?= $this->t('A delivery price') ?>: <?= $cf->format($deliveryPrice) ?></div>
            <?php endif ?>
            <div class="special-price">
                <span class="price-label"><?= $this->t('Total') ?>:</span>
                <span class="price-value">
                    <span class="in-total-same"><span class="price-total"><?= $cf->format($amount) ?></span></span>
                </span>
            </div>
        </div>
    </div>
    <div class="cart-form-footer">
        <div class="paid-title"><?= $this->t('Pay through') ?></div>
        <div class="paid-block no-select">
            <?php
            foreach ($availableGateways as $availableGateway) {
                echo $availableGateway->render($this, $uid);
            }
            ?>
        </div>
    </div>
</div>
