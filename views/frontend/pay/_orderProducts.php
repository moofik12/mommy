<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var $this FrontController
 * @var $order OrderRecord
 */

$cf = $this->app()->currencyFormatter;
$app = $this->app();
$positions = $order->getPositionsAvailableForPay();
$bonusAmount = $order->bonuses;
$discountAmount = $order->getCountDiscount();
$cn = $this->app()->countries->getCurrency();
?>

<table class="cart-goods">
    <thead>
    <tr>
        <th class="image"><?= $this->t('Product') ?></th>
        <th class="about"><?= $this->t('Description') ?></th>
        <th class="price"><?= $this->t('Number and cost') ?></th>
    </tr>
    </thead>
    <tfoot class="short">
    <tr>
        <td class="image" colspan="3"><span
                    class="number"><?= count($positions) ?></span> <?= $this->t('goods | goods |', count($positions)) ?> <?= $this->t('for purchase') ?>
        </td>
    </tr>
    </tfoot>
    <tbody>
    <?php foreach ($positions as $position): ?>
        <tr class="separator">
            <td colspan="4"></td>
        </tr>
        <tr data-position-price="<?= $position->product->price ?>"
            data-position-number="<?= $position->number ?>"
            data-position-event="<?= $position->event_id ?>"
            data-position-product="<?= $position->product->product_id ?>"
            data-position-size="<?= $position->product->size ?>"
            data-position-token="<?= $this->app()->tokenManager->getToken($position->product->product_id) ?>"
        >
            <td class="image">
                <?= CHtml::link(
                    CHtml::image($position->product->product->logo->getThumbnail('small70')->url),
                    ['product/index', 'id' => $position->product->product_id, 'eventId' => $position->event_id],
                    ['class' => 'title', 'data-toggle' => 'modal', 'data-target' => "#cart-img-{$position->product->product_id}"]
                ) ?>
            </td>
            <td class="about">
                <div class="about-inner">
                    <a class="item-link"
                       href="<?= $app->createUrl('product/index', ['id' => $position->product->product_id, 'eventId' => $position->event_id, '#' => 'content']) ?>">
                        <?= CHtml::encode($position->product->product->name) ?>
                    </a>
                    <a class="item-cat-link"
                       href="<?= $app->createUrl('event/index', ['id' => $position->event_id, '#' => 'content']) ?>">
                        <?= CHtml::encode($position->event->name) ?>
                    </a>
                    <?php if (!empty($position->product->color) && !empty($position->product->size)): ?>
                        <ul>
                            <?php if (!empty($position->product->color)): ?>
                                <li><?= $this->t('Colour') ?>: <span
                                            class="color"><?= $position->product->color ?></span></li>
                            <?php endif ?>

                            <?php if (!empty($position->product->size)): ?>
                                <li><?= $this->t('Size') ?>: <span
                                            class="size"><?= $position->product->size ?></span></li>
                            <?php endif ?>
                        </ul>
                    <?php endif ?>
                </div>
            </td>
            <td class="price">
                <div class="price-wrapper">
                    <div class="price-inner-wrapper">
                        <div class="price-inner">
                                <span class="our-price">
                                    <span class="item-price"><span
                                                class="number"><?= $cf->format($position->price) ?></span></span>
                                    <span class="el">x</span>
                                    <span class="number"><?= $position->number ?></span>
                                    <span class="el">=</span>
                                    <span class="result"><span
                                                class="number"><?= $cf->format($position->getPriceTotal()) ?></span></span>
                                </span>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
<div class="modals">
    <?php foreach ($positions as $position): ?>
        <div class="modal hide cart-img" id="cart-img-<?= $position->product->product_id ?>">
            <div class="modal-wrapper">
                <div class="big-image-container">
                    <?= CHtml::image($position->product->product->logo->getThumbnail('mid380')->url) ?>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
