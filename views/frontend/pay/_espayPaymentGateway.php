<?php

use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\PaymentGateway\EspayApi\EspayApi;
use MommyCom\Service\PaymentGateway\EspayApi\Provider;

/**
 * @var Controller $this
 * @var string $uid
 * @var string $backUrl
 * @var Provider[] $providers
 * @var EspayApi $espayApi
 */

$espayImageUrl = $espayApi->getImageUrl('espay');

foreach ($providers as $provider) {
    echo '<div class="paid-btn-field">';
    $image = CHtml::image($espayImageUrl, '', [
        'data-src' => $espayApi->getImageUrl($provider->productCode),
    ]);
    echo CHtml::tag('button', [
        'class' => 'btn btn-center btn-espay',
        'data-code' => $provider->bankCode,
        'data-product' => $provider->productCode,
    ], $image);
    echo CHtml::tag('span', [], $provider->productName);
    echo '</div>';
}

?>
<iframe id="sgoplus-iframe" scrolling="no" frameborder="0" style="display: none"></iframe>
<script type="text/javascript">
    $(function () {
        var
            espayLoaded = false,
            espayRedirect = function () {
                var btn = $('.paid-btn-field .btn-espay.clicked');

                if (!espayLoaded || !btn.length) {
                    return;
                }

                var data = {
                        key: '<?= $espayApi->getApiKey() ?>',
                        paymentId: '<?= $uid ?>',
                        backUrl: encodeURIComponent('<?= $backUrl ?>'),
                        bankCode: btn.attr('data-code'),
                        bankProduct: btn.attr('data-product')
                    },
                    sgoPlusIframe = document.getElementById("sgoplus-iframe");

                if (sgoPlusIframe === null) {
                    return;
                }
                sgoPlusIframe.src = SGOSignature.getIframeURL(data);
                SGOSignature.receiveForm();
            };

        $('.paid-btn-field .btn-espay img[data-src]').each(function () {
            var img = this, tmp = new Image();
            tmp.onload = function () {
                img.src = tmp.src;
            };
            tmp.src = img.getAttribute('data-src');
        });
        $.getScript('<?= $espayApi->getJsUrl() ?>').done(function () {
            espayLoaded = true;
            espayRedirect();
        });

        $('.paid-btn-field').on('click', '.btn-espay', function (e) {
            e.preventDefault();

            $('.paid-btn-field .btn-espay').removeClass('clicked');
            $(this).addClass('clicked');

            espayRedirect();
        });
    });
</script>
