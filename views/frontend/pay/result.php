<?php

use MommyCom\Controller\Frontend\PayController;

/**
 * @var PayController $this
 * @var string $status
 */

$this->pageTitle = $this->t('Order payment');
$ordersUrl = $this->app()->createUrl('account/orders', ['#' => 'events']);

?>
<div class="container">
    <div class="standard-message">
        <div class="message-wrapper">
            <div class="message-inner">
                <div class="message-title"><?= CHtml::encode($status) ?></div>
                <div class="message-footer">
                    <a class="btn red btn-center" href="<?= $ordersUrl ?>"><?= $this->t('My orders') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
