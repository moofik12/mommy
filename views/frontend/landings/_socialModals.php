<?php
/**
 * @var $social string
 */
?>

<div class="modal hide login-vk-end" id="vk-end">
    <div class="modal-wrapper">
        <div class="modal-body">
            <div class="forgot">
                <?php $form = $this->beginWidget('CActiveForm', [
                    'enableAjaxValidation' => false,
                    'action' => $this->app()->createUrl('landings/networkRegistration'),
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'submitting' => true,
                        'validateOnSubmit' => true,
                    ],
                    //'focus' => array($authForm, 'email'),
                ]) ?>
                <div class="form-title"><?= $this->t('Completion of registration') ?></div>
                <div class="field <?= $registrationSocial->hasErrors('email') ? 'error' : '' ?>">
                    <?= $form->textField($registrationSocial, 'email', ['value' => 'Enter your email', 'autocomplete' => 'off']) ?>
                    <?= $form->error($registrationSocial, 'email', ['class' => 'message']) ?>
                </div>

                <div class="login-vk-group">
                    <?= CHtml::submitButton('Log in', ['class' => 'left']) ?>
                    <?= CHtml::submitButton('Cancel', ['class' => 'right back', 'data-dismiss' => 'modal']) ?>
                </div>
                <?php $this->endWidget() ?>
            </div>
        </div>
    </div>
</div>
<div class="modal hide login-vk-end" id="vk-end-pass">
    <div class="modal-wrapper">
        <div class="modal-body">
            <div class="forgot">
                <?php $form = $this->beginWidget('CActiveForm', [
                    'enableAjaxValidation' => true,
                    'action' => $this->app()->createUrl('landings/networkRegistration'),
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'submitting' => true,
                        'validateOnSubmit' => true,
                    ],
                ]) ?>
                <div class="form-title"><?= $this->t('You are already registered. Enter your password for MOMMY.') ?></div>
                <div class="field <?= $registrationSocialConfirm->hasErrors('password') ? 'error' : '' ?>">
                    <?= $form->passwordField($registrationSocialConfirm, 'password', ['placeholder' => 'your password', 'autocomplete' => 'off']) ?>
                    <?= $form->error($registrationSocialConfirm, 'password', ['class' => 'message']) ?>
                </div>

                <div class="login-vk-group">
                    <?= CHtml::submitButton('Log in', ['class' => 'left']) ?>
                    <?= CHtml::submitButton('Cancel', ['class' => 'right back', 'data-dismiss' => 'modal']) ?>
                </div>
                <?php $this->endWidget() ?>
            </div>
        </div>
    </div>
</div>

<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('social', '$(".modals input[type=text]").one("click", function() {
        $(this).val("");
    });');

if ($social === 'socialNotEmail') {
    $cs->registerScript('socialNotEmail', '$("#vk-end").modal("show");');
}

if ($social === 'socialConfirm') {
    $cs->registerScript('socialConfirm', '$("#vk-end-pass").modal("show");');
}
?>
