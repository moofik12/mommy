<?php

use MommyCom\Controller\Frontend\LandingsController;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;

/**
 * @var $this LandingsController
 * @var $promoId integer promo по которой был сделан переход
 * @var $tab string
 * @var $registrationForm UserRecord
 * @var $social string
 * @var $registrationSocial NetworkRegistrationForm
 * @var $registrationSocialConfirm NetworkRegistrationConfirmForm
 */

$app = $this->app();
$this->pageTitle = $this->t('Mommy Shopping-club ');

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$tm = $tokenManager = $app->tokenManager;

$selector = EventProductRecord::model()->orderByRand()->with([
    'event' => [
        'joinType' => 'INNER JOIN',
        'scopes' => [
            'onlyTimeActive' => null,
        ],
    ],
])->limit(100)->cache(100);
$groupedProducts = GroupedProducts::fromProducts($selector->findAll());
?>
<div class="container-wrap">
    <header role="banner" class="land-header">
        <div class="land-header-bg">
            <div class="container">
                <a href="/" class="dib land-header-logo"><span class="pseudo-image logo-image"></span></a>
                <a href="#" class="land-btn h-btn" data-modal-registration="true"></a>
                <div class="h-box">
                    <h2 class="h-title">
                        <span class="pseudo-image small-logo-image"></span> <?= $this->t('это') ?>:
                    </h2>
                    <ul class="h-list">
                        <li class="h-item"><?= $this->t('regular sales up to 70%') ?></li>
                        <li class="h-item"><?= $this->t('the best brands at the best prices') ?></li>
                        <li class="h-item"><?= $this->t('special promotions for club members') ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div role="main" class="land-content">
        <section role="section" class="main-section">
            <div class="container">
                <div class="land-title"><?= $this->t('why us?') ?></div>
                <ul class="boxlist jbox">
                    <li class="dib boxlist-item">
                        <i class="icon sale-icon"></i>
                        <div class="boxlist-text"><?= $this->t('Discounts up to 70%') ?></div>
                    </li>
                    <li class="dib boxlist-item">
                        <i class="icon refresh-icon"></i>
                        <div class="boxlist-text"><?= $this->t('Purchase returns') ?></div>
                    </li>
                    <li class="dib boxlist-item">
                        <i class="icon delivery-icon"></i>
                        <div class="boxlist-text"><?= $this->t('Delivery all over ') ?></div>
                    </li>
                    <li class="dib boxlist-item">
                        <i class="icon pay-icon"></i>
                        <div class="boxlist-text"><?= $this->t('Payment upon receipt') ?></div>
                    </li>
                    <li class="dib boxlist-item">
                        <i class="icon ok-icon"></i>
                        <div class="boxlist-text"><?= $this->t('Guarantee of authenticity') ?></div>
                    </li>
                </ul>
                <a href="#" class="land-btn main-btn" data-modal-registration="true"></a>
            </div>
        </section><!--end main-section-->
        <section role="section" class="land-goods-section">
            <div class="container">
                <div class="land-title"><?= $this->t('выгодные товары сегодня') ?>:</div>
                <div class="land-goods-window">
                    <ul class="land-goods-list">
                        <?php foreach (array_slice($groupedProducts->toArray(), 0, 6) as $product): /* @var $product GroupedProduct */ ?>
                            <li class="land-goods-item">
                                <div class="land-goods-item-pic loading">
                                    <img src="<?= $product->product->logo->getThumbnail('mid380')->url ?>" alt=""
                                         class="land-goods-item-img">
                                </div>
                                <div class="land-goods-item-title"><?= $product->product->name ?></div>
                                <div class="land-goods-item-info clearfix">
                                    <div class="land-goods-item-old-price left"><?= $cf->format($product->priceOld, $cn->getName('short')) ?></div>
                                    <div class="land-goods-item-new-price right"><?= $cf->format($product->price, $cn->getName('short')) ?></div>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <a href="#" class="land-btn main-btn" data-modal-registration="true"></a>
            </div>
        </section><!--end land-goods-section-->
    </div><!--end content-->
</div><!--end main block-->
<div class="modals"></div>
<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.landings'));
$cs->registerCssFile($baseUrl . "/$promoId/css/promo.css");
$cs->registerScriptFile($baseUrl . "/$promoId/js/script.js");
?>
