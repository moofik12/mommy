<?php

use MommyCom\Controller\Frontend\LandingsController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;

/**
 * @var $this LandingsController
 * @var $promoId integer promo по которой был сделан переход
 * @var $tab string
 * @var $registrationForm UserRecord
 * @var $social string
 * @var $registrationSocial NetworkRegistrationForm
 * @var $registrationSocialConfirm NetworkRegistrationConfirmForm
 */

$app = $this->app();
$this->pageTitle = $this->t('Mommy Shopping-club ');
?>

<div class="top">
    <div class="top-img"></div>
</div>
<div class="top-shadow"></div>
<div class="container">
    <div class="invite">
        <div class="bg bg-enter <?= $tab === 'registration' ? '' : 'hide' ?>">
            <?php $form = $this->beginWidget('CActiveForm', [
                'action' => $this->app()->createUrl('landings/registration', ['id' => $promoId]),
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => [
                    'validateOnSubmit' => true,
                ],
                'htmlOptions' => [
                    'class' => 'form',
                ],
            ]) ?>
            <div class="text form-title text-center">Успей купить!</div>
            <div class="field">
                <?= $form->textField($registrationForm, 'name', ['value' => $this->t('Your name'), 'autocomplete' => 'off', 'class' => 'name']) ?>
                <?= $form->error($registrationForm, 'name', ['class' => 'message']) ?>
            </div>
            <div class="field">
                <?= $form->textField($registrationForm, 'email', ['value' => $this->t('Your E-mail'), 'autocomplete' => 'off', 'class' => 'email']) ?>
                <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
            </div>
            <div class="pc" data-toggle="modal" data-target="#pc"><?= $this->t('privacy policy') ?></div>
            <?= CHtml::submitButton($this->t('Send')) ?>
        </div>
        <div class="bg <?= $tab === 'success' ? '' : 'hide' ?>">
            <div class="text text-center text1"><?= $this->t('Пожалуйста, проверьте Вашу почту. Вам пришло письмо! ') ?></div>
        </div>

    </div>
</div>
<div class="modals">
    <div class="modal hide" id="pc">
        <div class="modal-wrapper">
            <span class="close" data-dismiss="modal">x</span>
            <div class="modal-body">
                <span class="pc-title"><?= $this->t('privacy policy') ?></span><br><br>
                <?= $this->t('The Company operates on the principle of recognition of the information transmitted by the Customer, including personal data, confidential, and therefore does not allow the use of such information for purposes other than fulfillment of the contract of retail sale or other purposes determined by this privacy policy.') ?>
                <br><br>
                <?= $this->t('When completing the Application on the website, the Client provides the following personal data:') ?>
                <br>
                <ul>
                    <li><?= $this->t('Surname, First name;') ?></li>
                    <li><?= $this->t('E-mail address;') ?></li>
                    <li><?= $this->t('Contact phone number;') ?></li>
                    <li><?= $this->t('Delivery address of the products,') ?></li>
                </ul>
                <?= $this->t('expressly, explicitly and unconditionally give their consent to the processing of such data by the Company without a time limit, with a view to executing a contract of retail sale, informing the Client about the conditions of cooperation and promotion of goods on the market.') ?>
                <br><br>
                <?= $this->t('The Company ensures the confidentiality of the client\'s personal data in accordance with the USA law regarding personal data, takes the necessary legal, organizational and technical measures to protect personal data from unauthorized or accidental access, destruction, modification, blocking, copying, distribution, as well as other illegal actions.') ?>
                <br><br>
                <?= $this->t('The Customer agrees to provide the Company with their data for the purposes of executing the concluded contracts and the Privacy Policy and the Company\'s obligations under them to sell and deliver the Goods to the Customer. The Customer\'s consent is expressed in the indication of the information in the appropriate boxes when completing the Application. Consent to the processing of personal data may be withdrawn by sending a written notice to the Company specified in the Contacts.') ?>
                <br><br>
                <?= $this->t('Within 7 days from the date of receipt of the notification, the available personal data will be destroyed and the processing terminated. The Customer agrees to the transfer by the Company of the information provided to the agents and third parties acting on the basis of the contract with the Company, in order to fulfill their obligations to the Customer.') ?>
                <br><br>
                <?= $this->t('The Company is not responsible for the information provided by the Customer on the Site being used publicly.') ?>
                <br><br>
                <?= $this->t('Being a user of the shopping club Mommy.COM, the user can receive periodic mailings (information, holidays and others) to the registered email address, as well as automatic confirmation of orders. Each user voluntarily chooses whether to receive it or not. We guarantee that your email address will not be used for spamming.') ?>
            </div>
        </div>
    </div>
</div>

<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.landings'));
$cs->registerCssFile($baseUrl . "/$promoId/css/promo.css");
$cs->registerScriptFile($baseUrl . "/$promoId/js/script.js");
?>
