<?php

use MommyCom\Controller\Frontend\LandingsController;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;
use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

/**
 * @var $this LandingsController
 * @var $promoId integer promo по которой был сделан переход
 * @var $formOfferRegistration RegistrationOfferForm
 * @var $social string
 * @var $fon string
 * @var $nameStateRedirectTo string
 */

$app = $this->app();
$this->pageTitle = $this->t('Mommy Shopping-club ');

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
$registrationSocial = new NetworkRegistrationForm();
$registrationSocialConfirm = new NetworkRegistrationConfirmForm();
$landingNum = $this->app()->user->getLandingNum();
$promo = isset($this->_promoEnabled[$landingNum]) ? $this->_promoEnabled[$landingNum] : '';
$webUser = $this->app()->user;
/** @var  $webUser ShopWebUser */
$redirectTo = $webUser->getState($nameStateRedirectTo, $this->createAbsoluteUrl('index/index'));
if (strpos($redirectTo, '?') !== false) {
    $redirectTo .= '&';
} else {
    $redirectTo .= '?';
}

$regLine = $this->t('Registration is completed through lending №');
?>
<script>
    var modalIsOpen = false;
</script>
<div class="page">
    <div class="land-header section">
        <a data-toggle="modal" data-target="#present-modal3" href="<?= $app->createUrl('index/index') ?>"
           class="land-logo">mommy.com</a>
        <div class="land-slogan">
            <h1><?= $this->t('Shopping Club with <span>maximum discounts</span>') ?></h1>
            <p><?= $this->t('Clothing for the whole family, household goods, toys, accessories') ?></p>
        </div>
        <div class="land-discount">
            <div class="percent">
                <span><?= $this->t('before') ?></span>-90<sub>%</sub>
            </div>
        </div>
    </div>
    <?php if ($social !== 'success'): ?>
        <div class="land-form-wrapp right-page-form">
            <div class="land-form-wrapp-inner" style="height: auto !important;">
                <div class="land-form-top" style="padding:14px 10px 14px;">
                    <strong><?= $this->t('Become a member and get discounts.') ?></strong>
                    <p style="color:#ff7c91; font-size: 22px;"><?= $this->t('enter your email address') ?></p>
                    <?php $form = $this->beginWidget('CActiveForm', [
                        'action' => $this->app()->createUrl('landings/offerRegistration', ['promo' => $promo, 'fon' => $fon]),
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => [
                            'validateOnSubmit' => true,
                            'afterValidate' => 'js:function($form, data, hasError){
                            if (hasError) {
                                modalIsOpen = true;
                                return;
                            }                
                            $.ajax({
                                url: $form.attr("action"),
                                type: $form.attr("method"),
                                data: $form.serialize(),
                                dataType: "json",
                                success: function (data) {
                                    modalIsOpen = true;
                                    if (data !== null && typeof data === "object") {
                                        if (data.success) {
                                            $form.closest(".land-form-wrapp").addClass("hide");                                                        
                                            $form[0].reset();                                                
                                            Mamam.trackEvent("modals", "present-registration-finish", "' . $regLine . $formOfferRegistration->bannerName . '");
                                            $(document).trigger("mamam.registration.success");
                                            $("#mailing-type").modal("show");                                            
                                        } else  {
                                            alert("' . Yii::t("common", "An error occurred while calculating bonuses. Contact Support") . '");
                                        }
                                    }
                                },
                                error: function () {
                                    modalIsOpen = true;
                                    alert("' . Yii::t("common", "Error. Check your internet connection") . '");
                                }
                            });
                            return false;
                        }',
                        ],
                    ]) ?>
                    <div class="field top">
                        <?= $form->textField($formOfferRegistration, 'email', ['placeholder' => $this->t('Enter your E-mail'), 'autocomplete' => 'off']) ?>
                        <?= $form->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                        <?= $form->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'bannerName') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                    </div>
                    <div class="form-submit">
                        <?= CHtml::submitButton($this->t('Join the club')) ?>
                    </div>
                    <?php $this->endWidget() ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="modal hide final-register" id="final-register">
    <div class="modal-wrapper">
        <div class="land-form-wrapp">
            <div class="land-form-wrapp-inner">
                <div class="land-form-top">
                    <strong><?= $this->t('Completion of registration') ?></strong>
                    <?php $form = $this->beginWidget('CActiveForm', [
                        'action' => $this->app()->createUrl('landings/networkFinishRegistration', ['fon' => $fon]),
                    ]) ?>
                    <div class="field top <?= $registrationSocial->hasErrors('email') ? 'error' : '' ?>">
                        <?= $form->textField($registrationSocial, 'email', ['placeholder' => $this->t('Enter your email'), 'autocomplete' => 'off']) ?>
                        <?= $form->error($registrationSocial, 'email', ['class' => 'message']) ?>
                    </div>
                    <div class="form-submit">
                        <?= CHtml::submitButton($this->t('Log in')) ?>
                    </div>
                    <?php $this->endWidget() ?>
                    <p><?= $this->t('We need your e-mail so that we can send you important notices and personal offers') ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal hide final-register" id="final-register-confirm">
    <div class="modal-wrapper">
        <div class="land-form-wrapp">
            <div class="land-form-wrapp-inner confirm-registration">
                <div class="land-form-top">
                    <strong><?= $this->t('You are already registered') ?></strong>
                    <p><?= $this->t('Enter your password for MOMMY') ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal hide present-modal3" id="present-modal3">
    <div class="modal-wrapper">
        <div class="present-modal3-img"></div>
        <div class="modal-inner">
            <div class="modal-body">
                <span class="close" data-dismiss="modal"></span>
                <?php $form = $this->beginWidget('CActiveForm', [
                    'action' => $this->app()->createUrl('landings/offerRegistration', ['promo' => $promo, 'fon' => $fon]),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'validateOnSubmit' => true,
                        'afterValidate' => 'js:function($form, data, hasError){
                            if (hasError) {
                                return;
                            }                
                            $.ajax({
                                url: $form.attr("action"),
                                type: $form.attr("method"),
                                data: $form.serialize(),
                                dataType: "json",
                                success: function (data) {
                                    if (data !== null && typeof data === "object") {
                                        if (data.success) {
                                            $form.closest(".modal").modal("hide");                                                        
                                            $form[0].reset();                                                
                                            Mamam.trackEvent("modals", "present-registration-finish", "' . $regLine . $formOfferRegistration->bannerName . '");
                                            $(document).trigger("mamam.registration.success");
                                            $("#mailing-type").modal("show");
                                            modalIsOpen = true;
                                        } else  {
                                            alert("' . Yii::t("common", "An error occurred while calculating bonuses. Contact Support") . '");
                                        }
                                    }
                                },
                                error: function () {
                                    alert("' . Yii::t("common", "Error. Check your internet connection") . '");
                                }
                            });
                            return false;
                        }',
                    ],
                ]) ?>
                <div class="standard-block">
                    <div class="field top">
                        <?= $form->textField($formOfferRegistration, 'email', ['placeholder' => $this->t('Enter your E-mail'), 'autocomplete' => 'off']) ?>
                        <?= $form->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                        <?= $form->hiddenField($formOfferRegistration, 'benefice') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'strategy') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'strategyPayments') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'bannerName') ?>
                        <?= $form->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                    </div>
                    <?= CHtml::submitButton($this->t('get a gift'), ['class' => 'btn']) ?>
                    <div class="modal-countdown">
                        <p><?= $this->t('Time is running out.') ?></p>
                        <div class="modal-countdown-wrapp">
                            <div class="modal-countdown-item h">
                                <div class="h1"><span>0</span></div>
                                <div class="h2"><span>0</span></div>
                                <b><?= $this->t('clock') ?></b>
                            </div>
                            <div class="modal-countdown-item m">
                                <div class="m1"><span>0</span></div>
                                <div class="m2"><span>2</span></div>
                                <b><?= $this->t('minutes') ?></b>
                            </div>
                            <div class="modal-countdown-item s">
                                <div class="s1"><span>1</span></div>
                                <div class="s2"><span>0</span></div>
                                <b><?= $this->t('seconds') ?></b>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox-field center">
                        <div><?= $this->t('I agree with the <span class="present-policy">site rules</span>') ?></div>
                    </div>
                </div>
                <?php $this->endWidget() ?>
            </div>
        </div>
    </div>
</div>
<div class="modal hide present-modal15" id="success-info">
    <div class="modal-wrapper">
        <div class="modal-inner">
            <div class="close" data-dismiss="modal"></div>
            <div class="modal-body">
                <div class="modal15-logo">
                    <img src="<?= $baseImgUrl ?>modal14-logo.png" alt=""/>
                </div>
                <div class="modal15-text">
                    <div>
                        <p><?= $this->t('Thank you for registering! <br/> You get') ?></p>
                        <strong><?= $this->t('100 IDR') ?></strong>
                        <div class="present-info"><?= $this->t('You have the promotional code in your mail') ?></div>
                    </div>
                </div>
                <a href="<?= $app->createUrl('index/index') ?>"
                   class="modal15-btn"><?= $this->t('Start shopping') ?></a>
            </div>
        </div>
    </div>
</div>
<!-- distribution begin -->
<div class="modal hide mailing-type" id="mailing-type" data-backdrop="static">
    <div class="modal-wrapper">
        <div class="modal-inner">
            <div class="modal-body">
                <div class="mailing-type-header">
                    <div class="header-image"></div>
                    <div class="header-title">
                        <h2><?= $this->t('We often launch new shares with the best<br /> products at the lowest prices for you') ?></h2>
                    </div>
                </div>
                <div class="mailing-type-content">
                    <h3><?= $this->t('Choose which e-mail notifications you want to receive from us') ?></h3>
                    <div class="mailing-type-items">
                        <div class="mailing-type-item">
                            <?= CHtml::ajaxLink(
                                '<div class="img mailing-icon3"></div>
                                <p>' . $this->t('Newsletters with individual offers') . '</p>
                                <div class="select"><span>' . $this->t('Choose') . '</span></div>',
                                $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_WEEK]),
                                [
                                    'success' => 'function(data) {
                                            modalIsOpen = true;
                                            if (data.success) {
                                                $("#mailing-type").modal("hide");
                                                window.location.replace("' . $redirectTo . 'success-reg-landing=success' . '");
                                                return;
                                            }                                    
                                            var error = data.errors.length > 0 ? data.errors[0] : false;
                                            if (error) {
                                                alert(error);
                                            }
                                        }',
                                    'error' => 'function() {
                                            modalIsOpen = true;
                                            alert("' . Yii::t("common", "Error. Check your internet connection") . '");
                                        }',
                                ],
                                ['class' => 'block-link']
                            ) ?>
                        </div>
                        <div class="mailing-type-item center-item">
                            <?= CHtml::ajaxLink(
                                '<div class="img mailing-icon2"></div>
                                <p>' . $this->t('Letters of new promotions, individual offers, <span>dollar\'s bonus</span> for <br /> purchases and <span>gifts</span>') . '</p>
                                <div class="select"><span>' . $this->t('Choose') . '</span></div>',
                                $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_DAY]),
                                [
                                    'success' => 'function(data) {
                                                modalIsOpen = true;
                                                if (data.success) {
                                                    $("#mailing-type").modal("hide");
                                                    window.location.replace("' . $redirectTo . 'success-reg-landing=success' . '");
                                                    return;
                                                }
                                                
                                                var error = data.errors.length > 0 ? data.errors[0] : false;
                                                if (error) {
                                                    alert(error);
                                                }
                                            }',
                                    'error' => 'function() {
                                                modalIsOpen = true;
                                                alert("' . Yii::t("common", "Error. Check your internet connection") . '");
                                        }',
                                ],
                                ['class' => 'block-link']
                            ) ?>
                        </div>
                        <div class="mailing-type-item">
                            <?= CHtml::ajaxLink(
                                '<div class="img mailing-icon1"></div>
                                    <p>' . Yii::t("common", "Letters<br />about new stocks") . '</p>
                                <div class="select"><span>' . Yii::t("common", "Choose") . '</span></div>',
                                $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_DAY]),
                                [
                                    'success' => 'function(data) {
                                            modalIsOpen = true;
                                            if (data.success) {
                                                $("#mailing-type").modal("hide");
                                                window.location.replace("' . $redirectTo . 'success-reg-landing=success' . '");
                                                return;
                                            }
                                            
                                            var error = data.errors.length > 0 ? data.errors[0] : false;
                                            if (error) {
                                                alert(error);
                                            }
                                        }',
                                    'error' => 'function() {
                                            alert("' . Yii::t("common", "Error. Check your internet connection") . '");
                                            modalIsOpen = true;
                                        }',
                                ],
                                ['class' => 'block-link']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- distribution end -->

<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.landings'));
$basePath = realpath(Yii::getPathOfAlias('assets.landings'));
$modifiedAt = @filemtime($basePath . "/$promoId/css/landing.css");
$cs->registerCssFile($baseUrl . "/$promoId/css/landing.css?v=" . $modifiedAt);
if ($social === '') {
    $modifiedAt = @filemtime($basePath . "/$promoId/js/functions.js");
    $cs->registerScriptFile($baseUrl . "/$promoId/js/functions.js?v=" . $modifiedAt);
}
if ($social === 'success') {
    $cs->registerScript('successInfo', '
        $("#mailing-type").modal("show");
        Mamam.trackEvent("modals", "present-registration-finish", "' . $regLine . $formOfferRegistration->bannerName . '");                                            
    ');
}
if ($social === 'socialNotEmail') {
    $cs->registerScript('socialNotEmail', '$("#final-register").modal("show");');
}

if ($social === 'socialConfirm') {
    $cs->registerScript('socialConfirm', '$("#final-register-confirm").modal("show");');
}
?>
<?php if ($social == '') : ?>
    <script>
        /*$(document).ready(function() {
            $(document).mouseup(function (e) {
                var container = $(".modal");
                if (e.target.nodeName != "A" && e.target.nodeName != "INPUT") {
                    if (container.has(e.target).length === 0) {
                        $("#present-modal3").modal("show");
                    }
                }
            });
        });*/
    </script>
<?php endif; ?>
