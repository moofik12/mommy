<?php

use MommyCom\Controller\Frontend\LandingsController;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;

/**
 * @var $this LandingsController
 * @var $promoId integer promo по которой был сделан переход
 * @var $tab string
 * @var $registrationForm UserRecord
 * @var $social string
 * @var $registrationSocial NetworkRegistrationForm
 * @var $registrationSocialConfirm NetworkRegistrationConfirmForm
 */

$app = $this->app();
$this->pageTitle = $this->t('Mommy Shopping-club ');

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$tm = $tokenManager = $app->tokenManager;

$selector = EventProductRecord::model()->orderByRand()->with([
    'event' => [
        'joinType' => 'INNER JOIN',
        'scopes' => [
            'onlyTimeActive' => null,
        ],
    ],
])->limit(100)->cache(100);
$groupedProducts = GroupedProducts::fromProducts($selector->findAll());

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.landings'));
$landingBaseUrl = $baseUrl . "/$promoId/";
$cs->registerCssFile($landingBaseUrl . 'css/promo.css');
//$cs->registerScriptFile($landingBaseUrl . 'js/script.js');

?>
<div class="head">
    <div class="head_position">
        <a href="/" class="logo" target="_blank">
            <img src="<?= $landingBaseUrl . 'images/logo.png' ?>"
                 alt="<?= $this->t('Shopping club for children and moms mommy.com') ?>">
        </a>
        <h2 class="one_club"
            style="font-size: 26px;"><?= $this->t('THE FIRST SHOPPING CLUB IN THE COUNTRY <br> FOR MOMS AND CHILDREN') ?></h2>
        <a id="od" href="#" class="button_vk button_klass" style="font-size: 19px;">
                <span class="bg_butt_main" data-modal-registration="true">
                    <img class="B O" src="<?= $landingBaseUrl . 'images/O.png' ?>" alt="О">
                    <img src="<?= $landingBaseUrl . 'images/vertical_line.png' ?>" class="vertical_line" alt="">
                    <?= $this->t('Join the club') ?>
                    <img src="<?= $landingBaseUrl . 'images/shadow_white_top.png' ?>" class="shadow_white_top" alt="">
                </span>
        </a>
        <!--///-->

        <h1 style="font-size: 19px;"><?= $this->t('Stylish and quality branded<br> products for moms, children and kids') ?></h1>
    </div>
    <img src="<?= $landingBaseUrl . 'images/landPhotoTest2.jpg' ?>" class="head_bg" alt="">
    <img src="<?= $landingBaseUrl . 'images/line_shadow_photo.png' ?>" class="line_shadow_photo" alt="">
</div>

<div class="content_1">
    <h2 class="size" style="font-size: 19px;"><?= $this->t('Guarantees') ?></h2>
    <ul>
        <li><?= $this->t('Guarantee of the best prices, discounts up to 70%') ?></li>
        <li><?= $this->t('100% guarantee of originality') ?></li>
        <li><?= $this->t('Warranty on return of goods') ?></li>
        <li><?= $this->t('Delivery<br>all over the country') ?></li>
        <li><?= $this->t('Payment after<br>receipt of the order') ?></li>
    </ul>
    <div class="clr"></div>
</div>

<div class="content_1 content_2">
    <h2 class="size" style="font-size: 19px;"><?= $this->t('Today on Mommy') ?></h2>
    <ul>
        <?php foreach (array_slice($groupedProducts->toArray(), 0, 5) as $product): /* @var $product GroupedProduct */ ?>
            <li>
                <span>
                    <h3><?= $product->product->name ?></h3>
                    <img src="<?= $product->product->logo->getThumbnail('mid380')->url ?>"
                         alt="<?= $product->product->name ?>">
                    <p>
                        <s><?= $cf->format($product->priceOld) ?><span></span></s>
                        <br>
                        <span class="actual_price">
                            <strong><?= $cf->format($product->price) ?></strong>
                        </span>
                    </p>
                </span>
            </li>
        <?php endforeach ?>
    </ul>
    <div class="clr"></div>
    <a id="od" href="#" class="button_vk button_klass button_vk_small" style="font-size: 19px;">
            <span class="bg_butt_main" data-modal-registration="true">
                <img class="B O" src="<?= $landingBaseUrl . 'images/O.png' ?>" alt="О">
                <img src="<?= $landingBaseUrl . 'images/vertical_line.png' ?>" class="vertical_line" alt="">
                <?= $this->t('Join the club') ?>
                <img src="<?= $landingBaseUrl . 'images/shadow_white_top.png' ?>" class="shadow_white_top" alt="">
            </span>
    </a>
    <!--///-->
</div>
<div class="modals"></div>
