<?php

use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;

/**
 * $this StaticController
 *
 * @var $category StaticPageCategoryRecord
 * @var $page StaticPageRecord
 */

$this->pageTitle = $pages[0]->title;
$this->description = $pages[0]->title . ' - ' . $this->t('MOMMY.COM The first shopping mall in USA for mothers and children. Products for the whole family and for your home. Daily sales with discounts up to 90%.');
$app = $this->app();
?>
<section>
    <header class="page-header">
        <h2 class="container page-title">
            <?= $category->name ?>
        </h2>
    </header>
    <div class="container">
        <div class="page-content">
            <div class="content-menu">
                <ul>
                    <?php foreach ($category->pagesVisible as $item): ?>
                        <li <?= $item->url == $pages[0]->url ? 'class="active"' : '' ?>>
                            <a href="<?= $app->createUrl('static/index', ['category' => $category->url, 'page' => $item->url]) ?>">
                                <?= $item->title ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="content-place">
                <article class="content">
                    <h1><?= $pages[0]->title ?></h1>
                    <?php foreach ($pages as $page): ?>
                        <div class="wrapper">
                            <?= $page->body ?>
                        </div>
                    <?php endforeach ?>
                </article>
            </div>
        </div>
    </div>
</section>
