<?php

use MommyCom\Controller\Frontend\MaintenanceController;

/**
 * @var $this MaintenanceController
 */

$this->pageTitle = $this->t('Technical work is carried out');
?>

<div class="maintenance">
    <h1 style=""><?= $this->t('Technical work is carried out. We apologize for the inconvenience.') ?></h1>
</div>

