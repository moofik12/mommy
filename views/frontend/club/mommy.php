<?php

use MommyCom\Controller\Frontend\ClubController;

/**
 * @var ClubController $this
 * @var string $subId
 * @var int $landingNum
 */

$registrationTrackerJs = $this->app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.front')) . '/js/registration.tracker.js';
$textError = $this->t('Error. Check your internet connection');

?>
<div class="block1">
    <div class="limit">
        <img src="<?= $this->assetPath('/mommy/img/logo.png') ?>" alt="" class="logo">
        <div class="left">

            <div class="ball">
                <h1><?= $this->t('Clothes for children and their parents') ?>
                    <span><?= $this->t('With big') ?></span> <?= $this->t('Discounts') ?></h1>
                <h2><?= $this->t('Mommy.com club will give you the simple advices for every stage of child growth') ?>
                    .</h2>
            </div>
        </div>
        <div class="mob_cont">
            <img src="<?= $this->assetPath('/mommy/img/block1_cont1.png') ?>" alt="" class="cont_img">
            <img src="<?= $this->assetPath('/mommy/img/block1_border.png') ?>" alt="" class="border">
        </div>
        <img src="<?= $this->assetPath('/mommy/img/block1_item.png') ?>" alt="" class="mob_hide item">
        <img src="<?= $this->assetPath('/mommy/img/block1_item.png') ?>" alt="" class="mob_hide item1">
        <button class="btn to_form"><?= $this->t('Join the club') ?></button>
    </div>
</div>

<div class="block2">
    <div class="limit">
        <h1><span><?= $this->t('Many mommies') ?></span> <?= $this->t('often face these problems') ?>:
        </h1>
        <ul class="list">
            <li>
                <img src="<?= $this->assetPath('/mommy/img/list_item1.png') ?>" alt="" class="avatar">
                <p><?= $this->t('Too little time for housework and child') ?></p>
            </li>
            <li>
                <img src="<?= $this->assetPath('/mommy/img/list_item2.png') ?>" alt="" class="avatar">
                <p><?= $this->t('Too little money for children\'s accessories') ?></p>
            </li>
            <li>
                <img src="<?= $this->assetPath('/mommy/img/list_item3.png') ?>" alt="" class="avatar">
                <p><?= $this->t('Lack of experience in upbringing') ?></p>
            </li>
            <li>
                <img src="<?= $this->assetPath('/mommy/img/list_item4.png') ?>" alt="" class="avatar">
                <p><?= $this->t('Moms often do not know what kind of things are necessary for the growth of the child') ?></p>
            </li>
        </ul>
        <h2><?= $this->t('We have the solution for you') ?></h2>
        <img src="<?= $this->assetPath('/mommy/img/arrow_btm.png') ?>" alt="" class="arrow">
        <ul class="slider active1">
            <li class="change">
                <h3> <?= $this->t('1-5 years') ?> </h3><img
                        src="<?= $this->assetPath('/mommy/img/slide1.jpg') ?>" alt="">
                <img src="<?= $this->assetPath('/mommy/img/slide1m.jpg') ?>" alt="" class="mob_sl">
                <div class="text_bg">
                    <div class="text">
                        <p class="label"> <?= $this->t('For children 5-7 years old') ?> </p>
                        <p>
                            <?= $this->t('At this age, it is important to pay attention to the primary development of the child: To develop large and small motor skills, to teach to speak and understand. Our club mommy.com collected a unique selection of products that helps kids avoid problems with undeveloped speech, intelligence and behavior') ?>
                            .
                        </p>
                        <div class="sale">
                            <?= $this->t('Discounts up to') ?>
                            <span>65%!</span>
                        </div>
                        <p>
                            <?= $this->t('You can get acquainted with her on the site of the club\'s store and choose what you like with a discount of up to 65 percent from mommy.com') ?></p>
                    </div>
                </div>
            </li>
            <li class="change">
                <h3> <?= $this->t('5-7 years') ?> </h3><img
                        src="<?= $this->assetPath('/mommy/img/slide2.jpg') ?>" alt="">
                <img src="<?= $this->assetPath('/mommy/img/slide2m.jpg') ?>" alt="" class="mob_sl">
                <div class="text_bg">
                    <div class="text">
                        <p class="label">
                            <?= $this->t('For children 5-7 years old') ?>
                        </p>
                        <p>
                            <?= $this->t('You need to think about the school just at this time, so that your child would like to learn and you did not spoil relations with him, our club has prepared for you the best assistants in the fight against refusal to study, poor discipline and lack of attention') ?>
                        </p>
                        <div class="sale">
                            <?= $this->t('Discounts up to') ?>
                            <span>75%!</span>
                        </div>
                        <p>
                            <?= $this->t('You can find out more on the site of the club\'s store and choose what you like with a discount of up to 75 percent from mommy.com') ?>
                            .
                        </p>
                    </div>
                </div>
            </li>
            <li class="change"><h3> <?= $this->t('7-15 years') ?> </h3><img
                        src="<?= $this->assetPath('/mommy/img/slide3.jpg') ?>"
                        alt="">
                <img src="<?= $this->assetPath('/mommy/img/slide3m.jpg') ?>" alt="" class="mob_sl">
                <div class="text_bg">
                    <div class="text">
                        <p class="label"> <?= $this->t('For children 7-15 years old') ?> </p>
                        <p>
                            <?= $this->t('At this age, children face the problems, which means that their parents too. The child should feel demanded and move to something') ?>
                            .
                        </p>
                        <div class="sale">
                            <?= $this->t('Discounts up to') ?>
                            <span>85%!</span>
                        </div>
                        <p>
                            <?= $this->t('In this difficult issue mommy.com has a lot of experience, we will give advice on how to find your child a lesson to enjoy and improve school performance, overcome aggression and not get into a bad company') ?>
                            .
                        </p>
                    </div>
                </div>
            </li>
            <li class="sale">
                <?= $this->t('Discounts up to') ?>
                <span class="sale_item _item1"> 65%! </span>
                <span class="sale_item _item2"> 75%! </span>
                <span class="sale_item _item3"> 85%! </span>
            </li>
        </ul>
    </div>
    <div class="limit _limit">
        <div class="section">
            <p class="plus"><img src="<?= $this->assetPath('/mommy/img/plus.png') ?>" alt=""></p>
            <h1 class="center"><?= $this->t('By joining the club') ?>,
                <br><?= $this->t('You will also earn') ?>:</h1>
            <ul class="list2">
                <li>
                    <img src="<?= $this->assetPath('/mommy/img/block2_item1.png') ?>" alt="">
                    <p><?= $this->t('Discounts in the online shop') ?></p>
                </li>
                <li>
                    <img src="<?= $this->assetPath('/mommy/img/block2_item2.png') ?>" alt="">
                    <p><?= $this->t('Advices of specialists, professional educators and upbringers') ?></p>
                </li>
                <li>
                    <img src="<?= $this->assetPath('/mommy/img/block2_item3.png') ?>" alt="">
                    <p><?= $this->t('Support and communication with other parents') ?></p>
                </li>

                <li>
                    <img src="<?= $this->assetPath('/mommy/img/block2_item4.png') ?>" alt="">
                    <p><?= $this->t('Unique knowledge that we collected especially for you') ?></p>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="block3">
    <div class="limit">
        <div class="right">
            <h1> <?= $this->t('Mary Evon Grandy') ?> </h1>
            <h2> <?= $this->t('pedagogue-psychologist') ?> </h2>
            <ul class="list">
                <li> <?= $this->t('Higher qualification pedagogical category') ?></li>
                <li> <?= $this->t('She graduated from the Australian State Pedagogical University of Murdoch, psychology and pedagogical faculty') ?></li>
                <li> <?= $this->t('Experience of practical consultation for more than 10 years') ?></li>
            </ul>
            <p class="quote">
                <?= $this->t('To grow a child happy, you need to have a lot of experience and give maximum attention to each of the stages of growing up. I will tell you in details about the children\'s age features in the club mommy.com, as well as answer your questions') ?>
            </p>
            <img src="<?= $this->assetPath('/mommy/img/block3_item1.png') ?>" alt="" class="item1">
            <img src="<?= $this->assetPath('/mommy/img/block3_item2.png') ?>" alt="" class="item2">
            <img src="<?= $this->assetPath('/mommy/img/block3_item3.png') ?>" alt="" class="item3">
            <img src="<?= $this->assetPath('/mommy/img/block3_line.png') ?>" alt="" class="line">
            <img src="<?= $this->assetPath('/mommy/img/block3_cont.png') ?>" alt="" class="girl">
        </div>
    </div>
</div>

<div class="block4">
    <div class="limit">
        <h1> <?= $this->t('Save time and money with other mommy.com members') ?> </h1>
        <h2> <?= $this->t('Participation in the club is free of charge and does not oblige you to make purchases, the above discounts and bonuses are valid only for members of the club') ?>
            . </h2>
        <div class="form_cont">
            <?php $form = $this->beginWidget('CActiveForm', [
                'action' => $this->app()->createUrl('club/registration', ['landing' => $landingNum]),
                'htmlOptions' => [
                    'class' => 'form',
                    'id' => 'order_form',
                ],
            ]) ?>
            <div class="left">
                <label for="RegistrationForm_email"
                       style="font-size: 14px; display: block; text-align: left; margin: 0 0 10px 10px">
                    <?= $this->t('Enter your e-mail') ?>:
                </label>
                <?= $form->textField($registrationForm, 'email', [
                    'required' => false,
                    'class' => 'email',
                    'placeholder' => $this->app()->params['validatorRegex']['email']['default']['placeholder'],
                ]) ?>
            </div>

            <?= CHtml::submitButton($this->t('Join the club'), ['class' => 'btn']) ?>
            <?php $this->endWidget() ?>
        </div>
        <img src="<?= $this->assetPath('/mommy/img/block4_item.png') ?>" alt="" class="item">
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="application/javascript" src="<?= $registrationTrackerJs ?>"></script>
<script>
    registrationTracker.validatorRegex = <?= json_encode($this->app()->params['validatorRegex']) ?>;
    registrationTracker.overrideSettings({
        errorBorderWidth: "2px",
        successBorderWidth: "2px"
    });

    $(document).ready(function () {
        var $slider = $('.slider'),
            slideNum = 1,
            sliderTimer;

        var nextSlide = function () {
            slideNum++;
            if (slideNum > 3) slideNum = 1;

            $slider.removeClass('active1 active2 active3').addClass('active' + slideNum);
        };
        var scheduleNextSile = function () {
            clearTimeout(sliderTimer);
            sliderTimer = setTimeout(nextSlide, 15000);
        };

        scheduleNextSile();

        $('.change').click(function () {
            scheduleNextSile();
            slideNum = $('.change').index(this) + 1;

            if ($slider.hasClass('active' + slideNum)) {
                $slider.removeClass('active1 active2 active3');
            } else {
                $slider.removeClass('active1 active2 active3').addClass('active' + slideNum);
            }
        });

        $('.to_form').click(function () {
            $("html, body").animate({
                scrollTop: $("#order_form").offset().top - 300
            }, 1000);
            return false;
        });

        $('#order_form').submit(function (e) {
            if (
                !registrationTracker.validateEmail('.email')
            ) {
                e.preventDefault();
                return false;
            } else {
                var form = $(this);
                var url = $('form').attr('action');
                $.ajax({
                    url: url,
                    type: form.attr('method'),
                    data: form.serialize(),
                    success: function (data) {
                        if (data.success) {
                            form.trigger('reset');
                            $(document).trigger('mamam.registration.success');
                        } else if (data.info) {
                            alert("<?= $this->t('This user already exists') ?>");
                        } else {
                            alert("<?= $this->t('Error. Please try again later') ?>");
                        }
                    },
                    error: function (error) {
                        alert(error.status + '<?= $textError ?>' + error.statusText);
                    }
                });
            }
            return false;
        });

        $(document).on('mamam.registration.success', function () {
            dataLayer.push({'event': 'mamam.registration.success'});

            var redirect = function () {
                window.location.href = '<?= CHtml::normalizeUrl(['club/thanks']) ?>';
            };

            if (Mamam.clickId) {
                $.ajax({
                    url: 'https://pstb.peerclicktrk.com/postback?cid=' + Mamam.clickId + '&summ_total=1&userid=2071',
                    timeout: 2000,
                    complete: redirect
                });

                return;
            }

            redirect();
        });
    });
</script>
