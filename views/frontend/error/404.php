<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Form\RegistrationForm;
use MommyCom\Model\Product\ViewedProducts;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var FrontController $this
 */

$this->pageTitle = $this->t('Error');
$app = $this->app();
/** @var $cs CClientScript */
$cs = $app->clientScript;
$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/notfound.css'));
$viewedProducts = new ViewedProducts();
$provider = EventRecord::model()
    ->isVirtual(false)
    ->onlyVisible()
    ->onlyPublished()
    ->onlyTimeActive()
    ->isDeleted(false)
    ->orderBy('end_at', 'desc')
    ->orderBy('id', 'rand')
    ->limit(3)
    ->getDataProvider(false, [
        'pagination' => false,
    ]);

$canSubscribe = $this->app()->user->isGuest;
$events = $provider->getData();
$tf = $timerFormatter = $app->timerFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$registrationForm = new RegistrationForm('simple');
$textError = $this->t('Error. Check your internet connection');
?>

<div class="container">
    <?= $this->renderPartial('_productsBlock', [
        'groupedProducts' => $viewedProducts->getMostViewedProducts(6),
    ]) ?>
    <div id="notfoundBlock" class="not-found page">
        <div class="not-found-page">
            <div class="not-found-image"></div>
            <div class="not-fount-subtitle"><?= $this->t('Unfortunately this page is not found, <br> but you can always find something interesting.') ?></div>
            <a class="btn red"
               href="<?= $app->createUrl('index/index') ?>"><?= $this->t('Back to the store') ?></a>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="not-found-events">
        <h2><?= $this->t('Promotions') ?></h2>
        <div class="promo-now">
            <?php foreach ($events as $index => $item): /* @var $item EventRecord */ ?>
                <section>
                    <a href="<?= $app->createUrl('event/index', ['id' => $item->id]) ?>">
                        <div class="box">
                            <header>
                                <div class="image-wrapper">
                                    <?= CHtml::image($item->logo->getThumbnail('mid320')->url) ?>
                                    <?php if ($item->is_drop_shipping): ?>
                                        <label class="fast-delivery"></label>
                                    <?php endif; ?>
                                </div>
                                <div class="title-wrapper"><h1 class="title"><?= CHtml::encode($item->name) ?></h1>
                                </div>
                            </header>
                            <div class="text-wrapper">
                                <p class="about text"><?= CHtml::encode($item->description_short) ?></p>
                                <p class="time text"><?= CHtml::tag(
                                        'time',
                                        ['datetime' => $tf->formatMachine($item->end_at), 'data-countdown' => true],
                                        $tf->format($item->end_at)
                                    ) ?></p>
                                <p class="price text"><?= $this->t('from') ?> <span
                                            class="lowest-price"><?= $cf->format($item->getMinSellingPrice()) ?></span>
                                </p>
                            </div>
                        </div>
                    </a>
                </section>
            <?php endforeach ?>
            <div class="all-events-button">
                <span><a href="/#events"><?= $this->t('All events') ?></a></span>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php if ($canSubscribe): ?>
    <div class="not-found-subscribe">
        <h2><?= $this->t('Subscribe to know about the sales') ?>!</h2>
        <div class="subscribe-inner-subscribe">
            <?php $form = $this->beginWidget('CActiveForm', [
                'id' => 'not-found-registration',
                'action' => $this->app()->createUrl('auth/registration'),
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'clientOptions' => [
                    'validateOnSubmit' => true,
                    'afterValidate' => 'js:function($form, data, hasError) {
                                if (hasError) {
                                    return;
                                }
                                
                                $.ajax({
                                    url: $form.attr("action"),
                                    type: $form.attr("method"),
                                    data: $form.serialize(),
                                    dataType: "json",
                                    success: function (data) {
                                        if (data !== null && typeof data === "object") {
                                            if (data.success) {
                                                $form.closest(".modal").modal("hide");
                                                $(document).trigger("mamam.registration.success");
                                                $(document).trigger("mamam.auth.success");
                                                $form[0].reset();
                                                $("#mailing-type").modal("show");
                                                comebackerModalIsOpen = true;
                                                if ($(\'[data-target="#comebacker"]\').length > 0) {
                                                    $(\'[data-target="#comebacker"]\').hide();
                                                }
                                                if (Mamam.push) {
                                                    Mamam.push.init();
                                                }
                                            } else if (data.errors && data.errors.length > 0) {
                                                
                                            }
                                        }
                                    },
                                    error: function () {
                                        alert("' . $textError . '");
                                    }
                                });

                                return false;
                            }',
                ],
            ]) ?>
            <div class="field">
                <?= $form->textField($registrationForm, 'email', ['id' => 'not-found-email', 'placeholder' => $this->t('Enter your email')]) ?>
                <?= $form->error($registrationForm, 'email', ['id' => 'not-found-email_em_', 'class' => 'message']) ?>
            </div>
            <?= CHtml::submitButton($this->t('Start shopping')) ?>
            <?php $this->endWidget() ?>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $(document).on('hide.bs.modal', function () {
                window.location.href = '/';
            });
        });
    </script>
<? endif; ?>
