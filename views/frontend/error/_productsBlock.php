<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $groupedProducts GroupedProduct */
/* @var $enableItems boolean */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */

$app = $this->app();
$req = $app->request;
$user = $app->user;

$tokenManager = $app->tokenManager;
$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();

?>

<?php if (!empty($groupedProducts)): ?>
    <div class="products-error-page">
        <div class="goods-list">
            <h2 class="recommended-block-title"><?= $this->t('You may like it') ?></h2>
            <?php foreach ($groupedProducts->toArray() as $item): /* @var $item GroupedProduct */ ?>
                <div class="goods-item form-in category-off" itemscope itemtype="http://schema.org/Product">
                    <div class="form-in-wrap">
                        <div class="form-in-wrap-content">

                            <a href="<?= $app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']) ?>">
                                <?php if ($item->isSoldOut): ?>
                                    <div class="label-sold">×</div>
                                <?php else: ?>
                                    <div class="label-discount">-<?= round((1 - $item->price / $item->priceOld) * 100) ?>%</div>
                                    <?php if ($item->isLimited): ?>
                                        <!--<div class="label-ends">!</div>-->
                                    <?php endif ?>
                                    <?php if ($item->isTopOfSelling): ?>
                                        <div class="label-hit"><?= $this->t('HIT') ?></div>
                                    <?php endif ?>
                                <?php endif ?>
                                <div class="box">
                                    <div class="image-wrapper">
                                        <div>
                                            <?= CHtml::image(
                                                $app->baseUrl . '/static/blank.gif', '', [
                                                    'data-src' => $item->product->logo->getThumbnail('mid320_prodlist')->url,
                                                    'data-lazyload' => 'true',
                                                    'itemprop' => "image",
                                                    'style' => 'margin-top: -50px',
                                                ]
                                            ) ?>
                                        </div>
                                    </div>
                                    <div class="text-wrapper">
                                        <p itemprop="name" class="title"><?= CHtml::encode($item->product->name) ?></p>
                                        <div class="price clearfix">
                                            <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                            <?php if ($item->priceOld != $item->price): ?>
                                                <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <div class="product-info-wrapper">
                                <div class="available-info">
                                    <?php if (!$item->isSoldOut && $item->sizeCount > 0): ?>
                                        <label class="available-info-sizes"><?= $this->t('Dimensions') ?>
                                            : </label>
                                        <?php if ($item->sizeCount > 0): ?>
                                            <?php for ($i = 0; $i < count($item->sizes); $i++): ?>
                                                <?php
                                                echo EventProductRecord::replaceLangAgeSize($item->sizes[$i]);
                                                if ($i !== count($item->sizes) - 1) {
                                                    echo ', ';
                                                }
                                                ?>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
<?php endif; ?>
