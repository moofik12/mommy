<?php

use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\ProductBrands;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var $this FrontController
 * @var $provider CDataProvider
 * @method EventRecord[] $provider->getData()
 * @var $futureProvider CDataProvider
 * @var array $events
 */

$this->description = $this->t('The first shopping-club for moms and children in the USA mommy.com Products for the whole family and for your home. Daily discounts of up to 90%');
$this->pageTitle = $this->t('MOMMY.COM: Shopping club for moms and children');
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

//carousel
$cacheCarouselDuration = 600;
$availableProductBrandsId = $this->app()->db
    ->cache($cacheCarouselDuration)
    ->createCommand()
    ->selectDistinct('brand_id')
    ->from(EventProductRecord::model()->tableName())
    ->where(['IN', 'event_id', ArrayUtils::getColumn($provider->getData(), 'id')])
    ->queryColumn();

$availableProductBrandsCombine = array_combine($availableProductBrandsId, $availableProductBrandsId);
$productBrands = ProductBrands::fromBrands(BrandRecord::model()->cache($cacheCarouselDuration)->hasLogo()->findAll());
$productBrandsLogo = $productBrands->random()->toArray();

/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;
?>

<div class="main">
    <div class="container">
        <div class="promo-now">
            <aside class="advantage-bar padding30">
                <ul>
                    <li>
                        <img width="60" height="61" src="<?= $baseImgUrl . 'advantage-best-price.png'?>" alt="<?= $this->t('The lowest prices') ?>">
                        <span><?= $this->t('The lowest prices') ?></span>
                    </li>
                    <li>
                        <img width="60" height="60" src="<?= $baseImgUrl . 'advantage-daily-updates.png'?>" alt="<?= $this->t('10,000 new products') ?>">
                        <span><?= $this->t('10,000 new products') ?></span>
                    </li>
                    <li>
                        <img width="60" height="66" src="<?= $baseImgUrl . 'advantage-back-guarantee.png'?>" alt="<?= $this->t('Refund Guarantee') ?>">
                        <span><?= $this->t('Refund Guarantee') ?></span>
                    </li>
                    <li>
                        <img width="60" height="63" src="<?= $baseImgUrl . 'advantage-best-service.png'?>" alt="<?= $this->t('The best service') ?>">
                        <span><?= $this->t('The best service') ?></span>
                    </li>
                </ul>
            </aside>
            <?php foreach ($events as $index => $item): /* @var $item EventRecord */ ?>
                <section>
                    <a href="<?= $app->createUrl('event/index', ['id' => $item->id]) ?>">
                        <div class="label-discount">
                            -<?= $item->promoDiscountPercent ?>%</span>
                        </div>
                        <div class="box">
                            <header>
                                <div class="image-wrapper">
                                    <?= CHtml::image($item->logo->getThumbnail('mid320')->url, '', [
                                        'style' => 'margin-top: -50px',
                                    ]) ?>
                                    <?php if ($item->is_drop_shipping): ?>
                                        <label class="fast-delivery"></label>
                                    <?php endif; ?>
                                </div>
                                <div class="title-wrapper"><h1 class="title"><?= CHtml::encode($item->name) ?></h1>
                                </div>
                            </header>
                            <div class="text-wrapper">
                                <p class="about text"><?= CHtml::encode($item->description_short) ?></p>
                                <p class="time text"><?= CHtml::tag(
                                        'time',
                                        ['datetime' => $tf->formatMachine($item->end_at), 'data-countdown' => true],
                                        $tf->format($item->end_at)
                                    ) ?></p>
                                <p class="price text"><?= $this->t('from') ?> <span
                                            class="lowest-price"><?= $cf->format($item->getMinSellingPrice()) ?></span>
                                </p>
                            </div>
                        </div>
                    </a>
                </section>
            <?php endforeach ?>
        </div>

        <?php if (false && count($productBrandsLogo) > 4): ?>
            <aside class="clearfix">
                <div class="lined-header">
                    <h2 class="title"><?= $this->t('OUR BRANDS') ?></h2>
                </div>
                <?php if ($this->beginCache('carousel-brands', ['varyByRoute' => true, 'varyByLanguage' => true, 'duration' => $cacheCarouselDuration])) { ?>
                    <div class="carousel-bar">
                        <div class="carousel-inner">
                            <ul class="carousel">
                                <?php foreach ($productBrandsLogo as $brand): ?>
                                    <li>
                                        <?php $hasProductBrand = isset($availableProductBrandsCombine[$brand->id]) ?>
                                        <?= $hasProductBrand ? CHtml::openTag('a', ['data-access-by-login' => 'true', 'href' => "{$app->createUrl('event/brand', ['name' => $brand->name])}"]) : '<span>' ?>
                                        <?= CHtml::image($brand->logo->getThumbnail('small150brand')->url) ?>
                                        <?= $hasProductBrand ? '</a>' : '</span>' ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <div class="carousel-left"><a href="#"></a></div>
                        <div class="carousel-right"><a href="#"></a></div>
                    </div>
                    <?php $this->endCache();
                } ?>
            </aside>
        <?php endif ?>

        <?php if ($futureProvider->totalItemCount > 0): ?>
            <section class="clearfix">
                <div class="lined-header">
                    <h2 class="title"><?= $this->t('COMING SOON') ?></h2>
                </div>
                <div class="promo-soon">
                    <?php foreach ($futureProvider->getData() as $key => $item): /* @var $item EventRecord */ ?>
                        <?php if ($key < floor(count($futureProvider->getData()) / 3) * 3): ?>
                        <article>
                            <div class="box">
                                <div class="image-wrapper">
                                    <?= CHtml::image($item->promo->getThumbnail('mid320')->url) ?>
                                </div>
                                <h3 class="title"><?= CHtml::encode($item->name) ?></h3>
                            </div>
                        </article>
                        <?php endif; ?>
                    <?php endforeach ?>
                </div>
            </section>
        <?php endif ?>
    </div>
</div>
