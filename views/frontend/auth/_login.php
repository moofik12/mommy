<div class="enter <?= $visible ? '' : 'hide' ?>" id="enter">
    <?php $form = $this->beginWidget('CActiveForm', [
        'enableAjaxValidation' => false,
        'action' => $this->app()->createUrl('auth/authenticate', ['name' => 'login']),
        'enableClientValidation' => true,
        'clientOptions' => [
            'submitting' => true,
            'validateOnSubmit' => true,
        ],
        //'focus' => array($authForm, 'email'),
    ]) ?>
    <?= $form->labelEx($authForm, 'email') ?>
    <div class="field <?= $authForm->hasErrors('email') ? 'error' : '' ?>">
        <?= $form->textField($authForm, 'email', ['placeholder' => $this->t('Your E-mail'), 'autocomplete' => 'off']) ?>
        <?= $form->error($authForm, 'email', ['class' => 'message']) ?>
    </div>
    <?= $form->labelEx($authForm, 'password') ?>
    <div class="field <?= $authForm->hasErrors('password') ? 'error' : '' ?>">
        <?= $form->passwordField($authForm, 'password', ['placeholder' => $this->t('your password')]) ?>
        <?= $form->error($authForm, 'password', ['class' => 'message']) ?>
    </div>
    <?= CHtml::submitButton($this->t('Login to the site')) ?>
    <div class="login-checkbox">
        <?= $form->checkBox($authForm, 'remember') ?>
        <?= $form->labelEx($authForm, 'remember') ?>
        <span class="reminder" data-tab="forgot"><?= $this->t('Password reminder') ?></span>
    </div>
    <?php $this->endWidget() ?>

    <div class="login-line"><span><?= $this->t('or') ?></span></div>
</div>
