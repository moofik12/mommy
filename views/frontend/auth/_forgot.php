<div class="forgot <?= $visible ? '' : 'hide' ?>" id="forgot">
    <?php $form = $this->beginWidget('CActiveForm', [
        'action' => $this->app()->createUrl('auth/recovery'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ],
    ]) ?>
    <div class="form-title"><?= $this->t('Password recovery') ?></div>
    <?= $form->labelEx($recoveryForm, 'email') ?>
    <div class="field">
        <?= $form->textField($recoveryForm, 'email', ['placeholder' => $this->t('Your E-mail'), 'autocomplete' => 'off']) ?>
        <?= $form->error($recoveryForm, 'email', ['class' => 'message']) ?>
    </div>
    <?= CHtml::submitButton($this->t('Send the letter')) ?>
    <?php $this->endWidget() ?>
    <div class="help"><?= $this->t('In the letter there will be a link to reset the password') ?></div>
</div>
