<?php

use MommyCom\Controller\Frontend\AuthController;

/**
 * @var $this AuthController
 */

$this->pageTitle = $this->t('thanks for the confirmation');
$app = $this->app();
?>

<div class="container">
    <div class="standard-message">
        <div class="message-wrapper">
            <div class="message-inner">
                <div class="message-title"><?= $this->t('Welcome!') ?></div>
                <div class="line"></div>
                <div class="message-text"><?= $this->t('Immerse yourself in the world of shopping for yourself and your child.') ?></div>
                <div class="message-footer">
                    <a class="btn green"
                       href="<?= $this->app()->createUrl('index/index') ?>"><?= $this->t('Home') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
