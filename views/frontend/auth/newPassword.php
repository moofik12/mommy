<?php

use MommyCom\Controller\Frontend\AuthController;
use MommyCom\Model\Form\NewPasswordForm;

/**
 * @var $this AuthController
 * @var $form CActiveForm
 * @var  $formPassword NewPasswordForm
 */

$this->pageTitle = $this->t('Password recovery');
$app = $this->app();
?>

<div class="container">
    <?= CHtml::link($app->name, ['index/index', '#' => 'events'], ['class' => 'logo']) ?>
    <div class="login-conteiner">
        <div class="login-wrapper">
            <h2><?= $this->t('Password recovery') ?></h2>
            <div>
                <?php $form = $this->beginWidget('CActiveForm', [
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'validateOnSubmit' => true,
                    ],
                ]) ?>
                <?= $form->labelEx($formPassword, 'password') ?>
                <div class="field">
                    <?= $form->passwordField($formPassword, 'password', ['placeholder' => $this->t('New password')]) ?>
                    <?= $form->error($formPassword, 'password', ['class' => 'message']) ?>
                </div>

                <?= CHtml::submitButton($this->t('Save')) ?>
                <?php $this->endWidget() ?>
            </div>
        </div>
    </div>
</div>
