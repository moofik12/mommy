<div class="registration <?= $visible ? '' : 'hide' ?>" id="registration">
    <?php $form = $this->beginWidget('CActiveForm', [
        'action' => $this->app()->createUrl('auth/registration'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
        ],
    ]) ?>
    <?= $form->labelEx($registrationForm, 'name') ?>
    <div class="field">
        <?= $form->textField($registrationForm, 'name', ['placeholder' => $this->t('Your name') . ' *', 'autocomplete' => 'off']) ?>
        <?= $form->error($registrationForm, 'name', ['class' => 'message']) ?>
    </div>
    <?= $form->labelEx($registrationForm, 'email') ?>
    <div class="field">
        <?= $form->textField($registrationForm, 'email', ['placeholder' => $this->t('E-mail address') . ' *', 'autocomplete' => 'off']) ?>
        <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
    </div>
    <?= $form->labelEx($registrationForm, 'password') ?>
    <div class="field">
        <?= $form->passwordField($registrationForm, 'password', ['placeholder' => $this->t('Your password') . ' *', 'autocomplete' => 'off']) ?>
        <?= $form->error($registrationForm, 'password', ['class' => 'message']) ?>
    </div>
    <div class="policy" data-toggle="modal" data-target="#policy"><?= $this->t('privacy policy') ?></div>
    <?= CHtml::submitButton($this->t('Sign Up')) ?>
    <?php $this->endWidget() ?>
    <div class="help"><?= $this->t('* - Required fields') ?></div>
</div>
