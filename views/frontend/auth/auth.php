<?php

use MommyCom\Controller\Frontend\AuthController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;
use MommyCom\Model\Form\RecoveryPasswordForm;

/**
 * @var $this AuthController
 * @var $authForm AuthForm
 * @var $recoveryForm RecoveryPasswordForm
 * @var $registrationSocial NetworkRegistrationForm
 * @var $registrationSocialConfirm NetworkRegistrationConfirmForm
 * @var $registrationForm UserRecord
 * @var $form CActiveForm
 * @var $tab string
 * @var $social string
 */
$this->pageTitle = $this->t('Login Registration');
$app = $this->app();
?>

<div class="container login-page">
    <?= CHtml::link($app->name, ['index/index', '#' => 'events'], ['class' => 'logo']) ?>
    <div class="login-conteiner">
        <div class="tabs">
            <div class="tab <?= $tab === 'login' ? 'active' : '' ?>"
                 data-tab="enter"><?= $this->t('Enter the site') ?></div>
            <div class="tab <?= $tab === 'registration' ? 'active' : '' ?>"
                 data-tab="registration"><?= $this->t('check in') ?></div>
        </div>
        <div class="login-wrapper">
            <?php $this->renderPartial(
                '_login',
                [
                    'authForm' => $authForm,
                    'visible' => $tab === 'login',
                ]
            ) ?>

            <?php $this->renderPartial(
                '_registration',
                [
                    'registrationForm' => $registrationForm,
                    'visible' => $tab === 'registration',
                ]
            ) ?>

            <?php $this->renderPartial(
                '_forgot',
                [
                    'recoveryForm' => $recoveryForm,
                    'visible' => $tab === 'forgot',
                ]
            ) ?>
        </div>
    </div>
</div>

<div class="modals">
    <div class="modal hide policy-box" id="policy">
        <div class="modal-wrapper">
            <span class="close" data-dismiss="modal">x</span>
            <div class="modal-body">
                <p class="pc-title"><?= $this->t('privacy policy') ?></p>
                <p><?= $this->t('The Company operates on the principle of recognition of the information transmitted by the Customer, including personal data, confidential, and therefore does not allow the use of such information for purposes other than fulfillment of the contract of retail sale or other purposes determined by this privacy policy.') ?></p>
                <p><?= $this->t('When completing the Application on the website, the Client provides the following personal data:') ?></p>
                <ul>
                    <li><?= $this->t('Surname, First name;') ?></li>
                    <li><?= $this->t('E-mail address;') ?></li>
                    <li><?= $this->t('Contact phone number;') ?></li>
                    <li><?= $this->t('Delivery address of the products,') ?></li>
                </ul>
                <p><?= $this->t('expressly, explicitly and unconditionally give their consent to the processing of such data by the Company without a time limit, with a view to executing a contract of retail sale, informing the Client about the conditions of cooperation and promotion of goods on the market.') ?></p>
                <p><?= $this->t('The Company ensures the confidentiality of the client\'s personal data in accordance with the USA law regarding personal data, takes the necessary legal, organizational and technical measures to protect personal data from unauthorized or accidental access, destruction, modification, blocking, copying, distribution, as well as other illegal actions.') ?></p>
                <p><?= $this->t('The Customer agrees to provide the Company with their data for the purposes of executing the concluded contracts and the Privacy Policy and the Company\'s obligations under them to sell and deliver the Goods to the Customer. The Customer\'s consent is expressed in the indication of the information in the appropriate boxes when completing the Application. Consent to the processing of personal data may be withdrawn by sending a written notice to the Company specified in the Contacts.') ?></p>
                <p><?= $this->t('Within 7 days from the date of receipt of the notification, the available personal data will be destroyed and the processing terminated. The Customer agrees to the transfer by the Company of the information provided to the agents and third parties acting on the basis of the contract with the Company, in order to fulfill their obligations to the Customer.') ?></p>
                <p><?= $this->t('The Company is not responsible for the information provided by the Customer on the Site being used publicly.') ?></p>
                <p><?= $this->t('Being a user of the shopping club Mommy.COM, the user can receive periodic mailings (information, holidays and others) to the registered email address, as well as automatic confirmation of orders. Each user voluntarily chooses whether to receive it or not. We guarantee that your email address will not be used for spamming.') ?></p>
            </div>
        </div>
    </div>
</div>


<?php
//пока нет дизайна вывода сообщений через alert()
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;

if ($app->user->hasFlash('message')) {
    $cs->registerScript('alertMessage', '
        alert("' . $app->user->getFlash('message') . '");
    ', CClientScript::POS_LOAD);
}

if ($social === 'socialNotEmail') {
    $cs->registerScript('socialNotEmail', '$("#social-end").modal("show");');
}

if ($social === 'socialConfirm') {
    $cs->registerScript('socialConfirm', '$("#social-confirm").modal("show");');
}
?>
