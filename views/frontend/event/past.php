<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Widget\Frontend\MamamLinkPager;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $model EventRecord */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */
/* @var $filterProducts GroupedProductsFilter */
$this->pageTitle = $model->name;

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
?>
<section>
    <header class="content-header container">
        <div class="promotional">
            <div class="title-wrapper">
                <h1 class="title"><?= CHtml::encode($model->name) ?></h1>
                <?php if ($model->is_drop_shipping): ?>
                    <a class="icon-fast-delivery what-is-it" target="_blank"
                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                       title="<?= $this->t('What is \'Fast Delievery\'?') ?>"><span><?= $this->t('what is it?') ?></span></a>
                <?php endif; ?>
            </div>
            <div class="promotional-time">
                <span class="style"><?= CHtml::tag(
                        'time',
                        ['datetime' => $tf->formatMachine($model->end_at), 'data-countdown' => true],
                        $tf->format($model->end_at)
                    ) ?></span>
            </div>
        </div>
        <div class="promotional-info">
            <div class="info-text <?php if (!$model->isDescriptionLong) { ?> is-short <?php } ?> <?php if ($model->is_charity) { ?>charity-event<?php } ?>">
                <?= $model->description ?>
                <span class="info-hide"><b></b><?= $this->t('Hide information') ?></span>
            </div>
            <!--            --><?php //if($model->is_charity): ?>
            <!--                <a class="charity-link" href="-->
            <? //= $app->urlManager->createUrl('static/index', array('category' => 'company', 'page' => 'charity'))?><!--"></a>-->
            <!--            --><?php //endif ?>
        </div>
    </header>
    <div class="dotted"></div>
    <div class="container">
        <nav>
            <div class="goods-header clearfix">
                <?= $this->renderPartial('_filter', [
                    'groupedProducts' => $groupedProducts,
                    'filterProducts' => $filterProducts,
                ]) ?>
                <div class="goods-number">
                    <span class="style"><span
                                class="goods-number-value"><?= $provider->totalItemCount ?> <?= $this->t('denomination of titles', $provider->totalItemCount) ?></span></span>
                </div>
            </div>
            <div class="filter-bar"></div>
        </nav>
        <div class="content-body">
            <?= $this->renderPartial('_productsPast', [
                'provider' => $provider,
                'groupedProducts' => $groupedProducts,
                'showTimer' => false,
                'showEvent' => false,
                'enableItems' => true,
            ]) ?>
            <nav class="pagination">
                <?php $this->widget(MamamLinkPager::class, [
                    'pages' => $provider->pagination,
                    'selectedPageCssClass' => '',
                    'activeClassLi' => 'active',
                    'maxButtonCount' => 9,
                ]) ?>
            </nav>
        </div>
    </div>
</section>
