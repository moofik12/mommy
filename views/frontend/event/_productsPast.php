<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\LinkPager;

/* @var $this FrontController */
/* @var $productGroup GroupedProduct */
/* @var $showTimer boolean */
/* @var $showEvent boolean */
/* @var $enableItems boolean */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
$splitTesting = $app->splitTesting;
/* @var $splitTesting SplitTesting */

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
//$splitTestVariant = $splitTesting->getNum('goodslisteconomy', 2);
$events = EventRecord::model()
    ->isVirtual(false)
    ->onlyVisible()
    ->onlyPublished()
    ->onlyTimeActive()
    ->isDeleted(false)
    ->orderBy('end_at', 'desc')
    ->orderBy('priority', 'asc')
    ->cache(300)
    ->limit(3)
    ->findAll();
?>
<div class="action-end action-end-from-list">
    <div class="action-end-header">
        <p><?= $this->t('Promotion for this product has expired') ?><br/>
            <span class="average-text">...<?= $this->t('but we can offer') ?>
                <a href="<?= $this->createUrl('index/index') ?>"><?= $this->t('hundreds of similar products by') ?></a>
                <br/></span>
            <span class="big-text"><?= $this->t('the lowest prices!') ?></span>
        </p>
        <div class="close"></div>
    </div>
    <div class="action-end-content">
        <?php foreach ($events as $item): /* @var $item EventRecord */ ?>
            <div class="action-end-elem">
                <a href="<?= $app->createUrl('event/index', ['id' => $item->id]) ?>">
                    <div class="img">
                        <div class="discount">-<?= $item->promoDiscountPercent ?>%</div>
                        <?= CHtml::image($item->logo->getThumbnail('mid320')->url) ?>
                    </div>
                    <div class="desc">
                        <?= CHtml::encode($item->name) ?>
                    </div>
                    <div class="price"><?= $this->t('from') ?> <?= $cf->format($item->getMinSellingPrice()) ?></div>
                </a>
            </div>
        <?php endforeach ?>
        <a href="<?= $this->createUrl('index/index') ?>" class="action-end-next"></a>
    </div>
    <script type="text/javascript">
        $(function () {
            $(".action-end .close").click(function () {
                $(".action-end").hide();
            });
        });
    </script>
</div>
<?php if ($groupedProducts->count == 0): ?>
    <div class="list-no-items">
        <p><?= $this->t('There are no products available according to your search.') ?></p>
        <p><?= $this->t('Try reducing the number of filters') ?></p>
    </div>
<?php endif; ?>
<div class="goods-list old-price-larger indent-price">
    <?php foreach ($groupedProducts->toArray() as $item): /* @var $item GroupedProduct */ ?>
    <div class="goods-item form-in" itemscope itemtype="http://schema.org/Product">
        <div class="form-in-wrap">
            <div class="form-in-wrap-content">
                <?php if ($enableItems): ?>
                <a class="product-link-details"
                   data-product-id="<?= $item->event->id ?>/<?= $item->product->id ?>"
                   href="<?= $app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']) ?>">
                    <?php else: ?>
                    <div class="box-wrapper">
                        <?php endif ?>

                        <?php if ($item->isSoldOut): ?>
                            <div class="label-sold">×</div>
                        <?php else: ?>
                            <div class="label-discount">-<?= round((1 - $item->price / $item->priceOld) * 100) ?>%</div>
                            <?php if ($item->isLimited): ?>
                                <!--<div class="label-ends">!</div>-->
                            <?php endif ?>
                            <?php if ($item->isTopOfSelling): ?>
                                <div class="label-hit"><?= $this->t('HIT') ?></div>
                            <?php endif ?>
                        <?php endif ?>
                        <div class="box">
                            <div class="image-wrapper">
                                <?= CHtml::image(
                                    $app->baseUrl . '/static/blank.gif', '', [
                                        'data-src' => $item->product->logo->getThumbnail('mid320_prodlist')->url,
                                        'data-lazyload' => 'true',
                                        'itemprop' => "image",
                                        'style' => 'margin-top: -50px',
                                    ]
                                ) ?>
                            </div>
                            <div class="text-wrapper">
                                <?php if ($showEvent): ?>
                                    <p class="category-title"><?= CHtml::encode($item->event->name) ?></p>
                                <?php endif ?>
                                <p itemprop="name" class="title"><?= CHtml::encode($item->product->name) ?></p>
                                <div class="price clearfix">
                                    <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                    <?php if ($item->priceOld != $item->price): ?>
                                        <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                    <?php endif ?>
                                </div>
                                <?php if (false/*$splitTestVariant*/) : ?>
                                    <span class="economy-string"><?= $this->t('Saving') ?>
                                        <span><?= $item->priceOld - $item->price ?></span> <?= $cf->getCurrency() ?></span>
                                <?php endif; ?>
                                <?php if ($showTimer): ?>
                                    <div class="time"><?= CHtml::tag(
                                            'time',
                                            ['datetime' => $tf->formatMachine($item->event->end_at), 'data-countdown' => true],
                                            $tf->format($item->event->end_at)
                                        ) ?></div>
                                <?php endif ?>
                            </div>
                        </div>

                        <?php if ($enableItems): ?>
                </a>
                <?php else: ?>
            </div>
            <?php endif ?>
            <div class="product-info-wrapper">
                <div class="available-info">
                    <?php if ($enableItems && !$item->isSoldOut && $item->sizeCount > 0): ?>
                        <label class="available-info-sizes"><?= $this->t('Dimensions') ?>: </label>
                        <?= $item->sizes ? implode(', ', $item->sizes) : '-' ?>
                    <?php endif; ?>
                </div>
                <div class="product-form">
                    <?php if ($enableItems && !$item->isSoldOut): ?>
                        <?= $this->renderPartial('//layouts/_productJson', ['productGroup' => $item]) ?>
                        <form>
                            <input type="hidden" name="token"
                                   value="<?= $tokenManager->getToken($item->product->id) ?>">
                            <input type="hidden" name="eventId" value="<?= $item->event->id ?>">
                            <input type="hidden" name="productId" value="<?= $item->product->id ?>">
                            <input type="hidden" name="number" value="1">
                            <?php if ($item->sizeCount > 0): ?>
                                <select name="size">
                                    <?php foreach ($item->sizes as $size): ?>
                                        <option value="<?= $size ?>"><?= $size ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                            <button class="in-cart btn green disabled" disabled="disabled"><i
                                        class="icon-in-cart-open"></i><?= $this->t('Add to cart') ?></button>
                        </form>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
</div>
<?php if ($provider->pagination->pageCount > 1) : ?>
    <nav class="pagination hidden">
        <?php $this->widget(LinkPager::class, [
            'pages' => $provider->pagination,
            'maxButtonCount' => 3,
            'cssFile' => false,
            'selectedPageCssClass' => 'active',
            'isShowFirstPage' => true,
            'isShowLastPage' => true,
            'prevPageLabel' => '...',
            'nextPageLabel' => '...',
        ]); ?>
    </nav>
<?php endif; ?>
