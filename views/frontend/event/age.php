<?php

use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Utf8;

/* @var $this FrontController */
/* @var $targets string */
/* @var $ageRange array */
/* @var $age string */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */
/* @var $filterProducts GroupedProductsFilter */

$productTargets = ProductTargets::instance();
$ageGroups = AgeGroups::instance();

$pageLabel = Utf8::ucfirst($productTargets->getLabel($target)) . ' ' . $ageGroups->getLabel($age);
$this->pageTitle = $this->t($pageLabel);
$this->description = $this->t('Goods {pageLabel} - MOMMY.COM The first shopping club in USA for mothers and children. Products for the whole family and for your home. Daily discounts of up to 90%.', ['{pageLabel}' => $pageLabel]);

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
?>
<section>
    <header class="page-header">
        <div class="page-title promotional">
            <div class="category-icon <?= $target ?>"></div>
            <h1 class="title"><?= Utf8::ucfirst($productTargets->getLabel($target)) ?> <?= $ageGroups->getLabel($age) ?></h1>
        </div>
    </header>
    <div class="container">
        <nav>
            <div class="goods-header clearfix">
                <?= $this->renderPartial('_filter', [
                    'groupedProducts' => $groupedProducts,
                    'filterProducts' => $filterProducts,
                    'showAges' => false,
                    'showTargets' => false,
                ]) ?>
                <div class="goods-number">
                    <span class="style"><span
                                class="goods-number-value"><?= $provider->totalItemCount ?> <?= $this->t('denomination of titles', $provider->totalItemCount) ?></span></span>
                </div>
            </div>
            <div class="filter-bar"></div>
        </nav>
        <div class="content-body">
            <?= $this->renderPartial('_products', [
                'provider' => $provider,
                'groupedProducts' => $groupedProducts,
                'showTimer' => true,
                'showEvent' => true,
                'enableItems' => true,
            ]) ?>
        </div>
    </div>
</section>
