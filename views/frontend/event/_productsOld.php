<?php

use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\LinkPager;

/* @var $this FrontController */
/* @var $productGroup GroupedProduct */
/* @var $showTimer boolean */
/* @var $showEvent boolean */
/* @var $enableItems boolean */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
$splitTesting = $app->splitTesting;
/* @var $splitTesting SplitTesting */

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$splitTesting = $this->app()->splitTesting;
?>
<?php if ($groupedProducts->count == 0): ?>
    <div class="list-no-items">
        <p><?= $this->t('There are no products available according to your search.') ?></p>
        <p><?= $this->t('Try reducing the number of filters') ?></p>
    </div>
<?php endif; ?>
<ul class="goods-list old-price-larger indent-price">
    <?php foreach ($groupedProducts->toArray() as $item): /* @var $item GroupedProduct */ ?>
        <li>
            <?php if ($enableItems): ?>
            <a href="<?= $app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']) ?>">
                <?php else: ?>
                <span class="box-wrapper">
            <?php endif ?>

                    <?php if ($item->isSoldOut): ?>
                        <span class="label-sold">×</span>
                    <?php else: ?>
                        <div class="label-discount">-<?= round((1 - $item->price / $item->priceOld) * 100) ?>%</div>
                        <?php if ($item->isLimited): ?>
                            <!--<span class="label-ends">!</span>-->
                        <?php endif ?>
                    <?php if ($item->isTopOfSelling): ?>
                            <span class="label-hit"><?= $this->t('HIT') ?></span>
                        <?php endif ?>
                    <?php endif ?>
                    <span class="box">
                <span class="image-wrapper">
                    <?= CHtml::image(
                        $app->baseUrl . '/static/blank.gif', '', [
                            'data-src' => $item->product->logo->getThumbnail('mid320_prodlist')->url,
                            'data-lazyload' => 'true',
                            'style' => 'margin-top: -50px',
                        ]
                    ) ?>
                </span>
                <span class="text-wrapper">
                    <?php if ($showEvent): ?>
                        <span class="category-title"><?= CHtml::encode($item->event->name) ?></span>
                    <?php endif ?>
                    <span class="title"><?= CHtml::encode($item->product->name) ?></span>
                    <span class="price">
                        <span class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></span>
                        <?php if ($item->priceOld != $item->price): ?>
                            <span class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></span>
                        <?php endif ?>
                    </span>
                    <?php if ($showTimer): ?>
                        <span class="time"><?= CHtml::tag(
                                'time',
                                ['datetime' => $tf->formatMachine($item->event->end_at), 'data-countdown' => true],
                                $tf->format($item->event->end_at)
                            ) ?></span>
                    <?php endif ?>
                </span>
            </span>

                    <?php if ($enableItems): ?>
            </a>
        <?php else: ?>
            </span>
        <?php endif ?>

            <?php if ($enableItems && !$item->isSoldOut): ?>
                <form class="size-bar">
                    <input type="hidden" name="token" value="<?= $tokenManager->getToken($item->product->id) ?>">
                    <input type="hidden" name="eventId" value="<?= $item->event->id ?>">
                    <input type="hidden" name="productId" value="<?= $item->product->id ?>">
                    <input type="hidden" name="number" value="1">

                    <?php if ($item->sizeCount > 0): ?>
                        <span class="size-title"><?= $this->t('Choose a size') ?></span>
                        <ul>
                            <?php foreach ($item->sizes as $size): ?>
                                <?php $sizeId = CHtml::getIdByName($item->product->id . '_' . $size) ?>
                                <li>
                                    <input type="radio" name="size" id="radio-<?= $sizeId ?>" value="<?= $size ?>">
                                    <label for="radio-<?= $sizeId ?>"><?= $size ?></label>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <button class="in-cart btn green" disabled="disabled"><i
                                    class="icon-in-cart"></i><?= $this->t('Add to cart') ?></button>
                        <a class="detail" href="<?= $this->createUrl('splitTesting/productViewDetailActivate',
                            ['redirectUrl' => urlencode($app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']))]) ?>"><?= $this->t('Learn More') ?></a>
                    <?php else: ?>
                        <button class="in-cart btn green"><i
                                    class="icon-in-cart"></i><?= $this->t('Add to cart') ?></button>
                        <a class="detail" href="<?= $this->createUrl('splitTesting/productViewDetailActivate',
                            ['redirectUrl' => urlencode($app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']))]) ?>"><?= $this->t('Learn More') ?></a>
                    <?php endif ?>
                </form>
            <?php endif ?>
        </li>
    <?php endforeach ?>
</ul>
<?php if ($provider->pagination->pageCount > 1) : ?>
    <nav class="pagination hidden">
        <?php $this->widget(LinkPager::class, [
            'pages' => $provider->pagination,
            'maxButtonCount' => 3,
            'cssFile' => false,
            'selectedPageCssClass' => 'active',
            'isShowFirstPage' => true,
            'isShowLastPage' => true,
            'prevPageLabel' => '...',
            'nextPageLabel' => '...',
        ]); ?>
    </nav>
<?php endif; ?>
