<?php

use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */
$this->pageTitle = $this->t('Final sale');

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
?>
<div class="page-header">
    <div class="container page-title">
        <span class="search-title">
            <span class="styled"><?= $this->t('Final sale') ?></span>
        </span>
    </div>
</div>
<div class="container">
    <div class="content-header">
        <?= $this->renderPartial('_filter', [
            'groupedProducts' => $groupedProducts,
        ]) ?>
        <div class="goods-number">
            <span class="style"><span
                        class="goods-number-value"><?= $provider->totalItemCount ?> <?= $this->t('denomination of titles', $provider->totalItemCount) ?></span></span>
        </div>
    </div>
    <div class="filter-bar"></div>
    <div class="content-body">
        <?= $this->renderPartial('_products', [
            'provider' => $provider,
            'groupedProducts' => $groupedProducts,
            'showTimer' => false,
            'showEvent' => false,
            'enableItems' => true,
        ]) ?>
    </div>
</div>
