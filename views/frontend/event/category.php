<?php

use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\Utf8;

/* @var $this FrontController */
/* @var $category string */
/* @var $targets string[] */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */
/* @var $filterProducts GroupedProductsFilter */
/* @var $subCategories array */
$categoryGroups = CategoryGroups::instance();

$pageLabel = Utf8::ucfirst($categoryGroups->getLabel($category));
$this->pageTitle = $pageLabel;

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
?>
<section>
    <header class="page-header">
        <div class="page-title promotional">
            <?php if ($targets !== false): ?>
                <?php foreach ($targets as $target): ?>
                    <div class="category-icon <?= $target ?>"></div>
                <?php endforeach ?>
            <?php endif ?>
            <h1 class="title"><?= $pageLabel ?></h1>
        </div>
    </header>
    <div class="container">
        <nav>
            <div class="goods-header clearfix">
                <?= $this->renderPartial('_filter', [
                    'groupedProducts' => $groupedProducts,
                    'filterProducts' => $filterProducts,
                    'showTargets' => false,
                ]) ?>
                <div class="goods-number">
                    <span class="style"><span
                                class="goods-number-value"><?= $provider->totalItemCount ?> <?= $this->t('denomination of titles', $provider->totalItemCount) ?></span></span>
                </div>
            </div>
            <div class="filter-bar"></div>
        </nav>
        <div class="content-body">
            <?= $this->renderPartial('_products', [
                'provider' => $provider,
                'groupedProducts' => $groupedProducts,
                'showTimer' => true,
                'showEvent' => true,
                'enableItems' => true,
            ]) ?>
        </div>
    </div>
</section>
