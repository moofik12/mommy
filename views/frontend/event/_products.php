<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;
use MommyCom\YiiComponent\LinkPager;

/* @var $this FrontController */
/* @var $productGroup GroupedProduct */
/* @var $showTimer boolean */
/* @var $showEvent boolean */
/* @var $enableItems boolean */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider */

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
$splitTesting = $app->splitTesting;
/* @var $splitTesting SplitTesting */

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
//$splitTestVariant = $splitTesting->getNum('goodslisteconomy', 2);
?>
<?php if ($groupedProducts->count == 0): ?>
    <div class="list-no-items">
        <p><?= $this->t('There are no products available according to your search.') ?></p>
        <p><?= $this->t('Try reducing the number of filters') ?></p>
    </div>
<?php endif; ?>
<div class="goods-list">
    <?php foreach ($groupedProducts->toArray() as $item): /* @var $item GroupedProduct */ ?>
    <div class="goods-item form-in <?= $showEvent ? '' : 'category-off' ?>" itemscope
         itemtype="http://schema.org/Product">
        <div class="form-in-wrap">
            <div class="form-in-wrap-content">
                <?php if ($enableItems): ?>
                <a class="product-link-details"
                   data-product-id="<?= $item->event->id ?>/<?= $item->product->id ?>"
                   href="<?= $app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']) ?>">
                    <?php else: ?>
                    <div class="box-wrapper">
                        <?php endif ?>

                        <?php if ($item->isSoldOut): ?>
                            <div class="label-sold">×</div>
                        <?php else: ?>
                            <div class="label-discount">-<?= round((1 - $item->price / $item->priceOld) * 100) ?>%</div>
                            <?php if ($item->isLimited): ?>
                                <!--<div class="label-ends">!</div>-->
                            <?php endif ?>
                            <?php if ($item->isTopOfSelling): ?>
                                <div class="label-hit"><?= $this->t('HIT') ?></div>
                            <?php endif ?>
                        <?php endif ?>
                        <div class="box">
                            <div class="image-wrapper">
                                <?= CHtml::image(
                                    $app->baseUrl . '/static/blank.gif', '', [
                                        'data-src' => $item->product->logo->getThumbnail('mid320_prodlist')->url,
                                        'data-lazyload' => 'true',
                                        'itemprop' => "image",
                                        'style' => 'margin-top: -50px',
                                    ]
                                ) ?>
                            </div>
                            <div class="text-wrapper">
                                <?php if ($showEvent): ?>
                                    <p class="category-title"><?= CHtml::encode($item->event->name) ?></p>
                                <?php endif ?>
                                <p itemprop="name" class="title"><?= CHtml::encode($item->product->name) ?></p>
                                <div class="price clearfix">
                                    <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                    <?php if ($item->priceOld != $item->price): ?>
                                        <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                    <?php endif ?>
                                </div>
                                <?php if (false/*$splitTestVariant*/) : ?>
                                    <span class="economy-string"><?= $this->t('Saving') ?>
                                        <span><?= $item->priceOld - $item->price ?></span> <?= $cf->getCurrency() ?></span>
                                <?php endif; ?>
                                <?php if ($showTimer): ?>
                                    <div class="time"><?= CHtml::tag(
                                            'time',
                                            ['datetime' => $tf->formatMachine($item->event->end_at), 'data-countdown' => true],
                                            $tf->format($item->event->end_at)
                                        ) ?></div>
                                <?php endif ?>
                            </div>
                        </div>

                        <?php if ($enableItems): ?>
                </a>
                <?php else: ?>
            </div>
            <?php endif ?>
            <div class="product-info-wrapper">
                <div class="available-info">
                    <?php if ($enableItems && !$item->isSoldOut && $item->sizeCount > 0): ?>
                        <label class="available-info-sizes"><?= $this->t('Dimensions') ?>: </label>
                        <?php if ($item->sizeCount > 0): ?>
                            <?php for ($i = 0; $i < count($item->sizes); $i++): ?>
                                <?php
                                echo EventProductRecord::replaceLangAgeSize($item->sizes[$i]);
                                if ($i !== count($item->sizes) - 1) {
                                    echo ', ';
                                }
                                ?>
                            <?php endfor; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="product-form <?= $item->isSoldOut ? 'unavailable' :'' ?>">
                    <?php if ($enableItems && !$item->isSoldOut): ?>
                        <?= $this->renderPartial('//layouts/_productJson', ['productGroup' => $item]) ?>
                        <form>
                            <input type="hidden" name="token"
                                   value="<?= $tokenManager->getToken($item->product->id) ?>">
                            <input type="hidden" name="eventId" value="<?= $item->event->id ?>">
                            <input type="hidden" name="productId" value="<?= $item->product->id ?>">
                            <input type="hidden" name="number" value="1">
                            <?php if ($item->sizeCount > 0): ?>
                                <select name="size">
                                    <?php foreach ($item->sizes as $size): ?>
                                        <option value="<?= $size ?>"><?= EventProductRecord::replaceLangAgeSize($size) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                            <button class="in-cart btn green"><i
                                        class="icon-in-cart-open"></i><?= $this->t('Add to cart') ?></button>
                        </form>
                    <?php else: ?>
                        <p><?= $this->t('Product is sold out') ?></p>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
</div>
<?php if ($provider->pagination->pageCount > 1) : ?>
    <nav class="pagination hidden">
        <?php $this->widget(LinkPager::class, [
            'pages' => $provider->pagination,
            'maxButtonCount' => 3,
            'cssFile' => false,
            'selectedPageCssClass' => 'active',
            'isShowFirstPage' => true,
            'isShowLastPage' => true,
            'prevPageLabel' => '...',
            'nextPageLabel' => '...',
        ]); ?>
    </nav>
<?php endif; ?>
