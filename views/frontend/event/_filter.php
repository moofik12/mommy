<?php

use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $groupedProducts GroupedProducts */
/* @var $filterProducts GroupedProductsFilter */
$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;

$showBrands = isset($showBrands) ? $showBrands : true;
$showSizes = isset($showSizes) ? $showSizes : true;
$showColors = isset($showColors) ? $showColors : true;
$showTargets = isset($showTargets) ? $showTargets : true;
$showAges = isset($showAges) ? $showAges : true;
?>

<ul class="goods-filter">
    <?php if ($filterProducts->getCountEventProducts() > 1): ?>
        <li class="dropdown">
            <span class="filter-item" role="button" data-toggle="dropdown"><?= $this->t('Sort by price') ?>
                <b></b></span>
            <div class="dropdown-menu filter-menu">
                <form>
                    <ul class="filter-price">
                        <li><input type="radio" name="sort" value="price_asc" id="sort-price-asc"><label
                                    for="sort-price-asc"><?= $this->t('Price low to high') ?></label></li>
                        <li><input type="radio" name="sort" value="price_desc" id="sort-price-desc"><label
                                    for="sort-price-desc"><?= $this->t('Price high to low') ?></label></li>
                        <li><input type="radio" name="sort" value="" checked id="sort-price-none"><label
                                    for="sort-price-none"><?= $this->t('Recommended') ?></label></li>
                    </ul>
                </form>
            </div>
        </li>
    <?php endif ?>
    <?php if ($showBrands && count($brands = $filterProducts->getBrands()) > 1): ?>
        <li class="dropdown">
            <span class="filter-item" role="button" data-toggle="dropdown"><?= $this->t('Brand') ?>
                <b></b></span>
            <div class="dropdown-menu filter-menu">
                <form>
                    <ul class="filter-price filter-scrollbar">
                        <?php foreach ($brands as $brand): ?>
                            <?php $brandId = CHtml::getIdByName($brand->name) ?>
                            <li><input type="checkbox" name="brand" value="<?= $brand->id ?>"
                                       id="brand-<?= $brandId ?>"><label
                                        for="brand-<?= $brandId ?>"><?= $brand->name ?></label></li>
                        <?php endforeach ?>
                    </ul>
                    <a class="filter-none" href="#"><?= $this->t('Reset filter') ?></a>
                </form>
            </div>
        </li>
    <?php endif ?>
    <?php if ($showSizes && count($sizes = $filterProducts->getSizes()) > 1): ?>
        <li class="dropdown">
            <span class="filter-item" role="button" data-toggle="dropdown"><?= $this->t('Show dimensions') ?>
                <b></b></span>
            <div class="dropdown-menu filter-menu  filter-wrapper">
                <form>
                    <ul class="filter-size filter-scrollbar">
                        <?php foreach ($sizes as $size): ?>
                            <?php $sizeId = CHtml::getIdByName($size) ?>
                            <li><input type="checkbox" name="size" value="<?= $size ?>"
                                       id="checkbox-<?= $sizeId ?>"><label
                                        for="checkbox-<?= $sizeId ?>"><span><?= $size ?></span></label></li>
                        <?php endforeach ?>
                    </ul>
                    <a class="filter-none" href="#"><?= $this->t('Reset filter') ?></a>
                </form>
            </div>
        </li>
    <?php endif ?>
    <?php if ($showColors && count($colors = $filterProducts->getColors()) > 1): ?>
        <li class="dropdown">
            <span class="filter-item" role="button" data-toggle="dropdown"><?= $this->t('Colour') ?>
                <b></b></span>
            <div class="dropdown-menu filter-menu">
                <form>
                    <ul class="filter-price filter-scrollbar">
                        <?php foreach ($colors as $color): ?>
                            <?php $colorId = CHtml::getIdByName($color) ?>
                            <li><input type="checkbox" name="color" value="<?= $color ?>"
                                       id="color-<?= $colorId ?>"><label
                                        for="color-<?= $colorId ?>"><?= $color ?></label></li>
                        <?php endforeach ?>
                    </ul>
                    <a class="filter-none" href="#"><?= $this->t('Reset filter') ?></a>
                </form>
            </div>
        </li>
    <?php endif ?>
    <?php if ($showAges && count($ageGroups = $filterProducts->getAgeGroups()) > 1): ?>
        <li class="dropdown">
            <span class="filter-item" role="button" data-toggle="dropdown"><?= $this->t('Age') ?><b></b></span>
            <div class="dropdown-menu filter-menu">
                <form>
                    <ul class="filter-price">
                        <?php foreach ($ageGroups as $ageGroup): ?>
                            <?php $ageGroupId = CHtml::getIdByName($ageGroup) ?>
                            <li><input type="radio" name="ageGroup" value="<?= $ageGroup ?>"
                                       id="ageGroup-<?= $ageGroupId ?>"><label
                                        for="ageGroup-<?= $ageGroupId ?>"><?= AgeGroups::instance()->getLabel($ageGroup) ?></label>
                            </li>
                        <?php endforeach ?>
                        <li><input type="radio" name="ageGroup" value="" id="ageGroup-filternone"><label
                                    for="ageGroup-filternone"><?= $this->t('Reset filter') ?></label></li>
                    </ul>
                </form>
            </div>
        </li>
    <?php endif ?>
    <?php if ($showTargets && count($targets = $filterProducts->getTargets()) > 1): ?>
        <li class="dropdown">
            <span class="filter-item" role="button" data-toggle="dropdown"><?= $this->t('For whom') ?>
                <b></b></span>
            <div class="dropdown-menu filter-menu">
                <form>
                    <ul class="filter-price">
                        <?php foreach ($targets as $target): ?>
                            <?php $targetId = CHtml::getIdByName($target) ?>
                            <li><input type="checkbox" name="target" value="<?= $target ?>"
                                       id="target-<?= $targetId ?>"><label
                                        for="target-<?= $targetId ?>"><?= ProductTargets::instance()->getLabel($target) ?></label>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <a class="filter-none" href="#"><?= $this->t('Reset filter') ?></a>
                </form>
            </div>
        </li>
    <?php endif ?>
</ul>
