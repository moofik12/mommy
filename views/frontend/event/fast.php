<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $event EventRecord */
/* @var $futureEvent EventRecord */
/* @var $provider CArrayDataProvider */
/* @var $futureProvider CArrayDataProvider */
$this->pageTitle = $this->t('Instant sale');

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
?>
<div class="super-offer container">
    <div class="inner option<?= $event->id % 7 + 1 ?>">
        <div class="inner-wrapper">
            <div class="img-block"></div>
            <div class="content-block">
                <div class="super-offer-header"><?= $this->t('INSTANT SALE') ?></div>
                <div class="timer-block" data-datetime="<?= $tf->formatMachine($event->end_at) ?>">
                    <div class="timer-header"><?= $this->t('time left until the end of the promotion:') ?>:
                    </div>
                    <div class="timer-inner">
                        <ul class="time">
                            <li class="m"></li>
                            <li class="separator"></li>
                            <li class="s"></li>
                        </ul>
                        <div class="time-text">
                            <div class="left"><?= $this->t('minutes') ?></div>
                            <div class="right"><?= $this->t('seconds') ?></div>
                        </div>
                    </div>
                </div>
                <div class="super-offer-content">
                    <?= $this->t('In the shopping club mommy.com there are always low prices. But now they are even lower than usual!') ?>
                    <?= $this->t('Over the next 15 minutes you have the unique opportunity to buy your favorite products for GREAT PRICES in USA!') ?>
                    <?= $this->t('Do not miss your chance to save! Catch the moment!') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="content-body">
        <?= $this->renderPartial('_products', [
            'provider' => $provider,
            'groupedProducts' => $groupedProducts,
            'showTimer' => false,
            'showEvent' => false,
            'enableItems' => true,
        ]) ?>
    </div>
</div>
<div class="lined-header other-lined-header">
    <span class="title"><?= $this->t('They will start soon') ?></span>
    <div class="solid"></div>
</div>
<div class="container other-goods-list">
    <div class="content-body">
        <?= $this->renderPartial('_products', [
            'provider' => $futureProvider,
            'showTimer' => false,
            'showEvent' => false,
            'enableItems' => false,
        ]) ?>
    </div>
</div>
