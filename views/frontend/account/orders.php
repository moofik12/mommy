<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\Frontend\FrontController;

/* @var $this FrontController */
/* @var $provider CDataProvider */
$this->pageTitle = $this->t('My orders');
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
?>


<div class="page-header">
    <div class="container page-title"><?= $this->t('My account') ?></div>
</div>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>

        <div class="content-place">
            <div class="styled-title"><?= $this->t('My orders') ?></div>
            <div class="white-wrapper">
                <table class="account-table orders-table">
                    <thead>
                    <tr>
                        <th class="order-number"><?= $this->t('Order number') ?></th>
                        <th class="order-date"><?= $this->t('order date') ?></th>
                        <th class="order-price"><?= $this->t('Cost') ?></th>
                        <th class="order-status"><?= $this->t('Status') ?></th>
                        <th class="order-pay"><?= $this->t('Pay now') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($provider->getData() as $item): /* @var $item OrderRecord */ ?>
                        <tr>
                            <td class="order-number">
                                    <span class="opener">
                                        <span class="styled"><?= $item->idAligned ?></span>
                                    </span>
                                <?php if ($item->is_drop_shipping): ?>
                                    <a class="icon-fast-delivery" target="_blank"
                                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                                       title="<?= $this->t('What is \'Fast Delievery\'?') ?>"></a>
                                <?php endif; ?>
                            </td>
                            <td class="order-date"><span
                                        class=""><?= $df->format('d MMMM yyyy', $item->ordered_at) ?></span></td>
                            <td class="order-price"><span class="order-price-td">
                                    <?= $cf->format($item->getPrice(false, true), $cn->getName('short')) ?></span>
                                <?php if ($item->card_payed > 0): ?>
                                    <span class="order-paid">(<span
                                                title="<?= $this->t('You have already paid') ?>"><?= $cf->format($item->card_payed, $cn->getName('short')) ?></span>)</span>
                                <?php endif ?>
                            </td>

                            <td class="order-status">
                                    <span class="orders-status">
                                        <?php if ($item->processing_status == OrderRecord::PROCESSING_UNMODERATED): ?>
                                            <i class="status-icon processed"></i><?= $this->t('Wait for the call') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_RECALL): ?>
                                            <i class="status-icon processed"></i><?= $this->t('Recall') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_CALL_LATER): ?>
                                            <i class="status-icon processed"></i><?= $this->t('Recall') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_PREPAY): ?>
                                            <i class="status-icon confirmed"></i><?= $this->t('Waiting for payment') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_CONFIRMED): ?>
                                            <i class="status-icon confirmed"></i><?= $this->t('Confirmed, expect delivery') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED): ?>
                                            <i class="status-icon confirmed"></i><?= $this->t('Confirmed, expect delivery') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_STOREKEEPER_PACKAGED): ?>
                                            <i class="status-icon assembled"></i><?= $this->t('Packed, will soon ship') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED): ?>
                                            <i class="status-icon posted"></i><?= $this->t('Sent') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CANCELLED
                                            || $item->processing_status == OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE): ?>
                                            <i class="status-icon canceled"></i><?= $this->t('Canceled') ?>
                                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER): ?>
                                            <i class="status-icon canceled"></i><?= $this->t('Unverified and canceled') ?>
                                        <?php endif ?>
                                    </span>
                                <?php if ($item->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED && $item->trackcode): ?>
                                    <span class="orders-trackcode">
                                            <?= $this->t('Consignment note') ?> <?= CHtml::encode($item->trackcode) ?>
                                        </span>
                                <?php endif; ?>
                            </td>

                            <td>
                                <?php if ($item->isAvailableForPayment()) : ?>
                                    <?php
                                    $countRelationOrders = count($item->getRelationOrders());
                                    $linkToPay = $app->createUrl('pay/index', ['uid' => $item->id]);
                                    $linkToPayOrders = $app->createUrl('pay/index', ['uid' => $item->uuid]);
                                    ?>
                                    <a class="extend-btn"
                                       href="<?= $countRelationOrders == 0 ? $linkToPay : $linkToPayOrders ?>"><?= $this->t('pay') ?></a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <tr class="open hide">
                            <td colspan="5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td colspan="5">
                                            <ul class="order-items">
                                                <?php foreach ($item->positions as $position): ?>
                                                    <li>
                                                        <div class="item">
                                                            <div class="title-line">
                                                                <?= CHtml::link(CHtml::encode($position->product->product->name), [
                                                                    'product/index',
                                                                    'id' => $position->product->product_id,
                                                                    'eventId' => $position->event_id,
                                                                ], [
                                                                    'class' => 'title top-title',
                                                                ]) ?>
                                                                <?= CHtml::link(
                                                                    CHtml::image($position->product->product->logo->getThumbnail('small60')->url),
                                                                    [
                                                                        'product/index',
                                                                        'id' => $position->product->product_id,
                                                                        'eventId' => $position->event_id,
                                                                    ],
                                                                    ['class' => 'title']
                                                                ) ?>
                                                                <span class="number-line">x<span
                                                                            class="number"><?= $position->number ?></span></span>
                                                            </div>
                                                            <?php if (!empty($position->product->size)): ?>
                                                                <span class="size-line"><?= $this->t('Size') ?>: <span
                                                                            class="size"><?= $position->product->size ?></span></span>
                                                            <?php endif ?>
                                                            <span class="foot-line">
                                                                        <span class="price"><?= $cf->format($position->product->price, $cn->getName('short')) ?></span>
                                                                    </span>
                                                            <span class="foot-line">
                                                                        <span class="item-status"><?= $position->getVirtualStatusText() ?></span>
                                                                    </span>
                                                        </div>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?= $this->renderPartial('_scripts') ?>

