<?php

use MommyCom\Controller\Frontend\AccountController;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $this AccountController
 */
$app = $this->app();
$isPartner = UserPartnerRecord::model()->userId($this->app()->user->id)->find() !== null ? true : false;
if ($isPartner) {
    $this->menu[] = ['label' => $this->t('Affiliate cabinet'), 'url' => ['partnerOffice/report']];
} else {
    $this->menu[] = [
        'label' => $this->t('Affiliate Program'),
        'url' => $app->createUrl('partner/landing', ['source' => 'myacc']),
        'linkOptions' => [
            'target' => '_blank',
        ],
    ];
}
?>
<div class="content-menu">
    <?php $this->widget('zii.widgets.CMenu', [
        'items' => $this->menu,
    ]); ?>
</div>
