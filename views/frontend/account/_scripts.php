<?php
/**
 * пока нет дизайна вывода сообщений через alert()
 * для совместимости регистрации на главной странице
 */
$app = $this->app();

if ($app->user->hasFlash('message')) {
    $app->clientScript->registerScript('alertMessage', '
        alert("' . $app->user->getFlash('message') . '");
    ', CClientScript::POS_READY);
}
