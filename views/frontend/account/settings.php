<?php

use MommyCom\Controller\Frontend\AccountController;
use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Form\UserSettingsForm;

/**
 * @var $form UserSettingsForm
 * @var $this AccountController
 * @var $actionId string
 * @var $actionForm CActiveForm
 */

$this->pageTitle = $this->t('account settings');
?>
<div class="page-header">
    <div class="container page-title"><?= $this->t('My account') ?></div>
</div>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>

        <div class="content-place">
            <?php $actionForm = $this->beginWidget('CActiveForm', [
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'clientOptions' => [
                    'validateOnSubmit' => true,
                ],
            ]) ?>
            <div class="styled-title"><?= $this->t('account settings') ?></div>
            <div class="white-place">
                <div class="standard-block">
                    <div class="standard-title">
                        <label class="left-part"><?= $this->t('Change Password') ?></label>
                    </div>
                    <div class="standard-wrapper">
                        <div class="left-part">
                            <label class="some-love-for-ie"><?= $this->t('Current password') ?></label>
                            <div class="field bottom <?= $form->hasErrors('password') ? 'error' : '' ?>">
                                <?= $actionForm->passwordField($form, 'password', ['placeholder' => $this->t('Current password')]) ?>
                                <?= $actionForm->error($form, 'password', ['class' => 'message']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="standard-wrapper">
                        <div class="left-part">
                            <label class="some-love-for-ie"><?= $this->t('New password') ?></label>
                            <div class="field bottom <?= $form->hasErrors('newPassword') ? 'error' : '' ?>">
                                <?= $actionForm->passwordField($form, 'newPassword', ['placeholder' => $this->t('New password')]) ?>
                                <?= $actionForm->error($form, 'newPassword', ['class' => 'message']) ?>
                            </div>
                        </div>
                        <div class="right-part">
                            <label class="some-love-for-ie"><?= $this->t('Confirm New Password') ?></label>
                            <div class="field bottom <?= $form->hasErrors('confirmPassword') ? 'error' : '' ?>">
                                <?= $actionForm->passwordField($form, 'confirmPassword', ['placeholder' => $this->t('Confirm New Password')]) ?>
                                <?= $actionForm->error($form, 'confirmPassword', ['class' => 'message']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="standard-block" id="mailing">
                    <div class="standard-title">
                        <label class="left-part"><?= $this->t('Newsletter') ?></label>
                    </div>
                    <div class="standard-wrapper radio-list">
                        <?= $actionForm->radioButtonList($form, 'distribution', UserDistributionRecord::typeReplacements(), [
                            'template' => '<span class="radio-item">{input} {label}</span>',
                            'separator' => '',
                            'container' => 'span',
                        ]) ?>
                    </div>
                </div>
                <div class="standard-block">
                    <div class="standard-wrapper btn-group">
                        <?= CHtml::submitButton($this->t('Save'), ['class' => 'btn red']) ?>
                        <?= CHtml::resetButton($this->t('Reset changes'), ['class' => 'btn reset']) ?>
                    </div>
                </div>

            </div>
        </div>
        <?php $this->endWidget() ?>
    </div>
</div>

<?= $this->renderPartial('_scripts') ?>
