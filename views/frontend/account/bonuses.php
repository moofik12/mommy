<?php

use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var $provider CDataProvider
 * @var $this FrontController
 * @var float $totalBonuses
 * @var float $holdBonuses
 */
$this->pageTitle = $this->t('Offers');
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
?>

<div class="page-header">
    <div class="container page-title"><?= $this->t('My account') ?></div>
</div>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>

        <div class="content-place">
            <div class="styled-title"><?= $this->t('Balance') ?></div>
            <div class="white-wrapper">
                <table class="account-table">
                    <thead>
                    <tr>
                        <th><?= $this->t('Date of change') ?></th>
                        <th><?= $this->t('Cause') ?></th>
                        <th><?= $this->t('Validity') ?></th>
                        <th><?= $this->t('Amount') ?></th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td colspan="4"><?= $this->t('Current number of bonuses:') ?>: <span
                                    class="bonus-total"><?= $totalBonuses ?></span></td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php foreach ($provider->getData() as $item): /* @var $item UserBonuspointsRecord */ ?>
                        <tr>
                            <td><span class="date"><?= $df->format('d MMMM yyyy', $item->created_at) ?></span></td>
                            <?php if ($item->is_custom): ?>
                                <td><span class="cause"><?= CHtml::encode($item->custom_message) ?></span></td>
                            <?php else: ?>
                                <td><span class="cause"><?= CHtml::encode($item->message) ?></span></td>
                            <?php endif; ?>
                            <td>
                                <span class="date"><?= $item->expire_at ? $df->format('d MMMM yyyy', $item->expire_at) : '' ?></span>
                            </td>
                            <?php if ($item->points > 0): ?>
                                <td><span class="bonus-up">+<?= $item->points ?></span></td>
                            <?php else: ?>
                                <td><span class="bonus-down"><?= $item->points ?></span></td>
                            <?php endif ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?= $this->renderPartial('_scripts') ?>
