<?php

use MommyCom\Controller\Frontend\AccountController;
use MommyCom\Model\Form\UserAccountForm;

/**
 * @var $model UserAccountForm
 * @var $this AccountController
 * @var $form CActiveForm
 */
$this->pageTitle = $this->t('Personal data');
?>

<div class="page-header">
    <div class="container page-title"><?= $this->t('My account') ?></div>
</div>
<div class="container">
    <div class="page-content">
        <?= $this->renderPartial('_menu') ?>

        <div class="content-place">
            <?php $form = $this->beginWidget('CActiveForm', [
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'clientOptions' => [
                    'validateOnSubmit' => true,
                ],
            ]) ?>
            <div class="styled-title"><?= $this->t('Personal data') ?></div>
            <div class="white-place">
                <div class="standard-block">
                    <div class="standard-title">
                        <label class="left-part"><?= $this->t('Your name') ?></label>
                        <label class="right-part"><?= $this->t('Your last name') ?></label>
                    </div>
                    <div class="standard-wrapper">
                        <div class="left-part">
                            <div class="field bottom <?= $model->hasErrors('name') ? 'error' : '' ?>">
                                <?= $form->textField($model, 'name') ?>
                                <?= $form->error($model, 'name', ['class' => 'message']) ?>
                            </div>
                        </div>
                        <div class="right-part">
                            <div class="field bottom <?= $model->hasErrors('surname') ? 'error' : '' ?>">
                                <?= $form->textField($model, 'surname') ?>
                                <?= $form->error($model, 'surname', ['class' => 'message']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="standard-block">
                    <div class="standard-title">
                        <label class="left-part"><?= $this->t('E-mail address') ?></label>
                        <label class="right-part"><?= $this->t('Phone') ?></label>
                    </div>
                    <div class="standard-wrapper">
                        <div class="left-part">
                            <div class="field bottom <?= $model->hasErrors('email') ? 'error' : '' ?>">
                                <?= $form->textField($model, 'email') ?>
                                <?= $form->error($model, 'email', ['class' => 'message']) ?>
                            </div>
                        </div>
                        <div class="right-part">
                            <div class="field bottom <?= $model->hasErrors('telephone') ? 'error' : '' ?>">
                                <?= $form->textField($model, 'telephone') ?>
                                <?= $form->error($model, 'telephone', ['class' => 'message']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="standard-block">
                    <div class="standard-title">
                        <label class="left-part"><?= $this->t('Address') ?></label>
                    </div>
                    <div class="standard-wrapper">
                        <div class="field textarea top <?= $model->hasErrors('address') ? 'error' : '' ?>">
                            <?= $form->textArea($model, 'address') ?>
                            <?= $form->error($model, 'address', ['class' => 'message']) ?>
                        </div>
                    </div>
                </div>
                <div class="standard-block">
                    <div class="standard-wrapper">
                        <?= CHtml::submitButton($this->t('Save'), ['class' => 'btn red']) ?>
                        <?= CHtml::resetButton($this->t('Reset changes'), ['class' => 'btn reset']) ?>
                    </div>
                </div>
            </div>
            <?php $this->endWidget() ?>
        </div>
    </div>
</div>

<?= $this->renderPartial('_scripts') ?>
