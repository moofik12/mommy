<?php

use MommyCom\Controller\Frontend\AccountController;
use MommyCom\Model\Db\InviteRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * @var InviteRecord[] $invitations
 * @var $this AccountController
 * @var $form CActiveForm
 * @var $model UserRecord
 * @var $invite InviteRecord
 */

$this->pageTitle = $this->t('Invitations');

$app = $this->app();
$df = $dateFormatter = $app->dateFormatter;
$tf = $timerFormatter = $app->timerFormatter;

$statusCssClass = [
    InviteRecord::STATUS_WAITING => 'awaiting',
    InviteRecord::STATUS_ACTIVATED => 'purchase',
    InviteRecord::STATUS_PURCHASE => 'activated',
];

?>
<div class="page-header">
    <div class="container page-title"><?= $this->t('My account') ?></div>
</div>
<div class="container">
    <div class="page-content">
        <div class="content-menu">
            <?= $this->renderPartial('_menu') ?>

        </div>
        <div class="content-place">
            <div class="styled-title"><?= $this->t('Invitations') ?></div>
            <div class="white-wrapper">
                <?php if ($provider->totalItemCount > 0): ?>
                    <table class="account-table">
                        <thead>
                        <tr>
                            <th><?= $this->t('Dispatch date') ?></th>
                            <th><?= $this->t('E-mail address') ?></th>
                            <th class="status"><?= $this->t('Status') ?></th>
                            <th class="status-bonus"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($provider->getData() as $item): /* @var $item InviteRecord */ ?>
                            <tr>
                                <td><span class="date"><?= CHtml::tag(
                                            'time',
                                            ['datetime' => $tf->formatMachine($item->created_at)],
                                            $df->format('d MMMM yyyy', $item->created_at)
                                        ) ?></span></td>
                                <td><span class="invite-mail"><?= CHtml::encode($item->email) ?></span></td>
                                <td><span class="<?= CHtml::value($statusCssClass, $item->status) ?>">
                                            <?= CHtml::value($item->statusReplacement(), $item->status) ?>
                                        </span>
                                </td>
                                <td><span class="invite-bonus"></span></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                <?php endif ?>
            </div>
            <div class="white-place">
                <div class="append-block">
                    <?= CHtml::beginForm() ?>
                    <label><?= $this->t('Referral link') ?></label>
                    <div class="append-wrapper">
                        <div class="input-append">
                            <div class="field red">
                                <?= CHtml::textField('invite-input',
                                    $app->createAbsoluteUrl('index/invite', ['name' => $model->getPrimaryKey()]),
                                    ['readonly' => 'readonly']
                                ) ?>
                            </div>
                        </div>
                        <div class="append-label"><?= $this->t('Copy the link for registration and send it to your friend') ?></div>
                    </div>
                    <?= CHtml::endForm() ?>
                </div>
                <div class="append-block">
                    <?php $form = $this->beginWidget('CActiveForm', [
                        'id' => 'invite-email-form',
                        'enableClientValidation' => false,
                        'action' => ['account/inviteByEmail'],
                    ]) ?>
                    <label><?= $this->t('Send invitation') ?></label>
                    <div class="append-wrapper">
                        <div class="input-append button">
                            <div class="field">
                                <?= $form->textField($invite, 'email',
                                    ['placeholder' => $this->t('Friend\'s email address'), 'autocomplete' => 'off']) ?>
                            </div>
                            <?= CHtml::submitButton($this->t('Send')) ?>
                        </div>
                        <div class="append-label"><?= $this->t('Write your friend\'s email address to whom we will send an invitation on your behalf') ?></div>
                    </div>
                    <?php $this->endWidget() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->renderPartial('_scripts') ?>
