<?php

use MommyCom\Controller\Frontend\CatalogController;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\ProductFilter\ProductsFilter;

/**
 * @var $this CatalogController
 * @var $filter ProductsFilter
 */

$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;
/** CurrencyFormatter */
$priceFilter = $filter->priceFilter;
$targetFilter = $filter->targetFilter;
$sectionFilter = $filter->sectionFilter;
$sizeFilter = $filter->sizeFilter;
$colorFilter = $filter->colorFilter;
$ageFilter = $filter->ageFilter;
$brandFilter = $filter->brandFilter;
$isExistFilter = false;
?>
<?php if ($sectionFilter->isApplicable() && !empty($sectionFilter->values)) : ?>
    <?php $isExistFilter = true; ?>
    <?php if ($sectionFilter->isParent) : ?>
        <div class="filter-item">
            <div class="title-middle">
                <span><?= $this->t($sectionFilter->parent->name) ?></span>
            </div>
            <i class="icon-filter-remove" data-type="filter-bar" data-name="<?= $sectionFilter->name() ?>[]"
               data-value="<?= $sectionFilter->parent->id ?>"></i>
        </div>
    <?php else: ?>
        <?php foreach ($sectionFilter->getSelectedValues() as $value) : /** @var $value ProductSectionRecord */ ?>
            <div class="filter-item">
                <div class="title-middle">
                    <span><?= $this->t($value->name) ?></span>
                </div>
                <i class="icon-filter-remove" data-type="filter-bar" data-name="<?= $sectionFilter->name() ?>[]"
                   data-value="<?= $value->id ?>"></i>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
<?php endif; ?>
<?php if ($brandFilter->isApplicable() && !empty($brandFilter->values)) : ?>
    <?php $isExistFilter = true; ?>
    <?php foreach ($brandFilter->getSelectedValues() as $brand) : /** @var $brand BrandRecord */ ?>
        <div class="filter-item">
            <div class="title-middle">
                <span><?= $this->t($brand->name) ?></span>
            </div>
            <i class="icon-filter-remove" data-type="filter-bar" data-name="<?= $brandFilter->name() ?>[]"
               data-value="<?= $brand->id ?>"></i>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if ($colorFilter->isApplicable() && !empty($colorFilter->values)) : ?>
    <?php $isExistFilter = true; ?>
    <?php foreach ($colorFilter->getSelectedValues() as $colorId => $name) :/** @var $name string */ ?>
        <div class="filter-item">
            <div class="title-middle">
                <span><?= $this->t($name) ?></span>
            </div>
            <i class="icon-filter-remove" data-type="filter-bar" data-name="<?= $colorFilter->name() ?>[]"
               data-value="<?= $colorId ?>"></i>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if ($sizeFilter->isApplicable() && !empty($sizeFilter->values)) : ?>
    <?php $isExistFilter = true; ?>
    <?php foreach ($sizeFilter->values as $value) : ?>
        <div class="filter-item">
            <div class="title-middle">
                <span><?= $this->t($value) ?></span>
            </div>
            <i class="icon-filter-remove" data-type="filter-bar" data-name="<?= $sizeFilter->name() ?>[]"
               data-value="<?= $value ?>"></i>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if ($ageFilter->isApplicable() && !empty($ageFilter->values)) : ?>
    <?php $isExistFilter = true; ?>
    <?php foreach ($ageFilter->values as $value) : ?>
        <div class="filter-item">
            <div class="title-middle">
                <span><?= $this->t(AgeGroups::instance()->getLabel($value)) ?></span>
            </div>
            <i class="icon-filter-remove" data-type="filter-bar" data-name="<?= $ageFilter->name() ?>[]"
               data-value="<?= $value ?>"></i>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if ($isExistFilter) : ?>
    <a href="<?= $this->createUrl('catalog/index') ?>" class="filter-item remove-all no-select">
        <div class="title-middle">
            <span><?= $this->t('Remove all') ?></span>
        </div>
    </a>
<?php endif; ?>
