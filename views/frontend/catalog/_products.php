<?php

use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var $this FrontController
 * @var $groupedProducts GroupedProducts
 * @var $provider CDataProvider
 */
$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
$splitTesting = $app->splitTesting;
/* @var $splitTesting \MommyCom\Service\SplitTesting */

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
?>
<?php if ($groupedProducts->count == 0): ?>
    <div class="list-no-items">
        <p><?= $this->t('There are no products available according to your search.') ?></p>
        <p><?= $this->t('Try reducing the number of filters') ?></p>
    </div>
<?php endif; ?>
<?php foreach ($groupedProducts->toArray() as $item): /* @var $item GroupedProduct */ ?>
    <div class="goods-item form-in" itemscope itemtype="http://schema.org/Product">
        <div class="form-in-wrap">
            <div class="form-in-wrap-content">
                <?= $this->renderPartial('//layouts/_productJson', ['productGroup' => $item]) ?>
                <a class="product-link-details"
                   data-product-id="<?= $item->event->id ?>/<?= $item->product->id ?>"
                   href="<?= $app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']) ?>">
                    <div class="box">
                        <?php if ($item->isSoldOut): ?>
                            <div class="label-sold">×</div>
                        <?php else: ?>
                            <div class="label-discount">-<?= round((1 - $item->price / $item->priceOld) * 100) ?>%</div>
                            <?php if ($item->isLimited): ?>
                                <!--<div class="label-ends">!</div>-->
                            <?php endif ?>
                            <?php if ($item->isTopOfSelling): ?>
                                <div class="label-hit"><?= $this->t('HIT') ?></div>
                            <?php endif ?>
                        <?php endif ?>
                        <div class="image-wrapper">
                            <?= CHtml::image(
                                $app->baseUrl . '/static/blank.gif', '', [
                                    'data-src' => $item->product->logo->getThumbnail('mid320_prodlist')->url,
                                    'data-lazyload' => 'true',
                                    'itemprop' => "image",
                                    'style' => 'margin-top: -50px',
                                ]
                            ) ?>
                        </div>
                        <div class="text-wrapper">
                            <p class="category-title"><?= CHtml::encode($this->t($item->event->name)) ?></p>
                            <p itemprop="name"
                               class="title"><?= CHtml::encode($this->t($item->product->name)) ?></p>
                            <div class="price">
                                <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                <?php if ($item->priceOld != $item->price): ?>
                                    <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                <?php endif ?>
                            </div>
                            <div class="time-right">
                                <?= CHtml::tag(
                                    'time',
                                    ['datetime' => $tf->formatMachine($item->event->end_at), 'data-countdown' => true],
                                    $tf->format($item->event->end_at)
                                ) ?>
                            </div>
                        </div>
                    </div>
                </a>
                <div class="product-info-wrapper">
                    <div class="available-info">
                        <?php if (!$item->isSoldOut && $item->sizeCount > 0): ?>
                            <label class="available-info-sizes"><?= $this->t('Dimensions') ?>: </label>
                            <?= $item->sizes ? implode(', ', $item->sizes) : '-' ?>
                        <?php endif; ?>
                    </div>
                    <div class="product-form <?= $item->isSoldOut ? 'unavailable' :'' ?>">
                        <?php if (!$item->isSoldOut): ?>
                            <form>
                                <input type="hidden" name="token"
                                       value="<?= $tokenManager->getToken($item->product->id) ?>">
                                <input type="hidden" name="eventId" value="<?= $item->event->id ?>">
                                <input type="hidden" name="productId" value="<?= $item->product->id ?>">
                                <input type="hidden" name="number" value="1">
                                <?php if ($item->sizeCount > 0): ?>
                                    <select name="size">
                                        <?php foreach ($item->sizes as $size): ?>
                                            <option value="<?= $size ?>"><?= $size ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>
                                <button class="in-cart btn green"><i
                                            class="icon-in-cart-open"></i><?= $this->t('Add to cart') ?>
                                </button>
                            </form>
                        <?php else: ?>
                            <p><?= $this->t('Product is sold out') ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>
