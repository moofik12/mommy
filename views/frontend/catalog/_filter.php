<?php

use MommyCom\Controller\Frontend\CatalogController;
use MommyCom\Model\Db\BrandRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Product\CategoryGroups;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Model\ProductFilter\ProductsFilter;
use MommyCom\Service\SplitTesting;

/**
 * @var $this CatalogController
 * @var $filter ProductsFilter
 */
$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting SplitTesting */
$splitTesting = $app->splitTesting;
$priceFilter = $filter->priceFilter;
$targetFilter = $filter->targetFilter;
$sectionFilter = $filter->sectionFilter;
$sizeFilter = $filter->sizeFilter;
$colorFilter = $filter->colorFilter;
$ageFilter = $filter->ageFilter;
$brandFilter = $filter->brandFilter;
$sortFilter = $filter->sortFilter;
$request = $this->app()->request; /** @var $request CHttpRequest */
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $this->app()->currencyFormatter;
?>
<div class="catalog-sidebar">
    <div class="results-window hide-i" id="show-result-catalog">
        <div><a href="javascript:void(0);" class="show-link"><?= $this->t('Show') ?></a></div>
        <div><span class="results-count"></span></div>
        <div><span class="results-loader"></span></div>
        <div><span class="results-close"></span></div>
    </div>
    <?php $form = $this->beginWidget('CActiveForm', [
        'action' => $this->createUrl('catalog/index', [$targetFilter->name() => $targetFilter->value]),
        'method' => 'get',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ])
    ?>
    <?php if ($sortFilter->isApplicable()): ?>
        <div class="element">
            <div class="element-title">
                <strong><?= $sortFilter->label() ?></strong>
            </div>
            <div class="element-content">
                <div class="element-content-inner">
                    <ul class="goods-filter cat-goods-filter">
                        <li class="dropdown">
                            <span class="filter-item" role="button" data-toggle="dropdown"
                                  id="sort-name"><?= $sortFilter->getValueName() ?><b></b></span>
                            <div class="dropdown-menu filter-menu">
                                <ul class="filter-price">
                                    <?php foreach ($sortFilter->getItems() as $value => $name): ?>
                                        <li>
                                            <input type="radio"
                                                   name="<?= $sortFilter->name() ?>" <?= ($value == $sortFilter->value) ? 'checked="checked"' : '' ?>
                                                   value="<?= $value ?>" id="sort-<?= $value ?>">
                                            <label for="sort-<?= $value ?>"><?= $name ?></label>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($priceFilter->isApplicable()): ?>
        <div class="element">
            <div class="element-title">
                <strong><?= $priceFilter->label() ?></strong>
            </div>
            <div class="element-content">
                <div class="element-content-inner">
                    <div class="sidebar-field">
                        <input class="small-input sm1" type="text"
                               value="<?= $priceFilter->valueMin ? $priceFilter->valueMin : '' ?>"
                               name="<?= $priceFilter->name() ?>_min"
                               placeholder="<?= $priceFilter->getAvailableFrom() ?>"/>
                        <span class="line">—</span>
                        <input class="small-input" type="text"
                               value="<?= $priceFilter->valueMax ? $priceFilter->valueMax : '' ?>"
                               name="<?= $priceFilter->name() ?>_max"
                               placeholder="<?= $priceFilter->getAvailableTo() ?>"/>
                        <span class="curr"><?= $cf->getCurrency() ?></span>
                    </div>
                    <button class="sidebar-btn" type="button"><?= $this->t('Show') ?></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="element">
        <div class="element-title">
            <strong><?= $targetFilter->label() ?></strong>
        </div>
        <div class="element-content">
            <?php foreach ($targetFilter->getItems() as $value => $name) : ?>
                <?php $classNameCategory = '';
                switch ($value) {
                    case CategoryGroups::CATEGORY_MOMS:
                        $classNameCategory = 'category-mother';
                        break;
                    case CategoryGroups::CATEGORY_DADS:
                        $classNameCategory = 'category-father';
                        break;
                    case CategoryGroups::CATEGORY_GIRLS:
                        $classNameCategory = 'category-girls';
                        break;
                    case CategoryGroups::CATEGORY_BOYS:
                        $classNameCategory = 'category-boys';
                        break;
                    case CategoryGroups::CATEGORY_HOME:
                        $classNameCategory = 'category-home';
                        break;
                    case CategoryGroups::CATEGORY_TOYS:
                        $classNameCategory = 'category-toys';
                        break;
                }
                ?>
                <a href="<?= $this->createUrl('catalog/index', [$targetFilter->name() => $value]) ?>"
                   class="element-category-title <?= $classNameCategory ?>"><?= $this->t($name) ?></a>
                <div class="element-categories">
                    <?php if ($sectionFilter->isApplicable() && $targetFilter->value == $value) : ?>
                        <ul class="categories-list">
                            <?php foreach ($sectionFilter->getGroupItems() as $items) : ?>
                                <?php
                                /** @var ProductSectionRecord $parentSection */
                                $parentSection = $items['parent'];
                                /** @var ProductSectionRecord[] $childrenSection */
                                $childrenSections = $items['children'];
                                $isSelectedChild = $items['isSelectedChild'];
                                ?>
                                <?php if ($parentSection !== null): ?>
                                    <li class="<?= $isSelectedChild ? 'active-category-item' : '' ?>">
                                        <a href="<?= $this->createUrl('catalog/index', [$targetFilter->name() => $value, $sectionFilter->name() . '[]' => $parentSection->id]) ?>"
                                           data-category-id="<?= $parentSection->id; ?>"><?= $this->t($parentSection->name) ?></a>
                                        <div class="element-content-inner switcher switcher-sub <?= !$isSelectedChild ? 'hide' : '' ?>"
                                             data-parent-id="<?= $parentSection->id; ?>">
                                            <?php if ($isSelectedChild): ?>
                                                <input type="hidden" name="<?= $sectionFilter->name() ?>[]"
                                                       id="cat<?= $parentSection->id ?>"
                                                       value="<?= $parentSection->id ?>" data-type="filter-action"/>
                                                <?php foreach ($childrenSections as $childrenSection) : /** @var $childrenSection ProductSectionRecord */ ?>
                                                    <div class="cb-item <?= (in_array($childrenSection->id, $sectionFilter->values)) ? 'cb-active' : '' ?>">
                                                        <input type="checkbox" name="<?= $sectionFilter->name() ?>[]"
                                                               id="cat<?= $childrenSection->id ?>"
                                                               value="<?= $childrenSection->id ?>" <?= (in_array($childrenSection->id, $sectionFilter->values)) ? 'checked="checked"' : '' ?>
                                                               data-type="filter-action"/>
                                                        <label for="cat<?= $childrenSection->id ?>"><span></span><?= $this->t($childrenSection->name) ?>
                                                        </label>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php if ($sizeFilter->isApplicable()): ?>
        <?php $sizeItems = $sizeFilter->getItems(); ?>
        <?php if (count($sizeItems) > 1): ?>
            <div class="element">
                <div class="element-title">
                    <strong><?= $sizeFilter->label() ?></strong>
                </div>
                <div class="element-content has-scrollbar">
                    <div class="element-content-inner switcher">
                        <? foreach ($sizeItems as $key => $size) : ?>
                            <div class="cb-item <?= in_array($size, $sizeFilter->values) ? 'cb-active"' : '' ?>">
                                <input type="checkbox" name="<?= $sizeFilter->name() ?>[]" id="size<?= $key ?>"
                                       value="<?= $size ?>"
                                       data-type="filter-action" <?= in_array($size, $sizeFilter->values) ? 'checked="checked"' : '' ?>/>
                                <label for="size<?= $key ?>"><span></span><?= $size ?> </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($brandFilter->isApplicable()): ?>
        <?php $brandItems = $brandFilter->getItems(); ?>
        <?php if (count($brandItems) > 1): ?>
            <div class="element">
                <div class="element-title">
                    <strong><?= $brandFilter->label() ?></strong>
                </div>
                <div class="element-content has-scrollbar">
                    <div class="element-content-inner switcher">
                        <?php foreach ($brandFilter->getItems() as $brand): /** @var $brand BrandRecord */ ?>
                            <div class="cb-item <?= in_array($brand->id, $brandFilter->values) ? 'cb-active"' : '' ?>">
                                <input type="checkbox" name="<?= $brandFilter->name() ?>[]" id="brand<?= $brand->id ?>"
                                       value="<?= $brand->id ?>" <?= in_array($brand->id, $brandFilter->values) ? 'checked="checked"' : '' ?>
                                       data-type="filter-action"/>
                                <label for="brand<?= $brand->id ?>"><span></span><?= $brand->name ?> </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($ageFilter->isApplicable()): ?>
        <div class="element">
            <div class="element-title">
                <strong><?= $ageFilter->label() ?></strong>
            </div>
            <div class="element-content">
                <div class="element-content-inner switcher">
                    <?php foreach ($ageFilter->getItems() as $value => $name) : ?>
                        <div class="cb-item <?= in_array($value, $ageFilter->values) ? 'cb-active"' : '' ?>">
                            <input type="checkbox" name="<?= $ageFilter->name() ?>[]" id="age<?= $value ?>"
                                   value="<?= $value ?>"
                                   data-type="filter-action" <?= in_array($value, $ageFilter->values) ? 'checked="checked"' : '' ?>/>
                            <label for="age<?= $value ?>"><span></span><?= $name ?></label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($colorFilter->isApplicable()): ?>
        <?php $colorItems = $colorFilter->getItems(); ?>
        <?php if (count($colorItems) > 1): ?>
            <div class="element">
                <div class="element-title">
                    <strong><?= $colorFilter->label() ?></strong>
                </div>
                <div class="element-content has-scrollbar">
                    <div class="element-categories colors">
                        <ul class="categories-list">
                            <?php foreach ($colorItems as $colorId => $name): ?>
                                <li class="<?= in_array($colorId, $colorFilter->values) ? 'color-active' : '' ?>">
                                    <a href="javascript:void(0);" data-type="color-filter" data-value="<?= $colorId ?>"
                                       data-name="<?= $colorFilter->name() ?>[]"
                                       data-checked="<?= in_array($colorId, $colorFilter->values) ? 1 : 0 ?>">
                                        <span class="color-icon filter_<?= ProductColorCodes::instance()->getFrontClassName($colorId) ?>"></span><?= $name ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php $this->endWidget() ?>
</div>
<script>
    $(function () {
        function format(number, separator) {
            var thousand_separator = separator;

            var number_string = number.toString(),
                rest = number_string.length % 3,
                result = number_string.substr(0, rest),
                thousands = number_string.substr(rest).match(/\d{3}/gi);

            if (thousands) {
                separator = rest ? thousand_separator : '';
                result += separator + thousands.join(thousand_separator);
            }

            return result;
        }

        $('.sidebar-field input').on('keyup change', function () {
            var value = $(this).val();
            value = value.replace(/\./g, '');
            $(this).val(format(value, '.'));
        });

        var prices = $('.sidebar-field input');
        prices.each(function (index, element) {
            var value = $(element).val();
            $(element).val(format(value, '.'));
        });
    });
</script>
