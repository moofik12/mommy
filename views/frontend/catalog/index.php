<?php

use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\ProductFilter\ProductsFilter;
use MommyCom\Widget\Frontend\MamamLinkAdvancedPager;
use MommyCom\Widget\Frontend\MamamLinkPager;
use MommyCom\YiiComponent\Frontend\FrontController;

/**
 * @var $this FrontController
 * @var $provider CDataProvider
 * @var $groupedProducts GroupedProducts
 * @var $title string
 * @var $filter ProductsFilter
 */
$this->pageTitle = $title;
$app = $this->app();
$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;

$this->description = $this->t('Catalog - the first shopping mall for moms and children in the country Mommy.COM. Products for the whole family and home. Daily sales with discounts up to 90%');
?>
<div class="catalog-preloader-overlay" id="catalog-loader">
    <div class="catalog-preloader-image"></div>
</div>
<div class="content-header container">
    <?php if ($provider->pagination->pageCount > 1) : ?>
        <div class="catalog-pagination catalog-pagination-top">
            <?php $this->widget(MamamLinkPager::class, [
                'pages' => $provider->pagination,
                'maxButtonCount' => 5,
            ]) ?>
        </div>
    <?php endif; ?>
    <div class="promotional">
        <span class="title"><a href="<?= $this->createUrl('catalog/index') ?>"
                               class="title-link"><?= $this->t('Catalog') ?></a></span>
    </div>
    <div class="clear-right"></div>
</div>
<div class="dotted"></div>
<div class="catalog-container container">
    <?= $this->renderPartial('_filter', [
        'filter' => $filter,
    ]) ?>
    <div class="catalog-content">
        <div class="catalog-content-top">
            <div class="filter-bar">
                <?= $this->renderPartial('_filterBar', [
                    'filter' => $filter,
                ]) ?>
            </div>
            <div class="clear-right"></div>
        </div>
        <div class="goods-list">
            <?= $this->renderPartial('_products', [
                'provider' => $provider,
                'groupedProducts' => $groupedProducts,
            ]) ?>
        </div>
        <?php if ($provider->pagination->pageCount > 1) : ?>
            <div class="catalog-pagination catalog-pagination-bottom">
                <?php $this->widget(MamamLinkAdvancedPager::class, [
                    'pages' => $provider->pagination,
                    'maxButtonCount' => 5,
                    'prevPageLabel' => $this->t('Previous'),
                    'firstPageLabel' => $this->t('Next'),
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="clear"></div>
</div>
