<?php

use MommyCom\Controller\Backend\AdminRoleController;

/**
 * @var $this AdminRoleController
 * @var $dataProvider CActiveDataProvider
 */

$this->pageTitle = $this->t('Managing roles');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Role list'),
    'headerIcon' => 'icon-edit',
]);

$this->widget('bootstrap.widgets.TbButton', [
    'type' => 'primary',
    'label' => $this->t('Add role'),
    'url' => $this->createUrl('add'),
]);

$this->renderPartial('_index', ['dataProvider' => $dataProvider]);

$this->endWidget();
?>
