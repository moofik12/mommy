<?php

use MommyCom\Controller\Backend\AdminRoleController;

/**
 * @var $this AdminRoleController
 */
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Adding role'),
    'headerIcon' => 'icon-plus',
]); ?>
<?php /* @var $form TbActiveForm */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'add-role-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
    'action' => $this->createUrl('adminRole/add'),
    'htmlOptions' => ['class' => 'well'],
]); ?>
<?= $form->errorSummary($model, CHtml::tag('a', ['class' => 'close', 'data-dismiss' => 'alert', 'href' => '#'], '&times;')); ?>
<fieldset>
    <legend><?= $this->t('Role') ?></legend>
    <?= $form->textFieldRow($model, 'name', ['autocomplete' => 'off', 'tabindex' => '1']); ?>
    <?= $form->textFieldRow($model, 'description', ['autocomplete' => 'off', 'tabindex' => '2']); ?>
</fieldset>
<fieldset>
    <legend><?= $this->t('Access permissions') ?></legend>
    <div>
        <?php foreach ($classList as $className) : ?>
            <?php
            $name = mb_strtoupper($className["className"]);
            $checkedClass = in_array("$name.*", $model['rulesArray']);
            $checkedMethods = false;
            ?>
            <div class="select-access-wrapper clearfix">
                <div style="margin: 0 0 10px 0;" class="select-access pull-left">
                    <div style="margin-left: 30px;" id="<?= $className["className"] ?>"
                         class="collapse select-access-method pull-right">
                        <?php if (isset($className['methods'])) : ?>
                            <?php foreach ($className['methods'] as $method) : ?>
                                <label>
                                    <?php
                                    $nameAction = mb_strtoupper($method["methodName"]);
                                    $checkedMethod = in_array("$name.$nameAction", $model['rulesArray']);
                                    $checkedMethod = $checkedClass ? $checkedClass : $checkedMethod;
                                    $checkedMethods = $checkedMethod ? $checkedMethod : $checkedMethods;
                                    ?>
                                    <?= CHtml::activeCheckBox($model, "rulesArray[$name][$nameAction]", ['checked' => $checkedMethod]); ?>
                                    <span>&nbsp;&nbsp;<?= $method["methodDoc"] ? $method["methodDoc"] : $method["methodName"]; ?></span>
                                </label>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="select-access-class pull-left">
                        <?= CHtml::checkBox("accessClass", $checkedClass || $checkedMethods); ?>
                        <label class="checkbox" data-toggle="collapse" data-target="#<?= $className["className"] ?>"
                               style="display: inline; padding: 0">
                            <span>&nbsp;&nbsp;<?= $className["classDoc"] ? $className["classDoc"] : $className["className"]; ?></span>
                        </label>
                        <span class="label label-success access-all <?= $checkedClass ? '' : 'hide' ?>">
                                <?= $this->t('Full access') ?>
                            </span>
                        <span class="label label-warning access-some-methods <?= !$checkedClass && $checkedMethods ? '' : 'hide' ?>">
                                <?= $this->t('Restricted access') ?>
                            </span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</fieldset>
<?= CHtml::htmlButton('<i class="icon-ok icon-white"></i> ' . $this->t('Add'), ['class' => 'btn btn-primary', 'type' => 'submit']); ?>
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>


<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('activeCheckbox', '
    $(".select-access-class").on("click", "input[type=checkbox]", function() {
        var self = $(this);
        var checked = self.prop("checked");
        var containerID = self.siblings("label").data("target");
        var methods = $(containerID).find("input[type=checkbox]");

        $.each(methods, function() {
            $(this).prop("checked", checked).trigger("change");
        });
    });

    $(".select-access-method").on("change", "input[type=checkbox]", function() {
        var self = $(this);
        var checked = self.prop("checked");
        var classChecked = false;
        var divContainer =  self.closest(".select-access-method");
        var countChecked = 0;

        var methods = divContainer.find("input[type=checkbox]");

        $.each(methods, function() {
            if ($(this).prop("checked")) {
                countChecked ++;
            }
        });

        if (countChecked > 0) {
            classChecked = true;
        }

        var classContainer = divContainer.siblings(".select-access-class");
        var classCheckbox = classContainer.find("input[type=checkbox]");
        var accessAll = classContainer.children(".access-all");
        var accessSomeMethods = classContainer.children(".access-some-methods");

        classCheckbox.prop("checked", classChecked);

        if (countChecked == methods.length) {
            accessAll.show();
            accessSomeMethods.hide();
        } else if (countChecked > 0) {
            accessAll.hide();
            accessSomeMethods.show();
        } else {
            accessAll.hide();
            accessSomeMethods.hide();
        }

    });
');

?>
