<?php

use MommyCom\Controller\Backend\AdminRoleController;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this AdminRoleController
 * @var $dataProvider CActiveDataProvider
 */

$this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'admin_grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    //'filterSelector' => '{filter}, form select, form input',
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'id') . '_filter'],
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        [
            'name' => 'name',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'login') . '_filter'],
        ],
        [
            'name' => 'description',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'fullname') . '_filter'],
        ],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
        ],
    ],
]);
