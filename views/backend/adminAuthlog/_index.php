<?php

use MommyCom\Controller\Backend\AdminAuthlogController;
use MommyCom\Model\Db\AdminAuthlogRecord;

/**
 * @var $this AdminAuthlogController
 * @var $data AdminAuthlogRecord
 * @var $grid TbExtendedGridView
 */
$grid = $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'ip-ban-grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'filterSelector' => '{filter}, form[id=filterForm] input',
    'rowCssClassExpression' => function ($row, $data) {
        return $data->is_success ? 'success' : '';
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        'login',
        'ip',
        [
            'name' => 'is_success',
            'type' => 'boolean',
            'filter' => [
                AdminAuthlogRecord::SUCCESS_TRUE => $this->t('Yes'),
                AdminAuthlogRecord::SUCCESS_FALSE => $this->t('No'),
            ],
        ],
        'comment',
        'user_agent',
        [
            'name' => 'created_at',
            'filter' => false,
            'value' => '\Yii::app()->getDateFormatter()->formatDateTime($data->created_at, "long")',
            'headerHtmlOptions' => ['class' => 'span3'],
        ],
    ],
]); ?>
