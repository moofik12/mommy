<?php

use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('Payments report');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, array(
    'title' => $this->t('Orders'),
    'headerIcon' => 'icon-edit'
)); ?>
    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $provider,
        'filter' => $provider->model,
        'summaryText' => 'Orders {start} - {end} out of {count}',
        'filterSelector' => '{filter}, #filterForm :input',
        'id' => 'orders',
        'columns' => array(
            array(
                'name' => 'order_id',
                'htmlOptions' => array('class' => 'span2'),
                'type' => 'raw',
                'value' => function($data) {
                    /* @var $data OrderTrackingRecord */
                    return CHtml::link($data->order_id, 'javascript:void(0)', array(
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverOrder',
                            array('order' => $data->order),
                            true
                        )
                    ));
                }
            ),
            array('name' => 'order_delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function($data) {
                /* @var $data OrderTrackingRecord */
                return $data->orderDeliveryTypeReplacement;
            }),

            array('name' => 'status', 'type' => 'raw', 'filter' => OrderBuyoutRecord::statusReplacements(), 'value' => function($data) {
                /* @var $data OrderTrackingRecord */
                return $data->statusReplacement;
            }),

            array('name' => 'order.trackcode', 'htmlOptions' => array('class' => 'span2')),

            array('name' => 'price', 'type' => 'number'),
            array('name' => 'delivery_price', 'type' => 'number'),
            array('name' => 'synchronized_at', 'type' => 'datetime'),
        )
    )); ?>
    <div class="clearfix"></div>
<?php $this->endWidget() ?>
