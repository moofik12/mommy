<?php

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Widget\Backend\ButtonColumn;

$this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'admin_grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => $this->t('Administrators {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form select, form input',
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'id') . '_filter'],
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        [
            'name' => 'login',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'login') . '_filter'],
        ],
        [
            'name' => 'fullname',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'fullname') . '_filter'],
        ],
        [
            'name' => 'email',
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'email') . '_filter'],
        ],
        [
            'name' => 'is_root',
            'type' => 'boolean',
            'filter' => [$this->t('No'), $this->t('Yes')],
        ],
        [
            'name' => 'is_supplier',
            'type' => 'boolean',
            'filter' => [$this->t('No'), $this->t('Yes')],
        ],
        [
            'name' => 'supplier.supplierData.name',
            'header' => 'Supplier Name',
        ],
        [
            'name' => 'roles',
            'header' => $this->t('Roles'),
            'type' => 'raw',
            'filter' => false,
            'value' => function ($data) {
                $text = '';
                /** @var $data AdminUserRecord */
                foreach ($data->roles as $role) {
                    $text .= '<span class="label label-info">' . $role->description . '</span> ';
                }

                return $text;
            },
        ],
        [
            'name' => 'status',
            'type' => 'html',
            'filter' => false,
            'value' => function ($data) {
                return CHtml::value($data->statusReplacements(), $data->status);
            },
            'cssClassExpression' => function ($row, $data) {
                $class = 'text-success';

                if ($data->status == AdminUserRecord::STATUS_BAN) {
                    $class = 'text-warning';
                } elseif ($data->status == AdminUserRecord::STATUS_HIDE) {
                    $class = 'text-error';
                }

                return $class;
            },
            'headerHtmlOptions' => ['class' => 'span2'],
            'filterInputOptions' => ['id' => CHtml::activeId($dataProvider->model, 'status') . '_filter'],
        ],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {ban} {delete}',
            'buttons' => [
                'ban' => [
                    'url' => function ($data, $row) {
                        return $this->app()->controller->createUrl("ban", ["id" => $data->primaryKey]);
                    },
                    'label' => '<i class="icon-ban-circle"></i>',
                    'options' => [
                        'title' => $this->t('Ban'),
                        'class' => 'btn btn-inverse',
                    ],
                    'click' => new CJavaScriptExpression("function(e) {
							e.preventDefault();
							var grid = jQuery(e.target).closest('.grid-view');
							grid.yiiGridView('update', {
								type: 'GET',
								url: jQuery(this).attr('href'),
								success: function(data) {
									grid.yiiGridView('update');
								},
								error: function(XHR) {
									return alert(XHR);
								}
							});
						}"),
                ],
            ],
        ],
    ],
]);
