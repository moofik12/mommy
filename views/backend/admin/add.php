<?php

use MommyCom\Controller\Backend\AdminController;
use MommyCom\Model\Db\AdminUserRecord;

/**
 * @var $this AdminController
 * @var $rolesData array
 * @var $suppliersData array
 * @var $model AdminUserRecord
 */
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Add admin'),
    'headerIcon' => 'icon-plus',
]); ?>
<?php /* @var $form TbActiveForm */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'add-admin-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
    'action' => $this->createUrl('admin/add'),
    'htmlOptions' => ['class' => 'well'],
]); ?>

<?= $form->errorSummary($model, CHtml::tag('a', ['class' => 'close', 'data-dismiss' => 'alert', 'href' => '#'], '&times;')); ?>
<fieldset>
    <legend><?= $this->t('Admin details') ?></legend>
    <?= $form->textFieldRow($model, 'login', ['autocomplete' => 'off', 'tabindex' => '1']); ?>
    <?= $form->passwordFieldRow($model, 'newPassword', ['autocomplete' => 'off', 'tabindex' => '2']); ?>
    <?= $form->passwordFieldRow($model, 'confirmPassword', ['autocomplete' => 'off', 'tabindex' => '3']); ?>
    <?= $form->textFieldRow($model, 'email', ['autocomplete' => 'off', 'tabindex' => '4']); ?>
    <?= $form->textFieldRow($model, 'fullname', ['autocomplete' => 'off', 'tabindex' => '5']); ?>
</fieldset>
<?= $form->error($model, 'is_root') ?>
<?= $form->error($model, 'is_supplier') ?>

<fieldset>
    <legend><?= $this->t('Access permissions') ?></legend>
    <?php if ($this->app()->user->isRoot()) : ?>
        <div class="well">
            <label class="">
                <?= $form->checkBox($model, 'is_root', ['data-toggle' => 'collapse',
                    'data-target' => '#no_root']); ?>
                <span>&nbsp;&nbsp;<?= $this->t('Super user') ?></span>
            </label>
        </div>
        <div class="well">
            <label class="">
                <?= $form->checkBox($model, 'is_supplier', ['data-toggle' => 'collapse',
                    'data-target' => '#suppliersData']); ?>
                <span>&nbsp;&nbsp;<?= $this->t('Supplier user') ?></span>
            </label>
        </div>
    <?php endif; ?>
    <?php if (!empty($suppliersData)): ?>
        <div id="suppliersData" class="collapse">
            <?php $form->widget('bootstrap.widgets.TbSelect2', [
                    'name' => 'supplierId',
                    'asDropDownList' => true,
                    'data' => $suppliersData,
                    'options' => [
                        'placeholder' => $this->t('Select supplier'),
                        'width' => '40%',
                        'tokenSeparators' => [',', ' '],
                    ],
                ]
            ); ?>
        </div>
    <?php endif; ?>
    <div id="no_root" class="collapse <?= $model->is_root ? '' : 'in'; ?>">
        <?php if (empty($rolesData)) {
            echo '<div class="alert alert-error"> ' . $this->t('No roles to be assigned') . '</div>';
        } else {
            $form->widget('bootstrap.widgets.TbSelect2', [
                    'name' => 'rolesIds',
                    'asDropDownList' => true,
                    'data' => $rolesData,
                    'options' => [
                        'placeholder' => $this->t('select role'),
                        'width' => '40%',
                        'tokenSeparators' => [',', ' '],
                    ],
                    'htmlOptions' => [
                        'multiple' => 'multiple',
                    ],
                ]
            );
        }
        ?>
    </div>
</fieldset>
<?= CHtml::htmlButton('<i class="icon-ok icon-white"></i> ' . $this->t('Add'), ['class' => 'btn btn-primary', 'type' => 'submit']); ?>
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
