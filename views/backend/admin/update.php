<?php

use MommyCom\Controller\Backend\AdminController;
use MommyCom\Model\Db\AdminUserRecord;

/**
 * @var $model AdminUserRecord
 * @var $this AdminController
 * @var $rolesHas array
 * @var $suppliersData array
 * @var $supplierId array
 */
?>
<?php $this->pageTitle = $this->t('Manage administrators'); ?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Editing an administrator'),
    'headerIcon' => 'icon-plus',
]); ?>
<?php /* @var $form TbActiveForm */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'edit-admin-form',
    'type' => 'horizontal',
    'htmlOptions' => ['class' => 'well'],
]); ?>
<?= $form->errorSummary($model, '<a class="close" data-dismiss="alert" href="#">&times;</a>'); ?>
<?php $model->clearErrors(); ?>
<fieldset>
    <legend><?= $this->t('Admin details') ?></legend>
    <?= $form->textFieldRow($model, 'login', ['autocomplete' => 'off', 'tabindex' => '1']); ?>
    <?= $form->textFieldRow($model, 'email', ['autocomplete' => 'off', 'tabindex' => '2']); ?>
    <?= $form->textFieldRow($model, 'fullname', ['autocomplete' => 'off', 'tabindex' => '3']); ?>
    <?= $form->dropDownListRow($model, 'status', $model->statusReplacements()); ?>
</fieldset>
<fieldset>
    <legend><?= $this->t('Change password') ?></legend>
    <?= $form->passwordFieldRow($model, 'newPassword', ['autocomplete' => 'off', 'tabindex' => '5']); ?>
    <?= $form->passwordFieldRow($model, 'confirmPassword', ['autocomplete' => 'off', 'tabindex' => '6']); ?>
</fieldset>
<?= $form->error($model, 'is_root') ?>
<fieldset>
    <legend><?= $this->t('Access permissions') ?></legend>
    <?php if ($this->app()->user->isRoot()) : ?>
        <div class="well">
            <label class="">
                <?= $form->checkBox($model, 'is_root', ['data-toggle' => 'collapse',
                    'data-target' => '#no_root']); ?>
                <span>&nbsp;&nbsp;<?= $this->t('Super user') ?></span>
            </label>
        </div>
        <div class="well">
            <label class="">
                <?= $form->checkBox($model, 'is_supplier', ['data-toggle' => 'collapse',
                    'data-target' => '#suppliersData']); ?>
                <span>&nbsp;&nbsp;<?= $this->t('Supplier user') ?></span>
            </label>
        </div>
    <?php endif; ?>
    <div id="suppliersData" class="collapse <?= $model->is_supplier ? 'in' : '' ?>">
        <?php $form->widget('bootstrap.widgets.TbSelect2', [
                'name' => 'supplierId',
                'asDropDownList' => true,
                'data' => $suppliersData,
                'val' => $supplierId,
                'options' => [
                    'placeholder' => $this->t('Select supplier'),
                    'width' => '40%',
                    'tokenSeparators' => [',', ' '],
                ],
            ]
        ); ?>
    </div>
    <div id="no_root" class="collapse <?= $model->is_root ? '' : 'in'; ?>">
        <fieldset>
            <legend><?= $this->t('Roles') ?></legend>
            <?php if (empty($rolesData)) {
                echo '<div class="alert alert-error"> ' . $this->t('No roles to be assigned') . '</div>';
            } else {
                $form->widget('bootstrap.widgets.TbSelect2', [
                        'name' => 'rolesIds',
                        'asDropDownList' => true,
                        'data' => $rolesData,
                        'val' => CJavaScript::encode($rolesHas),
                        'options' => [
                            'placeholder' => $this->t('select role'),
                            'width' => '40%',
                            'tokenSeparators' => [',', ' '],
                        ],
                        'htmlOptions' => [
                            'multiple' => 'multiple',
                        ],
                    ]
                );
            }
            ?>
        </fieldset>
    </div>
</fieldset>

<?= CHtml::htmlButton('<i class="icon-ok icon-white"></i>' . $this->t('Save'), ['class' => 'btn btn-primary', 'type' => 'submit']); ?>
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
