<?php

use MommyCom\Controller\Backend\AdminController;

/* @var $this AdminController */

$this->pageTitle = $this->t('Manage administrators');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Administrators list'),
    'headerIcon' => 'icon-edit',
]);

$this->widget('bootstrap.widgets.TbButton', [
    'type' => 'primary',
    'label' => $this->t('Add admin'),
    'url' => $this->createUrl('add'),
]);

//render filter form
/* @var $filterForm TbActiveForm */
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo $filterForm->dropDownListRow(
    $dataProvider->model,
    'status',
    $dataProvider->model->statusReplacements(),
    ['prompt' => $this->t('Any')]
);

echo $filterForm->dateRangeRow($dataProvider->model, 'searchDateRangeInput', [
    'options' => [
        'callback' => new CJavaScriptExpression('
                function(startDate, endDate) {
                    var inputStart = $("input[name*=searchDateStart]");
                    var inputEnd = $("input[name*=searchDateEnd]");

                    if (startDate && endDate) {
                        inputStart.val(startDate.getTime()/1000);
                        inputEnd.val(endDate.getTime()/1000);
                    } else {
                        inputStart.val("");
                        inputEnd.val("");
                    }
                }'),
    ],
    'placeholder' => $this->t('Please select date'),
    'autocomplete' => 'off',
    'id' => CHtml::activeId($dataProvider->model, 'searchDateRangeInput') . '_filter',
]);
echo $filterForm->hiddenField($dataProvider->model, 'searchDateStart',
    ['id' => CHtml::activeId($dataProvider->model, 'searchDateStart') . '_filter']);
echo $filterForm->hiddenField($dataProvider->model, 'searchDateEnd',
    ['id' => CHtml::activeId($dataProvider->model, 'searchDateEnd') . '_filter',]);
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {return false;}'],
    ['class' => 'invisible']);

$this->endWidget();
//end filter form

$this->renderPartial('_index', ['dataProvider' => $dataProvider]);

$this->endWidget(); ?>
