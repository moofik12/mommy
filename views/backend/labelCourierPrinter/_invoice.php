<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model OrderRecord */
$app = $this->app();
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
?>

<div class="printable-label printable-a4 label-courier-invoice">
    <div class="label-courier-invoice-header">
        <table>
            <tbody>
                <tr>
                    <th><?= $this->t('Supplier') ?></th>
                    <td>
                        <div>ТОВ "МАМАМ"</div>
                        <div>ЄДРПОУ 38804422</div>
                        <div>Р/р 26003013020020</div>
                        <div>АТ "Сбербанк Росії" м.Києва</div>
                        <div>МФО 320627</div>
                        <div>Адреса: 03150, м. Київ, вул. Тельмана, буд. 3Б</div>
                        <div>Тел. (044) 374-50-40</div>
                    </td>
                </tr>
                <tr>
                    <th><?= $this->t('Sender') ?></th>
                    <td>ТОВ "МАМАМ"</td>
                </tr>
                <tr>
                    <th><?= $this->t('Recipient
')?></th>
                    <td><?= $model->client_name ?> <?= $model->client_surname ?></td>
                </tr>
                <tr>
                    <th><?= $this->t('Payer') ?></th>
                    <td><?= $model->client_name ?> <?= $model->client_surname ?></td>
                </tr>
                <tr>
                    <th><?= $this->t('Bank') ?></th>
                    <td><!-- clients does not has bank account --></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="label-courier-invoice-subject">
        <div class="label-courier-invoice-subject-order">
            <?= $this->t('Invoice') ?> №<?= $model->idAligned ?>
        </div>
        <div class="label-courier-invoice-subject-date">
            "____" "_________________" 20__ <?= $this->t('year')?>.
        </div>
    </div>
    <div class="label-courier-invoice-products">
        <table>
            <thead>
                <tr>
                    <td>№</td>
                    <td><?= $this->t('Name') ?></td>
                    <td>(Од.???)</td>
                    <td><?= $this->t('Quantity') ?></td>
                    <td><?= $this->t('Price') ?></td>
                    <td><?= $this->t('Amount') ?></td>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="label-courier-invoice-total"><?= $this->t('Total cost')?> <?= $currencyFormatter->format($model->getPayPrice(), $this->t('$')) ?></div>
                        <div class="label-courier-invoice-totalpay"><?= $this->t('Total cost')?>: <?= $currencyFormatter->format($model->getPayPrice(), $this->t('$')) ?></div>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach($model->positionsCallcenterAccepted as $index => $position): ?>
                    <tr>
                        <td><?= $index + 1?></td>
                        <td><?= $position->product->idAligned ?>, <?= $position->product->name ?></td>
                        <td>шт</td>
                        <td><?= $position->number ?></td>
                        <td><?= $currencyFormatter->format($position->price, $this->t('$')) ?></td>
                        <td><?= $currencyFormatter->format($position->priceTotal, $this->t('$')) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="label-courier-invoice-signatures">
        <div class="label-courier-invoice-stamp">М.П.</div>
        <div class="label-courier-invoice-signature">
            <div class="label-courier-invoice-signature-label"><?= $this->t('Chief Accountant') ?></div>
            <div class="label-courier-invoice-signature-value">
                /&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/
            </div>
        </div>
    </div>
</div>
