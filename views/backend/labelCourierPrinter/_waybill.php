<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model OrderRecord */
$app = $this->app();
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
?>

<div class="printable-label printable-a4 label-courier-waybill">
    <div class="label-courier-waybill-header">
        <div class="label-courier-waybill-company">
            <div class="label-courier-waybill-company-label">Постачальник:</div>
            <div class="label-courier-waybill-company-value">ТОВ «МАМАМ»</div>

            <div class="label-courier-waybill-company-label">Адреса:</div>
            <div class="label-courier-waybill-company-value">м.Київ вул. Тельмана буд.3Б</div>

            <div class="label-courier-waybill-company-label">Р/рахунок:</div>
            <div class="label-courier-waybill-company-value">26007013011295</div>

            <div class="label-courier-waybill-company-label">в</div>
            <div class="label-courier-waybill-company-value">АТ "Сбербанк Росії"</div>

            <div class="label-courier-waybill-company-label">МФО</div>
            <div class="label-courier-waybill-company-value">320627</div>

            <div class="label-courier-waybill-company-label">ЄДРПОУ</div>
            <div class="label-courier-waybill-company-value">38804422</div>

            <div class="label-courier-waybill-company-label">Тел./ф.</div>
            <div class="label-courier-waybill-company-value">(044) 374-50-40</div>
        </div>
        <div class="label-courier-waybill-order">
            <div class="label-courier-waybill-order-caption">НАКЛАДНА</div>
            <div class="label-courier-waybill-order-number">№ <?= $model->idAligned ?></div>
            <div class="label-courier-waybill-order-date">від "____" "_________________" 20__ p.</div>
        </div>
    </div>
    <div class="label-courier-waybill-content">
        <table>
            <tbody>
            <tr>
                <th>Одержувач</th>
                <td><?= $model->client_name ?> <?= $model->client_surname ?></td>
            </tr>
            <tr>
                <th>Платник</th>
                <td><?= $model->client_name ?> <?= $model->client_surname ?></td>
            </tr>
            <tr>
                <th>Підстава</th>
                <td>Рахунок-фактура № <?= $model->idAligned ?></td>
            </tr>
            <tr>
                <th>Через кого</th>
                <td>
                    <div class="label-courier-waybill-content-hand-written-value">&nbsp;</div>
                    (ініціали, прізвище, номер та дата видачі довіреності)
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="label-courier-invoice-products">
        <table>
            <thead>
            <tr>
                <td>№</td>
                <td>Назва</td>
                <td>Од. виміру</td>
                <td>Кількість</td>
                <td>Ціна</td>
                <td>Сума</td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td colspan="3">
                    <div class="label-courier-invoice-totalpay-words">
                        Всього (прописом): <?= $currencyFormatter->convertToWords($model->getPayPrice(), 'uk_UA') ?>
                    </div>
                </td>
                <td colspan="3">
                    <div class="label-courier-invoice-total">
                        Разом: <?= $currencyFormatter->format($model->getPayPrice(), 'UAH') ?></div>
                    <div class="label-courier-invoice-totalpay">Всього до
                        сплати: <?= $currencyFormatter->format($model->getPayPrice(), 'UAH') ?></div>
                </td>
            </tr>
            </tfoot>
            <tbody>
            <?php foreach ($model->positionsCallcenterAccepted as $index => $position): ?>
                <tr>
                    <td><?= $index + 1 ?></td>
                    <td><?= $position->product->idAligned ?>, <?= $position->product->name ?></td>
                    <td>од</td>
                    <td><?= $position->number ?></td>
                    <td><?= $currencyFormatter->format($position->price, '$') ?></td>
                    <td><?= $currencyFormatter->format($position->priceTotal, '$') ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="label-courier-waybill-footer">
        <div class="label-courier-waybill-footer-city-label">Місце складання:</div>
        <div class="label-courier-waybill-footer-city-value">м. Київ</div>

        <div class="label-courier-waybill-footer-storekeeper">Відвантажив(ла):</div>
        <div class="label-courier-waybill-footer-storekeeper-value">&nbsp;</div>
        <div class="label-courier-waybill-footer-courier">Отримав(ла):</div>
        <div class="label-courier-waybill-footer-courier-value">&nbsp;</div>
    </div>
</div>
