<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model OrderRecord */
$app = $this->app();
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
?>

<div class="printable-label printable-a4-landscape label-courier-nko">
    <div class="label-courier-nko-caption">Типова форма N КО-1</div>
    <div class="label-courier-nko-left-panel">
        <div class="label-courier-nko-left-header">
            <div>Ідентифікаційний код ЄДРПОУ <span>38804422</span></div>
            <div>ТОВ «МАМАМ»</div>
            <div>(найменування підприємства (установи, організації))</div>
        </div>
        <div class="label-courier-nko-left-order">
            <div class="label-courier-nko-left-order-label">Прибутковий касовий ордер</div>
            <div class="label-courier-nko-left-order-value">№ <?= $model->idAligned ?></div>
            <div class="label-courier-nko-left-order-date">від "____" "_________________" 20__ p.</div>
        </div>
        <div class="label-courier-nko-left-accounts">
            <table>
                <thead>
                <tr>
                    <th>Кореспондуючий рахунок, субрахунок</th>
                    <th>Код аналітичного рахунку</th>
                    <th>Сума цифрами</th>
                    <th>Код цільового призначення</th>
                    <td>&nbsp;</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="label-courier-nko-left-client">
            <div class="label-courier-nko-left-client-label">Прийнято від:</div>
            <div class="label-courier-nko-left-client-value"><?= $model->client_name ?> <?= $model->client_surname ?></div>
        </div>
        <div class="label-courier-nko-left-subject">
            <div class="label-courier-nko-left-subject-label">Підстава:</div>
            <div class="label-courier-nko-left-subject-value">Рахунок-фактура № <?= $model->idAligned ?></div>
        </div>
        <div class="label-courier-nko-left-price">
            <div class="label-courier-nko-left-price-label">Сума</div>
            <div class="label-courier-nko-left-price-value"><?= $currencyFormatter->convertToWords($model->getPayPrice(), 'uk_UA') ?></div>
        </div>
        <div class="label-courier-nko-left-comments">
            <div class="label-courier-nko-left-comments-label">Додатки:</div>
            <div class="label-courier-nko-left-comments-value">&nbsp;</div>
        </div>
        <div class="label-courier-nko-left-cashier">
            <div class="label-courier-nko-left-cashier-label">Одержав касир:</div>
            <div class="label-courier-nko-left-cashier-value">&nbsp;</div>
            <div class="label-courier-nko-left-cashier-hint">(підпис, прізвище, ініціали)</div>
        </div>
    </div>
    <div class="label-courier-nko-middle-panel">лінія&nbsp;&nbsp;&nbsp;&nbsp;відрізу</div>
    <div class="label-courier-nko-right-panel">
        <div class="label-courier-nko-right-header">
            <div>ТОВ «МАМАМ»</div>
            <div>(найменування підприємства (установи, організації))</div>
        </div>
        <div class="label-courier-nko-right-receipt">
            <div class="label-courier-nko-right-receipt-caption">Квитанція</div>
            <div class="label-courier-nko-right-receipt-subject">
                <div class="label-courier-nko-right-receipt-subject-label">до прибуткового касового ордера</div>
                <div class="label-courier-nko-right-receipt-subject-value">№ <?= $model->idAligned ?></div>
            </div>
            <div class="label-courier-nko-right-receipt-date">від "____" "_________________" 20__ p.</div>
        </div>
        <div class="label-courier-nko-right-client">
            <div class="label-courier-nko-right-client-label">Прийнято від:</div>
            <div class="label-courier-nko-right-client-value"><?= $model->client_name ?> <?= $model->client_surname ?></div>
        </div>
        <div class="label-courier-nko-right-subject">
            <div class="label-courier-nko-right-subject-label">Підстава:</div>
            <div class="label-courier-nko-right-subject-value">Рахунок-фактура № <?= $model->idAligned ?></div>
        </div>
        <div class="label-courier-nko-right-price">
            <div class="label-courier-nko-right-price-label">Сума</div>
            <div class="label-courier-nko-right-price-value"><?= $currencyFormatter->convertToWords($model->getPayPrice(), 'uk_UA') ?></div>
        </div>
        <div class="label-courier-nko-right-stamp">М.П.</div>
        <div class="label-courier-nko-right-accountant">
            <div class="label-courier-nko-right-accountant-label">Головний бухгалтер:</div>
            <div class="label-courier-nko-right-accountant-value">&nbsp;</div>
            <div class="label-courier-nko-right-accountant-hint">(підпис, прізвище, ініціали)</div>
        </div>
        <div class="label-courier-nko-right-cashier">
            <div class="label-courier-nko-right-cashier-label">Касир:</div>
            <div class="label-courier-nko-right-cashier-value">&nbsp;</div>
            <div class="label-courier-nko-right-cashier-hint">(підпис, прізвище, ініціали)</div>
        </div>
    </div>
</div>
