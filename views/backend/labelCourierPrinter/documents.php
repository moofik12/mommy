<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CDataProvider */
$this->pageTitle = $this->t('Consignment - Print');
$app = $this->app();
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
?>

<?php foreach ($provider->getData() as $model): /* @var $model OrderRecord */ ?>
    <?php $this->renderPartial('_nko', ['model' => $model]) ?>
    <?php /*$this->renderPartial('_invoice', array('model' => $model))*/ ?>
    <?php $this->renderPartial('_waybill', ['model' => $model]) ?>
    <?php $this->renderPartial('_waybill', ['model' => $model]) ?>
<?php endforeach ?>
