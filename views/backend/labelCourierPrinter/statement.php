<?php

use MommyCom\Model\Db\StatementRecord;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model StatementRecord */
$this->pageTitle = $this->t('Delivery-Acceptance Act - Print');
$app = $this->app();
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
?>


<div class="printable-label printable-a4 printable-a4-multi-paged label-courier-statement">
    <div class="label-courier-statement-caption">
        <?= $this->t('Delivery-Acceptance Act') ?>
    </div>
    <div class="label-courier-statement-date">
        <?= $this->t('Created') ?>: <?= $dateFormatter->formatDateTime($model->created_at, 'full', null) ?></div>
    <div class="label-courier-statement-products">
        <table>
            <thead>
            <tr>
                <td><?= $this->t('№') ?></td>
                <td><?= $this->t('Order №') ?></td>
                <td><?= $this->t('Weight') ?></td>
                <td><?= $this->t('Cost') ?></td>
                <td><?= $this->t('Shipping cost') ?></td>
                <td><?= $this->t('Shipping info') ?></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->positions as $index => $position): ?>
                <tr>
                    <td class="label-courier-statement-products-column-index">
                        <?= $index + 1 ?>
                    </td>
                    <td class="label-courier-statement-products-column-order">
                        <?= $position->order->idAligned ?>
                    </td>
                    <td class="label-courier-statement-products-column-weight">
                        <?= ProductWeight::instance()->convertToString($position->order->weight) ?>
                    </td>
                    <td class="label-courier-statement-products-column-price">
                        <?= $currencyFormatter->format($position->order->payPrice, 'UAH') ?>
                    </td>
                    <td class="label-courier-statement-products-column-delivery-price">
                        <?= $currencyFormatter->format($position->order->deliveryPrice, 'UAH') ?>
                    </td>
                    <td class="label-courier-statement-products-column-receiver">
                        , <?= $position->order->getDeliveryAttribute('address', '') ?>,
                        <?= $position->order->client_name ?> <?= $position->order->client_surname ?>,
                        <?= $position->order->telephone ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>
    <div class="label-courier-statement-footer">
        <div class="label-courier-statement-sender">
            <div class="label-courier-statement-sender-label">
                <?= $this->t('Consignor') ?>
            </div>
            <div class="label-courier-statement-sender-value">&nbsp;</div>
        </div>
        <div class="label-courier-statement-receiver">
            <div class="label-courier-statement-receiver-label">
                <?= $this->t('Receiver-Consignee') ?></div>
            <div class="label-courier-statement-receiver-value">&nbsp;</div>
        </div>
    </div>
</div>
