<?php

use MommyCom\Controller\Backend\CallcenterTaskController;

/**
 * @var $this CallcenterTaskController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Actions list');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List'),
    'headerIcon' => 'icon-edit',
]);

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'link',
    'url' => $this->createUrl('callcenterTask/operationCreate'),
    'label' => $this->t('Create action'),
    'type' => 'primary',
]);

$this->renderPartial('operation/_index', [
    'provider' => $provider,
]);

$this->endWidget(); ?>
