<?php

use MommyCom\Controller\Backend\CallcenterTaskController;

/**
 * @var $this CallcenterTaskController
 * @var $form TbForm
 */

$this->pageTitle = $this->t('Update actions');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Update'),
    'headerIcon' => 'icon-edit',
]);

echo $form->render();

$this->endWidget(); ?>
