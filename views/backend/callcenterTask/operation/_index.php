<?php

use MommyCom\Controller\Backend\CallcenterTaskController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\CallcenterTaskOperationRecord;

/**
 * @var $this CallcenterTaskController
 * @var $provider CActiveDataProvider
 */
?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'city-grid',
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = '';
        /** @var $data CallcenterTaskOperationRecord */
        if ($data->is_deleted) {
            $cssClass = 'error';
        }

        return $cssClass;
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'filter' => false,
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        [
            'name' => 'title',
        ],
        [
            'type' => 'boolean',
            'filter' => [$this->t('No'), $this->t('Yes')],
            'name' => 'is_deleted',
        ],
        [
            'class' => ButtonColumn::class,
            'openNewWindow' => false,
            'template' => '{update} {delete}',
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("operationUpdate",array("id"=>$data->primaryKey))',
            'deleteButtonUrl' => '$this->app()->controller->createUrl("operationDelete",array("id"=>$data->primaryKey))',
        ],
    ],
]);
?>
