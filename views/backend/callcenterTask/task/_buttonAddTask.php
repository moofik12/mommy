<?php
$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'link',
    'url' => $this->createUrl('callcenterTask/taskCreate'),
    'label' => $this->t('Create task'),
    'type' => 'primary',
]);
