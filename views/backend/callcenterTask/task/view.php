<?php

use MommyCom\Controller\Backend\CallcenterTaskController;
use MommyCom\Model\Db\CallcenterTaskRecord;

/**
 * @var $this CallcenterTaskController
 * @var $provider CActiveDataProvider
 * @var $model CallcenterTaskRecord
 */

$this->pageTitle = $this->t('View task');

$request = $this->app()->request;
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Task') . ' №' . $model->id,
    'headerIcon' => 'icon-edit',
]);

if (!$this->app()->request->isAjaxRequest) {
    $this->widget('bootstrap.widgets.TbButton', [
        'buttonType' => 'link',
        'url' => $request->urlReferrer,
        'label' => $this->t('Back'),
        'type' => 'link',
    ]);
}

$this->widget('bootstrap.widgets.TbDetailView', [
    'type' => ['striped', 'bordered'],
    'data' => $model,
    'attributes' => [
        [
            'name' => 'operation_id',
            'value' => isset($model->operation) ? $model->operation->title : '',
        ],
        [
            'name' => 'status',
            'value' => $model->statusReplacement(),
        ],
        [
            'name' => 'performed_admin_id',
            'value' => isset($model->performedAdmin)
                ? $model->performedAdmin->login . " ({$model->performedAdmin->fullname})"
                : '',
        ],
        [
            'name' => 'order_id',
            'label' => $this->t('Order'),
            'value' => $model->order_id,
        ],
        [
            'name' => 'comment_task',
        ],
        [
            'type' => 'raw',
            'name' => 'comment_callcenter',
        ],
    ],
]);

$this->endWidget(); ?>
