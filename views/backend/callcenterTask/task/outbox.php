<?php

use MommyCom\Controller\Backend\CallcenterTaskController;

/**
 * @var $this CallcenterTaskController
 * @var $provider CActiveDataProvider
 * @var int $taskType
 */

$this->pageTitle = $this->t('Outcoming');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List'),
    'headerIcon' => 'icon-edit',
]);

$this->renderPartial('task/_filter', ['taskType' => $taskType, 'model' => $provider->model]);

$this->renderPartial('task/_buttonAddTask');

$this->renderPartial('task/_index', [
    'provider' => $provider,
]);

$this->endWidget(); ?>
