<?php

use MommyCom\Controller\Backend\CallcenterTaskController;

/**
 * @var $this CallcenterTaskController
 * @var $form TbForm
 */

$this->pageTitle = $this->t('Editing task');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Editing task') . ' №' . $form->model->id,
    'headerIcon' => 'icon-edit',
]);

echo $form->render();

$this->endWidget(); ?>
