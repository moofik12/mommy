<?php

use MommyCom\Controller\Backend\CallcenterTaskController;
use MommyCom\Model\Db\CallcenterTaskRecord;

/**
 * @var $this CallcenterTaskController
 * @var $provider CActiveDataProvider
 * @var int $taskType
 */

$this->pageTitle = $this->t('Incoming');

/** @var CallcenterTaskRecord $model */
$model = $provider->model;
$user = $this->app()->user;
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List'),
    'headerIcon' => 'icon-edit',
]);

$this->renderPartial('task/_filter', ['taskType' => $taskType, 'model' => $provider->model]);

$this->renderPartial('task/_buttonAddTask');

$this->renderPartial('task/_index', [
    'provider' => $provider,
    'columns' => [
        'operation_id' => [
            'name' => 'operation_id',
            'filter' => false,
            'type' => 'raw',
            'value' => function ($data) use ($user) {
                /** @var CallcenterTaskRecord $data */
                $text = $data->operation->title;
                if ($data->admin_id == $user->id && !$data->is_callcenter_answer_read) {
                    $text = '<strong>' . $data->operation->title . '</strong>';
                }

                return $text;
            },
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'url' => $this->createUrl('callcenterTask/taskView'),
        ],
    ],
]);

$this->endWidget(); ?>
