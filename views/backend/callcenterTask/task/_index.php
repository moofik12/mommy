<?php

use MommyCom\Controller\Backend\CallcenterTaskController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\CallcenterTaskOperationRecord;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\YiiComponent\ArrayUtils;

/**
 * @var $this CallcenterTaskController
 * @var $provider CActiveDataProvider
 */

/** @var CallcenterTaskRecord $model */
$model = $provider->model;
$user = $this->app()->user;

$operationData = ArrayUtils::getColumn(CallcenterTaskOperationRecord::model()->findAll(), 'title', 'id');
$columns = isset($columns) ? $columns : [];
?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'city-grid',
    'dataProvider' => $provider,
    'filter' => $model,
    'filterSelector' => '{filter}, form[id=task-filter-form] :input',
    'columns' => CMap::mergeArray([
        'id' => [
            'name' => 'id',
            'header' => '#',
            'htmlOptions' => ['style' => 'width:1%;'],
        ],
        'admin_id' => [
            'name' => 'admin_id',
            'value' => function ($data) {
                $name = '';

                if ($data->admin) {
                    $name = $data->admin->fullname;
                }

                return $name;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        'operation_id' => [
            'name' => 'operation_id',
            'filter' => $operationData,
            'value' => '$data->operation->title',
            'type' => 'raw',
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'url' => $this->createUrl('callcenterTask/taskView'),
        ],
        'status' => [
            'name' => 'status',
            'filter' => $this->action->id === 'task' ? $model->statusReplacements() : false,
            'value' => '$data->statusReplacement()',
            'htmlOptions' => ['class' => 'span2'],
        ],
        'order_id' => [
            'name' => 'order_id',
            'value' => function ($data) {
                /** @var $data CallcenterTaskRecord */
                return empty($data->order_id) ? '' : $data->order_id;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        'comment_task' => [
            'name' => 'comment_task',
        ],
        'percentStatus' => [
            'name' => 'percentStatus',
            'header' => $this->t('Status'),
            'type' => 'raw',
            'filter' => false,
            'value' => function ($data) {
                /** @var CallcenterTaskRecord $data */
                $percent = $data->statusPercent();
                $cssClass = [
                    CallcenterTaskRecord::STATUS_COMPLETED => 'bar-success',
                    CallcenterTaskRecord::STATUS_FAILED => 'bar-danger',
                ];

                $addCssClass = isset($cssClass[$data->status]) ? $cssClass[$data->status] : '';

                $text = '<div class="progress progress-striped">';
                $text .= '<div class="bar ' . $addCssClass . '" style="width:' . $percent . '%;"></div>';
                $text .= '</div>';

                return $text;
            },
        ],
        'created_at' => [
            'name' => 'created_at',
            'filter' => false,
            'value' => function ($data) {
                $text = '';
                /** @var $data CallcenterTaskRecord */
                if ($data->created_at > 0) {
                    $text = $this->app()->getDateFormatter()->formatDateTime($data->created_at, 'short');
                }
                return $text;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        'performed_at' => [
            'name' => 'performed_at',
            'filter' => false,
            'value' => function ($data) {
                $text = '';
                /** @var $data CallcenterTaskRecord */
                if ($data->performed_at > 0) {
                    $text = $this->app()->getDateFormatter()->formatDateTime($data->performed_at, 'short');
                }
                return $text;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        'buttons' => [
            'class' => ButtonColumn::class,
            'openNewWindow' => false,
            'template' => '{view} {update} {delete}',
            'viewButtonUrl' => '\Yii::app()->controller->createUrl("taskView",array("id"=>$data->primaryKey))',
            'updateButtonUrl' => '$this->app()->controller->createUrl("taskUpdate",array("id"=>$data->primaryKey))',
            'deleteButtonUrl' => '$this->app()->controller->createUrl("taskDelete",array("id"=>$data->primaryKey))',
            'buttons' => [
                'delete' => [
                    'visible' => function ($row, $data) use ($user) {
                        /** @var CallcenterTaskRecord $data */
                        $result = false;

                        if ($data->admin_id == $user->id) {
                            $result = true;
                        }

                        return $result;
                    },
                ],
                'update' => [
                    'visible' => function ($row, $data) use ($user) {
                        /** @var CallcenterTaskRecord $data */
                        $result = false;

                        if ($data->admin_id == $user->id) {
                            $result = true;
                        }

                        return $result;
                    },
                ],
            ],
        ],
    ], $columns),
]);
?>
