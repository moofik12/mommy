<?php

use MommyCom\Controller\Backend\CallcenterTaskController;
use MommyCom\Model\Db\CallcenterTaskRecord;

/**
 * @var $this CallcenterTaskController
 * @var int $taskType
 * @var $model CallcenterTaskRecord
 * @var $filterForm TbActiveForm
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'task-filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo CHtml::dropDownList('taskType', $taskType, [
    $this->t('My Tasks'),
    $this->t('All tasks'),
    $this->t('System'),
]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
