<?php
/**
 * @var $this FeedbackController
 * @var $dataProvider CActiveDataProvider
 */

use MommyCom\Controller\Backend\FeedbackController;

$this->pageTitle = $this->t('View the list of processed requests');
$grid = $this->renderPartial('_grid', ['dataProvider' => $dataProvider], true);
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View the list of processed requests'),
    'headerIcon' => 'icon-list',
]);
?>

<?php $this->renderPartial('_tabs', ['content' => $grid]) ?>
<?php $this->endWidget(); ?>
