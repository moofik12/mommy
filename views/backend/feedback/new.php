<?php

use MommyCom\Controller\Backend\FeedbackController;

/**
 * @var $this FeedbackController
 * @var $dataProvider CActiveDataProvider
 */
$this->pageTitle = $this->t('View new requests list');
$grid = $this->renderPartial('_grid', ['dataProvider' => $dataProvider], true);
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View new requests list'),
    'headerIcon' => 'icon-list',
]);
?>

<?php $this->renderPartial('_tabs', ['content' => $grid]) ?>
<script>
    $(function () {
        var grid = $('.grid-view');
        $(document).on('click', "a[data-action='make_cancel']", function (e) {
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: $(this).attr('href'),
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        grid.yiiGridView("update");
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        });
    });
</script>
<?php $this->endWidget(); ?>
