<?php

use MommyCom\Controller\Backend\FeedbackController;
use MommyCom\Model\Db\FeedbackRecord;

/**
 * @var $this FeedbackController
 * @var $dataProvider CActiveDataProvider
 * **/

/* @var $data FeedbackRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'grid-view',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Feedback {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form[id=order-filter-form] :input',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'user_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                $text = $data->user_id;
                if ($data->user !== null) {
                    $text .= ', ' . $data->user->email;
                }
                return $text;
            },
        ],
        [
            'name' => 'message',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                return nl2br($data->message);
            },
        ],
        [
            'name' => 'message_answer',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                return nl2br($data->message_answer);
            },
            'visible' => $this->action->id == 'history',
        ],
        [
            'name' => 'type',
            'filter' => FeedbackRecord::typeReplacements(),
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                return $data->typeReplacement;
            },
        ],
        [
            'name' => 'status',
            'filter' => FeedbackRecord::statusReplacements(),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                $cssClass = '';
                if ($data->status == FeedbackRecord::STATUS_PROCESSED) {
                    $cssClass = 'label-success';
                } elseif ($data->status == FeedbackRecord::STATUS_CANCEL) {
                    $cssClass = 'label-warning';
                }
                $text = ' <span class="label ' . $cssClass . '">' . $data->statusReplacement . '</span>';
                return $text;
            },
            'visible' => $this->action->id == 'history',
        ],
        [
            'name' => 'admin_id',
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                $text = '';
                if ($data->admin !== null) {
                    $text .= $data->admin->displayName;
                }
                return $text;
            },
            'visible' => $this->action->id == 'history',
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false, 'visible' => $this->action->id == 'history'],
        [
            'type' => 'raw',
            'name' => 'created_at',
            'header' => false,
            'filter' => false,
            'visible' => $this->action->id == 'new',
            'value' => function ($data) {
                /* @var $data FeedbackRecord */
                $text = $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'link',
                    'type' => 'success',
                    'label' => $this->t('Reply'),
                    'htmlOptions' => [
                        'class' => 'pull-right',
                    ],
                    'url' => $this->app()->createUrl('feedback/answer', ['id' => $data->id]),
                ], true);
                $text .= '<br>';
                $text .= $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'link',
                    'type' => 'danger',
                    'label' => $this->t('Cancel'),
                    'htmlOptions' => [
                        'class' => 'pull-right',
                        'data-action' => 'make_cancel',
                        'style' => 'margin-top: 5px;',
                    ],
                    'url' => $this->app()->createUrl('feedback/cancel', ['id' => $data->id]),
                ], true);
                return $text;
            },
            'headerHtmlOptions' => [
                'style' => 'width: 2%;',
            ],
        ],
    ],
]);
?>
