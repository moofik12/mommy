<?php

use MommyCom\Controller\Backend\FeedbackController;
use MommyCom\Model\Backend\AnswerForm;
use MommyCom\Model\Db\FeedbackRecord;

/**
 * @var $this FeedbackController
 * @var $model FeedbackRecord
 * @var $answerForm AnswerForm
 */

$this->pageTitle = $this->t('Ответ на обращение № ') . $model->id;
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Write a response'),
    'headerIcon' => 'icon-add',
]); ?>
<?php
$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        ['name' => 'id', 'label' => '#'],
        [
            'label' => $this->t('Name'),
            'name' => 'user.name',
        ],
        [
            'label' => $this->t('Last name'),
            'name' => 'user.surname',
        ],
        [
            'label' => $this->t('E-mail'),
            'name' => 'user.email',
        ],
        [
            'name' => 'type',
            'value' => $model->getTypeReplacement(),
        ],
        [
            'type' => 'raw',
            'name' => 'message',
            'value' => nl2br($model->message),
        ],
        [
            'label' => $this->t('Last edit'),
            'name' => 'updated_at',
            'value' => $this->app()->getDateFormatter()->formatDateTime($model->updated_at),
        ],
        [
            'label' => $this->t('Left a message'),
            'name' => 'created_at',
            'value' => $this->app()->getDateFormatter()->formatDateTime($model->created_at),
        ],
    ],
]); ?>
<?= $answerForm->render() ?>
<?php $this->endWidget() ?>
