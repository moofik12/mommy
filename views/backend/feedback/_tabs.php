<?php

use MommyCom\Controller\Backend\FeedbackController;

/**
 * @var $this FeedbackController
 * @var $content string
 */
?>
<?php
$this->widget(
    'bootstrap.widgets.TbTabs',
    [
        'type' => 'tabs',
        //'placement' => 'left',
        'tabs' => [
            [
                'label' => $this->t('New requests'),
                'content' => $this->action->id == 'new' ? $content : '',
                'active' => $this->action->id == 'new' ? true : false,
                'url' => $this->createUrl('feedback/new'),
            ],
            [
                'label' => $this->t('Requests history'),
                'content' => $this->action->id == 'history' ? $content : '',
                'active' => $this->action->id == 'history' ? true : false,
                'url' => $this->createUrl('feedback/history'),
            ],
        ],
    ]
);
?>
