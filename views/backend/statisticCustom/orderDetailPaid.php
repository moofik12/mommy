<?php

use MommyCom\Model\Db\OrderBuyoutRecord;

/**
 * @var $this CController
 * @var CArrayDataProvider $notConfirmProvider
 */

$this->pageTitle = $this->t('Statistics of order payments received');
$nf = $this->app()->getNumberFormatter();
?>

<?
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Problematic orders'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<h4> <?= $this->t('Orders that have been paid for but no refund for the order confirmed or waybill for return goods created') ?> </h4>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'dataProvider' => $notConfirmProvider,
    'columns' => [
        [
            'name' => 'order',
            'header' => $this->t('Order'),
            'value' => function ($data) {
                /** @var $data OrderBuyoutRecord */
                return $data->order_id;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'delivery_type',
            'header' => $this->t('Shipping method'),
            'value' => function ($data) {
                /** @var $data OrderBuyoutRecord */
                return $data->order->getDeliveryTypeReplacement();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'price',
            'header' => $this->t('Cost'),
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'ttn',
            'header' => $this->t('Waybill'),
            'value' => function ($data) {
                /** @var $data OrderBuyoutRecord */
                return $data->order->trackcode;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'return_ttn',
            'header' => $this->t('Waybill for return goods'),
            'value' => function ($data) {
                /** @var $data OrderBuyoutRecord */
                return $data->order->tracking->trackcode_return;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'return_ttn',
            'header' => $this->t('print waybill'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data OrderBuyoutRecord */
                return CHtml::link($this->t('print waybill'), 'javascript: alert("Новапошты больше нет!")', [
                    'class' => 'btn btn-link',
                    'target' => '_blank',
                ]);
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
    ],
]); ?>

<?php $this->endWidget(); ?>
