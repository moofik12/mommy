<?php

use MommyCom\Model\Statistic\StatisticOrdersBuyoutProductModel;

/**
 * @var StatisticOrdersBuyoutProductModel[] $data
 */

$showValues = ['all', 'sold', 'returned', 'notBought'];
$show = isset($show) ? $show : 'all';

$dataProvider = new CArrayDataProvider($data, [
    'keyField' => 'productID',
    'pagination' => [
        'pageSize' => 50,
    ],
    'sort' => [
        'sortVar' => 'suppliersProductsSort',
        'attributes' => [
            'productID', 'price', 'pricePurchase', 'count', 'countReturned', 'countNotBought',
        ],
        'defaultOrder' => [
            'count' => CSort::SORT_DESC,
            'price' => CSort::SORT_DESC,
        ],
    ],
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'template' => "{items}",

    'columns' => [
        [
            'name' => 'productID',
            'header' => 'ID',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var StatisticOrdersBuyoutProductModel $data */
                $text = $data->productID;
                if ($data->eventID && $data->baseProductID) {
                    $text = CHtml::link($text, $this->app()->frontendUrlManager->createUrl('product/index'
                        , ['id' => $data->baseProductID, 'eventId' => $data->eventID])
                        , ['target' => '_blank']
                    );
                }

                return $text;
            },
        ],
        [
            'name' => 'eventID',
            'header' => $this->t('Flash-sale'),
        ],
        [
            'name' => 'price',
            'header' => $this->t('Amount'),
        ],
        [
            'name' => 'count',
            'header' => $this->t('Qty'),
            'visible' => $show == 'all' || $show == 'sold',
        ],
        [
            'name' => 'countReturned',
            'header' => $this->t('Items returned'),
            'visible' => $show == 'all' || $show == 'returned',
        ],
        [
            'name' => 'countNotBought',
            'header' => $this->t('Unfulfilled'),
            'visible' => $show == 'all' || $show == 'notBought',
        ],
        [
            'name' => 'brandManagerName',
            'header' => $this->t('Manager'),
        ],
        [
            'name' => 'name',
            'header' => $this->t('Name'),
        ],
    ],
]);
