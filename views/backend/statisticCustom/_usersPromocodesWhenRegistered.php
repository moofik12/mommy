<?php

use MommyCom\Model\Statistic\StatisticUsersModel;
use MommyCom\Model\Statistic\StatisticUsersModelPromocodesWhenRegistering;

/**
 * @var $data StatisticUsersModel
 */

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $data->getPromocodesHelper(),
    'attributes' => [
        [
            'name' => $this->t('Amount received'),
            'value' => function ($data) {
                /* @var StatisticUsersModelPromocodesWhenRegistering $data */
                return $data->getCountUserPromocodes();
            },
        ],
        [
            'name' => $this->t('Amount spent'),
            'value' => function ($data) {
                /* @var StatisticUsersModelPromocodesWhenRegistering $data */
                return $data->getCountOrdersPromocodes();
            },
        ],
        [
            'name' => $this->t('Orders total on this day'),
            'value' => function ($data) {
                /* @var StatisticUsersModelPromocodesWhenRegistering $data */
                return $data->ordersAmount;
            },
        ],
        [
            'name' => $this->t('Bonus money'),
            'value' => function ($data) {
                /* @var StatisticUsersModelPromocodesWhenRegistering $data */
                return $data->promocodeUsedAmount;
            },
        ],
    ],
]);
