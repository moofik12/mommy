<?php

use MommyCom\Model\Statistic\StatisticOrdersModel;
use MommyCom\Model\Statistic\StatisticOrdersPaidModel;
use MommyCom\YiiComponent\Statistic\StatisticTimeFilter;

/**
 * @var StatisticOrdersModel|StatisticOrdersPaidModel $data
 */

if (!$data instanceof StatisticOrdersPaidModel) {
    echo $this->t('Payment data collection disabled');
    return;
}

if ($data->getStatistic()->getTimeFilter()->getPeriod() != StatisticTimeFilter::PERIOD_DAY) {
    echo $this->t('Payment data collection is only available per day');
    return;
}

$dataProvider = new CArrayDataProvider([$data->getPaid()], [
    'pagination' => [
        'pageSize' => 50,
    ],
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'template' => "{items}",

    'columns' => [
        [
            'name' => 'amount',
            'header' => $this->t('Amount'),
        ],
        [
            'name' => 'cardsAmount',
            'header' => $this->t('Card payment'),
        ],
        [
            'name' => 'novaPostaSender',
            'header' => $this->t('New Post payment'),
        ],
        [
            'name' => 'novaPostaSenderConfirm',
            'header' => 'Payment confirmed with New Post',
        ],
        [
            'name' => 'courierSender',
            'header' => $this->t('COD payment'),
        ],
        [
            'name' => 'courierSenderConfirm',
            'header' => 'Payment confirmed by courier',
        ],
    ],
]);

echo CHtml::link($this->t('Learn more'),
    ['statisticCustom/orderDetailPaid', 'from' => $data->getDateTimeFrom()->getTimestamp(), 'to' => $data->getDateTimeTo()->getTimestamp()],
    ['target' => '_blank']
);
