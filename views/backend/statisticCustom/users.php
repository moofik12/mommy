<?php

use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Statistic\StatisticUsersModel;
use MommyCom\YiiComponent\Statistic\Statistic;
use MommyCom\YiiComponent\Statistic\StatisticDataProvider;
use MommyCom\YiiComponent\Statistic\StatisticTimeFilter;

/**
 * @var $this CController
 * @var $provider StatisticDataProvider
 * @var $statistic Statistic
 * @var $statisticUsers Statistic
 */

$this->pageTitle = $this->t('User statistics');
$nf = $this->app()->getNumberFormatter();
$statisticUsers = $statistic->getTotalModel();
$pathAssets = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);

$this->app()->clientScript->registerCssFile($pathAssets . '/css/customStatistic.css');
?>

<?php

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('User statistics'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<div class="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?= $this->t('Statistics by e-mail confirmations') ?>!
</div>

<?php $this->renderPartial('_filters', ['statistic' => $statistic]) ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'template' => "{summary}\n{items}\n{pager}\n{extendedSummary}",
    'summaryText' => $statistic->getTimeFilter()->getPeriodName() . ' {start} - {end} out of {count}',
    'selectableRows' => 0,
    'filterSelector' => '{filter}, #statistic-filter input:not([data-not-filter]), #statistic-filter select, #statistic-filter textarea',
    'ajaxUpdate' => "totalInfo",
    'columns' => [
        [
            'name' => 'days',
            'header' => $statistic->getTimeFilter()->getPeriodName(),
            'value' => function ($data) use ($statistic) {
                /** @var StatisticUsersModel $data */
                $date = $this->app()->getDateFormatter()->formatDateTime($data->day, 'short', false);

                if ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
                    $date = $this->app()->getDateFormatter()->format('d MMMM y \'г\'., EEEE', $data->day);
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_WEEK) {
                    $date = $this->app()->getDateFormatter()->format('w \'неделя\' y \'г\'.', $data->day);
                    $date .= ", ({$data->getDateTimeFrom()->format('d.m')} - {$data->getDateTimeTo()->format('d.m')})";
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_MONTH) {
                    $date = $this->app()->getDateFormatter()->format('LLLL y \'г\'.', $data->day);
                }

                return $date;
            },
        ],
        [
            'header' => $this->t('Info'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                $info = '';

                if ($data->getUserOffersHistory()) {
                    $info .= CHtml::tag('span', [
                        'name' => "event-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => 'Flash-sales',
                        'data-content' => $this->renderPartial('_usersOffers',
                            ['data' => $data->getUserOffersHistory()], true),
                    ], 'Flash-sales');
                }

                if (count($data->getUserEmails()) > 0) {
                    if (!empty($info)) {
                        $info .= '<br>';
                    }
                    $info .= CHtml::tag('span', [
                        'name' => "users-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => 'Emails',
                        'data-content' => $this->renderPartial('_userEmails',
                            ['data' => $data->getUserEmails()], true),
                    ], 'Users');
                }

                return $info;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'header' => 'Qty',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->countEmailVerified;
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
        [
            'header' => '<span title="' . $this->t('Registration without an offer') . '" data-toggle="tooltip" >' . $this->t('Organic') . '<i class="icon-exclamation-sign"></i></span>',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->countCreatedOrganicEmailVerified;
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
        [
            'header' => $this->t('Registration promo codes '),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                $info = "{$data->getPromocodesHelper()->getCountUserPromocodes()} ({$data->getPromocodesHelper()->getCountOrdersPromocodes()})";

                if ($data->getUserOffersHistory()) {
                    $info = CHtml::tag('span', [
                        'name' => "event-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $this->t('Registration promo codes'),
                        'data-content' => $this->renderPartial('_usersPromocodesWhenRegistered',
                            ['data' => $data], true),
                    ], $info);
                }

                return $info;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'header' => $this->t('Bonuses at registration'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                $info = "{$data->totalReceivedBonusesWhenRegistering} ({$data->countReceivedBonusesWhenRegistering})";

                if ($data->getUserOffersHistory()) {
                    $info = CHtml::tag('span', [
                        'name' => "event-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $this->t('Bonuses at registration'),
                        'data-content' => $this->renderPartial('_usersBonusesWhenRegistered',
                            ['data' => $data], true),
                    ], $info);
                }

                return $info;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'header' => $this->t('Bonuses used during registration'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->totalSpentBonusesWhenRegistering;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'header' => $this->t('Bonuses applied during registration'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->totalBurnedBonusesWhenRegistering;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'header' => $this->t('Landed from search engines'),
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->countMovedSearchEngine;
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
        [
            'header' => 'Offer',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->countCreatedOfferEmailVerified;
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
        [
            'header' => '<span title="Offer - Admitad - Actionpay" data-toggle="tooltip" >Offer (context)<i class="icon-exclamation-sign"></i></span>',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->countCreatedOfferEmailVerified
                    - $data->getStatisticCreatedOfferEmailVerified(UserRecord::OFFER_PROVIDER_ADMITAD)
                    - $data->getStatisticCreatedOfferEmailVerified(UserRecord::OFFER_PROVIDER_ACTIONPAY);
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
        [
            'name' => 'Admitad',
            'header' => 'Admitad',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->getStatisticCreatedOfferEmailVerified(UserRecord::OFFER_PROVIDER_ADMITAD);
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
        [
            'name' => 'Actionpay',
            'header' => 'Actionpay',
            'value' => function ($data) {
                /** @var $data StatisticUsersModel */
                return $data->getStatisticCreatedOfferEmailVerified(UserRecord::OFFER_PROVIDER_ACTIONPAY);
            },
            'htmlOptions' => [
                'class' => 'span2',
            ],
        ],
    ],
]);
?>

<div id="totalInfo" class="well pull-right extended-summary" style="width:300px">
    <h4><?= $this->t('Total') ?>: </h4>
    <strong><?= $this->t('Registered') ?>: </strong><?= $nf->formatDecimal($statisticUsers->count) ?><br>

    <strong><?= $this->t('Organic') ?>: </strong><?= $nf->formatDecimal($statisticUsers->countCreatedOrganic) ?><br>

    <strong><?= $this->t('Offer') ?>: </strong><?= $nf->formatDecimal($statisticUsers->countCreatedOffer) ?><br>


    <h4><?= $this->t('E-mail confirmation') ?>: </h4>
    <strong><?= $this->t('Registered') ?>: </strong><?= $nf->formatDecimal($statisticUsers->countEmailVerified) ?><br>

    <strong><?= $this->t('Organic') ?>: </strong><?= $nf->formatDecimal($statisticUsers->countCreatedOrganicEmailVerified) ?><br>

    <strong><?= $this->t('Offer') ?>: </strong><?= $nf->formatDecimal($statisticUsers->countCreatedOfferEmailVerified) ?><br>

    <strong>Admitad: </strong><?= $nf->formatDecimal($statisticUsers->getStatisticCreatedOfferEmailVerified(UserRecord::OFFER_PROVIDER_ADMITAD)) ?>
    <br>
    <strong>Actionpay: </strong><?= $nf->formatDecimal($statisticUsers->getStatisticCreatedOfferEmailVerified(UserRecord::OFFER_PROVIDER_ACTIONPAY)) ?>
    <br>
</div>

<?php $this->endWidget(); ?>
