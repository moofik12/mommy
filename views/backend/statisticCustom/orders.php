<?php

use MommyCom\Model\Statistic\StatisticOrdersModel;
use MommyCom\Model\Statistic\StatisticTotalOrdersModel;
use MommyCom\YiiComponent\Statistic\Statistic;
use MommyCom\YiiComponent\Statistic\StatisticDataProvider;
use MommyCom\YiiComponent\Statistic\StatisticTimeFilter;

/**
 * @var $this CController
 * @var $provider StatisticDataProvider
 * @var $statistic Statistic
 * @var $ordersForDays StatisticTotalOrdersModel
 * @var $short bool
 */

$this->pageTitle = $this->t('Order statistics');
$nf = $this->app()->getNumberFormatter();
$ordersForDays = $statistic->getTotalModel();
$pathAssets = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);

$this->app()->clientScript->registerCssFile($pathAssets . '/css/customStatistic.css');

?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order statistics'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php $this->renderPartial('_filters', ['statistic' => $statistic]) ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'summaryText' => $statistic->getTimeFilter()->getPeriodName() . ' {start} - {end} out of {count}',
    'selectableRows' => 0,
    'filterSelector' => '{filter}, #statistic-filter input, #statistic-filter select',
    'ajaxUpdate' => "totalInfo",
    'rowCssClassExpression' => function ($row, $data) use ($statistic) {
        if ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
            $dayNumber = $this->app()->dateFormatter->format('e', $data->day);

            if ($dayNumber == 1 || $dayNumber == 3 || $dayNumber == 5) {
                return 'error';
            }
        }

        return '';
    },
    'columns' => [
        [
            'name' => 'days',
            'type' => 'raw',
            'header' => $statistic->getTimeFilter()->getPeriodName(),
            'value' => function ($data, $row) use ($statistic, $short) {
                /** @var StatisticOrdersModel $data */
                $date = $this->app()->getDateFormatter()->formatDateTime($data->day, 'short', false);

                if ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
                    $date = $this->app()->getDateFormatter()->format('d MMMM y \'г\'., EEEE', $data->day);
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_WEEK) {
                    $date = $this->app()->getDateFormatter()->format('w \'week\' y \'г\'.', $data->day);
                    $date .= ", ({$data->getDateTimeFrom()->format('d.m')} - {$data->getDateTimeTo()->format('d.m')})";
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_MONTH) {
                    $date = $this->app()->getDateFormatter()->format('LLLL y \'г\'.', $data->day);
                }

                $text = '';

                if (!$short) {
                    $params = $_GET;
                    if (isset($params[$this->app()->urlManager->routeVar])) {
                        unset($params[$this->app()->urlManager->routeVar]);
                    }
                    $params['toFilePosition'] = $row;
                    $text = $this->widget('bootstrap.widgets.TbButtonGroup', [
                        'buttons' => [
                            [
                                'type' => 'info',
                                'label' => '',
                                'items' => [
                                    [
                                        'label' => $this->t('Data on users'),
                                        'url' => $this->createUrl('orders', $params),
                                    ],
                                ],
                            ],
                        ],
                        'htmlOptions' => ['style' => 'float: right'],
                    ], true);
                }

                return $date . $text;
            },
        ],
        [
            'name' => 'info',
            'header' => $this->t('Info'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                $info = '';

                if ($data->getUserOffersHistory()) {
                    $info .= CHtml::tag('span', [
                        'name' => "event-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'right',
                        'data-html' => 'true',
                        'data-title' => 'Flash-sales',
                        'data-content' => $this->renderPartial('_ordersOffers',
                            ['data' => $data->getUserOffersHistory([])], true),
                    ], 'Flash-sales');
                }

                if (count($data->getUserEmails()) > 0) {
                    if (!empty($info)) {
                        $info .= '<br>';
                    }
                    $info .= CHtml::tag('span', [
                        'name' => "users-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => 'Emails',
                        'data-content' => $this->renderPartial('_userEmails',
                            ['data' => $data->getUserEmails()], true),
                    ], 'Users');
                }

                return $info;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'count',
            'header' => $this->t('Orders'),
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                return $data->count();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'countConfirmed',
            'header' => '<span title="' . $this->t('Confirmed') . '" data-toggle="tooltip" ><i class="icon-ok"></i></span>',
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                return $data->getCountConfirmed();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
            'headerHtmlOptions' => [
                'class' => 'button-column',
            ],
        ],
        [
            'name' => 'countMerged',
            'header' => '<span title="' . $this->t('Merged') . '" data-toggle="tooltip" ><i class="icon-download-alt"></i></span>',
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                return $data->getCountMerged();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
            'headerHtmlOptions' => [
                'class' => 'button-column',
            ],
        ],
        [
            'name' => 'countCanceled',
            'header' => '<span title="' . $this->t('Canceled') . '" data-toggle="tooltip" ><i class="icon-trash"></i></span>',
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                return $data->getCountCanceled();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
            'headerHtmlOptions' => [
                'class' => 'button-column',
            ],
        ],
        [
            'name' => 'countNotConfirmed',
            'header' => '<span title="' . $this->t('Not confirmed') . '" data-toggle="tooltip" ><i class="icon-eye-close"></i></span>',
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                return $data->getCountNotConfirmed();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
            'headerHtmlOptions' => [
                'class' => 'button-column',
            ],
        ],
        [
            'name' => 'count',
            'header' => $this->t('Products'),
            'value' => function ($data) {
                /** @var $data StatisticOrdersModel */
                return $data->getProductsTotalConfirmed();
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'averageProducts',
            'header' => $this->t('Average number of units'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getAverageProducts());
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'averageСheck',
            'header' => $this->t('Average check'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getAverageCheck());
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'averageСheckUnconfirmedIncludes',
            'header' => $this->t('Average check (+ not confirmed)'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getAverageCheckUnconfirmedIncludes());
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'price',
            'header' => $this->t('Amount'),
            'type' => 'raw',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                $info = $nf->formatDecimal($data->getPrice());

                if ($data->getBrandsManagerHistory()) {
                    $info = CHtml::tag('span', [
                        'name' => "brand-manager-{$data->id}",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'left',
                        'data-html' => 'true',

                        'data-title' => $this->t('Brand manager'),
                        'data-content' => $this->renderPartial('_ordersBrandManagers',
                            ['data' => $data->getBrandsManagerHistory()], true),
                    ], $nf->formatDecimal($data->getPrice()));
                }

                return $info;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'usedBonuses',
            'header' => '<span title="' . $this->t('Bonuses used') . '" data-toggle="tooltip" ><i class="icon-gift"></i></span>',
            'type' => 'raw',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                $text = $nf->formatDecimal($data->getUsedBonuses());
                $text .= ' <span class="muted">(' . $nf->formatDecimal($data->getPercentUsedBonuses()) . '%)</span>';
                return $text;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
            'headerHtmlOptions' => [
                'class' => 'button-column',
            ],
        ],
        [
            'name' => 'usedDiscounts',
            'header' => $this->t('Discount'),
            'type' => 'raw',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                $text = $nf->formatDecimal($data->getUsedDiscounts());
                $text .= ' <span class="muted">(' . $nf->formatDecimal($data->getPercentUsedDiscounts()) . '%)</span>';
                return $text;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'price',
            'header' => $this->t('Amount counting in bonuses and discounts'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                $price = $data->getPrice() - $data->getUsedBonuses() - $data->getUsedDiscounts();
                return $nf->formatDecimal($price);
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'profit',
            'header' => $this->t('Margin'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getProfit());
            },
            'visible' => !$short,
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'netProfit',
            'header' => '<span title="' . $this->t('margin - bonuses - discount') . '" data-toggle="tooltip" >' .
                $this->t('Forecasted profit') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getNetProfit());
            },
            'visible' => !$short,
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'expensesShop',
            'header' => '<span title="' . $this->t('money refunds to users + transportation costs + refunds to the card (for the current period)') . '" data-toggle="tooltip" >' . $this->t('Store costs') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getExpensesShop());
            },
            'visible' => !$short,
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'payed',
            'type' => 'raw',
            'header' => '<span title="' . $this->t('orders payments by card + upon receipt (for the current period)') . '" data-toggle="tooltip" >' . $this->t('Payments') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                $info = $nf->formatDecimal($data->getPaid()->amount);

                if ($data->getBrandsManagerHistory()) {
                    $info = CHtml::tag('span', [
                        'name' => "payed",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'left',
                        'data-html' => 'true',

                        'data-title' => $this->t('Data'),
                        'data-content' => $this->renderPartial('_ordersPaidDetail',
                            ['data' => $data], true),
                    ], $nf->formatDecimal($data->getPaid()->amount));
                }

                return $info;
            },
            'visible' => !$short,
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'dropShipping',
            'type' => 'raw',
            'header' => '<span title="' . $this->t('комиссия по дропшиппинговым акциям (за текущий период)') . '" data-toggle="tooltip" >' . $this->t('Dropshipping commission') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                /** @var $data StatisticOrdersModel */
                $info = $nf->formatDecimal($data->getDropShippingCommission());

                if ($data->getDropShippingOrdersForCommission()) {
                    $info = CHtml::tag('span', [
                        'name' => "payed",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'left',
                        'data-html' => 'true',

                        'data-title' => $this->t('Orders'),
                        'data-content' => $this->renderPartial('_listOrders',
                            ['data' => $data->getDropShippingOrdersForCommission()], true),
                    ], $info);
                }

                return $info;
            },
            'visible' => !$short,
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'netRealProfit',
            'header' => '<span title="' . $this->t('profit from paid orders + dropshipping - store costs - bonuses - discount (for the current period)') . '" data-toggle="tooltip" >Прибыль<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersModel */
                return $nf->formatDecimal($data->getNetRealProfit());
            },
            'visible' => !$short,
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
    ],
]); ?>

<?php
/** @var StatisticTotalOrdersModel $ordersForDays */
?>
<div id="totalInfo" class="well pull-right extended-summary" style="width:300px">
    <h3><?= $this->t('Total') ?>: </h3>
    <strong><?= $this->t('Orders') ?>: </strong><?= $nf->formatDecimal($ordersForDays->totalOrders) ?><br>

    <strong><?= $this->t('Confirmed') ?>: </strong><?= $nf->formatDecimal($ordersForDays->totalConfirmed) ?>
    <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentConfirmed) ?> %)</span><br>

    <strong><?= $this->t('Canceled') ?>: </strong><?= $nf->formatDecimal($ordersForDays->totalCanceled) ?>
    <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentCanceled) ?> %)</span><br>

    <strong><?= $this->t('Merged') ?>: </strong><?= $nf->formatDecimal($ordersForDays->totalMerged) ?>
    <span class="muted">(<?= $nf->formatDecimal(100 - $ordersForDays->percentCanceled - $ordersForDays->percentConfirmed - $ordersForDays->percentNotConfirmed) ?>
        %)</span><br>

    <strong>Не подтверждено: </strong><?= $nf->formatDecimal($ordersForDays->totalNotConfirmed) ?>
    <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentNotConfirmed) ?> %)</span><br>

    <strong><?= $this->t('Average number of units') ?>
        : </strong><?= $nf->formatDecimal($ordersForDays->totalAverageProducts) ?><br>

    <strong><?= $this->t('Average check') ?>
        : </strong><?= $nf->formatDecimal($ordersForDays->totalAverageCheck) ?><br>

    <strong><?= $this->t('Average check (+ not confirmed)') ?>
        : </strong><?= $nf->formatDecimal($ordersForDays->totalAverageCheckUnconfirmedIncludes) ?><br>

    <strong><?= $this->t('Bonuses used') ?>: </strong><?= $nf->formatDecimal($ordersForDays->usedBonuses) ?>
    <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentUsedBonuses) ?> %)</span><br>

    <strong><?= $this->t('Discount') ?>: </strong><?= $nf->formatDecimal($ordersForDays->usedDiscounts) ?>
    <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentUsedDiscount) ?> %)</span><br>

    <strong><?= $this->t('Amount counting in bonuses and discounts') ?>
        : </strong><?= $nf->formatDecimal($ordersForDays->totalPrice - $ordersForDays->usedBonuses - $ordersForDays->usedDiscounts) ?>
    <br>

    <strong><?= $this->t('Amount') ?>: </strong><?= $nf->formatDecimal($ordersForDays->totalPrice) ?><br>

    <?php if (!$short): ?>
        <strong><?= $this->t('Margin') ?>: </strong><?= $nf->formatDecimal($ordersForDays->profit) ?><br>

        <strong><?= $this->t('Forecasted profit') ?>
            : </strong> <?= $nf->formatDecimal($ordersForDays->netProfit) ?><br>

        <strong><?= $this->t('Store costs') ?>
            : </strong> <?= $nf->formatDecimal($ordersForDays->expensesShop) ?><br>

        <strong><?= $this->t('Payments') ?>: </strong> <?= $nf->formatDecimal($ordersForDays->payed) ?><br>

        <strong><?= $this->t('Dropshipping commission') ?>
            : </strong> <?= $nf->formatDecimal($ordersForDays->dropShippingCommission) ?><br>

        <strong><?= $this->t('Profit') ?>: </strong> <?= $nf->formatDecimal($ordersForDays->realNetProfit) ?>
        <br>

        <br>
        <strong><?= $this->t('Orders for Nova Post until') ?> 140
            : </strong>
        <?= $nf->formatDecimal($ordersForDays->countRedeliveryNpCard) ?>
        <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentRedeliveryNpCard) ?> %)</span>
        <br>

        <strong><?= $this->t('Prepaid orders') ?> </strong>
        <?= $nf->formatDecimal($ordersForDays->countPaidQuickly) ?>
        <span class="muted">(<?= $nf->formatDecimal($ordersForDays->percentPaidQuickly) ?> %)</span>
        <br>
    <?php endif ?>
</div>

<?php $this->endWidget(); ?>
