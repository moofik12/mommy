<?php

use MommyCom\Model\Statistic\StatisticOrdersBuyoutModel;
use MommyCom\Model\Statistic\StatisticOrdersModel;
use MommyCom\Model\Statistic\StatisticTotalOrdersBuyoutModel;
use MommyCom\YiiComponent\Statistic\Statistic;
use MommyCom\YiiComponent\Statistic\StatisticDataProvider;
use MommyCom\YiiComponent\Statistic\StatisticTimeFilter;

/* @var $this CController
 * @var $provider StatisticDataProvider
 * @var $statistic Statistic
 * @var $short bool
 * @var $orderCitiesStorage SplObjectStorage
 */

$this->pageTitle = $this->t('Purchased orders statistics');
$nf = $this->app()->getNumberFormatter();
/* @var StatisticTotalOrdersBuyoutModel $totalModel */
$totalModel = $statistic->getTotalModel();
$pathAssets = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);

$this->app()->clientScript->registerCssFile($pathAssets . '/css/customStatistic.css');

?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Dispatched orders statistics (dispatch period)'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php $this->renderPartial('_filters', ['statistic' => $statistic]) ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'summaryText' => $statistic->getTimeFilter()->getPeriodName() . ' {start} - {end} out of {count}',
    'selectableRows' => 0,
    'filterSelector' => '{filter}, #statistic-filter input, #statistic-filter select',
    'ajaxUpdate' => "totalInfo",
    'rowCssClassExpression' => function ($row, $data) use ($statistic) {
        if ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
            $dayNumber = $this->app()->dateFormatter->format('e', $data->day);

            if ($dayNumber == 1 || $dayNumber == 3 || $dayNumber == 5) {
                return 'error';
            }
        }

        return '';
    },
    'columns' => [
        [
            'name' => 'days',
            'header' => $statistic->getTimeFilter()->getPeriodName(),
            'value' => function ($data) use ($statistic) {
                /** @var StatisticOrdersModel $data */
                $date = $this->app()->getDateFormatter()->formatDateTime($data->day, 'short', false);

                if ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
                    $date = $this->app()->getDateFormatter()->format('d MMMM y ' . $this->t('year') . '., EEEE', $data->day);
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_WEEK) {
                    $date = $this->app()->getDateFormatter()->format('w ' . $this->t('week') . ' y ' . $this->t('year') . '.', $data->day);
                    $date .= ", ({$data->getDateTimeFrom()->format('d.m')} - {$data->getDateTimeTo()->format('d.m')})";
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_MONTH) {
                    $date = $this->app()->getDateFormatter()->format('LLLL y ' . $this->t('year') . '.', $data->day);
                }

                return $date;
            },
        ],
        [
            'name' => 'orders',
            'type' => 'html',
            'header' => $this->t('Shipped'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                $orders = $this->app()->numberFormatter->formatDecimal($data->orders);
                $products = $data->getCountProducts();
                $pattern = '<b>{orders}</b>';

                if ($products > 0) {
                    $pattern = '<b>{orders}</b> <br> (' . $this->t('Products') . ':&nbsp;{products})';
                }

                return strtr($pattern, ['{orders}' => $orders, '{products}' => $products]);
            },
        ],
        [
            'name' => 'ordersAmountToPay',
            'header' => '<span title="' . $this->t('Orders amount minus the discounts, you need to get') . '" data-toggle="tooltip" >' . $this->t('Amount to be paid') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                return $this->app()->numberFormatter->formatDecimal($data->ordersAmountToPay);
            },
        ],
        [
            'type' => 'raw',
            'name' => 'ordersAmountToPay',
            'header' => '<span title="' . $this->t('As well as showing a percentage of the amount due') . '" data-toggle="tooltip" >' .
                $this->t('Prepaid') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var StatisticOrdersBuyoutModel $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->ordersAmountPaid);
                $text .= ' <span class="muted">(' . $nf->formatDecimal($data->ordersAmountPaidPercent) . '%)</span>';

                return $text;
            },
        ],
        [
            'type' => 'raw',
            'name' => 'ordersAmountNeedPay',
            'header' => '<span title="' . $this->t('Difference between "payment due" and a "prepayment made"') . '" data-toggle="tooltip" >' . $this->t('Payment due') . '<span class="icon-exclamation-sign"></span></span>',
            'value' => function ($data) use ($nf) {
                /** @var StatisticOrdersBuyoutModel $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->ordersAmountNeedPay);
                $text .= ' <span class="muted">(' . $nf->formatDecimal($data->ordersAmountNeedPayPercent) . '%)</span>';

                return $text;
            },
        ],
        [
            'name' => 'ordersAmount',
            'header' => $this->t('Amount of dispatched'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                return $this->app()->numberFormatter->formatDecimal($data->ordersAmount);
            },
        ],
        [
            'name' => 'ordersBought',
            'type' => 'html',
            'header' => $this->t('Purchased'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                $orders = $this->app()->numberFormatter->formatDecimal($data->getOrdersBought());
                $products = $data->getOrdersBoughtProducts();
                $pattern = '<b>{orders}</b>';

                if ($products > 0) {
                    $pattern = '<b>{orders}</b> <br> (Products:&nbsp;{products})';
                }

                return strtr($pattern, ['{orders}' => $orders, '{products}' => $products]);
            },
        ],
        [
            'name' => 'ordersBoughtAmount',
            'header' => $this->t('Amount of purchased'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                return $this->app()->numberFormatter->formatDecimal($data->getOrdersBoughtAmount());
            },
        ],
        [
            'name' => 'notBoughtOrders',
            'type' => 'html',
            'header' => $this->t('Unfulfilled'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                $orders = $this->app()->numberFormatter->formatDecimal($data->notBoughtOrders);
                $products = $data->getCountNotBoughtProducts();

                $pattern = '<b>{orders}</b>';

                if ($products > 0) {
                    $pattern = '<b>{orders}</b> <br> (' . $this->t('Products') . ':&nbsp;{products})';
                }

                return strtr($pattern, ['{orders}' => $orders, '{products}' => $products]);
            },
        ],
        [
            'name' => 'notBoughtOrdersAmount',
            'header' => $this->t('Amount of unfulfilled'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                return $this->app()->numberFormatter->formatDecimal($data->notBoughtOrdersAmount);
            },
        ],
        [
            'name' => 'returnedOrders',
            'type' => 'html',
            'header' => $this->t('Returns'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                $orders = $this->app()->numberFormatter->formatDecimal($data->returnedOrders);
                $products = $data->getCountReturnedProducts();

                $pattern = '<b>{orders}</b>';

                if ($products > 0) {
                    $pattern = '<b>{orders}</b> <br> (' . $this->t('Products') . ':&nbsp;{products})';
                }

                return strtr($pattern, ['{orders}' => $orders, '{products}' => $products]);
            },
        ],
        [
            'name' => 'returnedOrdersAmount',
            'header' => $this->t('Total refunds'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                return $this->app()->numberFormatter->formatDecimal($data->returnedOrdersAmount);
            },
        ],
        [
            'type' => 'raw',
            'header' => $this->t('suppliers'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutModel $data */
                return CHtml::link($this->t('Data for the period'),
                    ['statisticCustom/ordersSuppliersBuyout',
                        'from' => $data->getDateTimeFrom()->getTimestamp(),
                        'to' => $data->getDateTimeTo()->getTimestamp(),
                        'period' => $data->getStatistic()->getTimeFilter()->getPeriod(),
                    ],
                    ['target' => '_blank']
                );
            },
        ],
    ],
]); ?>

<div id="totalInfo" class="well pull-right extended-summary" style="width:300px">
    <h3><?= $this->t('Total') ?>: </h3>
    <strong><?= $this->t('Shipped') ?>: </strong><?= $nf->formatDecimal($totalModel->orders) ?><br>

    <strong><?= $this->t('Amount to be paid') ?>
        : </strong><?= $nf->formatDecimal($totalModel->ordersAmountToPay) ?><br>

    <strong><?= $this->t('Prepaid') ?>: </strong>
    <?= $nf->formatDecimal($totalModel->ordersAmountPaid) ?>
    <span class="muted">(<?= $nf->formatDecimal($totalModel->getOrdersAmountPaidPercent()) ?>%)</span><br>

    <strong><?= $this->t('Payment due') ?>: </strong>
    <?= $nf->formatDecimal($totalModel->getOrdersAmountNeedPay()) ?>
    <span class="muted">(<?= $nf->formatDecimal($totalModel->getOrdersAmountNeedPayPercent()) ?>%)</span><br>

    <strong><?= $this->t('Amount of dispatched') ?>: </strong><?= $nf->formatDecimal($totalModel->ordersAmount) ?>
    <br>

    <strong><?= $this->t('Purchased') ?>: </strong><?= $nf->formatDecimal($totalModel->getOrdersBought()) ?><br>

    <strong><?= $this->t('Amount of purchased') ?>
        : </strong><?= $nf->formatDecimal($totalModel->getOrdersBoughtAmount()) ?><br>

    <strong><?= $this->t('Unfulfilled') ?>: </strong><?= $nf->formatDecimal($totalModel->notBoughtOrders) ?>
    <br>

    <strong><?= $this->t('Amount of unfulfilled') ?>
        : </strong><?= $nf->formatDecimal($totalModel->notBoughtOrdersAmount) ?><br>

    <strong><?= $this->t('Returns') ?>: </strong><?= $nf->formatDecimal($totalModel->returnedOrders) ?><br>

    <strong><?= $this->t('Total refunds') ?>
        : </strong><?= $nf->formatDecimal($totalModel->returnedOrdersAmount) ?><br>

    <br>
</div>

<?php $this->endWidget(); ?>
