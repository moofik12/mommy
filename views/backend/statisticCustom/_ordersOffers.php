<?php
/**
 * @var array $data
 */
$dataProvider = new CArrayDataProvider($data, [
    'pagination' => [
        'pageSize' => 50,
    ],
    'sort' => [
        'attributes' => ['totalOrders'],
        'defaultOrder' => [
            'totalOrders' => CSort::SORT_DESC,
        ],
    ],
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'name' => 'id',
            'header' => $this->t('Company'),
        ],
        [
            'name' => 'totalOrders',
            'header' => $this->t('Orders'),
        ],
        [
            'name' => 'ordersConfirmed',
            'header' => $this->t('Confirmed'),
        ],
        [
            'name' => 'ordersCanceled',
            'header' => $this->t('Canceled'),
        ],
        [
            'name' => 'ordersAmount',
            'header' => $this->t('Amount'),
        ],
    ],
]);
