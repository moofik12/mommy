<?php
/**
 * @var array $data
 */

$dataProvider = new CArrayDataProvider($data, [
    'pagination' => [
        'pageSize' => 50,
    ],
    'sort' => [
        'attributes' => ['countUsers'],
        'defaultOrder' => [
            'countUsers' => CSort::SORT_DESC,
        ],
    ],
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'name' => 'id',
            'header' => $this->t('Company'),
        ],
        [
            'name' => 'countUsers',
            'header' => $this->t('Qty'),
        ],
        [
            'name' => 'countEmailVerified',
            'header' => $this->t('Verified emails count'),
        ],
    ],
]);
