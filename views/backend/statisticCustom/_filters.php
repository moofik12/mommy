<?php

use MommyCom\YiiComponent\Statistic\Statistic;
use MommyCom\YiiComponent\Statistic\StatisticFilter;

/**
 * @var $statistic Statistic
 */

$filters = $statistic->getFilters();
$filtersMain = array_filter($filters, function ($filter) {
    /** @var StatisticFilter $filter */
    static $mainFiltersName = ['period', 'rangeData', 'orderType', 'channelType'];

    return in_array($filter->id, $mainFiltersName);
});
$filtersOthers = array_diff_key($filters, $filtersMain);
?>

<div id="statistic-filter">
    <div class="accordion-group">
        <div class="accordion-heading filters-other-header">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#filters-other">
                <?= $this->t('Additional filters') ?>
            </a>
        </div>
        <div id="filters-other" class="accordion-body collapse">
            <div class="accordion-inner">
                <?php

                foreach ($filtersOthers as $id => $filter) {
                    /** @var StatisticFilter $filter */
                    $statistic->renderFilter($id);
                }

                ?>
            </div>
        </div>
    </div>

    <div>
        <?php

        foreach ($filtersMain as $id => $filter) {
            /** @var StatisticFilter $filter */
            $statistic->renderFilter($id);
        }

        ?>
    </div>
</div>
