<?php
/**
 * @author: Zhenya Bondarev
 * @var array $data
 */
$dataStructure = array_map(function($item) {
    return array('email' => $item);
}, array_values($data));

$dataProvider = new CArrayDataProvider($dataStructure, array(
    'keyField' => 'email',
    'pagination' => array(
        'pageSize' => 50,
    ),
));

$this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'name' => 'email',
            'header' => 'Email',
        ),
    ),
));