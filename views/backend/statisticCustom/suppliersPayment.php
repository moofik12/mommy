<?php

use MommyCom\Model\Statistic\StatisticOrdersModel;
use MommyCom\Model\Statistic\StatisticSuppliersPaymentModel;
use MommyCom\Model\Statistic\StatisticTotalOrdersModel;
use MommyCom\Model\Statistic\StatisticTotalSuppliersPaymentModel;
use MommyCom\YiiComponent\Statistic\Statistic;
use MommyCom\YiiComponent\Statistic\StatisticDataProvider;
use MommyCom\YiiComponent\Statistic\StatisticTimeFilter;

/**
 * @var $this CController
 * @var $provider StatisticDataProvider
 * @var $statistic Statistic
 * @var $statisticTotal StatisticTotalOrdersModel
 * @var $short bool
 * @var StatisticTotalSuppliersPaymentModel $statisticTotal
 */

$this->pageTitle = $this->t('Statistics of supplier payments');
$nf = $this->app()->getNumberFormatter();
$statisticTotal = $statistic->getTotalModel();
$pathAssets = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);

$this->app()->clientScript->registerCssFile($pathAssets . '/css/customStatistic.css');

?>

<?
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Payment statistics'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php $this->renderPartial('_filters', ['statistic' => $statistic]) ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'summaryText' => $statistic->getTimeFilter()->getPeriodName() . ' {start} - {end} out of {count}',
    'selectableRows' => 0,
    'filterSelector' => '{filter}, #statistic-filter input, #statistic-filter select',
    'ajaxUpdate' => "totalInfo",
    'rowCssClassExpression' => function ($row, $data) {
        /** @var $data StatisticSuppliersPaymentModel */

        $css = '';
        if ($data->isFullRequestProcessed()) {
            /** @var $data StatisticSuppliersPaymentModel */
            if ($data->payAmount > $data->paidAmount) {
                $css = 'info';
            } elseif ($data->payAmount < $data->paidAmount) {
                $css = 'error';
            }
        }

        return $css;
    },
    'columns' => [
        [
            'name' => 'days',
            'header' => $statistic->getTimeFilter()->getPeriodName(),
            'value' => function ($data) use ($statistic) {
                /** @var StatisticOrdersModel $data */
                $date = $this->app()->getDateFormatter()->formatDateTime($data->day, 'short', false);

                if ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_DAY) {
                    $date = $this->app()->getDateFormatter()->format('d MMMM y ' . $this->t('year') . '., EEEE', $data->day);
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_WEEK) {
                    $date = $this->app()->getDateFormatter()->format('w ' . $this->t('week') . ' y ' . $this->t('year') . '.', $data->day);
                    $date .= ", ({$data->getDateTimeFrom()->format('d.m')} - {$data->getDateTimeTo()->format('d.m')})";
                } elseif ($statistic->getTimeFilter()->getPeriod() == StatisticTimeFilter::PERIOD_MONTH) {
                    $date = $this->app()->getDateFormatter()->format('LLLL y ' . $this->t('year') . '.', $data->day);
                }

                return $date;
            },
        ],
        [
            'name' => 'countPayments',
            'header' => $this->t('Number of payments'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->countPayments;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'countPaid',
            'header' => $this->t('Quantity paid'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->countPaid;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'requestAmount',
            'header' => $this->t('Amount for supplier inquiry'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->requestAmount;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'deliveredAmount',
            'header' => $this->t('Доставлено'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->deliveredAmount;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'returnedAmount',
            'header' => $this->t('Total refunds'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->returnedAmount;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'penaltyAmount',
            'header' => $this->t('Penatlies'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->penaltyAmount;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'customPenaltyAmount',
            'header' => $this->t('Additional penalties'),
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return $data->customPenaltyAmount;
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'payAmount',
            'header' => $this->t('Balance due'),
            'type' => 'html',
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return "<b>{$data->payAmount}</b>";
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'paidAmount',
            'header' => $this->t('Paid out'),
            'type' => 'html',
            'value' => function ($data) {
                /** @var $data StatisticSuppliersPaymentModel */
                return "<b>{$data->paidAmount}</b>";
            },
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
    ],
]); ?>
<?php
?>
<div id="totalInfo" class="well pull-right extended-summary" style="width:350px">
    <h3><?= $this->t('Total') ?>: </h3>
    <strong><?= $this->t('Number of payments') ?>
        : </strong><?= $nf->formatDecimal($statisticTotal->getCountPayments()) ?>
    <br>

    <strong><?= $this->t('Quantity paid') ?>
        : </strong><?= $nf->formatDecimal($statisticTotal->getCountPaid()) ?>
    <br>

    <strong><?= $this->t('Amount for supplier inquiry') ?>
        : </strong><?= $nf->formatDecimal($statisticTotal->getRequestAmount()) ?>
    <br>

    <strong><?= $this->t('Total refunds') ?>
        : </strong><?= $nf->formatDecimal($statisticTotal->getReturnedAmount()) ?>
    <br>

    <strong><?= $this->t('Penatlies') ?>
        : </strong><?= $nf->formatDecimal($statisticTotal->getPenaltyAmount()) ?>
    <br>

    <strong><?= $this->t('Balance due') ?>
        : <?= $nf->formatDecimal($statisticTotal->getPayAmount()) ?> </strong>
    <br>

    <strong><?= $this->t('Paid out') ?>: <?= $nf->formatDecimal($statisticTotal->getPaidAmount()) ?> </strong>
    <br>
    <br>

    <h4><?= $this->t('All payments processed') ?>: </h4>
    <strong><?= $this->t('Balance due') ?>
        : <?= $nf->formatDecimal($statisticTotal->getPayAmountAllRequest()) ?> </strong>
    <br>

    <strong><?= $this->t('Paid out') ?>
        : <?= $nf->formatDecimal($statisticTotal->getPaidAmountAllRequest()) ?> </strong>
    <br>

    <strong><?= $this->t('The difference in payment / debt') ?>
        : <?= $nf->formatDecimal($statisticTotal->getDiffPaymentsFullRequestProcessedAmount()) ?> </strong>
    <?php if ($statisticTotal->getDiffPaymentsFullRequestProcessedAmount() > 0): ?>
        <span class="muted">(<?= $this->t('переплата') ?></span>
    <?php endif ?>
    <br>
</div>

<?php $this->endWidget(); ?>
