<?php

use MommyCom\Model\Statistic\StatisticOrdersBuyoutModel;
use MommyCom\Model\Statistic\StatisticOrdersBuyoutSupplierModel;

/**
 * @var $this CController
 * @var $dataProvider CArrayDataProvider
 * @var $model StatisticOrdersBuyoutModel
 */

$this->pageTitle = $this->t('Purchased orders statistics');
$nf = $this->app()->getNumberFormatter();
$pathAssets = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);

$this->app()->clientScript->registerCssFile($pathAssets . '/css/customStatistic.css');

?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order statistics by suppliers over period') . $model->getDateTimeFrom()->format('d.m.Y') . ' - ' . $model->getDateTimeTo()->format('d.m.Y'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', [
    'buttons' => [
        [
            'type' => 'info',
            'label' => $this->t('Load'),
            'items' => [
                [
                    'label' => $this->t('By supplier'),
                    'url' => $this->createUrl('ordersSuppliersBuyout', [
                        'from' => $model->getDateTimeFrom()->getTimestamp(),
                        'to' => $model->getDateTimeTo()->getTimestamp(),
                        'period' => $model->getStatistic()->getTimeFilter()->getPeriod(),
                        'suppliersToFile' => 1,
                    ]),
                ],
                [
                    'label' => $this->t('By product'),
                    'url' => $this->createUrl('ordersSuppliersBuyout', [
                        'from' => $model->getDateTimeFrom()->getTimestamp(),
                        'to' => $model->getDateTimeTo()->getTimestamp(),
                        'period' => $model->getStatistic()->getTimeFilter()->getPeriod(),
                        'productsToFile' => 1,
                    ]),
                ],
                [
                    'label' => $this->t('By managers'),
                    'url' => $this->createUrl('ordersSuppliersBuyout', [
                        'from' => $model->getDateTimeFrom()->getTimestamp(),
                        'to' => $model->getDateTimeTo()->getTimestamp(),
                        'period' => $model->getStatistic()->getTimeFilter()->getPeriod(),
                        'brandManagerToFile' => 1,
                    ]),
                ],
            ],
        ],
    ],
]) ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'template' => "&nbsp;{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'selectableRows' => 0,
    'filterSelector' => '{filter}, #statistic-filter input, #statistic-filter select',
    'ajaxUpdate' => "totalInfo",
    'htmlOptions' => [
        'class' => 'max-popover',
    ],
    'columns' => [
        [
            'name' => 'supplerID',
            'header' => $this->t('Supplier'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->supplierName;
            },
        ],

        [
            'name' => 'ratioSold',
            'type' => 'number',
            'header' => $this->t('% purchase'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->ratioSold;
            },
        ],
        [
            'name' => 'ratioSoldAmount',
            'type' => 'number',
            'header' => $this->t('% purchase (in monetary equivalent)'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->ratioSoldAmount;
            },
        ],
        [
            'name' => 'ratioReturned',
            'type' => 'number',
            'header' => $this->t('% returns'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->ratioReturned;
            },
        ],
        [
            'name' => 'ratioNotBought',
            'type' => 'number',
            'header' => $this->t('% not fulfilled'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->ratioNotBought;
            },
        ],
        [
            'name' => 'countSold',
            'type' => 'raw',
            'header' => $this->t('Items sold'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersBuyoutSupplierModel */
                $info = $nf->formatDecimal($data->countSold);
                $supplierText = $data->supplierID;
                /*
                $supplier = SupplierRecord::model()->cache(120)->findByPk($data->supplierID);
                if ($supplier) {
                    $supplierText = "{$supplier->name} ($supplier->id)";
                }
                */

                if ($info) {
                    $info = CHtml::tag('span', [
                        'name' => "countReturnedInfo",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'left',
                        'data-html' => 'true',
                        'container' => '.statistic-info',

                        'data-title' => $this->t('Vendor information') . $supplierText . "<button type=\"button\" class=\"close\" onclick=\"$(this).closest('.popover').siblings('[data-toggle=popover]').popover('hide')\">&times;</button>",
                        'data-content' => $this->renderPartial('_orderSuppliersBuyoutProducts',
                            ['data' => $data->getProducts(), 'show' => 'sold'], true),
                    ], $info);
                }

                return $info;
            },
        ],
        [
            'name' => 'amountSold',
            'type' => 'number',
            'header' => $this->t('Items total'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->amountSold;
            },
        ],
        [
            'name' => 'countReturned',
            'type' => 'raw',
            'header' => $this->t('Items returned'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersBuyoutSupplierModel */
                $info = $nf->formatDecimal($data->countReturned);
                $supplierText = $data->supplierID;
                /*
                $supplier = SupplierRecord::model()->cache(120)->findByPk($data->supplierID);
                if ($supplier) {
                    $supplierText = "{$supplier->name} ($supplier->id)";
                }
                */

                if ($info) {
                    $info = CHtml::tag('span', [
                        'name' => "countReturnedInfo",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'left',
                        'data-html' => 'true',

                        'data-title' => $this->t('Vendor information') . $supplierText . "<button type=\"button\" class=\"close\" onclick=\"$(this).closest('.popover').siblings('[data-toggle=popover]').popover('hide')\">&times;</button>",
                        'data-content' => $this->renderPartial('_orderSuppliersBuyoutProducts',
                            ['data' => $data->getReturnedProducts(), 'show' => 'returned'], true),
                    ], $info);
                }

                return $info;
            },
        ],
        [
            'name' => 'amountReturned',
            'type' => 'number',
            'header' => $this->t('Total refunds'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->amountReturned;
            },
        ],
        [
            'name' => 'countNotBought',
            'type' => 'raw',
            'header' => $this->t('Unfulfilled'),
            'value' => function ($data) use ($nf) {
                /** @var $data StatisticOrdersBuyoutSupplierModel */
                $info = $nf->formatDecimal($data->countNotBought);
                $supplierText = $data->supplierID;
                /*
                $supplier = SupplierRecord::model()->cache(120)->findByPk($data->supplierID);
                if ($supplier) {
                    $supplierText = "{$supplier->name} ($supplier->id)";
                }
                */

                if ($info) {
                    $info = CHtml::tag('span', [
                        'name' => "countReturnedInfo",
                        'class' => 'statistic-info',
                        'data-toggle' => 'popover',
                        'data-trigger' => 'click',
                        'data-placement' => 'left',
                        'data-html' => 'true',

                        'data-title' => $this->t('Vendor information') . $supplierText . "<button type=\"button\" class=\"close\" onclick=\"$(this).closest('.popover').siblings('[data-toggle=popover]').popover('hide')\">&times;</button>",
                        'data-content' => $this->renderPartial('_orderSuppliersBuyoutProducts',
                            ['data' => $data->getNotBoughtProducts(), 'show' => 'notBought'], true),
                    ], $info);
                }

                return $info;
            },
        ],
        [
            'name' => 'amountNotBought',
            'type' => 'number',
            'header' => $this->t('Amount of unfulfilled'),
            'value' => function ($data) {
                /** @var StatisticOrdersBuyoutSupplierModel $data */
                return $data->amountNotBought;
            },
        ],
    ],
]); ?>


<?php $this->endWidget() ?>
