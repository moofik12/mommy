<?php

use MommyCom\Model\Statistic\StatisticUsersModel;
use MommyCom\Model\Statistic\StatisticUsersModelBonusWhenRegistering;

/**
 * @var $data StatisticUsersModel
 */

$dataProvider = new CArrayDataProvider($data->getDetailReceivedBonusesWhenRegistering(), [
    'keyField' => 'points',
    'pagination' => [
        'pageSize' => 50,
    ],
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'name' => 'points',
            'header' => $this->t('Bonuses'),
        ],
        [
            'name' => 'number',
            'header' => $this->t('Qty'),
        ],
        [
            'name' => 'number',
            'header' => $this->t('Amount'),
            'value' => function ($data) {
                /* @var StatisticUsersModelBonusWhenRegistering $data */
                return $data->getAmount();
            },
        ],
        [
            'name' => 'spent',
            'header' => $this->t('Spent'),
        ],
        [
            'name' => 'burned',
            'header' => $this->t('Withdrawal made'),
        ],
        [
            'header' => $this->t('Orders total on this day'),
            'value' => function () use ($data) {
                return $data->countOrdersUsedPresentBonus;
            },
        ],
        [
            'header' => $this->t('Orders total on this day'),
            'value' => function () use ($data) {
                return $data->ordersAmountUsedPresentBonus;
            },
        ],
    ],
]);
