<?php
/**
 * @var array $data
 */
$mas = array_map(function ($id) {
    return ['id' => $id];
}, $data);

$dataProvider = new CArrayDataProvider($mas, [
    'pagination' => [
        'pageSize' => 50,
    ],
    'sort' => false,
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'header' => '#',
            'value' => function ($mas) {
                return isset($mas['id']) ? $mas['id'] : $this->t('not found');
            },
        ],
    ],
]);
