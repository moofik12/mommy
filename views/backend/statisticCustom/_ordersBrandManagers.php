<?php
/**
 * @var array $data
 */
$dataProvider = new CArrayDataProvider($data, [
    'pagination' => [
        'pageSize' => 50,
    ],
    'sort' => [
        'attributes' => ['countProducts'],
        'defaultOrder' => [
            'totalOrders' => CSort::SORT_DESC,
        ],
    ],
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'name' => 'displayName',
            'header' => $this->t('Brand'),
        ],
        [
            'name' => 'countProducts',
            'header' => $this->t('Products'),
        ],
        [
            'name' => 'amount',
            'header' => $this->t('Amount'),
        ],
    ],
]);
