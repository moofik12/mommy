<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $provider
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Supplier contracts');
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Contracts list'),
    'headerIcon' => 'icon-th-list',
]);
?>

<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['contractCreate'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>

<?
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
//    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'name' => 'name',
            'type' => 'ntext',
            'value' => function ($data) {
                /* @var SupplierContractRecord $data */
                $text = $data->name;

                if ($data->is_default) {
                    $text .= "\n (по умолчанию для всех поставщиков)";
                }

                return $text;
            },
        ],
        [
            'name' => 'is_default',
            'type' => 'boolean',
            'filter' => $format->booleanFormat,
        ],
        [
            'name' => 'not_delivered_penalty_percent',
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        [
            'name' => 'delivery_working_days',
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        [
            'name' => 'add_delivery_working_days',
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        [
            'name' => 'suppliersCount',
            'type' => 'raw',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span1'],
            'value' => function ($data) {
                /* @var SupplierContractRecord $data */
                $text = $data->suppliersCount;

                $text .= '<br>' . CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('contractSuppliersUpdate', ['id' => $data->id]), ['target' => '_blank']);

                return $text;
            },
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {update}',
            'viewButtonUrl' => '\Yii::app()->controller->createUrl("contractView",array("id"=>$data->primaryKey))',
            'updateButtonUrl' => '$this->app()->controller->createUrl("contractUpdate",array("id"=>$data->primaryKey))',
        ],
    ],
]);

$this->endWidget();
?>

