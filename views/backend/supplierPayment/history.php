<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $provider
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Payments to suppliers');
?>

<style type="text/css">
    a[data-toggle="popover"] {
        display: block;
    }
</style>


<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Payments list'),
    'headerIcon' => 'icon-th-list',
]);

$this->renderPartial('_filter');

$this->widget('bootstrap.widgets.TbButtonGroup', [
    'buttons' => [
        [
            'type' => 'info',
            'label' => $this->t('Report'),
            'items' => [
                [
                    'label' => $this->t('By supplier'),
                    'url' => $this->createUrl('history', ['file' => SupplierPaymentController::HISTORY_SAVE_TYPE_SUPPLIER] + $_GET),
                ],
                [
                    'label' => $this->t('By supplier (in detail)'),
                    'url' => $this->createUrl('history', ['file' => SupplierPaymentController::HISTORY_SAVE_TYPE_SUPPLIER_FULL] + $_GET),
                ],
            ],
        ],
    ],
    'htmlOptions' => [
        'id' => 'filesButtons',
    ],
]);

$this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'supplier-history',
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'ajaxUpdate' => 'supplier-history, filesButtons',
    'rowCssClassExpression' => function ($row, $data) {
        /** @var SupplierPaymentRecord $data */
        $statusesShow = [SupplierPaymentRecord::STATUS_CONFIRMED, SupplierPaymentRecord::STATUS_ANALYZED];
        if (!in_array($data->status, $statusesShow)) {
            return '';
        }

        $cssClass = '';
        if ($data->status == SupplierPaymentRecord::STATUS_CONFIRMED) {
            if ($data->paid_amount > $data->payAmount) {
                $cssClass = 'error';
            } elseif ($data->paid_amount != $data->payAmount) {
                $cssClass = 'info';
            }
        } else {
            if ($data->request_amount != $data->delivered_amount) {
                $cssClass = 'warning';
            }
        }

        return $cssClass;
    },
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->supplier) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->supplier->name, '', [
                    'class' => 'btn-link',
                    'name' => "supplier-{$data->supplier->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => "{$data->supplier->name} ({$data->supplier->id})",
                    'data-content' => $this->renderPartial('_popoverSupplierPayment',
                        ['model' => $data], true),
                ]);
            },
        ],
        'event_id',
        [
            'header' => $this->t('Flash-sale title'),
            'filter' => false,
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->event) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->event->name, '', [
                    'class' => 'btn-link',
                    'name' => "event-{$data->event->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $data->event->name,
                    'data-content' => $this->renderPartial('_popoverEvent',
                        ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'status',
            'filter' => SupplierPaymentRecord::statuses(),
            'value' => '$data->statusReplacement',
        ],
        [
            'name' => 'contract_id',
            'value' => '$data->contract->name',
            'filter' => ArrayUtils::getColumn(SupplierContractRecord::model()->cache(60)->findAll(), 'name', 'id'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'act_id',
            'type' => 'raw',
            'value' => function ($data) {
                if ($data->act) {
                    return CHtml::link($data->act->id, $this->app()->controller->createUrl('actView', ['id' => $data->act->id]), ['target' => '_blank']);
                }

                return '';
            },
        ],
        [
            'name' => 'payment_after_at',
            'type' => 'html',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = '<span title="' . $this->t('Unassigned') . '" data-toggle="tooltip" ><i class="icon-time"></i></span>';

                if ($data->payment_after_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->payment_after_at, 'short', null);
                    $text .= '<br>' . $this->t('{n} day | {n} days | {n} days', $data->getStayPayment()->d);
                    $text .= '<br>' .
                        '(' . $this->t('{n} business day |{n} 1 business days |{n} 1 business days ', $data->getStayPaymentWorkDays()) . ')';
                }

                return $text;
            },
        ],
        [
            'name' => 'payment_at',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->t('no');

                if ($data->payment_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->payment_at, 'short', null);
                }

                return $text;
            },
        ],
        [
            'name' => 'request_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount_wrong_time',
            'header' => '<span title="' . $this->t('Delivery amount after delivery time') . '" data-toggle="tooltip" ><i class="icon-road"></i><i class="icon-calendar"></i></span>',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'payAmount',
            'filter' => false,
            'type' => 'numberLocale',
            'htmlOptions' => [
                'style' => 'font-weight: bold',
            ],
        ],
        [
            'name' => 'paid_amount',
            'type' => 'numberLocale',
            'htmlOptions' => [
                'style' => 'font-weight: bold',
            ],
        ],
        [
            'name' => 'returned_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'unpaid_amount',
            'header' => '<span title="' . SupplierPaymentRecord::model()->getAttributeLabel('unpaid_amount') . '"><i class="icon-minus-sign"></i></span>',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'unpaid_fixed_amount',
            'header' => '<span title="' . SupplierPaymentRecord::model()->getAttributeLabel('unpaid_fixed_amount') . '"><i class="icon-plus-sign"></i></span>',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'penalty_amount',
            'type' => 'html',
            'value' => function ($data) use ($format) {
                /* @var SupplierPaymentRecord $data */
                $text = $format->formatNumberLocale($data->penalty_amount);

                if ($data->count_products_problem_delivery) {
                    $text .= "&nbsp;&nbsp;<span title='" . $this->t('penalized items') . "'>(<i class='icon-thumbs-down'></i> {$data->count_products_problem_delivery})</span>";
                }

                return $text;
            },
        ],
        [
            'name' => 'custom_penalty_amount',
            'type' => 'numberLocale',
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view}',
//            'viewButtonUrl' => '$this->app()->controller->createUrl("contractView",array("id"=>$data->primaryKey))',
        ],

    ],
]);

$this->endWidget();
?>

