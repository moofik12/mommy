<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var SupplierContractRecord $model
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('View contract');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View contract') . ' №' . $model->id,
    'headerIcon' => 'icon-add',
]); ?>

<?
$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'name',
        'not_delivered_penalty_percent',
        'delivery_working_days',
        'add_delivery_working_days',
        'payment_after_full_delivery_working_days',
        'payment_after_not_full_delivery_working_days',
        [
            'type' => 'boolean',
            'name' => 'is_default',
        ],
    ],
]);
?>

<?php $this->endWidget() ?>
