<?php

use MommyCom\Model\Db\SupplierPaymentRecord;

/**
 * @var $model SupplierPaymentRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'delivery_start_at',
            'value' => function($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->t('no data available');
                if ($data->delivery_start_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->delivery_start_at);
                }

                return $text;
            },
        ),
        array(
            'name' => 'delivery_end_at',
            'value' => function($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->t('no data available');
                if ($data->delivery_end_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->delivery_end_at);
                }

                return $text;
            },
        ),
    ),
));
