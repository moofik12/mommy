<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $provider
 * @var CMap $summary
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Pending the additional delivery from the supplier');
$applyFilter = isset($applyFilter) ? $applyFilter : false;
?>

<style type="text/css">
    a[data-toggle="popover"] {
        display: block;
    }
</style>


<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Pending the additional delivery from the supplier'),
    'headerIcon' => 'icon-th-list',
]);

$this->renderPartial('_filter', ['excludeFilters' => ['paymentAt']]);
$this->renderPartial('_summary', ['summary' => $summary]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'ajaxUpdate' => 'supplier-payment-summary, createForAllPages',
    'rowCssClassExpression' => function ($row, $data) {
        /** @var SupplierPaymentRecord $data */
        $cssClass = '';
        if ($data->contract && $data->contract->getAvailableDeliveryDays($data->getMustDeliveryStartAt(), false) == 0) {
            $cssClass = 'error';
        }

        return $cssClass;
    },
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->supplier) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->supplier->name, '', [
                    'class' => 'btn-link',
                    'name' => "supplier-{$data->supplier->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => "{$data->supplier->name} ({$data->supplier->id})",
                    'data-content' => $this->renderPartial('_popoverSupplierPayment',
                        ['model' => $data], true),
                ]);
            },
        ],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->event) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link("№{$data->event_id} {$data->event->name}", '', [
                    'class' => 'btn-link',
                    'name' => "event-{$data->event->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $data->event->name,
                    'data-content' => $this->renderPartial('_popoverEvent',
                        ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'status',
            'filter' => SupplierPaymentRecord::statuses(true, [
                SupplierPaymentRecord::STATUS_NOT_ANALYZED,
                SupplierPaymentRecord::STATUS_WAIT_DELIVERY,
                SupplierPaymentRecord::STATUS_ANALYSIS,
            ]),
            'value' => '$data->statusReplacement',
        ],
        [
            'name' => 'order_table_loaded_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->t('no data available');
                if ($data->order_table_loaded_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->order_table_loaded_at);
                }

                return $text;

            },
        ],
        [
            'header' => $this->t('Last delivery'),
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->t('no data available');
                if ($data->delivery_end_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->delivery_end_at);
                }

                return $text;

            },
        ],
        [
            'name' => 'contract_id',
            'value' => '$data->contract->name',
            'filter' => ArrayUtils::getColumn(SupplierContractRecord::model()->cache(60)->findAll(), 'name', 'id'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'act_id',
            'type' => 'raw',
            'value' => function ($data) {
                if ($data->act) {
                    return CHtml::link($data->act->id, $this->app()->controller->createUrl('actView', ['id' => $data->act->id]), ['target' => '_blank']);
                }

                return '';
            },
        ],
        [
            'name' => 'payment_after_at',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->t('Not assigned');

                if ($data->payment_after_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->payment_after_at, 'short', null);
                }

                return $text;
            },
        ],
        [
            'name' => 'request_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'payAmount',
            'filter' => false,
            'type' => 'numberLocale',
            'htmlOptions' => [
                'style' => 'font-weight: bold',
            ],
        ],
        [
            'header' => $this->t('For payment excluding penalty'),
            'filter' => false,
            'type' => 'numberLocale',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->getPayAmount(false);

            },
        ],
        [
            'header' => '<span title="' . $this->t('Available delivery times in days') . '" data-toggle="tooltip" >
            ' . $this->t('Waiting for delivery') . '<span class="icon-exclamation-sign"></span></span>',
            'filter' => false,
            'type' => 'numberLocale',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->contract->getAvailableDeliveryDays($data->event_end_at, false);

            },
        ],
        [
            'class' => ButtonColumn::class,
            'template' => "{view} {delete}",
            'deleteConfirmation' => $this->t('Supplier has delivered all products'),
            'buttons' => [
                'delete' => [
                    'label' => $this->t('Supplier has delivered all products'),
                    'icon' => 'icon-ok',
                    'url' => '\Yii::app()->controller->createUrl("applyFullDelivery",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-success',
                    ],
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierPaymentRecord */
                        return $data->isCanApplyFullDelivery();
                    },
                ],
            ],
        ],

    ],
]);
?>

<?php
$this->endWidget();
?>
