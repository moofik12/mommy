<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\WarehouseProductRecord;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $providerWarehouseProducts
 * @var TbForm $form
 * @var SupplierPaymentRecord $model
 */

$this->pageTitle = $this->t('Supplier payment confirmation');

$nf = $this->app()->numberFormatter;
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier payment confirmation'),
    'headerIcon' => 'icon-th-list',
]);

echo $form->render();
?>

<h4><?= $this->t('Payment details') ?></h4>

<?php
$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        'statusReplacement',
        [
            'name' => 'supplier_id',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->supplier ? $data->supplier->name : $data->supplier_id;
            },
        ],
        [
            'name' => 'event_id',
        ],
        [
            'name' => $this->t('Flash-sale title'),
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->event ? $data->event->name : $data->event_id;
            },
        ],
        [
            'name' => 'contract_id',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->contract ? $data->contract->name : $data->event_id;
            },
        ],
        'payment_after_at:datetime',
        'request_amount:numberLocale',
        'delivered_amount:numberLocale',
        'returned_amount:numberLocale',
        'penalty_amount:numberLocale',
        'custom_penalty_amount:numberLocale',
        [
            'type' => 'html',
            'name' => 'payAmount',
            'value' => '<b>' . $nf->formatDecimal($model->payAmount) . '</b>',
        ],
        [
            'name' => $this->t('For payment excluding penalty'),
            'type' => 'numberLocale',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->getPayAmount(false);
            },
        ],
    ],
]);
?>

<h4><?= $this->t('Warehouse products') ?></h4>

<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerWarehouseProducts,
    'filter' => $providerWarehouseProducts->model,
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->event) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->event->name, '#', [
                    'name' => "event-{$data->event->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'hover',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $data->event->name,
                    'data-content' => $this->renderPartial('_popoverEvent',
                        ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'status',
            'filter' => WarehouseProductRecord::statusReplacements(),
            'value' => '$data->statusReplacement',
        ],
        [
            'name' => 'eventProduct.price_purchase',
            'value' => '$data->eventProduct->price_purchase',
        ],
    ],
]);


$this->endWidget();
?>
