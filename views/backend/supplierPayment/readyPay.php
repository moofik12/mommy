<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $provider
 * @var CMap $summary
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Due to be paid by supplier');
$applyFilter = isset($applyFilter) ? $applyFilter : false;
?>

<style type="text/css">
    a[data-toggle="popover"] {
        display: block;
    }
</style>


<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Payments list'),
    'headerIcon' => 'icon-th-list',
]);

$this->renderPartial('_filter', ['excludeFilters' => ['paymentAt']]);
$this->renderPartial('_summary', ['summary' => $summary]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'ajaxUpdate' => 'supplier-payment-summary, createForAllPages',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->supplier) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->supplier->name, '', [
                    'class' => 'btn-link',
                    'name' => "supplier-{$data->supplier->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => "{$data->supplier->name} ({$data->supplier->id})",
                    'data-content' => $this->renderPartial('_popoverSupplierPayment',
                        ['model' => $data], true),
                ]);
            },
        ],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->event) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link("№{$data->event_id} {$data->event->name}", '', [
                    'class' => 'btn-link',
                    'name' => "event-{$data->event->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $data->event->name,
                    'data-content' => $this->renderPartial('_popoverEvent',
                        ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'status',
            'filter' => false,
            'value' => '$data->statusReplacement',
        ],
        [
            'name' => 'contract_id',
            'type' => 'html',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $data->contract->name;
                $text .= '<br>';

                if ($data->payment_after_at > 0) {
                    $text .= '(' . $this->app()->dateFormatter->formatDateTime($data->payment_after_at, 'short', null) . ')';
                }

                return $text;
            },
            'filter' => ArrayUtils::getColumn(SupplierContractRecord::model()->cache(60)->findAll(), 'name', 'id'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'act_id',
            'type' => 'raw',
            'value' => function ($data) {
                if ($data->act) {
                    return CHtml::link($data->act->id, $this->app()->controller->createUrl('actView', ['id' => $data->act->id]), ['target' => '_blank']);
                }

                return '';
            },
        ],
        [
            'name' => 'request_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount_wrong_time',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'payAmount',
            'filter' => false,
            'type' => 'numberLocale',
        ],
        [
            'header' => $this->t('For payment excluding penalty'),
            'filter' => false,
            'type' => 'numberLocale',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->getPayAmount(false);

            },
        ],
        [
            'name' => 'paid_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'returned_amount',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->returned_amount);
                $text .= '<br>';

                if ($data->statementReturnsCount > 0) {
                    $text .= CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                } elseif ($data->statementReturnsPossibleCount) {
                    $text .= CHtml::link($this->t('Add refund'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                }

                return $text;
            },
        ],
        [
            'name' => 'unpaid_amount',
            'header' => '<span title="' . SupplierPaymentRecord::model()->getAttributeLabel('unpaid_amount') . '"><i class="icon-minus-sign"></i></span>',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'unpaid_fixed_amount',
            'header' => '<span title="' . SupplierPaymentRecord::model()->getAttributeLabel('unpaid_fixed_amount') . '"><i class="icon-plus-sign"></i></span>',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->unpaid_fixed_amount);
                $text .= '<br>';

                if ($data->includeUnpaidPaymentsCount > 0) {
                    $text .= CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                } elseif ($data->unpaidPaymentPossibleCount) {
                    $text .= CHtml::link($this->t('Add'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                }

                return $text;
            },
        ],
        [
            'name' => 'penalty_amount',
            'type' => 'html',
            'value' => function ($data) use ($format) {
                /* @var SupplierPaymentRecord $data */
                $text = $format->formatNumberLocale($data->penalty_amount);

                if ($data->count_products_problem_delivery) {
                    $text .= " <span title='" . $this->t('penalized items') . "'>(<i class='icon-thumbs-down'></i>{$data->count_products_problem_delivery})</span>";
                }

                return $text;
            },
        ],
        [
            'name' => 'custom_penalty_amount',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->custom_penalty_amount);
                $text .= '<br>';

                if ($data->custom_penalty_amount > 0) {
                    $text .= CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                } else {
                    $text .= CHtml::link($this->t('Add'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                }

                return $text;
            },
        ],
        [
            'class' => ButtonColumn::class,
            'template' => "{confirm} {view} {cancel}",
            'buttons' => [
                'confirm' => [
                    'label' => $this->t('Confirm'),
                    'icon' => 'icon-ok',
                    'url' => '\Yii::app()->controller->createUrl("confirm",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-success',
                    ],
                ],
                'cancel' => [
                    'label' => $this->t('Cancel'),
                    'icon' => 'icon-remove',
                    'url' => '$this->app()->controller->createUrl("cancel",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-danger',
                    ],
                ],
            ],
        ],

    ],
    'bulkActions' => [
        'align' => 'left',
        'selectableRows' => false,
        'noCheckedMessage' => $this->t('Select payment to create an Act'),
        'actionButtons' => [
            [
                'id' => 'createForId',
                'buttonType' => 'button',
                'type' => 'primary',
                'size' => 'medium',
                'label' => $this->t('Create Act from selected'),
                'htmlOptions' => [
                    'class' => '',
                    'data-url' => $this->createAbsoluteUrl('createActFromId'),
                    'target' => '_blank',
                ],
                'click' => new CJavaScriptExpression('function(values) {
                        var $this = $(this);
                        var url = $this.attr("data-url");
                        if (url.indexOf("?") == -1) {
                            url += "?";
                        }
                        $(values).each(function() {
                            url += "&" + "id[]=" + $(this).val();
                        });
                        window.open(url, "_blank");
                    }'),
            ],
        ],
        // if grid doesn't have a checkbox column type, it will attach
        // one and this configuration will be part of it
        'checkBoxColumnConfig' => [
            'name' => 'id',
        ],
    ],
]);
?>

<div class="well" id="createForAllPages">
    <?php
    $disableButton = $provider->itemCount == 0 || !$applyFilter;
    $disableButtonHtml = $disableButton ? ["disabled" => "disabled"] : [];
    $this->widget('bootstrap.widgets.TbButton', [
        'id' => 'createForAllPagesButton',
        'buttonType' => 'link',
        'type' => 'primary',
        'size' => 'medium',
        'label' => $this->t('Create Act containing payments from all pages'),
        'url' => $this->createUrl($this->action->id, ['createAct' => 1] + $_GET),
        'htmlOptions' => [
                'target' => '_blank',
            ] + $disableButtonHtml,
    ]);
    ?>
</div>

<?php
$this->endWidget();
?>

