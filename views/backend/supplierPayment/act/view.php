<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierPaymentActRecord;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $provider
 * @var CMap $summary
 * @var SupplierPaymentActRecord $model
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('For payments to suppliers');

$availableStatusesPaymentForEdit = [
    SupplierPaymentRecord::STATUS_ANALYSIS,
    SupplierPaymentRecord::STATUS_ANALYZED,
];
?>

<style type="text/css">
    a[data-toggle="popover"] {
        display: block;
    }
</style>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Act №') . $model->id . ', '  . $this->t('List of payments'),
    'headerIcon' => 'icon-th-list',
]);

//форма заказа
/** @var CActiveForm $form */
$form = $this->beginWidget('CActiveForm', [
    'id' => 'act-form',
    'action' => ['actAjaxUpdate', 'id' => $model->id],
    'enableClientValidation' => false,
    'clientOptions' => [
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'hideErrorMessage' => true,
        'afterValidate' => new CJavaScriptExpression('function(form, data, hasError) {
                if ($.isEmptyObject(data)) {
                    return true;
                }

                return false;
            }'),
    ],
    'htmlOptions' => [
        'name' => 'data-act',
    ],
]);


$this->widget('bootstrap.widgets.TbDetailView', [
    'id' => 'act-detail',
    'data' => $model,
    'attributes' => [
        'id',
        [
            'name' => 'name',
            'type' => 'raw',
            'value' => $form->textField($model, 'name',
                ['id' => CHtml::activeId($model, 'name') . $model->id, 'class' => 'input-xxlarge']),
        ],
        [
            'name' => 'countPayments',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'amountPayments',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'amountPaymentsWithoutPenalty',
            'type' => 'numberLocale',
        ],
    ],
]);
?>

<div>
    <?php
    $this->widget('bootstrap.widgets.TbButton', [
        'buttonType' => 'link',
        'url' => $this->app()->controller->createUrl('actDelete', ['id' => $model->id]),
        'type' => 'danger',
        'size' => 'small',
        'label' => $this->t('Delete'),
        'htmlOptions' => ["class" => "pull-right"],
    ]);

    $this->widget('bootstrap.widgets.TbButton', [
        'buttonType' => 'ajaxSubmit',
        'type' => 'success',
        'size' => 'small',
        'label' => $this->t('Save'),
        'url' => $this->app()->controller->createUrl('actAjaxUpdate', ['id' => $model->id]),
        'ajaxOptions' => [
            'beforeSend' => 'function() {
                var inputs = $form = document.getElementById("act-form").querySelectorAll("input");

                for(var i=0; inputs.length > i; i++) {
                    var input = inputs[i];
                    this.data[input.name] = input.value ? input.value : "";
                }
            }',
            'error' => 'function() {
                alert("Failed to update data");
            }',
        ],
        'htmlOptions' => ["class" => "pull-right", "style" => "margin-right: 20px;"],
    ]);


    $this->widget('bootstrap.widgets.TbSelect2', [
        'id' => 'supplier-payment-add',
        'name' => 'supplier-payment-add',
        'asDropDownList' => false,
        'htmlOptions' => [
            'placeholder' => $this->t('Select payment'),
        ],
        'options' => [
            'width' => '350px',
            'allowClear' => true,
            'ajax' => [
                'url' => $this->app()->controller->createUrl('ajaxSearch'),
                'type' => 'GET',
                'dataType' => 'json',
                'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            name: term,
                            page: page
                            };
                        }'),
                'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.id
                                });
                            });
                        return result;
                    }'),
            ],
            'initSelection' => new CJavaScriptExpression("function(element, callback) {
            }"),
        ],
    ]);

    echo CHtml::ajaxButton($this->t('Add'), ['supplierPayment/actAjaxAddPosition', 'id' => $model->id], [
            'type' => 'POST',
            'success' => 'function(result) {
                    if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                        alert(result.errors.shift());

                    } else {
                        $("#supplier-payment-add").data("select2").val("");
                        jQuery("#payments-list-grid").yiiGridView("update");
                    }
                }',
            'error' => 'function(XHR) {
                    alert("Error occurred adding payment to the Act");
            }',
        ]
    );
    ?>

</div>

<?php
$this->endWidget(); //конец формы
?>

<h4><?= $this->t('Payments') ?></h4>

<?php
$this->widget('bootstrap.widgets.TbButton', [
    'id' => 'refresh',
    'buttonType' => 'ajaxButton',
    'type' => 'primary',
    'size' => 'small',
    'label' => $this->t('Refresh data'),
    'htmlOptions' => [
        'title' => $this->t('Update data after changing payment data'),
    ],
    'ajaxOptions' => [
        'beforeSend' => 'function() {
            jQuery("#payments-list-grid").yiiGridView("update");
            return false;
        }',
    ],
]);

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'link',
    'url' => $this->app()->controller->createUrl('actDownload', ['id' => $model->id]),
    'type' => 'success',
    'size' => 'small',
    'label' => $this->t('Download payment details'),
    'htmlOptions' => ["style" => "margin-left: 10px;"],
]);


$this->widget('bootstrap.widgets.TbGroupGridView', [
    'id' => 'payments-list-grid',
    'dataProvider' => $provider,
    'filterSelector' => '{filter}',
    'ajaxUpdate' => 'act-detail',
    'extraRowExpression' => function ($data) use ($model) {
        /* @var SupplierPaymentRecord $data */
        $supplier = $data->supplier ? $data->supplier->name : $data->supplier_id;
        $amount = $model->getAmountPayments(true, $data->supplier_id);
        $amountWithoutPenalty = $model->getAmountPaymentsWithoutPenalty($data->supplier_id);

        return "<b>Due to be paid by supplier $supplier - <span style=\"font-size: +1.2em;\">$amount</span>, without penalty - <span style=\"font-size: +1.2em;\">$amountWithoutPenalty</span> </b>";
    },
    'extraRowColumns' => ['supplier_id'],
    'columns' => [
        [
            'class' => ButtonColumn::class,
            'header' => $this->t('Payment activity'),
            'template' => "{confirm} {view} {cancel}",
            'buttons' => [
                'confirm' => [
                    'label' => $this->t('Confirm'),
                    'icon' => 'icon-ok',
                    'url' => '\Yii::app()->controller->createUrl("confirm",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-success',
                    ],
                    'visible' => function ($row, $data) use ($availableStatusesPaymentForEdit) {
                        /* @var SupplierPaymentRecord $data */
                        return in_array($data->status, $availableStatusesPaymentForEdit);
                    },
                ],
                'cancel' => [
                    'label' => $this->t('Cancel'),
                    'icon' => 'icon-remove',
                    'url' => '$this->app()->controller->createUrl("cancel",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-danger',
                    ],
                    'visible' => function ($row, $data) use ($availableStatusesPaymentForEdit) {
                        /* @var SupplierPaymentRecord $data */
                        return in_array($data->status, $availableStatusesPaymentForEdit);
                    },
                ],
            ],
        ],
        [
            'name' => 'id',
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        [
            'name' => 'supplier_id',
            'htmlOptions' => ["style" => "display: none"],
            'headerHtmlOptions' => ["style" => "display: none"],
        ],
        [
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->supplier) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->supplier->name, '', [
                    'class' => 'btn-link',
                    'name' => "supplier-{$data->supplier->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => "{$data->supplier->name} ({$data->supplier->id})",
                    'data-content' => $this->renderPartial('_popoverSupplierPayment',
                        ['model' => $data], true),
                ]);
            },
        ],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */

                if (!$data->event) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link("№{$data->event_id} {$data->event->name}", '', [
                    'class' => 'btn-link',
                    'name' => "event-{$data->event->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $data->event->name,
                    'data-content' => $this->renderPartial('_popoverEvent',
                        ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'status',
            'filter' => SupplierPaymentRecord::statuses(),
            'value' => '$data->statusReplacement',
        ],
        [
            'name' => 'contract_id',
            'type' => 'html',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $data->contract->name;
                $text .= '<br>';

                if ($data->payment_after_at > 0) {
                    $text .= '(' . $this->app()->dateFormatter->formatDateTime($data->payment_after_at, 'short', null) . ')';
                }

                return $text;
            },
            'filter' => ArrayUtils::getColumn(SupplierContractRecord::model()->cache(60)->findAll(), 'name', 'id'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'payAmount',
            'filter' => false,
            'type' => 'numberLocale',
        ],
        [
            'header' => $this->t('For payment excluding penalty'),
            'filter' => false,
            'type' => 'numberLocale',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->getPayAmount(false);

            },
        ],
        [
            'name' => 'penalty_amount',
            'type' => 'html',
            'value' => function ($data) use ($format) {
                /* @var SupplierPaymentRecord $data */
                $text = $format->formatNumberLocale($data->penalty_amount);

                if ($data->count_products_problem_delivery) {
                    $text .= " <span title='" . $this->t('penalized items') . ">(<i class='icon-thumbs-down'></i>{$data->count_products_problem_delivery})</span>";
                }

                return $text;
            },
        ],
        [
            'name' => 'returned_amount',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->returned_amount);
                $text .= '<br>';

                if ($data->statementReturnsCount > 0) {
                    $text .= CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                } elseif ($data->statementReturnsPossibleCount) {
                    $text .= CHtml::link($this->t('Add refund'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                }

                return $text;
            },
        ],
        [
            'name' => 'custom_penalty_amount',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->custom_penalty_amount);
                $text .= '<br>';

                if ($data->custom_penalty_amount > 0) {
                    $text .= CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                } else {
                    $text .= CHtml::link($this->t('Add'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                }

                return $text;
            },
        ],
        [
            'name' => 'request_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'delivered_amount_wrong_time',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'paid_amount',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'unpaid_amount',
            'header' => '<span title="' . SupplierPaymentRecord::model()->getAttributeLabel('unpaid_amount') . '"><i class="icon-minus-sign"></i></span>',
            'type' => 'numberLocale',
        ],
        [
            'name' => 'unpaid_fixed_amount',
            'header' => '<span title="' . SupplierPaymentRecord::model()->getAttributeLabel('unpaid_fixed_amount') . '"><i class="icon-plus-sign"></i></span>',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                $text = $this->app()->numberFormatter->formatDecimal($data->unpaid_fixed_amount);
                $text .= '<br>';

                if ($data->includeUnpaidPaymentsCount > 0) {
                    $text .= CHtml::link($this->t('Edit'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                } elseif ($data->unpaidPaymentPossibleCount) {
                    $text .= CHtml::link($this->t('Add'),
                        $this->app()->controller->createUrl('updateAddition', ['id' => $data->id]), ['target' => '_blank']);
                }

                return $text;
            },
        ],
        [
            'class' => ButtonColumn::class,
            'header' => $this->t('Actions with the Act'),
            'template' => "{delete}",
            'deleteButtonUrl' => function ($data) use ($model) {
                return $this->app()->controller->createUrl("actDeletePosition", ["id" => $data->id, "fromActId" => $model->id]);
            },
            'buttons' => [
                'delete' => [
                    'label' => $this->t('Remove payment from Act'),
                ],
            ],
        ],

    ],
]);
?>

<?php
$this->endWidget();
?>

