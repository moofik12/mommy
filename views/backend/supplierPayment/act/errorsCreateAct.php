<?php

use MommyCom\Controller\Backend\SupplierPaymentController;

/**
 * @var SupplierPaymentController $this
 * @var array $errors
 */

$this->pageTitle = $this->t('Error creating payment statements for suppliers');
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Error creating the Act'),
    'headerIcon' => 'icon-th-list',
]);
?>

    <div class="well" xmlns="http://www.w3.org/1999/html">
        <?php
        foreach ($errors as $error) {
            echo "<h5 style='color: #953b39'>$error</h5>";
        }
        ?>
        <br>
        <div><b><?= $this->t('Remove these payments from the list and try again') ?></b></div>
    </div>

<?php
$this->endWidget();
