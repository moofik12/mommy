<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $provider
 * @var SupplierContractRecord $model
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Editing the suppliers list for the contract');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Editing the suppliers list for the contract') . " №{$model->id} " . CHtml::encode($model->name),
    'headerIcon' => 'icon-add',
]); ?>

<?= CHtml::beginForm('', 'post', ['class' => 'form-search', 'id' => 'contractSupplierForm']); ?>

<?php
$this->widget('bootstrap.widgets.TbSelect2', [
    'id' => 'supplier',
    'name' => 'supplier',
    'asDropDownList' => false,
    'options' => [
        'allowClear' => true,
        'minimumInputLength' => 0,
        'placeholder' => $this->t('Supplier'),
        'width' => 'resolve',
        'ajax' => [
            'url' => $this->app()->createUrl('supplier/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                return {
                        name: term,
                        page: page,
                    };
            }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: "<strong>" + value.name + "</strong>" + " <span style=\"color: graytext;\"> (" + value.id + ")</span>",
                    });
                });

                return result;
            }'),
        ],
        'initSelection' => new CJavaScriptExpression("function(element, callback) {
            callback(
                {id: 0, text: " . $this->t('Not selected') . "}
            );
        }"),
    ],
]);
?>
<?php $this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'ajaxSubmit',
    'type' => 'success',
    'label' => $this->t('Add'),
    'url' => $this->createUrl('contractSupplierAdd', ['id' => $model->id]),
    'htmlOptions' => [
        'id' => 'contractSupplierSubmit',
        'data-url-force' => $this->createUrl('contractSupplierAdd', ['id' => $model->id, 'force' => 1]),
    ],
    'ajaxOptions' => [
        'success' => 'function(data){
            if (data.hasInAnotherContract) {

                if (!confirm(data.error + " Перенести поставщика в этот контракт?")) {
                    return;
                }

                var url = $("#contractSupplierSubmit").data("urlForce");
                if (!url) {
                    return;
                }

                $.ajax({
                    "url": url,
                    "data": {
                        "supplier": $("#supplier").val()
                    },
                });

            } else if (data.error) {
                alert(data.error);
                return;
            }


            $("#supplier").select2("val", null);
            $("#suppliers-grid-view").yiiGridView("update");
        }',
    ],
]); ?>
<?= CHtml::endForm(); ?>

<h3><?= $this->t('Suppliers') ?></h3>
<?
//provider for SupplierRecord
$this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'suppliers-grid-view',
    'dataProvider' => $provider,
    'filter' => $provider->model,
//    'filterSelector' => '{filter}, form select',
    'columns' => [
        [
            'name' => 'id',
            'header' => $this->t('Supplier ID'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'name',
            'header' => $this->t('Supplier'),
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
            'deleteButtonUrl' => '\Yii::app()->controller->createUrl("contractSuppliersDelete",array("id"=>' . $model->id . ', "suppliers"=> array($data->id)))',
        ],
    ],
]);
?>

<?php $this->endWidget() ?>
