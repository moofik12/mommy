<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var TbForm $form
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Creating a contract for suppliers');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Creating a contract'),
    'headerIcon' => 'icon-add',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>


