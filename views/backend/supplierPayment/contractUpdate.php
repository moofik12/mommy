<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierPaymentController $this
 * @var TbForm $form
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Editing contract');
/* @var SupplierContractRecord $model */
$model = $form->model;
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Editing contract') . ' №' . $model->id,
    'headerIcon' => 'icon-add',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
