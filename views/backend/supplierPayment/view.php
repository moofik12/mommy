<?php

use MommyCom\Controller\Backend\SupplierPaymentController;
use MommyCom\Model\Db\SupplierPaymentRecord;
use MommyCom\Model\Db\WarehouseProductRecord;

/**
 * @var SupplierPaymentController $this
 * @var CActiveDataProvider $providerWarehouseProducts
 * @var SupplierPaymentRecord $model
 */

$this->pageTitle = $this->t('View payment to the supplier');

$nf = $this->app()->numberFormatter;
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View payment'),
    'headerIcon' => 'icon-th-list',
]);

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        [
            'name' => 'supplier_id',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->supplier ? $data->supplier->name : $data->supplier_id;
            },
        ],
        [
            'name' => 'event_id',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->event ? $data->event->name : $data->event_id;
            },
        ],
        [
            'name' => 'contract_id',
            'value' => function ($data) {
                /* @var SupplierPaymentRecord $data */
                return $data->contract ? $data->contract->name : $data->event_id;
            },
        ],
        'statusReplacement',
        'payment_after_at:datetime',
        'request_amount:number',
        'delivered_amount:number',
        [
            'type' => 'html',
            'name' => 'payAmount',
            'value' => '<b>' . $nf->formatDecimal($model->payAmount) . '</b>',
        ],
        [
            'type' => 'html',
            'name' => 'paid_amount',
            'value' => '<b>' . $nf->formatDecimal($model->paid_amount) . '</b>',
        ],
        'returned_amount:number',
        'penalty_amount:number',
        [
            'name' => $this->t('Return Acts are included'),
            'value' => implode(',', $model->getStatementReturnsID()),
        ],
        'comment',
    ],
]);

?>

<h4><?= $this->t('Warehouse products') ?></h4>

<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerWarehouseProducts,
    'filter' => $providerWarehouseProducts->model,
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */

                if (!$data->event) {
                    return '<b>' . $this->t('No') . '</b>';
                }

                return CHtml::link($data->event->name, '#', [
                    'name' => "event-{$data->event->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'hover',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $data->event->name,
                    'data-content' => $this->renderPartial('_popoverEvent',
                        ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'status',
            'filter' => WarehouseProductRecord::statusReplacements(),
            'value' => '$data->statusReplacement',
        ],
        [
            'name' => 'order_id',
        ],
        [
            'name' => 'is_return',
            'type' => 'boolean',
        ],
        [
            'name' => 'eventProduct.price_purchase',
            'value' => '$data->eventProduct->price_purchase',
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
        ],
    ],
]);


$this->endWidget();
?>
