<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 * @var $this MoneyControlController
 * @var array|string $excludeFilters
 */

$excludeFilters = isset($excludeFilters) ? (is_scalar($excludeFilters) ? [$excludeFilters] : $excludeFilters) : [];
$filters = [];

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

$filters['deliveryEndAt'] = $this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'deliveryEndAt',
    'value' => $this->app()->request->getParam('deliveryEndAt'),
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Period of the last delivery'),
    ],
], true);

$filters['eventStartAt'] = $this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'eventStartAt',
    'value' => $this->app()->request->getParam('eventStartAt'),
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Conducting a flash-sale'),
    ],
], true);

$filters['eventEndAt'] = $this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'eventEndAt',
    'value' => $this->app()->request->getParam('eventEndAt'),
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('End of flash-sale'),
    ],
], true);

$filters['paymentAfterAt'] = $this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'paymentAfterAt',
    'value' => $this->app()->request->getParam('paymentAfterAt'),
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Required payment period'),
    ],
], true);

$filters['paymentAt'] = $this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'paymentAt',
    'value' => $this->app()->request->getParam('paymentAt'),
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Period of recorded payment'),
    ],
], true);

$filters['suppliers'] = $this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'suppliers',
    'value' => null,
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Suppliers'),
    ],
    'asDropDownList' => false,
    'options' => [
        'multiple' => true,
        'width' => '250px;',
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('supplier/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: value.name + " (id:" + value.id + ")",
                    });
                });

                return result;
            }'),
        ],
    ],
], true);

$filters['events'] = $this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'events',
    'value' => null,
    'htmlOptions' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Flash-sales'),
    ],
    'asDropDownList' => false,
    'options' => [
        'multiple' => true,
        'width' => '250px;',
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('event/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: "№" + value.id + " " + value.name,
                    });
                });

                return result;
            }'),
        ],
    ],
], true);

$filters['payTo'] = CHtml::dropDownList('payTo', $this->app()->request->getParam('payTo', 'any'),
    [
        'any' => $this->t('Payment method'),
        'cart' => $this->t('To bank card'),
        'ca' => $this->t('To the settlement account'),
    ]);

$filters['hasInAct'] = CHtml::dropDownList('hasInAct', $this->app()->request->getParam('hasInAct', 'any'),
    [
        'any' => $this->t('Recorded in the Act'),
        1 => $this->t('Recorded in the Act'),
        0 => $this->t('Not recorded in the Act'),
    ]);


$filters = array_values(array_diff_key($filters, array_combine($excludeFilters, $excludeFilters)));
foreach ($filters as $key => $filter) {
    if ($key % 3 == 0) {
        echo '<br>';
    }
    echo $filter;
}

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
