<?php

use MommyCom\Model\Db\SupplierPaymentRecord;

/* @var CMap $summary */

$nf = $this->app()->numberFormatter;
?>

<div id="supplier-payment-summary" class="well pull-right extended-summary" style="width:300px">
    <h4><?= $this->t('Total') ?>: </h4>

    <?php foreach($summary->toArray() as $attribute => $value) : ?>
    <strong><?= SupplierPaymentRecord::model()->getAttributeLabel($attribute) ?>: </strong><?= $nf->formatDecimal($value) ?><br>
    <?php endforeach ?>
</div>
<div class="clearfix"></div>
