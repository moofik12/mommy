<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\BonuspointsPromoRecord;
use MommyCom\Service\BaseController\BackController;

/**
 * @var \MommyCom\Service\BaseController\BackController $this
 */

$this->pageTitle = $this->t('Bonus list');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Mail-out bonuses'),
    'headerIcon' => 'icon-th-list',
]);

$this->renderPartial('_filter');

?>

    <div class="pull-right">
        <?= CHtml::link($this->t('Create'), $this->createUrl('create'), [
            'class' => 'btn btn-primary',
        ]) ?>
    </div>
    <div class="clearfix"></div>

<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span1']],
        ['name' => 'name',],
        ['name' => 'points',],
        [
            'name' => 'bonus_available_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var BonuspointsPromoRecord $data */
                $dateTime = new DateTime('@0');
                $interval = $dateTime->diff(new DateTime('@' . $data->bonus_available_at), true);

                return "{$interval->days} " . $this->t('days');
            },
        ],
        ['name' => 'user_message',],
        ['name' => 'is_stopped', 'type' => 'boolean', 'filter' => $this->app()->format->booleanFormat],
        ['name' => 'start_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'end_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {update} {delete}',
        ],
    ],
]);

$this->endWidget();
