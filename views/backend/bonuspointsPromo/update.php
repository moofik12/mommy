<?php
/**
 * @var \MommyCom\Service\BaseController\BackController $this
 * @var TbForm $form
 */

use MommyCom\Service\BaseController\BackController;

$this->pageTitle = $this->t('Create a new bonus');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Editing mail-out bonus'),
    'headerIcon' => 'icon-th-list',
]);

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span1']],
        ['name' => 'name',],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
    ],
]);

echo $form->render();

$this->endWidget();
