<?php

use MommyCom\Service\BaseController\BackController;

/**
 * @var \MommyCom\Service\BaseController\BackController $this
 * @var TbForm $form
 */

$this->pageTitle = $this->t('Create a new bonus');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Creating mail-out bonus '),
    'headerIcon' => 'icon-th-list',
]);

echo $form->render();

$this->endWidget();
