<?php

use MommyCom\Model\Db\BonuspointsPromoRecord;
use MommyCom\Service\BaseController\BackController;

/**
 * @var \MommyCom\Service\BaseController\BackController $this
 * @var BonuspointsPromoRecord $model
 */

$this->pageTitle = $this->t('View bonus');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => 'Bonus for mailing out' . " {$model->name}",
    'headerIcon' => 'icon-th-list',
]);
?>

    <div>
        <?= CHtml::link($this->t('List'), $this->createUrl('index')) ?>
    </div>

<?php
$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span1']],
        ['name' => 'name',],
        ['name' => 'points',],
        [
            'name' => 'bonus_available_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var BonuspointsPromoRecord $data */
                $dateTime = new DateTime('@0');
                $interval = $dateTime->diff(new DateTime('@' . $data->bonus_available_at), true);

                return "{$interval->days} " . $this->t('days');
            },
        ],
        ['name' => 'user_message',],
        ['name' => 'ulrForUnisender',],

        ['name' => 'is_stopped', 'type' => 'boolean', 'filter' => $this->app()->format->booleanFormat],
        ['name' => 'start_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'end_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],

    ],
]);

$this->endWidget();
