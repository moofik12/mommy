<?php

use MommyCom\Controller\Backend\SupplierEventController;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Service\Translator\RegionalTranslator;

/* @var SupplierEventController $this */
/* @var TbForm $form */
/* @var SupplierRecord $model */
/* @var array $sections */
/* @var RegionalTranslator $translator */

$this->pageTitle = $this->t('Supplier profile');
$this->app()->user->hasFlash('error');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier profile'),
    'headerIcon' => 'icon-add',
]);
?>

<?php if($this->app()->user->hasFlash('error')): ?>
    <div class="text-error text-center">
        <?php echo $this->app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>

<?php
/** @var TbActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'add-admin-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'action' => $this->createUrl('supplierEvent/updateProfile'),
    'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data', 'method' => 'POST'],
]);
?>

<div class="control-group">
    <div class="controls">
        <?= $form->radioButtonList(
            $model,
            'type',
            [
                SupplierRecord::TYPE_LEGAL_ENTITY => 'Legal entity', SupplierRecord::TYPE_INDIVIDUAL => 'Individual',
            ]
        ); ?>
    </div>
</div>

<?php
echo $form->textFieldRow($model, 'full_name');
echo $form->textFieldRow($model, 'address');
echo $form->datepickerRow($model, 'registration_date');
echo $form->textFieldRow($model, 'chief_name');
echo $form->textFieldRow($model, 'chief_passport');
echo $form->textFieldRow($model, 'license');
echo $form->textFieldRow($model, 'npwp');
echo $form->textFieldRow($model, 'tdp');
echo $form->textFieldRow($model, 'bank_name');
echo $form->textFieldRow($model, 'bank_address');
echo $form->textFieldRow($model, 'email');
echo $form->textFieldRow($model, 'phone');
echo $form->textFieldRow($model, 'bank_account');
?>

<div class="control-group">
    <label class="control-label" for="SupplierRecord_messenger"><?= $translator->t('Messenger') ?></label>
    <div class="controls">
        <?= $form->dropDownList($model, 'messenger', ['WhatsApp', 'Line',], ['id' => 'messenger',]); ?>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="certifiable_sections"><?= $translator->t('Certifiable sections') ?></label>
    <div class="controls">
        <?= $form->dropDownList($model, 'certifiable_sections', $sections, [
            'id' => 'certifiable_sections',
            'multiple' => 'multiple',
        ]); ?>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="logo"><?= $translator->t('Logo') ?></label>
    <div class="controls">
        <?= $form->fileField($model, 'logo', ['id' => 'logo', 'name' => 'logo']); ?>
    </div>
</div>

<div class="control-group">
    <div class="controls">
        <?= CHtml::submitButton($translator->t('Save'), ['class' => 'btn btn-success', 'name' => 'submit-button']) ?>
    </div>
</div>

<?php
$this->endWidget();
$this->endWidget();
?>

<script>
    $(function () {
        function resolveFormFields(event) {
            var radioButton = $(event.target);
            var radioButtonValue = parseInt(radioButton.val());

            if ('<?= SupplierRecord::TYPE_INDIVIDUAL ?>' === radioButtonValue) {
                $('.control-group input, .control-group select').each(function () {
                    var name = $(this).attr('name');

                    if (
                        -1 === name.indexOf('chief_passport')
                        && -1 === name.indexOf('npwp')
                        && -1 === name.indexOf('submit-button')
                        && -1 === name.indexOf('type')
                    ) {
                        $(this).parent().parent().hide();
                    }
                });

                return;
            }

            $('.control-group input, .control-group select').each(function () {
                $(this).parent().parent().show();
            });
        }

        $('#SupplierRecord_type_0').on('click, change', resolveFormFields);
        $('#SupplierRecord_type_1').on('click, change', resolveFormFields);

        if (0 === $('input[type="radio"]:checked').length) {
            $('.control-group input, .control-group select').each(function () {
                var name = $(this).attr('name');

                if (-1 === name.indexOf('submit-button') && -1 === name.indexOf('type')) {
                    $(this).parent().parent().hide();
                }
            });
        } else {
            $('input[type="radio"]').parent().hide();
        }
    });
</script>
