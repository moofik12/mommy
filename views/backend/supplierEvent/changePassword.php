<?php

/* @var \MommyCom\Service\BaseController\BackController $this */
/* @var TbForm $form*/
$this->pageTitle = $this->t('Change password');
?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Change password'),
    'headerIcon' => 'icon-add',
]);
?>

<?php if($this->app()->user->hasFlash('success')): ?>
    <div class="text-success text-center">
        <?php echo $this->app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>

<?= $form->render() ?>
<?php $this->endWidget() ?>