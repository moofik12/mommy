<?php

use MommyCom\Controller\Backend\SupplierEventController;
use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Service\Translator\RegionalTranslator;
use MommyCom\Service\CurrentTime;
use Money\Currency;

/* @var SupplierEventController $this */
/* @var CActiveDataProvider $products */
/* @var EventRecord $event */
/* @var RegionalTranslator $translator */
/* @var Currency $currency */

$this->pageTitle = $translator->t('Supplier order');
$request = $this->app()->request;
$startCount = 1;
$isSupplier = (bool)$this->app()->user->getModel()->is_supplier;
$invoiceUrl = $this->createUrl('labelPrinter/supplierInvoice&id=') . $event->id;
$downloadInvoiceUrl = $this->createUrl('supplierEvent/downloadInvoice&id=') . $event->id;
?>

<?php
/** @var CActiveForm $form */
$form = $this->beginWidget('CActiveForm', [
    'action' => ['supplierEvent/update', 'id' => $event->id],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'clientOptions' => [
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'hideErrorMessage' => true,
        'afterValidate' => new CJavaScriptExpression('function(form, data, hasError) {
                        if ($.isEmptyObject(data)) {
                            return true;
                        }

                        return false;
                    }'),
    ],
    'htmlOptions' => [
        'name' => 'data-order',
        'class' => 'text-center',
    ],
]);
?>

<?php if ($event->supplier_confirmation): ?>
    <p class="alert alert-success text-center">
        <strong>
            <?= $translator->t('This order is confirmed by supplier') ?>.
        </strong>
    </p>
<? endif; ?>

<?php if ($event->manager_confirmation): ?>
    <p class="alert alert-success text-center">
        <strong>
            <?= $translator->t('This order is confirmed by brand-manager') ?>.
        </strong>
    </p>
<? endif; ?>

<p><?= $translator->t('Company name') . ': ' . $event->supplier->name ?></p>
<br/>
<strong><?= $translator->t('Flash-sale details') ?></strong>
<p>№<?= $event->id ?></p>
<p><?= $translator->t('Name') . ': ' . $event->name ?></p>
<p>
    <?= $translator->t('Start date') . ': ' . CurrentTime::getDateTimeMutable()
        ->setTimestamp($event->start_at)
        ->format('Y-m-d') ?>
</p>
<p>
    <?= $translator->t('End date') . ': ' . CurrentTime::getDateTimeMutable()
        ->setTimestamp($event->end_at)
        ->format('Y-m-d') ?>
</p>
<p>
    <?= $translator->t('Shipment start date') . ': ' . CurrentTime::getDateTimeMutable()
        ->setTimestamp($event->mailing_start_at)
        ->format('Y-m-d') ?>
</p>
<p><?= $translator->t('Total amount') . ': ' ?><span class="total-amount"></span></p>

<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $products,
    'filter' => $products->model,
    'enableSorting' => false,
    'summaryText' => $translator->t('Products') . ' {start} - {end} ' . $translator->t('out of') . ' {count}',
    'filterSelector' => '{filter}, form select',
    'rowCssClassExpression' => function ($row, $data) use ($event) {
        /* @var $data EventProductRecord */
        $stockNumber = $event->is_stock ? $data->getStockNumber() : 0;
        if (!is_null($data->number_supplied) && $data->number_supplied !== ($data->number_sold - $stockNumber)) {
            return 'error';
        }
    },
    'columns' => [
        [
            'header' => 'ID',
            'headerHtmlOptions' => ['class' => 'span2'],
            'value' => function () use (&$startCount) {
                return $startCount++;
            },
        ],
        [
            'header' => $translator->t('Stock number'),
            'name' => 'article',
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'header' => $translator->t('Barcode'),
            'headerHtmlOptions' => ['class' => 'span2'],
            'type' => 'raw',
            'value' => function ($data) {
                return \CHtml::image($this->createAbsoluteUrl('labelPrinter/barcode', [
                    'id' => $data->id,
                    'height' => '50',
                    'width' => '2',
                ]));
            },
        ],
        [
            'name' => 'product.name',
            'header' => $translator->t('Product'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'product.brand.name',
            'header' => $translator->t('Brand'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'product.color',
            'header' => $translator->t('Color'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'size',
            'header' => $translator->t('Size'),
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'price_purchase',
            'header' => $translator->t('Price'),
            'headerHtmlOptions' => ['class' => 'span2'],
            'htmlOptions' => ['name' => 'price'],
            'value' => function ($data) {
                return (int)$data->price_purchase;
            },
        ],
        [
            'name' => 'number_sold',
            'header' => $translator->t('Order Qty'),
            'headerHtmlOptions' => ['class' => 'span2'],
            'htmlOptions' => ['name' => 'order_qty'],
            'value' => function ($data) use ($event) {
                $stockNumber = $event->is_stock ? $data->getStockNumber() : 0;

                return (int)$data->number_sold - $stockNumber;
            },
        ],
        [
            'header' => $translator->t('Delivered Qty'),
            'headerHtmlOptions' => ['class' => 'span2'],
            'htmlOptions' => ['name' => 'delivered_qty'],
            'type' => 'raw',
            'value' => function ($data) use ($event) {
                if ($data->event->supplier_confirmation) {
                    $stockNumber = $event->is_stock ? $data->getStockNumber() : 0;

                    return $data->number_supplied ?? $data->number_sold - $stockNumber;
                }

                return \CHtml::tag('input', [
                    'id' => 'delivered_' . $data->id,
                    'type' => 'number',
                    'maxlength' => 4,
                ]);

            },
        ],
        [
            'headerTemplate' => $translator->t('Confirm') . '{item}',
            'headerHtmlOptions' => ['class' => 'span2'],
            'class' => \CCheckBoxColumn::class,
            'selectableRows' => 2,
            'checkBoxHtmlOptions' => [
                'name' => 'ids[]',
            ],
        ],
        [
            'header' => $translator->t('Amount'),
            'headerHtmlOptions' => ['class' => 'span2', 'name' => 'amount'],
            'value' => function ($data) use ($event) {
                $stockNumber = $event->is_stock ? $data->getStockNumber() : 0;

                return ($data->number_sold - $stockNumber) * $data->price_purchase;
            },
        ],
        [
            'header' => $translator->t('Delivered amount'),
            'headerHtmlOptions' => ['class' => 'span2'],
            'htmlOptions' => ['name' => 'delivered_amount'],
            'value' => function ($data) use ($event) {
                if ($data->event->supplier_confirmation) {
                    $stockNumber = $event->is_stock ? $data->getStockNumber() : 0;
                    $amount = ($data->number_supplied ?? ($data->number_sold - $stockNumber)) * $data->price_purchase;

                    return $amount;
                }

                return 0;

            },
        ],
        [
            'header' => $translator->t('Picture'),
            'headerHtmlOptions' => ['class' => 'span2'],
            'type' => 'raw',
            'value' => function ($data) {
                return !$data->product->logo->isEmpty ?
                    CHtml::image($data->product->logo->getThumbnail('mid320')->url) : '';
            },
        ],
        [
            'type' => 'raw',
            'header' => $translator->t('Brand-manager comment'),
            'htmlOptions' => ['name' => 'partial_delivery_comment', 'style' => (!$isSupplier && !$event->manager_confirmation) ?: 'display:none;'],
            'headerHtmlOptions' => ['style' => (!$isSupplier && !$event->manager_confirmation) ?: 'display:none;'],
            'filterHtmlOptions' => ['style' => (!$isSupplier && !$event->manager_confirmation) ?: 'display:none;'],
            'value' => function ($data) use ($request) {
                if ($data->number_sold > $data->number_supplied) {
                    return '<textarea></textarea>';
                }
            },
        ],
    ],
]);
?>

<p><?= $this->t('Total amount') ?>: <span class="total-amount"></span></p>

<?php
if (!$event->supplier_confirmation && ($isSupplier || $this->app()->user->isRoot())) {
    $this->widget(\TbButton::class, [
        'htmlOptions' => ['class' => 'btn-success', 'name' => 'supplier_confirm'],
        'buttonType' => 'submit',
        'label' => $translator->t('Send confirmation'),
    ]);
}

if ($event->supplier_confirmation && !$event->manager_confirmation && !$isSupplier) {
    $this->widget(\TbButton::class, [
        'htmlOptions' => ['class' => 'btn-success', 'name' => 'manager_confirm'],
        'buttonType' => 'submit',
        'label' => 'Confirm',
    ]);
}

?>

<a class="btn btn-info" role="button" href="<?= $invoiceUrl ?>"><?= $translator->t('Print invoice') ?></a>
<a class="btn btn-info" role="button"
   href="<?= $downloadInvoiceUrl ?>"><?= $translator->t('Download invoice in XLSX') ?></a>

<?php
$this->endWidget();
?>

<script>
    $(function () {
        var SUBMIT_MANAGER = 'manager_confirm';
        var SUBMIT_MANAGER_URL = '<?= $this->app()->createUrl('supplierEvent/confirm')?>';
        var SUBMIT_SUPPLIER = 'supplier_confirm';
        var SUBMIT_SUPPLIER_URL = '<?= $this->app()->createUrl('supplierEvent/update')?>';

        function currencyFormat(num) {
            return '<?= $currency ?>' + ' ' + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
        }

        function init() {
            init.dataSetCallback = function (index, value) {
                value.dataset.price = $(value).text();
            };

            $('[name="amount"]').each(init.dataSetCallback);
            $('[name="price"]').each(init.dataSetCallback);
            $('[name="delivered_amount"]').each(init.dataSetCallback);

            $('.total-amount').text(calcTotalAmount());

            $('[name="price"]').each(function (index, value) {
                $(value).text(currencyFormat($(value).text()));
            });

            $('[name="delivered_amount"]').each(function (index, value) {
                $(value).text(currencyFormat(value.dataset.price));
            });
        }

        function calcTotalAmount() {
            var totalAmount = 0;
            var deliveredAmounts = $('[name="delivered_amount"]');

            deliveredAmounts.each(function (index, value) {
                totalAmount += parseInt($(value).data('price'));
            });

            return currencyFormat(totalAmount);
        }

        function calcDeliveredAmount(deliveryQuantityElement) {
            if (!deliveryQuantityElement) {
                return;
            }

            if (!(deliveryQuantityElement instanceof jQuery)) {
                deliveryQuantityElement = $(deliveryQuantityElement);
            }

            var deliveredQuantity = deliveryQuantityElement.val();
            var priceElement = deliveryQuantityElement.parent().siblings('[name="price"]');
            var price = parseInt(priceElement.data('price'));
            var deliveredAmount = deliveredQuantity * price;
            var deliveredAmountElement = deliveryQuantityElement.parent().siblings('[name="delivered_amount"]');

            deliveredAmountElement.text(currencyFormat(deliveredAmount));
            deliveredAmountElement.data('price', deliveredAmount);
            $('.total-amount').text(calcTotalAmount());
        }

        function collectData() {
            var checkboxes = $(document).find('.checkbox-column input[type="checkbox"]');
            var confirmedProducts = {};
            $.each(checkboxes, function (index, value) {
                var currentCheckbox = $(value);
                var currentCheckboxParent = currentCheckbox.parent();
                var deliveredQuantity = currentCheckboxParent.siblings('[name="delivered_qty"]').find('input').val();
                var comment = currentCheckboxParent.siblings('[name="partial_delivery_comment"]').find('textarea').val();

                if (undefined === deliveredQuantity) {
                    deliveredQuantity = $(this).parent().siblings('[name="delivered_qty"]').text();
                }

                confirmedProducts[currentCheckbox.val()] = {
                    isConfirmed: value.checked,
                    deliveredQty: parseInt(deliveredQuantity),
                    comment: comment
                };
            });

            return confirmedProducts;
        }

        $(document).on('keyup change', '[name="delivered_qty"] input', function () {
            if ($(this).val() < 0) {
                $(this).val(0);
                return;
            }

            calcDeliveredAmount(this);
        });

        $(document).on('change', '.checkbox-column input[type="checkbox"]', function () {
            var deliveredQuantityElement = $(this).parent().siblings('[name="delivered_qty"]').find('input');

            if (this.checked) {
                var orderQuantity = parseInt($(this).parent().siblings('[name="order_qty"]').text());

                deliveredQuantityElement.prop('disabled', true);
                deliveredQuantityElement.val(orderQuantity);
                calcDeliveredAmount(deliveredQuantityElement);

                return;
            }

            deliveredQuantityElement.prop('disabled', false);
        });

        $(document).on('change', 'thead input[type="checkbox"]', function () {
            var checkboxes = $(document).find('.checkbox-column input[type="checkbox"]');
            $.each(checkboxes, function (index, value) {
                $(value).trigger('change');
            })
        });

        $(document).on('submit click', 'button[type="submit"]', function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (e.target.name === SUBMIT_SUPPLIER) {
                var url = SUBMIT_SUPPLIER_URL;
            } else if (e.target.name === SUBMIT_MANAGER) {
                var url = SUBMIT_MANAGER_URL;
            }

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    products: collectData(),
                    eventId: '<?= $event->id ?>',
                },
                success: function () {
                    window.location.reload();
                }
            });
        });

        init();
    });
</script>