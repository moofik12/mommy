<?php

use MommyCom\Controller\Backend\SupplierController;
use MommyCom\Model\Db\EventRecord;

/* @var $this SupplierController */

$this->pageTitle = $this->t('Suppliers flash-sales');
$controller = $this;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier flash-sales'),
    'headerIcon' => 'icon-th-list',
]);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Flash-sales') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, form select',
    'columns' => [
        [
            'name' => 'id',
            'headerHtmlOptions' => ['class' => 'span2']
        ],
        [
            'name' => 'name'
        ],
        ['name' => 'start_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'end_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'mailing_start_at', 'type' => 'date', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'header' => $this->t('Confirmed by supplier'),
            'type' => 'raw',
            'value' => function($data) {
                /** @var EventRecord $data */
                return $data->supplier_confirmation ? 'Yes' : 'No';
            }
        ],
        [
            'header' => $this->t('Confirmed by brand-manager'),
            'type' => 'raw',
            'value' => function($data) {
                /** @var EventRecord $data */
                return $data->manager_confirmation ? 'Yes' : 'No';
            }
        ],
        [
            'header' => $this->t('# products in stock'),
            'type' => 'raw',
            'value' => function($data) {
                /** @var EventRecord $data */
                $products = $data->products;
                $totalAmount = 0;
                foreach ($products as $product) {
                    $totalAmount += $product->number;
                }

                return $totalAmount;
            }
        ],
        [
            'header' => $this->t('Total amount in stock'),
            'type' => 'raw',
            'value' => function($data) {
                /** @var EventRecord $data */
                $products = $data->products;
                $totalAmount = 0;
                foreach ($products as $product) {
                    $totalAmount += $product->price_purchase * $product->number;
                }

                return $totalAmount;
            }
        ],
        [
            'header' => $this->t('Products sold'),
            'type' => 'raw',
            'value' => function($data) {
                /** @var EventRecord $data */
                $products = $data->products;
                $soldoutSummary = 0;
                foreach ($products as $product) {
                    $soldoutSummary += $product->number_sold;
                }

                return $soldoutSummary;
            }
        ],
        [
            'header' => $this->t('Total amount sold'),
            'type' => 'raw',
            'value' => function($data) {
                /** @var EventRecord $data */
                $products = $data->products;
                $totalAmount = 0;
                foreach ($products as $product) {
                    $totalAmount += $product->price_purchase * $product->number_sold;
                }

                return $totalAmount;
            }
        ],
        [
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                $url = $controller->createAbsoluteUrl('supplierEvent/index', ['id' => $data->id]);

                return '<a class="btn btn-primary" href="' . $url . '">' . $controller->t('Details') . '</a>';
            }
        ],
    ],
]);
?>

<div class="clearfix"></div>
<?php $this->endWidget() ?>
