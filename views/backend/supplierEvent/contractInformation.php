<?php

use MommyCom\Controller\Backend\SupplierEventController;
use MommyCom\YiiComponent\Format;

/**
 * @var SupplierEventController $this
 * @var TbForm $form
 */

/* @var Format $format */
$format = $this->app()->format;
$this->pageTitle = $this->t('Contract information');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View contract'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>

