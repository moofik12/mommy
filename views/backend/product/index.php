<?php

use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\YiiComponent\Utf8;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Products');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Products'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Flash-sales {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        'article',
        ['name' => 'name', 'headerHtmlOptions' => ['class' => 'span4']],
        'brand.name',
        'color',
        ['name' => 'color_code', 'type' => 'raw', 'filter' => ProductColorCodes::instance()->getLabels(), 'value' => function ($data) {
            /* @var $data ProductRecord */
            return $data->color_code > 0 ? ProductColorCodes::instance()->getLabel($data->color_code) : $this->t('Not specified');
        }],
        ['name' => 'section_id', 'type' => 'raw', 'filter' => ProductSectionRecord::getGroupListForFilter(), 'value' => function ($data) {
            /* @var $data ProductRecord */
            return $data->section !== null ? $data->section->name : $this->t('Not specified');
        }],
        ['name' => 'logo_fileid', 'type' => 'raw', 'filter' => false, 'value' => function ($data) {
            /* @var $data ProductRecord */
            return $data->logo->isEmpty ? $this->t('No') : CHtml::image($data->logo->getThumbnailByWidth(40)->url);
        }],
        ['name' => 'images_count', 'headerHtmlOptions' => ['class' => 'span2']],
        'made_in',
        'design_in',
        ['name' => 'ageRangeReplacement', 'filter' => false],
        ['name' => 'target', 'filter' => ProductTargets::instance()->getLabels(), 'value' => function ($data) {
            /* @var $data ProductRecord */
            return Utf8::implode(', ', ProductTargets::instance()->getLabels($data->target));
        }],
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
        ],
    ],
]); ?>
<?php $this->endWidget() ?>
