<?php

use MommyCom\Model\Backend\ProductForm;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\YiiComponent\Utf8;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
/* @var $model ProductRecord */
/* @var $modelForm ProductForm */

$this->pageTitle = $this->t('Products');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Current Information'),
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        'name',
        'article',
        'color',
        'brand.name',
        'category',
        'made_in',
        'design_in',
        'ageRangeReplacement',
        'description:html',
        ['name' => 'photo', 'type' => 'html', 'value' => Utf8::nl2br($model->photo)],
    ],
]); ?>
<?php $this->endWidget() ?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Edit product'),
    'headerIcon' => 'icon-add',
]); ?>
<?php /* Да, я знаю, что форма построена на костылях ;) */ ?>

<?= $form->renderBegin() ?>
<?php if ($model->isHasLogo): ?>
    <div class="control-group ">
        <label class="control-label"><?= $this->t('Current logo') ?></label>
        <div class="controls">
            <?= CHtml::image($model->logo->getThumbnail('mid380')->url, '', ['class' => 'img-polaroid', 'style' => 'max-width: 150px; max-height: 150px;']) ?>
        </div>
    </div>
<?php endif; ?>
<?= $form->elements['logo']->render() ?>

<div class="control-group ">
    <label class="control-label"><?= $this->t('Current images') ?></label>
    <div class="controls" id="current-image-views">
        <?php foreach ($model->images as $image): ?>
            <div class="pull-left current-image" style="padding-right: 5px">
                <?php echo CHtml::link(
                    CHtml::image($image->getThumbnail('mid380')->url, '', ['class' => 'img-polaroid', 'style' => 'max-width: 150px; max-height: 150px;']),
                    $image->file->getWebFullPath(),
                    ['target' => '_blank']
                ); ?>
                <?= CHtml::activeHiddenField($modelForm, 'images[]', ['value' => $image->encodedId, 'uncheckValue' => null]) ?>
                <label>
                    <?= CHtml::button($this->t('Delete'), ['class' => 'btn btn-small btn-danger delete-photo', 'data-id' => $image->encodedId]) ?>
                </label>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="clearfix"></div>
</div>


<?= $form->elements['imagesUpload']->render() ?>
<?= $form->elements['name']->render() ?>
<?= $form->elements['madeIn']->render() ?>
<?= $form->elements['designIn']->render() ?>
<?= $form->elements['description']->render() ?>
<?= $form->elements['sectionId']->render() ?>
<?= $form->elements['colorCode']->render() ?>
<?= $form->renderButtons() ?>
<?= $form->renderEnd() ?>
<?php $this->endWidget() ?>
