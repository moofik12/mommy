<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Model\Product\ProductTargets;

/**
 * @var $eventProduct EventProductRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $eventProduct,
    'attributes' => [
        [
            'name' => 'id',
            'label' => $this->t('Vendor code'),
        ],
        [
            'type' => 'raw',
            'name' => 'product.logo_fileid',
            'value' => CHtml::image($eventProduct->product->logo->getThumbnail('small70')->url, 'logo'
                , ['class' => 'title', 'style' => 'min-width: 72; min-height: 86;']),
        ],
        [
            'name' => 'supplier_id',
            'value' => $eventProduct->supplier ? $eventProduct->supplier->name : $eventProduct->supplier_id,
        ],
        [
            'name' => 'color',
            'type' => 'raw',
            'value' => !empty($eventProduct->color)
                ? '<span class="label label-info">' . $eventProduct->color . '</span>' : '',
        ],
        [
            'name' => 'color_code',
            'type' => 'raw',
            'value' => !empty($eventProduct->product->color_code > 0)
                ? '<span class="label label-info">' . ProductColorCodes::instance()->getLabel($eventProduct->product->color_code) . '</span>' :
                $this->t('Not specified'),
        ],
        [
            'label' => $this->t('Products section'),
            'name' => 'section_id',
            'type' => 'raw',
            'value' => !empty($eventProduct->product->section_id > 0)
                ? '<span class="label label-info">' . ($eventProduct->product->section !== null ? $eventProduct->product->section->name :
                    $this->t('Not found')) . '</span>' : $this->t('Not specified'),
        ],
        [
            'name' => 'size',
            'type' => 'raw',
            'value' => !empty($eventProduct->size)
                ? '<span class="label label-info">' . $eventProduct->size . '</span>' : '',
        ],
        [
            'name' => 'sizeformat',
            'type' => 'raw',
            'value' => !empty($eventProduct->sizeformat)
                ? '<span class="label label-info">' . $eventProduct->sizeformatReplacement . '</span>' : '',
        ],
        [
            'name' => 'label',
            'type' => 'raw',
            'value' => !empty($eventProduct->label)
                ? '<span class="label label-info">' . $eventProduct->label . '</span>' : '',
        ],
        'composition',
        'brand.name',
        'category',
        [
            'name' => 'target',
            'value' => implode(', ', ProductTargets::instance()->getLabels($eventProduct->target)),
        ],
        'made_in',
        'design_in',
    ],
]);
