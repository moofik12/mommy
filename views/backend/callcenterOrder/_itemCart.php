<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;

/**
 * @var $order OrderRecord
 * @var $this CController
 * @var $providerProduct CActiveRecord
 * @var $form CActiveForm
 * @var $enableEdit boolean
 */

$app = $this->app();
$id = 'grid-order-product-' . $order->id;
$csrf = $app->request->enableCsrfValidation ? [$app->request->csrfTokenName => $app->request->csrfToken] : [];
$cf = $app->currencyFormatter;
$nf = $app->numberFormatter;
$cn = $app->countries->getCurrency();

$orderWeightParser = ProductWeight::instance();
$orderWeight = $orderWeightParser->convertToString($order->getWeight());
$deliveryPreAt = $order->preDeliveryAt;
?>

<?php $grid = $this->widget('bootstrap.widgets.TbGroupGridView', [
    'id' => $id,
    'enableHistory' => false,
    'enableSorting' => false,
    'dataProvider' => $providerProduct,
    'selectableRows' => 0,
    'ajaxUpdate' => "{$id}-total",
    'responsiveTable' => true,
    'htmlOptions' => [
        'data-eventend' => $this->app()->getDateFormatter()->formatDateTime($lastEventEndAt, 'long', false),
        'data-deliveryend' => $this->app()->getDateFormatter()->formatDateTime($deliveryPreAt, 'long', false),

        'data-delivery-prices' => CJSON::encode($order->getDeliveryPrices()),
        'data-delivery-price' => $order->getDeliveryPrice(),
        'data-weight' => $orderWeight,
        'data-bonuses' => $order->bonuses,
        'data-params' => CJSON::encode([
            'deliveryType' => $order->delivery_type,
            'promocode' => $order->promocode,
        ]),
        'data-currency' => $this->app()->countries->getCurrency()->getName('short'),
    ],

    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = [
            OrderProductRecord::CALLCENTER_STATUS_UNMODERATED => 'info',
            OrderProductRecord::CALLCENTER_STATUS_CANCELLED => 'error',
            OrderProductRecord::CALLCENTER_STATUS_ACCEPTED => 'success',
        ];

        return CHtml::value($cssClass, $data->callcenter_status);
    },
    'extraRowExpression' => function ($data) {
        /** @var  $data OrderProductRecord */
        $eventEnable = $data->event->isTimeActive;
        $eventStatus = $eventEnable
            ? ' <span class="label label-success">' . $this->t('Active until') . ':' . $data->event->endAtDateString . '</span>'
            : ' <span class="label label-important">' . $this->t('Ended') . '</span>';
        $text = "{$data->event->name} ({$data->event->description_short})" . $eventStatus;

        $text = CHtml::link($text, '', [
            'name' => "event-{$data->event->id}",
            'data-toggle' => 'popover',
            'data-trigger' => 'hover',
            'data-placement' => 'top',
            'data-html' => 'true',

            'data-title' => $data->event->name,
            'data-content' => $this->renderPartial('_popoverEvent',
                ['event' => $data->event], true),
        ]);

        return '<strong>' . $this->t('Flash-sale') . ':' . '</strong> ' . $text;
    },
    'extraRowColumns' => ['event_id'],
    'beforeAjaxUpdate' => 'js:function(id,options) {
        var extendData = $("#" + id).data("params");
        extendData || (extendData = {});
        var params = {};

        var pramsUrl = decodeURIComponent(options.url).split("?");

        if(pramsUrl[1]) {
            var paramsArr = pramsUrl[1].split("&");
            paramsArr.forEach(function(item) { 
                var paramItem = item.split("=");
                params[paramItem[0]] = paramItem[1];
            });
        }

        params = $.extend(params, extendData);

        options.url = pramsUrl[0] + "?" + $.param(params);
        $(document).trigger("total.update.start");
    }',
    'afterAjaxUpdate' => 'js:function(id, data) {
        var grid = $("#" + id),
            form = $("#' . $form->getId() . '");

        var context = $("#contentbox");
        grid.trigger("ajaxUpdate.yiiGridView");
        context.trigger("update.delivery.price");

        form.find(".last-event-end td").html(grid.data("eventend"));
        form.find(".delivery-end td").html(grid.data("deliveryend"));
        form.find("#order-weight").html(grid.data("weight"));
        form.find("select[id*=delivery_type]").data("delivery-prices", grid.data("delivery-prices"));

        jQuery("[data-toggle=popover]").popover();
        $(document).trigger("total.update.end");
    }',
    'columns' => [
        [
            'name' => '',
            'value' => function ($data) {
                return CHtml::checkBox('products_id[]', null, ['value' => $data->product_id, 'products_id' => $data->product_id, 'class' => 'adding_products_id']);
            },
            'type' => 'raw',
            'htmlOptions' => [
                'width' => 5,
            ],
        ],
        [
            'name' => 'event_id',
            'visible' => false,
        ],
        [
            'name' => 'product.id',
            'header' => $this->t('Vendor code'),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var OrderProductRecord $data */
                $text = $data->product_id;
                $text .= $data->product ? "<br><span style=\"color:#959595;\" data-toggle=\"tooltip\" title=\"" .
                    $this->t('Products') . "\">({$data->product->product_id})</span>" : '';

                return $text;
            },
        ],
        [
            'header' => $this->t('Products delivery status '),
            'type' => 'raw',
            'visible' => !$order->is_drop_shipping || count($order->positions) > 1,
            'value' => function ($data) use ($order) {
                /** @var $data OrderProductRecord */
                $text = '<i class="icon-time" style="font-size:25px;" data-toggle="tooltip" title="' .
                    $this->t('Waiting for the end of flash-sale') .
                    '"></i>';
                $isAllInWarehouse = $data->getIsAllInWarehouse();

                if ($isAllInWarehouse || $order->is_drop_shipping) {
                    $text = '';
                    if ($data->getIsAllInWarehouse()) {
                        $text = '<i class="icon-home" style="font-size:25px;" data-toggle="tooltip" title="' .
                            $this->t('At warehouse') .
                            '"></i>';
                        $text .= '<br />';
                    }
                    /*if (
                        ($order->created_at + CallcenterOrderController::ORDER_SEPARATE_PERIOD > time() && !$data->relation_order_product_id)
                        || $order->is_drop_shipping
                    ) {
                        $text .= CHtml::label('Move to new order', 'created-new-order-' . $data->id);
                        $text .= CHtml::checkBox('created-new-order-' . $data->id, false, array('value' => $data->id));
                    }*/
                } elseif ($data->event->isTimeFinished) {
                    $text = '<i class="icon-road" style="font-size:25px;" data-toggle="tooltip" title="' .
                        $this->t('Pending delivery') .
                        '"></i>';
                }
                if (!$data->relation_order_product_id) {
                    $text .= CHtml::label($this->t('Move to new order'), 'created-new-order-' . $data->id);
                    $text .= CHtml::checkBox('created-new-order-' . $data->id, false, ['value' => $data->id]);
                }

                return $text;
            },
            'htmlOptions' => [
                'class' => 'text-center',
            ],
        ],
        [
            'header' => $this->t('Status'),
            'name' => 'virtualStatusText',
            'visible' => !$order->is_drop_shipping,
        ],
        [
            'type' => 'raw',
            'name' => 'product_id',
            'value' => function ($data) {
                return $this->renderPartial('_itemCartProduct', ['eventProduct' => $data->product], true);
            },
        ],
        [
            'name' => 'callcenter_status',
            'type' => 'raw',
            'value' => function ($data, $row, $thisColumn) {
                /** @var $data OrderProductRecord */
                $text = CHtml::value($data->callcenterStatusReplacements(), $data->callcenter_status);
                $text .= '</br>';

                /**
                 * @var EventRecord $event
                 * @var OrderProductRecord $data
                 */
                //fucking awesome code
                $dataColumn = $thisColumn;
                $grid = $dataColumn->grid;

                $options = [
                    'class' => 'TbEditableColumn',
                    'name' => 'callcenter_comment',
                    'value' => '$data->callcenter_comment',
                    'editable' => [
                        'model' => $data,
                        'type' => 'textarea',
                        'attribute' => 'callcenter_comment',
                        'safeOnly' => false,
                        'emptytext' => $this->t('Leave a comment'),
                        'url' => ['callcenterOrder/productEditable'],
                        'params' => [
                            $this->app()->request->csrfTokenName => $this->app()->request->csrfToken,
                        ],
                    ],
                ];
                /** @var TbEditableColumn $column */
                $column = Yii::createComponent($options, $grid);

                //доступ к защищенному методу
                $class = new ReflectionClass($column);
                $method = $class->getMethod('renderDataCellContent');
                $method->setAccessible(true);
                ob_start();
                $method->invokeArgs($column, [$row, $data]);
                $text .= ob_get_clean();

                return $text;
            },
        ],
        [
            'name' => 'storekeeper_status',
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data OrderProductRecord */
                $text = CHtml::value($data->storekeeperStatusReplacements(), $data->storekeeper_status);

                if ($data->storekeeper_status == OrderProductRecord::STOREKEEPER_STATUS_UNAVAILABLE) {
                    $text = CHtml::tag('div', ['class' => 'message alert alert-error'], $text);
                }

                $text .= mb_strlen($data->storekeeper_comment) > 0
                    ? CHtml::tag('div', ['class' => 'message alert alert-warning'], CHtml::encode($data->storekeeper_comment))
                    : '';

                return $text;
            },
        ],
        [
            'type' => 'raw',
            'name' => 'number',
            'value' => function ($data) use ($enableEdit) {
                /** @var OrderProductRecord $data */
                $visiblePrice = $data->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_CANCELLED ? false : true;
                $isEdit = $data->callcenter_status != OrderProductRecord::CALLCENTER_STATUS_ACCEPTED && $enableEdit;

                $addHtmlOptions = $isEdit ? [] : ['disabled' => 'disabled'];
                $maxNumber = $isEdit ? $data->number + $data->product->getNumberAvailable() : $data->number;
                if (!$visiblePrice) {
                    $maxNumber = $data->product->getNumberAvailable();
                }

                //создание формы правки
                ob_start();
                echo CHtml::openTag('div', ['class' => 'cart-form-container']);
                echo CHtml::beginForm();
                echo CHtml::htmlButton('<i class="icon-minus"></i>', ['name' => 'minus', 'class' => 'btn btn-small number']);
                echo CHtml::activeTextField($data, 'number',
                    [
                        'id' => 'number-' . $data->id,
                        'class' => 'number',
                        'size' => 2,
                        'max' => $maxNumber,
                        'maxlength' => 2,
                        'autocomplete' => 'off',

                        'data-price' => $visiblePrice ? $data->price : 0,
                        'data-total' => $visiblePrice ? $data->getPriceTotal() : 0,
                    ] + $addHtmlOptions
                );
                echo CHtml::htmlButton(' <i class="icon-plus"></i>', ['name' => 'plus', 'class' => 'btn btn-small number']);

                echo CHtml::endForm();

                if ($data->event->isTimeFinished
                    && $data->storekeeper_status == OrderProductRecord::STOREKEEPER_STATUS_UNAVAILABLE
                ) {
                    echo CHtml::tag('div', ['class' => 'alert alert-error'], $this->t('Max.') . ' ' . $data->connectedWarehouseProductCount);
                }

                echo CHtml::tag('div', ['class' => 'message-error process-active alert alert-error hide']);
                echo CHtml::closeTag('div');

                return ob_get_clean();
            },
        ],
        [
            'type' => 'raw',
            'name' => 'otherSize',
            'header' => $this->t('Size'),
            'value' => function ($data, $row, $thisColumn) use ($id, $enableEdit) {
                if ($enableEdit) {
                    /**
                     * @var EventRecord $event
                     * @var OrderProductRecord $data
                     */
                    //fucking awesome code
                    $dataColumn = $thisColumn;
                    $grid = $dataColumn->grid;

                    //все ради этого
                    $availableSize = [];

                    /** @var EventProductRecord[] $eventsRecords */
                    $eventsRecords = EventProductRecord::model()
                        ->eventId($data->event_id)
                        ->productId($data->product->product_id)
                        ->findAll();

                    foreach ($eventsRecords as $eventsRecord) {
                        if ($eventsRecord->getIsPossibleToBuy(1, false)) {
                            $availableSize[] = ['value' => $eventsRecord->id, 'text' => $eventsRecord->size];
                        }
                    }

                    $options = [
                        'class' => 'TbEditableColumn',
                        'name' => 'otherSize',
                        'value' => '$data->product->size',
                        'editable' => [
                            'url' => $this->app()->controller->createUrl('callcenterOrder/orderProductReplacementSize'),
                            'params' => [
                                $this->app()->request->csrfTokenName => $this->app()->request->csrfToken,
                            ],
                            'title' => $this->t('Size replacement'),
                            'type' => 'select',
                            'value' => '$data->product_id',
                            'safeOnly' => false,
                            'emptytext' => $this->t('No'),
                            'onSave' => 'js:function(){
                                $("#' . $id . '").yiiGridView("update");
                            }',
                            'htmlOptions' => [
                                'data-source' => CJSON::encode($availableSize),
                            ],
                        ],
                    ];
                    /** @var TbEditableColumn $column */
                    $column = Yii::createComponent($options, $grid);

                    //доступ к защищенному методу
                    $class = new ReflectionClass($column);
                    $method = $class->getMethod('renderDataCellContent');
                    $method->setAccessible(true);
                    $method->invokeArgs($column, [$row, $data]);

                    return '';
                }

                return $data->product->size;
            },
        ],
        [
            'type' => 'raw',
            'name' => 'price',
            'header' => $this->t('Cost') . '(' . $this->t('When ordering') . ')',
            'value' => function ($data) use ($id, $enableEdit, $cn) {
                /** @var $data OrderProductRecord
                 * @var $this CController
                 */
                $text = $data->price;

                if ($data->price != $data->product->price) {
                    $text = '<span class="badge badge-important">' . $data->price . '</span>';

                    if ($enableEdit) {
                        $text .= $this->widget('bootstrap.widgets.TbButton', [
                            'buttonType' => TbButton::BUTTON_AJAXLINK,
                            'type' => 'primary',
                            'icon' => 'icon-refresh',
                            'url' => $this->createUrl('callcenterOrder/refreshProductPrice', ['id' => $data->id]),
                            'ajaxOptions' => [
                                'success' => 'js:function(result, textStatus, jqXHR) {
                                if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                                    alert(result.errors.shift());

                                } else {
                                    var grid = $("#' . $id . '");
                                    grid.yiiGridView("update");
                                }
                            }',
                                'error' => 'js:function() {
                                alert("' . $this->t('Unable to update price') . '");
                            }',
                            ],
                            'htmlOptions' => [
                                'title' => sprintf($this->t('to the price') . ": %+.2f {$cn->getName('short')}", $data->product->price - $data->price),
                            ],
                        ], true);
                    }
                }

                return $text;
            },
        ],
        [
            'name' => 'product.price',
        ],
        [
            'name' => 'sum',
            'type' => 'raw',
            'header' => $this->t('Amount'),
            'value' => function ($data) {
                $enableEdit = $data->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_CANCELLED ? false : true;
                return $enableEdit ? '<b>' . sprintf("%.2f", $data->getPriceTotal()) . '</b>' : 0;
            },
            'htmlOptions' => [
                'class' => 'sum-column',
            ],
        ],
        [
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{confirm} {cancel} {restore}',
            'htmlOptions' => [
                'class' => 'button-column',
                'style' => 'width: 60px;',
            ],
            'buttons' => [
                'confirm' => [
                    'label' => '<i class="icon-ok"></i>',
                    'url' => 'array("callcenterOrder/orderProductUpdate", "id" => $data->id, "status" => "confirm")',
                    'visible' => function ($row, $data) use ($enableEdit) {
                        return $data->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_UNMODERATED && $enableEdit;
                    },
                    'options' => [
                        'title' => $this->t('Confirm'),
                        'class' => 'btn btn-success',
                    ],
                ],
                'cancel' => [
                    'label' => '<i class="icon-remove"></i>',
                    'url' => 'array("callcenterOrder/orderProductUpdate", "id" => $data->id, "status" => "cancel")',
                    'visible' => function ($row, $data) use ($enableEdit) {
                        $visible = in_array($data->callcenter_status, [
                            OrderProductRecord::CALLCENTER_STATUS_UNMODERATED,
                            OrderProductRecord::CALLCENTER_STATUS_ACCEPTED,
                        ]);
                        return $visible && $enableEdit;
                    },
                    'options' => [
                        'title' => $this->t('Cancel'),
                        'class' => 'btn btn-danger',
                    ],
                ],
                'restore' => [
                    'label' => '<i class="icon-repeat"></i>',
                    'url' => 'array("callcenterOrder/orderProductUpdate", "id" => $data->id, "status" => "restore")',
                    'visible' => function ($row, $data) use ($enableEdit) {
                        return $data->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_CANCELLED && $enableEdit;
                    },
                    'options' => [
                        'title' => $this->t('Retrieve'),
                        'class' => 'btn btn-inverse',
                    ],
                ],
            ],
        ],
    ],
]);

echo CHtml::ajaxLink($this->t('Create a task by type'), ['taskBoard/addTaskFromOrderCard'],
    [
        'type' => 'GET',
        'data' => ['products_id' => 'js:$(".helper-hidden-input").attr("data")', 'order_id' => 'js:$(".helper-hidden-input").attr("data-order-id")'],
        'success' => 'js:function(data) {
                    $("#task_by_type").modal("show");
                }',
    ],
    [
        'class' => 'btn btn-info',
        'id' => 'adding-task-from-order-card',
        'style' => 'margin-bottom:6px;margin-right:6px',
    ]
);

echo CHtml::ajaxLink($this->t('Create a manual task'), ['taskBoard/addManualTaskFromOrderCard'],
    [
        'type' => 'GET',
        'data' => ['products_id' => 'js:$(".helper-hidden-input").attr("data")', 'order_id' => 'js:$(".helper-hidden-input").attr("data-order-id")'],
        'success' => 'js:function(data) {
                    $("#manual-task").modal("show");
                }',
    ],
    [
        'class' => 'btn btn-warning',
        'id' => 'adding-manual-task-from-order-card',
        'style' => 'margin-bottom:6px',
    ]
);

echo '<input class="hidden helper-hidden-input" data-order-id = "' . $order->id . '">';

$this->renderPartial('//taskBoard/partial/modal/_task_by_type');
$this->renderPartial('//taskBoard/partial/modal/_manual_task');

$cs = $app->clientScript;
$js = <<<JS
$(".adding_products_id").live("click",function() {
        checkCheckboxList();
        function checkCheckboxList() {
            var array = [];
            $(".adding_products_id:checked").each(function() {
                array.push($(this).val());
            });

            var selected;
            selected = array.join(",") + "";

            if(selected.length > 1) {
                $('.helper-hidden-input').attr("data" , array);
            } else {
                $('.helper-hidden-input').removeAttr("data");
            }
        }
    });
JS;
$cs->registerScript('checkBoxForAddingTask', $js);
?>

<?php if ($enableEdit): ?>
    <?= CHtml::beginForm(['callcenterOrder/orderProductAdd'], 'post', [
        'id' => 'form-add-product',
        'class' => 'form-inline',
    ]) ?>
    <label> <?= $this->t('Add item') ?>: </label>
    <?php
    $this->widget('bootstrap.widgets.TbSelect2', [
        'name' => 'article',
        'asDropDownList' => false,
        'htmlOptions' => [
            'placeholder' => $this->t('Vendor code'),
        ],
        'options' => [
            'width' => '350px',
            'allowClear' => true,
            'ajax' => [
                'url' => $this->app()->createUrl('callcenterOrder/ajaxSearchEventProduct'),
                'type' => 'GET',
                'dataType' => 'json',
                'data' => new CJavaScriptExpression('function (term, page) {
                        return {
                            name: term,
                            page: page
                            };
                        }'),
                'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };

                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: "<strong class=\"label label-info\">" + value.id + "</strong>"
                                    + "<br><img src=\"" + value.logoUrl +  "\" class=\"pull-right\" style=\"max-width: 75px; max-height: 75px;\">"
                                    + "<strong>' . $this->t('Name') . ': </strong>" + value.name
                                    + "<br><strong>' . $this->t('Category') . ': </strong>" + value.category
                                    + "<br><strong>' . $this->t('Size') . ': </strong>" + value.size
                                    + "<br><strong>' . $this->t('Type of size') . ': </strong>" + value.sizeformat
                                    ,
                                });
                            });
                        return result;
                    }'),
            ],
        ],
    ]);
    echo CHtml::hiddenField('order', $order->id);

    echo CHtml::ajaxButton($this->t('Add'), ['callcenterOrder/orderProductAdd'], [
            'type' => 'POST',
            'success' => 'function(result) {
                    if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                        alert(result.errors.shift());
                    } else {
                        var grid = $("#' . $id . '");
                        var selectAddProduct = $("#form-add-product input[id=article]");

                        grid.yiiGridView("update");
                        selectAddProduct.data("select2").clear();
                    }
                }',
            'error' => 'function(XHR) {
                    alert("' . $this->t('Error occurred while adding the item to the order') . '");
                }',
        ]
    );
    ?>
    <?= CHtml::endForm() ?>

    <?php if (true): ?>
        <?= CHtml::beginForm(['callcenterOrder/orderProductAdd'], 'post', [
            'id' => 'order-separate-from',
            'class' => 'form-inline pull-right',
        ]) ?>
        <?php

        echo CHtml::ajaxButton($this->t('Split an order'), $this->createUrl('callcenterOrder/orderSeparate', ['id' => $order->id]), [
                'type' => 'POST',
                'beforeSend' => 'function() {
                    var data = "";
                    var products = $("#products");

                    $("input[name^=created-new-order]").each(function($carry, item) {
                        $item = $(item);

                        if ($item.prop("checked")) {
                            data += "&products[]=" + $item.val();
                        }
                    });

                    if (data.length == 0) {
                        alert("' . $this->t('Select products') . '!");
                        return false;
                    }

                    $("#separate-order-button").attr("disabled", "disabled");
                    this.data += data;
                }',
                'success' => 'function(result) {
                    if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                        $("#separate-order-button").attr("disabled", null);
                        alert(result.errors.shift());
                    } else {
                        var grid = $("#' . $id . '");
                        var $this = $(this);
                        var button = $("#separate-order-button");

                        grid.yiiGridView("update");

                        if (result.data && result.data.editUrl) {
                            button.after("<a target=\"_blank\" href=" + result.data.editUrl + ">' .
                    $this->t('Edit a new order') . '</a>");
                        }

                        button.remove();
                    }
                }',
                'error' => 'function(XHR) {
                    $("#separate-order-button").attr("disabled", null);
                    alert("' . $this->t('Error occurred while adding the item to the order') . '");
                }',
            ]
            , ['class' => 'btn btn-warning', 'id' => 'separate-order-button']);
        ?>
        <?= CHtml::endForm() ?>
    <?php endif ?>

    <div class="clearfix"></div>
<?php endif; ?>
<?php
$points = new ShopBonusPoints();
$points->init($order->user);

//с учетом следующего списания бонуса
$bonus = $bonusPresent = 0;
if (!$order->is_drop_shipping) {
    $bonus = $points->getBonusesCost($order->getPayPrice()) + $order->bonuses;
    $bonusPresent = $points->getAvailableToSpendOnlyPresent($order->getPayPrice()) + $order->bonuses_part_used_present;
}

$payPriceWithBonus = $order->getPrice(false, true, true) - $bonus;
if ($payPriceWithBonus < 0) {
    $payPriceWithBonus = 0;
}
?>
<div id="<?= $id ?>-total" class="well pull-right extended-summary" style="width:350px">
    <span class="icon-refresh show-update-process callcenter-update-button" style="float: right;"></span>
    <b><?= $this->t('Declared value') ?>:</b>
    <b class="total-sum"><?= $cf->format($order->getPrice(), $cn->getName('short')) ?></b><br>
    <?= $this->t('Promo code discount') ?>:
    <b class="total-promocode"><?= $cf->format($order->getPromocodeDiscount(), $cn->getName('short')) ?></b><br>
    <?= $this->t('Discount') ?>:
    <b class="total-promocode"><?= $cf->format($order->getUserDiscount(), $cn->getName('short')) ?></b><br>
    <?= $this->t('Will be credited bonuses') ?>:
    <b class="total-promocode"><?= $cf->format($order->getUserDiscountAsBonus(), $cn->getName('short')) ?></b><br>
    <?= $this->t('Discount offered') ?>:
    <b class="total-promocode"><?= $cf->format($order->getCampaignDiscount(), $cn->getName('short')) ?></b><br>
    <b><?= $this->t('Discount granted') ?>:</b>
    <b class="total-promocode"><?= $cf->format($order->getCountDiscount(), $cn->getName('short')) ?></b><br>
    <b><?= $this->t('Bonuses used') ?>:</b>
    <b class="total-bonuses"><?= $cf->format($bonus, $cn->getName('short')) ?></b>
    <span class="bonus-present" data-toggle="tooltip"
          title="<?= $this->t('Gift bonuses') . '(' . $this->t('discount') . ')' ?>">
			<?php if ($bonusPresent > 0) : ?>
                (<?= $cf->format($bonusPresent, $cn->getName('short')) ?>)
            <?php endif ?>
		</span>
    <br>
    <b><?= $this->t('Paid by bank transfer') ?>:</b>
    <b><?= $cf->format($order->card_payed, $cn->getName('short')) ?></b><br>
    <b><?= $this->t('Amount to be paid') ?>:</b>
    <b class="total-pay-sum"><?= $cf->format($payPriceWithBonus, $cn->getName('short')) ?></b><br>
    <span>
		<?= $this->t('Package weight') ?>:
		<b><?= $nf->formatDecimal($order->getWeight() / 1000) ?> kg</b><br>
        <?= $this->t('Delivery cost') ?>:
		<b class="total-delivery-price"><?= $cf->format($order->getDeliveryPrice(), $cn->getName('short')) ?></b>
        <?= $order->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA ? '<i> (' .
            $this->t('approximately') . ') </i>' : '' ?>
        <br>
		<b><?= $this->t('Amount to be paid including delivery:') ?></b>
		<b class="total-delivery-sum"><?= $cf->format($payPriceWithBonus + $order->getDeliveryPrice(), $cn->getName('short')) ?></b><br>
	</span>
</div>
<div class="clearfix"></div>
