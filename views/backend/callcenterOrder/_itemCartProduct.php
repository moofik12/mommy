<?php

use MommyCom\Model\Db\EventProductRecord;

/**
 * @var $eventProduct EventProductRecord
 * @var $this CController
 */

echo CHtml::link($eventProduct->product->name, '', [
    'name' => "product-{$eventProduct->id}",
    'data-toggle' => 'popover',
    'data-trigger' => 'hover',
    'data-placement' => 'top',
    'data-html' => 'true',

    'data-title' => $eventProduct->product->name,
    'data-content' => $this->renderPartial('_itemCartProductPopover',
        ['eventProduct' => $eventProduct], true),
]);

//fixme перевести на страницу view (пока нет)
echo CHtml::link('(' . $this->t('in the product database') . ')', ['product/update', 'id' => $eventProduct->product_id]
    , ['target' => '_blank', 'style' => 'color: #B44646;', 'class' => 'pull-right']
);

echo CHtml::link('(' . $this->t('on website') . ')', $this->app()->frontendUrlManager->createUrl('product/index'
    , ['id' => $eventProduct->product_id, 'eventId' => $eventProduct->event_id])
    , ['target' => '_blank', 'style' => 'color: #B44646;', 'class' => 'pull-right']
);
