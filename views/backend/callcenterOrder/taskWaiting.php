<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this CallcenterOrderController
 * @var $provider CActiveDataProvider
 */
$this->pageTitle = $this->t('Suspended tasks');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Suspended tasks'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php $this->renderPartial('_filterTask') ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => false,
    'type' => 'striped bordered hover',
    'dataProvider' => $provider,
    'selectableRows' => 1,
    'filterSelector' => '{filter}, form[id=task-filter-form] :input',
    'columns' => [
        [
            'header' => '#',
            'name' => 'id',
        ],
        [
            'name' => 'order_id',
        ],
        [
            'name' => 'order.client_name',
        ],
        [
            'name' => 'order.client_surname',
        ],
        [
            'name' => 'operation_id',
            'value' => '$data->operation->title',
            'htmlOptions' => ['class' => 'span3'],
        ],
        [
            'name' => 'comment_task',
        ],
        [
            'name' => 'next_task_at',
            'value' => function ($data) {
                /** @var $data CallcenterTaskRecord */
                return $this->app()->getDateFormatter()->formatDateTime($data->next_task_at, 'long');
            },
            'htmlOptions' => ['class' => 'span3'],
        ],
        [
            'name' => 'updated_at',
            'value' => function ($data) {
                /** @var $data CallcenterTaskRecord */
                return $this->app()->getDateFormatter()->formatDateTime($data->updated_at, 'long');
            },
            'htmlOptions' => ['class' => 'span3'],
        ],
        [
            'class' => ButtonColumn::class,
            'openNewWindow' => false,
            'template' => '{update}',
            'updateButtonLabel' => $this->t('Edit'),
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("processingTask",array("id"=>$data->primaryKey))',
        ],
    ],
]);
?>

<?php $this->endWidget(); ?>
