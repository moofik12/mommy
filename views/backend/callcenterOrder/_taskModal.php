<?php

use MommyCom\Model\Db\CallcenterTaskRecord;

/**
 * @var $this CController
 */

$this->beginWidget('bootstrap.widgets.TbModal', [
    'htmlOptions' => [
        'id' => 'recall-time-set',
    ],
]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?= $this->t('Time of the next call') ?></h4>
    </div>
<?php
echo CHtml::openTag('div', ['class' => 'modal-body', 'style' => 'overflow-y: visible;']);

echo '<div class="bootstrap-timepicker pull-left">';
$this->widget(
    'bootstrap.widgets.TbTimePicker',
    [
        'name' => 'nextTimeCall',
        'options' => [
            'defaultTime' => 'current',
            'value' => date('H:i'),
        ],
        'htmlOptions' => [
            'autocomplete' => 'off',
        ],
    ]
);
echo '</div>';

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'submit',
    'type' => 'info',
    'label' => $this->t('Apply'),
    'htmlOptions' => [
        'name' => 'status',
        'value' => CallcenterTaskRecord::STATUS_POSTPONED,
        'style' => 'margin-left: 10px;',
    ],
]);

echo CHtml::closeTag('div');

//end modal
$this->endWidget();
