<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this CallcenterOrderController
 * @var $provider CActiveDataProvider
 * @var $data OrderRecord
 */

$this->pageTitle = $this->t('Purchase history');
?>

<style>
    a:not([href]) {
        cursor: pointer;
    }

    .popover-content {
        padding: 0;
    }

    .popover-content th {
        border-left: 0;
    }

    #order-filter-form label {
        display: none;
    }

    #order-filter-form select {
        margin-right: 4px;
    }
</style>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Purchase history'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]);
//render filter
$this->renderPartial('_filter', ['model' => $provider->model]);

$grid = $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => true,
    'type' => 'striped bordered hover',
    'dataProvider' => $provider,
    'template' => "{pager}\n{summary}\n{items}\n{pager}",
    'selectableRows' => 1,
    'filterSelector' => '{filter}, form[id=order-filter-form] :input',
    'afterAjaxUpdate' => 'js:function(id, data) {
            jQuery("[data-toggle=popover]").popover();
        }',
    'columns' => [
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'name' => 'events',
            'header' => $this->t('Order'),
            'url' => $this->createUrl('callcenterOrder/historyDetails'),
            'value' => function ($data) {
                /** @var  OrderRecord $data */
                $text = $this->t('Order') . ' №' . $data->id;
                $text .= ' ' . $this->t('From') . ':' . $this->app()->format->formatDatetime($data->ordered_at);
                $text .= ' ' . $this->t('to whom') . ': ' . CHtml::encode($data->client_name) . ' ' . CHtml::encode($data->client_surname);

                if ($data->isProcessingActive() && $data->admin !== null) {
                    /** @var AdminUserRecord $admin */
                    $admin = $data->admin;
                    $text .= '<br> <span class="label label-important"> ' . $this->t('Editing now') . ': ' . $admin->fullname . '</span>';
                }

                return $text;
            },
            'afterAjaxUpdate' => 'js:function(id, data) {
                    jQuery("[data-toggle=popover]").popover();
                }',
        ],
        [
            'name' => 'user.email',
            'header' => $this->t('User'),
        ],
        [
            'header' => $this->t('Dropshipping order'),
            'name' => 'is_drop_shipping',
            'type' => 'boolean',
        ],
        [
            'name' => 'total',
            'type' => 'raw',
            'header' => $this->t('Amount'),
            'value' => function ($data) {
                /** @var OrderRecord $data */
                $total = $data->price_total;

                return "<b> {$total} </b>";
            },
            'htmlOptions' => [
                'class' => 'sum-column',
            ],
            'headerHtmlOptions' => [
                'width' => '1%',
            ],
        ],
        [
            'name' => 'bonuses',
            'type' => 'raw',
            'header' => $this->t('Bonuses<br>used'),
            'value' => function ($data) {
                /** @var OrderRecord $data */
                return "<b> {$data->bonuses} </b>";
            },
        ],
        [
            'name' => 'card_payed',
            'type' => 'raw',
            'header' => $this->t('Paid'),
            'value' => function ($data) {
                /** @var OrderRecord $data */
                return "<b> {$data->card_payed} </b>";
            },
        ],
        [
            'name' => 'status',
            'type' => 'raw',
            'header' => $this->t('Status'),
            'value' => function ($data) {
                /** @var OrderRecord $data */
                $cssClass = CHtml::value($this->labelCss, $data->processing_status);
                $text = '';
                $statusText = $data->processingStatusReplacement;

                if ($data->isNeedCall()) {
                    $statusText .= '<br>' . $this->t('Call back') . ': ' . $this->app()->format->formatDatetime($data->next_called_at);
                }

                $text .= ' <span class="label ' . $cssClass . '">'
                    . $statusText . '</span>';

                return $text;
            },
            'headerHtmlOptions' => [
                'width' => '1%',
            ],

        ],
        [
            'name' => 'updated_at',
            'header' => $this->t('Last order processing'),
            'value' => function ($data) {
                /** @var $data OrderRecord */
                return $this->app()->getDateFormatter()->formatDateTime($data->updated_at, 'long');
            },

        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("callcenterOrder/processing",array("id"=>$data->primaryKey))',
        ],
    ],
]);
$this->endWidget();
?>

<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
//закрытие popover по нажатию <a class="close"></a>, подключение popover
$cs->registerScript('closePopoverCart', '
    $("#contentbox").on("click", ".grid-view .popover .close", function() {
        $(this).closest("div.popover").siblings("a[data-toggle=\\"popover\\"]").popover("hide");
     });
');
?>
