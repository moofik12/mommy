<?php
/**
 * @var $this CController
 * @var $data OrderRecord
 */

use MommyCom\Model\Db\OrderRecord;

$this->pageTitle = $this->t('Processing tasks');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Processing tasks'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<div class="alert alert-error">
    <h4> <?= $this->t('At this time, the order is being processed by the administrator') ?>:
        <?= isset($data->processingAdmin) ? $data->processingAdmin->login . ' (' . $data->processingAdmin->fullname . ').' : '' ?>
    </h4>
</div>

<?php $this->endWidget(); ?>
