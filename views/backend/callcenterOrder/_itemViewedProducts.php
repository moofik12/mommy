<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\ViewedProducts;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Utf8;

/* @var $order OrderRecord */
/* @var $viewedProducts ViewedProducts */
$orderCatalogProductIds = ArrayUtils::getColumn($order->positions, 'catalog_product_id');
$groupedProducts = GroupedProducts::fromProducts($viewedProducts->getEventProducts());

$products = [];
foreach ($groupedProducts->toArray() as $product) {
    if (!$product->getIsSoldOut() && !in_array($product->product->id, $orderCatalogProductIds)) {
        $products[] = $product;
    }
}
?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'viewed-products',
    'dataProvider' => new CArrayDataProvider($products),
    'columns' => [
        ['header' => $this->t('Promotion item'), 'headerHtmlOptions' => ['class' => 'span1'], 'value' => function ($data) {
            /* @var $data GroupedProduct */
            $eventProductRecords = $data->eventProducts;
            return reset($eventProductRecords)->id;
        }],

        ['header' => $this->t('Flash-sale'), 'headerHtmlOptions' => ['class' => 'span4'], 'type' => 'raw', 'value' => function ($data) {
            /* @var $data GroupedProduct */
            return CHtml::link($data->event->name, '', [
                'data-toggle' => 'popover',
                'data-trigger' => 'hover',
                'data-placement' => 'top',
                'data-html' => 'true',

                'data-title' => $data->event->name,
                'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
            ]);
        }],
        ['header' => $this->t('Products'), 'type' => 'raw', 'value' => function ($data) {
            /* @var $data GroupedProduct */
            $eventProductRecords = $data->eventProducts;
            return CHtml::link($data->product->name, '', [
                'data-toggle' => 'popover',
                'data-trigger' => 'hover',
                'data-placement' => 'top',
                'data-html' => 'true',

                'data-title' => $data->event->name,
                'data-content' => $this->renderPartial('_itemCartProductPopover', ['eventProduct' => reset($eventProductRecords)], true),
            ]);
        }],
        ['header' => $this->t('Sizes available'), 'type' => 'raw', 'value' => function ($data) {
            /* @var $data GroupedProduct */
            return Utf8::implode(', ', $data->getSizes(true));
        }],
        ['header' => $this->t('Price'), 'type' => 'raw', 'value' => function ($data) {
            /* @var $data GroupedProduct */
            return $data->price;
        }],
    ],
]); ?>
