<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Model\Db\UserBlackListRecord;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Model\Product\ViewedProducts;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Delivery\FormModel\AttributesDefinition;
use MommyCom\Service\Deprecated\CurrencyFormatter;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;

/**
 * @var \MommyCom\Service\BaseController\BackController $this
 * @var OrderRecord $data
 * @var array $buyStatistics
 * @var AvailableDeliveries $availableDeliveries
 * @var string $paymentLink
 * @var bool $tokenIsExpired
 */

/** @var CurrencyFormatter $currencyFormatter */

$currencyFormatter = $this->app()->currencyFormatter;
$df = $dateFormatter = $this->app()->dateFormatter;
$csrfTokenName = $this->app()->request->csrfTokenName;
$csrfToken = $this->app()->request->csrfToken;
//возможность редактировать заказа
$enableEdit = isset($enableEdit) ? $enableEdit : true;
$orderWeightParser = ProductWeight::instance();
$orderWeight = $orderWeightParser->convertToString($data->getWeight());

$providerProduct = OrderProductRecord::model()->orderId($data->id)->getDataProvider(false, [
    'pagination' => [
        'pageSize' => 25,
    ],
    'sort' => [
        'defaultOrder' => [
            'event_id' => CSort::SORT_ASC,
            'product_id' => CSort::SORT_ASC,
        ],
    ],
]);
$providerHistory = OrderRecord::model()->notId($data->id)->userId($data->user_id)->getDataProvider(false, [
    'sort' => [
        'defaultOrder' => [
            'ordered_at' => CSort::SORT_DESC,
        ],
    ],
]);
$providerRelatedOrders = new CArrayDataProvider($data->getRelationOrders());
$countRelatedOrders = count($data->getRelationOrders());

//bonus
/** @var  $shopBonusPoints ShopBonusPoints */
$shopBonusPoints = new ShopBonusPoints();
if ($data->user) {
    $shopBonusPoints->init($data->user);
}

//время окончания последней акции (предварительная отгрузка)
$lastEventEndAt = $data->getLastEventEndAt();

//время доставки товара (предварительно)
$deliveryPreAt = $data->getPreDeliveryAt();

$prepareSelectWidget = function (
    DeliveryInterface $delivery,
    string $attribute,
    string $placeholder,
    string $initialText,
    string $ajaxUrl,
    array $dependencies = [],
    array $dependents = []
) use (&$form, $data) {
    $getHtmlId = function ($attribute) use ($data, $delivery) {
        return CHtml::activeId($data, $attribute) . '_' . $data->id . '_' . $delivery->getId();
    };

    $ajaxParams = [];
    foreach ($dependencies as $dependency) {
        // FIXME ugly hack
        if ('cashOnDelivery' === $dependency) {
            $ajaxParams[] = '"' . $dependency . '": $("#' . $getHtmlId($dependency) . '").val(),';
        } else {
            $ajaxParams[] = '"' . $dependency . '": $("#' . $getHtmlId($dependency) . '").select2("val"),';
        }
    }

    $clearEvents = [];
    foreach ($dependents as $dependent) {
        $clearEvents[] = '$("#' . $getHtmlId($dependent) . '").select2("val", "").trigger("change");';
    }

    // FIXME ugly hack
    if ('rateId' === $attribute) {
        $clearEvents[] = '$("#' . $getHtmlId('rateVisualName') . '").val(($("#' . $getHtmlId('rateId') . '").select2("data") || {}).text);';
    }

    $properties = [
        'form' => $form,
        'model' => $data,
        'attribute' => "deliveryAttributes[{$attribute}]",
        'asDropDownList' => false,
        'options' => [
            'allowClear' => true,
            'minimumInputLength' => 0,
            'placeholder' => $placeholder,
            'width' => 'resolve',
            'ajax' => [
                'url' => $ajaxUrl,
                'type' => 'GET',
                'dataType' => 'json',
                'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        ' . implode("\n", $ajaxParams) . '
                        weight: ' . $data->getWeight() . ',
                        price: ' . $data->getPrice() . ',
                        name: term,
                        page: page
                    };
                }'),
                'results' => new CJavaScriptExpression('function(response, page) {
                    var result = {
                        more: page < response.pageCount,
                        results: []
                    };

                    $.each(response.items, function(index, value) {
                        if ("undefined" === typeof value.name_national) {
                            result.results.push({
                                id: value.id,
                                text: "<strong>" + value.name + "</strong>",
                            });
                        } else {
                            result.results.push({
                                id: value.id,
                                text: "<strong>" + value.name + "</strong>" + " <span style=\"color: graytext;\">" + value.name_national + "</span>",
                            });
                        }
                    });

                    return result;
                }'),
            ],
            'initSelection' => new CJavaScriptExpression("function(element, callback) {
                var id=$(element).val();

                if(parseInt(id)) {
                    callback({id: id, text: '{$initialText}'});
                }
            }"),
        ],
        'events' => [
            'change' => new CJavaScriptExpression('function() {
                ' . implode("\n", $clearEvents) . '
            }'),
        ],
        'htmlOptions' => [
            'id' => $getHtmlId($attribute),
            'data-for-delivery' => $delivery->getId(),
            'data-container' => 'tr',
        ],
    ];

    return $this->widget('bootstrap.widgets.TbSelect2', $properties, true);
};

$prepareTextAreaWidget = function (DeliveryInterface $delivery, string $attribute) use (&$form, $data) {
    return $form->textArea($data, "deliveryAttributes[{$attribute}]", [
        'id' => CHtml::activeId($data, $attribute) . '_' . $data->id . '_' . $delivery->getId(),
        'data-for-delivery' => $delivery->getId(),
        'data-container' => 'tr',
    ]);
};

$prepareHiddenFieldWidget = function (DeliveryInterface $delivery, string $attribute) use (&$form, $data) {
    return $form->hiddenField($data, "deliveryAttributes[{$attribute}]", [
        'id' => CHtml::activeId($data, $attribute) . '_' . $data->id . '_' . $delivery->getId(),
    ]);
};

$prepareYesNoWidget = function (DeliveryInterface $delivery, string $attribute, array $dependents = []) use (&$form, $data) {
    $getHtmlId = function ($attribute) use ($data, $delivery) {
        return CHtml::activeId($data, $attribute) . '_' . $data->id . '_' . $delivery->getId();
    };

    $values = [
        (string)false => $this->t('No'),
        (string)true => $this->t('Yes'),
    ];

    $clearEvents = [];
    foreach ($dependents as $dependent) {
        $clearEvents[] = '$("#' . $getHtmlId($dependent) . '").select2("val", "").trigger("change");';
    }

    // FIXME ugly hack
    if ('rateId' === $attribute) {
        $clearEvents[] = '$(\'#' . $getHtmlId('rateVisualName') . '\').val(($(\'#' . $getHtmlId('rateId') . '\').select2(\'data\') || {}).text);';
    }

    $id = CHtml::activeId($data, $attribute) . '_' . $data->id . '_' . $delivery->getId();

    $script = implode("\n", $clearEvents);

    $script = <<<HTML
<script>
$(function() {
  $('#{$id}').on('change', function() {
    {$script}
  });
})
</script>
HTML;

    return $form->dropDownList($data, "deliveryAttributes[{$attribute}]", $values, [
            'id' => $id,
        ]) . $script;
};

?>

<div class="new-item">
    <div class="well new-item-shortinfo">
        <?= "<strong> " . $this->t('Order') . " №{$data->id} </strong>" ?>
        <?= $this->t('From') ?>: <?= $this->app()->format->formatDatetime($data->ordered_at) ?>
        <?= $this->t('to whom') ?>
        : <?= CHtml::encode($data->client_name) . ' ' . CHtml::encode($data->client_surname) ?>
        <span class="pull-right"><strong> <?= $this->t('Status') ?>
                : <?= $data->processingStatusReplacement ?> </strong></span>
    </div>

    <div class="new-item-container">
        <?php /** ФОРМА ЗАКАЗА */ ?>
        <?php
        //форма заказа
        /** @var CActiveForm $form */
        $form = $this->beginWidget('CActiveForm', [
            'action' => ['callcenterOrder/update', 'id' => $data->id],
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'hideErrorMessage' => true,
                'afterValidate' => new CJavaScriptExpression('function(form, data, hasError) {
                        if ($.isEmptyObject(data)) {
                            return true;
                        }

                        return false;
                    }'),
            ],
            'htmlOptions' => [
                'name' => 'data-order',
            ],
        ]);
        ?>
        <?= $form->errorSummary($data, null, null, ['class' => 'alert alert-error']) ?>
        <div class="new-item-buttons">
            <?php if ($enableEdit) : ?>
                <?php $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'submit',
                    'label' => $this->t('awaiting non-cash payment transfer'),
                    'htmlOptions' => [
                        'name' => 'status',
                        'value' => OrderRecord::PROCESSING_CALLCENTER_PREPAY,
                    ],
                ]) ?>
                <?php $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'submit',
                    'type' => 'success',
                    'label' => $this->t('Confirm'),
                    'htmlOptions' => [
                        'name' => 'status',
                        'value' => OrderRecord::PROCESSING_CALLCENTER_CONFIRMED,
                    ],
                    'visible' => $data->user && $data->user->blackList ?
                        !$data->user->blackList->isActive() || $data->card_payed > 0 || $data->getPayPrice() == 0 : true,
                ]) ?>
                <?php $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'button',
                    'type' => 'info',
                    'label' => $this->t('Repeat in...'),
                    'htmlOptions' => [
                        'data-toggle' => 'modal',
                        'data-target' => '#recall-time-set',
                    ],
                ]) ?>
                <?php $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'submit',
                    'type' => 'warning',
                    'label' => $this->t('Does not respond'),
                    'htmlOptions' => [
                        'name' => 'status',
                        'value' => OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
                    ],
                    'visible' => in_array($data->processing_status, [
                        OrderRecord::PROCESSING_UNMODERATED,
                        OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
                        OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING,
                    ]),
                ]) ?>
                <?php $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'submit',
                    'type' => 'danger',
                    'label' => $this->t('Cancel'),
                    'htmlOptions' => [
                        'name' => 'status',
                        'value' => OrderRecord::PROCESSING_CANCELLED,
                    ],
                ]) ?>
                <?php $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'submit',
                    'type' => 'danger',
                    'label' => $this->t('Sales not through the Site'),
                    'htmlOptions' => [
                        'name' => 'status',
                        'value' => OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE,
                    ],
                    'visible' => !!$data->is_drop_shipping,
                ]) ?>
            <?php endif; ?>
        </div>

        <?php /** ДАННЫЕ ЗАКАЗА */ ?>
        <div class="span12" style="width: auto; margin-left: 0;">
            <div class="new-item-order span5">
                <h4><?= $this->t('Order data') ?></h4>
                <?php
                $orderWeightCssClass = $orderWeight > 0 ? '' : 'badge badge-important';

                $deliveryAttributes = [];

                foreach ($availableDeliveries as $delivery) {
                    $deliveryModel = $delivery->createFormModel();

                    if ($delivery->getId() == $data->delivery_type) {
                        $deliveryModel->setAttributes($data->getDeliveryAttributes());
                    }

                    foreach ($deliveryModel->getAttributesDefinition() as $name => $definition) {
                        $template = null;
                        if ($enableEdit) {
                            switch ($definition->type) {
                                case AttributesDefinition::TYPE_HIDDEN:
                                    $attributeValue = $prepareHiddenFieldWidget($delivery, $name);
                                    $template = "<tr style='display: none'><th>{label}</th><td>{value}</td></tr>\n";
                                    break;
                                case AttributesDefinition::TYPE_TEXT_AREA:
                                    $attributeValue = $prepareTextAreaWidget($delivery, $name);
                                    break;
                                case AttributesDefinition::TYPE_YES_NO:
                                    $attributeValue = $prepareYesNoWidget($delivery, $name, $definition->dependents);
                                    break;
                                case AttributesDefinition::TYPE_SELECT:
                                    $attributeValue = $prepareSelectWidget(
                                        $delivery,
                                        $name,
                                        $definition->label,
                                        $definition->resolveInitialValue(),
                                        $definition->ajaxUrl,
                                        $definition->dependencies,
                                        $definition->dependents
                                    );
                                    break;
                                default:
                                    $attributeValue = null;
                            }
                        } else {
                            $attributeValue = $definition->resolveInitialValue();
                        }

                        $deliveryAttributes[] = [
                            'type' => 'raw',
                            'name' => $definition->label,
                            'value' => $attributeValue,
                            'template' => $template,
                        ];
                    }
                }

                $this->widget('bootstrap.widgets.TbDetailView', [
                    'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                    'data' => $data,
                    'attributes' => CMap::mergeArray(
                        [
                            [
                                'type' => 'boolean',
                                'name' => 'is_drop_shipping',
                                'cssClass' => $data->is_drop_shipping ? 'info' : '',
                            ],
                            [
                                'name' => 'supplier.displayName',
                                'cssClass' => $data->is_drop_shipping ? 'info' : '',
                            ],
                            [
                                'type' => 'raw',
                                'name' => 'promocode',
                                'value' => $form->textField($data, 'promocode', [
                                    'id' => CHtml::activeId($data, 'promocode') . $data->id,
                                ]),
                            ],
                            [
                                'type' => 'raw',
                                'name' => 'client_name',
                                'value' => $form->textField($data, 'client_name', [
                                    'id' => CHtml::activeId($data, 'client_name') . $data->id,
                                ]),
                            ],
                            [
                                'type' => 'raw',
                                'name' => 'client_surname',
                                'value' => $form->textField($data, 'client_surname', [
                                    'id' => CHtml::activeId($data, 'client_surname') . $data->id,
                                ]),
                            ],
                            [
                                'type' => 'raw',
                                'name' => 'telephone',
                                'value' => $form->textField($data, 'telephone', [
                                    'id' => CHtml::activeId($data, 'telephone') . $data->id,
                                    'value' => (null !== $data->country_code
                                        && 1 !== strpos($data->telephone, $data->country_code)) ?
                                        '+' . $data->country_code . $data->telephone :
                                        $data->telephone,
                                ]),
                            ],
                            [
                                'type' => 'raw',
                                'name' => 'line_account',
                                'value' => $form->textField($data, 'line_account', [
                                    'id' => CHtml::activeId($data, 'line_account') . $data->id,
                                ]),
                            ],
                            //Адрес доставки
                            [
                                'type' => 'raw',
                                'name' => 'delivery_type',
                                'value' => $enableEdit ? $form->dropDownList($data, 'delivery_type', $data->deliveryTypeReplacements(true),
                                    [
                                        'id' => CHtml::activeId($data, 'delivery_type') . $data->id,
                                        /** @var $data OrderRecord */
                                        'data-delivery-prices' => CJSON::encode($data->getDeliveryPrices()),
                                        'data-delivery-price' => $data->getDeliveryPrice(),
                                    ]
                                ) : $data->getDeliveryTypeReplacement(),
                            ],
                        ],
                        $deliveryAttributes,
                        [
                            [
                                'type' => 'raw',
                                'name' => $this->t('Additional actions'),
                                'value' => $this->widget('bootstrap.widgets.TbButton', [
                                    'buttonType' => TbButton::BUTTON_AJAXSUBMIT,
                                    'type' => TbButton::TYPE_INFO,
                                    'label' => $this->t('Copy delivery address to linked orders'),
                                    'url' => $this->createUrl('ajaxCopyDelivery', ['id' => $data->id]),
                                    'ajaxOptions' => [
                                        'success' => 'js:function(data, textStatus, jqXHR) {
                                        if (data) {
                                            if (data.success) {
                                                $.growl.notice({title: "' . $this->t('Data updated') . '", message: ""});
                                        
                                            } else if (data.errors) {
                                                $.growl.error({title: "' . $this->t('Server error') . '", message: data.errors.join()});
                                            }
                                        
                                            $("#grid-history-order-related-' . $data->id . '").yiiGridView("update");
                                            return;
                                        }
                                        
                                        $.growl.error({title: "' . $this->t('No result data') . '"});
                                    }',

                                        'error' => 'js:function(jqXHR, textStatus, errorThrown) {
                                        $.growl.error({title: "' . $this->t('Server error') . '", message: jqXHR.responseText});
                                    }',
                                    ],
                                ], true),
                                'visible' => $countRelatedOrders > 0,
                            ],

                            [
                                'name' => 'weight',
                                'type' => 'raw',
                                'value' => '<span id="order-weight" class="' . $orderWeightCssClass . '">'
                                    . $orderWeight . '</span>',
                            ],
                            [
                                'type' => 'raw',
                                'name' => 'mailAfterString',
                                'value' => $this->widget('bootstrap.widgets.TbDatePicker', [
                                    'model' => $data,
                                    'attribute' => 'mailAfterString',
                                    'options' => [
                                        'endDate' => date('d.m.Y', $data->getMailAfterEndAt()),
                                    ],
                                ], true),
                            ],
                        ]
                    ),
                ]); ?>

                <?php /** СТАТИСТИКА ЗАКАЗА */ ?>
                <h5><?= $this->t('Statistics on previous orders') . '(' . $this->t('by phone number') . ')' ?></h5>
                <?php $buyStatistics = $data->user->getBuyStatistics() ?>
                <?php $this->widget('bootstrap.widgets.TbDetailView', [
                    'data' => $buyStatistics,
                    'attributes' => [
                        'orderCount' => [
                            'label' => $this->t('Orders dispatched'),
                            'value' => $buyStatistics['orderCount'],
                        ],
                        'buyoutUnPayedCount' => [
                            'label' => $this->t('In the payment queue'),
                            'value' => $buyStatistics['buyoutUnPayedCount'],
                        ],
                        'buyoutPayedCount' => [
                            'label' => $this->t('Paid'),
                            'value' => $buyStatistics['buyoutPayedCount'],
                        ],
                        'buyoutCancelledCount' => [
                            'label' => $this->t('Not paid and wouldn\'t be paid'),
                            'value' => $buyStatistics['buyoutCancelledCount'],
                        ],
                        'returns' => [
                            'label' => $this->t('Number of returns'),
                            'value' => $buyStatistics['returns'],
                        ],
                        'telephones' => [
                            'label' => $this->t('Phone numbers used'),
                            'value' => implode(', ', $buyStatistics['telephones']),
                        ],
                        'userEmails' => [
                            'label' => $this->t('E-mail addresses used '),
                            'value' => implode(', ', $buyStatistics['userEmails']),
                        ],
                    ],
                ]) ?>
            </div>

            <?php /** ИНФО ЗАКАЗА */ ?>
            <div class="span4">
                <h4><?= $this->t('Order Info') ?></h4>
                <?php
                $this->widget('bootstrap.widgets.TbDetailView', [
                    'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                    'data' => $data,
                    'attributes' => [
                        [
                            'name' => 'ordered_at',
                            'value' => $this->app()->format->formatDatetime($data->ordered_at),
                        ],
                        [
                            'type' => 'raw',
                            'name' => 'is_inform_payment',
                            'value' => $this->widget('bootstrap.widgets.TbToggleButton', [
                                'id' => CHtml::activeId($data, 'is_inform_payment') . $data->id,
                                'model' => $data,
                                'form' => $form,
                                'attribute' => 'is_inform_payment',
                                'enabledLabel' => $this->t('Yes'),
                                'disabledLabel' => $this->t('NO'),
                            ], true),
                        ],
                        [
                            'type' => 'raw',
                            'label' => $this->t('Send SMS with bank details'),
                            'value' => function ($data) {
                                /* @var OrderRecord $data */
                                if (
                                    $data->processing_status == OrderRecord::PROCESSING_CALLCENTER_PREPAY
                                    || ($data->user && $data->user->blackList)
                                ) {
                                    /* @var OrderRecord $data */
                                    return $this->app()->controller->widget('bootstrap.widgets.TbButton', [
                                        'buttonType' => 'button',
                                        'type' => 'info',
                                        'label' => $this->t('Send SMS'),
                                        'htmlOptions' => [
                                            'data-toggle' => 'modal',
                                            'data-target' => '#send-requisites-sms-message',
                                        ],
                                    ], true);
                                }

                                return $this->t('unavailable');
                            },
                        ],
                        [
                            'name' => 'card_payed',
                            'type' => 'raw',
                            'value' => $data->card_payed > 0
                                ? '<span class="text-success">' . $currencyFormatter->format($data->card_payed) . '</span>'
                                : $currencyFormatter->format($data->card_payed),
                        ],
                        [
                            'name' => 'bonuses',
                            'label' => $this->t('Bonuses used'),
                            'value' => function ($data) use ($currencyFormatter) {
                                $result = $this->t('no');
                                /** @var $data OrderRecord */
                                if ($data->bonuses > 0) {
                                    $currencyFormatter->format($data->bonuses);
                                }

                                return $result;
                            },
                        ],
                        [
                            'name' => 'processing_status_prev',
                            'value' => $data->processingStatusReplacement,
                        ],
                        'called_count',
                        ['name' => 'trackcode', 'type' => 'raw', 'filter' => OrderTrackingRecord::statusReplacements(), 'value' => function ($data) {
                            /* @var $data OrderRecord */
                            return CHtml::link($data->trackcode, 'javascript:void(0)', [
                                'data-toggle' => 'popover',
                                'data-placement' => 'right',
                                'data-trigger' => 'hover',
                                'data-html' => 'true',
                                'data-content' => $this->renderPartial(
                                    '_popoverTrackingStatusHistory',
                                    ['history' => $data->tracking ? $data->tracking->statusHistory : []],
                                    true
                                ),
                            ]);
                        }],
                        [
                            'type' => 'raw',
                            'label' => $this->t('Link for tracking'),
                            'value' => function ($data) use ($availableDeliveries) {
                                if (!$data->trackcode || !$availableDeliveries->hasDelivery($data->delivery_type)) {
                                    return '';
                                }

                                $url = $availableDeliveries
                                    ->getDelivery($data->delivery_type)
                                    ->getApi()
                                    ->getTrackUrl($data->trackcode);

                                return CHtml::link($url, $url, ['target' => '_blank']);
                            },
                        ],
                        [
                            'type' => 'raw',
                            'name' => 'delivery',
                            'label' => $this->t('Expected time of order dispatch'),
                            'value' => $df->formatDateTime($data->getStartMailingAt(), 'long', false) . " – " . $df->formatDateTime($data->getMailAfterEndAt(), 'long', false),
                            'cssClass' => 'delivery-end',
                        ],
                        [
                            'name' => 'order_comment',
                            'label' => $this->t('User comment'),
                        ],
                        [
                            'type' => 'ntext',
                            'name' => 'storekeeper_comment',
                            'value' => $data->storekeeper_comment,
                        ],
                        [
                            'type' => 'ntext',
                            'name' => 'supplier_comment',
                            'value' => $data->supplier_comment,
                        ],
                        [
                            'type' => 'raw',
                            'name' => 'callcenter_comment',
                            'value' => $form->textArea($data, 'callcenter_comment',
                                ['id' => CHtml::activeId($data, 'callcenter_comment') . $data->id, 'rows' => 5]),
                        ],
                    ],
                ]); ?>
            </div>

            <?php /** ИНФО ПОЛЬЗОВАТЕЛЯ */ ?>
            <div class="new-item-user span3">
                <h4><?= $this->t('Data on users') ?></h4>
                <?php
                $hasInBlackList = $data->user->blackList && $data->user->blackList->status == UserBlackListRecord::STATUS_ACTIVE
                    ? true : false;
                if (isset($data->user)) {
                    $this->widget('bootstrap.widgets.TbDetailView', [
                        'data' => $data->user,
                        'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                        'attributes' => [
                            [
                                'label' => 'ID',
                                'name' => 'id',
                            ],
                            [
                                'label' => $this->t('Name'),
                                'name' => 'name',
                            ],
                            [
                                'label' => $this->t('Last name '),
                                'name' => 'surname',
                            ],
                            [
                                'type' => 'html',
                                'label' => $this->t('Blacklisted'),
                                'value' => $hasInBlackList ? '<span class="text-error">' . $this->t('YES') . '</span>' : '<span class="text-success">' . $this->t('NO') . '</span>',
                                'cssClass' => $hasInBlackList ? 'error' : 'success',
                            ],
                            [
                                'label' => $this->t('Bonuses'),
                                'name' => 'bonus',
                                'value' => $this->t('on the account') . ' ' .
                                    Yii::t('app', '{n} bonus | {n} bonuses |  {n} bonuses', $shopBonusPoints->getAvailableToSpend()),
                            ],
                            [
                                'label' => $this->t('Discount') . ' (%)',
                                'name' => 'discount_percent',
                            ],
                            'email',
                            'telephone',
                            'address',
                        ],
                        'htmlOptions' => [],
                    ]);
                } else {
                    echo $this->t('No result data') . '!';
                }
                ?>
            </div>
        </div>
        <?php
        //ХАК без этого не выводятся ошибки
        $validateAttributes = array_merge(array_keys($data->getAttributes()), ['mailAfterString']);
        foreach ($validateAttributes as $attribute) {
            $form->error($data, $attribute);
        }
        ?>
        <?php //modal
        if ($enableEdit) {
            $this->renderPartial('_itemModal', ['order' => $data]);
            $this->widget('bootstrap.widgets.TbButton', [
                'buttonType' => 'submit',
                'htmlOptions' => [
                    'class' => 'btn btn-primary',
                ],
                'label' => $this->t('Save'),
            ]);
        } ?>
        <?php $this->endWidget(); //конец формы?>
        <div class="clearfix"></div>


        <?php
        $form = $this->beginWidget('CActiveForm', [
            'action' => ['callcenterOrder/resendCreateMail', 'id' => $data->id],
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'hideErrorMessage' => true,
                'afterValidate' => new CJavaScriptExpression('function(form, data, hasError) {
                        if ($.isEmptyObject(data)) {
                            return true;
                        }

                        return false;
                    }'),
            ],
            'htmlOptions' => [
                'name' => 'data-order',
            ],
        ]);

        $this->widget('bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'label' => $this->t('Resend e-mail'),
            'htmlOptions' => [
                'name' => 'order',
                'value' => $data->id,
            ],
        ]);

        if ($tokenIsExpired) {
            $this->widget('bootstrap.widgets.TbButton', [
                'buttonType' => 'submit',
                'label' => $this->t('Generate link'),
                'htmlOptions' => [
                    'name' => 'payment_link',
                    'value' => $data->id,
                ],
            ]);
        }

        $this->widget('bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'label' => $this->t('Copy link'),
            'htmlOptions' => [
                'class' => 'copy-clipboard',
            ],
        ]);

        echo "<input class='margin-left-10 clipboard' value='" . $paymentLink . "'/>";

        $this->endWidget();
        ?>

        <h4><?= $this->t('Shopping cart') ?></h4>
        <?php $this->renderPartial('_itemCart', [
            'providerProduct' => $providerProduct,
            'order' => $data,
            'form' => $form,
            'enableEdit' => $enableEdit,
            'deliveryEndAt' => $deliveryPreAt,
            'lastEventEndAt' => $lastEventEndAt,
        ]) ?>

        <h4><?= $this->t('Related orders') ?></h4>
        <?php $this->renderPartial('_itemHistory', [
            'id' => 'grid-history-order-related-' . $data->id,
            'providerHistory' => $providerRelatedOrders,
            'order' => $data,
            'form' => $form,
            'enableEdit' => $enableEdit,
            'deliveryEndAt' => $deliveryPreAt,
            'lastEventEndAt' => $lastEventEndAt,
        ]) ?>

        <h4><?= $this->t('Questions about the current order') ?></h4>
        <?php
        $providerQuestions = new CArrayDataProvider($data->questions, [
            'sort' => [
                'defaultOrder' => [
                    'created_at' => CSort::SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        ?>
        <?php $this->renderPartial('_itemQuestions', ['providerQuestions' => $providerQuestions]); ?>

        <h4><?= $this->t('Previous orders:') ?></h4>
        <?php $this->renderPartial('_itemHistory', [
            'providerHistory' => $providerHistory,
            'order' => $data,
            'form' => $form,
            'enableEdit' => $enableEdit,
            'deliveryEndAt' => $deliveryPreAt,
            'lastEventEndAt' => $lastEventEndAt,
        ]) ?>

        <h4><?= $this->t('Recently viewed products') ?></h4>
        <?php $this->renderPartial('_itemViewedProducts', [
            'order' => $data,
            'viewedProducts' => ViewedProducts::fromUser($data->user, true, 50),
        ]) ?>
    </div>

</div>
<div id="process-load" class="alert alert-info hide">
    <?= $this->t('Data loading') ?>
</div>
<script>
    $('[name="payment_link"]').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "<?= $this->createUrl('ajaxGeneratePaymentLink', ['id' => $data->id]) ?>",
            data: {
                '<?= $csrfTokenName ?>': '<?= $csrfToken ?>'
            },
            dataType: 'json',
            success: function (data) {
                $('.clipboard').val(data.payment_link);
            }
        });
    });

    $('.copy-clipboard').on('click', function (e) {
        e.preventDefault();
        $('.clipboard').select();
        document.execCommand("copy");
    });
</script>
