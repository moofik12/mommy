<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Delivery\AvailableDeliveries;

/**
 * @var $this CallcenterOrderController
 * @var OrderRecord $data
 * @var bool $enableEdit
 * @var array $buyStatistic
 * @var null|AdminUserRecord $orderStartedFirstEditEndLostAdmin
 * @var AvailableDeliveries $availableDeliveries
 * @var string $paymentLink
 * @var bool $tokenIsExpired
 */

$request = $this->app()->request;
$this->pageTitle = $this->t('Order Processing');
$orderStartedFirstEditEndLostAdmin = isset($orderStartedFirstEditEndLostAdmin) ? $orderStartedFirstEditEndLostAdmin : null;
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order Processing'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]);
?>

<?php
$timeCorrection = OrderRecord::NEW_STRATEGY_USER_DISCOUNT_BENEFIT_AS_BONUS_UNTIL;
$dateTimeCorrection = date_create();
$dateTimeCorrection->setTimestamp($timeCorrection);
$interval = date_diff(new DateTime(), $dateTimeCorrection);
?>
<?php if (OrderRecord::isEnableUserDiscountAsBonus($data->ordered_at)) : ?>
    <!-- Уведомление об изменениях персональной скидки -->
    <div class="alert <?= $interval->d < 5 ? 'alert-error' : '' ?>">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?= $this->t('Attention! Discount is accrued in the form of a bonus after receiving an order if promotional code or discount offer has not been used') ?></strong>
    </div>
<?php endif ?>

<?php if ($orderStartedFirstEditEndLostAdmin && $enableEdit) : ?>
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong><?= $this->t('Attention') ?>
            !</strong> <?= $this->t('This order has already started being processed by') ?> <?= $orderStartedFirstEditEndLostAdmin->fullname ?>
        (<?= $orderStartedFirstEditEndLostAdmin->login ?>)
    </div>
<?php endif; ?>
    <!-- Уведомление об изменениях оплаты обратной доставки -->

<?php
$this->renderPartial('_item', [
    'data' => $data,
    'enableEdit' => $enableEdit,
    'availableDeliveries' => $availableDeliveries,
    'paymentLink' => $paymentLink,
    'tokenIsExpired' => $tokenIsExpired
]);

$this->endWidget();

if ($data instanceof CActiveRecord) {
    $path = $this->app()->assetManager->publish(
        Yii::getPathOfAlias('assets.markup')
    );
    /** @var CClientScript $cs */
    $cs = $this->app()->clientScript;
    if ($enableEdit) {
        $cs->registerScriptFile($path . '/js/callcenter.js');
    }

    $cs->registerCssFile($path . '/css/callcenter.css');
}
