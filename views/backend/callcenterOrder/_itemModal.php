<?php

use MommyCom\Model\Db\OrderRecord;

/* @var $this CController
 * @var $order OrderRecord
 */

$this->beginWidget('bootstrap.widgets.TbModal', [
    'htmlOptions' => [
        'id' => 'recall-time-set',
    ],
]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?= $this->t('Time of the next call') ?></h4>
    </div>
<?php
echo CHtml::openTag('div', ['class' => 'modal-body', 'style' => 'overflow-y: visible;']);

echo '<div class="bootstrap-timepicker pull-left">';
$this->widget(
    'bootstrap.widgets.TbTimePicker',
    [
        'name' => 'nextTimeCall',
        'options' => [
            'defaultTime' => 'current',
            'value' => date('H:i'),
        ],
        'htmlOptions' => [
            'autocomplete' => 'off',
        ],
    ]
);
echo '</div>';

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'submit',
    'type' => 'info',
    'label' => $this->t('Apply'),
    'htmlOptions' => [
        'name' => 'status',
        'value' => OrderRecord::PROCESSING_CALLCENTER_CALL_LATER,
        'style' => 'margin-left: 10px;',
    ],
]);

echo CHtml::closeTag('div');

//end modal
$this->endWidget();

//send requisites sms
$this->beginWidget('bootstrap.widgets.TbModal', [
    'htmlOptions' => [
        'id' => 'send-requisites-sms-message',
    ],
]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?= $this->t('Sending SMS with bank details') ?></h4>
    </div>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <b><?= $this->t('To send the correct order amount you must first confirm products in the new order') ?></b>
    </div>

<?php
echo CHtml::openTag('div', ['class' => 'modal-body', 'style' => 'overflow-y: visible;']);

echo '<div class="bootstrap-timepicker pull-left">';
echo CHtml::telField('phone', $order->telephone, ['id' => 'sms-message-test-phone']);
echo '</div>';

$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'ajaxButton',
    'type' => 'info',
    'label' => $this->t('Send'),
    'url' => $this->app()->controller->createUrl('sendRequisitesSmsMessage', ['id' => $order->id]),
    'ajaxOptions' => [
        'type' => 'POST',
        'data' => [$this->app()->request->csrfTokenName => $this->app()->request->csrfToken],
        'beforeSend' => 'function(jqXHR, settings) {
            var textPhone = $("#sms-message-test-phone");
            if (textPhone) {
                if (settings.data) {
                    settings.data += "&"
                }

                settings.data += ("phone=" + encodeURIComponent(textPhone.val()));
            }
        }',
        'error' => 'function(data, textStatus, jqXHR) {
            alert(textStatus);
        }',
        'success' => 'function(data) {
            if (data && data.error) {
                alert(data.error);
                return;
            }

            alert("' . $this->t('Message sent') . '");
            $("#send-requisites-sms-message").modal("hide");
        }',
    ],
]);

echo CHtml::closeTag('div');

//end modal
$this->endWidget();
