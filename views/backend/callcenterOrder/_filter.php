<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'order-filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo $filterForm->textFieldRow($model, 'id');

echo $filterForm->dropDownList($model, 'processing_status', $model->processingStatusReplacements(),
    ['prompt' => $this->t('Order status')]);

echo $filterForm->textFieldRow($model, 'price_total', ['placeholder' => $this->t('Subtotal') . '(' .
    $this->t('not canceled ') . ')']);

echo $filterForm->textFieldRow($model, 'client_name');

echo $filterForm->textFieldRow($model, 'client_surname');

echo $filterForm->textFieldRow($model, 'telephone');

echo $filterForm->textFieldRow($model, 'trackcode');

echo $filterForm->textFieldRow($model, 'promocode');

echo $filterForm->dropDownListRow($model, 'is_drop_shipping', [
    '' => $this->t('Dropshipping flash-sale'),
    0 => $this->t('No'),
    1 => $this->t('Yes'),
]);

echo CHtml::textField('filterProductId', '', ['placeholder' => $this->t('Vendor code')]);

echo CHtml::textField('filterEventId', '', ['placeholder' => 'ID of the promotion']);

$this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'filterSupplierID',
    'asDropDownList' => false,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Supplier'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('supplier/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: value.name,
                    });
                });

                return result;
            }'),
        ],
    ],

]);

echo $filterForm->select2Row($model, 'user_id', [
    'asDropDownList' => false,
    'multiple' => false,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('User'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('callcenterOrder/ajaxSearchUser'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    result.results.push({
                                        id: value.id,
                                        text: "<strong>" + value.email + "</strong>"
                                            + " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>",
                                    });
                                });

                                return result;
                            }'),
        ],
    ],
]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
