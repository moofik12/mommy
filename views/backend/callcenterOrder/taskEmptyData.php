<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this CController
 * @var $data OrderRecord
 */

$this->pageTitle = $this->t('Task history');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Task history'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<div class="alert alert-info">
    <h4> <?= $this->t('No tasks to complete') ?> </h4>
</div>

<?php $this->endWidget(); ?>
