<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this CallcenterOrderController
 * @var $model CallcenterTaskRecord
 * @var $orderProvider CActiveDataProvider
 * @var $taskProvider CActiveDataProvider
 */

$this->pageTitle = $this->t('Task history');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Task history'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?= CHtml::beginForm(['callcenterOrder/processingTask', 'id' => $model->id]) ?>
<?php if ($model->hasErrors()): ?>
    <div class="alert alert-error">
        <?= CHtml::errorSummary($model) ?>
    </div>
<?php endif; ?>

<div class="alert pull-left">
    <?= $this->t('When you perform the task <strong>, you must edit the order</strong>if the task requires it.') ?>
</div>

<div class="pull-right" style="margin-bottom: 10px;">
    <?php $this->widget('bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'type' => 'success',
        'label' => $this->t('Completed'),
        'htmlOptions' => [
            'name' => 'status',
            'value' => CallcenterTaskRecord::STATUS_COMPLETED,
        ],
    ]) ?>
    <?php $this->widget('bootstrap.widgets.TbButton', [
        'buttonType' => 'button',
        'type' => 'info',
        'label' => $this->t('Repeat in...'),
        'htmlOptions' => [
            'data-toggle' => 'modal',
            'data-target' => '#recall-time-set',
        ],
    ]) ?>
    <?php $this->widget('bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'type' => 'danger',
        'label' => $this->t('Cannot be fulfilled '),
        'htmlOptions' => [
            'name' => 'status',
            'value' => CallcenterTaskRecord::STATUS_FAILED,
        ],
    ]) ?>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
    'data' => $model,
    'attributes' => [
        [
            'name' => 'operation_id',
            'value' => isset($model->operation) ? $model->operation->title : '',
        ],
        [
            'name' => 'status',
            'value' => $model->statusReplacement(),
        ],
        [
            'name' => 'order_id',
            'label' => $this->t('Order'),
            'value' => $model->order_id,
        ],
        [
            'name' => 'comment_task',
        ],
        [
            'type' => 'raw',
            'name' => 'comment_callcenter',
            'value' => CHtml::activeTextArea($model, 'comment_callcenter', ['style' => 'width: 100%; padding: 0;',
                'rows' => 10]),
        ],
    ],
]); ?>

<?php //modal
$this->renderPartial('_taskModal'); ?>
<?= CHtml::endForm(); ?>

<h5><?= $this->t('Orders') ?></h5>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => false,
    'type' => 'striped bordered hover',
    'dataProvider' => $orderProvider,
    'selectableRows' => 1,
    'columns' => [
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'name' => 'events',
            'header' => $this->t('Order'),
            'url' => $this->createUrl('callcenterOrder/historyDetails'),
            'value' => function ($data) {
                /** @var  OrderRecord $data */
                $text = $this->t('Order') . ' №' . $data->id;
                $text .= ' ' . $this->t('From') . ':' . $this->app()->format->formatDatetime($data->ordered_at);
                $text .= ' ' . $this->t('to whom') . ': ' . CHtml::encode($data->client_name) . ' ' . CHtml::encode($data->client_surname);

                if ($data->isProcessingActive() && $data->admin !== null) {
                    /** @var AdminUserRecord $admin */
                    $admin = $data->admin;
                    $text .= '<br> <span class="label label-important"> ' . $this->t('Editing now') . ': ' . $admin->fullname . '</span>';
                }

                return $text;
            },
            'afterAjaxUpdate' => 'js:function(id, data) {
                    jQuery("[data-toggle=popover]").popover();
                }',
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
            'updateButtonLabel' => $this->t('Edit'),
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("callcenterOrder/processing",array("id"=>$data->primaryKey))',
            'headerHtmlOptions' => [
                'style' => 'width: 1%;',
            ],
        ],
    ],
]);
?>

<?php if ($orderProvider->itemCount > 0) : ?>
    <h5><?= $this->t('History') ?></h5>
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
        'enableHistory' => false,
        'type' => 'striped bordered hover',
        'dataProvider' => $taskProvider,
        'selectableRows' => 1,
        'columns' => [
            [
                'name' => 'id',
                'header' => '#',
                'headerHtmlOptions' => ['class' => 'span1'],
            ],
            [
                'name' => 'status',
                'value' => '$data->statusReplacement()',
                'headerHtmlOptions' => ['class' => 'span1'],
            ],
            [
                'name' => 'comment_task',
            ],
            [
                'name' => 'comment_callcenter',
            ],
            [
                'name' => 'operation.title',
            ],
            [
                'name' => 'created_at',
                'filter' => false,
                'value' => function ($data) {
                    $text = '';
                    /** @var $data CallcenterTaskRecord */
                    if ($data->created_at > 0) {
                        $text = $this->app()->getDateFormatter()->formatDateTime($data->updated_at, 'short');
                    }
                    return $text;
                },
                'htmlOptions' => ['class' => 'span2'],
            ],
            [
                'name' => 'performed_at',
                'filter' => false,
                'value' => function ($data) {
                    $text = '';
                    /** @var $data CallcenterTaskRecord */
                    if ($data->performed_at > 0) {
                        $text = $this->app()->getDateFormatter()->formatDateTime($data->updated_at, 'short');
                    }
                    return $text;
                },
                'htmlOptions' => ['class' => 'span2'],
            ],
            [
                'class' => ButtonColumn::class,
                'template' => '{update}',
                'updateButtonLabel' => $this->t('Edit'),
                'updateButtonUrl' => '$this->app()->controller->createUrl("callcenterOrder/processingTask",array("id"=>$data->primaryKey))',
                'headerHtmlOptions' => [
                    'style' => 'width: 1%;',
                ],
                'buttons' => [
                    'update' => [
                        'visible' => function ($index, $data) {
                            $statusVisible = [
                                CallcenterTaskRecord::STATUS_NEW,
                                CallcenterTaskRecord::STATUS_REPEAT,
                                CallcenterTaskRecord::STATUS_POSTPONED,
                            ];
                            $result = false;

                            /** @var $data CallcenterTaskRecord */
                            if (in_array($data->status, $statusVisible)) {
                                $result = true;
                            }

                            return $result;
                        },
                    ],
                ],
            ],
        ],
    ]);
    ?>

<?php endif; ?>

<?php $this->endWidget(); ?>
