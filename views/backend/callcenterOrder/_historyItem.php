<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Delivery\AvailableDeliveries;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;

/**
 * @var BackController $this
 * @var OrderRecord $model
 * @var AvailableDeliveries $availableDeliveries
 */

$id = 'form-order-history-' . $model->getPrimaryKey();
$productId = 'grid-order-product-history-' . $model->getPrimaryKey();
$providerProduct = OrderProductRecord::model()->orderId($model->getPrimaryKey())->getDataProvider(false, [
    'pagination' => false,
]);
$cf = $this->app()->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$df = $dateFormatter = $this->app()->dateFormatter;

//bonus
/** @var  $shopBonusPoints ShopBonusPoints */
$shopBonusPoints = new ShopBonusPoints();
if ($model->user) {
    $shopBonusPoints->init($model->user);
}

$functionGetOrderId = function (OrderRecord $order) {
    return $order->id;
}
?>


    <div class="span6">
        <h4><?= $this->t('Order data') ?></h4>
        <?php $form = $this->beginWidget('CActiveForm', [
            'id' => $id,
            'htmlOptions' => [
                'name' => 'copy-address',
            ],
        ]);
        $this->widget('bootstrap.widgets.TbDetailView', [
            'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
            'data' => $model,
            'attributes' => [
                [
                    'name' => 'ordered_at',
                    'value' => $this->app()->format->formatDatetime($model->ordered_at),
                ],
                [
                    'type' => 'boolean',
                    'name' => 'is_inform_payment',
                ],
                [
                    'type' => 'boolean',
                    'name' => 'is_drop_shipping',
                    'cssClass' => $model->is_drop_shipping ? 'info' : '',
                ],
                [
                    'name' => 'supplier.displayName',
                    'cssClass' => $model->is_drop_shipping ? 'info' : '',
                ],
                [
                    'name' => $this->t('Related orders'),
                    'value' => implode(',', array_map($functionGetOrderId, $model->getRelationOrders())),
                ],

                [
                    'name' => 'promocode',
                ],
                [
                    'name' => 'card_payed',
                    'type' => 'raw',
                    'value' => $model->card_payed > 0
                        ? '<span class="text-success">' . $cf->format($model->card_payed, $cn->getName('short')) . '</span>'
                        : $cf->format($model->card_payed, $cn->getName('short')),
                ],
                [
                    'name' => 'processing_status_prev',
                    'value' => CHtml::value($model->processingStatusReplacements(), $model->processing_status_prev),
                ],
                'called_count',
                'client_name',
                'client_surname',
                'telephone',

                //Адрес доставки
                [
                    'type' => 'raw',
                    'name' => 'delivery_type',
                    'value' => CHtml::value($model->deliveryTypeReplacements(), $model->delivery_type)
                        . $form->hiddenField($model, 'delivery_type', ['id' => $id . '-delivery_type']),
                ],
                [
                    'type' => 'raw',
                    'name' => $this->t('Street'),
                    'cssClass' => 'hide',
                    'value' => $this->t('no'),
                ],
                [
                    'type' => 'raw',
                    'name' => $this->t('Building number'),
                    'cssClass' => 'hide',
                    'value' => $model->getDeliveryAttribute('building', $this->t('no')),
                ],
                [
                    'type' => 'raw',
                    'name' => $this->t('Nova Post Branch'),
                    'cssClass' => 'hide',
                    'value' => $form->hiddenField($model, 'novaposta_id', [
                        'id' => $id . '-novaposta_id',
                        //для вставки значения в select2 при копировании формы
                        'data-name' => '',
                        'value' => '',
                    ]),
                ],
                [
                    'type' => 'raw',
                    'name' => 'address',
                    'value' => $model->getDeliveryAttribute('address', '')
                        . $form->hiddenField($model, 'deliveryAttributes[address]', ['id' => $id . '-address']),
                ],
                [
                    'type' => 'raw',
                    'name' => 'zipcode',
                    'value' => $model->getDeliveryAttribute('zipcode', '')
                        . $form->hiddenField($model, 'deliveryAttributes[zipcode]', ['id' => $id . '-zipcode']),
                ],
                [
                    'type' => 'raw',
                    'name' => 'history',
                    'label' => $this->t('History'),
                    'value' => CHtml::link($this->t('Order editing history'), '', [
                        'name' => "history-{$model->id}",
                        'data-toggle' => 'popover',
                        'data-trigger' => 'hover',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $this->t('History'),
                        'data-content' => $this->renderPartial('_processingStatusHistoryPopover',
                            ['dataHistory' => $model->getProcessingStatusHistory()], true),
                    ]),
                ],
                ['name' => 'trackcode', 'type' => 'raw', 'filter' => OrderTrackingRecord::statusReplacements(), 'value' => function ($data) {
                    /* @var $data OrderRecord */
                    return CHtml::link($data->trackcode, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverTrackingStatusHistory',
                            ['history' => $data->tracking ? $data->tracking->statusHistory : []],
                            true
                        ),
                    ]);
                }],
                [
                    'type' => 'raw',
                    'label' => $this->t('Link for tracking'),
                    'value' => function ($data) use ($availableDeliveries) {
                        if (!$data->trackcode || !$availableDeliveries->hasDelivery($data->delivery_type)) {
                            return '';
                        }

                        $url = $availableDeliveries
                            ->getDelivery($data->delivery_type)
                            ->getApi()
                            ->getTrackUrl($data->trackcode);

                        return CHtml::link($url, $url, ['target' => '_blank']);
                    },
                ],
                [
                    'type' => 'raw',
                    'name' => 'delivery',
                    'label' => $this->t('Expected time of order dispatch'),
                    'value' => $df->formatDateTime($model->getStartMailingAt(), 'long', false) . " – " . $df->formatDateTime($model->getMailAfterEndAt(), 'long', false),
                    'cssClass' => 'delivery-end',
                ],
                [
                    'name' => 'order_comment',
                    'type' => 'ntext',
                    'value' => $model->order_comment,
                ],
                [
                    'name' => 'callcenter_comment',
                    'type' => 'ntext',
                    'value' => $model->callcenter_comment,
                ],
                [
                    'name' => 'storekeeper_comment',
                    'type' => 'ntext',
                    'value' => $model->storekeeper_comment,
                ],
                [
                    'name' => 'supplier_comment',
                    'type' => 'ntext',
                    'value' => $model->supplier_comment,
                ],
            ],
        ]);
        //fixme показать кнопку копирования адреса только если refer action "processing" (пока так)
        $refer = $this->app()->request->urlReferrer;
        $referUrl = substr($refer, 0, strpos($refer, 'id'));
        if (strpos($referUrl, 'processing') && strpos($referUrl, 'Task') === false) {
            echo CHtml::submitButton($this->t('Copy shipping address'), ['class' => 'btn btn-primary']);
        }
        $this->endWidget(); ?>
    </div>

    <div class="span4">
        <?php if (isset($model->user)): ?>
            <h4><?= $this->t('Data on users') ?></h4>
            <?php $this->widget('bootstrap.widgets.TbDetailView', [
                'data' => $model->user,
                'attributes' => [
                    [
                        'label' => 'ID',
                        'name' => 'id',
                    ],
                    [
                        'label' => $this->t('Name'),
                        'name' => 'name',
                    ],
                    [
                        'label' => $this->t('Last name '),
                        'name' => 'surname',
                    ],
                    [
                        'label' => $this->t('Bonuses'),
                        'name' => 'bonus',
                        'value' => $this->t('on the account') . ' '
                            . Yii::t('app', '{n} bonus | {n} bonuses |  {n} bonuses', $shopBonusPoints->getAvailableToSpend()),
                    ],
                    [
                        'label' => $this->t('Discount') . ' (%)',
                        'name' => 'discount_percent',
                    ],
                    'email',
                    'telephone',
                    'address',
                ],
                'htmlOptions' => [
                ],
            ]); ?>

            <h5><?= $this->t('Statistics on previous orders') ?>
                (<?= $this->t('by phone number') ?>)</h5>
            <?php $buyStatistics = $model->user->getBuyStatistics() ?>
            <?php $this->widget('bootstrap.widgets.TbDetailView', [
                'data' => $buyStatistics,
                'attributes' => [
                    'orderCount' => [
                        'label' => $this->t('Orders dispatched'),
                        'value' => $buyStatistics['orderCount'],
                    ],
                    'buyoutUnPayedCount' => [
                        'label' => $this->t('In the payment queue'),
                        'value' => $buyStatistics['buyoutUnPayedCount'],
                    ],
                    'buyoutPayedCount' => [
                        'label' => $this->t('Paid'),
                        'value' => $buyStatistics['buyoutPayedCount'],
                    ],
                    'buyoutCancelledCount' => [
                        'label' => $this->t('Not paid and wouldn\'t be paid'),
                        'value' => $buyStatistics['buyoutCancelledCount'],
                    ],
                    'returns' => [
                        'label' => $this->t('Number of returns'),
                        'value' => $buyStatistics['returns'],
                    ],
                    'telephones' => [
                        'label' => $this->t('Phone numbers used'),
                        'value' => implode(', ', $buyStatistics['telephones']),
                    ],
                    'userEmails' => [
                        'label' => $this->t('E-mail addresses used '),
                        'value' => implode(', ', $buyStatistics['userEmails']),
                    ],
                ],
            ]) ?>
        <?php endif ?>
    </div>
    <div class="clearfix"></div>
<?php
/**
 * @var $data OrderProductRecord
 */
$this->renderPartial('_historyItemCart', [
    'data' => $providerProduct->model, 'providerProduct' => $providerProduct, 'model' => $model,
]);
