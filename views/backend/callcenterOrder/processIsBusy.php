<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this CController
 * @var $data OrderRecord
 */

$this->pageTitle = $this->t('Order Processing');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order Processing'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<div class="alert alert-error">
    <h4> <?= $this->t('At this time, the order is being processed by the administrator') ?>:
        <?= isset($data->admin) ? $data->admin->login . ' (' . $data->admin->fullname . ').' : '' ?>
    </h4>
</div>

<?php $this->endWidget(); ?>
