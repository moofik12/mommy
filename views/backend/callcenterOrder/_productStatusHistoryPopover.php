<?php

use MommyCom\Model\Db\OrderProductRecord;

/**
 * @var $callcenterHistory array
 * @var $storekeeperHistory array
 * @var $model OrderProductRecord
 * @var $this CController
 */

$dataHistory = $callcenterHistory + $storekeeperHistory;
ksort($dataHistory);

?>
<table class="items table table-striped table-bordered table-hover">
    <th style="text-align: center"><?= $this->t('Date') ?></th>
    <th style="text-align: center"><?= $this->t('Status') ?></th>
    <th style="text-align: center"><?= $this->t('Operator') ?></th>
    <?php
    foreach ($dataHistory as $time => $data) {
        echo '<tr>';
        echo '<td>' . $this->app()->dateFormatter->formatDateTime($time, 'medium', 'short') . '</td>';
        echo '<td>' . $data['status'] . '</td>';
        echo '<td>' . $data['admin_id'] . '</td>';
        echo '</tr>';
    }
    ?>
</table>
