<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $order OrderRecord
 * @var $this CController
 */

$id = isset($id) ? $id : 'grid-history-order-' . $order->id;

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => $id,
    'enableHistory' => false,
    'type' => 'striped bordered hover',
    'dataProvider' => $providerHistory,
    'selectableRows' => 1,
    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = '';
        /** @var $data OrderRecord */
        if ($data->processing_status == OrderRecord::PROCESSING_UNMODERATED) {
            $cssClass = 'info';
        }

        return $cssClass;
    },
    'columns' => [
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'name' => 'event',
            'header' => $this->t('Order'),
            'url' => $this->createUrl('callcenterOrder/historyDetails'),
            'afterAjaxUpdate' => 'js:function(id, data) {
                jQuery("[data-toggle=popover]").popover();
            }',
            'value' => function ($data) {
                /** @var OrderRecord $data */
                $cssClass = CHtml::value($this->labelCss, $data->processing_status);
                $template = $this->t('Order') . ' №{id}' .
                    $this->t('From') . ': {data} ' .
                    $this->t('Recipient ') . ': {owner} {edited} {status}';

                $replacements = [
                    '{id}' => $data->id,
                    '{data}' => $this->app()->format->formatDatetime($data->ordered_at),
                    '{owner}' => CHtml::encode($data->client_name) . ' ' . CHtml::encode($data->client_surname),
                    '{edited}' => $data->isProcessingActive() && !$data->isMyProcessingActive() ?
                        '<span class="label label-important"> ' . $this->t('Being processed by another operator') . ': '
                        . ($data->admin ? $data->admin->login : '')
                        . '</span>' : '',
                    '{status}' => '<span class="pull-right label ' . $cssClass . '">' .
                        $this->t('Status') . ": {$data->processingStatusReplacement} </span>",
                ];

                return str_replace(array_keys($replacements), array_values($replacements), $template);
            },
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{merge}',
            'buttons' => [
                'merge' => [
                    'label' => $this->t('Merge'),
                    'options' => [
                        'class' => 'btn-danger',
                    ],
                    'url' => function ($data) use ($order) {
                        return $this->createUrl('callcenterOrder/orderMerge', ['id' => $order->id, 'mergeId' => $data->id]);
                    },
                    'visible' => function ($row, $data) use ($order) {
                        $result = false;
                        /** @var $data OrderRecord */
                        if (($data->processing_status == OrderRecord::PROCESSING_UNMODERATED
                                || $data->processing_status == OrderRecord::PROCESSING_CALLCENTER_CONFIRMED
                                || $data->processing_status == OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED
                                || $data->processing_status == OrderRecord::PROCESSING_CALLCENTER_CALL_NOT_RESPONDING
                                || $data->processing_status == OrderRecord::PROCESSING_CALLCENTER_CALL_LATER
                            )
                            && !$order->isCanceled()
                            && ($data->admin_id == $this->app()->user->id || !$data->isProcessingActive())
                            && $this->app()->controller->action->id == 'processing'
                            && ($data->created_at + CallcenterOrderController::ORDER_MERGE_PERIOD > $order->ordered_at
                                || $this->app()->user->isRoot()
                            )
                        ) {
                            $result = true;
                        }

                        return $result;
                    },
                    'click' => 'js:function(e) {
                        var button = $(this);
                        e.preventDefault();

                        if (button.attr("disabled")) {
                            return;
                        }

                        if (!confirm("' . $this->t('Are you sure') . '?")) {
                            return;
                        }

                        button.attr("disabled", "disabled");
                        $.ajax({
                            type: "GET",
                            url: jQuery(this).attr("href"),
                            success: function(data, textStatus) {
                                if (textStatus != "success") {
                                    alert("' . $this->t('Unable to perform action') . '");
                                    return;
                                }

                                if (data.errors && $.isArray(data.errors) && data.errors.length > 0) {
                                    alert(data.errors[0]);
                                    return;
                                }

                                var grids = $(".grid-view");

                                grids.each(function(indx, grid) {
                                    var update = (function() {
                                        var tryNum = 0;
                                        return function() {
                                            var $grid = $(grid);
                                            var backup = grid.outerHTML;

                                            $(grid).yiiGridView("update", {
                                                error: function() {
                                                    if (tryNum++ < 4) {
                                                        $grid.replaceWith(backup);
                                                        setTimeout(update, 400);
                                                    } else {
                                                        alert("' .
                        $this->t('Updating data failed. Refresh page') . '!");
                                                    }
                                                }
                                            });
                                        }
                                    })();
                                    update();
                                });

                                if ($.isArray(data.info) && data.info.length > 0) {
                                    alert(data.info[0]);
                                }

                                if ($.isArray(data.warnings) && data.warnings.length > 0) {
                                    data.warnings.forEach(function($text){
                                        callcenter.warning($text);
                                    });
                                }
                            },
                            error: function () {
                                alert("' . $this->t('Unable to complete request') . '");
                            },
                            complete: function() {
                                button.attr("disabled", null);
                            },
                        });

                        return false;
                    }',
                ],
            ],
            'headerHtmlOptions' => [
                'style' => 'width: 5%;',
            ],
        ],
    ],
]);
