<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'task-filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo CHtml::textField('filterOrderId', '', ['placeholder' => $this->t('Order No. ')]);

echo CHtml::textField('filterOrderClientName', '', ['placeholder' => $this->t('Name')]);

echo CHtml::textField('filterOrderClientSurname', '', ['placeholder' => $this->t('Last name ')]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
