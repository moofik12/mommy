<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\OrderRecord;

// to render - orderid, username, ordertype, eventname,
/**
 * @var $this CallcenterOrderController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('New Orders');
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('New Orders'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => false,
    'enableSorting' => true,
    'type' => 'striped bordered hover',
    'dataProvider' => $provider,
    'selectableRows' => 0,
    'filter' => $provider->model,
    'columns' => [
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'filter' => CHtml::activeTextField($provider->model, 'id', ['value' => $this->app()->request->getParam('OrderRecord')['id']]),
            'type' => 'raw',
            'name' => 'ordered_at',
            'header' => $this->t('Order'),
            'url' => $this->createUrl('callcenterOrder/view'),
            'afterAjaxUpdate' => 'js:function(id, data) {
                    jQuery("[data-toggle=popover]").popover();
                }',
            'value' => function ($data) {
                $text = $this->t('Order') . ' №' . $data->id;
                $text .= ' ' . $this->t('From') . ':' . $this->app()->format->formatDatetime($data->ordered_at);
                $text .= ' ' . $this->t('to whom') . ': ' . CHtml::encode($data->client_name) . ' ' . CHtml::encode($data->client_surname);

                return $text;
            },
        ],
        [
            'name' => 'processing_status',
            'sortable' => false,
            'filter' => false,
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data OrderRecord */
                $text = '';
                $cssClass = CHtml::value($this->labelCss, $data->processing_status);
                $statusText = $data->processingStatusReplacement;

                if ($data->isNeedCall()) {
                    $statusText .= '<br>' . $this->t('Call back') . ': ' . $this->app()->format->formatDatetime($data->next_called_at);
                }

                $text .= ' <span class="label ' . $cssClass . '">'
                    . $statusText . '</span>';

                return $text;
            },
        ],
        [
            'name' => 'user.email',
            'header' => $this->t('User'),
        ],
        [
            'type' => 'raw',
            'name' => 'processing',
            'sortable' => false,
            'filter' => false,
            'header' => false,
            'value' => function ($data) {
                $text = $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'link',
                    'type' => 'danger',
                    'label' => $this->t('Edit'),
                    'htmlOptions' => [
                        'class' => 'pull-right',
                    ],
                    'url' => $this->createUrl('callcenterOrder/processing', ['id' => $data->id]),
                ], true);

                return $text;
            },
            'headerHtmlOptions' => [
                'style' => 'width: 1%;',
            ],
        ],
    ],
]);

$this->endWidget();
?>
<?php
$path = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);
/** @var CClientScript $cs */
$this->app()->clientScript->registerCssFile($path . '/css/callcenter.css');
?>
