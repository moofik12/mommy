<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;

/**
 * @var $data OrderProductRecord
 * @var $this CController
 * @var $providerProduct CActiveRecord
 * @var $model OrderRecord
 */

$id = 'grid-order-product-cart-history-' . $data->getPrimaryKey();
$cf = $this->app()->currencyFormatter;
$cn = $this->app()->countries->getCurrency();

$this->widget('bootstrap.widgets.TbGroupGridView', [
    'id' => $id,
    'enableHistory' => false,
    'enableSorting' => false,
    'type' => 'striped bordered hover',
    'dataProvider' => $providerProduct,
    'selectableRows' => 0,
    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = [
            OrderProductRecord::CALLCENTER_STATUS_UNMODERATED => 'info',
            OrderProductRecord::CALLCENTER_STATUS_CANCELLED => 'error',
            OrderProductRecord::CALLCENTER_STATUS_ACCEPTED => 'success',
        ];

        return CHtml::value($cssClass, $data->callcenter_status);
    },
    'afterAjaxUpdate' => 'js:function(id, data) {
        jQuery("[data-toggle=popover]").popover();
    }',
    'extraRowExpression' => function ($data) {
        /** @var  $data OrderProductRecord */
        $eventEnable = $data->event->isTimeActive;
        $eventStatus = $eventEnable
            ? ' <span class="label label-success">' . $this->t('Active until') . ' ' .
            $data->event->endAtDateString . '</span>'
            : ' <span class="label label-important">' . $this->t('Ended') . '</span>';
        $text = "{$data->event->name} ({$data->event->description_short})" . $eventStatus;

        $text = CHtml::link($text, '', [
            'name' => "event-{$data->event->id}",
            'data-toggle' => 'popover',
            'data-trigger' => 'hover',
            'data-placement' => 'top',
            'data-html' => 'true',

            'data-title' => $data->event->name,
            'data-content' => $this->renderPartial('_popoverEvent',
                ['event' => $data->event], true),
        ]);

        return '<strong>' . $this->t('Flash-sale') . ':' . '</strong> ' . $text;
    },
    'extraRowColumns' => ['event_id'],
    'columns' => [
        [
            'name' => 'event_id',
            'visible' => false,
        ],
        [
            'type' => 'raw',
            'header' => $this->t('Vendor code'),
            'value' => function ($data) {
                /** @var $data OrderProductRecord */
                return CHtml::link($data->product_id, '', [
                    'name' => "history-{$data->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'hover',
                    'data-placement' => 'right',
                    'data-html' => 'true',

                    'data-title' => $this->t('History'),
                    'data-content' => $this->renderPartial('_productStatusHistoryPopover',
                        [
                            'callcenterHistory' => $data->getCallcenterStatusHistory(true),
                            'storekeeperHistory' => $data->getStorekeeperStatusHistory(true),
                            'model' => $data,
                        ], true),
                ]);
            },
        ],
        [
            'header' => $this->t('Products delivery status '),
            'type' => 'raw',
            'visible' => !$model->is_drop_shipping,
            'value' => function ($data) {
                /** @var $data OrderProductRecord */
                $text = '<i class="icon-time" style="font-size:25px;" data-toggle="tooltip" title="' .
                    $this->t('Waiting for the end of flash-sale') . '"></i>';

                if ($data->getIsAllInWarehouse()) {
                    $text = '<i class="icon-home" style="font-size:25px;" data-toggle="tooltip" title="' .
                        $this->t('At warehouse') . '"></i>';
                    $text .= '<br />';
                } elseif ($data->event->isTimeFinished) {
                    $text = '<i class="icon-road" style="font-size:25px;" data-toggle="tooltip" title="' .
                        $this->t('Pending delivery') . '"></i>';
                }

                return $text;
            },
            'htmlOptions' => [
                'class' => 'text-center',
            ],
        ],
        [
            'header' => $this->t('Status'),
            'name' => 'virtualStatusText',
            'visible' => !$model->is_drop_shipping,
        ],
        [
            'type' => 'raw',
            'name' => 'product_id',
            'value' => function ($data) {
                return $this->renderPartial('_itemCartProduct', ['eventProduct' => $data->product], true);
            },
        ],
        [
            'name' => 'callcenter_status',
            'header' => $this->t('Processing status') . ' ' . 'Callcenter',
            'value' => 'CHtml::value($data->callcenterStatusReplacements(), $data->callcenter_status)',
        ],
        [
            'name' => 'storekeeper_status',
            'header' => $this->t('Warehouse processing status'),
            'value' => 'CHtml::value($data->storekeeperStatusReplacements(), $data->storekeeper_status)',
        ],
        [
            'type' => 'raw',
            'name' => 'number',
        ],
        [
            'type' => 'raw',
            'name' => 'price',
            'header' => $this->t('Cost') . '(' . $this->t('When ordering') . ')',
            'value' => function ($data) {
                $text = $data->price;

                if ($data->price != $data->product->price) {
                    $text = '<span class="badge badge-important">' . $data->price . '</span>';
                }

                return $text;
            },
        ],
        'product.price',
        [
            'name' => 'sum',
            'type' => 'raw',
            'header' => $this->t('Amount'),
            'value' => function ($data) {
                $enableEdit = $data->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_CANCELLED ? false : true;
                return $enableEdit ? '<b>' . sprintf("%.2f", $data->getPriceTotal()) . '</b>' : 0;
            },
            'htmlOptions' => [
                'class' => 'sum-column',
            ],
        ],
    ],
]);
?>
<?php //эмуляция тотал в grid так, как загрузка ajax и не изменяется
$total = 0;
foreach ($model->positions as $position) {
    $enable = $position->callcenter_status == OrderProductRecord::CALLCENTER_STATUS_CANCELLED ? false : true;
    $total += $enable ? $position->getPriceTotal() : 0;
}
?>

<div class="well pull-right extended-summary" style="width:350px">
    <b><?= $this->t('Declared value') ?>:</b>
    <b class="total-sum"><?= $cf->format($model->getPrice(), $cn->getName('short')) ?></b><br>
    <b><?= $this->t('Discount granted') ?>:</b>
    <b><?= $cf->format($model->discount, $cn->getName('short')) ?></b><br>
    <?= $this->t('Will be credited bonuses') ?>:
    <b class="total-promocode"><?= $cf->format($model->user_discount_benefit, $cn->getName('short')) ?></b><br>
    <?= $this->t('Bonus credited') ?>:
    <b class="total-promocode"><?= $cf->format($model->user_discount_benefit_paid, $cn->getName('short')) ?></b><br>
    <b><?= $this->t('Bonuses used') ?>:</b>
    <b class="total-bonuses"><?= $cf->format($model->bonuses, $cn->getName('short')) ?></b>
    <span class="bonus-present" data-toggle="tooltip" title="<?= $this->t('Gift bonuses') .
    '(' . $this->t('discount') . ')' ?>">
            <?php if ($model->bonuses_part_used_present > 0) : ?>
                (<?= $cf->format($model->bonuses_part_used_present, $cn->getName('short')) ?>)
            <?php endif ?>
        </span>
    <br>
    <b><?= $this->t('Paid by bank transfer') ?>:</b>
    <b class="total-bonuses"><?= $cf->format($model->card_payed, $cn->getName('short')) ?></b><br>
    <b><?= $this->t('Amount to be paid') ?>:</b>
    <b class="total-pay-sum"><?= $cf->format($model->getPayPriceHistory(), $cn->getName('short')) ?></b><br>
    <span>
		<?= $this->t('Delivery cost') ?>:
		<b class="total-delivery-price"><?= $cf->format($model->getDeliveryPrice(), $cn->getName('short')) ?></b>
        <?= $model->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA ? '<i> ' . '(' .
            $this->t('approximately') . '</i>' : '' ?>
        <br>
		<b><?= $this->t('Amount to be paid including delivery') ?>:</b>
		<b class="total-delivery-sum"><?= $cf->format($model->getPayPriceHistory() + $model->getDeliveryPrice(), $cn->getName('short')) ?></b><br>
	</span>
</div>
