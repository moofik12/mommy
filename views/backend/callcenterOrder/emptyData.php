<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this CController
 * @var $data OrderRecord
 */

$this->pageTitle = $this->t('Order Processing');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order Processing'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<div class="alert alert-info">
    <h4> <?= $this->t('No order') . ' ' . $this->t('To process') ?> </h4>
</div>
<?= CHtml::openTag('label', ['title' => $this->t('New order update every 15 sec')]) ?>
<?= $this->t('Automatic new order update') ?>
<?= CHtml::checkBox('autoUpdate', true, ['style' => 'vertical-align: top;']) ?>
<?= CHtml::closeTag('label') ?>

<?php $this->endWidget(); ?>

<?php

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('checkNewOrders', '
    var autoUpdateCheckBox = $("#autoUpdate");
    var d = $.Deferred();
    var startProcess = true;

    var checkNewOrders = function() {
        if (autoUpdateCheckBox.prop("checked") && d.state() != "rejected") {
            $.ajax({
                url: "' . $this->createUrl('callcenterOrder/ajaxCheckNewOrders') . '",
                beforeSend: function(jqXHR, settings) {
                    startProcess = false;
                },
            })
            .success(function(data, textStatus) {
                if (!data.errors) {
                    if(confirm("' . $this->t('There is an order for editing. Check it?') . '?' . '")) {
                        d.resolve();
                    } else {
                        startProcess = true;
                    }
                } else {
                    startProcess = true;
                }
            })
            .error(function(XHR) {
                d.reject();
            });
        }
    }

    setInterval(function(){
        if (startProcess) {
            checkNewOrders();
        }

    }, 1000*15);

    d.done(function(){
        window.location.href = "' . $this->createUrl('callcenterOrder/new') . '";
    })
    .fail(function(){
        autoUpdateCheckBox.prop("checked", false).prop("disabled", "disabled");
        alert("' . $this->t('Error. Further verification is not possible!') . '");
    });
')
?>
