<?php

use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Widget\Backend\ButtonColumn;

$this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'enableHistory'=> false,
    'type'=>'striped bordered hover',
    'dataProvider' => $providerQuestions,
    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = '';
        $app = $this->app();
        if (($app->user->id === $data->owner_id || $app->user->id === $data->user_id) && (int)$data->answer_counter !== 0) {
            $cssClass = 'success';
        } elseif ($app->user->id === $data->owner_id) {
            $cssClass = 'warning';
        } elseif ($app->user->id === $data->user_id) {
            $cssClass = 'info';
        }

        return $cssClass;
    },
    'columns' => array(
        array(
            'header' => '#',
            'type' => 'raw',
            'name' => 'id',
            'htmlOptions' => array(
                'style' => 'text-align:center; vertical-align: inherit',
                'class' => 'span2'
            )
        ),
        array(
            'header' => $this->t('Created by'),
            'type' => 'raw',
            'name' => 'owner_id',
            'value' => function($data) {
                return (int)$data->owner_id === (int)$this->app()->user->id ?
                    $this->t('You') : $data->owner->fullname;
            },
            'htmlOptions' => array(
                'style' => 'text-align:center; vertical-align: inherit',
                'class' => 'span2'
            )
        ),
        array(
            'header' => $this->t('Сonsignee'),
            'name' => 'user_id',
            'type' => 'raw',
            'filter' => false,
            'htmlOptions' => array(
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2'
            ),
            'value' => function ($data) {
                if ((int)$this->app()->user->id === (int)$data->user_id) {
                    return '<i class="icon-share-alt"></i>' . $this->t('For you');
                } elseif ((int)$data->user_id !== 0) {
                    return '<i class="icon-share-alt"></i> ' . $data->user->fullname;
                }
                return $this->t('No user defined. The task is available for all.');
            }
        ),
        array(
            'header' => $this->t('Created'),
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
            'htmlOptions' => array(
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2'
            )
        ),
        array(
            'header' => $this->t('Question'),
            'type' => 'raw',
            'name' => 'question_description',
            'filter' => false,
            'htmlOptions' => array(
                'style' => 'text-align:center; vertical-align: inherit',
                'class' => 'span2'
            )
        ),
        array(
            'header' => $this->t('Number of responses'),
            'type' => 'raw',
            'name' => 'answer_counter',
            'filter' => false,
            'htmlOptions' => array(
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2'
            ),
            'value' => function ($data) {
                if ((int)$data->answer_counter !== 0) {
                    /* @var $data OrderTrackingRecord */
                    return CHtml::link(Yii::t('application', '{n} answer|{n} answers', $data->answer_counter), 'javascript:void(0)', array(
                        'data-toggle' => 'popover',
                        'data-placement' => 'left',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('//taskBoard/partial/modal/_answers', array('id' => $data->id), true)
                    ));
                } else {
                    return $this->t('No responses');
                }
            }
        ),
        array(
            'class' => ButtonColumn::class,
            'template' => '{view} ',
            'buttons' => array(
                'view' => array(
                    'options' => array(
                        'class' => 'btn btn-primary',
                        'title' => $this->t('View task'),
                        'target' => '_blank'
                    ),
                    'label' => '<i class="icon-ok"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/view', array('id' => $data->id));
                    }
                )
            )
        )
    )
));
