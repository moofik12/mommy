<?php

use MommyCom\Controller\Backend\CallcenterOrderController;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this CallcenterOrderController
 * @var $provider CActiveDataProvider
 * @var $countActive integer
 */

$this->pageTitle = $this->t('Unconfirmed');
?>

<?php
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Orders'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]);

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'order-filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo CHtml::dropDownList('eventStatus', '', [
    'all' => $this->t('All flash-sales'),
    'passed' => $this->t('Flash-sale has expired'),
]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {return false;}'],
    ['class' => 'invisible']);
?>
<span id="info-active" class="pull-right"><?= $this->t('Processing now') ?>:
        <span class="badge <?= $countActive > 0 ? 'badge-important' : '' ?>"><?= $countActive ?></span>
    <?= Yii::t('app', 'order|orders|orders', $countActive) ?>
    </span>
<?php
$this->endWidget();

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableHistory' => false,
    'enableSorting' => false,
    'type' => 'striped bordered hover',
    'dataProvider' => $provider,
    'selectableRows' => 0,
    'ajaxUpdate' => 'info-active',
    'filterSelector' => '{filter}, form[id=order-filter-form] input, form[id=order-filter-form] select',
    'columns' => [
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'name' => 'event',
            'header' => $this->t('Order'),
            'url' => $this->createUrl('callcenterOrder/view'),
            'afterAjaxUpdate' => 'js:function(id, data) {
                    jQuery("[data-toggle=popover]").popover();
                }',
            'value' => function ($data) {
                $text = $this->t('Order') . ' №' . $data->id;
                $text .= ' ' . $this->t('From') . ':' . $this->app()->format->formatDatetime($data->ordered_at);
                $text .= ' ' . $this->t('to whom') . ':' . CHtml::encode($data->client_name) . ' ' . CHtml::encode($data->client_surname);

                return $text;
            },
        ],
        [
            'name' => 'processing_status',
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data OrderRecord */
                $text = '';
                $cssClass = CHtml::value($this->labelCss, $data->processing_status);
                $statusText = $data->processingStatusReplacement;

                if ($data->isNeedCall()) {
                    $statusText .= '<br>' . $this->t('Call back') . ': ' . $this->app()->format->formatDatetime($data->next_called_at);
                }

                $text .= ' <span class="label ' . $cssClass . '">'
                    . $statusText . '</span>';

                return $text;
            },
        ],
        [
            'name' => 'user.email',
            'header' => $this->t('User'),
        ],
        [
            'name' => 'updated_at',
            'header' => $this->t('Last order processing'),
            'value' => function ($data) {
                /** @var $data OrderRecord */
                return $this->app()->getDateFormatter()->formatDateTime($data->updated_at, 'long');
            },
        ],
        [
            'type' => 'raw',
            'name' => 'processing',
            'header' => false,
            'value' => function ($data) {
                $text = $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'link',
                    'type' => 'danger',
                    'label' => $this->t('Edit'),
                    'htmlOptions' => [
                        'class' => 'pull-right',
                    ],
                    'url' => $this->createUrl('callcenterOrder/processing', ['id' => $data->id]),
                ], true);

                return $text;
            },
            'headerHtmlOptions' => [
                'style' => 'width: 5%;',
            ],
        ],
    ],
]);

$this->endWidget();
?>
<?php
$path = $this->app()->assetManager->publish(
    Yii::getPathOfAlias('assets.markup')
);
/** @var CClientScript $cs */
$this->app()->clientScript->registerCssFile($path . '/css/callcenter.css');
?>
