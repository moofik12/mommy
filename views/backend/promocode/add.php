<?php

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
$this->pageTitle = $this->t('Promocodes');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Add promo code'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
