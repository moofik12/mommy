<?php

use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Widget\Backend\ButtonColumn;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('Promocodes');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Promocodes'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->renderPartial('_filter') ?>

<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['promocode/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>

<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Promocodes {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'promocode'],
        ['name' => 'type', 'filter' => PromocodeRecord::typeReplacements(), 'value' => function ($data) {
            /* @var $data PromocodeRecord */
            return $data->typeReplacement;
        }],
        ['name' => 'reason', 'type' => 'raw', 'filter' => PromocodeRecord::reasonReplacements(), 'value' => function ($data) {
            /* @var $data PromocodeRecord */
            if ($data->reason_description !== '') {
                return CHtml::tag('abbr', ['title' => $data->reason_description], $data->reasonReplacement);
            } else {
                return $data->reasonReplacement;
            }
        }],
        ['name' => 'user.fullname'],
        ['name' => 'user_id', 'header' => 'ID Пользователя'],
        ['name' => 'percent'],
        ['name' => 'cash'],
        ['name' => 'order_price_after'],
        ['name' => 'partner_id', 'header' => $this->t('Partner ID')],
        ['name' => 'admin.fullname'],
        ['name' => 'is_deleted', 'filter' => [false => $this->t('No'), true => $this->t('Yes')], 'type' => 'boolean'],
        ['name' => 'valid_until', 'type' => 'datetime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],

        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['promocode/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
