<?php
/* @var $this \MommyCom\Service\BaseController\BackController */

/* @var $form TbForm */
$this->pageTitle = $this->t('Promocodes');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Current Information'),
    'headerIcon' => 'icon-add',
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        'promocode',
        'typeReplacement',
        'user.fullname',
        'user.email',
    ],
]); ?>
<?php $this->endWidget() ?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Edit promo code'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
