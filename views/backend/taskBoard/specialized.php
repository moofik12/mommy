<?php

use MommyCom\Controller\Backend\TaskBoardController;

/**
 * @var $this TaskBoardController
 * @var $provider CActiveDataProvider
 * @var string $title
 */
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $title,
    'headerIcon' => 'icon-edit',
]);
?>
    <div style="display:inline">
        <?=
        CHtml::link($this->t('Add task'), $this->createUrl('addManualTask'), [
            'class' => 'btn btn-primary',
            'target' => '_blank',
        ]) ?>
    </div>
<?php
$this->renderPartial('partial/_self_tasks', ['key' => $key]);
$this->renderPartial('partial/_index', ['provider' => $provider]);
$this->endWidget();
