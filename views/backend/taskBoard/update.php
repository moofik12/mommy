<?php

use MommyCom\Controller\Backend\TaskBoardController;

/* @var $this TaskBoardController */
/* @var $form TbForm */

$this->pageTitle = 'Редактировать задачу #' . $model->id;

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, array(
    'title' => $this->t('Edit task') . ' #' .  $model->id,
    'headerIcon' => 'icon-add'
));

echo $form->render();
$this->endWidget();
