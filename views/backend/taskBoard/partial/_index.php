<?php

use MommyCom\Controller\Backend\TaskBoardController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Model\Db\TaskBoardRecord;

/**
 * @var $this TaskBoardController
 * @var $provider CActiveDataProvider
 * @var $data TaskBoardRecord
 */

$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = '';
        $app = $this->app();
        if (($app->user->id === $data->owner_id || $app->user->id === $data->user_id) && (int)$data->answer_counter !== 0) {
            $cssClass = 'success';
        } elseif ($app->user->id === $data->owner_id) {
            $cssClass = 'warning';
        } elseif ($app->user->id === $data->user_id) {
            $cssClass = 'info';
        }

        return $cssClass;
    },
    'columns' => [
        [
            'name' => 'id',
            'type' => 'raw',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'name' => 'user_id',
            'type' => 'raw',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
            'value' => function ($data) {
                if ((int)$this->app()->user->id === (int)$data->user_id) {
                    return '<i class="icon-share-alt"></i>' . $this->t('For you');
                } elseif ((int)$data->user_id !== 0) {
                    return '<i class="icon-share-alt"></i> ' . $data->user->fullname;
                }
                return $this->t('No user defined. The task is available for all.');
            },
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'name' => 'question_type',
            'filter' => TaskBoardRecord::qTypes(),
            'value' => function ($data) {
                return (int)$data->question_type !== 0 ? TaskBoardRecord::getQuestionTypes($data->question_type) : $this->t('No type');
            },
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'type' => 'raw',
            'name' => 'question_description',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'type' => 'raw',
            'name' => 'answer_counter',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
            'value' => function ($data) {
                if ((int)$data->answer_counter !== 0) {
                    /* @var $data OrderTrackingRecord */
                    return CHtml::link(Yii::t('application', '{n} answer|{n} answers', $data->answer_counter), 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'left',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('partial/modal/_answers', ['id' => $data->id], true),
                    ]);
                } else {
                    return $this->t('No responses');
                }
            },
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {archive} {renew} {update} {delete} {toBrand} {toCallCenter} {toCoordination} {toStorage}',
            'buttons' => [
                'archive' => [
                    'visible' => function ($url, $data) {
                        return (int)$data->is_archive === 0 && ((int)$this->app()->user->id === (int)$data->owner_id || (int)$data->user_id === 0);
                    },
                    'options' => [
                        'class' => 'btn btn-success',
                        'title' => $this->t('Archive task'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-ok"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/markArchived', ['id' => $data->id]);
                    },
                ],
                'renew' => [
                    'visible' => function ($url, $data) {
                        return (int)$data->is_archive === 1;
                    },

                    'options' => [
                        'class' => 'btn btn-success',
                        'title' => $this->t('Return task'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-refresh"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/refresh', ['id' => $data->id]);
                    },
                ],
                'update' => [
                    'visible' => function ($url, $data) {
                        return (int)$data->is_archive === 0 && (int)$data->owner_id === (int)$this->app()->user->id;
                    },
                    'options' => [
                        'class' => 'btn btn-warning',
                        'title' => $this->t('Send to warehouse board'),
                        'target' => '_blank',
                    ],
                    'label' => '<i class="icon-edit"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/update', ['id' => $data->id]);
                    },
                ],
                'toStorage' => [
                    'visible' => function ($url, $data) {
                        return ((int)$data->task_board_type !== (int)TaskBoardRecord::TYPE_STORAGE && (int)$data->is_archive === 0);
                    },
                    'options' => [
                        'class' => 'btn btn-info',
                        'title' => $this->t('Send to warehouse board'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-fullscreen"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/toStorage', ['id' => $data->id]);
                    },
                ],
                'toBrand' => [
                    'visible' => function ($url, $data) {
                        return ((int)$data->task_board_type !== (int)TaskBoardRecord::TYPE_BRAND_MANAGER && (int)$data->is_archive === 0);
                    },
                    'options' => [
                        'class' => 'btn btn-info',
                        'title' => $this->t('Send to brand manager board'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-user"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/toBrand', ['id' => $data->id]);
                    },
                ],
                'toCallCenter' => [
                    'visible' => function ($url, $data) {
                        return ((int)$data->task_board_type !== (int)TaskBoardRecord::TYPE_CALL_CENTER && (int)$data->is_archive === 0);
                    },
                    'options' => [
                        'class' => 'btn btn-info',
                        'title' => $this->t('Send to call-center board'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-bullhorn"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/toCallCenter', ['id' => $data->id]);
                    },
                ],
                'toCoordination' => [
                    'visible' => function ($url, $data) {
                        return ((int)$data->task_board_type !== (int)TaskBoardRecord::TYPE_COORDINATION && (int)$data->is_archive === 0);
                    },
                    'options' => [
                        'class' => 'btn btn-info',
                        'title' => $this->t('Send to the coordination board'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-share-alt"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/toCoordination', ['id' => $data->id]);
                    },
                ],
                'delete' => [
                    'visible' => function ($url, $data) {
                        return (int)$this->app()->user->id === (int)$data->owner_id && (int)$data->is_archive === 0;
                    },
                    'options' => [
                        'class' => 'btn btn-danger',
                        'title' => $this->t('Send to the coordination board'),
                        'target' => '',
                    ],
                    'label' => '<i class="icon-fullscreen"></i>',
                    'url' => function ($data) {
                        return $this->app()->createUrl('taskBoard/delete', ['id' => $data->id]);
                    },
                ],
            ],
        ],
    ],
]);

echo "<span class='modal-helper'>";
