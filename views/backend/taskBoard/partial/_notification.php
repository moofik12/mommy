<?php
$app = $this->app();
$cs = $app->clientScript;
$action = $this->createUrl('taskBoard/notification');
$js = <<<JS
        var show = true;

        var notification = function() {
            $.ajax({
                url: "$action"
            })
            .success(function(data, textStatus) {
                $('#growls').remove();
                if ((typeof data.info !== 'undefined') && data.info[0] !== "") {
                    $.growl.notice({title: "Ответы",message: data.info[0]});
                }

                if ((typeof data.warnings !== 'undefined') && data.warnings[0] !== "") {
                    $.growl.warning({title: "Ожидают ответа",message: data.warnings[0]});
                }

                if ((typeof data.errors !== 'undefined') && data.errors[0] !== "") {
                    $.growl({title: "Вопросы",message: data.errors[0] , style: 'info'});
                }
            }).error(function() {
                $('#growls').remove();
            })
        };

        setInterval(function(){
            if (show) {
                notification();
            }
        }, 20000);


JS;
$cs->registerScript('taskNotification', $js);
