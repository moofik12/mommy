<?php
$this->beginWidget('bootstrap.widgets.TbModal', [
    'htmlOptions' => [
        'id' => 'change-board',
    ],
]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?= $this->t('Link the task to another board') ?></h4>
    </div>
<?php
echo CHtml::openTag('div', ['class' => 'modal-body change-board-body', 'style' => 'overflow-y: visible;']);

echo $form->render();
echo CHtml::closeTag('div');

$this->endWidget();