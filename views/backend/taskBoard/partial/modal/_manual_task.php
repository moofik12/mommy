<?php

use MommyCom\Model\Db\TaskBoardRecord;

$this->beginWidget('bootstrap.widgets.TbModal', array(
    'htmlOptions' => array(
        'id' => 'manual-task'
    )
)); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?= $this->t('Add a manual task from the order card') ?></h4>
    </div>
<?php
echo CHtml::openTag('div', array('class' => 'modal-body', 'style' => 'overflow-y: visible;'));

$model = new TaskBoardRecord();
$form = TbForm::createForm('widgets.form.config.taskBoard.taskAddManualFromProductCard', $this, array(
    'type' => 'horizontal'
), $model);

echo $form->render();

echo CHtml::closeTag('div');

$this->endWidget();
