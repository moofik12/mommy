<?php

use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\YiiComponent\Facade\Translator;

/**
 * @var $this TaskBoardController
 * @var $id int
 * @var $model TaskBoardRecord
 */

$model = TaskBoardRecord::loadModel($id);
?>

<table class="items table table-striped table-bordered table-hover">
    <th style="text-align: center"><?php $this->t('Time') ?></th>
    <th style="text-align: center"><?php $this->t('User') ?></th>
    <th style="text-align: center"><?php $this->t('Answer') ?></th>
    <?php foreach ($model->answers as $k => $v): ?>
    <tr>
        <td>
            <?= date('d-m-y H:m:s',$v->created_at); ?>
        </td>
        <td>
            <?= $v->user->fullname; ?>
        </td>
        <td>
            <?= $v->answer; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
