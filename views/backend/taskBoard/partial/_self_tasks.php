<div class="checkbox" style="display:inline-block">
    <label>
        <?php
        $userId = $this->app()->user->id;
        $check = false;
        if (isset($_SESSION[$key][$userId])) {
            $check = true;
        }
        echo CHtml::checkbox('self-tasks', $check, ['submit' => ['taskBoard/selfTasks', ['id' => $userId]], 'params' => [$this->app()->request->csrfTokenName => $this->app()->request->csrfToken, 'key' => $key], 'style' => 'float:left;margin-left:18px;margin-right: 9px']) . $this->t('Enable only my tasks'); ?>
    </label>
</div>
