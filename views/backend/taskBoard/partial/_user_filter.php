<?php
/**
 * @var array $users
 * @var string $sessionKey
 * @var string $filterAdd
 * @var string $filterRemove
 */
?>
<div class="dropdown" style="display: inline">
    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="true">
        <?= $this->t('Enable other user\'s tasks') ?>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="width:400px">
        <?php
        $style = 'float:left;margin-left:18px;margin-right: 9px';
        foreach ($users as $key => $value) {
            $check = false;
            $submit = array($filterAdd, array('id' => $this->app()->user->id, 'key' => $sessionKey));
            if (isset($_SESSION[$sessionKey][$key])) {
                $check = true;
                $submit = array($filterRemove, array('id' => $this->app()->user->id, 'key' => $sessionKey));
            }
            echo '<li><span class="input-group-addon get-users-task" style="">' . CHtml::checkbox('adding-user', $check, array('submit' => $submit, 'params' => array($this->app()->request->csrfTokenName => $this->app()->request->csrfToken, 'adding-user' => $key), 'value' => $key, 'style' => $style)) . "<div>{$value}</div></span></li>";
        }
        ?>
    </ul>
</div>
