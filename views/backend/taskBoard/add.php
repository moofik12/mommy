<?php

use MommyCom\Controller\Backend\TaskBoardController;

/* @var $this TaskBoardController */
/* @var $form TbForm */
/* @var $title string */
$this->pageTitle = $title;

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, array(
    'title' => $title,
    'headerIcon' => 'icon-add'
));

echo $form->render();
$this->endWidget();

