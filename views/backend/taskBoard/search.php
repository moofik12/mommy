<?php

use MommyCom\Controller\Backend\TaskBoardController;

/**
 * @var $this TaskBoardController
 * @var $provider CActiveDataProvider
 * @var array $users
 * @var string $sessionKey
 * @var string $title
 * @var string $filterAdd
 * @var string $filterRemove
 */

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, array(
    'title' => $title,
    'headerIcon' => 'icon-edit'
));


echo $form->render();
if ($provider !== array()) {
    $this->renderPartial('partial/_index', array('provider' => $provider));
}
$this->endWidget();
