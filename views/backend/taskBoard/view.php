<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\TaskBoardRecord;

/**
 * @var $model TaskBoardRecord
 */
?>
<h4 style="text-align:center; vertical-align: inherit;"> <?= $this->t('Task information') ?> </h4>
<?php
$question_type = (int)$model->question_type !== 0 ? TaskBoardRecord::getQuestionTypes($model->question_type) : $this->t('No type');
$answer_count = (int)$model->answer_counter !== 0 ? Yii::t('application', '{n} answer|{n} answers', $model->answer_counter) : $this->t('No responses');
$archive = [$this->t('No'), $this->t('Yes')];
$link = (int)$model->order_id !== 0 ? CHtml::link($model->order_id, $this->app()->createUrl('callcenterOrder/processing', ['id' => $model->order_id]), ['target' => '_blank']) : $this->t('Manual task');
$user = (int)$model->user_id !== 0 ? $model->user->fullname : $this->t('No user defined');
$ownerId = (int)$model->owner_id === (int)$this->app()->user->id ? $this->t('You') : $model->owner->fullname;

$products = $model->products;
$data = '';
if ($products !== null && $products !== [] && $products !== "") {
    $products = explode(',', $products);
    $count = count($products) - 1;
    foreach ($products as $p) {
        $pModel = OrderProductRecord::model()->findByAttributes(['product_id' => $p])->product;
        $data .= CHtml::link($p, 'javascript:void(0)', [
            'data-toggle' => 'popover',
            'data-placement' => 'right',
            'data-trigger' => 'hover',
            'data-html' => 'true',
            'data-content' => $this->renderPartial('//callcenterOrder/_itemCartProductPopover', ['eventProduct' => $pModel], true),
        ]);
        if ($products[$count] !== $p) {
            $data .= ' , ';
        }
    }
} else {
    $data = $this->t('No products.');
}

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model->attributes,
    'attributes' => [
        [
            'name' => 'id',
            'label' => '№',
        ],
        [
            'name' => 'question_type',
            'label' => $this->t('Question type'),
            'value' => $question_type,
        ],
        [
            'name' => 'owner_id',
            'label' => $this->t('Created by'),
            'value' => $ownerId,
        ],
        [
            'name' => 'user_id',
            'label' => $this->t('Сonsignee'),
            'value' => $user,
        ],
        [
            'name' => 'task_board_type',
            'label' => $this->t('Board Type'),
            'value' => TaskBoardRecord::getQuestionTypes($model->task_board_type),
        ],
        [
            'name' => 'question_description',
            'label' => $this->t('Question description'),
            'value' => $model->question_description,
            'type' => 'raw',
        ],
        [
            'name' => 'response',
            'label' => $this->t('Number of answers'),
            'value' => $answer_count,
            'type' => 'raw',
        ],
        [
            'name' => 'is_archive',
            'label' => $this->t('Archive'),
            'value' => $archive[(int)$model->is_archive],
        ],
        [
            'name' => 'order_id',
            'label' => '№ Заказа',
            'value' => $link,
            'type' => 'raw',
        ],
        [
            'name' => 'products',
            'label' => $this->t('No. of product (s)'),
            'type' => 'raw',
            'value' => $data,
        ],
    ],
]);
?>

<h4 style="text-align:center; vertical-align: inherit;"> <?= $this->t('Answers') ?> </h4>
<div style="margin-left: 190px">
    <h4><strong><?= $this->t('Question') ?></strong> : <?= $model->question_description ?> </h4>
    <br>
    <?php foreach ($model->answers as $k => $v): ?>
        <p>
            <strong>
                <time style="padding-right: 10px"><?= date('d-m-y H:m:s', $v->created_at); ?></time>
            </strong>
            <span style="padding-right: 10px">
            <strong>
                <?= $v->user->fullname; ?> :
            </strong>
        </span>
            <span>
            <?= $v->answer; ?>
        </span>
        </p>
        <br>
    <?php endforeach; ?>
</div>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'headerIcon' => 'icon-add',
]);

echo $form->render();
$this->endWidget();

?>
