<?php

use MommyCom\Controller\Backend\TaskBoardController;

/**
 * @var $this TaskBoardController
 * @var $provider CActiveDataProvider
 * @var array $users
 * @var string $sessionKey
 * @var string $title
 * @var string $filterAdd
 * @var string $filterRemove
 */
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $title,
    'headerIcon' => 'icon-edit',
]);

?>
    <div style="display:inline">
        <?=
        CHtml::link($this->t('Add task'), $this->createUrl('addManualTask'), [
            'class' => 'btn btn-primary',
            'target' => '_blank',
        ]) ?>
    </div>
<?php
$this->renderPartial('partial/_user_filter', ['sessionKey' => $sessionKey, 'users' => $users, 'filterAdd' => $filterAdd, 'filterRemove' => $filterRemove]);
$this->renderPartial('partial/_index', ['provider' => $provider]);
$this->endWidget();
