<?php

use MommyCom\Model\Db\DistributionPushRecord;

$this->pageTitle = $this->t('View mail-out');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('PUSH-mail-outs'),
    'headerIcon' => 'icon-edit',
]);

$this->widget('bootstrap.widgets.TbDetailView', [
    'type' => ['striped', 'bordered', 'condensed'],
    'htmlOptions' => ['class' => 'table-compact'],
    'data' => $model,
    'attributes' => [
        'id',
        [
            'name' => 'status',
            'value' => function ($data) {
                /** @var $data DistributionPushRecord */
                return $data->statusReplacement();
            },
        ],
        'subject',
        'body',
        'url',
        'created_at:datetime',
        'start_at:datetime',
    ],
]);

$this->endWidget();
