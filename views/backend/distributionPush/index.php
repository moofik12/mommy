<?php

use MommyCom\Controller\Backend\DistributionPushController;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\ApplicationSubscriberRecord;
use MommyCom\Model\Db\DistributionPushRecord;

/**
 * @var $this DistributionPushController
 * @var $form Form
 * @var $model DistributionPushRecord
 * @var $statisticTypes array key as ApplicationSubscriberRecord::type, value as amount
 */
$this->pageTitle = $this->t('PUSH-mail-outs');
$model = $provider->model;

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('PUSH-mail-outs'),
    'headerIcon' => 'icon-edit',
]);
?>

<?php if ($statisticTypes): ?>
    <div class="well pull-right extended-summary">
        <h4><?= $this->t('Subscriber statistics') ?>: </h4>
        <?php foreach ($statisticTypes as $type => $amount): ?>
            <div><strong><?= ApplicationSubscriberRecord::typeReplacement($type) ?>
                    :</strong> <?= $this->t('{n} subscriber|{n} subscribers|{n} subscribers', $amount); ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => 'link',
    'url' => $this->createUrl('distributionPush/create'),
    'label' => $this->t('Create mail-out'),
    'type' => 'primary',
]);
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'enableSorting' => true,
    'fixedHeader' => false,
    'dataProvider' => $provider,
    'filter' => $model,
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        ['name' => 'status', 'header' => $this->t('Status'), 'htmlOptions' => ['class' => 'span3'],
            'filter' => $model->statusReplacements(), 'value' => function ($data) {
            /** @var $data DistributionPushRecord */
            return $data->statusReplacement();
        },
        ],
        ['name' => 'start_at', 'filter' => false, 'htmlOptions' => ['class' => 'span2']
            , 'value' => function ($data) {
            /** @var $data DistributionPushRecord */
            return $this->app()->dateFormatter->formatDateTime($data->start_at);
        },
        ],
        [
            'name' => 'subject',
            'htmlOptions' => ['class' => 'span4'],
        ],
        [
            'name' => 'body',
            'htmlOptions' => ['class' => 'span4'],
        ],
        [
            'name' => 'url',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var DistributionPushRecord $data */
                return "<span data-toggle=\"tooltip\" data-placement=\"top\" title=\"{$data->url}\">" . $this->app()->format->formatTextPreview($data->url, 20) . "</span>";
            },
            'htmlOptions' => ['class' => 'span5'],
        ],
        [
            'name' => 'url_desktop',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var DistributionPushRecord $data */
                return "<span data-toggle=\"tooltip\" data-placement=\"top\" title=\"{$data->url_desktop}\">" . $this->app()->format->formatTextPreview($data->url_desktop, 20) . "</span>";
            },
            'htmlOptions' => ['class' => 'span5'],
        ],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span2']],
        [
            'class' => ButtonColumn::class,
            'header' => $this->t('Actions'),
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'delete' => [
                    'url' => '\Yii::app()->controller->createUrl("delete",array("id"=>$data->primaryKey))',
                    'label' => $this->t('delete'),
                    'visible' => function ($row, $data) {
                        /** @var DistributionPushRecord $data */
                        $result = false;
                        if ($data->status == DistributionPushRecord::STATUS_PENDING) {
                            $result = true;
                        }
                        return $result;
                    },
                ],
                'update' => [
                    'icon' => 'pencil',
                    'label' => $this->t('edit'),
                    'url' => '$this->app()->controller->createUrl("update",array("id"=>$data->primaryKey))',
                    'visible' => function ($row, $data) {
                        /** @var DistributionPushRecord $data */
                        $result = false;
                        if ($data->status == DistributionPushRecord::STATUS_PENDING) {
                            $result = true;
                        }

                        return $result;
                    },
                ],
                'view' => [
                    'icon' => 'eye-open',
                    'label' => $this->t('View'),
                    'url' => '$this->app()->controller->createUrl("view",array("id"=>$data->primaryKey))',
                ],
            ],
        ],
    ],
]);

$this->endWidget();
