<?php

use MommyCom\Controller\Backend\DistributionPushController;
use MommyCom\YiiComponent\Backend\Form\Form;
use MommyCom\Model\Db\DistributionPushRecord;

/**
 * @var $this DistributionPushController
 * @var $form Form
 * @var $model DistributionPushRecord
 */
$this->pageTitle = $this->t('PUSH-Mailouts: edit');
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Mail-out'),
    'headerIcon' => 'icon-edit',
]);
?>

<?= $form->render() ?>
<?php $this->endWidget(); ?>
