<?php

use MommyCom\Model\Db\SupplierOrderReturnRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
/* @var $model SupplierOrderReturnRecord */
$this->pageTitle = $this->t('Update supplier return request');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Update of request') . ' №' . $model->id,
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>

<?php $this->endWidget() ?>
