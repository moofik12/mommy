<?php

use MommyCom\Model\Db\SupplierOrderReturnRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model SupplierOrderReturnRecord */
$this->pageTitle = $this->t('View supplier return request');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Details of Request') . ' №' . CHtml::encode($model->id),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'order_id',
        [
            'name' => 'supplier_id',
            'value' => function ($data) {
                /* @var $data SupplierOrderReturnRecord */
                return $data->supplier ? $data->supplier->name : $this->t('not found');
            },
        ],
        'is_created_supplier:boolean',
        'statusText',
        'message',
        'message_callcenter',
        [
            'name' => 'updated_at',
            'type' => 'datetime',
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
        ],
    ],
]); ?>
<?php $this->endWidget() ?>
