<?php
/* @var $this BackController */

use MommyCom\Service\BaseController\BackController;

/* @var $form TbForm */
$this->pageTitle = $this->t('Сreate suppliers return request');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Add request'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order data'),
]); ?>
<?php $form = $this->beginWidget('CActiveForm', [
    'htmlOptions' => [
        'class' => 'form-horizontal',
        'onsubmit' => 'return false;',
    ],
]); ?>
<div class="control-group ">
    <label class="control-label" for="find_order_id"><?= $this->t('Order №') ?></label>
    <div class="controls">
        <input maxlength="32" class="span4" name="id" type="text">
        <?php
        $this->widget('bootstrap.widgets.TbButton', [
            'id' => 'get-order-info',
            'buttonType' => TbButton::BUTTON_AJAXBUTTON,
            'type' => TbButton::TYPE_INFO,
            'label' => $this->t('Receive'),
            'url' => $this->createUrl('orderInfo'),
            'ajaxOptions' => [
                'cache' => false,
                'type' => 'GET',
                'data' => 'js:jQuery(this).parents("form").serialize()',
                'beforeSend' => 'js:function() {
                        jQuery("#order-answer").html("Получение данных...");
                    }',
                'success' => 'js:function(html){
                        jQuery("#order-answer").html(html);
                    }',
                'error' => 'js:function(xhr) {
                        var text = "Error! " + xhr.responseText;
                        jQuery("#order-answer").html("<div class=\"alert alert-danger\">" + text + "</div>");
                    }',
            ],
        ]);
        ?>
    </div>
</div>
<?php $this->endWidget(); ?>
<div id="order-answer">

</div>


<?php $this->endWidget() ?>
