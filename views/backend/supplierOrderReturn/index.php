<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierOrderReturnRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('View the list of requests');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View the list of suppliers returns requests'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<div class="row">
    <label>
        <div><?= $provider->model->getAttributeLabel('supplier_id') ?></div>
        <?php
        $this->widget('bootstrap.widgets.TbSelect2', [
            'name' => 'supplier',
            'asDropDownList' => false,
            'value' => $this->app()->request->getParam('supplier', ''),
            'options' => [
                'width' => '250px;',
                'placeholder' => $this->t('Enter title'),
                'allowClear' => true,
                'initSelection' => new CJavaScriptExpression('function (element, callback) {
                            $.ajax({
                                url: "' . $this->app()->createUrl('supplier/ajaxGetById') . '",
                                method: "GET",
                                data: {
                                    id: parseInt(element.val())
                                },
                                success: function(supplier) {
                                    if (!supplier) {
                                        return;
                                    }
                                    var data = {
                                        id: supplier.id,
                                        text: supplier.name
                                    };
                                    callback(data);
                                }
                            });
                        }'),
                'ajax' => [
                    'url' => $this->app()->createUrl('supplier/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                                    return {
                                        name: term,
                                        page: page
                                    };
                                }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };
                
                                $.each(response.items, function(index, value) {
                                    result.results.push({
                                        id: value.id,
                                        text: value.name,
                                    });
                                });
                
                                return result;
                            }'),
                ],
            ],

        ]);
        ?>
    </label>
    <label>
        <div><?= $provider->model->getAttributeLabel('created_at') ?></div>
        <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
            'name' => 'createdAtRange',
            'value' => $this->app()->request->getParam('createdAtRange'),
            'htmlOptions' => [
                'placeholder' => $provider->model->getAttributeLabel('created_at'),
            ],
        ]) ?>
    </label>
</div>
<div class="row">
    <?= CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-success']) ?>
</div>
<?php $this->endWidget() ?>

<div class="pull-right" style="margin-top: 10px;">
    <?= CHtml::link($this->t('Add request'), ['create'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Flash-sales {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form select',
    'columns' => [
        [
            'name' => 'id',
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        'order_id',
        [
            'name' => 'supplier_id',
            'filter' => false,
            'value' => function ($data) {
                /* @var $data SupplierOrderReturnRecord */
                return $data->supplier ? $data->supplier->name : '---';
            },
        ],
        [
            'name' => 'is_created_supplier',
            'type' => 'boolean',
            'filter' => $this->app()->format->booleanFormat,
        ],
        [
            'name' => 'status',
            'filter' => SupplierOrderReturnRecord::statuses(),
            'value' => function ($data) {
                /* @var $data SupplierOrderReturnRecord */
                return $data->statusText;
            },
        ],
        'message',
        'message_callcenter',
        [
            'name' => 'updated_at',
            'type' => 'datetime',
            'filter' => false,
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {view} {delete}',
            'buttons' => [
                'update' => [
                    'url' => function ($data) {
                        return $this->createUrl('update', ['id' => $data->id]);
                    },
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierOrderReturnRecord */
                        return $data->isPossibleCallcenterUpdate();
                    },
                ],
                'view' => [
                    'icon' => 'eye-open',
                    'url' => function ($data) {
                        return $this->createUrl('view', ['id' => $data->id]);
                    },
                ],
                'delete' => [
                    'url' => function ($data) {
                        return $this->createUrl('delete', ['id' => $data->id]);
                    },
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierOrderReturnRecord */
                        return $data->isPossibleCallcenterDelete();
                    },
                ],
            ],
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
