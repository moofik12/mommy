<?php

use MommyCom\YiiComponent\Backend\SupplierOrderReturnControllerTrackingInfo;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var OrderRecord $model
 * @var SupplierOrderReturnControllerTrackingInfo $trackcodeInfo
 */

$cf = $this->app()->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$df = $dateFormatter = $this->app()->dateFormatter;

?>

<div class="row-fluid">
    <div class="span12">
        <div class="span6">
            <h4><?= $this->t('Order data') ?></h4>
            <?php $this->widget('bootstrap.widgets.TbDetailView', [
                'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                'data' => $model,
                'attributes' => [
                    [
                        'name' => 'ordered_at',
                        'value' => $this->app()->format->formatDatetime($model->ordered_at),
                    ],
                    [
                        'type' => 'boolean',
                        'name' => 'is_drop_shipping',
                        'cssClass' => $model->is_drop_shipping ? 'info' : '',
                    ],
                    [
                        'name' => 'supplier.displayName',
                        'cssClass' => $model->is_drop_shipping ? 'info' : '',
                    ],
                    [
                        'name' => 'card_payed',
                        'type' => 'raw',
                        'value' => $model->card_payed > 0
                            ? '<span class="text-success">' . $cf->format($model->card_payed, $cn->getName('short')) . '</span>'
                            : $cf->format($model->card_payed, $cn->getName('short')),
                    ],
                    'called_count',
                    'client_name',
                    'client_surname',
                    'telephone',
                    [
                        'type' => 'raw',
                        'name' => 'delivery_type',
                        'value' => CHtml::value($model->deliveryTypeReplacements(), $model->delivery_type),
                    ],
                    [
                        'name' => 'trackcode',
                    ],
                    [
                        'name' => 'order_comment',
                        'type' => 'ntext',
                        'value' => $model->order_comment,
                    ],
                    [
                        'name' => 'callcenter_comment',
                        'type' => 'ntext',
                        'value' => $model->callcenter_comment,
                    ],
                    [
                        'name' => 'storekeeper_comment',
                        'type' => 'ntext',
                        'value' => $model->storekeeper_comment,
                    ],
                    [
                        'name' => 'supplier_comment',
                        'type' => 'ntext',
                        'value' => $model->supplier_comment,
                    ],
                ],
            ]);
            ?>
        </div>
        <div class="span6">
            <h4><?= $this->t('Tracking data') ?></h4>
            <?php $this->widget('bootstrap.widgets.TbDetailView', [
                'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                'data' => $trackcodeInfo,
                'attributes' => [
                    [
                        'label' => $this->t('Status of tracking data receipt'),
                        'name' => 'fetchStatusText',
                    ],
                    [
                        'label' => $this->t('Tracking number'),
                        'name' => 'number',
                    ],
                    [
                        'label' => $this->t('Tracking status'),
                        'name' => 'statusText',
                    ],
                    [
                        'label' => $this->t('Date of tracking creation'),
                        'name' => 'dateCreated',
                    ],
                    [
                        'label' => $this->t('Return delivery total'),
                        'name' => 'redeliverySum',
                    ],
                    [
                        'label' => $this->t('Sender\'s name'),
                        'name' => 'senderFullName',
                    ],
                    [
                        'label' => $this->t('Sender\'s city'),
                        'name' => 'senderCity',
                    ],
                    [
                        'label' => $this->t('Sender\'s address'),
                        'name' => 'senderAddress',
                    ],
                    [
                        'label' => $this->t('Sender\'s phone number'),
                        'name' => 'senderPhone',
                    ],
                    [
                        'label' => $this->t('Сonsignee\'s name'),
                        'name' => 'recipientFullName',
                    ],
                    [
                        'label' => $this->t('Сonsignee\'s city'),
                        'name' => 'recipientCity',
                    ],
                    [
                        'label' => $this->t('Сonsignee\'s address'),
                        'name' => 'recipientAddress',
                    ],
                    [
                        'label' => $this->t('Сonsignees phone number'),
                        'name' => 'recipientPhone',
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
