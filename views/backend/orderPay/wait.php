<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Pending payment');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Pending payment'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    //'enableHistory'=> false,
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Orders {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filterForm :input',
    'id' => 'orders',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span2']],
        ['name' => 'delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderRecord */
            return $data->deliveryTypeReplacement;
        }],
        ['name' => 'client_name'],
        ['name' => 'client_surname'],
        ['name' => 'price_total'],
        ['name' => 'payPrice', 'header' => $this->t('To be paid'), 'filter' => false],
        ['name' => 'ordered_at', 'type' => 'datetime', 'filter' => false],
        [
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{open}',
            'htmlOptions' => [
                'class' => 'span2',
            ],
            'buttons' => [
                'open' => [
                    'label' => '<i class="icon icon-check"></i> ' . $this->t('Confirm payment'),
                    'options' => [
                        'class' => 'btn btn-link',
                    ],
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createUrl('orderPay/createPaid', ['id' => $data->id]);
                    },
                ],
            ],
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
