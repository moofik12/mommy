<?php

use MommyCom\Controller\Backend\OrderPayController;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this OrderPayController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Order refunds');
$request = $this->app()->request;

$ajaxCsrf = '';
if ($this->app()->request->enableCsrfValidation) {
    $csrfTokenName = $this->app()->request->csrfTokenName;
    $csrfToken = $this->app()->request->csrfToken;
    $ajaxCsrf = "\n\t\tdata:{ '$csrfTokenName':'$csrfToken' },";
}

?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Payments list'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'bank-refund',
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
        ],
        [
            'type' => 'raw',
            'name' => 'order_id',
            'header' => $this->t('Order ID'),
            'value' => function ($data) {
                /** @var OrderBankRefundRecord $data */
                $text = $data->order_id;

                if ($data->order) {
                    $text .= '<span class="label label-info">'
                        . CHtml::encode($data->order->processingStatusReplacement) . '</span>';
                }
                return $text;
            },
        ],
        [
            'type' => 'raw',
            'name' => 'order_user_id',
            'value' => function ($data) {
                /* @var OrderBankRefundRecord $data */
                $text = $data->order_user_id;
                if ($data->orderUser) {
                    $text .= '<br>' . $data->orderUser->email;
                }
                $text .= '<br>' . CHtml::link($this->t('bonus account'),
                        $this->app()->createUrl('bonuspoints/userDetail', ['id' => $data->order_user_id]), ['target' => '_blank']);

                return $text;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
//        'price_order',
//        'discount',
        'order_card_payed',
        'price_refund',
        [
            'name' => 'type_refund',
            'filter' => OrderBankRefundRecord::typeRefundReplacements(),
            'value' => function ($item) {
                /** @var OrderBankRefundRecord $item */
                return $item->typeRefundReplacement();
            },
        ],
        [
            'name' => 'is_refunded',
            'type' => 'boolean',
            'filter' => [$this->t('no'), $this->t('yes')],
        ],
        [
            'name' => 'is_custom',
            'type' => 'boolean',
            'filter' => [$this->t('no'), $this->t('yes')],
        ],
        [
            'name' => 'status',
            'filter' => OrderBankRefundRecord::statusReplacements(),
            'value' => '$data->statusReplacement',
        ],
        [
            'class' => ButtonColumn::class,
            'header' => $this->t('Actions'),
            'template' => '{confirm} {refresh} {delete}',
            'openNewWindow' => false,
            'deleteButtonLabel' => $this->t('Cancel'),
            'deleteConfirmation' =>  $this->t('Cancel refund?'),
            'buttons' => [
                'confirm' => [
                    'label' =>  $this->t('Conduct'),
                    'icon' => 'icon-ok',
                    'url' => '\Yii::app()->controller->createUrl("bankRefundNeedPay",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-success',
                    ],
                    'visible' => function ($row, $data) {
                        /** @var OrderBankRefundRecord $data */
                        return $data->status == OrderBankRefundRecord::STATUS_CONFIGURED;
                    },
                ],
                'refresh' => [
                    'label' =>  $this->t('Reset changes'),
                    'icon' => 'icon-repeat',
                    'url' => '$this->app()->controller->createUrl("bankRefundRefresh",array("id"=>$data->primaryKey, "status" => ' .
                        OrderBankRefundRecord::STATUS_CONFIGURED . '))',
                    'options' => [
                        'class' => 'btn-warning',
                    ],
                    'visible' => function ($row, $data) {
                        /** @var OrderBankRefundRecord $data */
                        return $data->isAvailableEdit() && $data->status == OrderBankRefundRecord::STATUS_NEED_PAY;
                    },
                    'click' => new CJavaScriptExpression("function(e) {
                        e.preventDefault();

                        $.ajax({
                            type: 'POST',
                            url: jQuery(this).attr('href'),
                            $ajaxCsrf
                            success: function(data) {
                                jQuery('#bank-refund').yiiGridView('update');
                            },
                            error: function(XHR) {
                                alert(XHR.responseText);
                            },
                        });

                        return false;
                    }"),
                ],
                'delete' => [
                    'url' => '$this->app()->controller->createUrl("bankRefundDelete",array("id"=>$data->primaryKey))',
                    'visible' => function ($row, $data) {
                        /** @var OrderBankRefundRecord $data */
                        return $data->status == OrderBankRefundRecord::STATUS_CONFIGURED
                            || $data->isAvailableEdit() && $data->status == OrderBankRefundRecord::STATUS_NEED_PAY;
                    },
                ],
            ],
        ],
        [
            'name' => 'payment_after_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var OrderReturnRecord $data */
                $text =  $this->t('no');
                if ($data->payment_after_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->payment_after_at);
                }

                return $text;
            },
        ],
        [
            'name' => 'created_at',
            'filter' => false,
            'type' => 'datetime',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
