<?php

use MommyCom\Controller\Backend\OrderPayController;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Model\PayGateway\PayGatewayReceiveResult;

/**
 * @var $this OrderPayController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle =  $this->t('Logging order payments');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' =>  $this->t('Payments list'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
        ],
        'provider',
        'invoice_id',
        'unique_payment',
        'amount',
        [
            'name' => 'is_custom',
            'type' => 'boolean',
            'filter' => [ $this->t('No'),  $this->t('Yes')],
        ],
        [
            'name' => 'status',
            'filter' => PayGatewayReceiveResult::statusReplacements(),
            'value' => function ($data) {
                /* @var $data PayGatewayRecord */
                return CHtml::value(PayGatewayReceiveResult::statusReplacements(), $data->status);
            },
        ],
        'message',
//        'user_id',
        'user_ip',
        'user_browser',
        [
            'name' => 'updated_at',
            'filter' => false,
            'type' => 'datetime',
        ],
        [
            'name' => 'created_at',
            'filter' => false,
            'type' => 'datetime',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
