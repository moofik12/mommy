<?php

use MommyCom\Controller\Backend\OrderPayController;

/**
 * @var $this OrderPayController
 * @var $form TbForm
 */

$this->pageTitle =  $this->t('Create payment');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' =>  $this->t('New payment'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
