<?php

use MommyCom\Controller\Backend\OrderPayController;

/**
 * @var $this OrderPayController
 * @var $form TbForm
 */

$this->pageTitle = $this->t('Confirmation refund fulfilment');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Confirmation refund fulfilment'),
    'headerIcon' => 'icon-th-list',
]); ?>

<div class="alert alert-info">
    <h4> <?= $this->t('Money refunded AUTOMATICALLY in case of refund to MOMMY bonus account') ?> </h4>
</div>

<?= $form->render() ?>

<?php $this->endWidget() ?>
