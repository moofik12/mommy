<?php

use MommyCom\Controller\Backend\OrderPayController;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Model\PayGateway\PayGatewayReceiveResult;

/**
 * @var $this OrderPayController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Order payments');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' =>  $this->t('Payments list'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<div class="pull-right">
    <?= CHtml::link( $this->t('Add Payment'), ['createPaid'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'rowCssClassExpression' => function ($row, $data) {
        /* @var PayGatewayRecord $data */
        if ($data->isErrorPayStatus()) {
            return 'error';
        }

        return '';
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
        ],
        [
            'name' => 'provider',
            'value' => function ($data) {
                /* @var PayGatewayRecord $data */
                return $data->providerText;
            },
        ],
        'provider',
        'invoice_id',
        [
            'name' => 'unique_payment',
            'htmlOptions' => [
                'class' => 'span1',
            ],
        ],
        'amount',
        [
            'name' => 'is_custom',
            'type' => 'boolean',
            'filter' => [ $this->t('No'),  $this->t('Yes')],
        ],
        [
            'name' => 'status',
            'filter' => PayGatewayReceiveResult::statusReplacements(),
            'value' => function ($data) {
                /* @var $data PayGatewayRecord */
                return CHtml::value(PayGatewayReceiveResult::statusReplacements(), $data->status);
            },
        ],
        [
            'name' => 'pay_status',
            'filter' => PayGatewayRecord::getPayStatusesReplacement(),
            'value' => '$data->payStatusText',
        ],
        [
            'name' => 'order_bank_refund_id',
            'value' => function ($data) {
                /* @var PayGatewayRecord $data */

                return $data->order_bank_refund_id > 0 ? $data->order_bank_refund_id : '';
            },
        ],
        'message',
        [
            'name' => 'admin_id',
            'header' =>  $this->t('Operator (ID)'),
            'value' => function ($data) {
                /* @var PayGatewayRecord $data */
                return $data->admin ? "{$data->admin->login} ({$data->admin->fullname})" :  $this->t('no');
            },
        ],
        [
            'name' => 'created_at',
            'filter' => false,
            'type' => 'datetime',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
