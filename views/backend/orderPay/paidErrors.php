<?php

use MommyCom\Controller\Backend\OrderPayController;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Model\PayGateway\PayGatewayReceiveResult;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this OrderPayController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Problematic order payments');
$csrf = '';
$gridId = 'pay_gateway_errors';

if ($this->app()->request->enableCsrfValidation) {
    $csrfTokenName = $this->app()->request->csrfTokenName;
    $csrfToken = $this->app()->request->csrfToken;
    $csrf = "\n\t\tdata:{ '$csrfTokenName':'$csrfToken' },";
}
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Problem with payment list'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'id' => $gridId,
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'rowCssClassExpression' => function ($row, $data) {
        /* @var PayGatewayRecord $data */
        if ($data->isErrorPayStatus()) {
            return 'error';
        }

        return '';
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
        ],
        [
            'name' => 'provider',
            'value' => function ($data) {
                /* @var PayGatewayRecord $data */
                return $data->providerText;
            },
        ],
        [
            'type' => 'raw',
            'name' => 'invoice_id',
            'value' => function ($data) {
                /** @var PayGatewayRecord $data */
                $text = $data->invoice_id > 0 ? $data->invoice_id : '';

                if ($data->order) {
                    $text .= '<br>';
                    $text .= '<span class="label label-info" style="white-space: normal">'
                        . CHtml::encode($data->order->processingStatusReplacement) . '</span>';

                    if ($data->order->trackcode) {
                        $text .= '<br>';
                        if ($data->order->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA) {
                            $text .= CHtml::link("TTH: {$data->order->trackcode}",
                                "https://novaposhta.ua/tracking?cargo_number={$data->order->trackcode}", ['target' => '_blank']);
                        } else {
                            $text .= "TTH: {$data->order->trackcode}";
                        }
                    }
                }
                return $text;
            },
        ],
        'unique_payment',
        'amount',
        [
            'name' => 'is_custom',
            'type' => 'boolean',
            'filter' => [$this->t('No'), $this->t('Yes')],
        ],
        [
            'name' => 'status',
            'filter' => PayGatewayReceiveResult::statusReplacements(),
            'value' => function ($data) {
                /* @var $data PayGatewayRecord */
                return CHtml::value(PayGatewayReceiveResult::statusReplacements(), $data->status);
            },
        ],
        [
            'name' => 'pay_status',
            'filter' => PayGatewayRecord::getPayStatusesReplacement(),
            'value' => '$data->payStatusText',
        ],
        'message',
        [
            'name' => 'admin_id',
            'header' => $this->t('Operator (ID)'),
            'value' => function ($data) {
                /* @var PayGatewayRecord $data */
                return $data->admin ? "{$data->admin->login} ({$data->admin->fullname})" : 'no';
            },
        ],
        [
            'name' => 'created_at',
            'filter' => false,
            'type' => 'datetime',
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{return} {canceledOrderPay}',
            'deleteConfirmation' => $this->t('Payment will be marked as erroneous'),
            'buttons' => [
                'return' => [
                    'label' => $this->t('Create a refund'),
                    'icon' => 'icon-repeat',
                    'url' => '\Yii::app()->controller->createUrl("paidCreateReturn",array("id"=>$data->primaryKey))',
                    'options' => [
                        'class' => 'btn-warning',
                    ],
                    'click' => "function() {
                        if(!confirm('" . $this->t('Refund will be generated') . "')) {
                            return false;
                        }

                        jQuery('#$gridId').yiiGridView('update', {
                            type: 'POST',
                            url: jQuery(this).attr('href'),$csrf
                            success: function(data) {
                                jQuery('#$gridId').yiiGridView('update');
                            }
                        });

                        return false;
                    }",
                ],
                'canceledOrderPay' => [
                    'label' => $this->t('There was a cancellation of payment upon receipt of the order'),
                    'icon' => 'icon-briefcase',
                    'url' => function ($data) {
                        /* @var $data PayGatewayRecord */
                        return $this->app()->controller->createUrl('paidCanceled', ['id' => $data->id, 'status' => PayGatewayRecord::PAY_STATUS_CANCELED_ORDER_PAY]);
                    },
                    'options' => [
                        'class' => 'btn-success',
                    ],
                    'click' => "function() {
                        if(!confirm('" .
                        $this->t('The transaction will be marked as settled since the order was placed not using payment-on-delivery option') .
                        "')) {
                            return false;
                        }

                        jQuery('#$gridId').yiiGridView('update', {
                            type: 'POST',
                            url: jQuery(this).attr('href'),$csrf
                            success: function(data) {
                                jQuery('#$gridId').yiiGridView('update');
                            }
                        });

                        return false;
                    }",
                ],
            ],
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
