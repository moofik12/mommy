<?php

use MommyCom\Controller\Backend\OrderPayController;
use MommyCom\Model\Db\PayFromSettlementAccountRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;

/**
 * @var $this OrderPayController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Order payments from settlement account');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List of payments made to the settlement account'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'rowCssClassExpression' => function ($row, $data) {
        $cssClass = '';
        /* @var PayFromSettlementAccountRecord $data */
        if ($data->isWrong()) {
            $cssClass = 'error';
        } elseif ($data->isPaid()) {
            $cssClass = 'success';
        }

        return $cssClass;
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
        ],
        [
            'name' => 'provider',
            'filter' => PayGatewayRecord::getProviders(true),
            'value' => function ($data) {
                /* @var PayFromSettlementAccountRecord $data */
                return $data->providerText;
            },
        ],
        'unique_payment',
        [
            'type' => 'raw',
            'name' => 'possible_order_id',
            'value' => function ($data) {
                /** @var PayFromSettlementAccountRecord $data */
                $text = $data->possible_order_id > 0 ? $data->possible_order_id : '';

                if ($data->possibleOrder) {
                    $text .= '<br>';
                    $text .= '<span class="label label-info" style="white-space: normal">'
                        . CHtml::encode($data->possibleOrder->processingStatusReplacement) . '</span>';

                    if ($data->possibleOrder->trackcode) {
                        $text .= '<br>';
                        if ($data->possibleOrder->delivery_type == DeliveryTypeUa::DELIVERY_TYPE_NOVAPOSTA) {
                            $text .= CHtml::link("Tracking number: {$data->possibleOrder->trackcode}",
                                "https://novaposhta.ua/tracking?cargo_number={$data->possibleOrder->trackcode}", ['target' => '_blank']);
                        } else {
                            $text .= "Tracking number: {$data->possibleOrder->trackcode}";
                        }
                    }
                }
                return $text;
            },
        ],
        'amount',
        'order_id',
        'pay_comment',
        [
            'name' => 'status',
            'filter' => PayFromSettlementAccountRecord::getStatusesReplacement(),
            'value' => function ($data) {
                /* @var $data PayFromSettlementAccountRecord */
                return $data->statusText;
            },
        ],
        [
            'name' => 'pay_gateway_id',
            'value' => function ($data) {
                /* @var PayFromSettlementAccountRecord $data */
                if ($data->payGateway) {
                    return "{$data->payGateway->id} (order №{$data->payGateway->invoice_id})";
                } elseif ($data->pay_gateway_id > 0) {
                    return $data->pay_gateway_id;
                }

                return '';
            },
        ],
        [
            'name' => 'operation_at',
            'filter' => false,
            'type' => 'datetime',
        ],
        [
            'name' => 'created_at',
            'filter' => false,
            'type' => 'datetime',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
