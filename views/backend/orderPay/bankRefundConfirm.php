<?php

use MommyCom\Controller\Backend\OrderPayController;
use MommyCom\Model\Db\OrderBankRefundRecord;

/**
 * @var $this OrderPayController
 * @var $form TbForm
 */

$this->pageTitle = $this->t('Confirmation of refund');
/* @var OrderBankRefundRecord $model */
$model = $form->model;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Confirmation of the refund type by order') . ' №' . $model->order_id,
    'headerIcon' => 'icon-th-list',
]); ?>

<div class="alert alert-info">
    <h4> <?= $this->t('Money refunded AUTOMATICALLY in case of refund to MOMMY bonus account') ?> </h4>
</div>

<?= $form->render() ?>

<?php $this->endWidget() ?>
