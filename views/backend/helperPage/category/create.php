<?php

use MommyCom\Controller\Backend\HelperPageController;

/**
 * @var $this HelperPageController
 * @var $form TbForm
 */

$this->pageTitle = $this->t('Managing help categories');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Create category'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
