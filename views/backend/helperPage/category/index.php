<?php

use MommyCom\Controller\Backend\HelperPageController;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this HelperPageController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Managing help categories');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List of categories'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>
<div>
    <?= CHtml::link($this->t('Add'), ['helperPage/categoryCreate'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>

<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        'name',
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
            'openNewWindow' => false,
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("categoryUpdate",array("id"=>$data->primaryKey))',
            'deleteButtonUrl' => '$this->app()->controller->createUrl("categoryDelete",array("id"=>$data->primaryKey))',
            'deleteConfirmation' =>
                $this->t('Attention, deleting a category will delete the pages that this category contains! Continue?'),
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
