<?php

use MommyCom\Controller\Backend\HelperPageController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\HelperPageCategoryRecord;
use MommyCom\Model\Db\HelperPageRecord;
use MommyCom\YiiComponent\ArrayUtils;

/**
 * @var $this HelperPageController
 * @var $provider CActiveDataProvider
 * @var $categories HelperPageCategoryRecord
 */

$this->pageTitle = $this->t('Managing Help Menu');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('A list of pages'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>
<div>
    <?= CHtml::link($this->t('Add'), ['helperPage/pageCreate'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,

    //sortable
    'sortableRows' => true,
    'sortableAttribute' => 'position',
    'sortableAction' => 'helperPage/positionPage',
    'sortableAjaxSave' => true,

    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        'title',
        [
            'name' => 'category_id',
            'filter' => ArrayUtils::getColumn($categories, 'name', 'id'),
            'value' => function ($data) {
                /** @var $data HelperPageRecord */
                $result = $data->category_id;
                $category = $data->category;

                if ($category) {
                    $result = $category->name;
                }

                return $result;
            },
        ],
        [
            'name' => 'updated_at',
            'type' => 'humanTime',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'created_at',
            'type' => 'humanTime',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
            'openNewWindow' => false,
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("pageUpdate",array("id"=>$data->primaryKey))',
            'deleteButtonUrl' => '$this->app()->controller->createUrl("pageDelete",array("id"=>$data->primaryKey))',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
