<?php

use MommyCom\Controller\Backend\StaticPageController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\StaticPageRecord;

/**
 * @var $this StaticPageController
 * @var $data StaticPageRecord
 */
$grid = $this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'static-page-grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => $this->t('Page categories {start} - {end} out of {count}'),
//    'filterSelector' => '{filter}, form input',
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        'name',
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
        ],
    ],
]); ?>
