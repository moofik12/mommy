<?php

use MommyCom\Controller\Backend\StaticPageController;

/* @var $this StaticPageController */

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Categories'),
    'headerIcon' => 'icon-edit',
]);
?>

<?php $this->renderPartial('_index', [
    'dataProvider' => $dataProvider,
]); ?>

<?php $this->endWidget(); ?>


<?php
//верстка не расчитана более 2-х категорий
/*
$box=$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, array(
    'title' => $this->t('New category'),
    'headerIcon' => 'icon-edit'
));

echo $form->render();

$this->endWidget();
*/
?>
