<?php
/* @var $form TbForm */
$this->breadcrumbs = CMap::mergeArray($this->breadcrumbs, [$this->t('Update')]);
?>

<?php
$title = CHtml::encode($form->getModel()->name);
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Category update') . ' ' . $title,
    'headerIcon' => 'icon-edit',
]); ?>

<?= $form->render(); ?>

<?php $this->endWidget(); ?>
