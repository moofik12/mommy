<?php

use MommyCom\Controller\Backend\StaticPageController;

/* @var $this StaticPageController */

$this->breadcrumbs = CMap::mergeArray($this->breadcrumbs, [$this->t('Create')]);
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Create'),
    'headerIcon' => 'icon-edit',
]); ?>

<?= $form->render(); ?>

<?php $this->endWidget(); ?>
