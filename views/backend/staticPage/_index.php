<?php

use MommyCom\Controller\Backend\StaticPageController;
use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this StaticPageController
 * @var $data StaticPageRecord
 */

$grid = $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'static-page-grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,

    //sortable
    'sortableRows' => true,
    'sortableAttribute' => 'position',
    'sortableAction' => 'staticPage/position',
    'sortableAjaxSave' => true,

    'summaryText' => $this->t('Pages {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form input',
    'columns' => [
        [
            'name' => 'position',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        [
            'name' => 'category_id',
            'type' => 'raw',
            'filter' => CHtml::listData(StaticPageCategoryRecord::model()->findAll(), 'id', 'name'),
            'value' => function ($data) {
                return CHtml::link(
                    CHtml::encode($data->category->name),
                    $this->app()->createUrl('staticPageCategory/update', ['id' => $data->category->primaryKey]),
                    ['target' => '_blank']
                );
            },
//            'headerHtmlOptions' => array('class' => 'span3'),
        ],
        'title',
        'url',
        [
            'class' => 'bootstrap.widgets.TbToggleColumn',
            'toggleAction' => 'toggle',
            'name' => 'is_visible',
            'filter' => [$this->t('No'), $this->t('Yes')],
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'filter' => false,
            'name' => 'created_at',
            'type' => 'humanTime',
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'filter' => false,
            'name' => 'updated_at',
            'type' => 'humanTime',
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
        ],
    ],
]); ?>
