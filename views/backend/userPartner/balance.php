<?php

use MommyCom\Model\Db\UserPartnerBalanceRecord;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $model UserPartnerRecord
 * @var $dataProvider CActiveDataProvider
 */

$this->pageTitle = $this->t('Confirmed partner balance');
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Confirmed balance'),
    'headerIcon' => 'icon-list',
]);
?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        [
            'name' => 'id',
        ],
        [
            'name' => 'user_id',
            'header' => $this->t('User'),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */
                $text = 'ID ' . $data->user_id;
                if ($data->user !== null) {
                    $text .= ', ' . $data->user->email;
                }
                return $text;
            },
        ],
        [
            'name' => 'source',
        ],
        [
            'name' => 'partner_percent',
        ],
        [
            'name' => 'is_sent_unisender',
            'type' => 'boolean',
        ],
        [
            'name' => 'source', 'filter' => false, 'label' => $this->t('Sum on the balance'), 'type' => 'html',
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */
                $balance = $data->getBalance();
                if ($balance > 0) {
                    $codeColor = '#0ca447';
                } else {
                    $codeColor = '#ff7a8c';
                }
                return '<span style="color: ' . $codeColor . '; font-weight:bold;">' . $balance . '</span>';
            },
        ],
        [
            'name' => 'source', 'filter' => false, 'label' => $this->t('Paid to Mommy'),
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */

                return $data->getSumOutput('mamam');
            },
        ],
        [
            'name' => 'source', 'filter' => false, 'label' => $this->t('Declined amount'),
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */

                return $data->getSumCancel();
            },
        ],
        [
            'name' => 'source', 'filter' => false, 'label' => $this->t('Refunds amount'),
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */

                return $data->getSumReturn();
            },
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false, 'label' => $this->t('Linked to the program'),],
    ],
]); ?>
<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'grid-view',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Confirmed balance {start} - {end} out of {count}',
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n",
    'rowCssClassExpression' => function ($row, $data) {
        /** @var UserPartnerBalanceRecord $data */
        $cssClass = '';
        if (in_array($data->type, [UserPartnerBalanceRecord::TYPE_PRIVAT_MINUS, UserPartnerBalanceRecord::TYPE_MAMAM_MINUS, UserPartnerBalanceRecord::TYPE_RETURN_MINUS])) {
            $cssClass = '';
        } elseif (in_array($data->type, [UserPartnerBalanceRecord::TYPE_ORDER_PLUS, UserPartnerBalanceRecord::TYPE_CANCEL_PLUS])) {
            $cssClass = 'success';
        }
        return $cssClass;
    },
    'columns' => [
        [
            'name' => 'id',
        ],
        [
            'name' => 'type',
            'filter' => UserPartnerBalanceRecord::typeReplacements(),
            'value' => function ($data) {
                /* @var $data UserPartnerBalanceRecord */
                $cssClass = '';
                if ($data->type == UserPartnerBalanceRecord::TYPE_ORDER_PLUS) {
                    $cssClass = 'label-warning';
                } elseif ($data->type == UserPartnerBalanceRecord::TYPE_CANCEL_PLUS) {
                    $cssClass = 'label-primary';
                } elseif ($data->type == UserPartnerBalanceRecord::TYPE_MAMAM_MINUS) {
                    $cssClass = 'label-info';
                } elseif ($data->type == UserPartnerBalanceRecord::TYPE_PRIVAT_MINUS) {
                    $cssClass = 'label-success';
                }
                $text = ' <span class="label ' . $cssClass . '">' . $data->typeReplacement . '</span>';
                return $text;
            },
            'type' => 'raw',
        ],
        [
            'name' => 'amount',
        ],
        [
            'name' => 'description',
        ],
        [
            'name' => 'order_id',
        ],
        ['name' => 'created_at', 'type' => 'datetime'],
    ],
]);
?>

<?php $this->endWidget(); ?>
