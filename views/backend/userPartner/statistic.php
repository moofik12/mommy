<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Statistic\UserPartnerOrderModel;

/**
 * @var $this UserPartnerController
 * @var $rangeData string
 * @var $dataProviderReport CArrayDataProvider
 */

$this->pageTitle = $this->t('Order statistics');
$partnerId = $this->app()->request->getParam('partner_id', 0);

$partner = '';
if ($partnerId > 0) {
    $partnerModel = UserPartnerRecord::model()->findByPk($partnerId);
    if ($partnerModel !== null) {
        $partner = "<strong>" . $partnerModel->id . ", " . $partnerModel->user->email . "</strong>";
        if ($partnerModel->user->name != '' || $partnerModel->user->surname != '') {
            $partner .= " <span style=\"color: graytext;\"> (" . $partnerModel->user->name . " " . $partnerModel->user->surname . ")</span>";
        }
    }
}

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order statistics'),
    'headerIcon' => 'icon-th-list',
]);
?>
<?php //filter
/** @var TbActiveForm $filterForm */
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'GET',
]); ?>
<?php
echo '<label for="partner_id">' . $this->t('Partner') . '</label>';
$this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'partner_id',
    'asDropDownList' => false,
    'val' => $partnerId,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Partner ID or e-mail'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('userPartner/ajaxSearchByIdOrEmail'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    graytext = "";
                                    if (value.name != "" || value.surname != "") {
                                        graytext = " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>"
                                    }
                                
                                    result.results.push({
                                        id: value.id,
                                        text: "<strong>" + value.id + ", " + value.email + "</strong>" + graytext,
                                    });
                                });

                                return result;
                            }'),
        ],
        'initSelection' => new CJavaScriptExpression("function(element, callback) {
            var id = $(element).val();

            if(parseInt(id)) {
                callback(
                    {id: id, text: '$partner'}
                );
            }
        }"),
    ],

]);
?>

<?php
echo '<label for="rangeData">' . $this->t('Period') . '</label>';
$this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'rangeData',
    'value' => $rangeData,
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period')],
]);
echo '<br>' . '<br>';
echo CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-primary']);

$this->endWidget(); ?>

<?php //grid data
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProviderReport,
    'template' => "{items}\n{pager}\n{extendedSummary}",
    'sortableAttribute' => 'startTime',
    'columns' => [
        [
            'name' => 'startTime',
            'header' => $this->t('Date'),
            'type' => 'date',
        ],
        /*array(
            'name' => 'endTime',
            'header' => 'Date',
            'type' => 'datetime',
        ),*/
        [
            'name' => 'countRegPartnerInvitedUser',
            'header' => $this->t('Registrations'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->countRegPartnerInvitedUser;
            },
        ],
        [
            'name' => 'countUsedPromocode',
            'header' => $this->t('Promocodes used'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->countUsedPromocode;
            },
        ],
        [
            'name' => 'countOrder',
            'header' => $this->t('Qty'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->countOrder;
            },
        ],
        [
            'name' => 'countProduct',
            'header' => $this->t('Quantity of products'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->countProduct;
            },
        ],
        [
            'name' => 'sumOrder',
            'header' => $this->t('Total'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->sumOrder;
            },
        ],
        [
            'name' => 'countOutput',
            'header' => $this->t('Quantity of forecasted payments'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->countOutput;
            },
        ],
        [
            'name' => 'sumOutput',
            'header' => $this->t('Forecasted amount'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->sumOutput;
            },
        ],
        [
            'name' => 'countConfirmedOutput',
            'header' => $this->t('Quantity of confirmed payments'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->countConfirmedOutput;
            },
        ],
        [
            'name' => 'sumConfirmedOutput',
            'header' => $this->t('Confirmed amount'),
            'value' => function ($data) {
                /** @var $data UserPartnerOrderModel */
                return $data->sumConfirmedOutput;
            },
        ],
        [
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{viewInvitedUser} {orderUsers}',
            'htmlOptions' => [
                'class' => 'span2',
            ],
            'buttons' => [
                'viewInvitedUser' => [
                    'icon' => 'user',
                    'url' => function ($data) use ($partnerId) {
                        /** @var $data UserPartnerOrderModel */
                        $params = [];
                        if ($partnerId > 0) {
                            $params['partner_id'] = $partnerId;
                        }
                        $params['rangeDate'] = implode(' - ', [date('d.m.Y', $data->startTime), date('d.m.Y', $data->endTime)]);
                        return $this->createUrl('userPartner/invited', $params);
                    },
                    'label' => $this->t('Registrations'),
                    'options' => [
                        'class' => 'btn btn-primary',
                        'style' => 'margin-bottom: 5px;',
                    ],
                ],
                'orderUsers' => [
                    'icon' => 'shopping-cart',
                    'url' => function ($data) use ($partnerId) {
                        /** @var $data UserPartnerOrderModel */
                        $params = [];
                        if ($partnerId > 0) {
                            $params['partner_id'] = $partnerId;
                        }
                        $params['rangeDate'] = implode(' - ', [date('d.m.Y', $data->startTime), date('d.m.Y', $data->endTime)]);
                        return $this->createUrl('userPartner/orderUsers', $params);
                    },
                    'label' => $this->t('Orders'),
                    'options' => [
                        'class' => 'btn btn-success',
                        'style' => 'margin-bottom: 5px;',
                    ],
                ],
            ],
        ],
    ],
    'extendedSummary' => [
        'title' => $this->t('Total'),
        'columns' => [
            'countRegPartnerInvitedUser' => ['label' => $this->t('Number of registrations'), 'class' => 'TbSumOperation'],
            'countOrder' => ['label' => $this->t('Qty'), 'class' => 'TbSumOperation'],
            'countProduct' => ['label' => $this->t('Quantity of products'), 'class' => 'TbSumOperation'],
            'sumOrder' => ['label' => $this->t('Total'), 'class' => 'TbSumOperation'],
            'countOutput' => ['label' => $this->t('Quantity of forecasted payments'), 'class' => 'TbSumOperation'],
            'sumOutput' => ['label' => $this->t('Amount of forecasted payments'), 'class' => 'TbSumOperation'],
            'countConfirmedOutput' => ['label' => $this->t('Quantity of confirmed payments'), 'class' => 'TbSumOperation'],
            'sumConfirmedOutput' => ['label' => $this->t('Amount of confirmed payments'), 'class' => 'TbSumOperation'],
        ],
    ],
    'extendedSummaryOptions' => [
        'class' => 'well pull-right',
        'style' => 'width:300px',
    ],
]);
?>
<?php $this->endWidget(); ?>
