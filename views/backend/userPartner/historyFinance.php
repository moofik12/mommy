<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $this UserPartnerController
 * @var $dataProvider CActiveDataProvider
 * @var $reasonForm Form
 * @var $requisiteForm Form
 * @var $summary array
 * @var $rangeData string
 */

$this->pageTitle = $this->t('View your history of fund withdrawal to partner requests');
$grid = $this->renderPartial('_gridHistoryPartnerOutput', [
    'dataProvider' => $dataProvider,
    'summary' => $summary,
], true);
$filter = $this->renderPartial('_filterHistory', [
    'rangeData' => $rangeData,
    'dataProvider' => $dataProvider,
], true);
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View your history of fund withdrawal to partner requests'),
    'headerIcon' => 'icon-list',
]);
?>
<?php $this->renderPartial('_financeTabs', ['content' => $filter . $grid]) ?>
<?php $this->endWidget(); ?>
