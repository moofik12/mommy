<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Statistic\UserPartnerOrderModel;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this UserPartnerController
 * @var $dataProvider CActiveDataProvider
 * @var $rangeData string
 */

$this->pageTitle = $this->t('Partners');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Partners'),
    'headerIcon' => 'icon-list',
]);
?>
<?php
/* @var $data UserPartnerRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Partners {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form[id=filter-form] :input',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'user_id',
            'header' => $this->t('User'),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */
                $text = 'ID ' . $data->user_id;
                if ($data->user !== null) {
                    $text .= ', ' . $data->user->email;
                }
                return $text;
            },
        ],
        [
            'name' => 'source',
        ],
        [
            'name' => 'partner_percent',
        ],
        [
            'name' => 'source', 'filter' => false, 'header' => $this->t('Sum on the balance'),
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */

                return $data->getBalance();
            },
        ],
        [
            'name' => 'source', 'filter' => false, 'header' => $this->t('Paid to Mommy'),
            'value' => function ($data) {
                /* @var $data UserPartnerRecord */

                return $data->getSumOutput('mamam');
            },
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false, 'header' => $this->t('Linked to the program'),],
        [
            'class' => ButtonColumn::class,
            'template' => '{balance} {viewInvitedUser} {orderUsers}',
            'buttons' => [
                'balance' => [
                    'icon' => 'money',
                    'label' => $this->t('Balance'),
                    'options' => [
                        'class' => 'btn btn-success',
                    ],
                    'url' => function ($data) {
                        return $this->createUrl('userPartner/balance', ['id' => $data->id]);
                    },
                ],
                'viewInvitedUser' => [
                    'icon' => 'user',
                    'url' => function ($data) {
                        /** @var $data UserPartnerOrderModel */
                        $params['partner_id'] = $data->id;
                        return $this->createUrl('userPartner/invited', $params);
                    },
                    'label' => $this->t('Brought in by'),
                    'options' => [
                        'class' => 'btn btn-primary',
                    ],
                ],
                'orderUsers' => [
                    'icon' => 'shopping-cart"',
                    'url' => function ($data) {
                        /** @var $data UserPartnerOrderModel */
                        $params = [];
                        $params['partner_id'] = $data->id;
                        return $this->createUrl('userPartner/orderUsers', $params);
                    },
                    'label' => $this->t('Orders'),
                    'options' => [
                        'class' => 'btn btn-info',
                    ],
                ],
            ],
        ],
    ],
]);
?>
<?php $this->endWidget(); ?>
