<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\Model\Db\UserPartnerOutputRecord;

/**
 * @var $this UserPartnerController
 * @var $dataProviderReport CArrayDataProvider
 */

/* @var $data UserPartnerOutputRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'grid-view',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Requests {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form[id=order-filter-form] :input',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'partner_id',
            'header' => $this->t('Partner'),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                $text = 'ID ' . $data->partner_id;
                if ($data->partner !== null) {
                    if ($data->partner->user !== null) {
                        $text .= ', ' . $data->partner->user->email;
                        $text .= ',<br> ' . $this->t('Date of registration') . ': ' . date('d.m.Y H:i', $data->partner->created_at);
                        $text .= ',<br> ' . $this->t('Total amount earned') . ': ' . $data->partner->getBalance();
                        $text .= ',<br> ' . $this->t('Total amount of funds withdrawn') . ': ' . $data->partner->getSumOutput();
                    }
                }
                return $text;
            },
        ],
        [
            'name' => 'amount',
        ],
        [
            'name' => 'type_output',
            'filter' => UserPartnerOutputRecord::typeOutputReplacements(),
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                return $data->typeOutputReplacement;
            },
        ],
        [
            'name' => 'number_card',
        ],
        [
            'name' => 'surname_card',
            'header' => $this->t('Cardholder full name'),
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                return $data->surname_card . ' ' . $data->name_card;
            },
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
        [
            'type' => 'raw',
            'name' => 'number_card',
            'header' => false,
            'filter' => false,
            'value' => function ($data) {
                $text = '';
                /* @var $data UserPartnerOutputRecord */
                if ($data->type_output == UserPartnerOutputRecord::TYPE_OUTPUT_PRIVAT) {
                    $text = $this->widget('bootstrap.widgets.TbButton', [
                        'buttonType' => 'link',
                        'type' => 'success',
                        'label' => $this->t('Make payment'),
                        'htmlOptions' => [
                            'class' => 'pull-right',
                            'data-id' => $data->id,
                            'data-action' => 'make_output',
                        ],
                        'url' => '#',
                    ], true);
                } elseif ($data->type_output == UserPartnerOutputRecord::TYPE_OUTPUT_MAMAM) {
                    $text = CHtml::ajaxButton($this->t('Make payment'),
                        $this->createUrl('userPartner/makeOutputMamam', ['id' => $data->id]),
                        [
                            'success' => 'function(result) {
                                var content = "";
            
                                if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                                    $.each(result.errors, function (j, message) {
                                        content = content  + message;
                                    });
            
                                    alert(content);
                                } else {
                                    var gridView = $(\'.grid-view\');
                                    gridView.yiiGridView(\'update\');
                                }
            
                            }'],
                        ['class' => 'btn btn-success pull-right']
                    );
                }

                $text .= '<br>';
                $text .= $this->widget('bootstrap.widgets.TbButton', [
                    'buttonType' => 'link',
                    'type' => 'danger',
                    'label' => $this->t('Reject payment'),
                    'htmlOptions' => [
                        'class' => 'pull-right',
                        'data-action' => 'canceled_output',
                        'data-id' => $data->id,
                        'style' => 'margin-top: 5px;',
                    ],
                    'url' => '#',
                ], true);
                return $text;
            },
            'headerHtmlOptions' => [
                'style' => 'width: 2%;',
            ],
        ],
    ],
]);
?>
