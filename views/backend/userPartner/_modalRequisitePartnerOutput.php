<?php

use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $requisiteForm Form
 */
?>
<?php    $this->beginWidget('bootstrap.widgets.TbModal', array(
    'events' => array(
        //запрет на скрол так как в окне select2 style="position: absolute;"
        'shown' => 'js:function() {
                    $("body").css("overflow", "hidden");
        
                }',
        'hidden' => 'js:function() {
                    $("#requisite-form-reset").trigger("click");
                    $("#requisite-errors").html("");
                    $("#requisite-errors").addClass("hide");
                    $("body").css("overflow", "auto");
                }',

    ),
    'htmlOptions' => array(
        'id' => 'requisite-modal',
    ),

)); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><?= $this->t('Bank details for refund') ?></h4>
</div>
<?php
echo CHtml::openTag('div', array('class' => 'modal-body change-board-body', 'style' => 'overflow-y: visible;'));
?>
<?= $requisiteForm->renderBegin() ?>
<div style="display: none;"><?= $requisiteForm->elements['id'] ?></div>
<?= $requisiteForm['requisite'] ?>
<div class="btn-toolbar">
    <?= CHtml::ajaxSubmitButton($this->t('Send'), $this->createUrl('userPartner/makeOutputPrivat')
        , array(
            'type' => 'GET',
            'success' => 'function(result) {
                            var outErrors = $("#requisite-errors");
                            var content = "";
        
                            if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                                $.each(result.errors, function (j, message) {
                                    content = content + "<li>" + message + "</li>";
                                });
        
                                outErrors.html("<ul>" + content + "</ul>").removeClass("hide");
                            } else {
                                outErrors.html("");
                                $("#requisite-modal").modal("hide");
                                var gridView = $(\'.grid-view\');
                                gridView.yiiGridView(\'update\');
                            }
        
                        }'), array('class' => 'btn btn-primary')
    ) ?>
    <?= CHtml::resetButton($this->t('Clear'), array('class' => 'btn', 'id' => 'requisite-form-reset')) ?>
</div>
<?= $requisiteForm->renderEnd() ?>
<div id="requisite-errors" class="alert alert-block alert-error hide"></div>
<?php echo CHtml::closeTag('div');

//end modal
$this->endWidget();?>
