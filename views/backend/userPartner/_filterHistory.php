<?php

use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $rangeData string
 * @var $dataProvider CActiveDataProvider
 */

$paramName = get_class($dataProvider->model);
if (false !== strpos($paramName, '\\')) {
    $paramName = substr($paramName, strpos($paramName, '\\') + 1);
}
$getParam = $this->app()->request->getParam($paramName, []);
$partner = '';
if (isset($getParam['partner_id'])) {
    $partnerModel = UserPartnerRecord::model()->findByPk($getParam['partner_id']);
    if ($partnerModel !== null) {
        $partner = "<strong>" . $partnerModel->id . ", " . $partnerModel->user->email . "</strong>";
        if ($partnerModel->user->name != '' || $partnerModel->user->surname != '') {
            $partner .= " <span style=\"color: graytext;\"> (" . $partnerModel->user->name . " " . $partnerModel->user->surname . ")</span>";
        }
    }
}
/** @var TbActiveForm $filterForm */
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo $filterForm->select2Row($dataProvider->model, 'partner_id', [
    'asDropDownList' => false,
    'multiple' => false,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Partner ID or e-mail'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('userPartner/ajaxSearchByIdOrEmail'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    graytext = "";
                                    if (value.name != "" || value.surname != "") {
                                        graytext = " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>"
                                    }
                                
                                    result.results.push({
                                        id: value.id,
                                        text: "<strong>" + value.id + ", " + value.email + "</strong>" + graytext,
                                    });
                                });

                                return result;
                            }'),
        ],
        'initSelection' => new CJavaScriptExpression("function(element, callback) {
            var id = $(element).val();

            if(parseInt(id)) {
                callback(
                    {id: id, text: '$partner'}
                );
            }
        }"),
    ],
]);
echo '<label for="rangeData">' . $this->t('Period') . '</label>';
$this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'rangeData',
    'value' => $rangeData,
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period')],
]);
echo '<br>';
echo CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-primary']);

$this->endWidget(); ?>
