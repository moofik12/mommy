<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Statistic\UserPartnerModel;

/**
 * @var $this UserPartnerController
 * @var $dataProvider CActiveDataProvider
 * @var $rangeData string
 * @var $dataProviderReport CArrayDataProvider
 */
$this->pageTitle = $this->t('Statistics of partner registrations');

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'GET',
]);
echo $filterForm->textFieldRow($dataProvider->model, 'source');

$this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'rangeData',
    'value' => $rangeData,
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period')],
]);
echo '<br>';
echo CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-primary']);

$this->endWidget();

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Statistics of partner registrations'),
    'headerIcon' => 'icon-list',
]);
?>

<?php
/* @var $data UserPartnerRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Partners {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form[id=order-filter-form] :input',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'user_id',
            'header' => $this->t('User / Orders'),
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data UserPartnerRecord */
                $countOrders = OrderRecord::model()
                    ->userId($data->user_id)
                    ->processingStatusNotCancelled()
                    ->count();
                return $data->user_id . ', ' . $data->user->email . ' / ' . $countOrders;
            },
        ],
        [
            'name' => 'source',
        ],
        [
            'name' => 'is_sent_unisender',
            'type' => 'boolean',
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
    ],
]);
?>
<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProviderReport,
    'template' => "{items}\n{pager}\n{extendedSummary}",
    'sortableAttribute' => 'days',
    'columns' => [
        [
            'name' => 'days',
            'header' => $this->t('Date'),
            'type' => 'datetime',
        ],
        [
            'name' => 'countUser',
            'header' => $this->t('Number of applicants'),
            'value' => function ($data) {
                /** @var $data UserPartnerModel */
                return $data->countUser;
            },
        ],
    ],
    'extendedSummary' => [
        'title' => $this->t('Total'),
        'columns' => [
            'countUser' => ['label' => $this->t('Number of applicants'), 'class' => 'TbSumOperation'],
        ],
    ],
    'extendedSummaryOptions' => [
        'class' => 'well pull-right',
        'style' => 'width:300px',
    ],
]);
?>
<?php $this->endWidget(); ?>

