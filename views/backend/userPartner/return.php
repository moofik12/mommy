<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\Model\Db\UserPartnerBalanceRecord;
use MommyCom\Model\Db\UserPartnerRecord;

/**
 * @var $this UserPartnerController
 * @var $dataProvider CActiveDataProvider
 * @var $rangeDate string
 * @var $partnerId int
 */

$this->pageTitle = $this->t('Returns');
$partner = '';
if ($partnerId > 0) {
    $this->pageTitle = $this->t('Returns by partner') . ' (ID ' . $partnerId . ')';
    $partnerModel = UserPartnerRecord::model()->findByPk($partnerId);
    if ($partnerModel !== null) {
        $partner = "<strong>" . $partnerModel->id . ", " . $partnerModel->user->email . "</strong>";
        if ($partnerModel->user->name != '' || $partnerModel->user->surname != '') {
            $partner .= " <span style=\"color: graytext;\"> (" . $partnerModel->user->name . " " . $partnerModel->user->surname . ")</span>";
        }
    }
}
if ($rangeDate != '') {
    $this->pageTitle .= ' ' . $this->t('for the period') . ' ' . $rangeDate;
}

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->pageTitle,
    'headerIcon' => 'icon-list',
]);
?>
<?php //filter
/** @var TbActiveForm $filterForm */
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
    'method' => 'GET',
]); ?>
<?php
echo '<label for="partner_id">' . $this->t('Partner') . '</label>';
$this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'partner_id',
    'asDropDownList' => false,
    'val' => $partnerId,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Partner ID or e-mail'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('userPartner/ajaxSearchByIdOrEmail'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                                return {
                                    like: term,
                                    page: page
                                };
                            }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                                var result = {
                                    more: page < response.pageCount,
                                    results: []
                                };

                                $.each(response.items, function(index, value) {
                                    graytext = "";
                                    if (value.name != "" || value.surname != "") {
                                        graytext = " <span style=\"color: graytext;\"> (" + value.name + " " + value.surname + ")</span>"
                                    }
                                
                                    result.results.push({
                                        id: value.id,
                                        text: "<strong>" + value.id + ", " + value.email + "</strong>" + graytext,
                                    });
                                });

                                return result;
                            }'),
        ],
        'initSelection' => new CJavaScriptExpression("function(element, callback) {
            var id = $(element).val();

            if(parseInt(id)) {
                callback(
                    {id: id, text: '$partner'}
                );
            }
        }"),
    ],

]);
?>

<?php
echo '<label for="rangeDate">' . $this->t('Period') . '</label>';
$this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'rangeDate',
    'value' => $rangeDate,
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period')],
]);
echo '<br>' . '<br>';
echo CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-primary']);
echo ' ' . CHtml::link($this->t('Reset'), $this->createUrl('userPartner/return'), ['class' => 'btn btn-default']);

$this->endWidget(); ?>
<?php
/* @var $data UserPartnerBalanceRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Refunds {start} - {end} out of {count}',
    'filterSelector' => '{filter}',
    'columns' => [
        [
            'name' => 'partner_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data UserPartnerBalanceRecord */
                $text = 'ID ' . $data->partner_id;
                if ($data->partner !== null) {
                    if ($data->partner->user !== null) {
                        $text .= ', ' . $data->partner->user->email;
                    }
                }
                return $text;
            },
        ],
        [
            'name' => 'order_id',
        ],
        [
            'name' => 'amount',
        ],
        [
            'name' => 'description',
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
    ],
]);
?>
<?php $this->endWidget(); ?>
