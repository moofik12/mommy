<?php

use MommyCom\Controller\Backend\UserPartnerController;

/**
 * @var $this UserPartnerController
 * @var $content string
 */
?>
<?php
$this->widget(
    'bootstrap.widgets.TbTabs',
    [
        'type' => 'tabs',
        //'placement' => 'left',
        'tabs' => [
            [
                'label' => $this->t('Refund requests'),
                'content' => $this->action->id == 'finance' ? $content : '',
                'active' => $this->action->id == 'finance' ? true : false,
                'url' => $this->createUrl('userPartner/finance'),
            ],
            [
                'label' => $this->t('Refund requests history'),
                'content' => $this->action->id == 'historyFinance' ? $content : '',
                'active' => $this->action->id == 'historyFinance' ? true : false,
                'url' => $this->createUrl('userPartner/historyFinance'),
            ],
        ],
    ]
);
?>
