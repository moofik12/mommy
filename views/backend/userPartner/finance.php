<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $this UserPartnerController
 * @var $dataProviderReport CArrayDataProvider
 * @var $reasonForm Form
 * @var $requisiteForm Form
 */

$this->pageTitle = $this->t('View the list of requests for partner\'s funds withdrawal');
$grid = $this->renderPartial('_gridWaitPartnerOutput', ['dataProvider' => $dataProvider], true);
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View the list of requests for partner\'s funds withdrawal'),
    'headerIcon' => 'icon-list',
]);
?>

<?php $this->renderPartial('_financeTabs', ['content' => $grid]) ?>
<?php $this->renderPartial('_modalReasonPartnerOutput', ['reasonForm' => $reasonForm]) ?>
<?php $this->renderPartial('_modalRequisitePartnerOutput', ['requisiteForm' => $requisiteForm]) ?>
<script>
    $(function () {
        var grid = $('.grid-view');
        var $reasonForm = $('form#reason-form');
        var $requisiteForm = $('form#requisite-form');
        $(document).on('click', "a[data-action='make_output']", function (e) {
            e.preventDefault();
            $requisiteForm.find("button[type='reset']").trigger('click');
            $('#requisite-id').val($(this).attr('data-id'));
            $('#requisite-modal').modal('show');

        });
        $(document).on('click', "a[data-action='canceled_output']", function (e) {
            e.preventDefault();
            $reasonForm.find("button[type='reset']").trigger('click');
            $('#reason-id').val($(this).attr('data-id'));
            $('#reason-modal').modal('show');
        });
    });
</script>
<?php $this->endWidget(); ?>
