<?php

use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $reasonForm Form
 */
?>
<?php $this->beginWidget('bootstrap.widgets.TbModal', [
    'events' => [
        //запрет на скрол так как в окне select2 style="position: absolute;"
        'shown' => 'js:function() {
                    $("body").css("overflow", "hidden");
        
                }',
        'hidden' => 'js:function() {
                    $("#reason-form-reset").trigger("click");
                    $("#reason-errors").html("");
                    $("#reason-errors").addClass("hide");
                    $("body").css("overflow", "auto");
                }',

    ],
    'htmlOptions' => [
        'id' => 'reason-modal',
    ],

]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><?= $this->t('Reason for canceling request') ?></h4>
</div>
<?php
echo CHtml::openTag('div', ['class' => 'modal-body change-board-body', 'style' => 'overflow-y: visible;']);
?>
<?= $reasonForm->renderBegin() ?>
<div style="display: none;"><?= $reasonForm->elements['id'] ?></div>
<?= $reasonForm['reason'] ?>
<div class="btn-toolbar">
    <?= CHtml::ajaxSubmitButton($this->t('Send'), $this->createUrl('userPartner/canceledOutput')
        , [
            'type' => 'GET',
            'success' => 'function(result) {
                            var outErrors = $("#reason-errors");
                            var content = "";
        
                            if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                                $.each(result.errors, function (j, message) {
                                    content = content + "<li>" + message + "</li>";
                                });
        
                                outErrors.html("<ul>" + content + "</ul>").removeClass("hide");
                            } else {
                                outErrors.html("");
                                $("#reason-modal").modal("hide");
                                var gridView = $(\'.grid-view\');
                                gridView.yiiGridView(\'update\');
                            }
        
                        }'], ['class' => 'btn btn-primary']
    ) ?>
    <?= CHtml::resetButton($this->t('Clear'), ['class' => 'btn', 'id' => 'reason-form-reset']) ?>
</div>
<?= $reasonForm->renderEnd() ?>
<div id="reason-errors" class="alert alert-block alert-error hide"></div>
<?php echo CHtml::closeTag('div');

//end modal
$this->endWidget(); ?>
