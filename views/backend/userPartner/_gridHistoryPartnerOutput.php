<?php

use MommyCom\Controller\Backend\UserPartnerController;
use MommyCom\Model\Db\UserPartnerOutputRecord;

/**
 * @var $this UserPartnerController
 * @var $dataProviderReport CArrayDataProvider
 * @var $summary array
 */

/* @var $data UserPartnerOutputRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'grid-view',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Requests {start} - {end} out of {count}',
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'filterSelector' => '{filter}, form[id=filter-form] :input',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'partner_id',
            'header' => $this->t("Partner"),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                $text = 'ID ' . $data->partner_id;
                if ($data->partner !== null) {
                    if ($data->partner->user !== null) {
                        $text .= ', ' . $data->partner->user->email;
                        $text .= ',<br> ' . $this->t('Date of registration') . ': ' . date('d.m.Y H:i', $data->partner->created_at);
                        $text .= ',<br> ' . $this->t('Total amount earned') . ': ' . $data->partner->getBalance();
                        $text .= ',<br> ' . $this->t('Total amount of funds withdrawn') . ': ' . $data->partner->getSumOutput();
                    }
                }
                return $text;
            },
        ],
        [
            'name' => 'status',
            'filter' => UserPartnerOutputRecord::statusReplacements(),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                $cssClass = '';
                if ($data->status == UserPartnerOutputRecord::STATUS_DONE) {
                    $cssClass = 'label-success';
                } elseif ($data->status == UserPartnerOutputRecord::STATUS_CANCEL) {
                    $cssClass = 'label-warning';
                }
                $text = ' <span class="label ' . $cssClass . '">' . $data->statusReplacement . '</span>';
                return $text;
            },
        ],
        [
            'name' => 'amount',
        ],
        [
            'name' => 'type_output',
            'filter' => UserPartnerOutputRecord::typeOutputReplacements(),
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                return $data->typeOutputReplacement;
            },
        ],
        [
            'name' => 'unique_payment',
        ],
        [
            'name' => 'status_reason',
        ],
        [
            'name' => 'number_card',
        ],
        [
            'name' => 'surname_card',
            'header' => $this->t('Cardholder full name'),
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                return $data->surname_card . ' ' . $data->name_card;
            },
        ],
        [
            'name' => 'admin_id',
            'header' => $this->t('Operator'),
            'value' => function ($data) {
                /* @var $data UserPartnerOutputRecord */
                return $data->admin !== null ? $data->admin->displayName : '';
            },
        ],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false],
    ],
]);
?>
<div class="well pull-right extended-summary" style="width:300px">
    <h3><?= $this->t('Total') ?></h3>
    <?= $this->t('Amount of withdrawn funds') ?>: <?= $summary['countOutputAmount'] ?><br>
    - <?= $this->t('to Mommy') ?>: <?= $summary['countOutputAmountMamam'] ?><br>
    <br>
    <?= $this->t('Amount of blocked funds') ?>: <?= $summary['countCanceledOutputAmount'] ?><br>
</div>
