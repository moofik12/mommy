<?php

use MommyCom\Model\Db\EventRecord;

/**
 * @var $event EventRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $event,
    'attributes' => [
        'id',
        [
            'type' => 'raw',
            'name' => 'logo_fileid',
            'value' => CHtml::image($event->logo->getThumbnail('small70')->url, 'logo', ['class' => 'title']),
            'visible' => !$event->is_virtual,
        ],
        'name',
        [
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => '<span class="label label-info">'
                . ($event->supplier ? $event->supplier->name : $this->t('Not set'))
                . '</span>',
        ],
        'is_virtual:boolean',
        'is_stock:boolean',
        'startAtString',
        'endAtString',
        'mailingStartAtDateString',
        'statusReplacement',
    ],
]);
