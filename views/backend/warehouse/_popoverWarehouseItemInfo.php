<?php

use MommyCom\Model\Db\WarehouseProductRecord;

/**
 * @var $product WarehouseProductRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $product,
    'attributes' => array(
        'event_product_price',
        array(
            'name' => 'order_product_price',
            'visible' => $product->order_id > 0
        ),
        'status_comment',
        'lost_status_comment'
    ),
));
