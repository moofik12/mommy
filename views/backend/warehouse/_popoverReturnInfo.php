<?php
/**
 * @var $warehouse WarehouseRecord
 */
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $warehouse,
    'attributes' => array(
        'return_id',
        'prev_order_id',
        'prev_warehouse_id',
    ),
));