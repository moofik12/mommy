<?php

use MommyCom\Model\Db\WarehouseProductRecord;

/* @var $this CController */
/* @var $data WarehouseProductRecord */

/* @var $warehouseCreatedRange string */
/* @var $warehouseUpdatedRange string */
/* @var $createdAtFrom integer */
/* @var $createdAtTo integer */
/* @var $updatedAtFrom integer */
/* @var $updatedAtTo integer */

$selector = WarehouseProductRecord::model()
    ->eventProductId($data->event_product_id)
    ->with([
        'product' => [
            'together' => false,
        ],
        'eventProduct' => [
            'together' => false,
        ],
        'order' => [
            'together' => false,
        ],
        'user' => [
            'together' => false,
        ],
    ]);

if (!empty($warehouseCreatedRange)) {
    $selector->createdAtBetween($createdAtFrom, $createdAtTo);
}

if (!empty($warehouseUpdatedRange)) {
    $selector->updatedAtBetween($updatedAtFrom, $updatedAtTo);
}

$provider = $selector
    ->getDataProvider(false, [
        'pagination' => [
            'pageSize' => 50,
            'pageVar' => 'gridPage',
        ],
        'sort' => [
            'defaultOrder' => 't.id DESC',
        ],
        'filter' => [
            'attributes' => $this->app()->request->getParam('WarehouseProductRecord', []),
            'scenario' => 'searchPrivileged',
        ],
    ]);
?>
<div class="warehouse-item">
    <div class="warehouse-item-shortinfo">
        <?php
        $items[] = CHtml::tag('a', [
            'href' => '#',
            'class' => 'warehouse-item-expand',
        ], $this->t($this->t('Show')));

        $items[] = $data->eventProduct->id;
        $items[] = $data->product->id;

        $items[] = $data->eventProduct->article;

        if (mb_strlen($data->eventProduct->color) > 0) {
            $items[] = $data->eventProduct->color;
        }
        if (mb_strlen($data->eventProduct->size) > 0) {
            $items[] = $data->eventProduct->sizeformatReplacement . ' ' . $data->eventProduct->size;
        }

        $items[] = CHtml::link($data->product->name, 'javascript:void(0)', [
            'data-toggle' => 'popover',
            'data-placement' => 'left',
            'data-trigger' => 'hover',
            'data-html' => 'true',
            'data-content' => $this->renderPartial('_popoverProduct', ['product' => $data->eventProduct], true),
        ]);

        $items[] = CHtml::link($data->event->name, 'javascript:void(0)', [
            'data-toggle' => 'popover',
            'data-placement' => 'left',
            'data-trigger' => 'hover',
            'data-html' => 'true',
            'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
        ]);

        echo implode(', ', $items);
        ?>
    </div>
    <div class="warehouse-item-info hide">
        <div class="warehouse-item-info-images">
            <?php foreach ($data->product->getPhotogalleryImages() as $image): ?>
                <?php echo CHtml::link(
                    CHtml::image(
                        $image->getThumbnail('mid380')->url,
                        '',
                        [
                            'class' => 'img-polaroid',
                        ]
                    ),
                    $image->file->getWebFullPath(),
                    [
                        'target' => '_blank',
                    ]
                ); ?>
            <?php endforeach ?>
        </div>

        <?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
            'enableSorting' => false,
            'fixedHeader' => false,
            'dataProvider' => $provider,
            'id' => 'grid' . $data->event_product_id,

            'rowCssClassExpression' => function ($row, $data) {
                /* @var $data WarehouseProductRecord */
                if ($data->lost_status != WarehouseProductRecord::LOST_STATUS_CORRECT) {
                    return 'error';
                } elseif ($data->status == WarehouseProductRecord::STATUS_MAILED_TO_CLIENT) {
                    return 'success';
                } elseif ($data->status == WarehouseProductRecord::STATUS_UNMODERATED) {
                    return 'info';
                }

                return '';
            },

            'bulkActions' => [
                'actionButtons' => [
                    [
                        'id' => 'printLabels' . $data->event_product_id,
                        'buttonType' => 'button',
                        'type' => 'primary',
                        'size' => 'medium',
                        'label' => $this->t('Print tags'),
                        'htmlOptions' => ['data-url' => $this->createAbsoluteUrl('labelPrinter/warehouseProducts')],
                        'click' => new CJavaScriptExpression('function(values) {
                            var $this = $(this);
                            var url = $this.attr("data-url");
                            if (url.indexOf("?") == -1) {
                                url += "?";
                            }
                            $(values).each(function() {
                                url += "&" + "id[]=" + $(this).val();
                            });
                            window.open(url, "_blank");
                        }'),
                    ],
                ],
                // if grid doesn't have a checkbox column type, it will attach
                // one and this configuration will be part of it
                'checkBoxColumnConfig' => [
                    'name' => 'id',
                ],
            ],

            'columns' => [
                ['name' => 'id', 'type' => 'raw', 'value' => function ($data) {
                    /* @var $data WarehouseProductRecord */
                    return CHtml::link($data->id, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverWarehouseItemInfo',
                            ['product' => $data],
                            true
                        ),
                    ]);
                }, 'htmlOptions' => ['class' => 'span2']],
                ['name' => 'event_id', 'headerHtmlOptions' => ['class' => 'span2']],
                ['name' => 'event_product_id', 'headerHtmlOptions' => ['class' => 'span2']],
                ['name' => 'product_id', 'headerHtmlOptions' => ['class' => 'span2']],
                ['name' => 'status', 'type' => 'raw', 'filter' => WarehouseProductRecord::statusReplacements(), 'value' => function ($data) {
                    /* @var $data WarehouseProductRecord */
                    return CHtml::link($data->statusReplacement, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverWarehouseStatusHistory',
                            ['history' => $data->getStatusHistory()],
                            true
                        ),
                    ]);
                }, 'htmlOptions' => ['class' => 'span2']],
                ['name' => 'lost_status', 'type' => 'raw', 'filter' => WarehouseProductRecord::lostStatusReplacements(), 'value' => function ($data) {
                    /* @var $data WarehouseProductRecord */
                    return CHtml::link($data->lostStatusReplacement, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverWarehouseStatusHistory',
                            ['history' => $data->getLostStatusHistory()],
                            true
                        ),
                    ]);
                }, 'htmlOptions' => ['class' => 'span2']],
                ['header' => $this->t('Return'), 'type' => 'raw', 'value' => function ($data) {
                    /* @var $data WarehouseProductRecord */
                    if ($data->is_return > 0) {
                        return CHtml::link($this->t('yes'), 'javascript:void(0)', [
                            'data-toggle' => 'popover',
                            'data-placement' => 'left',
                            'data-trigger' => 'hover',
                            'data-html' => 'true',
                            'data-content' => $this->renderPartial('_popoverReturnInfo', ['warehouse' => $data], true),
                        ]);
                    }
                    return $this->t('no');
                }],
                [
                    'name' => 'created_at',
                    'type' => 'html',
                    'value' => function ($data) {
                        /* @var WarehouseProductRecord $data */
                        $text = $this->app()->dateFormatter->formatDateTime($data->created_at, 'short', 'short');
                        if ($data->requestnum > 0 && $data->request_fileid) {
                            $url = $this->app()->createUrl('warehouse/getRequestFile', ['id' => $data->id]);
                            $text = CHtml::link($text, $url, ['target' => '_blank']);
                        }

                        return $text;
                    },
                    'htmlOptions' => ['class' => 'span3'],
                ],
            ],
        ]) ?>
    </div>
</div>
