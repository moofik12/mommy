<?php

use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */

/* @var $warehouseCreatedRange string */
/* @var $warehouseUpdatedRange string */
/* @var $createdAtFrom integer */
/* @var $createdAtTo integer */
/* @var $updatedAtFrom integer */
/* @var $updatedAtTo integer */

$this->pageTitle = $this->t('Warehouse');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Products'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<style>
    .warehouse-item {
        border: 1px solid #dddddd;
        border-top: none;
        background-color: #f5f5f5;
        padding: 4px 5px;
    }

    .warehouse-item:first-child {
        border-top: 1px solid #dddddd;
    }

    .warehouse-item:nth-child(odd) {
        background-color: #f9f9f9;
    }

    .warehouse-item-shortinfo {
        font-weight: bold;
    }

    .warehouse-item-expand {
        border-bottom: dashed 1px #0088cc;
        text-decoration: none !important;
    }

    .warehouse-item-info-images {
        padding-top: 20px;
    }

    .warehouse-item-info-images img {
        max-width: 100px;
        max-height: 100px;
    }

</style>

<script>
    $(document).on('click', '.warehouse-item-expand', function (e) {
        e.preventDefault();
        var $this = $(this),
            $item = $this.closest('.warehouse-item'),
            $info = $item.children('.warehouse-item-info');
        $info.toggleClass('hide');
    })
</script>

<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<div class="row">
    <?= $filterForm->textFieldRow($provider->model, 'id', ['class' => 'span2']); ?>
    <?= $filterForm->textFieldRow($provider->model, 'event_id', ['class' => 'span1']); ?>
    <?= $filterForm->textFieldRow($provider->model, 'event_product_id', ['class' => 'span2']); ?>
    <?= $filterForm->textFieldRow($provider->model, 'product_id', ['class' => 'span2']); ?>
</div>
<div class="row" style="padding-top: 10px">
    <?= $filterForm->dropDownListRow($provider->model, 'is_return', [
        null => '',
        true => $this->t('Yes'),
        false => $this->t('No'),
    ], ['class' => 'span1']); ?>
    <?= $filterForm->textFieldRow($provider->model, 'return_id', ['class' => 'span2']); ?>
    <?= $filterForm->textFieldRow($provider->model, 'prev_warehouse_id', ['class' => 'span2']); ?>
    <div class="clearfix"></div>
</div>
<div class="row" style="padding-top: 10px">
    <?= $filterForm->dropDownListRow($provider->model, 'status', WarehouseProductRecord::statusReplacements(), ['class' => 'span1', 'prompt' => '']); ?>
    <?= $filterForm->dropDownListRow($provider->model, 'lost_status', WarehouseProductRecord::lostStatusReplacements(), ['class' => 'span1', 'prompt' => '']); ?>
</div>
<div class="row" style="padding-top: 10px">
    <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
        'name' => 'warehouseCreatedRange',
        'value' => $warehouseCreatedRange,
        'htmlOptions' => [
            'placeholder' => $provider->model->getAttributeLabel('created_at'),
        ],
    ]) ?>
    <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
        'name' => 'warehouseUpdatedRange',
        'value' => $warehouseUpdatedRange,
        'htmlOptions' => [
            'placeholder' => $provider->model->getAttributeLabel('updated_at'),
        ],
    ]) ?>
</div>
<div class="row" style="padding-top: 15px">
    <div class="pull-right">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', [
            'buttons' => [
                [
                    'type' => 'info',
                    'label' => $this->t('Download'),
                    'dropdownOptions' => [
                        'style' => 'right: 0; left: auto;',
                    ],
                    'items' => [
                        [
                            'label' => $this->t('Line-by-line'),
                            'url' => ['warehouseDownload/index'] + $_GET,
                        ],
                        '---',
                        [
                            'label' => $this->t('Product groups') . '(' .
                                $this->t('by flash-sale number ') . ')',
                            'url' => ['warehouseDownload/grouped', 'name' => 'simple'] + $_GET,
                        ],
                        [
                            'label' => $this->t('Product groups') . '(' .
                                $this->t('by product number') . ')',
                            'url' => ['warehouseDownload/grouped', 'name' => 'product'] + $_GET,
                        ],
                        [
                            'label' => $this->t('Product groups') . '(' .
                                $this->t('by item number and size') . ')',
                            'url' => ['warehouseDownload/grouped', 'name' => 'productBySize'] + $_GET,
                        ],
                    ],
                ],
            ],
        ]) ?>
    </div>
    <div>
        <?= CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php $this->endWidget() ?>

<?php $this->widget('bootstrap.widgets.TbListView', [
    'dataProvider' => $provider,
    'ajaxUpdate' => false,
    'itemView' => '_indexItem',
    'viewData' => [
        'warehouseCreatedRange' => $warehouseCreatedRange,
        'warehouseUpdatedRange' => $warehouseUpdatedRange,
        'createdAtFrom' => $createdAtFrom,
        'createdAtTo' => $createdAtTo,
        'updatedAtFrom' => $updatedAtFrom,
        'updatedAtTo' => $updatedAtTo,
    ],
    'htmlOptions' => ['style' => 'margin: 0; padding: 0;'],
]) ?>

<?php $this->endWidget() ?>
