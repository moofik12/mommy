<?php

use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Picking');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('New status'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?= $form->renderBegin() ?>
<?= $form->getActiveFormWidget()->errorSummary($form->model) ?>
<?= $form->elements['warehouseIds']->render() ?>
<?= $form->elements['status']->render() ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'enableSorting' => false,
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'headerHtmlOptions' => ['class' => 'span2'],
            'value' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return CHtml::link($data->event_id, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
                ]);
            },
        ],
        [
            'name' => 'event_product_id',
            'type' => 'raw',
            'headerHtmlOptions' => ['class' => 'span2'],
            'value' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return CHtml::link($data->event_product_id, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'left',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial('_popoverProduct', ['product' => $data->eventProduct], true),
                ]);
            },
        ],
        ['name' => 'product_id', 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'name' => 'status',
            'filter' => WarehouseProductRecord::statusReplacements(),
            'value' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return $data->statusReplacement;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'lost_status',
            'filter' => WarehouseProductRecord::lostStatusReplacements(),
            'value' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return $data->lostStatusReplacement;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'order_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data WarehouseProductRecord */
                if ($data->order_id > 0) {
                    return CHtml::link($data->order_id, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'left',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('_popoverOrder', ['order' => $data->order], true),
                    ]);
                }
                return $this->t('no');
            },
            'cssClassExpression' => function ($row, $data) {
                /* @var WarehouseProductRecord $data */
                return $data->order_id > 0 ? 'success' : '';
            },
        ],
        ['name' => 'user.fullname', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'created_at', 'type' => 'datetime', 'htmlOptions' => ['class' => 'span3']],
        [
            'class' => ButtonColumn::class,
            'template' => '{remove}',
            'buttons' => [
                'remove' => [
                    'url' => function () { // hack
                        return 'javascript:void(0)';
                    },
                    'label' => '',
                    'icon' => 'remove',
                    'options' => [
                        'class' => 'btn btn-danger',
                        'onclick' => new CJavaScriptExpression('
                                    var $tr = $(this).closest("tr");
                                    var id = $tr.find("td:first").text();
                                    $("input[name*=warehouseIds]").filter(function() {
                                        return $(this).val() == id;
                                    }).remove();
                                    $tr.remove();
                                    return false;
                                '),
                    ],
                ],
            ],
        ],
    ],
]); ?>

<div class="pull-left">
    <?= $form->elements['warehouseId']->render() ?>
</div>
<div class="pull-left">
    <?= $form->elements['addProduct']->render() ?>
</div>
<div class="clearfix"></div>

<?= $form->renderButtons() ?>
<?= $form->renderEnd() ?>
<?php $this->endWidget() ?>

