<?php

use MommyCom\Controller\Backend\EmailController;
use MommyCom\Model\Db\EmailMessage;

/**
 * @var $model EmailMessage
 * @var $this EmailController
 */
$sentAt = $model->status == EmailMessage::STATUS_SENT ? '(' . $this->app()->format->humanTime($model->sent_at) . ')' : '';

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model->attributes,
    'attributes' => [
        ['name' => 'subject', 'label' => $this->t('Subject')],
        ['name' => 'body', 'label' => $this->t('Mail-out')],
        ['name' => 'status', 'label' => $this->t('Status'), 'value' => CHtml::value($model->statusReplacement(), $model->status)
            . " $sentAt ",
        ],
        ['name' => 'created_at', 'label' => $this->t('Created'), 'value' => $this->app()->format->humanTime($model->created_at)],
    ],
]);
