<?php

use MommyCom\Controller\Backend\SmsController;
use MommyCom\Model\Db\SmsMessageRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this SmsController
 * @var $data SmsMessageRecord
 */

$grid = $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'static-page-grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,

    'summaryText' => $this->t('Messages {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form input, form select',
    'rowCssClassExpression' => function ($row, $data) {
        $css = [
            SmsMessageRecord::STATUS_WAITING => 'warning',
            SmsMessageRecord::STATUS_SENT => 'success',
            SmsMessageRecord::STATUS_ERROR => 'error',
        ];

        return CHtml::value($css, $data->status);
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        [
            'name' => 'userLike',
            'header' => $this->t('User'),
            'type' => 'raw',
            'value' => function ($data) {
                return isset($data->user) ? CHtml::link($data->user->email . " ({$data->user->name} {$data->user->surname}) ",
                    ['user/update', 'id' => $data->user->getPrimaryKey()], ['target' => '_blank']) : $data->user_id;
            },
        ],
        'telephone',
        'text',
        [
            'filter' => false,
            'name' => 'type',
            'value' => function ($data) {
                return CHtml::value($data->typeReplacement(), $data->type);
            },
        ],
        [
            'filter' => false,
            'name' => 'status',
            'value' => function ($data) {
                return CHtml::value($data->statusReplacement(), $data->status);
            },
        ],
        [
            'name' => 'is_need_to_translit',
            'type' => 'boolean',
            'filter' => $this->app()->format->booleanFormat,
        ],
        [
            'name' => 'sent_at',
            'value' => function ($data) {
                return empty($data->sent_at) ? $this->t('no') : $this->app()->format->humanTime($data->sent_at);
            },
        ],
        [
            'filter' => false,
            'name' => 'created_at',
            'type' => 'humanTime',
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
        ],
    ],
]); ?>
