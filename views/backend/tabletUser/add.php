<?php

use MommyCom\YiiComponent\Backend\Form\Form;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form Form */
$this->pageTitle = $this->t('Tablet users');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('New tablet user'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
