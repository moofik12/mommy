<?php

use MommyCom\Service\BaseController\BackController;
use MommyCom\Widget\Backend\ButtonColumn;

/* @var $this BackController */
/* @var $provider CDataProvider */
$this->pageTitle = $this->t('Tablet users');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Database of tablet users'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="pull-right">
    <?=
    CHtml::link($this->t('Add'), ['tabletUser/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => '{start} - {end} out of {count}',
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'user_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'user.login'],
        ['name' => 'user.fullname'],
        ['name' => 'last_activity_at', 'type' => 'datetime'],
        ['name' => 'created_at', 'type' => 'datetime'],
        ['name' => 'updated_at', 'type' => 'datetime'],
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
        ],
    ],
]); ?>
<div class="pull-right">
    <?=
    CHtml::link($this->t('Add'), ['tabletUser/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
