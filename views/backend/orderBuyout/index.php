<?php

use MommyCom\Model\Db\OrderBuyoutRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Widget\Backend\ButtonColumn;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('Payments report');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Orders'),
    'headerIcon' => 'icon-edit',
]); ?>
<?php $grid = $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Orders {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filterForm :input',
    'id' => 'orders',
    'columns' => [
        [
            'name' => 'order_id',
            'htmlOptions' => ['class' => 'span2'],
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data OrderTrackingRecord */
                return CHtml::link($data->order_id, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverOrder',
                        ['order' => $data->order],
                        true
                    ),
                ]);
            },
        ],
        ['name' => 'order_delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderTrackingRecord */
            return $data->orderDeliveryTypeReplacement;
        }],

        ['name' => 'status', 'type' => 'raw',
            'filter' => OrderBuyoutRecord::statusReplacements(),
            'value' => function ($data) {
                /* @var $data OrderBuyoutRecord */
                return CHtml::link($data->statusReplacement, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverStatusHistory',
                        ['dataHistory' => $data->getStatusHistory()],
                        true
                    ),
                ]);
            },
        ],

        ['name' => 'order.trackcode', 'htmlOptions' => ['class' => 'span2']],

        ['name' => 'price', 'filter' => false, 'type' => 'number'],
        ['name' => 'delivery_price', 'filter' => false, 'type' => 'number'],
        [
            'class' => ButtonColumn::class,
            'template' => '{payed} {canceled} {unpayed}',
            'buttons' => [
                'payed' => [
                    'label' => $this->t('Paid'),
                    'url' => 'array("orderBuyout/changeStatus", "id" => $data->id, "status" => ' . OrderBuyoutRecord::STATUS_PAYED . ')',
                    'visible' => function ($row, $data) {
                        /** @var $data OrderBuyoutRecord */
                        return $data->getIsStatusChangingAllowed() && $data->status == OrderBuyoutRecord::STATUS_UNPAYED;
                    },
                    'options' => [
                        'title' => $this->t('Paid'),
                        'class' => 'btn btn-success change-status',
                    ],
                ],
                'canceled' => [
                    'label' => $this->t('Will not be paid'),
                    'url' => 'array("orderBuyout/changeStatus", "id" => $data->id, "status" => ' . OrderBuyoutRecord::STATUS_CANCELLED . ')',
                    'visible' => function ($row, $data) {
                        /** @var $data OrderBuyoutRecord */
                        return $data->getIsStatusChangingAllowed() && $data->status == OrderBuyoutRecord::STATUS_UNPAYED;
                    },
                    'options' => [
                        'title' => $this->t('Will not be paid'),
                        'class' => 'btn btn-danger change-status',
                    ],
                ],
                'unpayed' => [
                    'label' => $this->t('Return to edit'),
                    'url' => 'array("orderBuyout/changeStatus", "id" => $data->id, "status" => ' . OrderBuyoutRecord::STATUS_UNPAYED . ')',
                    'visible' => function ($row, $data) {
                        /** @var $data OrderBuyoutRecord */
                        return $data->getIsStatusChangingAllowed() && $data->status != OrderBuyoutRecord::STATUS_UNPAYED;
                    },
                    'options' => [
                        'title' => $this->t('Return to edit'),
                        'class' => 'btn btn-warning change-status',
                    ],
                ],
            ],
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>


<?php
//ajax upload to click button
$csrf = '';
if ($this->app()->request->enableCsrfValidation) {
    $csrfTokenName = $this->app()->request->csrfTokenName;
    $csrfToken = $this->app()->request->csrfToken;
    $csrf = "data:{ '$csrfTokenName':'$csrfToken' },";
}

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('ajaxUpload', "
jQuery(document).on('click','#{$grid->id} a.change-status',
    function() {
        var th = this;
        jQuery('#{$grid->id}').yiiGridView('update', {
            type: 'POST',
            url: jQuery(this).attr('href'),
            $csrf
            success: function(data) {
                jQuery('#{$grid->id}').yiiGridView('update');
            },
        });
        return false;
    }
);");

?>
