<?php

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderBuyoutRecord;

$dataHistory = array_slice($dataHistory, -10, null, true);
?>


<table class="items table table-striped table-bordered table-hover">
    <th style="text-align: center">Дата</th>
    <th style="text-align: center">Статус</th>
    <th style="text-align: center">Оператор</th>
    <?php
    foreach ($dataHistory as $time => $data) {
        /** @var AdminUserRecord $adminModel */
        $adminModel = empty($data['admin_id']) ? null : AdminUserRecord::model()->findByPk($data['admin_id']);

        $admin = $adminModel !== null ? $adminModel->login . "<br>(" . $adminModel->fullname . ')' : '';

        echo '<tr>';
        echo '<td>'  . $this->app()->dateFormatter->formatDateTime($time, 'medium', 'short') . '</td>';
        echo '<td>' . CHtml::value(OrderBuyoutRecord::statusReplacements(), $data['status']) . '</td>';
        echo '<td>' . $admin . '</td>';
        echo '</tr>';
    }
    ?>
</table>
