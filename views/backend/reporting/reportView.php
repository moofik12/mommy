<?php

use MommyCom\YiiComponent\Reporting\ActiveReport;
use MommyCom\YiiComponent\Reporting\ActiveReportDataProvider;
use MommyCom\YiiComponent\Reporting\ActiveReportModel;
use MommyCom\YiiComponent\Type\Cast;

/**
 * @var $this CController
 * @var $provider ActiveReportDataProvider
 * @var $report ActiveReport
 * @var $time string
 * @var $chartType string
 */

$this->pageTitle = $this->title;

$provider = $report->getDataProvider([
    'pagination' => [
        'pageSize' => 100,
    ],
]);

Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/jsapi');

$gridColumns = [];
$columns = $provider->getColumns();

$availableColumns = []; // name => label

foreach ($columns as $column) {
    $availableColumns[$column->name] = $column->label;
}

foreach ($columns as $column) {
    $gridColumns[] = [
        'name' => $column->name,
        'header' => $column->label,
        'type' => 'raw',
        'value' => function ($data) use ($column) {
            /* @var  $data ActiveReportModel */
            $raw = $data->{$column->name};
            $formatted = $data->getFormatted($column->name, true);
            return CHtml::tag('span', ['data-raw' => $raw], $formatted);
        },
    ];
}

$availableCustomGroups = [];
foreach ($report->getReportCustomGroups() as $name => $value) {
    $availableCustomGroups[$value->name] = $value->label;
}

$gridId = 'dataGrid';
$chartId = 'dataChart';

$chartPrimary = 'time';
$chartColumns = array_keys($availableColumns);

$chartData = [];
$chartHeader = [];

$selectedChartColumns = Cast::toStrArr(Yii::app()->request->getParam('chartItems'));
if (empty($selectedChartColumns)) {
    $selectedChartColumns = $chartColumns;
}
foreach ($selectedChartColumns as $index => $value) {
    if (!array_key_exists($value, $columns)) {
        unset($selectedChartColumns[$index]);
    }
}

foreach ($selectedChartColumns as $name) {
    $chartHeader[] = $columns[$name]->label;
}

$chartData[] = $chartHeader;

foreach ($provider->getData() as $item) {
    /* @var $item ActiveReportModel */
    $chartItem = [];
    foreach ($selectedChartColumns as $name) {
        $chartItem[] = $item->getFormatted($name);
    }
    foreach ($chartItem as &$chartItemData) {
        if ($chartItemData instanceof DateTime) {
            $chartItemData = new CJavaScriptExpression('new Date("' . $chartItemData->format(DateTime::W3C) . '")');
        }
    }
    $chartData[] = $chartItem;
}

?>


<script>
    google.load('visualization', '1.0', {'packages': ['corechart']});
    google.setOnLoadCallback(drawChart);

    var chart = null;

    function drawChart() {
        var chartContainer = document.getElementById('<?= $chartId ?>');
        var gridContainer = document.getElementById('<?= $gridId ?>');

        var chartData = eval(gridContainer.dataset.chartData);


        var chartType = $('[name=chartType]').val();

        if (chartData.length > 1 && chartData[1].length > 0 && chartData[1][0] instanceof Date) {
            // chart type select
            // correct data sorting
            var header = chartData.slice(0, 1);
            var content = chartData.slice(1);
            content.sort(function (a, b) {
                if (a[0] < b[0]) {
                    return -1;
                } else if (a[0] > b[0]) {
                    return 1;
                }
                return 0;
            });

            // свертка custom groups
            var removeColumns = [];

            for (var i = 0; i < content[0].length; i++) {
                if (typeof content[0][i] == 'string' || content[0][i] instanceof String) {
                    removeColumns.push(i);

                    header[0] = header[0].slice(0, i).concat(header[0].slice(i + 1));
                }
            }

            for (var i = 0; i < content.length; i++) {
                for (var j = 0; j < removeColumns.length; j++) {
                    var k = removeColumns[j];
                    content[i] = content[i].slice(0, k).concat(content[i].slice(k + 1));
                }
            }

            for (var i = 0; i < content.length; i++) {
                var item1 = content[i];
                for (var j = i + 1; j < content.length; j++) {
                    var item2 = content[j];
                    if (+item1[0] != +item2[0]) {
                        break;
                    }

                    for (var k = 0; k < item1.length; k++) {
                        if (typeof item1[k] === 'number' && isFinite(item1[k])) {
                            item1[k] = item1[k] + item2[k];
                        }
                    }
                }
                content = content.slice(0, i + 1).concat(content.slice(j));
            }
            chartData = header.concat(content);
        } else {
            chartType = 'ColumnChart'
        }


        var data = google.visualization.arrayToDataTable(chartData);

        var options = {
            titleTextStyle: {color: '#FF0000'},
            chartArea: {
                left: '0%',
                width: '100%'
            },
            colors: ['#0088CC', '#dc3912', '#109618', '#12AFE9', '#ff9900', 'green', '#FF00FF', 'red', 'blue'],
            areaOpacity: 0.15,
            //curveType: 'function',
            legend: {position: 'top'},
            backgroundColor: {
                fill: 'transparent'
            },
            pointSize: 8,
            lineWidth: 4,
            hAxis: {},
            vAxis: {
                textPosition: 'in'
            },
            tooltip: {
                trigger: 'both'
            }
        };

        if (chart !== null) {
            chart.clearChart();
        }

        if (chartData[0].length < 2) {
            return;
        }

        chart = new google.visualization[chartType](chartContainer);

        chart.draw(data, options);
    }

    function gridUpdated(id, data) {
        drawChart();
    }
</script>

<style>
    .report-dropdown {
        background: #ffffff;
        -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1);
        -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1);
        box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1);
        color: #333333;
        padding: 8px;
        line-height: 18px;
        cursor: pointer;
    }

    .report-dropdown:hover {
        -webkit-box-shadow: 0 1px 3px rgba(82, 168, 236, .6), inset 0 -1px 0 rgba(0, 0, 0, .1);
        -moz-box-shadow: 0 1px 3px rgba(82, 168, 236, .6), inset 0 -1px 0 rgba(0, 0, 0, .1);
        box-shadow: 0 1px 3px rgba(82, 168, 236, .6), inset 0 -1px 0 rgba(0, 0, 0, .1);
    }

    .report-dropdown .add-on {
        background: none;
        border: none;
    }

    .report-dropdown input[readonly] {
        background: transparent;
        border: none;
        box-shadow: none;
        height: 100%;

        font-weight: bold;
        color: rgb(60, 60, 60);
        font-size: 17px;
    }

    .report-dropdown input[readonly]:focus {
        box-shadow: none;
    }

    .report-dropdown .select2-choice, .report-dropdown .select2-choices {
        background: transparent !important;
        border: none !important;
        box-shadow: none !important;
    }

    .report-dropdown .select2-choices .select2-search-field {
        max-width: 100px !important;
    }

    .report-dropdown .select2-container {
        min-width: 250px !important;
    }

    .report-dropdown .select2-container-multi .select2-choices .select2-search-choice {
        background-image: none;
        background-color: #f5f5f5;
        border: 1px solid #dddddd;
    }

    .report-dropdown .select2-container-multi .select2-choices .select2-search-choice div {
        padding: 3px;
    }

    .chart {
        position: relative;
        left: 2%;
        width: 96%;
    }
</style>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Report'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="pull-right">

    <?php if (!empty($availableCustomGroups)): ?>
        <label class="input-prepend report-dropdown report-chart-items" data-filter-grid="true">
                <span class="add-on">
                    <i class="icon icon-cogs"></i>
                </span>
            <?php $this->widget('bootstrap.widgets.TbSelect2', [
                'name' => 'customGroups',
                'data' => $availableCustomGroups,
                'options' => [
                    'width' => 'off',
                    'tokenSeparators' => [",", " "],
                ],
                'htmlOptions' => [
                    'multiple' => 'multiple',
                    'placeholder' => $this->t('Advanced options'),
                    'min-width' => '300px',
                    'value' => implode(', ', $report->selectedCustomGroups),
                ],
            ]) ?>
            <?= CHtml::hiddenField('customGroups', 'notselected') ?>
        </label>
    <?php endif ?>
    <label class="input-prepend report-dropdown" data-filter-grid="true">
            <span class="add-on pull-left">
                <i class="icon icon-th-large icon-lg"></i>
            </span>
        <?= CHtml::dropDownList('group', $report->getSelectedReportGroup()->name, CHtml::listData($report->groups, 'name', 'label')) ?>
    </label>
    <label class="input-prepend input-append report-dropdown" data-filter-grid="true">
            <span class="add-on pull-left">
                <i class="icon icon-calendar icon-lg"></i>
            </span>
        <span class="add-on pull-right">
                <i class="icon icon-caret-down"></i>
            </span>
        <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
            'name' => 'time',
            'value' => $time,
            'htmlOptions' => [
                'readonly' => 'readonly',
                'class' => 'pull-right',
            ],
            'callback' => new CJavaScriptExpression("function() {
                    $('.report-dropdown input[name=time]').trigger('change');
                }"),
        ]) ?>
    </label>
</div>
<div class="clearfix"></div>


<div class="chart">
    <div class="chart-settings">
        <div>
            <label class="input-prepend report-dropdown report-chart-items" data-filter-grid="true">
                    <span class="add-on">
                        <i class="icon icon-cogs"></i>
                    </span>
                <?php $this->widget('bootstrap.widgets.TbSelect2', [
                    'name' => 'chartItems',
                    'data' => $availableColumns,
                    'val' => CJSON::encode($selectedChartColumns),
                    'options' => [
                        'width' => 'off',
                        'tokenSeparators' => [",", " "],
                    ],
                    'htmlOptions' => [
                        'multiple' => 'multiple',
                        'min-width' => '300px',
                        'value' => implode(', ', $chartColumns),
                    ],
                ]) ?>
                <?= CHtml::hiddenField('chartItems', 'notselected') ?>
            </label>
            <label class="input-prepend report-dropdown" data-modify-chart="true">
                    <span class="add-on">
                        <i class="icon icon-cogs"></i>
                    </span>

                <?= CHtml::dropDownList('chartType', $chartType, [
                    'AreaChart' => $this->t('Frequency polygon'),
                    'ColumnChart' => $this->t('Histogram'),
                ]) ?>
            </label>
        </div>

        <div id="colFilter_div"></div>
    </div>
    <div class="chart-render" id="<?= $chartId ?>">
    </div>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'columns' => $gridColumns,
    'id' => $gridId,
    'filterSelector' => '{filter}, *[data-filter-grid] :input, *[data-modify-chart] :input',
    'htmlOptions' => [
        'data-chart-data' => CJavaScript::encode($chartData),
    ],
    'afterAjaxUpdate' => 'gridUpdated',
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>


<?php
//дополнительная валидация
/** @var CClientScript $cs */
$cs = Yii::app()->clientScript;
$cs->registerScript('groupValidate', '
    $("#group").on("change", function(e) {
        var value = $(this).val();
        var monthMillisecond = 2628000000;

        if (value === "months") {
            var dateRange = $("#time").val().split(" - ");
            var period = Math.abs(Date.parse(dateRange[0]) - Date.parse(dateRange[1]));

            if (period < monthMillisecond) {
                e.stopPropagation();
                alert("Select a period longer than a month");
            }
        }
    });
');

?>

