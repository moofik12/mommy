<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $smsForm TbForm отправка СМС модальное окно
 * @var $emailForm TbForm отправка E-mail модальное окно
 * @var $this UserController
 * @var $dataProvider CActiveDataProvider
 */
$this->pageTitle = $this->t('Users');
$csrf = $this->app()->request->enableCsrfValidation
    ? [$this->app()->request->csrfTokenName => $this->app()->request->csrfToken] : [];

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('User database'),
    'headerIcon' => 'icon-edit',
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
    'type' => 'primary',
    'label' => $this->t('Add user'),
    'url' => $this->createUrl('add'),
    'htmlOptions' => [
        'target' => '_blank',
    ],
]); ?>

<div class="pull-right">
    <?= CHtml::dropDownList('pageSize', $dataProvider->pagination->pageSize
        , [10 => '10', 30 => '30', 50 => '50', 75 => '75', 100 => '100']) ?>
</div>

<?php
/* @var $data UserRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Users {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #pageSize',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'name',
            'header' => $this->t('Name'),
            'filterInputOptions' => ['id' => 'name-id'],
        ],
        [
            'name' => 'surname',
            'header' => $this->t('Last name'),
            'filterInputOptions' => ['id' => 'surname-id'],
        ],
        [
            'name' => 'email',
            'filterInputOptions' => ['id' => 'email-id'],
        ],
        [
            'name' => 'telephone',
        ],
        [
            'filter' => false,
            'name' => 'invited_by',
            'value' => function ($data) {
                return isset($data->invitedBy) ? $data->invitedBy->email : '';
            },
        ],
        [
            'name' => 'status',
            'value' => '$data->statusReplacement',
            'filter' => $dataProvider->model->statusReplacements(),
        ],
        [
            'name' => 'category',
            'value' => '$data->categoryReplacement',
            'filter' => $dataProvider->model->categoryReplacements(),
            'headerHtmlOptions' => [
                'title' => $this->t('people interested in Reselling program'),
            ],
        ],
        [
            'header' => 'Sms, E-mail',
            'class' => ButtonColumn::class,
            'template' => '{email} {sms} {history}',
            'buttons' => [
                'sms' => [
                    'label' => '<i class="icon-envelope"></i>',
                    'click' => 'function(){
                            var data = $(this).data("user");
                            $("#' . CHtml::activeId($smsForm->model, 'userIds') . '").select2("data", data);
                        }',
                    'options' => [
                        'title' => $this->t('Send SMS'),
                        'data-toggle' => 'modal',
                        'data-target' => '#send-sms',
                        'data' => [
                            'user' => 'CJavaScript::jsonEncode(array("id" => "$data->primaryKey", "text" => "$data->email"))',
                        ],
                    ],
                ],
                'email' => [
                    'label' => '@',
                    'click' => 'function(){
                            var data = $(this).data("user");
                            $("#' . CHtml::activeId($emailForm->model, 'userIds') . '").select2("data", data);
                        }',
                    'options' => [
                        'title' => $this->t('Send e-mail'),
                        'data-toggle' => 'modal',
                        'data-target' => '#send-email',
                        'data' => [
                            'user' => 'CJavaScript::jsonEncode(array("id" => "$data->primaryKey", "text" => "$data->email"))',
                        ],
                    ],
                ],
                'history' => [
                    'label' => '<i class="icon-book"></i>',
                    'url' => '\Yii::app()->createUrl("user/messages", array("id" => "$data->primaryKey"))',
                    'options' => [
                        'title' => $this->t('Message history'),

                    ],
                ],
            ],
        ],
        ['name' => 'last_visit_at', 'type' => 'humanTime', 'filter' => false, 'header' => 'Last activity'],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {banned}',
            'buttons' => [
                'banned' => [
                    'label' => '<i class="icon-ban-circle"></i>',
                    'url' => 'array("user/banned", "id" => $data->id)',
                    'options' => [
                        'title' => $this->t('Block'),
                        'class' => 'btn btn-danger',
                    ],
                    'click' => "js:function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Update not possible') ."');
                                }
                            });

                            return false;
                        }",
                ],
            ],
        ],
    ],
    'bulkActions' => [
        'align' => 'left',
        'actionButtons' => [
            'email' => [
                'id' => 'select-all-email',
                'buttonType' => 'button',
                'type' => 'info',
                'size' => 'small',
                'label' => 'E-mail',
                'click' => 'js:function(values){
                        var data = [];
                        values.each(function(indx, element) {
                            data.push($.parseJSON($(element).val()));
                        });
                        $("#' . CHtml::activeId($emailForm->model, 'userIds') . '").select2("data", data);
                    }',
                'htmlOptions' => [
                    'title' => $this->t('Send Email'),
                    'data-toggle' => 'modal',
                    'data-target' => '#send-email',
                ],
            ],
            'sms' => [
                'id' => 'select-all-sms',
                'buttonType' => 'button',
                'type' => 'info',
                'size' => 'small',
                'label' => 'SMS',
                'click' => 'js:function(values){
                        var data = [];
                        values.each(function(indx, element) {
                            data.push($.parseJSON($(element).val()));
                        });
                        $("#' . CHtml::activeId($smsForm->model, 'userIds') . '").select2("data", data);
                    }',
                'htmlOptions' => [
                    'title' => $this->t('Send SMS'),
                    'data-toggle' => 'modal',
                    'data-target' => '#send-sms',
                ],
            ],
        ],
        'checkBoxColumnConfig' => [
            'name' => 'id',
            //дабы не выполнять еще один запрос
            'value' => 'CJavaScript::jsonEncode(array("id" => "$data->primaryKey", "text" => "$data->email"))',
        ],
    ],
]);

$this->endWidget(); ?>

<?php $this->renderPartial('_smsModal', ['smsForm' => $smsForm]) ?>
<?php $this->renderPartial('_emailModal', ['emailForm' => $emailForm]) ?>
