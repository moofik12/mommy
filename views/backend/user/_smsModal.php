<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\Model\Db\SmsMessageRecord;

/**
 * @var $this UserController
 * @var $smsForm TbForm
 */

$this->beginWidget('bootstrap.widgets.TbModal', [
    'id' => 'send-sms',
    'fade' => false,
    'events' => [
        //запрет на скрол так как в окне select2 style="position: absolute;"
        'shown' => 'js:function() {
            $("body").css("overflow", "hidden");

        }',
        'hidden' => 'js:function() {
            $("#sms-errors").addClass("hide");
            $("body").css("overflow", "auto");
        }',

    ],
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?= $this->t('Send SMS') ?></h3>
</div>
<div class="modal-body">
    <?= $smsForm->renderBegin() ?>
    <?= $smsForm['userIds'] ?>
    <?= $smsForm['text'] ?>
    <div class="btn-toolbar">
        <?= CHtml::resetButton($this->t('Clear'), ['class' => 'btn', 'id' => 'sms-form-reset']) ?>
        <?= CHtml::ajaxSubmitButton($this->t('Send'), $this->createUrl('sms/createFromUsers'
            , ['type' => SmsMessageRecord::TYPE_CUSTOM])
            , ['success' => 'function(result) {
                    var outErrors = $("#sms-errors");
                    var content = "";

                    if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                        $.each(result.errors, function (j, message) {
                            content = content + "<li>" + message + "</li>";
                        });

                        outErrors.html("<ul>" + content + "</ul>").removeClass("hide");
                    } else {
                        outErrors.addClass("hide");
                        outErrors.closest("#send-sms").modal("hide");
                    }

                }'], ['class' => 'btn btn-primary']
        ) ?>
    </div>
    <?= $smsForm->renderEnd() ?>
    <div id="sms-errors" class="alert alert-block alert-error hide"></div>
</div>

<?php $this->endWidget();
//end modal
?>
