<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $this  UserController
 * @var $form Form
 * @var $formPassword Form
 */
$this->pageTitle = $this->t('Editing user data');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('User data') . ' ' . $model->email,
    'headerIcon' => 'icon-edit',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Change password'),
    'headerIcon' => 'icon-wrench',
]); ?>

<?= $formPassword->render() ?>

<?php $this->endWidget(); ?>
