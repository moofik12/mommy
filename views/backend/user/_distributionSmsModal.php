<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\Model\Db\SmsMessageRecord;

/**
 * @var $this UserController
 * @var $distributionSmsForm TbForm
 */

$this->beginWidget('bootstrap.widgets.TbModal', [
    'id' => 'distribution-send-sms',
    'fade' => false,
    'events' => [
        //запрет на скрол так как в окне select2 style="position: absolute;"
        'shown' => 'js:function() {
            $("body").css("overflow", "hidden");

        }',
        'hidden' => 'js:function() {
            $("#distribution-sms-errors").addClass("hide");
            $("#distribution-sms-info").addClass("hide");
            $("body").css("overflow", "auto");
        }',

    ],
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?= $this->t('SMS mail-out') ?></h3>
</div>
<div class="modal-body">
    <?= $distributionSmsForm->renderBegin() ?>
    <?= $distributionSmsForm['region'] ?>
    <?= $distributionSmsForm['cities'] ?>
    <?= $distributionSmsForm['text'] ?>
    <div class="btn-toolbar">
        <?= CHtml::resetButton($this->t('Clear'), ['class' => 'btn', 'id' => 'distribution-sms-form-reset']) ?>
        <?= CHtml::ajaxSubmitButton($this->t('Send'), $this->createUrl('sms/createDistribution'
            , ['type' => SmsMessageRecord::TYPE_CUSTOM])
            , ['success' => 'function(result) {
                    var outErrors = $("#distribution-sms-errors");
                    var outInfo = $("#distribution-sms-info");
                    var resetButton = $("#distribution-sms-form-reset");

                    if ($.isArray(result.errors) && result.errors.length > 0) {
                        var content = "";
                        $.each(result.errors, function (j, message) {
                            content = content + "<li>" + message + "</li>";
                        });

                        outErrors.html("<ol>" + content + "</ol>").removeClass("hide");
                    } else {
                        outErrors.addClass("hide");
                        resetButton.trigger("click");
                    }

                    if ($.isArray(result.info) && result.info.length > 0) {
                        var content = "";
                        $.each(result.info, function (j, message) {
                            content = content + "<li>" + message + "</li>";
                        });

                        outInfo.html("<ul>" + content + "</ul>").removeClass("hide");
                    } else {
                        outInfo.addClass("hide");
                    }

                }'], ['class' => 'btn btn-primary']
        ) ?>

        <?= CHtml::ajaxSubmitButton($this->t('Check'), $this->createUrl('sms/createDistribution'
            , ['type' => SmsMessageRecord::TYPE_CUSTOM, 'info' => true])
            , ['success' => 'function(result) {
                    var outErrors = $("#distribution-sms-errors");
                    var outInfo = $("#distribution-sms-info");

                    if ($.isArray(result.errors) && result.errors.length > 0) {
                        var content = "";
                        $.each(result.errors, function (j, message) {
                            content = content + "<li>" + message + "</li>";
                        });

                        outErrors.html("<ol>" + content + "</ol>").removeClass("hide");
                    } else {
                        outErrors.addClass("hide");
                    }

                    if ($.isArray(result.info) && result.info.length > 0) {
                        var content = "";
                        $.each(result.info, function (j, message) {
                            content = content + "<li>" + message + "</li>";
                        });

                        outInfo.html("<ul>" + content + "</ul>").removeClass("hide");
                    } else {
                        outInfo.addClass("hide");
                    }

                }'], ['class' => 'btn btn-info pull-right']
        ) ?>
    </div>
    <?= $distributionSmsForm->renderEnd() ?>
    <div id="distribution-sms-info" class="alert alert-block alert-info hide"></div>
    <div id="distribution-sms-errors" class="alert alert-block alert-error hide"></div>
</div>

<?php $this->endWidget();
//end modal
?>
