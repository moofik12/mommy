<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\Model\Db\EmailMessage;

/**
 * @var $this UserController
 * @var $emailForm TbForm
 */

$this->beginWidget('bootstrap.widgets.TbModal', [
    'id' => 'send-email',
    'fade' => false,
    'htmlOptions' => [
        'style' => "width: 600px;",
    ],
    'events' => [
        //запрет на скрол так как в окне select2 style="position: absolute;"
        'shown' => 'js:function() {
            $("body").css("overflow", "hidden");

        }',
        'hidden' => 'js:function() {
            $("#email-errors").addClass("hide");
            $("body").css("overflow", "auto");
        }',

    ],
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?= $this->t('E-mail sending') ?></h3>
</div>
<div class="modal-body" style="max-height: 480px;">
    <?= $emailForm->renderBegin() ?>
    <?= $emailForm['userIds'] ?>
    <?= $emailForm['subject'] ?>
    <?= $emailForm['body'] ?>
    <div class="btn-toolbar">
        <?= CHtml::resetButton($this->t('Clear'), ['class' => 'btn', 'id' => 'email-form-reset']) ?>
        <?= CHtml::ajaxSubmitButton($this->t('Send'), $this->createUrl('email/createFromUsers'
            , ['type' => EmailMessage::TYPE_CUSTOM])
            , ['success' => 'function(result) {
                    var outErrors = $("#email-errors");
                    var content = "";

                    if (result.errors && $.isArray(result.errors) && result.errors.length > 0) {
                        $.each(result.errors, function (j, message) {
                            content = content + "<li>" + message + "</li>";
                        });

                        outErrors.html("<ul>" + content + "</ul>").removeClass("hide");
                    } else {
                        outErrors.addClass("hide");
                        outErrors.closest("#send-email").modal("hide");
                    }

                }'], ['class' => 'btn btn-primary']
        ) ?>
    </div>
    <?= $emailForm->renderEnd() ?>
    <div id="email-errors" class="alert alert-block alert-error hide"></div>
</div>

<?php $this->endWidget();
//end modal
?>

<?php
//при отмене очищаются все данные в редакторе
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('reset-form-email', new CJavaScriptExpression("
    $('#email-form-reset').on('click', function(){
        $('#" . CHtml::activeId($emailForm->model, 'body') . "').setCode('');
    });
"));
?>
