<?php
/**
 * @var $this CController
 */

$this->pageTitle = $this->t('Add user');
?>


<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('New user'),
    'headerIcon' => 'icon-plus',
    'id' => 'add-user',
]);

echo $form->renderBegin();
echo $form->renderElement('profile');
echo $form->renderEnd();

$this->endWidget(); ?>
