<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\Model\Db\SmsMessageRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * @var $this UserController
 * @var $model UserRecord
 * @var $smsProvider CActiveDataProvider
 * @var $emailProvider CActiveDataProvider
 */
?>

<h3>
    <small> <?= $this->t('Message history') ?> <?= $model->email ?> (<?= "$model->name $model->surname" ?>)</small>
</h3>

<?php
//sms message
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => 'SMS',
    'headerIcon' => 'icon-envelope',
    'id' => 'sms-message',
]);

$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $smsProvider,
//    'template'=>"{items}",
    'summaryText' => $this->t('Messages') . ' <span class="badge badge-info">{count}</span>',
    'rowCssClassExpression' => function ($row, $data) {
        $css = [
            SmsMessageRecord::STATUS_WAITING => 'warning',
            SmsMessageRecord::STATUS_SENT => 'success',
            SmsMessageRecord::STATUS_ERROR => 'error',
        ];

        return CHtml::value($css, $data->status);
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        'telephone',
        'text',
        [
            'filter' => false,
            'name' => 'status',
            'value' => function ($data) {
                return CHtml::value($data->statusReplacement(), $data->status);
            },
        ],
        [
            'filter' => false,
            'name' => 'type',
            'value' => function ($data) {
                return CHtml::value($data->typeReplacement(), $data->type);
            },
        ],
        [
            'filter' => false,
            'name' => 'sent_at',
            'value' => function ($data) {
                return empty($data->sent_at) ? $this->t('no') : $this->app()->format->humanTime($data->sent_at);
            },
        ],
        [
            'filter' => false,
            'name' => 'created_at',
            'type' => 'humanTime',
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
    ],
]);

$this->endWidget();
?>

<?php
//email message
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => '&nbsp;@' . 'E-mail messages',
    'id' => 'email-message',
]);

$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $emailProvider,
//    'template'=>"{items}",
    'summaryText' => $this->t('Messages') . '<span class="badge badge-info">{count}</span>',
    'rowCssClassExpression' => function ($row, $data) {
        $css = [
            SmsMessageRecord::STATUS_WAITING => 'warning',
            SmsMessageRecord::STATUS_SENT => 'success',
            SmsMessageRecord::STATUS_ERROR => 'error',
        ];

        return CHtml::value($css, $data->status);
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'url' => $this->createUrl('email/view'),
            'name' => 'from',
            'value' => function ($data) {
                $text = is_string($data->from) ? $data->from : '';

                if (is_array($data->from)) {
                    foreach ($data->from as $from => $name) {
                        $name = isset($name) ? "($name)" : '';
                        $text .= "$from $name\n";
                    }
                }

                return $text;
            },
        ],
        [
            'name' => 'to',
            'value' => function ($data) {
                $text = is_string($data->to) ? $data->to : '';

                if (is_array($data->to)) {
                    foreach ($data->to as $from => $name) {
                        $name = isset($name) ? "($name)" : '';
                        $text .= "$from $name\n";
                    }
                }

                return $text;
            },
        ],
        'subject',
        [
            'name' => 'status',
            'filter' => false,
            'value' => function ($data) {
                return CHtml::value($data->statusReplacement(), $data->status);
            },
        ],
        [
            'name' => 'type',
            'filter' => false,
            'value' => function ($data) {
                return CHtml::value($data->typeReplacement(), $data->type);
            },
        ],
        [
            'name' => 'created_at',
            'type' => 'humanTime',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
    ],
]);

$this->endWidget();
?>
