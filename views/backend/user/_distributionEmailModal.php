<?php

use MommyCom\Controller\Backend\UserController;
use MommyCom\Model\Db\SmsMessageRecord;

/**
 * @var $this UserController
 * @var $distributionEmailForm TbForm
 */

$this->beginWidget('bootstrap.widgets.TbModal', [
    'id' => 'distribution-send-email',
    'fade' => false,
    'events' => [
        //запрет на скрол так как в окне select2 style="position: absolute;"
        'shown' => 'js:function() {
            $("body").css("overflow", "hidden");

        }',
        'hidden' => 'js:function() {
            $("#distribution-email-errors").addClass("hide");
            $("#distribution-email-info").addClass("hide");
            $("body").css("overflow", "auto");
        }',

    ],
]); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?= $this->t('Mail-out') ?></h3>
</div>
<div class="modal-body" style="max-height: 500px;">
    <?= $distributionEmailForm->renderBegin() ?>
    <?= $distributionEmailForm['region'] ?>
    <?= $distributionEmailForm['cities'] ?>
    <?= $distributionEmailForm['subject'] ?>
    <?= $distributionEmailForm['body'] ?>
    <div class="btn-toolbar">
        <?= CHtml::resetButton($this->t('Clear'), ['class' => 'btn', 'id' => 'distribution-email-form-reset']) ?>
        <?= CHtml::ajaxSubmitButton($this->t('Send'), $this->createUrl('email/createDistribution'
            , ['type' => SmsMessageRecord::TYPE_CUSTOM])
            , [
                'success' => 'function(result) {
                        var outErrors = $("#distribution-email-errors");
                        var outInfo = $("#distribution-email-info");
                        var resetButton = $("#distribution-email-form-reset");

                        if ($.isArray(result.errors) && result.errors.length > 0) {
                            var content = "";
                            $.each(result.errors, function (j, message) {
                                content = content + "<li>" + message + "</li>";
                            });

                            outErrors.html("<ol>" + content + "</ol>").removeClass("hide");
                        } else {
                            outErrors.addClass("hide");
                            resetButton.trigger("click");
                        }

                        if ($.isArray(result.info) && result.info.length > 0) {
                            var content = "";
                            $.each(result.info, function (j, message) {
                                content = content + "<li>" + message + "</li>";
                            });

                            outInfo.html("<ul>" + content + "</ul>").removeClass("hide");
                        } else {
                            outInfo.addClass("hide");
                        }
                    }',
            ], ['class' => 'btn btn-primary']
        ) ?>

        <?= CHtml::ajaxSubmitButton($this->t('Check'), $this->createUrl('email/createDistribution'
            , ['type' => SmsMessageRecord::TYPE_CUSTOM, 'info' => true])
            , [
                'success' => 'function(result) {
                        var outErrors = $("#distribution-email-errors");
                        var outInfo = $("#distribution-email-info");

                        if ($.isArray(result.errors) && result.errors.length > 0) {
                            var content = "";
                            $.each(result.errors, function (j, message) {
                                content = content + "<li>" + message + "</li>";
                            });

                            outErrors.html("<ol>" + content + "</ol>").removeClass("hide");
                        } else {
                            outErrors.addClass("hide");
                        }

                        if ($.isArray(result.info) && result.info.length > 0) {
                            var content = "";
                            $.each(result.info, function (j, message) {
                                content = content + "<li>" + message + "</li>";
                            });

                            outInfo.html("<ul>" + content + "</ul>").removeClass("hide");
                        } else {
                            outInfo.addClass("hide");
                        }
                    }',
            ], ['class' => 'btn btn-info pull-right']
        ) ?>
    </div>
    <?= $distributionEmailForm->renderEnd() ?>
    <div id="distribution-email-info" class="alert alert-block alert-info hide"></div>
    <div id="distribution-email-errors" class="alert alert-block alert-error hide"></div>
</div>

<?php
$this->endWidget();
//end modal
?>

<?php
//при отмене очищаются все данные в редакторе
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('distribution-reset-form-email', new CJavaScriptExpression("
    $('#distribution-email-form-reset').on('click', function(){
        $('#" . CHtml::activeId($distributionEmailForm->model, 'body') . "').setCode('');
    });
"));
?>
