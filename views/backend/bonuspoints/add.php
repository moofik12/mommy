<?php

/* @var $this \MommyCom\Service\BaseController\BackController */

/* @var $form TbForm */

$this->pageTitle = $this->t('Bonuses');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Bonuses credited'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
