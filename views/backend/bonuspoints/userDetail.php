<?php

use MommyCom\Controller\Backend\BonuspointsController;
use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;

/* @var $this BonuspointsController */
/* @var $provider CActiveDataProvider */
/* @var $bonusPoints ShopBonusPoints */
/* @var $provider CArrayDataProvider */
/* @var $user UserRecord */

$this->pageTitle = $this->t('User bonus account');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('User bonus account') . ' ' . CHtml::encode($user->fullname) . ' (' . CHtml::encode($user->email) . ')',
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $bonusPoints,
    'attributes' => [
        [
            'label' => $this->t('Balance'),
            'value' => $bonusPoints->getAvailableToSpend(),
        ],
        [
            'label' => $this->t('Gift bonuses'),
            'value' => $bonusPoints->getAvailableToSpendOnlyPresent(),
        ],
        [
            'label' => $this->t('Internal bonuses'),
            'value' => $bonusPoints->getAvailableToSpendOnlyInner(),
        ],
        [
            'label' => $this->t('Blocked bonuses for orders'),
            'value' => $bonusPoints->getLocks()->getLockedPoints(),
        ],
        [
            'label' => $this->t('Blocked gift bonuses for orders '),
            'value' => $bonusPoints->getLocks()->getLockedPoints(UserBonuspointsRecord::TYPE_PRESENT),
        ],
        [
            'label' => $this->t('Blocked internal bonuses for orders'),
            'value' => $bonusPoints->getLocks()->getLockedPoints(UserBonuspointsRecord::TYPE_INNER),
        ],
    ],
]); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'enableHistory' => false,
    'dataProvider' => $provider,
    'summaryText' => $this->t('Bonuses') . ' {start} - {end} ' . $this->t('from') . ' {count}',
    'columns' => [
        [
            'name' => 'id',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('id'),
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'type',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('type'),
            'filter' => UserBonuspointsRecord::getTypes(),
            'value' => '$data->typeReplacement',
        ],
        [
            'name' => 'user.fullname',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('user.fullname'),
            'value' => '$data->user->fullname',
        ],
        [
            'name' => 'points',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('points'),
        ],
        [
            'name' => 'message',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('message'),
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'is_custom',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('is_custom'),
            'type' => 'boolean',
        ],
        [
            'name' => 'custom_message',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('custom_message'),
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'expire_at',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('expire_at'),
            'value' => function ($date) {
                /** @var UserBonuspointsRecord $date */
                return $date->getExpireAtString('%d.%m.%Y');
            },

        ],
        [
            'name' => 'created_at',
            'header' => UserBonuspointsRecord::model()->getAttributeLabel('created_at'),
            'type' => 'datetime',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
