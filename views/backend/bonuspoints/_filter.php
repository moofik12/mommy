<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 */
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

$this->widget(
    'TbDateRangePicker', [
        'name' => 'createdAtRange',
        'value' => $this->app()->request->getParam('createdAtRange', false),
        'htmlOptions' => [
            'placeHolder' => $this->t('Time created'),
        ],
    ]
);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
