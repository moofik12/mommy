<?php

use MommyCom\Model\Db\UserBonuspointsRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Bonuses');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Bonuses'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<div class="pull-right">
    <?= CHtml::link($this->t('Credit'), ['bonuspoints/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
    <?= CHtml::link($this->t('Credit by order number'), ['bonuspoints/addByOrder'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'enableHistory' => true,
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Bonuses') . ' {start} - {end} ' . $this->t('from') . ' {count}',
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span2']],
        [
            'name' => 'type',
            'filter' => UserBonuspointsRecord::getTypes(),
            'value' => '$data->typeReplacement',
        ],
        [
            'name' => 'user_id',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var UserBonuspointsRecord $data */
                $text = CHtml::encode($data->user_id);
                $text .= "<br>" . CHtml::link($this->t('Balance'), $this->app()->controller->createUrl('userDetail', ['id' => $data->user_id]), ['target' => '_black']);

                return $text;
            },
        ],
        [
            'name' => 'user.fullname',
        ],
        ['name' => 'points'],
        [
            'name' => 'message',
            'htmlOptions' => ['class' => 'span2'],
        ],
        ['name' => 'is_custom', 'filter' => [false => $this->t('No'), true => $this->t('Yes')], 'type' => 'boolean'],
        [
            'name' => 'custom_message',
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'expire_at',
            'value' => function ($date) {
                /** @var UserBonuspointsRecord $date */
                return $date->getExpireAtString('%d.%m.%Y');
            },

        ],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
