<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\ProductRecord;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Utf8;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */

/* @var $assortmentUploadForm TbForm */
/* @var $requestUploadForm TbForm */

$this->pageTitle = $this->t('Product catalog');
$request = $this->app()->request;
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Catalog'),
    'headerIcon' => 'icon-add',
]); ?>
<?php $this->renderPartial('_filter') ?>

<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'enableSorting' => false,
    'summaryText' => $this->t('Flash-sales {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, #filter-form',
    'extraRowColumns' => ['product.name'],
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'product_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'product.logo_fileid', 'type' => 'raw', 'filter' => false, 'value' => function ($data) {
            /* @var $data EventProductRecord */
            return $data->product->logo->isEmpty ? $this->t('No') : CHtml::image($data->product->logo->getThumbnailByWidth(40)->url);
        }],
        'article',
        'event_id',
        ['name' => 'name', 'headerHtmlOptions' => ['class' => 'span4']],
        'supplier.name',
        'brand.name',
        'color',
        'size',
        'made_in',
        'design_in',
        ['name' => 'ageRangeReplacement', 'filter' => false],
        ['name' => 'target', 'filter' => ProductTargets::instance()->getLabels(), 'value' => function ($data) {
            /* @var $data ProductRecord */
            return Utf8::implode(', ', ProductTargets::instance()->getLabels($data->target));
        }],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],

        [
            'name' => 'product.name',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data EventProductRecord */
                $result = CHtml::encode($data->product->name);

                if (!empty($data->product->color)) {
                    $result .= ', ' . CHtml::encode($data->product->color);
                }

                if (!empty($data->size)) {
                    $result .= ', ' . $data->sizeformatReplacement . ' ' . CHtml::encode($data->size);
                }

                $result .= ', ' . CHtml::link($data->event->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
                    ]);

                $result .= ', ' . CHtml::link(
                        $this->t('View on the site'),
                        $this->app()->frontendUrlManager->createUrl('product/index', ['id' => $data->product_id, 'eventId' => $data->event_id]),
                        ['target' => '_blank']
                    );
                return $result;
            },
            'headerHtmlOptions' => ['class' => 'hide'],
            'htmlOptions' => ['class' => 'hide'],
        ],
    ],
]); ?>
<?php $this->endWidget() ?>
