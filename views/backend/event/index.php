<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\YiiComponent\Type\Cast;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */

/* @var $supplier string */
/* @var $isTimeActive string */
/* @var $isStock string */
/* @var $eventStartAtRange string */
/* @var $eventEndAtRange string */
/* @var $isDropShipping string */

$this->pageTitle = $this->t('Flash-sales');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Flash-sales (base)'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<div class="row">
    <label>
        <div><?= $provider->model->getAttributeLabel('supplier_id') ?></div>
        <?= CHtml::textField('supplier', $supplier) ?>
    </label>
    <label>
        <div><?= $provider->model->getAttributeLabel('isTimeActive') ?></div>
        <?= CHtml::dropDownList(
            'isTimeActive',
            $isTimeActive,
            [
                '' => '',
                'not' => $this->t('No'),
                'yes' => $this->t('Yes'),
            ]
        ) ?>
    </label>
    <label>
        <div><?= $provider->model->getAttributeLabel('is_stock') ?></div>
        <?= CHtml::dropDownList(
            'isStock',
            $isStock,
            [
                '' => '',
                'not' => $this->t('No'),
                'yes' => $this->t('Yes'),
            ]
        ) ?>
    </label>
    <label>
        <div><?= $provider->model->getAttributeLabel('start_at') ?></div>
        <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
            'name' => 'eventStartAtRange',
            'value' => $eventStartAtRange,
            'htmlOptions' => [
                'placeholder' => $provider->model->getAttributeLabel('start_at'),
            ],
        ]) ?>
    </label>
    <label>
        <div><?= $provider->model->getAttributeLabel('end_at') ?></div>
        <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
            'name' => 'eventEndAtRange',
            'value' => $eventEndAtRange,
            'htmlOptions' => [
                'placeholder' => $provider->model->getAttributeLabel('end_at'),
            ],
        ]) ?>
    </label>
</div>
<div class="row">
    <?= CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-success']) ?>
</div>
<?php $this->endWidget() ?>

<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['event/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Flash-sales') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'logo_fileid', 'type' => 'raw', 'filter' => false, 'value' => function ($data) {
            /* @var $data EventRecord */
            return $data->logo->isEmpty ? $this->t('No') : CHtml::image($data->logo->getThumbnailByWidth(40)->url);
        }],
        ['name' => 'promo_fileid', 'type' => 'raw', 'filter' => false, 'value' => function ($data) {
            /* @var $data EventRecord */
            return $data->logo->isEmpty ? $this->t('No') : CHtml::image($data->promo->getThumbnailByWidth(40)->url);
        }],
        ['name' => 'name'],
        ['name' => 'supplier.name', 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'name' => 'status',
            'headerHtmlOptions' => ['class' => 'span2'],
            'filter' => EventRecord::statusReplacements(),
            'value' => function ($data) {
                /* @var $data EventRecord */
                return $data->statusReplacement;
            },
        ],
        ['name' => 'is_drop_shipping', 'type' => 'boolean', 'filter' => $this->app()->format->booleanFormat, 'headerHtmlOptions' => ['class' => 'span1']],
        ['name' => 'start_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'end_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'mailing_start_at', 'type' => 'date', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'name' => 'productsSoldRealNumber',
            'type' => 'raw',
            'headerHtmlOptions' => ['class' => 'span2'],
            'filter' => false,
            'value' => function ($data) {
                /* @var $data EventRecord */
                $sold = Cast::toStr($data->productsSoldNumber);
                $soldReal = Cast::toStr($data->productsSoldRealNumber);
                if ($sold == $soldReal || $data->is_stock) {
                    return $soldReal;
                } else {
                    return $soldReal . ' (' . CHtml::tag('strong', ['title' => $this->t('From the current flash-sale')], $sold) . ')';
                }
            },
        ],
        ['name' => 'is_deleted', 'type' => 'boolean', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
            /*'buttons' => array(
                'refresh' => array(
                    'icon' => 'refresh',
                    'options' => array('target' => '', 'title' => 'Update the number of items sold'),
                    'url' => function($data) {
                        return $this->app()->createUrl('event/refreshSoldNumber', array('id' => $data->id));
                    }
                )
            )*/
        ],
    ],
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['event/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
