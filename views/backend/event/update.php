<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\YiiComponent\Utf8;

/**
 * @var $this BackController
 * @var $form TbForm
 * @var $model EventRecord
 * @var $productsProvider CActiveDataProvider
 * @var $assortmentUploadForm TbForm
 * @var $stockAssortmentUploadForm TbForm
 * @var $requestUploadForm TbForm
 * @var $productImageZipForm TbForm
 */

$this->pageTitle = $this->t('Flash-sales');
$request = $this->app()->request;
$productSectionList = ProductSectionRecord::getGroupList();
$productColorCodes = ProductColorCodes::instance()->getLabels();
ini_set('memory_limit', '200M');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Current Information'),
    'headerIcon' => 'icon-add',
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        'is_stock:boolean',
        'is_charity:boolean',
        'is_drop_shipping:boolean',
        'can_prepay:boolean',
        'supplier.name',
        [
            'name' => 'brand_manager_id',
            'type' => 'text',
            'value' => $model->brandManager
                ? "{$model->brandManager->login} ({$model->brandManager->fullname})" : $this->t('no'),
        ],
        'name',
        'description:html',
        'description_short',
        'description_mailing:html',
        'productCount',
        [
            'name' => 'logo',
            'type' => 'html',
            'value' => !$model->logo->isEmpty ? CHtml::image($model->logo->getThumbnail('mid320')->url) : '',
        ],
        [
            'name' => 'promo',
            'type' => 'html',
            'value' => !$model->promo->isEmpty ? CHtml::image($model->promo->getThumbnail('mid320promo')->url) : '',
        ],
        [
            'name' => 'distribution',
            'type' => 'html',
            'value' => !$model->distribution->isEmpty ? CHtml::image($model->distribution->getThumbnail('mid260promo')->url) : '',
        ],
        [
            'name' => 'sizeChart',
            'type' => 'html',
            'value' => !$model->sizeChart->isEmpty ? CHtml::image($model->sizeChart->getThumbnail('mid380')->url, '', ['style' => 'height: 200px;']) : $this->t('Not specified'),
        ],
        [
            'name' => 'frontendUrl',
            'label' => $this->t('On website'),
            'type' => 'raw',
            'value' => CHtml::link($this->t('On website'), $this->app()->frontendUrlManager->createUrl('event/index', ['id' => $model->id]), ['target' => '_blank']),
        ],
    ],
]); ?>
<?php $this->endWidget() ?>

<?php
if (!$model->is_virtual) {
    $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
        'title' => $this->t('Edit flash-sale'),
        'headerIcon' => 'icon-add',
    ]);

    echo $form->render();

    $this->endWidget();
}
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Items inventory'),
    'headerIcon' => 'icon-add',
]); ?>
<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $productsProvider,
    'filter' => $productsProvider->model,
    'enableSorting' => false,
    'summaryText' => $this->t('Products') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, form select',

    'extraRowColumns' => ['product.name'],

    'rowCssClassExpression' => function ($row, $data) {
        /* @var $data EventProductRecord */
        if (!$data->product->isHasLogo || $data->product->section_id == 0 || $data->product->color_code == 0) {
            return 'error';
        } elseif ($data->product->images_count == 0) {
            return 'info';
        }
    },

    'columns' => [
        [
            'name' => 'product.name',
            'type' => 'raw',
            'value' => function ($data) use ($productSectionList, $productColorCodes) {
                /* @var $data EventProductRecord */
                $result = CHtml::encode($data->product->name) . ', ' . CHtml::encode($data->product->brand->name);
                if (!empty($data->product->color)) {
                    $result .= ', ' . CHtml::encode($data->product->color);
                }

                $editTitle = $this->t('Edit');
                if (!$data->product->isHasLogo) {
                    $editTitle = $this->t('Add logo');
                } elseif ($data->product->images_count == 0) {
                    $editTitle = $this->t('Upload gallery');
                }
                $result .= ', ' . CHtml::link(
                        $editTitle,
                        ['product/update', 'id' => $data->product->id],
                        ['target' => '_blank']
                    );

                $result .= ', ' . CHtml::link(
                        $this->t('On website'),
                        $this->app()->frontendUrlManager->createUrl('product/index', ['id' => $data->product_id, 'eventId' => $data->event_id]),
                        ['target' => '_blank']
                    );
                $request = $this->app()->request;

                $result .= ', ' . $this->widget('bootstrap.widgets.TbEditableField', [
                        'model' => $data,
                        'attribute' => 'product.section_id',
                        'safeOnly' => false,
                        'url' => $this->createUrl("event/setSection"),
                        'source' => $productSectionList,
                        'params' => [
                            $request->csrfTokenName => $request->getCsrfToken(),
                        ],
                        'type' => 'select',
                        'emptytext' => $this->t('Not specified'),
                        'title' => $this->t('Enter the item section'),
                    ], true);

                $result .= ', ' . $this->widget('bootstrap.widgets.TbEditableField', [
                        'model' => $data,
                        'attribute' => 'product.color_code',
                        'safeOnly' => false,
                        'url' => $this->createUrl("event/setColorCode"),
                        'source' => $productColorCodes,
                        'params' => [
                            $request->csrfTokenName => $request->getCsrfToken(),
                        ],
                        'type' => 'select',
                        'emptytext' => $this->t('Not specified'),
                        'title' => $this->t('Enter colour code'),
                    ], true);
                return $result;
            },
            'headerHtmlOptions' => ['class' => 'hide'],
            'htmlOptions' => ['class' => 'hide'],
        ],
        [
            'name' => 'id',
            'headerHtmlOptions' => ['class' => 'span2'],
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data EventProductRecord */
                $result = $data->id;

                return CHtml::link($result, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProduct',
                        ['product' => $data],
                        true
                    ),
                ]);
            },
        ],
        'article',

        ['name' => 'price_purchase', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span1'],
            'value' => function ($data) {
                return (int)$data->price_purchase;
            }],
        ['name' => 'price_market', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span1'],
            'value' => function ($data) {
                return (int)$data->price_market;
            }],
        ['name' => 'price', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span1'],
            'value' => function ($data) {
                return (int)$data->price;
            }],

        [
            'name' => 'number',
            'header' => $this->t('Qty'),
            'visible' => !$model->is_stock,
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'htmlOptions' => ['class' => 'span1'],
            'filter' => false,
            'editable' => [
                'safeOnly' => false,
                'url' => ['event/setReservedNumber'],
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'text',
                'emptytext' => $this->t('No'),
                'title' => $this->t('Enter the number'),
            ],
        ],
        [
            'name' => 'number_arrived',
            'visible' => !$model->is_stock,
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span1'],
        ],
        ['name' => 'number_sold', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span1']],

        ['name' => 'ageRangeReplacement', 'filter' => false],

        ['name' => 'sizeformatReplacement', 'filter' => false],
        'size',

        'made_in',

        [
            'name' => 'weight',
            'filter' => false,
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data EventProductRecord */
                $arr = [];
                if ($data->weight != '') {
                    $arr[] = $data->weight;
                }
                if ($data->dimensions != '') {
                    $arr[] = $data->dimensions;
                }
                return count($arr) == 2 ? implode(' / ', $arr) : implode('', $arr);
            },
        ],
        [
            'header' => $this->t('Stock product'),
            'filter' => false,
            'type' => 'raw',
            'value' => function ($data) {
                /** @var EventProductRecord $data */
                return EventProductRecord::STATUS_STOCK === $data->status ? 'Yes' : 'No';
            },
        ],

        'created_at:humanTime',
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
            'buttons' => [
                'update' => [
                    'url' => function ($data) {
                        return $this->app()->createUrl('eventProduct/update', ['id' => $data->id]);
                    },
                ],
            ],
        ],
    ],
]); ?>

<?php if (!$model->isStatusEmpty && !$model->is_stock && !$model->is_drop_shipping): ?>
    <div class="pull-right span6">
        <div class="well">
            <?php
            $items = [];
            foreach ($model->productSuppliers as $supplier) {
                $items[] = '---';
                $items[] = [
                    'label' => Utf8::ucfirst($supplier->name) . ' - ' . $this->t('All items'),
                    'url' => ['eventProduct/downloadProductRequest', 'id' => $model->id, 'supplierId' => $supplier->id],
                ];
                $items[] = [
                    'label' => Utf8::ucfirst($supplier->name) . ' - ' . $this->t('Only with sales or supplies'),
                    'url' => ['eventProduct/downloadProductRequest', 'id' => $model->id, 'supplierId' => $supplier->id, 'addZeroSoldItems' => false],
                ];
            }
            ?>
            <?php $this->widget('bootstrap.widgets.TbButtonGroup', [
                'buttons' => [
                    [
                        'type' => 'info',
                        'label' => $this->t('Download order table'),
                        'dropdownOptions' => [
                            'style' => 'right: 0; left: auto;',
                        ],
                        'items' => array_merge([
                            [
                                'label' => $this->t('All items'),
                                'url' => ['eventProduct/downloadProductRequest', 'id' => $model->id],
                            ],
                            [
                                'label' => $this->t('Only with sales or supplies'),
                                'url' => ['eventProduct/downloadProductRequest', 'id' => $model->id, 'addZeroSoldItems' => false],
                            ],
                        ], $items),
                    ],
                ],
            ]) ?>
            <div>&nbsp;</div>
            <div><?= $this->t('Upload order table') ?></div>
            <?= $requestUploadForm->render() ?>
        </div>
    </div>
    <div class="clearfix"></div>
<?php endif ?>

<div class="pull-right span6">
    <div class="well">
        <?php if ($model->isStatusEmpty): ?>
            <div><?= $this->t('Upload Preliminary Specification') ?>.</div>
        <?= $assortmentUploadForm->render() ?>
        <?php else: ?>
        <?php if (!$model->is_stock): ?>
            <?= CHtml::link($this->t('Download Preliminary Specification'), ['eventProduct/downloadAssortment', 'id' => $model->id], [
                'class' => 'btn btn-info',
            ]) ?>
        <?php endif ?>
            <div>&nbsp;</div>
            <a href="#" class="btn btn-danger"
               id="uploadOther"><?= $this->t('Upload other Preliminary Specification') ?></a>
            <div class="hide" id="uploadOtherPanel">
                <?= $this->t('Upload other Preliminary Specification') ?>
                <div>
                    <b><?= $this->t('Items from virtual warehouse and zero orders will be deleted') ?></b>
                </div>
                <?= $assortmentUploadForm->render() ?>
            </div>
            <script>
                $(function () {
                    $('#uploadOther').on('click', function (e) {
                        e.preventDefault();
                        $(this).addClass('hide');
                        $('#uploadOtherPanel').removeClass('hide');
                    });
                })
            </script>
        <?php endif; ?>
    </div>
</div>
<div class="clearfix"></div>

<?php if ($model->is_stock): ?>

    <div class="pull-right span6">
        <div class="well">
            <?php if ($model->isStatusEmpty): ?>
                <div><?= $this->t('Upload Preliminary Specification') ?>.</div>
            <?= $stockAssortmentUploadForm->render() ?>
            <?php else: ?>
                <div>&nbsp;</div>
                <a href="#" class="btn btn-danger"
                   id="uploadStockOther"><?= $this->t('Upload stock Preliminary Specification') ?></a>
                <div class="hide" id="uploadStockOtherPanel">
                    <?= $this->t('Upload stock Preliminary Specification') ?>
                    <div>
                        <b><?= $this->t('Items from virtual warehouse and zero orders will be deleted') ?></b>
                    </div>
                    <?= $stockAssortmentUploadForm->render() ?>
                </div>
                <script>
                    $(function () {
                        $('#uploadStockOther').on('click', function (e) {
                            e.preventDefault();
                            $(this).addClass('hide');
                            $('#uploadStockOtherPanel').removeClass('hide');
                        });
                    })
                </script>
            <?php endif; ?>
        </div>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>

<div class="pull-right span6">
    <div class="well">
        <?php if (!$model->isStatusEmpty): ?>
            <a href="#" class="btn btn-warning" id="uploadZip"><?= $this->t('Upload .zip images') ?></a>
            <div class="hide" id="uploadZipPanel">
                <?= $this->t('Loading image packs') ?>
                <div id="current-zip-views"></div>
                <?= $productImageZipForm->render() ?>
            </div>
            <script>
                $(function () {
                    $('#uploadZip').on('click', function (e) {
                        e.preventDefault();
                        $(this).addClass('hide');
                        $('#uploadZipPanel').removeClass('hide');
                    });
                })
            </script>
        <?php endif; ?>
    </div>
</div>
<div class="clearfix"></div>

<?php if ($model->is_virtual): ?>
    <div class="pull-right span6">
        <div class="well">
            <?= CHtml::link($this->t('Ready for warehouse'), ['event/readyForWarehouse', 'id' => $model->id], [
                'class' => 'btn btn-primary',
            ]) ?>
        </div>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>

<?php $this->endWidget() ?>

<?php
/**--- Обрезка фото для рассылок  ---*/
$inputId = CHtml::activeId($form->model, 'distribution');

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerCoreScript('jcrop');

$cs->registerScript('distributionAreaSelect', "
$('#$inputId').on('change', function() {
    var url = URL.createObjectURL(this.files[0]);
    var img = $('#distribution-preview-resize');
    var imgMini = $('#distribution-preview-mini');
    var inputResize = $('#distribution-resize')[0];
    inputResize.value = '{}';
    var minSize = [678, 392];

    img[0].src = imgMini[0].src = url;

   // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        preview = $('#preview-pane'),
        pcnt = $('#preview-pane .preview-container'),
        pimg = $(imgMini),

        xsize = pcnt.width(),
        ysize = pcnt.height();

    img.Jcrop({
      onChange: updatePreview,
      onSelect: saveSize,
      aspectRatio: xsize / ysize,
      minSize: minSize
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];

      this.setClass('mgauto');
      this.setSelect([0, 0, minSize[0], minSize[1]]);

      // Store the API in the jcrop_api variable
      jcrop_api = this;
    });

    function updatePreview(c)
    {
      if (parseInt(c.w) < 0)
      {
        return;
      }

      var rx = xsize / c.w;
      var ry = ysize / c.h;

      pimg.css({
        width: Math.round(rx * boundx) + 'px',
        height: Math.round(ry * boundy) + 'px',
        marginLeft: '-' + Math.round(rx * c.x) + 'px',
        marginTop: '-' + Math.round(ry * c.y) + 'px'
      });
    };

    function saveSize(size) {
        inputResize.value = JSON.stringify(size);
    };

     $('#distribution-area-select').modal('show').on('hide', function() {
        jcrop_api.destroy();
    });
});
");
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', [
    'id' => 'distribution-area-select',
    'htmlOptions' => [
        'style' => 'top: 25px; left: 25px; right: 25px; margin-left: 0;  width: initial; ',
    ],
]) ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><?= $this->t('Preview') ?></h4>
    <div id="preview-pane">
        <!--    Предполагаемое соотношение сторон     width / height -->
        <div class="preview-container mgauto" style="width: 263px; height: 152px; overflow: hidden;">
            <img id="distribution-preview-mini"/>
        </div>
    </div>
</div>
<div class="modal-body" style="max-height: 600px;">
    <h4><?= $this->t('Select area') ?></h4>
    <img id="distribution-preview-resize"/>
</div>
<?php $this->endWidget() ?>
<?php /**--- Обрезка фото для рассылок  ---*/ ?>
