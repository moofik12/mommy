<?php

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
$this->pageTitle = $this->t('Flash-sales');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Add flash-sale'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>

<?php $this->endWidget() ?>

<?php
/**--- Обрезка фото для рассылок  ---*/
$inputId = CHtml::activeId($form->model, 'distribution');

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerCoreScript('jcrop');

$cs->registerScript('distributionAreaSelect', "
$('#$inputId').on('change', function() {
    var url = URL.createObjectURL(this.files[0]);
    var img = $('#distribution-preview-resize');
    var imgMini = $('#distribution-preview-mini');
    var inputResize = $('#distribution-resize')[0];
    inputResize.value = '{}';
    var minSize = [678, 392];

    img[0].src = imgMini[0].src = url;

   // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        preview = $('#preview-pane'),
        pcnt = $('#preview-pane .preview-container'),
        pimg = $(imgMini),

        xsize = pcnt.width(),
        ysize = pcnt.height();

    img.Jcrop({
      onChange: updatePreview,
      onSelect: saveSize,
      aspectRatio: xsize / ysize,
      minSize: minSize
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];

      this.setClass('mgauto');
      this.setSelect([0, 0, minSize[0], minSize[1]]);

      // Store the API in the jcrop_api variable
      jcrop_api = this;
    });

    function updatePreview(c)
    {
      if (parseInt(c.w) < 0)
      {
        return;
      }

      var rx = xsize / c.w;
      var ry = ysize / c.h;

      pimg.css({
        width: Math.round(rx * boundx) + 'px',
        height: Math.round(ry * boundy) + 'px',
        marginLeft: '-' + Math.round(rx * c.x) + 'px',
        marginTop: '-' + Math.round(ry * c.y) + 'px'
      });
    };

    function saveSize(size) {
        inputResize.value = JSON.stringify(size);
    };

     $('#distribution-area-select').modal('show').on('hide', function() {
        jcrop_api.destroy();
    });
});
");
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', [
    'id' => 'distribution-area-select',
    'htmlOptions' => [
        'style' => 'top: 25px; left: 25px; right: 25px; margin-left: 0;  width: initial; ',
    ],
]) ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4><?= $this->t('Preview') ?></h4>
    <div id="preview-pane">
        <!--    Предполагаемое соотношение сторон     width / height -->
        <div class="preview-container mgauto" style="width: 263px; height: 152px; overflow: hidden;">
            <img id="distribution-preview-mini"/>
        </div>
    </div>
</div>
<div class="modal-body" style="max-height: 600px;">
    <h4><?= $this->t('Select area') ?></h4>
    <img id="distribution-preview-resize"/>
</div>
<?php $this->endWidget() ?>
<?php /**--- Обрезка фото для рассылок  ---*/ ?>
