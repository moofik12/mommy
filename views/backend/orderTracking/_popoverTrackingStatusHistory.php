<?php

use MommyCom\Model\Db\OrderTrackingRecord;

/**
 * @var $history array
 * @var $this CController
 */
?>
<table class="items table table-striped table-bordered table-hover">
    <th style="text-align: center">Дата</th>
    <th style="text-align: center">Статус</th>
<?php
    foreach ($history as $time => $data) {
        $status = isset($data['status']) ? CHtml::value(OrderTrackingRecord::statusReplacements(), $data['status']) : '';

        echo '<tr>';
        echo '<td>'  . $this->app()->dateFormatter->formatDateTime($time, 'medium', 'short') . '</td>';
        echo  '<td>' . $status . '</td>';
        echo '</tr>';
    }
?>
</table>
