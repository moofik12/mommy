<?php

use MommyCom\Model\Db\OrderTrackingRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderTrackingRecord
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'filterForm',
    'htmlOptions'=>array('class'=>'well'),
    'type' => 'search',
));

echo CHtml::textField('filterEventId', $this->app()->request->getParam('filterEventId', false), array('placeholder' => $this->t('Flash-sale')));

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', array('beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'), array('class' => 'btn btn-primary'));

$this->endWidget();
