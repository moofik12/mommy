<?php

use MommyCom\Controller\Backend\OrderTrackingController;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderTrackingRecord;
use MommyCom\Service\Delivery\AvailableDeliveries;

/**
 * @var OrderTrackingController $this
 * @var CActiveDataProvider $provider
 * @var AvailableDeliveries $availableDeliveries
 */

$this->pageTitle = $this->t('Order tracking');
$request = $this->app()->request;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Orders'),
    'headerIcon' => 'icon-edit',
]); ?>

<?php $this->renderPartial('_filter', ['model' => $provider->model]); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Orders {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filterForm :input',
    'id' => 'orders',
    'columns' => [
        [
            'name' => 'order_id',
            'htmlOptions' => ['class' => 'span2'],
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data OrderTrackingRecord */
                return CHtml::link($data->order_id, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverOrder',
                        ['order' => $data->order],
                        true
                    ),
                ]);
            },
        ],
        ['name' => 'order_delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderTrackingRecord */
            return $data->orderDeliveryTypeReplacement;
        }],

        ['name' => 'status', 'type' => 'raw', 'filter' => OrderTrackingRecord::statusReplacements(), 'value' => function ($data) {
            /* @var $data OrderTrackingRecord */
            return CHtml::link($data->statusReplacement, 'javascript:void(0)', [
                'data-toggle' => 'popover',
                'data-placement' => 'right',
                'data-trigger' => 'hover',
                'data-html' => 'true',
                'data-content' => $this->renderPartial(
                    '_popoverTrackingStatusHistory',
                    ['history' => $data->statusHistory],
                    true
                ),
            ]);
        }],

        [
            'name' => 'trackcode',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'htmlOptions' => ['class' => 'span3'],
            'editable' => [
                'url' => ['orderTracking/changeTrackcode'],
                'safeOnly' => false,
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'text',
                'emptytext' => $this->t('Add tracking number'),
                'title' => $this->t('Enter tracking number'),
            ],
        ],

        ['name' => 'trackcode_return', 'htmlOptions' => ['class' => 'span2']],

        ['name' => 'order.price_total', 'type' => 'number', 'filter' => false],
        ['name' => 'order.delivery_price', 'type' => 'number', 'filter' => false],
        ['name' => 'delivery_price', 'type' => 'number', 'filter' => false],
        ['name' => 'deadline_for_delivery', 'type' => 'datetime', 'value' => function (OrderTrackingRecord $record) {
            return $record->deadline_for_delivery ?: null;
        }],
        ['name' => 'delivered_date', 'type' => 'datetime', 'value' => function (OrderTrackingRecord $record) {
            return $record->delivered_date ?: null;
        }],
        ['name' => 'created_at', 'type' => 'datetime'],
        [
            'name' => 'synchronized_at',
            'filter' => false,
            'value' => function (OrderTrackingRecord $record) {
                if ($record->synchronized_at > 0) {
                    return $this->app()->dateFormatter->formatDateTime($record->synchronized_at);
                }

                return $this->t('no');
            },
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
