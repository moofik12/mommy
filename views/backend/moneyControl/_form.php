<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Backend\MoneyControlForm;
use MommyCom\Model\Backend\MoneyControlItemsFromFileForm;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this MoneyControlController
 * @var $provider CActiveDataProvider
 * @var $form TbForm
 * @var $formItem TbForm
 * @var $formFile TbForm
 */

/** @var MoneyControlForm $model */
$model = $form->model;
$modelName = get_class($model);
if (false !== strpos($modelName, '\\')) {
    $modelName = substr($modelName, strpos($modelName, '\\') + 1);
}
?>

<div class="well">
    <?= $formItem->render() ?>
</div>
<div class="well">
    <h4><?= $this->t('Add data from the settlement account statement or from New Post') ?></h4>
    <div class="controls alert item-form">
        <?= $this->t('All supported formats') ?>:
        <ul>
            <li><strong>*.<?=MoneyControlItemsFromFileForm::FILE_CSV?>:</strong>
                <?= $this->t('field divider') ?> - <?=MoneyControlItemsFromFileForm::DELIMITER?>,
                <?= $this->t('text divider') ?> - <?=MoneyControlItemsFromFileForm::ENCLOSURE?>.<br>
                <?= $this->t('When selecting this file type, specify the column number.') ?>
            </li>
            <li><strong>*.<?=MoneyControlItemsFromFileForm::FILE_PDF?>:</strong>
                <?= $this->t('Tracking number list through Enter') ?>
            </li>
            <li><strong>*.<?=MoneyControlItemsFromFileForm::FILE_TXT?>:</strong>
                <?= $this->t('Tracking number list through Enter') ?>
            </li>
        </ul>

    </div>
    <?= $formFile->render() ?>
</div>

<?= $form->renderBegin() ?>

<?php foreach($model->items as $orderId => $trackcode) : ?>
    <?php
        /** @var OrderRecord[] $orders */
        $orders = $model->getOrders();
        $order = isset($orders[$orderId]) ? $orders[$orderId] : null;
        $price = $order ? $order->getPayPrice() : 0;
    ?>
    <div class="controls alert item-form">
        <?= CHtml::hiddenField($modelName . "[items][$orderId]", $trackcode, array('data-price' => $price)) ?>
        <strong>Заказ:</strong> <?= $orderId ?> / <strong><?= $this->t('Tracking number') ?></strong> <?= $trackcode ?> / <strong><?= $this->t('Amount') ?>:</strong> <?= $price ?>
        <button class="btn btn-danger btn-small pull-right delete-item" type="button"><?= $this->t('Delete') ?></button>
    </div>

<?php endforeach; ?>

<?= $form['file'] ?>
<?= $form['moneyExpected'] ?>
<?= $form['moneyReal'] ?>
<?= $form->renderButtons() ?>
<?= $form->renderEnd() ?>
