<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\MoneyControlRecord;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this MoneyControlController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Monitoring order payments');
$request = $this->app()->request;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Order monitoring'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'filterSelector' => '{filter}, #filter-form select, #filter-form input',
    'rowCssClassExpression' => function ($row, $data) {
        /** @var MoneyControlRecord $data */
        $cssClasses = [
            MoneyControlRecord::STATUS_ERROR_TRACKCODE => 'error',
            MoneyControlRecord::STATUS_CONFIRMATION => 'success',
        ];

        return CHtml::value($cssClasses, $data->status, '');
    },
    'columns' => [
        ['name' => 'id', 'header' => '#', 'htmlOptions' => ['class' => 'span1']],
        [
            'name' => 'delivery_type',
            'filter' => OrderRecord::deliveryTypeReplacements(),
            'value' => function ($data) {
                /** @var MoneyControlRecord $data */
                return CHtml::value(OrderRecord::deliveryTypeReplacements(), $data->delivery_type);
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'status',
            'filter' => MoneyControlRecord::statusReplacements(),
            'value' => function ($data) {
                /** @var MoneyControlRecord $data */
                return $data->statusReplacement();
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        ['name' => 'trackcode'],
        ['name' => 'order_id'],
        ['name' => 'order_price'],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span3']],
        ['name' => 'updated_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span3']],
    ],
    'extendedSummary' => [
        'title' => $this->t('Total'),
        'columns' => [
            'order_price' => [
                'label' => $this->t('Amount in orders shown'),
                'class' => 'TbSumOperation'
            ],
        ],
    ],
    'extendedSummaryOptions' => [
        'class' => 'well pull-right',
        'style' => 'width:300px',
    ],
]); ?>

<?php $this->endWidget() ?>
