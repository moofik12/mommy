<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 * @var $this MoneyControlController
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

$this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'rangeData',
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period')],
]);

echo CHtml::telField('orderID', $this->app()->request->getParam('orderID', false), ['autocomplete' => 'off', 'placeholder' => $this->t('Order №')]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
