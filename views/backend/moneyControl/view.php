<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\MoneyReceiveStatementRecord;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this MoneyControlController
 * @var $model MoneyReceiveStatementRecord
 */

$this->pageTitle = $this->t('Editing an Act');
$request = $this->app()->request;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t("View act"),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
    'type' => 'link',
    'label' => $this->t('Back to the list'),
    'url' => $this->app()->controller->createUrl('index'),
]) ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        [
            'name' => 'delivery_type',
            'value' => CHtml::value(OrderRecord::deliveryTypeReplacements(), $model->delivery_type, ''),
        ],
        [
            'name' => 'user_id',
            'value' => $model->user ? $model->user->displayName : $model->user_id,
        ],
        'money_expected',
        'money_real',
        [
            'name' => 'updated_at',
            'value' => $this->app()->dateFormatter->formatDateTime($model->updated_at),
        ],
        [
            'name' => 'created_at',
            'value' => $this->app()->dateFormatter->formatDateTime($model->created_at),
        ],
    ],
]); ?>

<h3> <?= $this->t('Orders') ?> </h3>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'header' => '#', 'htmlOptions' => ['class' => 'span1']],
        [
            'name' => 'order_id',
        ],
        [
            'name' => 'trackcode',
        ],
        [
            'name' => 'order_price',
        ],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span3']],
        ['name' => 'updated_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span3']],
    ],
]); ?>
<?php $this->endWidget() ?>
