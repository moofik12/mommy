<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\MoneyReceiveStatementRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Deprecated\Delivery\DeliveryTypeUa;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this MoneyControlController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Monitoring order payments');
$request = $this->app()->request;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Acts'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter') ?>

<div class="pull-right">
    <?= CHtml::link($this->t('Create an Act for shipping via Courier'), ['create', 'delivery' => DeliveryTypeUa::DELIVERY_TYPE_COURIER], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Acts {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filter-form select, #filter-form input',
    'rowCssClassExpression' => function ($row, $data) {
        /** @var MoneyReceiveStatementRecord $data */
        $cssClass = '';
        if ($data->money_real < $data->money_expected) {
            $cssClass = 'error';
        } elseif ($data->money_real > $data->money_expected) {
            $cssClass = 'info';
        }

        return $cssClass;
    },
    'columns' => [
        ['name' => 'id', 'header' => '#', 'htmlOptions' => ['class' => 'span1']],
        [
            'name' => 'delivery_type',
            'filter' => OrderRecord::deliveryTypeReplacements(),
            'value' => function ($data) {
                /** @var MoneyReceiveStatementRecord $data */
                return CHtml::value(OrderRecord::deliveryTypeReplacements(), $data->delivery_type);
            },
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'user_id',
            'value' => function ($data) {
                /** @var  MoneyReceiveStatementRecord $data */
                $user = $data->user;
                return $user ? "{$user->login} (#{$user->id})" : '';
            },
            'htmlOptions' => ['class' => 'span3'],
        ],
        ['name' => 'money_expected'],
        ['name' => 'money_real'],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span3']],
        ['name' => 'updated_at', 'type' => 'datetime', 'filter' => false, 'htmlOptions' => ['class' => 'span3']],
        [
            'class' => 'CLinkColumn',
            'htmlOptions' => ['target' => '_blank'],
            'header' => $this->t('View'),
            'label' => $this->t('View'),
            'urlExpression' => function ($data) {
                /* @var $data OrderRecord */
                return $this->app()->createUrl('moneyControl/view', ['id' => $data->id]);
            },
        ],
        [
            'class' => ButtonColumn::class,
            'buttons' => [
                'update' => [
                    'visible' => function ($row, $data) {
                        /** @var MoneyReceiveStatementRecord $data */
                        return $data->isAvailableEditing();
                    },
                ],
                'delete' => [
                    'visible' => function ($row, $data) {
                        /** @var MoneyReceiveStatementRecord $data */
                        return $data->isAvailableEditing();
                    },
                ],
            ],
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
