<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this MoneyControlController
 * @var $provider CActiveDataProvider
 * @var $form TbForm
 * @var $formItem TbForm
 * @var $formFile TbForm
 */

$this->pageTitle = $this->t('Create Act');
$request = $this->app()->request;
$deliveryReplacement = CHtml::encode(CHtml::value(OrderRecord::deliveryTypeReplacements(), $form->model->deliveryType, ''));
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Create act for') . ' '. $deliveryReplacement,
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_form', ['form' => $form, 'formItem' => $formItem, 'formFile' => $formFile]) ?>

<?php $this->endWidget() ?>
