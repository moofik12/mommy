<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\WarehouseStatementReturnProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Format;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model WarehouseStatementReturnRecord */
/* @var $productsProvider CActiveDataProvider */
/* @var $form TbForm */

$this->pageTitle = $this->t('Editing Return to supplier form');
$df = $this->app()->dateFormatter;
/* @var Format $format */
$format = $this->app()->format;
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Editing a form') . ' №' . $model->id,
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?= CHtml::link($this->t('List of forms'), ['index', 'id' => $model->id], [
    'class' => 'btn btn-link',
]) ?>


<?= $form->renderBegin() ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        [
            'label' => $this->t('Supplier'),
            'value' => $model->supplier ? $model->supplier->name . " ({$model->supplier_id})" : $model->supplier_id,
        ],
        [
            'label' => $model->getAttributeLabel('supplier.phone'),
            'value' => $model->supplier ? $model->supplier->phone : $this->t('no'),
        ],
        [
            'label' => $model->getAttributeLabel('supplier.email'),
            'value' => $model->supplier ? $model->supplier->email : $this->t('no'),
        ],
        [
            'type' => 'raw',
            'label' => $this->t('System'),
            'value' => $form->renderElement('is_need_notify'),
        ],
        [
            'type' => 'raw',
            'label' => $model->getAttributeLabel('notify_text'),
            'value' => $form['notify_text']->renderInput(),
        ],
        [
            'label' => $this->t('Status'),
            'value' => $model->statusReplacement,
        ],
        [
            'label' => $this->t('Updated'),
            'value' => $df->formatDateTime($model->updated_at),
        ],
        [
            'label' => $this->t('Created at'),
            'value' => $df->formatDateTime($model->created_at),
        ],

    ],
]) ?>
<?= $form->renderButtons() ?>
<?= $form->renderEnd() ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $productsProvider,
    'summaryText' => $this->t('Documents') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, #statement-filter-form',
    'columns' => [
        [
            'type' => 'raw',
            'name' => 'product_id',
            'value' => function ($data) {
                /* @var WarehouseStatementReturnProductRecord $data */
                $value = $data->product_id;

                if ($data->eventProduct) {
                    $value = CHtml::link($data->product_id, '', [
                        'name' => "product-{$data->eventProduct->id}",
                        'data-toggle' => 'popover',
                        'data-trigger' => 'hover',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $data->eventProduct->product->name,
                        'data-content' => $this->renderPartial('_productPopover',
                            ['eventProduct' => $data->eventProduct], true),
                    ]);
                }
                return $value;
            },
        ],
        [
            'name' => 'event_id',
        ],
        [
            'name' => 'event_product_id',
        ],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
            'deleteButtonUrl' => '\Yii::app()->controller->createUrl("deletePosition", array("id" => ' . $model->id . ', "position" => $data->id))',
        ],
    ],
]);
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'changeForm',
    'type' => 'search',
    'method' => 'POST',
]); /* @var $form TbActiveForm */ ?>
<?= $form->errorSummary($model) ?>

<div class="form-actions">
    <div class="pull-left">
        <?php
        echo CHtml::link($this->t('Printing form'), ['labelPrinter/warehouseStatementReturn', 'id' => $model->id], [
            'target' => '_blank',
            'class' => 'btn btn-link',
        ]);

        echo CHtml::link($this->t('Download file'), ['file', 'id' => $model->id], [
            'target' => '_blank',
            'class' => 'btn btn-link',
        ]);
        ?>
    </div>
    <div class="pull-right">
        <?php
        $this->widget('bootstrap.widgets.TbButton', [
            'buttonType' => 'link',
            'type' => 'warning',
            'size' => 'large',
            'label' => $this->t('Send later'),
            'url' => $this->createUrl('index'),
        ]);
        ?>

        <?= CHtml::tag('button', [
            'type' => 'submit',
            'name' => 'submit',
            'value' => 'accept',
            'class' => 'btn btn-success btn-large',
        ], $this->t('Send')) ?>
    </div>

</div>
<?php $this->endWidget() ?>

<?php $this->endWidget() ?>
