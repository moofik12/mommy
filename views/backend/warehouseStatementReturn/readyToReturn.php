<?php

use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('Supplier Return');

$request = $this->app()->request;

$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier Return'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php
//filter
$this->renderPartial('_filterReadyToReturn', ['model' => $provider->model]);
?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Products') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, #statement-filter-form',

    'bulkActions' => [
        'actionButtons' => [
            [
                'id' => 'createSupplierReturnStatement',
                'buttonType' => 'button',
                'type' => 'primary',
                'size' => 'large',
                'label' => $this->t('Create form'),
                'htmlOptions' => [
                    'data-url' => $this->createAbsoluteUrl('warehouseStatementReturn/create'),
                    'data-check-url' => $this->createAbsoluteUrl('warehouseStatementReturn/checkCreate'),
                ],
                'click' => new CJavaScriptExpression('function(values) {
						var products = [];
						var $this = $(this);

                        $(values).each(function() {
                            products.push($(this).val());
                        });

                        $.ajax({
                            "url" : $this.attr("data-check-url"),
                            "type" : "GET",
                            "data" : {"products" : products}
                        })
                        .success(function(data, textStatus, jqXHR) {

                            if (data.errors) {
                                var errors = data.errors;
                                if ($.isArray(errors)) {
                                    errors = errors.join("\n");
                                }

                                alert(errors);
                                return;
                            }

                            var url = $this.attr("data-url");
                            if (url.indexOf("?") == -1) {
                                url += "?";
                            }
                            $(products).each(function($key, $item) {
                                url += "&" + "products[]=" + $item;
                            });
                            window.open(url, "_blank");
                        })
                        .error(function(jqXHR, textStatus, errorThrown) {
                            alert("' . $this->t('Processing error') . '!");
                        });
                    }'),
            ],
        ],
        // if grid doesn't have a checkbox column type, it will attach
        // one and this configuration will be part of it
        'checkBoxColumnConfig' => [
            'name' => 'id',
        ],
    ],

    'columns' => [
        [
            'type' => 'raw',
            'name' => 'id',
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                $value = $data->id;

                if ($data->eventProduct) {
                    $value = CHtml::link($data->id, '', [
                        'name' => "product-{$data->eventProduct->id}",
                        'data-toggle' => 'popover',
                        'data-trigger' => 'hover',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $data->eventProduct->product->name,
                        'data-content' => $this->renderPartial('_productPopover',
                            ['eventProduct' => $data->eventProduct], true),
                    ]);
                }
                return $value;
            },
        ],

        [
            'name' => 'event_product_price',
            'header' => $this->t('Order Total'),
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                return $data->eventProduct->price_purchase;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'event_id',
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'event_product_id',
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'prev_order_id',
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'type' => 'raw',
            'name' => 'status',
            'filter' => WarehouseProductRecord::statusReplacements(),
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                /* @var $data WarehouseProductRecord */
                return CHtml::link($data->statusReplacement, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverWarehouseStatusHistory',
                        ['history' => $data->getStatusHistory()],
                        true
                    ),
                ]);
            },
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'lost_status',
            'filter' => WarehouseProductRecord::lostStatusReplacements(),
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                return $data->getLostStatusReplacement();
            },
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'is_return',
            'type' => 'boolean',
            'filter' => [$this->t('No'), $this->t('Yes')],
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'name' => 'supplier_id',
            'header' => $this->t('Supplier'),
            'filter' => false,
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                $value = $data->supplier_id;

                if ($data->supplier) {
                    $value = $data->supplier->name;
                }

                return $value;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'header' => $this->t('Reference to order'),
            'name' => 'order_id',
            'htmlOptions' => ['class' => 'span2'],
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                return $data->order_id ?: $this->t('no');
            },
        ],

        [
            'header' => $this->t('Added'),
            'name' => 'created_at',
            'type' => 'date',
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'header' => $this->t('Warehouse delivery date'),
            'type' => 'text',
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                $text = $this->t('no');
                $timestamp = $data->getChangeStatusFromHistory(WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED);

                if ($timestamp) {
                    $text = $this->app()->format->formatDate($timestamp);
                }

                return $text;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],

        [
            'class' => 'CLinkColumn',
            'linkHtmlOptions' => ['target' => '_blank'],
            'header' => $this->t('View'),
            'label' => $this->t('View'),
            'urlExpression' => function ($data) {
                /* @var $data WarehouseProductRecord */
                return $this->app()->createUrl('storekeeperOrder/view', ['id' => $data->prev_order_id]);
            },
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
