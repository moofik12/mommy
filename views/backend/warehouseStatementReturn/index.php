<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\WarehouseStatementReturnProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Format;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('Return Documentation');
/* @var Format $format */
$format = $this->app()->format;
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Ret.Doc.'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php
//filter
$this->renderPartial('_filter', ['model' => $provider->model]);

$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Documents') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, #statement-filter-form',
    'rowCssClassExpression' => function ($row, $data) {
        /** @var WarehouseStatementReturnRecord $data */
        $cssClass = '';
        if ($data->status == WarehouseStatementReturnRecord::STATUS_READY) {
            $cssClass = 'warning';
        } elseif ($data->status == WarehouseStatementReturnRecord::STATUS_CONFIRMED && $data->is_accounted_returns) {
            $cssClass = 'success';
        } elseif ($data->status == WarehouseStatementReturnRecord::STATUS_CONFIRMED) {
            $cssClass = 'info';
        }

        return $cssClass;
    },
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'supplier_id',
            'value' => function ($data) {
                /* @var WarehouseStatementReturnRecord $data */
                $text = $data->supplier_id;
                if ($data->supplier) {
                    $text = $data->supplier->name . " (№$text)";
                }
                return $text;
            },
        ],
        [
            'name' => 'positionCount',
            'filter' => false,
            'header' => $this->t('Item quantity'),
            'value' => function ($data) {
                /* @var WarehouseStatementReturnRecord $data */
                return $data->positionCount;
            },
        ],
        [
            'name' => 'positionCount',
            'filter' => false,
            'header' => $this->t('Items total'),
            'value' => function ($data) {
                /* @var WarehouseStatementReturnRecord $data */
                $sum = array_reduce($data->positions, function ($carry, $item) {
                    /* @var  WarehouseStatementReturnProductRecord $item */
                    return $carry = $carry + $item->eventProduct->price_purchase;
                }, 0.0);

                return $sum;
            },
        ],
        [
            'name' => 'status',
            'value' => '$data->statusReplacement',
            'filter' => WarehouseStatementReturnRecord::statuses(),
        ],
        [
            'name' => 'is_need_notify',
            'type' => 'boolean',
            'filter' => $format->booleanFormat,
        ],
        [
            'name' => 'is_notified',
            'type' => 'boolean',
            'filter' => $format->booleanFormat,
        ],
        [
            'class' => 'bootstrap.widgets.TbToggleColumn',
            'name' => 'is_accounted_returns',
            'filter' => [$this->t('No'), $this->t('Yes')],
            'toggleAction' => 'warehouseStatementReturn/toggleAccountedReturns',
            'headerHtmlOptions' => [
                'class' => 'span2',
            ],
            'htmlOptions' => [
                'class' => 'span2',
                'style' => 'text-align: center; vertical-align: middle; font-size: 19px;',
            ],
            'afterToggle' => 'function(success, data){
                if (data && data.error) {
                    alert(data.error);
                }
            }',
        ],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {update}   {delete}',
            'buttons' => [
                'update' => [
                    'visible' => function ($rom, $item) {
                        /* @var WarehouseStatementReturnRecord $item */
                        return $item->status == WarehouseStatementReturnRecord::STATUS_UNCONFIRMED
                            || $item->status == WarehouseStatementReturnRecord::STATUS_READY;
                    },
                ],
                'delete' => [
                    'visible' => function ($rom, $item) {
                        /* @var WarehouseStatementReturnRecord $item */
                        return $item->status == WarehouseStatementReturnRecord::STATUS_UNCONFIRMED
                            || $item->status == WarehouseStatementReturnRecord::STATUS_READY;
                    },
                ],
            ],

        ],
    ],
]);
?>

<?php $this->endWidget() ?>
