<?php

use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnProductRecord;
use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\YiiComponent\Format;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model WarehouseStatementReturnRecord */
/* @var $productsProvider CActiveDataProvider */

$this->pageTitle = $this->t('View Return to supplier form');
$df = $this->app()->dateFormatter;
/* @var Format $format */
$format = $this->app()->format;
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View form') . ' №' . $model->id,
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?= CHtml::link($this->t('List of forms'), ['index', 'id' => $model->id], [
    'class' => 'btn btn-link',
]) ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        [
            'label' => 'Supplier',
            'value' => $model->supplier ? $model->supplier->name : $model->supplier_id,
        ],
        [
            'label' => $model->getAttributeLabel('supplier.phone'),
            'value' => $model->supplier ? $model->supplier->phone : $this->t('no'),
        ],
        [
            'label' => $model->getAttributeLabel('supplier.email'),
            'value' => $model->supplier ? $model->supplier->email : $this->t('no'),
        ],
        [
            'label' => $model->getAttributeLabel('is_need_notify'),
            'value' => $format->formatBoolean($model->is_need_notify),
        ],
        [
            'label' => $model->getAttributeLabel('notify_text'),
            'value' => CHtml::encode($model->notify_text),
        ],
        [
            'label' => $this->t('Status'),
            'value' => $model->statusReplacement,
        ],
        [
            'label' => $this->t('Updated'),
            'value' => $df->formatDateTime($model->updated_at),
        ],
        [
            'label' => $this->t('Created at'),
            'value' => $df->formatDateTime($model->created_at),
        ],
    ],
]) ?>

<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $productsProvider,
    'summaryText' => $this->t('Documents') . ' {start} - {end} ' . $this->t('out of') . ' {count}',
    'filterSelector' => '{filter}, #statement-filter-form',
    'columns' => [
        [
            'type' => 'raw',
            'name' => 'product_id',
            'value' => function ($data) {
                /* @var WarehouseStatementReturnProductRecord $data */
                $value = $data->product_id;

                if ($data->eventProduct) {
                    $value = CHtml::link($data->product_id, '', [
                        'name' => "product-{$data->eventProduct->id}",
                        'data-toggle' => 'popover',
                        'data-trigger' => 'hover',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $data->eventProduct->product->name,
                        'data-content' => $this->renderPartial('_productPopover',
                            ['eventProduct' => $data->eventProduct], true),
                    ]);
                }
                return $value;
            },
        ],
        [
            'name' => 'event_product_price',
            'header' => $this->t('Order Total'),
            'value' => function ($data) {
                /* @var WarehouseProductRecord $data */
                return $data->eventProduct->price_purchase;
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'event_id',
        ],
        [
            'name' => 'event_product_id',
        ],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
    ],
]);
?>

<?php if ($model->status != WarehouseStatementReturnRecord::STATUS_UNCONFIRMED) {
    echo CHtml::link($this->t('Printing form'), ['labelPrinter/warehouseStatementReturn', 'id' => $model->id], [
        'target' => '_blank',
        'class' => 'btn btn-link',
    ]);

    echo CHtml::link($this->t('Download file'), ['file', 'id' => $model->id], [
        'target' => '_blank',
        'class' => 'btn btn-link',
    ]);
}
?>

<?php $this->endWidget() ?>
