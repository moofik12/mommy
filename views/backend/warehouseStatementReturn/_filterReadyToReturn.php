<?php

use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\BackController;

/**
 * @var $this BackController
 * @var $filterForm TbActiveForm
 * @var $model WarehouseStatementReturnRecord
 */

$request = $this->app()->request;
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'statement-filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo CHtml::textField('eventProductPriceFrom', $request->getParam('eventProductPriceFrom', ''), [
    'placeholder' => $this->t('Price from'),
]);

echo CHtml::textField('eventProductPriceTo', $request->getParam('eventProductPriceTo', ''), [
    'placeholder' => $this->t('Price up to'),
]);

$this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'filterSupplierID',
    'asDropDownList' => false,
    'value' => $request->getParam('filterSupplierID', ''),
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Supplier'),
        'allowClear' => true,
        'initSelection' => new CJavaScriptExpression('function (element, callback) {
            $.ajax({
                url: "' . $this->app()->createUrl('supplier/ajaxGetById') . '",
                method: "GET",
                data: {
                    id: parseInt(element.val())
                },
                success: function(supplier) {
                    if (!supplier) {
                        return;
                    }
                    var data = {
                        id: supplier.id,
                        text: supplier.name
                    };
                    callback(data);
                }
            });
        }'),
        'ajax' => [
            'url' => $this->app()->createUrl('supplier/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: value.name,
                    });
                });

                return result;
            }'),
        ],
    ],

]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
