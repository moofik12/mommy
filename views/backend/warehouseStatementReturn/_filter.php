<?php
/**
 * @var $this \MommyCom\Service\BaseController\BackController
 * @var $filterForm TbActiveForm
 * @var $model WarehouseStatementReturnRecord
 */

use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\BackController;

$request = $this->app()->request;
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'statement-filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo CHtml::textField('filterEventID', $request->getParam('filterEventID', ''), ['placeholder' => $this->t('Flash-sale ID')]);

echo CHtml::textField('filterEventProductID', $request->getParam('filterEventProductID', ''), ['placeholder' => $this->t('Product ID in flash-sale')]);

echo CHtml::textField('filterProductID', $request->getParam('filterProductID', ''), ['placeholder' => $this->t('ID at warehouse')]);

$this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'filterSupplierID',
    'asDropDownList' => false,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Supplier'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('supplier/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: value.name,
                    });
                });

                return result;
            }'),
        ],
    ],

]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
