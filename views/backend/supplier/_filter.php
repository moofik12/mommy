<?php
$request = $this->app()->request;
?>

<?php
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo CHtml::dropDownList('hasEnableAccess', $request->getParam('hasEnableAccess', 'any'),
    [
        'any' => $this->t('Admin access'),
        '0' => $this->t('No admin access granted'),
        '1' => $this->t('Admin access granted'),
    ]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
