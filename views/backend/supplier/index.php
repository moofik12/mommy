<?php

use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\YiiComponent\ArrayUtils;

/* @var $this \MommyCom\Service\BaseController\BackController */

$this->pageTitle = $this->t('Suppliers');
//меньше памяти
$contracts = SupplierContractRecord::model()->getSqlCommand()->queryAll();
$contractsFilter = ArrayUtils::getColumn($contracts, 'name', 'id');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier database'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php
//filter
$this->renderPartial('_filter', ['model' => $provider->model]);
?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['supplier/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Suppliers {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filter-form',
    'rowCssClassExpression' => function ($row, $data) {
        /* @var $data SupplierRecord */
        if ($data->is_approved) {
            return 'success';
        }

        return '';
    },
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'contract_id',
            'filter' => $contractsFilter,
            'value' => function ($data) {
                /* @var SupplierRecord $data */
                $text = '<b>НЕТ</b>';
                $contract = $data->contractConnected;
                if ($contract) {
                    $text = $contract->name;
                }

                return $text;
            },
        ],
        ['name' => 'name'],
        ['name' => 'display_name'],
        ['name' => 'adminSupplier.user.login'],
        ['name' => 'phone'],
        [
            'name' => 'is_approved',
            'value' => function($data) {
                return $data->is_approved ? $this->t('Yes') : $this->t('No');
            }
        ],
        ['name' => 'email'],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {update}',
        ],
    ],
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['supplier/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
