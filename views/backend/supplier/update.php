<?php
/* @var $this SupplierController */

use MommyCom\Controller\Backend\SupplierController;
use MommyCom\Model\Db\SupplierRecord;

/* @var $form TbForm */
/* @var $formAccess TbForm */
/* @var $model SupplierRecord */
$this->pageTitle = $this->t('Suppliers');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Edit supplier'),
    'headerIcon' => 'icon-edit',
]);

?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
<script>
    $(document).ready(function () {
        $('[name="createAccount"]').on('click submit', function(e) {
            $.ajax({
                method: "POST",
                url: "<?= $this->app()->createUrl('supplier/createSupplier', ['id' => $model->id]) ?>",
                success: function () {
                    window.location.reload();
                }
            });
        });
    });
</script>
