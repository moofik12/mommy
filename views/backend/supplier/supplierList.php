<?php

use MommyCom\Controller\Backend\SupplierController;
use MommyCom\Model\Db\SupplierContractRecord;
use MommyCom\Model\Db\SupplierRecord;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\YiiComponent\ArrayUtils;

/* @var $this SupplierController */

$this->pageTitle = $this->t('Suppliers');
$controller = $this;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier database'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php
$this->renderPartial('_filter', ['model' => $provider->model]);
?>
<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Suppliers {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        ['name' => 'name'],
        ['name' => 'display_name'],
        ['name' => 'phone'],
        ['name' => 'email'],
        [
            'type' => 'raw',
            'value' => function($data) use ($controller) {
                $url = $controller->createAbsoluteUrl('supplierEvent/flashSales', ['id' => $data->id]);

                return '<a class="btn btn-primary" href="' . $url . '">' . $controller->t('Flash-sales') . '</a>';
            }
        ],
    ],
]);
?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
