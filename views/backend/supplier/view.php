<?php

use MommyCom\Model\Db\SupplierRecord;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model SupplierRecord */
$this->pageTitle = $this->t('View Supplier Data');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Supplier Data') . ': ' . CHtml::encode($model->name),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span1']],
        [
            'name' => 'contract_id',
            'value' => function ($data) {
                /* @var SupplierRecord $data */
                $text = '<b>НЕТ</b>';
                $contract = $data->contractConnected;
                if ($contract) {
                    $text = $contract->name;
                }

                return $text;
            },
        ],
        'name',
        [
            'label' => $this->t('Admin access'),
            'type' => 'boolean',
            'value' => function ($data) {
                /* @var $data SupplierRecord */
                return $data->isHasAccess();
            },
        ],
        'full_name',
        'phone',
        'email',
        [
            'name' => 'is_inform',
            'type' => 'boolean',
            'value' => function ($data) {
                /* @var $data SupplierRecord */
                return $data->isHasAccess();
            },
        ],
        'statusText',
        'display_name',
        'login',
        [
            'name' => 'last_activity_at',
            'type' => 'datetime',
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
        ],
    ],
]); ?>
<?php $this->endWidget() ?>
