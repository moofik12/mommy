<?php

use MommyCom\Controller\Backend\IndexController;
use MommyCom\Model\Db\HelperPageCategoryRecord;

/**
 * @var $this IndexController
 * @var $category HelperPageCategoryRecord
 */
$tabs = [];
$pages = $category->pages;

foreach ($pages as $page) {
    $active = false;

    if ($category->name == 'Сallcenter') {
        if ($page->title == '"Nova Post" checkout') {
            $active = true;
        }
    } else {
        $active = $pages[0] === $page ? true : false;
    }

    $tabs[] = [
        'label' => $page->title,
        'content' => $page->body,
        'active' => $active,
    ];
}

$this->widget(
    'bootstrap.widgets.TbTabs',
    [
        'type' => 'tabs',
        'placement' => 'left',
        'tabs' => $tabs,
    ]
);
