<?php

use MommyCom\Controller\Backend\IndexController;
use MommyCom\Model\Db\HelperPageCategoryRecord;

/**
 * @var $this IndexController
 * @var $categories HelperPageCategoryRecord[]
 * @var boolean $isSupplier
 */

$this->pageTitle = $this->t('Main page');

$tabs = [];

if (!$isSupplier) {
    foreach ($categories as $category) {
        $tabs[] = [
            'label' => '<h5>' . CHtml::encode($category->name) . '</h5>',
            'content' => $this->renderPartial('_helperCategory', ['category' => $category], true),
            'active' => $category->name == $this->t('Callcenter'),
        ];
    };
}
?>

<style>
    ul.nav-tabs > li {
        max-width: 230px;
    }
</style>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Help menu'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<?php $this->widget(
    'bootstrap.widgets.TbTabs',
    [
        'type' => 'tabs',
        'encodeLabel' => false,
        'tabs' => $tabs,
    ]
); ?>

<?php $this->endWidget(); ?>
