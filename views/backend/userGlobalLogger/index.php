<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\UserGlobalLogger\UserGlobalLoggerRecord;

/* @var $this CController */

$this->pageTitle = 'Логирование';
?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('History'),
    'headerIcon' => 'icon-edit',
]);
?>

<?php $this->renderPartial('_filter'); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
        ],
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'header' => '',
            'rowIdColumn' => 'id',
            'url' => $this->createUrl('view'),
            'value' => function ($data) {
                return $this->t('Learn more');
            },
        ],
        [
            'name' => 'action',
            'filter' => UserGlobalLoggerRecord::actions(),
            'value' => function ($data) {
                /* @var UserGlobalLoggerRecord $data */
                return $data->getActionReplacement();
            },
        ],
        [
            'name' => 'controller',
            'filter' => UserGlobalLoggerRecord::controllerDescriptions(),
            'value' => '$data->controllerReplacement',
        ],
        [
            'name' => 'userClassReplacement',
            'filter' => false,
        ],
        [
            'name' => 'user_id',
            'filter' => false,
            'value' => function ($data) {
                /* @var UserGlobalLoggerRecord $data */
                /* @var AdminUserRecord $model */
                $model = $data->getUserModel();
                if ($model instanceof AdminUserRecord) {
                    return "{$model->fullname} ({$model->login})";
                }

                return $data->user_id;
            },
        ],
        [
            'name' => 'class',
            'filter' => UserGlobalLoggerRecord::classDescriptions(),
            'value' => function ($data) {
                /* @var UserGlobalLoggerRecord $data */
                return $data->getClassReplacement();
            },
        ],
        'class_object_id',
        'description',
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view}',
        ],
    ],
]);
?>

<?php $this->endWidget(); ?>
