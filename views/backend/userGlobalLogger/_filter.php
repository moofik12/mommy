<?php
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

$this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'createdAt',
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Period')],
]);

$this->widget('bootstrap.widgets.TbSelect2', [
    'name' => 'userId',
    'asDropDownList' => false,
    'options' => [
        'width' => '250px;',
        'placeholder' => $this->t('Administrator'),
        'allowClear' => true,
        'ajax' => [
            'url' => $this->app()->createUrl('admin/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                return {
                    like: term,
                    page: page
                };
            }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: "<strong>" + value.login + "</strong>"
                            + " <span style=\"color: graytext;\"> (" + value.fullname + ")</span>",
                    });
                });

                return result;
            }'),
        ],
    ],
]);


//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
