<?php

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\UserGlobalLogger\GlobalLoggerAttributeHistory;
use MommyCom\Model\UserGlobalLogger\UserGlobalLoggerRecord;

/* @var $this CController */
/* @var $model UserGlobalLoggerRecord */

$this->pageTitle = $this->t('Logging (view)');
$historyDataProvider = new CArrayDataProvider($model->getAttributesHistory(), ['pagination' => ['pageSize' => 50]]);
//скорее всего таких больших чисел не будет
$numberAfterIsTimestamp = strtotime('01.01.2010');
$df = $this->app()->dateFormatter;
?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('View log'),
    'headerIcon' => 'icon-edit',
]);
?>

<?php
$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'actionReplacement',
        'userClassReplacement',
        [
            'name' => 'user_id',
            'value' => function ($data) {
                /* @var UserGlobalLoggerRecord $data */
                /* @var AdminUserRecord $model */
                $model = $data->getUserModel();
                if ($model instanceof AdminUserRecord) {
                    return "{$model->fullname} ({$model->login})";
                }

                return $data->user_id;
            },
        ],
        'classReplacement',
        'class_object_id',
        [
            'name' => 'created_at',
            'value' => function ($data) use ($df) {
                /* @var UserGlobalLoggerRecord $data */
                return $df->formatDateTime($data->created_at, 'medium', 'long');
            },
        ],
    ],
]);


$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $historyDataProvider,
    'columns' => [
        [
            'name' => 'id',
            'header' => $this->t('Attribute'),
            'value' => function ($data) use ($model) {
                /* @var GlobalLoggerAttributeHistory $data */
                $text = $data->id;
                $model = $model->getModel(true);
                if ($model) {
                    $text = $model->getAttributeLabel($data->id);
                }

                return $text;
            },
        ],
        [
            'name' => 'before',
            'header' => $this->t('Till'),
            'value' => function ($data) use ($numberAfterIsTimestamp) {
                /* @var GlobalLoggerAttributeHistory $data */
                $text = $data->getValue()->before;
                if (is_scalar($text) && $text > $numberAfterIsTimestamp) {
                    $text .= " (" . $this->app()->dateFormatter->formatDateTime($text) . ")";

                } elseif (is_array($text)) {
                    $text = json_encode($text, JSON_UNESCAPED_UNICODE);
                }

                return $text;
            },
        ],
        [
            'name' => 'after',
            'header' => $this->t('After'),
            'value' => function ($data) use ($numberAfterIsTimestamp) {
                /* @var GlobalLoggerAttributeHistory $data */
                $text = $data->getValue()->after;
                if (is_scalar($text) && $text > $numberAfterIsTimestamp) {
                    $text .= " (" . $this->app()->dateFormatter->formatDateTime($text) . ")";

                } elseif (is_array($text)) {
                    $text = json_encode($text, JSON_UNESCAPED_UNICODE);
                }

                return $text;
            },
        ],
    ],
]);
?>
<?php $this->endWidget(); ?>
