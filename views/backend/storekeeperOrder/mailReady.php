<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Product\ProductWeight;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Shipping');

$request = $this->app()->request;

$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Shipping'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<label>
    <div><?= $this->t('Statement №') ?></div>
    <?= CHtml::textField('filterStatementId', $this->app()->request->getParam('filterStatementId', '')) ?>
</label>
<?php $this->endWidget() ?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => 'Orders {start} - {end} out of {count}',
    'filterSelector' => '{filter}, #filterForm input',

    'bulkActions' => [
        'actionButtons' => [
            [
                'id' => 'createCourierStatement',
                'buttonType' => 'button',
                'type' => 'primary',
                'size' => 'medium',
                'label' => $this->t('Create statement of acceptance for courier'),
                'htmlOptions' => ['data-url' => $this->createAbsoluteUrl('courierStatement/add')],
                'click' => new CJavaScriptExpression('function(values) {
                        var $this = $(this);
                        var url = $this.attr("data-url");
                        if (url.indexOf("?") == -1) {
                            url += "?";
                        }
                        $(values).each(function() {
                            url += "&" + "id[]=" + $(this).val();
                        });
                        window.open(url, "_blank");
                    }'),
            ],
        ],
        // if grid doesn't have a checkbox column type, it will attach
        // one and this configuration will be part of it
        'checkBoxColumnConfig' => [
            'name' => 'id',
        ],
    ],

    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span2']],
        ['name' => 'delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderRecord */
            return $data->deliveryTypeReplacement;
        }],
        ['name' => 'positionsCallcenterAcceptedCount', 'filter' => false],
        ['name' => 'price_total', 'type' => 'number', 'filter' => false],
        ['name' => 'weight', 'filter' => false, 'value' => function ($data) {
            /* @var $data OrderRecord */
            return ProductWeight::instance()->convertToString($data->getWeight());
        }],
        ['name' => 'ordered_at', 'type' => 'datetime', 'filter' => false],
        [
            'name' => 'trackcode',
            'htmlOptions' => ['class' => 'span4'],
        ],
        [
            'class' => 'CLinkColumn',
            'htmlOptions' => ['target' => '_blank'],
            'header' => $this->t('View'),
            'label' => $this->t('View'),
            'urlExpression' => function ($data) {
                /* @var $data OrderRecord */
                return $this->app()->createUrl('storekeeperOrder/view', ['id' => $data->id]);
            },
        ],
        [
            'class' => ButtonColumn::class,
            'htmlOptions' => ['class' => 'span5'],
            'header' => $this->t('Actions'),
            'template' => '{backToPackaging} {setAsMailed}',
            'buttons' => [
                'backToPackaging' => [
                    'label' => '<i class="icon-arrow-left"></i> ' . $this->t('Return to packing'),
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('storekeeperOrder/backToPackaging', ['id' => $data->id]);
                    },
                    'options' => [
                        'title' => $this->t('Return to packing'),
                        'class' => 'btn btn-warning span6',
                    ],
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Can not update') . "');
                                }
                            });
                            return false;
                        }"),
                ],
                'setAsMailed' => [
                    'label' => '<i class="icon-ok"></i> ' . $this->t('Send'),
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('storekeeperOrder/setAsMailed', ['id' => $data->id]);
                    },
                    'options' => [
                        'title' => $this->t('Send'),
                        'class' => 'btn btn-success span6',
                    ],
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Can not update') . "');
                                }
                            });
                            return false;
                        }"),
                ],
            ],
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
