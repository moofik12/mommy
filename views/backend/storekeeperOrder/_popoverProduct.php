<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Product\ProductColorCodes;
use MommyCom\Model\Product\ProductTargets;

/**
 * @var $product EventProductRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $product,
    'attributes' => [
        ['name' => 'id'],
        ['name' => 'product_id'],
        [
            'type' => 'raw',
            'name' => 'product.logo_fileid',
            'value' => CHtml::image($product->product->logo->getThumbnail('small70')->url, 'logo', ['class' => 'title']),
        ],
        [
            'name' => 'event_id',
            'type' => 'raw',
            'value' => $product->event_id . ', ' . $product->event->name,
        ],
        'article',
        [
            'name' => 'name',
        ],
        [
            'name' => 'color',
            'type' => 'raw',
            'value' => !empty($product->color)
                ? '<span class="label label-info">' . $product->color . '</span>' : '',
        ],
        [
            'name' => 'color_code',
            'type' => 'raw',
            'value' => !empty($product->product->color_code > 0)
                ? '<span class="label label-info">' . ProductColorCodes::instance()->getLabel($product->product->color_code) . '</span>'
                : $this->t('Missing'),
        ],
        [
            'label' => $this->t('Products section'),
            'name' => 'section_id',
            'type' => 'raw',
            'value' => !empty($product->product->section_id > 0)
                ? '<span class="label label-info">' . ($product->product->section !== null ? $product->product->section->name
                    : $this->t('Not found')) . '</span>'
                : $this->t('Missing'),
        ],
        [
            'name' => 'size',
            'type' => 'raw',
            'value' => !empty($product->size)
                ? '<span class="label label-info">' . $product->size . '</span>' : '',
        ],
        [
            'name' => 'sizeformat',
            'type' => 'raw',
            'value' => !empty($product->sizeformat)
                ? '<span class="label label-info">' . $product->sizeformatReplacement . '</span>' : '',
        ],
        [
            'name' => 'label',
            'type' => 'raw',
            'value' => !empty($product->label)
                ? '<span class="label label-info">' . $product->label . '</span>' : '',
        ],
        'dimensions',
        'weightReplacement',
        'brand.name',
        'category',
        [
            'name' => 'target',
            'value' => implode(', ', ProductTargets::instance()->getLabels($product->target)),
        ],
        'made_in',
        'design_in',
    ],
]);
