<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model EventProductRecord */
/* @var $warehouseProduct WarehouseProductRecord */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Product at warehouse');
$controller = $this;
$request = $this->app()->request;
?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => 'warehouse-switch-' . $model->id,
    'enableSorting' => false,
    'dataProvider' => $provider,

    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'event_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'event_product_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'product_id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'created_at', 'type' => 'datetime', 'htmlOptions' => ['class' => 'span3']],
        [
            'class' => 'CLinkColumn',
            'linkHtmlOptions' => [
                'class' => 'btn btn-success',
                'data-is-switch-btn' => true,
            ],
            'label' => '<i class="icon icon-ok"></i> ' . $this->t('Replace'),
            'urlExpression' => function ($data) use ($controller, $model, $warehouseProduct) {
                /* @var $data WarehouseProductRecord */
                return $controller->createAbsoluteUrl('switchProductWarehouse', [
                    'id' => $warehouseProduct->id,
                    'switchId' => $data->id,
                ]);
            },
        ],
    ],
]) ?>
