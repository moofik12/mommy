<?php

use MommyCom\Service\Order\MommyOrder;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Service\BaseController\BackController;

/**
 * @var \MommyCom\Service\BaseController\BackController $this
 * @var OrderRecord $model
 * @var CActiveDataProvider $positionsProvider
 * @var bool $isFulfilmentDelivery
 * @var bool $fulfilmentStatus
 */

$this->pageTitle = $this->t('Packing details');
$controller = $this;
$app = $this->app();
$request = $app->request;
?>


<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Packing'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="span3">
    <h4><?= $this->t('Main information') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'htmlOptions' => ['class' => 'table-compact'],
        'data' => $model,
        'attributes' => [
            'id',
            'positionsCallcenterAcceptedCount',
            'price:number',
            'ordered_at:datetime',
            'mailAfterString',
            ['name' => 'processing_status', 'value' => $model->processingStatusReplacement],
            ['name' => 'processing_status_prev', 'value' => $model->processingStatusPrevReplacement],
            [
                'name' => 'processingStatusHistory',
                'type' => 'raw',
                'value' => CHtml::link($this->t('History'), 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProcessingHistory',
                        ['history' => $model->getProcessingStatusHistory()],
                        true
                    ),
                ]),
            ],
        ],
    ]) ?>
</div>
<div class="span4">
    <h4><?= $this->t('Delivery') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model,
        'attributes' => [
            'deliveryTypeReplacement',
            'client_name',
            'client_surname',
            'region.name',
            'city.name',
            [
                'name' => $this->t('Nova Post Branch'),
                'type' => 'ntext',
                'value' => '',
            ],
            'zipcode',
            'address',
            'telephone',
        ],
    ]) ?>
</div>
<div class="span4">
    <h4><?= $this->t('Information about the user') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model->user,
        'attributes' => [
            'id',
            ['name' => 'name', 'label' => $this->t('Name')],
            ['name' => 'surname', 'label' => $this->t('Last name')],
            ['name' => 'email'],
            ['name' => 'telephone'],
            ['name' => 'region.name'],
            ['name' => 'city.name'],
            ['name' => 'address'],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'type' => ['striped', 'bordered'],
    'data' => $model,
    'attributes' => [
        'order_comment:ntext',
        'callcenter_comment:ntext',

        [
            'name' => 'storekeeper_comment',
            'type' => 'raw',
            'value' => ($isEditAllowed ? $this->widget('bootstrap.widgets.TbEditableField', [
                'model' => $model,
                'attribute' => 'storekeeper_comment',
                'safeOnly' => false,
                'url' => ['storekeeperOrder/setOrderComment'],
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'textarea',
                'emptytext' => $this->t('No'),
                'title' => $this->t('Enter your comment'),
            ], true) : $model->storekeeper_comment),
        ],
    ],
]) ?>

<div>
    <legend><?= $this->t('Products') ?></legend>

    <?php $this->widget('bootstrap.widgets.TbGroupGridView', [
        'id' => 'packaging-grid',
        'enableSorting' => false,
        'enableHistory' => false,
        'dataProvider' => $positionsProvider,
        'summaryText' => '',

        'extraRowColumns' => ['event.name'],

        'rowCssClassExpression' => function ($row, $data) {
            /* @var $data OrderProductRecord */
            $classes = [
                OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED => '',
                OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED => 'success',
                OrderProductRecord::STOREKEEPER_STATUS_UNAVAILABLE => 'warning',
            ];
            return isset($classes[$data->storekeeper_status]) ? $classes[$data->storekeeper_status] : '';
        },

        'columns' => [
            ['name' => 'id', 'htmlOptions' => ['class' => 'span2'], 'value' => function ($data, $row) {
                /* @var OrderProductRecord $data */
                return $row + 1;
            }],
            ['name' => 'product_id', 'type' => 'raw', 'htmlOptions' => ['class' => 'span2'], 'value' => function ($data) {
                /* @var OrderProductRecord $data */
                $product = $data->product;
                $result = CHtml::link($product->id, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial('_popoverProduct', ['product' => $product], true),
                ]);
                return $result;
            }],
            [
                'name' => 'number',
                'type' => 'raw',
                'value' => function ($data) {
                    /* @var OrderProductRecord $data */
                    return $data->number . ' ' . '(' . $this->t('available') . ' ' . $data->warehouseNumberAvailable . ')';
                },
                'cssClassExpression' => function ($row, $data) {
                    /* @var OrderProductRecord $data */
                    return $data->number > $data->warehouseNumberAvailable ? 'error' : 'success';
                },
                'htmlOptions' => ['class' => 'span2'],
            ],
            ['name' => 'price', 'type' => 'number', 'htmlOptions' => ['class' => 'span2']],
            ['name' => 'priceTotal', 'type' => 'number', 'htmlOptions' => ['class' => 'span2']],
            [
                'name' => 'callcenter_comment',
                'type' => 'ntext',
                'htmlOptions' => ['class' => 'span3'],
            ],

            [
                'name' => 'storekeeper_comment',
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'htmlOptions' => ['class' => 'span3'],
                'editable' => [
                    'safeOnly' => false,
                    'url' => ['storekeeperOrder/setProductComment'],
                    'params' => [
                        $request->csrfTokenName => $request->getCsrfToken(),
                    ],
                    'type' => 'textarea',
                    'emptytext' => $this->t('No'),
                    'title' => $this->t('Enter your comment'),
                ],
            ],

            [
                'name' => 'connectWarehouse',
                'header' => $this->t('Warehouse'),
                'type' => 'raw',
                'htmlOptions' => ['class' => 'span3'],
                'value' => function ($data, $row) use ($controller) {
                    /* @var $data OrderProductRecord */
                    // the Fucking Awesome Code

                    $output = '';

                    $output .= CHtml::openTag('div', [
                        'data-connect-warehouse' => 'true',
                        'data-row-num' => $row,
                        'data-id' => $data->id,
                        'data-order-id' => $data->order_id,
                        'data-product-id' => $data->product_id,
                    ]);

                    $output .= $controller->widget('bootstrap.widgets.TbGridView', [
                        'enableSorting' => false,
                        'enableHistory' => false,
                        'dataProvider' => WarehouseProductRecord::model()->orderProductId($data->id)->getDataProvider(false, [
                            'pagination' => false,
                        ]),
                        'rowCssClassExpression' => function ($row, $data) {
                            /* @var $data WarehouseProductRecord */
                            if (!in_array($data->status, [WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED, WarehouseProductRecord::STATUS_MAILED_TO_CLIENT])) {
                                return 'error';
                            }
                            return $data->is_security_code_checked ? 'success' : 'warning';
                        },
                        'columns' => [
                            ['name' => 'is_security_code_checked', 'type' => 'boolean'],
                            [
                                'class' => ButtonColumn::class,
                                'template' => '{update}',
                                'buttons' => [
                                    'update' => [
                                        'options' => [
                                            'data-toggle' => 'modal',
                                            'data-target' => '#warehouseProductsModal',
                                            'class' => 'btn btn-warning',
                                        ],
                                        'url' => function ($warehouseData) use ($data, $controller) {
                                            /* @var $warehouseData WarehouseProductRecord */
                                            return $controller->createAbsoluteUrl('storekeeperOrder/ajaxSwitchProductWarehouse', ['id' => $warehouseData->orderProduct->product_id, 'warehouseId' => $warehouseData->id]);
                                        },
                                    ],
                                ],
                            ],
                        ],
                    ], true);

                    $label = WarehouseProductRecord::model()->getAttributeLabel('packaging_security_code');

                    $verifiedNumber = WarehouseProductRecord::model()
                        ->orderProductId($data->id)
                        ->securityCodeChecked(true)
                        ->count();

                    if ($data->number > $verifiedNumber) {
                        $output .= CHtml::label($label, '');
                        $output .= CHtml::textField('warehouseProductSecurityCode', '', ['autocomplete' => 'off']);
                        $output .= CHtml::tag('div', ['data-message-container' => 'true']);
                    }

                    $output .= '</div>';

                    return $output;
                },
            ],

            [
                'name' => 'storekeeper_status',
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'htmlOptions' => ['class' => 'span3'],
                'editable' => [
                    'safeOnly' => false,
                    'url' => ['storekeeperOrder/switchProductStatus'],
                    'params' => [
                        $request->csrfTokenName => $request->getCsrfToken(),
                    ],
                    'type' => 'select',
                    'htmlOptions' => [
                        'data-placement' => 'left',
                        'data-source' => json_encode(OrderProductRecord::storekeeperStatusReplacements(), JSON_FORCE_OBJECT),
                    ],
                    'options' => [
                        'success' => new CJavaScriptExpression('function() {
                                $.fn.yiiGridView.update("packaging-grid");
                            }'),
                    ],
                    'title' => $this->t('Change status'),
                ],
            ],

            [
                'name' => 'event.name',
                'type' => 'raw',
                'headerHtmlOptions' => ['class' => 'hide'],
                'htmlOptions' => ['class' => 'hide'],
                'value' => function ($data) {
                    /* @var OrderProductRecord $data */
                    return CHtml::link($data->event_id . ', ' . $data->event->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
                    ]);
                },
            ],
        ],
    ]); ?>

    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered'],
        'data' => $model,
        'attributes' => [
            [
                'name' => 'weight',
                'type' => 'raw',
                'value' => $this->widget('bootstrap.widgets.TbEditableField', [
                    'model' => $model,
                    'attribute' => 'weight',
                    'safeOnly' => false,
                    'url' => ['storekeeperOrder/setOrderWeight'],
                    'params' => [
                        $request->csrfTokenName => $request->getCsrfToken(),
                    ],
                    'type' => 'text',
                    'emptytext' => $this->t('No'),
                    'title' => $this->t('Specify parcel weight'),
                    'onInit' => new CJavaScriptExpression('function(e, t) {
                            t.options.display.call(this, t.value);
                        }'),
                    'display' => new CJavaScriptExpression('function(value) {
                            return $(this).html(value + " g");
                        }'),
                ], true),
            ],
        ],
    ]) ?>

    <script>
        $(function () {
            $(document).on('click', 'a[data-target="#warehouseProductsModal"]', function () {
                var $this = $(this),
                    $modal = $($this.data('target')),
                    $modalBody = $modal.find('.modal-body');
                var link = ($this.attr('href') || '').trim();
                if (link.length > 0 && link !== '#') {
                    $modalBody.text('Loading');
                    $.ajax({
                        url: link,
                        type: 'GET',
                        success: function (result) {
                            $modalBody.html(result);
                        }
                    });
                }
            });

            $('#warehouseProductsModal').on('click', '[data-is-switch-btn]', function (e) {
                e.preventDefault();
                if (!confirm('Are you sure you want to change the warehouse item to a new one?')) {
                    return;
                }

                var $this = $(this), $modal = $this.closest('.modal');
                console.log($this);
                $.ajax({
                    url: $this.attr('href'),
                    success: function () {
                        $modal.modal('hide');
                        $.fn.yiiGridView.update("packaging-grid");
                    },
                    error: function () {
                        alert('An error has occurred. Failed to update the warehouse item.');
                        $modal.modal('hide');
                    }
                })
            });
        });
    </script>

    <script>
        $(function () {
            var inputSelector = '[data-connect-warehouse] [name="warehouseProductSecurityCode"]';
            var $form = $(document);

            $form.on('keyup keydown submit', inputSelector, function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) {
                    e.preventDefault();
                }
            });

            $form.on('keyup', inputSelector, function (e) {
                e.preventDefault();
                var code = e.keyCode || e.which;

                var $this = $(this),
                    $container = $this.closest('[data-id]'),
                    $messageContainer = $container.find('[data-message-container]'),
                    $connectedTable = $container.find('table'),
                    $connectedTableBody = $connectedTable.children('tbody'),
                    $connectedTableView = $connectedTable.closest('.grid-view');

                var data = $container.data();

                if (code == 13) {
                    var value = $this.val();


                    var tokenName = '<?= $request->csrfTokenName ?>',
                        tokenValue = '<?= $request->getCsrfToken() ?>';

                    var data = {
                        pk: data.id,
                        value: value
                    };

                    data[tokenName] = tokenValue;

                    $.ajax({
                        url: '<?= $this->app()->createUrl('storekeeperOrder/bindWarehouseConfirm') ?>',
                        cache: false,
                        type: 'POST',
                        data: data,
                        success: function (result, status, xhr) {
                            $messageContainer.text(result);
                            if (xhr.status == 200) {
                                $.fn.yiiGridView.update($connectedTableView.attr('id'));
                                $messageContainer.text('Confirmed');
                                $this.val(''); // clear
                            }
                        },
                        error: function (xhr) {
                            $messageContainer.text(xhr.responseText);
                        }
                    })
                } else {
                    $messageContainer.text('');
                }
            });
        });
    </script>

    <?php $this->beginWidget('bootstrap.widgets.TbModal', ['id' => 'warehouseProductsModal', 'fade' => false]); ?>
    <!-- Сюда подгружаются товары которые есть на складе -->
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?= $this->t('Products at warehouse') ?></h4>
    </div>
    <div class="modal-body"></div>
    <?php $this->endWidget() ?>

    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
        'id' => 'filterForm',
        'type' => 'search',
        'method' => 'POST',
    ]); /* @var $form TbActiveForm */ ?>
    <?= $form->errorSummary($model) ?>

    <?php if ($isEditAllowed): ?>
    <div class="form-actions">
        <?= CHtml::tag('button', [
            'type' => 'submit',
            'name' => 'submit',
            'value' => 'recall',
            'class' => 'btn btn-warning btn-large pull-right',
        ], 'Send to recall') ?>
    <?endif;?>
        <?php
        if ($isFulfilmentDelivery && $isEditAllowed) {
            echo CHtml::tag('button', [
                'type' => 'submit',
                'name' => 'submit',
                'value' => 'send',
                'class' => 'btn btn-success btn-large',
            ], $this->t('Send'));

            $generatingLabelLink = CHtml::link(
                $this->t('Label is generating... Click to refresh data.'),
                ['storekeeperOrder/packaging', 'id' => $model->id],
                [
                    'name' => 'generating_label',
                    'class' => 'btn btn-primary btn-large margin-left-10' . ($fulfilmentStatus !== MommyOrder::STATUS_FULFILMENT_CREATED ? ' hidden' : ''),
                ]
            );

            $generateLabelLink = CHtml::ajaxLink(
                $this->t('Generate label'),
                $this->app()->createUrl('storekeeperOrder/createFulfilmentOrder', ['id' => $model->id]),
                [],
                [
                    'target' => '_blank',
                    'name' => 'generate_label',
                    'class' => 'btn btn-primary btn-large margin-left-10',
                ]
            );

            $printLabelLink = CHtml::link($this->t('Print label'), ['labelPrinter/fulfilment', 'id' => $model->id], [
                'target' => '_blank',
                'class' => 'btn btn-primary btn-large margin-left-10',
            ]);

            switch ($fulfilmentStatus) {
                case MommyOrder::STATUS_FULFILMENT_INITIAL:
                    echo $generateLabelLink . $generatingLabelLink;
                    break;
                case MommyOrder::STATUS_FULFILMENT_CREATED:
                    echo $generatingLabelLink;
                    break;
                case MommyOrder::STATUS_FULFILMENT_READY:
                    echo $printLabelLink;
                    break;
            }

            echo CHtml::link(
                $this->t('Refresh shipper and tracking id'),
                ['storekeeperOrder/refreshFulfilmentData', 'id' => $model->id],
                [
                    'name' => 'refresh_data',
                    'class' => 'btn btn-primary btn-large margin-left-10'
                ]
            );
        } elseif($isEditAllowed) {
            echo CHtml::tag('button', [
                'type' => 'submit',
                'name' => 'submit',
                'value' => 'accept',
                'class' => 'btn btn-success btn-large',
            ], $this->t('Confirm'));

            echo CHtml::link($this->t('Print mailing list'), ['labelPrinter/address', 'id' => $model->id], [
                'target' => '_blank',
                'class' => 'btn btn-link',
            ]);
            echo CHtml::link($this->t('Print invoice'), ['labelPrinter/orderInvoice', 'id' => $model->id], [
                'target' => '_blank',
                'class' => 'btn btn-link',
            ]);
        }
        ?>
    </div>
    <?php $this->endWidget() ?>
</div>
<?php $this->endWidget() ?>
<script>
    $('[name="generate_label"]').on('click', function (e) {
        $(this).hide();
        $('[name="generating_label"]').removeClass('hidden');
    });
</script>
