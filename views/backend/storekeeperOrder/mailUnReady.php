<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Product\ProductWeight;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Answer pending');

$request = $this->app()->request;

$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Answer pending'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Orders {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form select',

    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span2']],
        ['name' => 'delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderRecord */
            return $data->deliveryTypeReplacement;
        }],
        ['name' => 'positionsCallcenterAcceptedCount', 'filter' => false],
        ['name' => 'price_total', 'type' => 'number', 'filter' => false],
        ['name' => 'weight', 'filter' => false, 'value' => function ($data) {
            /* @var $data OrderRecord */
            return ProductWeight::instance()->convertToString($data->getWeight());
        }],
        ['name' => 'ordered_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'mailAfterString', 'filter' => false],

        [
            'class' => 'CLinkColumn',
            'htmlOptions' => ['target' => '_blank'],
            'header' => $this->t('View'),
            'label' => $this->t('View'),
            'urlExpression' => function ($data) {
                /* @var $data OrderRecord */
                return $this->app()->createUrl('storekeeperOrder/view', ['id' => $data->id]);
            },
        ],
        [
            'class' => ButtonColumn::class,
            'htmlOptions' => ['class' => 'span4'],
            'header' => $this->t('Actions'),
            'template' => '{backToPackaging}',
            'buttons' => [
                'backToPackaging' => [
                    'label' => '<i class="icon-arrow-left"></i> ' . $this->t('Return to packing'),
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('storekeeperOrder/backToPackaging', ['id' => $data->id]);
                    },
                    'options' => [
                        'title' => $this->t('Return to packing'),
                        'class' => 'btn btn-warning span12',
                    ],
                    'click' => new CJavaScriptExpression("function() {
                        var grid  = $(this).closest('.grid-view');
                        grid.yiiGridView('update', {
                            type: 'POST',
                            data: " . CJavaScript::encode($csrf) . ",
                            url: jQuery(this).attr('href'),
                            success: function(data) {
                                grid.yiiGridView('update');
                            },
                            error: function(XHR) {
                                alert('" . $this->t('Can not update') . "');
                            }
                        });
                        return false;
                    }"),
                ],
            ],
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
