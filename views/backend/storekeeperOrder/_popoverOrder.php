<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $order OrderRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $order,
    'attributes' => [
        'id',
        'client_name',
        'client_surname',
        'ordered_at:datetime',
        'processingStatusReplacement',
    ],
]);
