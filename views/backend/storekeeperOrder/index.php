<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\TabletPackagingRecord;
use MommyCom\Model\Product\ProductWeight;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */
/* @var $tabletBinds TabletPackagingRecord[] array(orderId => TabletPackagingRecord, ...) */

/* @var $packagingStatus string */
/* @var $eventProductId integer */
/* @var $eventId integer */
/* @var $positionNumber integer */

$this->pageTitle = $this->t('Orders to send');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Orders to send'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<label>
    <div><?= $this->t('Packing status') . ':' ?></div>
    <?= CHtml::dropDownList('packagingStatus', $packagingStatus, [
        '' => $this->t('Not applied'),
        'ready' => $this->t('Ready for packing'),
        'unready' => $this->t('Not ready for packing'),
        'packaged' => $this->t('Packed'),
        'dangling' => $this->t('Pending orders'),
    ]) ?>
</label>
<label>
    <div><?= $this->t('№ product in flash-sale') ?></div>
    <?= CHtml::textField('eventProductId', $eventProductId) ?>
</label>
<label>
    <div><?= $this->t('№ Flash-sale') ?></div>
    <?= CHtml::textField('eventId', $eventId) ?>
</label>
<?php $this->endWidget() ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    //'enableHistory'=> false,
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Order {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, #filterForm :input',
    'id' => 'orders',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span2']],
        ['name' => 'delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderRecord */
            return $data->deliveryTypeReplacement;
        }],
        ['name' => 'positionsCallcenterAcceptedCount', 'filter' => false],
        ['name' => 'weight', 'filter' => false, 'value' => function ($data) {
            /* @var $data OrderRecord */
            return ProductWeight::instance()->convertToString($data->getWeight());
        }],
        ['name' => 'ordered_at', 'type' => 'datetime', 'filter' => false],
        [
            'name' => 'admin_id',
            'type' => 'raw',
            'filter' => false,
            'value' => function ($data) {
                /* @var $data OrderRecord */
                if (!$data->isProcessingActive()) {
                    return $this->t('no');
                }
                $style = 'label ' . ($data->admin_id == $this->app()->user->id ? 'label-success' : 'label-warning');
                return CHtml::tag(
                    'span',
                    ['class' => $style],
                    $data->admin->fullname
                );
            },
        ],
        [
            'header' => $this->t('Packer'),
            'type' => 'raw',
            'value' => function ($data) use ($tabletBinds) {
                /* @var $data OrderRecord */
                $has = isset($tabletBinds[$data->id]);
                $result = '';
                if ($has) {
                    $bind = $tabletBinds[$data->id];
                    $result .= CHtml::tag('div', [], $bind->user->fullname);
                }

                $result .= CHtml::ajaxButton(
                    $has ? $this->t('Assign another')
                        : $this->t('Assign'),
                    $this->app()->createUrl('storekeeperOrder/tabletBind', ['id' => $data->id]),
                    [
                        'success' => new CJavaScriptExpression('function() {
                            $.fn.yiiGridView.update("orders");
                        }'),
                        'error' => new CJavaScriptExpression("function(response) {
                            alert(response.responseText);
                        }"),
                    ],
                    ['class' => 'btn']
                );

                return $result;
            },
        ],
        [
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{packaging} {open}',
            'htmlOptions' => [
                'class' => 'span2',
            ],
            'buttons' => [
                'open' => [
                    'label' => '<i class="icon icon-eye-open"></i> ' . $this->t('Open'),
                    'options' => [
                        'class' => 'btn btn-link',
                    ],
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createUrl('storekeeperOrder/view', ['id' => $data->id]);
                    },
                ],
                'packaging' => [
                    'label' => '<i class="icon icon-edit"></i> ' . $this->t('Packing'),
                    'options' => [
                        'class' => 'btn btn-link',
                    ],
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createUrl('storekeeperOrder/packaging', ['id' => $data->id]);
                    },
                ],
            ],
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
