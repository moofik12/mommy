<?php

use MommyCom\Model\Db\OrderProductRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\WarehouseProductRecord;
use MommyCom\Model\Product\ProductWeight;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model OrderRecord */
/* @var $positionsProvider CActiveDataProvider */
$this->pageTitle = $this->t('Review');
$controller = $this;
$request = $this->app()->request;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Packing'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="span3">
    <h4><?= $this->t('Main information') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'htmlOptions' => ['class' => 'table-compact'],
        'data' => $model,
        'attributes' => [
            'id',
            'positionsCallcenterAcceptedCount',
            'price:number',
            'ordered_at:datetime',
            'mailAfterString',
            ['name' => 'processing_status', 'value' => $model->processingStatusReplacement],
            ['name' => 'processing_status_prev', 'value' => $model->processingStatusPrevReplacement],
            [
                'name' => 'processingStatusHistory',
                'type' => 'raw',
                'value' => CHtml::link($this->t('History'), 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProcessingHistory',
                        ['history' => $model->getProcessingStatusHistory()],
                        true
                    ),
                ]),
            ],
        ],
    ]) ?>
</div>
<div class="span4">
    <h4><?= $this->t('Delivery') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model,
        'attributes' => [
            'deliveryTypeReplacement',
            'client_name',
            'client_surname',
            'region.name',
            'city.name',
            [
                'name' => $this->t('Nova Post Branch'),
                'cssClass' => 'hide',
                'type' => 'ntext',
                'value' => '',
            ],
            [
                'type' => 'raw',
                'name' => $this->t('Street'),
                'cssClass' => 'hide',
                'value' => '',
            ],
            [
                'type' => 'raw',
                'name' => $this->t('House number'),
                'cssClass' => 'hide',
                'value' => $model->getDeliveryAttribute('building', ''),
            ],
            'zipcode',
            'address',
            'telephone',
            'trackcode',
        ],
    ]) ?>
</div>
<div class="span4">
    <h4><?= $this->t('Information about the user') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model->user,
        'attributes' => [
            'id',
            ['name' => 'name', 'label' => $this->t('Name')],
            ['name' => 'surname', 'label' => $this->t('Last name ')],
            ['name' => 'email'],
            ['name' => 'telephone'],
            ['name' => 'region.name'],
            ['name' => 'city.name'],
            ['name' => 'address'],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'type' => ['striped', 'bordered'],
    'data' => $model,
    'attributes' => [
        'order_comment:ntext',
        'callcenter_comment:ntext',
        'storekeeper_comment:ntext',
    ],
]) ?>

<div>
    <legend><?= $this->t('Products') ?></legend>

    <?php $this->widget('bootstrap.widgets.TbGroupGridView', [
        'id' => 'orderProducts',
        'enableSorting' => false,
        'enableHistory' => false,
        'dataProvider' => $positionsProvider,
        'summaryText' => '',

        'extraRowColumns' => ['event.name'],

        'rowCssClassExpression' => function ($row, $data) {
            /* @var $data OrderProductRecord */
            $classes = [
                OrderProductRecord::STOREKEEPER_STATUS_UNMODERATED => '',
                OrderProductRecord::STOREKEEPER_STATUS_ACCEPTED => 'success',
                OrderProductRecord::STOREKEEPER_STATUS_UNAVAILABLE => 'warning',
            ];
            return isset($classes[$data->storekeeper_status]) ? $classes[$data->storekeeper_status] : '';
        },

        'columns' => [
            ['name' => 'id', 'htmlOptions' => ['class' => 'span2'], 'value' => function ($data, $row) {
                /* @var OrderProductRecord $data */
                return $row + 1;
            }],
            ['name' => 'product_id', 'type' => 'raw', 'htmlOptions' => ['class' => 'span2'], 'value' => function ($data) {
                /* @var OrderProductRecord $data */
                return CHtml::link($data->product_id, 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial('_popoverProduct', ['product' => $data->product], true),
                ]);
            }],
            ['name' => 'price', 'type' => 'number', 'htmlOptions' => ['class' => 'span2']],

            [
                'name' => 'number',
                'type' => 'raw',
                'value' => function ($data) {
                    /* @var OrderProductRecord $data */
                    return $data->number . ' ' . '(' . $this->t('available') . ' ' . $data->warehouseNumberAvailable . ')';
                },
                'cssClassExpression' => function ($row, $data) {
                    /* @var OrderProductRecord $data */
                    return $data->number > $data->warehouseNumberAvailable ? 'error' : 'success';
                },
                'htmlOptions' => ['class' => 'span2'],
            ],

            ['name' => 'priceTotal', 'type' => 'number', 'htmlOptions' => ['class' => 'span2']],
            [
                'name' => 'callcenter_comment',
                'type' => 'ntext',
                'htmlOptions' => ['class' => 'span3'],
            ],

            [
                'name' => 'storekeeper_comment',
                'type' => 'ntext',
                'htmlOptions' => ['class' => 'span3'],
            ],

            'callcenterStatusReplacement',
            'storekeeperStatusReplacement',

            [
                'name' => 'connectWarehouse',
                'header' => $this->t('Warehouse'),
                'type' => 'raw',
                'htmlOptions' => ['class' => 'span3'],
                'value' => function ($data, $row) use ($controller) {
                    /* @var $data OrderProductRecord */
                    return $controller->widget('bootstrap.widgets.TbGridView', [
                        'enableSorting' => false,
                        'enableHistory' => false,
                        'dataProvider' => WarehouseProductRecord::model()->orderProductId($data->id)->getDataProvider(false, [
                            'pagination' => false,
                        ]),
                        'rowCssClassExpression' => function ($row, $data) {
                            /* @var $data WarehouseProductRecord */
                            if (!in_array($data->status, [WarehouseProductRecord::STATUS_IN_WAREHOUSE_RESERVED, WarehouseProductRecord::STATUS_MAILED_TO_CLIENT])) {
                                return 'error';
                            }
                            return $data->is_security_code_checked ? 'success' : 'warning';
                        },
                        'columns' => [
                            ['name' => 'id'],
                            ['name' => 'is_security_code_checked', 'type' => 'boolean'],
                        ],
                    ], true);
                },
            ],

            [
                'name' => 'event.name',
                'type' => 'raw',
                'headerHtmlOptions' => ['class' => 'hide'],
                'htmlOptions' => ['class' => 'hide'],
                'value' => function ($data) {
                    /* @var OrderProductRecord $data */
                    return CHtml::link($data->event_id . ', ' . $data->event->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('_popoverEvent', ['event' => $data->event], true),
                    ]);
                },
            ],
        ],
    ]); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'type' => ['striped', 'bordered'],
    'data' => $model,
    'attributes' => [
        [
            'name' => 'weight',
            'type' => 'raw',
            'value' => ProductWeight::instance()->convertToString($model->weight),
        ],
    ],
]) ?>

<div class="well">
    <?= CHtml::link($this->t('Print mailing list'), ['labelPrinter/address', 'id' => $model->id], [
        'target' => '_blank',
        'class' => 'btn btn-link',
    ]) ?>
    <?= CHtml::link($this->t('Print invoice'), ['labelPrinter/orderInvoice', 'id' => $model->id], [
        'target' => '_blank',
        'class' => 'btn btn-link',
    ]) ?>
</div>
<?php $this->endWidget() ?>
