<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Product\ProductWeight;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Dispatched');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Dispatched'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Orders {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span2']],
        ['name' => 'delivery_type', 'filter' => OrderRecord::deliveryTypeReplacements(), 'value' => function ($data) {
            /* @var $data OrderRecord */
            return $data->deliveryTypeReplacement;
        }],
        ['name' => 'positionsCallcenterAcceptedCount', 'filter' => false],
        ['name' => 'price_total', 'type' => 'number', 'filter' => false],
        ['name' => 'ordered_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'weight', 'filter' => false, 'value' => function ($data) {
            /* @var $data OrderRecord */
            return ProductWeight::instance()->convertToString($data->getWeight());
        }],
        ['name' => 'trackcode'],
        [
            'class' => 'CLinkColumn',
            'htmlOptions' => ['target' => '_blank'],
            'header' => $this->t('View'),
            'label' => $this->t('View'),
            'urlExpression' => function ($data) {
                /* @var $data OrderRecord */
                return $this->app()->createUrl('storekeeperOrder/view', ['id' => $data->id]);
            },
        ],
    ],
]); ?>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
