<?php

use MommyCom\Controller\Backend\ProductSectionController;

/**
 * @var $this ProductSectionController
 * @var $form TbForm
 */
$this->pageTitle = $this->t('Adding item section');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Add product section'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>

<?php $this->endWidget() ?>
