<?php

use MommyCom\Controller\Backend\ProductSectionController;

/**
 * @var $this ProductSectionController
 * @var $form TbForm
 * */
$this->pageTitle = $this->t('Product section editing');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Product section editing'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>

<?php $this->endWidget() ?>
