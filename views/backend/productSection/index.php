<?php

use MommyCom\Controller\Backend\ProductSectionController;
use MommyCom\Model\Db\ProductSectionRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this ProductSectionController
 */

$this->pageTitle = $this->t('Product sections');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Product sections'),
    'headerIcon' => 'icon-add',
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['productSection/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<br>
<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}',
    'summaryText' => 'Sections {start} - {end} out of {count}',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        [
            'name' => 'parent_id',
            'filter' => [0 => $this->t('Select 1st level sections')] + CHtml::listData(ProductSectionRecord::model()->onlyParent()->findAll(), 'id', 'name'),
            'value' => function ($data) {
                /* @var $data ProductSectionRecord */
                return ($data->parent !== null) ? $data->parent->name : '';
            },
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        ['name' => 'name', 'headerHtmlOptions' => ['class' => 'span6']],
        ['name' => 'keywords', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2'], 'value' => function ($data) {
            /* @var $data ProductSectionRecord */
            return implode(',', $data->keywords);
        }],
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
        ],
    ],
]); ?>
<?php $this->endWidget() ?>
