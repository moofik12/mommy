<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\OrderControlRecord;

/**
 * @var $this MoneyControlController
 * @var $provider CActiveDataProvider
 * @var $form TbForm
 * @var $model OrderControlRecord
 */

$this->pageTitle = $this->t('Editing');
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t("Editing order №") . $model->order_id,
    'headerIcon' => 'icon-th-list',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
