<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\OrderControlRecord;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 * @var $this MoneyControlController
 * @var $filter array
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

$timeDeliveryInterval = DateInterval::createFromDateString(OrderControlRecord::ACTUAL_MAILED_ORDER);

//default
$defaultFilter = [
    'mailingEnded' => false,
    'orderMailedAt' => false,
    'orderDeliveryTypes' => OrderRecord::deliveryTypeReplacements(),
    'statuses' => OrderControlRecord::statusReplacements(),
    'pageSize' => 50,
];

$filter += $defaultFilter;
?>
    <div style="float: right;">
        <label>
            <div><?= $this->t('Records per page') ?></div>
            <?= CHtml::dropDownList('pageSize', $filter['pageSize'], [10 => 10, 25 => 25, 50 => 50, 100 => 100, 200 => 200, 500 => 500,]) ?>
        </label>
    </div>

    <label>
        <div><?= $this->t('Date of dispatch') ?></div>
        <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
            'name' => 'orderMailedAt',
            'value' => $filter['orderMailedAt'],
            'htmlOptions' => [
                'placeholder' => $this->t('Date of dispatch'),
            ],
        ]) ?>
    </label>

    <label style="vertical-align: top;">
        <div><?= $this->t('Delivery type') ?></div>
        <?php
        $this->widget('bootstrap.widgets.TbSelect2', [
            'name' => 'orderDeliveryTypes',
            'value' => $filter['orderDeliveryTypes'],
            'data' => OrderRecord::deliveryTypeReplacements(),
            'asDropDownList' => true,
            'htmlOptions' => [
                'placeholder' => $this->t('Delivery type'),
                'multiple' => 'multiple',
                'data-tags' => 'true',
            ],
            'options' => [
                'tokenSeparators' => [","],
            ],
        ]);
        ?>

    </label>

    <label style="vertical-align: top;">
        <div><?= $this->t('Statuses') ?></div>
        <?php
        $this->widget('bootstrap.widgets.TbSelect2', [
            'name' => 'statuses',
            'value' => $filter['statuses'],
            'data' => OrderControlRecord::statusReplacements(),
            'asDropDownList' => true,
            'htmlOptions' => [
                'placeholder' => $this->t('Statuses'),
                'multiple' => 'multiple',
                'data-tags' => 'true',
            ],
            'options' => [
                'tokenSeparators' => [","],
            ],
        ]);
        ?>
    </label>

    <label>
        <div><?= $this->t('Delivery time') ?> <?= $this->t('{n} day | {n} days | {n} days', $timeDeliveryInterval->d) ?> <?= $this->t('is out')?>:</div>
        <?= CHtml::dropDownList('mailingEnded', $filter['mailingEnded'],
            [$this->t('No'), $this->t('Yes')]) ?>
    </label>

    <label>
        <div><?= $this->t('Dropshipping flash-sale') ?></div>
        <?= CHtml::dropDownList('isDropShipping', $filter['isDropShipping'], ['all' => $this->t('Any'), $this->t('No'), $this->t('Yes')]) ?>
    </label>

    <label>
        <?php
        $this->widget('bootstrap.widgets.TbSelect2', [
            'name' => 'supplierId',
            'asDropDownList' => false,
            'val' => $filter['supplierId'],
            'options' => [
                'width' => '250px;',
                'placeholder' => $this->t('Supplier'),
                'allowClear' => true,
                'ajax' => [
                    'url' => $this->app()->createUrl('supplier/ajaxSearch'),
                    'type' => 'GET',
                    'dataType' => 'json',
                    'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
                    'results' => new CJavaScriptExpression('function(response, page) {
                        var result = {
                            more: page < response.pageCount,
                            results: []
                        };
        
                        $.each(response.items, function(index, value) {
                            result.results.push({
                                id: value.id,
                                text: value.name,
                            });
                        });
        
                        return result;
                }'),
                ],
            ],

        ]);
        ?>
    </label>

<?php
//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
