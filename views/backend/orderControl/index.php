<?php

use MommyCom\Controller\Backend\OrderControlController;
use MommyCom\Model\Db\OrderControlRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this OrderControlController
 * @var $provider CActiveDataProvider
 * @var $filter array
 */

$this->pageTitle = $this->t('Order monitoring');

$request = $this->app()->request;
/** @var OrderControlRecord $model */
$model = $provider->model;

$modelsParams = [];
$paramName = get_class($model);
if (false !== strpos($paramName, '\\')) {
    $paramName = substr($paramName, strpos($paramName, '\\') + 1);
}
foreach ($this->app()->request->getParam($paramName, []) as $key => $value) {
    $modelsParams[CHtml::activeName($model, $key)] = $value;
}
$filterParams = array_merge($filter, $modelsParams);

?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Orders'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter', ['filter' => $filter]) ?>

<div>
    <div class="text-right" id="save-csv">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', [
            'buttons' => [
                [
                    'type' => 'info',
                    'label' => $this->t('Download data'),
                    'dropdownOptions' => [
                        'class' => 'text-left',
                        'style' => 'right: 0; left: auto;',
                    ],
                    'items' => [
                        [
                            'label' => $this->t('All'),
                            'url' => $this->createUrl('index', array_merge(
                                    ['saveCsvMode' => OrderControlController::SAVE_SCV_MODE_FULL],
                                    $filterParams
                                )
                            ),
                        ],
                        [
                            'label' => $this->t('Everything including additional data'),
                            'url' => $this->createUrl('index', array_merge(
                                    [
                                        'saveCsvMode' => OrderControlController::SAVE_SCV_MODE_FULL_DETAIL,
                                    ],
                                    $filterParams
                                )
                            ),
                        ],
                    ],
                ],
            ],
        ]) ?>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $model,
    'template' => "{pager}\n{summary}\n{items}\n{pager}\n{extendedSummary}",
    'filterSelector' => '{filter}, #filter-form',
    'ajaxUpdate' => 'save-csv',
    'columns' => [
        [
            'name' => 'id',
            'htmlOptions' => ['class' => 'span1'],

        ],
        [
            'class' => 'CLinkColumn',
            'labelExpression' => function ($data) {
                /* @var OrderControlRecord $data */
                return $data->order_id;
            },
            'urlExpression' => function ($data) {
                /* @var OrderControlRecord $data */
                return $this->app()->createAbsoluteUrl('callcenterOrder/history', ['OrderRecord[id]' => $data->order_id]);
            },
            'htmlOptions' => ['class' => 'span2'],
            'linkHtmlOptions' => [
                'target' => '_blank',
            ],
        ],
        [
            'header' => $this->t('Flash-sale with dropshipping'),
            'name' => 'order.is_drop_shipping',
            'type' => 'boolean',
        ],
        [
            'header' => $this->t('Supplier'),
            'value' => function ($data) {
                /* @var OrderControlRecord $data */
                $text = $this->t('no');
                if ($data->order->supplier) {
                    $text = $data->order->supplier->name;
                }

                return $text;
            },
        ],
        [
            'name' => 'order_mailed_at',
            'value' => function ($data) {
                /* @var OrderControlRecord $data */
                return $this->app()->dateFormatter->formatDateTime($data->order_mailed_at);
            },
            'htmlOptions' => ['class' => 'span2'],

        ],
        [
            'name' => 'order.delivery_type',
            'type' => 'raw',
            'filter' => OrderRecord::deliveryTypeReplacements(),
            'value' => function ($data) {
                /* @var $data OrderControlRecord */
                $text = $data->order->deliveryTypeReplacement;

                return $text;
            },
            'htmlOptions' => ['class' => 'span3'],

        ],
        [
            'name' => 'order.price_total',
            'htmlOptions' => ['class' => 'span2'],

        ],
        [
            'header' => $this->t('Amount due'),
            'value' => function ($data) {
                /* @var OrderControlRecord $data */
                return $data->order->getPayPriceHistory();
            },
            'htmlOptions' => ['class' => 'span2'],

        ],
        [
            'name' => 'status',
            'filter' => false,
            'value' => function ($data) {
                /* @var $data OrderControlRecord */
                return $data->statusReplacement();
            },
        ],
        [
            'name' => 'operator_status',
            'filter' => OrderControlRecord::operatorStatusReplacements(),
            'value' => function ($data) {
                /* @var $data OrderControlRecord */
                return $data->operatorStatusReplacement();
            },
        ],
        'comment',
        'system_comment',
        [
            'class' => ButtonColumn::class,
            'template' => '{update}',
            'buttons' => [
                'update' => [
                    'visible' => function ($row, $data) {
                        /** @var OrderControlRecord $data */
                        return $data->isMailedOrderEnded() && $data->isAvailableChangeOperatorStatus();
                    },
                ],
            ],
        ],
    ],
    'extendedSummary' => [
        'title' => $this->t('Total (on the page)'),
        'columns' => [
            'order.price_total' => [
                'class' => 'TbSumOperation',
                'label' => $this->t('Total'),
            ],
        ],
    ],
    'extendedSummaryOptions' => [
        'class' => 'well pull-right',
        'style' => 'width:300px',
    ],
]); ?>

<?php $this->endWidget() ?>
