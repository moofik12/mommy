<?php

use MommyCom\Controller\Backend\TaskBoardController;

/* @var $this TaskBoardController */
/* @var $form TbForm */
/* @var $title string */
$this->pageTitle = $this->t('Adding Roles to the Task Manager');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Adding Roles to the Task Manager'),
    'headerIcon' => 'icon-add',
]);

echo $form->render();
$this->endWidget();

