<?php

use MommyCom\Controller\Backend\TaskBoardUserRoleConnectionController;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\TaskBoardRecord;

/**
 * @var $this TaskBoardUserRoleConnectionController
 * @var $provider CActiveDataProvider
 */
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => '',
    'headerIcon' => 'icon-edit',
]);
?>
    <div style="display:inline">
        <?=
        CHtml::link($this->t('Add role'), $this->createUrl('add'), [
            'class' => 'btn btn-primary',
        ]) ?>
    </div>
<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'columns' => [
        [
            'name' => 'task_board_id',
            'filter' => false,
            'value' => function ($data) {
                return TaskBoardRecord::getQuestionTypes($data->task_board_id);
            },
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'name' => 'user_role',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
            'htmlOptions' => [
                'style' => 'text-align:center; vertical-align: inherit;',
                'class' => 'span2',
            ],
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
        ],
    ],
]);
$this->endWidget();
