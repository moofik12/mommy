<?php
/**
 * @author: Zhenya Bondarev
 * Date: 16.02.15
 */
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'filterUsers',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'columns' => array(
        array('name' => 'id', 'headerHtmlOptions' => array('class' => 'span2')),
        array('name' => 'email'),

        array('name' => 'created_at', 'type' => 'datetime', 'filter' => false),
        array('name' => 'updated_at', 'type' => 'datetime', 'filter' => false),

    ),
)); ?>