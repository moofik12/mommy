<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */

$this->pageTitle = $this->t('User filter');

$csrf = '';
if ($this->app()->request->enableCsrfValidation) {
    $csrfTokenName = $this->app()->request->csrfTokenName;
    $csrfToken = $this->app()->request->csrfToken;
    $csrf = "\n\t\tdata:{ '$csrfTokenName':'$csrfToken' },";
}

?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), $this->createUrl('listAdd'), [
        'class' => 'btn btn-primary',
    ]) ?>
</div>

<div class="clearfix"></div>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'filterUsersList',
    'dataProvider' => $provider,
    'filter' => $provider->model,
//        'filterSelector' => '{filter}, #filter-form',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'name'],
        ['name' => 'total_items'],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false],
        ['name' => 'updated_at', 'type' => 'datetime', 'filter' => false],

        [
            'class' => ButtonColumn::class,
            'openNewWindow' => false,
            'template' => '{update} {delete}',
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("listUpdate",array("id"=>$data->primaryKey))',
            'deleteButtonUrl' => '$this->app()->controller->createUrl("listDelete",array("id"=>$data->primaryKey))',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
