<?php

use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
$this->pageTitle = $this->t('User filter');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('New filter'),
    'headerIcon' => 'icon-add',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
