<?php

use MommyCom\Model\Db\StatisticFilterUserListRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
/* @var $dataProvider CActiveDataProvider */
/* @var $model StatisticFilterUserListRecord */
$this->pageTitle = $this->t('User filter');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Filter editing'),
    'headerIcon' => 'icon-add',
]); ?>

<?= $form->render() ?>

<?php
$this->widget('bootstrap.widgets.TbButton', [
    'buttonType' => TbButton::BUTTON_AJAXBUTTON,
    'type' => 'warning',
    'label' => $this->t('clear'),
    'url' => $this->app()->controller->createUrl("listClear", ["id" => $model->id]),
    'ajaxOptions' => [
        'success' => new CJavaScriptExpression("
                    function(data) {
                        jQuery('#filterUsers').yiiGridView('update');
                    }"
        ),
        'error' => new CJavaScriptExpression("
                    function(XHr) {
                        alert('" . $this->t('Error while deleting items') . "');
                    }"
        ),
    ],
]);
?>

<?= $this->renderPartial('_listItems', ['dataProvider' => $dataProvider]) ?>

<?php $this->endWidget() ?>
