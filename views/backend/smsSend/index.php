<?php

use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $this CController
 * @var $form Form
 */

?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Sending SMS'),
    'headerIcon' => 'icon-edit',
]);

$this->widget('bootstrap.widgets.TbAlert', [
    'alerts' => [
        'error' => ['block' => true, 'fade' => true, 'closeText' => '×'], // success, info, warning, error or danger
        'info' => ['block' => true, 'fade' => true, 'closeText' => '×'],
    ],
]);
?>

<?= $form->render(); ?>

<?php $this->endWidget(); ?>
