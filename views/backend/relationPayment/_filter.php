<?php

use MommyCom\Controller\Backend\MoneyControlController;
use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 * @var $this MoneyControlController
 * @var array|string $excludeFilters
 */

$excludeFilters = isset($excludeFilters) ? (is_scalar($excludeFilters) ? [$excludeFilters] : $excludeFilters) : [];
$filters = [];

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter-form',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

$filters['eventStartAt'] = $this->widget('bootstrap.widgets.TbDateRangePicker', [
    'name' => 'eventStartAt',
    'value' => $this->app()->request->getParam('eventStartAt'),
    'htmlOptions' => ['autocomplete' => 'off', 'placeholder' => $this->t('Conducting a promo campaign')],
], true);

$filters = array_values(array_diff_key($filters, array_combine($excludeFilters, $excludeFilters)));
foreach ($filters as $key => $filter) {
    if ($key % 3 == 0) {
        echo '<br>';
    }
    echo $filter;
}

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {
    $("#' . $filterForm->id . ' input:first").trigger("change");
    return false;
}'], ['class' => 'btn btn-primary']);

$this->endWidget();
