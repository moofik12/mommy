<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierRelationPaymentPenaltyRecord;
use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Model\Db\SupplierRelationPaymentTransactionRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController
 * @var $model SupplierRelationPaymentRecord
 * @var $providerPenalty CActiveDataProvider
 * @var $providerTransactions CActiveDataProvider
 */
$this->pageTitle = $this->t('Edit payment');
$request = $this->app()->request;
?>
<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Basic info'),
    'headerIcon' => 'icon-edit',
]);
?>
<div class="row">
    <div class="span6">
        <?php $this->widget('bootstrap.widgets.TbDetailView', [
            'data' => $model,
            'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
            'attributes' => [
                'event_id',
                [
                    'name' => 'status',
                    'value' => $model->statusText,
                ],
                'is_force_refresh:boolean',
                'orders',
                'orders_canceled',
                'orders_mailed',
                'orders_delivered',
                'orders_not_delivered',
                'orders_returned',
                'orders_amount',
            ],
        ]); ?>
    </div>
    <div class="span6">
        <?php $this->widget('bootstrap.widgets.TbDetailView', [
            'data' => $model,
            'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
            'attributes' => [
                'orders_delivered_amount',
                'orders_not_delivered_amount',
                'orders_prepaid_amount',
                'penalty_amount',
                'commission',
                'waiting_sending_days',
                'to_pay',
                'paid',
                'synchronized_at:datetime',
            ],
        ]); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Penatlies'),
    'headerIcon' => 'icon-edit',
]);
?>
<?php if ($model->isPossibleAddPenalty()): ?>
    <div>
        <?= CHtml::link($this->t('Add penalty'), ['addPenalty', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'target' => '_blank',
        ]) ?>
    </div>
<?php endif; ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerPenalty,
    'filter' => $providerPenalty->model,
    'rowCssClassExpression' => function ($row, $data) {
        /* @var SupplierRelationPaymentPenaltyRecord $data */
        $cssClass = [
            SupplierRelationPaymentPenaltyRecord::STATE_ACTIVE => 'info',
            SupplierRelationPaymentPenaltyRecord::STATE_INACTIVE => 'danger',
        ];

        return CHtml::value($cssClass, $data->state, '');
    },

    'columns' => [
        [
            'name' => 'state',
            'filter' => SupplierRelationPaymentPenaltyRecord::states(),
            'type' => 'raw',
            'value' => function ($data) use ($request) {
                /* @var $data SupplierRelationPaymentPenaltyRecord */
                return $this->widget('bootstrap.widgets.TbEditableField', [
                    'model' => $data,
                    'attribute' => 'state',
                    'safeOnly' => false,
                    'url' => ['changePenaltyState'],
                    'params' => [
                        $request->csrfTokenName => $request->getCsrfToken(),
                    ],
                    'type' => 'select',
                    'source' => SupplierRelationPaymentPenaltyRecord::states(),
                    'emptytext' => $this->t('No'),
                ], true);
            },
        ],
        'event_id',
        'order_id',
        'amount',
        'description',
    ],
]); ?>

<?php $this->endWidget(); ?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Транзакции (оплаты/выплаты)'),
    'headerIcon' => 'icon-edit',
]);
?>

<?php if ($model->isPossibleAddPayment()): ?>
    <div>
        <?= CHtml::link($this->t('Make payment'), ['pay', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'target' => '_blank',
        ]) ?>
    </div>
<?php endif; ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerTransactions,
    'filter' => $providerTransactions->model,
    'columns' => [
        'event_id',
        'amount',
        'comment',
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
            'buttons' => [
                'delete' => [
                    'label' => $this->t('Make payment'),
                    'url' => function ($data) {
                        return CHtml::normalizeUrl(['deletePay', 'id' => $data->id]);
                    },
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierRelationPaymentTransactionRecord */
                        return $data->isAvailableEdit();
                    },
                ],
            ],
        ],
    ],
]); ?>

<?php $this->endWidget(); ?>
