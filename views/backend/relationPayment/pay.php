<?php

use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $form TbForm */
/* @var $model SupplierRelationPaymentRecord */
$this->pageTitle = $this->t('Recording payment');
$supplierName = $model->supplier ? $model->supplier->name : '';
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => "Recording payment / payment for the offer No. {$model->event_id}, supplier: $supplierName",
    'headerIcon' => 'icon-edit',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
