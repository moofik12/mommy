<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Service\BaseController\BackController;

/**
 * @var $this BackController
 * @var $provider CActiveDataProvider
 * @var $enableStatusFilter bool
 * @var $summary CMap
 * @var $additionSummary CMap
 */

$this->pageTitle = $this->t('Dropshipping');
?>
<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List'),
    'headerIcon' => 'icon-edit',
]);

$this->renderPartial('_filter');

$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, #filter-form',
    'ajaxUpdate' => 'payment-summary',
    'columns' => [
        'event_id',
        [
            'name' => 'status',
            'filter' => SupplierRelationPaymentRecord::statuses(),
            'value' => function ($data) {
                /* @var SupplierRelationPaymentRecord $data */
                return $data->getStatusText();
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'orders',
            'type' => 'raw',
            'value' => function ($data) {
                /* @var SupplierRelationPaymentRecord $data */
                return "<span title=\"" . $this->t('Confirmed Orders') . "\" data-toggle=\"tooltip\" >{$data->ordersConfirmed}</span> <span style=\"color:#808080;\" title=\"" . $this->t('All Orders') . "\" data-toggle=\"tooltip\" >({$data->orders})</span>";
            },
        ],
        [
            'name' => 'orders_mailed',
            'header' => '<span title="' . $this->t('Orders placed') . '" data-toggle="tooltip" ><i class="icon-truck"></i></span>',
        ],
        'orders_amount',
        [
            'name' => 'orders_delivered_amount',
            'header' => $this->t('Amount of delivered orders'),
        ],
        'penalty_amount',
        'commission',
        'to_pay',
        'paid',
        [
            'name' => 'synchronized_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var SupplierRelationPaymentRecord $data */
                if ($data->synchronized_at > 0) {
                    return $this->app()->dateFormatter->formatDateTime($data->synchronized_at);
                }

                return $this->t('no');
            },
        ],
        [
            'name' => 'is_force_refresh',
            'type' => 'boolean',
            'filter' => $this->app()->format->booleanFormat,
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {update} {delete}',
            'viewButtonOptions' => ['title' => $this->t('Learn more')],
            'deleteConfirmation' => false,
            'buttons' => [
                'view' => [
                    'icon' => 'eye-open',
                    'url' => function ($data) {
                        return CHtml::normalizeUrl(['view', 'id' => $data->id]);
                    },
                ],
                'update' => [
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierRelationPaymentRecord */
                        return $data->status != SupplierRelationPaymentRecord::STATUS_PAID;
                    },
                ],
                'delete' => [
                    'icon' => 'refresh',
                    'url' => function ($data) {
                        return CHtml::normalizeUrl(['ajaxForceUpdateStatistic', 'id' => $data->id]);
                    },
                    'options' => [
                        'class' => 'btn-info',
                    ],
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierRelationPaymentRecord */
                        return $data->status != SupplierRelationPaymentRecord::STATUS_PAID
                            && !$data->is_force_refresh;
                    },
                ],
            ],
        ],
    ],
]);
?>
<?php if ($summary == false || $additionSummary == false): ?>
    <div id="payment-summary" class="well pull-right extended-summary" style="width:300px">
        <h4><?= $this->t('Total') ?>: </h4>
        <?= $this->t('To display information, it is necessary to select the period of flash-sale') ?>
    </div>
    <div class="clearfix"></div>
<?php else: ?>
    <?php $this->renderPartial('_summary', ['summary' => $summary, 'additionSummary' => $additionSummary]); ?>
<?php endif;; ?>

<?php
$this->endWidget();
?>

