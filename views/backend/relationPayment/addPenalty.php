<?php

use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $form TbForm */
/* @var $model SupplierRelationPaymentRecord */
$this->pageTitle = $this->t('Add penalty');
$supplierName = $model->supplier ? $model->supplier->name : '';
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Adding a fine on the flash-sale') . "№{$model->event_id}," . $this->t('поставщик') . ':' . $supplierName,
    'headerIcon' => 'icon-edit',
]); ?>

<?= $form->render() ?>

<?php $this->endWidget() ?>
