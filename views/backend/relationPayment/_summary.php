<?php

use MommyCom\Model\Db\SupplierRelationPaymentRecord;

/* @var CMap $summary */
/* @var CMap $additionSummary */
$nf = $this->app()->numberFormatter;
?>

<div id="payment-summary" class="well pull-right extended-summary" style="width:300px">
    <h4><?= $this->t('Total') ?>: </h4>
    <strong> <span title="<?= $this->t('Flash-sales that generate orders') ?>" data-toggle="tooltip"><span
                    class="icon-exclamation-sign"></span><?= $this->t('Flash-sales (payments)') ?>
            : </span></strong><?= $nf->formatDecimal($additionSummary['events']) ?><br>
    <strong> <?= $this->t('Average flash-sale amount') ?>
        : </strong><?= $nf->formatDecimal($additionSummary['avr_events_amount']) ?><br>
    <strong> <?= $this->t('Average commission rate') ?>
        : </strong><?= $nf->formatDecimal($additionSummary['avr_commission']) ?><br>
    <strong> <?= $this->t('Settling an order') ?>
        : </strong><?= $nf->formatDecimal($additionSummary['orders_delivered_percent']) ?> %<br>

    <?php foreach ($summary->toArray() as $attribute => $value) : ?>
        <strong><?= SupplierRelationPaymentRecord::model()->getAttributeLabel($attribute) ?>
        : </strong><?= $nf->formatDecimal($value) ?><br>
    <?php endforeach ?>
</div>
<div class="clearfix"></div>
