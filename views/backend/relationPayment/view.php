<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\SupplierRelationPaymentPenaltyRecord;
use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController
 * @var $model SupplierRelationPaymentRecord
 * @var $providerPenalty CActiveDataProvider
 * @var $providerTransactions CActiveDataProvider
 * @var $providerOrders CActiveDataProvider
 */
$this->pageTitle = $this->t('View payment');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Basic info'),
    'headerIcon' => 'icon-edit',
]);
?>
<div class="row">
    <div class="span6">
        <?php $this->widget('bootstrap.widgets.TbDetailView', [
            'data' => $model,
            'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
            'attributes' => [
                'event_id',
                [
                    'name' => 'status',
                    'value' => $model->statusText,
                ],
                [
                    'name' => 'orders',
                    'type' => 'raw',
                    'value' => "<span title=\"" . $this->t('Confirmed Orders') . "\" data-toggle=\"tooltip\" >{$model->ordersConfirmed}</span> <span style=\"color:#808080;\" title=\"" . $this->t('All Orders') . "\" data-toggle=\"tooltip\" >({$model->orders})</span>",
                ],
                'orders_hold_delivery',
                'orders_mailed',
                'orders_canceled',
                'orders_delivered',
                'orders_not_delivered',
                'waiting_sending_days',
            ],
        ]); ?>
    </div>
    <div class="span6">
        <?php $this->widget('bootstrap.widgets.TbDetailView', [
            'data' => $model,
            'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
            'attributes' => [
                'orders_amount',
                'orders_delivered_amount',
                'orders_not_delivered_amount',
                'orders_prepaid_amount',
                'penalty_amount',
                'commission',
                'to_pay',
                'paid',
                'synchronized_at:datetime',
            ],
        ]); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Penatlies'),
    'headerIcon' => 'icon-edit',
]);
?>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerPenalty,
    'filter' => $providerPenalty->model,
    'rowCssClassExpression' => function ($row, $data) {
        /* @var SupplierRelationPaymentPenaltyRecord $data */
        $cssClass = [
            SupplierRelationPaymentPenaltyRecord::STATE_ACTIVE => 'info',
            SupplierRelationPaymentPenaltyRecord::STATE_INACTIVE => 'danger',
        ];

        return CHtml::value($cssClass, $data->state, '');
    },

    'columns' => [
        [
            'name' => 'state',
            'filter' => SupplierRelationPaymentPenaltyRecord::states(),
            'value' => '$data->stateText',
        ],
        'event_id',
        'order_id',
        'amount',
        'description',
    ],
]); ?>

<?php $this->endWidget(); ?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Транзакции (оплаты/выплаты)'),
    'headerIcon' => 'icon-edit',
]);
?>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerTransactions,
    'filter' => $providerTransactions->model,
    'columns' => [
        'event_id',
        'amount',
        'comment',
    ],
]); ?>

<?php $this->endWidget(); ?>

<?php
$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Orders'),
    'headerIcon' => 'icon-edit',
]);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerOrders,
    'filter' => $providerOrders->model,
    'rowCssClassExpression' => function ($row, $data) {
        /* @var OrderRecord $data */
        return $data->drop_shipping_commission ? 'info' : '';
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => $this->t('Order'),
        ],
        [
            'name' => 'price',
            'filter' => false,
        ],
        [
            'name' => 'payPriceHistory',
            'filter' => false,
        ],
        'drop_shipping_commission',
        [
            'name' => 'paid_commission_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var OrderRecord $data */
                if ($data->paid_commission_at > 0) {
                    return $this->app()->dateFormatter->formatDateTime($data->paid_commission_at, 'medium', null);
                }

                return $this->t('no');
            },
        ],
    ],
]); ?>

<?php $this->endWidget(); ?>
