<?php

use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\Model\Db\SupplierRelationPaymentRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController
 * @var $provider CActiveDataProvider
 * @var $enableStatusFilter bool
 * @var $summary CMap
 * @var $additionSummary CMap
 */
$this->pageTitle = $this->t('Dropshipping');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('List'),
    'headerIcon' => 'icon-edit',
]);

$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, form select',
    'ajaxUpdate' => 'payment-summary',
    'columns' => [
        'event_id',
        'orders',
        'orders_mailed',
        'orders_amount',
        'penalty_amount',
        'waiting_sending_days',
        'commission',
        'to_pay',
        'paid',
        [
            'name' => 'synchronized_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var SupplierRelationPaymentRecord $data */
                if ($data->synchronized_at > 0) {
                    return $this->app()->dateFormatter->formatDateTime($data->synchronized_at);
                }

                return $this->t('no');
            },
        ],
        [
            'name' => 'is_force_refresh',
            'type' => 'boolean',
            'filter' => $this->app()->format->booleanFormat,
        ],
        [
            'class' => ButtonColumn::class,
            'openNewWindow' => false,
            'template' => '{update} {pay} {view} {delete}',
            'viewButtonOptions' => ['title' => $this->t('Learn more')],
            'deleteConfirmation' => false,
            'buttons' => [
                'pay' => [
                    'label' => $this->t('Make payment'),
                    'icon' => 'money',
                    'url' => function ($data) {
                        return CHtml::normalizeUrl(['pay', 'id' => $data->id]);
                    },
                    'options' => [
                        'class' => 'btn-success',
                    ],
                ],
                'view' => [
                    'icon' => 'eye-open',
                    'url' => function ($data) {
                        return CHtml::normalizeUrl(['view', 'id' => $data->id]);
                    },
                ],
                'delete' => [
                    'icon' => 'refresh',
                    'url' => function ($data) {
                        return CHtml::normalizeUrl(['ajaxForceUpdateStatistic', 'id' => $data->id]);
                    },
                    'options' => [
                        'class' => 'btn-info',
                    ],
                    'visible' => function ($row, $data) {
                        /* @var $data SupplierRelationPaymentRecord */
                        return $data->status != SupplierRelationPaymentRecord::STATUS_PAID
                            && !$data->is_force_refresh;
                    },
                ],
            ],
        ],
    ],
]);

$this->renderPartial('_summary', ['summary' => $summary, 'additionSummary' => $additionSummary]);

$this->endWidget();

