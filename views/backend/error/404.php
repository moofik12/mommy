<?php
/**
 * @var \MommyCom\Service\BaseController\Controller $this
 * @var string $errorCode
 * @var string $errorMessage
 */
?>
<!doctype html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Oops - <?= $this->app()->name ?></title>
</head>
<body class="errorLayout">
<!-- #main -->
<div id="main">
    <!-- .container-fluid -->
    <div class="container-fluid">
        <!-- .row-fluid -->
        <div class="row-fluid">
            <!-- .span12 -->
            <div class="span12 logo">
                <h1><?= $errorCode ?></h1>
            </div>
            <!-- /.span12 -->
        </div>
        <!-- /.row-fluid -->
        <!-- .row-fluid -->
        <div class="row-fluid">
            <!-- .span6 -->
            <div class="span6 offset3">
                <p class="lead"><?= $errorMessage ?></p>
            </div>
            <!-- /.span6 -->
        </div>
        <!-- /.row-fluid -->
        <div class="row-fluid">
            <div class="span6 offset3">
                <div class="row-fluid">
                    <?= CHtml::link($this->t('Go home'), $this->app()->homeUrl, [
                        'class' => 'btn btn-warning span6',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#main -->
</body>
</html>
