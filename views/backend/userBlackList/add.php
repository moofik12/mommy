<?php
/**
 * @var $this CController
 */

$this->pageTitle = $this->t('Adding a user to the blacklist');
?>


<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Adding a user to the blacklist'),
    'headerIcon' => 'icon-plus',
    'id' => 'add-user',
]);

echo $form->render();

$this->endWidget(); ?>
