<?php

use MommyCom\Model\Db\UserBlackListHistory;
use MommyCom\Model\Db\UserBlackListRecord;

/**
 * @var UserBlackListRecord $model
 */

$historyProvider = new CArrayDataProvider($model->getHistory(), array(
        'keyField' => 'timestamp',
        'pagination' =>  array('pageSize' => 30)
    )
);

$this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'dataProvider' => $historyProvider,
    'id' => "history_black_list_{$model->id}",
    'summaryText' => 'History {start} - {end} out of {count}',
    'columns' => array(
        array(
            'header' => $this->t('Date'),
            'name' => 'timestamp',
            'type' => 'datetime',
            'filterInputOptions' => array(
                'style' => 'width: 30px;'
            ),
        ),
        array(
            'header' => $this->t('Added/Edited'),
            'name' => 'person_name',
            'filterInputOptions' => array(
                'style' => 'width: 30px;'
            ),
        ),
        array(
            'header' => $this->t('Status'),
            'name' => 'status',
            'value' => function($model) {
                /** @var UserBlackListHistory  $model */
                return $model->getStatusReplacement($this->t('not defined'));
            }
        ),
        array(
            'header' => $this->t('Comments'),
            'name' => 'comment',
            'filterInputOptions' => array(
                'style' => 'width: 30px;'
            ),
        ),
        array(
            'header' => $this->t('User'),
            'name' => 'user_id',
            'filterInputOptions' => array(
                'style' => 'width: 30px;'
            ),
        ),
    ),
));
