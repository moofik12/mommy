<?php
/**
 * @var $this CController
 */

$this->pageTitle = $this->t('Change user data in the black list');
?>


<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Change user data in the black list'),
    'headerIcon' => 'icon-plus',
    'id' => 'add-user',
]);

echo $form->render();

$this->endWidget(); ?>
