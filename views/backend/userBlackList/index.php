<?php

use MommyCom\Model\Db\UserBlackListRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $dataProvider CActiveDataProvider
 */

$this->pageTitle = $this->t('Blacklist');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('User database'),
    'headerIcon' => 'icon-edit',
]); ?>

<?php $this->widget('bootstrap.widgets.TbButton', [
    'type' => 'primary',
    'label' => $this->t('Add user'),
    'url' => $this->createUrl('add'),
    'htmlOptions' => [
        'target' => '_blank',
    ],
]); ?>

<?php
/* @var $data UserRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Users {start} - {end} out of {count}',
//    'filterSelector' => '{filter}, #pageSize',
    'columns' => [
        [
            'name' => 'id',
            'filterInputOptions' => [
                'style' => 'width: 30px;',
            ],
        ],
        [
            'name' => 'searchUserEmail',
            'value' => '$data->user->email',
        ],
        [
            'name' => 'comment',
        ],
        [
            'name' => 'status',
            'filter' => UserBlackListRecord::statuses(),
            'value' => function ($data) {
                /** @var UserBlackListRecord $data */
                return $data->getStatusReplacement();
            },
        ],
        [
            'type' => 'raw',
            'header' => $this->t('History'),
            'value' => function ($model) {
                /** @var UserBlackListRecord $model */
                return CHtml::link($this->t('Edit history'), '', [
                    'name' => "history-{$model->id}",
                    'data-toggle' => 'popover',
                    'data-trigger' => 'click',
                    'data-placement' => 'left',
                    'data-html' => 'true',

                    'data-title' => $this->t('History'),
                    'data-content' => $this->renderPartial('_historyPopover',
                        ['model' => $model], true),

                    'style' => 'cursor: pointer',
                ]);
            },
        ],
        ['name' => 'updated_at', 'type' => 'humanTime', 'filter' => false],
        ['name' => 'created_at', 'type' => 'humanTime', 'filter' => false],
        [
            'class' => ButtonColumn::class,
            'template' => '{update} {delete}',
            'openNewWindow' => false,
        ],
    ],
]);

$this->endWidget(); ?>
