<?php

use MommyCom\Model\Db\BrandRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/* @var $this \MommyCom\Service\BaseController\BackController */

$this->pageTitle = $this->t('Brands');
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Brand database'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['brand/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Brands') . ' {start} - {end} ' . 'from' . ' {count}',
    'filterSelector' => '{filter}, #filterForm :input',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span1']],
        [
            'name' => 'logo_fileid',
            'type' => 'raw',
            'filter' => [$this->t('No'), $this->t('Available')],
            'htmlOptions' => ['class' => 'span3'],
            'value' => function ($data) {
                /** @var BrandRecord $data */
                return $data->logo->isEmpty
                    ? $this->t('No') : CHtml::image($data->logo->getThumbnail('small150brand')->url);
            },
        ],
        ['name' => 'name', 'htmlOptions' => ['class' => 'span4']],
        [
            'name' => 'aliasesStrings',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span4'],
        ],
        [
            'class' => ButtonColumn::class,
        ],
    ],
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['brand/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
