<?php
/* @var $this \MommyCom\Service\BaseController\BackController */

/* @var $form TbForm */
$this->pageTitle = $this->t('Brands');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Edit brand'),
    'headerIcon' => 'icon-edit',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
