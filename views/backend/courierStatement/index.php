<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Shipping documents');

$request = $this->app()->request;

$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Documents'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'summaryText' => $this->t('Statements {start} - {end} out of {count}'),
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],

        ['name' => 'positionCount', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],

        ['name' => 'totalPrice', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],

        ['name' => 'user.fullname'],
        ['name' => 'created_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span3']],
        ['name' => 'updated_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span3']],
        [
            'class' => 'CLinkColumn',
            'htmlOptions' => ['target' => '_blank'],
            'header' => $this->t('View'),
            'label' => $this->t('View'),
            'urlExpression' => function ($data) {
                /* @var $data OrderRecord */
                return $this->app()->createUrl('courierStatement/view', ['id' => $data->id]);
            },
        ],
    ],
]); ?>
<?php $this->endWidget() ?>
