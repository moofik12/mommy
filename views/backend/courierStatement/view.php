<?php

use MommyCom\Model\Db\StatementRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model StatementRecord */
/* @var $ordersProvider CActiveDataProvider */

$this->pageTitle = $this->t('Shipping');

$request = $this->app()->request;

$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
?>



<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Delivery-Acceptance Act for courier'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $ordersProvider,
    'filter' => $ordersProvider->model,
    'summaryText' => 'Orders {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form select',
    'columns' => [
        ['name' => 'id', 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'positionCount', 'filter' => false],
        ['name' => 'price_total', 'type' => 'number', 'filter' => false],
        ['name' => 'deliveryPrice', 'type' => 'number', 'filter' => false],
        ['name' => 'ordered_at', 'type' => 'datetime', 'filter' => false, 'headerHtmlOptions' => ['class' => 'span2']],
        ['name' => 'processingStatusReplacement', 'filter' => false],
    ],
]); ?>

<div class="form-actions">
    <?= CHtml::link($this->t('Print Delivery-Acceptance Act'), ['labelCourierPrinter/statement', 'id' => $model->id], [
        'target' => '_blank',
        'class' => 'btn btn-link',
    ]) ?>
    <?= CHtml::link($this->t('Print shipping documents'), ['labelCourierPrinter/documents', 'id' => ArrayUtils::getColumn($ordersProvider->getData(), 'id')], [
        'target' => '_blank',
        'class' => 'btn btn-link',
    ]) ?>
</div>
<?php $this->endWidget() ?>
