<?php

use MommyCom\Model\Db\CallbackRecord;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;

/**
 * @var $providerHistory CActiveDataProvider
 * @var $model CallbackRecord
 */

//bonus
/** @var  $shopBonusPoints ShopBonusPoints */
$shopBonusPoints = new ShopBonusPoints();
if ($model->user) {
    $shopBonusPoints->init($model->user);
}
?>
<div class="row">
    <div class="span3">
        <h4><?= $this->t('Data') . ' ' . $this->t('Call back Request') ?></h4>
        <?php
        //данные пользователя
        if (isset($model)) {
            $this->widget('bootstrap.widgets.TbDetailView', [
                'data' => $model,
                'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                'attributes' => [
                    [
                        'label' => '#',
                        'name' => 'id',
                    ],
                    'telephone',
                    [
                        'name' => 'status',
                        'value' => $model->statusReplacement(),
                    ],
                    [
                        'type' => 'raw',
                        'label' => $this->t('History'),
                        'name' => 'updated_at',
                        'value' => CHtml::link($this->t('Order editing history'), '', [
                            'name' => "history-{$model->id}",
                            'data-toggle' => 'popover',
                            'data-trigger' => 'hover',
                            'data-placement' => 'right',
                            'data-html' => 'true',

                            'data-title' => $this->t('History'),
                            'data-content' => $this->renderPartial('_statusHistoryPopover',
                                ['dataHistory' => $model->getProcessingStatusHistory()], true),
                        ]),
                    ],
                    [
                        'label' => $this->t('Last edit'),
                        'name' => 'updated_at',
                        'value' => $this->app()->getDateFormatter()->formatDateTime($model->updated_at),
                    ],
                    [
                        'label' => $this->t('Left a request'),
                        'name' => 'created_at',
                        'value' => $this->app()->getDateFormatter()->formatDateTime($model->created_at),
                    ],
                ],
                'htmlOptions' => [
                ],
            ]);
        } else {
            echo $this->t('No result data') . '!';
        }
        ?>
    </div>
    <div class="span3">
        <h4><?= $this->t('Data on users') ?></h4>
        <?php
        //данные пользователя
        if (isset($model->user)) {
            $this->widget('bootstrap.widgets.TbDetailView', [
                'data' => $model->user,
                'type' => [TbDetailView::TYPE_STRIPED, TbDetailView::TYPE_BORDERED],
                'attributes' => [
                    [
                        'label' => 'ID',
                        'name' => 'id',
                    ],
                    [
                        'label' => $this->t('Name'),
                        'name' => 'name',
                    ],
                    [
                        'label' => $this->t('Last name '),
                        'name' => 'surname',
                    ],
                    [
                        'label' => $this->t('Bonuses'),
                        'name' => 'bonus',
                        'value' => $this->t('on the account ') . ' ' .
                            Yii::t('app', '{n} bonus | {n} bonuses |  {n} bonuses', $shopBonusPoints->getAvailableToSpend()),
                    ],
                    'email',
                    'telephone',
                    'address',
                ],
                'htmlOptions' => [
                ],
            ]);
        } else {
            echo $this->t('No result data') . '!';
        }
        ?>
    </div>
    <div class="span4">
        <h5><?= $this->t('Statistics on previous orders') .
            '(' . $this->t('by phone number') . ')' ?></h5>
        <?php $buyStatistics = $model->user->getBuyStatistics() ?>
        <?php $this->widget('bootstrap.widgets.TbDetailView', [
            'data' => $buyStatistics,
            'attributes' => [
                'orderCount' => [
                    'label' => $this->t('Orders dispatched'),
                    'value' => $buyStatistics['orderCount'],
                ],
                'buyoutUnPayedCount' => [
                    'label' => $this->t('In the payment queue'),
                    'value' => $buyStatistics['buyoutUnPayedCount'],
                ],
                'buyoutPayedCount' => [
                    'label' => $this->t('Paid'),
                    'value' => $buyStatistics['buyoutPayedCount'],
                ],
                'buyoutCancelledCount' => [
                    'label' => $this->t('Not paid and wouldn\'t be paid'),
                    'value' => $buyStatistics['buyoutCancelledCount'],
                ],
                'returns' => [
                    'label' => $this->t('Number of returns'),
                    'value' => $buyStatistics['returns'],
                ],
                'telephones' => [
                    'label' => $this->t('Phone numbers used'),
                    'value' => implode(', ', $buyStatistics['telephones']),
                ],
                'userEmails' => [
                    'label' => $this->t('E-mail addresses used '),
                    'value' => implode(', ', $buyStatistics['userEmails']),
                ],
            ],
        ]) ?>
    </div>
</div>

<h5> <?= $this->t('History') ?> </h5>
<?php
$this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $providerHistory,
    'columns' => [
        ['name' => 'id', 'header' => '#', 'htmlOptions' => ['class' => 'span1']],
        [
            'name' => 'telephone',
            'htmlOptions' => ['class' => 'span3'],
        ],
        [
            'name' => 'status',
            'value' => function ($data) {
                /** @var CallbackRecord $data */
                return $data->statusReplacement();
            },
            'htmlOptions' => ['class' => 'span2'],
        ],
        [
            'name' => 'comment_callcenter',
        ],
        [
            'header' => $this->t('Left a request'),
            'name' => 'created_at',
            'type' => 'humanTime',
            'htmlOptions' => ['class' => 'span2'],
        ],
    ],
]); ?>
