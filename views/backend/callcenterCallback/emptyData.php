<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this CController
 * @var $data OrderRecord
 */

$this->pageTitle = $this->t('Call back Request');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Call back Request'),
]); ?>

<div class="alert alert-info">
    <h4> <?= $this->t('No data to process') ?> </h4>
</div>
<?= CHtml::openTag('label', ['title' => $this->t('Automatically check for new data')]) ?>
<?= $this->t('Automatically check for new data') ?>
<?= CHtml::checkBox('autoUpdate', true, ['style' => 'vertical-align: top;']) ?>
<?= CHtml::closeTag('label') ?>

<?php $this->endWidget(); ?>

<?php

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$cs->registerScript('notUpdateProcessing', '
    window.location.hash = "empty"; //не продливать редактирование
', CClientScript::POS_BEGIN);

$cs->registerScript('checkQueue', '
    var autoUpdateCheckBox = $("#autoUpdate");
    var d = $.Deferred();
    var startProcess = true;

    var checkNewOrders = function() {
        if (autoUpdateCheckBox.prop("checked") && d.state() != "rejected") {
            $.ajax({
                url: "' . $this->createUrl('callcenterCallback/ajaxCheckQueue') . '",
                beforeSend: function(jqXHR, settings) {
                    startProcess = false;
                },
            })
            .success(function(data, textStatus) {
                if (!data.errors) {
                    if(confirm("' . $this->t('There is a new call back. Check it?') . '")) {
                        d.resolve();
                    } else {
                        startProcess = true;
                    }
                } else {
                    startProcess = true;
                }
            })
            .error(function(XHR) {
                d.reject();
            });
        }
    }

    setInterval(function(){
        if (startProcess) {
            checkNewOrders();
        }

    }, 1000*30);

    d.done(function(){
        window.location.href = "' . $this->createUrl('callcenterCallback/queue') . '";
    })
    .fail(function(){
        autoUpdateCheckBox.prop("checked", false).prop("disabled", "disabled");
        alert("' . $this->t('Error. Further verification is not possible!') . '");
    });
');
?>
