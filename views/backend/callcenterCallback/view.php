<?php

use MommyCom\Controller\Backend\CallcenterCallbackController;
use MommyCom\Model\Db\CallbackRecord;
use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $this CallcenterCallbackController
 * @var $model CallbackRecord
 * @var $form Form
 * @var $providerHistory CActiveDataProvider
 */

$this->pageTitle = $this->t('Call back Request');

?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Call back Request'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php
$this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        ['name' => 'id', 'label' => '#'],
        [
            'label' => 'Name',
            'name' => 'user.name',
        ],
        [
            'label' => 'Last name ',
            'name' => 'user.surname',
        ],
        [
            'label' => 'E-mail',
            'name' => 'user.email',
        ],
        'telephone',
        [
            'name' => 'status',
            'value' => $model->statusReplacement(),
        ],
        'comment_callcenter',
        [
            'type' => 'raw',
            'label' => 'History',
            'name' => 'updated_at',
            'value' => CHtml::link('Edit history', '', [
                'name' => "history-{$model->id}",
                'data-toggle' => 'popover',
                'data-trigger' => 'hover',
                'data-placement' => 'right',
                'data-html' => 'true',

                'data-title' => 'History',
                'data-content' => $this->renderPartial('_statusHistoryPopover',
                    ['dataHistory' => $model->getProcessingStatusHistory()], true),
            ]),
        ],
        [
            'label' => 'Last edit',
            'name' => 'updated_at',
            'value' => $this->app()->getDateFormatter()->formatDateTime($model->updated_at),
        ],
        [
            'label' => 'Left a request',
            'name' => 'created_at',
            'value' => $this->app()->getDateFormatter()->formatDateTime($model->created_at),
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
