<?php
/**
 * @var $this CController
 * @var $form TbForm
 */
$this->beginWidget('bootstrap.widgets.TbModal', [
    'htmlOptions' => [
        'id' => 'recall-time-set',
    ],
]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?= $this->t('Time of the next call') ?></h4>
    </div>
<?php
echo CHtml::openTag('div', ['class' => 'modal-body', 'style' => 'overflow-y: visible;']);

echo $form->elements['timeString'];
echo $form->renderElement('postponed');

echo CHtml::closeTag('div');

//end modal
$this->endWidget();
