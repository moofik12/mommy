<?php

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\CallbackRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * @var $dataHistory array
 * @var $this CController
 */
?>
<table class="items table table-striped table-bordered table-hover">
    <th style="text-align: center"><?= $this->t('Date') ?></th>
    <th style="text-align: center"><?= $this->t('Status') ?></th>
    <th style="text-align: center"><?= $this->t('Operator') ?></th>
    <?php
    foreach ($dataHistory as $time => $data) {
        /** @var AdminUserRecord $adminModel */
        $adminModel = isset($data['admin_id']) ? AdminUserRecord::model()->findByPk($data['admin_id']) : null;
        /** @var  UserRecord $userModel */
        $userModel = isset($data['user_id']) ? UserRecord::model()->findByPk($data['user_id']) : null;

        $status = isset($data['status']) ? CHtml::value(CallbackRecord::statusReplacements(), $data['status']) : '';
        $admin = $adminModel !== null ? $adminModel->login . "<br>(" . $adminModel->fullname . ')' : '';
        $user = $userModel !== null ? $userModel->email . ' (' . $this->t('User') . ')' : '';

        echo '<tr>';
        echo '<td>' . $this->app()->dateFormatter->formatDateTime($time, 'medium', 'short') . '</td>';
        echo '<td>' . $status . '</td>';
        echo '<td>' . $admin . $user . '</td>';
        echo '</tr>';
    }
    ?>
</table>
