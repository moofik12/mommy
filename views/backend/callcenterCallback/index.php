<?php

use MommyCom\Controller\Backend\CallcenterCallbackController;
use MommyCom\Model\Db\CallbackRecord;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this CallcenterCallbackController
 * @var $provider CActiveDataProvider
 */

$this->pageTitle = $this->t('Callbacks');
/** @var CallbackRecord $model */
$model = $provider->model;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Callbacks'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php $this->renderPartial('_filter', ['model' => $model]) ?>

<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $model,
    'filterSelector' => '{filter}, form input',
    'columns' => [
        ['name' => 'id', 'htmlOptions' => ['class' => 'span1']],
        [
            'header' => $this->t('Request'),
            'filter' => false,
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'name' => 'id',
            'url' => $this->createUrl('callcenterCallback/view'),
            'value' => function ($data) {
                /** @var $data CallbackRecord */
                return $this->app()->getDateFormatter()->formatDateTime($data->created_at) .
                    ' ' . $this->t('View more') . ' ';
            },
            'afterAjaxUpdate' => 'js:function(tr,rowid,data){
                jQuery("[data-toggle=popover]").popover();
            }',
            'htmlOptions' => ['class' => 'span3'],
        ],
        [
            'header' => $this->t('User'),
            'value' => function ($data) {
                /** @var $data CallbackRecord */
                if ($data->user) {
                    /** @var UserRecord $user */
                    $user = $data->user;
                    return CHtml::encode("$user->fullname ($user->email)");
                }

                return '';
            },
        ],
        [
            'name' => 'telephone',
        ],
        [
            'type' => 'raw',
            'name' => 'status',
            'filter' => $model->statusReplacements(),
            'value' => function ($data) {
                /** @var CallbackRecord $data */
                $status = $data->statusReplacement();

                if ($data->status == CallbackRecord::STATUS_POSTPONED) {
                    $status .= ' ';
                    $status .= CHtml::tag('span', ['class' => 'label label-warning'],
                        $this->t('Call back') . ': ' . $this->app()->dateFormatter->formatDateTime($data->next_callback_at)
                    );
                }

                return $status;
            },
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {update}',
            'updateButtonUrl' => '\Yii::app()->controller->createUrl("processing",array("id"=>$data->primaryKey))',
        ],
    ],
]); ?>

<?php $this->endWidget() ?>
