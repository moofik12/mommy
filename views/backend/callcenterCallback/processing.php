<?php

use MommyCom\Controller\Backend\CallcenterCallbackController;
use MommyCom\Model\Db\CallbackRecord;
use MommyCom\YiiComponent\Backend\Form\Form;

/**
 * @var $this CallcenterCallbackController
 * @var $model CallbackRecord
 * @var $form Form
 * @var $providerHistory CActiveDataProvider
 */

$this->pageTitle = $this->t('Call back Request');

?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Call back Request'),
    'headerIcon' => 'icon-th-list',
]); ?>

<?php
//форма

echo $form->renderBegin();
?>
<div class="new-item-buttons text-right">
    <?= $form->renderElement('processed') ?>
    <?= $form->renderElement('modal') ?>
    <?= $form->renderElement('canceled') ?>
</div>

<?php $this->renderPartial('_processingModal', ['form' => $form]); ?>
<?= $form->renderElement('callcenterComment') ?>
<?= $form->renderEnd() ?>

<?php $this->renderPartial('_userData', ['model' => $model, 'providerHistory' => $providerHistory]) ?>
<?php $this->endWidget() ?>
