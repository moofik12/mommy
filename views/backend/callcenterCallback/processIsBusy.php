<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $this CController
 * @var $data OrderRecord
 */

$this->pageTitle = $this->t('Call back Request');

$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Call back Request'),
    'headerIcon' => 'icon-th-list',
    'id' => 'contentbox',
]); ?>

<div class="alert alert-error">
    <h4> <?= $this->t('At this time, the order is being processed by the administrator') ?>:
        <?= isset($model->processingAdmin) ? $model->processingAdmin->login . ' (' . $model->processingAdmin->fullname . ').' : '' ?>
    </h4>
</div>

<?php $this->endWidget(); ?>
