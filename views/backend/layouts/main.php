<?php

use MommyCom\Model\Db\CallbackRecord;
use MommyCom\Model\Db\CallcenterTaskRecord;
use MommyCom\Model\Db\FeedbackRecord;
use MommyCom\Model\Db\OrderBankRefundRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PayFromSettlementAccountRecord;
use MommyCom\Model\Db\PayGatewayRecord;
use MommyCom\Model\Db\TaskBoardRecord;
use MommyCom\Model\Db\UserPartnerOutputRecord;
use MommyCom\Widget\Backend\LeftMenu;

$user = $this->app()->user;
?>
<!doctype html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div id="wrap">
    <!-- BEGIN TOP BAR -->
    <div id="top">
        <!-- .navbar -->
        <div style="padding-top: 40px;"></div>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <?= CHtml::link($this->app()->name, ['/index'], ['class' => 'brand']) ?>
                    <!-- .topnav -->
                    <div class="btn-toolbar topnav">
                        <?php $this->widget('bootstrap.widgets.TbButtonGroup', [
                            'buttons' => [
                                [
                                    'icon' => 'icon-bell',
                                    'label' => TaskBoardRecord::model()->userId($this->app()->user->id)->archived(0)->count(),
                                    'url' => ['taskBoard/all'],
                                    'type' => 'warning',
                                    'htmlOptions' => [
                                        'data-placement' => 'bottom',
                                        'data-original-title' => $this->t('All tasks'),
                                        'rel' => 'tooltip',
                                    ],
                                ],
                            ],
                        ]); ?>
                        <?php $this->widget('bootstrap.widgets.TbButtonGroup', [
                            'buttons' => [
                                [
                                    'icon' => 'icon-resize-horizontal',
                                    'label' => '',
                                    'url' => ['auth/logout'],
                                    'type' => 'success',
                                    'htmlOptions' => [
                                        'id' => 'changeSidebarPos',
                                        'data-placement' => 'bottom',
                                        'data-original-title' => 'Show / Hide Sidebar',
                                        'rel' => 'tooltip',
                                    ],
                                ],
                            ],
                        ]); ?>

                        <?php $this->widget('bootstrap.widgets.TbButtonGroup', [
                            'buttons' => [
                                [
                                    'icon' => 'icon-off',
                                    'label' => '',
                                    'url' => ['auth/logout'],
                                    'type' => 'inverse',
                                    'htmlOptions' => [
                                        'data-placement' => 'bottom',
                                        'data-original-title' => 'Logout',
                                        'rel' => 'tooltip',
                                    ],
                                ],
                            ],
                        ]); ?>

                    </div>
                    <!-- /.topnav -->
                    <div class="nav-collapse collapse">
                        <!-- .nav -->
                        <?php $this->widget('bootstrap.widgets.TbMenu', [
                            'items' => [
                                [
                                    'label' => $this->t('Main page'),
                                    'url' => ['/index'],
                                ],
                            ],
                        ]) ?>
                        <!-- /.nav -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.navbar -->
    </div>
    <!-- END TOP BAR -->


    <!-- BEGIN HEADER.head -->
    <header class="head">
        <div class="search-bar">
            <div class="row-fluid">
                <div class="span12">
                    <div class="search-bar-inner">
                        <a id="menu-toggle" href="#menu" data-toggle="collapse"
                           class="accordion-toggle btn btn-inverse visible-phone"
                           rel="tooltip" data-placement="bottom" data-original-title="Show/Hide Menu">
                            <i class="icon-sort"></i>
                        </a>

                        <form class="main-search" style="visibility: hidden">
                            <input class="input-block-level" type="text" placeholder="Live search...">
                            <button id="searchBtn" type="submit" class="btn btn-inverse"><i class="icon-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- ."main-bar -->
        <div class="main-bar">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <h3><i class="icon-home"></i>
                            <?= $this->pageTitle ?>
                        </h3>
                    </div>
                </div>
                <!-- /.row-fluid -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.main-bar -->
    </header>
    <!-- END HEADER.head -->

    <!-- BEGIN LEFT  -->
    <div id="left">
        <!-- .user-media -->
        <div class="media user-media hidden-phone">
            <div class="media-body hidden-tablet">
                <h5 class="media-heading"><?= $this->app()->user->name ?></h5>
                <ul class="unstyled user-info">
                    <li><?= $this->t('Administrator') ?></li>
                    <li><?= $this->t('Last login') ?> : <br/>
                        <small>
                            <i class="icon-calendar"></i>
                            <?= $this->app()->user->isGuest ? '' : $this->app()->format->humanTime($this->app()->user->getModel()->updated_at); ?>
                        </small>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.user-media -->

        <!-- BEGIN MAIN NAVIGATION -->

        <?php $this->widget(LeftMenu::class, [
            'id' => 'menu',
            'activateParents' => true,
            'checkAccess' => true,
            'items' => [
                ['label' => $this->t('Main page'), 'url' => ['/index']],
                [
                    'label' => $this->t('Task manager'), 'icon' => 'question-sign',
                    'items' => [
                        ['label' => $this->t('Brand managers'), 'url' => ['taskBoard/managers']],
                        ['label' => 'CallCenter', 'url' => ['taskBoard/callCenter']],
                        ['label' => $this->t('Warehouse'), 'url' => ['taskBoard/storage']],
                        ['label' => $this->t('Coordination'), 'url' => ['taskBoard/coordination']],
                        ['label' => $this->t('Search'), 'url' => ['taskBoard/search']],
                        ['label' => $this->t('Archive'), 'url' => ['taskBoard/archive']],
                    ],
                ],
                [
                    'label' => $this->t('Warehouse'), 'icon' => 'home',
                    'items' => [
                        ['label' => $this->t('Products'), 'url' => ['warehouse/index']],
                        ['label' => $this->t('Picking'), 'url' => ['warehouse/productReception']],
                        ['label' => $this->t('Product disposals'), 'url' => ['warehouse/productCancellation']],
                        ['---'],
                        ['label' => $this->t('Supplier Return'), 'url' => ['warehouseStatementReturn/readyToReturn']],
                        ['label' => $this->t('Return Documentation'), 'url' => ['warehouseStatementReturn/index']],
                        ['---'],
                        ['label' => $this->t('Create fake flash-sale'), 'url' => ['event/createFake']],
                    ],
                ],

                [
                    'label' => $this->t('Packing'), 'icon' => 'home',
                    'items' => [
                        ['label' => $this->t('Packing'), 'url' => ['storekeeperOrder/index']],
                        ['label' => $this->t('Answer pending'), 'url' => ['storekeeperOrder/mailUnReady']],
                        ['label' => $this->t('Shipping'), 'url' => ['storekeeperOrder/mailReady']],
                        ['label' => $this->t('Dispatched'), 'url' => ['storekeeperOrder/mailed']],
                    ],
                ],

                [
                    'label' => $this->t('Shipping documents'), 'icon' => 'home',
                    'items' => [
                        ['label' => $this->t('Courier'), 'url' => ['courierStatement/index']],
                    ],
                ],

                [
                    'label' => $this->t('Order tracking'), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('Tracking'), 'url' => ['orderTracking/index']],
                        ['label' => $this->t('Payments'), 'url' => ['orderBuyoutTracking/index']],
                    ],
                ],

                [
                    'label' => $this->t('Payment confirmation'), 'icon' => 'icon-star',
                    'url' => ['orderBuyout/index'],
                ],

                [
                    'label' => $this->t('3PL Inpayments'), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('List'), 'url' => ['moneyControl/index']],
                        ['label' => $this->t('Orders'), 'url' => ['moneyControl/orders']],
                    ],
                ],

                [
                    'label' => $this->t('Order payments'), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('Orders expected to be paid'), 'url' => ['orderPay/wait']],
                        ['---'],
                        ['label' => $this->t('Payments through c/a'), 'url' => ['orderPay/paidFromSettlementAccount'],
                            'countType' => LeftMenu::COUNT_TYPE_IMPORTANT,
                            'count' => PayFromSettlementAccountRecord::model()
                                ->statusIs(PayFromSettlementAccountRecord::STATUS_NOT_PROCESSED)
                                ->count(),
                        ],
                        ['label' => $this->t('Problematic payments'), 'url' => ['orderPay/paidErrors'],
                            'countType' => LeftMenu::COUNT_TYPE_IMPORTANT,
                            'count' => PayGatewayRecord::model()
                                ->payStatusIn(PayGatewayRecord::getErrorPayStatuses())
                                ->count(),
                        ],
                        ['---'],
                        ['label' => $this->t('New returns'), 'url' => ['orderPay/bankRefundConfirmReady'],
                            'count' => OrderBankRefundRecord::model()
                                ->status(OrderBankRefundRecord::STATUS_UNCONFIGURED)
                                ->count(),
                        ],
                        ['label' => $this->t('Confirmation of returns'), 'url' => ['orderPay/bankRefundPayReady'],
                            'count' => OrderBankRefundRecord::model()
                                ->onlyPayReady()
                                ->count()],
                        ['---'],
                        ['label' => $this->t('Returns history'), 'url' => ['orderPay/bankRefund']],
                        ['label' => $this->t('History of payments through c/a'), 'url' => ['orderPay/paidFromSettlementAccountHistory']],
                        ['label' => $this->t('Transactions history'), 'url' => ['orderPay/paid']],
                        ['label' => $this->t('Logging of payments through the site'), 'url' => ['orderPay/logs']],
                    ],
                ],

                [
                    'label' => $this->t('Payments to suppliers '), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('Balance due'), 'url' => ['supplierPayment/readyPay']],
                        ['label' => $this->t('In the payment queue'), 'url' => ['supplierPayment/waitPay']],
                        ['label' => $this->t('Deliveries'), 'url' => ['supplierPayment/delivery']],
                        ['label' => $this->t('History'), 'url' => ['supplierPayment/history']],
                        ['---'],
                        ['label' => $this->t('Contracts'), 'url' => ['supplierPayment/contracts']],
                    ],
                ],

                [
                    'label' => $this->t('Dropshipping'), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('Balance due'), 'url' => ['relationPayment/toPay']],
                        ['label' => $this->t('All'), 'url' => ['relationPayment/index']],
                        ['---'],
                        ['label' => $this->t('Return requests'), 'url' => ['supplierOrderReturn/index']],
                    ],
                ],

                [
                    'label' => $this->t('Order monitoring '), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('Orders'), 'url' => ['orderControl/index']],
                    ],
                ],

                [
                    'label' => $this->t('Returns'), 'icon' => 'home',
                    'items' => [
                        ['label' => $this->t('New'), 'url' => ['orderReturn/index']],
                        ['label' => $this->t('Payment'), 'url' => ['orderReturn/payReady']],
                        ['---'],
                        ['label' => $this->t('History'), 'url' => ['orderReturn/history']],
                    ],
                ],

                [
                    'label' => $this->t('Products'), 'icon' => 'question-sign',
                    'items' => [
                        ['label' => $this->t('Product catalog'), 'url' => ['productCatalog/index']],
                        ['label' => $this->t('Products'), 'url' => ['product/index']],
                        ['label' => $this->t('Brands'), 'url' => ['brand/index']],
                        ['label' => $this->t('Sections'), 'url' => ['productSection/index']],
                    ],
                ],
                [
                    'label' => $this->t('Suppliers'), 'icon' => 'home',
                    'items' => [
                        ['label' => $this->t('Flash-sales'), 'url' => ['supplier/supplierList']],
                        ['label' => $this->t('Suppliers information'), 'url' => ['supplier/index']],
                    ],
                ],
                [
                    'label' => $this->t('For suppliers'),
                    'items' => [
                        ['label' => $this->t('Sale'), 'url' => ['supplierEvent/flashSales']],
                        ['label' => $this->t('Contract information'), 'url' => ['supplierEvent/contractInformation']],
                        ['label' => $this->t('Update profile'), 'url' => ['supplierEvent/updateProfile']],
                        ['label' => $this->t('Change password'), 'url' => ['supplierEvent/changePassword']],
                    ],
                ],

                [
                    'label' => $this->t('Flash-sales'), 'icon' => 'icon-star',
                    'items' => [
                        ['label' => $this->t('List'), 'url' => ['event/index']],
                    ],
                ],

                [
                    'label' => 'Callcenter', 'icon' => 'question-sign',
                    'items' => [
                        ['label' => $this->t('New Orders'), 'url' => ['callcenterOrder/new'],
                            'countType' => LeftMenu::COUNT_TYPE_IMPORTANT,
                            'count' => OrderRecord::model()
                                ->orderNew()
                                ->processingNotActive()
                                ->count(),
                        ],
                        ['label' => $this->t('Unconfirmed'), 'url' => ['callcenterOrder/notСonfirmed']],
                        ['label' => $this->t('Call back'), 'url' => ['callcenterOrder/repeat'],
                            'count' => OrderRecord::model()
                                ->orderRepeat()
                                ->processingNotActive()
                                ->count(),
                        ],
                        ['label' => $this->t('Call back Request'), 'url' => ['callcenterCallback/queue'],
                            'count' => CallbackRecord::model()
                                ->queue()
                                ->count(),
                        ],
                        ['label' => $this->t('Tasks'), 'url' => ['callcenterOrder/task'],
                            'count' => $model = CallcenterTaskRecord::model()
                                ->statusIs(CallcenterTaskRecord::STATUS_NEW)
                                ->statusIs(CallcenterTaskRecord::STATUS_POSTPONED)
                                ->statusIs(CallcenterTaskRecord::STATUS_REPEAT)
                                ->processingNotActive()
                                ->count(),
                        ],
                        ['label' => $this->t('Suspended tasks'), 'url' => ['callcenterOrder/waitingTask']],
                        ['label' => $this->t('Canceled orders'), 'url' => ['callcenterOrder/cancelled']],
                        ['---'],
                        ['label' => $this->t('Purchase history'), 'url' => ['callcenterOrder/history']],
                        ['label' => $this->t('Callback history'), 'url' => ['callcenterCallback/index']],
                    ],
                ],

                [
                    'label' => $this->t('Callcenter task management'), 'icon' => 'question-sign',
                    'items' => [
                        ['label' => $this->t('Incoming'), 'url' => ['callcenterTask/inbox'],
                            'count' => CallcenterTaskRecord::model()
                                ->inbox()
                                ->answerRead(false)
                                ->adminId($user->id)
                                ->count(),
                        ],
                        ['label' => $this->t('Outcoming'), 'url' => ['callcenterTask/outbox'],
                            'count' => CallcenterTaskRecord::model()
                                ->outbox()
                                ->adminId($user->id)
                                ->count(),
                        ],
                        ['label' => $this->t('List'), 'url' => ['callcenterTask/task']],
                        ['---'],
                        ['label' => $this->t('Task actions'), 'url' => ['callcenterTask/operation']],
                    ],
                ],

                [
                    'label' => $this->t('Mail-out'),
                    'items' => [
                        ['label' => 'PUSH-mail-outs', 'url' => ['distributionPush/index']],
                    ],
                ],

                [
                    'label' => $this->t('Statistics'), 'icon' => 'icon-tasks',
                    'items' => [
                        ['label' => $this->t('Orders (default)'), 'url' => ['statisticCustom/ordersBase']],
                        ['label' => $this->t('Orders'), 'url' => ['statisticCustom/orders']],
                        ['label' => $this->t('Users'), 'url' => ['statisticCustom/users']],
                        ['label' => $this->t('Settling an order'), 'url' => ['statisticCustom/ordersBuyout']],
                        ['label' => $this->t('Payments to suppliers '), 'url' => ['statisticCustom/suppliersPayment']],
                    ],
                ],

                [
                    'label' => $this->t('Reports'), 'icon' => 'icon-tasks',
                    'items' => [
                        ['label' => $this->t('Orders'), 'url' => ['reporting/orders']],
                        ['label' => $this->t('Order processing'), 'url' => ['reporting/orderProcessing']],
                    ],
                ],

                [
                    'label' => $this->t('Messages'),
                    'items' => [
                        ['label' => $this->t('E-mail messages'), 'url' => ['email/index']],
                        ['label' => $this->t('SMS messages'), 'url' => ['sms/index']],
                        ['label' => $this->t('Sending SMS'), 'url' => ['smsSend/index']],
                        ['label' => $this->t('Contacts'), 'url' => ['feedback/new'],
                            'count' => FeedbackRecord::model()
                                ->status(FeedbackRecord::STATUS_NEW)
                                ->count(),
                        ],
                    ],
                ],

                [
                    'label' => $this->t('Partner Program'),
                    'items' => [
                        ['label' => $this->t('Partners'), 'url' => ['userPartner/index']],
                        ['label' => $this->t('Finances'), 'url' => ['userPartner/finance'],
                            'count' => UserPartnerOutputRecord::model()
                                ->status(UserPartnerOutputRecord::STATUS_WAIT)
                                ->count(),],
                        ['label' => $this->t('Returns'), 'url' => ['userPartner/return']],
                        ['label' => $this->t('Order statistics'), 'url' => ['userPartner/statistic']],
                        ['label' => $this->t('Registration statistics'), 'url' => ['userPartner/resource']],
                    ],
                ],
                [
                    'label' => $this->t('Users'),
                    'items' => [
                        ['label' => $this->t('List'), 'url' => ['user/index']],
                        ['label' => $this->t('Blacklist'), 'url' => ['userBlackList/index']],
                        ['label' => $this->t('Bonuses'), 'url' => ['bonuspoints/index']],
                        ['label' => $this->t('Bonus Flash-sales for newsletters'), 'url' => ['bonuspointsPromo/index']],
                        ['label' => $this->t('Promocodes'), 'url' => ['promocode/index']],
                        ['label' => $this->t('There are still items left in the user\'s shopping bag'), 'url' => ['userUnformedCart/index']],
                    ],
                ],

                [
                    'label' => $this->t('Static pages'),
                    'items' => [
                        ['label' => $this->t('List'), 'url' => ['staticPage/index']],
                        ['label' => $this->t('Categories'), 'url' => ['staticPageCategory/index']],
                    ],
                ],

                [
                    'label' => $this->t('Certificate'),
                    'items' => [
                        ['label' => $this->t('Categories'), 'url' => ['helperPage/category']],
                        ['label' => $this->t('Pages'), 'url' => ['helperPage/page']],
                    ],
                ],

                [
                    'label' => $this->t('Administration'),
                    'items' => [
                        ['label' => $this->t('Administrators'), 'url' => ['admin/index']],
                        ['label' => $this->t('Roles'), 'url' => ['adminRole/index']],
                        ['---'],
                        ['label' => $this->t('Adding Roles to the Task Manager'), 'url' => ['taskBoardUserRoleConnection/index']],
                        ['label' => $this->t('Logging user actions'), 'url' => ['userGlobalLogger/index']],
                        ['label' => $this->t('Access to tablet version'), 'url' => ['tabletUser/index']],
                        ['label' => $this->t('IP Block'), 'url' => ['ipBan/index']],
                        ['label' => $this->t('Logon statistics'), 'url' => ['adminAuthlog/index']],
                    ],
                ],
            ],
        ]) ?>
        <!-- END MAIN NAVIGATION -->
    </div>
    <!-- END LEFT -->

    <div id="content" class="">
        <!-- .outer -->
        <div class="container-fluid outer">
            <?php $this->widget('bootstrap.widgets.TbAlert', [
                'alerts' => [ // configurations per alert type
                    'error' => ['block' => true, 'fade' => true, 'closeText' => '×'], // success, info, warning, error or danger
                ],
            ]); ?>
            <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', [
                'links' => $this->breadcrumbs,
                'htmlOptions' => ['class' => 'body'],
            ]); ?>
            <div class="row-fluid">
                <div class="span12 inner">
                    <?= $content; ?>
                </div>
            </div>
        </div>

    </div>

    <!-- #push do not remove -->
    <div id="push"></div>
    <!-- /#push -->
    <div class="clearfix"></div>

</div>
<?php
// TODO :: think about optimize
if ($this->app()->user->checkAccess('taskBoard/notification')) {
    $this->renderPartial('/taskBoard/partial/_notification');
}
?>
<!-- BEGIN FOOTER -->
<div id="footer">
    <p><?= date('Y') ?> &copy; <?= $this->app()->name ?></p>
</div>
</body>
</html>
