<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body class="printable">
<?= CHtml::button($this->t('Print'), [
    'class' => 'printable-printbtn',
    'onclick' => new CJavaScriptExpression('
            window.print();
            return false;
        '),
]) ?>
<?= $content ?>
</body>
</html>
