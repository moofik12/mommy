<?php

use MommyCom\Model\Db\WarehouseStatementReturnRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Service\Deprecated\Currency2Words;

/**
 * @var $this BackController
 * @var $model WarehouseStatementReturnRecord
 */

$this->pageTitle = $this->t('Returns document - Print');
$app = $this->app();
$df = $app->dateFormatter;
$cf = $app->currencyFormatter;
$nf = $app->numberFormatter;
$cn = $this->app()->countries->getCurrency();
$time = time();
$c2w = Currency2Words::instance('ua_ru');

?>

<div class="printable-label printable-a4 label-invoice label-warehouse-statement">
    <div class="header"><?= $this->t('Return №') ?><?= $model->id ?>
        от <?= $df->format('d MMMM y', $model->created_at) ?></div>
    <div class="date-print"><?= $this->t('Print') ?>: <?= $df->format('dd.MM.y H:mm', $time) ?></div>
    <div class="header-sender"><?= $this->t('Consignor') ?>: <span class="text-important"><?= $this->app()->params['company'] ?></span></div>
    <div class="header-sender"><?= $this->t('Consignee') ?>: <span
                class="text-important"><?= $model->supplier->name ?></span></div>
    <div class="main-products">
        <table>
            <thead>
            <tr>
                <td><?= $this->t('№') ?></td>
                <td><?= $this->t('Vendor code') ?></td>
                <td><?= $this->t('Product') ?></td>
                <td><?= $this->t('Amount') ?></td>
                <td><?= $this->t('Price') ?></td>
                <td><?= $this->t('Cost') ?></td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td colspan="6">
                    <div class="total-print"><?= $this->t('Total') ?>:
                        <span class="text-important "><?= $cf->format($model->getProductsPricePurchase(), $cn->getName('short')) ?></span>
                    </div>
                </td>
            </tr>
            </tfoot>
            <tbody>
            <?php foreach ($model->getProducts() as $index => $product): ?>
                <tr>
                    <td><?= $index + 1 ?></td>
                    <td><?= \CHtml::encode($product->eventProduct->article) ?></td>
                    <td class="product"><?= \CHtml::encode($product->eventProduct->name) ?><br>
                        <span class="product-event"> <?= $this->t('Flash-sale') ?> №<?= $product->event->id ?>
                            : <?= \CHtml::encode($product->event->name) ?> </span>
                    </td>
                    <td> 1</td>
                    <td><?= $nf->formatDecimal($product->eventProduct->price_purchase) ?></td>
                    <td><?= $nf->formatDecimal($product->eventProduct->price_purchase) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div>
        <?= $c2w->convert($model->getProductsPricePurchase()) ?>
    </div>
    <div class="sings">
        <div class="sender">
            <?= $this->t('Consignor')?>______________________
        </div>
        <div class="receiving">
            <?= $this->t('Receiver-Consignee')?>______________________
        </div>
    </div>
</div>
