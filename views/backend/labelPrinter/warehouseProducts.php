<?php

use MommyCom\Model\Db\WarehouseProductRecord;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $provider CDataProvider */
$this->pageTitle = $this->t('Stock count - Print');
$app = $this->app();
$dateFormatter = $app->dateFormatter;
/* @var $dateFormatter CDateFormatter */
?>
<?php foreach ($provider->getData() as $index => $item): /* @var $item WarehouseProductRecord */ ?>
    <div class="printable-label warehouse-label">
        <div class="label-header">
            <?= $this->t('№ Product') ?>: <span class="highlight"><?= $item->id ?> <?php if ($item->return_id > 0) { ?> <?= $this->t('In') ?> <?php } ?></span>
            <?php if ($item->order_id > 0): ?>
                &nbsp;&nbsp;&nbsp;
                <?= $this->t('Order № ') ?>: <span class="hightlight"><?= $item->order_id ?>
                    (<?= $item->order->positionNumber ?>)</span>
            <?php endif ?>
        </div>
        <div class="label-content">
            <div class="label-description">
                <div>
                    <div class="pull-left"><?= $this->t('Order date') ?>
                        : <?= $dateFormatter->formatDateTime($item->requestnum, 'short', 'short') ?></div>
                    <div class="pull-left">&nbsp;&nbsp;&nbsp;</div>
                    <div class="pull-left"><?= $this->t('Print date') ?>: <?= $dateFormatter->formatDateTime(time(), 'short', 'short') ?></div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <?= $this->t('№ Product') ?>: <strong
                            class="highlight"><?= $item->id ?> <?php if ($item->return_id > 0) { ?> <?= $this->t('In') ?> <?php } ?></strong>
                </div>
                <?php if ($item->order_id > 0): ?>
                    <div>
                        <strong class="highlight">
                            <?= $this->t('Order№ ') ?>: <?= $item->order_id ?>
                        </strong>,
                        <strong>
                            <?= $this->t('Quantity of products') ?>: <?= $item->order->positionNumber ?>
                        </strong>
                    </div>
                <?php endif ?>
                <div>
                    <?= $this->t('Product') ?>:
                    <?= $item->event_product_id ?>,
                    <?= $item->product_id ?>,

                    <?php if (mb_strlen($item->eventProduct->size) > 0): ?>
                        <?= $item->eventProduct->sizeformatReplacement . ' ' . $item->eventProduct->size ?>,
                    <?php endif ?>

                    <?= $item->product->name ?>
                </div>
                <div><?= $this->t('Flash-sale') ?>: <?= $item->event->id ?>, <?= $item->event->name ?></div>
            </div>
            <div class="label-security-code">
                <?= $this->t('Verification Code') ?>: <strong class="highlight"><?= $item->getPackagingSecurityCode() ?></strong></div>
        </div>
        <div class="label-barcode">
            <?= \CHtml::image($this->createAbsoluteUrl('labelPrinter/barcode', ['id' => $item->id, 'height' => '50', 'width' => '2'])) ?>
        </div>
    </div>
    </div>
<?php endforeach ?>
