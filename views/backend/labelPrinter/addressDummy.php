<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model OrderRecord */
$this->pageTitle = $this->t('Nova Post - Address list - Print');
$app = $this->app();
$dateFormatter = $app->dateFormatter;
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $currencyFormatter */
$currencyFormatter = $app->currencyFormatter;

?>

<div class="printable-label delivery-label delivery-dummy">
    <div class="delivery-dummy-from">
        <span class="delivery-label-attribute"><?= $this->t('Shop') ?>:</span>
        <span class="delivery-label-value"><?= $this->app()->name ?></span>
    </div>
    <?php if ($model->mail_after > $model->ordered_at): ?>
        <div>
            <strong>
                <span class="delivery-label-attribute"><?= $this->t('Send after') ?>:</span>
                <span class="delivery-label-value"><?= $dateFormatter->formatDateTime($model->mail_after, 'short', false) ?></span>
            </strong>
        </div>
    <?php endif ?>
    <div class="delivery-dummy-type">
        <span class="delivery-label-attribute"><?= $this->t('Shipping method') ?></span>
        <span class="delivery-label-value"><?= $this->t('Nova Post') ?></span>
    </div>
    <div>
        <span class="delivery-label-attribute"><?= $this->t('Order') ?>:</span>
        <span class="delivery-label-value"><?= $model->idAligned ?>
            , <?= $dateFormatter->formatDateTime($model->ordered_at, 'short', false) ?></span>
    </div>
    <?php if ($model->bonuses > 0): ?>
        <div>
            <span class="delivery-label-attribute"><?= $this->t('Bonuses') ?>:</span>
            <span class="delivery-label-value"><?= $currencyFormatter->format($model->bonuses) ?></span>
        </div>
    <?php endif ?>
    <div>
        <span class="delivery-label-attribute"><?= $this->t('Quantity of products') ?>:</span>
        <span class="delivery-label-value"><?= $model->getPositionsCallcenterAcceptedNumber() ?></span>
    </div>
    <div>
        <span class="delivery-label-attribute"><?= $this->t('Declared value') ?>:</span>
        <span class="delivery-label-value"><?= $currencyFormatter->format($model->price) ?></span>
    </div>
    <div>
        <strong>
            <span class="delivery-label-attribute"><?= $this->t('To pay') ?>:</span>
            <span class="delivery-label-value"><?= $currencyFormatter->format($model->getPayPrice()) ?></span>
        </strong>
    </div>
    <div class="delivery-dummy-client">
        <span class="delivery-label-attribute"><?= $this->t('Addressee') ?>:</span>
        <span class="delivery-label-value">
            <?= $model->client_name ?> <?= $model->client_surname ?>, <?= $model->telephone ?>
        </span>
    </div>
    <div class="delivery-dummy-region">
        <span class="delivery-label-attribute"><?= $this->t('Address') ?>:</span>
        <span class="delivery-label-value"></span>
    </div>

    <div class="label-barcode">
        <?= CHtml::image($this->createAbsoluteUrl('labelPrinter/barcode', ['id' => $model->id, 'height' => '50', 'width' => '2'])) ?>
    </div>
</div>
