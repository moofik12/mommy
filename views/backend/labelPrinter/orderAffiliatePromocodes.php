<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\PromocodeRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model OrderRecord */
$this->pageTitle = $this->t('Promocodes - Print');
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.coupon')) . '/img/';

$sortCoupons = PromocodeRecord::model()->promocodeIn($model->getAffiliatePromocodes())->findAll();
usort($sortCoupons, function ($item1, $item2) {
    /** @var PromocodeRecord $item1 */
    /** @var PromocodeRecord $item2 */
    if ($item1->user_id == $item2->user_id) {
        return 0;
    }
    return $item1->user_id > $item2->user_id ? -1 : 1;
});
?>

<div class="page">
    <div class="main-blank">
        <div class="header">
            <div class="header-wrapper">
                <div class="header-order"><?= $model->id ?></div>
                <div class="head-logo"><img src="<?= $baseImgUrl . 'coupon-logo.png' ?>" alt=""></div>
                <div class="head-header">Подарочные Купоны</div>
                <div class="head-content">
                    <div class="head-label">Что это?</div>
                    <div class="head-text">Это четыре одноразовых скидочных купона для дальнейших покупок в
                        шоппинг-клубе MOMMY. Один для Вас и три для Ваших друзей.
                    </div>
                    <div class="head-label">Какую скидку они дают?</div>
                    <div class="head-text">Ваш друг получит скидку 5% на любой заказ, если использует этот купон.
                        Более того, <b>когда он оплатит покупку</b> в MOMMY, на Ваш Бонусный счет будет начислено 5% от
                        суммы его заказа<b>*</b>.
                    </div>
                    <div class="head-label">Могу ли я воспользоваться этими купонами?</div>
                    <div class="head-text">Вы можете воспользоваться только своим личным скидочным купоном на 6%.</div>
                </div>
            </div>
        </div>
        <div class="coupons">
            <?php foreach ($sortCoupons as $promocode) : ?>
                <div class="coupons-row-wrap">
                    <img class="coupon-cut-off" src="<?= $baseImgUrl . 'coupon-cut-off.png' ?>" alt="">
                    <div class="coupons-row">
                        <div class="coupon-cell-left">
                            <div class="coupon-cell coupon-personal">
                                <img class="coupon-sale" src="<?= $baseImgUrl . 'coupon-sale.png' ?>" alt="">
                                <div class="coupon-sale-text"><?= $promocode->percent ?>%</div>
                                <div class="coupon-title"><?= $promocode->user_id ? $this->t('Your personal coupon')
                                        : $this->t('For friends') ?></div>
                                <div class="coupon-main-text">Скидка: <?= $promocode->percent ?>%</div>

                                <div class="coupon-text">Срок действия:
                                    до <?= date('d.m.Y', $promocode->valid_until) ?></div>
                                <img class="coupon-cut-off-line" src="<?= $baseImgUrl . 'coupon-cut-off-line.png' ?>"
                                     alt="">
                            </div>
                        </div>
                        <div class="coupon-cell-right">
                            <div class="coupon-cell coupon-code">
                                <div class="coupon-title"><?= $this->t('Promocode') ?></div>
                                <div class="code"><?= $promocode->promocode ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <div class="notice">* - бонусы зачисляются не сразу, а спустя некоторое время после совершения оплаты</div>
    </div>
</div>
