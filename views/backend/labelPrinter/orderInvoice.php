<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\Deprecated\CurrencyFormatter;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model OrderRecord */
$this->pageTitle = $this->t('Invoice - Print');
$app = $this->app();
$dateFormatter = $app->dateFormatter;
/** @var CurrencyFormatter $currencyFormatter */
$currencyFormatter = $app->currencyFormatter;
?>


<div class="printable-label printable-a4 label-invoice">
    <div><?= $this->t('Order') ?>: <?= $model->idAligned ?></div>
    <div><?= $this->t('Client') ?>: <?= $model->client_name ?> <?= $model->client_surname ?></div>
    <div><?= $this->t('Order date') ?>: <?= $dateFormatter->formatDateTime($model->ordered_at, 'long') ?></div>
    <div class="label-invoice-products">
        <table>
            <thead>
            <tr>
                <td>№</td>
                <td></td>
                <td><?= $this->t('Price') ?></td>
                <td><?= $this->t('Quantity') ?></td>
                <td><?= $this->t('Total') ?></td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td colspan="6">
                    <div><?= $this->t('Bonuses') ?>
                        : <?= $currencyFormatter->format($model->bonuses) ?></div>
                    <div><?= $this->t('Discount') ?>
                        : <?= $currencyFormatter->format($model->discount) ?></div>
                    <div><?= $this->t('Cost') ?>
                        : <?= $currencyFormatter->format($model->getPrice()) ?></div>
                    <div><?= $this->t('Total cost') ?>
                        : <?= $currencyFormatter->format($model->getPayPrice()) ?></div>
                </td>
            </tr>
            </tfoot>
            <tbody>
            <?php foreach ($model->positionsCallcenterAccepted as $index => $position): ?>
                <tr>
                    <td><?= $index + 1 ?></td>
                    <td><?= $position->product->idAligned ?>, <?= $position->product->name ?></td>
                    <td><?= $currencyFormatter->format($position->price) ?></td>
                    <td><?= $position->number ?></td>
                    <td><?= $currencyFormatter->format($position->priceTotal) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
