<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Controller\Backend\LabelPrinterController;
use MommyCom\Service\CurrentTime;
use MommyCom\Service\Money\CurrencyFormatter;

/* @var $this LabelPrinterController */
/* @var $model EventRecord */
/* @var $products CDataProvider */
/* @var $logoUrl string */
/* @var $currencyFormatter CurrencyFormatter */

$this->pageTitle = $translator->t('Invoice - Print');
$total = 0;
?>

<div class="print-container">
    <div>
        <div class="pull-left">
            <div><img class="thumbnail" src="<?= $logoUrl ?>"/></div>
        </div>
        <div class="pull-right">
            <div><?= $model->supplier->name ?></div>
            <div><?= $model->supplier->address ?></div>
            <div><?= $model->supplier->phone ?></div>
            <div><?= $model->supplier->email ?></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="text-center"><?= $translator->t('Invoice') ?></div>
    <div>
        <div class="pull-left">
            <div><?= $translator->t('SupplierInvoiceText') ?></div>
        </div>
        <div class="pull-right">
            <div><?= $model->id ?></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="label-invoice-products">
        <table class="supplier-invoice">
            <thead>
            <tr>
                <td><?= $translator->t('№') ?></td>
                <td class="width-40"><?= $translator->t('Product') ?></td>
                <td><?= $translator->t('Stock number') ?></td>
                <td class="width-10"><?= $translator->t('Qty') ?></td>
                <td><?= $translator->t('Price') ?></td>
                <td><?= $translator->t('Amount') ?></td
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $index => $product): ?>
                <?php
                    if(empty($product->numberSold)) {
                        continue;
                    }
                ?>
                <?php
                $quantity = is_null($product->number_supplied) ? $product->numberSold : $product->number_supplied;
                $total += $quantity*round($product->price_purchase);
                ?>
                <tr>
                    <td class="width-10"><?= $index+1 ?></td>
                    <td class="width-40"><?= $product->name ?></td>
                    <td><?= $product->article ?></td>
                    <td class="width-10"><?= $quantity ?></td>
                    <td><?= $currencyFormatter->format(round($product->price_purchase)) ?></td>
                    <td><?= $currencyFormatter->format($quantity*round($product->price_purchase)) ?></td>
                </tr>
            <?php endforeach; ?>
                <tr class="total">
                    <td colspan="6">
                        <div><?= $translator->t('Total') ?>: <?= $currencyFormatter->format($total) ?></div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="requisites">
        <div class="pull-left">
            <div><?= CurrentTime::getDateTimeImmutable()->format('Y-m-d') ?></div>
        </div>
        <div class="pull-right">
            <div>&nbsp;</div>
            <div><?= $translator->t('Position') ?>______________________</div>
            <br/>
            <div><?= $translator->t('Signature') ?>________________</div>
            <br/>
            <div><?= $translator->t('Stamp') ?></div>
        </div>
    </div>
</div>