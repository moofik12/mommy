<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\BaseController\BackController;

/* @var $this BackController */
/* @var $model OrderRecord */
$this->pageTitle = $this->t('Courier - Address list - Print');
$app = $this->app();
$dateFormatter = $app->dateFormatter;
$currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
?>

<div class="printable-label delivery-label delivery-novaposta">
    <div class="delivery-novaposta-from">
        <span class="delivery-label-attribute"><? $this->t('Shop') ?>:</span>
        <span class="delivery-label-value"><?= $this->app()->name ?></span>
    </div>
    <?php if ($model->mail_after > $model->ordered_at): ?>
        <div>
            <strong>
                <span class="delivery-label-attribute"><?= $this->t('Send after') ?>:</span>
                <span class="delivery-label-value"><?= $dateFormatter->formatDateTime($model->mail_after, 'short', false) ?></span>
            </strong>
        </div>
    <?php endif ?>
    <div class="delivery-novaposta-type">
        <span class="delivery-label-attribute"><?= $this->t('Shipping method') ?>:</span>
        <span class="delivery-label-value"><?= $this->t('Courier') ?></span>
    </div>
    <div>
        <span class="delivery-label-attribute"><?= $this->t('Order') ?>:</span>
        <span class="delivery-label-value"><?= $model->idAligned ?>
            , <?= $dateFormatter->formatDateTime($model->ordered_at, 'short', false) ?></span>
    </div>
    <?php if ($model->bonuses > 0): ?>
        <div>
            <span class="delivery-label-attribute"><? $this->t('Bonuses') ?>:</span>
            <span class="delivery-label-value"><?= $currencyFormatter->format($model->bonuses, $cn->getName('short')) ?></span>
        </div>
    <?php endif ?>
    <div>
        <span class="delivery-label-attribute"><?= $this->t('Quantity of products') ?>:</span>
        <span class="delivery-label-value"><?= $model->getPositionsCallcenterAcceptedNumber() ?></span>
    </div>
    <div>
        <span class="delivery-label-attribute"><?= $this->t('Declared value') ?>:</span>
        <span class="delivery-label-value"><?= $currencyFormatter->format($model->price, $cn->getName('short')) ?></span>
    </div>
    <div>
        <strong>
            <span class="delivery-label-attribute"><?= $this->t('To pay') ?>:</span>
            <span class="delivery-label-value"><?= $currencyFormatter->format($model->getPayPrice(), $cn->getName('short')) ?></span>
        </strong>
    </div>
    <div class="delivery-novaposta-client">
        <span class="delivery-label-attribute"><?= $this->t('Addressee') ?>:</span>
        <span class="delivery-label-value"><?= $model->client_name ?> <?= $model->client_surname ?></span>
    </div>
    <?php if (mb_strlen($model->telephone)): ?>
        <div class="delivery-novaposta-client">
            <span class="delivery-label-attribute"><?= $this->t('Addressee\'s phone') ?>:</span>
            <span class="delivery-label-value"><?= $model->telephone ?></span>
        </div>
    <?php endif ?>
    <div class="delivery-novaposta-region">
        <span class="delivery-label-attribute"><?= $this->t('Address') ?>:</span>
        <span class="delivery-label-value"></span>
        <div class="delivery-label-value">
            <?= $model->getDeliveryAttribute('address') ?>
        </div>
    </div>

    <div class="label-barcode">
        <?= \CHtml::image($this->createAbsoluteUrl('labelPrinter/barcode', ['id' => $model->id, 'height' => '50', 'width' => '2'])) ?>
    </div>
</div>
