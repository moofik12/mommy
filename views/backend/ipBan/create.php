<?php

use MommyCom\Controller\Backend\IpBanController;

/* @var $this IpBanController */

//render form
/** @var $form TbForm */
$this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Add IP'),
    'headerIcon' => 'icon-edit',
]); ?>

<?= $form->render(); ?>

<?php $this->endWidget(); ?>
