<?php

use MommyCom\Controller\Backend\IpBanController;
use MommyCom\Model\Db\IpBanRecord;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $this IpBanController
 * @var $data IpBanRecord
 */

$grid = $this->widget('bootstrap.widgets.TbGridView', [
    'id' => 'ip-ban-grid',
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'IP {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form[id=filterForm] input, form[id=filterForm] select',
    'rowCssClassExpression' => function ($row, $data) {
        return ($data->banned_to > time()) ? 'success' : '';
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        'ip',
        [
            'name' => 'section',
            'value' => function ($data) {
                /** @var $data  IpBanRecord */
                return $data->getSectionReplacement();
            },
            'filter' => $dataProvider->model->getSectionReplacements(),
        ],
        [
            'name' => 'banned_to',
            'type' => 'datetime',
            'filter' => false,
        ],
        'comment',
        [
            'name' => 'created_at',
            'type' => 'humanTime',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{delete}',
        ],
    ],
]); ?>
