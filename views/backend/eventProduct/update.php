<?php

use MommyCom\Model\Db\EventProductRecord;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $form TbForm */
/* @var $model EventProductRecord */

$this->pageTitle = $this->t('Product');
?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Current Information'),
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data' => $model,
    'attributes' => [
        'id',
        [
            'name' => 'product.logo',
            'type' => 'raw',
            'value' => CHtml::link(CHtml::image($model->product->logo->url, '', ['style' => 'max-width: 130px; max-height: 130px;']), $model->product->logo->url, ['target' => '_blank']),
        ],
        'name',
        'max_per_buy',
        'ageRangeReplacement',
        'sizeformatReplacement',
        'size',
    ],
]); ?>
<?php $this->endWidget() ?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Edit product'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
