<?php
/* @var $this BackController */

use MommyCom\Service\BaseController\BackController;

/* @var $form TbForm */
$this->pageTitle = $this->t('Upload order table');
?>
<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Upload order table'),
    'headerIcon' => 'icon-add',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>
