<?php
/* @var $this CController */

?>

<?php $box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Messages'),
    'headerIcon' => 'icon-edit',
]);

//render filter form
/* @var $filterForm TbActiveForm */
$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well'],
    'type' => 'search',
]);

echo $filterForm->dropDownListRow($dataProvider->model, 'status', $dataProvider->model->statusReplacement()
    , ['prompt' => $this->t('All')]);

echo $filterForm->dropDownListRow($dataProvider->model, 'type', $dataProvider->model->typeReplacement()
    , ['prompt' => $this->t('All')]);

echo $filterForm->dateRangeRow($dataProvider->model, 'searchDateRangeInput', [
    'options' => [
        'callback' => new CJavaScriptExpression('
                    function(startDate, endDate) {
                        var inputStart = $("input[name*=searchDateStart]");
                        var inputEnd = $("input[name*=searchDateEnd]");

                        if (startDate && endDate) {
                            inputStart.val(startDate.getTime()/1000);
                            inputEnd.val(endDate.getTime()/1000);
                        } else {
                            inputStart.val("");
                            inputEnd.val("");
                        }
                    }'),
    ],
    'placeholder' => $this->t('Please select date'),
    'autocomplete' => 'off',
    'id' => CHtml::activeId($dataProvider->model, 'searchDateRangeInput') . '_filter',
]);
echo $filterForm->hiddenField($dataProvider->model, 'searchDateStart',
    ['id' => CHtml::activeId($dataProvider->model, 'searchDateStart') . '_filter']);
echo $filterForm->hiddenField($dataProvider->model, 'searchDateEnd',
    ['id' => CHtml::activeId($dataProvider->model, 'searchDateEnd') . '_filter',]);

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', ['beforeSend' => 'function() {return false;}'],
    ['class' => 'invisible']);

$this->endWidget();
//end filter form
?>

<?php $this->renderPartial('_index', [
    'dataProvider' => $dataProvider,
]); ?>

<?php $this->endWidget(); ?>
