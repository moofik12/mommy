<?php

use MommyCom\Controller\Backend\EmailController;
use MommyCom\Model\Db\EmailMessage;
use MommyCom\Widget\Backend\ButtonColumn;

/**
 * @var $data  EmailMessage
 * @var $this EmailController
 */

$gridId = 'email-grid';
$grid = $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,

    'summaryText' => 'Messages {start} - {end} out of {count}',
    'filterSelector' => '{filter}, form input, form select',
    'rowCssClassExpression' => function ($row, $data) {
        $css = [
            EmailMessage::STATUS_WAITING => 'warning',
            EmailMessage::STATUS_SENT => 'success',
            EmailMessage::STATUS_ERROR => 'error',
        ];

        return CHtml::value($css, $data->status);
    },
    'columns' => [
        [
            'name' => 'id',
            'header' => '#',
            'headerHtmlOptions' => ['style' => 'width:1%;'],
        ],
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'url' => $this->createUrl('email/view'),
            'name' => 'from',
            'value' => function ($data) {
                $text = is_string($data->from) ? $data->from : '';

                if (is_array($data->from)) {
                    foreach ($data->from as $from => $name) {
                        $name = isset($name) ? "($name)" : '';
                        $text .= "$from $name\n";
                    }
                }

                return $text;
            },
        ],
        [
            'name' => 'to',
            'value' => function ($data) {
                $text = is_string($data->to) ? $data->to : '';

                if (is_array($data->to)) {
                    foreach ($data->to as $from => $name) {
                        $name = isset($name) ? "($name)" : '';
                        $text .= "$from $name\n";
                    }
                }

                return $text;
            },
        ],
        'subject',
        [
            'name' => 'status',
            'filter' => false,
            'value' => function ($data) {
                /** @var EmailMessage $data */
                return CHtml::value($data->statusReplacement(), $data->status);
            },
        ],
        [
            'name' => 'type',
            'filter' => false,
            'value' => function ($data) {
                /** @var EmailMessage $data */
                return CHtml::value($data->typeReplacement(), $data->type);
            },
        ],
        [
            'name' => 'created_at',
            'type' => 'humanTime',
            'filter' => false,
            'headerHtmlOptions' => ['class' => 'span2'],
        ],
        [
            'class' => ButtonColumn::class,
            'template' => '{view} {delete}',
            'viewButtonUrl' => '\Yii::app()->controller->createUrl("viewBody",array("id"=>$data->primaryKey))',
        ],
    ],
    'bulkActions' => [
        'actionButtons' => [
            [
                'id' => 'repeatEmail',
                'buttonType' => 'button',
                'type' => 'primary',
                'size' => 'medium',
                'label' => $this->t('Re-send'),
                'htmlOptions' => ['data-url' => $this->createAbsoluteUrl('email/updateStatus', ['status' => EmailMessage::STATUS_WAITING])],
                'click' => new CJavaScriptExpression('function(values) {
                    var $this = $(this);
                    var url = $this.attr("data-url");
                    if (url.indexOf("?") == -1) {
                        url += "?";
                    }
                    $(values).each(function() {
                        url += "&" + "id[]=" + $(this).val();
                    });

                    $.ajax({
                        url: url,
                        dataType: "json"
                    })
                    .success(function(data){
                        if (data && !data["result"]) {
                            alert("Error, please contact system administrator");
                        }
                    })
                    .error(function() {
                        alert("Error, please contact system administrator");
                    })
                    .complete(function(){
                        $.fn.yiiGridView.update("' . $gridId . '");
                    });
                }'),
            ],
        ],
        // if grid doesn't have a checkbox column type, it will attach
        // one and this configuration will be part of it
        'checkBoxColumnConfig' => [
            'name' => 'id',
        ],
    ],
]); ?>
