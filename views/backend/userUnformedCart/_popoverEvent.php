<?php

use MommyCom\Model\Db\EventRecord;

/**
 * @var $event EventRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $event,
    'attributes' => array(
        'id',
        array(
            'type' => 'raw',
            'name' => 'logo_fileid',
            'value' => CHtml::image($event->logo->getThumbnail('small70')->url, 'logo', array('class' => 'title')),
            'visible' => !$event->is_virtual
        ),
        'name',
        array(
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => '<span class="label label-info">' . $event->supplier->name . '</span>',
        ),
        'is_virtual:boolean',
        'is_stock:boolean',
        'startAtString',
        'endAtString',
        'mailingStartAtDateString',
        'statusReplacement'
    ),
));
