<?php

use MommyCom\Controller\Backend\UserUnformedCartController;
use MommyCom\Model\Db\CartRecord;

/**
 * @var $this UserUnformedCartController
 * @var $dataProvider CActiveDataProvider
 * @var string $hasTelephone
 * @var string $eventName
 */

$this->pageTitle = $this->t('Abandoned Shopping Cart');
$csrf = $this->app()->request->enableCsrfValidation
    ? [$this->app()->request->csrfTokenName => $this->app()->request->csrfToken] : [];

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Shopping carts'),
    'headerIcon' => 'icon-edit',
]); ?>

<?php $filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filterForm',
    'htmlOptions' => ['class' => 'well', 'style' => 'margin: 0;'],
    'type' => 'search',
    'method' => 'GET',
]); /* @var $filterForm TbActiveForm */ ?>
<div class="row">
    <label>
        <div><?= $this->t('Phone number') ?></div>
        <?= CHtml::dropDownList(
            'hasTelephone',
            $hasTelephone,
            [
                '' => '',
                'not' => $this->t('No'),
                'yes' => $this->t('Yes'),
            ]
        ) ?>
    </label>
    <label>
        <div><?= $this->t('Flash-sale') ?></div>
        <?= CHtml::textField('eventName', $eventName) ?>
    </label>
    <label>
        <div><?= $this->t('Date') ?></div>
        <?php $this->widget('bootstrap.widgets.TbDateRangePicker', [
            'name' => 'rangeData',
            'value' => $rangeData,
            'htmlOptions' => ['autocomplete' => 'off'],
        ]); ?>
        <label>
</div>
<div class="row">
    <?= CHtml::submitButton($this->t('Apply'), ['class' => 'btn btn-success']) ?>
</div>
<?php $this->endWidget() ?>

<?php
/* @var $data CartRecord */
$this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider,
    'filter' => $dataProvider->model,
    'summaryText' => 'Carts {start} - {end} out of {count}',
    'columns' => [
        [
            'name' => 'user_id',
            'header' => $this->t('User №'),
            'headerHtmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'class' => 'bootstrap.widgets.TbRelationalColumn',
            'type' => 'raw',
            'name' => 'user_id',
            'filter' => false,
            'value' => function ($data) {
                /** @var $data CartRecord */
                if ($data->user !== null) {
                    $user = $data->user;
                    $label = $user->email;
                    if (!empty($user->fullname)) {
                        $label .= ' ' . '(' . $user->fullname . ')';
                    }
                    if (!empty($user->telephone)) {
                        $label .= ', ' . $user->telephone;
                    }

                    return CHtml::link($label, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial('_popoverUser', ['user' => $data->user], true),
                    ]);
                }
            },
            'url' => $this->createUrl('userUnformedCart/view'),
            'afterAjaxUpdate' => 'js:function(id, data) {
                    jQuery("[data-toggle=popover]").popover();
                }',
        ],
        [
            'name' => 'number',
            'filter' => false,
            'value' => function ($data) {
                /** @var $data CartRecord */
                return CartRecord::model()->userId($data->user_id)->createdAtDay($data->created_at)->findColumnSum('number');
            },
            'headerHtmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
        ],
        [
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}',
        ],
    ],
]);

$this->endWidget();
?>
