<?php

use MommyCom\Model\Db\UserRecord;

/**
 * @var $user UserRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $user,
    'attributes' => array(
        'id',
        'email',
        'fullname',
        'telephone',
        'created_at:datetime',
        'updated_at:datetime',
    ),
));
