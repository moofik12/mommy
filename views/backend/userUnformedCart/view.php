<?php

use MommyCom\Controller\Backend\UserUnformedCartController;
use MommyCom\Model\Db\CartRecord;

/**
 * @var $this UserUnformedCartController
 * @var $dataProvider CActiveDataProvider
 * @var string $hasTelephone
 * @var string $eventName
 */
$this->pageTitle = $this->t('Abandoned shopping cart');

$box = $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Shopping cart'),
    'headerIcon' => 'icon-edit',
]); ?>

<?php
/** @var $data CartRecord */
$this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $dataProvider,
    'summaryText' => 'Products {start} - {end} out of {count}',
    'extraRowExpression' => function ($data) {
        /** @var $data CartRecord */
        return CHtml::link($data->event->name,
            $this->app()->frontendUrlManager->createUrl('event/index'
                , ['id' => $data->event->id])
            , [
                'name' => "event-{$data->event->id}",
                'data-toggle' => 'popover',
                'data-trigger' => 'hover',
                'data-placement' => 'right',
                'data-html' => 'true',

                'data-title' => $data->event->name,
                'data-content' => $this->renderPartial('_popoverEvent',
                    ['event' => $data->event], true),
                'target' => '_blank',
            ]);
    },
    'extraRowColumns' => ['event_id'],
    'mergeColumns' => ['event_id'],
    'columns' => [
        [
            'name' => 'event_id',
            'visible' => false,
        ],
        [
            'name' => 'product.product.id',
        ],
        [
            'name' => 'product.name',
            'type' => 'raw',
            'value' => function ($data) {
                /** @var $data CartRecord */
                return CHtml::link($data->product->name,
                    $this->app()->frontendUrlManager->createUrl('product/index'
                        , ['id' => $data->product->product_id, 'eventId' => $data->product->event_id])
                    , [
                        'name' => "product-{$data->product->id}",
                        'data-toggle' => 'popover',
                        'data-trigger' => 'hover',
                        'data-placement' => 'right',
                        'data-html' => 'true',

                        'data-title' => $data->product->product->name,
                        'data-content' => $this->renderPartial('_popoverProduct',
                            ['eventProduct' => $data->product], true),
                        'target' => '_blank',
                    ]);
            },
        ],
        [
            'name' => 'number',
            'headerHtmlOptions' => [
                'class' => 'span1',
            ],
        ],
        [
            'name' => 'created_at',
            'type' => 'datetime',
            'filter' => false,
        ],
    ],
]);
?>

<?php $this->endWidget(); ?>
