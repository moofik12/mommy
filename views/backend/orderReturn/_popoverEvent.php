<?php

use MommyCom\Model\Db\EventRecord;

/**
 * @var $event EventRecord
 */

$suppliersPrint = array_reduce($event->getProductSuppliersName(), function($carry, $item) {
    /* @var string $item */
    return $carry . ' <span class="label label-info">' . $item . '</span>';
}, '');

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $event,
    'attributes' => array(
        'id',
        array(
            'type' => 'raw',
            'name' => 'logo_fileid',
            'value' => CHtml::image($event->logo->getThumbnail('small70')->url, 'logo', array('class' => 'title')),
            'visible' => !$event->is_virtual
        ),
        'name',
        'is_virtual:boolean',
        'is_stock:boolean',
        'startAtString',
        'endAtString',
        'mailingStartAtDateString',
        'statusReplacement',
        array(
            'name' => 'supplier_id',
            'type' => 'raw',
            'value' => $suppliersPrint,
        ),
    ),
));
