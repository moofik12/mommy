<?php

use MommyCom\Model\Db\AdminUserRecord;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * @var $history array
 * @var $this CController
 */
?>
<table class="items table table-striped table-bordered table-hover">
    <th style="text-align: center">Дата</th>
    <th style="text-align: center">Статус</th>
    <th style="text-align: center">Пользователь</th>
<?php
    foreach ($history as $time => $data) {
        /** @var AdminUserRecord $adminModel */
        $adminModel = isset($data['admin_id']) ? AdminUserRecord::model()->findByPk($data['admin_id']) : null;
        /** @var  UserRecord $userModel */
        $userModel = isset($data['user_id']) ? UserRecord::model()->findByPk($data['user_id']) : null;

        $status = isset($data['status']) ? CHtml::value(OrderRecord::processingStatusReplacements(), $data['status']) : '';
        $admin = $adminModel !== null ? $adminModel->login . "<br>(" . $adminModel->fullname . ')' : '';
        $user = $userModel !== null ? $userModel->email . ' (Пользователь)' : '';

        echo '<tr>';
        echo '<td>'  . $this->app()->dateFormatter->formatDateTime($time, 'medium', 'short') . '</td>';
        echo  '<td>' . $status . '</td>';
        echo  '<td>' . $admin . $user . '</td>';
        echo '</tr>';
    }
?>
</table>
