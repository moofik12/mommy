<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Service\BaseController\BackController;
use MommyCom\Widget\Backend\ButtonColumn;

/* @var $this BackController */
/* @var $provider CActiveDataProvider */
$this->pageTitle = $this->t('Returns');

$request = $this->app()->request;
$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
?>
<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Refund list'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['orderReturn/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->widget('bootstrap.widgets.TbGridView', [
    'dataProvider' => $provider,
    'filter' => $provider->model,
    'filterSelector' => '{filter}, form select',
    'columns' => [
        'id',
        'order_id',
        [
            'name' => 'order_user_id',
            'filter' => false,
            'value' => function ($data) {
                /* @var $data OrderReturnRecord */
                return $data->order->client_name . ' ' . $data->order->client_surname;
            },
        ],
        ['name' => 'is_custom',
            'type' => 'boolean',
            'filter' =>
                [true => $this->t('Yes'), false => $this->t('No')]],
        'trackcode',
        'trackcode_return',
        [
            'name' => 'delivery_type',
            'filter' => OrderRecord::deliveryTypeReplacements(),
            'value' => function ($data) {
                /* @var $data OrderReturnRecord */
                return $data->deliveryTypeReplacement;
            },
        ],

        [
            'name' => 'status',
            'filter' => OrderReturnRecord::statusReplacements(),
            'value' => function ($data) {
                /* @var $data OrderReturnRecord */
                return $data->statusReplacement;
            },
        ],

        [
            'name' => 'refund_type',
            'filter' => OrderReturnRecord::refundTypeReplacements(),
            'value' => function ($data) {
                /* @var $data OrderReturnRecord */
                return $data->refundTypeReplacement;
            },
        ],
        'price_products',
        'price_refund',

        [
            'name' => 'payment_after_at',
            'filter' => false,
            'value' => function ($data) {
                /* @var OrderReturnRecord $data */
                $text = $this->t('no');
                if ($data->payment_after_at > 0) {
                    $text = $this->app()->dateFormatter->formatDateTime($data->payment_after_at);
                }

                return $text;
            },
        ],

        [
            'header' => $this->t('Bank'),
            'type' => 'raw',
            'value' => function ($data) {
                /* @var $data OrderReturnRecord */

                if ($data->refund_type == OrderReturnRecord::REFUND_TYPE_BANK) {
                    return CHtml::link($this->t('Bank'), 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'left',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverBank',
                            ['return' => $data],
                            true
                        ),
                    ]);
                }

                return '';
            },
        ],

        [
            'class' => ButtonColumn::class,
            'template' => '{back} {edit} {cancel} {restore}',
            'buttons' => [
                'edit' => [
                    'options' => [
                        'class' => 'btn btn-warning',
                    ],
                    'label' => '<i class="icon-edit"></i> ' . $this->t('Edit') ,
                    'visible' => function ($row, $data) {
                        /* @var $data OrderReturnRecord */
                        return !in_array($data->status, [OrderReturnRecord::STATUS_CONFIGURED, OrderReturnRecord::STATUS_CANCELLED]);
                    },
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('orderReturn/update', ['id' => $data->id]);
                    },
                ],
                'back' => [
                    'options' => [
                        'class' => 'btn btn-danger',
                    ],
                    'label' => '<i class="icon-arrow-left"></i>' . $this->t('Return to edit'),
                    'visible' => function ($row, $data) {
                        /* @var $data OrderReturnRecord */
                        return $data->status == OrderReturnRecord::STATUS_CONFIGURED;
                    },
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('orderReturn/back', ['id' => $data->id]);
                    },
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Update not possible') . "');
                                }
                            });
                            return false;
                        }"),
                ],
                'cancel' => [
                    'options' => [
                        'class' => 'btn btn-danger',
                    ],
                    'label' => '<i class="icon-remove"></i>' . $this->t('Cancel'),
                    'visible' => function ($row, $data) {
                        /* @var $data OrderReturnRecord */
                        return !in_array($data->status, [OrderReturnRecord::STATUS_CONFIGURED, OrderReturnRecord::STATUS_CANCELLED]);
                    },
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('orderReturn/cancel', ['id' => $data->id]);
                    },
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Update not possible') . "');
                                }
                            });
                            return false;
                        }"),
                ],
                'restore' => [
                    'options' => [
                        'class' => 'btn btn-danger',
                    ],
                    'label' => '<i class="icon-repeat"></i> Востановить',
                    'visible' => function ($row, $data) {
                        /* @var $data OrderReturnRecord */
                        return $data->status == OrderReturnRecord::STATUS_CANCELLED;
                    },
                    'url' => function ($data) {
                        /* @var $data OrderRecord */
                        return $this->app()->createAbsoluteUrl('orderReturn/restore', ['id' => $data->id]);
                    },
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Update not possible') . "');
                                }
                            });
                            return false;
                        }"),
                ],
            ],
        ],
    ],
]); ?>
<div class="pull-right">
    <?= CHtml::link($this->t('Add'), ['orderReturn/add'], [
        'class' => 'btn btn-primary',
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>
