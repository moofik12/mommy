<?php

use MommyCom\Model\Db\OrderReturnRecord;

/**
 * @var $return OrderReturnRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $return,
    'attributes' => array(
        'refundDeliveryTypeReplacement',
        'refundRedeliveryTypeReplacement',
        'price_delivery',
        'price_redelivery',

        'exps_client',
        'exps_shop'
    ),
));
