<?php

use MommyCom\Model\Db\OrderReturnRecord;

/**
 * @var $return OrderReturnRecord
 */

$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $return,
    'attributes' => array(
        'bankFullname',
        'bankVatin',
        'bankCardnum',
        'bankCorrespondentAcc',
        'bankIdCode',
        'bankMFOCode'
    ),
));
