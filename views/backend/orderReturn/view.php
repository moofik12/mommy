<?php

use MommyCom\Model\Db\OrderReturnProductRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\YiiComponent\Utf8;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model OrderReturnRecord */
/* @var $positionsProvider CActiveDataProvider */
/* @var $form TbForm */
/* @var $confirmForm TbForm */
$this->pageTitle = $this->t('Return');
$controller = $this;
$request = $this->app()->request;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Return'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="span3">
    <h4><?= $this->t('Return information') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'htmlOptions' => ['class' => 'table-compact'],
        'data' => $model,
        'attributes' => [
            'id',
            'positionCount',
            'price_delivery:number',
            'refundDeliveryTypeReplacement',
            'price_redelivery:number',
            'refundRedeliveryTypeReplacement',
            'price_refund:number',
            'exps_client:number',
            'exps_shop:number',
            'created_at:datetime',
            'updated_at:datetime',
            'statusReplacement',
            'statusPrevReplacement',
            [
                'name' => 'processingStatusHistory',
                'type' => 'raw',
                'value' => CHtml::link($this->t('History'), 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProcessingHistory',
                        ['history' => $model->getStatusHistory()],
                        true
                    ),
                ]),
            ],
        ],
    ]) ?>
</div>
<div class="span3">
    <h4><?= $this->t('Order info') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'htmlOptions' => ['class' => 'table-compact'],
        'data' => $model->order,
        'attributes' => [
            'id',
            'positionsCallcenterAcceptedCount',
            'price:number',
            'deliveryPrice:number',
            'ordered_at:datetime',
            'updated_at:datetime',
            ['name' => 'processing_status', 'value' => $model->order->processingStatusReplacement],
            ['name' => 'processing_status_prev', 'value' => $model->order->processingStatusPrevReplacement],
            [
                'name' => 'processingStatusHistory',
                'type' => 'raw',
                'value' => CHtml::link($this->t('History'), 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProcessingHistory',
                        ['history' => $model->order->getProcessingStatusHistory()],
                        true
                    ),
                ]),
            ],
        ],
    ]) ?>
</div>
<div class="span3">
    <h4><?= $this->t('Information on the order delivery') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model->order,
        'attributes' => [
            'deliveryTypeReplacement',
            'client_name',
            'client_surname',
            'region.name',
            'city.name',
            [
                'name' => 'novaposta.name',
                'type' => 'ntext',
                'value' => '',
            ],
            'zipcode',
            'address',
            'telephone',
            'trackcode',
        ],
    ]) ?>
</div>
<div class="span3">
    <h4><?= $this->t('User information') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model->order->user,
        'attributes' => [
            'id',
            ['name' => 'name', 'label' => $this->t('Name')],
            ['name' => 'surname', 'label' => $this->t('Last name')],
            'email',
            'telephone',
            'region.name',
            'city.name',
            'address',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>



<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Products'),
    'headerIcon' => 'icon-th-list',
    'htmlOptions' => [
        'class' => $model->status == OrderReturnRecord::STATUS_UNCONFIGURED ? 'hide' : '',
    ],
]); ?>
<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $positionsProvider,
    'extraRowColumns' => ['product'],
    'columns' => [
        //'id',
        'event_product_id',
        'event_id',
        'warehouse_id',
        'price',
        'conditionReplacement',
        'is_pay:boolean',
        'client_comment',
        'comment',
        'created_at:datetime',
        'updated_at:datetime',
        [
            'name' => 'product',
            'type' => 'raw',
            'headerHtmlOptions' => ['class' => 'hide'],
            'htmlOptions' => ['class' => 'hide'],
            'value' => function ($data) use ($controller) {
                static $renders = [];
                /* @var $data OrderReturnProductRecord */
                if (isset($renders[$data->event_product_id])) {
                    return $renders[$data->event_product_id];
                }
                $items = [];

                $items[] = $this->t('Акция') . ': ' . CHtml::link($data->event->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverEvent',
                            ['event' => $data->event],
                            true
                        ),
                    ]);

                $items[] = 'Товар: ' . CHtml::link($data->eventProduct->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverProduct',
                            ['product' => $data->eventProduct],
                            true
                        ),
                    ]);

                return $renders[$data->event_product_id] = Utf8::implode(', ', $items);
            },
        ],
    ],
]) ?>

<?php $this->endWidget() ?>



<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Information on compensation'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'type' => ['striped', 'bordered', 'condensed'],
    'htmlOptions' => ['class' => 'table-compact'],
    'data' => $model,
    'attributes' => [
        'order_id',
        'comment',
        'client_comment',
        'price_refund:number',
        'refundTypeReplacement',
        'bankFullname',
        'bankVatin',
        'bankCardnum',
        'bankCorrespondentAcc',
        'bankIdCode',
        'bankMFOCode',
    ],
]) ?>
<?php $this->endWidget() ?>
