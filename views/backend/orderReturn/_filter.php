<?php

use MommyCom\Model\Db\OrderRecord;

/**
 * @var $filterForm TbActiveForm
 * @var $model OrderRecord
 */

$filterForm = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'order-filter-form',
    'htmlOptions'=>array('class'=>'well'),
    'type' => 'search',
));

$this->widget('bootstrap.widgets.TbSelect2', array(
    'name' => 'filterSupplierID',
    'asDropDownList' => false,
    'options' => array(
        'width' => '250px;',
        'placeholder' => $this->t('Supplier'),
        'allowClear' => true,
        'ajax' => array(
            'url' => $this->app()->createUrl('supplier/ajaxSearch'),
            'type' => 'GET',
            'dataType' => 'json',
            'data' => new CJavaScriptExpression('function (term, page) {
                    return {
                        name: term,
                        page: page
                    };
                }'),
            'results' => new CJavaScriptExpression('function(response, page) {
                var result = {
                    more: page < response.pageCount,
                    results: []
                };

                $.each(response.items, function(index, value) {
                    result.results.push({
                        id: value.id,
                        text: value.name,
                    });
                });

                return result;
            }'),
        ),
    ),

));

//отмена отправки формы
echo CHtml::ajaxSubmitButton($this->t('Apply'), '', array('beforeSend' => 'function() {
    $("#' . $filterForm->id . ' :input:first").trigger("change");
    return false;
}'), array('class' => 'btn btn-primary'));

$this->endWidget();
