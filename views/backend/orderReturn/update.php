<?php

use MommyCom\Model\Db\OrderReturnProductRecord;
use MommyCom\Model\Db\OrderReturnRecord;
use MommyCom\Widget\Backend\ButtonColumn;
use MommyCom\YiiComponent\Utf8;

/* @var $this \MommyCom\Service\BaseController\BackController */
/* @var $model OrderReturnRecord */
/* @var $positionsProvider CActiveDataProvider */
/* @var $form TbForm */
/* @var $acceptForm TbForm */

$this->pageTitle = $this->t('Returns');
$controller = $this;

$request = $this->app()->request;
$csrf = $request->enableCsrfValidation ? [$request->csrfTokenName => $request->csrfToken] : [];
$nf = $this->app()->numberFormatter;
?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Return'),
    'headerIcon' => 'icon-th-list',
]); ?>
<div class="span3">
    <h4><?= $this->t('Return information') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'htmlOptions' => ['class' => 'table-compact'],
        'data' => $model,
        'attributes' => [
            'id',
            'positionCount',
            'price_delivery:number',
            'refundDeliveryTypeReplacement',
            'price_redelivery:number',
            'refundRedeliveryTypeReplacement',
            'price_refund:number',
            'exps_client:number',
            'exps_shop:number',
            'created_at:datetime',
            'updated_at:datetime',
            'statusReplacement',
            'statusPrevReplacement',
            [
                'name' => 'statusHistory',
                'type' => 'raw',
                'value' => CHtml::link($this->t('History'), 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProcessingHistory',
                        ['history' => $model->getStatusHistory()],
                        true
                    ),
                ]),
            ],
        ],
    ]) ?>
</div>
<div class="span3">
    <h4><?= $this->t('Order info') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'htmlOptions' => ['class' => 'table-compact'],
        'data' => $model->order,
        'attributes' => [
            'id',
            'positionsCallcenterAcceptedCount',
            'ordered_at:datetime',
            'updated_at:datetime',
            ['name' => 'processing_status', 'value' => $model->order->processingStatusReplacement],
            ['name' => 'processing_status_prev', 'value' => $model->order->processingStatusPrevReplacement],
            [
                'name' => 'processingStatusHistory',
                'type' => 'raw',
                'value' => CHtml::link($this->t('History'), 'javascript:void(0)', [
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'data-content' => $this->renderPartial(
                        '_popoverProcessingHistory',
                        ['history' => $model->order->getProcessingStatusHistory()],
                        true
                    ),
                ]),
            ],
            'price:number',
            [
                'type' => 'number',
                'label' => $this->t('Discount granted'),
                'value' => $model->order->getUsedDiscounts(),
            ],
            //был баг, данные не верны
            /*
            array(
                'type' => 'html',
                'name' => 'card_payed',
                'label' => 'Оплачено (картой + при получении)',
                'value' => '<span style="color: #ff7a8c">' . $nf->formatDecimal($model->getOrderPayed()) . '</span>',
            ),
            */
            [
                'type' => 'number',
                'label' => $this->t('Bonuses used'),
                'value' => $model->order->bonuses,
            ],
            [
                'type' => 'html',
                'label' => $this->t('Gift bonuses'),
                'value' => '<span style="color: #ff7a8c">' . $nf->formatDecimal($model->order->bonuses_part_used_present) . '</span>',
            ],
            [
                'type' => 'html',
                'label' => $this->t('Refunded for this order'),
                'value' => '<span style="color: #ff7a8c">' . $nf->formatDecimal($model->getCountReturned()) . '</span>',
            ],
            [
                'type' => 'html',
                'label' => $this->t('Max compensation amount'),
                'value' => '<span style="color: #ff7a8c">' . $nf->formatDecimal($model->getAvailableRefund()) . '</span>',
            ],
            'deliveryPrice:number',

        ],
    ]) ?>
</div>
<div class="span3">
    <h4><?= $this->t('Information on the order delivery') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model->order,
        'attributes' => [
            'deliveryTypeReplacement',
            'client_name',
            'client_surname',
            'region.name',
            'city.name',
            [
                'name' => 'novaposta.name',
                'type' => 'ntext',
                'value' => '',
            ],
            'zipcode',
            'address',
            'telephone',
            'trackcode',
        ],
    ]) ?>
</div>
<div class="span3">
    <h4><?= $this->t('User information') ?></h4>
    <?php $this->widget('bootstrap.widgets.TbDetailView', [
        'type' => ['striped', 'bordered', 'condensed'],
        'data' => $model->order->user,
        'attributes' => [
            'id',
            ['name' => 'name', 'label' => $this->t('Name')],
            ['name' => 'surname', 'label' => $this->t('Last name')],
            'email',
            'telephone',
            'region.name',
            'city.name',
            'address',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
</div>
<div class="clearfix"></div>
<?php $this->endWidget() ?>

<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('General information'),
    'headerIcon' => 'icon-th-list',
]); ?>
<?= $form->render() ?>
<?php $this->endWidget() ?>


<?php $this->beginWidget(\MommyCom\Widget\Backend\ContentBox::class, [
    'title' => $this->t('Products'),
    'headerIcon' => 'icon-th-list',
    'htmlOptions' => [
        'class' => $model->status == OrderReturnRecord::STATUS_UNCONFIGURED ? 'hide' : '',
    ],
]); ?>
<?php $this->widget('bootstrap.widgets.TbGroupGridView', [
    'dataProvider' => $positionsProvider,
    'extraRowColumns' => ['product'],
    'columns' => [
        //'id',
        'event_product_id',
        'event_id',
        'warehouse_id',
        'price',
        [
            'header' => $this->t('Refund due'),
            'value' => function ($data) use ($model) {
                /** @var $data OrderReturnProductRecord */
                return $this->app()->numberFormatter->formatDecimal($data->getRefundPrice());
            },
        ],
        [
            'name' => 'item_condition',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'htmlOptions' => ['class' => 'span3'],
            'editable' => [
                'safeOnly' => false,
                'url' => ['orderReturn/setParameter'],
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'select',
                'htmlOptions' => [
                    'data-source' => json_encode(OrderReturnProductRecord::conditionReplacements(), JSON_FORCE_OBJECT),
                ],
                'emptytext' => $this->t('No'),
                'title' => $this->t('Enter product condition'),
            ],
        ],
        [
            'name' => 'is_pay',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'htmlOptions' => ['class' => 'span3'],
            'editable' => [
                'safeOnly' => false,
                'url' => ['orderReturn/setParameter'],
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'select',
                'htmlOptions' => [
                    'data-source' => json_encode([$this->t('No'), $this->t('Yes')], JSON_FORCE_OBJECT),
                ],
                'emptytext' => $this->t('No'),
                'title' => $this->t('To be compensated'),
                'success' => 'js: function() {$(this).closest(".grid-view").yiiGridView("update");}',
            ],
        ],
        [
            'name' => 'client_comment',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'htmlOptions' => ['class' => 'span3'],
            'editable' => [
                'safeOnly' => false,
                'url' => ['orderReturn/setParameter'],
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'textarea',
                'emptytext' => $this->t('No'),
                'title' => $this->t('Enter a comment'),
            ],
        ],
        [
            'name' => 'comment',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'htmlOptions' => ['class' => 'span3'],
            'editable' => [
                'safeOnly' => false,
                'url' => ['orderReturn/setParameter'],
                'params' => [
                    $request->csrfTokenName => $request->getCsrfToken(),
                ],
                'type' => 'textarea',
                'emptytext' => $this->t('No'),
                'title' => $this->t('Enter a comment'),
            ],
        ],
        'created_at:datetime',
        [
            'class' => ButtonColumn::class,
            'template' => '{cancel} {restore}',
            'buttons' => [
                'cancel' => [
                    'options' => [
                        'class' => 'btn btn-danger',
                    ],
                    'label' => '<i class="icon-remove"></i> ' . $this->t('Cancel'),
                    'visible' => function ($row, $data) {
                        /* @var $data OrderReturnProductRecord */
                        return $data->status == OrderReturnProductRecord::STATUS_OK;
                    },
                    'url' => function ($data) {
                        /* @var $data OrderReturnProductRecord */
                        return $this->app()->createAbsoluteUrl('orderReturn/productCancel', ['id' => $data->id]);
                    },
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Update not possible') . "');
                                }
                            });
                            return false;
                        }"),
                ],
                'restore' => [
                    'options' => [
                        'class' => 'btn btn-danger',
                    ],
                    'label' => '<i class="icon-ok"></i> ' . $this->t('Restore'),
                    'visible' => function ($row, $data) {
                        /* @var $data OrderReturnProductRecord */
                        return $data->status == OrderReturnProductRecord::STATUS_DELETED;
                    },
                    'url' => function ($data) {
                        /* @var $data OrderReturnProductRecord */
                        return $this->app()->createAbsoluteUrl('orderReturn/productRestore', ['id' => $data->id]);
                    },
                    'click' => new CJavaScriptExpression("function() {
                            var grid  = $(this).closest('.grid-view');
                            grid.yiiGridView('update', {
                                type: 'POST',
                                data: " . CJavaScript::encode($csrf) . ",
                                url: jQuery(this).attr('href'),
                                success: function(data) {
                                    grid.yiiGridView('update');
                                },
                                error: function(XHR) {
                                    alert('" . $this->t('Update not possible') . "');
                                }
                            });
                            return false;
                        }"),
                ],
            ],
        ],
        [
            'name' => 'product',
            'type' => 'raw',
            'headerHtmlOptions' => ['class' => 'hide'],
            'htmlOptions' => ['class' => 'hide'],
            'value' => function ($data) use ($controller) {
                static $renders = [];
                /* @var $data OrderReturnProductRecord */
                if (isset($renders[$data->event_product_id])) {
                    return $renders[$data->event_product_id];
                }
                $items = [];

                $items[] = $this->t('Flash-sale') . ': ' . CHtml::link($data->event->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverEvent',
                            ['event' => $data->event],
                            true
                        ),
                    ]);

                $items[] = $this->t('Product') . ': ' . CHtml::link($data->eventProduct->name, 'javascript:void(0)', [
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-trigger' => 'hover',
                        'data-html' => 'true',
                        'data-content' => $this->renderPartial(
                            '_popoverProduct',
                            ['product' => $data->eventProduct],
                            true
                        ),
                    ]);

                return $renders[$data->event_product_id] = Utf8::implode(', ', $items);
            },
        ],
    ],
]) ?>

<?= $acceptForm->render() ?>
<?php $this->endWidget() ?>
