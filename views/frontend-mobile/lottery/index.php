<?php

use MommyCom\Model\Form\RegistrationOfferForm;
use MommyCom\YiiComponent\AssetManager;

/**
 * @var int $minBonus
 * @var int $maxBonus
 * @var int $startTime
 * @var int $endTime
 * @var RegistrationOfferForm $formOfferRegistration
 */

$app = $this->app();
/* @var AssetManager $assetManager */
$assetManager = $app->getAssetManager();

$baseImgUrl = $assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
$percentage = '-70%';
$currency = 'руб';

?>
<main class="page">
    <section class="section section1">
        <div class="wrapper">
            <div class="to-left logo-wrapp">
                <div><?= CHtml::link('<img src="' . $baseImgUrl . 'lottery/logo.svg" alt="" />', ['index/index']) ?></div>
                <p><?= $this->t('affordable care for yourself and your loved ones') ?></p>
            </div>
            <div class="to-right top-right-text">
                <p>
                    <?= $this->t('affordable care for yourself and your loved ones') ?>
                    <?= $this->t('Discounts for quality branded clothes and accessories up to <span>{percentage}</span>',
                        ['{percentage}' => $percentage]) ?>
                </p>
            </div>
            <div class="clear"></div>
            <div class="top-center-text">
                <h2>
                    <span><?= $this->t('SUPER stock from MOMMY') ?></span>
                    <?= $this->t('A unique opportunity to take part in the draw and get a discount on your first order!') ?>
                </h2>
            </div>
        </div>
        <div class="wrapper wide">
            <div class="gifts">
                <div class="center-text">
                    <p>
                        <?= $this->t(
                            'Вы можете <b>гарантированно</b><br />получить от<br /><span>{minBonus}</span> до <span>{maxBonus}</span> {currency} бонусов<br />на первую покупку!',
                            [
                                '{minBonus}' => $minBonus,
                                '{maxBonus}' => $maxBonus,
                                '{currency}' => $currency,
                            ]) ?>
                    </p>
                </div>
                <div class="right-text">
                    <p>
                        <?= $this->t('Buy quality clothes and accessories for yourself and your relatives and friends at any time. <span>MOMMY.COM</span> will help you with that. Only our shop have such affordable prices in the country!') ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="section section2">
        <div class="wrapper">
            <div class="title">
                <h2><?= $this->t('Everybody, without exception, WINS in our raffle!') ?></h2>
            </div>
            <div class="form-wrapp">
                <?php
                /** @var CActiveForm $formOffer */
                $formOffer = $this->beginWidget('CActiveForm', [
                    'action' => ['auth/offerRegistration'],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'validateOnSubmit' => true,
                        'afterValidate' => "js:function(form, data, hasError){
                                if (!hasError) {
                                    Mamam.trackEvent('modals', 'present-registration-finish', " . 'Регистрация завершена через страницу лотереи' . ");
                                    return true;
                                }

                                return false;
                            }",
                    ],
                ]) ?>
                <div class="inputs">
                    <div class="field top <?= $formOfferRegistration->hasErrors('email') ? 'error' : '' ?>">
                        <label><?= $this->t('Enter your email address') ?></label>
                        <?= $formOffer->textField($formOfferRegistration, 'email', ['placeholder' => 'E-mail address', 'autocomplete' => 'off']) ?>
                        <?= $formOffer->error($formOfferRegistration, 'email', ['class' => 'message']) ?>
                    </div>
                    <?php if ($formOfferRegistration->secretCode != RegistrationOfferForm::SECRET_CODE_NOT_PRESENT): ?>
                        <div class="field top <?= $formOfferRegistration->hasErrors('secretCode') ? 'error' : '' ?>">
                            <label><?= $this->t('Enter the secret code from the flyer') ?></label>
                            <?= $formOffer->textField($formOfferRegistration, 'secretCode', ['placeholder' => 'Code from the flyer', 'autocomplete' => 'off']) ?>
                            <?= $formOffer->error($formOfferRegistration, 'secretCode', ['class' => 'message']) ?>
                        </div>
                    <?php else: ?>
                        <?= $formOffer->hiddenField($formOfferRegistration, 'secretCode') ?>
                    <?php endif; ?>
                </div>
                <div class="privacy">
                    <?= $formOffer->hiddenField($formOfferRegistration, 'isConfirmPrivacy') ?>
                    <?= $this->t('By sending your e-mail you agree with') ?>
                    <a target="_blank"
                       href="<?= $this->app()->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode']) ?>"
                       class="present-policy"><?= $this->t('With the terms of use') ?>
                    </a>
                </div>
                <div class="action-date">
                    <div>
                        <p><?= $this->t('The stock event is <br /> valid until') ?>
                            <br/><?= date('d.m.Y', $endTime) ?></p>
                    </div>
                </div>
                <div class="form-action">
                    <input type="submit" value="<?= $this->t('Participate in the drawing') ?>"/>
                </div>
                <?= $formOffer->hiddenField($formOfferRegistration, 'benefice') ?>
                <?= $formOffer->hiddenField($formOfferRegistration, 'strategy') ?>
                <?= $formOffer->hiddenField($formOfferRegistration, 'strategyPayments') ?>

                <?php $this->endWidget() ?>
            </div>
            <div class="bottom-text">
                <strong><?= $this->t('Check your e-mail, because you won the prize! *') ?></strong>
                <p>
                    <?= $this->t('* By "prize", it means a discount from {minBonus} to {maxBonus} USD bonuses for the first order in the shopping club MOMMY',
                        [
                            '{minBonus}' => $minBonus,
                            '{maxBonus}' => $maxBonus,
                        ]) ?>
                </p>
            </div>
        </div>
    </section>
</main>
