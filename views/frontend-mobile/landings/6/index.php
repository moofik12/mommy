<?php

use MommyCom\Controller\FrontendMobile\LandingsController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;

/**
 * @var $this LandingsController
 * @var $promoId integer promo по которой был сделан переход
 * @var $tab string
 * @var $registrationForm UserRecord
 * @var $social string
 * @var $registrationSocial NetworkRegistrationForm
 * @var $registrationSocialConfirm NetworkRegistrationConfirmForm
 */

//$this->pageTitle = 'The first shopping club for children and moms in the USA';
$app = $this->app();
$this->pageTitle = $this->t('Mommy Shopping-club ');

?>
<div class="main-container">
    <div class="container">
        <div class="overflow">
            <span class="logo"></span>
        </div>
        <div class="form-block <?= $tab === 'registration' ? '' : 'hide' ?>">
            <div class="overflow">
                <div class="top-text">
                    <span class="text1"><?= $this->t('Brands Offers') ?></span>
                    <span class="text2"><?= $this->t('The best first') ?></span>
                    <span class="text3"><?= $this->t('by eXclusive prices') ?></span>
                </div>
            </div>
            <div class="content-bg">
                <?php $form = $this->beginWidget('CActiveForm', [
                    'action' => $this->app()->createUrl('landings/registration', ['id' => $promoId]),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => true,
                    'clientOptions' => [
                        'validateOnSubmit' => true,
                    ],
                ]) ?>
                <div class="input-wrap top">
                    <?= $form->textField($registrationForm, 'name', ['placeholder' => $this->t('Your Name'), 'autocomplete' => 'off', 'class' => 'name']) ?>
                    <?= $form->error($registrationForm, 'name', ['class' => 'message']) ?>
                </div>
                <div class="input-wrap top">
                    <?= $form->textField($registrationForm, 'email', ['placeholder' => $this->t('Your E-mail'), 'autocomplete' => 'off', 'class' => 'email']) ?>
                    <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
                </div>
                <?= CHtml::submitButton($this->t('Send'), ['class' => 'input-wrap']) ?>

                <?php $this->endWidget() ?>
            </div>
            <div class="bottom-text">
                <span class="text1"><?= $this->t('Know first') ?></span>
                <span class="text2"><?= $this->t('super sales!') ?></span>
            </div>
        </div>
        <div class="form-block <?= $tab === 'success' ? '' : 'hide' ?>"
        ">
        <div class="content-bg">
            <div class="after"><?= $this->t('check your email :)') ?></div>
        </div>
    </div>
</div>
<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.landings'));
$cs->registerCssFile($baseUrl . "/$promoId/css/style.css");
$cs->registerScriptFile($baseUrl . "/$promoId/js/script.js");
?>
