<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/**
 * @var $this FrontController
 * @var $provider CDataProvider
 * @var $futureProvider CDataProvider
 * @var array $events
 */

$app = $this->app();
$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$cart = $app->user->cart;

$this->description = $this->t('The first shopping-club for moms and children in the USA mommy.com Products for the whole family and for your home. Daily discounts of up to 90%');
$this->pageTitle = $this->t('MOMMY.COM: Shopping club for moms and children');

?>
<div class="main">
    <div class="promo-now">
        <?php foreach ($events as $index => $item): /* @var $item EventRecord */ ?>
            <section>
                <a class="a" href="<?= $app->createUrl('event/index', ['id' => $item->id]) ?>">
                    <div class="box">
                        <header>
                            <div class="label-discount">
                                -<?= $item->promoDiscountPercent ?>%</span>
                            </div>
                            <div class="image-wrapper waiting">
                                <div class="box-img">
                                    <?= CHtml::image(
                                        $app->baseUrl . '/static/blank.gif', $this->t('stock'), [
                                            'data-src' => $item->logo->getThumbnail('mid320')->url,
                                            'data-lazyload' => 'true',
                                            'style' => 'margin-top: -40px',
                                        ]
                                    ) ?>

                                    <?php if ($this->beginCache('event-promo-discount-' . $item->id, ['varyByLanguage' => true])) { ?>
                                        <?php if ($item->is_drop_shipping): ?>
                                            <label class="fast-delivery"></label>
                                        <?php endif; ?>
                                        <?php $this->endCache();
                                    } ?>
                                </div>
                            </div>
                            <div class="title-wrapper">
                                <h1 class="title"><?= CHtml::encode($item->name) ?></h1>
                            </div>
                        </header>
                        <div class="text-wrapper">
                            <p class="about text"><?= CHtml::encode($item->description_short) ?></p>
                            <p class="time text"><?= CHtml::tag(
                                    'time',
                                    ['datetime' => $tf->formatMachine($item->end_at), 'data-countdown' => true],
                                    $tf->format($item->end_at)
                                ) ?></p>

                            <?php if ($this->beginCache('event-lowest-price-' . $item->id, ['varyByLanguage' => true])) { ?>
                                <p class="price text"><?= $this->t('from') ?> <span
                                            class="lowest-price"><?= $cf->format($item->getMinSellingPrice()) ?></span>
                                </p>
                                <?php $this->endCache();
                            } ?>
                        </div>
                    </div>
                </a>
            </section>
        <?php endforeach ?>
    </div>
    <?php if ($futureProvider->totalItemCount > 0): ?>
        <section class="clearfix">
            <h3 class="main-title"><?= $this->t('They will start soon') ?></h3>
            <div class="promo-soon">
                <?php foreach ($futureProvider->getData() as $item): /* @var $item EventRecord */ ?>
                    <article>
                        <div class="box">
                            <div class="image-wrapper waiting">
                                <div class="box-img">
                                    <?= CHtml::image(
                                        $app->baseUrl . '/static/blank.gif', $this->t('stock'), [
                                            'data-src' => $item->promo->getThumbnail('mid320')->url,
                                            'data-lazyload' => 'true',
                                        ]
                                    ) ?>
                                </div>
                            </div>
                            <h3 class="title"><?= CHtml::encode($item->name) ?></h3>
                        </div>
                    </article>
                <?php endforeach ?>
            </div>
        </section>
        <div class="up-btn"><a href="#"><?= $this->t('Up') ?></a></div>
    <?php endif ?>
</div>
<?php

/**
 * пока нет дизайна вывода сообщений через alert()
 * для совместимости регистрации на главной странице
 */
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;

if ($app->user->hasFlash('message')) {
    $cs->registerScript('alertMessage', '
        alert("' . $app->user->getFlash('message') . '");
    ', CClientScript::POS_READY);
}
?>
