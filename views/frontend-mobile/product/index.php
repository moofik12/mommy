<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\ProductTargets;
use MommyCom\Model\Product\SizeGuideGroups;
use MommyCom\Service\SplitTesting;
use MommyCom\YiiComponent\FileStorage\FileStorageThumbnail;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\Utf8;

/* @var $this FrontController */
/* @var $event EventRecord */
/* @var $productGroup GroupedProduct */
/* @var $othersProvider CArrayDataProvider */
$app = $this->app();

$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;
$splitTesting = $app->splitTesting;
/* @var $splitTesting SplitTesting */

$this->pageTitle = $productGroup->product->name . ' — ' . $productGroup->event->name;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
/** @var $cs CClientScript */
$cs = $app->clientScript;
$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/sizeguide.css'));
$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/sizeguide-modal.css'));
$currentUrl = $this->createAbsoluteUrl($req->url);
$eventMailingStartAt = $event->is_drop_shipping ? $event->end_at : $event->mailing_start_at;
/* @var FileStorageThumbnail[] $images */
$images = array_slice($productGroup->product->getPhotogalleryImages(), 0, 5);
$firstImage = $images ? reset($images) : null;
$isPastEvent = ($event->end_at <= time());
$urlManager = $this->app()->getComponent('urlManager');
?>
<script type="text/javascript">
    (function () {
        var mailruData = {
            productid: '<?= $productGroup->product->id ?>',
            totalvalue: '<?= $productGroup->price ?>'
        };
        Mamam.mailru.send(Mamam.mailru.PAGE_LEVEL_PRODUCT, mailruData);
    })();
</script>

<div class="main product">
    <div class="inserted">
        <article class="content-body card-body" itemscope itemtype="http://schema.org/Product">
            <?= $this->renderPartial('//layouts/_productJson', ['productGroup' => $productGroup]) ?>
            <script>
                Mamam.ecommerce.detail('<?= $productGroup->event->id . '/' . $productGroup->product->id ?>');
            </script>
            <meta itemprop="image" content="<?= $productGroup->product->getLogo()->url ?>">
            <meta itemprop="name" content="<?= $productGroup->product->name ?>">
            <meta itemprop="manufacturer" content="<?= $productGroup->product->brand->name ?>">
            <meta itemprop="color" content="<?= $productGroup->product->color ?>">
            <header class="content-header card-header">
                <h1>
                    <label class="gender">
                        <?php if ($productGroup->isForTarget(ProductTargets::TARGET_BOY) || $productGroup->isForTarget(ProductTargets::TARGET_MAN)) { ?>
                            <i class="header-icon male"></i>
                        <?php } ?>
                        <?php if ($productGroup->isForTarget(ProductTargets::TARGET_GIRL) || $productGroup->isForTarget(ProductTargets::TARGET_WOMAN)) { ?>
                            <i class="header-icon female"></i>
                        <?php } ?>
                    </label>
                    <span class="title"><?= $productGroup->product->name ?></span>
                </h1>
            </header>
            <div class="clearfix">
                <div class="image-bar">
                    <div class="box">
                        <div class="image-wrapper">
                            <div class="label-discount-lower">-<?= round((1 - $productGroup->price / $productGroup->priceOld) * 100) ?>%</div>
                            <?php if ($firstImage): ?>
                                <img src="<?= $firstImage->getThumbnail('mid380')->url ?>">
                            <?php endif; ?>
                        </div>
                        <ul class="image-list <?= count($images) <= 1 ? 'hide-i' : '' ?>">
                            <?php foreach ($images as $image) { ?>
                                <li class="<?= $image === $firstImage ? 'active' : '' ?>" data-image-item="true"
                                    data-big-url=<?= $image->getThumbnail('mid380')->url ?>>
                                    <div class="hover-style"></div>
                                    <img src="<?= $image->getThumbnail('small70')->url ?>">
                                </li>
                            <?php } ?>
                            <?php if (count($images) < 5 && $event->size_chart_fileid) : ?>
                                <li data-big-url="<?= $event->sizeChart->getThumbnail('mid380')->url ?>"
                                    data-source-url="<?= $event->sizeChart->file->getWebFullPath() ?>">
                                    <div class="hover-style"></div>
                                    <?= CHtml::image($event->sizeChart->getThumbnail('small70')->url) ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <script> $('.image-bar').productCarousel(); </script>
                </div>
                <div class="card">
                    <div class="card-timer">
                        <div class="promotional-time">
                            <span class="style"><?= CHtml::tag(
                                    'time',
                                    ['datetime' => $tf->formatMachine($event->end_at), 'data-countdown' => true],
                                    $tf->format($event->end_at)
                                ) ?></span>
                        </div>
                    </div>
                    <div class="card-inner">
                        <form class="card-form">
                            <input type="hidden" name="token"
                                   value="<?= $tokenManager->getToken($productGroup->product->id) ?>">
                            <input type="hidden" name="eventId" value="<?= $event->id ?>">
                            <input type="hidden" name="productId" value="<?= $productGroup->product->id ?>">
                            <input type="hidden" name="titleProduct" value="<?= $productGroup->product->name ?>">
                            <input type="hidden" name="urlProduct"
                                   value="<?= $productGroup->product->logo->getThumbnail('small60')->url ?>">
                            <input type="hidden" name="price" value="<?= $cf->format($productGroup->price) ?>">
                            <ul>
                                <li>
                                    <meta itemprop="priceCurrency" content="UAH">
                                    <label class="label"><?= $this->t('Price') ?></label>
                                    <div class="form-price">
                                        <?php if ($productGroup->priceOld != $productGroup->price): ?>
                                            <div class="old-price"><?= $cf->format($productGroup->priceOld, $cn->getName('short')) ?></div>
                                            <div class="new-price">
                                                <span class="number" itemprop="price"><?= $cf->format($productGroup->price) ?></span>
                                            </div>
                                        <?php else: ?>
                                            <div class="old-price"><?= $cf->format($productGroup->priceOld, $cn->getName('short')) ?></div>
                                        <?php endif ?>
                                    </div>
                                </li>
                                <li class="size" <?php if ($productGroup->sizeCount == 0) { ?> style="display:none;" <?php } ?>>
                                    <label class="label"><?= $this->t('Size') ?></label>
                                    <div class="form-size">
                                        <label class="extselect-container extselect-single extselect-state-readonly extselect-state-enabled"
                                               tabindex="0" style="">
                                            <div class="extselect-inputwrapper"><input type="hidden" name="size"
                                                                                       value=""><span
                                                        class="extselect-input"><?= $this->t('Choose a size') ?></span></div>
                                            <div class="extselect-btn"></div>
                                            <select>
                                                <?php foreach ($productGroup->eventProducts as $product): ?>
                                                    <?php
                                                    $numberAvailable = $product->numberAvailable;
                                                    $text = $product->size;
                                                    if ($numberAvailable == 0) {
                                                        $text .= ' (' . $this->t('ended') . ')';
                                                    }
                                                    ?>
                                                    <?= CHtml::tag(
                                                        'option',
                                                        [
                                                            'value' => $product->size,
                                                            'data-number-available' => $numberAvailable,
                                                            'data-max-per-buy' => min($numberAvailable, $product->max_per_buy),
                                                            'data-price' => $product->price,
                                                            'data-price-old' => $product->price_market,
                                                            'data-discount' => $productGroup->discountPercent,
                                                            'data-currency' => $cf->getCurrency(),
                                                        ],
                                                        $text
                                                    ) ?>
                                                <?php endforeach ?>
                                            </select>
                                        </label>
                                        <?php $sizeGuide = $productGroup->getSizeGuide() === '' ? false : SizeGuideGroups::instance()->getGroupTargetForAssociation($productGroup->getSizeGuide()) ?>
                                        <div class="size-description <?= $sizeGuide ? 'new-size-description' : 'hide-i' ?>">
                                            <?php if ($sizeGuide) : ?>
                                                <a class="anchor-size-modal" data-toggle="modal" href="#sizeGuideModal"
                                                   data-src="<?= $app->createUrl('sizeGuide/group', ['name' => $sizeGuide]) ?>"><?= $this->t('which size to choose') ?>
                                                    <svg version="1.2" preserveAspectRatio="none" viewBox="0 -6 24 24"
                                                         class="ng-element" data-id="8c248f5a3e3ff78a6f53693b32d7d296"
                                                         style="opacity: 1; fill: rgb(223, 90, 109); margin-top: 6px; width: 20px; height: 20px;">
                                                        <g>
                                                            <path
                                                                    d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"
                                                                    style="fill: rgb(223, 90, 109);"></path>
                                                        </g>
                                                    </svg>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                        <script> $('.card-form .extselect-container').simpleExtSelect(); </script>
                                    </div>
                                </li>
                                <li class="cart-form-number">
                                    <label class="label"><?= $this->t('Quantity') ?></label>
                                    <div class="form-number" data-min="1"
                                         data-max="<?= min(reset($productGroup->eventProducts)->numberAvailable, $productGroup->maxPerBuy) ?>">
                                        <div class="minus">&#8722;</div>
                                        <input type="hidden" name="number" value="1">
                                        <input type="text" value="1">
                                        <div class="plus">&#43;</div>
                                    </div>
                                    <script> $('.card-form .form-number').numericalSpinner(); </script>
                                </li>
                                <li>
                                    <input type="submit"
                                           class="btn green <?= $isPastEvent ? 'disabled' : '' ?>" <?= $isPastEvent ? 'disabled="disabled"' : '' ?>
                                           value="<?= $this->t('Add to cart') ?>"
                                </li>
                            </ul>
                        </form>
                        <ul class="product-info">
                            <?php if ($event->is_drop_shipping && $event->supplier): ?>
                                <li>
                                    <label class="label"><?= $this->t('Provider') ?></label>
                                    <span class="info"><?= CHtml::encode($event->supplier->getDisplayName()) ?></span>
                                </li>
                            <?php endif; ?>
                            <?php foreach ($productGroup->productBaseAttributes as $attribute => $data): ?>
                                <li>
                                    <label class="label"><?= CHtml::encode($data['label']) ?></label>
                                    <span class="info"><?= CHtml::encode($data['value']) ?></span>
                                </li>
                            <?php endforeach ?>
                            <li>
                                <label class="label"><?= $this->t('Delivery') ?></label>
                                <span class="info"><?= $this->t('from') ?> <?= $df->format('d MMMM yyyy', $eventMailingStartAt) ?> <?= $this->t('year') ?></span>
                            </li>
                        </ul>
                    </div>
                    <!-- Size guide box -->
                    <div class="size-guide-body">
                    </div>
                    <script>
                        $('.anchor-size-modal').on('click', function () {
                            $('.size-guide-body').show();
                            $('.card-inner').hide();
                        });
                        $(document).on('click', '.size-close-button', function () {
                            $('.size-guide-body').hide();
                            $('.card-inner').show();
                        });
                    </script>
                    <!-- Size modal block end -->
                </div>
            </div>
            <aside>
                <ul class="product-info-list">
                    <li>
                        <a href="javascript:void(0);"><?= $this->t('Description') ?><b></b></a>
                        <ul class="product-info-text" style="display: none;">
                            <li>
                                <span class="text">
                                    <?= $productGroup->product->description ?>
                                </span>
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);"><?= $this->t('Learn more') ?><b></b></a>
                        <ul class="product-info-content">
                            <?php if ($event->is_drop_shipping && $event->supplier): ?>
                                <li>
                                    <label class="label"><?= $this->t('Provider') ?></label>
                                    <span class="info"><?= CHtml::encode($event->supplier->getDisplayName()) ?></span>
                                </li>
                            <?php endif; ?>
                            <?php foreach ($productGroup->productBaseAttributes as $attribute => $data): ?>
                                <li>
                                    <label class="label"><?= CHtml::encode($data['label']) ?></label>
                                    <span class="info"><?= CHtml::encode($data['value']) ?></span>
                                </li>
                            <?php endforeach ?>
                            <li>
                                <label class="label"><?= $this->t('Delivery') ?></label>
                                <span class="info"><?= $this->t('from') ?> <?= $df->format('d MMMM yyyy', $eventMailingStartAt) ?> <?= $this->t('year') ?></span>
                            </li>
                            <?php if (!empty($productGroup->getCertificate())): ?>
                                <li>
                                    <label class="label"><?= $this->t('Certificate') ?></label>
                                    <span class="info"><?= $productGroup->getCertificate() ?></span>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php if (!empty($productGroup->getCertificate())): ?>
                        <li>
                            <a href="javascript:void(0);"><?= $this->t('Certificate') ?><b></b></a>
                            <ul class="product-info-text" style="display: none;">
                                <li>
                        <span class="text certificate">
                            <?= $this->t('You can check certificate on the next website') ?>
                        </span>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="javascript:void(0);"><?= $this->t('Delivery') ?><b></b></a>
                        <ul class="product-info-text" style="display: none;">
                            <li>
                    <span class="text">
                        <?= $this->t('from') ?> <?= $df->format('d MMMM yyyy', $eventMailingStartAt) ?> <?= $this->t('year') ?>
                    </span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><?= $this->t('Payment') ?><b></b></a>
                        <ul class="product-info-text" style="display: none;">
                            <li>
                                <span class="text">
                                    <?= $this->t('Cash on delivery') ?>
                                </span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><?= $this->t('Return') ?><b></b></a>
                        <ul class="product-info-text" style="display: none;">
                            <li>
                                <span class="text"><?= $this->t('It is carried out within 14 days from the date of purchase, except for personal hygiene, hosiery, underwear, and other goods not subject to return in accordance with the law.') ?></span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </aside>
            <div class="cart-relation-buttons">
                <input onclick="$('form.card-form').submit()" type="submit" class="btn green"
                       value="<?= $this->t('Add to cart') ?>">
            </div>
        </article>
        <?php if ($this->beginCache('mobile-product-others-new', ['duration' => 30, 'varyByLanguage' => true, 'varyByRoute' => true, 'varyByParam' => ['id', 'eventId']])) { ?>
            <?php if ($othersProvider->totalItemCount >= 6): ?>
                <aside class="clearfix">
                    <h3 class="main-title"><?= $this->t('People who viewed this item also viewed') ?></h3>
                    <div class="other-goods">
                        <?php foreach (array_slice($othersProvider->getData(), 0, 6) as $item): /* @var $item GroupedProduct */ ?>
                            <div class="other-item" itemscope itemtype="http://schema.org/Product">
                                <a href="<?= $app->createUrl('product/index', ['id' => $item->product->id, 'eventId' => $item->event->id]) ?>">
                                    <div class="box">
                                        <div class="image-wrapper">
                                            <?= CHtml::image($item->product->logo->getThumbnail('small150')->url, '', ['itemprop' => "image"]) ?>
                                        </div>
                                        <div class="text-wrapper">
                                            <h4 class="title"
                                                temprop="name"><?= CHtml::encode($item->product->name) ?></h4>
                                            <div class="price">
                                                <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                                <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach ?>
                    </div>
                </aside>
            <?php endif ?>
            <?php $this->endCache();
        } ?>
    </div>
    <aside class="bottom-likes-bar hide">
        <div class="likes-bar">
            <h3><?= $this->t('Share with friends:') ?></h3>
            <div>
                <?= CHtml::link(
                    'Facebook',
                    'https://www.facebook.com/sharer/sharer.php?' .
                    'u=' . urlencode($currentUrl),
                    ['class' => 'fb', 'target' => '_blank'])
                ?>
            </div>
            <div>
                <?= CHtml::link(
                    'Twitter',
                    'https://twitter.com/share?' .
                    'url=' . urlencode($currentUrl) . '&' .
                    'text=' . urlencode($this->pageTitle),
                    ['class' => 'tw', 'target' => '_blank'])
                ?>
            </div>
            <div>
                <?= CHtml::link(
                    'Vkontakte',
                    'https://vk.com/share.php?' .
                    'url=' . urlencode($currentUrl) . '&' .
                    'title=' . urlencode($this->pageTitle) . '&' .
                    'image=' . urlencode($productGroup->product->logo->getThumbnail('mid380')->url) . '&' .
                    'description=' . urlencode(Utf8::truncate($productGroup->product->description)) . '&' .
                    'noparse=true',
                    ['class' => 'vk', 'target' => '_blank'])
                ?>
            </div>
            <div>
                <?= CHtml::link(
                    'Google+',
                    'https://plus.google.com/share?' .
                    'url=' . urlencode($currentUrl) . '&' .
                    'hl=' . $app->locale->id,
                    ['class' => 'gp', 'target' => '_blank'])
                ?>
            </div>
        </div>
    </aside>
</div>
<script>
    /*Modal box info request*/
    $(document).ready(function () {
        $('.size-guide-body').hide();
        var url = $('a[href="#sizeGuideModal"]').data('src');
        $.ajax({
            url: url,
            dataType: 'html',
            success: function (data) {
                var actualData = $(data).find('section').html();
                var closeSpan = "<div class='size-close-button'>×</div>";
                actualData = closeSpan + actualData;
                $('.size-guide-body').html(actualData);
            }
        });
    });
</script>
