<div class="forgot" <?php if (!$visible) { ?> style="display: none;" <?php } ?> id="forgot">
    <?php $form = $this->beginWidget('CActiveForm', [
        'action' => $this->app()->createUrl('auth/recovery'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'beforeValidate' => 'js:function($form) {
                    var $field = $("#RecoveryPasswordForm_email");
                    if ($field && $field.val()) {
                        $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                    }
                    
                    return true;
                }',
        ],
    ]) ?>
    <div class="form-title"><?= $this->t('Password recovery') ?></div>
    <?= $form->labelEx($recoveryForm, 'email') ?>
    <div class="field top <?= $recoveryForm->hasErrors('email') ? 'error' : '' ?>">
        <?= $form->textField($recoveryForm, 'email', ['placeholder' => $this->t('Your E-mail')]) ?>
        <?= $form->error($recoveryForm, 'email', ['class' => 'message']) ?>
    </div>
    <?= CHtml::submitButton($this->t('Send the letter')) ?>
    <?php $this->endWidget() ?>
    <div class="help"><?= $this->t('In the letter there will be a link to reset the password.') ?></div>
</div>
