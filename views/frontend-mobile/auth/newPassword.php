<?php

use MommyCom\Controller\FrontendMobile\AuthController;
use MommyCom\Model\Form\NewPasswordForm;

/**
 * @var $this AuthController
 * @var $form CActiveForm
 * @var  $formPassword NewPasswordForm
 */

$this->pageTitle = $this->t('Password recovery');
$app = $this->app();
?>

<div class="login-wrapper">
    <div class="forgot">
        <?php $form = $this->beginWidget('CActiveForm', [
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => false,
            ],
        ]) ?>
        <div class="form-title"><?= $this->t('Password recovery') ?></div>
        <?= $form->labelEx($formPassword, 'password') ?>
        <div class="field bottom <?= $formPassword->hasErrors('email') ? 'error' : '' ?>">
            <?= $form->passwordField($formPassword, 'password', ['placeholder' => $this->t('New password')]) ?>
            <?= $form->error($formPassword, 'password', ['class' => 'message']) ?>
        </div>
        <?= CHtml::submitButton($this->t('Save')) ?>
        <?php $this->endWidget() ?>
    </div>
</div>
