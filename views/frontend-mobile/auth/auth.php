<?php

use MommyCom\Controller\FrontendMobile\AuthController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\RecoveryPasswordForm;

/**
 * @var $this AuthController
 * @var $authForm AuthForm
 * @var $recoveryForm RecoveryPasswordForm
 * @var $registrationForm UserRecord
 * @var $form CActiveForm
 * @var $tab string
 * @var $social string
 * @var $tokenExpired bool
 */

$app = $this->app();
$this->pageTitle = $this->t('Login and Registration');
?>
<?php if ($app->user->hasFlash('message')): ?>
    <script>
        alert("<?= $app->user->getFlash('message')?>");
    </script>
<?php endif; ?>
<div class="tabs">
    <div class="tab <?= $tab === 'login' ? 'active' : '' ?>" data-tab="login"><?= $this->t('Login') ?></div>
    <div class="tab <?= $tab === 'registration' ? 'active' : '' ?>"
         data-tab="registration"><?= $this->t('check in') ?></div>
</div>
<div class="login-wrapper">
    <?php $this->renderPartial(
        '_logIn',
        [
            'authForm' => $authForm,
            'visible' => $tab === 'login',
        ]
    ) ?>

    <?php $this->renderPartial(
        '_registration',
        [
            'registrationForm' => $registrationForm,
            'visible' => $tab === 'registration',
        ]
    ) ?>

    <?php $this->renderPartial(
        '_forgot',
        [
            'recoveryForm' => $recoveryForm,
            'visible' => $tab === 'forgot',
        ]
    ) ?>
</div>
<?php if ($tokenExpired): ?>
    <script>
            alert('<?= $this->t("Unfortunately the link is no longer valid, send a new password recovery request") ?>');
    </script>
<?php endif; ?>