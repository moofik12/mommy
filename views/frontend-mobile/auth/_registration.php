<div class="registration" <?php if (!$visible) { ?> style="display: none;" <?php } ?> id="registration">
    <script type="application/javascript">
        registrationTracker.validatorRegex = <?= json_encode($this->app()
            ->params['validatorRegex']) ?>;
    </script>
    <?php
    $form = $this->beginWidget('CActiveForm', [
        'action' => $this->app()->createUrl('auth/registration'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'beforeValidate' => 'js:function($form) {
                    var $field = $("#RegistrationForm_email");
                    if ($field && $field.val()) {
                        $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                    }
                    
                    return true;
                }',
            'afterValidate' => 'js:function($form, data, hasError) {
                    registrationTracker.overrideSettings({
                        errorBorderWidth: "5px",
                        errorBorderColor: "#ff7591",
                        successBorderWidth: "5px"
                    });

                    if (!registrationTracker.validateEmail("#RegistrationForm_email")) {
                        return false;
                    }

                    $(document).trigger("mamam.registration.success");

                    return true;
                }',
        ],
    ]);
    ?>
    <div class="field top <?= $registrationForm->hasErrors('email') ? 'error' : '' ?>">
        <label class="visible-title"><?= $this->t('Your E-mail') ?>:</label>
        <?= $form->textField($registrationForm, 'email', ['placeholder' => $this->app()
            ->params['validatorRegex']['email']['default']['placeholder']]); ?>
        <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
    </div>
    <?= CHtml::submitButton($this->t('Start shopping')); ?>
    <?php $this->endWidget() ?>
    <div class="social-enter-container">
        <span class="title-services"><?= $this->t('or through') ?></span>
        <div class="services clearfix">
            <ul>
                <li><a class="fb" href="/connect/facebook">Facebook</a></li>
            </ul>
        </div>
    </div>
    <div class="auth-modal-bottom-wrapp">
        <div class="more-discounts-text-right">
            <!--<div class="more-discounts-img"></div>-->
            <p><?= $this->t('We are giving you the opportunity to register to the mommy shopping club!') ?></p>
        </div>
        <div class="more-discounts-text-center">
            <p><?= $this->t('Hurry up to get access to the lowest prices for <span>{count} popular products</span>',
                    ['{count}' => 2729]) ?></p>
            <p></p>
        </div>
        <div class="more-discounts-text-bottom">
            <p><?= $this->t('The number of places in our club is limited!') ?></p>
        </div>
    </div>
</div>

