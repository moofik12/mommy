<div class="enter" <?php if (!$visible) { ?> style="display: none;" <?php } ?> id="login">
    <?php $form = $this->beginWidget('CActiveForm', [
        'enableAjaxValidation' => true,
        'action' => $this->app()->createUrl('auth/authenticate'),
        'enableClientValidation' => true,
        'clientOptions' => [
            'submitting' => true,
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'beforeValidate' => 'js:function($form) {
                    var $field = $("#AuthForm_email");
                    if ($field && $field.val()) {
                        $field.val($field.val().replace(/^\s+|\s+$/g, ""));
                    }
                    
                    return true;
                }',
            'afterValidate' => "js:function(form, data, hasError){
                                    if (hasError) {
                                        return;
                                    }                
                                    $(document).trigger('mamam.auth.success');
                                    return true;
                                }",
        ],
    ]) ?>
    <?= $form->labelEx($authForm, 'email') ?>
    <div class="field top <?= $authForm->hasErrors('email') ? 'error' : '' ?>">
        <label class="visible-title"><?= $this->t('Your E-mail') ?>:</label>
        <?= $form->textField($authForm, 'email', ['placeholder' => $this->app()
            ->params['validatorRegex']['email']['default']['placeholder']]) ?>
        <?= $form->error($authForm, 'email', ['class' => 'message']) ?>
    </div>
    <label class="visible-title"><?= $this->t('Your password') ?>:</label>
    <div class="field top <?= $authForm->hasErrors('password') ? 'error' : '' ?>">
        <?= $form->passwordField($authForm, 'password', ['placeholder' => $this->t('Password')]) ?>
        <?= $form->error($authForm, 'password', ['class' => 'message']) ?>
    </div>
    <div class="reminder-field" data-tab="forgot"><?= $this->t('Forgot password?') ?></div>
    <?= CHtml::submitButton($this->t('Log in')) ?>
    <?php $this->endWidget() ?>
    <div class="social-enter-container">
        <span class="title-services"><?= $this->t('or through') ?></span>
        <div class="services clearfix">
            <ul>
                <li><a class="fb" href="/connect/facebook">Facebook</a></li>
            </ul>
        </div>
    </div>
</div>
