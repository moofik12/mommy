<?php

use MommyCom\Model\Form\NetworkRegistrationConfirmForm;
use MommyCom\Model\Form\NetworkRegistrationForm;

/**
 * @var $registrationSocial NetworkRegistrationForm
 * @var $registrationSocialConfirm NetworkRegistrationConfirmForm
 * @var $action string
 */

$this->pageTitle = $this->t('Completion of registration');
?>
<div class="login-wrapper">
    <div class="registration <?= $action == 'email' ? '' : 'hide' ?>" data-tab="networkFinishRegistration">
        <?php $form = $this->beginWidget('CActiveForm', [
            'enableAjaxValidation' => true,
            'action' => $this->app()->createUrl('auth/networkFinishRegistration'),
            'enableClientValidation' => true,
            'clientOptions' => [
                'submitting' => true,
                'validateOnSubmit' => true,
                'afterValidate' => 'js:function($form, data, hasError) {
                                    if (hasError) {
                                        return;
                                    }
                                    
                                    $.ajax({
                                        url: $form.attr("action"),
                                        type: $form.attr("method"),
                                        data: $form.serialize(),
                                        dataType: "json",
                                        success: function (data) {
                                            if (data !== null && typeof data === "object") {
                                                if (data.success) {
                                                    window.location.replace(Mamam.createUrl("index/index", {typeMailing:1}));
                                                } else if (data.errors && data.errors.length > 0) {
                                                    var error = data.errors[0];
                                                    if (error == "CONFIRM_PASSWORD") {
                                                        $("[data-tab=\'networkFinishRegistration\']").addClass("hide");
                                                        $("[data-tab=\'networkConfirmPassword\']").removeClass("hide");
                                                        return;
                                                    }
    
                                                    alert(error);
                                                }
                                            }
                                        },
                                        error: function () {
                                            alert("' . $this->t('Error. Check your internet connection') . '");
                                        }
                                    });
    
                                    return false;
                            }',
            ],
        ]) ?>
        <div class="form-title"><?= $this->t('Completion of registration') ?></div>
        <?= $form->labelEx($registrationSocial, 'email') ?>
        <div class="field top <?= $registrationSocial->hasErrors('email') ? 'error' : '' ?>">
            <?= $form->textField($registrationSocial, 'email', ['placeholder' => 'E-mail *']) ?>
            <?= $form->error($registrationSocial, 'email', ['class' => 'message']) ?>
        </div>
        <div class="help"><?= $this->t('We need your e-mail so that we can send you important notices and personal offers') ?></div>
        <?= CHtml::submitButton($this->t('Log in')) ?>
        <?php $this->endWidget() ?>
    </div>
    <div class="enter <?= $action === 'password' ? '' : 'hide' ?>" data-tab="networkConfirmPassword">
        <?php $form = $this->beginWidget('CActiveForm', [
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'action' => $this->app()->createUrl('auth/networkFinishRegistration', ['action' => 'password']),
            'clientOptions' => [
                'submitting' => true,
                'validateOnSubmit' => true,
                'afterValidate' => 'js:function($form, data, hasError) {
                                if (hasError) {
                                    return;
                                }
                                
                                $.ajax({
                                    url: $form.attr("action"),
                                    type: $form.attr("method"),
                                    data: $form.serialize(),
                                    dataType: "json",
                                    success: function (data) {
                                        if (data !== null && typeof data === "object") {
                                            if (data.success) {
                                                window.location.replace(Mamam.createUrl("index/index", {typeMailing:1}));
                                            } else if (data.errors && data.errors.length > 0) {
                                                var error = data.errors[0];
                                                alert(error);
                                            }
                                        }
                                    },
                                    error: function () {
                                        alert("' . $this->t('Error. Check your internet connection') . '");
                                    }
                                });

                                return false;
                        }',
            ],
        ]) ?>
        <div class="form-title"><?= $this->t('You are already registered. Enter your password for MOMMY.') ?></div>
        <?= $form->labelEx($registrationSocialConfirm, 'password') ?>
        <div class="field top <?= $registrationSocialConfirm->hasErrors('password') ? 'error' : '' ?>">
            <?= $form->passwordField($registrationSocialConfirm, 'password', ['placeholder' => $this->t('Password')]) ?>
            <?= $form->error($registrationSocialConfirm, 'password', ['class' => 'message']) ?>
        </div>
        <?= CHtml::submitButton($this->t('Log in')) ?>
        <div class="login-checkbox">
            <?= CHtml::ajaxLink(
                $this->t('Password reminder'),
                $this->app()->createUrl('auth/networkFinishRegistrationForgotPassword'),
                [
                    'success' => 'function(data) {
                                    if (data.success) {
                                        if (data.info) {
                                            alert(data.info);
                                        }
                                        return;
                                    }
                                    
                                    var error = data.errors.length > 0 ? data.errors[0] : false;
                                    if (error) {
                                        alert(error);
                                    }
                                }',
                    'error' => 'function() {
                                    alert("' . $this->t('Error. Check your internet connection') . '");
                                }',
                ],
                ['class' => 'reminder']
            ) ?>
        </div>
        <?php $this->endWidget() ?>
    </div>
</div>
