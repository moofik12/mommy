<?php

use MommyCom\Controller\Frontend\ClubController;

/**
 * @var ClubController $this
 * @var string $subId
 * @var int $landingNum
 */

$registrationTrackerJs = $this->app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.front')) . '/js/registration.tracker.js';
$textError = $this->t('Error. Check your internet connection');

?>
<div class="wrapper">
    <div class="header">
        <div class="limit">
            <div class="right">
                <div class="logo">Mommy</div>
                <h1><?= $this->t('Clothes for children and their parents') ?>
                    <b><?= $this->t('With big') ?> <?= $this->t('Discounts') ?></b></h1>
                <p><?= $this->t('At this age, it is important to pay attention to the primary development of the child: To develop large and small motor skills, to teach to speak and understand. Our club mommy.com collected a unique selection of products that helps kids avoid problems with undeveloped speech, intelligence and behavior') ?></p>
                <a href="#" class="button to_form"><?= $this->t('Join the club') ?></a>
            </div>
            <img src="<?= $this->assetPath('/transfer/img/header.png') ?>" alt="" class="img-responsive">
        </div>
    </div>

    <section class="s2">
        <div class="limit">
            <div class="text-center">
                <h2><?= $this->t('Many mommies') ?> <?= $this->t('often face these problems') ?>:</h2>
                <div class="s2-list">
                    <div class="s2-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s2-1.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Too little time for housework and child') ?></p>
                    </div>
                    <div class="s2-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s2-2.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Too little money for children\'s accessories') ?></p>
                    </div>
                    <div class="s2-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s2-3.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Lack of experience in upbringing') ?></p>
                    </div>
                    <div class="s2-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s2-4.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Moms often do not know what kind of things are necessary for the growth of the child') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="s3">
        <div class="limit">
            <div class="text-center">
                <h2><?= $this->t('We have the solution for you') ?></h2>
                <img src="<?= $this->assetPath('/transfer/img/next.png') ?>" alt="">
            </div>
            <ul class="slider active1">
                <li class="change">
                    <h3> <?= $this->t('For children 5-7 years old') ?> </h3>
                    <img src="<?= $this->assetPath('/transfer/img/slider_item1.jpg') ?>" alt="">
                    <img src="<?= $this->assetPath('/transfer/img/slide1m.png') ?>" alt="" class="mob_sl">
                    <div class="text_bg">
                        <div class="text">
                            <p>
                                <?= $this->t('At this age, it is important to pay attention to the primary development of the child: To develop large and small motor skills, to teach to speak and understand. Our club mommy.com collected a unique selection of products that helps kids avoid problems with undeveloped speech, intelligence and behavior') ?>
                                .
                                <?= $this->t('You can get acquainted with her on the site of the club\'s store and choose what you like with a discount of up to 65 percent from mommy.com') ?>
                            </p>
                        </div>
                    </div>
                    <span class="cloud"><?= $this->t('1-5 years') ?></span>
                </li>
                <li class="change">
                    <h3> <?= $this->t('For children 5-7 years old') ?> </h3>
                    <img src="<?= $this->assetPath('/transfer/img/slider_item2.jpg') ?>" alt="">
                    <img src="<?= $this->assetPath('/transfer/img/slide2m.png') ?>" alt="" class="mob_sl">
                    <div class="text_bg">
                        <div class="text">
                            <p>
                                <?= $this->t('You need to think about the school just at this time, so that your child would like to learn and you did not spoil relations with him, our club has prepared for you the best assistants in the fight against refusal to study, poor discipline and lack of attention') ?>
                                .
                                <?= $this->t('You can find out more on the site of the club\'s store and choose what you like with a discount of up to 75 percent from mommy.com') ?>
                                .
                            </p>
                        </div>
                    </div>
                    <span class="cloud"><?= $this->t('5-7 years') ?></span>
                </li>
                <li class="change">
                    <h3> <?= $this->t('For children 7-15 years old') ?> </h3>
                    <img src="<?= $this->assetPath('/transfer/img/slider_item3.jpg') ?>" alt="">
                    <img src="<?= $this->assetPath('/transfer/img/slide3m.png') ?>" alt="" class="mob_sl">
                    <div class="text_bg">
                        <div class="text">
                            <p>
                                <?= $this->t('At this age, children face the problems, which means that their parents too. The child should feel demanded and move to something') ?>
                                .
                                <?= $this->t('In this difficult issue mommy.com has a lot of experience, we will give advice on how to find your child a lesson to enjoy and improve school performance, overcome aggression and not get into a bad company') ?>
                                .
                            </p>
                        </div>
                    </div>
                    <span class="cloud"><?= $this->t('7-15 years') ?></span>
                </li>
                <li class="sale">
                    <p> <?= $this->t('Discounts up to') ?> <br>
                        <span class="sale_item1">65%!</span>
                        <span class="sale_item2">75%!</span>
                        <span class="sale_item3">85%!</span>
                    </p>
                </li>
            </ul>
        </div>
    </section>

    <section class="s4">
        <div class="limit">
            <div class="text-center">
                <h2><?= $this->t('By joining the club') ?>, <?= $this->t('You will also earn') ?>:</h2>
                <div class="s4-list">
                    <div class="s4-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s4-1.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Discounts in the online shop') ?></p>
                    </div>
                    <div class="s4-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s4-2.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Advices of specialists, professional educators and upbringers') ?></p>
                    </div>
                    <div class="s4-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s4-3.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Support and communication with other parents') ?></p>
                    </div>
                    <div class="s4-list-item">
                        <img src="<?= $this->assetPath('/transfer/img/s4-4.png') ?>" alt="" class="img-responsive">
                        <p><?= $this->t('Unique knowledge that we collected especially for you') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="s5">
        <div class="limit">
            <img src="<?= $this->assetPath('/transfer/img/doc.png') ?>" alt="" class="img-responsive mob-hidden">
            <img src="<?= $this->assetPath('/transfer/img/mob-doc.png') ?>" alt="" class="img-responsive desc-hidden">
            <div class="right">
                <h3><?= $this->t('Mary Evon Grandy') ?></h3>
                <span><?= $this->t('pedagogue-psychologist') ?></span>
                <ul>
                    <li><?= $this->t('Higher qualification pedagogical category') ?></li>
                    <li><?= $this->t('She graduated from the Australian State Pedagogical University of Murdoch, psychology and pedagogical faculty') ?></li>
                    <li><?= $this->t('Experience of practical consultation for more than 10 years') ?></li>
                </ul>
                <p>
                    “<?= $this->t('To grow a child happy, you need to have a lot of experience and give maximum attention to each of the stages of growing up. I will tell you in details about the children\'s age features in the club mommy.com, as well as answer your questions') ?>
                    ”</p>
            </div>
        </div>
    </section>
    <section class="s6">
        <div class="limit">
            <div class="left">
                <h3><?= $this->t('Save time and money with other mommy.com members') ?></h3>
                <p>
                    <?= $this->t('Participation in the club is free of charge and does not oblige you to make purchases, the above discounts and bonuses are valid only for members of the club') ?>
                </p>
                <?php $form = $this->beginWidget('CActiveForm', [
                    'action' => $this->app()->createUrl('club/registration', ['landing' => $landingNum]),
                    'htmlOptions' => [
                        'class' => 'form',
                        'id' => 'order_form',
                    ],
                ]) ?>
                <label for="RegistrationForm_email"
                       style="font-size: 14px; display: block; text-align: left; margin: 0 0 -10px 10px">
                    <?= $this->t('Enter your e-mail') ?>:
                </label>
                <?= $form->textField($registrationForm, 'email', [
                    'required' => false,
                    'class' => 'email',
                    'placeholder' => $this->app()->params['validatorRegex']['email']['default']['placeholder'],
                ]) ?>

                <?= CHtml::submitButton($this->t('Join the club'), ['class' => 'button js_submit']) ?>
                <?php $this->endWidget() ?>
            </div>

            <img src="<?= $this->assetPath('/transfer/img/footer.png') ?>" alt="" class="img-responsive">

        </div>
    </section>

</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="application/javascript" src="<?= $registrationTrackerJs ?>"></script>
<script>
    registrationTracker.validatorRegex = <?= json_encode($this->app()->params['validatorRegex']) ?>;

    $(document).ready(function () {
        var $slider = $('.slider'),
            slideNum = 1,
            sliderTimer;

        var nextSlide = function () {
            slideNum++;
            if (slideNum > 3) slideNum = 1;

            $slider.removeClass('active1 active2 active3').addClass('active' + slideNum);
        };
        var scheduleNextSile = function () {
            clearTimeout(sliderTimer);
            sliderTimer = setTimeout(nextSlide, 15000);
        };

        scheduleNextSile();

        $('.change').click(function () {
            scheduleNextSile();
            slideNum = $('.change').index(this) + 1;

            if ($slider.hasClass('active' + slideNum)) {
                $slider.removeClass('active1 active2 active3');
            } else {
                $slider.removeClass('active1 active2 active3').addClass('active' + slideNum);
            }
        });

        $('.to_form').click(function () {
            $("html, body").animate({
                scrollTop: $("#order_form").offset().top - 300
            }, 1000);
            return false;
        });

        $('#order_form').submit(function (e) {
            if (
                !registrationTracker.validateEmail('.email')
            ) {
                e.preventDefault();
                return false;
            } else {
                var form = $(this);
                var url = $('form').attr('action');
                $.ajax({
                    url: url,
                    type: form.attr('method'),
                    data: form.serialize(),
                    success: function (data) {
                        if (data.success) {
                            form.trigger('reset');
                            $(document).trigger('mamam.registration.success');
                        } else if (data.info) {
                            alert("<?= $this->t('This user already exists') ?>");
                        } else {
                            alert("<?= $this->t('Error. Please try again later') ?>");
                        }
                    },
                    error: function (error) {
                        alert(error.status + '<?= $textError ?>' + error.statusText);
                    }
                });
            }
            return false;
        });

        $(document).on('mamam.registration.success', function () {
            dataLayer.push({'event': 'mamam.registration.success'});

            var redirect = function () {
                window.location.href = '<?= CHtml::normalizeUrl(['club/thanks']) ?>';
            };

            if (Mamam.clickId) {
                $.ajax({
                    url: 'https://pstb.peerclicktrk.com/postback?cid=' + Mamam.clickId + '&summ_total=1&userid=2071',
                    timeout: 2000,
                    complete: redirect
                });

                return;
            }

            redirect();
        });
    });
</script>
