<?php

use MommyCom\Controller\Frontend\ClubController;
use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Service\Money\CurrencyFormatter;

/**
 * @var ClubController $this
 * @var string $subId
 * @var int $landingNum
 * @var GroupedProduct[] $groupedProducts
 * @var CurrencyFormatter $currencyFormatter
 */

$frontPath = $this->app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.front'));
$registrationTrackerJs = $frontPath . '/js/registration.tracker.js';
$ecommerceJs = $frontPath . '/js/mamam.ecommerce.js';
$visibilityJs = $frontPath . '/js/mamam.visibility.js';
$textError = $this->t('Error. Check your internet connection');

?>
<div class="welcome-block">
    <div class="limit">
        <img src="<?= $this->assetPath('/white/img/welcomeBlock.png') ?>" class="welcome-block_imagesAbs">
        <a href="#" class="logo"><img src="<?= $this->assetPath('/white/img/logo.svg') ?>" width="130"></a>
        <h1 class="welcome-block_title">
            <?= Yii::t('club', 'Welcome to the<br class="br-desktop">«Mommy» club!') ?>
        </h1>
        <p class="mob_text">
            <?= Yii::t('club', 'You can save up to 70% of your family income with «Mommy»!') ?>
        </p>
        <ul class="welcome-block_advantList">
            <li class="welcome-block_advantItem"><?= Yii::t('club', 'Free registration') ?></li>
            <li class="welcome-block_advantItem"><?= Yii::t('club', 'Savings up to 70%') ?></li>
            <li class="welcome-block_advantItem"><?= Yii::t('club', 'Only the highest-quality goods for you and your family') ?></li>
            <li class="welcome-block_advantItem"><?= Yii::t('club', 'A huge range of goods - over 3000 daily!') ?></li>
            <li class="welcome-block_advantItem"><?= Yii::t('club', 'Only the most relevant offers') ?></li>
        </ul>
        <div class="btns-wrap">
            <button class="_peach to-form"><?= Yii::t('club', 'sign up') ?></button>
            <button class="_peach scroll_to_about"><?= Yii::t('club', 'find out more') ?></button>
        </div>
    </div>
</div>
<div class="what-block">
    <div class="limit">
        <img src="<?= $this->assetPath('/white/img/whatBlock.png') ?>" class="what-block_imagesAbs">
        <div class="wrapp_stars"></div>
        <div class="what-block_right">
            <h2 class="_title">
                <?= Yii::t('club', 'What is «Mommy» ?') ?>
            </h2>
            <p class="_text">
                <?= Yii::t('club', 'The most profitable service for mommies, that saves your family budget day-to-day! Only the best high-quality goodsat the most relevant prices special for you and your babies every new day!') ?>
            </p>
            <p class="_text">
                <?= Yii::t('club', 'The most profitable club for moms will take all the budget worries over: we always have the most cosy-effective prices ! <b>Don’t believe? Check it out!</b>') ?>
            </p>
        </div>
    </div>
</div>
<div class="hottest-block">
    <div class="limit">
        <h2 class="_title">
            <?= Yii::t('club', 'Take a look at the hottest<br>offers for today!') ?>
        </h2>
        <ul class="hottest-block_offerList">
            <?php foreach ($groupedProducts as $item): ?>
                <li class="hottest-block_offerItem" data-product-id="<?= $item->event->id ?>/<?= $item->product->id ?>"
                    data-href="<?= $this->app()->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id, '#' => 'content']) ?>">
                    <div class="block_offerItem_imgWrapper">
                        <img src="<?= $item->product->logo->getThumbnail('mid320_prodlist')->url ?>"
                             class="hottest-block_offerItem_img">
                    </div>
                    <div class="hottest-block_offerItem_info">
                        <p class="hottest-block_offerItem_name"><?= CHtml::encode($item->product->name) ?></p>
                        <span class="hottest-block_offerItem_price"><?= $currencyFormatter->format($item->getPrice()) ?></span><span
                                class="hottest-block_offerItem_oldPrice"><?= $currencyFormatter->format($item->getPriceOld()) ?></span>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="steps-block">
    <div class="limit">
        <h3 class="title"><?= Yii::t('club', 'TOP-5 reasons to join our club of moms!') ?></h3>
        <ul class="steps-block_stepList">
            <li class="steps-block_stepItem _1">
                <span class="helpText"><?= Yii::t('club', 'Tap to see more') ?></span>
                <span class="wrap_number _1"><span class="number"></span></span>
                <p class="_title"><?= Yii::t('club', 'Convenient and secure methods of payment') ?></p>
                <p class="_text">
                    <?= Yii::t('club', 'You can choose for yourself what is <b>the most convenient</b> way of paying for your orders. There is a COD (Cash On Delivery) option available for those who got used to thе offline shopping, and Credit Card payments for those who are always in a rush.') ?>
                </p>
            </li>
            <li class="steps-block_stepItem _2">
                <span class="wrap_number _2"><span class="number"></span></span>
                <p class="_title"><?= Yii::t('club', 'Free next day delivery') ?></p>
                <p class="_text">
                    <?= Yii::t('club', 'Don’t miss <b>IMPORTANT</b> events and family holidays! No need to puzzle over the gifts! If the item currently is in stock, delivery will be at your doorstep already the next day!') ?>
                </p>
            </li>
            <li class="steps-block_stepItem _3">
                <span class="wrap_number _3"><span class="number"></span></span>
                <p class="_title"><?= Yii::t('club', 'Simple and easy-to-youse mobile app') ?></p>
                <p class="_text">
                    <?= Yii::t('club', 'Check your subscriptions, see items get back on stock, new promo launches and much more right on the go! Home budget <b>savings right in your pocket!</b>') ?>
                </p>
            </li>
            <li class="steps-block_stepItem _4">
                <span class="wrap_number _4"><span class="number"></span></span>
                <p class="_title"><?= Yii::t('club', 'Subscribition to your favorite brands proms') ?></p>
                <p class="_text">
                    <?= Yii::t('club', '<b>Always be up to date</b> with the best offers for you and your baby from your favorite brands! Don\'t miss the most profitable price-offers, like the brands or products that grab your attention and get notifications before the start of the offer.') ?>
                </p>
            </li>
            <li class="steps-block_stepItem _5">
                <span class="wrap_number _5"><span class="number"></span></span>
                <p class="_title"><?= Yii::t('club', 'High-quality and best prices guarantee') ?></p>
                <p class="_text">
                    <?= Yii::t('club', 'The quality and authenticity of the goods is confirmed by certificates. But, <b>if you find a similar product</b> with the same conditions and for less money, we\'ll refund the price difference!') ?>
                </p>
            </li>
        </ul>
    </div>
</div>
<div class="weAre-block">
    <div class="limit">
        <img src="<?= $this->assetPath('/white/img/girl.jpg') ?>" class="weAre-block_imgAbs">
        <div class="weAre-block_left">
            <p class="weAre-block_text">
                <?= Yii::t('club', 'We are making progress day-to-day! And, especiallyfor you and your baby, we replenish the range of goods with new categories daily:') ?>
            </p>
            <ul class="weAre-block_list">
                <li class="weAre-block_item">
                    <span class="fcWrapp"><img src="<?= $this->assetPath('/white/img/wabli1.png') ?>"
                                               class="weAre-block_itemImg"></span>
                    <p><?= Yii::t('club', 'Toys for your babies') ?></p>
                </li>
                <li class="weAre-block_item">
                    <span class="fcWrapp"><img src="<?= $this->assetPath('/white/img/wabli4.png') ?>"
                                               class="weAre-block_itemImg"></span>
                    <p><?= Yii::t('club', 'Clothes for moms') ?></p>
                </li>
                <li class="weAre-block_item">
                    <span class="fcWrapp"><img src="<?= $this->assetPath('/white/img/wabli2.png') ?>"
                                               class="weAre-block_itemImg"></span>
                    <p><?= Yii::t('club', 'Goods for coziness of your home') ?></p>
                </li>
                <li class="weAre-block_item">
                    <span class="fcWrapp"><img src="<?= $this->assetPath('/white/img/wabli5.png') ?>"
                                               class="weAre-block_itemImg"></span>
                    <p><?= Yii::t('club', 'Cosmetics and care products') ?></p>
                </li>
                <li class="weAre-block_item">
                    <span class="fcWrapp"><img src="<?= $this->assetPath('/white/img/wabli3.png') ?>"
                                               class="weAre-block_itemImg"></span>
                    <p><?= Yii::t('club', 'Hobby and leisure goods') ?></p>
                </li>
                <li class="weAre-block_item">
                    <span class="fcWrapp"><img src="<?= $this->assetPath('/white/img/wabli6.png') ?>"
                                               class="weAre-block_itemImg"></span>
                    <p><?= Yii::t('club', 'Shoes and accessories for you and your baby') ?></p>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="form-block">
    <div class="limit">
        <img src="<?= $this->assetPath('/white/img/formBlockBack.png') ?>" class="_backForm">
        <img src="<?= $this->assetPath('/white/img/formStars.png') ?>" class="_stars">
        <div class="mob_top_text">
            <h2 class="title">
                <?= Yii::t('club', '«Mommy» – the area of harmony and coziness') ?>
            </h2>
            <p class="text">
                <?= Yii::t('club', 'And, also, the way to keep your savings!') ?>
            </p>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', [
            'action' => $this->app()->createUrl('club/registration', ['landing' => $landingNum]),
            'htmlOptions' => [
                'class' => 'form',
                'id' => 'order_form',
            ],
        ]) ?>
        <p class="title"><?= Yii::t('club', 'Join the club for the moms!') ?></p>
        <label>
            <?= $form->textField($registrationForm, 'email', [
                'required' => false,
                'class' => 'email',
                'placeholder' => $this->app()->params['validatorRegex']['email']['default']['placeholder'],
            ]) ?>
        </label>
        <?= CHtml::htmlButton(Yii::t('club', 'sign up'), ['class' => '_peach']) ?>
        <?php $this->endWidget() ?>


        <div class="right">
            <h2 class="title">
                <?= Yii::t('club', '«Mommy» – the area of harmony and coziness') ?>
            </h2>
            <p class="text">
                <?= Yii::t('club', 'And, also, the way to keep your savings!') ?>
            </p>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="application/javascript" src="<?= $registrationTrackerJs ?>"></script>
<script type="application/javascript" src="<?= $ecommerceJs ?>"></script>
<script type="application/javascript" src="<?= $visibilityJs ?>"></script>
<?php
foreach ($groupedProducts as $item) {
    $this->renderPartial('//layouts/_productJson', ['productGroup' => $item]);
}
?>
<script>
    registrationTracker.validatorRegex = <?= json_encode($this->app()->params['validatorRegex']) ?>;

    $(document).ready(function () {
        if ($(window).width() > 1025) {
            $('.steps-block_stepItem').on('touchend, click', function (e) {
                e.stopPropagation();
                $('.steps-block_stepItem ._text').fadeOut(200).removeClass('visible');
                $(this).find('._text').toggleClass('visible').fadeToggle(200);
            });
            $('body').on('click', function () {
                if ($('._text.visible').is(':visible')) {
                    $('.visible').removeClass('visible').fadeOut(200);
                }
            });
        } else {
            $('.steps-block_stepItem').on('touchend, click', function (e) {
                e.stopPropagation();
                $(this).toggleClass('_open').find('._text').fadeToggle(0).toggleClass('visible');
            });
        }
        $(".to-form").on("touchend, click", function (e) {
            e.preventDefault();
            $('body,html').animate({scrollTop: $('.form-block').offset().top}, 400);
        });

        // main page scroll to models
        $('.scroll_to_about').click(function (e) {
            e.preventDefault();
            if ($(window).width() > 800) {
                $("html, body").animate({scrollTop: $('.what-block').offset().top - 50}, 800);
            } else {
                $("html, body").animate({scrollTop: $('.what-block_right').offset().top - 30}, 800);
            }
        });

        var viewChangedTimer = null;

        function onViewChanged() {
            if (viewChangedTimer) return;

            viewChangedTimer = setTimeout(function () {
                Mamam.visibility.eventOnFirstSeen('.hottest-block_offerItem[data-product-id]', 'mamam.product.impressions');
                viewChangedTimer = null;
            }, 200);
        }

        $(document).on('scroll', onViewChanged);
        $(window).on('resize', onViewChanged);
        $(window).on('touchmove', onViewChanged);

        onViewChanged();

        $('.hottest-block_offerList').on('touchend, click', '.hottest-block_offerItem[data-product-id]', function (e) {
            e.preventDefault();

            var url = $(this).data('href'), navigate = function () {
                document.location = url;
            };
            Mamam.ecommerce.click($(this).data('product-id'), navigate);
            setTimeout(navigate, 1000)
        });

        $('.form-block button').on('touchend, click', function () {
            $('#order_form').submit();
        });

        $('#order_form').on('submit', function (e) {
            if (!registrationTracker.validateEmail('.email')) {
                e.preventDefault();
                return false;
            }

            var $form = $(this);
            var url = $form.attr('action');
            $.ajax({
                url: url,
                type: $form.attr('method'),
                data: $form.serialize(),
                success: function (data) {
                    if (data.success) {
                        $form.trigger('reset');
                        $(document).trigger('mamam.registration.success');
                    } else if (data.info) {
                        alert("<?= $this->t('This user already exists') ?>");
                    } else {
                        alert("<?= $this->t('Error. Please try again later') ?>");
                    }
                },
                error: function (error) {
                    alert(error.status + '<?= $textError ?>' + error.statusText);
                }
            });

            return false;
        });

        $(document).on('mamam.product.impressions', function (e, $impressions) {
            if (!$impressions || !$impressions.length) return;

            var newSeenElements = [];
            $impressions.filter('[data-product-id]').each(function () {
                newSeenElements.push($(this).data('product-id'));
            });

            Mamam.ecommerce.impressions(newSeenElements);
        });

        $(document).on('mamam.registration.success', function () {
            dataLayer.push({'event': 'mamam.registration.success'});

            var redirect = function () {
                window.location.href = '<?= CHtml::normalizeUrl(['club/thanks']) ?>';
            };

            if (Mamam.clickId) {
                $.ajax({
                    url: 'https://pstb.peerclicktrk.com/postback?cid=' + Mamam.clickId + '&summ_total=1&userid=2071',
                    timeout: 2000,
                    complete: redirect
                });

                return;
            }

            redirect();
        });
    });
</script>
