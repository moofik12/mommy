<?php

use MommyCom\Controller\Frontend\ClubController;

/**
 * @var ClubController $this
 * @var string $subId
 * @var int $landingNum
 */

$registrationTrackerJs = $this->app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.front')) . '/js/registration.tracker.js';
$textError = $this->t('Error. Check your internet connection');

?>
<div class="wrapper">
    <header class="header">
        <div class="header__logo">
            <img src="<?= $this->assetPath('/index/img/mommycom.svg') ?>" alt="">
        </div>
        <div class="header__text">
            <b><?= $this->t('Shopping club for loving mothers') ?></b>
            <span><?= $this->t('the best products for your child and family') ?></span>
        </div>
    </header>
    <div class="limit">
        <div class="block">
            <p class="block__title"><b><?= $this->t('Become a member of the club to be the best mother') ?>.</b>
                <?= $this->t('E-mail') ?></p>

            <?php $form = $this->beginWidget('CActiveForm', [
                'action' => $this->app()->createUrl('club/registration', ['landing' => $landingNum]),
                'htmlOptions' => [
                    'class' => 'form',
                    'id' => 'order_form',
                ],
            ]) ?>

            <label for="RegistrationForm_email"
                   style="font-size: 14px; display: block; text-align: left; margin: 0 0 10px 70px">
                <?= $this->t('Enter your e-mail') ?>:
            </label>
            <img src="<?= $this->assetPath('/index/img/email.svg') ?>" alt="" class="form__ico">
            <?= $form->textField($registrationForm, 'email', [
                'required' => false,
                'class' => 'form__inp email',
                'placeholder' => $this->app()->params['validatorRegex']['email']['default']['placeholder'],
            ]) ?>

            <?= CHtml::submitButton($this->t('Join the club'), ['class' => 'form__btn']) ?>
            <p class="form__error _exists"><?= $this->t('This user already exists') ?></p>
            <p class="form__error _error"><?= $this->t('Error. Please try again later') ?></p>
            <p class="success"><?= $this->t('Registration is successful. Thank you!') ?></p>

            <?php $this->endWidget() ?>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="application/javascript" src="<?= $registrationTrackerJs ?>"></script>
<script>
    registrationTracker.validatorRegex = <?= json_encode($this->app()->params['validatorRegex']) ?>;

    $(function () {
        $('#order_form').submit(function (e) {
            if (
                !registrationTracker.validateEmail('.email')
            ) {
                e.preventDefault();
                return false;
            } else {
                var form = $(this);
                var url = $('form').attr('action');
                $.ajax({
                    url: url,
                    type: form.attr('method'),
                    data: form.serialize(),
                    success: function (data) {
                        if (data.success) {
                            form.trigger('reset');
                            $('.form').removeClass('_exists _error');
                            $('#order_form .success').show();
                            $(document).trigger('mamam.registration.success');
                        } else if (data.info) {
                            $('.form').addClass('_exists');
                            $('#order_form .success').hide();
                        } else {
                            $('.form').addClass('_error');
                            $('#order_form .success').hide();
                        }
                    },
                    error: function (error) {
                        alert(error.status + '<?= $textError ?>' + error.statusText);
                    }
                });
            }
            return false;
        });

        $(document).on('mamam.registration.success', function () {
            dataLayer.push({'event': 'mamam.registration.success'});

            var redirect = function () {
                window.location.href = '<?= CHtml::normalizeUrl(['club/thanks']) ?>';
            };

            if (Mamam.clickId) {
                $.ajax({
                    url: 'https://pstb.peerclicktrk.com/postback?cid=' + Mamam.clickId + '&summ_total=1&userid=2071',
                    timeout: 2000,
                    complete: redirect
                });

                return;
            }

            redirect();
        });
    });
</script>
