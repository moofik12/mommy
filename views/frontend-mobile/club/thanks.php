<?php

use MommyCom\Controller\FrontendMobile\ClubController;

/**
 * @var ClubController $this
 */

$baseImgUrl = $this->app()->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

?>
<div class="main">
    <div class="container">
        <div class="club-congratulation">
            <?= CHtml::image($baseImgUrl . 'fireworks.svg') ?>
            <h1><?= $this->t('Thank you for registering!') ?></h1>

            <a href="<?= CHtml::normalizeUrl(['index/index', '#' => 'events']) ?>"
               class="btn red"><?= $this->t('Start Shopping') ?></a>
        </div>
    </div>
</div>
<script>
    if (Mamam.push) {
        Mamam.push.init();
    }
</script>
