<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/**
 * @var $this FrontController
 * @var $order OrderRecord
 */

$cf = $this->app()->currencyFormatter;
$app = $this->app();
$positions = $order->getPositionsAvailableForPay();
$promocodeSavings = $order->getPromocodeDiscount();
$cn = $this->app()->countries->getCurrency();
?>

<div class="back-link-content success-title pay-title"><?= $this->t('Payment for order №') ?> <?= $order->id ?></div>
<div class="payment-block">
    <?php foreach ($positions as $position): ?>
        <div class="payment-item">
            <div class="cart-item">
                <div class="cart-item-content">
                    <div class="cart-wrapper-image">
                        <?= CHtml::link(
                            CHtml::image($position->product->product->logo->getThumbnail('small150')->url, '', ['width' => 96, 'height' => 114]),
                            ['product/index', 'id' => $position->product->product_id, 'eventId' => $position->event_id]
                        ) ?>
                    </div>
                    <div class="cart-mobile">
                        <div class="title-line">
                            <a class="title"
                               href="<?= $app->createUrl('product/index', ['id' => $position->product->product_id, 'eventId' => $position->event_id, '#' => 'content']) ?>">
                                <?= CHtml::encode($position->product->product->name) ?>
                            </a>
                            <span class="number-line">x<span class="number"><?= $position->number ?></span></span>
                        </div>
                        <?php if (!empty($position->product->color) && !empty($position->product->size)): ?>
                            <ul>
                                <?php if (!empty($position->product->color)): ?>
                                    <li>Цвет: <span class="color"><?= $position->product->color ?></span></li>
                                <?php endif ?>

                                <?php if (!empty($position->product->size)): ?>
                                    <li>Размер: <span class="size"><?= $position->product->size ?></span></li>
                                <?php endif ?>
                            </ul>
                        <?php endif ?>
                        <div class="cart-price">
                            <span class="price"><?= $cf->format($position->price) ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>

    <div class="number"><?= Yii::t('goods-number', '{n} product|{n} products|{n} products', count($positions)) ?> <?= $this->t('for purchase') ?></div>
</div>
