<?php

use MommyCom\Controller\FrontendMobile\PayController;
use MommyCom\Service\Money\CurrencyFormatter;
use MommyCom\Service\PaymentGateway\PaymentGatewayInterface;
use MommyCom\YiiComponent\ShopLogic\ChoosePaymentMethod;

/**
 * @var PayController $this
 * @var string $uid
 * @var ChoosePaymentMethod $order
 * @var float $amount
 * @var float $deliveryPrice
 * @var float $bonusesAmount
 * @var float $discountAmount
 * @var int $countProductsNumber
 * @var PaymentGatewayInterface[] $availableGateways
 * @var array $availableMethods
 */

/** @var CurrencyFormatter $cf */
$cf = $this->container->get(CurrencyFormatter::class);

$this->pageTitle = $this->t('Order payment');

?>
<div class="main">
    <?php $this->renderPartial('_orderProducts', ['order' => $order]) ?>
    <div class="main main-cheque">
        <?php $form = $this->beginWidget('CActiveForm', [
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'beforeValidateAttribute' => 'js:function(form, attribute, data, hasError) {
            var flagElement = $(".selected-flag")[0];
            
            if (flagElement === document.activeElement) {
                return false;
            }
            
            return true;
        }',
                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
            $(document).trigger({
                type: "mommy.hideErrors",
                delay: 0
            });
                
            if (hasError) {
                if ($(".error").hasClass("intl-tel-input")) {
                    var trueParent = $(".intl-tel-input").parent();
                    trueParent.addClass("error");             
                }
                
                $(document).trigger("mommy.hideErrors");
            }
        }',
                'afterValidate' => 'js:function(form, data, hasError) {
            if (!hasError) {
                return true;
            }
            
            if ($(".error").hasClass("intl-tel-input")) {
                var trueParent = $(".error.intl-tel-input").parent();
                trueParent.addClass("error");
            }
            
            var getScrollTopId = form.attr("id");
            $.each(data, function (fieldId, val) {
                getScrollTopId = fieldId;
            });
            if ($("#" + getScrollTopId).length > 0) {
                $("html, body").animate({
                    scrollTop: $("#" + getScrollTopId).offset().top
                }, 500);
            }
            
            $(document).trigger("mommy.hideErrors");
            
            return false;
        }',
            ],
            'htmlOptions' => ['name' => 'payment-form'],
        ]); /* @var $form CActiveForm */ ?>

        <div class="action-submenu action-about">

            <div class="standard-block <?= !$model->cashOnDelivery ? 'hide' : '' ?>" id="payments-type">
                <div class="standard-wrapper radio-list">
                    <div class="left-part">
                        <?= $form->radioButtonList($model, 'cashOnDelivery', [
                            (string)false => $this->t('Prepay'),
                            (string)true => $this->t('Pay on delivery'),
                        ], [
                            'template' => '<div class="field bottom"><div class="radio-item">{input} {label}</div></div>',
                            'separator' => "</div>\n<div class=\"right-part\">",
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="standard-block <?= $model->cashOnDelivery ? 'hide' : '' ?>" id="payments-list">
                <div class="standard-wrapper">
                    <div class="left-part left-part-width">
                        <div class="field textarea bottom">
                            <?php $this->widget('extSelect.widgets.ExtSelect', [
                                'model' => $model,
                                'attribute' => 'id',
                                'data' => [
                                    $availableMethods, 'id', 'name',
                                ],
                                'settings' => [
                                    'placeholder' => $this->t('Payment type'),
                                    'clearSelection' => false,
                                    'events' => [
                                        'select, setSelectedItem' => new CJavaScriptExpression('function(item) {

                                }'),
                                    ],
                                ],
                            ]) ?>
                            <?= $form->error($model, 'id', ['class' => 'message']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cart-form-footer">
            <button class="btn green" type="submit"><?= $this->t('Checkout') ?></button>
        </div>
        <?php $this->endWidget() ?>
    </div>
    <div class="cart-footer">
        <div class="cart-number">
            <span class="number"><?= $countProductsNumber ?></span> <?= Yii::t('goods-number', 'goods | goods |', $countProductsNumber) ?> <?= $this->t('for purchase') ?>
        </div>
        <div class="price">
            <?php if ($bonusesAmount > 0): ?>
                <div class="price-bonus"><?= $this->t('Bonuses:') ?> <?= $cf->format($bonusesAmount) ?></div>
            <?php endif ?>
            <?php if ($discountAmount > 0): ?>
                <div class="price-sale"><?= $this->t('A discount:') ?> <?= $cf->format($discountAmount) ?></div>
            <?php endif ?>
            <?php if ($deliveryPrice > 0): ?>
                <div class="price-delivery"><?= $this->t('A delivery price:') ?>: <?= $cf->format($deliveryPrice) ?></div>
            <?php endif ?>
            <div class="special-price">
                <span class="price-label"><?= $this->t('Total:') ?></span>
                <span class="price-value">
                        <span class="in-total-same"><span class="price-total"><?= $cf->format($amount) ?></span></span>
                </span>
            </div>
        </div>
    </div>

    <script>
        $('[name=payment-form] [type=radio]').on('change', function (e) {
            if ($(this).val()) {
                $('#payments-list').addClass('hide');
            } else {
                $('#payments-list').removeClass('hide');
            }
        });
        $('[name="payment-form"]').on('submit', function (e) {
            e.preventDefault();

            if (!$('#payments-type').find('input:checked').val()) {
                return;
            }

            e.stopPropagation();

            $('#confirm_message').removeClass('hide-i');
            $('.main-cheque').addClass('hide-i');
            $('.back-container').addClass('hide-i');
        });
    </script>
    <div class="paid-block">
        <?php
        foreach ($availableGateways as $availableGateway) {
            echo $availableGateway->render($this, $uid);
        }
        ?>
    </div>
</div>
