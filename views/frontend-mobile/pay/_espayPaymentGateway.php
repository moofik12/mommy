<?php

use MommyCom\Service\BaseController\Controller;
use MommyCom\Service\PaymentGateway\EspayApi\EspayApi;
use MommyCom\Service\PaymentGateway\EspayApi\Provider;

/**
 * @var Controller $this
 * @var string $uid
 * @var string $backUrl
 * @var Provider[] $providers
 * @var EspayApi $espayApi
 */

$espayImageUrl = $espayApi->getImageUrl('espay');

/** @var Provider[] $methods */
$methods = [];

foreach ($providers as $provider) {
    $methods[$provider->getId()] = $provider;
}

?>
<iframe id="sgoplus-iframe" scrolling="no" frameborder="0" style="display: none"></iframe>
<script type="text/javascript">
    $(function () {
        var
            espayLoaded = false,
            espayMethods = <?= json_encode($methods) ?>;

        $.getScript('<?= $espayApi->getJsUrl() ?>').done(function () {
            espayLoaded = true;
        });

        $('[name="payment-form"]').on('submit', function (e) {
            if ($('#payments-type').find('input:checked').val()) {
                return;
            }

            var method = $('#payments-list').find('input').val();
            if (!method) {
                return;
            }

            e.stopPropagation();

            if (!espayLoaded) {
                return;
            }

            var data = {
                    key: '<?= $espayApi->getApiKey() ?>',
                    paymentId: '<?= $uid ?>',
                    backUrl: encodeURIComponent('<?= $backUrl ?>'),
                    bankCode: espayMethods[method]['bankCode'],
                    bankProduct: espayMethods[method]['productCode']
                },
                sgoPlusIframe = document.getElementById("sgoplus-iframe");

            if (sgoPlusIframe === null) {
                return;
            }
            sgoPlusIframe.src = SGOSignature.getIframeURL(data);
            SGOSignature.receiveForm();
        });
    });
</script>
