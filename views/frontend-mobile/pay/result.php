<?php

/**
 * @var PayController $this
 * @var string $status
 */

use MommyCom\Controller\FrontendMobile\PayController;

$this->pageTitle = $this->t('Order payment');
?>

<div class="main">
    <div class="pay-result">
        <h1><?= CHtml::encode($status) ?></h1>
    </div>
</div>
