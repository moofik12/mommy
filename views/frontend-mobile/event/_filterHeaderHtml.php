<?php

use MommyCom\Controller\FrontendMobile\EventController;
use MommyCom\Model\Product\AgeGroups;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Model\Product\ProductTargets;

/**
 * @var EventController $this
 * @var GroupedProducts $groupedProducts
 * @var GroupedProductsFilter $filterProducts
 */

$request = $this->app()->request;
$showSizeFilter = isset($showSizeFilter) ? $showSizeFilter : true;
$showColorFilter = isset($showColorFilter) ? $showColorFilter : true;
$showTargetFilter = isset($showTargetFilter) ? $showTargetFilter : true;
$showBrandFilter = isset($showBrandFilter) ? $showBrandFilter : true;
$showAgeGroupFilter = isset($showAgeGroupFilter) ? $showAgeGroupFilter : true;

$sortFilter = $request->getParam('sort', false);
$sortName = $this->t('Recommended');
if ($sortFilter == 'price_asc') {
    $sortName = $this->t('Most cheapest first');
} elseif ($sortFilter == 'price_desc') {
    $sortName = $this->t('Most expensive first');
}
$sizeFilter = $request->getParam('size', false);
$colorFilter = $request->getParam('color', false);
$targetFilter = $request->getParam('target', false);
$brandFilter = $request->getParam('brand', false);
$ageGroupFilter = $request->getParam('ageGroup', false);

$hasFilter = (bool)array_filter([$sortFilter, $sizeFilter, $colorFilter, $targetFilter, $brandFilter, $ageGroupFilter]);
?>
<div id="filter-block">
    <form method="get" action="<?= $this->createUrl($this->action->id, $_GET) ?>">
        <?php if ($filterProducts->getCountEventProducts() > 1): ?>
            <div class="sorting-bar">
                <div class="container">
                    <span class="sorting-title"><?= $this->t('Sort') ?></span>
                    <label class="extselect-container extselect-single extselect-state-readonly extselect-state-enabled extselect-state-clrbtn-hidden extselect-state-dropdownopened"
                           tabindex="0" style="">
                        <div class="extselect-inputwrapper"><input type="hidden" name="sort" value="<?= $sortFilter ?>"><span
                                    class="extselect-input"><?= $sortName ?></span></div>
                        <div class="extselect-btn"></div>
                        <select name="sort">
                            <option value=""
                                    id="sort-price-none" <?= $sortFilter == false || $sortFilter == '' ? 'selected' : '' ?>><?= $this->t('Recommended'); ?></option>
                            <option value="price_asc"
                                    id="sort-price-asc" <?= $sortFilter == 'price_asc' ? 'selected' : '' ?>><?= $this->t('Most cheapest first'); ?></option>
                            <option value="price_desc"
                                    id="sort-price-desc" <?= $sortFilter == 'price_desc' ? 'selected' : '' ?>><?= $this->t('Most expensive first'); ?></option>
                        </select>
                    </label>
                </div>
            </div>
        <?php endif ?>
        <nav>
            <div class="filter-bar">
                <div class="container">
                    <div class="filter-header">
                        <div class="filter-title collapsed" data-target="#filter-fields" data-toggle="collapse">
                            <span class="title"><?= $this->t('Filter') ?></span>
                        </div>
                        <div class="filter-select"></div>
                        <div class="filters-remove" data-remove-all="true">
                            <div class="title-middle">
                                <span><?= $this->t('Remove all') ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="filter-field collapse width" id="filter-fields">
                        <?php if ($showBrandFilter && count($brands = $filterProducts->getBrands()) > 1): ?>
                            <div class="filter-line">
                                <div class="filter-label">
                                    <i class="icon-filter icon-filter-brand"></i>
                                    <label for="choose-brand" class="title"><?= $this->t('Brand') ?></label>
                                    <select id="choose-brand" name="brand" data-multiple="true">
                                        <option name="brand" disabled selected id="brand-no-selected"
                                                value=""><?= $this->t('Have not choosen') ?></option>
                                        <?php foreach ($brands as $brand): ?>
                                            <?php $brandId = CHtml::getIdByName($brand->name) ?>
                                            <option name="brand" id="brand-<?= $brandId ?>"
                                                    value="<?= $brand->id ?>"><?= $brand->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="filter-items"></div>
                            </div>
                        <?php endif ?>
                        <?php if ($showSizeFilter && count($sizes = $filterProducts->getSizes()) > 1): ?>
                            <div class="filter-line">
                                <div class="filter-label">
                                    <i class="icon-filter icon-filter-size"></i>
                                    <label for="choose-size" class="title"><?= $this->t('Size') ?></label>
                                    <select id="choose-size" name="size" data-multiple="true">
                                        <option name="brand" disabled selected id="size-no-selected"
                                                value=""><?= $this->t('Have not choosen') ?></option>
                                        <?php foreach ($sizes as $size): ?>
                                            <?php $sizeId = CHtml::getIdByName($size) ?>
                                            <option name="size" id="size-<?= $sizeId ?>"
                                                    value="<?= $size ?>"><?= $size ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="filter-items"></div>
                            </div>
                        <?php endif ?>
                        <?php if (count($colors = $filterProducts->getColors()) > 1): ?>
                            <div class="filter-line">
                                <div class="filter-label">
                                    <i class="icon-filter icon-filter-color"></i>
                                    <label for="choose-color" class="title"><?= $this->t('Colour') ?></label>
                                    <select id="choose-color" name="color" data-multiple="true">
                                        <option name="color" disabled selected id="color-no-selected"
                                                value=""><?= $this->t('Have not choosen') ?></option>
                                        <?php foreach ($colors as $color): ?>
                                            <?php $colorId = CHtml::getIdByName($color) ?>
                                            <option name="size" id="color-<?= $colorId ?>"
                                                    value="<?= $color ?>"><?= $color ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="filter-items"></div>
                            </div>
                        <?php endif ?>
                        <?php if ($showAgeGroupFilter && count($ageGroups = $filterProducts->getAgeGroups()) > 1): ?>
                            <div class="filter-line">
                                <div class="filter-label">
                                    <i class="icon-filter icon-filter-age"></i>
                                    <label for="choose-age" class="title"><?= $this->t('Age') ?></label>
                                    <select id="choose-age" name="ageGroup">
                                        <option name="ageGroup" disabled selected id="color-no-selected"
                                                value=""><?= $this->t('Have not choosen') ?></option>
                                        <?php foreach ($ageGroups as $ageGroup): ?>
                                            <?php $ageGroupId = CHtml::getIdByName($ageGroup) ?>
                                            <option name="ageGroup" id="color-<?= $ageGroupId ?>"
                                                    value="<?= $ageGroup ?>"><?= AgeGroups::instance()->getLabel($ageGroup) ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="filter-items"></div>
                            </div>
                        <?php endif ?>
                        <?php if ($showTargetFilter && count($targets = $filterProducts->getTargets()) > 1): ?>
                            <div class="filter-line">
                                <div class="filter-label">
                                    <i class="icon-filter icon-filter-gender"></i>
                                    <label for="choose-target" class="title"><?= $this->t('For whom') ?></label>
                                    <select id="choose-target" name="target" data-multiple="true">
                                        <option name="target" disabled selected id="target-no-selected"
                                                value=""><?= $this->t('Have not choosen') ?></option>
                                        <?php foreach ($targets as $target): ?>
                                            <?php $targetId = CHtml::getIdByName($target) ?>
                                            <option name="target" id="target-<?= $targetId ?>"
                                                    value="<?= $target ?>"><?= ProductTargets::instance()->getLabel($target) ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="filter-items"></div>
                            </div>
                        <?php endif ?>
                        <div class="filter-footer">
                            <button class="btn-reset btn-standard btn-green"
                                    type="submit"><?= $this->t('Apply') ?></button>
                            <button class="btn-reset btn-standard"
                                    type="reset"><?= $this->t('Reset filter') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </form>
</div>
