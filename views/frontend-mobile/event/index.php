<?php

use MommyCom\Model\Product\GroupedProduct;
use MommyCom\Model\Product\GroupedProducts;
use MommyCom\Model\Product\GroupedProductsFilter;
use MommyCom\Widget\FrontendMobile\MamamLinkPager;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $groupedProducts GroupedProducts */
/* @var $provider CDataProvider Provider for products ID */
/* @var $returnParams array */
/* @var $showEvent bool */
/* @var GroupedProductsFilter $filterProducts */
/* @var string $title */

$this->pageTitle = $this->t($title);

$app = $this->app();

$req = $app->request;
$tokenManager = $app->tokenManager;
$user = $app->user;

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
?>

<?php if ($this->getRoute() === 'event/category'): ?>
    <script type="text/javascript">
        (function () {
            Mamam.mailru.send(Mamam.mailru.PAGE_LEVEL_CATEGORY, {});
        })();
    </script>
<?php endif; ?>
<div class="main no-bg-color">
    <?php if ($groupedProducts->count == 0): ?>
        <div class="list-no-items">
            <p><?= $this->t('There are no products available according to your search.') ?></p>
            <p><?= $this->t('Try reducing the number of filters') ?></p>
        </div>
    <?php endif; ?>
    <div class="promo-now goods-list">
        <?php foreach ($groupedProducts->toArray() as $item): /* @var $item GroupedProduct */ ?>
            <?php $isPastEvent = ($item->event->end_at <= time()); ?>
            <?= $this->renderPartial('//layouts/_productJson', ['productGroup' => $item]) ?>
            <div class="goods-item" itemscope itemtype="http://schema.org/Product" id="<?= $item->product->id ?>">
                <div class="a">
                    <a class="product-link-details"
                       data-product-id="<?= $item->event->id ?>/<?= $item->product->id ?>"
                       href="<?= $app->createUrl('product/index', ['eventId' => $item->event->id, 'id' => $item->product->id] + $returnParams) ?>">
                        <div class="box">
                            <div class="image-wrapper waiting">
                                    <?php if ($item->isSoldOut): ?>
                                    <div class="label-sold">×</div>
                                <?php else: ?>
                                    <div class="label-discount">-<?= round((1 - $item->price / $item->priceOld) * 100) ?>%</div>
                                    <?php if ($item->isLimited): ?>
                                        <!--<div class="label-ends">!</div>-->
                                    <?php endif ?>
                                    <?php if ($item->isTopOfSelling): ?>
                                        <div class="label-hit">HIT</div>
                                    <?php endif ?>
                                <?php endif ?>
                                <div class="box-img">
                                    <?= '';/* CHtml::image(
                                    $app->baseUrl . '/static/blank.gif', 'товар', array(
                                        'data-src' => $item->product->logo->getThumbnail('mid320_prodlist')->url,
                                        'data-lazyload' => 'true'
                                    )
                                )*/ ?>
                                    <?= CHtml::image(
                                        $item->product->logo->getThumbnail('mid320_prodlist')->url, 'товар',
                                        [
                                            'itemprop' => "image",
                                            'style' => 'margin-top: -40px',
                                        ]

                                    ) ?>
                                </div>
                            </div>
                            <div class="text-wrapper">
                                <?php if ($showEvent): ?>
                                    <p class="category-title text"><?= CHtml::encode($item->event->name) ?></p>
                                <?php endif ?>
                                <p class="title text" itemprop="name"><?= CHtml::encode($item->product->name) ?></p>
                                <div class="price">
                                    <div class="new-price"><?= $cf->format($item->price, $cn->getName('short')) ?></div>
                                    <?php if ($item->priceOld != $item->price): ?>
                                        <div class="old-price"><?= $cf->format($item->priceOld, $cn->getName('short')) ?></div>
                                    <?php endif ?>
                                </div>
                                <div class="time">
                                    <?= CHtml::tag(
                                        'time',
                                        ['datetime' => $tf->formatMachine($item->event->end_at), 'data-countdown' => true],
                                        $tf->format($item->event->end_at)
                                    ) ?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php if (!$item->isSoldOut): ?>
                        <?php if ($item->sizeCount > 0): ?>
                            <label class="cart">
                                    <span class="cart-inner" <?= ($isPastEvent) ? 'style="background-color:#d5d5d5;"' : '' ?>>
                                        <select name="size" <?= ($isPastEvent) ? 'disabled="disabled"' : '' ?>>
                                            <option selected disabled><?= $this->t('Choose a size') ?></option>
                                            <?php foreach ($item->sizes as $size): ?>
                                                <?php $sizeId = CHtml::getIdByName($item->product->id . '_' . $size) ?>
                                                <?= CHtml::tag(
                                                    'option',
                                                    [
                                                        'id' => $sizeId,
                                                        'data-token' => $tokenManager->getToken($item->product->id),
                                                        'data-event-id' => $item->event->id,
                                                        'data-product-id' => $item->product->id,
                                                        'data-number' => 1,
                                                    ],
                                                    $size
                                                ) ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </span>
                            </label>
                        <?php else: ?>
                            <a class="cart in-my-cart" href="javascript:void(0);"
                               data-token="<?= $tokenManager->getToken($item->product->id) ?>"
                               data-event-id="<?= $item->event->id ?>"
                               data-product-id="<?= $item->product->id ?>"
                               data-number="1"><span
                                        class="cart-inner" <?= ($isPastEvent) ? 'style="background-color:#d5d5d5;"' : '' ?> ></span></a>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <?php if ($provider->pagination->pageCount > 1) : ?>
        <nav>
            <div class="goods-pagination clearfix">
                <?php $this->widget(MamamLinkPager::class, [
                    'pages' => $provider->pagination,
                ]) ?>
            </div>
        </nav>
    <?php endif ?>
    <?php $showButtonUp = $provider->pagination->pageCount > 1 || $groupedProducts->count > 3; ?>
    <div class="up-btn <?= $showButtonUp ? '' : 'hide' ?>"><a
                href="javascript:scrollTop()"><?= $this->t('Up') ?></a></div>
</div>
