<?php

use MommyCom\Controller\FrontendMobile\LandingsController;

/* @var $this LandingsController */
/* @var $content string */

$app = $this->app();
$user = $this->app()->user;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
</head>
<body>
<?php $this->renderPartial('//layouts/_globalScripts') ?>

<?= $content; ?>
</body>
</html>
