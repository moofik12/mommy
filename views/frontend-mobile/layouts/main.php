<?php

use MommyCom\Controller\FrontendMobile\StaticController;
use MommyCom\Service\Region\Regions;

/**
 * @var $this StaticController
 * @var $content string
 */

$app = $this->app();
$req = $request = $app->request;
$cart = $app->user->cart;
$user = $this->app()->user;
$userAgent = $app->userAgent;
$cn = $this->app()->countries->getCurrency();

/** @var $cs CClientScript */
$cs = $this->app()->clientScript;
$cs->registerScriptFile($app->createUrl('language/dictionary'));

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
$regionName = $regions->getUserRegion()->getRegionName();
$facebookLink = $this->app()->params['facebookLinks'][$regionName];

$bodyClass = $this->visibleOnlyLogo ? 'product' : '';
$isShowMailingType = (isset($_REQUEST['typeMailing'])) ? true : false;
$bodyClass .= $isShowMailingType ? ' login' : '';
$isShowInfoMailBlock = false;
/*if (!$user->isGuest) {
    $userModel = $user->getModel();
    $emailParts = explode('@', $userModel->email);
    $rusEmails = array('mail.ru', 'yandex.ru', 'yandex.ua', 'ya.ru', 'list.ru', 'inbox.ru', 'bk.ru', 'vk.ru', 'ok.ru', 'mail.ua', 'rambler.ru', 'yandex.kz', 'yandex.com', 'yandex.by');
    if (in_array($emailParts[1], $rusEmails)) {
        $isShowInfoMailBlock = true;
    }
}*/
?>
<!DOCTYPE html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#ff7c91">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <meta name="description"
          content="<?= $this->description ? $this->description : $this->t('The first shopping-club for children and moms in the USA mommy.com. Discounts of up to 70%.') ?>">
</head>
<body class="<?= $bodyClass ?>" itemscope itemtype="http://schema.org/WebPage">
<?php if (!empty($facebookLink)): ?>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<?php endif; ?>
<?php if ($isShowInfoMailBlock) : ?>
    <aside class="prompt-top">
        <div class="container">
            <a class="prompt-link" style="font-size: 18px;" href="<?= $this->createUrl('account/about'); ?>">
                <span style="color: red;"><?= $this->t('Important!') ?></span>
                <?= $this->t('You need to change the e-mail in connection with the decree about the blocking. Click here to change now') ?>
            </a>
        </div>
    </aside>
<?php endif; ?>
<?= $app->user->getOfferSuccessCode() ?>

<?php $this->renderPartial('//layouts/_globalScripts') ?>
<?php if (!$isShowMailingType) : ?>
    <?php $this->renderPartial('//layouts/_mainHeader') ?>

    <?php if ($this->showRegionConfirmPopup || $this->showRegionChooseModal): ?>
        <?php $this->renderPartial('//layouts/_modalRegionSwitcher') ?>
    <?php endif ?>
<?php endif; ?>

<?php if ($isShowMailingType) : ?>
    <script>
        $(document).one('actions.setup.loaded', function () {
            $(document).trigger("mamam.registration.success");
        });
    </script>
<?php endif; ?>

<main class="main-container main-container-margin">
    <div class="container">
        <?php if (!$isShowMailingType) : ?>
            <?= $content; ?>
        <?php endif; ?>
        <?php if ($isShowMailingType) : ?>
            <?php $this->renderPartial('//layouts/_mailingType') ?>
        <?php endif; ?>
    </div>
</main>
<?php if (!$isShowMailingType) : ?>
    <?php $this->renderPartial('//layouts/_footer') ?>
    <?php $this->renderPartial('//layouts/_askQuestion') ?>
<?php endif; ?>
<?php $session = new \Symfony\Component\HttpFoundation\Session\Session(); ?>
<?php if ($session->get('justRegistered')): ?>
    <script>dataLayer.push({'event': 'mamam.registration.success'})</script>
    <?php $session->set('justRegistered', null) ?>
<?php endif; ?>
</body>
</html>
