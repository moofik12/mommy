<?php

use MommyCom\Model\Product\GroupedProduct;

/* @var $productGroup GroupedProduct */

$productParams = [
    'id' => $productGroup->event->id . '/' . $productGroup->product->id,
    'name' => $productGroup->product->name,
    'brand' => $productGroup->product->brand->name,
    'category' => $productGroup->product->category,
    'price' => $productGroup->price,
];

?>
<script>
    Mamam.ecommerce.products.store(<?= json_encode($productParams, JSON_UNESCAPED_UNICODE)?>);
</script>

