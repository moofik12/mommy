<?php

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/**
 * @var $this FrontController
 */

$app = $this->app();
$cart = $app->user->cart;
$user = $this->app()->user;
?>
<script>
    (function () {
        <?php
        $orderPositions = [];
        foreach ($cart->getPositions() as $position) {
            $orderPositions[$position->event_id . '/' . $position->product->product_id] = [
                'id' => $orderPositions[] = $position->event_id . ' / ' . $position->product->product_id,
                'name' => $position->product->product->name,
                'brand' => $position->product->product->brand->name,
                'category' => $position->product->product->category,
                'variant' => $position->product->size,
                'price' => $position->price,
                'quantity' => $position->number,
            ];
        }
        ?>
        Mamam.ecommerce.products.setCache(<?= json_encode($orderPositions, JSON_UNESCAPED_UNICODE) ?>);
    })();
</script>
<header class="top-container">
    <div class="container">
        <a class="logo flag" href="<?= $app->createUrl('index/index') ?>"></a>
        <div class="top-menu-wrapper" data-quickcart-container="true">
            <?php $this->widget('zii.widgets.CMenu', [
                'items' => [
                    [
                        'label' => '<i class="icon-top-menu icon-goods"></i>' . $this->t('Goods'),
                        'url' => $app->createUrl('goods'),
                        'active' => $this->id == 'goods',
                    ],
                    [
                        'label' => '<i class="icon-top-menu icon-cart"></i>' . $this->t('Basket') . ' (<span id="numGoods">' . $cart->getCount() . '</span>)',
                        'url' => $app->createUrl('cart'),
                        'active' => in_array($this->id, ['order', 'cart']),
                    ],
                    [
                        'label' => '<i class="icon-top-menu icon-login"></i>' . $this->t('Sign Up / Sign In'),
                        'url' => $app->createUrl('auth/auth', ['name' => 'registration']),
                        'itemOptions' => ["class" => "top-menu-login"],
                        'visible' => $app->user->isGuest,
                    ],
                    [
                        'label' => '<i class="icon-top-menu icon-login"></i>' . $this->t('Account'),
                        'url' => $app->createUrl('account'),
                        'visible' => !$app->user->isGuest,
                        'active' => $this->id == 'account',
                    ],
                ],
                'htmlOptions' => ['class' => 'top-menu'],
                'encodeLabel' => false,
                'activeCssClass' => 'active',
                'activateParents' => true,
                'activateItems' => true,
            ]); ?>
        </div>
    </div>
</header>
<?php
$domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);
$holidaysMessageCookie = sprintf('holidaysMessage=1;domain=%s;expires=Thu, 21 Jun 2018 00:00:00 GMT;path=/', $domain);

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
$isHolidays = (Region::INDONESIA === $regions->getServerRegion()->getRegionName() && strtotime('2018-06-21') > time());
?>
<?php if ($isHolidays): ?>
    <div class="main-container hide" id="holidaysMessage">
        <div class="container" style="border-bottom: 1px solid #ff7c91; margin: 0; position: relative">
            <div style="font-size: 1.5em; padding: 20px 40px 20px 20px">
                <p>Selamat Hari Raya Idul Fitri!</p>
                <p>Sehubungan dengan perayaan Idul Fitri Mommy.com tidak melakukan pengiriman dari tgl&nbsp;13/06/18 -
                    20/06/18</p>
                <p>Semua pesanan akan dikirim pada tgl&nbsp;21/06/18</p>
                <p>
                    Layanan pelanggan kami aktif selama liburan, Anda bisa hubungi kami di
                    <a target="_blank" style="color: #ff7c91; text-decoration: underline"
                       href="https://api.whatsapp.com/send?phone=6282147486726">WhatsApp</a>, dan
                    <a target="_blank" style="color: #ff7c91; text-decoration: underline"
                       href="https://m.me/mommy.com.Indonesia">Facebook</a>
                </p>
            </div>
            <div style="position: absolute; top:0;right: 0; width: 24px; height: 24px; padding: 8px"
                 id="holidaysMessageClose">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" width="24" height="24"
                     viewBox="0 0 24 24"
                     enable-background="new 0 0 24 24" xml:space="preserve">
<path fill="#ff7c91"
      d="M12,24C5.4,24,0,18.6,0,12S5.4,0,12,0s12,5.4,12,12S18.6,24,12,24z M17.4,8.5c0.2-0.2,0.2-0.6,0-0.8 l-1.1-1.3c-0.3-0.2-0.6-0.2-0.8,0L12,9.9L8.5,6.4C8.3,6.2,8,6.2,7.7,6.4L6.4,7.7c-0.2,0.2-0.2,0.6,0,0.8L9.9,12l-3.5,3.5 c-0.2,0.2-0.2,0.5,0,0.8l1.3,1.3c0.2,0.2,0.6,0.2,0.8,0l3.5-3.5l3.5,3.5c0.2,0.2,0.6,0.2,0.8,0l1.3-1.3c0.2-0.2,0.2-0.6,0-0.8 L14.1,12L17.4,8.5z"/>
</svg>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            if ($.cookie('holidaysMessage') !== '1') {
                $('#holidaysMessage').removeClass('hide');
            }

            $('#holidaysMessageClose').click(function () {
                $('#holidaysMessage').addClass('hide');
                document.cookie = '<?= $holidaysMessageCookie ?>';
            });
        });
    </script>
<?php endif; ?>
<?php if (!empty($this->productHeader)): ?>
    <div class="back-container">
        <div class="container">
            <div class="back-wrapper">
                <?= $this->productHeader ?>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if (!empty($this->filterHeaderHtml)): ?>
    <?= $this->filterHeaderHtml ?>
<?php endif; ?>
