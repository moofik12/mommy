<?php

use MommyCom\YiiComponent\ClientScript;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();
/* @var ClientScript $clientScript */
$clientScript = $app->clientScript;
$clientScript->registerPackage('lottery');
?>
<!DOCTYPE html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#ff7c91">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
</head>
<body class="lottery">
<?php $this->renderPartial('//layouts/_globalScripts') ?>

<?= $content ?>

</body>
</html>
