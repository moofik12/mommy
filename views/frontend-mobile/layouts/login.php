<?php

use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();
$user = $this->app()->user;
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#ff7c91">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
</head>
<body class="auth-login login">
<?php $this->renderPartial('//layouts/_globalScripts') ?>

<main class="main-container">
    <div class="container login-page">
        <a class="logo" href="<?= $app->createUrl('index') ?>">mommy</a>
        <div class="login-conteiner">
            <?= $content; ?>
        </div>
    </div>
</main>
</body>
</html>
