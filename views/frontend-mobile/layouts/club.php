<?php

use MommyCom\Controller\FrontendMobile\ClubController;

/**
 * @var ClubController $this
 * @var string $content
 */
?>
<!DOCTYPE html>
<html dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= CHtml::encode($this->getPageTitle()); ?></title>
    <link rel="shortcut icon" href="/static/favicon.ico" type="image/png">
</head>
<body class="<?= $this->bodyClass ?>">
<?php $this->renderPartial('//layouts/_globalScripts') ?>
<?= $content ?>
</body>
</html>
