<?php

use MommyCom\Service\Region\Region;
use MommyCom\Service\Region\RegionDetector;
use MommyCom\Service\Region\Regions;
use MommyCom\YiiComponent\ClientScript;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/**
 * @var FrontController $this
 */

$app = $this->app();
$user = $app->user;

/** @var ClientScript $cs */
$cs = $app->clientScript;

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
/** @var $cs CClientScript */
$cs = $app->clientScript;

$cs->registerCssFile($app->assetManager->publish(Yii::getPathOfAlias('assets.markup.css') . '/region.css'));

$baseImgUrl = $app->assetManager->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$createRegionCookie = function ($value) use ($regions, $user) {
    $value = $this->app()->getSecurityManager()->hashData(serialize($value));
    $value = rawurlencode($value);

    $name = RegionDetector::getCookieName();
    $domain = $user->identityCookie['domain'] ?? '.' . $regions->getHostname(Region::DEFAULT);

    return sprintf('%s=%s;domain=%s;expires=Tue, 19 Jan 2038 03:14:07 GMT;path=/', $name, $value, $domain);
};

$popupHide = $this->showRegionConfirmPopup ? '' : 'hide';
$modalHide = $this->showRegionChooseModal ? '' : 'hide';

?>
<div class="new_pop_container">
    <div class="choose_country_pop _pop1 <?= $modalHide ?>">
        <?= CHtml::image($baseImgUrl . 'geo.svg', '', ['class' => 'choose_country_pop_logo']) ?>
        <form action="">
            <p class="choose_country_pop_label">Choose your country</p>
            <p class="choose_country_pop_country"
               data-href="//<?= $regions->getHostname(Region::INDONESIA) ?>"
               data-cookie="<?= $createRegionCookie(Region::INDONESIA) ?>"
            >Indonesia</p>
            <p class="choose_country_pop_country"
               data-href="//<?= $regions->getHostname(Region::VIETNAM) ?>"
               data-cookie="<?= $createRegionCookie(Region::VIETNAM) ?>"
            >Vietnam</p>
        </form>
    </div>
    <div class="choose_country_pop _pop2 <?= $popupHide ?>">
        <form action="">
            <p class="choose_country_pop_label">Your country</p>
            <p class="choose_country_pop_country"><?= $regions->getServerRegion()->getRegionName() ?></p>
            <button class="choose_country_pop_btn _btn1">Yes, thank you</button>
            <button class="choose_country_pop_btn _btn2">No, choose</button>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('.new_pop_container ._pop1 .choose_country_pop_country').click(function (e) {
        e.preventDefault();
        var $checked = $(this);
        $('.new_pop_container .choose_country_pop_country').removeClass('active');
        $checked.addClass('active');
        $checked.closest('.choose_country_pop').find('.choose_country_pop_btn').prop('disabled', false);
        document.cookie = $checked.data('cookie');
        window.location.replace($checked.data('href'));
    });

    $('.new_pop_container .choose_country_pop_btn._btn1').click(function (e) {
        e.preventDefault();

        $(this).closest('.new_pop_container').hide();

        document.cookie = '<?= $createRegionCookie($regions->getServerRegion()->getRegionName()) ?>';
    });

    $('.new_pop_container .choose_country_pop_btn._btn2').click(function (e) {
        e.preventDefault();

        $('.new_pop_container .choose_country_pop._pop2').hide();
        $('.new_pop_container .choose_country_pop._pop1').show();
    });
</script>
