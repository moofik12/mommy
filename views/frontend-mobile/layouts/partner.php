<?php

use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $content string */

$app = $this->app();
$user = $this->app()->user;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
</head>
<body class="<?= $this->bodyClass ?>">
<?php $this->renderPartial('//layouts/_globalScripts') ?>
<?= $content; ?>
<footer class="main-footer">
    <div class="container">
        <nav class="footer-nav">
            <div class="logo">
                <a href="<?= $app->createUrl('index/index') ?>">mommy</a>
            </div>
            <?php if ($this->beginCache('footer-menu', ['varyByLanguage' => true, 'duration' => 0])) { ?>
                <?php $isPastEvent = true;
                $i = 1; ?>
                <?php foreach (StaticPageCategoryRecord::model()->cache(60)->findAll() as $category): /* @var $category StaticPageCategoryRecord */ ?>
                    <div class="footer-group">
                        <h3><?= $category->name ?></h3>
                        <ul>
                            <?php foreach ($category->pagesVisible as $item): /* @var $item StaticPageRecord */ ?>
                                <li>
                                    <a href="<?= $app->createUrl('static/index', ['category' => $category->url, 'page' => $item->url]) ?>">
                                        <?= $item->title ?>
                                    </a>
                                </li>
                                <?php if ($isPastEvent && count($category->pagesVisible) == $i): ?>
                                    <li>
                                        <a href="<?= $app->createUrl('past/index') ?>"><?= $this->t('Past promotions') ?></a>
                                    </li>
                                    <?php $isPastEvent = false ?>
                                <?php endif; ?>
                                <?php $i++; ?>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php endforeach ?>
                <?php $this->endCache();
            } ?>
            <div class="footer-group contacts">
                <h3><?= $this->t('We are in social networks') ?></h3>
                <ul class="social">
                    <!--li><a class="vk" href="<?= $this->app()->params['groupVk'] ?>" target="_blank">Vkontakte</a></li>
                        <li><a class="od" href="<?= $this->app()->params['groupOd'] ?>" target="_blank">Odnoklassniki</a></li-->
                    <li><a class="fb" href="<?= $this->app()->params['groupFb'] ?>" target="_blank">Facebook</a></li>
                </ul>
            </div>
        </nav>
    </div>
</footer>
<aside class="modals">
    <?php //$this->renderPartial('//layouts/modals/_partner') ?>
</aside>
</body>
</html>
<?php
/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseUrl = $this->app()->getAssetManager()->publish(Yii::getPathOfAlias('assets.partner.landings'));
$basePath = realpath(Yii::getPathOfAlias('assets.partner.landings'));
$modifiedAt = @filemtime($basePath . '/css/style.css');
$cs->registerCssFile($baseUrl . "/css/style.css?v=" . $modifiedAt);
?>
