<?php
/**
 * $this StaticController
 */
$app = $this->app();
?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN"
        "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="noindex">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
</head>
<body>

<div class="top-container">
    <div class="container">
        <header>
            <a class="logo flag" href="<?= $app->createUrl('index/index') ?>"></a>
        </header>
    </div>
</div>

<div class="main-container not-found page">
    <div class="container">
        <div class="main">
            <div class="maintenance page">
                <div class="maintenance-page">
                    <div class="maintenance-title"><?= $this->t('Technical work is carried out') ?></div>
                    <div class="maintenance-subtitle"><?= $this->t('We apologize for the inconvenience') ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bottom-container maintenance-foot">
    <div class="container">
        <div class="footer-container">
            <div class="link-bar-wrapper">
                <div class="link-bar">
                    <a class="footer-logo" href="<?= $app->createUrl('index/index') ?>"></a>
                    <ul class="footer-social">
                        <li><a class="footer-vk-logo" href="<?= $this->app()->params['groupVk'] ?>" target="_blank"></a>
                        </li>
                        <li><a class="footer-od-logo" href="<?= $this->app()->params['groupOd'] ?>" target="_blank"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="footer-wrapper">
                <div class="logo">
                    <a href="<?= $app->createUrl('index/index') ?>">mommy</a>
                </div>

                <div class="contacts">
                    <dl class="social">
                        <!--                        <dt>-->
                        <? //= $this->t('We are in social networks')?><!--</dt>-->
                        <!--                        <dd><a class="vk" href="-->
                        <? //= $this->app()->params['groupVk'] ?><!--" target="_blank">Vkontakte</a></dd>-->
                        <!--                        <dd><a class="od" href="-->
                        <? //= $this->app()->params['groupOd'] ?><!--" target="_blank">Odnoklassniki</a></dd>-->
                    </dl>
                </div>
            </div>
            <div class="footer-bottom">
            </div>
        </div>
    </div>
</div>
</body>
</html>
