<?php

use MommyCom\Controller\FrontendMobile\StaticController;

/* @var $this StaticController */
/* @var $content string */

$app = $this->app();
$req = $request = $app->request;
$cart = $app->user->cart;
$user = $this->app()->user;
$userAgent = $app->userAgent;
$cn = $this->app()->countries->getCurrency();

?>
<!DOCTYPE html>
<html lang="<?= $this->app()->getLanguage(); ?>">
<head>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#ff7c91">
    <link rel="icon" type="image/png" href="<?= $app->baseUrl ?>/static/favicon.ico">
    <meta name="description"
          content="<?= $this->description ? $this->description : $this->t('The first shopping-club for children and moms in the USA mommy.com. Discounts of up to 70%.') ?>">
</head>
<body class="login" itemscope itemtype="http://schema.org/WebPage">
<?= $app->user->getOfferSuccessCode() ?>
<?php $this->renderPartial('//layouts/_globalScripts') ?>
<main class="main-container">
    <div class="container">
        <?= $content; ?>
    </div>
</main>
</body>
</html>
