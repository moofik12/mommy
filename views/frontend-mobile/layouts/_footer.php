<?php

use MommyCom\Controller\FrontendMobile\StaticController;
use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;
use MommyCom\Service\Region\Regions;

/* @var $this StaticController */
/* @var $content string */

$app = $this->app();
$req = $request = $app->request;

$phone = $app->params['phone'];

/** @var Regions $regions */
$regions = $this->container->get(Regions::class);
$regionName = $regions->getServerRegion()->getRegionName();
$facebookLink = $app->params['facebookLinks'][$regionName];
?>
<footer class="bottom-container">
    <div class="container">
        <nav>
            <div class="footer clearfix">
                <div class="logo">
                    <a href="<?= $app->createUrl('index/index') ?>"></a>
                    <div class="contacts-inside">
                        <h3 class="footer-title social-title"><?= $this->t('We are in social networks') ?>:</h3>
                        <?php if (!empty($facebookLink)): ?>
                            <div class="fb-page"
                                 data-href="<?= $app->params['facebookLinks'][$regionName] ?>"
                                 data-tabs="timeline" data-width="220" data-height="70" data-small-header="false"
                                 data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false">
                                <blockquote cite="<?= $app->params['facebookLinks'][$regionName] ?>"
                                            class="fb-xfbml-parse-ignore">
                                    <a href="<?= $app->params['facebookLinks'][$regionName] ?>">
                                        Mommy.com - <?= $regionName ?>
                                    </a>
                                </blockquote>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="contacts">
                    <h3 class="footer-title social-title"><?= $this->t('We are in social networks') ?>:</h3>
                    <?php if (!empty($facebookLink)): ?>
                        <div class="padding-block-10">
                            <div class="fb-page"
                                 data-href="<?= $app->params['facebookLinks'][$regionName] ?>"
                                 data-tabs="timeline" data-width="280" data-height="70" data-small-header="false"
                                 data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false">
                                <blockquote cite="<?= $app->params['facebookLinks'][$regionName] ?>"
                                            class="fb-xfbml-parse-ignore">
                                    <a href="<?= $app->params['facebookLinks'][$regionName] ?>">
                                        Mommy.com - <?= $regionName ?>
                                    </a>
                                </blockquote>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php if ($this->beginCache('mob-footer-menu-new', ['varyByLanguage' => true, 'duration' => 30])) { ?>
                    <?php $isPastEvent = true;
                    $i = 1; ?>
                    <?php foreach (StaticPageCategoryRecord::model()->findAll() as $category): /* @var $category StaticPageCategoryRecord */ ?>
                        <div class="footer-group <?= $category->url == $req->getParam('category') ? 'active' : '' ?>">
                            <h3 class="footer-title" href="javascript:void(0);"><?= $this->t($category->name) ?>
                                <b></b></h3>
                            <ul class="footer-submenu">
                                <?php foreach ($category->pagesVisible as $item): /* @var $item StaticPageRecord */ ?>
                                    <li style="<?= $category->url != $req->getParam('category') ? '' : '' ?>">
                                        <a class="footer-submenu-item"
                                           href="<?= $app->createUrl('static/index', ['category' => $category->url, 'page' => $item->url]) ?>">
                                            <?= $item->title ?>
                                        </a>
                                    </li>
                                    <?php if ($isPastEvent && count($category->pagesVisible) == $i): ?>
                                        <li>
                                            <a class="footer-submenu-item"
                                               href="<?= $app->createUrl('past/index') ?>"><?= $this->t('Past promotions') ?></a>
                                        </li>
                                        <?php $isPastEvent = false ?>
                                    <?php endif; ?>
                                    <?php $i++; ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endforeach ?>
                    <div class="footer-group">
                        <h3 class="footer-title spec">
                            <a class="detached-pink-link" href="<?= $app->createUrl('instruction/index') ?>">
                                <?= $this->t('How to determine a size') ?>
                                <svg version="1.2" preserveAspectRatio="none" viewBox="0 -6 24 24" class="ng-element"
                                     data-id="8c248f5a3e3ff78a6f53693b32d7d296"
                                     style="opacity: 1; fill: rgb(223, 90, 109); margin-top: 6px; width: 20px; height: 20px; overflow: visible;">
                                    <g>
                                        <path
                                                d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"
                                                style="fill: rgb(223, 90, 109);"></path>
                                    </g>
                                </svg>
                            </a>
                    </div>
                    <?php $this->endCache();
                } ?>


            </div>
        </nav>
        <div class="footer-bottom">
            <br>
        </div>
    </div>
</footer>
