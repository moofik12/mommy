<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Widget\FrontendMobile\MamamLinkPager;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/**
 * @var $this FrontController
 * @var $provider CDataProvider
 * @method EventRecord[] $provider->getData()
 * @var array $events
 */

$this->pageTitle = $this->t('Past promotions');
$app = $this->app();
$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
$cart = $app->user->cart;
?>
<div class="main">
    <div class="promo-now">
        <?php foreach ($events as $index => $item): /* @var $item EventRecord */ ?>
            <section>
                <a class="a" href="<?= $app->createUrl('event/index', ['id' => $item->id]) ?>">
                    <div class="box">
                        <header>
                            <div class="image-wrapper waiting">
                                <div class="box-img">
                                    <?= CHtml::image(
                                        $app->baseUrl . '/static/blank.gif', $this->t('stock'), [
                                            'data-src' => $item->logo->getThumbnail('mid320')->url,
                                            'data-lazyload' => 'true',
                                        ]
                                    ) ?>

                                    <?php if ($this->beginCache('event-promo-discount-' . $item->id, ['varyByLanguage' => true])) { ?>
                                        <label class="sale-event sale-spring">
                                            <span class="text">-<?= $item->promoDiscountPercent ?><span>%</span></span>
                                        </label>
                                        <?php if ($item->is_drop_shipping): ?>
                                            <label class="fast-delivery"></label>
                                        <?php endif; ?>
                                        <?php $this->endCache();
                                    } ?>
                                </div>
                            </div>
                            <div class="title-wrapper">
                                <h1 class="title"><?= CHtml::encode($item->name) ?></h1>
                            </div>
                        </header>
                        <div class="text-wrapper">
                            <p class="about text"><?= CHtml::encode($item->description_short) ?></p>
                            <p class="time text"><?= CHtml::tag(
                                    'time',
                                    ['datetime' => $tf->formatMachine($item->end_at), 'data-countdown' => true],
                                    $tf->format($item->end_at)
                                ) ?></p>

                            <?php if ($this->beginCache('event-lowest-price-' . $item->id, ['varyByLanguage' => true])) { ?>
                                <p class="price text"><?= $this->t('from') ?> <span
                                            class="lowest-price"><?= $cf->format($item->getMinSellingPrice()) ?></span>
                                </p>
                                <?php $this->endCache();
                            } ?>
                        </div>
                    </div>
                </a>
            </section>
        <?php endforeach ?>
    </div>
    <nav>
        <div class="goods-pagination clearfix">
            <?php $this->widget(MamamLinkPager::class, [
                'pages' => $provider->pagination,
            ]) ?>
        </div>
    </nav>
    <div class="up-btn"><a href="javascript:scrollTop();"><?= $this->t('Up') ?></a></div>
</div>
