<?php

use MommyCom\Model\Db\UserDistributionRecord;

?>
<div class="mailing-type" id="mailing-type">
    <div class="mailing-type-header">
        <div class="header-image"></div>
        <div class="header-title">
            <h2><?= $this->t('We often launch new promotions with the best products at the lowest prices for you') ?></h2>
        </div>
    </div>
    <div class="mailing-type-content">
        <h3><?= $this->t('Choose which e-mail notifications you want to receive from us') ?></h3>
        <div class="mailing-type-items">
            <div class="mailing-type-item">
                <?= CHtml::ajaxLink(
                    '<div class="img mailing-icon3"></div>
                                <p>' . $this->t('Newsletters with individual offers') . '</p>
                                <div class="select"><span>' . $this->t('Choose') . '</span></div>',
                    $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_WEEK]),
                    [
                        'success' => 'function(data) {
                            if (data.success) {
                                    $("#mailing-type").hide();
                                    window.location.replace(Mamam.createUrl("partner/success"));
                                    return;
                                }
                                            
                                var error = data.errors.length > 0 ? data.errors[0] : false;
                            if (error) {
                                alert(error);
                            }
                        }',
                        'error' => 'function() {
                            alert("' . $this->t('Error. Check your internet connection') . '");
                        }',
                    ],
                    ['class' => 'block-link']
                ) ?>
            </div>
            <div class="mailing-type-item center-item">
                <?= CHtml::ajaxLink(
                    '<div class="img mailing-icon2"></div>
                                <p>' .
                    $this->t('Letters of new promotions, individual offers, <span>dollar\'s bonus</span> for <br /> purchases and <span>gifts</span>') .
                    '</p>
                                <div class="select"><span>' . $this->t('Choose') . '</span></div>',
                    $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_DAY]),
                    [
                        'success' => 'function(data) {
                            if (data.success) {
                                    $("#mailing-type").hide();
                                    window.location.replace(Mamam.createUrl("partner/success"));
                                    return;
                                }
                                            
                                var error = data.errors.length > 0 ? data.errors[0] : false;
                            if (error) {
                                alert(error);
                            }
                        }',
                        'error' => 'function() {
                            alert("Error. Check your internet connection");
                        }',
                    ],
                    ['class' => 'block-link']
                ) ?>
            </div>
            <div class="mailing-type-item">
                <?= CHtml::ajaxLink(
                    '<div class="img mailing-icon1"></div>
                                <p>' . $this->t('Letters<br />about new stocks') . '</p>
                                <div class="select"><span>' . $this->t('Choose') . '</span></div>',
                    $this->app()->createUrl('account/updateDistribution', ['type' => UserDistributionRecord::TYPE_EVERY_DAY]),
                    [
                        'success' => 'function(data) {
                            if (data.success) {
                                    $("#mailing-type").hide();
                                    window.location.replace(Mamam.createUrl("partner/success"));
                                    return;
                                }
                                            
                                var error = data.errors.length > 0 ? data.errors[0] : false;
                            if (error) {
                                alert(error);
                            }
                        }',
                        'error' => 'function() {
                            alert("' . $this->t('Error. Check your internet connection') . '");
                        }',
                    ],
                    ['class' => 'block-link']
                ) ?>
            </div>
        </div>
    </div>
</div>
