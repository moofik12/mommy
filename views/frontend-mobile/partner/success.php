<?php

use MommyCom\Controller\FrontendMobile\PartnerController;

/**
 * @var $this PartnerController
 */

$this->pageTitle = 'Your application is accepted';
$app = $this->app();
?>
<header class="finish-header">
    <div class="container clearfix">
        <a class="finish-logo" href="<?= $app->createUrl('index/index') ?>"></a>
    </div>
</header>
<main class="supreme-container">
    <div class="supreme-inner">
        <div class="block-finish">
            <div class="container">
                <div class="finish-inner clearfix">
                    <div class="finish-image"></div>
                    <h1 class="finish-title"><?= $this->t('Your application is accepted') ?></h1>
                    <p class="finish-text"><?= $this->t('Once the affiliate program is launched - you will receive an email notification.') ?>
                        <br>
                        <strong><?= $this->t('Do not miss our announcements and promotions!') ?></strong></p>
                    <a class="finish-btn"
                       href="<?= $app->createUrl('index/index') ?>"><?= $this->t('Home') ?></a>
                </div>
            </div>
        </div>
    </div>
</main>
