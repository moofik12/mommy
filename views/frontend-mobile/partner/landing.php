<?php

use MommyCom\Controller\FrontendMobile\PartnerController;
use MommyCom\Model\Db\UserPartnerRecord;
use MommyCom\Model\Form\PartnerRegistrationForm;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;

/**
 * @var $this PartnerController
 * @var $source string
 * @var $model PartnerRegistrationForm
 */

$this->pageTitle = 'Заработай с MOMMY';
$app = $this->app();
/** @var $user ShopWebUser */
$user = $app->user;
/** @var \MommyCom\Service\Deprecated\CurrencyFormatter $cf */
$cf = $app->currencyFormatter;

$modelName = get_class($model);
if (false !== strpos($modelName, '\\')) {
    $modelName = substr($modelName, strpos($modelName, '\\') + 1);
}
?>
<header class="main-header">
    <div class="container clearfix">
        <div class="header-logo"><a href="<?= $app->createUrl('index/index') ?>"></a></div>
        <div class="header-block">
            <h1 class="header-title"><?= $this->t('Earn money<br>with Mommy!') ?></h1>
            <p class="header-text">
                <?= $this->t('Soon you will have the opportunity to earn<br> passive income with the affiliate program<br> of the MOMMY.COM Shopping Club. All you need is MOMMY.COM account, friends, relatives and a little bit of personal time') ?>
            </p>
        </div>
    </div>
</header>
<main class="supreme-container">
    <div class="supreme-inner">
        <section class="block1 clearfix">
            <div class="container">
                <header class="header-section">
                    <h1 class="title-section"><?= $this->t('How does it work?') ?></h1>
                </header>
                <p>
                    <?= $this->t('All you need to do is invite your friends and strangers<br>to shop at MOMMY.COM. They get a <strong>discount on the purchase, </strong><br>and you - <strong>a percentage of profit from sales</strong>') ?></p>
                <p>
                    <?= $this->t('Members of the partner program MOMMY.COM will be granted access to<br> the partner office. It will display the statistics of registrations<br> and purchases, pictures to attract users and special promotional codes') ?>
                </p>
                <ul class="block1-list1">
                    <li class="block1-list1-li1">
                        <?= $this->t('Help friends and family make purchases at the lowest prices') ?>
                    </li>
                    <li class="block1-list1-li2">
                        <?= Yii::t('category', 'Get passive income without any<br>effort. Dispose of what you earn as you wish to - for purchases on MOMMY.COM or withdrawal to a bank card.') ?>
                    </li>
                    <li class="block1-list1-li3">
                        <?= Yii::t('category', 'A convenient partnership cabinet, rich advertising opportunities') ?>
                    </li>
                </ul>
            </div>
        </section>
        <section class="block2 clearfix">
            <div class="container">
                <header class="header-section">
                    <h1 class="title-section">
                        <?= $this->t('How much can you earn?') ?>
                    </h1>
                </header>
                <p>
                    <?= $this->t('Consider a small example and calculate the profit') ?>
                </p>
                <ul class="block2-list1">
                    <li class="block2-list1-li1">
                        <?= $this->t('You have invited') ?>
                        <span class="list-li-bold"><strong>20</strong></span>
                    </li>
                    <li class="block2-list1-li2">
                        <?= $this->t('Everybody will do <span class="list-li-bold">2</span> purchases per month') ?>
                        <span class="list-li-sub">(<?= $this->t('Allowed, but many people buy more often') ?>
                            )</span>
                    </li>
                    <li class="block2-list1-li3">
                        <?= $this->t('Average check') ?>
                        <span class="list-li-bold"><strong>500</strong><small><small><?= $cf->getCurrency() ?></small></small></span>
                        <?= $this->t('one order') ?>
                        <span class="list-li-sub">(<?= $this->t('About') ?>)</span>
                    </li>
                </ul>
                <p class="block2-text"><?= $this->t('this means that every month,') ?><br>
                    <?= $this->t('this means that every month,') ?><br>
                    <?= $this->t('By doing almost nothing you will receive additional') ?></p>
                <p class="block2-profit">36$</p>
                <p class="block2-bold-text2"><?= $this->t('And what if you invite 50 people?') ?></p>
                <p class="block2-text2"><?= $this->t('This is real, given that we have a large assortment<br> and very democratic prices!') ?></p>
            </div>
        </section>
        <div class="block3 clearfix">
            <div class="container clearfix">
                <p class="block3-text1">
                    <?= $this->t('The affiliate program will work in a trial mode soon -<br>only a limited number of people will receive early access!') ?>
                </p>
                <p class="block3-text2">
                    <?= $this->t('Apply for participation as early as possible<br>to be among the lucky ones!') ?>
                </p>
                <div class="block3-wrapper">
                    <?php if ($user->getIsGuest()) : ?>
                        <div class="block3-inner">
                            <?php /** @var $form CActiveForm */
                            $form = $this->beginWidget('CActiveForm', [
                                'action' => $this->app()->createUrl('partner/landing', ['source' => $source]),
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => true,
                                'clientOptions' => [
                                    'validateOnSubmit' => true,
                                    'afterValidate' => 'js:function($form, data, hasError) {
                                    if (hasError) {
                                        return;
                                    }                                    
                                    $.ajax({
                                        url: $form.attr("action"),
                                        type: $form.attr("method"),
                                        data: $form.serialize(),
                                        dataType: "json",
                                        success: function (data) {
                                            if (data !== null && typeof data === "object") {
                                                if (data.success) {
                                                    $(".block3-inner").hide();
                                                    //$("#mailing-type").modal("show");
                                                    window.location.replace(Mamam.createUrl("partner/mailingType"));
                                                } else if (data.errors && data.errors.length > 0) {
                                                    alert(data.errors[0]);
                                                }
                                            }
                                        },
                                        error: function () {
                                            console.log("' . $this->t('Error. Check your internet connection') . '");
                                        }
                                    });

                                return false;
                            }',
                                ],
                            ]) ?>
                            <div class="form-inner">
                                <?= $form->labelEx($model, 'email', ['class' => 'form-label']) ?>
                                <div class="form-input-field <?= $model->hasErrors('email') ? 'has-error' : '' ?>">
                                    <?= $form->textField($model, 'email', ['placeholder' => 'Введите ваш email', 'autocomplete' => 'off', 'class' => 'form-input']) ?>
                                    <?= $form->error($model, 'email', ['class' => 'message']) ?>
                                </div>
                                <div class="checkbox-line">
                                    <input id="checkbox1" name="<?= $modelName ?>[isConfirmPartner]"
                                           value="<?= $model->isConfirmPartner ?>" <?= $model->isConfirmPartner ? 'checked="checked"' : '' ?>
                                           type="checkbox">
                                    <label for="checkbox1"><?= $this->t('I agree with the <a href="{url}">terms and conditions of the affiliate program</a>', ['{url}' => $app->createUrl('static/index', ['category' => 'company', 'page' => 'partner-rules'])]) ?></label>
                                </div>
                                <div class="checkbox-line">
                                    <input id="checkbox2" name="<?= $modelName ?>[isConfirmPrivacy]"
                                           value="<?= $model->isConfirmPrivacy ?>" <?= $model->isConfirmPrivacy ? 'checked="checked"' : '' ?>
                                           type="checkbox">
                                    <label for="checkbox2"><?= $this->t('I agree with <a href="{url}">site rules</a>',
                                            ['{msg}' => $app->createUrl('static/index', ['category' => 'service', 'page' => 'privacy-with-promocode'])]) ?></label>
                                </div>
                                <button class="form-btn" type="submit"
                                        name="submitBtn" <?= (!$model->isConfirmPartner || !$model->isConfirmPrivacy) ? 'disabled' : '' ?>
                                        id="form-btn"><?= $this->t('Apply now') ?></button>
                            </div>
                            <script>
                                $(function () {
                                    $('#checkbox1, #checkbox2').change(function () {
                                        if ($('#checkbox1').is(':checked') && $('#checkbox2').is(':checked')) {
                                            $('#form-btn').removeAttr('disabled');
                                        } else {
                                            $('#form-btn').attr('disabled', 'disabled');
                                        }
                                    });
                                });
                            </script>
                            <?php $this->endWidget() ?>
                        </div>
                    <?php else: ?>
                        <?php $partner = UserPartnerRecord::model()->userId($user->getModel()->id)->find(); ?>
                        <?php if ($partner === null) : ?>
                            <div class="block3-inner">
                                <?php /** @var $form CActiveForm */
                                $form = $this->beginWidget('CActiveForm', [
                                    'action' => $this->app()->createUrl('partner/landing', ['source' => $source]),
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => true,
                                    'clientOptions' => [
                                        'validateOnSubmit' => true,
                                        'afterValidate' => 'js:function($form, data, hasError) {
                                            if (hasError) {
                                                return;
                                            }                                    
                                            $.ajax({
                                                url: $form.attr("action"),
                                                type: $form.attr("method"),
                                                data: $form.serialize(),
                                                dataType: "json",
                                                success: function (data) {
                                                    if (data !== null && typeof data === "object") {
                                                        if (data.success) {
                                                            $(".block3-inner").hide();
                                                            window.location.replace(Mamam.createUrl("partner/success"));
                                                        } else if (data.errors && data.errors.length > 0) {
                                                            alert(data.errors[0]);
                                                        }
                                                    }
                                                },
                                                error: function () {
                                                    console.log("' . $this->t('Error. Check your internet connection') . '");
                                                }
                                            });
        
                                        return false;
                                    }',
                                    ],
                                ]) ?>
                                <div class="form-inner">
                                    <div class="checkbox-line checkbox-line-margin">
                                        <input id="<?= $modelName ?>_isConfirmPartner"
                                               name="<?= $modelName ?>[isConfirmPartner]"
                                               value="<?= $model->isConfirmPartner ?>" <?= $model->isConfirmPartner ? 'checked="checked"' : '' ?>
                                               type="checkbox">
                                        <label for="<?= $modelName ?>_isConfirmPartner"><?= $this->t('I agree with the <a href="{url}">terms and conditions of the affiliate program</a>', ['{url}' => $app->createUrl('static/index', ['category' => 'company', 'page' => 'partner-rules'])]) ?></label>
                                    </div>
                                    <div style="display: none;"><?= $form->error($model, 'isConfirmPartner') ?></div>
                                    <button class="form-btn"
                                            type="submit" <?= (!$model->isConfirmPartner) ? 'disabled' : '' ?>
                                            id="form-btn"><?= $this->t('Apply now') ?></button>
                                    <script>
                                        $(function () {
                                            $('#<?=$modelName?>_isConfirmPartner').change(function () {
                                                if ($(this).is(':checked')) {
                                                    $('#form-btn').removeAttr('disabled');
                                                } else {
                                                    $('#form-btn').attr('disabled', 'disabled');
                                                }
                                            });
                                        });
                                    </script>
                                </div>
                                <?php $this->endWidget() ?>
                            </div>
                        <?php else: ?>
                            <div class="block3-inner">
                                <div class="form-inner">
                                    <label class="form-label">
                                        <?= $this->t('You have already applied.') ?><br>
                                        <?= $this->t('Wait for news on your e-mail') ?></label>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>


