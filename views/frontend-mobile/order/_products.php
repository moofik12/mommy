<?php

use MommyCom\Model\Db\EventRecord;
use MommyCom\Model\Db\OrderDiscountCampaignRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCartOrderOwn;

/* @var $this FrontController */
/* @var $cart ShopShoppingCart */
/* @var $bonuspoints ShopBonusPoints */
/* @var $deliveryToken string */

$this->pageTitle = $this->t('List of products - Order');
$app = $this->app();

$splitTesting = $app->splitTesting;
/* @var $splitTesting \MommyCom\Service\SplitTesting */

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();

$cartCost = $cart->getCost(false);
$costTotal = $cart->getCost();
$orderDiscountCampaign = $cart->getOrderDiscountCampaign();
$bonusAmount = $cart->getBonusesCost();
$discountCampaignText = '';
$countPositionsAsOrders = count($cart->getPositionsAsOrders());

if ($orderDiscountCampaign
    && $cart->getReservedCount() > 0
    && $cart->isEnableOrderDiscountCampaign(true, true)
    && $orderDiscountCampaign->getAmountNotEnoughUseCampaign($cartCost) > 0) {
    $type = $orderDiscountCampaign->getMayBeUsedType($cartCost);
    $amountNotEnoughUseCampaign = $orderDiscountCampaign->getAmountNotEnoughUseCampaign($cartCost);

    if ($type == OrderDiscountCampaignRecord::USED_PERCENT) {
        $discountCampaignText = Yii::t('cart',
            '<div class="more-discount">for a discount <span class="green">{percent}%</span> <span class="red">{amount} {currency}</span> left</div> ',
            ['{percent}' => $orderDiscountCampaign->percent, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
    } elseif ($type == OrderDiscountCampaignRecord::USED_CASH) {
        $discountCampaignText = Yii::t('cart',
            '<div class="more-discount">for a discount <span class="green">{cash} {currency}</span> <span class="red">{amount} {currency}</span> left</div>',
            ['{cash}' => $orderDiscountCampaign->cash, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
    }
}

?>

<div class="cart">
    <?php foreach ($cart->getPositionsAsOrders() as $key => $cartOrder): ?>
        <?php
        $orderId = $key + 1;
        $orderCost = $cartOrder->getCost(false);

        $discountCampaignText = '';

        if ($orderDiscountCampaign
            && $cartOrder->getReservedCount() > 0
            && $cartOrder->isEnableOrderDiscountCampaign(true, true)
            && $orderDiscountCampaign->getAmountNotEnoughUseCampaign($orderCost) > 0) {
            $type = $orderDiscountCampaign->getMayBeUsedType($orderCost);
            $amountNotEnoughUseCampaign = $orderDiscountCampaign->getAmountNotEnoughUseCampaign($orderCost);

            if ($type == OrderDiscountCampaignRecord::USED_PERCENT) {
                $discountCampaignText = Yii::t('cart',
                    '<div class="more-discount">for a discount <span class="green">{percent}%</span> <span class="red">{amount} {currency}</span> left</div> ',
                    ['{percent}' => $orderDiscountCampaign->percent, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
            } elseif ($type == OrderDiscountCampaignRecord::USED_CASH) {
                $discountCampaignText = Yii::t('cart',
                    '<div class="more-discount">for a discount <span class="green">{cash} {currency}</span> <span class="red">{amount} {currency}</span> left</div>',
                    ['{cash}' => $orderDiscountCampaign->cash, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
            }
        }
        ?>

        <div class="cart-items">
            <div class="cart-title <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>">
                <?= $this->t('Order №') ?><?= $orderId ?> (<?= CHtml::encode($cartOrder->getDisplayName()) ?>)
                <?php if (!$cartOrder->isOwn): ?>
                    <a class="icon-fast-delivery"
                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                       title="<?= $this->t('What is \'Fast Delievery\'?') ?>"></a>
                <?php endif; ?>
                <?php if ((!$cartOrder->isOwn || $countPositionsAsOrders > 1) && !$cartOrder->isEnoughToBuy() && $cartOrder->getReservedItemsCount() > 0): ?>
                    <div class="cart-form-not-enough">
                        <span>
                            <?= $this->t('Minimum order amount') ?> <?= $cf->format($cartOrder->getMinOrderAmount()) ?>
                        </span>
                    </div>
                <?php endif; ?>
            </div>

            <ul class="cart-items-list">
                <?php foreach ($cartOrder->getPositions() as $position): ?>
                    <?php $event = EventRecord::model()->findByPk($position->event_id);
                    $productGroup = $event->getGroupedProduct($position->product->product_id);
                    $sale = $productGroup->discountPercent; ?>

                    <li class="cart-detail-position"
                        data-position-price="<?= $position->product->price ?>"
                        data-position-number="<?= $position->number ?>"
                        data-position-event="<?= $position->event_id ?>"
                        data-position-product="<?= $position->product->product_id ?>"
                        data-position-size="<?= $position->product->size ?>"
                        data-position-token="<?= $this->app()->tokenManager->getToken($position->product->product_id) ?>">
                        <div class="cart-item">
                            <div class="cart-item-content">
                                <div class="cart-wrapper-image">
                                    <?= CHtml::link(
                                        CHtml::image($position->product->product->logo->getThumbnail('mid320_prodlist')->url, '', ['width' => 96, 'height' => 114]),
                                        ['product/index', 'id' => $position->product->product_id, 'eventId' => $position->event_id],
                                        ['class' => 'title']
                                    ) ?>
                                </div>
                                <div class="cart-mobile">
                                    <div class="title-line">
                                        <a class="title"
                                           href="<?= $app->createUrl('product/index', ['id' => $position->product->product_id, 'eventId' => $position->event_id]) ?>">
                                            <?= CHtml::encode($position->product->product->name) ?>
                                        </a>
                                        <span class="number-line">x<span class="number"><?= $position->number ?></span></span>
                                    </div>
                                    <?php if (!empty($position->product->color) && !empty($position->product->size)): ?>
                                        <ul class="about-inner">
                                            <?php if (!empty($position->product->color)): ?>
                                                <li><?= $this->t('Colour:') ?> <span
                                                        class="color"><?= $position->product->color ?></span></li>
                                            <?php endif ?>

                                            <?php if (!empty($position->product->size)): ?>
                                                <li><?= $this->t('Size:') ?> <span
                                                        class="size"><?= $position->product->size ?></span></li>
                                            <?php endif ?>
                                        </ul>
                                    <?php endif ?>
                                    <div class="cart-time-info <?= $position->isExpired ? 'cross-out' : '' ?>">
                                        <div class="cart-price">
                                            <span class="price"><?= $cf->format($position->totalPrice) ?></span>
                                            <span class="sale">-<?= $sale ?>%</span>
                                            <?= CHtml::ajaxLink(
                                                '',
                                                [
                                                    'cart/numberDown',
                                                    'eventId' => $position->event_id,
                                                    'productId' => $position->product->product_id,
                                                    'size' => $position->product->size,
                                                ],
                                                [
                                                    'update' => '.cart',
                                                    'complete' => 'function() {
                                                    Mamam.ecommerce.remove("' . $position->event_id . '/' . $position->product->product_id . '", 1);
                                                    Mamam.cart.reloadQuickCart();
                                                    Mamam.cart.getList(null, function(result) {
                                                        var data = [];
                                                        if (result["data"]) {
                                                            data = result["data"];
                                                        }
        
                                                        Mamam.app.cart.updateList(data);
                                                    });
                                                }',
                                                ],
                                                [
                                                    'class' => 'delete',
                                                    'id' => 'numdown1' . $position->id,
                                                ]
                                            ) ?>
                                        </div>
                                        <div class="extend">
                                            <span class="time-out"><?= $this->t('time is over') ?></span>
                                            <a class="extend-btn"
                                               data-is-update-btn="true"><?= $this->t('prolong the reservation') ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach ?>
            </ul>

            <div class="cart-foot <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>">
                <div class="image"><span
                        class="number"><?= $cartOrder->getItemsCount() ?></span> <?= $this->t('goods') ?> <?= $this->t('for purchase') ?>
                </div>
                <div class="price">
                    <?php if ($cartOrder->getBonusesCost() > 0): ?>
                        <div class="price-bonus">
                            <?= $this->t('Bonuses:') ?> <?= $cf->format($cartOrder->getBonusesCost()) ?>
                        </div>
                    <?php endif ?>
                    <?php if ($discountCampaignText) : ?>
                        <?= $discountCampaignText ?>
                    <?php endif ?>
                </div>
            </div>

        </div>
    <?php endforeach; ?>

    <?php foreach ($cart->getPositionsAsOrders() as $key => $cartOrder): ?>
        <?php
        $orderId = $key + 1;
        $orderCost = $cartOrder->getCost(false);

        $discountCampaignText = '';

        if ($orderDiscountCampaign
            && $cartOrder->getReservedCount() > 0
            && $cartOrder->isEnableOrderDiscountCampaign(true, true)
            && $orderDiscountCampaign->getAmountNotEnoughUseCampaign($orderCost) > 0) {
            $type = $orderDiscountCampaign->getMayBeUsedType($orderCost);
            $amountNotEnoughUseCampaign = $orderDiscountCampaign->getAmountNotEnoughUseCampaign($orderCost);

            if ($type == OrderDiscountCampaignRecord::USED_PERCENT) {
                $discountCampaignText = Yii::t('cart',
                    '<div class="more-discount">for a discount <span class="green">{percent}%</span> <span class="red">{amount} {currency}</span> left</div> ',
                    ['{percent}' => $orderDiscountCampaign->percent, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
            } elseif ($type == OrderDiscountCampaignRecord::USED_CASH) {
                $discountCampaignText = $this->t(
                    '<div class="more-discount">for a discount <span class="green">{cash} {currency}</span> <span class="red">{amount} {currency}</span> left</div>',
                    ['{cash}' => $orderDiscountCampaign->cash, '{amount}' => $amountNotEnoughUseCampaign, '{currency}' => $cn->getName('short')]);
            }
        }
        ?>
        <div class="cart-full">
            <div class="cart-title <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>">
                <?= $this->t('Order') ?> №<?= $orderId ?> (<?= CHtml::encode($cartOrder->getDisplayName()) ?>)
                <?php if (!$cartOrder->isOwn): ?>
                    <a class="icon-fast-delivery"
                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                       title="What is 'Fast Delievery'?"></a>
                <?php endif; ?>
                <?php if ((!$cartOrder->isOwn || $countPositionsAsOrders > 1) && !$cartOrder->isEnoughToBuy() && $cartOrder->getReservedItemsCount() > 0): ?>
                    <div class="cart-form-not-enough">
                        <span>
                            <?= $this->t('Minimum order amount') ?> <?= $cf->format($cartOrder->getMinOrderAmount()) ?>
                        </span>
                    </div>
                <?php endif; ?>
            </div>

            <table class="cart-goods">
                <thead>
                <tr>
                    <th class="image"><?= $this->t('Product') ?></th>
                    <th class="about"><?= $this->t('Description') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($cartOrder->getPositions() as $position): ?>
                    <tr class="separator">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="cart-detail-position"
                        data-position-price="<?= $position->product->price ?>"
                        data-position-number="<?= $position->number ?>"
                        data-position-event="<?= $position->event_id ?>"
                        data-position-product="<?= $position->product->product_id ?>"
                        data-position-size="<?= $position->product->size ?>"
                        data-position-token="<?= $this->app()->tokenManager->getToken($position->product->product_id) ?>">
                        <td class="image">
                            <?= CHtml::link(
                                CHtml::image($position->product->product->logo->getThumbnail('mid320_prodlist')->url),
                                ['product/index', 'id' => $position->product->product_id, 'eventId' => $position->event_id],
                                ['class' => 'title']
                            ) ?>
                        </td>
                        <td class="about">
                            <div class="about-inner">
                                <a class="item-link"
                                   href="<?= $app->createUrl('product/index', ['id' => $position->product->product_id, 'eventId' => $position->event_id]) ?>">
                                    <?= CHtml::encode($position->product->product->name) ?>
                                </a>
                                <a class="item-cat-link"
                                   href="<?= $app->createUrl('event/index', ['id' => $position->event_id]) ?>">
                                    <?= CHtml::encode($position->event->name) ?>
                                </a>
                                <?php if (!empty($position->product->color) && !empty($position->product->size)): ?>
                                    <ul>
                                        <?php if (!empty($position->product->color)): ?>
                                            <li><?= $this->t('Colour:') ?> <span
                                                    class="color"><?= $position->product->color ?></span></li>
                                        <?php endif ?>

                                        <?php if (!empty($position->product->size)): ?>
                                            <li><?= $this->t('Size:') ?> <span
                                                    class="size"><?= $position->product->size ?></span></li>
                                        <?php endif ?>
                                        <li>
                                            <?= $this->t('Price') ?> <?= $cf->format($position->price) ?>
                                        </li>
                                        <li>
                                            <?= $this->t('Quantity') ?> <?= $position->number ?>
                                        </li>
                                    </ul>
                                <?php endif ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
            <div class="cart-foot <?= !$cartOrder->isEnoughToBuy() ? 'cart-form-not-enough-text' : '' ?>">
                <div class="image"><span
                        class="number"><?= $cartOrder->getItemsCount() ?></span> <?= $this->t('goods') ?> <?= $this->t('for purchase') ?>
                </div>
                <div class="price">
                    <?php if ($cartOrder->getBonusesCost() > 0): ?>
                        <div class="price-bonus">
                            <?= $this->t('Offers') ?>: <?= $cf->format($cartOrder->getBonusesCost()) ?>
                        </div>
                    <?php endif ?>
                    <?php if ($discountCampaignText) : ?>
                        <?= $discountCampaignText ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <?php if ($countPositionsAsOrders > 1): ?>
        <div class="cart-footer">
            <div class="cart-number">
                <span class="number"><?= $cart->getOrdersReservedItemsCount() ?></span> <?= $this->t('goods') ?> <?= $this->t('for purchase') ?>
            </div>
            <div class="price">
                <?php if ($bonusAmount): ?>
                    <div class="price-bonus"><?= $this->t('Offers') ?>
                        : <?= $bonusAmount ?> <?= $cn->getName('short') ?></div>
                <?php endif ?>
            </div>
        </div>
    <?php endif; ?>
</div>
