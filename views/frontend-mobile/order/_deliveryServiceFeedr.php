<?php

use MommyCom\Controller\FrontendMobile\OrderController;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var OrderController $this
 * @var CActiveForm $form
 * @var Feedr $model
 * @var string $tabId
 * @var ShopShoppingCart $cart
 * @var array $rates
 */

/** @var FeedrApi $feedrApi */
$feedrApi = $this->container->get(FeedrApi::class);

?>
<div data-tab-for="<?= $tabId ?>" data-block="info">
    <div class="standard-block">
        <div class="standard-title">
            <?= $form->label($model, 'rateId', ['class' => 'left-part']) ?>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field <?= $model->hasErrors('rateId') ? 'error' : '' ?> bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'rateId',
                        'data' => [
                            $rates ?? [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a delivery service'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    if (!item) return;
                                    var price = parseInt(item.getSelectedText().split(\' - \')[2])
                                    var $container = $("li[data-delivery-price=true]");
                                    
                                    $container
                                        .find(".cheque-right")
                                        .removeClass("hide-i")
                                        .find("[data-value]")
                                        .data("value", price)
                                        .text(price);
                                    $container.find(".undertitle").text(item.getSelectedText().split(\' - \')[0]);
                                    $(".container .cheque").trigger("recalculate");
                                    
                                    $("#' . CHtml::activeId($model, 'rateVisualName') . '").val(item.getSelectedText());
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'rateId', ['class' => 'message']) ?>
                    <?= $form->hiddenField($model, 'rateVisualName') ?>
                </div>
            </div>
        </div>
    </div>
</div>
