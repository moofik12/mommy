<?php

use MommyCom\Controller\FrontendMobile\OrderController;
use MommyCom\Service\Delivery\Api\FeedrApi;
use MommyCom\Service\Delivery\FormModel\Feedr;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var OrderController $this
 * @var CActiveForm $form
 * @var Feedr $model
 * @var string $tabId
 * @var ShopShoppingCart $cart
 */

/** @var FeedrApi $feedrApi */
$feedrApi = $this->container->get(FeedrApi::class);

?>
<div data-tab-for="<?= $tabId ?>" data-block="info">
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Where to deliver?') ?></label>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'provinceId',
                        'data' => [
                            $feedrApi->getProvinces(), 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a province'),
                            'clearSelection' => false,
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var select = $("#' . CHtml::activeId($model, 'cityId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                    select.getDataProvider().reload();

                                    select = $("#' . CHtml::activeId($model, 'suburbId') . '").data("extselect");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);

                                    select = $("#' . CHtml::activeId($model, 'areaId') . '").data("extselect");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'provinceId', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'cityId',
                        'data' => [
                            $model->provinceId ? $feedrApi->getCities($model->provinceId) : [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a city'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'dataProvider' => [
                                'model' => 'ajax',
                                'ajax' => [
                                    'url' => $this->createUrl('order/ajaxFeedrCities'),
                                    'cache' => true,
                                    'data' => new CJavaScriptExpression('function(filter) {
                                        var select = $("#' . CHtml::activeId($model, 'provinceId') . '").data("extselect");
                                        return {
                                            filter: filter || "",
                                            provinceId: select.val()
                                        };
                                    }'),
                                    'results' => new CJavaScriptExpression('function(response) {
                                        var results = {};
                                        $.each(response, function(index, value) {
                                            results[index] = {
                                                val: value.id,
                                                text: value.name,
                                            };
                                        });
                                        return results;
                                    }'),
                                ],
                            ],
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var select = $("#' . CHtml::activeId($model, 'suburbId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                    select.getDataProvider().reload();

                                    select = $("#' . CHtml::activeId($model, 'areaId') . '").data("extselect");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'cityId', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'suburbId',
                        'data' => [
                            $model->cityId ? $feedrApi->getSuburbs($model->cityId) : [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select a suburb'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'dataProvider' => [
                                'model' => 'ajax',
                                'ajax' => [
                                    'url' => $this->createUrl('order/ajaxFeedrSuburbs'),
                                    'cache' => true,
                                    'data' => new CJavaScriptExpression('function(filter) {
                                        var select = $("#' . CHtml::activeId($model, 'cityId') . '").data("extselect");
                                        return {
                                            filter: filter || "",
                                            cityId: select.val()
                                        };
                                    }'),
                                    'results' => new CJavaScriptExpression('function(response) {
                                        var results = {};
                                        $.each(response, function(index, value) {
                                            results[index] = {
                                                val: value.id,
                                                text: value.name,
                                            };
                                        });
                                        return results;
                                    }'),
                                ],
                            ],
                            'events' => [
                                'select, setSelectedItem' => new CJavaScriptExpression('function(item) {
                                    var select = $("#' . CHtml::activeId($model, 'areaId') . '").data("extselect");
                                    select.getDropdown().filterText("");
                                    select.val("");
                                    select.text("");
                                    select.enable();
                                    select.getDataProvider().setData({}, false);
                                    select.getDataProvider().reload();
                                }'),
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'suburbId', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field bottom">
                    <?php $this->widget('extSelect.widgets.ExtSelect', [
                        'model' => $model,
                        'attribute' => 'areaId',
                        'data' => [
                            $model->suburbId ? $feedrApi->getAreas($model->suburbId) : [], 'id', 'name',
                        ],
                        'settings' => [
                            'placeholder' => $this->t('Select an area'),
                            'clearSelection' => false,
                            'disableOnEmpty' => true,
                            'dataProvider' => [
                                'model' => 'ajax',
                                'ajax' => [
                                    'url' => $this->createUrl('order/ajaxFeedrAreas'),
                                    'cache' => true,
                                    'data' => new CJavaScriptExpression('function(filter) {
                                        var select = $("#' . CHtml::activeId($model, 'suburbId') . '").data("extselect");
                                        return {
                                            filter: filter || "",
                                            suburbId: select.val()
                                        };
                                    }'),
                                    'results' => new CJavaScriptExpression('function(response) {
                                        var results = {};
                                        $.each(response, function(index, value) {
                                            results[index] = {
                                                val: value.id,
                                                text: value.name,
                                            };
                                        });
                                        return results;
                                    }'),
                                ],
                            ],
                        ],
                    ]) ?>
                    <?= $form->error($model, 'areaId', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <?= $form->label($model, 'address', ['class' => 'left-part']) ?>
        </div>
        <div class="standard-wrapper">
            <div class="field textarea <?= $model->hasErrors('address') ? 'error' : '' ?> bottom">
                <?= $form->textArea($model, 'address', []) ?>
                <?= $form->error($model, 'address', ['class' => 'message']) ?>
            </div>
        </div>
    </div>
</div>
