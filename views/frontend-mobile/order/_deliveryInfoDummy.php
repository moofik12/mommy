<?php

use MommyCom\Service\Delivery\FormModel\Dummy;

/**
 * @var CActiveForm $form
 * @var Dummy $model
 * @var string $tabId
 */

?>
<div class="standard-block" data-tab-for="<?= $tabId ?>" data-block="info">
    <div class="standard-title">
        <?= $form->label($model, 'address', ['class' => 'left-part left-part-width']) ?>
    </div>
    <div class="standard-wrapper">
        <div class="field textarea bottom">
            <?= $form->textArea($model, 'address') ?>
            <?= $form->error($model, 'address', ['class' => 'message']) ?>
        </div>
    </div>
</div>
