<div class="registration" <?php if (!$visible) { ?> style="display: none;" <?php } ?> id="registration">
    <script type="application/javascript">
        registrationTracker.validatorRegex = <?= json_encode($this->app()
            ->params['validatorRegex']) ?>;
    </script>
    <?php
    $form = $this->beginWidget('CActiveForm', [
        'action' => $this->app()->createUrl('order/registration'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'beforeValidate' => "js:function(form) {
                    var field = $('#RegistrationForm_email');
                    if (field && field.val()) {
                        field.val(field.val().replace(/^\s+|\s+$/g, ''));
                    }
                    
                    return true;
                }",
            'afterValidate' => "js:function(form, data, hasError) {
                    if (!hasError) {
                        $(document).trigger('mamam.registration.success');
                        $(document).trigger('mamam.auth.success');
    
                        return true;
                    }
                }",
        ],
    ]);
    ?>
    <h3><?= $this->t('To proceed with your order please enter your contact dertails:') ?></h3>
    <div class="login-left">
        <div class="field top <?= $registrationForm->hasErrors('name') ? 'error' : '' ?>">
            <label class="visible-title"><?= $this->t('Your Name') ?>:</label>
            <span class="text-input-wrapper"><?= $form->textField($registrationForm, 'name'); ?></span>
            <?= $form->error($registrationForm, 'name', ['class' => 'message']) ?>
        </div>
        <div class="field top <?= $registrationForm->hasErrors('email') ? 'error' : '' ?>">
            <label class="visible-title"><?= $this->t('Your E-mail') ?>:</label>
            <span class="text-input-wrapper"><?= $form->textField($registrationForm, 'email', ['placeholder' => $this->app()
                    ->params['validatorRegex']['email']['default']['placeholder']]); ?></span>
            <?= $form->error($registrationForm, 'email', ['class' => 'message']) ?>
        </div>
        <div class="field top <?= $registrationForm->hasErrors('email') ? 'error' : '' ?>">
            <?= $form->checkBox($registrationForm, 'agreeToTerms', ['checked' => 'checked', 'class' => 'large-checkbox']); ?>
            <?= $form->error($registrationForm, 'agreeToTerms', ['class' => 'message']) ?>
            <label class="visible-title"><?= $this->t('Agree to terms of service') ?></label>
        </div>
    </div>
    <div class="login-right">
        <?= CHtml::submitButton($this->t('Start shopping')); ?>
        <div class="social-enter-container">
            <span class="title-services"><?= $this->t('or through') ?></span>
            <div class="services clearfix">
                <ul>
                    <li><a class="fb" href="/connect/facebook">Facebook</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php $this->endWidget() ?>
</div>
<script>
    $(function () {
        $('a.fb').on('click', function (e) {
            if (!$(this).hasClass('disabled-btn')) {
                return;
            }

            e.preventDefault();
        });

        $('.large-checkbox').on('change', function () {
            if (this.checked) {
                $('a.fb').removeClass('disabled-btn');
                $('.login input[type="submit"]')
                    .removeAttr('disabled')
                    .removeClass('disabled-btn');
            } else {
                $('a.fb').addClass('disabled-btn');
                $('.login input[type="submit"]')
                    .attr('disabled', 'true')
                    .addClass('disabled-btn');
            }
        });
    });
</script>

