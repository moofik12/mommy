<?php

use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Deprecated\CurrencyFormatter;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopOrder;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PhoneNumber;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var FrontController $this
 * @var ShopShoppingCart $cart
 * @var ShopOrder $order
 * @var int $deliveryType
 * @var DeliveryInterface[] $availableDeliveries
 * @var array $sessionOrderData
 * @var array $deliveryModels
 */

$this->pageTitle = $this->t('Delivery method - Ordering');

/** @var MommyWebApplication $app */
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
/** @var CurrencyFormatter $cf */
$cf = $currencyFormatter = $app->currencyFormatter;

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseAssetsUrl = $this->app()->getAssetManager()->publish(\Yii::getPathOfAlias('assets.front'));
$cs->registerCssFile($baseAssetsUrl . "/css/intlTelInput.css");
$cs->registerScriptFile($baseAssetsUrl . "/js/intlTelInput.js");

$userDiscountSaving = $cart->getUserTotalSaving();

$orderAttributes = $sessionOrderData[get_class($order)]['form_attributes'] ?? [];

$orderPositions = [];
foreach ($cart->getPositions() as $position) {
    $orderPositions[] = $position->event_id . '/' . $position->product->product_id;
}

$hasChoose = (count($availableDeliveries) > 1);

?>

<script type="application/javascript">
    Mamam.currency = '<?= $cf->getCurrency() ?>';
    Mamam.ecommerce.checkout(<?= json_encode($orderPositions) ?>, 3);
</script>

<div class="main main-cheque">
    <div class="promotional-time">
            <span class="style"><?= CHtml::tag(
                    'time',
                    ['datetime' => $tf->formatMachine($cart->getTotalReservedTo()), 'data-countdown' => true],
                    $tf->format($cart->getTotalReservedTo())
                ) ?>
            </span>
    </div>

    <?php $form = $this->beginWidget('CActiveForm', [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'beforeValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                var flagElement = $(".selected-flag")[0];
                
                if (flagElement === document.activeElement) {
                    return false;
                }
                
                return true;
            }',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                $(document).trigger({
                    type: "mommy.hideErrors",
                    delay: 0
                });
                    
                if (hasError) {
                    if ($(".error").hasClass("intl-tel-input")) {
                        var trueParent = $(".intl-tel-input").parent();
                        trueParent.addClass("error");             
                    }
                    
                    $(document).trigger("mommy.hideErrors");
                }
            }',
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
                    return true;
                }
                
                if ($(".error").hasClass("intl-tel-input")) {
                    var trueParent = $(".error.intl-tel-input").parent();
                    trueParent.addClass("error");
                }
                
                var getScrollTopId = form.attr("id");
                $.each(data, function (fieldId, val) {
                    getScrollTopId = fieldId;
                });
                if ($("#" + getScrollTopId).length > 0) {
                    $("html, body").animate({
                        scrollTop: $("#" + getScrollTopId).offset().top
                    }, 500);
                }
                
                $(document).trigger("mommy.hideErrors");
                
                return false;
            }',
        ],
        'focus' => [$order, PhoneNumber::NAME],
        'htmlOptions' => ['name' => 'delivery-form'],
    ]); /* @var $form CActiveForm */ ?>

    <div class="action-submenu action-about">
        <div class="standard-block checkbox-mob <?= $hasChoose ? '' : 'hide' ?>">
            <div class="standard-title">
                <label class="left-part"><?= $this->t('Choose a shipping method') ?></label>
            </div>
            <div class="standard-wrapper">
                <?php
                foreach ($availableDeliveries as $delivery) {
                    echo CHtml::tag(
                        'div',
                        ['class' => 'checkbox-field'],
                        $form->radioButton($order, 'deliveryType', [
                            'checked' => $delivery->getId() == $deliveryType,
                            'value' => $delivery->getId(),
                            'id' => 'deliveryType_' . $delivery->getId(),
                        ]) .
                        CHtml::label($this->t($delivery->getName()), 'deliveryType_' . $delivery->getId())
                    );
                }
                ?>
            </div>
        </div>

        <div class="standard-title">
            <label class="left-part"><?= $this->t('Contacts') ?></label>
        </div>
        <ul class="cheque">
            <li data-products-price="true">
                <div class="cheque-item">
                    <div class="cheque-left">
                        <span class="title"><?= $this->t('Name') ?>:</span>
                    </div>
                    <div class="cheque-right">
                        <span class="price"><?= ($orderAttributes['clientName'] ?? '') . ' ' . ($orderAttributes['clientSurname'] ?? '') ?></span>
                    </div>
                </div>
            </li>

            <li data-products-price="true">
                <div class="cheque-item">
                    <div class="cheque-left">
                        <span class="title"><?= $this->t('You phone number') ?>:</span>
                    </div>
                    <div class="cheque-right">
                        <span class="price"><?= $orderAttributes['telephone'] ?? '' ?></span>
                    </div>
                </div>
            </li>
        </ul>

        <br>

        <?php
            $this->renderPartial('_products', [
                'cart' => $cart,
            ]);
        ?>

        <ul class="cheque">
            <li data-products-price="true">
                <div class="cheque-item">
                    <div class="cheque-left">
                        <span class="title"><?= $this->t('Order price') ?></span>
                        <span class="undertitle">(<?= $cart->getOrdersReservedItemsCount() ?>
                            <?= Yii::t('delivery', 'thing', $cart->getOrdersReservedItemsCount()) ?>)</span>
                    </div>
                    <div class="cheque-right"><span class="price" data-value="<?= $cart->getCost(false) ?>"><?=
                            $cf->format($cart->getCost(false)) ?></span></div>
                </div>
            </li>
            <li data-delivery-price="true">
                <div class="cheque-item">
                    <div class="cheque-left">
                        <span class="title"><?= $this->t('Cost of delivery') ?></span>
                        <span class="undertitle"><?= $this->t('not selected') ?></span>
                    </div>
                    <div class="cheque-right hide-i">
                        <span class="price"><span data-value="0"></span> <?= $cf->format(0) ?></span>
                    </div>
                </div>
            </li>
            <li data-delivery-full="true" style="display: none">
                <div class="cheque-item">
                    <div class="cheque-left"><span class="title"><?= $this->t('Amount') ?></span></div>
                    <div class="cheque-right"><span class="price" data-value="0"><?= $cf->format(0) ?></span></div>
                </div>
            </li>

            <li data-user-discount="true">
                <div class="cheque-item">
                    <div class="cheque-left"><span class="title"><?= $this->t('A discount') ?></span></div>
                    <div class="cheque-right">
                        <span class="price bonus"
                              data-value="-<?= $userDiscountSaving ?>"><?= $cf->format($userDiscountSaving) ?></span>
                    </div>
                </div>
            </li>
            <li data-delivery-total="true">
                <div class="cheque-item">
                    <div class="cheque-left"><span class="title"><?= $this->t('Total') ?></span></div>
                    <div class="cheque-right"><span class="full-price" data-value="0"><?= $cf->format(0) ?></span></div>
                </div>
            </li>
        </ul>

        <?php
        foreach ($availableDeliveries as $delivery) {
            $deliveryModel = $deliveryModels[$delivery->getId()];
            $this->renderPartial('_deliveryService' . $deliveryModel->getModelName(), [
                'form' => $form,
                'model' => $deliveryModel,
                'rates' => $sessionOrderData[get_class($deliveryModel)]['special_data']['rates'] ?? [],
                'tabId' => 'deliveryType_' . $delivery->getId(),
                'cart' => $cart,
            ]);
        }
        ?>
    </div>

    <div class="cart-form-footer">
        <button class="btn green" type="submit"><?= $this->t('Checkout') ?>
        </button>
    </div>
    <?php $this->endWidget() ?>
</div>
<script>
    $(function () {
        if (Mamam.push) {
            Mamam.push.init();
        }

        var timer = null;

        function clearErrors() {
            $('.error .message').css('display', 'none');
            $('.error').removeClass('error');
        }

        $(document).on('mommy.hideErrors', function (e) {
            if (null !== timer) {
                clearTimeout(timer);
            }

            if (undefined === e.delay) {
                e.delay = 3500;
            }

            timer = setTimeout(clearErrors, e.delay);
        });
    });
</script>
