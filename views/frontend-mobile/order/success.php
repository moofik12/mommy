<?php

use MommyCom\Controller\FrontendMobile\OrderController;
use MommyCom\Model\Db\OrderRecord;
use MommyCom\Service\PaymentGateway\PaymentGatewayInterface;
use MommyCom\Service\PaymentGateway\PaymentMethodInterface;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\AuthManager\ShopWebUser;
use MommyCom\YiiComponent\ShopLogic\ChoosePaymentMethod;

/**
 * @var OrderController $this
 * @var ChoosePaymentMethod $model
 * @var OrderRecord $order
 * @var string $uid
 * @var bool $canPrepay
 * @var PaymentGatewayInterface[] $availableGateways
 * @var PaymentMethodInterface[] $availableMethods
 */

$this->pageTitle = $this->t('Success');

/** @var MommyWebApplication $app */
$app = $this->app();

$countRelationOrders = count($order->getRelationOrders());
$baseImgUrl = $app->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';

$linkToPay = $app->createUrl('pay/index', ['uid' => $order->id]);
$linkToPayOrders = $app->createUrl('pay/index', ['uid' => $order->uuid]);
/** @var $user ShopWebUser */
$user = $app->user;

$orderPositions = [];
foreach ($order->positions as $position) {
    $orderPositions[$position->event_id . '/' . $position->product->product_id] = [
        'id' => $orderPositions[] = $position->event_id . ' / ' . $position->product->product_id,
        'name' => $position->product->product->name,
        'brand' => $position->product->product->brand->name,
        'category' => $position->product->product->category,
        'variant' => $position->product->size,
        'price' => $position->price,
        'quantity' => $position->number,
    ];
}
?>
<script>
    Mamam.ecommerce.products.setCache(<?= json_encode($orderPositions, JSON_UNESCAPED_UNICODE) ?>);
    Mamam.ecommerce.purchase(<?= json_encode(array_keys($orderPositions)) ?>, <?= $order->id ?>);
    $(document).trigger($.Event('mamam.purchase.success', {
        totalPrice: <?= $order->getPrice() ?>,
        purchases: <?= json_encode($orderPositions) ?>
    }));
</script>

<script type="text/javascript">
    (function () {
        var mailruData = {
            productid: [],
            totalvalue: '<?= $order->getPrice() ?>'
        };

        <?php foreach($order->positions as $position): ?>
        mailruData.productid.push("<?= $position->product->product->id ?>");
        <?php endforeach ?>

        Mamam.mailru.send(Mamam.mailru.PAGE_LEVEL_ORDER_SUCCESS, mailruData);
    })();
</script>

<div class="main main-cheque">
    <?php $form = $this->beginWidget('CActiveForm', [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'beforeValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                var flagElement = $(".selected-flag")[0];
                
                if (flagElement === document.activeElement) {
                    return false;
                }
                
                return true;
            }',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                $(document).trigger({
                    type: "mommy.hideErrors",
                    delay: 0
                });
                    
                if (hasError) {
                    if ($(".error").hasClass("intl-tel-input")) {
                        var trueParent = $(".intl-tel-input").parent();
                        trueParent.addClass("error");             
                    }
                    
                    $(document).trigger("mommy.hideErrors");
                }
            }',
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
                    return true;
                }
                
                if ($(".error").hasClass("intl-tel-input")) {
                    var trueParent = $(".error.intl-tel-input").parent();
                    trueParent.addClass("error");
                }
                
                var getScrollTopId = form.attr("id");
                $.each(data, function (fieldId, val) {
                    getScrollTopId = fieldId;
                });
                if ($("#" + getScrollTopId).length > 0) {
                    $("html, body").animate({
                        scrollTop: $("#" + getScrollTopId).offset().top
                    }, 500);
                }
                
                $(document).trigger("mommy.hideErrors");
                
                return false;
            }',
        ],
        'htmlOptions' => ['name' => 'payment-form'],
    ]); /* @var $form CActiveForm */ ?>

    <div class="action-submenu action-about">

        <div class="standard-block <?= !$model->cashOnDelivery ? 'hide' : '' ?>" id="payments-type">
            <div class="standard-wrapper radio-list">
                <div class="left-part">
                    <?= $form->radioButtonList($model, 'cashOnDelivery', [
                        (string)false => $this->t('Prepay'),
                        (string)true => $this->t('Pay on delivery'),
                    ], [
                        'template' => '<div class="field bottom"><div class="radio-item">{input} {label}</div></div>',
                        'separator' => "</div>\n<div class=\"right-part\">",
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="standard-block <?= $model->cashOnDelivery ? 'hide' : '' ?>" id="payments-list">
            <div class="standard-wrapper">
                <div class="left-part left-part-width">
                    <div class="field textarea bottom">
                        <?php $this->widget('extSelect.widgets.ExtSelect', [
                            'model' => $model,
                            'attribute' => 'id',
                            'data' => [
                                $availableMethods, 'id', 'name',
                            ],
                            'settings' => [
                                'placeholder' => $this->t('Payment type'),
                                'clearSelection' => false,
                                'events' => [
                                    'select, setSelectedItem' => new CJavaScriptExpression('function(item) {

                                }'),
                                ],
                            ],
                        ]) ?>
                        <?= $form->error($model, 'id', ['class' => 'message']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cart-form-footer">
        <button class="btn green" type="submit"><?= $this->t('Checkout') ?></button>
    </div>
    <?php $this->endWidget() ?>
</div>
<script>
    $('[name=payment-form] [type=radio]').on('change', function (e) {
        if ($(this).val()) {
            $('#payments-list').addClass('hide');
        } else {
            $('#payments-list').removeClass('hide');
        }
    });
    $('[name="payment-form"]').on('submit', function (e) {
        e.preventDefault();

        if (!$('#payments-type').find('input:checked').val()) {
            return;
        }

        e.stopPropagation();

        $('#confirm_message').removeClass('hide-i');
        $('.main-cheque').addClass('hide-i');
        $('.back-container').addClass('hide-i');
    });
</script>

<div class="message-wrapper order-wrapper hide-i" id="confirm_message">
    <div class="message-inner">
        <div class="order-success-img"></div>
        <div class="message-title order-message">
            <?php if ($countRelationOrders == 0) : ?>
                <?= $this->t('Your order was successfully processed') ?>
            <?php else: ?>
                <?= $this->t('Your orders are successfully processed') ?>
            <?php endif; ?>
        </div>
        <div class="order-success-text">
            <?php if ($countRelationOrders == 0) : ?>
                <?= $this->t('Your order will be accepted for fulfilment after confirmation on the specified phone.') ?>
            <?php else: ?>
                <?= $this->t('Your orders will be accepted for execution after confirmation on the specified phone.') ?>
            <?php endif; ?>
        </div>
        <div class="paid-block">
            <?php
            foreach ($canPrepay ? $availableGateways : [] as $availableGateway) {
                echo $availableGateway->render($this, $uid);
            }
            ?>
        </div>
        <div class="message-footer order-success-footer">
            <a class="btn gray center-item"
               href="<?= $app->createUrl('account/orders'); ?>"><?= $this->t('My orders') ?></a>
        </div>
    </div>
</div>
