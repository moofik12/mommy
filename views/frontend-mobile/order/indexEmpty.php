<?php

use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopBonusPoints;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/* @var $this FrontController */
/* @var $cart ShopShoppingCart */
/* @var $bonuspoints ShopBonusPoints */
/* @var $returnUrl string */
/* @var $deliveryToken string */

$this->pageTitle = $this->t('List of products - Order');
$app = $this->app();
/* @var $splitTesting \MommyCom\Service\SplitTesting */
$splitTesting = $app->splitTesting;

$baseImgUrl = $this->app()->getAssetManager()->getPublishedUrl(Yii::getPathOfAlias('assets.markup')) . '/img/';
?>

<div class="cart cart-empty">
    <div class="cart-clear">
        <div class="cart-clear-img">
            <img width="222" height="115" src="<?= $baseImgUrl . 'cart-clear.svg' ?>"
                 alt="<?= $this->t('Empty shopping cart') ?>"/>
        </div>
        <p><?= $this->t('There\'s nothing here yet') ?></p>
        <p><?= $this->t('But it can be fixed!') ?></p>
    </div>
    <div class="cart-form-footer">
        <a class="btn back green" href="<?= $returnUrl ?>">
            <span><?= $this->t('Continue shopping') ?></span>
        </a>
    </div>
</div>
