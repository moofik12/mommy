<?php

use MommyCom\Controller\FrontendMobile\AuthController;
use MommyCom\Model\Db\UserRecord;
use MommyCom\Model\Form\AuthForm;
use MommyCom\Model\Form\RecoveryPasswordForm;

/**
 * @var $this AuthController
 * @var $authForm AuthForm
 * @var $recoveryForm RecoveryPasswordForm
 * @var $registrationForm UserRecord
 * @var $form CActiveForm
 * @var $tab string
 * @var $social string
 */

$app = $this->app();
$this->pageTitle = $this->t('Order checkout');

$orderPositions = [];
foreach ($app->user->cart->getPositions() as $position) {
    $orderPositions[] = $position->event_id . '/' . $position->product->product_id;
}

$isGuest = $app->user->isGuest;
?>

<script>
    Mamam.ecommerce.checkout(<?= json_encode($orderPositions) ?>, 1, function () {
        <?php if (!$isGuest): ?>
        window.location.replace('<?= $app->createUrl("order/delivery1") ?>');
        <?php endif; ?>
    });

    <?php if (!$isGuest): ?>
    setTimeout(function () {
        window.location.replace('<?= $app->createUrl("order/delivery1") ?>');
    }, 2000);
    <?php endif; ?>

</script>

<?php if ($app->user->hasFlash('message')): ?>
    <script>
        alert("<?= $app->user->getFlash('message')?>");
    </script>
<?php endif; ?>
<?php if (!$isGuest): ?>
    <div style="padding: 30px 0">
        <div class="big-loader"></div>
    </div>
<?php else: ?>
   <div class="login login-conteiner login-container-order">
        <div class="tabs">
            <div class="tab <?= $tab === 'login' ? 'active' : '' ?>" data-tab="login"><?= $this->t('Login') ?></div>
            <div class="tab <?= $tab === 'registration' ? 'active' : '' ?>"
                 data-tab="registration"><?= $this->t('check in') ?></div>
        </div>
        <div class="login-wrapper">
            <?php $this->renderPartial(
                '_logIn',
                [
                    'authForm' => $authForm,
                    'visible' => ('login' === $tab),
                ]
            ) ?>

            <?php $this->renderPartial(
                '_registration',
                [
                    'registrationForm' => $registrationForm,
                    'visible' => ('registration' === $tab),
                ]
            ) ?>

            <?php $this->renderPartial(
                '_forgot',
                [
                    'recoveryForm' => $recoveryForm,
                    'visible' => ('forgot' === $tab),
                ]
            ) ?>
        </div>
    </div>
<?php endif; ?>
