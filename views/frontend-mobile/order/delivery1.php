<?php

use MommyCom\Service\Delivery\DeliveryInterface;
use MommyCom\Service\Deprecated\CurrencyFormatter;
use MommyCom\YiiComponent\Application\MommyWebApplication;
use MommyCom\YiiComponent\FrontendMobile\FrontController;
use MommyCom\YiiComponent\ShopLogic\ShopOrder;
use MommyCom\YiiComponent\ShopLogic\ShopOrderAttribute\PhoneNumber;
use MommyCom\YiiComponent\ShopLogic\ShopShoppingCart;

/**
 * @var FrontController $this
 * @var ShopShoppingCart $cart
 * @var ShopOrder $order
 * @var int $deliveryType
 * @var DeliveryInterface[] $availableDeliveries
 * @var string $countryCode
 * @var array $deliveryModels
 */

$this->pageTitle = $this->t('Delivery method - Ordering');

/** @var MommyWebApplication $app */
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
/** @var CurrencyFormatter $cf */
$cf = $currencyFormatter = $app->currencyFormatter;

/** @var CClientScript $cs */
$cs = $this->app()->clientScript;
$baseAssetsUrl = $this->app()->getAssetManager()->publish(\Yii::getPathOfAlias('assets.front'));
$cs->registerCssFile($baseAssetsUrl . "/css/intlTelInput.css");
$cs->registerScriptFile($baseAssetsUrl . "/js/intlTelInput.js");

$orderPositions = [];
foreach ($cart->getPositions() as $position) {
    $orderPositions[] = $position->event_id . '/' . $position->product->product_id;
}

$hasChoose = (count($availableDeliveries) > 1);

?>

<script type="application/javascript">
    Mamam.currency = '<?= $cf->getCurrency() ?>';
    Mamam.ecommerce.checkout(<?= json_encode($orderPositions) ?>, 2);
</script>

<div class="main main-cheque">
    <div class="promotional-time">
            <span class="style"><?= CHtml::tag(
                    'time',
                    ['datetime' => $tf->formatMachine($cart->getTotalReservedTo()), 'data-countdown' => true],
                    $tf->format($cart->getTotalReservedTo())
                ) ?>
            </span>
    </div>
    <?php $form = $this->beginWidget('CActiveForm', [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'beforeValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                var flagElement = $(".selected-flag")[0];
                
                if (flagElement === document.activeElement) {
                    return false;
                }
                
                return true;
            }',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                $(document).trigger({
                    type: "mommy.hideErrors",
                    delay: 0
                });
                    
                if (hasError) {
                    if ($(".error").hasClass("intl-tel-input")) {
                        var trueParent = $(".intl-tel-input").parent();
                        trueParent.addClass("error");             
                    }
                    
                    $(document).trigger("mommy.hideErrors");
                }
            }',
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
                    return true;
                }
                
                if ($(".error").hasClass("intl-tel-input")) {
                    var trueParent = $(".error.intl-tel-input").parent();
                    trueParent.addClass("error");
                }
                
                var getScrollTopId = form.attr("id");
                $.each(data, function (fieldId, val) {
                    getScrollTopId = fieldId;
                });
                if ($("#" + getScrollTopId).length > 0) {
                    $("html, body").animate({
                        scrollTop: $("#" + getScrollTopId).offset().top
                    }, 500);
                }
                
                $(document).trigger("mommy.hideErrors");
                
                return false;
            }',
        ],
        'focus' => [$order, PhoneNumber::NAME],
        'htmlOptions' => ['name' => 'delivery-form'],
    ]); /* @var $form CActiveForm */ ?>

    <div class="standard-block checkbox-mob <?= $hasChoose ? '' : 'hide' ?>">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Choose a shipping method') ?></label>
        </div>
        <div class="standard-wrapper">
            <?php
            foreach ($availableDeliveries as $delivery) {
                echo CHtml::tag(
                    'div',
                    ['class' => 'checkbox-field'],
                    $form->radioButton($order, 'deliveryType', [
                        'checked' => $delivery->getId() == $deliveryType,
                        'value' => $delivery->getId(),
                        'id' => 'deliveryType_' . $delivery->getId(),
                    ]) .
                    CHtml::label($this->t($delivery->getName()), 'deliveryType_' . $delivery->getId())
                );
            }
            ?>
        </div>
    </div>

    <div class="standard-block border-top" data-block="info">
        <div class="standard-wrapper">
            <div class="standard-title">
                <label class="left-part"><?= $this->t('Your data') ?></label>
            </div>
            <div class="left-part">
                <div class="standard-title">
                    <?= $form->label($order, 'clientName') ?>
                </div>
                <div class="field bottom">
                    <?= $form->textField($order, 'clientName') ?>
                    <?= $form->error($order, 'clientName', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="standard-title">
                    <?= $form->label($order, 'clientSurname') ?>
                </div>
                <div class="field bottom">
                    <?= $form->textField($order, 'clientSurname') ?>
                    <?= $form->error($order, 'clientSurname', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
        <?php if (isset($order->{PhoneNumber::NAME})) : ?>
            <div id="phoneNumberBlock" class="standard-block border-top">
                <div class="standard-title">
                    <?= $form->label($order, PhoneNumber::NAME, [
                        'class' => 'left-part',
                        'label' => $this->t('You phone number'),
                    ]) ?>
                </div>
                <div class="standard-wrapper part-width">
                    <div class="left-part">
                        <div class="field bottom">
                            <?= $form->textField($order, PhoneNumber::NAME) ?>
                            <?= $form->error($order, PhoneNumber::NAME, ['class' => 'message']) ?>
                        </div>
                    </div>
                    <div class="right-part"></div>
                </div>
            </div>
        <?php endif; ?>

        <?php
        foreach ($availableDeliveries as $delivery) {
            $deliveryModel = $deliveryModels[$delivery->getId()];
            $this->renderPartial('_deliveryInfo' . $deliveryModel->getModelName(), [
                'form' => $form,
                'model' => $deliveryModel,
                'tabId' => 'deliveryType_' . $delivery->getId(),
                'cart' => $cart,
            ]);
        }
        ?>
    </div>
    <div class="cart-form-footer">
        <button class="btn green" type="submit"><?= $this->t('Continue') ?>
        </button>
    </div>
    <?php $this->endWidget() ?>
</div>
<script>
    $(function () {
        if (Mamam.push) {
            Mamam.push.init();
        }

        $('[name="delivery-form"]').on('submit', function (e) {
            var countryCode = $("#ShopOrder_telephone").intlTelInput("getSelectedCountryData").dialCode;
            $(this).append('<input type="hidden" name="ShopOrder[countryCode]" value="' + countryCode + '">');

            return true;
        });

        $('#ShopOrder_telephone').intlTelInput({
            autoPlaceholder: 'off',
            preferredCountries: [],
            initialCountry: '<?= $countryCode ?>',
            utilsScript: '<?= $baseAssetsUrl . "/js/intlTelInput.utils.js"?>'
        });

        var timer = null;

        function clearErrors() {
            $('.error .message').css('display', 'none');
            $('.error').removeClass('error');
        }

        $(document).on('mommy.hideErrors', function (e) {
            if (null !== timer) {
                clearTimeout(timer);
            }

            if (undefined === e.delay) {
                e.delay = 3500;
            }

            timer = setTimeout(clearErrors, e.delay);
        });
    });
</script>
