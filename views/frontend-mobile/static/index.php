<?php

use MommyCom\Model\Db\StaticPageCategoryRecord;
use MommyCom\Model\Db\StaticPageRecord;

/**
 * @var $this StaticController
 * @var $category StaticPageCategoryRecord
 * @var $page StaticPageRecord
 */

$this->pageTitle = $pages[0]->title;
$app = $this->app();
?>
<div class="main">
    <nav class="content-menu">
        <ul>
            <?php foreach ($category->pagesVisible as $item): ?>
                <li <?= $item->url == $pages[0]->url ? 'class="active"' : '' ?>>
                    <a href="<?= $app->createUrl('static/index', ['category' => $category->url, 'page' => $item->url]) ?>">
                        <?= $item->title ?>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    </nav>
    <div class="content-place">
        <article class="content">
            <div class="wrapper">
                <h2><?= $category->name ?></h2>
                <?php foreach ($pages as $page): ?>
                    <div class="wrapper">
                        <?= $page->body ?>
                    </div>
                <?php endforeach ?>
        </article>
    </div>
</div>
