<?php
/**
 * @author: Bondarev
 */
?>
<section>
    <header>
        <div class="page-title promotional">
            <h1 class="title"><?= $this->t('Baby clothes') ?></h1>
            <h2 class="size-table-title"><?= $this->t('Boys') ?></h2>
        </div>
    </header>
    <div class="container">
        <div class="content-body">
            <div class="size-content">
                <table id="size-baby-first" class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Age (years)') ?></td>
                        <td class="shaded">3</td>
                        <td class="shaded">4</td>
                        <td class="shaded">5</td>
                        <td class="shaded">6</td>
                        <td class="shaded">7</td>
                        <td class="shaded">8</td>
                        <td class="shaded">9</td>
                        <td class="shaded">10</td>
                        <td class="shaded">11</td>
                        <td class="shaded">12</td>
                        <td class="shaded">13</td>
                        <td class="shaded">14</td>
                        <td class="shaded">15</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Height') ?> (cm)</td>
                        <td class="shaded">98</td>
                        <td class="shaded">104</td>
                        <td class="shaded">110</td>
                        <td class="shaded">116</td>
                        <td class="shaded">122</td>
                        <td class="shaded">128</td>
                        <td class="shaded">134</td>
                        <td class="shaded">140</td>
                        <td class="shaded">146</td>
                        <td class="shaded">152</td>
                        <td class="shaded">156</td>
                        <td class="shaded">158</td>
                        <td class="shaded">164</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Chest circumference') ?> (cm)</td>
                        <td class="shaded">56</td>
                        <td class="shaded">56</td>
                        <td class="shaded">60</td>
                        <td class="shaded">60</td>
                        <td class="shaded">64</td>
                        <td class="shaded">64</td>
                        <td class="shaded">68</td>
                        <td class="shaded">68</td>
                        <td class="shaded">72</td>
                        <td class="shaded">72</td>
                        <td class="shaded">76</td>
                        <td class="shaded">80</td>
                        <td class="shaded">84</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded">1</td>
                        <td class="shaded">1</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">5</td>
                        <td class="shaded">5</td>
                        <td class="shaded">7</td>
                        <td class="shaded">7</td>
                        <td class="shaded">9</td>
                        <td class="shaded">9</td>
                        <td class="shaded">9</td>
                        <td class="shaded">9</td>
                        <td class="shaded">11</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded">3</td>
                        <td class="shaded">3</td>
                        <td class="shaded">4</td>
                        <td class="shaded">4</td>
                        <td class="shaded">6</td>
                        <td class="shaded">6</td>
                        <td class="shaded">8</td>
                        <td class="shaded">8</td>
                        <td class="shaded">10</td>
                        <td class="shaded">10</td>
                        <td class="shaded">12</td>
                        <td class="shaded">12</td>
                        <td class="shaded">12</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded">3T</td>
                        <td class="shaded">4T</td>
                        <td class="shaded">5-6</td>
                        <td class="shaded">6-6</td>
                        <td class="shaded">7</td>
                        <td class="shaded">7</td>
                        <td class="shaded">S</td>
                        <td class="shaded">S</td>
                        <td class="shaded">S/M</td>
                        <td class="shaded">M/L</td>
                        <td class="shaded">L</td>
                        <td class="shaded">L</td>
                        <td class="shaded">L</td>
                    </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h2 class="size-table-title"><?= $this->t('Girls') ?></h2>
                <table id="size-baby-second" class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Age (years)') ?></td>
                        <td class="shaded">3</td>
                        <td class="shaded">4</td>
                        <td class="shaded">5</td>
                        <td class="shaded">6</td>
                        <td class="shaded">7</td>
                        <td class="shaded">8</td>
                        <td class="shaded">9</td>
                        <td class="shaded">10</td>
                        <td class="shaded">11</td>
                        <td class="shaded">12</td>
                        <td class="shaded">13</td>
                        <td class="shaded">14</td>
                        <td class="shaded">15</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Height') ?> (cm)</td>
                        <td class="shaded">98</td>
                        <td class="shaded">104</td>
                        <td class="shaded">110</td>
                        <td class="shaded">116</td>
                        <td class="shaded">122</td>
                        <td class="shaded">128</td>
                        <td class="shaded">134</td>
                        <td class="shaded">140</td>
                        <td class="shaded">146</td>
                        <td class="shaded">152</td>
                        <td class="shaded">156</td>
                        <td class="shaded">158</td>
                        <td class="shaded">164</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Chest circumference') ?> (cm)</td>
                        <td class="shaded">56</td>
                        <td class="shaded">56</td>
                        <td class="shaded">60</td>
                        <td class="shaded">60</td>
                        <td class="shaded">64</td>
                        <td class="shaded">64</td>
                        <td class="shaded">68</td>
                        <td class="shaded">68</td>
                        <td class="shaded">72</td>
                        <td class="shaded">72</td>
                        <td class="shaded">76</td>
                        <td class="shaded">80</td>
                        <td class="shaded">84</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded">1</td>
                        <td class="shaded">1</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">5</td>
                        <td class="shaded">5</td>
                        <td class="shaded">7</td>
                        <td class="shaded">7</td>
                        <td class="shaded">9</td>
                        <td class="shaded">9</td>
                        <td class="shaded">9</td>
                        <td class="shaded">9</td>
                        <td class="shaded">11</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded">3</td>
                        <td class="shaded">3</td>
                        <td class="shaded">4</td>
                        <td class="shaded">4</td>
                        <td class="shaded">6</td>
                        <td class="shaded">6</td>
                        <td class="shaded">8</td>
                        <td class="shaded">8</td>
                        <td class="shaded">10</td>
                        <td class="shaded">10</td>
                        <td class="shaded">12</td>
                        <td class="shaded">12</td>
                        <td class="shaded">12</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded">3T</td>
                        <td class="shaded">4T</td>
                        <td class="shaded">5-6</td>
                        <td class="shaded">6-6</td>
                        <td class="shaded">7</td>
                        <td class="shaded">7</td>
                        <td class="shaded">S</td>
                        <td class="shaded">S</td>
                        <td class="shaded">S/M</td>
                        <td class="shaded">M/L</td>
                        <td class="shaded">L</td>
                        <td class="shaded">L</td>
                        <td class="shaded">L</td>
                    </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h2 class="size-table-title"><?= $this->t('Babies (up to 3 years)') ?></h2>
                <table id="size-baby-third" class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Age (months)') ?></td>
                        <td class="shaded">0-2</td>
                        <td class="shaded">3</td>
                        <td class="shaded">4</td>
                        <td class="shaded">6</td>
                        <td class="shaded">9</td>
                        <td class="shaded">12</td>
                        <td class="shaded">18</td>
                        <td class="shaded">24</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Height') ?> (cm)</td>
                        <td class="shaded">56</td>
                        <td class="shaded">58</td>
                        <td class="shaded">62</td>
                        <td class="shaded">68</td>
                        <td class="shaded">74</td>
                        <td class="shaded">80</td>
                        <td class="shaded">86</td>
                        <td class="shaded">92</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Chest circumference') ?> (cm)</td>
                        <td class="shaded">36</td>
                        <td class="shaded">38</td>
                        <td class="shaded">40</td>
                        <td class="shaded">44</td>
                        <td class="shaded">46</td>
                        <td class="shaded">48</td>
                        <td class="shaded">50</td>
                        <td class="shaded">52</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded">56</td>
                        <td class="shaded">58</td>
                        <td class="shaded">62</td>
                        <td class="shaded">68</td>
                        <td class="shaded">74</td>
                        <td class="shaded">80</td>
                        <td class="shaded">86</td>
                        <td class="shaded">92</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded">0/3</td>
                        <td class="shaded">0/3</td>
                        <td class="shaded">3/6</td>
                        <td class="shaded">3/6</td>
                        <td class="shaded">6/9</td>
                        <td class="shaded">S/M</td>
                        <td class="shaded">2-2T</td>
                        <td class="shaded">2-2T</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">2</td>
                        <td class="shaded">3</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

