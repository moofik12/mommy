<?php
/**
 * @author: Bondarev
 */
?>
<section>
    <header>
        <div class="page-title promotional">
            <h1 class="size-title"><?= $this->t('Women\'s clothing') ?></h1>
            <h2 class="size-table-title"><?= $this->t('Blouses, tunics, jackets, dresses') ?></h2>
        </div>
    </header>
    <div class="container">
        <div class="content-body">
            <div class="size-content">
                <table id="size-woman-first" class="size-table">
                    <tfoot>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded color">32</td>
                        <td class="shaded color">34</td>
                        <td class="shaded color">36</td>
                        <td class="shaded color">38</td>
                        <td class="shaded color">40</td>
                        <td class="shaded color">42</td>
                        <td class="shaded color">44</td>
                        <td class="shaded color">46</td>
                        <td class="shaded color">48</td>
                        <td class="shaded color">50</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded color">0</td>
                        <td class="shaded color">2</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">14</td>
                        <td class="shaded color">16</td>
                        <td class="shaded color">18</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">14</td>
                        <td class="shaded color">16</td>
                        <td class="shaded color">18</td>
                        <td class="shaded color">20</td>
                        <td class="shaded color">22</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('International Size') ?></td>
                        <td class="shaded color">XSS/&zwnj;XS</td>
                        <td class="shaded color">XS/&zwnj;S</td>
                        <td class="shaded color">S</td>
                        <td class="shaded color">M</td>
                        <td class="shaded color">L</td>
                        <td class="shaded color">L/XL</td>
                        <td class="shaded color">XL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXXL</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Chest size') ?></td>
                        <td class="shaded">78-82</td>
                        <td class="shaded">82-86</td>
                        <td class="shaded">86-90</td>
                        <td class="shaded">90-94</td>
                        <td class="shaded">94-98</td>
                        <td class="shaded">98-102</td>
                        <td class="shaded">102-106</td>
                        <td class="shaded">106-110</td>
                        <td class="shaded">110-114</td>
                        <td class="shaded">114-118</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Waist') ?></td>
                        <td class="shaded">58-62</td>
                        <td class="shaded">62-66</td>
                        <td class="shaded">66-70</td>
                        <td class="shaded">70-74</td>
                        <td class="shaded">74-78</td>
                        <td class="shaded">78-82</td>
                        <td class="shaded">82-86</td>
                        <td class="shaded">86-90</td>
                        <td class="shaded">90-94</td>
                        <td class="shaded">94-98</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Thigh size') ?></td>
                        <td class="shaded">84-88</td>
                        <td class="shaded">88-92</td>
                        <td class="shaded">92-96</td>
                        <td class="shaded">96-100</td>
                        <td class="shaded">100-104</td>
                        <td class="shaded">104-108</td>
                        <td class="shaded">108-112</td>
                        <td class="shaded">112-116</td>
                        <td class="shaded">116-120</td>
                        <td class="shaded">120-124</td>
                    </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h2 class="size-table-title"><?= $this->t('Pants, skirts, shorts, jeans') ?></h2>
                <table id="size-woman-second" class="size-table">
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Waist') ?></td>
                        <td class="shaded">58-62</td>
                        <td class="shaded">62-66</td>
                        <td class="shaded">66-70</td>
                        <td class="shaded">70-74</td>
                        <td class="shaded">74-78</td>
                        <td class="shaded">78-82</td>
                        <td class="shaded">82-86</td>
                        <td class="shaded">86-90</td>
                        <td class="shaded">90-94</td>
                        <td class="shaded">94-98</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Thigh size') ?></td>
                        <td class="shaded">84-88</td>
                        <td class="shaded">88-92</td>
                        <td class="shaded">92-96</td>
                        <td class="shaded">96-100</td>
                        <td class="shaded">100-104</td>
                        <td class="shaded">104-108</td>
                        <td class="shaded">108-112</td>
                        <td class="shaded">112-116</td>
                        <td class="shaded">116-120</td>
                        <td class="shaded">120-124</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded color">32</td>
                        <td class="shaded color">34</td>
                        <td class="shaded color">36</td>
                        <td class="shaded color">38</td>
                        <td class="shaded color">40</td>
                        <td class="shaded color">42</td>
                        <td class="shaded color">44</td>
                        <td class="shaded color">46</td>
                        <td class="shaded color">48</td>
                        <td class="shaded color">50</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US)</td>
                        <td class="shaded color">0</td>
                        <td class="shaded color">2</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">14</td>
                        <td class="shaded color">16</td>
                        <td class="shaded color">18</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('England') ?> (UK)</td>
                        <td class="shaded color">4</td>
                        <td class="shaded color">6</td>
                        <td class="shaded color">8</td>
                        <td class="shaded color">10</td>
                        <td class="shaded color">12</td>
                        <td class="shaded color">14</td>
                        <td class="shaded color">16</td>
                        <td class="shaded color">18</td>
                        <td class="shaded color">20</td>
                        <td class="shaded color">22</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('International Size') ?></td>
                        <td class="shaded color">XSS/&zwnj;XS</td>
                        <td class="shaded color">XS/&zwnj;S</td>
                        <td class="shaded color">S</td>
                        <td class="shaded color">M</td>
                        <td class="shaded color">L</td>
                        <td class="shaded color">L/XL</td>
                        <td class="shaded color">XL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXXL</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
