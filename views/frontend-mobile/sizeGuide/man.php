<?php
/**
 * @author: Bondarev
 */
?>
<section>
    <header>
        <div class="page-title promotional">
            <h1 class="title"><?= $this->t('Men\'s Clothing') ?></h1>
            <h2 class="size-table-title"><?= $this->t('Jackets, sweaters, vests, bathrobes, shirts') ?></h2>
        </div>
    </header>
    <div class="container">
        <div class="content-body">
            <div class="size-content">

                <table id="size-men-first" class="size-table">
                    <tfoot>
                    <tr>
                        <td class="size title"><?= $this->t('Size') ?></td>
                        <td class="shaded color">44</td>
                        <td class="shaded color">46</td>
                        <td class="shaded color">48</td>
                        <td class="shaded color">50</td>
                        <td class="shaded color">52</td>
                        <td class="shaded color">54</td>
                        <td class="shaded color">56</td>
                        <td class="shaded color">58</td>
                        <td class="shaded color">60</td>
                        <td class="shaded color">62</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('International Size') ?></td>
                        <td class="shaded color">XS/&zwnj;S</td>
                        <td class="shaded color">S</td>
                        <td class="shaded color">M</td>
                        <td class="shaded color">L</td>
                        <td class="shaded color">L/XL</td>
                        <td class="shaded color">XL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXXL</td>
                        <td class="shaded color">XXXL</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Chest size') ?></td>
                        <td class="shaded">86-90</td>
                        <td class="shaded">90-94</td>
                        <td class="shaded">94-98</td>
                        <td class="shaded">98-102</td>
                        <td class="shaded">102-106</td>
                        <td class="shaded">106-110</td>
                        <td class="shaded">110-114</td>
                        <td class="shaded">114-118</td>
                        <td class="shaded">118-122</td>
                        <td class="shaded">122-126</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Waist') ?></td>
                        <td class="shaded">74-78</td>
                        <td class="shaded">78-82</td>
                        <td class="shaded">82-86</td>
                        <td class="shaded">86-90</td>
                        <td class="shaded">92-94</td>
                        <td class="shaded">96-100</td>
                        <td class="shaded">106-110</td>
                        <td class="shaded">110-114</td>
                        <td class="shaded">114-118</td>
                        <td class="shaded">118-122</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Thigh size') ?></td>
                        <td class="shaded">94-97</td>
                        <td class="shaded">97-100</td>
                        <td class="shaded">100-103</td>
                        <td class="shaded">103-106</td>
                        <td class="shaded">106-109</td>
                        <td class="shaded">109-112</td>
                        <td class="shaded">112-115</td>
                        <td class="shaded">115-118</td>
                        <td class="shaded">118-121</td>
                        <td class="shaded">121-124</td>
                    </tr>
                    </tbody>
                </table>
                <br/><br/>
                <h2 class="size-table-title"><?= $this->t('Pants, shorts, jeans') ?></h2>
                <table id="size-men-second" class="size-table">
                    <tfoot>
                    <tr>
                        <td class="size title"><?= $this->t('Europe') ?> (EU)</td>
                        <td class="shaded color">44</td>
                        <td class="shaded color">46</td>
                        <td class="shaded color">48</td>
                        <td class="shaded color">50</td>
                        <td class="shaded color">52</td>
                        <td class="shaded color">54</td>
                        <td class="shaded color">56</td>
                        <td class="shaded color">58</td>
                        <td class="shaded color">60</td>
                        <td class="shaded color">62</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('USA') ?> (US) /
                            <?= $this->t('England') ?> (UK)
                        </td>
                        <td class="shaded color">34</td>
                        <td class="shaded color">36</td>
                        <td class="shaded color">38</td>
                        <td class="shaded color">40</td>
                        <td class="shaded color">42</td>
                        <td class="shaded color">44</td>
                        <td class="shaded color">46</td>
                        <td class="shaded color">48</td>
                        <td class="shaded color">50</td>
                        <td class="shaded color">52</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('International Size') ?></td>
                        <td class="shaded color">XS/&zwnj;S</td>
                        <td class="shaded color">S</td>
                        <td class="shaded color">M</td>
                        <td class="shaded color">L</td>
                        <td class="shaded color">L/XL</td>
                        <td class="shaded color">XL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXL</td>
                        <td class="shaded color">XXXL</td>
                        <td class="shaded color">XXXL</td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <td class="size title"><?= $this->t('Waist') ?></td>
                        <td class="shaded">74-78</td>
                        <td class="shaded">78-82</td>
                        <td class="shaded">82-86</td>
                        <td class="shaded">86-90</td>
                        <td class="shaded">92-94</td>
                        <td class="shaded">96-100</td>
                        <td class="shaded">106-110</td>
                        <td class="shaded">110-114</td>
                        <td class="shaded">114-118</td>
                        <td class="shaded">118-122</td>
                    </tr>
                    <tr>
                        <td class="size title"><?= $this->t('Thigh size') ?></td>
                        <td class="shaded">94-97</td>
                        <td class="shaded">97-100</td>
                        <td class="shaded">100-103</td>
                        <td class="shaded">103-106</td>
                        <td class="shaded">106-109</td>
                        <td class="shaded">109-112</td>
                        <td class="shaded">112-115</td>
                        <td class="shaded">115-118</td>
                        <td class="shaded">118-121</td>
                        <td class="shaded">121-124</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
