<?php

use MommyCom\Model\Db\UserDistributionRecord;
use MommyCom\Model\Form\UserSettingsForm;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $form UserSettingsForm */
/* @var $actionForm CActiveForm */

$app = $this->app();
$this->pageTitle = $this->t('account settings');
?>
<div class="action-submenu action-settings">
    <?php $actionForm = $this->beginWidget('CActiveForm', [
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
        ],
    ]) ?>
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Change Password'); ?></label>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field <?= $form->hasErrors('password') ? 'error' : '' ?> top">
                    <?= $actionForm->passwordField($form, 'password', ['placeholder' => $this->t('Current password')]) ?>
                    <?= $actionForm->error($form, 'password', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field <?= $form->hasErrors('newPassword') ? 'error' : '' ?> top">
                    <?= $actionForm->passwordField($form, 'newPassword', ['placeholder' => $this->t('New password')]) ?>
                    <?= $actionForm->error($form, 'newPassword', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field <?= $form->hasErrors('confirmPassword') ? 'error' : '' ?> top">
                    <?= $actionForm->passwordField($form, 'confirmPassword', ['placeholder' => $this->t('Confirm New Password')]) ?>
                    <?= $actionForm->error($form, 'confirmPassword', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Newsletter'); ?></label>
        </div>
        <div class="standard-wrapper">
            <div class="checkbox-field checkbox-distribute">
                <?= $actionForm->checkBox($form, 'isWishPromoMail', [
                    'id' => 'promo-mailing',
                    'data-confirm' => $this->t('You will not be able to receive new personal offers! Are you sure?'),
                ]) ?>
                <label for="promo-mailing"><span><?= $this->t('Promotional Newsletter'); ?></span></label>
            </div>
        </div>
        <div class="standard-wrapper radio-list">
            <?= $actionForm->radioButtonList($form, 'distribution', UserDistributionRecord::typeReplacements(), [
                'template' => '<span class="radio-item">{input} {label}</span>',
                'separator' => '',
                'container' => 'span',
            ]) ?>
        </div>
    </div>

    <div class="standard-block">
        <div class="standard-wrapper">
            <?= CHtml::submitButton($this->t('Save'), ['class' => 'btn red']) ?>
            <?= CHtml::resetButton($this->t('Reset changes'), ['class' => 'back']) ?>
        </div>
        <div class="standard-wrapper">
            <a class="btn back exit"
               href="<?= $app->createUrl('auth/logout') ?>"><?= $this->t('Sign out of your account'); ?><i
                        class="icon-exit"></i></a>
        </div>
    </div>
    <?php $this->endWidget() ?>
</div>

<?php
$this->app()->clientScript->registerScript('confirmPromoMailChange', '
        $("#promo-mailing").on("click", function(e) {
            var $this = $(this);
            if ( !$this.prop("checked") ) {
                var confirmText = $this.data("confirm") ||' . $this->t('Are you sure?') . ';
                return confirm(confirmText);
            }
        });
    ', CClientScript::POS_END);
?>
