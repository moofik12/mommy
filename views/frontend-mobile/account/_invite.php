<?php

use MommyCom\Controller\FrontendMobile\AccountController;
use MommyCom\Model\Db\InviteRecord;
use MommyCom\Model\Db\UserRecord;

/**
 * @var InviteRecord[] $invitations
 * @var $this AccountController
 * @var $form CActiveForm
 * @var $model UserRecord
 * @var $invite InviteRecord
 */

$this->pageTitle = $this->t('Invitations');

$app = $this->app();
$df = $dateFormatter = $app->dateFormatter;
$tf = $timerFormatter = $app->timerFormatter;

$statusCssClass = [
    InviteRecord::STATUS_WAITING => 'awaiting',
    InviteRecord::STATUS_ACTIVATED => 'purchase',
    InviteRecord::STATUS_PURCHASE => 'activated',
];

?>

<div class="action-submenu action-invite">
    <table class="account-table">
        <thead>
        <tr>
            <th><?= $this->t('Dispatch date'); ?></th>
            <th>E-mail</th>
            <th class="status"><?= $this->t('Status'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($provider->getData() as $item): /* @var $item InviteRecord */ ?>
            <tr>
                <td><span class="date"><?= CHtml::tag(
                            'time',
                            ['datetime' => $tf->formatMachine($item->created_at)],
                            $df->format('d MMMM yyyy', $item->created_at)
                        ) ?></span></td>
                <td><span class="invite-mail"><?= CHtml::encode($item->email) ?></span></td>
                <td><span class="<?= CHtml::value($statusCssClass, $item->status) ?>">
                    <?= CHtml::value($item->statusReplacement(), $item->status) ?>
                </span></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
    <div class="append-block">
        <?php $form = $this->beginWidget('CActiveForm', [
            'id' => 'invite-email-form',
            'enableClientValidation' => false,
            'action' => ['account/inviteByEmail'],
        ]) ?>
        <label><?= $this->t('Status'); ?></label>
        <div class="input-append button">
            <div class="field">
                <?= $form->textField($invite, 'email',
                    ['placeholder' => $this->t('Friend\'s email address'), 'autocomplete' => 'off']) ?>
            </div>
            <div class="append-label"><?= $this->t('Write your friend\'s email address to whom we will send an invitation on your behalf'); ?></div>
            <?= CHtml::submitButton($this->t('Send')) ?>
        </div>
        <?php $this->endWidget() ?>
    </div>
</div>
