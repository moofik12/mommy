<?php

use MommyCom\Model\Form\UserAccountForm;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $model UserAccountForm */

$app = $this->app();
$this->pageTitle = $this->t('Personal data');
?>
<div class="action-submenu action-about">
    <?php $form = $this->beginWidget('CActiveForm', [
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => [
            'validateOnSubmit' => true,
        ],
    ]) ?>
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Your name') ?></label>
            <label class="right-part"><?= $this->t('Your surname') ?></label>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field <?= $model->hasErrors('name') ? 'error' : '' ?> top">
                    <?= $form->textField($model, 'name') ?>
                    <?= $form->error($model, 'name', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field <?= $model->hasErrors('surname') ? 'error' : '' ?> top">
                    <?= $form->textField($model, 'surname') ?>
                    <?= $form->error($model, 'surname', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part">E-mail</label>
            <label class="right-part"><?= $this->t('Phone') ?></label>
        </div>
        <div class="standard-wrapper">
            <div class="left-part">
                <div class="field <?= $model->hasErrors('email') ? 'error' : '' ?> top">
                    <?= $form->textField($model, 'email') ?>
                    <?= $form->error($model, 'email', ['class' => 'message']) ?>
                </div>
            </div>
            <div class="right-part">
                <div class="field <?= $model->hasErrors('telephone') ? 'error' : '' ?> top">
                    <?= $form->textField($model, 'telephone') ?>
                    <?= $form->error($model, 'telephone', ['class' => 'message']) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-title">
            <label class="left-part"><?= $this->t('Address') ?></label>
        </div>
        <div class="standard-wrapper">
            <div class="field textarea <?= $model->hasErrors('address') ? 'error' : '' ?> top">
                <?= $form->textArea($model, 'address') ?>
                <?= $form->error($model, 'address', ['class' => 'message']) ?>
            </div>
        </div>
    </div>
    <div class="standard-block">
        <div class="standard-wrapper">
            <?= CHtml::submitButton($this->t('Save'), ['class' => 'btn red']) ?>
            <?= CHtml::resetButton($this->t('Reset changes'), ['class' => 'back']) ?>
        </div>
    </div>
    <?php $this->endWidget() ?>
</div>
