<?php

use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */

$app = $this->app();
$this->pageTitle = $this->t('Offers');
?>
<div class="action-submenu action-bonus">
    <span class="label"><?= $this->t('Current number of bonuses:') ?></span>
    <span class="bonus"><?= $totalBonuses ?></span>
</div>
