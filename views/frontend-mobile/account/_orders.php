<?php

use MommyCom\Model\Db\OrderRecord;
use MommyCom\YiiComponent\FrontendMobile\FrontController;

/* @var $this FrontController */
/* @var $provider CDataProvider */

$this->pageTitle = $this->t('My orders');
$app = $this->app();

$tf = $timerFormatter = $app->timerFormatter;
$nf = $numberFormatter = $app->numberFormatter;
$df = $dateFormatter = $app->dateFormatter;
$cf = $currencyFormatter = $app->currencyFormatter;
$cn = $this->app()->countries->getCurrency();
?>
<div class="action-submenu action-orders">
    <div class="clearfix">
        <?php foreach ($provider->getData() as $item): /* @var $item OrderRecord */ ?>
            <div class="orders-item">
                <div class="head-line clearfix">
                    <time>
                        <?= CHtml::tag('time', ['datetime' => $tf->formatMachine($item->ordered_at)],
                            $df->format('d MMMM yyyy', $item->ordered_at)
                        ) ?>
                    </time>
                    <p>
                        <?php if ($item->processing_status == OrderRecord::PROCESSING_UNMODERATED): ?>
                            <span class="orders-status">
                                <i class="status-icon processed"></i>
                                <span class="status-text"><?= $this->t('Wait for the call'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_RECALL): ?>
                            <span class="orders-status">
                                <i class="status-icon processed"></i>
                                <span class="status-text"><?= $this->t('Recall'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_CALL_LATER): ?>
                            <span class="orders-status">
                                <i class="status-icon processed"></i>
                                <span class="status-text"><?= $this->t('Recall'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_PREPAY): ?>
                            <span class="orders-status">
                                <i class="status-icon confirmed"></i>
                                <span class="status-text"><?= $this->t('Waiting for payment'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_CONFIRMED): ?>
                            <span class="orders-status">
                                <i class="status-icon confirmed"></i>
                                <span class="status-text"><?= $this->t('Confirmed, expect delivery'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_AUTOMATIC_CONFIRMED): ?>
                            <span class="orders-status">
                                <i class="status-icon confirmed"></i>
                                <span class="status-text"><?= $this->t('Confirmed, expect delivery'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_STOREKEEPER_PACKAGED): ?>
                            <span class="orders-status">
                                <i class="status-icon posted"></i>
                                <span class="status-text"><?= $this->t('Packed, will soon ship'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_STOREKEEPER_MAILED): ?>
                            <span class="orders-status green">
                                <i class="status-icon posted"></i>
                                <span class="status-text"><?= $this->t('Delivered'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CANCELLED
                            || $item->processing_status == OrderRecord::PROCESSING_CANCELLED_MAILED_OVER_SITE): ?>
                            <span class="orders-status">
                                <i class="status-icon canceled"></i>
                                <span class="status-text"><?= $this->t('Canceled'); ?></span>
                            </span>
                        <?php elseif ($item->processing_status == OrderRecord::PROCESSING_CALLCENTER_NOT_ANSWER): ?>
                            <span class="orders-status red">
                                <i class="status-icon canceled"></i>
                                <span class="status-text"><?= $this->t('Unverified and canceled'); ?></span>
                            </span>
                        <?php endif ?>
                    </p>
                </div>
                <div class="item-body">
                    <div>
                        <div class="number">
                            <span class="label"><?= $this->t('Order number'); ?></span>
                            <p>
                                <?= $item->idAligned ?>
                                <?php if ($item->is_drop_shipping): ?>
                                    <a class="icon-fast-delivery"
                                       href="<?= $app->createUrl('static/index', ['category' => 'service', 'page' => 'fast-delivery', '#' => 'definition']) ?>"
                                       title="
                                    <?= $this->t('What is \'Fast Delievery\'?') ?>"></a>
                                <?php endif; ?>
                            </p>
                        </div>
                        <div class="price">
                            <span class="label"><?= $this->t('Amount') ?></span>
                            <p><?= $cf->format($item->getPrice(false, true), $cn->getName('short')) ?></p>
                        </div>
                        <?php if ($item->isAvailableForPayment()) : ?>
                            <div class="payment">
                                <a class="btn green"
                                   href="<?= $this->app()->createUrl('pay/index', ['uid' => $item->id]) ?>">
                                    <?= $this->t('Pay now'); ?>
                                </a>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
