<?php
/**
 * @var \MommyCom\Service\BaseController\Controller $this
 * @var string $errorMessage
 */

$this->pageTitle = $this->t('Error');;
$app = $this->app();
?>
<div class="main-container not-found page full-width">
    <div class="container">
        <div class="main">
            <div class="not-found-page">
                <div class="not-fount-subtitle">
                    <p class="lead"><?= $this->t($errorMessage) ?></p>
                </div>
                <a class="btn red" href="<?= $app->createUrl('index/index') ?>"><?= $this->t('Back to the store') ?></a>
            </div>
        </div>
    </div>
</div>
