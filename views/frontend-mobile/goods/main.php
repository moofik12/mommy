<?php

use MommyCom\Controller\FrontendMobile\GoodsController;

/**
 * @var GoodsController $this
 */

$this->pageTitle = $this->t('Goods');
?>
<nav>
    <?php
    $this->widget('zii.widgets.CMenu', [
        'items' => $this->menu,
        'htmlOptions' => ['class' => 'product-menu'],
        'encodeLabel' => false,
        'submenuHtmlOptions' => ['class' => $class],
    ]);
    ?>
</nav>
