<?php
/**
 * @var bool $isSystemSubscribe
 * @var bool $isDistributionSubscribe
 */

$this->pageTitle = Yii::t('common' . 'Unsubscribe from newsletters');
$app = $this->app();
?>

<div class="message-wrapper">
    <div class="message-inner">
        <div class="message-title"><?= $this->t('You unsubscribed from notifications') ?></div>
        <div class="line"></div>
        <?php if ($isDistributionSubscribe): ?>
            <div class="message-text">
                <?= $this->t('Хотите отписаться от промо рассылок? Можете сделать в любой момент на странице "My account" -> "Настройки"') ?>
                , <?= CHtml::link($this->t('unsubscribe'), ['account/settings', '#' => 'mailing']) ?>
            </div>
        <?php endif ?>
        <div class="message-footer">
            <a class="btn green" href="<?= $app->createUrl('index/index') ?>"><?= $this->t('Home') ?></a>
        </div>
    </div>
</div>
