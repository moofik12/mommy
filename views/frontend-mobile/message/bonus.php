<?php

use MommyCom\Controller\FrontendMobile\MessageController;

/**
 * @var MessageController $this
 * @var string $title
 * @var string $message
 * @var string $description
 */

$this->pageTitle = $title;
$app = $this->app();
?>

<div class="message-wrapper">
    <div class="message-inner">
        <div class="message-title"><?= $message ?></div>
        <?php if ($description) : ?>
            <div class="line"></div>
            <div class="message-text"><?= $description ?></div>
        <?php endif ?>
        <div class="message-footer">
            <a class="btn green left"
               href="<?= $app->createUrl('account/bonuses') ?>"><?= $this->t('My offers') ?></a>
            <a class="btn red right" href="<?= $app->createUrl('index/index') ?>"><?= $this->t('Home') ?></a>
        </div>
    </div>
</div>
