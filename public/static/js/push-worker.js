self.addEventListener('push', function (event) {
    function sendNotification(title, body, url) {
        var notificationOptions = {
            body: body,
            icon: '../icon-192.png',
            badge: '../icon-72.png', //72px
            tag: 'mamam-notification',
            data: {
                url: url
            }
        };
        return self.registration.showNotification(title, notificationOptions)
    }

    event.waitUntil(
        self.registration.pushManager.getSubscription().then(function (subscription) {
            if (!subscription) {
                return;
            }

            return fetch('../../?r=application/webMessage&endpoint=' + encodeURIComponent(subscription.endpoint)).then(function (response) {
                if (response.status !== 200) {
                    console.warn('error get message for user');
                    return;
                }

                // Examine the text in the response
                return response.json().then(function (data) {
                    if (data.subject && data.body) {
                        return sendNotification(data.subject, data.body, data.url);
                    }
                });
            }).catch(function () {
                console.warn('error fetch message for user');
            });
        })
    );
});

self.addEventListener('notificationclick', function (event) {
    var clickResponsePromise = Promise.resolve();
    if (event.notification.data && event.notification.data.url) {
        clickResponsePromise = clients.openWindow(event.notification.data.url);
    }

    event.waitUntil(
        clickResponsePromise
    );

    event.notification.close();
});
