<?php

/**
 * Class Yii
 *
 * Альтернативная реализация класса Yii для переопределения части статических методов
 */
class Yii extends YiiBase
{
    /**
     * @deprecated все работает через Composer
     *
     * @param string $className
     * @param bool $classMapOnly
     *
     * @return bool
     */
    public static function autoload($className, $classMapOnly = false)
    {
        return false;
    }
}
