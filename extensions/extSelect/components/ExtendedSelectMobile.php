<?php

class ExtendedSelectMobile extends CApplicationComponent
{
    const CORE_JS_NAME = 'select.js';
    const CORE_CSS_NAME = 'select.css';

    public $coreCss = true;
    public $coreJs = true;

    protected $_assetsUrl;

    public function init()
    {
        if (!Yii::getPathOfAlias('extSelect')) {
            Yii::setPathOfAlias('extSelect', realpath(dirname(__FILE__) . '/..'));
        }

        $package = [
            'basePath' => 'extSelect.assets',
        ];

        if ($this->coreCss) {
            $package['css'] = [
                'css/' . self::CORE_CSS_NAME,
            ];
        }

        if ($this->coreJs) {
            $package['js'] = [
                'js/' . self::CORE_JS_NAME,
            ];
        }

        Yii::app()->clientScript->addPackage('_extSelect', $package);
        Yii::app()->clientScript->registerPackage('_extSelect');
    }

}
