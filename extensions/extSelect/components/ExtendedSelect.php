<?php

class ExtendedSelect extends CApplicationComponent
{
    const CORE_JS_NAME = 'select.js';
    const CORE_CSS_NAME = 'select.css';

    public $coreCss = true;
    public $coreJs = true;

    protected $_assetsUrl;

    public function init()
    {
        if (!Yii::getPathOfAlias('extSelect')) {
            Yii::setPathOfAlias('extSelect', realpath(dirname(__FILE__) . '/..'));
        }

        if ($this->coreCss) {
            $this->registerCoreCssFile();
        }

        if ($this->coreJs) {
            $this->registerCoreScriptFile();
        }
    }

    /**
     * Регистрирует css нужный компоненту
     */
    protected function registerCoreCssFile()
    {
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/' . self::CORE_CSS_NAME);
    }

    /**
     * Регистрирует javascript нужный компоненту
     */
    protected function registerCoreScriptFile($position = CClientScript::POS_HEAD)
    {
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/js/' . self::CORE_JS_NAME, $position);
    }

    /**
     * Возвращает URL к бубличной папке с ресурсами.
     *
     * @return string the URL
     */
    protected function getAssetsUrl()
    {
        if ($this->_assetsUrl !== null) {
            return $this->_assetsUrl;
        } else {
            $assetsPath = Yii::getPathOfAlias('extSelect.assets');

            if (YII_DEBUG) {
                $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, true);
            } else {
                $assetsUrl = Yii::app()->assetManager->publish($assetsPath);
            }

            return $this->_assetsUrl = $assetsUrl;
        }
    }

}
