/*!
 * author scriptbunny
 */
(function(window, $, _) {
	"use strict";
	
	var global = window;
	
	var el = document.createElement('input');
	var inputEventSupported = 'oninput' in el;
	el = null;
	
	var Key = { // from select2, Copyright 2012 Igor Vaynberg, license at http://www.apache.org/licenses/LICENSE-2.0
		Tab: 9,
		Spase: 32,
		Enter: 13,
		Esc: 27,
		Left: 37,
		Up: 38,
		Right: 39,
		Down: 40,
		Shift: 16,
		Ctrl: 17,
		Alt: 18,
		
		isArrow: function (k) {
			k = k.which? k.which : k;
			switch (k) {
				case Key.Left:
				case Key.Right:
				case Key.Up:
				case Key.Down:
					return true;
			}
			return false;
		},
		
		isControl: function (k) {
			k = k.which ? k.which : k;
			switch (k) {
				case Key.Shift:
				case Key.Ctrl:
				case Key.Alt:
					return true;
			}
			return false;
		}
	};
	
	// -------------------- IDEAL INPUT WIDTH ------------------
	function measureInputWidth(input, minWidth, maxWidth) {
		minWidth || (minWidth = 0);
		maxWidth || (maxWidth = 0);
		
		var $element = $(input);
		var css = {
			left: -9999,
			top: -9999,
			position: 'absolute',
			lineHeight: $element.css('lineHeight'),
			textDecoration: $element.css('textDecoration'),
			whiteSpace: 'pre',
			textIndent: $element.css('textIndent'),
			boxSizing: $element.css('boxSizing'),
			padding: $element.css('padding'),
			wordWrap: $element.css('wordWrap'),
			fontSize: $element.css('fontSize'),
			fontWeight: $element.css('fontWeight'),
			fontFamily: $element.css('fontFamily'),
			letterSpacing: $element.css('letterSpacing'),
			wordSpacing: $element.css('wordSpacing')
		};
		var $sizeContainer = $('<div/>').insertAfter($element).css(css).attr('data-sizecontainer', 1);
		$sizeContainer.html($element.val() + ' ');
		var width = $sizeContainer.width();
		$sizeContainer.remove();
		
		return (width > minWidth? (width < maxWidth? width: maxWidth) : minWidth);
	}
	
	/**
	 * @param {String} sentence
	 */
	function hyphenateSentence(sentence) {
		return ((sentence || '').match(/.{1,4}/g) || []).join('&shy');
	}
	
	// ------------------- HASH UTILS --------------------------
	
	var HashUtils = {
		/**
		 * @param {Object} hash
		 * @param {String} key
		 * @param {Integer} offset (optional)
		 * @return {Integer} -1 if key not exists or index + offset is out of bounds
		 */
		keyIndex: function(hash, key, offset) {
			offset || (offset = 0);
			
			var keys = _.keys(hash),
				index = _.indexOf(keys, key);
			
			if (index !== -1) {
				index += offset;
			}
			
			return index >= 0 && index < keys.length? index: -1;
		},

		/**
		 * @param {Object} hash
		 * @param {Integer} index
		 * @param {Integer} offset (optional)
		 * @return {String|null} null if index of bounds
		 */
		indexKey: function(hash, index, offset) {
			var keys = _.keys(hash);
			index += offset || 0;
			return index >= 0 && index < keys.length? keys[index]: null;
		},
		
		/**
		 * @param {Object} hash
		 * @return {String}
		 */
		firstKey: function(hash) {
			return _.keys(hash)[0];
		},
		
		/**
		 * @param {Object} hash
		 * @return {String}
		 */
		lastKey: function(hash) {
			var keys = _.keys(hash);
			return keys[keys.length - 1];
		},
		
		/**
		 * @param {Object} hash
		 * @param {String} key
		 * @return {String}
		 */
		nextKey: function(hash, key) {
			var keys = _.keys(hash),
				index = _.indexOf(keys, key);
			
			return index >= 0 && index < keys.length - 1? keys[index + 1]: -1;
		},
		
		/**
		 * @param {Object} hash
		 * @param {String} key
		 * @return {String}
		 */
		prevKey: function(hash, key) {
			var keys = _.keys(hash),
				index = _.indexOf(keys, key);
			
			return index > 0 && index < keys.length? keys[index - 1]: -1;
		},
		
		/**
		 * @param {Object} hash
		 * @return {Mixed}
		 */
		first: function(hash) {
			return hash[this.firstKey(hash)];
		},
		
		/**
		 * @param {Object} hash
		 * @return {Mixed}
		 */
		last: function(hash) {
			return hash[this.lastKey(hash)];
		},
		
		/**
		 * @param {Object} hash
		 * @param {Integer} key
		 * @param {Integer} offset (optional)
		 * @return {Mixed}
		 */
		valueByIndex: function(hash, key, offset) {
			return hash[this.indexKey(hash, key, offset)];
		}
	};
	
	// ------------------- BaseObject --------------------------
	var BaseObject = function() {};
	BaseObject.prototype.initialize = function() {};
	
	var extend = function(SuperClass) {
		var constructor = function () {
			var context = this, initializers = [], args = _.toArray(arguments);
			
			do {
				if (!!context.initialize) { // collect initializers
					initializers.push(context.initialize);
				}
			} while(!!(context = context.parent));
			
			// reverse the initializers
			_.each(initializers.reverse(), function(initialize) {
				initialize.apply(this, args);
			}, this);
		};
		constructor.prototype = _.clone(SuperClass.prototype);
		constructor.prototype.constructor = constructor;
		constructor.prototype.parent = SuperClass.prototype;
		
		// remove copied initializer
		constructor.prototype.initialize && (delete constructor.prototype.initialize);
		
		var mixins = _.toArray(arguments).slice(1).reverse();
		_.each(mixins, function(mixin) {
			_.extend(constructor.prototype, mixin);
		});
		
		return constructor;
	};
	
	
	// -------------------- EVENTS MIXIN --------------------
	
	/**
	 *  Copy from backbone source
	 *  -------------------------------
     *  (c) 2010-2012 Jeremy Ashkenas, DocumentCloud Inc.
     *  Backbone may be freely distributed under the MIT license.
     *  For all details and documentation:
     *  http://backbonejs.org
	 */
	var eventSplitter = /\s+/;
	var slice = Array.prototype.slice;
	var splice = Array.prototype.splice;
	var EventsMixin = {
		// Bind one or more space separated events, `events`, to a `callback`
		// function. Passing `"all"` will bind the callback to all events fired.
		on: function(events, callback, context) {

			var calls, event, node, tail, list;
			if (!callback) return this;
			events = events.split(eventSplitter);
			calls = this._callbacks || (this._callbacks = {});

			// Create an immutable callback list, allowing traversal during
			// modification.  The tail is an empty object that will always be used
			// as the next node.
			while (event = events.shift()) {
				list = calls[event];
				node = list ? list.tail : {};
				node.next = tail = {};
				node.context = context;
				node.callback = callback;
				calls[event] = {
					tail: tail, 
					next: list? list.next : node
				};
			}

			return this;
		},

		// Remove one or many callbacks. If `context` is null, removes all callbacks
		// with that function. If `callback` is null, removes all callbacks for the
		// event. If `events` is null, removes all bound callbacks for all events.
		off: function(events, callback, context) {
			var event, calls, node, tail, cb, ctx;

			// No events, or removing *all* events.
			if (!(calls = this._callbacks)) return this;
			if (!(events || callback || context)) {
				delete this._callbacks;
				return this;
			}

			// Loop through the listed events and contexts, splicing them out of the
			// linked list of callbacks if appropriate.
			events = events ? events.split(eventSplitter) : _.keys(calls);
			while (event = events.shift()) {
				node = calls[event];
				delete calls[event];
				if (!node || !(callback || context)) continue;
				// Create a new list, omitting the indicated callbacks.
				tail = node.tail;
				while ((node = node.next) !== tail) {
					cb = node.callback;
					ctx = node.context;
					if ((callback && cb !== callback) || (context && ctx !== context)) {
						this.on(event, cb, ctx);
					}
				}
			}

			return this;
		},

		// Trigger one or many events, firing all bound callbacks. Callbacks are
		// passed the same arguments as `trigger` is, apart from the event name
		// (unless you're listening on `"all"`, which will cause your callback to
		// receive the true name of the event as the first argument).
		trigger: function(events) {
			var event, node, calls, tail, args, all, rest;
			if (!(calls = this._callbacks)) return this;
			all = calls.all;
			events = events.split(eventSplitter);
			rest = slice.call(arguments, 1);

			// For each event, walk through the linked list of callbacks twice,
			// first to trigger the event, then to trigger any `"all"` callbacks.
			while (event = events.shift()) {
				if (node = calls[event]) {
					tail = node.tail;
					while ((node = node.next) !== tail) {
						node.callback.apply(node.context || this, rest);
					}
				}
				if (node = all) {
					tail = node.tail;
					args = [event].concat(rest);
					while ((node = node.next) !== tail) {
						node.callback.apply(node.context || this, args);
					}
				}
			}
			return this;
		}
	};
	
	// Aliases for backwards compatibility.
	EventsMixin.bind   = EventsMixin.on;
	EventsMixin.unbind = EventsMixin.off;
	
	// ------------------- DATA PROVIDER -----------------------
	
	// DataItem
	var DataItem = {
		_typePrefix: {
			prompt: 'a',
			data: 'b',
			empty: 'c'
		},
		
		initialize: function(item) {
			this.val = '';
			this.text = '';
			this.html = '';
			this.group = '';
			this.storage = null;
			this.selected = false;
			
			this.isPrompt =  false;
			this.isEmpty = false;
			this.isSeparator = false;
			
			if (_.isString(item)) {
				if (item === '---') {
					item = {
						isSeparator: true
					};
				} else {
					item = {text: item};
				}
			}
			
			this.assignFrom(item || {});
			this.updatedAt = 0;
		},
		
		assignFrom: function(item) {
			this._id = item.id || null;
			this.val = item.val !== undefined && item.val !== null? item.val: '';
			this.text = item.text !== undefined && item.text !== null? item.text: '';
			this.html = item.html !== undefined && item.text !== null? item.html: '';
			this.group = item.group !== undefined && item.group !== null? item.group: '';
			this.pillState = item.pillState || '';
			this.storage = item.storage !== undefined? item.storage: '';
			this.selected = item.selected || false;
			this.isPrompt = item.isPrompt || false;
			this.isEmpty = item.isEmpty || false;
			this.isSeparator = item.isSeparator || false;
		
			if (this.filterable !== undefined) {
				this.filterable = !!item.filterable;
			}
			
			this.markAsUpdated();
		},
		
		_getIdPrefix: function() {
			return this.isPrompt? this._typePrefix.prompt: (this.isEmpty? this._typePrefix.empty: this._typePrefix.data);
		},
		
		isFilterable: function() {
			return this.filterable === undefined? !(this.isPrompt || this.isEmpty || this.isSeparator): this.filterable;
		},
		
		
		/**
		 * Id of this item
		 * @return {String}
		 */
		getId: function() {
			var prefix = this.isPrompt? this._typePrefix.prompt: (this.isEmpty? this._typePrefix.empty: this._typePrefix.data);
			return prefix + (this._id || (this._id = _.uniqueId('iid')));
		},
		
		/**
		 * Отмечает что элемент обновился
		 */
		markAsUpdated: function() {
			this.updatedAt = +new Date;
		},
		
		/**
		 * Обновлялось ли содержимое элемента с определенного времени
		 * @param {Integer|Date} time
		 * @return {Boolean}
		 */
		isUpdated: function(time) {
			return this.updatedAt >= +new Date(time);
		},
		
		/**
		 * Возвращает текст который будет рендериться в выпадающий список
		 * @param {Boolean} hyphenate (optional, default true)
		 * @return {String}
		 */
		getDropdownContent: function(hyphenate) {
			typeof hyphenate === 'undefined' && (hyphenate = true);
			
			var content = this.html || this.text || this.val || '';
			if (hyphenate && content.indexOf('<') === -1) { // no html and support hyphenation
				return hyphenateSentence(content);
			}
			return content;
		},
		
		/**
		 * Возвращает данные которые будут вставляться в значение выпадающего списка
		 * @return {String}
		 */
		getVal: function() {
			return this.val || '';
		},
		
		/**
		 * Возвращает текст который будет вставляться в значение инпута
		 * @param {Boolean} supportHtml (optional, default false)
		 * @param {Boolean} hyphenate (optional, default true)
		 * @return {String}
		 */
		getSelectedText: function(supportHtml, hyphenate) {
			typeof hyphenate === 'undefined' && (hyphenate = true);
			
			var content;
			if (!!supportHtml) {
				content = this.text || this.html;
				if (content.indexOf('<') === -1) { // no html
					content = hyphenateSentence(content);
				}
			} else {
				content = this.text || _.escape(this.html) || this.val;
			}
			return content;
		}
	};
	
	DataItem = extend(BaseObject, DataItem);
	// DataItem End
	
	var DataProviderAbstract = {
		_defaultSettings: {
			prompt: null,
			empty: null
		},
		
		initialize: function(data, settings) {
			this._select = null;
			
			this.settings = _.extend(_.clone(this._defaultSettings), settings || {});
			
			this._prompt = {};
			this._empty = {};
			
			var prompt = this.settings.prompt, empty = this.settings.empty;
			
			if (!!prompt) {
				if (_.isString(prompt) || !!prompt.text || !!prompt.val || !!prompt.isSeparator) {
					prompt = [prompt];
				}
				_.each(prompt, function(prompt) {
					var item = new DataItem(prompt);
					item.isPrompt = true;
					this._prompt[item.getId()] = item;
				}, this);
			}
			
			if (!!empty) {
				if (_.isString(empty) || !!empty.text || !!empty.val || !!empty.isSeparator) {
					prompt = [empty];
				}
				_.each(empty, function(empty) {
					var item = new DataItem(empty);
					item.isEmpty = true;
					this._empty[item.getId()] = item;
				}, this);
			}
		},
		
		_applyPromptAndEmptyItems: function(data) {
			var newData = {};
			
			if (!!this._prompt) {
				_.each(this._prompt, function(prompt) {
					newData[prompt.getId()] = prompt;
				});
			}
			
			_.each(data, function(item, key) {
				newData[key] = item;
			});
			
			if (!!this._empty) {
				_.each(this._empty, function(empty) {
					newData[empty.getId()] = empty;
				});
			}
			
			return newData;
		},
		
		isUpdated: function(time) {
			return true;
		},
		
		setSelect: function(value) {
			this._select = value;
		},
		
		getSelect: function() {
			return this._select;
		},
		
		assignFrom: function(data) {
			throw new Exception('Abstract method');
		},
		
		getData: function(filter, callback, context) {
			throw new Exception('Abstract method');
		},
		
		getItem: function(id) {
			throw new Exception('Abstract method');
		}
	};
	
	DataProviderAbstract = extend(BaseObject, DataProviderAbstract, EventsMixin);
	
	var ArrayDataProvider = {
		initialize: function (data, settings) {
			this._realSize = 0;
			this._size = 0;
			
			this._data = {};
			this.setData(data);
		},
		
		setData: function(data) {
			var newData = {};
			
			this._realSize = 0; // size WITHOUT additions like prompt and empty
			this._size = 0; // size WITH addtions like prompt and empty
			
			_.each(data || {}, function(value) {
				var item = new DataItem(value), id = item.getId();
				if (value.selected === undefined) { // если нам никто явно не сказал выбран ли элемент, пытаемся определить
					item.selected || (item.selected = ((this._data[id] || {}).selected || false));
				}
				newData[id] = item;
				this._realSize++;
			}, this);
			
			
			this._data = this._applyPromptAndEmptyItems(newData);
		
			this._size = _.size(this._data);
			
			this.trigger('setData', this._data, this._size, this._realSize);
		},
	
		getData: function(filter, callback, context) {
			_.defer(_.bind(function() { // асинхронно вызываем колбэк
				var data = _.clone(this._data);
				
				var size = this.size, realSize = this._realSize;
				
				if (!!filter) {
					var prevItem = null;
					var filterFunction = _.isString(filter)? function(item) {
						filter = filter.toLowerCase();
						return item.getDropdownContent().toLowerCase().indexOf(filter) !== -1 ||
							item.group.toLowerCase().indexOf(filter) !== -1;
					}: filter;
					
					_.map(data, function(item, key) {
						if (item.isFilterable()) {
							if (!filterFunction.call(context, item, key)) {
								delete data[key];
								size--;
								realSize--;
							}
						} else if (prevItem !== null && item.isSeparator && prevItem.isSeparator) {
							delete data[key];
							//fix here
							size--;
							realSize--;
						}
						prevItem = item;
					});
				}
				
				var lastItem = HashUtils.last(data);
				
				if (!!lastItem && lastItem.isSeparator) {
					delete data[lastItem.getId()];
					size--;
					if (!lastItem.isPrompt && !lastItem.isEmpty) {
						realSize--;
					}
				}
				
				callback.call(context, data, size, realSize);
				this.trigger('getData', data, size, realSize);
			}, this));
		},
		
		/**
		 * Возвращает элемент по id
		 * @param {String} id
		 * @param {Function} callback
		 * @param {Object} context
		 */
		getItem: function(id, callback, context) {
			_.defer(_.bind(function() { // асинхронно вызываем колбэк
				var item = this._data[id];
				callback.call(context, item);
				this.trigger('getItem', item);
			}, this));
		}
	};
	
	ArrayDataProvider = extend(DataProviderAbstract, ArrayDataProvider);
	
	var AjaxDataProvider = {
		_defaultSettings: {
			ajax: {
				cache: false,
				dataType: 'json',
				results: function(response) {
					return response;
				},
				error: function(xhr, textStatus, errorThrown) {
					if (!!xhr.getAllResponseHeaders()) { // not by xhr.abort
						global.console && global.console.log('Ajax fail. Status: "' + textStatus + '". Error: "' + errorThrown + '"');
					}
				}
			}
		},
		
		initialize: function(data, settings) {
			this._ajaxHandle = null;
		},
		
		getAjaxHandle: function() {
			return this._ajaxHandle;
		},
		
		isAjaxInProcess: function() {
			return !!this._ajaxHandle;
		},
		
		stopAjax: function() {
			if (this.isAjaxInProcess()) {
				this._ajaxHandle.abort();
				this._ajaxHandle = null;
			}
		},
		
		reload: function(filter, callback, context) {
			this.stopAjax();
			var settings = $.extend(true, {}, this._defaultSettings.ajax, this.settings.ajax || {}, {
				success: _.bind(function(response) {
					if (this.settings.ajax.success) {
						this.settings.ajax.success.call(this, response);
					}
					
					var data = settings.results.call(this, response);
					this._ajaxHandle = null;
					this.setData(data, false); // no dont stop current request
					
					callback && callback.call(context, data, filter);
				}, this)
			});
			
			if (_.isFunction(settings.data)) {
				settings.data = settings.data.call(this, filter);
			}
			
			this._ajaxHandle = $.ajax(settings);
		},
		
		setData: function(data, stopAjax) {
			if (stopAjax === undefined || stopAjax === true) {
				this.stopAjax();
			}
			return ArrayDataProvider.prototype.setData.apply(this, arguments);
		},
		
		getData: function(filter, callback, context) {
			var args = arguments;
			if (this.isAjaxInProcess()) {
				this.getAjaxHandle().done(_.bind(function() {
					ArrayDataProvider.prototype.getData.apply(this, args);
				}, this, args));
			} else if (this._prevFilter != filter) {
				this._prevFilter = filter;
				this.reload(filter, function() {
					// filter applied as null cause he is used in ajax request
					ArrayDataProvider.prototype.getData.call(this, null, callback, context); 
				}, this);
			} else {
				// filter applied as null cause he is used in ajax request
				ArrayDataProvider.prototype.getData.call(this, null, callback, context);
			}
		},
		
		getItem: function(filter, callback, context) {
			var args = arguments;
			if (this.isAjaxInProcess()) {
				this.getAjaxHandle().done(_.bind(function() {
					ArrayDataProvider.prototype.getItem.apply(this, args);
				}, this, arguments));
			} else {
				ArrayDataProvider.prototype.getItem.apply(this, args);
			}
		}
	};
	
	AjaxDataProvider = extend(ArrayDataProvider, AjaxDataProvider);
	
	var BackboneDataProvider = {
		_defaultSettings: {
			source: {
				model: null,
				results: function(response) {
					return response;
				}
			}
		},
		
		reload: function(filter, callback, context) {
			this.stopAjax();
			var settings = $.extend(true, {}, this._defaultSettings.source, this.settings.source || {}, {
				success: _.bind(function(collection) {
					if (this.settings.source.success) {
						this.settings.source.success.apply(this, arguments);
					}
					var data = settings.results.apply(this, arguments);
					
					this._ajaxHandle = null;
					this.setData(data, false); // no dont stop current request
					
					callback && callback.call(context, data, filter);
				}, this)
			});
			
			delete settings.model;
			var model = new this.settings.source.model;
			
			var forbiddenProps = ['success', 'fail', 'error', 'results'];
			_.each(settings, function(value, key) {
				if (_.indexOf(forbiddenProps, key) === -1 && _.isFunction(value)) {
					settings[key] = value.call(this, filter);
				}
			});
			
			this._ajaxHandle = model.fetch(settings);
		}
	};
	
	BackboneDataProvider = extend(AjaxDataProvider, BackboneDataProvider);
	
	var EnosoDataProvider = {
		reload: function(filter, callback, context) {
			if (_.isString(this.settings.source.model)) {
				if (typeof(Enoso) === 'undefined') { // Enoso App is currently not loaded
					var args = arguments;
					var hInterval = setInterval(_.bind(function() { // wait for loading
						if (typeof(Enoso) !== 'undefined') {
							clearInterval(hInterval);
							this.reload.apply(this, args); // restore reload process
						}
					}, this), 300);
					return; // exit;
				}
				
				// load model
				Enoso.require([this.settings.source.model], _.bind(function(model) {
					this.settings.source.model = model;
					BackboneDataProvider.prototype.reload.call(this, filter, callback, context); // proxy to backbone data provider
				}, this));
			} else {
				BackboneDataProvider.prototype.reload.call(this, filter, callback, context);
			}
		}
	};
	
	EnosoDataProvider = extend(BackboneDataProvider, EnosoDataProvider);
	
	// ------------------- SELECT ------------------------------
	var Dropdown = {
		_defaultSettings: {
			model: Dropdown,
			cssClassPrefix: 'extdropdown-',
			search: true,
			multipleSelection: false,
			hightlightMatches: true,
			floating: false,
			header: '',
			footer: '',
			showHeader: 'auto',
			showFooter: 'auto'
		},
		
		_template: _.template(
			'<div class="<%= containerCssClass %>" style="display: none">' + 
			'<div class="<%= wrapFootCssClass%>">' +
			'<div class="<%= wrapHeadCssClass %>">' +
			'<div class="<%= headerCssClass %>" <% if (!isHeaderVisible) { %> style="display: none" <% } %> ><%= header %></div>' +
			'<div class="<%= searcherCssClass %>" <% if (!isSearcherVisible) { %> style="display: none" <% } %> >&nbsp;<input autocomplete="off" type="text"></div>' +
			'<ul class="<%= itemsCssClass %>"/>' +
			'</div>' +
			'<div class="<%= footerCssClass %>" <% if (!isFooterVisible) { %> style="display: none" <% } %> ><%= footer %></div>' +
			'</div>' +
			'</div>'
		),
		
		_itemTemplate: _.template(
			'<li class="<%= itemCssClass %>" data-itemid="<%= item.getId() %>">' +
			'<%= item.getDropdownContent() %>' +
			'</li>'
		),

		_groupTemplate: _.template(
			'<li class="<%= groupCssClass %>">' + 
			'<div class="<%= groupLabelCssClass %>"><%= groupLabel %></div>' + 
			'<ul class="<%= groupItemsCssClass %>"><%= items %></ul>' + 
			'</li>'
		),
			
		_getScrollWidth: _.once(function() { // этот метод может быть закеширован
			var t = document.createElement('textarea');
			t.rows = t.cols = 1;
			t.style.visibility = 'hidden';
			t.style.border = 'none';
			document.body.appendChild(t);
			var w = t.offsetWidth - t.clientWidth;
			document.body.removeChild(t);
			return w;
		}),
		
		/**
		 * @param {Object|String} key key or object of item
		 * @return {Object}
		 */
		_getItem: function(key) {
			return this._controls.items[
				_.isObject(key)? 
					key instanceof $? key.data('itemid'): key.getId(): // is jQueryObject or DataItem
					key // is Id
			];
		},
		
		/**
		 * Возвращает цсс-класс с префиксом
		 * @param {String|Array|Object} value базовый css-класс или их массив
		 * @return {String}
		 */
		_getCssClass: function(value) {
			if (_.isArray(value) || _.isObject(value)) {
				return _.reduce(value, function(memo, item) {
					return memo + (item !== ''? ' ' + this.settings.cssClassPrefix + item: '');
				}, '', this);
			} else {
				return this.settings.cssClassPrefix + value;
			}
		},
		
		_createContainer: function() {
			var html = this._template({
				'containerCssClass': this._getCssClass(['container', 'state-hasheader', 'state-hasfooter', 'state-hassearcher']),
				'wrapFootCssClass': this._getCssClass('wrapfoot'),
				'wrapHeadCssClass': this._getCssClass('wraphead'),
				'headerCssClass': this._getCssClass('header'),
				'searcherCssClass': this._getCssClass('searcher'),
				'footerCssClass': this._getCssClass('footer'),
				'itemsCssClass': this._getCssClass('items'),
				'header': this.settings.header,
				'footer': this.settings.footer,
				
				
				'isSearcherVisible': this.settings.search,
				'isHeaderVisible': !!this.settings.header,
				'isFooterVisible': !!this.settings.footer
			});
			
			this._controls.container = $(html).appendTo(this.getSelect().getControl('dropdownWrapper')); //дропдаун помещается в специальный отсек у селекта
			this._controls.itemsContainer = this._controls.container.find('.' + this._getCssClass('items'));
			this._controls.header = this._controls.container.find('.' + this._getCssClass('header'));
			this._controls.footer = this._controls.container.find('.' + this._getCssClass('footer'));
			this._controls.seacher = this._controls.container.find('.' + this._getCssClass('searcher'));
			this._controls.seacherInput = this._controls.seacher.find('input');
		},
		
		
		_bindEvents: function() {
			var self = this;
			var itemClass = '.' + this._getCssClass('item');
			
			this._controls.itemsContainer.delegate(itemClass + ':not(.' + this._getCssClass('state-isseparator') + ')', 'mouseenter', function() {
				self.hoveredItem($(this));
			});
			
			this._controls.itemsContainer.delegate(itemClass, 'click', function() {
				var $this = $(this);
				self.trigger('click:item', $this[0], $this.data('item'));
			});
			
			this._controls.seacherInput.bind(inputEventSupported? 'input change': 'change keyup', _.debounce(function() {
				self.reload(function() {
					self.trigger('change:search', this, self.filterText());
				});
			}, 300));
			
			this._controls.itemsContainer.on('mousewheel', function(e, d) {
				var scrollHeight = this.scrollHeight, height = self._controls.itemsContainer.height();
				
				if((this.scrollTop === (scrollHeight - height) && d < 0) || (this.scrollTop === 0 && d > 0)) {
					e.preventDefault();
				}
			});
			
			
			var needReload = true, data = null;
			this.on('open:dropdown', function() {
				if (needReload) {
					needReload = false;
					if (data !== null) {
						var self = this;
						_.defer(function() {
							self._setItems(data);
						});
					} else {
						this.getDataProvider().getData(this.filterText(), this._setItems, this);
					}
				}
			}, this);
			
			this.getDataProvider().on('setData', function(newData) {
				needReload = true;
				data = newData;
				if (this.isOpened()) { // force render 
					needReload = false;
					this._setItems(newData);
				}
			}, this);
		},
		
		_getFilter: function() {
			return this._controls.seacherInput.val().trim() || '';
		},
		
		initialize: function(select, settings) {
			this._select = select;
			this._controls = {
				container: null,
				search: null,
				seacherInput: null,
				itemsContainer: null,
				header: null,
				footer: null,
				items: {} // is array of itemsContainer children
			};
			
			this.settings = $.extend({}, this._defaultSettings, settings || {});
			this._createContainer();
			this._bindEvents();
			this._opened = false; // debug only
		},
		
		/**
		 * Возвращает субконтрол
		 * @param {String} name
		 * @return {jQuery}
		 */
		getControl: function(name) {
			return this._controls[name];
		},
		
		setSelect: function(value) {
			this._select = value;
			
		},
		
		getSelect: function() {
			return this._select;
		},
		
		getDataProvider: function() {
			return this._select.getDataProvider();
		},
		
		/**
		 * Открывает дропдаун
		 */
		open: function() {
			if (!this._opened) {
				this._opened = true;
				this._controls.container.show();
				this.trigger('open:dropdown', this._controls.container);
				_.defer(_.bind(function() { // стараемся не блочить ui
					if (this._opened) { // все еще открыт
						this._controls.container.scrollTop(this._controls.container.data('prev-scrolltop') || 0);
						this._controls.itemsContainer.scrollTop(this._controls.itemsContainer.data('prev-scrolltop') || 0);
						
						if ((this._controls.seacherInput.css('display') || '') !== 'none') {
							//this._controls.seacherInput.focus();
						}
						this.updateSize(); // обновляем размер бокса
					}
				}, this));
			}
		},
		
		/**
		 * Скрывает дропдаун
		 */
		close: function() {
			if (this._opened) {
				this._opened = false;
				this._controls.container.data('prev-scrolltop', this._controls.container.scrollTop());
				this._controls.itemsContainer.data('prev-scrolltop', this._controls.itemsContainer.scrollTop());
				this.trigger('close:dropdown', this._controls.container);
				this._controls.container.hide();
			}
		},
		
		/**
		 * Открыт ли дропдаун
		 * @return {Boolean}
		 */
		isOpened: function() {
			return this._opened;
		},
		
		_setItems: function(data) {
			var itemCssClass = this._getCssClass('item');
			var selectedItems = [];
			
			var grouped = _.groupBy(_.clone(data), function(item) {
				return item.group;
			});
			//hyphenateSentence
			var itemsHtml = _.map(grouped, function(data, label) {
				var itemsGroupHtml = _.map(data, function(item) {
					var cssClass = itemCssClass;
					if (item.isEmpty || item.isPrompt || item.isSeparator) {
						cssClass += ' ' + this._getCssClass([
							item.isEmpty? 'state-isempty': '',
							item.isPrompt? 'state-isprompt': '',
							item.isSeparator? 'state-isseparator': ''
						]);
					}
					return this._itemTemplate({
						itemCssClass: cssClass,
						item: item
					});
				}, this).join('');
				if ((label || '') !== '') {
					itemsGroupHtml = this._groupTemplate({
						groupCssClass: this._getCssClass('itemgroup'),
						groupLabelCssClass: this._getCssClass('grouplabel'),
						groupItemsCssClass: this._getCssClass('group'),
						groupLabel: hyphenateSentence(label),
						items: itemsGroupHtml
					});
				}
				return itemsGroupHtml;
			}, this);
			this._controls.itemsContainer.html(itemsHtml.join(''));
			this._controls.items = {};

			var self = this;
			this._controls.itemsContainer.find('.' + itemCssClass).each(function() {
				var $this = $(this), itemid = $this.data('itemid'), itemData = data[itemid];
				$this.data('item', itemData);
				self._controls.items[itemid] = $this;

				if (itemData.selected) {
					selectedItems.push($this);
				}
			});

			this._controls.itemsContainer.data('selectedItems', null);
			self.selectedItems(null);

			if (selectedItems.length > 0) {
				this.selectedItems(selectedItems);
			}
		},
		
		/**
		 * перезагружает данные из провайдера
		 * @param {function} callback
		 * @param {Object} context
		 */
		reload: function(callback, context) {
			this._deferedReload = this._deferedReload || _.debounce(_.bind(function(callback, context) { // стараемся не блочить ui
				this.getDataProvider().getData(this.filterText(), function(data) {
					this._setItems(data);
					callback && callback.apply(context, data); // вызываем колбэк
				}, this);
			}, this), 150); // отложена на 150 мсек
			
			this._deferedReload(callback, context); // в певый раз нужно вызвать вручную
			return this;
		},
		
		filterText: function(value) {
			if (value !== undefined) {
				this._controls.seacherInput.val(value).trigger('change');
				return value;
			}
			return this._controls.seacherInput.val();
		},
		
		updateSize: function() {
			return this;
		},
		
		minWidth: function(value) {
			return this._controls.container.css('min-width', value);
		},
		
		scrollToItem: function(value) {
			var itemPos = value.position(),
				containerHeight = this._controls.itemsContainer.innerHeight(true);
			
			if (itemPos.top + value.outerHeight(true) > containerHeight) {
				this._controls.itemsContainer.scrollTop(
					this._controls.itemsContainer.scrollTop() + itemPos.top
				);
			} else if (itemPos.top < 0) {
				this._controls.itemsContainer.scrollTop(
					this._controls.itemsContainer.scrollTop() - containerHeight - itemPos.top
				);
			}
		},
		
		selectedItem: function(value) {
			return this.selectedItems(value !== undefined? [value]: undefined);
		},
		
		/**
		 * @param {jQuery[]|null|undefined} values
		 * @return {jQuery[]}
		 */
		selectedItems: function(values) {
			var selectedItems = this._controls.itemsContainer.data('selectedItems') || [];
		
			values !== undefined || (values = selectedItems);
			
			var possibleSelectedClasses = this._getCssClass(['state-itemselected', 'state-itemmultiselected']);
			var selectedClass = this._getCssClass(
				!this.settings.multipleSelection? 'state-itemselected': ['state-itemselected', 'state-itemmultiselected']
			);
			
			_.each(selectedItems, function(selected) { // удаляем классы у предыдущих выбранных элементов
				this._getItem(selected).removeClass(possibleSelectedClasses);
			}, this);
			
			values = _.compact(_.map(values, function(selected) {
				var item = this._getItem(selected);
				if (!!item) {
					item.addClass(selectedClass);
				}
				return item;
			}, this));
			
			this._controls.itemsContainer.data('selectedItems', values);
			
			return values;
		},
		
		/**
		 * @param {jQuery|null|undefined} value
		 * @return {jQuery}
		 */
		hoveredItem: function(value) {
			var selectedItems = this._controls.itemsContainer.data('selectedItems') || [];
			if (!this.settings.multipleSelection) {
				this.selectedItems(null);
				value !== undefined || (value = selectedItems[0]);
			}
				
			var hoveredClass = this._getCssClass('state-itemhovered');
			
			var item = this._controls.itemsContainer.data('hoveredItem') || HashUtils.first(this._controls.items);
			
			value !== undefined || (value = item);
			value = this._controls.items[value.data('itemid')];

			item.removeClass(hoveredClass);
			value.addClass(hoveredClass);
			
			this._controls.itemsContainer.data('hoveredItem', value);

			return value;
		},
		
		hoverNext: function() {
			var hovered = this.hoveredItem(),
				nextKey = HashUtils.nextKey(this._controls.items, hovered.data('itemid'));
			
			hovered = this.hoveredItem(nextKey !== -1? this._controls.items[nextKey]: hovered);
			
			return hovered;
		},
		
		hoverPrev: function() {
			var hovered = this.hoveredItem(),
				prevKey = HashUtils.prevKey(this._controls.items, hovered.data('itemid'));
			
			return this.hoveredItem(prevKey !== -1? this._controls.items[prevKey]: hovered);
		},
		
		header: function(html) {
			var curHtml = this._controls.header.html(html);
			if (curHtml === '') {
				this.hideHeader();
			}
			return curHtml;
		},
		
		footer: function(html) {
			var curHtml = this._controls.footer.html(html);
			if (curHtml === '') {
				this.hideFooter();
			}
			return curHtml;
		},
		
		showHeader: function() {
			this._controls.header.show();
			return this;
		},
		
		hideHeader: function() {
			this._controls.header.hide();
			return this;
		},
		
		showFooter: function() {
			this._controls.footer.show();
			return this;
		},
		
		hideFooter: function() {
			this._controls.footer.hide();
			return this;
		},
		
		focus: function() {
			this._controls.seacherInput.focus(); 
			return this;
		},
		
		blur: function() {
			this._controls.seacherInput.blur();
			return this;
		},
		
		isFocused: function() {
			return document.activeElement === this._controls.container.get(0) || 
				document.activeElement === this._controls.seacherInput.get(0);
		}
	};
	
	Dropdown = extend(BaseObject, Dropdown, EventsMixin);
	
	var ExtSelectAbstract = {
		dataProviders: {
			Abstract: DataProviderAbstract,
			Array: ArrayDataProvider,
			Ajax: AjaxDataProvider,
			Backbone: BackboneDataProvider,
			Enoso: EnosoDataProvider
		},
		
		dropdowns: {
			dropdown: Dropdown
		},
		
		/**
		 * Стандартные настройки
		 * @static
		 */
		_defaultSettings: {
			visible: 'auto', // Если 'auto' то получено из контейнера на основое которого инициализируется селект
			enabled: 'auto', // Если 'auto' то получено из контейнера на основое которого инициализируется селект
			disabled: 'auto',
			
			disableOnEmpty: false,
			hiddeOnEmpty: false,
			
			cssClassPrefix: 'extselect-',
			showDropdownButton: true,
			clearSelection: 'auto', 
			placeholder: '',
			placeholderValue: '',
			allowEmpty: false,
			editable: false,
			copyInlineStyles: true,
			copyCssClasses: true,
			allowFreeInput: false, // разрешать "вольный ввод" (это когда вписываешь текст но не выбераешь из списка)
			name: null, // Если null то получено из контейнера на основое которого инициализируется селект
			dataProvider: {
				model: ArrayDataProvider
			},
			dropdown: {
				model: Dropdown
			}
		},
		
		/**
		 * Шаблон
		 * @static
		 */
		_template: _.template(
			'<div data-isextselect="1" class="<%= containerCssClass %> <%= settings.cssClassPrefix %>single" tabindex="<% if(!editable) { %>0<% } else { %>-1<% } %>">' +
			'<div class="<%= inputWrapperCssClass %>">' +
			'<input type="hidden" name="<%= name %>" value="">' +
			'<% if(editable) { %>' +
			'<input type="hidden" name="<%= name %>_freeinputmark" value="0">' +
			'<input type="text" name="<%= name %>_text" autocomplete="off" class="<%= inputCssClass %>" value="<%= placeholder %>">' +
			'<% } else { %>' +
			'<span class="<%= inputCssClass %>"><%= placeholder %></span>' +
			'<% } %>' +
			'</div>' +
			'<div class="<%= clearSelectionCssClass %>"/>' +
			'<div class="<%= buttonCssClass %>"/>' +
			'<div class="<%= dropdownWrapperCssClass %>"/>' +
			'</div>'
		),
		
		/**
		 * Возвращает цсс класс с префиксом
		 * @param {String|Array|Object} value базовый css-класс или их массив
		 * @return {String}
		 */
		_getCssClass: function(value) {
			if (_.isArray(value) || _.isObject(value)) {
				return _.reduce(value, function(memo, item) {
					return memo + (item !== ''? ' ' + this.settings.cssClassPrefix + item: '');
				}, '', this);
			} else {
				return this.settings.cssClassPrefix + value;
			}
		},
		
		/**
		 * Устанавливает и/или возвращает значение визуального представления данных
		 * @param {String} value
		 * @return {String}
		 */
		_inputBoxVal: function(value) {
			if (value !== undefined) {
				if (this.settings.editable) {
					this._controls.inputBox.val(value);
				} else {
					this._controls.inputBox.html(value);
				}
			}
			
			return this.settings.editable? this._controls.inputBox.val(): this._controls.inputBox.html();
		},
		
		/**
		 * Устанавливает и/или возвращает значение внутреннего представления данных
		 * @param {String} value
		 * @return {String}
		 */
		_inputValueVal: function(value) {
			if (value !== undefined) {
				this._controls.inputValue.val(value);
			}
			
			return this._controls.inputValue.val();
		},
		
		
		/**
		 * Устанавливает и/или возвращает маркер вольного ввода
		 * @param {String} value
		 * @return {String}
		 */
		_inputFreeInputMarkVal: function(value) {
			if (value !== undefined) {
				this._controls.inputFreeValueMark.val(value);
			}
			
			return this._controls.inputFreeValueMark.val();
		},
		
		/**
		 * Устанавливает визуальное представление текущего состояния селекта
		 * @param {String} state
		 * @param {Boolean|undefined} enable
		 */
		_setViewState: function(state, enable) {
			var cssClass = this._getCssClass('state-' + state);
			if (enable === undefined || enable === true) {
				this._controls.container.addClass(cssClass);
			} else {
				this._controls.container.removeClass(cssClass);
			}
		},
		
		/**
		 * Проверяет состояние
		 * @return {Boolean}
		 */
		_isViewState: function(state) {
			return this._controls.container.hasClass(this._getCssClass('state-' + state));
		},
		
		_compileTemplate: function() {
			return this._template({
				settings: this.settings,
				containerCssClass: this._getCssClass('container'),
				inputWrapperCssClass: this._getCssClass('inputwrapper'),
				name: this.settings.name,
				editable: this.settings.editable,
				inputCssClass: this._getCssClass('input'),
				placeholder: this.settings.placeholder,
				clearSelectionCssClass: this._getCssClass('clrbtn'),
				buttonCssClass: this._getCssClass('btn'),
				dropdownWrapperCssClass: this._getCssClass('dropdownwrapper')
			});
		},
		
		/**
		 * Создает контейнер на основе сервер-сайд сгенерированного html
		 */
		_createContainer: function() {
			
		},
		
		/**
		 * Создает селект из стандартного
		 */
		_createContainerFromSelect: function() {
			var name = this.settings.name === null? this.settings.name = this.$element.attr('name'): this.settings.name;
			this.$element.attr('name', ''); //empty name
			var html = this._compileTemplate();
			
			if (this.settings.enabled === 'auto') {
				this.settings.enabled = this.$element.is(':enabled');
			}
			
			if (this.settings.disabled === 'auto') {
				this.settings.disabled = this.$element.is(':disabled');
			}
			
			if (this.settings.visible === 'auto') {
				this.settings.visible = this.$element.is(':visible');
			}
			
			this.$element.hide();
			
			this._controls.container = $(html).insertAfter(this.$element);
			this._controls.button = this._controls.container.find('.' + this._getCssClass('btn'));
			this._controls.clearSelection = this._controls.container.find('.' + this._getCssClass('clrbtn'));
			this._controls.inputValue = this._controls.container.find('input[type="hidden"]:first');
			this._controls.inputBox = this._controls.container.find('.' + this._getCssClass('input'));
			this._controls.inputFreeValueMark = this._controls.container.find('input[type="hidden"]:last');
			this._controls.inputWrapper = this._controls.container.find('.' + this._getCssClass('inputwrapper'));
			this._controls.dropdownWrapper = this._controls.container.find('.' + this._getCssClass('dropdownwrapper'));
			
			this._setViewState(this.settings.editable? 'editable': 'readonly');
			this._setViewState(this.settings.enabled? 'enabled': 'disabled');

			if (!this.settings.clearSelection) {
				this._setViewState('clrbtn-hidden');
			}

			if (!this.settings.showDropdownButton) {
				this._controls.button.hide();
			}
			
			if (this.settings.copyInlineStyles) {
				this._controls.container.attr('style', this.$element.attr('style') || '');
			}
			
			if (this.settings.copyCssClasses) {
				this._controls.container.addClass(this.$element.attr('class') || '');
			}
			
			if (this.$element.attr('form') !== undefined) {
				this._controls.container.attr('form', this.$element.attr('form'));
			}
			
			this._controls.container.show();
		},
		
		_getItemsFromSelect: function() {
			var items = [];
			this.$element.children().each(function() {
				var $this = $(this);
				if (this.tagName === 'OPTGROUP') { // это группа, получаем дочерние элементы
					var group = this.label;
					$this.find('option').each(function() {
						var $this = $(this);
						items.push({
							group: group,
							val: $this.attr('value'),
							text: $this.text(),
							html: $this.html(),
							selected: !!$this.attr('selected'),
							storage: $.parseJSON($this.attr('data-storage') || null)
						});
					});
				} else { // элемент не является группой
					items.push({
						group: null,
						val: $this.attr('value'),
						text: $this.text(),
						html: $this.html(),
						selected: !!$this.attr('selected'),
						storage: $.parseJSON($this.attr('data-storage') || null)
					});
				}
			});
			//also add items from settings
			return items.concat(this.settings.items || []);
		},
		
		_importItemsFromSelect: function() {
			throw new Exception('Abstract method');
		},
		
		/**
		 * Устанавливает события на созданые элементы
		 */
		_bindEvents: function() {
			var self = this;
			
			var prevDisableState = this.isDisabled(), prevHiddeState = this.isHidden();
			
			this.getDataProvider().on('setData', _.debounce(function(data, size, realSize) {
				if (this.settings.disableOnEmpty || this.settings.hiddeOnEmpty) {
					var empty = this.getDropdown()._getFilter().length == 0 && _.filter(data, function(item) {
						return !item.isEmpty && !item.isPrompt;
					}).length === 0;
					
					if (this.settings.disableOnEmpty) {
						if (!this.isDisabled() && empty) {
							prevDisableState = this.isDisabled();
							this.disable();
						} else {
							if (prevDisableState && empty) {
								this.disable();
							} else {
								this.enable();
							}
						}
					} else if (this.settings.hiddeOnEmpty) {
						if (!this.isHidden() && empty) {
							prevHiddeState = this.isHidden();
							this.disable();
						} else {
							if (prevHiddeState && empty) {
								this.hide();
							} else {
								this.show();
							}
						}
					}
				}
			}, 100), this);
			
			this._controls.container.bind('keydown', function(e) {
				if (self.isDisabled()) {
					return;
				}
				
				var keyCode = e.keyCode || e.which;
				var dropdown = self.getDropdown();
				
				if (keyCode === Key.Tab) { // do nothing than pressed tab
					return;
				}
				
				if (dropdown.isOpened()) {
					switch(keyCode) {
						case Key.Esc:
							dropdown.close();
							return; // prevent dropdown open in next stage
						case Key.Enter:
							dropdown.open();
							self.getDropdown().hoveredItem().click();
							dropdown.close();
							break;
						case Key.Up:
							dropdown.scrollToItem(dropdown.hoverPrev());
							break;
						case Key.Down:
							dropdown.open();
							dropdown.scrollToItem(dropdown.hoverNext());
							break;
					}
				} else {
					dropdown.open();
				}
				
				if (_.indexOf([Key.Up, Key.Down], keyCode) !== -1) {
					e.preventDefault();
				}
			});
			
			this.getDropdown().bind('open:dropdown', function() {
				this._setViewState('dropdownopened', true);
			}, this);
			
			this.getDropdown().bind('close:dropdown', function() {
				this._setViewState('dropdownopened', false);
			}, this);
			
			this._controls.clearSelection.on('mousedown', function(e) {
				e.preventDefault();
				e.stopPropagation();
				e.stopImmediatePropagation();
			}).on('click', _.bind(this.clearSelection, this));
			
			if (this.settings.editable) {
				if (this.settings.allowFreeInput) {
					this._controls.inputBox.bind(inputEventSupported? 'input change': 'change keyup', function() {
						self._inputValueVal($(this).val());
						self._inputFreeInputMarkVal(1); // it's a free input
					});
					this.bind('select', _.throttle(function() {
						self._inputFreeInputMarkVal(0); // it's not a free input
					}, 200)); // дело в том что вызываются стандартный дом ивент на чейнж и выставлят что это "свободные ввод", потом ставим задержку
				} else  {
					var selectedText, filter;
					
					this.getDropdown().bind('open:dropdown', function() {
						selectedText = this._inputBoxVal();
						filter = this.getDropdown().filterText();
					}, this);
					
					//console.log(selectedText);
					this.bind('select', function() {
						selectedText = this._inputBoxVal();
						//console.log('select: ' + selectedText);
					}, this);
					this.getDropdown().bind('close:dropdown', function() {
						//console.log('closeDropdown: '+ this._inputBoxVal());
						if (selectedText !== this._inputBoxVal()) {
							//console.log(selectedText + ' = ' + this._inputBoxVal());
							this.getDropdown().filterText(filter);
							this.getDropdown().reload();
							this._setViewState('error', true);
							_.delay(_.bind(function() {
								this._setViewState('error', false);
							}, this), 500);
							this.selectedItem();
						}
					}, this);
				}
			
				this._controls.inputBox.bind(inputEventSupported? 'input change': 'change keyup', function() {
					self.getDropdown().filterText($(this).val());
				});
			}
			
			this.getDataProvider().on('setData', function(data) {
				this._data = data;
			}, this);
		},
		
		_emulateSelectEvents: function() {
			throw new Exception('Abstract method');
		},
		
		/**
		 * Специальные хаки для правильной работы фокусировки на селекте пользователем
		 */
		_bindFocusHacks: function() {
			var self = this, $window = $(window);
			
			var dropdownWrapperWidth = _.throttle(function() {
				var width = $window.width(),
					offset = self._controls.container.offset();

				width -= width / 10; // -10% percents
				width -= offset.left;
				
				self._controls.dropdownWrapper.css('width', width);
			}, 50);
			
			var dropdownWrapperWidthCollapse = _.throttle(function() {
				self._controls.dropdownWrapper.css('width', '');
			}, 50);
			
			/*this._controls.container.one('focusin', function() { 
				self.getDropdown().reload();
			});*/
			
			this._controls.container.bind('focusin', function() {
				if (!self.isDisabled() && self.isFocused() && !self.getDropdown().isOpened()) {
					self.getDropdown().minWidth(self._controls.container.width());
					dropdownWrapperWidth();
					self.getDropdown().open();
					$window.bind('resize', dropdownWrapperWidth);
				}
			}).bind('focusout', _.debounce(function() {
				if (self.getDropdown().isOpened() && !self.isFocused()) {
					self.getDropdown().close();
					$window.unbind('resize', dropdownWrapperWidth);
					dropdownWrapperWidthCollapse();
				}
			}, 100));
			
			if (self.settings.editable) {
				this._controls.inputWrapper.on('click', function() {
					self._controls.inputBox.focus();
				});
			}
		},
		
		/**
		 * Инициализатор
		 */
		initialize: function(element, settings) {
			this.$element = $(element);
			var dataSettings = this.$element.data('extselect-settings') || {};
			
			this.$element.data('extselect', this);
			
			settings === undefined && (settings = {});
			
			this.settings = $.extend(true, {}, this._defaultSettings, dataSettings, settings);
			//this.settings.dataProvider = $.extend({}, this._defaultSettings.dataProvider, settings.dataProvider);
			//this.settings.dropdown = $.extend({}, this._defaultSettings.dropdown, settings.dropdown);
			
			this._controls = {
				container: null,
				button: null,
				clearSelection: null,
				inputBox: null,
				inputValue: null,
				dropdownWrapper: null
			};
			
			this._dataProvider = this._dropdown = null;
			this._selectedItem = null;
			this._data = {};
			
			var isSelect = this.$element[0].tagName === 'SELECT';
			
			if (isSelect) {
				this._createContainerFromSelect();
				this._emulateSelectEvents();
			} else {
				this._createContainer();
			}
			
			this._bindEvents();
			this._bindFocusHacks();
			this._visible = (this._controls.container.css('display') || '') !== 'none';
			
			if (isSelect) {
				this._importItemsFromSelect();
			}
						
			if (!!settings.events) {
				_.defer(_.bind(function() {
					_.each(settings.events, function(callback, event) {
						this.on(event, callback);
					}, this);
				}, this));
			}

			setTimeout(_.bind(function() {
				this.trigger('initialize', this);
			}, this), 100);
		},
		
		/**
		 * Возвращает субконтрол
		 * @param {String} name
		 * @return {jQuery}
		 */
		getControl: function(name) {
			return this._controls[name];
		},
		
		/**
		 * Возвращает провайдера данных
		 * @return {DataProviderAbstract}
		 */
		getDataProvider: function() {
			if (this._dataProvider === null) { //lazy initialization of dataprovider
				var settings = _.clone(this.settings.dataProvider);
				delete settings.model;
				this._dataProvider = new this.settings.dataProvider.model({}, settings);
				this._dataProvider.setSelect(this);
			}
			return this._dataProvider;
		},
		
		/**
		 *
		 * @param {DataProviderAbstract} dataProvider
		 * @param {Boolean} copyData копировать ли данные из ТЕКУЩЕГО провайдера в НОВЫЙ, по умолчанию false
		 */
		setDataProvider: function(dataProvider, copyData) {
			if (!!copyData) {
				this._dataProvider.getData(function(data) {
					dataProvider.setData(data);
				});
			}
			this._dataProvider = dataProvider;
		},
		
		reload: function(callback, context) {
			this.getDataProvider().reload(this.getDropdown().filterText(), callback, context);
		},
		
		/**
		 * @return {Boolean}
		 */
		_isDropdownInitialized: function() {
			return this._dropdown !== null;
		},
		
		/**
		 * @return {Dropdown}
		 */
		getDropdown: function() {
			// abstract
		},
		
		/**
		 * Устанавливает настройки селекта
		 */
		setOptions: function(settings) {
			this._settings = settings;
		},
		
		/**
		 * Включен ли селект
		 * @return {Boolean}
		 */
		isEnabled: function() {
			return this._isViewState('enabled');
		},
		
		/**
		 * Выключен ли селект
		 * @return {Boolean}
		 */
		isDisabled: function() {
			return this._isViewState('disabled');
		},
		
		/**
		 * Включает селект
		 */
		enable: function() {
			this._setViewState('enabled', true);
			this._setViewState('disabled', false);
			this.trigger('enable:select', this._controls.container, this);
		},
		
		/**
		 * Выключает селект
		 */
		disable: function() {
			this._setViewState('enabled', false);
			this._setViewState('disabled', true);
			this.trigger('disable:select', this._controls.container, this);
		},
		
		isFocused: function() {
			return document.activeElement === this._controls.container.get(0) || 
				document.activeElement === this._controls.inputBox.get(0) || 
				this.getDropdown().isFocused();
		},
		
		focus: function() {
			this._controls.container.focus();
		},
		
		blur: function() {
			this.getDropdown().blur();
			if (this.isFocused()) {
				this._controls.inputBox.blur();
				this._controls.container.blur();
			}
		},
		
		/**
		 * Проверка на видимость селекта
		 * @return {Boolean}
		 */
		isVisible: function() {
			return this._isViewState('visible');
		},
		
		/**
		 * Проверка на видимость селекта
		 * @return {Boolean}
		 */
		isHidden: function() {
			return !this.isVisible();
		},
		
		/**
		 * Скрыть селект
		 */
		hide: function() {
			this._setViewState('hidden', true);
			this.trigger('hide:select');
		},
		
		/**
		 * Показать селект
		 */
		show: function() {
			this._setViewState('hidden', false);
			this.trigger('show:select');
		},
		
		receiveDefaultSelection: function() {
			throw new Exception('Abstract method');
		},
		
		clearSelection: function() {
			throw new Exception('Abstract method');
		},
		
		val: function(val, force) {
			return this._inputValueVal(val);
		},
		
		selectedItem: function(item) {
			throw new Exception('Abstract method');
		},
		
		selectedIndex: function(index) {
			throw new Exception('Abstract method');
		},
		
		text: function(value, force) {
			return this._inputBoxVal(value);
		},
		
		html: function(value) {
			return this._inputBoxVal(value);
		}
	};
	
	ExtSelectAbstract = extend(BaseObject, ExtSelectAbstract, EventsMixin);
	
	var ExtSelect = {
		_importItemsFromSelect: function() {
			var items = this._getItemsFromSelect();
			
			this.getDataProvider().setData(items);
			
			this.getDataProvider().getData(null, function(data) {
				
				var selected = _.find(data, function(item) {
					return item.selected;
				});
				
				if (!selected && this.settings.selection !== undefined) {
					selected = _.find(data, function(item) {
						return item.selected = !item.isSeparator && item.val === this.settings.selection;
					}, this);
				}
				
				this.selectedItem(selected);
				this.receiveDefaultSelection();
			}, this);
		},
		
		_bindEvents: function() {
			this.parent._bindEvents.call(this);
			
			var self = this;
			
			this.getDropdown().bind('click:item', function(element, item) {
				var selectedItem = this.selectedItem() || null;
				if (this.isEnabled() && (selectedItem === null || selectedItem.getId() !== item.getId()) &&
					this.trigger('allowSelect', element, item) !== false) 
				{
					this.trigger('beforeSelect', element, item);
					this.selectedItem(item);
					this.getDropdown().selectedItem(item);
					this.trigger('select', element, item);
					this.getDropdown().close();
					this.blur();
					this.trigger('afterSelect', element, item);
				} else {
					this.getDropdown().close();
					this.blur();
				}
			}, this);
		},
		
		_emulateSelectEvents: function() {
			
			this.bind('setSelectedItem', function(item) {
				if (!!item) {
					this.$element.html('<option value="' + item.getVal() + '" selected>' + item.getSelectedText(false) + '</option>');
				} else {
					this.$element.html('<option value="" selected></option>');
				}
			}, this);
		
			this.bind('afterSelect', function(element, item) {
				//this.$element.html('<option value="' + item.getVal() + '" selected>' + item.getSelectedText(false) + '</option>');
				this.$element.trigger('change');
			}, this);
		},
		
		getDropdown: function() {
			if (!this._isDropdownInitialized()) {
				var dropdownSettings = _.clone(this.settings.dropdown);
				if (this.settings.editable) {
					dropdownSettings['search'] = false;
				}
				dropdownSettings['multipleSelection'] = false;
				delete dropdownSettings.model;
				
				this._dropdown = new this.settings.dropdown.model(this, dropdownSettings);
			}
			return this._dropdown;
		},
		
		receiveDefaultSelection: function() {
			this._defaultSelectedItem = this.selectedItem() || null;
			this.getDataProvider().getData(null, function(data) {
				this._defaultData = _.clone(data);
			}, this);
		},
		
		clearSelection: function() {
			this.getDataProvider().setData(this._defaultData || {});
			this.selectedItem(this._defaultSelectedItem || null);
			this.getDropdown().reload();
		},
		
		selectedItem: function(item) {
			if (item === undefined) {
				item = this._selectedItem || null;
			} else if(item === null) {
				this._inputBoxVal('');
				this._inputValueVal('');
				//this._inputBoxVal(this.settings.placeholder);
				//this._inputValueVal(this.settings.placeholderValue);
				
				this.trigger('setSelectedItem', null);
			}
			
			if (item !== null) {
				this._inputBoxVal(item.getSelectedText(!this.settings.editable));
				this._inputValueVal(item.getVal());
				if (!!this._selectedItem) {
					this._selectedItem.selected = false;
				}
				this._selectedItem = item;
				this._selectedItem.selected = true;
				
				this.trigger('setSelectedItem', item);
			}

			return item;
		},
		
		selectedIndex: function(index) {
			var key;
			if (typeof(index) !== 'undefined') {
				key = HashUtils.indexKey(this._data || {}, index);
				var selectedItem = this.selectedItem(!!key? this._data[key]: null);
				if (this._isDropdownInitialized()) {
					this.getDropdown().selectedItem(selectedItem);
				}
				return selectedItem;
			}
			
			key = !!this._selectedItem? this._selectedItem.getId(): null;
			
			return key !== null? HashUtils.keyIndex(this._data || {}, key): -1;
		}
	};
	
	ExtSelect = extend(ExtSelectAbstract, ExtSelect);
	
	var ExtMultiSelect = {
		/**
		 * Шаблон
		 * @static
		 */
		_template: _.template(
			'<div data-isextselect="1" class="<%= containerCssClass %>" tabindex="<% if(!editable) { %>0<% } else { %>-1<% } %>">' +
			'<div class="<%= inputWrapperCssClass %>">' +
			//'<input type="hidden" name="<%= name %>" value="">' +
			'<span class="<%= itemsCssClass %>"></span>' +
			'<% if(editable) { %>' +
			'<input type="hidden" name="freeinputmark_<%= name %>" value="0">' +
			'<input type="text" autocomplete="off" class="<%= inputCssClass %>" placeholder="<%= placeholder %>">' +
			'<% } else { %>' +
			'<span class="<%= inputCssClass %>"><%= placeholder %></span>' +
			'<% } %>' +
			'</div>' +
			'<div class="<%= clearSelectionCssClass %>"/>' +
			'<div class="<%= buttonCssClass %>"/>' +
			'<div class="<%= dropdownWrapperCssClass %>"/>' +
			'</div>'
		),
			
		_itemTemplate: _.template(
			'<span class="<%= itemCssClass %> <%= pillStateCssClass %>" data-itemid="<%= item.getId() %>">' +
			'<input type="hidden" name="<%= name %>" value="<%= item.getVal() %>">' +
			'<span class="<%= textCssClass %>"><%= item.getSelectedText(true) %></span>' +
			'<span class="<%= removeCssClass %>"/>' +
			'</span>'
		),
		
		_compileTemplate: function() {
			return this._template({
				settings: this.settings,
				containerCssClass: this._getCssClass(['container', 'multiple']),
				inputWrapperCssClass: this._getCssClass('inputwrapper'),
				name: this.settings.name,
				editable: this.settings.editable, 
				itemsCssClass: this._getCssClass('items'),
				inputCssClass: this._getCssClass('input'),
				placeholder: this.settings.placeholder,
				clearSelectionCssClass: this._getCssClass('clrbtn'),
				buttonCssClass: this._getCssClass('btn'),
				dropdownWrapperCssClass: this._getCssClass('dropdownwrapper')
			});
		},
		
		_importItemsFromSelect: function() {
			var items = this._getItemsFromSelect();
			
			this.getDataProvider().setData(items);
			
			this.getDataProvider().getData(null, function(data) {
				var selectedItems = {};
				
				_.each(data, function(item) {
					if (item.selected) {
						selectedItems[item.getId()] = item;
					}
				});
				
				if (!selectedItems && this.settings.selection !== undefined) {
					_.each(data, function(item) {
						item.selected = item.val === this.settings.selection;
						if (item.selected) {
							selectedItems[item.getId()] = item;
						}
					}, this);
				}
				
				this.selectedItems(selectedItems);
				
				this.receiveDefaultSelection();
			}, this);
		},
		
		_createContainerFromSelect: function() {
			this.parent._createContainerFromSelect.apply(this);
			this._controls.items = this._controls.container.find('.' + this._getCssClass('items'));
		},
		
		_bindEvents: function() {
			this.parent._bindEvents.call(this);

			var self = this;
			
			this.getDropdown().bind('click:item', function(element, item) {
				var items = this.selectedItems() || {};
				if (this.isEnabled() && !_.has(items, item.getId()) &&
					this.trigger('allowSelect', element, item, items) !== false) 
				{
					items[item.getId()] = item;
					this.trigger('beforeSelect', element, item, items);
					this.selectedItem(item, true); // push an item
					this.getDropdown().selectedItems(items);
					this.trigger('select', element, item, items);
					this.getDropdown().close();
					this.blur();
					this.trigger('afterSelect', element, item, items);
				}
			}, this);
			
			var itemCssClass = self._getCssClass('itemmultiple');
			var rmbtnCssClass = self._getCssClass('rmbtn');
			
			// disable focusing on remove btn click
			this._controls.items.on('mousedown', '.' + rmbtnCssClass, function(e) {
				e.stopPropagation();
				e.preventDefault();
				e.stopImmediatePropagation();
			});
			
			this._controls.items.on('click dblclick', '.' + self._getCssClass('rmbtn') + ', .' + rmbtnCssClass, function(e) {
				var $item = $(this).closest('.' + itemCssClass),
					id = $item.data('itemid');
				var item = self.selectedItems()[id];
				self.unselectItem(item);
				_.defer(function() {
					self.getDropdown().reload();
				});
			});
			
			if (this.settings.editable) {
				var text = self._controls.inputBox.val();
				self._controls.inputBox.val(self.settings.placeholder);
				this._placeholderWidth = measureInputWidth(
					self._controls.inputBox, 10, self._controls.inputBox.parent().innerWidth()
				);
				self._controls.inputBox.val(text);
					
				this._controls.inputBox.on(inputEventSupported? 'change input': 'change keyup', function() {
					var width = measureInputWidth(self._controls.inputBox, this._placeholderWidth, self._controls.inputBox.parent().innerWidth());
					self._controls.inputBox.width(width);
				});
				
				this.getDropdown().bind('close:dropdown', function() {
					self._controls.inputBox.val(self.settings.placeholder);
					var width = measureInputWidth(self._controls.inputBox, self._placeholderWidth, self._controls.inputBox.parent().innerWidth());
					self._controls.inputBox.width(width);
					self._controls.inputBox.val('');
				});
			}
		},
		
		_emulateSelectEvents: function() {
			this.bind('afterSelect', _.throttle(function(element, item) {
				var html = '';
				_.each(this._selectedItems, function(item) {
					html += '<option value="' + item.getVal() + '" selected>' + item.getSelectedText(false) + '</option>';
				});
				this.$element.html(html);
				this.$element.trigger('change');
			}, 100), this);
		},
		
		initialize: function() {
			this.settings.itemName = this.settings.name + (this.settings.name.substr(-2) !== '[]'? '[]': '');
		},
		
		getDropdown: function() {
			if (this._dropdown === null) {
				var dropdownSettings = _.clone(this.settings.dropdown);
				dropdownSettings['search'] = !this.settings.editable;
				dropdownSettings['multipleSelection'] = true;
				delete dropdownSettings.model;
				
				this._dropdown = new this.settings.dropdown.model(this, dropdownSettings);
			}
			return this._dropdown;
		},
		
		receiveDefaultSelection: function() {
			this._defaultSelectedItems = this.selectedItems() || null;
			this.getDataProvider().getData(null, function(data) {
				this._defaultData = _.clone(data);
			}, this);
		},
		
		clearSelection: function() {
			this.getDataProvider().setData(this._defaultData || {});
			this.selectedItems(this._defaultSelectedItems || null);
			this.getDropdown().reload();
		},
		
		selectedItem: function(item, add) {
			this._selectedItems === undefined && (this._selectedItems = {});
			
			item = item || HashUtils.first(this._selectedItems) || {};
			add === undefined && (add = false);
			
			var self = this;
			
			if (!_.has(item, this._selectedItems)) {
				var items = [];
				if (item instanceof DataItem) {
					items.push(item);
				} else {
					_.each(item || [], function(item) {
						items.push(item);
					});
				}
				
				if (!!this._selectedItems && !add) {
					return this.selectedItems(items);
				}
				
				_.defer(function() {
					var html = '';
					_.each(items, function(item) {
						html += self._itemTemplate({
							name: self.settings.itemName,
							item: item,
							itemCssClass: self._getCssClass('itemmultiple'),
							pillStateCssClass: item.pillState !== ''? self._getCssClass('state-' + item.pillState): '',
							textCssClass: self._getCssClass('text'),
							removeCssClass: self._getCssClass('rmbtn')
						});
					});
					self._controls.items.append(html);
				});
				
				_.each(items, function(item) {
					item.selected = true;
					self._selectedItems[item.getId()] = item;
				});
			}
			
			return item;
		},
		
		selectedItems: function(items) {
			items = items || this._selectedItems || {};
			var self = this;
			if (!_.isEqual(items, this._selectedItems)) {
				var itemsHash = {};
				
				_.each(this._selectedItems, function(selectedItem) {
					selectedItem.selected = false;
				});
				
				_.each(items, function(item) {
					item.selected = true;
					itemsHash[item.getId()] = item;
				});
				
				_.defer(function() {
					var html = ''; 
					_.each(items, function(item) {
						html += self._itemTemplate({
							name: self.settings.itemName,
							item: item,
							itemCssClass: self._getCssClass('itemmultiple'),
							pillStateCssClass: item.pillState === '' || self._getCssClass('state-' + item.pillState),
							textCssClass: self._getCssClass('text'),
							removeCssClass: self._getCssClass('rmbtn')
						});
					});
					self._controls.items.html(html);
				});
				
				this._selectedItems = itemsHash;
			}
			return items;
		},
		
		unselectItem: function(item) {
			if (!!item) {
				this.unselectItems([item]);
			}
		},
	
		unselectItems: function(items) {
			var selectedItems = this._selectedItems || {};
			var self = this;
			var $items = this._controls.items.children('.' + this._getCssClass('itemmultiple'));
			if (!!selectedItems && $items.length > 0 && _.size(selectedItems) > 0) {
				_.each(items, function(item) {
					$items.filter('[data-itemid="' + item.getId() + '"]').remove();
					item.selected = false;
					delete self._selectedItems[item.getId()];
				});
			}
		},
		
		unselectAll: function() {
			this._controls.items.html('');
			var items = this._selectedItems || {};
			_.each(items, function(item) {
				item.selected = false;
			});
			this._selectedItems = {};
		}
		
		/*val: function(val) {
			
		},
		
		selectedIndex: function(index) {
			
		},
		
		text: function(value) {
			
		}*/
	};
	
	ExtMultiSelect = extend(ExtSelectAbstract, ExtMultiSelect);
	
	// ------------------- JQUERY ------------------------------
	
	$.fn.extSelect = function(settings) {
		settings === undefined && (settings = {});
		
		return this.each(function() {
			var $this = $(this);
			if (!$this.data('extselect')) {
				var data = $this.data('settings') || {};

				var options = $.extend({}, data, settings, {
					multiple: !!(settings.multiple === undefined? $this.is('[multiple]') || data.multiple || false : settings.multiple)
				});

				var model = options.multiple? ExtMultiSelect: ExtSelect;
			}
			return this;
		});
	};
	
	$.fn.extSelect.dataProviders = ExtSelectAbstract.prototype.dataProviders;
	$.fn.extSelect.dropdowns = ExtSelectAbstract.prototype.dropdowns;
	
	$(function() {
		$('[data-extselect]').extSelect();
	});
	
	global['ExtSelectAbstract'] = ExtSelectAbstract;
	global['ExtSelect'] = ExtSelect;
	global['ExtMultiSelect'] = ExtMultiSelect;
}).call({}, window, jQuery, _);
