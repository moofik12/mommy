<?php

use MommyCom\Model\Db\EventProductRecord;
use MommyCom\YiiComponent\ArrayUtils;
use MommyCom\YiiComponent\Type\Cast;

/**
 * @author scriptbunny
 * @package application.widgets
 */
class ExtSelect extends CInputWidget
{
    const DATAPROVIDER_ARRAY = 'ExtSelectAbstract.prototype.dataProviders.Array';
    const DATAPROVIDER_AJAX = 'ExtSelectAbstract.prototype.dataProviders.Ajax';
    const DATAPROVIDER_BACKBONE = 'ExtSelectAbstract.prototype.dataProviders.Backbone';
    const DATAPROVIDER_CLOSURE = '';
    const DATAPROVIDER_DEFAULT = self::DATAPROVIDER_ARRAY;

    protected static $_buildinProviders = array(
        'array' => self::DATAPROVIDER_ARRAY,
        'ajax' => self::DATAPROVIDER_AJAX,
        'backbone' => self::DATAPROVIDER_BACKBONE,
        'closure' => self::DATAPROVIDER_CLOSURE,
    );

    /**
     * Данные селекта
     * @var array
     */
    public $data = array();

    public $standardData = false;

    public $select = null;

    public $htmlOptions = array();

    public $settings = array();

    public $transformData = null;

    public $selection = null;

    protected static $_defaultSettings = array(
        'dataProvider' => array(
            'model' => self::DATAPROVIDER_DEFAULT,
        ),
        'dropdown' => array(
            'model' => 'ExtSelectAbstract.prototype.dropdowns.dropdown'
        ),

        'visible' => 'auto', // Если 'auto' то получено из контейнера на основое которого инициализируется селект
        'enabled' => 'auto', // Если 'auto' то получено из контейнера на основое которого инициализируется селект

        'cssClassPrefix' => 'extselect-',
        'showDropdownButton' => true,
        'clearSelection' => 'auto',
        'placeholder' => '',
        'placeholderValue' => '',
        'allowEmpty' => false, // разрешать пустой селект
        'editable' => false, // редактируемых
        'copyInlineStyles' => true, // копировать стили
        'copyCssClasses' => true, // копировать css классы
        'allowFreeInput' => false, // разрешать "вольный ввод" (это когда вписываешь текст но не выбераешь из списка)
        'name' => null, // Если null то получ
    );

    public function run()
    {
        $settings = ArrayUtils::merge(static::$_defaultSettings, $this->settings);

        if (isset(static::$_buildinProviders[$settings['dataProvider']['model']])) {
            $settings['dataProvider']['model'] = static::$_buildinProviders[$settings['dataProvider']['model']];
        }

        list($name, $id) = $this->resolveNameID();
        $this->htmlOptions['id'] = $id;
        $this->htmlOptions['name'] = $name;


        if ($this->selection === null && $this->hasModel()) {
            $this->selection = CHtml::resolveValue($this->model, $this->attribute);
            $settings['selection'] = $this->selection;
        }

        $items = $this->_getData();

        if (empty($settings['items'])) {
            $settings['items'] = $items;
        } else {
            $settings['items'] = array_merge($settings['items'], $items);
        }

        if ($this->hasModel()) {
            echo CHtml::tag('select', $this->htmlOptions, '');
        } else {
            echo CHtml::dropDownList($name, $this->select, array(), $this->htmlOptions);
        }

        $multiple = isset($settings['multiple']) ? $settings['multiple'] : false;
        $this->htmlOptions['multiple'] = $multiple = isset($this->htmlOptions['multiple']) ? $this->htmlOptions['multiple'] : $multiple;
        unset($settings['multiple']);

        $model = $multiple ? 'ExtMultiSelect' : 'ExtSelect';

        $jsSettings = ArrayUtils::diff($settings, static::$_defaultSettings);

        if (isset($jsSettings['dataProvider']['model'])) {
            $jsSettings['dataProvider']['model'] = new CJavaScriptExpression($jsSettings['dataProvider']['model']);
        }

        if (isset($jsSettings['dropdown']['model'])) {
            $jsSettings['dropdown']['model'] = new CJavaScriptExpression($jsSettings['dropdown']['model']);
        }

        $javascript = "new $model(document.getElementById('{$id}')" . (!empty($jsSettings) ? ', ' . CJavaScript::encode($jsSettings) : '') . ");";

        echo "<script>{$javascript}</script>";
    }


    protected function _getData()
    {
        $items = array();
        $data = $this->data;
        $selection = $this->selection;
        $isMultiple = is_array($selection);

        $transform = $this->transformData;
        if (!$this->standardData) {
            if (!empty($data) && !empty($data[0])) { // has settings and has models in
                $i = 0;
                list($models, $valueField, $textField, $groupField, $idField) = $data + array(array(), 'id', 'name', '', 'id');

                foreach ($models as $key => $model) {
                    $items[$i] = array(
                        'val' => Cast::toStr(ArrayUtils::getValueByPath($model, $valueField)),
                        'text' => Cast::toStr(ArrayUtils::getValueByPath($model, $textField))
                    );
                    $item = &$items[$i];
                    if ($selection !== false && ($isMultiple && in_array($item['val'], $selection) || $selection == $item['val'])) {
                        $item += array(
                            'selected' => true
                        );
                    }
                    if (!empty($groupField)) {
                        $item += array(
                            'group' => ArrayUtils::getValueByPath($model, $groupField)
                        );
                    }
                    if (!empty($idField)) {
                        $item += array(
                            'id' => ArrayUtils::getValueByPath($model, $idField)
                        );
                    }

                    if ($transform !== null && is_callable($transform)) {
                        $items[$i] = $transform($i, $item, $model);
                    }

                    $i++;
                }
            }
        } else {
            $i = 0;
            foreach ($data as $group => $dataItems) {
                if (is_array($dataItems)) {
                    foreach ($dataItems as $key => $value) {
                        $items[$i] = Cast::toStrArr(array(
                            'val' => $key,
                            'text' => $value,
                            'group' => $group
                        ));

                        if ($selection !== false && ($isMultiple && in_array($items[$i]['val'], $selection) || $selection == $items[$i]['val'])) {
                            $items[$i] += array(
                                'selected' => true
                            );
                        }
                    }
                } else {
                    $items[$i] = array(
                        'val' => Cast::toStr($group),
                        'text' => $dataItems,
                    );

                    if ($selection !== false && ($isMultiple && in_array($items[$i]['val'], $selection) || $selection == $items[$i]['val'])) {
                        $items[$i] += array(
                            'selected' => true
                        );
                    }
                }

                if ($transform !== null && is_callable($transform)) {
                    $items[$i] = $transform($i, $items[$i], null);
                }
                $i++;
            }
        }

        foreach ($items as &$item) {
            $item['text'] = EventProductRecord::replaceLangAgeSize($item['text']);
        }

        return $items;
    }
}
