$(function () {
    var container = $('.login-wrapper');
    var containerTabs = $('.tabs');

    $('.login-conteiner').on('click', '[data-tab]', function () {
        var $this = $(this);
        var id = $this.attr('data-tab');


        containerTabs.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('data-tab') == id) {
                element.addClass('active');
            } else {
                element.removeClass('active');
            }
        });

        container.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('id') == id) {
                element.show();
            } else {
                element.hide();
            }
        });
    });

    $('.message').on('click', function (e) {
        $(this).hide();
    });
});
