$(function () {
    Mamam.countdown('body', {
        tick: 1000,
        reloadTime: 10000
    });
    Mamam.cart.initialize('*[data-quickcart-container]', 'header *[data-quickcart-timer]');
    Mamam.imagesLazyload.initialize('img[data-lazyload]');

    $(document).on('mamam.auth.success', function () {
        dataLayer.push({'event': 'mamam.auth.success'})
    });

    $(document).on('mamam.registration.success', function () {
        dataLayer.push({'event': 'mamam.registration.success'});

        if (Mamam.clickId) {
            $.ajax('https://pstb.peerclicktrk.com/postback?cid=' + Mamam.clickId + '&summ_total=1&userid=2071');
        }
    });

    $(document).on('mamam.purchase.success', function (e) {
        dataLayer.push({
            'event': 'mamam.purchase.success', 'mamam.purchase.success': {
                totalPrice: e.totalPrice,
                purchases: e.purchases
            }
        })
    });

    $(document).on('mamam.product.impressions', function (e, $impressions) {
        if (!$impressions || !$impressions.length) return;

        var newSeenElements = [];
        $impressions.filter('[data-product-id]').each(function () {
            newSeenElements.push($(this).data('product-id'));
        });

        Mamam.ecommerce.impressions(newSeenElements);
    });

    window.scrollTop = function () {
        $('body,html').animate({scrollTop: 0}, 400);
    };

    $('.footer-phone').click(function () {
        var phone = $(this).text();
        Mamam.app.callNumber(phone);
    });

    Mamam.app.afterLoad();
    setTimeout(function () {
        //фикс для старых устройств
        $(window).trigger('scroll');
    }, 0);

    $(document).trigger('actions.setup.loaded');
});
