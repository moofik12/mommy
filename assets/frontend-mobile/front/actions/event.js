$(function () {
    var EVENT_PRODUCT_HEIGHT = 347;
    var mainContainer = $('.main-container');
    var filterBar = $('#filter-block');
    var apiLoadingMoreGoods;

    function initDownloadMore() {
        /*var $downloadMoreButton = $('.main .show-more');
        if ($downloadMoreButton.length && apiLoadingMoreGoods == null) {
            apiLoadingMoreGoods = $downloadMoreButton.loadingMoreGoods({
                loadingClass: "loader",
                autoDownload: true,
                autoDownloadOffset: EVENT_PRODUCT_HEIGHT * 3,
                events: {
                    afterDownload: function () {
                        afterLoadingMoreGoods(true);
                    }
                }
            }).data('loadingMoreGoods');
            console.log("event initDownloadMore");
        }*/
    }

    //initDownloadMore();

    filterBar.find('.filter-select').on('click', function () {
        filterBar.find('.filter-field').collapse('toggle');
    });

    mainContainer.filterableGoods({
        filter: filterBar,
        events: {
            beforeSend: function () {
                filterBar.find('.filter-field').collapse('hide');

                var offset = filterBar.offset().top;
                var animateData = isNaN(offset) ? {} : {scrollTop: offset};
                $('html, body').animate(animateData, 50, 'linear');
            },
            done: function (triggerName) {
                /*if (apiLoadingMoreGoods) {
                    apiLoadingMoreGoods.destroy();
                    apiLoadingMoreGoods = null;
                }*/

                afterLoadingMoreGoods(true);
                //initDownloadMore();
                $(window).trigger('scroll'); //laziLoad иногда не отрабатывает при статре
            },
            error: function () {
                //alert('Фильтр не применён, проверьте подключение к интренету');
            }
        }
    });

    function afterLoadingMoreGoods(imagesLazyloadReload) {
        if (imagesLazyloadReload && !!Mamam && !!Mamam.imagesLazyload) {
            Mamam.imagesLazyload.initialize('img[data-lazyload]:not([data-loaded])'); //download new images
        }
    }

    function addCart(formData) {
        Mamam.trackEvent('cart', 'add', 'Добавление в корзину в списке товаров', formData['productId']);
        Mamam.cart.add(formData['eventId'], formData['productId'], formData['size'], formData['number'], formData['token'], function (data) {
            if (data.message == 'low_warehouse') {
                alert(I18n('Sorry, the item has just been reserved for another member.'));
            } else if (data.message == 'inactive_event') {
                alert(I18n('Sorry, but the stock is over.'));
            } else if (data.message == 'cart_is_full') {
                alert(I18n('Your shopping cart is full. One order can contain a maximum of 20 items.') +
                    I18n('Please make another order if you want to buy more than 20 items.'));
            } else if (data.message == 'unknown_error') {
                alert(I18n('An unknown error occurred. Try refresh the page and add again.'));
            } else if (data.message == 'network_error') {
                alert(I18n('Network error. Check the internet connection and try adding the item again.'));
            } else if (data.message == 'cart_block') {
                alert(I18n('Adding products is blocked.'));
            } else if (data.message == 'not_authorized') {
                alert(I18n('To make purchases you need to register.') + ' ' +
                    I18n('Now we will redirect you to the registration page. Registration takes only 20 seconds of your time!'));
                window.location = Mamam.createUrl('auth/auth', {name: 'registration'});
            }

        }, function () {
            Mamam.ecommerce.add(formData['eventId'] + '/' + formData['productId'], formData['number']);
        }, true);
    }

    $(document).on('click', '.product-link-details[data-product-id]', function (e) {
        e.preventDefault();

        var url = this.href, navigate = function () {
            document.location = url;
        };
        Mamam.ecommerce.click($(this).data('product-id'), navigate);
        setTimeout(navigate, 1000)
    });

    var viewChangedTimer;

    function onViewChanged() {
        if (viewChangedTimer) return;

        viewChangedTimer = setTimeout(function () {
            Mamam.visibility.eventOnFirstSeen('.product-link-details[data-product-id]', 'mamam.product.impressions', 0.75);
            viewChangedTimer = null;
        }, 200);
    }

    $(document).on('scroll', onViewChanged);
    $(window).on('resize', onViewChanged);
    $(window).on('touchmove', onViewChanged);

    onViewChanged();

    $('.in-my-cart').on('click', function (e) {
        e.preventDefault();
        var formData = {
            eventId: $(this).attr('data-event-id'),
            productId: $(this).attr('data-product-id'),
            size: '',
            number: $(this).attr('data-number'),
            token: $(this).attr('data-token')
        };
        addCart(formData);
    });

    $('.main-container select[name=size]').on('change', function (e) {
        if (this.selectedIndex == 0) {
            return;
        }

        var option = this.options[this.selectedIndex];
        var formData = {
            eventId: option.getAttribute('data-event-id'),
            productId: option.getAttribute('data-product-id'),
            size: this.value,
            number: option.getAttribute('data-number'),
            token: option.getAttribute('data-token')
        };
        addCart(formData);
    });
});
