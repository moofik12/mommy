$(function () {
    $('.footer-title').on('click', function (e) {
        var activeFooterMenu1 = $('.footer-title.active');
        if (!$(this).hasClass("active")) {
            activeFooterMenu1.removeClass("active");
            $(this).addClass("active");

            $(this).find('.footer-submenu li').show();
            activeFooterMenu1.find('.footer-submenu li').hide();
        }
        else {
            $(this).removeClass("active");
            activeFooterMenu1.find('.footer-submenu li').hide();
        }
    });
});


