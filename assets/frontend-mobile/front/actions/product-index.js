$(function () {
    var $form = $('form.card-form'),
        $submitBtn = $form.find('input[type=submit]');

    $form.on('submit', function (e) {
        e.preventDefault();

        var formData = $(this).serializeObject();

        $submitBtn.attr('disabled', 'disabled');

        if (!$.mask.isLoaded()) {
            $submitBtn.expose({
                loadSpeed: 30,
                closeSpeed: 100,
                closeOnEsc: false,
                closeOnClick: false
            });
        }

        Mamam.trackEvent('cart', 'add', 'Добавление в корзину на странице товара', formData['productId']);
        Mamam.cart.add(formData['eventId'], formData['productId'], formData['size'], formData['number'], formData['token'], function (data) {
            if ('low_warehouse' === data.message) {
                alert(I18n('Sorry, the item has just been reserved for another member.'));
            } else if ('inactive_event' === data.message) {
                alert(I18n('Sorry, but the stock is over.'));
            } else if ('cart_is_full' === data.message) {
                alert(I18n('Sorry, your shopping cart is full. One order can contain only 20 items.') +
                    I18n('Please make another order if you want to buy more than 20 items..'));
            } else if ('unknown_error' === data.message) {
                alert(I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
            } else if ('network_error' === data.message) {
                alert(I18n('Network error. Check your internet connection and try adding again.'));
            } else if ('cart_block' === data.message) {
                alert(I18n('Sorry, adding items has been blocked.'));
            } else if ('not_authorized' === data.message) {
                alert(I18n('To make purchases you need to register.') + ' ' +
                    I18n('Now we will redirect you to the registration page. Registration takes only 20 seconds of your time!'));
                $submitBtn.removeAttr('disabled');
                $.mask.close();
                window.location = Mamam.createUrl('auth/auth', {name: 'registration'});
            }

            $submitBtn.removeAttr('disabled');
            $.mask.close();

        }, function () {
            Mamam.ecommerce.add(formData['eventId'] + '/' + formData['productId'], formData['number']);
        });
    });

    $form.on('change', '.form-number', function () {
        var $this = $(this);
        var value = parseInt($this.attr('data-value'));

        if (0 === value) {
            $submitBtn.attr('disabled', 'disabled');
        } else {
            $submitBtn.removeAttr('disabled');
        }
    });

    var $formPrice = $form.find('.form-price'),
        $price = $formPrice.find('.new-price .number'),
        $priceOld = $formPrice.find('.old-price'),
        $sale = $formPrice.find('.sale'),
        $number = $form.find(".form-number");

    var $select = $form.find('.form-size select');

    $select.on('change', function () {
        var selected = this.options[this.selectedIndex];
        var $selected = $(selected);

        var storage = $selected.data();

        $price.text(Mamam.formatCurrency(storage.price, storage.currency));
        $priceOld.text(Mamam.formatCurrency(storage.priceOld, storage.currency));
        $sale.text('-' + storage.discount + '%');

        var max = Math.min(storage.numberAvailable, storage.maxPerBuy);
        $number.attr("data-max", max);
        $number.find(":input:visible").trigger("change");
    });

    $select.trigger('change');

    $('ul.product-info-list li > a').on('click', function (e) {
        e.preventDefault();
        var $this = $(this), $item = $this.parent();
        if ($item.hasClass('active')) {
            $item.children('ul').hide();
            $item.removeClass('active');
        }
        else {
            $item.addClass('active');
            $item.children('ul').show();
        }
    });
});
