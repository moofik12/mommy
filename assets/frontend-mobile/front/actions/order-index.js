$(function () {
    $(document).on('countdown-finished', '.container', function (e) {
        var $target = $(e.target);
        var $td = $target.closest('.reserve');

        $td.addClass('cross-out');
        $td.siblings('.price').find('.price-inner-wrapper').addClass('cross-out');

        var $div = $target.closest('.cart-time-info');
        $div.addClass('cross-out');
    });

    var $supreme = $('.cart');

    $supreme.on('click', '[data-is-update-btn]', function (e) {
        e.preventDefault();

        var $this = $(this),
            $item = $this.closest('.cart-detail-position');

        var itemData = $item.data();

        Mamam.trackEvent('cart', 'update', 'Продление резерва');

        Mamam.cart.update(itemData.positionEvent, itemData.positionProduct, itemData.positionSize, itemData.positionToken, function (data) {
            if (data.message == 'low_warehouse') {
                alert(I18n('Sorry, the item has just been reserved for another member.'));
            } else if (data.message == 'inactive_event') {
                alert(I18n('Sorry, but the stock is over.'));
            } else if (data.message == 'cart_is_full') {
                alert(I18n('Sorry, your shopping cart is full. One order can contain only 20 items.') + ' ' +
                    I18n('Please make another order if you want to buy more than 20 items.'));
            } else if (data.message == 'unknown_error') {
                alert(I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
            } else if (data.message == 'network_error') {
                alert(I18n('Network error. Check your internet connection and try adding again.'));
            } else if (data.message == 'wrong_number') {
                alert(I18n('Sorry, you can not add a product with this quantity.'));
            } else if (data.message == 'not_authorized') {
                alert(I18n('Sorry, you need to log in to make purchases.'));
            }

        }, function () {
            $.ajax({
                cache: false,
                success: function (html) {
                    $('.cart').html(html);
                }
            });

            Mamam.cart.reloadQuickCart();
        }, false);
    });
});
