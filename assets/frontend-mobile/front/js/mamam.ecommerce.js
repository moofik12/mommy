(function (Mamam, dataLayerName, currencyCode) {
    function Products() {
        this.cache = {};
    }

    Products.prototype.setCache = function (cache) {
        this.cache = cache;
    };

    Products.prototype.store = function (product) {
        this.cache[product.id] = product;
    };

    Products.prototype.retrieve = function (id) {
        return this.cache[id];
    };

    function ECommerce() {
        this.products = new Products();
    }

    var actions = {
        impressions: 'Product Impressions',
        click: 'Product Clicks',
        detail: 'Product Detail Impressions',
        add: 'Add to Cart',
        remove: 'Remove from Cart',
        promotions: 'Promotion Impressions',
        promoClick: 'Promotion Clicks',
        checkout: 'Checkout',
        purchase: 'Purchases',
        refund: 'Refunds'
    };

    var pushToDataLayer = function (action, products, actionField, eventCallback) {
        var container = {
            "event": "ecommerce." + action,
            "ecommerce": {},
            'gtm-ee-event-action': actions[action],
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-non-interaction': ('impressions' === action)
        }, actionData = {};

        if ("function" === typeof eventCallback) {
            container.eventCallback = eventCallback;
        }

        if (null !== Mamam.currencyCode) {
            container.ecommerce.currencyCode = Mamam.currencyCode;
        }

        if (actionField) {
            actionData.actionField = actionField;
        }

        if ('impressions' === action) {
            actionData = products;
        } else {
            actionData.products = products;
        }

        container.ecommerce[action] = actionData;

        return window[dataLayerName].push(container);
    };

    var fetchProducts = function (ecommerce, list, quantity) {
        var products = [];
        list = list || [];

        for (var i = 0; i < list.length; i++) {
            var p = ecommerce.products.retrieve(list[i]);
            if (!p) {
                console.warn('Product id ' + list[i] + ' not found');

                continue;
            }

            if (quantity) {
                p.quantity = quantity;
            }

            products.push(p);
        }

        return products;
    };

    ECommerce.prototype.impressions = function (list, callback) {
        pushToDataLayer('impressions', fetchProducts(this, list), null, callback);
    };

    ECommerce.prototype.click = function (id, callback) {
        pushToDataLayer('click', fetchProducts(this, [id]), null, callback);
    };

    ECommerce.prototype.detail = function (id, callback) {
        pushToDataLayer('detail', fetchProducts(this, [id]), null, callback);
    };

    ECommerce.prototype.add = function (id, quantity, callback) {
        pushToDataLayer('add', fetchProducts(this, [id], quantity), null, callback);
    };

    ECommerce.prototype.remove = function (id, quantity, callback) {
        pushToDataLayer('remove', fetchProducts(this, [id], quantity), null, callback);
    };

    ECommerce.prototype.checkout = function (list, step, callback) {
        pushToDataLayer('checkout', fetchProducts(this, list), {step: step}, callback);
    };

    ECommerce.prototype.purchase = function (list, orderId, callback) {
        pushToDataLayer('purchase', fetchProducts(this, list), {id: orderId}, callback);
    };

    Mamam.ecommerce = new ECommerce();
    Mamam.currencyCode = currencyCode || null;
})(Mamam, 'dataLayer', Mamam.currencyCode);
