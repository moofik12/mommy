(function ($) {
    $.fn.filterableGoods = function (settings) {
        var TRIGGER_EVENTS = {
            DEFAULT: "default",
            SUBMIT: "submit",
            RESET: "reset",
            SORT: "sort",
            FROM_HISTORY: "history"
        };

        var events = $.extend({
            success: $.noop,
            error: $.noop,
            done: $.noop,
            beforeSend: $.noop
        }, settings.events);

        var filterVisualizationItemTemplate = '<div class="filter-item" data-value="<%= value %>" data-label="<%= label %>"><div class="title-middle">' +
            '<span><%= label %></span></div><i class="icon-filter-remove"></i><input type="hidden" name="<%= name %>" value="<%= value %>"></div>';
        var hashMapFiltersMatch = {};

        return this.each(function () {
            var $this = $(this);
            var _enableHistory = typeof window.history !== "undefined" && !!window.history.pushState;

            var GOODS_CLASS_SELECTOR = '.goods-list';
            var PAGINATION_CLASS_SELECTOR = '.goods-pagination';
            var DUMMY_NO_ITEMS = '.list-no-items';
            var BUTTON_UP = '.up-btn';

            var $content = $this.find('.main');
            var $filter = settings.filter ? settings.filter : $this.find('#filter-block');
            var $filters = $filter.find('.filter-bar');
            var $filterInfo = $filters.find('.filter-select');
            var $forms = $filter.find('form');
            var $goods = $content.find(GOODS_CLASS_SELECTOR);
            var _filterValuesNodes = {};
            var _filterLabels = [];
            var _filterSortLabel = '';

            var filterNames = _.uniq($filter.find(':input').map(function (key, item) {
                if (item.nodeName == 'BUTTON') {
                    return "";

                } else if (item.nodeName == 'SELECT') {
                    $(item).children().each(function (key, option) {
                        fillHashMapFiltersMatch(item, option, option.value);
                    });
                    return item.name;
                }

                //побочноя логика чтобы не обходить снова
                fillHashMapFiltersMatch(item);
                return item.name;
            }));
            filterNames = _.filter(filterNames, function (value) {
                return !!value;
            });

            _.each(filterNames, function (name) {
                try {
                    _filterValuesNodes[name] = $('select[name=' + name + ']').closest('.filter-label').siblings('.filter-items');
                } catch (e) {
                    throw e;
                }
            });

            function _updateFilterInfo() {
                var labels = _filterLabels.slice(0);
                if (_filterSortLabel) {
                    labels.unshift(_filterSortLabel);
                }

                if (labels.length == 0) {
                    $filters.removeClass('selected');
                    $filterInfo.html('');
                    return;
                }

                $filters.addClass('selected');
                $filterInfo.html('<div>(' + labels.join(',') + ')</div>');
            }

            function fillHashMapFiltersMatch(keyNode, valueNode, keyNodeValue, valueNodeValue) {
                valueNode || (valueNode = keyNode);
                keyNodeValue || (keyNodeValue = keyNode.value);
                valueNodeValue || (valueNodeValue = valueNode.value);

                hashMapFiltersMatch[getHashKeyFromInputNode(keyNode, keyNodeValue)] = getMatchStringFromInputNode(valueNode, valueNodeValue);
            }

            function getHashKeyFromInputNode(node, value) {
                value || (value = node.value);
                return getHashKey(node.name, value);
            }

            function getHashKey(name, value) {
                return name + '-' + value;
            }

            function makeFilterValueNode(name, value, label) {
                return _.template(filterVisualizationItemTemplate)({name: name, value: value, label: label});
            }

            function getMatchStringFromInputNode(node, value) {
                value || (value = node.value);

                //если мачиться на id то jquery не найден селектор типа #brand-М.Eight :(
                return node.id
                    ? '[id=\"' + node.id + '\"]'
                    : '[name=\"' + node.name + '\"][value=\"' + value + '\"]';
            }

            function onPopState(event) {
                removeFilters();
                restoreCheckedFilterFromQuery(true);
                refreshData(false, TRIGGER_EVENTS.FROM_HISTORY);
            }

            function saveFiltersData(data) {
                var searchQuery = getQueryStringFromData(data);

                if (!_enableHistory) {
                    window.location.search = searchQuery;
                } else {
                    window.history.pushState({'filter': data}, null, '?' + searchQuery)
                }
            }

            function removeFilters() {
                for (var name in _filterValuesNodes) {
                    if (!_filterValuesNodes.hasOwnProperty(name)) {
                        continue;
                    }

                    _filterValuesNodes[name].html('');
                }

                $forms.find('option[name!=""][disabled]:not([selected])').attr('disabled', false);
                $forms.find('input[type="radio"][value=""]').each(function (key, node) {
                    node.checked = true;
                });
                _filterLabels = [];
                _filterSortLabel = '';
                _updateFilterInfo();
            }

            function getQueryStringFromData(data) {
                var nowData = Mamam.getObjectFromQueryString(window.location.search);
                for (var i in nowData) {
                    if (!nowData.hasOwnProperty(i)) {
                        continue;
                    }

                    if (_.indexOf(filterNames, i) >= 0) {
                        delete nowData[i];
                    }
                }

                //delete data["page"];
                for (var l in data) {
                    if (!data.hasOwnProperty(l)) {
                        continue;
                    }

                    if (data[l] === "") {
                        delete data[l];
                    }
                }

                return $.param($.extend({}, nowData, data));
            }

            var ajaxHandle = null;
            var filter = function (url, data, success, error, done, triggerEvent) {
                url = url ? url : window.location;
                success = (success || $.noop);
                error = (error || $.noop);
                done = (done || $.noop);
                triggerEvent = (triggerEvent || TRIGGER_EVENTS.DEFAULT);

                if (!!ajaxHandle && !!ajaxHandle['abort']) {
                    ajaxHandle.abort();
                    console.log('abort');
                }

                //$(document).mask({ closeOnEsc: false, closeOnClick: false });
                if (!$.mask.isLoaded()) {
                    $filter.expose({
                        loadSpeed: 30,
                        closeSpeed: 100,
                        closeOnEsc: false,
                        closeOnClick: false
                    });
                }

                events.beforeSend.call(this);

                ajaxHandle = $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    cache: true
                }).success(function (result) {
                    success.call(this, result);
                    events.success.call(this, result, triggerEvent);

                }).error(function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call();
                        events.error.call();
                    }

                    done.call();
                }).then(function () {
                    // nothing
                }).always(function () {
                    ajaxHandle = null;
                    $.mask.close();
                    events.done.call(triggerEvent);

                    if (!!Mamam && !!Mamam.imagesLazyload) {
                        Mamam.imagesLazyload.initialize(); // reload
                    }
                });
            };

            var refreshData = function (pushFilterDataToHistory, triggerEvent) {
                pushFilterDataToHistory = typeof pushFilterDataToHistory === "undefined" ? true : pushFilterDataToHistory;

                var $forms = $filter.find('form input');
                var formData = $.extend(formData, $forms.not("select").serializeObject());
                formData['page'] = 1;

                if (pushFilterDataToHistory) {
                    saveFiltersData(formData);
                }

                if (!_enableHistory) {
                    return;
                }

                filter(null, null, function (html) {
                    $(DUMMY_NO_ITEMS).remove();
                    $goods.empty();
                    $(html).find(GOODS_CLASS_SELECTOR).children().appendTo($goods);
                    var buttonUp = $(BUTTON_UP);
                    var dummyNoItems = $(html).find(DUMMY_NO_ITEMS);
                    if (dummyNoItems.length > 0) {
                        $goods.before(dummyNoItems);
                    }
                    var paginationHtml = $(html).find(PAGINATION_CLASS_SELECTOR);
                    var $paginator = $content.find(PAGINATION_CLASS_SELECTOR);

                    if ($paginator) {
                        $paginator.replaceWith(paginationHtml.length ? paginationHtml[0] : '');
                    }
                    var fetchButtonUp = $(html).find(BUTTON_UP);
                    if (fetchButtonUp.length) {
                        buttonUp.replaceWith(fetchButtonUp[0]);
                    }
                }, function () {
                }, function () {
                }, triggerEvent);
            };

            function _removeDisplayFilterItem(item) {
                if (!item) {
                    return;
                }

                item = $(item);
                var value = item.data('value');
                var label = item.data('label');

                var option = $filter.find('option[value="' + decodeURI(value) + '"]');
                if (option) {
                    option.attr('disabled', false);
                }

                if (label) {
                    var index = _.indexOf(_filterLabels, label.toString());
                    if (index >= 0) {
                        _filterLabels.splice(index, 1);
                    }
                }
                item.remove();
            }

            function _addSortLabelFromNode(node) {
                if (node.name !== 'sort') {
                    return;
                }

                if (node.value.length == 0) {
                    _filterSortLabel = '';
                    return;
                }

                if (node.id) {
                    var labels = $('label[for="' + node.id + '"]');
                    if (labels.length > 0) {
                        var matches = labels.text().match(/.[^\t\n]*/);
                        if (matches !== null) {
                            _filterSortLabel = matches[0];
                        }
                    }
                }
            }

            function restoreCheckedFilterFromQuery(refreshAllFilters) {
                if (refreshAllFilters) {
                    $forms.find('input[checked]').each(function (i, input) {
                        input.checked = true;
                    });
                }

                _filterLabels = [];

                function _checkNodeFromMatch(name, match) {
                    if (!match) {
                        return;
                    }

                    var node = $(match)[0];
                    if (!node) {
                        return;
                    }

                    if (node.nodeName == 'OPTION' && _filterValuesNodes[name] && name != 'sort') {
                        if (_filterValuesNodes[name]) {
                            var text = jQuery.text(node);
                            var itemValue = makeFilterValueNode(name, node.value, text);
                            _filterValuesNodes[name].append(itemValue);
                            _filterLabels.push(text);
                            $(node).attr('disabled', 'disabled');
                        }
                    } else {
                        node.checked = true;
                        _addSortLabelFromNode(node);
                    }
                }

                //restore filter from search params
                var nowData = Mamam.getObjectFromQueryString(window.location.search);
                for (var name in nowData) {
                    if (!nowData.hasOwnProperty(name)) {
                        continue;
                    }

                    if (_.indexOf(filterNames, name) >= 0) {
                        if ($.isArray(nowData[name])) {
                            $.each(nowData[name], function (i, value) {
                                var match = hashMapFiltersMatch[getHashKey(name, value)];
                                _checkNodeFromMatch(name, match);
                            });

                            continue;
                        }

                        var match = hashMapFiltersMatch[getHashKey(name, nowData[name])];
                        if (match) {
                            _checkNodeFromMatch(name, match);
                        }
                    }
                }

                _updateFilterInfo();
            }

            //EVENTS
            $filter.delegate('.filter-none', 'click', function (e) {
                e.preventDefault();
                var $this = $(this);
                $this.closest('form')[0].reset();
            });

            $filter.on('click', '.icon-filter-remove', function () {
                var items = $(this).closest('.filter-item');
                if (items) {
                    var item = $(items[0]);
                    _removeDisplayFilterItem(item);
                }
                _updateFilterInfo();
            });

            $filter.delegate('form', 'reset', _.debounce(function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                var hasFilters = _filterLabels.length > 0;
                removeFilters();
                refreshData(hasFilters, TRIGGER_EVENTS.RESET);

                return false;
            }, 300));

            $filter.delegate('form select', 'change', _.debounce(function (e, prev, current) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                if (this.nodeName != 'SELECT') {
                    return;
                }

                var value = this.value;
                var name = this.name;
                var option = $(this).find('[value="' + value + '"]');

                if (name == 'sort') {
                    $filter.find("input[name='" + name + "']").val(value);
                    $filter.find(".extselect-input").text(option.text());
                    option.attr('selected', 'selected');
                    return;
                }

                if (option) {
                    option.attr('disabled', 'disabled');
                }
                if (!_filterValuesNodes[name]) {
                    return;
                }

                var label = option ? jQuery.text(option) : value;
                var itemValue = makeFilterValueNode(name, value, label);
                var multiple = $(this).data('multiple');

                if (!multiple) {
                    _filterValuesNodes[name].children().each(function (key, item) {
                        _removeDisplayFilterItem(item);
                    });
                }

                _filterValuesNodes[name].append(itemValue);
                this.value = '';
                _filterLabels.push(label);
                _updateFilterInfo();
            }, 300));

            $(document).on('click', '[data-remove-all]', function () {
                $filter.find('form').trigger('reset');
            });

            var eventRefreshData = _.debounce(function (triggerEvent) {
                refreshData(true, triggerEvent);
            }, 300);

            $filter.on('submit', 'form', function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                eventRefreshData(TRIGGER_EVENTS.SUBMIT);
                return false;
            });

            $filter.delegate('form select[name="sort"]', 'change', function (e) {
                eventRefreshData(TRIGGER_EVENTS.SORT);
                _addSortLabelFromNode(e.target);
                _updateFilterInfo();
            });

            //init
            (function init() {
                if (_enableHistory) {
                    $(window).on('popstate', onPopState);
                }

                restoreCheckedFilterFromQuery();
            })();
        });
    }
})(jQuery);
