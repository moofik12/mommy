(function (window, $) {
    var Mamam = {};

    Mamam.baseUrl = Mamam.baseUrl || window.location.protocol + '//' + window.location.host;
    Mamam.urlFormat = Mamam.urlFormat || 'path';

    Mamam.timeClient = Mamam.timeClient || (new Date());
    Mamam.timeServer = Mamam.timeServer || (new Date());

    Mamam.modalsContainerSelector = '.modals:first';

    Mamam.currency = Mamam.currency || '$';

    Mamam.timeClientAndServerDiff = function () {
        return Mamam.timeClient - Mamam.timeServer;
    };

    Mamam.format = function (string) {
        var args = _.toArray(arguments).slice(1);
        return string.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined'
                ? args[number] : match;
        });
    };

    Mamam.createUrl = function (route, params) {
        params || (params = {});

        var url = this.baseUrl;
        if (this.urlFormat === 'path') {
            url += '/' + route + '?' + $.param(params);
        } else {
            params['r'] = route;
            url += '?' + $.param(params);
        }

        return url;
    };

    Mamam.plural = function (n, str1, str2, str3) {
        function plural(a) {
            if (a % 10 === 1 && a % 100 !== 11) return 0;
            else if (a % 10 >= 2 && a % 10 <= 4 && (a % 100 < 10 || a % 100 >= 20)) return 1;
            else return 2;
        }

        switch (plural(n)) {
            case 0:
                return str1;
            case 1:
                return str2;
            default:
                return str3;
        }
    };

    //pads left
    Mamam.lpad = function (str, padString, length) {
        str = str.toString();
        while (str.length < length) {
            str = padString + str;
        }
        return str;
    };

    //pads right
    Mamam.rpad = function (str, padString, length) {
        str = str.toString();
        while (str.length < length) {
            str = str + padString;
        }
        return str;
    };

    Mamam.formatCurrency = function (number, currency) {
        var decimals, decimalPoint, thousandsSeparator, format;

        switch (currency) {
            case 'idr':
                decimals = 0;
                decimalPoint = '.';
                thousandsSeparator = ',';
                format = '{currency} {value}';
                break;
            case '₫':
                decimals = 0;
                decimalPoint = ',';
                thousandsSeparator = ' ';
                format = '{value} {currency}';
                break;
            default:
                decimals = 2;
                decimalPoint = '.';
                thousandsSeparator = ' ';
                format = '{value} {currency}';
        }

        if (decimals > 0) {
            number = (+number)
                .toFixed(decimals)
                .replace(/(\d)(?=(\d{3})+\.)/g, '$1' + thousandsSeparator)
                .replace(/\.?0+$/g, '')
                .replace('.', decimalPoint);
        } else {
            number = (+number)
                .toFixed(0)
                .replace(/(\d)(?=(\d{3})+$)/g, '$1' + thousandsSeparator)
        }


        if (!currency) {
            return number;
        }

        return format.replace('{value}', number).replace('{currency}', currency);
    };

    Mamam.loadImage = function (src, afterLoad) {
        var image = new Image();

        var $image = $(image);
        var loadCalled = false;

        var hTimeout = setTimeout(function () {
            $image.trigger('load');
        }, 5000); //считать что файл загружается максимум за 5сек

        $image.attr('src', src).on('load', function () {
            clearTimeout(hTimeout);
            if (!loadCalled && $.isFunction(afterLoad)) {
                afterLoad.call(window);
            }
            loadCalled = true;
            $image.off('load');
        });

        if ($image.get(0).complete) {
            setTimeout(function () { // emulate async
                $image.trigger("load");
            }, 100);
        }
    };

    Mamam.modalAuth = function (callback, isOffer) {
        var setTop = function ($modal) {
            var top = ($(window).height() - $modal.height()) / 2;

            $modal.css({
                top: 0,
                marginTop: top
            });
        };

        var isLoaded = true;
        var $modal = $(Mamam.modalsContainerSelector).children('.modal.login.general');

        isOffer = isOffer === undefined ? false : !!isOffer;

        if ($modal.length === 0) { // create modal container for login panel
            isLoaded = false;
            $modal = $('<div class="modal hide login general"><div class="modal-wrapper"></div></div>')
                .appendTo(Mamam.modalsContainerSelector);
        }

        if (isOffer) {
            $modal.addClass('offer');
        } else {
            $modal.removeClass('offer');
        }

        if (isLoaded) {
            setTop($modal);
            callback.call($modal, $modal);
            return;
        }

        var $modalWrapper = $modal.find('.modal-wrapper');

        $.ajax({
            url: Mamam.createUrl('auth/auth', {name: 'login'}),
            success: function (result) {
                var $result = $('<div>' + result + '</div>');

                var $login = $('.login-conteiner', $result);
                var $policy = $('.modal.policy-box', $result); // policy is already a modal
                var $loginSocial = $('.modal.login-social-end', $result); // finish social login is already a modal
                var $scripts = $('script', $result);

                $login.appendTo($modalWrapper); // insert login panel
                $policy.appendTo(Mamam.modalsContainerSelector); // insert policy modal

                setTop($loginSocial);
                $loginSocial.addClass('login').appendTo(Mamam.modalsContainerSelector); // insert policy modal

                // connect scripts
                var $availableScripts = $('script');
                var availableScripts = [];

                var getScriptSource = function ($script) {
                    return $script.attr('src') ||
                        $script.get(0).innerHTML ||
                        $script.get(0).innerText ||
                        '';
                };

                $availableScripts.each(function () {
                    var $this = $(this), source = getScriptSource($this);
                    if (source.length > 0) {
                        availableScripts.push(getScriptSource($this));
                    }
                });

                $scripts.each(function () {
                    var $this = $(this);
                    if (availableScripts.indexOf(getScriptSource($this)) === -1) {
                        $this.appendTo('body');
                    }
                });

                setTop($modal);
                callback.call($modal, $modal);
            }
        });
    };

    //для поддержки IE
    Mamam.oauth = function (messageObj) {
        $(window.document).trigger("oauth", messageObj);
    };

    Mamam.getCookies = function () {
        var pairs = window.document.cookie.split(";");
        var cookies = {};
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split("=");
            cookies[pair[0]] = decodeURIComponent(pair[1]);
        }
        return cookies;
    };

    Mamam.getActiveSplitTests = function () {
        var cookies = this.getCookies();
        var splitTestPrefix = 'splittest-';
        var splitTests = {};

        _.each(cookies, function (value, key) {
            if (key.substr(1, splitTestPrefix.length) === splitTestPrefix) {
                splitTests[key] = value;
            }
        });

        return splitTests;
    };

    /**
     * @param text
     * @returns {{}}
     */
    Mamam.getObjectFromQueryString = function (text) {
        var pairs = text.replace(/^\?/, '').split("&"),
            obj = {};

        $.each(pairs, function (i, item) {
            if (item === "") {
                return;
            }

            var pair = $(item.split("=")).map(function (key, value) {
                return decodeURIComponent(value.replace(/\+/g, '%20'));
            });
            var key = pair[0].replace(/\[]$/, "");
            if (key !== pair[0]) {
                if (typeof obj[key] === "undefined") {
                    obj[key] = [];
                }

                obj[key].push(pair[1]);
            } else {
                obj[pair[0]] = pair[1];
            }

        });

        return obj;
    };

    window.Mamam = Mamam;
})(window, jQuery);
