(function ($) {
    //'use strict';
    $.fn.loadingMoreGoods = function (settings) {
        settings || (settings = {});

        var $element = $(this);

        var options = $.extend({
            pageSize: 20,
            pageCount: false,
            loadingClass: 'loading',
            pageVar: 'page',
            itemsSelector: '.goods-list',
            autoDownload: false,
            autoDownloadPause: $.noop,
            autoDownloadOffset: 0, //distance (px) to the block for start update
            currentPage: 1
        }, settings, $element.data());

        var events = $.extend({
            afterDownload: $.noop
        }, settings.events);

        var autoDownloadPause = $.isFunction(options.autoDownloadPause) ? options.autoDownloadPause : $.noop;
        var autoDownloadOffset = -parseInt(options.autoDownloadOffset, 10);

        function API() {
            var currentPage = options.currentPage,
                pagesDownloaded = false;

            var self = this;
            var ajaxHandle = null;

            var show = true;

            this.refresh = function () {
                currentPage = 1;
            };

            this.getCurrentPage = function () {
                return currentPage;
            };

            this.setCurrentPage = function (value) {
                if (value >= 0) {
                    currentPage = value;
                }
            };

            this.loadNextPage = function (url, ajaxSetting) {
                url || (url = window.location.href);

                var df = $.Deferred();
                var data = {};
                var loadingPage = currentPage + 1;
                data[options.pageVar] = loadingPage;

                if (!this.isHasNextPage()) {
                    return false;
                }

                var targetUrl = createUrl(url, data);
                var ajaxOptions = $.extend({}, ajaxSetting, {
                    type: "GET",
                    url: targetUrl
                });

                if (ajaxHandle && ajaxHandle['abort']) {
                    ajaxHandle.abort();
                }

                setDownloadClass(true);
                ajaxHandle = $.ajax(ajaxOptions)
                    .then(function (data) {
                            setCurrentPage(loadingPage);
                            pushDownloadedData(data);

                            if ($.isFunction(events.afterDownload)) {
                                events.afterDownload.call();
                            }

                            checkState();
                            df.resolve(arguments);
                        }
                        , function () {
                            df.reject(arguments);
                            alert('Ошибка получения данных новой страницы. Сообщите нам!');
                        }
                    )
                    .always(function () {
                        ajaxHandle = null;
                        setDownloadClass(false);
                    });

                return df.promise();
            };

            this.isHasNextPage = function () {
                return !pagesDownloaded;
            };

            this.isShow = function () {
                return !!show;
            };

            this.destroy = function () {
                $element.off('click', _eventON);
                if (options.autoDownload) {
                    $(window).off('scroll', autoDownloadEvent);
                }
            };

            function pushDownloadedData(data) {
                var $data = $('<div></div>').append(data),
                    $items = $data.find(options.itemsSelector);

                try {
                    var countDownloadItems = $items.children().length;

                    $(options.itemsSelector).append($items.children());
                    if (countDownloadItems < options.pageSize
                        || (options.pageSize !== false && options.pageSize <= currentPage)
                    ) {
                        pagesDownloaded = true;
                    }

                    return true;
                } catch (err) {

                    return false;
                }
            }

            function autoDownloadEvent() {
                if (!self.isHasNextPage() || !self.isShow() || autoDownloadPause()) {
                    return;
                }

                if (ajaxHandle == null && window.scrollY && window.innerHeight
                    && window.scrollY + windowInnerHeight() >= elementOffsetTop()
                ) {
                    _eventON();
                }
            }

            function fetchElementOffsetTop() {
                return $element[0].offsetTop + autoDownloadOffset;
            }

            function fetchWindowInnerHeight() {
                return window.innerHeight;
            }

            var elementOffsetTop = _.throttle(fetchElementOffsetTop, 50);
            var windowInnerHeight = _.throttle(fetchWindowInnerHeight, 100);

            function clearStates() {
                pagesDownloaded = show = false;
                currentPage = 1;
            }

            function checkState() {
                if (!pagesDownloaded) {
                    show = true;
                    $element.show();

                } else {
                    show = false;
                    $element.hide();
                }
            }

            function setCurrentPage(number) {
                currentPage = number;
            }

            function setDownloadClass(value) {
                if (value && !$element.hasClass(options.loadingClass)) {
                    $element.addClass(options.loadingClass);

                } else if ($element.hasClass(options.loadingClass)) {
                    $element.removeClass(options.loadingClass);
                }
            }

            function createUrl(url, params) {
                var elUrl = document.createElement('a');
                elUrl.href = url;

                var search = elUrl.search.replace(/^\?/, '');
                var data = {};

                $.each(search.split("&"), function (index, param) {
                    param = param.split("=");

                    if (param[0] && param[1]) {
                        data[param[0]] = param[1];
                    }
                });

                elUrl.search = $.param($.extend({}, data, params));

                return decodeURIComponent(elUrl.href);
            }

            function _eventON(event) {
                if (event && event.preventDefault) {
                    event.preventDefault();
                }

                self.loadNextPage();
                return false;
            }

            //INIT
            (function () {
                $element.on('click', _eventON);
                if (options.autoDownload) {
                    console.log('init scroll');
                    $(window).on('scroll', autoDownloadEvent);
                }
            })();
        }

        return this.each(function () {
            $(this).data('loadingMoreGoods', new API());
        });
    }
})(jQuery);
