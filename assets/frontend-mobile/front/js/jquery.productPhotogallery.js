(function ($) {
    $.fn.productPhotogallery = function () {
        return this.each(function () {
            var $this = $(this), $photogallery = $this;
            var $imageWrapper = $this.find('.image-wrapper');

            $imageWrapper.css({overflow: 'hidden'});

            var $imageList = $this.find('.image-list');

            var $lis = $imageList.children();

            if ($lis.length < 5) { // 5 - magic, max count of thumbs
                $(new Array(5 - $lis.length + 1).join('<li></li>')).appendTo($imageList);
            }

            $imageList.delegate('li', 'click', function (e) {
                var $this = $(this);
                var data = $this.data();

                if (!data.bigUrl) {
                    return; // do nothing with empty li's
                }

                $imageList.children().removeClass('active');
                $this.addClass('active');

                $image = $('<img>', {src: data.bigUrl, 'data-zoom-image': data.sourceUrl});
                $imageWrapper.empty();

                $image.appendTo($imageWrapper);

                Mamam.loadImage(data.bigUrl, function () {
                    $image.elevateZoom({
                        zoomType: "lens",
                        lensShape: "round",
                        containLensZoom: true,
                        lensSize: 250
                    });
                });
            });

            $imageList.children(':first-child').trigger('click');
        });
    }
})(jQuery);
