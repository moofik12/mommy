(function () {
    var I18n = function () {
        return I18n.invoke.apply(this, arguments);
    };

    I18n.dic = {};
    I18n.invoke = function (text) {
        return ('undefined' !== typeof I18n.dic[text]) ? I18n.dic[text] : text;
    };

    window.I18n = I18n;
})();
