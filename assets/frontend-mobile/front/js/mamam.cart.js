(function ($) {
    Mamam.cart = {
        $container: null,
        $timer: null,

        initialize: function (containerSelector, timerSelector) {
            this.$container = $(containerSelector);
            this.$timer = $(timerSelector);

            this._bindQuickCartEvents();
        },

        add: function (eventId, productId, size, number, token, error, success, redirectToOrder) {
            error || (error = $.noop);
            success || (success = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxAdd'),
                type: 'GET',
                data: {
                    eventId: eventId,
                    productId: productId,
                    size: size,
                    number: number,
                    token: token
                },
                success: function (result) {
                    if (result.success && redirectToOrder !== false) {
                        window.location.replace(Mamam.createUrl('cart'));
                    }

                    if (!result['success']) {
                        error.call(self, result);
                    } else {
                        success.call(self, result);
                    }

                    self.reloadQuickCart();
                },
                error: function (xhr) {
                    var ob = {
                        message: 'network_error',
                        success: false
                    };
                    console.log(xhr.statusText);
                    if ((xhr.statusText === 'Forbidden') || (xhr.statusText === 'HTTP/2.0 403')) {
                        ob.message = 'not_authorized';
                        error.call(self, ob);
                    } else if (xhr.statusText !== 'abort') {
                        error.call(self, ob);
                    }
                }
            })
        },

        update: function (eventId, productId, size, token, error, success, redirectToOrder) {
            error || (error = $.noop);
            success || (success = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxUpdate'),
                type: 'GET',
                data: {
                    eventId: eventId,
                    productId: productId,
                    size: size,
                    token: token
                },
                success: function (result) {
                    if (result.success && redirectToOrder !== false) {
                        window.location.replace(Mamam.createUrl('cart'));
                    }

                    if (!result['success']) {
                        error.call(self, result);
                    } else {
                        success.call(self, result);
                    }

                    self.reloadQuickCart();
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            })
        },

        getPromocodeDiscount: function (token, promocode, success, error) {
            success || (success = $.noop);
            error || (error = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxPromocodeDiscount'),
                type: 'GET',
                data: {
                    promocode: promocode,
                    token: token
                },
                success: function (result) {
                    if (!result['success']) {
                        error.call(self, result);
                    } else {
                        success.call(self, result);
                    }
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            })
        },

        _bindQuickCartEvents: function () {
            var self = this;

            var $quickCart = this.$container.find('.quick-cart');

            $quickCart.on('click mouseup', function (e) {
                e.stopPropagation();
            });

            $quickCart.on('click', '.foot-line .delete', function (e) {
                e.preventDefault();
                var $this = $(this), $item = $this.closest('.item'), $li = $this.closest('li'), $list = $li.parent();

                $li.remove();


                var $items = $list.children();

                self.$container.find('.cart .goods').text($items.length);
                if ($items.length == 0) {
                    $list.closest('.quick-cart').hide();
                    self.$timer.hide();
                }

                var totalPrice = 0.0;
                $items.each(function () {
                    var $item = $(this).find('.item');

                    if (!$item.data('isExpired')) {
                        var price = parseFloat($item.data('positionPrice')),
                            number = parseInt($item.data('positionNumber'));

                        totalPrice += price * number;
                    }
                });

                self.$container.find('.quick-cart-footer .price').text(Mamam.formatCurrency(totalPrice, Mamam.currency));

                $.ajax({
                    url: Mamam.createUrl('/cart/ajaxDelete'),
                    type: 'GET',
                    data: {
                        eventId: $item.data('positionEvent'),
                        productId: $item.data('positionProduct'),
                        size: $item.data('positionSize'),
                        success: function () {
                            var id = $item.data('positionEvent') + '/' + $item.data('positionProduct'),
                                quantity = $item.data('positionNumber');
                            Mamam.ecommerce.remove(id, quantity);
                        }
                    }
                });

            });
        },

        reloadQuickCart: function () {
            var self = this;
            var $container = this.$container;
            var $timer = this.$timer;

            $.ajax({
                url: Mamam.createUrl('cart/quickCart'),
                type: 'GET',
                cache: false,
                success: function (html) {
                    var $output = $(html);
                    var $containerReplace = $output.find($container.selector);

                    if ($containerReplace.length > 0) {
                        $container.html($containerReplace.html());
                    }
                    self._bindQuickCartEvents();
                }
            });
        },

        getList: function (error, success) {
            error || (error = $.noop);
            success || (success = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxList'),
                type: 'GET',
                dataType: 'json',
                cache: false,
                success: function (result) {

                    if (!result['success']) {
                        error.call(self, result['data']);
                    } else {
                        success.call(self, result);
                    }
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            });
        },

        reloadTopMenuCart: function () {
            var numGoods = parseInt($('#numGoods').text());
            if (numGoods > 0) {
                numGoods = numGoods - 1;
                $('#numGoods').text(numGoods);
            }
        }
    };
})(jQuery);
