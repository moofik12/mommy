(function ($) {
    Mamam.countdown = function (context, settings) {
        var $context = $(context);

        var options = $.extend({
            selector: 'time[data-countdown]',
            reloadTime: 3000,
            tick: 1000
        }, settings);

        var $elements;

        function getCountdownText($element) {
            var endDate = new Date(Date.parse($element.attr('datetime'))) - 1,
                nowDate = new Date() - Mamam.timeClientAndServerDiff();
            var timespan = countdown(nowDate, endDate);

            if (nowDate > endDate) {
                return I18n('ended');
            } else if (timespan.days >= 7) {
                return I18n('more than a week');
            } else if (timespan.days > 0) {
                return Mamam.format(
                    '{0}, {1}:{2}:{3}',
                    timespan.days + ' ' + Mamam.plural(timespan.days, I18n('day'), I18n('days'), I18n('days')),
                    Mamam.lpad(timespan.hours, '0', 2),
                    Mamam.lpad(timespan.minutes, '0', 2),
                    Mamam.lpad(timespan.seconds, '0', 2)
                );
            } else if (timespan.hours > 0) {
                return Mamam.format(
                    '{0} {1}:{2}',
                    timespan.hours + ' ' + Mamam.plural(timespan.hours, I18n('hour'), I18n('hours'), I18n('hours')),
                    Mamam.lpad(timespan.minutes, '0', 2),
                    Mamam.lpad(timespan.seconds, '0', 2)
                );
            } else if (timespan.minutes > 0) {
                return Mamam.format(
                    '{0} {1} ' + I18n('seconds'),
                    timespan.minutes + ' ' + Mamam.plural(timespan.minutes, I18n('minute'), I18n('minutes'), I18n('minutes')),
                    Mamam.lpad(timespan.seconds, '0', 2)
                );
            } else if (timespan.seconds > 0) {
                return Mamam.format(
                    '{0}',
                    timespan.seconds + ' ' + Mamam.plural(timespan.seconds, I18n('second'), I18n('seconds'), I18n('seconds')),
                    Mamam.lpad(timespan.seconds, '0', 2)
                );
            }
        }

        function reload() {
            $elements = $context.find(options.selector);

            $elements.each(function (index, value) {
                var $this = $(value);

                if (!$this.attr('data-countdown-connected')) {
                    $this.attr('data-countdown-connected', true);

                    var interval = setInterval(function () {
                        value.innerHTML = getCountdownText($this);

                        if ($this.text() === I18n('ended')) {
                            $this.attr('data-is-finished', true);
                            $this.trigger('countdown-finished');
                        }

                        if ($this.attr('data-is-finished')) {
                            clearInterval(interval);
                        }
                    }, options.tick);
                }
            }).trigger('countdown-tick');
        }

        reload();
        setInterval(reload, options.reloadTime);

        return this;
    }
})(jQuery);
