(function ($) {
    var ACCOUNT_RECEIVED = "accountReceived";
    var TOKEN_RECEIVED = "tokenReceived";

    var IOS = 0;
    var ANDROID = 1;
    var DESKTOP = 2;
    var CURRENT_OS = DESKTOP;

    var IOS_PREFIX = "application";
    var IOS_SEPARATOR = ":///";
    var IOS_ACTION_JSON_CART = "jsonCart";

    var App = {};

    App.IOS = IOS;
    App.ANDROID = ANDROID;
    App.DESKTOP = DESKTOP;

    App.init = function () {
        if (typeof(window.application) != 'undefined') {
            App.setOS(ANDROID);
        }
    };

    App.setOS = function (os) {
        CURRENT_OS = os;
    };

    App.getOS = function () {
        return CURRENT_OS;
    };

    var AppCart = {};

    AppCart.addItem = function (id, event, name, price, count, imgUrl) {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.addItem(id, event, name, price, count, imgUrl);
                break;
            case IOS:

                break;
        }
    };

    AppCart.downItem = function (id) {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.downItem(id);
                break;
            case IOS:

                break;
        }
    };

    App.callNumber = function (number) {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.callNumber(number);
                break;
            case IOS:

                break;
        }
    };

    AppCart.updateList = function (json) {
        json = JSON.stringify(json);

        switch (CURRENT_OS) {
            case ANDROID:
                window.application.updateCart(json);
                break;
            case IOS:
                window.lastData = json;
                var iframe = document.createElement('iframe');

                iframe.setAttribute('src', IOS_PREFIX + IOS_SEPARATOR + IOS_ACTION_JSON_CART);

                document.documentElement.getElementsByTagName('body')[0].appendChild(iframe);
                iframe.parentNode.removeChild(iframe);
                iframe = null;
                break;
        }
    };

    AppCart.remove = function (eventId, productId, size) {
        var rout = "cart/ajaxDelete";
        var params = {
            'eventId': eventId,
            'productId': productId,
            'size': size
        };

        $.ajax({
            url: Mamam.createUrl(rout, params),
            type: "get",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (json) {
                if (json['success']) {

                    Mamam.cart.getList(
                        function () {
                        },
                        function (result) {
                            AppCart.updateList(result['data']);
                        }
                    );

                }
            },
            error: function () {
            }
        });
    };

    AppCart.update = function (eventId, productId, size, token) {
        var rout = "cart/ajaxUpdate";
        var params = {
            'token': token,
            'eventId': eventId,
            'productId': productId,
            'size': size
        };

        $.ajax({
            url: Mamam.createUrl(rout, params),
            type: "get",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (json) {
                if (json['success']) {

                    Mamam.cart.getList(
                        function () {
                        },
                        function (result) {
                            AppCart.updateList(result['data']);
                        }
                    );

                }
            },
            error: function () {
            }
        });
    };

    App.setEventListener = function (event, callback) {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.onSetEventListener(event, callback.toString());
                break;
            case IOS:

                break;
        }
    };

    App.getAccountName = function () {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.getAccount();
                break;
            case IOS:

                break;
        }
    };

    App.getAccessToken = function () {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.getAccessToken();
                break;
            case IOS:

                break;
        }
    };

    App.log = function (message) {
        switch (CURRENT_OS) {
            case ANDROID:
                window.application.log(message);
                break;
            case IOS:

                break;
        }
    };

    App.afterLoad = function () {
        var os = Mamam.app.getOS();
        if (os == Mamam.app.ANDROID || os == Mamam.app.IOS) {
            $('.application-hide').hide();
            $('.application-show').show();
        } else {
            $('.application-hide').show();
            $('.application-show').hide();
        }
    };

    App.cart = AppCart;
    Mamam.app = App;


    Mamam.app.init();

})(jQuery);
