(function ($) {
    Mamam.imagesLazyload = {
        selector: 'img[data-lazyload]',

        initialize: function (selector) {
            this.selector = selector || this.selector;
            $(this.selector).lazyload({
                threshold: 450,
                data_attribute: 'src',
                effect: "fadeIn",
                appear: function () {
                    $(this).attr('data-loaded', 'true');
                    $(this).closest('.image-wrapper').removeClass('waiting');
                }
            });
        }
    }
})(jQuery);
