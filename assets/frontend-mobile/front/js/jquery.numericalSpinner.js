(function ($) {
    $.fn.numericalSpinner = function (settings) {
        var options = $.extend({
            downSelector: '.minus',
            upSelector: '.plus',
            textSelector: 'input[type=text]',
            valueSelector: 'input[type=hidden]',
            step: 1
        }, settings);

        return this.each(function () {
            var $this = $(this), $context = $this;
            var elements = {
                $up: $this.find(options.upSelector),
                $down: $this.find(options.downSelector),
                $text: $this.find(options.textSelector),
                $value: $this.find(options.valueSelector)
            };

            elements.$up.bind('click', function () {
                var max = parseInt($context.attr('data-max')),
                    min = parseInt($context.attr('data-min'));

                var val = parseInt(elements.$value.val()) + options.step;
                if (isNaN(val)) {
                    val = min;
                }

                val = Math.max(val, min);
                val = Math.min(val, max);

                elements.$text.val(val);
                elements.$value.val(val);

                $context.attr('data-value', val).trigger('change');
            });

            elements.$down.bind('click', function () {
                var max = parseInt($context.attr('data-max')),
                    min = parseInt($context.attr('data-min'));

                var val = parseInt(elements.$value.val()) - options.step;
                if (isNaN(val)) {
                    val = min;
                }

                val = Math.max(val, min);
                val = Math.min(val, max);

                elements.$text.val(val);
                elements.$value.val(val);

                $context.attr('data-value', val).trigger('change');
            });

            elements.$text.bind('keydown keyup keypress', function (e) {
                var zeroCode = '0'.charCodeAt(0),
                    nineCode = '9'.charCodeAt(0),
                    minusCode = '-'.charCodeAt(0);

                var charCode = e.charCode;
                if (charCode > 0 && (charCode < zeroCode || charCode > nineCode) && charCode !== minusCode) {
                    //e.keyCode = e.which = 0;
                    e.preventDefault();
                }
            });

            elements.$text.bind('change keyup', function () {
                var max = parseInt($context.attr('data-max')),
                    min = parseInt($context.attr('data-min'));

                var val = parseInt(elements.$text.val());
                if (isNaN(val)) {
                    val = min;
                }

                val = Math.max(val, min);
                val = Math.min(val, max);

                elements.$text.val(val);
                elements.$value.val(val);

                $context.attr('data-value', val).trigger('change');
            });

            if (elements.$value.val() === '') {
                elements.$text.change();
            }
        });
    }
})(jQuery);
