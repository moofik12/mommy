(function (Mamam) {
    function GaEvent(category, action, label, value) {
        this.category = category;
        this.action = action;
        this.label = label;
        this.value = value;
    }

    function GaEventQueue() {
        this.lastId = 0;
        this.queue = [];
        this.events = [];
    }

    GaEventQueue.prototype.push = function () {
        for (var i = 0; i < arguments.length; ++i) {
            var event = arguments[i];
            event.id = this.lastId++;

            this.events[event.id] = event;
            this.queue.push(event);
        }
    };

    GaEventQueue.prototype.dequeue = function () {
        var queue = this.queue;

        this.queue = [];

        return queue;
    };

    var GaEvents = new GaEventQueue();

    Mamam.trackEvent = function (category, action, label, value) {
        var event = (category.constructor === GaEvent) ? category : new GaEvent(category, action, label, value);

        GaEvents.push(event);

        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'gaEvent',
                'gaEvent.queue': GaEvents.queue,
                'gaEvent.lastId': GaEvents.lastId
            });
        }
    };

    Mamam.flushEvents = function () {
        return GaEvents.dequeue();
    };
})(Mamam);
