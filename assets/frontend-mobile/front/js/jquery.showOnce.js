/**
 * Created by zhenya on 11/2/16.
 */
+function ($) {
    'use strict';

    var ShowOnce = function (element, options) {
        this.$element = options.parent ? $(options.parent) : $(element);
        this.options = $.extend({}, ShowOnce.DEFAULTS, options);
        this.$target = $(this.options.target)
    };

    ShowOnce.DEFAULTS = {
        shown: false,
        hideClass: 'hide'
    };

    ShowOnce.prototype.show = function () {
        if (this.options.shown) {
            return
        }

        this.$target
            .removeClass(this.options.hideClass);
        this.$element.addClass(this.options.hideClass);
        this.options.shown = true
    };

    function getTargetFromTrigger($trigger) {
        var href;
        var target = $trigger.attr('data-target')
            || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, ''); // strip for ie7

        return $(target)
    }

    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var instance = $this.data('showOnce');
            var options = $.extend({}, ShowOnce.DEFAULTS, $this.data(), typeof option == 'object' && option);

            if (!instance) {
                $this.data('showOnce', (instance = new ShowOnce(this, options)))
            }

            instance.show()
        })
    }

    var old = $.fn.ShowOnce;

    $.fn.ShowOnce = Plugin;
    $.fn.ShowOnce.Constructor = ShowOnce;

    $.fn.ShowOnce.noConflict = function () {
        $.fn.ShowOnce = old;
        return this
    };


    // DATA-API
    $(document).on('click', '[data-toggle="showOnce"]', function (e) {
        var $this = $(this);
        var data = $this.data('showOnce');
        if (data && data.options && data.options.shown) {
            return;
        }

        if (!$this.attr('data-target')) e.preventDefault();

        var $target = getTargetFromTrigger($this);
        var option = $.extend({}, data, {target: $target});

        Plugin.call($this, option)
    })

}(jQuery);
