(function (Mamam, $) {
    var Visibility = {};

    Visibility.isVisible = function ($element, minPercent) {
        minPercent = minPercent || 1;

        var screen = {
            top: $(document).scrollTop(),
            left: $(document).scrollLeft(),
            width: $(window).width(),
            height: $(window).height()
        };

        var bound = $element.offset();

        bound.width = $element.width();
        bound.height = $element.height();

        var delta = {
            width: bound.width,
            height: bound.height
        };

        if (screen.left > bound.left) {
            delta.width -= screen.left - bound.left
        }
        if (screen.left + screen.width < bound.left + bound.width) {
            delta.width -= (bound.left + bound.width) - (screen.left + screen.width);
        }
        if (screen.top > bound.top) {
            delta.height -= screen.top - bound.top
        }
        if (screen.top + screen.height < bound.top + bound.height) {
            delta.height -= (bound.top + bound.height) - (screen.top + screen.height)
        }

        if (delta.width < 0 || delta.height < 0) {
            return false;
        }

        bound.square = bound.width * bound.height;
        delta.square = delta.width * delta.height;

        return (minPercent <= delta.square / bound.square);
    };

    Visibility.eventOnFirstSeen = function (selector, eventName, minPercent) {
        var firstSeenElements = [];

        $(selector).each(function () {
            var $element = $(this);
            if ('yes' === $element.data('seen')) {
                return;
            }
            if (!Visibility.isVisible($element, minPercent)) {
                return;
            }

            $element.data('seen', 'yes');

            firstSeenElements.push($element[0]);
        });

        $(document).trigger(eventName, [$(firstSeenElements)]);
    };

    Mamam.visibility = Visibility;
})(Mamam, jQuery);
