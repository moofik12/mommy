(function (window, $) {
    Mamam.mailru = {
        PAGE_LEVEL_INDEX: 'home',
        PAGE_LEVEL_CATEGORY: 'category',
        PAGE_LEVEL_PRODUCT: 'product',
        PAGE_LEVEL_BASKET: 'cart',
        PAGE_LEVEL_ORDER_SUCCESS: 'purchase',
        PAGE_LEVEL_OTHER: 'other',

        send: function (level, data) {
            window._tmr = window._tmr || [];
            data || (data = {});

            var model = $.extend({
                type: 'itemView',
                productid: '',
                pagetype: level,
                totalvalue: '',
                list: ''
            }, data);

            window._tmr.push(model);
        }
    }
})(window, jQuery);
