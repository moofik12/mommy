(function ($) {
    $.fn.productCarousel = function (options) {
        var DEFAULTS = {
            "trigger": "click" // click or swipe
        };
        var setting = $.extend({}, DEFAULTS, options);

        return this.each(function () {
            var $this = $(this);
            var $imageWrapper = $this.find('.image-wrapper');

            var $currentImage = $(),
                currentImageIndex = 0;

            var $imageList = $this.find('.image-list');
            var $imageListItems = $imageList.find('li');

            var clickImage = function (imageIndex) {
                var selectedIndex = $imageListItems.index($imageListItems.filter('.active'));
                var isEmpty = $imageWrapper.find('img').length == 0;

                if (selectedIndex == imageIndex && !isEmpty) {
                    return;
                }

                $currentImage = $imageWrapper.find('img');
                var $item = $imageListItems.eq(imageIndex);
                var data = $item.data();

                if (!data.bigUrl) {
                    return;
                }

                var left = $imageWrapper.width();
                var $image = $('<img>', {src: data.bigUrl});
                $image.css('z-index', 2);

                if (!isEmpty) {
                    if (selectedIndex >= imageIndex) {
                        left = -left;
                        $image.appendTo($imageWrapper);
                    } else {
                        $image.prependTo($imageWrapper);
                    }
                    $image.css({
                        left: left
                    });
                }

                $currentImage.stop();
                $image.stop();
                $currentImage.animate({left: -left}, 300);
                $image.animate({left: 0}, 300, function () {
                    $currentImage.remove();
                    $currentImage = $image;
                    $currentImage.css('left', '');
                    $currentImage.css('z-index', 1);
                    currentImageIndex = imageIndex;
                });

                $imageListItems.removeClass('active');
                $item.addClass('active');
            };

            var swipeImage = function (imageIndex) {
                var selectedIndex = $imageListItems.index($imageListItems.filter('.active'));
                var isEmpty = $imageWrapper.find('img').length == 0;

                if (selectedIndex == imageIndex && !isEmpty) {
                    return;
                }

                var $item = $imageListItems.eq(imageIndex);
                var data = $item.data();

                if (!data.bigUrl) {
                    return;
                }

                var $image = $('<img>', {src: data.bigUrl});

                if (!isEmpty) {
                    var left;
                    if (selectedIndex >= imageIndex) {
                        left = -$imageWrapper.width();
                    } else {
                        left = $imageWrapper.width();
                    }
                    $image.css({
                        left: left
                    });
                }

                $image.prependTo($imageWrapper);

                $currentImage.animate({left: -left});
                $image.animate({
                    left: 0
                }, function () {
                    $imageWrapper.find('img').not($image).remove();
                    $currentImage = $image;
                    currentImageIndex = imageIndex;
                });

                $imageListItems.removeClass('active');
                $item.addClass('active');

            };

            $imageListItems.on('click', function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                if (setting.trigger == 'swipe') {
                    swipeImage($imageListItems.index(this));
                } else {
                    clickImage($imageListItems.index(this));
                }
            });

            if (setting.trigger == 'swipe') {
                $imageWrapper.css({overflow: 'hidden', position: 'relative'});
                var $imageWrapperGlass = $('<div>').css({
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    width: '100%',
                    height: '100%'
                });

                $imageWrapperGlass.appendTo($imageWrapper);
                var $window = $(window);
                var startTouch, moveTouch, startScrollX, swipeType = null, swipeDelta = 40;

                $imageWrapperGlass.on({
                    touchstart: function (e) {
                        e.preventDefault();
                        startTouch = $.extend({}, e.originalEvent.touches[0]);
                        startScrollX = $window.scrollTop();
                    },
                    touchmove: function (e) {
                        moveTouch = $.extend({}, e.originalEvent.touches[0]);

                        var offsetX = moveTouch.clientX - startTouch.clientX,
                            offsetY = startTouch.clientY - moveTouch.clientY;
                        var offsetXAbs = Math.abs(offsetX),
                            offsetYAbs = Math.abs(offsetY);

                        if (swipeType === 'y' || offsetYAbs > swipeDelta) {
                            $window.scrollTop(startScrollX + offsetY);
                            $currentImage.stop().css({
                                left: 0
                            });
                            swipeType = 'y';
                        } else if (swipeType === 'x' || offsetXAbs > swipeDelta) {
                            $currentImage.stop().css({
                                left: offsetX
                            });
                            swipeType = 'x';
                        } else if (swipeType == null) {
                            $currentImage.stop().css({
                                left: offsetX
                            });
                        }

                    },
                    touchcancel: function (e) {
                        swipeType = null;
                    },
                    touchend: function (e) {
                        if (swipeType !== 'x') {
                            swipeType = null;
                            $currentImage.animate({left: 0});
                            return;
                        }
                        swipeType = null;

                        if (startTouch.clientX > moveTouch.clientX) {
                            if (currentImageIndex < $imageListItems.length - 1) {
                                swipeImage(currentImageIndex + 1);
                            } else {
                                $currentImage.animate({left: 0});
                            }
                        } else {
                            if (currentImageIndex > 0) {
                                swipeImage(currentImageIndex - 1);
                            } else {
                                $currentImage.animate({left: 0});
                            }
                        }
                    }
                });
            }

            $imageListItems.first().trigger('click');

            // preload all images
            $imageListItems.parent().find('[data-big-url]').each(function () {
                var image = new Image();
                image.src = $(this).data().bigUrl;
            });
        });
    }
})(jQuery);
