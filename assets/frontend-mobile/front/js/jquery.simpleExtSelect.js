(function ($) {
    $.fn.simpleExtSelect = function (settings) {
        var options = $.extend({
            selectSelector: 'select',
            textSelector: '.extselect-input',
            valueSelector: 'input[type=hidden]'
        }, settings);

        return this.each(function () {
            var $this = $(this);
            var elements = {
                $select: $this.find(options.selectSelector),
                $text: $this.find(options.textSelector),
                $value: $this.find(options.valueSelector)
            };

            elements.$select.on('change', function () {
                var item = this.options[this.selectedIndex];
                elements.$text.html(item.innerHTML);
                elements.$value.val(this.value);
            });

            elements.$select.trigger('change');
        });
    }
})(jQuery);
