$(function () {
    var $goodsList = $('.goods-list');
    var $catalogSidebar = $('.catalog-sidebar');
    var $formFilter = $catalogSidebar.find('form');
    var $buttonForm = $formFilter.find('button');
    var $footer = $('.main-footer');
    var $catalogContent = $('.catalog-content');
    var $catalogLoader = $('#catalog-loader');

    var sidebarTop = $catalogSidebar.offset().top;
    var footerInnerHeight = $footer.innerHeight();

    var catalogContentBottom = $catalogContent.offset().top + $catalogContent.height();

    var $resultBlock = $('#show-result-catalog');
    var $closeResultBlock = $resultBlock.find('.results-close');
    var $showLinkResultBlock = $resultBlock.find('.show-link');
    var $countResultBlock = $resultBlock.find('.results-count');

    function sidebarScroll() {
        var sidebarInnerHeight = $catalogSidebar.height();
        var scrollTop = $(this).scrollTop();
        var footerOffsetTop = $footer.offset().top;

        if ($catalogContent.height() > sidebarInnerHeight) {
            if (scrollTop > $catalogContent.offset().top + 5/*sidebarTop + sidebarInnerHeight - window.innerHeight-5*/) {
                var top = 0;
                var bottom = 0;
                var chislo = screen.height <= 800 ? 70 : 0;

                var scrollHeight = Math.max(
                    document.body.scrollHeight, document.documentElement.scrollHeight,
                    document.body.offsetHeight, document.documentElement.offsetHeight,
                    document.body.clientHeight, document.documentElement.clientHeight
                );
                var footerPx = scrollHeight - scrollTop - window.innerHeight - footerInnerHeight;
                if (footerPx <= 0) {
                    bottom = -(footerPx);
                }
                if (sidebarInnerHeight < window.innerHeight) {
                    if (scrollTop > sidebarTop + 5) {
                        if (footerPx + (window.innerHeight - sidebarInnerHeight) <= 0) {
                            $catalogSidebar.removeAttr('style');//alert(1);
                            $catalogSidebar.css('position', 'fixed').css('bottom', bottom + 'px');
                        } else {
                            $catalogSidebar.removeAttr('style');
                            $catalogSidebar.css('position', 'fixed').css('top', top + 'px');
                        }
                    }
                } else {
                    if (scrollTop > sidebarTop + sidebarInnerHeight - window.innerHeight - 5) {
                        if (footerPx <= 0) {
                            bottom = -(footerPx); //chislo = 0;
                        }
                        $catalogSidebar.removeAttr('style');
                        //alert(bottom);
                        $catalogSidebar.css('position', 'fixed').css('bottom', (bottom + chislo) + 'px');
                    } else {
                        $catalogSidebar.removeAttr('style');
                    }
                }
            } else {
                $catalogSidebar.removeAttr('style');
            }
        }
    }

    $(window).on('scroll', function () {
        sidebarScroll();
    });

    sidebarScroll();

    function formFilterSubmit(showCount) {
        $("input[type='hidden'][data-type='color-filter']").remove();
        $("a[data-type='color-filter'][data-checked='1']").each(function () {
            var nameFilter = $(this).attr('data-name');
            var value = $(this).attr('data-value');
            $formFilter.append('<input name="' + nameFilter + '" type="hidden" value="' + value + '" data-type="color-filter">');
        });

        if (showCount) {
            $.ajax({
                url: $formFilter.attr('action'),
                type: $formFilter.attr('method'),
                data: $formFilter.serialize().replace(/\./g, ''),
                dataType: 'json',
                success: function (data) {
                    $countResultBlock.text(data.count);
                    $resultBlock.removeClass('result-window-load');
                }, error: function (err) {
                    console.log(err);
                    alert(I18n('Network error. Check your internet connection and try adding again.'));
                    $resultBlock.removeClass('result-window-load');
                }
            });
        } else {
            $catalogLoader.addClass('catalog-preloader-active');
            var $prices = $formFilter.find('.small-input');
            $prices.each(function (index, element) {
                var sanitizedValue = $(element).val().replace(/\./g, '');
                $(element).val(sanitizedValue);
            });
            $formFilter.submit();
        }
    }

    $(window).trigger('refreshScrollToTop');

    $buttonForm.on('click', function () {
        formFilterSubmit(false);
    });

    function showResultBlock(element, offsetTop) {
        var parentClientHeight = element.clientHeight;
        var halfParentClientHeight = Math.round(parentClientHeight / 2);
        var top = offsetTop - $catalogSidebar.offset().top + halfParentClientHeight;
        $resultBlock.css('top', top + 'px');
        $resultBlock.removeClass('hide-i');
        $countResultBlock.text('');
        //$resultBlock.addClass('result-window-load');
        formFilterSubmit(true);
    }

    function closeResultBlock() {
        $resultBlock.addClass('hide-i');
        $resultBlock.removeClass('result-window-load');
    }

    $showLinkResultBlock.on('click', function () {
        formFilterSubmit();
    });

    $(document).on('click', "[data-type='filter-bar']", function () {
        var $this = $(this);
        var filterName = $this.attr('data-name');
        var filterValue = $this.attr('data-value');
        if (filterName != 'color[]') {
            var $input = $("input[type='checkbox'][name='" + filterName + "'][value='" + filterValue + "']");
            $input.prop("checked", false);
            $input.parent().removeClass('cb-active');
            closeResultBlock();
            $this.parent().remove();
            formFilterSubmit(false);
        } else {
            var $input = $("a[data-name='" + filterName + "'][data-value='" + filterValue + "']");
            $input.attr('data-checked', 0);
            $input.parent().removeClass('color-active');
            closeResultBlock();
            $this.parent().remove();
            formFilterSubmit(false);
        }
    });

    $(document).on('click', "select[name='size']", function () {
        $(this).closest('.goods-item').addClass('active');
    });

    $(document).on('change', "input[name='sort']", function () {
        var $sortName = $('#sort-name');
        $sortName.html($(this).parent().find('label').text() + '<b></b>');
        formFilterSubmit(false);
    });

    $(document).on('change', "input[type='checkbox'][data-type='filter-action']", function (e) {
        var $this = $(this);
        var parent = $this.parent()[0];
        if (this.checked) {
            $(this).parent().addClass('cb-active');
            showResultBlock(parent, $this.parent().offset().top);
        } else {
            $(this).parent().removeClass('cb-active');
            if ($("input[type='checkbox'][data-type='filter-action']:checked").length == 0) {
                //closeResultBlock();
                showResultBlock(parent, $this.parent().offset().top);
            } else {
                showResultBlock(parent, $this.parent().offset().top);
            }
        }
    });

    $(document).on('click', "a[data-type='color-filter']", function (e) {
        var $this = $(this);
        var liParent = $this.parent();
        if (liParent.hasClass('color-active')) {
            liParent.removeClass('color-active');
            $closeResultBlock.trigger('click');
            $this.attr('data-checked', 0);
        } else {
            liParent.addClass('color-active');
            $this.attr('data-checked', 1);
            showResultBlock(liParent[0], liParent.offset().top);
        }
    });

    $closeResultBlock.on('click', function () {
        closeResultBlock();
    });

    $goodsList.on('submit', 'form.size-bar, .product-form form', function (e) {
        e.preventDefault();

        /*if (Mamam.userId == 0) {
            Mamam.modalAuth(function($modal) {
                Mamam.trackEvent('modals', 'registration', 'Открытие модального окна авторизации при добавления в корзину');
                $modal.modal('show');
            });
            return;
        }*/

        var $this = $(this),
            $form = $this,
            $submitBtn = $form.find('.in-cart');

        var formData = $form.serializeObject();

        $submitBtn.attr('disabled', 'disabled');
        $this.addClass('show');

        if (!$.mask.isLoaded()) {
            $form.expose({
                loadSpeed: 30,
                closeSpeed: 100,
                closeOnEsc: false,
                closeOnClick: false
            });
        }

        Mamam.trackEvent('cart', 'add', 'Добавление в корзину в списке товаров', formData['productId']);
        Mamam.cart.add(formData['eventId'], formData['productId'], formData['size'], formData['number'], formData['token'], function (data) {
            if (data.message == 'low_warehouse') {
                alert(I18n('Sorry, the item was purchased by another member.'));
            } else if (data.message == 'inactive_event') {
                alert(I18n('Sorry, but the stock is over.'));
            } else if (data.message == 'cart_is_full') {
                alert(I18n('Sorry, your shopping cart is full. One order can contain only 20 items.') +
                    I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
            } else if (data.message == 'unknown_error') {

                alert(I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
            } else if (data.message == 'network_error') {
                alert(I18n('Network error. Check your internet connection and try adding again.'));
            } else if (data.message == 'cart_block') {
                alert(I18n('Sorry, adding items has been blocked.'));
            }

            $submitBtn.removeAttr('disabled');
            $this.removeClass('show');

            $.mask.close();
        }, function () {
            Mamam.ecommerce.add(formData['eventId'] + '/' + formData['productId'], formData['number']);

            $submitBtn.removeAttr('disabled');
            $this.removeClass('show');
            $.mask.close();
        }, true);
    });

    $(document).on('click', '.product-link-details[data-product-id]', function (e) {
        e.preventDefault();

        var url = this.href, navigate = function () {
            document.location = url;
        };
        Mamam.ecommerce.click($(this).data('product-id'), navigate);
        setTimeout(navigate, 1000)
    });

    var viewChangedTimer = null;

    function onViewChanged() {
        if (viewChangedTimer) return;

        viewChangedTimer = setTimeout(function () {
            Mamam.visibility.eventOnFirstSeen('.product-link-details[data-product-id]', 'mamam.product.impressions', 0.75);
            viewChangedTimer = null;
        }, 200);
    }

    $(document).on('scroll', onViewChanged);
    $(window).on('resize', onViewChanged);

    onViewChanged();

    $(".element-drop").click(function () {
        $(this).toggleClass("element-drop-hide");
        var elementShow = $(this).parents(".element").find(".element-content div");
        if (elementShow.is(":hidden")) {
            elementShow.show();
        } else {
            elementShow.hide();
        }
    });

    $(".element-category-title").click(function () {
        var elementShow = $(this).next(".element-categories");
        if (elementShow.hasClass("hide-list")) {
            elementShow.removeClass("hide-list");
        } else {
            elementShow.addClass("hide-list");
        }
    });

    $(document).on('click', ".element-category-title, [data-category-id]", function () {
        $catalogLoader.addClass('catalog-preloader-active');
    });
});
