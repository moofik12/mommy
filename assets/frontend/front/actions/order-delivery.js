$(function () {
    var $container = $('.container'),
        $form = $container.find('form'),
        $submitBtn = $form.find(':submit'),
        $deliveryTabs = $form.find('.delivery-service');

    var $chequeContainer = $container.find('.check');
    var $submitButton = $container.find('.cart-form .check-buttons');
    var $submitButtonOffsetTop = $submitButton.offset().top;

    var $deliveryCountdown = $container.find('.promotional-time time');
    $deliveryCountdown.on('countdown-finished', function () {
        Mamam.trackEvent('delivery', 'timeExpired', 'Истекло время на странице оформления заказа');
        alert(I18n('Dear Customer, you have expired time for order confirmation. Please re-add the item to the shopping cart.'));
        $submitBtn.attr('disabled', 'disabled');
    });

    function submitButtonToTop() {
        if (Mamam.eventsThrottle.possitionTop() > $submitButtonOffsetTop) {
            if (!$submitButton.hasClass('fixed')) {
                $submitButton.addClass('fixed');
            }
        } else if ($submitButton.hasClass('fixed')) {
            $submitButton.removeClass('fixed');
        }
    }

    Mamam.eventsThrottle.addScrollCb(submitButtonToTop);

    $chequeContainer.on('recalculate', _.throttle(function () {
        var totalPrice = 0;
        var fullPrice = 0;

        var $priceList = $chequeContainer.find('li:not(.hide)'),
            $priceDeliveryList = $priceList.filter('[data-delivery-price-for]'),
            $priceTotal = $priceList.filter('[data-delivery-total]'),
            $priceFull = $priceList.filter('[data-delivery-full]');

        var $priceProducts = $priceList.filter('[data-products-price]'),
            $pricePromocode = $priceList.filter('[data-promocode]'),
            $priceBonuspoints = $priceList.filter('[data-bonuspoints]'),
            $priceUserDiscount = $priceList.filter('[data-user-discount]');

        var priceProducts = ($priceProducts.find('.price span').data('value') + $pricePromocode.find('.price span').data('value')
            + $priceUserDiscount.find('.price span').data('value')) || 0;

        var totalBonuspoints = parseInt($priceBonuspoints.data('bonuspointsTotal')) || 0;
        var priceBonuspoints = Math.min(priceProducts, totalBonuspoints);

        var $priceBonuspointsPrice = $priceBonuspoints.find('.price span');
        $priceBonuspointsPrice.text(priceBonuspoints);
        $priceBonuspointsPrice.data('value', -priceBonuspoints);

        $priceDeliveryList.each(function () {
            var $this = $(this),
                $prices = $this.find('.price'),
                $deliveryPrice = $prices.filter('[data-free-delivery-price="false"]'),
                $deliveryFreePrice = $prices.filter('[data-free-delivery-price="true"]');

            var freeDeliveryEnabled = !!$this.data('freeDeliveryEnabled');
            var freeDeliveryFrom = parseInt($this.data('freeDeliveryFrom')) || 0;

            if (freeDeliveryEnabled && priceProducts >= freeDeliveryFrom) {
                $deliveryPrice.addClass('hide-i');
                $deliveryFreePrice.removeClass('hide-i');
            } else {
                $deliveryPrice.removeClass('hide-i');
                $deliveryFreePrice.addClass('hide-i');
            }
        });

        $priceList.not($priceTotal).not($priceFull).each(function () {
            var $item = $(this), $price = $item.find('.price:not(.hide-i)').find('[data-value]');
            totalPrice += parseFloat($price.data('value')) || 0;
        });

        $priceList.filter('[data-products-price], [data-delivery-price-for]').each(function () {
            var $item = $(this), $price = $item.find('.price:not(.hide-i)').find('[data-value]');
            fullPrice += parseFloat($price.data('value')) || 0;
        });

        var $priceTotalValue = $priceTotal.find('.price.end-price').find('[data-value]');
        $priceTotalValue.text(Mamam.formatCurrency(totalPrice, Mamam.currency));
        $priceTotalValue.attr('data-value', totalPrice);

        var $priceFullValue = $priceFull.find('[data-value]');
        $priceFullValue.text(Mamam.formatCurrency(fullPrice, Mamam.currency));
        $priceFullValue.attr('data-value', fullPrice);
    }, 300));

    $deliveryTabs.on('change', ':radio', function () {
        var $radio = $(this);
        if ($radio.is(':checked')) {
            $deliveryTabs.find(':radio').not($radio).each(function () {
                $('[data-tab-for="' + this.id + '"]').addClass('hide');
                $('[data-delivery-price-for="' + this.id + '"]').addClass('hide');
            });

            $('[data-tab-for="' + this.id + '"]').removeClass('hide');
            $('[data-delivery-price-for="' + this.id + '"]').removeClass('hide');
        }

        $chequeContainer.trigger('recalculate');

        Mamam.trackEvent('delivery', 'deliveryTabChange', 'Выбор другого типа доставки');
    });
    $deliveryTabs.find(':radio').change();

    var $promocodeContainer = $form.find('[data-promocode]'),
        $promocodeInput = $promocodeContainer.find('[data-promocode-input]'),
        $userDiscount = $form.find('[data-user-discount]').find('.price span'),
        $userDiscountCost = $userDiscount.data('value'),
        $userDiscountCostPositive = $userDiscountCost > 0 ? $userDiscountCost : -$userDiscountCost,
        $priceUserDiscountAsBonus = $chequeContainer.find('[data-user-discount-as-bonus] .price span');

    $promocodeInput.on('change input focusout', _.throttle(function () {
        var $this = $(this);
        var token = $promocodeContainer.data('token') || '',
            value = $this.val().trim();

        var $discount = $promocodeContainer.find('.price span');
        $userDiscount.removeClass('inactive-price');
        $discount.removeClass('inactive-price');
        $priceUserDiscountAsBonus.removeClass('inactive-price');

        if (value.length == 0) {
            $discount.data('value', 0);
            $discount.text(0);
            $userDiscount.data('value', $userDiscountCost);

            $chequeContainer.trigger('recalculate');
            return;
        }

        var formSettings = $form.data('settings') || {},
            formAttributes = formSettings.attributes || [],
            formPromocodeAttr = _.findWhere(formAttributes, {name: 'promocode'}) || {};

        Mamam.cart.getPromocodeDiscount(token, value, function (data) {
            $discount.text(data['discount']);

            $priceUserDiscountAsBonus.addClass('inactive-price');

            if ($userDiscountCostPositive > 0) {
                if (data['discount'] > $userDiscountCostPositive) {
                    $userDiscount.data('value', 0);
                    $userDiscount.addClass('inactive-price');

                    $discount.data('value', -data['discount']);
                } else {
                    $userDiscount.data('value', $userDiscountCost);
                    $discount.data('value', 0);
                    $discount.addClass('inactive-price');
                }
            } else {
                $discount.data('value', -data['discount']);
            }

            $chequeContainer.trigger('recalculate');
            Mamam.trackEvent('delivery', 'promocode', 'Ввод промокода');


            $.fn.yiiactiveform.updateInput(formPromocodeAttr, null, $form);
        }, function (data) {
            $discount.data('value', 0);
            $discount.text('0');
            $userDiscount.data('value', $userDiscountCost);

            var messages = {
                invalid_promocode: I18n('Incorrect code'),
                not_exists: I18n('Not exist'),
                inactive: I18n('Promotional code is too old'),
                not_usable: I18n('You can not use it'),
                already_used: I18n('You have already use it'),
                not_suitable: I18n('Not suitable for this item'),
                network_error: I18n('Network error. Check your internet connection and try adding again.'),
                not_own_order: I18n('In Fast delivery orders, the promotional code is not active')
            };

            var message = messages[data['message']] || I18n('Unknown error');


            var errorMessage = {};
            errorMessage[formPromocodeAttr.id] = [message];
            $.fn.yiiactiveform.updateInput(formPromocodeAttr, errorMessage, $form);

            $chequeContainer.trigger('recalculate');
        });
    }, 400));

    $promocodeInput.trigger('change');
});
