$(function () {
    Mamam.carousel('.carousel', {
        prevBtn: '.carousel-left',
        nextBtn: '.carousel-right',
        steep: 4,
        repeat: true,
        timerEnable: true,
        timerControlActive: '.carousel-bar'
    });
});
