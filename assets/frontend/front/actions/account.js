$(function () {
    var $orders = $('.orders-table');

    $orders.on('click', '.opener', function (e) {
        e.preventDefault();
        var $this = $(this), $tr;
        var isExpand = $this.parents('tr.open').length > 0;

        var $expanded, $closed;

        if (isExpand) {
            $tr = $this.closest('tr.open');
            $expanded = $tr;
            $closed = $tr.prev();
        } else {
            $tr = $this.closest('tr');
            $expanded = $tr.next();
            $closed = $tr;
        }

        if (!isExpand) {
            $closed.siblings('tr').removeClass('hide')
                .filter('.open').addClass('hide');

            var $trs = $expanded.find('tbody > tr');
            if ($trs.length == 1) {
                $closed.clone().insertBefore($trs);
            }

            $closed.addClass('hide');
            $expanded.removeClass('hide');
        } else {
            $closed.removeClass('hide');
            $expanded.addClass('hide');
        }
    });
});
