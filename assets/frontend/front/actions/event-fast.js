$(function () {
    var $container = $('.supreme-inner');

    var reload = (function () {
        var ajaxHandle = null;
        return function () {
            if (!!ajaxHandle) {
                return;
            }

            ajaxHandle = $.ajax({
                url: window.location,
                type: 'GET',
                data: {}
            }).success(function (html) {
                $container.html(html);
            }).error(function (xhr) {
                alert(I18n('An error occurred, loading the list of products failed. Please refresh the page.'));
            }).always(function () {
                ajaxHandle = null;
                if (!!Mamam && !!Mamam.imagesLazyload) {
                    Mamam.imagesLazyload.initialize(); // reload
                }
            });
        }
    })();

    setInterval(function () {
        var $timer = $('.timer-block'),
            $timerMinutes = $timer.find('.time .m'),
            $timerSeconds = $timer.find('.time .s');

        var endDate = new Date(Date.parse($timer.attr('data-datetime'))),
            nowDate = new Date() - Mamam.timeClientAndServerDiff();
        var timespan = countdown(nowDate, endDate);

        $timerMinutes.text(Mamam.lpad(timespan.minutes, '0', 2));
        $timerSeconds.text(Mamam.lpad(timespan.seconds, '0', 2));

        if (timespan.value <= 0) {
            reload();
        }
    }, 1000);
});
