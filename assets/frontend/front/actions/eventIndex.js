$(function () {
    var EVENT_PRODUCT_HEIGHT = 477;
    var $eventCountdown = $('.content-header .promotional-time time');

    var $goodsList = $('.goods-list');
    var $goodsItem = $('.goods-item');
    var $goodsContainer = $goodsList.closest('.container');
    var $navigation = $('.navigation');
    var pauseAutoDownload = false;


    function fixProductHover() {
        var list = $goodsList.find('div.form-in:not([data-custom-hover])');
        list.attr('data-custom-hover', true);
        list.find('form button')
            .on('click', function (e) {
                $(this).closest('div.form-in').addClass('active');
            });

        if (!$.browser.msie && !$.browser.mozilla) {
            return;
        }

        list.find('select')
            .on('click', function (e) {
                $(this).closest('div.form-in').addClass('active');
            });
    }

    fixProductHover();

    function afterLoadingMoreGoods(imagesLazyloadReload) {
        fixProductHover();

        $(window).trigger('refreshScrollToTop');
        if (imagesLazyloadReload && !!Mamam && !!Mamam.imagesLazyload) {
            Mamam.imagesLazyload.initialize('img[data-lazyload]:not([data-loaded])'); //download new images
        }
    }

    var loadingMoreGoodsApi = $goodsList.loadingMoreGoods({
        autoDownload: true,
        autoDownloadOffset: EVENT_PRODUCT_HEIGHT * 3, //отступ в 3 ряда
        autoDownloadPause: function () {
            return pauseAutoDownload;
        },
        events: {
            afterDownload: function () {
                afterLoadingMoreGoods(true);
            }
        }
    }).data('loadingMoreGoods');

    $goodsContainer.filterableGoods({
        events: {
            success: function () {
                loadingMoreGoodsApi.initDataFromPagination();
                pauseAutoDownload = true;
                var offset = $navigation.offset().top + $navigation.outerHeight();
                $('html, body').animate({
                    scrollTop: isNaN(offset) ? 158 : offset
                }, 300, null, function () {
                    afterLoadingMoreGoods();
                    pauseAutoDownload = false;
                });
            }
        }
    });

    $(window).trigger('refreshScrollToTop');

    $(document).on('click', "select[name='size']", function () {
        $(this).closest('.goods-item').addClass('active');
    });

    $goodsList.on('change', ':radio', function () {
        var $this = $(this),
            $form = $this.closest('form.size-bar'),
            $submitBtn = $form.find('.in-cart');

        var $radio = $form.find(':radio, :checkbox'),
            $radioChecked = $radio.filter(':checked');

        $radio.not($radioChecked).removeClass('checked');
        $radioChecked.addClass('checked');

        if ($radioChecked.length > 0) {
            $submitBtn.removeAttr('disabled');
        } else {
            $submitBtn.attr('disabled', 'disabled');
        }
    });

    $goodsList.on('submit', 'form.size-bar, .product-form form', function (e) {
        e.preventDefault();

        /*if (Mamam.userId == 0) {
            Mamam.modalAuth(function($modal) {
                Mamam.trackEvent('modals', 'registration', 'Открытие модального окна авторизации при добавления в корзину');
                $modal.modal('show');
            });
            return;
        }*/

        var $this = $(this),
            $form = $this,
            $submitBtn = $form.find('.in-cart');

        var formData = $form.serializeObject();

        $submitBtn.attr('disabled', 'disabled');
        $this.addClass('show');

        if (!$.mask.isLoaded()) {
            $form.expose({
                loadSpeed: 30,
                closeSpeed: 100,
                closeOnEsc: false,
                closeOnClick: false
            });
        }

        Mamam.trackEvent('cart', 'add', 'Добавление в корзину на странице товара', formData['productId']);
        Mamam.cart.add(formData['eventId'], formData['productId'], formData['size'], formData['number'], formData['token'], function (data) {
            if (data.message == 'low_warehouse') {
                alert(I18n('Sorry, the item was purchased by another member.'));
            } else if (data.message == 'inactive_event') {
                alert(I18n('Sorry, but the stock is over.'));
            } else if (data.message == 'cart_is_full') {
                alert(I18n('Sorry, your shopping cart is full. One order can contain only 20 items.') +
                    I18n('Please make another order if you want to buy more than 20 items.'));
            } else if (data.message == 'unknown_error') {
                alert(I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
            } else if (data.message == 'network_error') {
                alert(I18n('Network error. Check your internet connection and try adding again.'));
            } else if (data.message == 'cart_block') {
                alert(I18n('Sorry, adding items has been blocked.'));
            }
            $submitBtn.removeAttr('disabled');
            $this.removeClass('show');
            $.mask.close();

        }, function () {
            Mamam.ecommerce.add(formData['eventId'] + '/' + formData['productId'], formData['number']);

            $submitBtn.removeAttr('disabled');
            $this.removeClass('show');
            $.mask.close();
        }, true);
    });

    $(document).on('click', '.product-link-details[data-product-id]', function (e) {
        e.preventDefault();

        var url = this.href, navigate = function () {
            document.location = url;
        };
        Mamam.ecommerce.click($(this).data('product-id'), navigate);
        setTimeout(navigate, 1000)
    });

    var viewChangedTimer;

    function onViewChanged() {
        if (viewChangedTimer) return;

        viewChangedTimer = setTimeout(function () {
            Mamam.visibility.eventOnFirstSeen('.product-link-details[data-product-id]', 'mamam.product.impressions', 0.75);
            viewChangedTimer = null;
        }, 200);
    }

    $(document).on('scroll', onViewChanged);
    $(window).on('resize', onViewChanged);

    onViewChanged();

    $eventCountdown.on('countdown-finished', function () {
        /*$eventCountdown.expose({
         loadSpeed: 30,
         closeSpeed: 100,
         closeOnEsc: false,
         closeOnClick: false
         });*/
        //alert('Извините, но акция закончилась');
        //window.location.replace(Mamam.baseUrl);
    });
});
