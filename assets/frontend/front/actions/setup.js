$(function () {
    Mamam.countdown('body');
    Mamam.cart.initialize('.container *[data-quickcart-container]', '.header-container .container *[data-quickcart-timer]');
    Mamam.imagesLazyload.initialize('img[data-lazyload]');

    $(document).on('mamam.auth.success', function () {
        Mamam.userId = 'entered';
        Mamam
            .updateViewAfterAuth()
            .then(function () {
                Mamam.cart.initialize('.container *[data-quickcart-container]', '.header-container .container *[data-quickcart-timer]');
            });

        dataLayer.push({'event': 'mamam.auth.success'})
    });

    $(document).on('mamam.registration.success', function () {
        dataLayer.push({'event': 'mamam.registration.success'});

        if (Mamam.clickId) {
            $.ajax('https://pstb.peerclicktrk.com/postback?cid=' + Mamam.clickId + '&summ_total=1&userid=2071');
        }
    });

    $(document).on('mamam.purchase.success', function (e) {
        dataLayer.push({
            'event': 'mamam.purchase.success', 'mamam.purchase.success': {
                totalPrice: e.totalPrice,
                purchases: e.purchases
            }
        })
    });

    $(document).on('mamam.product.impressions', function (e, $impressions) {
        if (!$impressions || !$impressions.length) return;

        var newSeenElements = [];
        $impressions.filter('[data-product-id]').each(function () {
            newSeenElements.push($(this).data('product-id'));
        });

        Mamam.ecommerce.impressions(newSeenElements);
    });

    //startup auth modal
    var authMark = 'auth-';
    var startUpAuthModal = false;
    if (window.location.hash) {
        var index = window.location.hash.search(authMark);
        if (index >= 0) {
            startUpAuthModal = window.location.hash.slice(index + authMark.length);
            window.location.hash = '';
        }
    }

    if (startUpAuthModal) {
        $('#' + startUpAuthModal).modal();
    }

    $authModal = $('#enter');
    // attach auth modal
    $(document).on('click', '[data-modal-auth]', function (e) {
        e.preventDefault();
        var tab = $(this).data('modalAuth');
        $authModal.modal('show');
        $authModal.find('.tabs > [data-tab="' + tab + '"]').trigger('click');
    });

    $(document).on('click', '.other-goods a', function (e) {
        Mamam.trackEvent('goods', 'otherGoods', 'Переход по "Люди, просматривающие этот товар, также интересовались"');
    });

    $(document).on("oauth", function (e, data) {
        var showModalId = 'hide-all-modals';
        if (!data.error) {
            $(document).trigger('mamam.auth.success');
            showModalId = 'mailing-type';
        }


        if (data.error == 'NOT_EMAIL') {
            showModalId = 'social-end';
            Mamam.trackEvent('modals', 'socialRegistration', 'Открытие модального окна ввода E-mail при попытке авторизации через соц. сеть');

        } else if (data.error == 'CONFIRM_PASSWORD') {
            showModalId = 'social-confirm';
            Mamam.trackEvent('modals', 'socialRegistration', 'Открытие модального окна ввода пароля при попытке авторизации через соц. сеть');

        } else if (data.error == 'FORM_ERROR') {
            if (data.message) {
                //alert(data.message);
            }
        }

        var $modals = $('body').children('.modals');
        $modals.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('id') == showModalId) {
                element.modal('show');
            } else {
                element.modal('hide');
            }
        });
    });

    var bodyData = $('body').data();
    if (!!bodyData.showPromoLogin && Mamam.userId == 0) {
        //TODO change IT
        Mamam.modalAuth(function ($modal) {
            Mamam.trackEvent('modals', 'registration', 'Открытие модального окна авторизации с промо изображением');
            $modal.modal('show');
            $modal.find('.tabs > [data-tab="registration"]').trigger('click');
        }, true);
    }


    //scroll to top, offer benefice
    var eventScrollToTop = function () {
        var scroll = $('.scroll-to-top');
        var offerBenefice = $('.get-gift-label');
        var offerBeneficeBottom = offerBenefice ? parseInt(offerBenefice.css('bottom'), 10) : 0;

        var scrollTarget = getScrollTarget();

        var windowParams = {
            scrollTop: scrollTarget.scrollTop,
            scrollHeight: scrollTarget.scrollHeight,
            height: window.innerHeight
        };

        var setup = {
            footerHeight: $('.main-footer').height(),
            defaultBottom: parseInt(scroll.css('bottom'), 10),
            showInPage: 2, //float
            showFooter: -parseInt(windowParams.scrollHeight - scrollTarget.scrollTop - windowParams.height - this.footerHeight + 75, 10) > 0
        };

        function getScrollTarget() {
            var scrollElement = document.body;

            if (document.documentElement.scrollTop > scrollElement.scrollTop) {
                scrollElement = document.documentElement;
            }

            return scrollElement;
        }

        function updateScrollParams() {
            var scrollTarget = getScrollTarget();

            windowParams.scrollTop = scrollTarget.scrollTop;
        }

        function updateWindowParams() {
            var scrollTarget = getScrollTarget();

            windowParams.scrollHeight = scrollTarget.scrollHeight;
            windowParams.height = window.innerHeight;
        }

        var scrollToTop = _.throttle(function () {
            updateScrollParams();

            var scrollVisible = !scroll.is(':hidden');

            var showFooterPx = -parseInt(windowParams.scrollHeight - windowParams.scrollTop - windowParams.height - setup.footerHeight + 75, 10);

            var currentPage = windowParams.scrollTop > 0
                ? parseFloat(((windowParams.scrollTop + windowParams.height) / windowParams.height).toFixed(2))
                : 1;

            if (showFooterPx > 0) {
                scroll.css({bottom: setup.defaultBottom + showFooterPx});
                offerBenefice.css({bottom: offerBeneficeBottom + showFooterPx});
                setup.showFooter = true;

            } else if (setup.showFooter) {
                scroll.css({bottom: setup.defaultBottom});
                offerBenefice.css({bottom: offerBeneficeBottom});
                setup.showFooter = false;
            }

            if (currentPage > setup.showInPage) {
                if (!scrollVisible) {
                    scroll.css('display', 'block');
                }

            } else if (scrollVisible) {
                scroll.css('display', 'none');
            }

        }, 10);

        scroll.on('click', function () {
            var scrollTarget = getScrollTarget();

            $(scrollTarget).animate({
                scrollTop: 0
            }, "fast");
        });

        function event() {
            updateWindowParams();
            scrollToTop();
        }

        $(window).on('resize', event);
        $(window).on('refreshScrollToTop', event);

        scrollToTop();
        return scrollToTop;
    }();

    Mamam.eventsThrottle.addScrollCb(eventScrollToTop);

    //fix scroll to top button
    setTimeout(function () {
        $(document).trigger('resize')
    }, 0);

    //start up modals
    $('.modal-start-up:first').modal('show');

    //auth modal
    var authContainer = $('.auth .auth-conteiner');
    var authContainerTabs = authContainer.find('.tabs');
    var bottomWrappFromRegister = authContainer.find('.auth-modal-bottom-wrapp');

    authContainer.on('click', '[data-tab]', function () {
        var $this = $(this);
        var id = $this.attr('data-tab');

        Mamam.trackEvent('auth', 'interface', 'Выбор другой вкладки:' + '"' + $this.text() + '"');
        if (id == 'signup') {
            bottomWrappFromRegister.removeClass("hide");
        } else {
            bottomWrappFromRegister.addClass("hide");
        }
        authContainerTabs.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('data-tab') == id) {
                element.addClass('active');
            } else {
                element.removeClass('active');
            }
        });

        authContainer.children('[data-tab-view]').each(function (indx, element) {
            element = $(element);

            if (element.attr('id') == id) {
                element.removeClass('hide');
            } else {
                element.addClass('hide');
            }
        });
    });

});
