$(function () {
    var container = $('.landing-wrapper');
    var containerTabs = $('.tabs');

    $('.subscribe').on('click', '[data-tab]', function () {
        var $this = $(this);
        var id = $this.attr('data-tab');

        Mamam.trackEvent('authLanding', 'interface', 'Выбор другой вкладки:' + '"' + $this.text() + '"');

        containerTabs.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('data-tab') == id) {
                element.addClass('active');
            } else {
                element.removeClass('active');
            }
        });

        container.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('id') == id) {
                element.removeClass('hide');
            } else {
                element.addClass('hide');
            }
        });
    });
});
