$(function () {
    var submitInvite = false;

    $('#invite-email-form').on('submit', function (e) {
        e.preventDefault();

        if (submitInvite) {
            return false;
        }

        submitInvite = true;
        var form = $(this),
            input = form.find('input:visible:first');

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            success: function (data, textStatus) {
                if (data.length == 0 && textStatus == 'success') {
                    input.val('');
                    alert(I18n('Invitation sent to specified address'));
                    location.reload();
                }

                submitInvite = false;
            },
            error: function () {
                alert(I18n('The request can not be processed. Contact Support.'));
                submitInvite = false;
            }
        });

        return false;
    });

    $('#invite-input').on('click', function () {
        this.select();
    });
});
