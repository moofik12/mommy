$(function () {
    var $form = $('form.card-form'),
        $submitBtn = $form.find('input[type=submit]');

    $form.on('submit', function (e) {
        e.preventDefault();

        var formData = $(this).serializeObject();

        $submitBtn.attr('disabled', 'disabled');

        if (!$.mask.isLoaded()) {
            $submitBtn.expose({
                loadSpeed: 30,
                closeSpeed: 100,
                closeOnEsc: false,
                closeOnClick: false
            });
        }

        Mamam.trackEvent('cart', 'add', 'Добавление в корзину на странице товара', formData['productId']);
        Mamam.cart.add(formData['eventId'], formData['productId'], formData['size'], formData['number'], formData['token'], function (data) {
            if ('low_warehouse' === data.message) {
                alert(I18n('Sorry, the item was purchased by another member.'));
            } else if ('inactive_event' === data.message) {
                alert(I18n('Sorry, but the stock is over.'));
            } else if ('cart_is_full' === data.message) {
                alert(I18n('Sorry, your shopping cart is full. One order can contain only 20 items.') +
                    I18n('Please make another order if you want to buy more than 20 items.'));
            } else if ('unknown_error' === data.message) {
                alert(I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
            } else if ('network_error' === data.message) {
                alert(I18n('Network error. Check your internet connection and try adding again.'));
            } else if ('cart_block' === data.message) {
                alert(I18n('Sorry, adding items has been blocked.'));
            }
            $submitBtn.removeAttr('disabled');
            $.mask.close();
        }, function () {
            Mamam.ecommerce.add(formData['eventId'] + '/' + formData['productId'], formData['number']);

            $submitBtn.removeAttr('disabled');
            $.mask.close();
        }, true);
    });

    $form.on('change', '.form-number', function () {
        var $this = $(this);
        var value = parseInt($this.attr('data-value'));

        if (0 === value) {
            $submitBtn.attr('disabled', 'disabled');
        } else {
            $submitBtn.removeAttr('disabled');
        }
    });

    var $formPrice = $form.find('.form-price'),
        $price = $formPrice.find('.new-price .number'),
        $priceOld = $formPrice.find('.old-price'),
        $sale = $formPrice.find('.sale'),
        $priceTitle = $('.title-price .title-new-price'),
        $priceOldTitle = $('.title-price .title-old-price'),
        $saleTitle = $('.title-price .title-perc');

    var $select = $form.find('.form-size select');
    var extSelect = $select.data('extselect');

    extSelect.on('initialize select', function () {
        var selected = extSelect.selectedItem();

        if (selected === null) {
            return; // do nothing
        }

        var storage = selected.storage;

        $price.text(storage.price);
        $priceTitle.text(storage.price);
        $priceOld.text(storage.priceOld);
        $priceOldTitle.text(storage.priceOld);

        $sale.text('-' + storage.discount + '%');
        $saleTitle.text('-' + storage.discount + '%');
    });
});
