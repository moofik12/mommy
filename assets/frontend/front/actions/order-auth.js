$(function () {
    var $preBlockRegistration = $('#pre-block-reg');
    var $form = $('#pre-form-reg');

    $(document).on('click', '#pre-auth', function (e) {
        e.preventDefault();

        if (Mamam.userId == 0) {
            Mamam.modalAuth(function ($modal) {
                Mamam.trackEvent('modals', 'registration', 'Открытие модального окна авторизации при пререгистрации');
                isRedirectToDelivery = true;
                $modal.modal('show');
                $("[data-tab='signin']").trigger('click');//делаем модалку с активной вкладкой входа
            });
        }
    });

    $(document).on('click', "input[name='pre-radio']", function () {
        var id = $(this).attr('id');
        if (id === 'pre-radio1') {
            $preBlockRegistration.hide();
            $('#pre-modal').modal('show');
        } else if (id === 'pre-radio2') {
            $preBlockRegistration.show();
        }
    });

    $(document).on('click', "#pre-btn-modal1, #pre-btn-modal2", function () {
        $('#pre-modal').modal('hide');
        $('#pre-radio2').trigger('click');
    });
});
