$(function () {
    var container = $('.login-wrapper');
    var containerTabs = $('.tabs');

    $('.login-conteiner').on('click', '[data-tab]', function () {
        var $this = $(this);
        var id = $this.attr('data-tab');

        Mamam.trackEvent('auth', 'interface', 'Выбор другой вкладки:' + '"' + $this.text() + '"');

        containerTabs.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('data-tab') == id) {
                element.addClass('active');
            } else {
                element.removeClass('active');
            }
        });

        container.children().each(function (indx, element) {
            element = $(element);

            if (element.attr('id') == id) {
                element.removeClass('hide');
            } else {
                element.addClass('hide');
            }
        });
    });

    $('.modals input[type=text]').one('click', function () {
        $(this).val('');
    });

    $(document).on("oauth", function (e, data) {
        Mamam.trackEvent('auth', 'interface', 'Открытие модального окна ввода E-mail при попытке авторизации через соц. сеть');
        if (data.error) {
            var $id = data.id;

            var $modals = $('body').children('.modals');
            $modals.children().each(function (indx, element) {
                element = $(element);

                if (element.attr('id') == $id) {
                    element.modal('show');
                } else {
                    element.modal('hide');
                }
            });
        }
    });
});
