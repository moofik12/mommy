(function (window, $) {
    var callbacksScroll = [];
    var callbacksResize = [];
    var TIME_OUT = 60;
    var wrapper = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        function (func) {
            window.setTimeout(func, TIME_OUT)
        };

    var positionTop = 0;
    var _tickScroll = false;
    var _tickResize = false;

    function getPositionTop() {
        return Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    }

    function applyCallbacks(callbacks, e) {
        $.each(callbacks, function (index, callback) {
            callback(e);
        });
    }

    function fireScroll(e) {
        if (_tickScroll) {
            return;
        }

        _tickScroll = true;
        wrapper(function () {
            _tickScroll = false;
            positionTop = getPositionTop();
            applyCallbacks(callbacksScroll, e);
        });
    }

    function fireResize(e) {
        if (_tickResize) {
            return;
        }

        _tickResize = true;
        wrapper(function () {
            _tickResize = false;
            positionTop = getPositionTop();
            applyCallbacks(callbacksResize, e);
        });
    }

    function subscribeEvents() {
        if (callbacksScroll) {
            $(document).on('scroll', fireScroll);
        }

        if (callbacksResize) {
            $(document).on('resize', fireResize);
        }
    }

    function unsubscribeEvents() {
        positionTop = 0;
        $(document).off('scroll', fireScroll);
        $(document).off('resize', fireResize);
    }

    function updateSubscribeEvents() {
        unsubscribeEvents();
        subscribeEvents();
    }

    function API() {
        this.possitionTop = function () {
            return positionTop;
        };
        this.addScrollCb = function (callback) {
            if (!$.isFunction(callback)) {
                return;
            }

            if ($.inArray(callback, callbacksScroll) == -1) {
                callbacksScroll.push(callback);
                updateSubscribeEvents();
            }
        };
        this.removeScrollCb = function (callback) {
            if (!$.isFunction(callback)) {
                return;
            }

            var index = $.inArray(callback, callbacksScroll);
            if (index == -1) {
                return;
            }
            callbacksScroll.splice(index, 1);
            updateSubscribeEvents();
        };
        this.flushScrollCb = function () {
            callbacksScroll = [];
            updateSubscribeEvents();
        };
        this.addResizeCb = function (callback) {
            if (!$.isFunction(callback)) {
                return;
            }

            if ($.inArray(callback, callbacksResize) == -1) {
                callbacksResize.push(callback);
                updateSubscribeEvents();
            }
        };
        this.removeResizeCb = function (callback) {
            if (!$.isFunction(callback)) {
                return;
            }

            var index = $.inArray(callback, callbacksResize);
            if (index == -1) {
                return;
            }

            callbacksResize.splice(index, 1);
            updateSubscribeEvents();
        };
        this.flushResizeCb = function () {
            callbacksResize = [];
            updateSubscribeEvents();
        };
    }


    Mamam.eventsThrottle = new API();

})(window, jQuery);
