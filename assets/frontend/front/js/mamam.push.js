(function (window, $) {
    var isEnabledPush = false;
    var serviceWorker;
    var storage = window.sessionStorage;
    var STORAGE_REQUEST_PERMISSION_ASK_COUNT = 'ask_permission';
    var STORAGE_REQUEST_UPDATE_SUBSCRIBE = 'update_subscribe';
    var MAX_REQUEST_PERMISSION_ASK_COUNT = 1;

    function requestPermission() {
        //не спрашиваем по множесту раз
        if (
            Notification.permission
            && Notification.permission == 'default'
        ) {
            var count = storage.getItem(STORAGE_REQUEST_PERMISSION_ASK_COUNT)
                ? storage.getItem(STORAGE_REQUEST_PERMISSION_ASK_COUNT) : 0;

            if (count >= MAX_REQUEST_PERMISSION_ASK_COUNT) {
                return;
            }

            storage.setItem(STORAGE_REQUEST_PERMISSION_ASK_COUNT, ++count);

            Notification.requestPermission(function (result) {
                isEnabledPush = result === 'granted';
                if (isEnabledPush && serviceWorker) {
                    serviceWorker.pushManager.subscribe({userVisibleOnly: true})
                        .then(function (subscription) {
                            return sendSubscriptionToServer(subscription);
                        });
                }
            });
        }

        isEnabledPush = Notification.permission === 'granted';
        if (isEnabledPush && serviceWorker && !storage.getItem('STORAGE_REQUEST_UPDATE_SUBSCRIBE')) {
            storage.setItem('STORAGE_REQUEST_UPDATE_SUBSCRIBE', 1);
            serviceWorker.pushManager.subscribe({userVisibleOnly: true})
                .then(function (subscription) {
                    return sendSubscriptionToServer(subscription);
                });
        }
    }

    function sendSubscriptionToServer(subscription) {
        $.ajax({
            'url': '?r=application/webToken&endpoint=' + subscription.endpoint,
            'success': function (data) {
                if (!data.time || !data.token) {
                    console.warn('subscription error token body');
                    return;
                }
                $.ajax({
                    'url': '?r=application/subscribeWeb&endpoint=' + subscription.endpoint
                    + '&token=' + data.token + '&time=' + data.time,
                    'success': function (data) {
                    },
                    'error': function (e) {
                        console.warn('subscription error');
                    }
                })
            },
            'error': function (e) {
                console.warn('subscription error get token');
            }
        })
    }

    Mamam.push = {
        init: function () {
            if (
                'serviceWorker' in navigator
                && 'PushManager' in window
                && 'Promise' in window
                && 'fetch' in window
            ) {
                setTimeout(requestPermission, 500);

                try {
                    navigator.serviceWorker.register('/static/js/push-worker.js')
                        .then(function (swReg) {
                            serviceWorker = swReg;
                        })
                        .catch(function (error) {
                            console.warn('Service Worker Error', error);
                        });
                } catch (e) {
                    console.warn('Init service worker error', error);
                }

            } else {
                if (window.console) {
                    console.warn('Push messaging is not supported');
                }
            }
        }
    }
})(window, jQuery);
