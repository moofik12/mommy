(function (window, $) {

    var Event = {};
    //установки по умолчанию
    Event.params = {
        active: true,
        delay: false, //для таймера
        percentage: false //для скрола
    };

    Event.rules = {
        maxCountRun: 1 //кол-во раз которое событие может вызываться, false - бесконечно
    };

    Event.createHandler = function (name, rules, func) {
        rules = $.extend(Event.rules, rules);

        Event[name].prototype = new Event.EventHandler(rules, func);
        var e = new Event[name]();
        return function (params) {
            params = $.extend(Event.params, params);

            return e.handle(params)
        }
    };

    Event.EventHandler = function (rules, func) {
        var self = this;
        this.countRun = 0;
        this.rules = rules;
        this.runFunc = func;
        this.active = true;
        this.validator = new Event.EventValidator(this);

        this.handle = function (params) {
        };
        this.isActive = function () {
            return this.active;
        };
        this.run = function () {
            self.countRun++;
            if ($.isFunction(this.runFunc)) {
                self.runFunc.call(this);
            }
        };
    };

    Event.ExitIntentHandler = function () {
        var self = this;
        this.handle = function (params) {
            this.active = params.active;

            if (this.isActive()) {
                $(document).on('mouseleave', this.onMouseOut);
            }
        };
        this.onMouseOut = function (event) {
            if (event.clientY < 10 && self.validator.validate()) {
                self.run();
            }
        }
    };
    Event.TimedHandler = function () {
        var self = this, n;
        this.handle = function (params) {
            this.active = params.active;

            if (params.delay && this.isActive()) {
                setTimeout(this.setTimeOutCallback, params.delay * 1000)
            }
        };
        this.setTimeOutCallback = function () {
            if (self.validator.validate()) {
                self.run();
                if (n !== undefined) {
                    clearInterval(n)
                }
            } else if (n === undefined) {
                n = setInterval(self.setTimeOutCallback, 1000)
            }
        }
    };
    Event.ScrollDownHandler = function () {
        var self = this;
        this.documentGet = function (attribute) {
            return document.body[attribute] || document.documentElement[attribute]
        };
        this.documentHeight = Math.max(self.documentGet('scrollHeight'), self.documentGet('offsetHeight'), self.documentGet('clientHeight'));
        this.handle = function (params) {
            this.active = params.active;

            var e, i = self.getTreshold(params);
            if (this.isActive()) {
                $(document).on('scroll', function () {
                    e = self.documentGet('scrollTop') + window.innerHeight;
                    if (self.validator.validate() && i <= e) {
                        self.run();
                    }
                })
            }
        };
        this.getTreshold = function (params) {
            if (params.percentage) {
                return params.percentage / 100 * self.documentHeight
            }

            return self.documentHeight
        }
    };
    Event.EventValidator = function (handle) {
        var self = this;
        this.handleTarget = handle;

        function availableRun() {
            return self.handleTarget.rules.maxCountRun > self.handleTarget.countRun;
        }

        function validate() {
            return availableRun();
        }

        this.validate = validate;
    };

    Mamam.activateEvents = (function () {
        var t = {};
        t.exitIntent = function (params, rules, func) {
            Event.createHandler('ExitIntentHandler', rules, func)(params)
        };
        t.timed = function (params, rules, func) {
            Event.createHandler('TimedHandler', rules, func)(params)
        };
        t.scrollDown = function (params, rules, func) {
            Event.createHandler('ScrollDownHandler', rules, func)(params)
        };

        return t;
    })();

})(window, jQuery);
