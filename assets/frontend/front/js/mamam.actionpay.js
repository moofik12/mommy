(function (window, $) {
    Mamam.actionpay = {
        PAGE_TYPE_INDEX: 1,
        PAGE_TYPE_PRODUCT: 2,
        PAGE_TYPE_CATEGORY: 3,
        PAGE_TYPE_BASKET: 4,
        PAGE_TYPE_ORDER: 5,
        PAGE_TYPE_ORDER_SUCCESS: 6,
        PAGE_TYPE_QUICK_VIEW: 7,
        PAGE_TYPE_EVENT_ADD_PRODUCT: 8,
        PAGE_TYPE_EVENT_REMOVE_PRODUCT: 9,
        PAGE_TYPE_OTHER: 9,

        shopId: 0,


        send: function (pageType, data) {
            data.pageType = pageType;

            var url = (window.location.protocol == "https:" ? "https:" : "http:");
            url += "//aprtx.com/code/";
            url += this.shopId.toString();
            url += '/';

            var self = this;
            this._initialized = !!this._initialized;

            if (!this._initialized) {
                window.APRT_DATA = data;
            }

            $.ajax({
                url: url,
                dataType: 'script',
                cache: true,
                success: function () {
                    if (self._initialized) {
                        window.APRT_SEND(data);
                    }
                    self._initialized = true;
                }
            })
        },

        init: function () {
            var url = (window.location.protocol == "https:" ? "https:" : "http:");
            url += "//aprtx.com/code/";
            url += this.shopId.toString();
            url += '/';

            var self = this;
            this._initialized = !!this._initialized;

            $.ajax({
                url: url,
                dataType: 'script',
                cache: true,
                success: function () {
                    self._initialized = true;
                }
            })
        }
    }
})(window, jQuery);
