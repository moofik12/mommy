(function ($) {
    $.fn.filterableGoods = function (settings) {
        var events = $.extend({
            success: $.noop,
            error: $.noop,
            done: $.noop
        }, settings.events);

        var filterVisualizationItemTemplate = '<div class="filter-item" data-match="<%= id %>"><div class="title-middle"><span><%= name %></span></div><i class="icon-filter-remove"></i></div>';
        var filterVisualizationItemRemoveAllTemplate = '<div class="filter-item remove-all no-select" data-remove-all><div class="title-middle"><span>' + I18n('Remove all') + '</span></div></div>';
        var hashMapFiltersMatch = {};

        return this.each(function () {
            var $this = $(this);
            var _enableHistory = typeof window.history !== "undefined" && window.history.pushState;
            var GOODS_CLASS_SELECTOR = '.goods-list';
            var GOODS_NUMBER_VALUE_CLASS_SELECTOR = '.goods-number-value';
            var PAGINATION_CLASS_SELECTOR = '.pagination';
            var DUMMY_NO_ITEMS = '.content-body .list-no-items';

            var $header = $this.find('.goods-header'),
                $content = $this.find('.content-body'),
                $pagination = $content.find(PAGINATION_CLASS_SELECTOR);

            var $filter = $header.find('.goods-filter');
            var $forms = $filter.find('form');
            var $filterVisualization = $('.filter-bar');
            var $goods = $content.find(GOODS_CLASS_SELECTOR);
            var $goodsNumber = $header.find('.goods-number');
            var $goodsNumberVal = $goodsNumber.find(GOODS_NUMBER_VALUE_CLASS_SELECTOR);
            var filterNames = _.uniq($filter.find(':input').map(function (key, item) {
                //побочноя логика чтобы не обходить снова
                hashMapFiltersMatch[getHashKeyFromInputNode(item)] = getMatchStringFromInputNode(item);

                return item.name;
            }));

            function getHashKeyFromInputNode(node) {
                return getHashKey(node.name, node.value);
            }

            function getHashKey(name, value) {
                return name + '-' + value;
            }

            function getMatchStringFromInputNode(node) {
                //если мачиться на id то jquery не найден селектор типа #brand-М.Eight :(
                return node.id
                    ? '[id=\"' + node.id + '\"]'
                    : '[name=\"' + node.name + '\"][value=\"' + node.value + '\"]';
            }

            function onPopState(event) {
                refreshData(false);
                restoreCheckedFilterFromQuery(true);
            }

            function refreshVisualizationFilters() {
                var htmlItems = [];
                $forms.find(':checked').each(function (i, input) {
                    if (!input.value) {
                        return;
                    }

                    var labels = input.id ? $('label[for="' + input.id + '"]') : [];
                    if (labels.length > 0) {
                        var label = labels[0];
                        if (label.textContent) {
                            htmlItems.push(_.template(filterVisualizationItemTemplate)({
                                id: encodeURI(getMatchStringFromInputNode(input)),
                                name: label.textContent
                            }));
                        }
                    }
                });

                if (htmlItems.length > 0) {
                    htmlItems.push(filterVisualizationItemRemoveAllTemplate);
                }

                if ($filterVisualization.length > 0) {
                    $filterVisualization[0].innerHTML = htmlItems.join("");
                }
            }

            function saveFiltersData(data) {
                var searchQuery = getQueryStringFromData(data);

                if (!_enableHistory) {
                    window.location.search = searchQuery;
                } else {
                    window.history.pushState({'filter': data}, null, '?' + searchQuery)
                }
            }

            function getQueryStringFromData(data) {
                var nowData = Mamam.getObjectFromQueryString(window.location.search);
                for (var i in nowData) {
                    if (!nowData.hasOwnProperty(i)) {
                        continue;
                    }

                    if (_.indexOf(filterNames, i) >= 0) {
                        delete nowData[i];
                    }
                }

                delete data["page"];
                for (var l in data) {
                    if (!data.hasOwnProperty(l)) {
                        continue;
                    }

                    if (data[l] === "") {
                        delete data[l];
                    }
                }

                return $.param($.extend({}, nowData, data));
            }

            var ajaxHandle = null;
            var filter = function (url, data, success, error, done) {
                url = url ? url : window.location;
                success = (success || $.noop);
                error = (error || $.noop);
                done = (done || $.noop);

                if (!!ajaxHandle && !!ajaxHandle['abort']) {
                    ajaxHandle.abort();
                    console.log('abort');
                }

                //$(document).mask({ closeOnEsc: false, closeOnClick: false });
                if (!$.mask.isLoaded()) {
                    $header.expose({
                        loadSpeed: 30,
                        closeSpeed: 100,
                        closeOnEsc: false,
                        closeOnClick: false
                    });
                }

                ajaxHandle = $.ajax({
                    url: url,
                    type: 'GET',
                    data: data,
                    cache: true
                }).success(function (result) {
                    success.call(this, result);
                    events.success.call(this, result);

                }).error(function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call();
                        events.error.call();
                    }

                    done.call();
                }).then(function () {
                    // nothing
                }).always(function () {
                    ajaxHandle = null;
                    $.mask.close();
                    events.done.call();
                    refreshVisualizationFilters();

                    if (!!Mamam && !!Mamam.imagesLazyload) {
                        Mamam.imagesLazyload.initialize(); // reload
                    }
                });
            };
            var refreshData = function (pushFilterDataToHistory) {
                pushFilterDataToHistory = typeof pushFilterDataToHistory === "undefined" ? true : pushFilterDataToHistory;

                var $forms = $filter.find('form');
                var formData = {};
                $forms.each(function () {
                    formData = $.extend(formData, $(this).serializeObject());
                });
                formData['page'] = 1;

                if (pushFilterDataToHistory) {
                    saveFiltersData(formData);
                }

                if (!_enableHistory) {
                    return;
                }

                filter(null, null, function (html) {
                    $(DUMMY_NO_ITEMS).remove();
                    $goods.empty();
                    $(html).find(GOODS_CLASS_SELECTOR).children().appendTo($goods);

                    var dummyNoItems = $(html).find(DUMMY_NO_ITEMS);
                    if (dummyNoItems.length > 0) {
                        $goods.before(dummyNoItems);
                    }

                    var goodsCount = $(html).find(GOODS_NUMBER_VALUE_CLASS_SELECTOR);
                    if (goodsCount && goodsCount.text()) {
                        $goodsNumberVal.text(goodsCount.text());
                    } else {
                        $goodsNumberVal.text(
                            '0 ' + Mamam.plural(0, 'наименование', 'наименования', 'наименований')
                        );
                    }


                    var paginationHtml = $(html).find(PAGINATION_CLASS_SELECTOR).html();
                    $pagination.html(paginationHtml ? paginationHtml : '');
                }, function () {
                    $goods.html('');
                    $goodsNumberVal.text(0);
                });
            };

            $header.delegate(':input, form', 'change reset', _.debounce(function (e) {
                var $this = $(this),
                    $currentForm = $this.closest('form'),
                    $currentFilter = $currentForm.closest('li'),
                    $currentFilterTitle = $currentFilter.find('.filter-item:first');

                if (e.target && e.target.type == 'radio') {
                    $currentFilter.removeClass('open');
                }

                _.defer(function () {
                    var currentFilterValues = [];
                    $currentForm.find(':radio, :checkbox').filter(':checked').each(function () {
                        var $label = $currentForm.find('label[for="' + this.id + '"]:first');
                        currentFilterValues.push($label.text());
                    });

                    Mamam.trackEvent('filter', 'products', 'Фильтрация или сортировка по "' + $currentFilterTitle.text() + '"' + ', текущие значения: ' + currentFilterValues.join(', '));
                });

                refreshData(true);
            }, 300));

            $header.delegate('.filter-none', 'click', function (e) {
                e.preventDefault();
                var $this = $(this);
                $this.closest('form')[0].reset();
            });

            $filterVisualization.on('click', '.icon-filter-remove', function () {
                var items = $(this).closest('.filter-item');
                if (items) {
                    var item = $(items[0]);
                    var match = item.data('match');

                    if (match) {
                        $(decodeURI(match)).each(function (key, item) {
                            item.checked = false;
                        });
                    }
                    item.remove();
                    refreshData(true);
                }
            });

            $filterVisualization.on('click', '[data-remove-all]', function () {
                $forms.find(':checked').each(function (i, input) {
                    if (!input.value) {
                        return;
                    }

                    input.checked = false;
                });
                refreshVisualizationFilters();
                refreshData(true);
            });

            $pagination.delegate('a', 'click', function (event) {
                if (event && event.preventDefault) {
                    event.preventDefault();
                }

                var url = $(this)[0].href;
                filter(url, {}, function (html) {
                    $goods.empty();
                    $(html).find(GOODS_CLASS_SELECTOR).children().appendTo($goods);

                    var goodsCount = $(html).find(GOODS_NUMBER_VALUE_CLASS_SELECTOR);
                    $goodsNumberVal.text(goodsCount.text());


                    var paginationHtml = $(html).find(PAGINATION_CLASS_SELECTOR).html();
                    $pagination.html(paginationHtml ? paginationHtml : '');
                }, function () {
                    $goods.html('');
                    $goodsNumberVal.text(0);
                });

                return false;
            });

            function restoreCheckedFilterFromQuery(refreshAllFilters) {
                if (refreshAllFilters) {
                    $forms.find(':checked').each(function (i, input) {
                        if (!input.value) {
                            return;
                        }

                        input.checked = false;
                    });
                }

                //restore filter from search params
                var nowData = Mamam.getObjectFromQueryString(window.location.search);
                var filtersMatchCheck = [];
                for (var name in nowData) {
                    if (!nowData.hasOwnProperty(name)) {
                        continue;
                    }

                    if (_.indexOf(filterNames, name) >= 0) {
                        if ($.isArray(nowData[name])) {
                            $.each(nowData[name], function (i, value) {
                                var match = hashMapFiltersMatch[getHashKey(name, value)];
                                if (match) {
                                    filtersMatchCheck.push(match);
                                }
                            });

                            continue;
                        }

                        var match = hashMapFiltersMatch[getHashKey(name, nowData[name])];
                        if (match) {
                            filtersMatchCheck.push(match);
                        }
                    }
                }

                if (filtersMatchCheck) {
                    $(filtersMatchCheck.join(',')).each(function (key, value) {
                        value.checked = true;
                    })
                }
            }

            //init
            (function init() {
                if (_enableHistory) {
                    $(window).on('popstate', onPopState);
                }

                restoreCheckedFilterFromQuery();
                refreshVisualizationFilters();
            })();
        });
    }
})(jQuery);
