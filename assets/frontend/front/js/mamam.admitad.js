(function (window, $) {
    Mamam.admitad = {
        PAGE_LEVEL_INDEX: 0,
        PAGE_LEVEL_CATEGORY: 1,
        PAGE_LEVEL_PRODUCT: 2,
        PAGE_LEVEL_BASKET: 3,
        PAGE_LEVEL_ORDER_SUCCESS: 4,
        PAGE_LEVEL_REGISTRY: 5,

        send: function (level, data) {
            //init global variables
            switch (level) {
                case this.PAGE_LEVEL_CATEGORY:
                    /** @var {String} data */
                    window.ad_category = data;
                    break;

                case this.PAGE_LEVEL_PRODUCT:
                    /**
                     * @var {Object} data, pre:
                     * { "id": "",  // required
					 *   "vendor": "",
					 *   "price": "",
					 *   "url": "",
					 *   "picture": "",
					 *   "name": "",
					 *   "category": "",
					 * }
                     */
                    window.ad_product = data;
                    break;

                case this.PAGE_LEVEL_BASKET:
                    /**
                     * @var {Array} $data, pre:
                     * [{
					 * "id": "",   // required
					 * "number": "",
					 * }, ...]
                     */
                    window.ad_products = data;
                    break;

                case this.PAGE_LEVEL_ORDER_SUCCESS:
                    /** @var {String} data.order */
                    window.ad_order = data.order;
                    /** @var {Float} data.amount */
                    window.ad_amount = data.amount;
                    /**
                     * @var {Array} data.products, pre:
                     * [{
					 * "id": "",
					 * "number": "",
					 * }, ...]
                     */
                    window.ad_products = data.products;
                    break;

                case this.PAGE_LEVEL_REGISTRY:
                    window._retag_data = {};
                    break;

                default:
                    break;
            }

            this._initParams(level);
            this._initScript();
        },

        _levelCodes: function () {
            var self = this;
            var levelsCode = {};
            levelsCode[self.PAGE_LEVEL_INDEX] = '9ce88861ef';
            levelsCode[self.PAGE_LEVEL_CATEGORY] = '9ce88861ee';
            levelsCode[self.PAGE_LEVEL_PRODUCT] = '9ce88861ed';
            levelsCode[self.PAGE_LEVEL_BASKET] = '9ce88861ec';
            levelsCode[self.PAGE_LEVEL_ORDER_SUCCESS] = '9ce88861eb';
            levelsCode[self.PAGE_LEVEL_REGISTRY] = '9ce88861ea';

            return levelsCode;
        },

        _levelObject: function (level) {
            var levelObject = {};
            var levelCodes = this._levelCodes();
            if (levelCodes[level]) {
                switch (level) {
                    case this.PAGE_LEVEL_REGISTRY:
                        levelObject.code = levelCodes[level];
                        break;

                    default:
                        levelObject.code = levelCodes[level];
                        levelObject.level = level;
                        break;
                }
            }

            return levelObject;
        },

        _initParams: function (level) {
            var levelObject = this._levelObject(level);

            window._retag = window._retag || [];
            window._retag.push(levelObject);
        },

        _initScript: function () {
            var id = "admitad-retag";
            if (document.getElementById(id)) {
                return;
            }
            var s = document.createElement("script");
            s.async = true;
            s.id = id;
            var r = (new Date).getTime();
            s.src = (document.location.protocol == "https:" ? "https:" : "http:") + "//cdn.admitad.com/static/js/retag.min.js?r=" + r;
            var a = document.getElementsByTagName("script")[0];
            a.parentNode.insertBefore(s, a);
        }
    }
})(window, jQuery);
