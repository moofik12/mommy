(function ($) {
    var registrationTracker = {};

    var params = {
        errorBorderColor: '#ff0000',
        errorBorderWidth: '1px',
        errorBorderStyle: 'solid',
        successBorderColor: '#569b44',
        successBorderWidth: '1px',
        successBorderStyle: 'solid'
    };

    var successBorder = params.successBorderWidth + ' ' + params.successBorderStyle + ' ' + params.successBorderColor;
    var errorBorder = params.errorBorderWidth + ' ' + params.errorBorderStyle + ' ' + params.errorBorderColor;

    registrationTracker.overrideSettings = function (options) {
        params = $.extend(params, options);

        successBorder = params.successBorderWidth + ' ' + params.successBorderStyle + ' ' + params.successBorderColor;
        errorBorder = params.errorBorderWidth + ' ' + params.errorBorderStyle + ' ' + params.errorBorderColor;
    };

    registrationTracker.track = function (form, url) {
        $.ajax({
            url: url,
            type: form.attr('method'),
            data: form.serialize()
        });
    };

    registrationTracker.validateEmail = function (selector) {
        if ($(selector).val() === undefined) {
            return true;
        }

        var email = $(selector);

        if (email.val() !== '') {
            var pattern = new RegExp(this.validatorRegex.email.default.pattern, this.validatorRegex.email.default.flags);
            var emailValue = email.val().replace(/\s*/g, '');

            if (pattern.test(emailValue)) {
                email.css({'border': successBorder});
            } else {
                email.css({'border': errorBorder});
                return false;
            }
        } else {
            email.css({'border': errorBorder});
            email.focus();
            return false;
        }

        return true;
    };

    registrationTracker.validatePhone = function (selector, lang) {
        if ($(selector).val() === undefined) {
            return true;
        }

        var phone = $(selector);

        if (phone.val() !== '' && typeof this.validatorRegex.phone[lang] !== 'undefined') {
            var phoneValue = phone.val().replace(/\s*/g, '');

            var pattern = new RegExp(
                this.validatorRegex.phone[lang].pattern,
                this.validatorRegex.phone[lang].flags
            );

            if (pattern.test(phoneValue)) {
                phone.css({'border': successBorder});
            } else {
                phone.css({'border': errorBorder});
                return false;
            }
        } else {
            phone.css({'border': successBorder});
        }

        return true;
    };

    registrationTracker.validateName = function (selector) {
        if ($(selector).val() === undefined) {
            return true;
        }

        var name = $(selector);

        if (name.val() !== '') {
            var value = name.val();
            if (value.length <= 40) {
                name.css({'border': successBorder});
            } else {
                name.css({'border': errorBorder});
                return false;
            }
        } else {
            name.css({'border': successBorder});
        }

        return true;
    };

    window.registrationTracker = registrationTracker;
})(jQuery);
