(function ($) {
    Mamam.imagesLazyload = {
        selector: 'img[data-lazyload]',
        initialize: function (selector) {
            this.selector = selector || this.selector;
            $(this.selector).lazyload({
                threshold: 1000,
                effect: "fadeIn",
                data_attribute: 'src',
                skip_invisible: false,
                appear: function () {
                    $(this).attr('data-loaded', 'true');
                }
            });
        }
    }
})(jQuery);
