'use strict';

(function ($) {
    $.fn.loadingMoreGoods = function (settings) {
        settings || (settings = {});

        var options = $.extend({
            template: '<div class="goods-item goods-more"><a class="more" href="#"> <div class="box"><span>Смотреть еще</span> </div></a></div>',
            loadingClass: 'loading',
            pageVar: 'page',
            paginationBlock: '.pagination',
            paginationBlockItem: 'li',
            paginationBlockItemActiveClass: 'active',
            paginationPages: 'a[data-page]',
            datePageVar: 'page',
            selectedPage: 'li.active a[data-page]',
            itemsSelector: '.goods-list',
            autoDownload: false,
            autoDownloadPause: $.noop,
            autoDownloadOffset: 0 //distance (px) to the block for start update
        }, settings);

        var events = $.extend({
            afterDownload: $.noop
        }, settings.events);

        var instanceNumber = 0;
        var autoDownloadPause = $.isFunction(options.autoDownloadPause) ? options.autoDownloadPause : $.noop;
        var autoDownloadOffset = -parseInt(options.autoDownloadOffset, 10);

        function API() {
            instanceNumber++;

            var templateId = 'loading-more-goods-' + instanceNumber;
            var currentPage = false,
                startPage = false,
                endPage = false,
                initFromPage = false;

            var self = this;
            var ajaxHandle = null;

            var show = false;
            var templateCompile = $(options.template);
            templateCompile[0].id = templateId;

            this.refresh = function () {
                currentPage = 1;
            };

            this.getCurrentPage = function () {
                return currentPage;
            };

            this.setCurrentPage = function (value) {
                if (value >= 0) {
                    currentPage = value;
                }
            };

            this.loadNextPage = function (url, ajaxSetting) {
                url || (url = window.location.href);

                var df = $.Deferred();
                var data = {};
                var loadingPage = currentPage + 1;
                data[options.pageVar] = loadingPage;


                if (!this.isHasNextPage()) {
                    return false;
                }

                var targetUrl = createUrl(url, data);
                var ajaxOptions = $.extend({}, ajaxSetting, {
                    type: "GET",
                    url: targetUrl
                });

                if (ajaxHandle && ajaxHandle['abort']) {
                    ajaxHandle.abort();
                }

                setDownloadClass(true);
                ajaxHandle = $.ajax(ajaxOptions)
                    .then(function (data) {
                            removeTemplate();

                            if (pushDownloadedData(data)) {
                                setCurrentPage(loadingPage);
                                checkDownloadPageInPagination();
                            }

                            addTemplate();
                            checkState();

                            if ($.isFunction(events.afterDownload)) {
                                events.afterDownload.call();
                            }

                            df.resolve(arguments);
                        }
                        , function () {
                            df.reject(arguments);
                            //alert('Ошибка получения данных. Сообщите нам!');
                        }
                    )
                    .always(function () {
                        ajaxHandle = null;
                        setDownloadClass(false);
                    });

                return df.promise();
            };

            this.isHasNextPage = function () {
                return currentPage < endPage;
            };

            this.initDataFromPagination = function () {
                var paginationBlock = $(options.paginationBlock);
                var pages = [];
                var currentPageMatch = paginationBlock.find(options.selectedPage);

                clearStates();

                paginationBlock.find(options.paginationPages).each(function () {
                    var page = $(this).data(options.datePageVar);
                    if (page) {
                        pages.push(page);
                    }
                });

                startPage = Math.min.apply(null, pages);
                endPage = Math.max.apply(null, pages);
                if (currentPageMatch && currentPageMatch.data(options.datePageVar) > 0) {
                    setCurrentPage(currentPageMatch.data(options.datePageVar));
                } else {
                    setCurrentPage(startPage);
                }

                removeTemplate();
                addTemplate();
                checkState();
            };

            this.isShow = function () {
                return !!show;
            };

            function clearStates() {
                currentPage = initFromPage = endPage = startPage = show = false;
            }

            function checkState() {
                if (currentPage < endPage) {
                    show = true;
                    templateCompile.show();

                } else {
                    show = false;
                    templateCompile.hide();
                }
            }

            function addTemplate() {
                if ($(options.itemsSelector + ' #' + templateId).length === 0) {
                    templateCompile.appendTo($(options.itemsSelector));
                    templateCompile.on('click', templateEvent);

                    if (options.autoDownload) {
                        $(window).on('scroll', autoDownloadEvent);
                    }
                }
            }

            function removeTemplate() {
                templateCompile.off('click', templateEvent);
                $(window).off('scroll', autoDownloadEvent);
                templateCompile.detach();
            }

            function templateEvent(event) {
                if (event && event['preventDefault']) {
                    event.preventDefault();
                }

                var linkUrl = $(options.paginationBlock + ' ' + options.paginationPages + ':first');
                if (linkUrl.length > 0) {
                    self.loadNextPage(decodeURIComponent(linkUrl[0].href));
                    return;
                }

                self.loadNextPage();
            }

            function autoDownloadEvent() {
                if (!self.isHasNextPage() || !self.isShow() || autoDownloadPause()) {
                    return;
                }

                if (ajaxHandle == null && window.scrollY && window.innerHeight
                    && window.scrollY + windowInnerHeight() >= templateCompileOffsetTop()
                ) {
                    templateEvent();
                }
            }

            function fetchTemplateCompileOffsetTop() {
                return templateCompile[0].offsetTop + autoDownloadOffset;
            }

            function fetchWindowInnerHeight() {
                return window.innerHeight;
            }

            var templateCompileOffsetTop = _.throttle(fetchTemplateCompileOffsetTop, 50);
            var windowInnerHeight = _.throttle(fetchWindowInnerHeight, 100);

            function setCurrentPage(number) {
                if (number > endPage) {
                    currentPage = endPage;
                } else if (number >= startPage && number <= endPage) {
                    currentPage = number;
                }

                if (initFromPage === false) {
                    initFromPage = currentPage;
                }
            }

            function pushDownloadedData(data) {
                var $data = $('<div></div>').append(data),
                    $items = $data.find(options.itemsSelector),
                    $pagination = $data.find(options.paginationBlock);

                try {
                    $(options.itemsSelector).append($items.children());
                    $(options.paginationBlock).html($pagination.html());

                    return true;
                } catch (err) {
                    return false;
                }
            }

            function checkDownloadPageInPagination() {
                var pagers = $(options.paginationBlock).find(options.paginationPages);
                pagers.each(function (index, item) {
                    var $item = $(item);
                    var page = $item.data(options.datePageVar);
                    if (page == currentPage || (page >= initFromPage && page <= currentPage)) {
                        var block = $item.closest(options.paginationBlockItem);
                        if (block) {
                            $(block).addClass(options.paginationBlockItemActiveClass);
                        }
                    }
                });
            }

            function setDownloadClass(value) {
                if (value && !templateCompile.hasClass(options.loadingClass)) {
                    templateCompile.addClass(options.loadingClass);

                } else if (templateCompile.hasClass(options.loadingClass)) {
                    templateCompile.removeClass(options.loadingClass);
                }
            }

            function createUrl(url, params) {
                var elUrl = document.createElement('a');
                elUrl.href = url;

                var search = elUrl.search.replace(/^\?/, '');
                var data = {};

                $.each(search.split("&"), function (index, param) {
                    param = param.split("=");

                    if (param[0] && param[1]) {
                        data[param[0]] = param[1];
                    }
                });

                elUrl.search = $.param($.extend({}, data, params));

                return decodeURIComponent(elUrl.href);
            }

            (function initialize() {
                self.initDataFromPagination();
            })();
        }

        return this.each(function () {
            $(this).data('loadingMoreGoods', new API());
        });
    }
})(jQuery);
