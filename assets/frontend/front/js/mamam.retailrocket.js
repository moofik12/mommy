(function (window, $) {
    var _initScriptWatcher = false;

    Mamam.retailrocket = {
        PAGE_LEVEL_CATEGORY: 1,
        PAGE_LEVEL_PRODUCT: 2,
        PAGE_LEVEL_BASKET: 3,
        PAGE_LEVEL_ORDER_SUCCESS: 4,


        addToBasket: function (productId) {
            this.init();

            window.rrApi.addToBasket(productId);
        },

        send: function (level, data) {
            this.init();

            //init global variables
            switch (level) {
                case this.PAGE_LEVEL_CATEGORY:
                    if ($.isArray(data)) {
                        $.each(data, function (key, item) {
                            /** @var {String} data */
                            (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function () {
                                try {
                                    rrApi.categoryView(item);
                                } catch (e) {
                                }
                            });
                        });

                    } else {
                        /** @var {String} data */
                        (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function () {
                            try {
                                rrApi.categoryView(data);
                            } catch (e) {
                            }
                        });
                    }
                    break;

                case this.PAGE_LEVEL_PRODUCT:
                    /** @var {String} data */
                    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function () {
                        try {
                            rrApi.view(data);
                        } catch (e) {
                        }
                    });
                    break;

                case this.PAGE_LEVEL_ORDER_SUCCESS:
                    /** @var {String} data.order */
                    /**
                     * @var {Array} data.products, pre:
                     * [{ id: <product_id>, qnt: <quantity>, price: <price>}]
                     */
                    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function () {
                        try {
                            rrApi.order({
                                transaction: data.order,
                                items: data.products
                            });
                        } catch (e) {
                        }
                    });
                    break;

                case 1:
                    window.rrApi.addToBasket(data);
                    break;

                default:
                    break;
            }
        },

        init: function () {
            if (_initScriptWatcher !== false) {
                return;
            }

            try {
                this._initScript();
                _initScriptWatcher = true;

            } catch (e) {
                if (console && console.error) {
                    console.error('Ошибка инициализации скрипта retailrocket. ' + e);

                } else if (console && console.info) {
                    console.info('Ошибка инициализации скрипта retailrocket. ' + e);
                }
            }
        },

        _initScript: function () {
            window.rrPartnerId = "57d6b3689872e55f78bdb3bd";
            window.rrApi = {};
            window.rrApiOnReady = window.rrApiOnReady || [];
            window.rrApi.addToBasket = window.rrApi.order = window.rrApi.categoryView = window.rrApi.view =
                window.rrApi.recomMouseDown = window.rrApi.recomAddToCart = function () {
                };
            (function (d) {
                var ref = d.getElementsByTagName('script')[0];
                var apiJs, apiJsId = 'rrApi-jssdk';
                if (d.getElementById(apiJsId)) return;
                apiJs = d.createElement('script');
                apiJs.id = apiJsId;
                apiJs.async = true;
                apiJs.src = "//cdn.retailrocket.ru/content/javascript/tracking.js";
                ref.parentNode.insertBefore(apiJs, ref);
            }(document));
        }
    }
})(window, jQuery);
