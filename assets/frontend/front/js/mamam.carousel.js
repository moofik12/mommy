(function ($) {
    Mamam.carousel = function (context, settings) {
        var $context = $(context);

        var options = $.extend({
            nextBtn: '',
            prevBtn: '',
            elements: 'li',
            animation: {
                duration: 400
            },
            steep: 1,
            window: 4, //размер окна
            repeat: false, //повторять по кругу
            timerEnable: false,
            timerDelay: 4000, //ms
            timerControlActive: null //остановка, запуск таймера при наведении на блок, по умолчанию context. '.class'
        }, settings);

        var Carousel = function (context, options) {
            this.context = context;
            this.options = options;
            this.elements = context.find(options.elements);
            this.elWidth = 0;
            this._timerControl = options.timerControlActive ? $(options.timerControlActive) : context;

            this.maxPositions = this.elements.length - this.options.window;
            this._current = 0;
            this._aminated = false;
            this._timer = null;

            if (this.elements[0]) {
                this.elWidth = this.elements[0].offsetWidth;
                var img = $(this.elements[0]).find('img')[0];
                var me = this;
                if (img) {
                    img.onload = function () {
                        me.elWidth = me.elements[0].offsetWidth;
                    }
                }
            }

            this._bindEvents();
            this._initTimer();
        };

        Carousel.prototype.next = function (e) {
            if (e && e.preventDefault) {
                e.preventDefault();
            }

            if (this._current >= this.maxPositions) {
                return;
            }

            var nextPosition = this._current + this.options.steep;

            if (nextPosition >= this.maxPositions) {
                nextPosition = this.maxPositions;

                if (this.options.repeat) {
                    this._animate(0);
                    return;
                }
            }

            this._animate(nextPosition);
        };

        Carousel.prototype.prev = function (e) {
            if (e && e.preventDefault) {
                e.preventDefault();
            }

            var nextPosition = this._current - this.options.steep;

            if (this._current <= 0) {
                return;
            }

            if (nextPosition < 0) {
                nextPosition = 0;
            }

            this._animate(nextPosition);
        };

        Carousel.prototype._animate = function (position) {
            var me = this;
            var left = this.elWidth * position;

            if (this._aminated) {
                return;
            }

            this._aminated = true;

            this.context.animate({
                left: '-' + left
            }, this.options.animation.duration, function () {
                me._aminated = false;
                me._current = position;
            });
        };

        Carousel.prototype._bindEvents = function () {
            $(this.options.nextBtn).on('click', $.proxy(this.next, this));
            $(this.options.prevBtn).on('click', $.proxy(this.prev, this));

            $(this._timerControl).on('mouseover', $.proxy(this.timerStop, this));
            $(this._timerControl).on('mouseout', $.proxy(this.timerStart, this));
        };

        Carousel.prototype.destroy = function () {
            $(this.options.nextBtn).off('click', $.proxy(this.next, this));
            $(this.options.prevBtn).off('click', $.proxy(this.prev, this));

            $(this._timerControl).off('mouseenter', $.proxy(this.timerStop, this));
            $(this._timerControl).off('mouseleave', $.proxy(this.timerStart, this));

            $(this.context).data('carousel', null);
        };


        Carousel.prototype.timerStop = function (e) {
            //ненужное всплытие
            if (e && e.currentTarget !== e.relatedTarget) {
                return;
            }

            if (this._timer) {
                clearInterval(this._timer);
                this._timer = null;
            }
        };

        Carousel.prototype.timerStart = function (e) {
            //ненужное всплытие
            if (e && e.currentTarget !== e.relatedTarget) {
                return;
            }

            if (!this._timer) {
                this._initTimer();
            }
        };

        Carousel.prototype._initTimer = function () {
            if (!this.options.timerEnable) {
                return;
            }

            var me = this;
            me._timer = setInterval(function () {
                me.next();
            }, me.options.timerDelay);
        };

        $context.data('carousel', new Carousel($context, options));

        return this;
    }
})(jQuery);
