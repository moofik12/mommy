(function (document, window, $) {
    var scr = '//creativecdn.com/tags';
    var _hash = 'cg8RH6r3O7swrJC9gjyD';
    var init = false; //не больше 1-й инициализации

    Mamam.rtbhouse = {
        PAGE_LEVEL_INDEX: 'home',
        PAGE_LEVEL_PRODUCT: 'product',
        PAGE_LEVEL_BASKET: 'cart',
        PAGE_LEVEL_ORDER_SUCCESS: 'purchase',
        PAGE_LEVEL_LISTING: 'listing',
        PAGE_LEVEL_STARTORDER: 'startorder', //начало оформления заказа
        PAGE_LEVEL_OTHER: 'other',

        /**
         * @param {Number} type
         * @param {Array} products
         * @param {Object} data
         */
        send: function (type, products, data) {
            products || (products = []);
            data || (data = {});

            $.extend({
                cost: 0,
                order: 0
            }, data);

            var modelParams = [];
            modelParams.push({
                'pr': _hash
            });

            var uniqueParam = {};

            switch (type) {
                case this.PAGE_LEVEL_INDEX:
                    modelParams.push({home: ''});
                    break;

                case this.PAGE_LEVEL_LISTING:
                    modelParams.push({listing: products.join(',')});
                    break;

                case this.PAGE_LEVEL_PRODUCT:
                    modelParams.push({offer: products.join(',')});
                    break;

                case this.PAGE_LEVEL_BASKET:
                    modelParams.push({basketstatus: products.join(',')});
                    break;

                case this.PAGE_LEVEL_STARTORDER:
                    modelParams.push({startorder: products.join(',')});
                    break;

                case this.PAGE_LEVEL_ORDER_SUCCESS:
                    modelParams.push({orderstatus2: parseFloat(data.cost).toFixed(1)});
                    modelParams.push(data.order);
                    modelParams.push(products.join(','));

                    uniqueParam = {cd: false};
                    break;

                default:
                    break;
            }

            var params = $.extend({id: this._buildParamId(modelParams)}, uniqueParam);

            this._init($.param(params));
        },

        _init: function (params) {
            if (init) {
                return;
            }

            var fr = document.createElement("iframe");
            fr.src = encodeURI(scr + '?' + params);
            fr.width = "1";
            fr.height = "1";
            fr.scrolling = "no";
            fr.frameBorder = "0";
            fr.style.display = "none";

            var f = function () {
                document.body.appendChild(fr);
                init = true;
            };
            if (window.opera == "[object Opera]") {
                document.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        },

        /**
         * @param {Array} data
         * @returns {string}
         * @private
         */
        _buildParamId: function (data) {
            var params = [];
            var delimiter = '_';
            var typeItem = '';

            data.forEach(function (item) {
                typeItem = typeof item;

                if (typeItem === 'string' && typeItem === 'number' && typeItem === 'boolean') {
                    params.push(item);
                } else if (typeItem === 'object') {
                    for (var index in item) {
                        if (item.hasOwnProperty(index)) {
                            if (item[index]) {
                                params.push(index + delimiter + item[index]);
                            } else {
                                params.push(index);
                            }
                        }
                    }
                }
            });

            return params.join(delimiter);
        }
    }
})(document, window, jQuery);
