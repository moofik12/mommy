(function ($) {
    $(document).ready(function () {
        /**
         * Возвращает идентификатор страны (имя субдомена)
         *
         * @returns {string}
         */
        function getCountryIdentifier() {
            var full = window.location.host;
            var parts = full.split('.');
            var sub = parts[0];
            if ("undefined" !== typeof sub && sub !== 'mommy') {
                return sub;
            }

            return '';
        }

        /**
         * Возвращает количество посещенных пользователем страниц
         *
         * @returns {*}
         */
        function getVisitedPagesCount() {
            var count = $.cookie('pagesVisited');
            if ("undefined" === typeof count|| isNaN(count)) {
                return 0;
            }

            return parseInt(count);
        }

        /**
         * Устанавливает количество посещенных страниц сайта в cookies
         * Expires: кука действительна пока открыта текущая сессия браузера
         * После установки/изменения куки метод так же инициирует событие visitedChanged
         */
        function setVisitedPagesCount() {
            $.cookie('pagesVisited', getVisitedPagesCount() + 1, {path: '/'});
            var visitedChangedEvent = $.Event('visitedChanged');
            $(document).trigger(visitedChangedEvent);
        }

        var UNFINISHED_REGISTRATION_SURFING = 0;
        var UNFINISHED_REGISTRATION_REGISTER_BUTTON_CLICK = 1;
        var UNFINISHED_REGISTRATION_FILLING_EMAIL = 2;
        var FINISHED_REGISTRATION = 3;

        var ajaxReq = undefined;

        function sendEventData(data) {
            ajaxReq = $.ajax({
                url: '/tracker/record',
                dataType: 'text',
                type: 'POST',
                data: data,
                beforeSend: function() {
                    if ("undefined" !== typeof ajaxReq  && ajaxReq.readyState < 4) {
                        ajaxReq.abort();
                    }
                }
            });
        }

        function resolveData(params) {
            var data = {
                source: document.referrer,
                anonymousId: $.cookie('anonymousId'),
                country: getCountryIdentifier()
            };

            if ("undefined" !== typeof params && "object" === typeof params) {
                data = Object.assign({}, data, params);
            }

            return data;
        }

        function handlerGeneric(event) {
            sendEventData(resolveData({
                event: event.code
            }));
        }

        function handlerEmail(event) {
            sendEventData(resolveData({
                event: event.code,
                email: event.email
            }))
        }

        function startRecording() {
            $(document).on('visitedChanged', function() {
                $(document).trigger({
                    type: 'mommy.registrationSurfing',
                    code: UNFINISHED_REGISTRATION_SURFING
                });
            });

            $(document).on('click', '[data-modal-auth="signup"]', function() {
                $(document).trigger({
                    type: 'mommy.registerClick',
                    code: UNFINISHED_REGISTRATION_REGISTER_BUTTON_CLICK
                });
            });

            $(document).on('input', '#RegistrationForm_email', function() {
                $(document).trigger({
                    type: 'mommy.fillingEmail',
                    code: UNFINISHED_REGISTRATION_FILLING_EMAIL,
                    email: $('#RegistrationForm_email').val()
                });
            });

            $(document).on('mommy.registrationSurfing', handlerGeneric);
            $(document).on('mommy.registerClick', handlerGeneric);
            $(document).on('mommy.finishedRegistration', handlerGeneric);
            $(document).on('mommy.fillingEmail', handlerEmail);

            $(document).trigger({
                type: 'mommy.finishedRegistration',
                code: FINISHED_REGISTRATION
            });

            setTimeout(function() {
                setVisitedPagesCount(getVisitedPagesCount() + 1);
            }, 2000);
        }

        startRecording();
    })
})(jQuery);