(function ($) {
    Mamam.cart = {
        $container: null,
        $timer: null,

        initialize: function (containerSelector, timerSelector) {
            this.$container = $(containerSelector);
            this.$timer = $(timerSelector);

            this._bindQuickCartEvents();
        },

        add: function (eventId, productId, size, number, token, error, success, redirectToOrder) {
            error || (error = $.noop);
            success || (success = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxAdd'),
                type: 'GET',
                data: {
                    eventId: eventId,
                    productId: productId,
                    size: size,
                    number: number,
                    token: token
                },
                success: function (result) {
                    if (!result['success']) {
                        error.call(self, result);
                    } else {
                        success.call(self, result);
                    }
                    if (result.success && redirectToOrder !== false) {
                        window.location.replace(Mamam.createUrl('cart'));
                        return;
                    }

                    self.reloadQuickCart();
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            })
        },

        update: function (eventId, productId, size, token, error, success, redirectToOrder) {
            error || (error = $.noop);
            success || (success = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxUpdate'),
                type: 'GET',
                data: {
                    eventId: eventId,
                    productId: productId,
                    size: size,
                    token: token
                },
                success: function (result) {
                    if (!result['success']) {
                        error.call(self, result);
                    } else {
                        success.call(self, result);
                    }

                    if (result.success && redirectToOrder !== false) {
                        window.location.replace(Mamam.createUrl('cart'));
                        return;
                    }

                    self.reloadQuickCart();
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            })
        },

        getList: function (error, success) {
            error || (error = $.noop);
            success || (success = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxList'),
                type: 'GET',
                dataType: 'json',
                cache: false,
                success: function (result) {

                    if (!result['success']) {
                        error.call(self, result['data']);
                    } else {
                        success.call(self, result);
                    }
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            });
        },

        getPromocodeDiscount: function (token, promocode, success, error) {
            success || (success = $.noop);
            error || (error = $.noop);

            var self = this;
            $.ajax({
                url: Mamam.createUrl('cart/ajaxPromocodeDiscount'),
                type: 'GET',
                data: {
                    promocode: promocode,
                    token: token
                },
                success: function (result) {
                    if (!result['success']) {
                        error.call(self, result);
                    } else {
                        success.call(self, result);
                    }
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        error.call(self, {
                            message: 'network_error',
                            success: false
                        });
                    }
                }
            })
        },

        _bindQuickCartEvents: function () {
            var self = this;
            var $quickCart = this.$container.find('.quick-cart');
            var timerCloseQuickCartId = null;
            var timerCloseQuickCartAfterLeave = 1200;

            function openQuickCart() {
                if (timerCloseQuickCartId) {
                    window.clearTimeout(timerCloseQuickCartId);
                    timerCloseQuickCartId = null;
                }

                if (self.$container.hasClass('open')) {
                    return;
                }

                self.$container.addClass('open');
            }

            function hideQuickCart() {
                if (!self.$container.hasClass('open')) {
                    return;
                }

                self.$container.removeClass('open');
            }

            function hideQuickCartLater() {
                if (timerCloseQuickCartId) {
                    window.clearTimeout(timerCloseQuickCartId);
                }

                timerCloseQuickCartId = window.setTimeout(hideQuickCart, timerCloseQuickCartAfterLeave);
            }

            this.$timer.on('countdown-finished', '.timer', function () {
                self.reloadQuickCart();
            });

            this.$container
                .on('mouseenter', function () {
                    openQuickCart();
                })
                .on('mouseleave', function () {
                    if ($('.quick-cart-goods').children().length) {
                        hideQuickCartLater();
                        return;
                    }

                    hideQuickCart();
                });

            $quickCart
                .on('click mouseup', function (e) {
                    e.stopPropagation(); // disable bootstrap dropdown automatic hide on click
                });

            $quickCart.on('click', '.foot-line .delete', function (e) {
                e.preventDefault();
                var $this = $(this),
                    $item = $this.closest('.item'),
                    $li = $this.closest('li'),
                    $list = $li.parent();

                $li.remove();

                var $items = $list.children();

                self.$container.find('.cart .goods').text($items.length);
                if (0 === $items.length) {
                    $list.closest('.quick-cart').hide();
                    self.$timer.find('.timer').hide();
                }

                var totalPrice = 0.0, minDate = null;
                $items.each(function () {
                    var $item = $(this).find('.item');

                    if (!$item.data('isExpired')) {
                        var price = parseFloat($item.data('positionPrice')),
                            number = parseInt($item.data('positionNumber'));

                        totalPrice += price * number;

                        var $time = $item.find('.time time');
                        var date = new Date($time.attr('datetime'));
                        if (minDate === null || minDate > date) {
                            minDate = date;
                        }
                    }
                });

                self.$container.find('.quick-cart-footer .price').text(Mamam.formatCurrency(totalPrice, Mamam.currency));
                self.$timer.find('time').attr('datetime', minDate);

                $.ajax({
                    url: Mamam.createUrl('/cart/ajaxDelete'),
                    type: 'GET',
                    data: {
                        eventId: $item.data('positionEvent'),
                        productId: $item.data('positionProduct'),
                        size: $item.data('positionSize'),
                        success: function () {
                            var id = $item.data('positionEvent') + '/' + $item.data('positionProduct'),
                                quantity = $item.data('positionNumber');
                            Mamam.ecommerce.remove(id, quantity);
                        }
                    }
                });
            });

            $quickCart.on('countdown-finished', '.quick-cart-goods', function (e) {
                var $target = $(e.target);
                var $button = $('<a class="extend-btn" href="#">' + I18n('prolong the reservation') + '</a>');

                $target.parent('.time').addClass('cross-out');
                $target.before($button).hide();
            });

            $quickCart.on('click', '.extend-btn', function (e) {
                e.preventDefault();

                var $this = $(this),
                    $item = $this.closest('.item');

                var itemData = $item.data();

                Mamam.trackEvent('cart', 'update', 'Продление резерва');

                Mamam.cart.update(itemData.positionEvent, itemData.positionProduct, itemData.positionSize, itemData.positionToken, function (data) {
                    if ('low_warehouse' === data.message) {
                        alert(I18n('Sorry, the item has just been reserved for another member.'));
                    } else if ('inactive_event' === data.message) {
                        alert(I18n('Sorry, but the stock is over.'));
                    } else if ('cart_is_full' === data.message) {
                        alert(I18n('Sorry, your shopping cart is full. One order can contain only 20 items.') +
                            I18n('Please make another order if you want to buy more than 20 items.'));
                    } else if ('unknown_error' === data.message) {
                        alert(I18n('Sorry, there was an unknown error. Try refreshing the page and try adding again.'));
                    } else if ('network_error' === data.message) {
                        alert(I18n('Network error. Check your internet connection and try adding again.'));
                    } else if ('wrong_number' === data.message) {
                        alert(I18n('Sorry, you can not add a product with this quantity.'));
                    }
                }, null, false);
            });
        },

        reloadQuickCart: function () {
            var $container = this.$container;
            var $timer = this.$timer;
            var defer = $.Deferred();

            $.ajax({
                url: Mamam.createUrl('cart/quickCart'),
                type: 'GET',
                cache: false,
                success: function (html) {
                    var output = $(html);
                    var $containerReplace = output.find($container.selector);
                    var $timerReplace = output.find($timer.selector);

                    var quickCartSelector = '.cart';
                    var quickCartItemsSelector = '.quick-cart';

                    $container.find(quickCartSelector).html(
                        $containerReplace.find(quickCartSelector).html()
                    );
                    $container.find(quickCartItemsSelector).html(
                        $containerReplace.find(quickCartItemsSelector).html()
                    );

                    $timer.html($timerReplace.html());

                    Mamam.countdown('body');
                    defer.resolve();
                },
                error: function () {
                    defer.reject();
                }
            });

            return defer;
        }
    };
})(jQuery);
