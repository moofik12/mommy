$(function () {
	var minContainerHeight = 800;
	var containerHeight = $(window).height();
	if(containerHeight <= minContainerHeight) {
		$('body').height(minContainerHeight);
		$('html').height(minContainerHeight);
	}
	$(window).resize(function(){
		var height = $(window).height();
		if(height< minContainerHeight) {
			$('body').height(minContainerHeight);
			$('html').height(minContainerHeight);
		} else {
			$('body').height(height);
			$('html').height(height);
		}
	});
	
	$( ".name").focus(function() {
		var nameVal = $(this).val();
		if (nameVal=="Ваше Имя") {
			$(this).val("");
		}
	});
	$( ".name").focusout(function() {
		var nameVal = $(this).val();
		if (nameVal=="") {
			$(this).val("Ваше Имя");
		}
	});
	$( ".email").focus(function() {
		var nameVal = $(this).val();
		if (nameVal=="Ваш E-mail") {
			$(this).val("");
		}
	});
	$( ".email").focusout(function() {
		var nameVal = $(this).val();
		if (nameVal=="") {
			$(this).val("Ваш E-mail");
		}
	});
});
