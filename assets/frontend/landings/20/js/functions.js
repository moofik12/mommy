function startTimer() {
	var countDown = true;

	var h1 = $(".h .h1 span").html();
	var h2 = $(".h .h2 span").html();
	var h = h1 + h2;

	var m1 = $(".m .m1 span").html();
	var m2 = $(".m .m2 span").html();
	var m = m1 + m2;
	
	var s1 = $(".s .s1 span").html();
	var s2 = $(".s .s2 span").html();
	var s = s1 + s2;

	if (s == 0) {
		if (m == 0) {
			if (h == 0) {
				h = 0; 
			}
			h--;
			m = 60;
			if (h < 10) h = "0" + h;
		}
		m--;
		if (m < 10) m = "0" + m;
		s = 59;
    }
    else s--;
    if (s < 10) s = "0" + s;
	if (h == 0 && m == 0 && s == 0) countDown = false;
	
	h = h.toString().split("");
	m = m.toString().split("");
	s = s.toString().split("");
	
	$(".h .h1").html("<span>"+h[0]+"</span>");
	$(".h .h2").html("<span>"+h[1]+"</span>");
	
	$(".m .m1").html("<span>"+m[0]+"</span>");
	$(".m .m2").html("<span>"+m[1]+"</span>");
	
	$(".s .s1").html("<span>"+s[0]+"</span>");
	$(".s .s2").html("<span>"+s[1]+"</span>");
	
	if (countDown == true) setTimeout(startTimer, 1000);
}

$(document).ready(function() {
	$(document).mouseleave(function(e) {
		if(e.pageY <= 2 && modalIsOpen == false) {
			if(!$(".present-modal3").hasClass("in")) {
				//$(".call-modal-link").trigger("click");
				startTimer();
				$("#present-modal3").modal("show");
				modalIsOpen = true;
				$(".right-page-form").css("visibility", "hidden");
			}
		}
	});

	$(document).click(function() {
		if(!$(".present-modal3").hasClass("in")) {
			$(".right-page-form").css("visibility", "visible");
		}
	});
});
