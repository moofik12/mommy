$(function () {

    var Callcenter = {};
    Callcenter.warning = (function () {
        var $wrapper = $('<div class="callcenter-warnings"></div>');
        var $body = $(document.body);

        if (0 === $body.children('.' + $wrapper[0].className).length) {
            $body.append($wrapper)
        }

        return function ($text) {
            var $message = $('<div class="callcenter-warning alert alert-error"><a class="close" data-dismiss="alert" href="#">&times;</a></div>');
            var $item = $('<div>' + $text + '</div>');

            $message.append($item);
            $wrapper.append($message);
        }
    })();

    window.callcenter = Callcenter;

    //отправка AJAX запроса редактирования заказа каждые 2-е минуты
    setInterval(function () {
        var url = window.location.href;
        url = url.replace('processing', 'processingActiveUpdate');
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data, textStatus) {
                if (!data.success || 'success' !== textStatus) {
                    alert('Ошибка при сохранении статуса редактирования');
                }
            },
            error: function () {
                alert('Невозможно обновить статус редактирования');
            }
        });
    }, 1000 * 60 * 2);

    var $contentbox = $("#contentbox");

    //событие при изменении типа доставки (нет привязки к значениям delivery_type)
    $contentbox.on("change", "form[name='data-order'] select[name*='delivery_type']", function () {
        var self = $(this),
            table = self.closest('table'),

            deliveryPrices = self.data('delivery-prices'),
            textDeliveryPrice = table.find('#delivery-price');

        $("form[name='data-order'] [data-for-delivery]").each(function (key, item) {
            var $item = $(item), $container = $item.attr('data-container'), name;

            $container = $container ? $item.closest($container) : $item;

            name = $item.data('name') || $item.attr('name');

            if (parseInt($item.attr('data-for-delivery')) !== parseInt(self.val())) {
                $container.hide();
                $item.data('name', name);
                $item.attr('name', null);
            } else {
                $item.attr('name', name);
                $container.show();
            }
        });

        var deliveryPrice = deliveryPrices[self.val()] !== 'undefined' ? deliveryPrices[self.val()] : 0;
        textDeliveryPrice.html(deliveryPrice);

        self.data('delivery-price', deliveryPrice);
        self.closest('#contentbox').find('.extended-summary').trigger("total.update", {"deliveryType": self.val()});
    });
    $("form[name='data-order'] select[name*='delivery_type']").trigger('change'); //force change

    $("form[name='data-order'] select[name*='[deliveryAttributes][service]']").trigger('change'); //force change

    //измененние промокода
    $contentbox.on("change", "form[name='data-order'] input[name*='promocode']", function () {
        var self = $(this);

        $("#contentbox").find(".extended-summary").trigger("total.update", {"promocode": self.val()});
    });

    //предотвращение отправки форм в Корзине
    $contentbox.on("submit", ".cart-form-container form", function (event) {
        event.preventDefault();
    });

    //собития при нажатии на кнопки добавления, удаления кол-ва
    $contentbox.on("mouseup", ".cart-form-container form button", function () {
        var button = $(this),
            input = button.siblings("input[id^=number]");

        var disable = input.prop("disabled") || input.prop("readonly");
        if (disable) {
            return;
        }

        var buttonName = button.attr("name"),
            inputVal = parseInt(input.val());

        if ("minus" === buttonName) {
            if (inputVal > 1) {
                input.val(inputVal - 1).trigger("change");
            }

        } else if ("plus" === buttonName) {
            if (input.attr("max") >= inputVal + 1) {
                input.val(inputVal + 1).trigger("change");
            }
        }
    });

    //обновление цены доставки
    $contentbox.on("update.delivery.price", function () {
        var self = $(this),
            elDeliveryPrice = self.find('#delivery-price'),
            elDeliveryType = self.find('select[id*=delivery_type]'),
            dataPrices = elDeliveryType.data('delivery-prices');

        var elDeliveryTypeVal = elDeliveryType.val();
        elDeliveryPrice.html(dataPrices[elDeliveryTypeVal]);
    });

    //события при изменении кол-ва товаров
    $contentbox.on("change", ".cart-form-container form input[id^=number]", function () {
        var self = $(this);

        var disable = self.prop("disabled") || self.prop("readonly");
        if (disable) {
            return;
        }

        var cellSum = self.closest("td").siblings(".sum-column"),
            price = self.data("price"),
            total = (self.val() * price).toFixed(2);

        self.data("total", total);
        self.closest(".grid-view").siblings(".extended-summary").trigger("total.update");

        cellSum.html("<b>" + total + "</b>");
    });


    //изменение общей суммы товаров
    $contentbox.on("total.update", ".extended-summary", function (e, ajaxData) {
        ajaxData || (ajaxData = {});

        var self = $(this),
            grid = self.siblings(".grid-view"),
            bonuses = grid.data("bonuses"),
            inputs = grid.find("form input[id^=number]"),
            deliveryPrice = self.closest("#contentbox").find('form[name="data-order"] select[id*=delivery_type]').data('delivery-price'),
            total = 0,
            currency = grid.data("currency");

        var ajaxExtendData = grid.data('params');
        ajaxExtendData || (ajaxExtendData = {});

        grid.data('params', $.extend(ajaxExtendData, ajaxData));

        inputs.each(function (indx, element) {
            total += $(element).data("total") - 0;
        });

        self.find(".total-bonuses").html(bonuses.toFixed() + ' ' + currency);
        self.find(".total-sum").html(total.toFixed() + ' ' + currency);
        self.find(".total-pay-sum").html((total - bonuses).toFixed() + ' ' + currency);
        self.find(".total-delivery-sum").html((total + deliveryPrice).toFixed() + ' ' + currency);
        self.find(".total-delivery-price").html((deliveryPrice).toFixed() + ' ' + currency);
    });

    //отмена отправки формы в "истории" и копирование в форму доставки
    $contentbox.on("submit", ".grid-view form[name=copy-address]", function (e) {
        e.preventDefault();
        var historyForm = $(this),
            orderForm = historyForm.closest(".new-item-container").find("form[name=data-order]"),
            countUpdate = 0,
            historyData = [],
            historyInputsNames = [],
            elements = orderForm.prop("elements"),
            historyElements = historyForm.prop("elements");

        $(historyElements).each(function (index, element) {
            element = $(element);
            historyInputsNames.push(element.prop("name"));

            historyData.push({
                name: element.prop("name"),
                value: element.prop("value"),
                dataName: element.data("name")
            });
        });

        $(elements).map(function (index, element) {
            element = $(element);
            var name = element.prop("name"),
                findId = $.inArray(name, historyInputsNames);

            if (findId >= 0) {
                var find = historyData[findId];
                countUpdate++;

                if (element.data("select2") === undefined) {
                    element.val(find["value"]);
                } else {
                    if (find["value"].length > 0) {
                        element.select2("data", {id: find["value"], text: find["dataName"]});
                    } else {
                        element.select2("val", "").trigger("change");
                    }

                }
            }
        });

        if (countUpdate) {
            alert("Данные вставлены в форму заказа");
        } else {
            alert("Ошибка при вставке");
        }
    });

    //события кнопок изменения корзины
    $contentbox.on('click', '.grid-view[id^=grid-order] .button-column a', function (e) {
        e.preventDefault();
        var form = $(this).closest('tr').find('form'),
            message = form.siblings('div[class~=message-error]'),
            grid = form.closest('.grid-view');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: jQuery(this).attr('href'),
            data: form.serialize(),
            success: function (data, textStatus) {
                if ('success' !== textStatus) {
                    alert('Невозможно выполнить дествие.');
                    return;
                }
                var text = 'Нет данных в ответе';

                if (data.errors && $.isArray(data.errors) && data.errors.length > 0) {
                    text = '<ul>';
                    data.errors.forEach(function (error) {
                        text += '<li>' + error + '</li>';
                    });
                    text += '</ul>';
                } else if (data.success) {
                    grid.yiiGridView('update');
                    return;
                }

                message.fadeIn().html(text).delay(3000).fadeOut('slow');
            },
            error: function () {
                alert('Невозможно выполнить запрос');
            }
        });
    });

    $(document).on('total.update.start', function () {
        $('.show-update-process').addClass('callcenter-icon-cycle');
    });

    $(document).on('total.update.end', function () {
        $('.show-update-process').removeClass('callcenter-icon-cycle');
    });

    $(document).on('click', '.callcenter-update-button', function () {
        var grid = $('[id^="grid-order-product"][class*=grid-view]');
        if (grid) {
            grid.yiiGridView("update");
        }
    });
});
