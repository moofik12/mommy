$(document).ready(function () {
    whenHideModal();

    function whenHideModal() {
        $("#task_by_type").on("hide.bs.modal", function () {
            $(".task-order-card-question-type").css("border", "1px solid #cccccc");
            $(".task-order-card-question-description").css("border", "1px solid #cccccc");
            $(".users-drop-down").css("border", "1px solid #cccccc"); //task-board-drop-down
            $(".task-board-drop-down").css("border", "1px solid #cccccc"); //task-board-drop-down
        });
    }
});
