$(function () {
    $(document).on('click', 'input[class*="delete-photo"]', function () {
        if (confirm('Удалить?')) {
            var self = $(this);
            var data = self.data();
            var url = window.location.href;
            var routeParam = 'r=';
            var routePos = url.indexOf(routeParam);

            if (routePos != -1) {
                url = url.substring(0, routePos + routeParam.length) + 'product/photoDelete';
            } else {
                url = 'product/photoDelete';
            }

            $.ajax({
                type: 'GET',
                url: url,
                data: {'fileId': data.id},
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    if (data.success) {
                        self.closest('.current-image').remove();
                    }
                },
                error: function () {
                    alert('Ошибка при удалении!')
                }
            });
        }
    });
});
