$(function () {
    var mainForm = helperForm({selector: '#moneyControl', name: 'MoneyControlForm'});
    mainForm.bindDependence('price', 'input[name *= moneyExpected]', true, true);

    $(document).on('helperForm.add', function (e, data) {
        if (data) {
            mainForm.addItem(['items', data.order], data.trackcode, data.order, data.price);
        }
    });

    $('#moneyControlItem').on('click', 'input', function () {
        this.select();
    });
});
