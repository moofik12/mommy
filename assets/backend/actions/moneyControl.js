'use strict';

var helperForm = function (options) {
    var instOptions = {
        '_itemCss': 'item-form',

        'selector': '',
        'name': '',
        'itemCss': 'alert',
        'itemAlertCss': 'alert-info',
        'deleteButtonCss': 'delete-item',
        'template': '<div class="controls"><input data-price="{price}" name="{nameForm}{name}" value="{value}" type="hidden">'
        + '<strong>Заказ:</strong> {label} / <strong>ТТН:</strong> {value} / <strong>Сумма:</strong> {price}'
        + '<button class="btn btn-danger btn-small pull-right delete-item" type="button">Удалить</button>'
        + '</div>'
    };

    $.extend(instOptions, options);

    var instance = {
        '_dependencies': [],
        '_items': [],
        '_form': false,
        options: instOptions
    };

    instance._init = function () {
        var form = instance._form = $(instance.options.selector);
        if (form && form[0]) {
            form = form[0];
            var elements = $(form).find('.' + instance.options._itemCss);
            $.each(elements, function (i, el) {
                instance._bind(el, el.name);
            });

            $.each(form.elements, function (i, el) {
                instance._items.push(el.name);
            });
        }
    };

    /**
     * вставка нового элемента в форму
     * @param name
     * @param value
     * @param label
     * @param price
     */
    instance.addItem = function (name, value, label, price) {
        var form = instance._form;
        price = parseFloat(price).toFixed(2);

        if ($.isArray(name)) {
            name = '[' + name.join('][') + ']';
        }

        var nameIndex = instance.options.name + name;
        if (instance._items.indexOf(nameIndex) !== -1) {
            alert('Такая запись уже есть!');
            return;
        }

        if (form.length) {
            var template = instance._normalizeTemplate(name, value, label, price);
            var child = $(template).addClass(instance.options.itemCssClass);
            instance._bind(child, name, true);
            form.prepend(child);
            instance._items.push(nameIndex);
        }

        instance._callBinds();
    };

    /**
     * добавить событие при зменении элементов формы обновить поле
     * @param dataAttribute
     * @param selector
     * @param isInteger
     * @param rewrite
     */
    instance.bindDependence = function (dataAttribute, selector, isInteger, rewrite) {
        instance._dependencies.push(Array.prototype.slice.call(arguments, 0, arguments.length));
    };

    instance._dependence = function (dataAttribute, selector, isInteger, rewrite) {
        var form = instance._form;
        var elements = form.find('[data-' + dataAttribute + ']');
        var elementUpdate = form.find(selector);
        var value = isInteger ? 0 : '';

        $.each(elements, function (i, el) {
            var attrValue = $(el).data(dataAttribute);
            if (attrValue) {
                if (isInteger) {
                    attrValue = parseFloat(attrValue);
                }

                value += attrValue;
            }
        });

        if (elementUpdate && elementUpdate[0]) {
            var valueUpdate = elementUpdate[0].value;
            if (isInteger) {
                valueUpdate = parseFloat(valueUpdate);
            }

            valueUpdate += value;

            if (rewrite) {
                valueUpdate = value;
            }

            elementUpdate[0].value = isInteger ? valueUpdate.toFixed(2) : valueUpdate;
        }
    };

    /**
     * @private
     */
    instance._callBinds = function () {
        instance._dependencies.forEach(function (arg) {
            instance._dependence.apply(this, arg);
        });
    };

    instance._normalizeTemplate = function (name, value, label, price) {
        var template = instance.options.template;
        var reps = [
            ["{nameForm}", instance.options.name],
            ["{name}", name],
            ["{value}", value],
            ["{label}", label],
            ["{price}", price]
        ];

        reps.forEach(function (el) {
            template = template.replace(new RegExp(el[0], 'g'), el[1]);
        });

        return template;
    };

    instance._bind = function (el, name, newEl) {
        var $el = $(el);
        $el.addClass(instance.options.itemCss + ' ' + instance.options._itemCss);

        $el.on('click', '.' + instance.options.deleteButtonCss, function () {
            $el.remove();
            var nameIndex = instance.options.name + name;
            var index = instance._items.indexOf(nameIndex);

            if (index >= 0) {
                delete instance._items[index];
            }
            instance._callBinds();
        });

        if (newEl) {
            $el.addClass(instance.options.itemAlertCss);
            setTimeout(function () {
                $el.removeClass(instance.options.itemAlertCss);
            }, 2000);
        }
    };

    instance._init();
    return instance;
};

helperForm.message = function (text, hide, type) {
    type = type ? 'alert-' + type : 'alert-error';
    hide = typeof hide === "undefined" ? true : hide;

    var $newContainer = $('<div class="helper-form-massages" style="position: fixed; top: 100px; right: 20px; z-index: 2000; max-width: 400px; max-height: 500px; overflow-y: auto;"></div>');
    var $message = $('<div class="alert ' + type + '"><button type="button" class="close" data-dismiss="alert">×</button>' + text + '</div>');
    var $body = $('body');
    var $container;

    if ($body.children('.helper-form-massages').length == 0) {
        $body.append($newContainer);
        $container = $newContainer;
    }

    if (!$container) {
        $container = $body.children('.helper-form-massages');
    }

    $container.prepend($message);

    if (hide) {
        setTimeout(function () {
            $message.remove();
        }, 4000)
    }
};
