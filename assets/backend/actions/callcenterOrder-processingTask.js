$(function () {
//отправка AJAX запроса редактирования заказа каждые 2-е минуты
    setInterval(function () {
        var url = window.location.href;
        url = url.replace('processingTask', 'processingTaskActiveUpdate');
        $.ajax({
            url: url,
            type: 'GET',
//			dataType: 'json',
            success: function (data, textStatus) {
                if (!data.success || textStatus != 'success') {
                    alert('Ошибка при сохранении статуса редактирования');
                }
            },
            error: function (XHR) {
                alert('Невозможно обновить статус редактирования');
            }
        });
    }, 1000 * 60 * 2);
});
